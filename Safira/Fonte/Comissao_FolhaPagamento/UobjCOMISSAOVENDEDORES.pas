unit UobjCOMISSAOVENDEDORES;
Interface
Uses forms,rdprint,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,uobjvendedor,uobjpedido,uobjpendencia,uobjchequedevolvido,uobjlotecomissao;

Type
   TObjCOMISSAOVENDEDORES=class

          Public

                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                VENDEDOR:TOBJVENDEDOR;
                PEDIDO:TOBJPEDIDO;
                PENDENCIA:TOBJPENDENCIA;
                chequedevolvido:TOBJchequedevolvido;
                Lotepagamento:TobjLoteComissao;

                //LOTEPAGAMENTO:TOBJLOTEPAGCOMISSAO;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Comissao(parametro: string);
                Function Get_Comissao: string;
                Procedure Submit_Valor(parametro: string);

                Function Get_observacao:string;
                Procedure Submit_observacao(parametro:string);

                Function Get_Valor: string;
                Function Get_VALORCOMISSAO: string;
                
                procedure EdtVENDEDORExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtVENDEDORKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtVENDEDORKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;

                procedure EdtPEDIDOExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtPEDIDOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtPENDENCIAExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtPENDENCIAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                procedure EdtchequedevolvidoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtchequedevolvidoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                Function RetornaValorComissaoPedido(Ppedido:string):Currency;
                procedure opcoes(PLote: string);
         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Comissao:string;
               Valor:string;
               VALORCOMISSAO:string;
               observacao:string;

               //CODIFICA VARIAVEIS PRIVADAS
               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
               Procedure ReprocessaCOmissoes(Plote:string);
               Procedure ExtornaComissoes(Plote:string);
               Procedure ImprimeLoteAnalitico(pcodigo:string);


   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls
,uVENDEDOR
,uPEDIDO
,uPENDENCIA
,uchequedevolvido
, UMenuRelatorios, UMostraBarraProgresso,
  UReltxtRDPRINT;


{ TTabTitulo }


Function  TObjCOMISSAOVENDEDORES.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('VENDEDOR').asstring<>'')
        Then Begin
                 If (Self.VENDEDOR.LocalizaCodigo(FieldByName('VENDEDOR').asstring)=False)
                 Then Begin
                          Messagedlg('Vendedor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.VENDEDOR.TabelaparaObjeto;
        End;
        If(FieldByName('PEDIDO').asstring<>'')
        Then Begin
                 If (Self.PEDIDO.LocalizaCodigo(FieldByName('PEDIDO').asstring)=False)
                 Then Begin
                          Messagedlg('Pedido N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PEDIDO.TabelaparaObjeto;
        End;

        If(FieldByName('PENDENCIA').asstring<>'')
        Then Begin
                 If (Self.PENDENCIA.LocalizaCodigo(FieldByName('PENDENCIA').asstring)=False)
                 Then Begin
                          Messagedlg('Pend�ncia N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PENDENCIA.TabelaparaObjeto;
        End;

        If(FieldByName('chequedevolvido').asstring<>'')
        Then Begin
                 If (Self.chequedevolvido.LocalizaCodigo(FieldByName('chequedevolvido').asstring)=False)
                 Then Begin
                          Messagedlg('Cheque Devolvido N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.chequedevolvido.TabelaparaObjeto;
        End;

        If(FieldByName('lotepagamento').asstring<>'')
        Then Begin
                 If (Self.lotepagamento.LocalizaCodigo(FieldByName('lotepagamento').asstring)=False)
                 Then Begin
                          Messagedlg('Lote de Pagamento N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.lotepagamento.TabelaparaObjeto;
        End;

        Self.Comissao:=fieldbyname('Comissao').asstring;
        Self.Valor:=fieldbyname('Valor').asstring;
        Self.VALORCOMISSAO:=fieldbyname('VALORCOMISSAO').asstring;
        Self.observacao:=fieldbyname('observacao').asstring;
        result:=True;
     End;
end;


Procedure TObjCOMISSAOVENDEDORES.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('VENDEDOR').asstring:=Self.VENDEDOR.GET_CODIGO;
        ParamByName('PEDIDO').asstring:=Self.PEDIDO.GET_CODIGO;
        ParamByName('PENDENCIA').asstring:=Self.PENDENCIA.GET_CODIGO;
        ParamByName('chequedevolvido').asstring:=Self.chequedevolvido.GET_CODIGO;
        ParamByName('lotepagamento').asstring:=Self.lotepagamento.GET_CODIGO;
        ParamByName('Comissao').asstring:=virgulaparaponto(Self.Comissao);
        ParamByName('Valor').asstring:=virgulaparaponto(Self.Valor);
        ParamByName('observacao').asstring:=Self.observacao;
  End;
End;

//***********************************************************************

function TObjCOMISSAOVENDEDORES.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCOMISSAOVENDEDORES.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        VENDEDOR.ZerarTabela;
        PEDIDO.ZerarTabela;
        PENDENCIA.ZerarTabela;
        chequedevolvido.ZerarTabela;
        LotePagamento.zerartabela;
        Comissao:='';
        Valor:='';
        observacao:='';
        VALORCOMISSAO:='';
     End;
end;

Function TObjCOMISSAOVENDEDORES.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCOMISSAOVENDEDORES.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.VENDEDOR.LocalizaCodigo(Self.VENDEDOR.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Vendedor n�o Encontrado!';
      If (Self.PEDIDO.LocalizaCodigo(Self.PEDIDO.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Pedido n�o Encontrado!';

      If (Self.PENDENCIA.LocalizaCodigo(Self.PENDENCIA.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Pend�ncia n�o Encontrado!';


      if (Self.chequedevolvido.Get_CODIGO<>'')
      then Begin
              If (Self.chequedevolvido.LocalizaCodigo(Self.chequedevolvido.Get_CODIGO)=False)
              Then Mensagem:=mensagem+'/ Cheque Devolvido n�o Encontrado!';
      End;

      if (self.Lotepagamento.get_codigo<>'')
      Then Begin
                If (Self.Lotepagamento.LocalizaCodigo(Self.Lotepagamento.Get_CODIGO)=False)
                Then Mensagem:=mensagem+'/ Lote de Pagamento n�o Encontrado!';
      End;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCOMISSAOVENDEDORES.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.VENDEDOR.Get_Codigo<>'')
        Then Strtoint(Self.VENDEDOR.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Vendedor';
     End;
     try
        If (Self.PEDIDO.Get_Codigo<>'')
        Then Strtoint(Self.PEDIDO.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Pedido';
     End;

     try
        If (Self.PENDENCIA.Get_Codigo<>'')
        Then Strtoint(Self.PENDENCIA.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Pend�ncia';
     End;


     try
        If (Self.chequedevolvido.Get_Codigo<>'')
        Then Strtoint(Self.chequedevolvido.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Cheque Devolvido';
     End;

      try
        If (Self.lotepagamento.Get_Codigo<>'')
        Then Strtoint(Self.lotepagamento.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Lote de Pagamento';
     End;


     try
        Strtofloat(Self.Comissao);
     Except
           Mensagem:=mensagem+'/% Comiss�o';
     End;
     try
        Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCOMISSAOVENDEDORES.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCOMISSAOVENDEDORES.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCOMISSAOVENDEDORES.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro COMISSAOVENDEDORES vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,VENDEDOR,PEDIDO,PENDENCIA,chequedevolvido,Comissao,Valor,VALORCOMISSAO,lotepagamento,Observacao');
           SQL.ADD(' from  TABCOMISSAOVENDEDORES');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCOMISSAOVENDEDORES.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCOMISSAOVENDEDORES.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCOMISSAOVENDEDORES.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.VENDEDOR:=TOBJVENDEDOR.create;
        Self.PEDIDO:=TOBJPEDIDO.create;
        Self.PENDENCIA:=TOBJPENDENCIA.create;
        Self.chequedevolvido:=TOBJchequedevolvido.create;
        Self.Lotepagamento:=TobjLoteComissao.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABCOMISSAOVENDEDORES(CODIGO,VENDEDOR,PEDIDO');
                InsertSQL.add(' ,PENDENCIA,chequedevolvido,Comissao,Valor,lotepagamento,Observacao');
                InsertSQL.add(' )');
                InsertSQL.add('values (:CODIGO,:VENDEDOR,:PEDIDO,:PENDENCIA,:chequedevolvido,:Comissao');
                InsertSQL.add(' ,:Valor,:lotepagamento,:Observacao)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABCOMISSAOVENDEDORES set CODIGO=:CODIGO,VENDEDOR=:VENDEDOR');
                ModifySQL.add(',PEDIDO=:PEDIDO,PENDENCIA=:PENDENCIA,chequedevolvido=:chequedevolvido,Comissao=:Comissao');
                ModifySQL.add(',Valor=:Valor,lotepagamento=:lotepagamento,Observacao=:Observacao');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABCOMISSAOVENDEDORES where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCOMISSAOVENDEDORES.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCOMISSAOVENDEDORES.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCOMISSAOVENDEDORES');
     Result:=Self.ParametroPesquisa;
end;

function TObjCOMISSAOVENDEDORES.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Comiss�o de Vendedores ';
end;


function TObjCOMISSAOVENDEDORES.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENCOMISSAOVENDEDORES,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCOMISSAOVENDEDORES.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.objquerypesquisa);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.VENDEDOR.FREE;
    Self.PEDIDO.FREE;
    Self.PENDENCIA.FREE;
    Self.chequedevolvido.FREE;
    Self.Lotepagamento.Free;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCOMISSAOVENDEDORES.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCOMISSAOVENDEDORES.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjCOMISSAOVENDEDORES.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjCOMISSAOVENDEDORES.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjCOMISSAOVENDEDORES.Submit_Comissao(parametro: string);
begin
        Self.Comissao:=Parametro;
end;
function TObjCOMISSAOVENDEDORES.Get_Comissao: string;
begin
        Result:=Self.Comissao;
end;
procedure TObjCOMISSAOVENDEDORES.Submit_Valor(parametro: string);
begin
        Self.Valor:=Parametro;
end;
function TObjCOMISSAOVENDEDORES.Get_Valor: string;
begin
        Result:=Self.Valor;
end;
function TObjCOMISSAOVENDEDORES.Get_VALORCOMISSAO: string;
begin
        Result:=Self.VALORCOMISSAO;
end;
//CODIFICA GETSESUBMITS


procedure TObjCOMISSAOVENDEDORES.EdtVENDEDORExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.VENDEDOR.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.VENDEDOR.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.VENDEDOR.GET_NOME;
End;
procedure TObjCOMISSAOVENDEDORES.EdtVENDEDORKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FVENDEDOR:TFVENDEDOR;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FVENDEDOR:=TFVENDEDOR.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.VENDEDOR.Get_Pesquisa,Self.VENDEDOR.Get_TituloPesquisa,FVENDEDOR)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.VENDEDOR.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.VENDEDOR.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.VENDEDOR.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FVENDEDOR);
     End;
end;

procedure TObjCOMISSAOVENDEDORES.EdtVENDEDORKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.VENDEDOR.Get_Pesquisa,Self.VENDEDOR.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.VENDEDOR.RETORNACAMPOCODIGO).asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjCOMISSAOVENDEDORES.EdtPEDIDOExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PEDIDO.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PEDIDO.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.PEDIDO.Get_Descricao;
End;
procedure TObjCOMISSAOVENDEDORES.EdtPEDIDOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPEDIDO:TFPEDIDO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPEDIDO:=TFPEDIDO.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PEDIDO.Get_Pesquisa,Self.PEDIDO.Get_TituloPesquisa,FPEDIDO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PEDIDO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.PEDIDO.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PEDIDO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FPEDIDO);
     End;
end;
procedure TObjCOMISSAOVENDEDORES.EdtPENDENCIAExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PENDENCIA.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PENDENCIA.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.PENDENCIA.Get_Historico;
End;
procedure TObjCOMISSAOVENDEDORES.EdtPENDENCIAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPENDENCIA:=TFPENDENCIA.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PENDENCIA.Get_Pesquisa,Self.PENDENCIA.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PENDENCIA.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.PENDENCIA.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PENDENCIA.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//***********
procedure TObjCOMISSAOVENDEDORES.EdtchequedevolvidoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.chequedevolvido.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.chequedevolvido.tabelaparaobjeto;
End;

procedure TObjCOMISSAOVENDEDORES.EdtchequedevolvidoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.chequedevolvido.Get_Pesquisa,Self.chequedevolvido.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.chequedevolvido.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.chequedevolvido.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.chequedevolvido.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjCOMISSAOVENDEDORES.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCOMISSAOVENDEDORES';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Comiss�es do Lote Anal�tico');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
          0:Self.ImprimeLoteAnalitico(pcodigo);


          End;
     end;

end;


procedure TObjCOMISSAOVENDEDORES.opcoes(PLote: string);
begin
     With FOpcaorel do
     Begin

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Reprocessa Comiss�es');
                items.add('Extorna    Comiss�es');


          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:Self.ReprocessaCOmissoes(Plote);
            1:Self.ExtornaComissoes(plote);
          End;
     end;

end;



function TObjCOMISSAOVENDEDORES.RetornaValorComissaoPedido(
  Ppedido: string): Currency;
begin
     result:=0;
     if (Ppedido='')
     Then exit;

     With Self.Objquery do
     begin
          close;
          sql.clear;
          sql.add('Select sum(valorcomissao) as SOMA from tabComissaovendedores where Pedido='+Ppedido);
          open;
          result:=Fieldbyname('soma').asfloat;
          close;
     End;


end;

procedure TObjCOMISSAOVENDEDORES.ReprocessaCOmissoes(Plote: string);
var
DataLote:string;
paga,PagaComissaoDescontoTotal:boolean;
ObjQLocal:Tibquery;
Begin
     PagaComissaoDescontoTotal:=true;
     {if (ObjParametroGlobal.ValidaParametro('PAGAR COMISSAO PARA PEND�NCIAS QUE SOFRERAM DESCONTO TOTAL?')=False)
     Then exit;

     if (ObjParametroGlobal.Get_Valor='SIM')
     Then PagaComissaoDescontoTotal:=True;
     }

     //********************************************************************************
       
     if (Self.LotePagamento.LocalizaCodigo(plote)=False)
     Then Begin
               Messagedlg('Lote n�o encontrado!',mterror,[mbok],0);
               exit;
     End;
     Self.LotePagamento.TabelaparaObjeto;

     if (Self.Lotepagamento.Get_Concluido='S')
     Then Begin
               MensagemErro('O lote j� foi conclu�do');
               exit;
     End;

     DataLote:=Self.LotePagamento.Get_Data;
     
     

     If (PLote='')
     Then Begin
               Messagedlg('Escolha um Lote para Gerar a Comiss�o!',mterror,[mbok],0);
               exit;
     End;

     With Objquery do
     Begin
          close;
          SQL.clear;
          Try
             ObjQLocal:=Tibquery.create(nil);
             ObjQLocal.database:=Self.Objquery.Database;

          Except
                Messagedlg('Erro na tentativa de Criar a Query',mterror,[mbok],0);
                exit;
          End;

          Try
            //aqui soh pega as pendencias onde o saldo ta zero e teve quitacao
            FMostraBarraProgresso.BarradeProgresso.Progress:=0;
            FMostraBarraProgresso.BarradeProgresso.MinValue:=0;
            FMostraBarraProgresso.Lbmensagem.Caption:='PROCESSANDO PEND�NCIA SEM SALDO';
            FMostraBarraProgresso.show;

            SQL.add('select tabcomissaovendedores.codigo,tabpendencia.codigo as PENDENCIA,tabpendencia.titulo,tabpendencia.boletobancario,tabpendencia.duplicata,tabpendencia.saldo,');
            SQL.add('Tabboletobancario.datadoc as databoleto,');
            SQL.add('cast(tabduplicata.datac as date) as dataduplicata,tabpedido.titulo');
            SQL.add('from tabcomissaovendedores');
            SQL.add('join tabpendencia on tabcomissaovendedores.pendencia=tabpendencia.codigo');
            SQL.add('join tabpedido on tabcomissaovendedores.pedido=tabpedido.codigo');
            SQL.add('left join tabboletobancario on Tabboletobancario.codigo=tabpendencia.boletobancario');
            SQL.add('left join tabduplicata on tabpendencia.duplicata=tabduplicata.codigo'); 
            SQL.add('where tabcomissaovendedores.LotePagamento is null and Tabpendencia.Saldo=0');
            SQL.add('and TabPedido.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(DataLote))+#39);
            //sql.SaveToFile('c:\comissao.sql');
            open;

            last;
            FMostraBarraProgresso.BarradeProgresso.MaxValue:=RecordCount;
            first;
            While not(eof) do
            Begin
                 FMostraBarraProgresso.BarradeProgresso.Progress:=FMostraBarraProgresso.BarradeProgresso.Progress+1;
                 FMostraBarraProgresso.Refresh;
                 Application.processmessages;

                 ObjQLocal.close;
                 ObjQLocal.sql.clear;
                 ObjQLocal.sql.add('Select psomaquitacao,pultimaquitacao from prochistoricopendtit('+Self.Objquery.fieldbyname('titulo').asstring+')');
                 ObjQLocal.sql.add('where pcodigo='+Self.Objquery.fieldbyname('pendencia').asstring);
                 ObjQLocal.open;

                 Paga:=false;

                 if (fieldbyname('saldo').ascurrency=0)
                 Then Begin
                           //nao tem boleto nem duplicata mas o saldo esta zero
                           if (strtofloat(ObjQLocal.FieldByName('psomaquitacao').asstring)>0) //nao teve desconto total
                           Then Begin
                                     if (ObjQlocal.Fieldbyname('pultimaquitacao').asdatetime<=strtodate(datalote))
                                     Then paga:=True;
                           End
                           else Begin
                                    if (PagaComissaoDescontoTotal=True)
                                    Then paga:=true;
                           End;
                 End;

                 if (Paga=true)
                 Then Begin
                           ObjQLocal.close;
                           ObjQLocal.sql.clear;
                           ObjQLocal.sql.add('Update tabcomissaovendedores set LotePagamento='+Plote+' where');
                           ObjQLocal.sql.add('codigo='+Self.Objquery.fieldbyname('codigo').asstring);
                           ObjQLocal.ExecSQL;
                 End;
                 Self.Objquery.Next;
            End;
            //*****************************************************************

            Self.Lotepagamento.Submit_Concluido('S');
            Self.Lotepagamento.status:=dsedit;

            if (self.Lotepagamento.salvar(False)=False)
            Then Begin
                      MensagemErro('Erro na tentativa de Salvar o Lote de Pagamento');
                      exit;
            End;
            FdataModulo.IBTransaction.CommitRetaining;
            FMostraBarraProgresso.close;
            Messagedlg('Comiss�o gerada com Sucesso!',mtinformation,[mbok],0);
          Finally
                 Freeandnil(ObjQLocal);
          End;
          
     End;
End;

procedure TObjCOMISSAOVENDEDORES.ExtornaComissoes(Plote: string);
Begin

     If (PLote='')
     Then Begin
               Messagedlg('Escolha um Lote para Gerar a Comiss�o!',mterror,[mbok],0);
               exit;
     End;

     if (Self.Lotepagamento.LocalizaCodigo(plote)=False)
     Then Begin
               mensagemerro('Lote n�o encontrado');
               exit;
     End;
     Self.Lotepagamento.TabelaparaObjeto;

     if (lotepagamento.get_concluido='N')
     Then Begin
               mensagemerro('O lote n�o foi conclu�do');
               exit;
     End;

try
     With Self.Objquery do
     Begin
          close;
          SQL.clear;
          SQL.add('Update TabComissaovendedores set Lotepagamento=null where Lotepagamento='+plote);
          ExecSQL;

          Self.Lotepagamento.Status:=dsedit;
          Self.Lotepagamento.submit_concluido('N');
          if (Self.Lotepagamento.Salvar(False)=False)
          Then begin
                    Mensagemerro('Erro na tentativa de passar o lote para Concluido=N');
                    exit;
          End;
          FDataModulo.IBTransaction.CommitRetaining;
     End;
Finally
       FDataModulo.IBTransaction.RollbackRetaining;
End;

End;

procedure TObjCOMISSAOVENDEDORES.ImprimeLoteAnalitico(pcodigo: string);
var
pvendedor:string;
psomatotal,psomavendedor:currency;
begin
     if (pcodigo='')
     then Begin
               mensagemAviso('Escolha primeiramente o lote');
               exit;
     End;

     if (self.Lotepagamento.LocalizaCodigo(pcodigo)=False)
     then Begin
               Mensagemerro('Lote n�o localizado');
               exit;
     End;
     Self.Lotepagamento.TabelaparaObjeto;

     with FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.enabled:=true;
          lbgrupo01.caption:='Vendedor';
          edtgrupo01.OnKeyDown:=Self.EdtVENDEDORKeyDown;

          showmodal;

          if (Tag=0)
          Then exit;

          pvendedor:='';
          if (edtgrupo01.text<>'')
          Then Begin
                    if (Self.VENDEDOR.LocalizaCodigo(edtgrupo01.text)=false)
                    Then Begin
                              MensagemAviso('Vendedor n�o localizado');
                              exit;
                    End;
                    Self.VENDEDOR.TabelaparaObjeto;
                    pvendedor:=Self.vendedor.get_codigo;
          End;

     End;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select');
          sql.add('tabcomissaovendedores.pedido,');
          sql.add('tabpedido.data,');
          sql.add('Tabpedido.vendedor,');
          sql.add('Tabvendedor.nome as nomevendedor,');
          sql.add('tabpedido.cliente,');
          sql.add('tabcliente.nome as nomecliente,');
          sql.add('tabcomissaovendedores.comissao,');
          sql.add('tabcomissaovendedores.valor,');
          sql.add('tabcomissaovendedores.valorcomissao');
          sql.add('from tabcomissaovendedores');
          sql.add('join tabvendedor on tabcomissaovendedores.vendedor=tabvendedor.codigo');
          sql.add('join tabpedido on tabcomissaovendedores.pedido=tabpedido.codigo');
          sql.add('join tabpendencia on tabcomissaovendedores.pendencia=tabpendencia.codigo');
          sql.add('join tabcliente on tabpedido.cliente=tabcliente.codigo');
          sql.add('where tabcomissaovendedores.lotepagamento='+pcodigo);

          if (Pvendedor<>'')
          then sql.add('and TabComissaoVendedores.vendedor='+pvendedor);

          sql.add('order by Tabcomissaovendedores.vendedor,tabpedido.data');



          open;
          last;
          if (recordcount=0)
          then Begin
                    mensagemAviso('Nenhuma informa��o foi selecionada');
                    exit;
          End;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.lbmensagem.caption:='Gerando Relat�rio';
          first;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               RDprint.FonteTamanhoPadrao:=s17cpp;
               RDprint.TamanhoQteColunas:=130;
               linhalocal:=3;
               rdprint.Abrir;

               if (RDprint.setup=False)
               then BEgin
                         RDprint.fechar;
                         exit;
               end;
               rdprint.impc(linhalocal,65,'COMISS�ES ANAL�TICAS DO LOTE '+Pcodigo+' - '+Self.Lotepagamento.get_data,[negrito]);
               IncrementaLinha(2);

               if (pvendedor<>'')
               Then Begin
                         rdprint.impf(linhalocal,1,'Vendedor: '+pvendedor+'-'+Self.vendedor.get_nome,[negrito]);
                         IncrementaLinha(1);
               End;

               VerificaLinha;
               RDprint.impf(linhalocal,1,CompletaPalavra('PEDIDO',6,' ')+' '+
                                         CompletaPalavra('DATA',10,' ')+' '+
                                         CompletaPalavra('CLIENTE',50,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VL COMISSAO',12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;


               pvendedor:='';
               psomavendedor:=0;
               psomatotal:=0;
               while not(Self.Objquery.eof) do
               Begin

                    if (pvendedor<>fieldbyname('vendedor').asstring)
                    then Begin
                              if (pvendedor<>'')
                              then Begin
                                        IncrementaLinha(1);
                                        VerificaLinha;
                                        rdprint.impf(linhalocal,1,'Soma da Comiss�o do Vendedor: '+formata_valor(psomavendedor),[negrito]);
                                        IncrementaLinha(1);

                              End;
                              pvendedor:=fieldbyname('vendedor').asstring;
                              psomavendedor:=0;

                              IncrementaLinha(1);
                              VerificaLinha;
                              rdprint.impf(linhalocal,1,'Vendedor: '+fieldbyname('vendedor').asstring+' '+fieldbyname('nomevendedor').asstring,[negrito]);
                              IncrementaLinha(2);

                    End;

                    VerificaLinha;
                    RDprint.imp(linhalocal,1,CompletaPalavra(fieldbyname('pedido').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('data').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('cliente').asstring+' '+fieldbyname('nomecliente').asstring,50,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorcomissao').asstring),12,' '));
                    IncrementaLinha(1);
                    
                   psomavendedor:=psomavendedor+fieldbyname('valorcomissao').ascurrency;
                   psomatotal:=psomatotal+fieldbyname('valorcomissao').ascurrency;
                   Self.Objquery.next;
               End;

               IncrementaLinha(1);
               VerificaLinha;
               rdprint.impf(linhalocal,1,'Soma da Comiss�o do Vendedor: '+formata_valor(psomavendedor),[negrito]);
               IncrementaLinha(2);
               DesenhaLinha;

               VerificaLinha;
               rdprint.impf(linhalocal,1,'Soma Total da Comiss�o: '+formata_valor(psomatotal),[negrito]);
               IncrementaLinha(2);




               rdprint.fechar;
          End;
          FMostraBarraProgresso.close;
     End;
end;

function TObjCOMISSAOVENDEDORES.Get_observacao: string;
begin
     Result:=Self.observacao;
end;

procedure TObjCOMISSAOVENDEDORES.Submit_observacao(parametro: string);
begin
     Self.observacao:=parametro;
end;

end.




