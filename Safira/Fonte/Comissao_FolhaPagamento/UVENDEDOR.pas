unit UVENDEDOR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls,db, UObjVENDEDOR,
  UessencialGlobal, Tabs,IBQuery,UFuncionarios,UFAIXADESCONTO;

type
  TFVENDEDOR = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigoVendedor: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    lb2: TLabel;
    edtcodigofuncionario: TEdit;
    EdtNome: TEdit;
    lbLbNome: TLabel;
    lbLbSexo: TLabel;
    cbbComboSexo: TComboBox;
    EdtEmail: TEdit;
    lbLbEmail: TLabel;
    lbLbFone: TLabel;
    EdtFone: TEdit;
    EdtCelular: TEdit;
    lbLbCelular: TLabel;
    lbLbDataCadastro: TLabel;
    edtDataCadastro: TMaskEdit;
    edtDataNascimento: TMaskEdit;
    lbLbDataNascimento: TLabel;
    lbLbAtivo: TLabel;
    lbLbObservacao: TLabel;
    mmoObservacao: TMemo;
    cbbComboAtivo: TComboBox;
    lb3: TLabel;
    lb4: TLabel;
    edtsenharelatorios: TEdit;
    edtfaixadesconto: TEdit;
    pnl1: TPanel;
    imgrodape: TImage;
    lb14: TLabel;
    btAjuda: TSpeedButton;
    lbNomeFuncionario: TLabel;
    lbFaixaDesconto: TLabel;
//DECLARA COMPONENTES

    Procedure PosicionaForm;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtcodigofuncionarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigofuncionarioExit(Sender: TObject);
    procedure edtfaixadescontoExit(Sender: TObject);
    procedure edtfaixadescontoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigofuncionarioKeyPress(Sender: TObject; var Key: Char);
    procedure edtfaixadescontoKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure btAjudaClick(Sender: TObject);
    procedure lbNomeFuncionarioClick(Sender: TObject);
    procedure lbNomeFuncionarioMouseLeave(Sender: TObject);
    procedure lbNomeFuncionarioMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure lbFaixaDescontoClick(Sender: TObject);
  private
    ObjVENDEDOR:TObjVENDEDOR;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    procedure MostraQuantidadeVendedores;
    { Private declarations }
  public

  end;

var
  FVENDEDOR: TFVENDEDOR;


implementation

uses Upesquisa, UessencialLocal, UDataModulo, UescolheImagemBotao,Uprincipal,
  UAjuda;

{$R *.dfm}




procedure TFVENDEDOR.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjVENDEDOR=Nil)
     Then exit;

     If (Self.ObjVENDEDOR.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjVENDEDOR.free;
    Self.Tag:=0;
end;


procedure TFVENDEDOR.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbCodigoVendedor.caption:='0';
     //edtcodigo.text:=ObjVENDEDOR.Get_novocodigo;
    // edtcodigo.enabled:=False;
     EdtDataCadastro.Text:=datetostr(now);

     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjVENDEDOR.status:=dsInsert;

     btalterar.visible:=false;
     btrelatorio.visible:=false;
     btsair.Visible:=false;
     btopcoes.visible:=false;
     btexcluir.Visible:=false;
     btnovo.Visible:=false;

     EdtCodigoFuncionario.setfocus;

end;

procedure TFVENDEDOR.btSalvarClick(Sender: TObject);
begin

     If ObjVENDEDOR.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjVENDEDOR.salvar(true)=False)
     Then exit;

    lbCodigoVendedor.caption:=ObjVENDEDOR.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btalterar.visible:=true;
     btrelatorio.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
     MostraQuantidadeVendedores;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFVENDEDOR.btAlterarClick(Sender: TObject);
begin
    If (ObjVENDEDOR.Status=dsinactive) and (lbCodigoVendedor.caption<>'')
    Then Begin
                habilita_campos(Self);
                //EdtCodigo.enabled:=False;
                ObjVENDEDOR.Status:=dsEdit;
                edtCodigoFuncionario.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
               btalterar.visible:=false;
               btrelatorio.visible:=false;
               btsair.Visible:=false;
               btopcoes.visible:=false;
               btexcluir.Visible:=false;
               btnovo.Visible:=false;
          End;

end;

procedure TFVENDEDOR.btCancelarClick(Sender: TObject);
begin
     ObjVENDEDOR.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btalterar.visible:=true;
     btrelatorio.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:= true;

end;

procedure TFVENDEDOR.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjVENDEDOR.Get_pesquisa,ObjVENDEDOR.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjVENDEDOR.status<>dsinactive
                                  then exit;

                                  If (ObjVENDEDOR.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjVENDEDOR.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFVENDEDOR.btExcluirClick(Sender: TObject);
begin
     If (ObjVENDEDOR.status<>dsinactive) or (lbCodigoVendedor.caption='')
     Then exit;

     If (ObjVENDEDOR.LocalizaCodigo(lbCodigoVendedor.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjVENDEDOR.exclui(lbCodigoVendedor.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFVENDEDOR.btRelatorioClick(Sender: TObject);
begin
//    ObjVENDEDOR.Imprime(lbCodigoVendedor.caption);
end;

procedure TFVENDEDOR.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFVENDEDOR.PosicionaForm;
begin
    Self.Caption:='      '+Self.Caption;
    Self.Left:=-18;
    Self.Top:=0;
end;
procedure TFVENDEDOR.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFVENDEDOR.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFVENDEDOR.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
    if(key=vk_f1) then
    begin
        Fprincipal.chamaPesquisaMenu;
    end;
    If (Key = VK_F2) then
    begin

           FAjuda.PassaAjuda('CADASTRO DE VENDEDOR');
           FAjuda.ShowModal;
    end;

end;

procedure TFVENDEDOR.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFVENDEDOR.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjVENDEDOR do
    Begin
        Submit_Codigo(lbCodigoVendedor.caption);
        Submit_Nome(edtNome.text);
        Submit_Sexo(cbbComboSexo.text);
        Submit_Email(edtEmail.text);
        Submit_Fone(edtFone.text);
        Submit_Celular(edtCelular.text);
        Submit_DataCadastro(edtDataCadastro.text);
        Submit_DataNascimento(edtDataNascimento.text);
        Submit_Ativo(cbbComboAtivo.text);
        Submit_Observacao(mmoObservacao.text);
        CodigoFuncionario.Submit_CODIGO(edtcodigofuncionario.text);
        FaixaDesconto.Submit_CODIGO(edtfaixadesconto.Text);
        Submit_senharelatorios(edtsenharelatorios.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFVENDEDOR.LimpaLabels;
begin
//LIMPA LABELS
  lbNomeFuncionario.caption:='';
  lbFaixaDesconto.Caption:='';
end;

function TFVENDEDOR.ObjetoParaControles: Boolean;
begin
  Try
     With ObjVENDEDOR do
     Begin
        lbCodigoVendedor.caption:=Get_Codigo;
        EdtNome.text:=Get_Nome;
        cbbComboSexo.text:=Get_Sexo;
        EdtEmail.text:=Get_Email;
        EdtFone.text:=Get_Fone;
        EdtCelular.text:=Get_Celular;
        EdtDataCadastro.text:=Get_DataCadastro;
        EdtDataNascimento.text:=Get_DataNascimento;
        cbbComboAtivo.text:=Get_Ativo;
        MmoObservacao.text:=Get_Observacao;
        edtcodigofuncionario.text:=CodigoFuncionario.Get_CODIGO;
        edtfaixadesconto.Text:=FaixaDesconto.Get_CODIGO;
        edtsenharelatorios.text:=Get_senharelatorios;
        lbNomeFuncionario.Caption:=CodigoFuncionario.Get_Nome;
        lbFaixaDesconto.Caption:=FaixaDesconto.Get_Nome;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFVENDEDOR.TabelaParaControles: Boolean;
begin
     If (ObjVENDEDOR.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;


procedure TFVENDEDOR.edtcodigofuncionarioKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:TFpesquisa;
   Ffuncionario:TFfuncionarios;
begin
     if (key<>vk_f9)
     Then exit;

     Try
           Fpesquisalocal:=Tfpesquisa.create(nil);
            Ffuncionario:=TFfuncionarios.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabfuncionarios','Pesquisa de Cidades',Ffuncionario)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  edtcodigofuncionario.Text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                                  EdtNome.Text:=FpesquisaLocal.querypesq.fieldbyname('nome').asstring;
                                  EdtFone.text:=FpesquisaLocal.querypesq.fieldbyname('fone').asstring;
                                  EdtCelular.Text:=FpesquisaLocal.querypesq.fieldbyname('celular').AsString;
                                  edtDataNascimento.Text:=FpesquisaLocal.querypesq.fieldbyname('datanascimento').AsString;
                                  EdtCelular.Text:=FpesquisaLocal.querypesq.fieldbyname('celular').AsString;
                                  EdtFone.Text:=FpesquisaLocal.querypesq.fieldbyname('fone').AsString;

                        End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;

                      End;
                 End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Ffuncionario);

     End;
end;

{begin
     //ObjVENDEDOR.EdtCodigoFuncionarioKeyDown(sender,key,shift,nil);
end; }

procedure TFVENDEDOR.edtcodigofuncionarioExit(Sender: TObject);
begin
     ObjVENDEDOR.EdtCodigoFuncionarioExit(sender,nil);
end;

procedure TFVENDEDOR.edtfaixadescontoExit(Sender: TObject);
begin
     ObjVENDEDOR.EdtFaixaDescontoExit(sender,nil);
end;

procedure TFVENDEDOR.edtfaixadescontoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjVENDEDOR.EdtFaixaDescontoKeyDown(sender,key,shift,nil); 
end;

procedure TFVENDEDOR.edtcodigofuncionarioKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (Key in['0'..'9',Chr(8)]) then
     begin
            Key:= #0;
     end;
end;

procedure TFVENDEDOR.edtfaixadescontoKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
       begin
              Key:= #0;
       end;
end;

procedure TFVENDEDOR.MostraQuantidadeVendedores;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabvendedor');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb14.Caption:='Existem '+IntToStr(contador)+' vendedores cadastrados'
       else
       begin
            lb14.Caption:='Existe '+IntToStr(contador)+' vendedor cadastrado';
       end;

    finally

    end;


end;


procedure TFVENDEDOR.FormShow(Sender: TObject);
begin

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     ColocaUpperCaseEdit(Self);

     Try
        Self.ObjVENDEDOR:=TObjVENDEDOR.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     lbCodigoVendedor.caption:='';


     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE VENDEDORES')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);
     MostraQuantidadeVendedores;

     if(Tag<>0)
     then
     begin
          if(ObjVENDEDOR.LocalizaCodigo(IntToStr(Tag))=True)
          then begin
            ObjVENDEDOR.TabelaparaObjeto;
            Self.ObjetoParaControles;
          end;
     end;

end;



procedure TFVENDEDOR.btAjudaClick(Sender: TObject);
begin
     FAjuda.PassaAjuda('CADASTRO DE VENDEDOR');
     FAjuda.ShowModal;
end;

procedure TFVENDEDOR.lbNomeFuncionarioClick(Sender: TObject);
var
  FFuncionarios:TFfuncionarios;
begin

  try
    FFuncionarios:=TFfuncionarios.Create(nil);
  except
    Exit;
  end;

  try
    if(edtcodigofuncionario.Text='')
    then Exit;

    FFuncionarios.Tag:=StrToInt(edtcodigofuncionario.Text);
    FFuncionarios.ShowModal;
  finally
    FreeAndNil(FFuncionarios);
  end;



end;

procedure TFVENDEDOR.lbNomeFuncionarioMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFVENDEDOR.lbNomeFuncionarioMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
       TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFVENDEDOR.lbFaixaDescontoClick(Sender: TObject);
var
  FFaixaDesconto:TFFAIXADESCONTO;
begin
  try
    FFaixaDesconto:=TFFAIXADESCONTO.Create(nil);
  except
     Exit;
  end;

  try
    if(edtfaixadesconto.Text='')
    then Exit;

    FFaixaDesconto.Tag:=StrToInt(edtfaixadesconto.Text);
    FFaixaDesconto.ShowModal;
  finally
    FreeAndNil(FFaixaDesconto);
  end;


end;

end.
