unit USALARIOFAMILIA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjSALARIOFAMILIA,
  UessencialGlobal,UessencialLocal, Tabs;

type
  TFSALARIOFAMILIA = class(TForm)
    btNovo: TSpeedButton;
    btFechar: TSpeedButton;
    btMinimizar: TSpeedButton;
    btSalvar: TSpeedButton;
    btAlterar: TSpeedButton;
    btCancelar: TSpeedButton;
    btSair: TSpeedButton;
    btRelatorio: TSpeedButton;
    btExcluir: TSpeedButton;
    btPesquisar: TSpeedButton;
    lbAjuda: TLabel;
    btAjuda: TSpeedButton;
    PainelExtra: TPanel;
    ImageGuiaPrincipal: TImage;
    ImageGuiaExtra: TImage;
    PainelPrincipal: TPanel;
    ImagePainelPrincipal: TImage;
    ImagePainelExtra: TImage;
    Guia: TTabSet;
    Notebook: TNotebook;
    Bevel: TBevel;
    LbCODIGO: TLabel;
    LbSalarioInicial: TLabel;
    LbSalarioFinal: TLabel;
    LbValorporDependente: TLabel;
    EdtCODIGO: TEdit;
    EdtSalarioInicial: TEdit;
    EdtSalarioFinal: TEdit;
    EdtValorporDependente: TEdit;
//DECLARA COMPONENTES

    procedure FormCreate(Sender: TObject);
    Procedure PosicionaPaineis;
    Procedure PegaFiguras;
    Procedure ColocaAtalhoBotoes;
    Procedure PosicionaForm;
    procedure FormShow(Sender: TObject);
    procedure ImageGuiaPrincipalClick(Sender: TObject);
    procedure ImageGuiaExtraClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSALARIOFAMILIA: TFSALARIOFAMILIA;
  ObjSALARIOFAMILIA:TObjSALARIOFAMILIA;

implementation

uses Upesquisa;

{$R *.dfm}


procedure TFSALARIOFAMILIA.FormCreate(Sender: TObject);
var
  Points: array [0..15] of TPoint;
  Regiao1:HRgn;
begin

Points[0].X :=305;      Points[0].Y := 22;
Points[1].X :=286;      Points[1].Y := 3;
Points[2].X :=41;       Points[2].Y := 3;
Points[3].X :=41;       Points[3].Y := 22;
Points[4].X :=28;       Points[4].Y := 35;
Points[5].X :=28;       Points[5].Y := 164;
Points[6].X :=41;       Points[6].Y := 177;
Points[7].X :=41;       Points[7].Y := 360;
Points[8].X :=62;       Points[8].Y := 381;
Points[9].X :=632;      Points[9].Y := 381;
Points[10].X :=647;     Points[10].Y := 396;
Points[11].X :=764;     Points[11].Y := 396;
Points[12].X :=764;     Points[12].Y := 95;
Points[13].X :=780;     Points[13].Y := 79;
Points[14].X :=780;     Points[14].Y := 22;
Points[15].X :=305;     Points[15].Y := 22;

Regiao1:=CreatePolygonRgn(Points, 16, WINDING);
SetWindowRgn(Self.Handle, regiao1, False);


limpaedit(Self);
Self.limpaLabels;
desabilita_campos(Self);
Guia.TabIndex:=0;

Try
   ObjSALARIOFAMILIA:=TObjSALARIOFAMILIA.create;
Except
      Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
      Self.close;
End;

end;

procedure TFSALARIOFAMILIA.PosicionaPaineis;
begin
  With Self.PainelPrincipal do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelPrincipal do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  With Self.PainelExtra  do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelExtra  do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  Self.PainelPrincipal.Enabled:=true;
  Self.PainelPrincipal.Visible:=true;
  Self.PainelExtra.Enabled:=false;
  Self.PainelExtra.Visible:=false;
end;

procedure TFSALARIOFAMILIA.FormShow(Sender: TObject);
begin
    PegaCorForm(Self);
    Self.PosicionaPaineis;
    Self.PosicionaForm;
    Self.PegaFiguras;
    Self.ColocaAtalhoBotoes;

end;

procedure TFSALARIOFAMILIA.ImageGuiaPrincipalClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=true;
PainelPrincipal.Visible:=true;
PainelExtra.Enabled:=false;
PainelExtra.Visible:=false;
end;

procedure TFSALARIOFAMILIA.ImageGuiaExtraClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=false;
PainelPrincipal.Visible:=false;
PainelExtra.Enabled:=true;
PainelExtra.Visible:=true;

end;

procedure TFSALARIOFAMILIA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjSALARIOFAMILIA=Nil)
     Then exit;

     If (ObjSALARIOFAMILIA.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjSALARIOFAMILIA.free;
    Action := caFree;
    Self := nil;
end;

procedure TFSALARIOFAMILIA.PegaFiguras;
begin
    PegaFigura(ImageGuiaPrincipal,'guia_principal.jpg');
    PegaFigura(ImageGuiaExtra,'guia_extra.jpg');
    PegaFigura(ImagePainelPrincipal,'fundo_menu.jpg');
    PegaFigura(ImagePainelExtra,'fundo_menu.jpg');
    PegaFiguraBotao(btNovo,'botao_novo.bmp');
    PegaFiguraBotao(btSalvar,'botao_salvar.bmp');
    PegaFiguraBotao(btAlterar,'botao_alterar.bmp');
    PegaFiguraBotao(btCancelar,'botao_Cancelar.bmp');
    PegaFiguraBotao(btPesquisar,'botao_Pesquisar.bmp');
    PegaFiguraBotao(btExcluir,'botao_excluir.bmp');
    PegaFiguraBotao(btRelatorio,'botao_relatorios.bmp');
    PegaFiguraBotao(btSair,'botao_sair.bmp');
    PegaFiguraBotao(btFechar,'botao_close.bmp');
    PegaFiguraBotao(btMinimizar,'botao_minimizar.bmp');
    PegaFiguraBotao(btAjuda,'lampada.bmp');

end;

procedure TFSALARIOFAMILIA.ColocaAtalhoBotoes;
begin
   Coloca_Atalho_Botoes(BtNovo, 'N');
   Coloca_Atalho_Botoes(BtSalvar, 'S');
   Coloca_Atalho_Botoes(BtAlterar, 'A');
   Coloca_Atalho_Botoes(BtCancelar, 'C');
   Coloca_Atalho_Botoes(BtExcluir, 'E');
   Coloca_Atalho_Botoes(BtPesquisar, 'P');
   Coloca_Atalho_Botoes(BtRelatorio, 'R');
   Coloca_Atalho_Botoes(BtSair, 'I');
end;

procedure TFSALARIOFAMILIA.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjSALARIOFAMILIA.Get_novocodigo;
     edtcodigo.enabled:=False;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjSALARIOFAMILIA.status:=dsInsert;
     Guia.TabIndex:=0;
     EdtSalarioInicial.setfocus;

end;

procedure TFSALARIOFAMILIA.btSalvarClick(Sender: TObject);
begin

     If ObjSALARIOFAMILIA.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjSALARIOFAMILIA.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjSALARIOFAMILIA.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFSALARIOFAMILIA.btAlterarClick(Sender: TObject);
begin
    If (ObjSALARIOFAMILIA.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjSALARIOFAMILIA.Status:=dsEdit;
                guia.TabIndex:=0;
                EdtSalarioInicial.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;

end;

procedure TFSALARIOFAMILIA.btCancelarClick(Sender: TObject);
begin
     ObjSALARIOFAMILIA.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFSALARIOFAMILIA.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjSALARIOFAMILIA.Get_pesquisa,ObjSALARIOFAMILIA.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjSALARIOFAMILIA.status<>dsinactive
                                  then exit;

                                  If (ObjSALARIOFAMILIA.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjSALARIOFAMILIA.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFSALARIOFAMILIA.btExcluirClick(Sender: TObject);
begin
     If (ObjSALARIOFAMILIA.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjSALARIOFAMILIA.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjSALARIOFAMILIA.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFSALARIOFAMILIA.btRelatorioClick(Sender: TObject);
begin
    ObjSALARIOFAMILIA.Imprime(edtcodigo.Text);
end;

procedure TFSALARIOFAMILIA.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFSALARIOFAMILIA.PosicionaForm;
begin
    Self.Caption:='      '+Self.Caption;
    Self.Left:=-18;
    Self.Top:=0;
end;
procedure TFSALARIOFAMILIA.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFSALARIOFAMILIA.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFSALARIOFAMILIA.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
end;

procedure TFSALARIOFAMILIA.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFSALARIOFAMILIA.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjSALARIOFAMILIA do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_SalarioInicial(edtSalarioInicial.text);
        Submit_SalarioFinal(edtSalarioFinal.text);
        Submit_ValorporDependente(edtValorporDependente.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFSALARIOFAMILIA.LimpaLabels;
begin
//LIMPA LABELS
end;

function TFSALARIOFAMILIA.ObjetoParaControles: Boolean;
begin
  Try
     With ObjSALARIOFAMILIA do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtSalarioInicial.text:=Get_SalarioInicial;
        EdtSalarioFinal.text:=Get_SalarioFinal;
        EdtValorporDependente.text:=Get_ValorporDependente;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFSALARIOFAMILIA.TabelaParaControles: Boolean;
begin
     If (ObjSALARIOFAMILIA.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

end.
