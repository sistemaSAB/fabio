unit UFAIXADESCONTO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjFAIXADESCONTO_comissao,
  UessencialGlobal, Tabs, Grids, DBGrids;

type
  TFFAIXADESCONTO = class(TForm)
    pnlcomissao: TPanel;
    lbLbDescontoInicial: TLabel;
    lbLbComissao: TLabel;
    lbLbDescontoFinal: TLabel;
    edtDescontoInicial: TEdit;
    edtDescontoFinal: TEdit;
    edtcomissao: TEdit;
    edtcodigo_faixa: TEdit;
    DBGrid: TDBGrid;
    imgrodape: TImage;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb1: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    edtNome: TEdit;
    lbLbNome: TLabel;
    btGravarComissao: TBitBtn;
    btcancelarcomissao: TBitBtn;
    btxcluircomissao: TBitBtn;
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btgravarcomissaoClick(Sender: TObject);
    procedure btcancelarcomissaoClick(Sender: TObject);
    procedure DBGridDblClick(Sender: TObject);
    procedure btexcluircomissaoClick(Sender: TObject);
    procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    Procedure resgatafaixacomissao;
    Procedure AtivaPanelComissao;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFAIXADESCONTO: TFFAIXADESCONTO;
  ObjFAIXADESCONTO_Comissao:TObjFAIXADESCONTO_Comissao;

implementation

uses Upesquisa, UessencialLocal, UDataModulo, UescolheImagemBotao,
  Uprincipal;

{$R *.dfm}





procedure TFFAIXADESCONTO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjFAIXADESCONTO_comissao=Nil)
     Then exit;

     If (ObjFAIXADESCONTO_comissao.Faixa.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjFAIXADESCONTO_comissao.free;

    
end;


procedure TFFAIXADESCONTO.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbCodigo.Caption:='0';


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjFAIXADESCONTO_comissao.Faixa.status:=dsInsert;


     limpaedit(Pnlcomissao);
     Pnlcomissao.Enabled:=False;

       btalterar.visible:=false;
     btrelatorios.visible:=false;
     btsair.Visible:=false;
     btopcoes.visible:=false;
     btexcluir.Visible:=false;
     btnovo.Visible:=false;
     btopcoes.visible:=false;

     

     Edtnome.setfocus;


end;

procedure TFFAIXADESCONTO.btSalvarClick(Sender: TObject);
begin

     If ObjFAIXADESCONTO_comissao.Faixa.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjFAIXADESCONTO_comissao.Faixa.salvar(true)=False)
     Then exit;

     lbCodigo.Caption:=ObjFAIXADESCONTO_comissao.Faixa.Get_codigo;
     habilita_botoes(Self);
     desabilita_campos(Self);
     self.resgatafaixacomissao;
      btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
      btopcoes.visible:=true;
     AtivaPanelComissao;


end;

procedure TFFAIXADESCONTO.btAlterarClick(Sender: TObject);
begin
    If (ObjFAIXADESCONTO_comissao.Faixa.Status=dsinactive) and (lbCodigo.Caption<>'')
    Then Begin
                habilita_campos(Self);
                limpaedit(Pnlcomissao);
                Pnlcomissao.Enabled:=False;

                ObjFAIXADESCONTO_comissao.Faixa.Status:=dsEdit;

                edtnome.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                  btalterar.visible:=false;
               btrelatorios.visible:=false;
               btsair.Visible:=false;
               btopcoes.visible:=false;
               btexcluir.Visible:=false;
               btnovo.Visible:=false;
               btopcoes.visible:=false;
          End;

end;

procedure TFFAIXADESCONTO.btCancelarClick(Sender: TObject);
begin
     ObjFAIXADESCONTO_comissao.Faixa.cancelar;
     limpaedit(Pnlcomissao);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     Self.resgatafaixacomissao;
      btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
      btopcoes.visible:=true;

end;

procedure TFFAIXADESCONTO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjFAIXADESCONTO_comissao.Faixa.Get_pesquisa,ObjFAIXADESCONTO_comissao.Faixa.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjFAIXADESCONTO_comissao.Faixa.status<>dsinactive
                                  then exit;

                                  If (ObjFAIXADESCONTO_comissao.Faixa.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjFAIXADESCONTO_comissao.Faixa.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFFAIXADESCONTO.btExcluirClick(Sender: TObject);
begin
     If (ObjFAIXADESCONTO_comissao.Faixa.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (ObjFAIXADESCONTO_comissao.Faixa.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjFAIXADESCONTO_comissao.Faixa.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFFAIXADESCONTO.btRelatorioClick(Sender: TObject);
begin
    ObjFAIXADESCONTO_comissao.Faixa.Imprime(lbCodigo.Caption);
end;

procedure TFFAIXADESCONTO.btSairClick(Sender: TObject);
begin
    Self.close;
end;


procedure TFFAIXADESCONTO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFFAIXADESCONTO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFFAIXADESCONTO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
    if(key=vk_f1) then
    begin
        Fprincipal.chamaPesquisaMenu;
    end;

end;

procedure TFFAIXADESCONTO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFFAIXADESCONTO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjFAIXADESCONTO_comissao.Faixa do
    Begin
        Submit_CODIGO(lbCodigo.Caption);
        Submit_Nome(edtNome.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFFAIXADESCONTO.LimpaLabels;
begin
      lbCodigo.caption:='';
end;

function TFFAIXADESCONTO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjFAIXADESCONTO_comissao.Faixa do
     Begin
        lbCodigo.Caption:=Get_CODIGO;
        EdtNome.text:=Get_Nome;
        Self.resgatafaixacomissao;
        Self.AtivaPanelComissao;
        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFFAIXADESCONTO.TabelaParaControles: Boolean;
begin
     If (ObjFAIXADESCONTO_comissao.Faixa.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFFAIXADESCONTO.btgravarcomissaoClick(Sender: TObject);
begin
     if (lbCodigo.Caption='')
     Then Begin
               Messagedlg('Escolha a faixa de Comiss�o',mterror,[mbok],0);
               exit;
     end;

     with ObjFAIXADESCONTO_Comissao do
     Begin
          ZerarTabela;
          Submit_CODIGO(edtcodigo_faixa.text);
          Submit_DescontoInicial(EdtDescontoInicial.text);
          Submit_DescontoFinal(EdtDescontoFinal.text);
          Faixa.Submit_CODIGO(lbCodigo.Caption);
          Submit_Comissao(edtcomissao.text);
          if (edtcodigo_faixa.Text='')
          Then Begin
                    status:=dsInsert;
                    Submit_CODIGO('0');
          end
          Else Status:=dsEdit;
          if (Salvar(true)=false)
          Then Begin
                    EdtDescontoInicial.setfocus;
                    exit;
          End;
          btcancelarcomissaoclick(sender);
          Self.resgatafaixacomissao;
     End;

end;

procedure TFFAIXADESCONTO.btcancelarcomissaoClick(Sender: TObject);
begin
     limpaedit(pnlcomissao);
     EdtDescontoInicial.setfocus;
end;

procedure TFFAIXADESCONTO.resgatafaixacomissao;
begin
     ObjFAIXADESCONTO_Comissao.ResgataFaixaComissao(lbCodigo.Caption);
     formatadbgrid(DBGrid);

end;

procedure TFFAIXADESCONTO.DBGridDblClick(Sender: TObject);
begin
     if (DBGrid.DataSource.DataSet.Active=False)
     Then exit;

     if (DBGrid.DataSource.DataSet.RecordCount=0)
     Then exit;

     If (ObjFAIXADESCONTO_Comissao.LocalizaCodigo(DBGrid.DataSource.DataSet.fieldbyname('codigo').AsString)=False)
     Then Begin
               Messagedlg('Faixa de Comiss�o n�o encontrada para ser alterada',mterror,[mbok],0);
               exit;
     End;
     ObjFAIXADESCONTO_Comissao.TabelaparaObjeto;
     btcancelarcomissaoClick(sender);
     edtcodigo_faixa.text:=ObjFAIXADESCONTO_Comissao.Get_CODIGO;
     EdtDescontoInicial.Text:=ObjFAIXADESCONTO_Comissao.Get_DescontoInicial;
     EdtDescontoFinal.Text:=ObjFAIXADESCONTO_Comissao.Get_DescontoFinal;
     edtcomissao.Text:=ObjFAIXADESCONTO_Comissao.Get_Comissao;
end;

procedure TFFAIXADESCONTO.btexcluircomissaoClick(Sender: TObject);
begin
     if (DBGrid.DataSource.DataSet.Active=False)
     Then exit;

     if (DBGrid.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem certeza que deseja excluir essa faixa de comiss�o?',mtConfirmation,[mbyes,mbno],0)=mrno)
     Then exit;

     ObjFAIXADESCONTO_Comissao.Exclui(DBGrid.DataSource.DataSet.fieldbyname('codigo').AsString,true);
     self.resgatafaixacomissao;

end;

procedure TFFAIXADESCONTO.AtivaPanelComissao;
begin
     Pnlcomissao.Enabled:=true;
     habilita_campos(Pnlcomissao);
     btcancelarcomissaoClick(nil);
end;

procedure TFFAIXADESCONTO.DBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGRID.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGRID.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGRID.DefaultDrawDataCell(Rect,DBGRID.Columns[DataCol].Field, State);
          End;
end;

procedure TFFAIXADESCONTO.FormShow(Sender: TObject);
begin
       limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Pnlcomissao.Enabled:=False;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btGravarComissao,'BOTAOINSERIR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btCancelarcomissao,'BOTAOCANCELARPRODUTO.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btxcluircomissao,'BOTAORETIRAR.BMP');

     Try
        ObjFAIXADESCONTO_Comissao:=TObjFAIXADESCONTO_comissao.create;
        DBGrid.DataSource:=ObjFAIXADESCONTO_Comissao.objdatasource;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE FAIXA DE DESCONTO')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);
    if(Tag<>0)
    then begin
         if(ObjFAIXADESCONTO_Comissao.Faixa.LocalizaCodigo(IntToStr(Tag))=True)then
         begin
              ObjFAIXADESCONTO_Comissao.Faixa.TabelaparaObjeto;
              self.ObjetoParaControles;
         end;

    end;     
end;

end.
