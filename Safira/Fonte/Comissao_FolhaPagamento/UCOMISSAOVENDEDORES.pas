unit UCOMISSAOVENDEDORES;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjCOMISSAOVENDEDORES,
  Uessencialglobal,UessencialLocal, Tabs;

type
  TFCOMISSAOVENDEDORES = class(TForm)
    btNovo: TSpeedButton;
    btFechar: TSpeedButton;
    btMinimizar: TSpeedButton;
    btSalvar: TSpeedButton;
    btAlterar: TSpeedButton;
    btCancelar: TSpeedButton;
    btSair: TSpeedButton;
    btRelatorio: TSpeedButton;
    btExcluir: TSpeedButton;
    btPesquisar: TSpeedButton;
    lbAjuda: TLabel;
    btAjuda: TSpeedButton;
    PainelExtra: TPanel;
    ImageGuiaPrincipal: TImage;
    ImageGuiaExtra: TImage;
    PainelPrincipal: TPanel;
    ImagePainelPrincipal: TImage;
    ImagePainelExtra: TImage;
    Guia: TTabSet;
    Notebook: TNotebook;
    Bevel: TBevel;
    LbCODIGO: TLabel;
    LbVENDEDOR: TLabel;
    LbNomeVENDEDOR: TLabel;
    LbPEDIDO: TLabel;
    LbNomePEDIDO: TLabel;
    LbPENDENCIA: TLabel;
    LbNomePENDENCIA: TLabel;
    LbComissao: TLabel;
    LbValor: TLabel;
    LbVALORCOMISSAO: TLabel;
    EdtCODIGO: TEdit;
    EdtVENDEDOR: TEdit;
    EdtPEDIDO: TEdit;
    EdtPENDENCIA: TEdit;
    EdtComissao: TEdit;
    EdtValor: TEdit;
    EdtVALORCOMISSAO: TEdit;
    Label1: TLabel;
    edtchequedevolvido: TEdit;
    edtobservacao: TEdit;
    Label2: TLabel;
//DECLARA COMPONENTES

    procedure FormCreate(Sender: TObject);
    Procedure PosicionaPaineis;
    Procedure PegaFiguras;
    Procedure ColocaAtalhoBotoes;
    Procedure PosicionaForm;
    procedure FormShow(Sender: TObject);
    procedure ImageGuiaPrincipalClick(Sender: TObject);
    procedure ImageGuiaExtraClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtVENDEDORKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtVENDEDORExit(Sender: TObject);
    procedure EdtPEDIDOExit(Sender: TObject);
    procedure EdtPEDIDOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtPENDENCIAKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtPENDENCIAExit(Sender: TObject);
    procedure edtchequedevolvidoExit(Sender: TObject);
    procedure edtchequedevolvidoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCOMISSAOVENDEDORES: TFCOMISSAOVENDEDORES;
  ObjCOMISSAOVENDEDORES:TObjCOMISSAOVENDEDORES;

implementation

uses Upesquisa;

{$R *.dfm}


procedure TFCOMISSAOVENDEDORES.FormCreate(Sender: TObject);
var
  Points: array [0..15] of TPoint;
  Regiao1:HRgn;
begin

Points[0].X :=305;      Points[0].Y := 22;
Points[1].X :=286;      Points[1].Y := 3;
Points[2].X :=41;       Points[2].Y := 3;
Points[3].X :=41;       Points[3].Y := 22;
Points[4].X :=28;       Points[4].Y := 35;
Points[5].X :=28;       Points[5].Y := 164;
Points[6].X :=41;       Points[6].Y := 177;
Points[7].X :=41;       Points[7].Y := 360;
Points[8].X :=62;       Points[8].Y := 381;
Points[9].X :=632;      Points[9].Y := 381;
Points[10].X :=647;     Points[10].Y := 396;
Points[11].X :=764;     Points[11].Y := 396;
Points[12].X :=764;     Points[12].Y := 95;
Points[13].X :=780;     Points[13].Y := 79;
Points[14].X :=780;     Points[14].Y := 22;
Points[15].X :=305;     Points[15].Y := 22;

Regiao1:=CreatePolygonRgn(Points, 16, WINDING);
SetWindowRgn(Self.Handle, regiao1, False);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.TabIndex:=0;

     Try
        ObjCOMISSAOVENDEDORES:=TObjCOMISSAOVENDEDORES.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

end;

procedure TFCOMISSAOVENDEDORES.PosicionaPaineis;
begin
  With Self.PainelPrincipal do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelPrincipal do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  With Self.PainelExtra  do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelExtra  do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  Self.PainelPrincipal.Enabled:=true;
  Self.PainelPrincipal.Visible:=true;
  Self.PainelExtra.Enabled:=false;
  Self.PainelExtra.Visible:=false;
end;

procedure TFCOMISSAOVENDEDORES.FormShow(Sender: TObject);
begin
    PegaCorForm(Self);
    Self.PosicionaPaineis;
    Self.PosicionaForm;
    Self.PegaFiguras;
    Self.ColocaAtalhoBotoes;

end;

procedure TFCOMISSAOVENDEDORES.ImageGuiaPrincipalClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=true;
PainelPrincipal.Visible:=true;
PainelExtra.Enabled:=false;
PainelExtra.Visible:=false;
end;

procedure TFCOMISSAOVENDEDORES.ImageGuiaExtraClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=false;
PainelPrincipal.Visible:=false;
PainelExtra.Enabled:=true;
PainelExtra.Visible:=true;

end;

procedure TFCOMISSAOVENDEDORES.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjCOMISSAOVENDEDORES=Nil)
     Then exit;

     If (ObjCOMISSAOVENDEDORES.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjCOMISSAOVENDEDORES.free;
    Action := caFree;
    Self := nil;
end;

procedure TFCOMISSAOVENDEDORES.PegaFiguras;
begin
    PegaFigura(ImageGuiaPrincipal,'guia_principal.jpg');
    PegaFigura(ImageGuiaExtra,'guia_extra.jpg');
    PegaFigura(ImagePainelPrincipal,'fundo_menu.jpg');
    PegaFigura(ImagePainelExtra,'fundo_menu.jpg');
    PegaFiguraBotao(btNovo,'botao_novo.bmp');
    PegaFiguraBotao(btSalvar,'botao_salvar.bmp');
    PegaFiguraBotao(btAlterar,'botao_alterar.bmp');
    PegaFiguraBotao(btCancelar,'botao_Cancelar.bmp');
    PegaFiguraBotao(btPesquisar,'botao_Pesquisar.bmp');
    PegaFiguraBotao(btExcluir,'botao_excluir.bmp');
    PegaFiguraBotao(btRelatorio,'botao_relatorios.bmp');
    PegaFiguraBotao(btSair,'botao_sair.bmp');
    PegaFiguraBotao(btFechar,'botao_close.bmp');
    PegaFiguraBotao(btMinimizar,'botao_minimizar.bmp');
    PegaFiguraBotao(btAjuda,'lampada.bmp');

end;

procedure TFCOMISSAOVENDEDORES.ColocaAtalhoBotoes;
begin
   Coloca_Atalho_Botoes(BtNovo, 'N');
   Coloca_Atalho_Botoes(BtSalvar, 'S');
   Coloca_Atalho_Botoes(BtAlterar, 'A');
   Coloca_Atalho_Botoes(BtCancelar, 'C');
   Coloca_Atalho_Botoes(BtExcluir, 'E');
   Coloca_Atalho_Botoes(BtPesquisar, 'P');
   Coloca_Atalho_Botoes(BtRelatorio, 'R');
   Coloca_Atalho_Botoes(BtSair, 'I');
end;

procedure TFCOMISSAOVENDEDORES.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjCOMISSAOVENDEDORES.Get_novocodigo;
     edtcodigo.enabled:=False;
     EdtVALORCOMISSAO.enabled:=False;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjCOMISSAOVENDEDORES.status:=dsInsert;
     Guia.TabIndex:=0;
     Edtvendedor.setfocus;

end;

procedure TFCOMISSAOVENDEDORES.btSalvarClick(Sender: TObject);
begin

     If ObjCOMISSAOVENDEDORES.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjCOMISSAOVENDEDORES.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjCOMISSAOVENDEDORES.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCOMISSAOVENDEDORES.btAlterarClick(Sender: TObject);
begin
    If (ObjCOMISSAOVENDEDORES.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                EdtVALORCOMISSAO.enabled:=False;
                ObjCOMISSAOVENDEDORES.Status:=dsEdit;
                guia.TabIndex:=0;
                Edtvendedor.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;

end;

procedure TFCOMISSAOVENDEDORES.btCancelarClick(Sender: TObject);
begin
     ObjCOMISSAOVENDEDORES.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFCOMISSAOVENDEDORES.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjCOMISSAOVENDEDORES.Get_pesquisa,ObjCOMISSAOVENDEDORES.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjCOMISSAOVENDEDORES.status<>dsinactive
                                  then exit;

                                  If (ObjCOMISSAOVENDEDORES.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjCOMISSAOVENDEDORES.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFCOMISSAOVENDEDORES.btExcluirClick(Sender: TObject);
begin
     If (ObjCOMISSAOVENDEDORES.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjCOMISSAOVENDEDORES.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjCOMISSAOVENDEDORES.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCOMISSAOVENDEDORES.btRelatorioClick(Sender: TObject);
begin
    ObjCOMISSAOVENDEDORES.Imprime(edtcodigo.Text);
end;

procedure TFCOMISSAOVENDEDORES.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFCOMISSAOVENDEDORES.PosicionaForm;
begin
    Self.Caption:='      '+Self.Caption;
    Self.Left:=-18;
    Self.Top:=0;
end;
procedure TFCOMISSAOVENDEDORES.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFCOMISSAOVENDEDORES.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFCOMISSAOVENDEDORES.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;

    if key=vk_f5
    Then Begin
              if (Self.PainelPrincipal.Visible=true)
              Then Begin
                        Self.PainelPrincipal.Visible:=False;
                        Self.PainelExtra.Visible:=True;
              End
              Else Begin
                        Self.PainelPrincipal.Visible:=True;
                        Self.PainelExtra.Visible:=False;
              End;
    End;
end;

procedure TFCOMISSAOVENDEDORES.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFCOMISSAOVENDEDORES.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjCOMISSAOVENDEDORES do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        VENDEDOR.Submit_codigo(edtVENDEDOR.text);
        PEDIDO.Submit_codigo(edtPEDIDO.text);
        PENDENCIA.Submit_codigo(edtPENDENCIA.text);
        chequedevolvido.Submit_codigo(edtchequedevolvido.text);
        Submit_observacao(edtobservacao.Text);
        Submit_Comissao(edtComissao.text);
        Submit_Valor(edtValor.text);
         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFCOMISSAOVENDEDORES.LimpaLabels;
begin
     LbNomeVENDEDOR.caption:='';
     LbNomePEDIDO.caption:='';
     LbNomePENDENCIA.caption:='';

end;

function TFCOMISSAOVENDEDORES.ObjetoParaControles: Boolean;
begin
  Try
     With ObjCOMISSAOVENDEDORES do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtVENDEDOR.text:=VENDEDOR.Get_codigo;
        EdtPEDIDO.text:=PEDIDO.Get_codigo;
        EdtPENDENCIA.text:=PENDENCIA.Get_codigo;
        Edtchequedevolvido.text:=chequedevolvido.Get_codigo;
        edtobservacao.text:=Get_observacao;
        EdtComissao.text:=Get_Comissao;
        EdtValor.text:=Get_Valor;
        EdtVALORCOMISSAO.text:=Get_VALORCOMISSAO;
        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFCOMISSAOVENDEDORES.TabelaParaControles: Boolean;
begin
     If (ObjCOMISSAOVENDEDORES.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFCOMISSAOVENDEDORES.EdtVENDEDORKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjCOMISSAOVENDEDORES.EdtVENDEDORKeyDown(sender,key,shift,LbNomeVENDEDOR);
end;

procedure TFCOMISSAOVENDEDORES.EdtVENDEDORExit(Sender: TObject);
begin
     ObjCOMISSAOVENDEDORES.EdtVENDEDORExit(sender,LbNomeVENDEDOR);
end;

procedure TFCOMISSAOVENDEDORES.EdtPEDIDOExit(Sender: TObject);
begin
     ObjCOMISSAOVENDEDORES.EdtPEDIDOExit(sender,LbNomePEDIDO);
end;

procedure TFCOMISSAOVENDEDORES.EdtPEDIDOKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjCOMISSAOVENDEDORES.EdtPEDIDOKeyDown(sender,key,shift,LbNomePEDIDO);
end;

procedure TFCOMISSAOVENDEDORES.EdtPENDENCIAKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjCOMISSAOVENDEDORES.EdtPENDENCIAKeyDown(sender,key,shift,LbNomePENDENCIA);
end;

procedure TFCOMISSAOVENDEDORES.EdtPENDENCIAExit(Sender: TObject);
begin
ObjCOMISSAOVENDEDORES.EdtPENDENCIAExit(sender,LbNomePENDENCIA);
end;

procedure TFCOMISSAOVENDEDORES.edtchequedevolvidoExit(Sender: TObject);
begin
ObjCOMISSAOVENDEDORES.EdtchequedevolvidoExit(sender,nil);
end;

procedure TFCOMISSAOVENDEDORES.edtchequedevolvidoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjCOMISSAOVENDEDORES.EdtchequedevolvidoKeyDown(sender,key,shift,nil);
end;

end.
