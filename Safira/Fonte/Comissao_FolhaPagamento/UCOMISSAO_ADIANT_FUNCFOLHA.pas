unit UCOMISSAO_ADIANT_FUNCFOLHA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjCOMISSAO_ADIANT_FUNCFOLHA,
  UessencialGlobal,UessencialLocal, Tabs;

type
  TFCOMISSAO_ADIANT_FUNCFOLHA = class(TForm)
    btNovo: TSpeedButton;
    btFechar: TSpeedButton;
    btMinimizar: TSpeedButton;
    btSalvar: TSpeedButton;
    btAlterar: TSpeedButton;
    btCancelar: TSpeedButton;
    btSair: TSpeedButton;
    btRelatorio: TSpeedButton;
    btExcluir: TSpeedButton;
    btPesquisar: TSpeedButton;
    lbAjuda: TLabel;
    btAjuda: TSpeedButton;
    PainelExtra: TPanel;
    ImageGuiaPrincipal: TImage;
    ImageGuiaExtra: TImage;
    PainelPrincipal: TPanel;
    ImagePainelPrincipal: TImage;
    ImagePainelExtra: TImage;
    Guia: TTabSet;
    Notebook: TNotebook;
    LbCODIGO: TLabel;
    LbComissaoVendedor: TLabel;
    LbComissaoColocador: TLabel;
    LbAdiantamentoFuncionario: TLabel;
    LbFuncionarioFolhaPagamento: TLabel;
    EdtCODIGO: TEdit;
    EdtComissaoVendedor: TEdit;
    EdtComissaoColocador: TEdit;
    EdtAdiantamentoFuncionario: TEdit;
    EdtFuncionarioFolhaPagamento: TEdit;
    Bevel: TBevel;
    Label1: TLabel;
    edthoraextra: TEdit;
    Label2: TLabel;
    edtgratificacao: TEdit;
    Label3: TLabel;
    edtAcrescimoDSR: TEdit;
    Label4: TLabel;
    edtdescontocontsindical: TEdit;
//DECLARA COMPONENTES

    procedure FormCreate(Sender: TObject);
    Procedure PosicionaPaineis;
    Procedure PegaFiguras;
    Procedure ColocaAtalhoBotoes;
    Procedure PosicionaForm;
    procedure ImageGuiaPrincipalClick(Sender: TObject);
    procedure ImageGuiaExtraClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtComissaoVendedorExit(Sender: TObject);
    procedure EdtComissaoVendedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtComissaoColocadorExit(Sender: TObject);
    procedure EdtComissaoColocadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtAdiantamentoFuncionarioExit(Sender: TObject);
    procedure EdtAdiantamentoFuncionarioKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure EdtFuncionarioFolhaPagamentoExit(Sender: TObject);
    procedure EdtFuncionarioFolhaPagamentoKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edthoraextraExit(Sender: TObject);
    procedure edthoraextraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtgratificacaoExit(Sender: TObject);
    procedure edtgratificacaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCOMISSAO_ADIANT_FUNCFOLHA: TFCOMISSAO_ADIANT_FUNCFOLHA;
  ObjCOMISSAO_ADIANT_FUNCFOLHA:TObjCOMISSAO_ADIANT_FUNCFOLHA;

implementation

uses Upesquisa, UobjHORAEXTRA;

{$R *.dfm}


procedure TFCOMISSAO_ADIANT_FUNCFOLHA.FormCreate(Sender: TObject);
var
  Points: array [0..15] of TPoint;
  Regiao1:HRgn;
begin

Points[0].X :=305;      Points[0].Y := 22;
Points[1].X :=286;      Points[1].Y := 3;
Points[2].X :=41;       Points[2].Y := 3;
Points[3].X :=41;       Points[3].Y := 22;
Points[4].X :=28;       Points[4].Y := 35;
Points[5].X :=28;       Points[5].Y := 164;
Points[6].X :=41;       Points[6].Y := 177;
Points[7].X :=41;       Points[7].Y := 360;
Points[8].X :=62;       Points[8].Y := 381;
Points[9].X :=632;      Points[9].Y := 381;
Points[10].X :=647;     Points[10].Y := 396;
Points[11].X :=764;     Points[11].Y := 396;
Points[12].X :=764;     Points[12].Y := 95;
Points[13].X :=780;     Points[13].Y := 79;
Points[14].X :=780;     Points[14].Y := 22;
Points[15].X :=305;     Points[15].Y := 22;

Regiao1:=CreatePolygonRgn(Points, 16, WINDING);
SetWindowRgn(Self.Handle, regiao1, False);


limpaedit(Self);
Self.limpaLabels;
desabilita_campos(Self);
Guia.TabIndex:=0;

PegaCorForm(Self);
Self.PosicionaPaineis;
Self.PosicionaForm;
Self.PegaFiguras;
Self.ColocaAtalhoBotoes;


Try
   ObjCOMISSAO_ADIANT_FUNCFOLHA:=TObjCOMISSAO_ADIANT_FUNCFOLHA.create(self);
Except
      Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
      Self.close;
End;

end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.PosicionaPaineis;
begin
  With Self.PainelPrincipal do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelPrincipal do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  With Self.PainelExtra  do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelExtra  do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  Self.PainelPrincipal.Enabled:=true;
  Self.PainelPrincipal.Visible:=true;
  Self.PainelExtra.Enabled:=false;
  Self.PainelExtra.Visible:=false;
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.ImageGuiaPrincipalClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=true;
PainelPrincipal.Visible:=true;
PainelExtra.Enabled:=false;
PainelExtra.Visible:=false;
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.ImageGuiaExtraClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=false;
PainelPrincipal.Visible:=false;
PainelExtra.Enabled:=true;
PainelExtra.Visible:=true;

end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjCOMISSAO_ADIANT_FUNCFOLHA=Nil)
     Then exit;

     If (ObjCOMISSAO_ADIANT_FUNCFOLHA.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjCOMISSAO_ADIANT_FUNCFOLHA.free;
    Action := caFree;
    Self := nil;
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.PegaFiguras;
begin
    PegaFigura(ImageGuiaPrincipal,'guia_principal.jpg');
    PegaFigura(ImageGuiaExtra,'guia_extra.jpg');
    PegaFigura(ImagePainelPrincipal,'fundo_menu.jpg');
    PegaFigura(ImagePainelExtra,'fundo_menu.jpg');
    PegaFiguraBotao(btNovo,'botao_novo.bmp');
    PegaFiguraBotao(btSalvar,'botao_salvar.bmp');
    PegaFiguraBotao(btAlterar,'botao_alterar.bmp');
    PegaFiguraBotao(btCancelar,'botao_Cancelar.bmp');
    PegaFiguraBotao(btPesquisar,'botao_Pesquisar.bmp');
    PegaFiguraBotao(btExcluir,'botao_excluir.bmp');
    PegaFiguraBotao(btRelatorio,'botao_relatorios.bmp');
    PegaFiguraBotao(btSair,'botao_sair.bmp');
    PegaFiguraBotao(btFechar,'botao_close.bmp');
    PegaFiguraBotao(btMinimizar,'botao_minimizar.bmp');
    PegaFiguraBotao(btAjuda,'lampada.bmp');

end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.ColocaAtalhoBotoes;
begin
   Coloca_Atalho_Botoes(BtNovo, 'N');
   Coloca_Atalho_Botoes(BtSalvar, 'S');
   Coloca_Atalho_Botoes(BtAlterar, 'A');
   Coloca_Atalho_Botoes(BtCancelar, 'C');
   Coloca_Atalho_Botoes(BtExcluir, 'E');
   Coloca_Atalho_Botoes(BtPesquisar, 'P');
   Coloca_Atalho_Botoes(BtRelatorio, 'R');
   Coloca_Atalho_Botoes(BtSair, 'I');
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjCOMISSAO_ADIANT_FUNCFOLHA.Get_novocodigo;
     edtcodigo.enabled:=False;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjCOMISSAO_ADIANT_FUNCFOLHA.status:=dsInsert;
     Guia.TabIndex:=0;
     EdtComissaoVendedor.setfocus;

end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.btSalvarClick(Sender: TObject);
begin

     If ObjCOMISSAO_ADIANT_FUNCFOLHA.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjCOMISSAO_ADIANT_FUNCFOLHA.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjCOMISSAO_ADIANT_FUNCFOLHA.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.btAlterarClick(Sender: TObject);
begin
    If (ObjCOMISSAO_ADIANT_FUNCFOLHA.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjCOMISSAO_ADIANT_FUNCFOLHA.Status:=dsEdit;
                guia.TabIndex:=0;
                EdtComissaoVendedor.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;

end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.btCancelarClick(Sender: TObject);
begin
     ObjCOMISSAO_ADIANT_FUNCFOLHA.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjCOMISSAO_ADIANT_FUNCFOLHA.Get_pesquisa,ObjCOMISSAO_ADIANT_FUNCFOLHA.Get_TituloPesquisa,Nil)
=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjCOMISSAO_ADIANT_FUNCFOLHA.status<>dsinactive
                                  then exit;

                                  If (ObjCOMISSAO_ADIANT_FUNCFOLHA.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjCOMISSAO_ADIANT_FUNCFOLHA.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.btExcluirClick(Sender: TObject);
begin
     If (ObjCOMISSAO_ADIANT_FUNCFOLHA.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjCOMISSAO_ADIANT_FUNCFOLHA.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjCOMISSAO_ADIANT_FUNCFOLHA.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.btRelatorioClick(Sender: TObject);
begin
    ObjCOMISSAO_ADIANT_FUNCFOLHA.Imprime(edtcodigo.Text);
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.PosicionaForm;
begin
    Self.Caption:='      '+Self.Caption;
    Self.Left:=-18;
    Self.Top:=0;
end;
procedure TFCOMISSAO_ADIANT_FUNCFOLHA.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;

    if key=vk_f5
    Then Begin
              if (Self.PainelPrincipal.Visible=true)
              Then Begin
                        Self.PainelPrincipal.Visible:=False;
                        Self.PainelExtra.Visible:=True;
              End
              Else Begin
                        Self.PainelPrincipal.Visible:=True;
                        Self.PainelExtra.Visible:=False;
              End;
    End;
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFCOMISSAO_ADIANT_FUNCFOLHA.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjCOMISSAO_ADIANT_FUNCFOLHA do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        ComissaoVendedor.Submit_codigo(edtComissaoVendedor.text);
        ComissaoColocador.Submit_codigo(edtComissaoColocador.text);
        AdiantamentoFuncionario.Submit_codigo(edtAdiantamentoFuncionario.text);
        FuncionarioFolhaPagamento.Submit_codigo(edtFuncionarioFolhaPagamento.text);
        HoraExtra.Submit_CODIGO(edthoraextra.Text);
        Gratificacao.Submit_CODIGO(edtGratificacao.Text);
        AcrescimoDSR.Submit_codigo(edtAcrescimoDSR.text);
        DescontoContSindical.submit_codigo(edtdescontocontsindical.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.LimpaLabels;
begin
//LIMPA LABELS
end;

function TFCOMISSAO_ADIANT_FUNCFOLHA.ObjetoParaControles: Boolean;
begin
  Try
     With ObjCOMISSAO_ADIANT_FUNCFOLHA do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtComissaoVendedor.text:=ComissaoVendedor.Get_codigo;
        EdtComissaoColocador.text:=ComissaoColocador.Get_codigo;
        EdtAdiantamentoFuncionario.text:=AdiantamentoFuncionario.Get_codigo;
        EdtFuncionarioFolhaPagamento.text:=FuncionarioFolhaPagamento.Get_codigo;
        EdtHoraExtra.text:=HoraExtra.Get_CODIGO;
        edtgratificacao.Text:=Gratificacao.Get_CODIGO;
        edtAcrescimoDSR.Text:=AcrescimoDSR.Get_CODIGO;
        edtdescontocontsindical.Text:=DescontoContSindical.Get_CODIGO;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFCOMISSAO_ADIANT_FUNCFOLHA.TabelaParaControles: Boolean;
begin
     If (ObjCOMISSAO_ADIANT_FUNCFOLHA.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.EdtComissaoVendedorExit(
  Sender: TObject);
begin
     ObjCOMISSAO_ADIANT_FUNCFOLHA.EdtComissaoVendedorExit(sender,nil);
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.EdtComissaoVendedorKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     ObjCOMISSAO_ADIANT_FUNCFOLHA.EdtComissaoVendedorKeyDown(sender,key,shift,nil);
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.EdtComissaoColocadorExit(
  Sender: TObject);
begin
     ObjCOMISSAO_ADIANT_FUNCFOLHA.EdtComissaoColocadorExit(sender,nil);
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.EdtComissaoColocadorKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     ObjCOMISSAO_ADIANT_FUNCFOLHA.EdtComissaoColocadorKeyDown(sender,key,shift,nil);
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.EdtAdiantamentoFuncionarioExit(
  Sender: TObject);
begin
     ObjCOMISSAO_ADIANT_FUNCFOLHA.EdtAdiantamentoFuncionarioExit(sender,nil);
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.EdtAdiantamentoFuncionarioKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     ObjCOMISSAO_ADIANT_FUNCFOLHA.EdtAdiantamentoFuncionarioKeyDown(sender,key,shift,nil);
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.EdtFuncionarioFolhaPagamentoExit(
  Sender: TObject);
begin
     ObjCOMISSAO_ADIANT_FUNCFOLHA.EdtFuncionarioFolhaPagamentoExit(sender,nil);
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.EdtFuncionarioFolhaPagamentoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     ObjCOMISSAO_ADIANT_FUNCFOLHA.EdtFuncionarioFolhaPagamentoKeyDown(sender,key,shift,nil);
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.edthoraextraExit(Sender: TObject);
begin
     ObjCOMISSAO_ADIANT_FUNCFOLHA.EdtHoraExtraExit(sender,nil);
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.edthoraextraKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjCOMISSAO_ADIANT_FUNCFOLHA.EdtHoraExtraKeyDown(sender,key,shift,nil);
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.edtgratificacaoExit(Sender: TObject);
begin
ObjCOMISSAO_ADIANT_FUNCFOLHA.EdtGratificacaoExit(sender,nil);
end;

procedure TFCOMISSAO_ADIANT_FUNCFOLHA.edtgratificacaoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     ObjCOMISSAO_ADIANT_FUNCFOLHA.EdtGratificacaoKeyDown(sender,key,shift,nil);
end;

end.
