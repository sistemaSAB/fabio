unit UPERSIANAGRUPODIAMETROCOR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjPERSIANAGRUPODIAMETROCOR,
  jpeg, UessencialGlobal, Tabs;

type
  TFPERSIANAGRUPODIAMETROCOR = class(TForm)
    btNovo: TSpeedButton;
    btFechar: TSpeedButton;
    btMinimizar: TSpeedButton;
    btSalvar: TSpeedButton;
    btAlterar: TSpeedButton;
    btCancelar: TSpeedButton;
    btSair: TSpeedButton;
    btRelatorio: TSpeedButton;
    btExcluir: TSpeedButton;
    btPesquisar: TSpeedButton;
    lbAjuda: TLabel;
    btAjuda: TSpeedButton;
    PainelExtra: TPanel;
    ImageGuiaPrincipal: TImage;
    ImageGuiaExtra: TImage;
    PainelPrincipal: TPanel;
    ImagePainelPrincipal: TImage;
    ImagePainelExtra: TImage;
    Guia: TTabSet;
    Notebook: TNotebook;
    Bevel1: TBevel;
    EdtCodigo: TEdit;
    LbCodigo: TLabel;
    LbPersiana: TLabel;
    EdtPersiana: TEdit;
    LbNomePersiana: TLabel;
    LbGrupoPersiana: TLabel;
    EdtGrupoPersiana: TEdit;
    LbNomeGrupoPersiana: TLabel;
    LbCor: TLabel;
    EdtCor: TEdit;
    LbNomeCor: TLabel;
    LbDiametro: TLabel;
    EdtDiametro: TEdit;
    LbNomeDiametro: TLabel;
    LbEstoque: TLabel;
    EdtEstoque: TEdit;
    LbPrecoCusto: TLabel;
    EdtPrecoCusto: TEdit;
    GroupBox1: TGroupBox;
    LbPorcentagemFornecido: TLabel;
    LbPorcentagemRetirado: TLabel;
    LbPorcentagemInstalado: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdtPorcentagemInstalado: TEdit;
    EdtPorcentagemFornecido: TEdit;
    EdtPorcentagemRetirado: TEdit;
    GroupBox2: TGroupBox;
    LbPrecoVendaInstalado: TLabel;
    LbPrecoVendaFornecido: TLabel;
    LbPrecoVendaRetirado: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdtPrecoVendaInstalado: TEdit;
    EdtPrecoVendaFornecido: TEdit;
    EdtPrecoVendaRetirado: TEdit;
    procedure edtPersianaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtPersianaExit(Sender: TObject);
    procedure edtGrupoPersianaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtGrupoPersianaExit(Sender: TObject);
    procedure edtCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtCorExit(Sender: TObject);
    procedure edtDiametroKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtDiametroExit(Sender: TObject);
//DECLARA COMPONENTES

    procedure FormCreate(Sender: TObject);
    Procedure PosicionaPaineis;
    Procedure PegaFiguras;
    Procedure ColocaAtalhoBotoes;
    Procedure PosicionaForm;
    procedure ImageGuiaPrincipalClick(Sender: TObject);
    procedure ImageGuiaExtraClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GuiaClick(Sender: TObject);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPERSIANAGRUPODIAMETROCOR: TFPERSIANAGRUPODIAMETROCOR;
  ObjPERSIANAGRUPODIAMETROCOR:TObjPERSIANAGRUPODIAMETROCOR;

implementation

uses Upesquisa, UessencialLocal;

{$R *.dfm}


procedure TFPERSIANAGRUPODIAMETROCOR.FormCreate(Sender: TObject);
var
  Points: array [0..15] of TPoint;
  Regiao1:HRgn;
begin

Points[0].X :=305;      Points[0].Y := 22;
Points[1].X :=286;      Points[1].Y := 3;
Points[2].X :=41;       Points[2].Y := 3;
Points[3].X :=41;       Points[3].Y := 22;
Points[4].X :=28;       Points[4].Y := 35;
Points[5].X :=28;       Points[5].Y := 164;
Points[6].X :=41;       Points[6].Y := 177;
Points[7].X :=41;       Points[7].Y := 360;
Points[8].X :=62;       Points[8].Y := 381;
Points[9].X :=632;      Points[9].Y := 381;
Points[10].X :=647;     Points[10].Y := 396;
Points[11].X :=764;     Points[11].Y := 396;
Points[12].X :=764;     Points[12].Y := 95;
Points[13].X :=780;     Points[13].Y := 79;
Points[14].X :=780;     Points[14].Y := 22;
Points[15].X :=305;     Points[15].Y := 22;

Regiao1:=CreatePolygonRgn(Points, 16, WINDING);
SetWindowRgn(Self.Handle, regiao1, False);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.TabIndex:=0;
     Notebook.PageIndex:=0;

     Try
        ObjPERSIANAGRUPODIAMETROCOR:=TObjPERSIANAGRUPODIAMETROCOR.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     PegaCorForm(Self);
     Self.PosicionaPaineis;
     Self.PosicionaForm;
     Self.PegaFiguras;
     Self.ColocaAtalhoBotoes;
end;

procedure TFPERSIANAGRUPODIAMETROCOR.PosicionaPaineis;
begin
  With Self.PainelPrincipal do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelPrincipal do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  With Self.PainelExtra  do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelExtra  do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  Self.PainelPrincipal.Enabled:=true;
  Self.PainelPrincipal.Visible:=true;
  Self.PainelExtra.Enabled:=false;
  Self.PainelExtra.Visible:=false;
end;

procedure TFPERSIANAGRUPODIAMETROCOR.ImageGuiaPrincipalClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=true;
PainelPrincipal.Visible:=true;
PainelExtra.Enabled:=false;
PainelExtra.Visible:=false;
end;

procedure TFPERSIANAGRUPODIAMETROCOR.ImageGuiaExtraClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=false;
PainelPrincipal.Visible:=false;
PainelExtra.Enabled:=true;
PainelExtra.Visible:=true;

end;

procedure TFPERSIANAGRUPODIAMETROCOR.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjPERSIANAGRUPODIAMETROCOR=Nil)
     Then exit;

     If (ObjPERSIANAGRUPODIAMETROCOR.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjPERSIANAGRUPODIAMETROCOR.free;
    Action := caFree;
    Self := nil;
end;

procedure TFPERSIANAGRUPODIAMETROCOR.PegaFiguras;
begin
    PegaFigura(ImageGuiaPrincipal,'guia_principal.jpg');
    PegaFigura(ImageGuiaExtra,'guia_extra.jpg');
    PegaFigura(ImagePainelPrincipal,'fundo_menu.jpg');
    PegaFigura(ImagePainelExtra,'fundo_menu.jpg');
    PegaFiguraBotao(btNovo,'botao_novo.bmp');
    PegaFiguraBotao(btSalvar,'botao_salvar.bmp');
    PegaFiguraBotao(btAlterar,'botao_alterar.bmp');
    PegaFiguraBotao(btCancelar,'botao_Cancelar.bmp');
    PegaFiguraBotao(btPesquisar,'botao_Pesquisar.bmp');
    PegaFiguraBotao(btExcluir,'botao_excluir.bmp');
    PegaFiguraBotao(btRelatorio,'botao_relatorios.bmp');
    PegaFiguraBotao(btSair,'botao_sair.bmp');
    PegaFiguraBotao(btFechar,'botao_close.bmp');
    PegaFiguraBotao(btMinimizar,'botao_minimizar.bmp');
    PegaFiguraBotao(btAjuda,'lampada.bmp');

end;

procedure TFPERSIANAGRUPODIAMETROCOR.ColocaAtalhoBotoes;
begin
   Coloca_Atalho_Botoes(BtNovo, 'N');
   Coloca_Atalho_Botoes(BtSalvar, 'S');
   Coloca_Atalho_Botoes(BtAlterar, 'A');
   Coloca_Atalho_Botoes(BtCancelar, 'C');
   Coloca_Atalho_Botoes(BtExcluir, 'E');
   Coloca_Atalho_Botoes(BtPesquisar, 'P');
   Coloca_Atalho_Botoes(BtRelatorio, 'R');
   Coloca_Atalho_Botoes(BtSair, 'I');
end;

procedure TFPERSIANAGRUPODIAMETROCOR.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjPERSIANAGRUPODIAMETROCOR.Get_novocodigo;
     edtcodigo.enabled:=False;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjPERSIANAGRUPODIAMETROCOR.status:=dsInsert;
     Guia.TabIndex:=0;
     Notebook.PageIndex:=0;

     EdtPersiana.setfocus;

end;

procedure TFPERSIANAGRUPODIAMETROCOR.btSalvarClick(Sender: TObject);
begin

     If ObjPERSIANAGRUPODIAMETROCOR.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjPERSIANAGRUPODIAMETROCOR.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjPERSIANAGRUPODIAMETROCOR.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFPERSIANAGRUPODIAMETROCOR.btAlterarClick(Sender: TObject);
begin
    If (ObjPERSIANAGRUPODIAMETROCOR.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjPERSIANAGRUPODIAMETROCOR.Status:=dsEdit;
                guia.TabIndex:=0;
                EdtPersiana.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;

end;

procedure TFPERSIANAGRUPODIAMETROCOR.btCancelarClick(Sender: TObject);
begin
     ObjPERSIANAGRUPODIAMETROCOR.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFPERSIANAGRUPODIAMETROCOR.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjPERSIANAGRUPODIAMETROCOR.Get_pesquisa,ObjPERSIANAGRUPODIAMETROCOR.Get_TituloPesquisa,Nil)
=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjPERSIANAGRUPODIAMETROCOR.status<>dsinactive
                                  then exit;

                                  If (ObjPERSIANAGRUPODIAMETROCOR.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjPERSIANAGRUPODIAMETROCOR.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFPERSIANAGRUPODIAMETROCOR.btExcluirClick(Sender: TObject);
begin
     If (ObjPERSIANAGRUPODIAMETROCOR.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjPERSIANAGRUPODIAMETROCOR.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjPERSIANAGRUPODIAMETROCOR.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFPERSIANAGRUPODIAMETROCOR.btRelatorioClick(Sender: TObject);
begin
    ObjPERSIANAGRUPODIAMETROCOR.Imprime(edtCodigo.text);
end;

procedure TFPERSIANAGRUPODIAMETROCOR.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFPERSIANAGRUPODIAMETROCOR.PosicionaForm;
begin
    Self.Caption:='      '+Self.Caption;
    Self.Left:=-18;
    Self.Top:=0;
end;
procedure TFPERSIANAGRUPODIAMETROCOR.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFPERSIANAGRUPODIAMETROCOR.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFPERSIANAGRUPODIAMETROCOR.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
end;

procedure TFPERSIANAGRUPODIAMETROCOR.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFPERSIANAGRUPODIAMETROCOR.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjPERSIANAGRUPODIAMETROCOR do
    Begin
        Submit_Codigo(edtCodigo.text);
        Persiana.Submit_codigo(edtPersiana.text);
        GrupoPersiana.Submit_codigo(edtGrupoPersiana.text);
        Cor.Submit_codigo(edtCor.text);
        Diametro.Submit_codigo(edtDiametro.text);
        Submit_Estoque(edtEstoque.text);
        Submit_PrecoCusto(edtPrecoCusto.text);
        Submit_PorcentagemInstalado(edtPorcentagemInstalado.text);
        Submit_PorcentagemFornecido(edtPorcentagemFornecido.text);
        Submit_PorcentagemRetirado(edtPorcentagemRetirado.text);
        result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFPERSIANAGRUPODIAMETROCOR.LimpaLabels;
begin
    LbNomePersiana.Caption:='';
    LbNomeGrupoPersiana.Caption:='';
    LbNomeCor.Caption:='';
    LbNomeDiametro.Caption:='';
end;

function TFPERSIANAGRUPODIAMETROCOR.ObjetoParaControles: Boolean;
begin
  Try
     With ObjPERSIANAGRUPODIAMETROCOR do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtPersiana.text:=Persiana.Get_codigo;
        EdtGrupoPersiana.text:=GrupoPersiana.Get_codigo;
        EdtCor.text:=Cor.Get_codigo;
        EdtDiametro.text:=Diametro.Get_codigo;
        EdtEstoque.text:=Get_Estoque;
        EdtPrecoCusto.text:=Get_PrecoCusto;
        EdtPorcentagemInstalado.text:=Get_PorcentagemInstalado;
        EdtPorcentagemFornecido.text:=Get_PorcentagemFornecido;
        EdtPorcentagemRetirado.text:=Get_PorcentagemRetirado;
        EdtPrecoVendaInstalado.text:=Get_PrecoVendaInstalado;
        EdtPrecoVendaFornecido.text:=Get_PrecoVendaFornecido;
        EdtPrecoVendaRetirado.text:=Get_PrecoVendaRetirado;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFPERSIANAGRUPODIAMETROCOR.TabelaParaControles: Boolean;
begin
     If (ObjPERSIANAGRUPODIAMETROCOR.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFPERSIANAGRUPODIAMETROCOR.GuiaClick(Sender: TObject);
begin
Notebook.PageIndex:=Guia.TabIndex;
end;

procedure TFPERSIANAGRUPODIAMETROCOR.edtPersianaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjPERSIANAGRUPODIAMETROCOR.edtPersianakeydown(sender,key,shift,lbnomePersiana);
end;
 
procedure TFPERSIANAGRUPODIAMETROCOR.edtPersianaExit(Sender: TObject);
begin
    ObjPERSIANAGRUPODIAMETROCOR.edtPersianaExit(sender,lbnomePersiana);
end;
procedure TFPERSIANAGRUPODIAMETROCOR.edtGrupoPersianaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjPERSIANAGRUPODIAMETROCOR.edtGrupoPersianakeydown(sender,key,shift,lbnomeGrupoPersiana);
end;
 
procedure TFPERSIANAGRUPODIAMETROCOR.edtGrupoPersianaExit(Sender: TObject);
begin
    ObjPERSIANAGRUPODIAMETROCOR.edtGrupoPersianaExit(sender,lbnomeGrupoPersiana);
end;
procedure TFPERSIANAGRUPODIAMETROCOR.edtCorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjPERSIANAGRUPODIAMETROCOR.edtCorkeydown(sender,key,shift,lbnomeCor);
end;
 
procedure TFPERSIANAGRUPODIAMETROCOR.edtCorExit(Sender: TObject);
begin
    ObjPERSIANAGRUPODIAMETROCOR.edtCorExit(sender,lbnomeCor);
end;
procedure TFPERSIANAGRUPODIAMETROCOR.edtDiametroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjPERSIANAGRUPODIAMETROCOR.edtDiametrokeydown(sender,key,shift,lbnomeDiametro);
end;
 
procedure TFPERSIANAGRUPODIAMETROCOR.edtDiametroExit(Sender: TObject);
begin
    ObjPERSIANAGRUPODIAMETROCOR.edtDiametroExit(sender,lbnomeDiametro);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.
