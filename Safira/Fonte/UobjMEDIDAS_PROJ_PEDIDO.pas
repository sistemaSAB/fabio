unit UobjMEDIDAS_PROJ_PEDIDO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal
,UOBJPEDIDO_PROJ_PEDIDO;

Type
   TObjMEDIDAS_PROJ_PEDIDO=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                PedidoProjeto:TOBJPEDIDO_PROJ_PEDIDO;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaPorCodigoPedidoProjeto(Parametro:string):Boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Componente(parametro: string);
                Function Get_Componente: string;
                Procedure Submit_Altura(parametro: string);
                Function Get_Altura: string;
                Procedure Submit_Largura(parametro: string);
                Function Get_Largura: string;

                
                Procedure Submit_AlturaArredondamento(parametro: string);
                Function Get_AlturaArredondamento: string;
                Procedure Submit_LarguraArredondamento(parametro: string);
                Function Get_LarguraArredondamento: string;



                procedure EdtPedidoProjetoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;Ppedido:string);overload;
                Function  exclui_PP(PpedidoProjeto:string):boolean;

                Function BuscaComponente(PpedidoProjeto:string;PComponente:string;var Paltura:string;var PLargura:string;var palturaarredondamento:string;var plarguraarredondamento:string):boolean;
                Function RetornaMedidas(ppedidoprojeto:string):boolean;
                Function RetornaAlturaLarguraPedidiProjeto(Var PAltura, PLargura:string;PPedidoProjeto:string):Boolean;

                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Componente:string;
               Altura:string;
               Largura:string;

                AlturaArredondamento:string;
               LarguraArredondamento:string;


               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
  UMenuRelatorios;


{ TTabTitulo }


Function  TObjMEDIDAS_PROJ_PEDIDO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('PedidoProjeto').asstring<>'')
        Then Begin
                 If (Self.PedidoProjeto.LocalizaCodigo(FieldByName('PedidoProjeto').asstring)=False)
                 Then Begin
                          Messagedlg('Pedido Projeto N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PedidoProjeto.TabelaparaObjeto;
        End;
        Self.Componente:=fieldbyname('Componente').asstring;

        Self.Altura:=fieldbyname('Altura').asstring;
        Self.Largura:=fieldbyname('Largura').asstring;

        Self.AlturaArredondamento:=fieldbyname('AlturaArredondamento').asstring;
        Self.LarguraArredondamento:=fieldbyname('LarguraArredondamento').asstring;

//CODIFICA TABELAPARAOBJETO





        result:=True;
     End;
end;


Procedure TObjMEDIDAS_PROJ_PEDIDO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('PedidoProjeto').asstring:=Self.PedidoProjeto.GET_CODIGO;
        ParamByName('Componente').asstring:=Self.Componente;
        ParamByName('Altura').asstring:=virgulaparaponto(Self.Altura);
        ParamByName('Largura').asstring:=virgulaparaponto(Self.Largura);


        ParamByName('Alturaarredondamento').asstring:=virgulaparaponto(Self.Alturaarredondamento);
        ParamByName('Larguraarredondamento').asstring:=virgulaparaponto(Self.Larguraarredondamento);
  End;
End;

//***********************************************************************

function TObjMEDIDAS_PROJ_PEDIDO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjMEDIDAS_PROJ_PEDIDO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        PedidoProjeto.ZerarTabela;
        Componente:='';
        Altura:='';
        Largura:='';


        AlturaArredondamento:='';
        LarguraArredondamento:='';
//CODIFICA ZERARTABELA





     End;
end;

Function TObjMEDIDAS_PROJ_PEDIDO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (PedidoProjeto.get_codigo='')
      Then Mensagem:=mensagem+'/Pedido Projeto';
      If (Componente='')
      Then Mensagem:=mensagem+'/Componente';


      If (Altura='')
      Then Mensagem:=mensagem+'/Altura';

      If (Largura='')
      Then Mensagem:=mensagem+'/Largura';

      If (AlturaArredondamento='')
      Then Self.AlturaArredondamento:='0';

      If (LarguraArredondamento='')
      Then self.LarguraArredondamento:='0';

      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjMEDIDAS_PROJ_PEDIDO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.PedidoProjeto.LocalizaCodigo(Self.PedidoProjeto.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Pedido Projeto n�o Encontrado!'
      Else Self.PedidoProjeto.tabelaparaobjeto;
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjMEDIDAS_PROJ_PEDIDO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.PedidoProjeto.Get_Codigo<>'')
        Then Strtoint(Self.PedidoProjeto.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Pedido Projeto';
     End;

     try
        Strtofloat(Self.Altura);
     Except
           Mensagem:=mensagem+'/Altura';
     End;
     try
        Strtofloat(Self.Largura);
     Except
           Mensagem:=mensagem+'/Largura';
     End;


     try
        Strtofloat(Self.AlturaArredondamento);
     Except
           Mensagem:=mensagem+'/Altura Arredondamento';
     End;
     try
        Strtofloat(Self.LarguraArredondamento);
     Except
           Mensagem:=mensagem+'/Largura Arredondamento';
     End;

//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjMEDIDAS_PROJ_PEDIDO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjMEDIDAS_PROJ_PEDIDO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;


function TObjMEDIDAS_PROJ_PEDIDO.BuscaComponente(PpedidoProjeto: string;
  PComponente: string; var Paltura: string; var PLargura: string;var palturaarredondamento:string;var plarguraarredondamento:string): boolean;
begin
       result:=False;
       PLargura:='0';
       Paltura:='0';
       
       if (PpedidoProjeto='')
       or (Pcomponente='')
       Then Begin
                 Messagedlg('Par�metros vazios no BuscaComponente da Unit Uobjmedidas_proj',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,PedidoProjeto,Componente,Altura,Largura,AlturaArredondamento,LarguraArredondamento');
           SQL.ADD(' from  TabMedidas_Proj');
           SQL.ADD(' WHERE PedidoProjeto='+PpedidoProjeto);
           SQL.ADD(' and Componente='+#39+PComponente+#39);
           Open;
           If (recordcount>0)
           Then Begin
                    Result:=True;
                    Paltura:=Fieldbyname('altura').asstring;
                    PLargura:=Fieldbyname('largura').asstring;
                    PalturaArredondamento:=Fieldbyname('alturaarredondamento').asstring;
                    PlarguraArredondamento:=Fieldbyname('larguraarredondamento').asstring;
           End
           Else Result:=False;
       End;

end;

function TObjMEDIDAS_PROJ_PEDIDO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       result:=False;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro MEDIDAS_PROJ vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;                                                    
           SQL.ADD('Select CODIGO,PedidoProjeto,Componente,Altura,Largura,AlturaArredondamento,LarguraArredondamento');
           SQL.ADD(' from  TabMedidas_Proj');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjMEDIDAS_PROJ_PEDIDO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjMEDIDAS_PROJ_PEDIDO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjMEDIDAS_PROJ_PEDIDO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ObjdataSource:=TDataSource.create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjqueryPesquisa;
        
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;

        Self.PedidoProjeto:=TOBJPEDIDO_PROJ_PEDIDO.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabMedidas_Proj(CODIGO,PedidoProjeto,Componente');
                InsertSQL.add(' ,Altura,Largura,AlturaArredondamento,LarguraArredondamento)');
                InsertSQL.add('values (:CODIGO,:PedidoProjeto,:Componente,:Altura,:Largura,:AlturaArredondamento,:LarguraArredondamento');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabMedidas_Proj set CODIGO=:CODIGO,PedidoProjeto=:PedidoProjeto');
                ModifySQL.add(',Componente=:Componente,Altura=:Altura,Largura=:Largura,AlturaArredondamento=:AlturaArredondamento,LarguraArredondamento=:LarguraArredondamento');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabMedidas_Proj where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjMEDIDAS_PROJ_PEDIDO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjMEDIDAS_PROJ_PEDIDO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabMEDIDAS_PROJ');
     Result:=Self.ParametroPesquisa;
end;

function TObjMEDIDAS_PROJ_PEDIDO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de MEDIDAS_PROJ ';
end;


function TObjMEDIDAS_PROJ_PEDIDO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENMEDIDAS_PROJ,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENMEDIDAS_PROJ,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjMEDIDAS_PROJ_PEDIDO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(ObjqueryPesquisa);
    Freeandnil(ObjdataSource);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.PedidoProjeto.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjMEDIDAS_PROJ_PEDIDO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjMEDIDAS_PROJ_PEDIDO.RetornaCampoNome: string;
begin
      result:='componente';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjMEDIDAS_PROJ_PEDIDO.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjMEDIDAS_PROJ_PEDIDO.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjMEDIDAS_PROJ_PEDIDO.Submit_Componente(parametro: string);
begin
        Self.Componente:=Parametro;
end;
function TObjMEDIDAS_PROJ_PEDIDO.Get_Componente: string;
begin
        Result:=Self.Componente;
end;
procedure TObjMEDIDAS_PROJ_PEDIDO.Submit_Altura(parametro: string);
begin
        Self.Altura:=Parametro;
end;
function TObjMEDIDAS_PROJ_PEDIDO.Get_Altura: string;
begin
        Result:=Self.Altura;
end;
procedure TObjMEDIDAS_PROJ_PEDIDO.Submit_Largura(parametro: string);
begin
        Self.Largura:=Parametro;
end;
function TObjMEDIDAS_PROJ_PEDIDO.Get_Largura: string;
begin
        Result:=Self.Largura;
end;
//CODIFICA GETSESUBMITS


procedure TObjMEDIDAS_PROJ_PEDIDO.EdtPedidoProjetoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PedidoProjeto.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PedidoProjeto.tabelaparaobjeto;
End;


procedure TObjMEDIDAS_PROJ_PEDIDO.EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PedidoProjeto.Get_Pesquisa,Self.PedidoProjeto.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoProjeto.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.PedidoProjeto.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoProjeto.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjMEDIDAS_PROJ_PEDIDO.EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;Ppedido:string);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PedidoProjeto.Get_Pesquisaview(Ppedido),Self.PedidoProjeto.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoProjeto.RETORNACAMPOCODIGO).asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

//CODIFICA EXITONKEYDOWN

procedure TObjMEDIDAS_PROJ_PEDIDO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJMEDIDAS_PROJ';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
          0:begin
          End;
          End;
     end;

end;



function TObjMEDIDAS_PROJ_PEDIDO.exclui_PP(PpedidoProjeto: string): boolean;
begin
     Result:=False;
     Try
        With Self.Objquery do
        Begin
             close;
             sql.clear;
             sql.add('Delete from Tabmedidas_proj where PedidoProjeto='+Ppedidoprojeto);
             execsql;
             Result:=true;
        End;
     Except
           on e:exception do
           Begin
             Messagedlg('Erro na tentativas de Excluir as Medidas do Pedido Projeto N� '+PpedidoProjeto+#13+'Erro: '+E.message,mterror,[mbok],0);
             exit;
           End; 
     End;
end;




function TObjMEDIDAS_PROJ_PEDIDO.RetornaMedidas(ppedidoprojeto: string): boolean;
begin
     result:=False;
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select * from TabMedidas_proj where PedidoProjeto='+PpedidoProjeto);
          open;
     End;
     result:=true;
end;

function TObjMEDIDAS_PROJ_PEDIDO.LocalizaPorCodigoPedidoProjeto(Parametro: string): Boolean;
// Esta Funcao foi desenvoilvida apenas para ser usado no pedido impresso
// a mesma s� serve para devolver somente um registro.
// para projetos com eixo diferentes de AxL nao funciona
begin
       result:=False;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro MEDIDAS_PROJ vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,PedidoProjeto,Componente,Altura,Largura,AlturaArredondamento,LarguraArredondamento');
           SQL.ADD(' from  TabMedidas_Proj');
           SQL.ADD(' WHERE PedidoProjeto='+parametro);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjMEDIDAS_PROJ_PEDIDO.RetornaAlturaLarguraPedidiProjeto(Var PAltura, PLargura:string; PPedidoProjeto: string): Boolean;
begin
     // Este SQL somente traz a altura e largura casa o projeto seja de eixo = AxL
     // porque ser� usado somente pra mostrar as medidas anteriores, na hora de alterar
     // um projeto

     result:=false;
     try
         With Self.Objquery do
         Begin
              Close;
              Sql.Clear;
              Sql.Add('Select TabMedidas_Proj.Altura, TabMedidas_Proj.Largura');
              Sql.Add('from TabMEdidas_Proj');
              Sql.Add('Join TabPEdido_Proj on TabPedido_Proj.Codigo = TabMedidas_Proj.PedidoPRojeto');
              Sql.Add('Join TabProjeto on TabProjeto.Codigo = TabPedido_Proj.Projeto');
              Sql.Add('Where TabPedido_PRoj.Codigo = '+PPedidoprojeto);
              Sql.Add('and   TabProjeto.Eixo = ''AxL''  ');
              Open;

              PAltura:=fieldbyname('Altura').AsString;
              PLargura:=fieldbyname('LArgura').AsString;
              Result:=true;
         end;
     except
          Result:=false;
     end;
end;


function TObjMEDIDAS_PROJ_PEDIDO.Get_AlturaArredondamento: string;
begin
     Result:=Self.AlturaArredondamento;
end;

function TObjMEDIDAS_PROJ_PEDIDO.Get_LarguraArredondamento: string;
begin
     Result:=Self.LarguraArredondamento;
end;

procedure TObjMEDIDAS_PROJ_PEDIDO.Submit_AlturaArredondamento(
  parametro: string);
begin
     Self.AlturaArredondamento:=parametro;
end;

procedure TObjMEDIDAS_PROJ_PEDIDO.Submit_LarguraArredondamento(
  parametro: string);
begin
     Self.LarguraArredondamento:=parametro;
end;

end.



