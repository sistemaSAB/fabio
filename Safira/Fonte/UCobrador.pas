unit UCobrador;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask;

type
  TFCobrador = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    bt1: TSpeedButton;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    lbCodigo: TLabel;
    pnl1: TPanel;
    Imgimgrodape: TImage;
    lb14: TLabel;
    lb2: TLabel;
    edtcodigofuncionario: TEdit;
    lbLbNome: TLabel;
    edtEdtNome: TEdit;
    lbLbSexo: TLabel;
    COMBOComboSexo: TComboBox;
    lbLbEmail: TLabel;
    edtEdtEmail: TEdit;
    lbLbFone: TLabel;
    edtEdtFone: TEdit;
    lbLbCelular: TLabel;
    edtEdtCelular: TEdit;
    lbLbDataCadastro: TLabel;
    edtDataCadastro: TMaskEdit;
    lbLbDataNascimento: TLabel;
    edtDataNascimento: TMaskEdit;
    lb4: TLabel;
    edtsenharelatorios: TEdit;
    edtfaixadesconto: TEdit;
    lb3: TLabel;
    COMBOComboAtivo: TComboBox;
    lbLbAtivo: TLabel;
    lbLbObservacao: TLabel;
    mmoObservacao: TMemo;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCobrador: TFCobrador;

implementation

{$R *.dfm}

end.
