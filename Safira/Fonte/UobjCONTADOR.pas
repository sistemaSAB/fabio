unit UobjCONTADOR;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UOBJCIDADE
;
//USES_INTERFACE



Type
   TObjCONTADOR=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               CIDADE:TOBJCIDADE;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_NOME(parametro: string);
                Function Get_NOME: string;
                Procedure Submit_CPF(parametro: string);
                Function Get_CPF: string;
                Procedure Submit_CRC(parametro: string);
                Function Get_CRC: string;
                Procedure Submit_CNPJ(parametro: string);
                Function Get_CNPJ: string;
                Procedure Submit_CEP(parametro: string);
                Function Get_CEP: string;
                Procedure Submit_ENDERECO(parametro: string);
                Function Get_ENDERECO: string;
                Procedure Submit_NUMERO(parametro: string);
                Function Get_NUMERO: string;
                Procedure Submit_COMPLEMENTO(parametro: string);
                Function Get_COMPLEMENTO: string;
                Procedure Submit_BAIRRO(parametro: string);
                Function Get_BAIRRO: string;
                Procedure Submit_FONE(parametro: string);
                Function Get_FONE: string;
                Procedure Submit_FAX(parametro: string);
                Function Get_FAX: string;
                Procedure Submit_EMAIL(parametro: string);
                Function Get_EMAIL: string;
                procedure EdtCIDADEExit(Sender: TObject;LABELNOME:TLABEL);
procedure EdtCIDADEKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
//CODIFICA DECLARA GETSESUBMITS


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               NOME:string;
               CPF:string;
               CRC:string;
               CNPJ:string;
               CEP:string;
               ENDERECO:string;
               NUMERO:string;
               COMPLEMENTO:string;
               BAIRRO:string;
               FONE:string;
               FAX:string;
               EMAIL:string;
//CODIFICA VARIAVEIS PRIVADAS



               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, Ucidade;





Function  TObjCONTADOR.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.NOME:=fieldbyname('NOME').asstring;
        Self.CPF:=fieldbyname('CPF').asstring;
        Self.CRC:=fieldbyname('CRC').asstring;
        Self.CNPJ:=fieldbyname('CNPJ').asstring;
        Self.CEP:=fieldbyname('CEP').asstring;
        Self.ENDERECO:=fieldbyname('ENDERECO').asstring;
        Self.NUMERO:=fieldbyname('NUMERO').asstring;
        Self.COMPLEMENTO:=fieldbyname('COMPLEMENTO').asstring;
        Self.BAIRRO:=fieldbyname('BAIRRO').asstring;
        Self.FONE:=fieldbyname('FONE').asstring;
        Self.FAX:=fieldbyname('FAX').asstring;
        Self.EMAIL:=fieldbyname('EMAIL').asstring;
        If(FieldByName('CIDADE').asstring<>'')
        Then Begin
                 If (Self.CIDADE.LocalizaCodigo(FieldByName('CIDADE').asstring)=False)
                 Then Begin
                          Messagedlg('CIDADE N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CIDADE.TabelaparaObjeto;
        End;
//CODIFICA TABELAPARAOBJETO














        result:=True;
     End;
end;


Procedure TObjCONTADOR.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('NOME').asstring:=Self.NOME;
        ParamByName('CPF').asstring:=Self.CPF;
        ParamByName('CRC').asstring:=Self.CRC;
        ParamByName('CNPJ').asstring:=Self.CNPJ;
        ParamByName('CEP').asstring:=Self.CEP;
        ParamByName('ENDERECO').asstring:=Self.ENDERECO;
        ParamByName('NUMERO').asstring:=Self.NUMERO;
        ParamByName('COMPLEMENTO').asstring:=Self.COMPLEMENTO;
        ParamByName('BAIRRO').asstring:=Self.BAIRRO;
        ParamByName('FONE').asstring:=Self.FONE;
        ParamByName('FAX').asstring:=Self.FAX;
        ParamByName('EMAIL').asstring:=Self.EMAIL;
        ParamByName('CIDADE').asstring:=Self.CIDADE.GET_CODIGO;
//CODIFICA OBJETOPARATABELA    

  End;
End;

//***********************************************************************

function TObjCONTADOR.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCONTADOR.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        NOME:='';
        CPF:='';
        CRC:='';
        CNPJ:='';
        CEP:='';
        ENDERECO:='';
        NUMERO:='';
        COMPLEMENTO:='';
        BAIRRO:='';
        FONE:='';
        FAX:='';
        EMAIL:='';
        CIDADE.ZerarTabela;
//CODIFICA ZERARTABELA














     End;
end;

Function TObjCONTADOR.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin

    If (Codigo='') Then
      Mensagem:=mensagem+'/Codigo';

    if (CIDADE.Get_CODIGO = '') then
      Mensagem:=mensagem+'/Codigo';

    if (CPF = '') then
      Mensagem:=Mensagem+'/CPF';

    if (CRC = '') then
      Mensagem:=Mensagem+'/CRC';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCONTADOR.VerificaRelacionamentos: Boolean;
var
  mensagem:string;
Begin

  Result:=False;
  mensagem:='';

  if not (Self.CIDADE.LocalizaCodigo(Self.CIDADE.Get_CODIGO)) then
    Mensagem:=mensagem+'/ CIDADE n�o Encontrado!';

  If (mensagem<>'') then
  begin
    Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
    exit;
  end;

  result:=true;
  
End;

function TObjCONTADOR.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.CIDADE.Get_Codigo<>'')
        Then Strtoint(Self.CIDADE.Get_Codigo);
     Except
           Mensagem:=mensagem+'/CIDADE';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCONTADOR.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCONTADOR.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCONTADOR.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CONTADOR vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,NOME,CPF,CRC,CNPJ,CEP,ENDERECO,NUMERO,COMPLEMENTO');
           SQL.ADD(' ,BAIRRO,FONE,FAX,EMAIL,CIDADE');
           SQL.ADD(' from  TABCONTADOR');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCONTADOR.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjCONTADOR.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjCONTADOR.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjCONTADOR.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.CIDADE:=TOBJCIDADE.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABCONTADOR(Codigo,NOME,CPF,CRC,CNPJ,CEP');
                InsertSQL.add(' ,ENDERECO,NUMERO,COMPLEMENTO,BAIRRO,FONE,FAX,EMAIL');
                InsertSQL.add(' ,CIDADE)');
                InsertSQL.add('values (:Codigo,:NOME,:CPF,:CRC,:CNPJ,:CEP,:ENDERECO');
                InsertSQL.add(' ,:NUMERO,:COMPLEMENTO,:BAIRRO,:FONE,:FAX,:EMAIL,:CIDADE');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABCONTADOR set Codigo=:Codigo,NOME=:NOME,CPF=:CPF');
                ModifySQL.add(',CRC=:CRC,CNPJ=:CNPJ,CEP=:CEP,ENDERECO=:ENDERECO,NUMERO=:NUMERO');
                ModifySQL.add(',COMPLEMENTO=:COMPLEMENTO,BAIRRO=:BAIRRO,FONE=:FONE');
                ModifySQL.add(',FAX=:FAX,EMAIL=:EMAIL,CIDADE=:CIDADE');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABCONTADOR where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCONTADOR.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCONTADOR.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCONTADOR');
     Result:=Self.ParametroPesquisa;
end;

function TObjCONTADOR.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de CONTADOR ';
end;


function TObjCONTADOR.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCONTADOR,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCONTADOR,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCONTADOR.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.CIDADE.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCONTADOR.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCONTADOR.RetornaCampoNome: string;
begin
      result:='nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjCONTADOR.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjCONTADOR.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjCONTADOR.Submit_NOME(parametro: string);
begin
        Self.NOME:=Parametro;
end;
function TObjCONTADOR.Get_NOME: string;
begin
        Result:=Self.NOME;
end;
procedure TObjCONTADOR.Submit_CPF(parametro: string);
begin
        Self.CPF:=Parametro;
end;
function TObjCONTADOR.Get_CPF: string;
begin
        Result:=Self.CPF;
end;
procedure TObjCONTADOR.Submit_CRC(parametro: string);
begin
        Self.CRC:=Parametro;
end;
function TObjCONTADOR.Get_CRC: string;
begin
        Result:=Self.CRC;
end;
procedure TObjCONTADOR.Submit_CNPJ(parametro: string);
begin
        Self.CNPJ:=Parametro;
end;
function TObjCONTADOR.Get_CNPJ: string;
begin
        Result:=Self.CNPJ;
end;
procedure TObjCONTADOR.Submit_CEP(parametro: string);
begin
        Self.CEP:=Parametro;
end;
function TObjCONTADOR.Get_CEP: string;
begin
        Result:=Self.CEP;
end;
procedure TObjCONTADOR.Submit_ENDERECO(parametro: string);
begin
        Self.ENDERECO:=Parametro;
end;
function TObjCONTADOR.Get_ENDERECO: string;
begin
        Result:=Self.ENDERECO;
end;
procedure TObjCONTADOR.Submit_NUMERO(parametro: string);
begin
        Self.NUMERO:=Parametro;
end;
function TObjCONTADOR.Get_NUMERO: string;
begin
        Result:=Self.NUMERO;
end;
procedure TObjCONTADOR.Submit_COMPLEMENTO(parametro: string);
begin
        Self.COMPLEMENTO:=Parametro;
end;
function TObjCONTADOR.Get_COMPLEMENTO: string;
begin
        Result:=Self.COMPLEMENTO;
end;
procedure TObjCONTADOR.Submit_BAIRRO(parametro: string);
begin
        Self.BAIRRO:=Parametro;
end;
function TObjCONTADOR.Get_BAIRRO: string;
begin
        Result:=Self.BAIRRO;
end;
procedure TObjCONTADOR.Submit_FONE(parametro: string);
begin
        Self.FONE:=Parametro;
end;
function TObjCONTADOR.Get_FONE: string;
begin
        Result:=Self.FONE;
end;
procedure TObjCONTADOR.Submit_FAX(parametro: string);
begin
        Self.FAX:=Parametro;
end;
function TObjCONTADOR.Get_FAX: string;
begin
        Result:=Self.FAX;
end;
procedure TObjCONTADOR.Submit_EMAIL(parametro: string);
begin
        Self.EMAIL:=Parametro;
end;
function TObjCONTADOR.Get_EMAIL: string;
begin
        Result:=Self.EMAIL;
end;
//CODIFICA GETSESUBMITS


procedure TObjCONTADOR.EdtCIDADEExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.CIDADE.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.CIDADE.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.CIDADE.GET_NOME;
End;
procedure TObjCONTADOR.EdtCIDADEKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FCIDADE:TFCIDADE;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCIDADE:=TFCIDADE.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.CIDADE.Get_Pesquisa,Self.CIDADE.Get_TituloPesquisa,FCIDADE)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('CODIGO').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCIDADE);
     End;
end;
//CODIFICA EXITONKEYDOWN

procedure TObjCONTADOR.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCONTADOR';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



end.



