NFE ORIENTACAO DANFE
RETRATO;

NFE FORMA DE EMISAO DO DANFE
NORMAL ON-LINE;

NFE AMBIENTE
HOMOLOGACAO;

NFE CAMINHO LOGO MARCA DANFE
;

NFE SALVAR ARQUIVOS DE ENVIO E RESPOSTA
-1;

NFE CAMINHO DOS ARQUIVOS DE ENVIO E RESPOSTA
C:\EXCLAIM\NF-E\XML\;

NFE CAMINHO DOS ARQUIVOS XML
C:\Exclaim\NF-E\XML\;

CAMINHO XML DA CARTA DE CORRECAO
C:\Exclaim\NF-E\CARTACORRECAO\;

NFE CAMINHO DOS ARQUIVOS DE CANCELAMENTO
C:\Exclaim\NF-E\CANCELAMENTO\;

NFE CAMINHO DOS ARQUIVOS DE INUTILIZACAO
C:\Exclaim\NF-E\INUTILIZACAO\;

NFE UF WEBSERVICE
MS;

NFE VISUALIZAR MENSAGEM DE TRANSMISSAO
0;

NFE ARQUIVO RAVE
c:\exclaim\nf-e\notafiscaleletronica.rav;

NFE DIRETORIO BACKUP
C:\Exclaim\NF-E\NFE XML DE BACKUP\;

NFE DESENVOLVEDOR DO SISTEMA
Exclaim Tecnologia;

EXPANDIR LOGO MARCA
0;

CERTIFICADO
6F671DA66CD83AFAA247F3BF2167C83F;

ZERA IMPOSTO NO SIMPLES
-1;

IDE
select nf.naturezaoperacao,nf.dataemissao,nf.datasaida,'1' as serienf,
nf.horasaida,nf.indpag
from tabnotafiscal nf
where nf.codigo = :pcodigoNF;

INFADIC
select nf.dadosadicionais from tabnotafiscal nf 
where nf.codigo = :pcodigoNF;

EMITENTE
select e.cnpj,e.razaosocial,e.ie, '' as IEST,e.fantasia,e.estado,e.crt
from tabempresa e
where e.codigo = 1;

ENDERECO EMITENTE
select e.fone,e.cep,e.endereco,e.numero,e.complemento,e.bairro,e.codigocidade,
e.cidade,e.estado,'1058' as CODIGOPAIS,'BRASIL' as PAIS
from tabempresa e
where e.codigo = 1;

DESTINATARIO CLIENTE
select c.cpf_cgc,'' AS ieprodutorrural,c.nome,c.estado,c.rg_ie,c.fisica_juridica,
c.email
from tabcliente c
inner join tabnotafiscal nf on nf.cliente = c.codigo
where nf.codigo = :pCodigoNF;

DESTINATARIO FORNECEDOR
select f.cgc,f.ie,f.razaosocial,f.estado,f.email
from tabfornecedor f
inner join tabnotafiscal nf on nf.fornecedor = f.codigo
where nf.codigo = :pCodigoNF;

ENDERECO DESTINATARIO CLIENTE 
select c.cep,c.codigocidade,c.estado,c.fone,
c.bairro,c.codigopais,'BRASIL' as pais,c.endereco,
c.cidade,c.numero
from tabcliente c
inner join tabnotafiscal nf on nf.cliente = c.codigo
where nf.codigo = :pCodigoNF;

ENDERECO DESTINATARIO FORNECEDOR
select f.cep,f.codigocidade,f.estado,f.fone,f.bairro,
f.codigopais,'BRASIL' as pais,f.endereco,f.cidade,f.numero
from tabfornecedor f
inner join tabnotafiscal nf on nf.fornecedor = f.codigo
where nf.codigo = :pCodigoNF;

TRANSPORTE
select nf.freteporcontatransportadora
from tabnotafiscal nf
where nf.codigo = :pCodigoNF;

TRANSPORTA
select t.cpfcnpj,t.ie,t.municipio,t.uf,t.endereco,t.nome
from tabtransportadora t
inner join tabnotafiscal nf on nf.transportadora = t.codigo
where nf.codigo = :pCodigoNF;

VEICULO TRANSPORTADORA
select t.placa,t.ufplaca
from tabtransportadora t
inner join tabnotafiscal nf on nf.transportadora = t.codigo
where nf.codigo = :pCodigoNF;

VOLUMES TRANSPORTADOS
select nf.quantidade,nf.especie,nf.marca,nf.numerovolumes,nf.pesobruto,nf.pesoliquido
from tabnotafiscal nf
where nf.codigo = :pCodigoNF;

PRODUTOS 
select pnf.cfop,pnf.codigoproduto as produto,pnf.descricao||' - '||pnf.cor as descricao,pnf.quantidade,
pnf.quantidade as qTrib, pnf.unidade,pnf.unidade as uTrib,pnf.valorunitario,pnf.valorunitario as vUnTrib, 
(pnf.valorfinal)as valorfinal,pnf.desconto,pnf.ncm,pnf.valorfrete,pnf.valorseguro, '0' as valoroutros,
pnf.codigo,null as codigoanp from viewprodutosvenda_NFE pnf where pnf.notafiscal = :pCodigoNF;

ICMS NORMAL
select pnf.situacaotributaria_tabelaa,pnf.situacaotributaria_tabelab,pnf.csosn, pnf.bc_icms,pnf.valor_icms,pnf.aliquota,icms.perc_reducao_bc,
pnf.vtottrib from viewprodutosvenda_nfe pnf join tabimposto_icms icms on icms.codigo=pnf.imposto_icms_origem 
where pnf.notafiscal = :pCodigoNF and pnf.codigo = :pCodigoProdNF;

ICMS ST 
select icms.perc_reducao_bc_st,pnf.bc_icms_st,pnf.aliquotast,
pnf.valor_icms_st,pnf.situacaotributaria_tabelab
from viewprodutosvenda_nfe pnf
join tabimposto_icms icms on icms.codigo=pnf.imposto_icms_origem
where pnf.notafiscal = :pCodigoNF and pnf.codigo = :pCodigoProdNF;

PIS
select pnf.valorfinal as bc_pis,ipis.aliquotapercentual,pnf.valor_pis,'0' as vAliqProd,
'0' as qBCprod,ipis.st
from viewprodutosvenda_nfe pnf
join tabimposto_pis ipis on ipis.codigo=pnf.imposto_pis_origem
where pnf.notafiscal = :pCodigoNF and pnf.codigo = :pCodigoProdNF;

COFINS
select pnf.valorfinal as bc_cofins,cofins.aliquotapercentual,pnf.valor_cofins,'0' as vAliqProd,
'0' as qBCprod,cofins.st
from viewprodutosvenda_nfe pnf
join tabimposto_cofins cofins on cofins.codigo=pnf.imposto_cofins_destino
where pnf.notafiscal = :pCodigoNF and pnf.codigo = :pCodigoProdNF;

IPI
select ipi.st,pnf.valorfinal as BCIPI,ipi.aliquota,pnf.valor_ipi
from viewprodutosvenda_nfe pnf
join tabimposto_ipi ipi on ipi.codigo=pnf.imposto_ipi
where pnf.notafiscal = :pCodigoNF and pnf.codigo = :pCodigoProdNF;

COBRANCA FATURA
select f.nfat,f.vorig,f.vdesc,f.vliq
from tabcobrfatura f
where f.notafiscal = :pCodigoNF;

COBRANCA DUPLICATA
select d.ndup,d.dvenc,d.vdup
from tabcobrduplicata d
inner join tabcobrfatura f on f.codigo = d.cobrfatura
where f.notafiscal = :pCodigoNF;

PROXIMO NUMERO
select first 1 codigo,numero
from tabnotafiscal
where upper (situacao) = 'N' and modelo_nf = 2
order by codigo;

CODIGO UF EMITENTE
select e.codigoestado
from tabempresa e
where e.codigo = 1;

CODIGO CIDADE EMITENTE
select e.codigocidade
from tabempresa e
where e.codigo = 1;

EVENTO CCE
select MAX(cc.NSEQEVENTO)  from TABCARTACORRECAO cc  
where cc.chaveacesso = :chaveAcesso;
