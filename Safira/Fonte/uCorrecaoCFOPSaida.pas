unit uCorrecaoCFOPSaida;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ComCtrls, StdCtrls, ExtCtrls,{UobjPRODUTOSNOTAFISCAL,}db,
  IBCustomDataSet, IBQuery;

type
  TfCorrecaoCFOPSaida = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    Label2: TLabel;
    editNota: TEdit;
    Panel3: TPanel;
    StatusBar1: TStatusBar;
    dbGridProdutos: TDBGrid;
    Label1: TLabel;
    lbCodigoProduto: TLabel;
    Label3: TLabel;
    lbNumero: TLabel;
    Label4: TLabel;
    lbModeloNF: TLabel;
    Label6: TLabel;
    lbSituacao: TLabel;
    Label8: TLabel;
    lbNotaComplementar: TLabel;
    Label10: TLabel;
    lbValorBaseCalculo: TLabel;
    Label12: TLabel;
    lbDataEmissao: TLabel;
    Label5: TLabel;
    lbValorNota: TLabel;
    Label7: TLabel;
    lbValorDesconto: TLabel;
    Label13: TLabel;
    lbValorICMS: TLabel;
    Label15: TLabel;
    lbValorProduto: TLabel;
    Label16: TLabel;
    lbQuantidadeProduto: TLabel;
    Label17: TLabel;
    lbValorDescontoProduto: TLabel;
    Label19: TLabel;
    lbReducaoProduto: TLabel;
    Label21: TLabel;
    lbValorBaseCalculoProduto: TLabel;
    Label23: TLabel;
    lbAliquotaProduto: TLabel;
    Label25: TLabel;
    lbValorICMSProduto: TLabel;
    IBQuery1: TIBQuery;
    DataSource1: TDataSource;
    Label9: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    lbCFOP: TLabel;
    lbCSOSN: TLabel;
    lbCSTICMS: TLabel;
    Label18: TLabel;
    lbValorFinalProduto: TLabel;
    lbCodigoNota: TLabel;
    Label20: TLabel;
    Label22: TLabel;
    lbCodigoProdutoNF: TLabel;
    Label24: TLabel;
    lbValorTotal: TLabel;
    procedure editNotaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbGridProdutosDblClick(Sender: TObject);
    procedure dbGridProdutosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure editNotaExit(Sender: TObject);
    procedure Label14DblClick(Sender: TObject);
    procedure Label11DblClick(Sender: TObject);
    procedure Label9DblClick(Sender: TObject);
    procedure Label23DblClick(Sender: TObject);
    procedure Label19DblClick(Sender: TObject);
    procedure Label18DblClick(Sender: TObject);
    procedure Label17DblClick(Sender: TObject);
    procedure Label16DblClick(Sender: TObject);
    procedure Label15DblClick(Sender: TObject);
    procedure Label13DblClick(Sender: TObject);
    procedure Label10DblClick(Sender: TObject);
    procedure Label24DblClick(Sender: TObject);
    procedure Label5Click(Sender: TObject);
    procedure Label7DblClick(Sender: TObject);
  private

      function get_situacao(pSituacao:string):string;
      function get_complementar(pComplementar:string):string;
      function get_valor(parametro:string;addFormato:string = ' R$'):string;
      function get_percentual(parametro:string):string;
      function get_campoUpdatePNF(pCampo:string):string;
      function get_campoUpdateNF(pCampo:string):string;
      function get_codigoProduto(parametro:string):string;

      procedure atualizaDadosProdutos();
      procedure gravaDadoProdutoNF(Sender:TObject);
      procedure gravaDadosNF(Sender:TObject);

  public


    posicaoLinhaGrid:Integer;
    
  //  objProdutosNotafiscal:TObjPRODUTOSNOTAFISCAL;

    function tabelaParaControles():Boolean;

    procedure limpaEdit();
    procedure limpaLabels();
    procedure retornaProdutos(pnota:string);

  end;

var
  fCorrecaoCFOPSaida: TfCorrecaoCFOPSaida;

implementation

uses Upesquisa, UessencialGlobal, UDataModulo;

{$R *.dfm}

procedure TfCorrecaoCFOPSaida.editNotaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
   FpesquisaLocal:TFpesquisa;
   pPesquisa:string;
begin

 { FpesquisaLocal:=nil;

  if (Key <> vk_f9) then Exit;

  Try

    if (TEdit(sender).Text <> '') then
      pPesquisa:=TEdit(sender).Text
    else
    begin

      Fpesquisalocal:=Tfpesquisa.create(Self);

      If (FpesquisaLocal.PreparaPesquisa(objProdutosNotafiscal.NOTAFISCAL.Get_pesquisa,objProdutosNotafiscal.NOTAFISCAL.Get_TituloPesquisa,Nil)) Then
      Begin

        Try

          If (FpesquisaLocal.showmodal=mrok) Then
          Begin

            If objProdutosNotafiscal.NOTAFISCAL.status<>dsinactive then exit;

            objProdutosNotafiscal.NOTAFISCAL.ZerarTabela;

            If (objProdutosNotafiscal.NOTAFISCAL.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False) Then
            Begin
              Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
              exit;
            End;

            objProdutosNotafiscal.NOTAFISCAL.TabelaparaObjeto;
            pPesquisa:=self.objProdutosNotafiscal.NOTAFISCAL.Get_CODIGO;

          End;

        Finally

          FpesquisaLocal.QueryPesq.close;

        End;

      End;

    end;


    If objProdutosNotafiscal.NOTAFISCAL.status<>dsinactive then exit;

    objProdutosNotafiscal.NOTAFISCAL.ZerarTabela;

    If not (objProdutosNotafiscal.NOTAFISCAL.LocalizaCodigo(pPesquisa)) Then
    Begin
      Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
      exit;
    End else
      objProdutosNotafiscal.NOTAFISCAL.TabelaparaObjeto;


    If (self.TabelaParaControles=False) Then
    Begin

      Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
      self.limpaedit();
      Self.LimpaLabels;
      exit;

    End else
    begin
      self.retornaProdutos(pPesquisa);
      self.atualizaDadosProdutos();
    end;

  Finally

    if (FPesquisaLocal <> nil) then
      FreeandNil(FPesquisaLocal);

  End;     }

end;

procedure TfCorrecaoCFOPSaida.FormShow(Sender: TObject);
begin

  {self.objProdutosNotafiscal:=TobjProdutosNotafiscal.Create;
  self.IBQuery1.Database:=FDataModulo.IBDatabase;

  self.limpaLabels;
  self.limpaEdit;     }

end;

procedure TfCorrecaoCFOPSaida.FormClose(Sender: TObject;var Action: TCloseAction);
begin

  //self.objProdutosNotafiscal.Free;

end;

function TfCorrecaoCFOPSaida.tabelaParaControles: Boolean;
begin

  {result:=False;

  self.limpaLabels;
  self.limpaEdit;

  {dados da nota}
 { self.editNota.Text               := self.objProdutosNotafiscal.NOTAFISCAL.Get_CODIGO;
  self.lbNumero.Caption            := self.objProdutosNotafiscal.NOTAFISCAL.Get_Numero;
  self.lbModeloNF.Caption          := self.objProdutosNotafiscal.NOTAFISCAL.Modelonf.Get_CODIGO + ' - '+self.objProdutosNotafiscal.NOTAFISCAL.Modelonf.Get_MODELO;
  Self.lbSituacao.Caption          := self.get_situacao(self.objProdutosNotafiscal.NOTAFISCAL.Get_Situacao);
  self.lbNotaComplementar.Caption  := self.get_complementar(self.objProdutosNotafiscal.NOTAFISCAL.get_NFcomplementar);
  self.lbDataEmissao.Caption       := self.objProdutosNotafiscal.NOTAFISCAL.Get_dataemissao;
  self.lbValorNota.Caption         := self.get_valor(self.objProdutosNotafiscal.NOTAFISCAL.Get_VALORFINAL);
  self.lbValorDesconto.Caption     := self.get_valor(self.objProdutosNotafiscal.NOTAFISCAL.Get_DESCONTO);
  self.lbValorBaseCalculo.Caption  := self.get_valor(self.objProdutosNotafiscal.NOTAFISCAL.Get_BASECALCULOICMS);
  self.lbValorICMS.Caption         := self.get_valor(self.objProdutosNotafiscal.NOTAFISCAL.Get_VALORICMS);
  self.lbValorTotal.Caption        := self.get_valor(self.objProdutosNotafiscal.NOTAFISCAL.Get_VALORTOTAL);
  self.lbCodigoNota.Caption        := self.objProdutosNotafiscal.NOTAFISCAL.Get_CODIGO;

  result:=True;       }

end;

procedure TfCorrecaoCFOPSaida.limpaEdit;
begin

  self.editNota.Text:='';

end;

procedure TfCorrecaoCFOPSaida.limpaLabels;
begin

  {DADOS DA NOTA}
  self.lbNumero.Caption:='';
  self.lbSituacao.Caption:='';
  self.lbNotaComplementar.Caption:='';
  self.lbDataEmissao.Caption:='__/__/__';
  self.lbModeloNF.Caption:='';
  self.lbValorNota.Caption:='';
  Self.lbValorDesconto.Caption:='';
  Self.lbValorBaseCalculo.Caption:='';
  self.lbValorICMS.Caption:='';
  self.lbCodigoNota.Caption:='';
  self.lbCodigoProdutoNF.Caption:='';


  {DADOS DO PRODUTO}
  self.lbCodigoProduto.Caption:='';
  Self.lbValorProduto.Caption:='';
  self.lbQuantidadeProduto.Caption:='';
  self.lbValorDescontoProduto.Caption:='';
  self.lbReducaoProduto.Caption:='';
  self.lbValorBaseCalculoProduto.Caption:='';
  self.lbAliquotaProduto.Caption:='';
  self.lbValorICMSProduto.Caption:='';
  self.lbCFOP.Caption:='';
  self.lbCSOSN.Caption:='';
  self.lbCSTICMS.Caption:='';
  self.lbValorFinalProduto.Caption:='';

end;

function TfCorrecaoCFOPSaida.get_situacao(pSituacao:string): string;
begin

  pSituacao:=UpperCase(pSituacao);

  if (pSituacao = 'G') or (pSituacao = 'I') then
    result := 'G - Gerada'
  else if (pSituacao = 'C') then
    result := 'C - Cancelada'

  else if (pSituacao = 'Z') then
    result := 'Z - Inutilizada'

  else if (pSituacao = 'A') or (pSituacao = 'N') then
    result := pSituacao+' - Aberta para uso'

  else if (pSituacao = 'T') then
    result := 'T - Conting�ncia'

  else if (pSituacao = 'P') then
    result := 'P - Em processamento'

  else
    result := 'ERRO';


end;

function TfCorrecaoCFOPSaida.get_complementar(pComplementar: string): string;
begin

  if (pComplementar = 'S') then
    Result := 'S - SIM'

  else if (pComplementar = 'N') then
    result := 'N - N�O'
    
  else
    result := 'ERRO';

end;

function TfCorrecaoCFOPSaida.get_valor(parametro: string;addFormato:string): string;
begin

  result:=FormatCurr('0.00,'+addFormato,StrToCurrDef(parametro,0));

end;

function TfCorrecaoCFOPSaida.get_percentual(parametro:string): string;
begin

  result:=get_valor(parametro,' %');

end;

procedure TfCorrecaoCFOPSaida.atualizaDadosProdutos;
var
  aliquota,reducao,BC_ICMS,valorFinal,VL_ICMS:Currency;
begin

  {self.lbCodigoProduto.Caption           := self.dbGridProdutos.DataSource.DataSet.fieldByName('produto').AsString +' - '+get_campoTabela('descricao','codigo','TABPRODUTO',self.dbGridProdutos.DataSource.DataSet.fieldByName('produto').AsString);
  Self.lbQuantidadeProduto.Caption       := self.dbGridProdutos.DataSource.DataSet.fieldByName('quantidade').AsString;
  self.lbValorDescontoProduto.Caption    := self.dbGridProdutos.DataSource.DataSet.fieldByName('desconto').AsString;
  self.lbValorProduto.Caption            := self.get_valor(self.dbGridProdutos.DataSource.DataSet.fieldByName('valor').AsString);
  self.lbValorFinalProduto.Caption       := self.get_valor(self.dbGridProdutos.DataSource.DataSet.fieldByName('valorFinal').AsString);
  self.lbCFOP.Caption                    := self.dbGridProdutos.DataSource.DataSet.fieldByName('cfop').AsString;
  self.lbCSOSN.Caption                   := self.dbGridProdutos.DataSource.DataSet.fieldByName('csosn').AsString;
  self.lbCodigoNota.Caption              := self.dbGridProdutos.DataSource.DataSet.fieldByName('notafiscal').AsString;
  self.lbCodigoProdutoNF.Caption         := self.dbGridProdutos.DataSource.DataSet.fieldByName('codigo').AsString;


  if (self.dbGridProdutos.DataSource.DataSet.fieldByName('CST_ICMS').AsString <> '') then
    self.lbCSTICMS.Caption := CompletaPalavra_a_Esquerda(self.dbGridProdutos.DataSource.DataSet.fieldByName('CST_ICMS').AsString,2,'0');

  if (self.dbGridProdutos.DataSource.DataSet.fieldByName('aliquota').AsCurrency > 0) then
  begin

    aliquota   := self.dbGridProdutos.DataSource.DataSet.fieldByName('aliquota').AsCurrency;
    reducao    := self.dbGridProdutos.DataSource.DataSet.fieldByName('reducaobasecalculo').AsCurrency;
    valorFinal := self.dbGridProdutos.DataSource.DataSet.fieldByName('valorFinal').AsCurrency;

    BC_ICMS :=  valorFinal - (reducao*valorFinal/100);
    VL_ICMS :=  BC_ICMS*aliquota/100;

    self.lbReducaoProduto.Caption:=self.get_valor(CurrToStr(reducao));
    self.lbValorBaseCalculoProduto.Caption:=self.get_valor(CurrToStr(BC_ICMS));
    self.lbAliquotaProduto.Caption:=self.get_valor(CurrToStr(aliquota));
    self.lbValorICMSProduto.Caption:=self.get_valor(CurrToStr(VL_ICMS));

  end else
  begin

    self.lbReducaoProduto.Caption:=self.get_valor(self.dbGridProdutos.DataSource.DataSet.fieldByName('reducaobasecalculo').AsString);
    self.lbValorBaseCalculoProduto.Caption:=self.get_valor('0');
    self.lbAliquotaProduto.Caption:=self.get_valor(self.dbGridProdutos.DataSource.DataSet.fieldByName('aliquota').AsString);
    self.lbValorICMSProduto.Caption:=self.get_valor('0');

  end;   }

end;

procedure TfCorrecaoCFOPSaida.retornaProdutos(pnota:string);
begin

 { if (pnota = '') then pnota:='-1';

  self.IBQuery1.Active:=False;
  self.IBQuery1.SQL.Clear;

  self.IBQuery1.SQL.Add('select pnf.codigo,pnf.produto,pnf.quantidade,pnf.desconto,pnf.reducaobasecalculo,');
  self.IBQuery1.SQL.Add('pnf.aliquota,pnf.cfop,pnf.csosn,pnf.sttabelab as CST_ICMS,');
  self.IBQuery1.SQL.Add('pnf.valor,pnf.valorFinal,pnf.notafiscal');
  self.IBQuery1.SQL.Add('from tabprodutosnotafiscal pnf');
  self.IBQuery1.SQL.Add('where pnf.notafiscal = '+pnota);

  self.IBQuery1.Active:=True;     }

end;

procedure TfCorrecaoCFOPSaida.dbGridProdutosDblClick(Sender: TObject);
begin

  self.posicaoLinhaGrid:=self.dbGridProdutos.DataSource.DataSet.RecNo;
  self.atualizaDadosProdutos();

end;

procedure TfCorrecaoCFOPSaida.dbGridProdutosKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin

  if (Key = VK_RETURN) then self.atualizaDadosProdutos();

end;

procedure TfCorrecaoCFOPSaida.editNotaExit(Sender: TObject);
var
  a: TShiftState;
  b: Word;
begin

  b:=vk_f9;
  
  editNotaKeyDown(Sender,b,a);

end;

procedure TfCorrecaoCFOPSaida.gravaDadoProdutoNF(Sender:TObject);
var
  campoUpdate,novoValor:string;
begin

 { if not (InputQuery('Altera valores','Digite novo valor:',novoValor)) then
    Exit;


  campoUpdate:=get_campoUpdatePNF(TLabel(Sender).Caption);

  if (exec_sql('update TABPRODUTOSNOTAFISCAL set '+campoUpdate+' = '+#39+novoValor+#39+' where codigo = '+self.lbCodigoProdutoNF.Caption)) then
  begin

    self.retornaProdutos(lbCodigoNota.Caption);
    self.dbGridProdutos.DataSource.DataSet.RecNo:=self.posicaoLinhaGrid;
    self.atualizaDadosProdutos();
    self.dbGridProdutos.DataSource.DataSet.RecNo:=self.posicaoLinhaGrid;

    FDataModulo.IBTransaction.CommitRetaining;
  end                                              
  else
  begin

    FDataModulo.IBTransaction.RollbackRetaining;
    MensagemErro('N�o foi possivel alterar');

  end;      }

end;

procedure TFCorrecaoCFOPSaida.gravaDadosNF(sender:TObject);
var
  campoUpdate,novoValor:string;
begin

 { if not (InputQuery('Altera valores','Digite novo valor:',novoValor)) then
    Exit;


  campoUpdate:=get_campoUpdateNF(TLabel(Sender).Caption);

  if (exec_sql('update TABNOTAFISCAL set '+campoUpdate+' = '+#39+novoValor+#39+' where codigo = '+self.lbCodigoNota.Caption)) then
  begin

    FDataModulo.IBTransaction.CommitRetaining;
    self.objProdutosNotafiscal.NOTAFISCAL.LocalizaCodigo(lbCodigoNota.Caption);
    self.objProdutosNotafiscal.NOTAFISCAL.TabelaparaObjeto;
    self.tabelaParaControles;

  end                                              
  else
  begin

    FDataModulo.IBTransaction.RollbackRetaining;
    MensagemErro('N�o foi possivel alterar');

  end;      }

  

end;

function TfCorrecaoCFOPSaida.get_campoUpdatePNF(pCampo: string): string;
begin

  if (pCampo = 'CST ICMS') then
    Result := 'sttabelab'

  else if (pCampo = 'Redu��o BC' ) then
    result := 'reducaobasecalculo'

  else if (pCampo = 'Valor Final') then
    Result := 'valorfinal'

  else if (pCampo = 'Base c�lculo') then
    result := 'BASECALCULOICMS'

  else if (pCampo = 'Valor da nota') then
    result := 'VALORFINAL'

  else
    result := pCampo;

end;

function TfCorrecaoCFOPSaida.get_campoUpdateNF(pCampo: string): string;
begin

  if (pCampo = 'ICMS') then
    Result := 'VALORICMS'

  else if (pCampo = 'Base c�lculo' ) then
    result := 'BASECALCULOICMS'

  else if (pCampo = 'Valor Final') then
    Result := 'VALORFINAL'

  else if (pCampo = 'Desconto') then
    result := 'DESCONTO'

  else if (pCampo = 'Valor total') then
    result := 'VALORTOTAL'
  else
    result := pCampo;

end;

function TfCorrecaoCFOPSaida.get_codigoProduto(parametro: string): string;
var
  i:integer;
begin

  result:='';

  i:=1;
  while (parametro[i] <> ' ') and (i<=Length(parametro)) do
  begin
    result:=Result + parametro[i];
    i:=i+1;
  end;

end;

procedure TfCorrecaoCFOPSaida.Label14DblClick(Sender: TObject);
begin
self.gravaDadoProdutoNF(Sender);
end;

procedure TfCorrecaoCFOPSaida.Label11DblClick(Sender: TObject);
begin
self.gravaDadoProdutoNF(Sender);
end;

procedure TfCorrecaoCFOPSaida.Label9DblClick(Sender: TObject);
begin
self.gravaDadoProdutoNF(Sender);
end;

procedure TfCorrecaoCFOPSaida.Label23DblClick(Sender: TObject);
begin
self.gravaDadoProdutoNF(Sender);
end;

procedure TfCorrecaoCFOPSaida.Label19DblClick(Sender: TObject);
begin
self.gravaDadoProdutoNF(Sender);
end;

procedure TfCorrecaoCFOPSaida.Label18DblClick(Sender: TObject);
begin
self.gravaDadoProdutoNF(Sender);
end;

procedure TfCorrecaoCFOPSaida.Label17DblClick(Sender: TObject);
begin
self.gravaDadoProdutoNF(Sender);
end;

procedure TfCorrecaoCFOPSaida.Label16DblClick(Sender: TObject);
begin
self.gravaDadoProdutoNF(Sender);
end;

procedure TfCorrecaoCFOPSaida.Label15DblClick(Sender: TObject);
begin
self.gravaDadoProdutoNF(Sender);
end;

procedure TfCorrecaoCFOPSaida.Label13DblClick(Sender: TObject);
begin
self.gravaDadosNF(Sender);
end;

procedure TfCorrecaoCFOPSaida.Label10DblClick(Sender: TObject);
begin
self.gravaDadosNF(Sender);
end;

procedure TfCorrecaoCFOPSaida.Label24DblClick(Sender: TObject);
begin
self.gravaDadosNF(Sender);
end;

procedure TfCorrecaoCFOPSaida.Label5Click(Sender: TObject);
begin
self.gravaDadosNF(Sender);
end;

procedure TfCorrecaoCFOPSaida.Label7DblClick(Sender: TObject);
begin
self.gravaDadosNF(Sender);
end;

end.
