unit UOrdemServico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, ComCtrls, TabNotBk, Grids, DBGrids,UessencialGlobal,UobjORDEMSERVICO,
  DB,UDataModulo,Uobjservicosos,UCLIENTE,UobjPEDIDO_PEDIDO,IBQuery,UobjMATERIAISOS,UobjVIDROSERVICOOS,
  ImgList, Menus;

type
  TFOrdemServico = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    ImageRodape: TImage;
    lbDataOS: TLabel;
    lb13: TLabel;
    lbValorFinalOS: TLabel;
    bt1: TSpeedButton;
    panel2: TPanel;
    lbLbCliente: TLabel;
    lbNomeCliente: TLabel;
    lbNomeVendedor: TLabel;
    lbLbVendedor: TLabel;
    lbLbDescricao: TLabel;
    lbconcluido: TLabel;
    lb9: TLabel;
    lb12: TLabel;
    lbNumPedido: TLabel;
    edtCliente: TEdit;
    edtVendedor: TEdit;
    edtDescricao: TEdit;
    memoObservacao: TMemo;
    ilProdutos: TImageList;
    PopUpCadastros: TPopupMenu;
    AlterarCor1: TMenuItem;
    PopUpVidros: TPopupMenu;
    MenuItem1: TMenuItem;
    MostrarTodososServios1: TMenuItem;
    MostrarApenasosServiosdoVidro1: TMenuItem;
    MostrarTodososServios2: TMenuItem;
    MostrarApenasosServiosdoVidro2: TMenuItem;
    MarcarTodos1: TMenuItem;
    Desmarcartodos1: TMenuItem;
    panel3: TPanel;
    pgcPagServicos: TPageControl;
    ts2: TTabSheet;
    pnl3: TPanel;
    lb4: TLabel;
    lbLbNomeProduto: TLabel;
    lb5: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    lb8: TLabel;
    lb10: TLabel;
    lbCodigoMaterial: TLabel;
    lb11: TLabel;
    lb14: TLabel;
    btGravaprodutos: TBitBtn;
    btExcluiProdutos: TBitBtn;
    btCancelaProduto: TBitBtn;
    edtproduto: TEdit;
    edtquantidade_pedido: TEdit;
    edtvalor_pedido: TEdit;
    edtdesconto_reais_pedido: TEdit;
    edtdesconto_porcento_pedido: TEdit;
    cbbMateriais: TComboBox;
    edtaltura: TEdit;
    edtlargura: TEdit;
    dbgrdMaterias: TDBGrid;
    pnl7: TPanel;
    edtPesquisaVidroOS: TEdit;
    ts1: TTabSheet;
    pnl2: TPanel;
    lb1: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lbnomefuncservico: TLabel;
    lbNomeFuncionarioServico: TLabel;
    lbnomeservico: TLabel;
    lbCodigoServiOS: TLabel;
    lblAltura: TLabel;
    lblLarg: TLabel;
    lblControla: TLabel;
    btGravaServico: TBitBtn;
    btExcluiServico: TBitBtn;
    btCancelaServico: TBitBtn;
    edtservico: TEdit;
    edtquantidadeservico: TEdit;
    edtvalorservico: TEdit;
    edtFuncionarioServico: TEdit;
    edtAltServ: TEdit;
    edtLargServ: TEdit;
    rbSim: TRadioButton;
    rbNao: TRadioButton;
    dbgrdServicos: TDBGrid;
    pnl6: TPanel;
    edtpesquisa: TEdit;
    tsServicosVidros: TTabSheet;
    STRGVidrosServicos: TStringGrid;
    STRGListaVidros: TStringGrid;
    Panel1: TPanel;
    lb20: TLabel;
    lb21: TLabel;
    tsControleCod: TTabSheet;
    panel5: TPanel;
    img1: TImage;
    shpVidro: TShape;
    btProcessar: TSpeedButton;
    btCancelarExecucao: TSpeedButton;
    lbVidro: TLabel;
    lbServico: TLabel;
    lbNomedoVidro: TLabel;
    lbNomedoServico: TLabel;
    imgCodBar: TImage;
    shp2: TShape;
    edtCodigoBarras: TEdit;
    Moverpracima1: TMenuItem;
    MoverpraBaixo1: TMenuItem;
    MoverPLinha1: TMenuItem;
    AtualizarServiosParaoVidro1: TMenuItem;
    PopUp1: TPopupMenu;
    MenuItem2: TMenuItem;      {Rodolfo}
    procedure FormShow(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsalvarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edtservicoDblClick(Sender: TObject);
    procedure edtprodutoDblClick(Sender: TObject);
    procedure cbbMateriaisClick(Sender: TObject);
    procedure btGravaServicoClick(Sender: TObject);
    procedure dbgrdServicosDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure edtservicoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtFuncionarioServicoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtFuncionarioServicoDblClick(Sender: TObject);
    procedure btAlteraServicoClick(Sender: TObject);
    procedure dbgrdServicosDblClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtVendedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btCancelaServicoClick(Sender: TObject);
    procedure btExcluiServicoClick(Sender: TObject);
    procedure pgcPagServicosChange(Sender: TObject);
    procedure StrGridVidrosDblClick(Sender: TObject);
    procedure btGravaprodutosClick(Sender: TObject);
    procedure dbgrdMateriasDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dbgrdMateriasDblClick(Sender: TObject);
    procedure btCancelaProdutoClick(Sender: TObject);
    procedure btExcluiProdutosClick(Sender: TObject);
    procedure edtClienteExit(Sender: TObject);
    procedure edtVendedorExit(Sender: TObject);
    procedure edtClienteKeyPress(Sender: TObject; var Key: Char);
    procedure lbNomeClienteClick(Sender: TObject);
    procedure lbNomeVendedorClick(Sender: TObject);
    procedure lbNomeVendedorMouseLeave(Sender: TObject);
    procedure lbNomeClienteMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure edtservicoExit(Sender: TObject);
    procedure edtFuncionarioServicoExit(Sender: TObject);
    procedure edtprodutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtprodutoExit(Sender: TObject);
    procedure edtvalorservicoExit(Sender: TObject);
    procedure edtvalor_pedidoExit(Sender: TObject);
    procedure edtlarguraExit(Sender: TObject);
    procedure lbconcluidoClick(Sender: TObject);
    procedure edtVidroDblClick(Sender: TObject);
    procedure edtServicoVidroDblClick(Sender: TObject);
    procedure edtVidroExit(Sender: TObject);
    procedure edtServicoVidroExit(Sender: TObject);
    procedure lbNumPedidoClick(Sender: TObject);
    procedure dbgrdServicosKeyPress(Sender: TObject; var Key: Char);
    procedure edtpesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure edtpesquisaExit(Sender: TObject);
    procedure dbgrdMateriasKeyPress(Sender: TObject; var Key: Char);
    procedure edtPesquisaVidroOSExit(Sender: TObject);
    procedure edtPesquisaVidroOSKeyPress(Sender: TObject; var Key: Char);
    procedure edtPesquisaServicoVidroKeyPress(Sender: TObject;
      var Key: Char);
    procedure btopcoesClick(Sender: TObject);
    procedure btrelatorioClick(Sender: TObject);
    procedure bt1Click(Sender: TObject);
    procedure edtClienteDblClick(Sender: TObject);
    procedure edtVendedorDblClick(Sender: TObject);
    procedure edtLargServExit(Sender: TObject);
    procedure edtAltServExit(Sender: TObject);
    procedure rbSimClick(Sender: TObject);
    procedure rbNaoClick(Sender: TObject);
    procedure edtAltServKeyPress(Sender: TObject; var Key: Char);
    procedure edtLargServKeyPress(Sender: TObject; var Key: Char);
    procedure edtquantidadeservicoKeyPress(Sender: TObject; var Key: Char);
    procedure edtvalorservicoKeyPress(Sender: TObject; var Key: Char);
    procedure edtservicoClick(Sender: TObject);
    procedure STRGVidrosServicosDrawCell(Sender: TObject; ACol,
      ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure STRGVidrosServicosDblClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MostrarTodososServios1Click(Sender: TObject);
    procedure MostrarTodososServios2Click(Sender: TObject);
    procedure MostrarApenasosServiosdoVidro1Click(Sender: TObject);
    procedure MostrarApenasosServiosdoVidro2Click(Sender: TObject);
    procedure STRGListaVidrosClick(Sender: TObject);
    procedure MarcarTodos1Click(Sender: TObject);
    procedure Desmarcartodos1Click(Sender: TObject);
    procedure STRGVidrosServicosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure STRGListaVidrosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AlterarCor1Click(Sender: TObject);
    procedure OrdenarExecuodosServios1Click(Sender: TObject);
    procedure edtCodigoBarrasKeyPress(Sender: TObject; var Key: Char);
    procedure Moverpracima1Click(Sender: TObject);
    procedure MoverpraBaixo1Click(Sender: TObject);
    procedure MoverPLinha1Click(Sender: TObject);
    procedure AtualizarServiosParaoVidro1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);

  private
        ObjORDEMSERVICO:TObjORDEMSERVICO;
        ObjServicoos:TObjSERVICOSOS;
        ObjMATERIAISOS:TObjMATERIAISOS;
        ObjVidroServico:TObjVIDROSERVICOOS;
        CampoPesquisaServico:string;
        CampoPesquisaVidro:string;
        CampoPesquisaServicoVidro:string;

        auxStr : String; {Rodolfo - Guarda o nome do servico para verificar se no onclick do edtservico foi mudado alguma coisa}

        Function  ControlesParaObjeto:Boolean;
        Function  TabelaParaControles:Boolean; 
        Function  ObjetoParaControles:Boolean;
        procedure limpalabels;
        function CalculaValorFinal:Currency;
        procedure MontaStringGrid;
        procedure verificaosconcluida;
        function Verifica_CodigoBarras(codigobarras:string):Boolean;
        procedure CriaCodigo(cod:string;imagem:Tcanvas);
        procedure VerificaCodigoBarras;
        procedure ConcluirServico(codigoservico:string);

  public
    { Public declarations }
  end;

var
  FOrdemServico: TFOrdemServico;


implementation

uses UescolheImagemBotao, Upesquisa, Uprincipal, UMenuRelatorios,
  UpesquisaMenu, Math, UobjVIDRO, UVENDEDOR, UObjFUNCIONARIOS,UPEDIDO,
  UAjuda;

{$R *.dfm}

procedure TFOrdemServico.FormShow(Sender: TObject);
begin

     Try
           ObjServicoos:=TObjSERVICOSOS.create;

     Except
              Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
              Self.close;
     End;

     Try
           ObjORDEMSERVICO:=TObjORDEMSERVICO.create;
     Except
              Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
              Self.close;
     End;

     try
          ObjMATERIAISOS:=TObjMATERIAISOS.Create;
     except

     end;

     try
          ObjVidroServico:=TObjVIDROSERVICOOS.Create;
     except

     end;

     try
            dbgrdServicos.DataSource:=Self.ObjServicoos.ObjDatasource;
            dbgrdMaterias.DataSource:=Self.ObjMATERIAISOS.ObjDatasource;
     except

     end;

     limpalabels;
     desabilita_campos(Self);
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImageRodape,'RODAPE');

     FescolheImagemBotao.PegaFiguraBotaopequeno(btGravaServico,'BOTAOINSERIR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(BtCancelaServico,'BOTAOCANCELARPRODUTO.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(BtExcluiServico,'BOTAORETIRAR.BMP');

     FescolheImagemBotao.PegaFiguraBotaopequeno(btGravaprodutos,'BOTAOINSERIR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btCancelaProduto,'BOTAOCANCELARPRODUTO.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btExcluiProdutos,'BOTAORETIRAR.BMP');
     pgcPagServicos.TabIndex:=0;

     if(Tag<>0)
     then begin
            if(ObjORDEMSERVICO.LocalizaCodigo(inttostr(Self.tag))=True)
            then begin
              ObjORDEMSERVICO.TabelaparaObjeto;
              self.ObjetoParaControles;
            end;
     end;

     ObjServicoos.Resgata_ServicoOS(lbCodigo.caption,'');
     ObjMATERIAISOS.ResgataMateriais(lbCodigo.Caption,'');
     edtpesquisa.Text:='';
     edtPesquisaVidroOS.Text:='';
     habilita_campos(pnl2);
     habilita_campos(pnl3);
     MontaStringGrid;
     imgCodBar.Visible:=False;

end;

procedure TFOrdemServico.btsairClick(Sender: TObject);
begin
      self.Close;
end;

procedure TFOrdemServico.limpalabels;
begin
    lbconcluido.caption:='';
    lbLbNomeProduto.caption:='';
    lbNomeFuncionarioServico.Caption:='';
    lbnomeservico.caption:='';
    lbDataOS.Caption:='';
    lbCodigo.Caption:='';
    lbNomeCliente.Caption:='';
    lbNomeVendedor.Caption:='';
    lbValorFinalOS.Caption:='';
    lbNumPedido.Caption:='';
end;

procedure TFOrdemServico.btnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     limpalabels;
     habilita_campos(Self);
     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;
     ObjORDEMSERVICO.Status:=dsInsert;
     lbCodigo.Caption:='0';
     lbDataOS.Caption:=DateToStr(Date);
     lbCodigoServiOS.Caption:='';
     lbCodigoMaterial.Caption:='';
     lbValorFinalOS.Caption:='0,00';
     desabilita_campos(pnl2);
     desabilita_campos(pnl3);
end;

procedure TFOrdemServico.btalterarClick(Sender: TObject);
begin
     if(lbCodigo.Caption='')
     THEN Exit;

     habilita_campos(Self);
     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;
     ObjORDEMSERVICO.Status:=dsEdit;
     lbCodigoServiOS.Caption:='';
     lbCodigoMaterial.caption:='';
     desabilita_campos(pnl2);
     desabilita_campos(pnl3);
end;

procedure TFOrdemServico.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
      ObjORDEMSERVICO.Free;
      ObjServicoos.free;
      ObjMATERIAISOS.Free;
      ObjVidroServico.Free;
      Self.tag:=0;
end;

procedure TFOrdemServico.btsalvarClick(Sender: TObject);
begin
     If ObjORDEMSERVICO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjORDEMSERVICO.salvar(true)=False)
     Then exit;
     lbCodigo.Caption:=ObjORDEMSERVICO.Get_CODIGO;

     ObjORDEMSERVICO.LocalizaCodigo(lbCodigo.Caption) ;
     ObjORDEMSERVICO.TabelaparaObjeto;
     Self.TabelaParaControles;

     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     desabilita_campos(self);
     habilita_campos(pnl2);
     habilita_campos(pnl3);
end;

procedure TFOrdemServico.btcancelarClick(Sender: TObject);
begin
     limpaedit(Self);
     limpalabels;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true ;
     btopcoes.Visible :=true;
     desabilita_campos(Self);
     lbCodigo.Caption:='';
     ObjServicoos.Resgata_ServicoOS(lbCodigo.Caption,'');
     ObjMATERIAISOS.ResgataMateriais(lbCodigo.Caption,'');
     ObjORDEMSERVICO.Status:=dsInactive;
end;

function TFORDEMSERVICO.ControlesParaObjeto: Boolean;
var
  ValorFinal:Currency;
begin
  Try
    With ObjORDEMSERVICO do
    Begin
        Submit_CODIGO(lbCodigo.Caption);
        Submit_DESCRICAO(edtDESCRICAO.text);
        Submit_CLIENTE(edtCLIENTE.text);
        Submit_VENDEDOR(edtVENDEDOR.text);
        Submit_OBSERVACAO(memoObservacao.text);
        ValorFinal:=CalculaValorFinal;
        Submit_VALORFINAL(virgulaparaponto(formata_valor(ValorFinal)));
        Submit_CONCLUIDO('N');
        Submit_DATAOS(lbdataos.Caption);
        Submit_Pedido(lbNumPedido.Caption);
        result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFOrdemServico.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin
        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjORDEMSERVICO.Get_pesquisa,ObjORDEMSERVICO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjORDEMSERVICO.status<>dsinactive
                                  then exit;

                                  If (ObjORDEMSERVICO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjORDEMSERVICO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            //Self.limpaLabels;
                                            exit;
                                       End;
                                  ObjServicoos.Resgata_ServicoOS(lbCodigo.Caption,'');
                                  ObjMATERIAISOS.ResgataMateriais(lbCodigo.Caption,'');
                                  desabilita_campos(self);
                                  habilita_campos(pnl2);
                                  habilita_campos(pnl3);
                                  lbCodigoServiOS.Caption:='';
                                  lbCodigoMaterial.caption:='';
                                  pgcPagServicos.TabIndex:=0;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

function TFORDEMSERVICO.TabelaParaControles: Boolean;
begin
     If (ObjORDEMSERVICO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;


function TFORDEMSERVICO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjORDEMSERVICO do
     Begin
          lbcodigo.caption:=Get_CODIGO;
          EdtDESCRICAO.text:=Get_DESCRICAO;
          EdtCLIENTE.text:=Get_CLIENTE;
          EdtVENDEDOR.text:=Get_VENDEDOR;
          memoOBSERVACAO.text:=Get_OBSERVACAO;
          if(Get_CONCLUIDO = 'S')
          then lbconcluido.Caption:='OS Conclu�da'
          else lbconcluido.Caption:='OS Aberta';
          lbValorFinalOS.Caption:=formata_valor(Get_VALORFINAL);
          ObjCLIENTE.ZerarTabela;
          ObjVENDEDOR.ZerarTabela;
          ObjCLIENTE.LocalizaCodigo(Get_CLIENTE);
          ObjVENDEDOR.LocalizaCodigo(Get_VENDEDOR);
          ObjCLIENTE.TabelaparaObjeto;
          ObjVENDEDOR.TabelaparaObjeto;
          lbNumPedido.Caption:=Get_Pedido;
          if(Get_Pedido<>'')
          then begin
               lbNumPedido.Visible:=True;
          end;

          lbNomeCliente.Caption:=ObjCLIENTE.Get_Nome;
          lbNomeVendedor.Caption:=ObjVENDEDOR.Get_Nome;
          lbdataos.caption:=Get_DATAOS;
          ObjServicoos.Resgata_ServicoOS(lbCodigo.caption,'');
          ObjMATERIAISOS.ResgataMateriais(lbCodigo.Caption,'');

          

         result:=True;
     End;
  Except
        Result:=False;
  End;
end;


procedure TFOrdemServico.btexcluirClick(Sender: TObject);
begin
      If (ObjORDEMSERVICO.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (ObjORDEMSERVICO.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjORDEMSERVICO.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     ObjMATERIAISOS.ResgataMateriais(lbCodigo.Caption,'');
     ObjServicoos.Resgata_ServicoOS(lbCodigo.Caption,'');
     limpaedit(Self);
     limpalabels;

end;

procedure TFOrdemServico.edtservicoDblClick(Sender: TObject);
var
  Key:Word;
  FpesquisaLocal:Tfpesquisa;
begin

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabservico','',Nil)=True) Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 edtservico.Text   :=FpesquisaLocal.querypesq.fieldbyname('codigo').AsString;
                                 lbnomeservico.Caption       :=FpesquisaLocal.QueryPesq.fieldbyname('descricao').AsString;

                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFOrdemServico.edtprodutoDblClick(Sender: TObject);
var
  Key:Word;
  FpesquisaLocal:Tfpesquisa;
  Sql:string;
begin
     {if(cbbMateriais.Text='')
     then Exit;  }

     sql:='Select * from tabvidro ';
     
      //SE PRECISAR FAZER SERVI�OS EM CIMA DE OUTROS MATERIAIS
      //A PRINCIPIOR TODOS OS SERVI�OS S�O FEITO EM CIMA DOS VIDROS
     {case cbbMateriais.ItemIndex of
          0:sql:=sql+'tabvidro';
          1:sql:=sql+'Tabferragem';
          2:sql:=sql+'TabDiverso';
          3:sql:=sql+'TabKitBox';
          4:sql:=sql+'TabPerfilado';
          5:sql:=sql+'TabPersiana' ;
     end;}

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(sql,'',Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 edtproduto.Text   :=FpesquisaLocal.querypesq.fieldbyname('codigo').AsString;
                                 lbLbNomeProduto.Caption       :=FpesquisaLocal.QueryPesq.fieldbyname('descricao').AsString;
                                 //EdtCodigoServico_PP.Text       :=FpesquisaLocal.QueryPesq.fieldbyname('referencia').AsString;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TFOrdemServico.cbbMateriaisClick(Sender: TObject);
begin
        edtproduto.text:='';
        lbLbNomeProduto.Caption:='';
end;

procedure TFOrdemServico.btGravaServicoClick(Sender: TObject);
var
  ValorFinalOS,ValorFinal:Currency;
  QueryUpdade:TIBQuery;
begin
    if(lbCodigo.Caption='')
    then Exit;

    if(edtservico.Text='')
    then exit;

    try
      QueryUpdade:=TIBQuery.Create(nil);
      QueryUpdade.Database:=FDataModulo.IBDatabase;
    except

    end;
    try
        with  ObjServicoos do
        begin

              ZerarTabela;
              if(lbCodigoServiOS.Caption='')
              then begin
                    Submit_CODIGO('0');
                    Status:=dsInsert;
              end
              else
              begin
                   Submit_CODIGO(lbCodigoServiOS.Caption);
                   Status:=dsEdit;
              end;

              Submit_ORDEMSERVICO(lbCodigo.Caption);
              Submit_SERVICO(edtservico.Text);
              Submit_VALOR(edtvalorservico.Text);
              Submit_QUANTIDADE(edtquantidadeservico.text);
              Submit_FUNCIONARIO(edtFuncionarioServico.Text);

              if( rbSim.Checked = True ) then begin    {Rodolfo}
                   Submit_ALTURA(edtAltServ.text);
                   Submit_LARGURA(edtLargServ.text);
                   Submit_ControlaPorMilimetro('SIM');
              end
              else begin  //se nao controla por milimetro
                   Submit_ControlaPorMilimetro('NAO');
              end;
              if(edtvalorservico.Text<>'') then
              begin
                 ValorFinal:=StrToCurr(edtvalorservico.Text)*StrToCurr(edtquantidadeservico.text);
                 Submit_VALORFINAL(tira_ponto(formata_valor(ValorFinal)));
              end
              else Submit_VALORFINAL('');

              if(Salvar(true)=false)
              then exit;
              Resgata_ServicoOS(lbcodigo.caption,'');


              lbCodigoServiOS.Caption:='';
              lbNomeFuncionarioServico.caption:='';
              lbnomeservico.caption:='';
              limpaedit(pnl2);

              with QueryUpdade do
              begin
                    ValorFinalOS:=CalculaValorFinal;
                    Close;
                    sql.Clear;
                    sql.Add('update tabordemservico set valorfinal='+virgulaparaponto(tira_ponto(formata_valor(ValorFinalOS))));
                    sql.Add('where codigo='+lbCodigo.Caption) ;
                    ExecSQL;
                    FDataModulo.IBTransaction.CommitRetaining;
                    lbValorFinalOS.Caption:=formata_valor(ValorFinalOS);
              end;
        end;
    
    finally
        FreeAndNil(QueryUpdade);
    end;

end;

procedure TFOrdemServico.dbgrdServicosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With dbgrdServicos.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(dbgrdServicos.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                dbgrdServicos.DefaultDrawDataCell(Rect, dbgrdServicos.Columns[DataCol].Field, State);
          End;
end;

procedure TFOrdemServico.edtservicoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
       if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
       Begin
             exit;
       end;
       ObjServicoos.Objservico.EdtServicoKeyDown(Sender,edtservico,Key,Shift,lbnomeservico);
end;

procedure TFOrdemServico.edtFuncionarioServicoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
      Begin
            exit;
      end;
      ObjServicoos.ObjFuncionarios.EdtFuncionariosKeyDown(Sender,Key,Shift,edtFuncionarioServico,lbNomeFuncionarioServico);
end;

procedure TFOrdemServico.edtFuncionarioServicoDblClick(Sender: TObject);
var
  Key:Word;
  FpesquisaLocal:Tfpesquisa;
begin

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabfuncionarios','',Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 edtFuncionarioServico.Text   :=FpesquisaLocal.querypesq.fieldbyname('codigo').AsString;
                                 lbNomeFuncionarioServico.Caption       :=FpesquisaLocal.QueryPesq.fieldbyname('nome').AsString;

                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFOrdemServico.btAlteraServicoClick(Sender: TObject);
begin
      { if(lbCodigo.Caption='')
       then Exit;

        with  ObjServicoos do
        begin
              Status:=dsEdit;
              ZerarTabela;
              Submit_CODIGO('0');
              Submit_ORDEMSERVICO(lbCodigo.Caption);
              Submit_SERVICO(edtservico.Text);
              Submit_VALOR(edtvalorservico.Text);
              Submit_QUANTIDADE(edtquantidadeservico.text);
              Submit_FUNCIONARIO(edtFuncionarioServico.Text);
              if(Salvar(true)=false)
              then exit;
              Resgata_ServicoOS(lbcodigo.caption);


        end;   }
end;

procedure TFOrdemServico.dbgrdServicosDblClick(Sender: TObject);
var descricao : string;
begin
     lbCodigoServiOS.Caption:='';
     If (dbgrdServicos.DataSource.DataSet.Active=False)
     Then exit;

     If (dbgrdServicos.DataSource.DataSet.RecordCount=0)
     Then exit;

     limpaedit(pnl2);
     if (ObjServicoos.LocalizaCodigo(dbgrdServicos.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               exit;
     End;

     ObjServicoos.TabelaparaObjeto;
     edtservico.Text:=ObjServicoos.Get_SERVICO;
     lbnomeservico.caption:=ObjServicoos.Get_Descricao;
     edtvalorservico.text:=formata_valor(ObjServicoos.Get_VALOR);
     edtquantidadeservico.text:=ObjServicoos.Get_QUANTIDADE;
     edtFuncionarioServico.Text:=ObjServicoos.Get_FUNCIONARIO;
     ObjServicoos.ObjFuncionarios.ZerarTabela;
     ObjServicoos.ObjFuncionarios.LocalizaCodigo(ObjServicoos.Get_FUNCIONARIO);
     ObjServicoos.ObjFuncionarios.TabelaparaObjeto;
     lbNomeFuncionarioServico.Caption:=ObjServicoos.ObjFuncionarios.Get_Nome;
     lbCodigoServiOS.Caption:=ObjServicoos.Get_CODIGO;

     {Rodolfo ==}
     if(ObjServicoos.Get_ControlaPorMilimetro = 'SIM') then begin
          edtAltServ.text := ObjServicoos.Get_Altura;
          edtLargServ.text := ObjServicoos.Get_Largura;
          rbSim.Checked := True;
          rbSimClick(sender);
     end
     else begin
          edtAltServ.text := '';
          edtLargServ.text := '';
          rbNao.Checked := True;
          rbNaoClick(sender);
     end;
     {== Rodolfo}

end;

procedure TFOrdemServico.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('ORDEM DE MEDI��O');
         FAjuda.ShowModal;
    end;

end;

procedure TFOrdemServico.FormKeyPress(Sender: TObject; var Key: Char);
begin
       if key=#13
       Then Perform(Wm_NextDlgCtl,0,0);
       if (Key=#27)
       Then Self.Close;
end;

procedure TFOrdemServico.edtClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  ObjPedido:TObjPEDIDO_PEDIDO;

begin

        try
              ObjPedido:=TObjPEDIDO_PEDIDO.Create;

        except

        end;


        try
             ObjPedido.EdtClienteKeyDown(Sender,Key,Shift,lbNomeCliente);
        finally
             ObjPedido.Free;
        end;



end;

procedure TFOrdemServico.edtVendedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  ObjPedido:TObjPEDIDO_PEDIDO;

begin
        try
              ObjPedido:=TObjPEDIDO_PEDIDO.Create;

        except

        end;


        try
             ObjPedido.EdtVendedorKeyDown(Sender,Key,Shift,lbNomeVendedor);;
        finally
             ObjPedido.Free;
        end;



end;

procedure TFOrdemServico.btCancelaServicoClick(Sender: TObject);
begin
     limpaedit(pnl2);
     lbnomeservico.caption:='';
     lbNomeFuncionarioServico.caption:='';
     lbCodigoServiOS.Caption:='';
end;

procedure TFOrdemServico.btExcluiServicoClick(Sender: TObject);
var
  ValorFinalOS:Currency;
  QueryUpdade:TIBQuery;
begin
     if(lbCodigo.Caption='')
     then Exit;

     if(lbCodigoServiOS.Caption='')
     then Exit;

     try
        QueryUpdade:=TIBQuery.Create(nil);
        QueryUpdade.Database:=FDataModulo.IBDatabase;
     except

     end;
     try
         if(ObjServicoos.Exclui(lbCodigoServiOS.Caption,True)=False)
         then MensagemErro('N�o foi possiv�l excluir o servi�o da lista');
         limpaedit(pnl2);
         lbnomeservico.caption:='';
         lbNomeFuncionarioServico.caption:='';
         ObjServicoos.Resgata_ServicoOS(lbCodigo.Caption,'');
         ObjMATERIAISOS.ResgataMateriais(lbCodigo.Caption,'');
         with QueryUpdade do
         begin
                 ValorFinalOS:=CalculaValorFinal;
                 Close;
                 sql.Clear;
                 sql.Add('update tabordemservico set valorfinal='+virgulaparaponto(formata_valor(ValorFinalOS)));
                 sql.Add('where codigo='+lbCodigo.Caption) ;
                 ExecSQL;
                 FDataModulo.IBTransaction.CommitRetaining;
                 lbValorFinalOS.Caption:=formata_valor(ValorFinalOS);
         end;
         lbCodigoServiOS.caption:='';
     finally
          FreeAndNil(QueryUpdade);
     end;




end;

procedure TFOrdemServico.pgcPagServicosChange(Sender: TObject);
var
    Query:TIBQuery;
begin
    try
          Query:=TIBQuery.Create(nil);
          Query.Database :=FDataModulo.IBDatabase;
    except

    end;

    try
       if(lbCodigo.Caption='')
       then exit;

       with query do
       begin
             if(pgcPagServicos.TabIndex=0)
             then
             begin
                  ObjMATERIAISOS.ResgataMateriais(lbCodigo.Caption,'');
                  limpaedit(pnl3);
                  lbLbNomeProduto.caption:='';
                  lbCodigoMaterial.Caption:='';
             end;

             if(pgcPagServicos.TabIndex=1)
             then begin
                ObjServicoos.Resgata_ServicoOS(lbCodigo.Caption,'');
                limpaedit(pnl2);
                lbnomeservico.Caption:='';
                lbNomeFuncionarioServico.Caption:='';
                lbCodigoServiOS.Caption:='';

                rbSim.Enabled := True;
                rbNao.Enabled := True;
                rbNaoClick(Sender);
                
             end;

             if(pgcPagServicos.TabIndex=2) then
             begin
                  MontaStringGrid;
                  MostrarApenasosServiosdoVidro1Click(Sender);
             end;

             if(pgcPagServicos.TabIndex=3) then
             begin
                  habilita_campos(panel5);
                  img1.Visible:=false;
                  lbNomedoVidro.Caption:='';
                  lbNomedoServico.Caption:='';
             end;


       end;
    finally
       FreeAndNil(Query);
    end;


end;

procedure TFOrdemServico.StrGridVidrosDblClick(Sender: TObject);
begin
     //alguma coisa
end;

procedure TFOrdemServico.btGravaprodutosClick(Sender: TObject);
var
  ValorFinal:Currency;
  sentinela,i,numrand:Integer;
  codigobarras:string;
begin
   if(lbCodigo.Caption='')
   then Exit;

   if(edtproduto.Text='')
   then Exit;

   with ObjMATERIAISOS  do
   begin
      ZerarTabela;
      if(lbCodigoMaterial.Caption='')
      then begin
        Submit_CODIGO('0');
        Status:=dsInsert;
      end
      else
      begin
        Submit_CODIGO(lbCodigoMaterial.Caption);
        Status:=dsEdit;
      end;
      //A principio so ira fazer servi�os em vidro
      //Se acaso precisar fazer para os outros, � so acrescetar
      Submit_FERRAGEM('');
      Submit_DIVERSO('');
      Submit_KITBOX('');
      Submit_PERFILADO('');
      Submit_PERSIANA('');

      Submit_VIDRO(edtproduto.Text);
      Submit_ORDEMSERVICO(lbCodigo.Caption);
      Submit_VALOR(edtvalor_pedido.Text);
      ///Submit_DESCONTO(edtdesconto_reais_pedido.Text);
      Submit_QUANTIDADE(edtquantidade_pedido.Text);
      ValorFinal:=StrToCurr(edtvalor_pedido.Text)* StrToCurr(edtquantidade_pedido.Text);
      Submit_VALORFINAL(formata_valor(ValorFinal));
      Submit_Altura(edtaltura.Text);
      Submit_Largura(edtlargura.Text);  

      codigobarras:='';
      sentinela:=0;
      while(sentinela = 0)
      do begin
          Randomize;
          for I := 1 to 10 do
          begin
              numrand:=Random(10);
              codigobarras:= codigobarras+IntToStr(numrand);
          end;
          codigobarras:=codigobarras+'0';
          if (Verifica_CodigoBarras(codigobarras) =True ) then
          begin
              sentinela:=0;
          end
          else
          begin
              sentinela:=1;
          end;
      end;

      Submit_CodigoBarras(codigobarras);

      if(Salvar(true)=false)
      then exit;
      ResgataMateriais(lbCodigo.Caption,'');
      lbCodigoMaterial.Caption:='';
      limpaedit(pnl3);


   end;
end;

//esta fun��o � responsavel por gerar o codigo de barras de cada evento
//o codigo de barras usado � o Barcode Padr�o 2 de 5 Intercalado
//usando um TImagem ele desenha o codigo de barras
//Para desenhar o codigo de barras � necessario passar pra a fun��o uma sequencia de
//caracteres, no caso o codigo do evento,a qual � completada com zeros at� conter 11 caracteres,
//para assim efetuar o desenho do mesmo
procedure TFOrdemServico.CriaCodigo(cod:string;imagem:Tcanvas);
Const
  digitos : array['0'..'9'] of string[5]= ('00110',
  '10001',
  '01001',
  '11000',
  '00101',
  '10100',
  '01100',
  '00011',
  '10010',
  '01010');
Var
  Numero : String;
  Cod1 : Array[1..1000] Of Char;
  Cod2 : Array[1..1000] Of Char;
  Codigo : Array[1..1000] Of Char;
  Digito : String;
  c1,c2 : Integer;
  x,y,z,h : LongInt;
  a,b,c,d : TPoint;
  I : Boolean;
Begin
  Numero := Cod;
  For x := 1 to 1000 Do
  Begin
    Cod1 [x] := #0;
    Cod2 [x] := #0;
    Codigo[x] := #0;
  End;
  c1 := 1;
  c2 := 1;
  x := 1;
  For y := 1 to Length(Numero) div 2 do
  Begin
    Digito := Digitos[Numero[x ]];
    For z := 1 to 5 do
    Begin
      Cod1[c1] := Digito[z];
      Inc(c1);
    End;
    Digito := Digitos[Numero[x+1]];
    For z := 1 to 5 do
    Begin
      Cod2[c2] := Digito[z]; 
      Inc(c2); 
    End;
    Inc(x,2);
  End;
  y := 5;
  Codigo[1] := '0'; 
  Codigo[2] := '0'; 
  Codigo[3] := '0'; 
  Codigo[4] := '0'; { Inicio do Codigo}
  For x := 1 to c1-1 do
  begin
    Codigo[y] := Cod1[x]; Inc(y);
    Codigo[y] := Cod2[x]; Inc(y);
  end;
  Codigo[y] := '1'; Inc(y); { Final do Codigo}
  Codigo[y] := '0'; Inc(y);
  Codigo[y] := '0';
  Imagem.Pen .Width := 1;
  Imagem.Brush.Color := ClWhite;
  Imagem.Pen .Color := ClWhite;
  a.x := 1; a.y := 0;
  b.x := 1; b.y := 79;
  c.x := 2000; c.y := 79;
  d.x := 2000; d.y := 0;
  Imagem.Polygon([a,b,c,d]);
  Imagem.Brush.Color := ClBlack;
  Imagem.Pen .Color := ClBlack;
  x := 0;
  i := True;
  for y:=1 to 1000 do
  begin
    If Codigo[y] <> #0 Then
    Begin
      If Codigo[y] = '0' then 
        h := 1 
      Else 
        h := 3;
      a.x := x; a.y := 0;
      b.x := x; b.y := 79;
      c.x := x+h-1; c.y := 79;
      d.x := x+h-1; d.y := 0;
      If i Then 
        Imagem.Polygon([a,b,c,d]);
      i := Not(i);
      x := x + h;
    End;
  end;
end;

function TFOrdemServico.Verifica_CodigoBarras(codigobarras:string):Boolean;
var
  Query:TIBQuery;
begin
  query:=TIBQuery.Create(nil);
  Query.Database:=FDataModulo.IBDatabase;
  try
    with Query do
    begin
        Close;
        sql.Clear;
        sql.Add('select codigobarras from tabmateriaisos');
        SQL.Add('where vidro='+edtproduto.Text);
        SQL.Add('and ordemservico ='+lbcodigo.Caption);
        SQL.Add('and codigobarras ='+#39+codigobarras+#39);
        Open;
        Last;
        if(recordcount<>0)
        then begin
          Result:=True;
        end
        else begin
          Result:=False;
        end;
    end;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TFOrdemServico.dbgrdMateriasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With dbgrdMaterias.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(dbgrdMaterias.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                dbgrdMaterias.DefaultDrawDataCell(Rect, dbgrdMaterias.Columns[DataCol].Field, State);
          End;
end;

procedure TFOrdemServico.dbgrdMateriasDblClick(Sender: TObject);
begin
      lbCodigoMaterial.Caption:='';
     If (dbgrdMaterias.DataSource.DataSet.Active=False)
     Then exit;

     If (dbgrdMaterias.DataSource.DataSet.RecordCount=0)
     Then exit;

     limpaedit(pnl2);
     if (ObjMATERIAISOS.LocalizaCodigo(dbgrdMaterias.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               exit;
     End;
     ObjMATERIAISOS.TabelaparaObjeto;
     edtproduto.Text:=ObjMATERIAISOS.Get_VIDRO;
     edtvalor_pedido.text:=formata_valor(ObjMATERIAISOS.Get_VALOR);
     edtquantidade_pedido.text:=ObjMATERIAISOS.Get_QUANTIDADE;
     edtlargura.text:=ObjMATERIAISOS.Get_Largura;
     edtaltura.Text:=ObjMATERIAISOS.Get_Altura;
     ObjMATERIAISOS.Objvidro.ZerarTabela;
     ObjMATERIAISOS.Objvidro.LocalizaCodigo(ObjMATERIAISOS.Get_VIDRO);
     ObjMATERIAISOS.Objvidro.TabelaparaObjeto;
     lbLbNomeProduto.Caption:=ObjMATERIAISOS.Objvidro.Get_Descricao;
     lbCodigoMaterial.Caption:=ObjMATERIAISOS.Get_CODIGO;
     imgCodBar.Visible:=True;
     CriaCodigo(ObjMATERIAISOS.Get_CodigoBarras,imgCodBar.Canvas);
end;

procedure TFOrdemServico.btCancelaProdutoClick(Sender: TObject);
begin
      limpaedit(pnl3);
      lbCodigoMaterial.Caption:='';
      lbLbNomeProduto.Caption:='';
      ObjMATERIAISOS.Status:=dsInactive;
      imgCodBar.Visible:=False;
end;

procedure TFOrdemServico.btExcluiProdutosClick(Sender: TObject);
begin
     if(lbCodigo.Caption='')
     then Exit;

     if(lbCodigoMaterial.Caption='')
     then Exit;

     if(ObjMATERIAISOS.Exclui(lbCodigoMaterial.Caption,True)=False)
     then MensagemErro('N�o foi possiv�l excluir o servi�o da lista');
     limpaedit(pnl3);
     lbLbNomeProduto.caption:='';
     ObjServicoos.Resgata_ServicoOS(lbCodigo.Caption,'');
     ObjMATERIAISOS.ResgataMateriais(lbCodigo.Caption,'');
     lbCodigoMaterial.Caption:='';
end;

procedure TFOrdemServico.edtClienteExit(Sender: TObject);
begin
      if(edtCliente.text='')
      then exit;

      with ObjORDEMSERVICO do
      begin
           if(ObjCLIENTE.LocalizaCodigo(edtCliente.text)=False)
           then
           begin
                edtCliente.text:='';
           end
           else
           begin
                ObjCLIENTE.ZerarTabela;
                ObjCLIENTE.TabelaparaObjeto;
                lbNomeCliente.Caption:=ObjCLIENTE.Get_Nome;
           end;
      end;
end;

procedure TFOrdemServico.edtVendedorExit(Sender: TObject);
begin
     if(edtVendedor.Text ='')
     then Exit;
     with ObjORDEMSERVICO do
     begin
          if(ObjVENDEDOR.LocalizaCodigo(edtVendedor.Text)=False)
          then
          begin
                edtVendedor.Text:='';
          end
          else
          begin
                ObjVENDEDOR.ZerarTabela;
                ObjVENDEDOR.TabelaparaObjeto;
                lbNomeVendedor.Caption:=ObjVENDEDOR.Get_Nome;
          end;

     end;
end;

procedure TFOrdemServico.edtClienteKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8),',']) then
       begin
                Key:= #0;
       end;
end;

procedure TFOrdemServico.lbNomeClienteClick(Sender: TObject);
var
  Fcliente:TFCLIENTE;
begin
     if(EdtCliente.Text='')
     then Exit;

     try
       fcliente:=TFCLIENTE.Create(nil);

     except

     end;

     try
        Fcliente.Tag:=StrToInt(EdtCliente.Text);
        Fcliente.ShowModal;

     finally
        FreeAndNil(Fcliente);
     end;
end;



procedure TFOrdemServico.lbNomeVendedorClick(Sender: TObject);
var
  Fvendedor:TFVENDEDOR;
begin
      if(EdtVendedor.Text='')
      then Exit;

      try
        Fvendedor:=TFVENDEDOR.Create(nil);
      except

      end;

      try
        Fvendedor.Tag:=StrToInt(EdtVendedor.Text) ;
        Fvendedor.ShowModal;
      finally
         FreeAndNil(Fvendedor);
      end;



end;

procedure TFOrdemServico.lbNomeVendedorMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFOrdemServico.lbNomeClienteMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFOrdemServico.edtservicoExit(Sender: TObject);
begin
        if(edtservico.Text='')
        then Exit;

        with ObjServicoos do
        begin
             if(Objservico.LocalizaCodigo(edtservico.text)=false) then
             begin
                    edtservico.text:='';
                    lbnomeservico.Caption:='';
             end
             else begin
                    Objservico.ZerarTabela;
                    Objservico.TabelaparaObjeto;
                    lbnomeservico.Caption:=Objservico.Get_Descricao;
                    edtvalorservico.Text:=formata_valor(Objservico.Get_PrecoCusto);

                    edtquantidadeservico.Text := '0';
                    edtAltServ.Text := '';
                    edtLargServ.Text:= '';
             end;
        end;
end;

procedure TFOrdemServico.edtFuncionarioServicoExit(Sender: TObject);
begin
     if(edtFuncionarioServico.text='')
     then Exit;
     with ObjServicoos do
     begin
          if(ObjFuncionarios.LocalizaCodigo(edtFuncionarioServico.Text)=false)
          then
          begin
                edtFuncionarioServico.text:='';
                lbNomeFuncionarioServico.caption:='';
          end
          else begin
                ObjFuncionarios.ZerarTabela;
                ObjFuncionarios.TabelaparaObjeto;
                lbNomeFuncionarioServico.Caption:=ObjFuncionarios.Get_Nome;
          end;
     end;
end;

procedure TFOrdemServico.edtprodutoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
       if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
       Begin
             exit;
       end;
       ObjMATERIAISOS.Objvidro.EdtVidroKeyDown(Sender,Key,Shift,lbLbNomeProduto);
end;

procedure TFOrdemServico.edtprodutoExit(Sender: TObject);
begin
      if(edtproduto.text='')
      then Exit;

      with ObjMATERIAISOS do
      begin
            if(Objvidro.LocalizaCodigo(edtproduto.text)=false)
            then begin
                  edtproduto.Text:='';
                  lbLbNomeProduto.caption:='';
            end
            else
            begin
                  Objvidro.ZerarTabela;
                  Objvidro.TabelaparaObjeto;
                  lbLbNomeProduto.Caption:=objvidro.Get_Descricao;
                  edtvalor_pedido.Text:=formata_valor(Objvidro.Get_PrecoCusto);
            end;
      end;
end;

function TFOrdemServico.CalculaValorFinal:Currency;
var
  QueryValor:TIBQuery;
begin
     result:=0.00;
      try
          QueryValor:=TIBQuery.Create(nil);
          QueryValor.Database:=FDataModulo.IBDatabase;
      except

      end;

      try
          with  QueryValor do
          begin
                close;
                sql.clear;
                sql.Add('select sum(tabservicosos.valorfinal) as soma');
                sql.Add('from tabservicosos');
                sql.Add('where tabservicosos.ordemservico='+lbCodigo.Caption);
                Open;
                if(recordcount>0)
                then Result:=fieldbyname('soma').AsCurrency;
          end;
      finally
          FreeAndNil(QueryValor);
      end;


end;

procedure TFOrdemServico.edtvalorservicoExit(Sender: TObject);
begin
      if(edtvalorservico.Text<>'')
      then edtvalorservico.Text:=formata_valor(edtvalorservico.Text);
end;

procedure TFOrdemServico.edtvalor_pedidoExit(Sender: TObject);
begin
        if(edtvalor_pedido.Text<>'')
        then edtvalor_pedido.text:=formata_valor(edtvalor_pedido.text);
end;

procedure TFOrdemServico.edtlarguraExit(Sender: TObject);
var
pquantidade,paltura,plargura,plarguraarredondamento,palturaarredondamento:Currency;
begin
   if (EdtAltura.Text <> '') and (EdtLargura.Text <> '') then
   Begin
        Try
           paltura:=strtocurr(tira_ponto(EdtAltura.Text));
        Except
              paltura:=0;
        End;

        Try
           plargura:=strtocurr(tira_ponto(Edtlargura.Text));
        Except
            plargura:=0;
        End;

        FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
        FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);

        pquantidade:=((plargura+plarguraarredondamento)*(paltura+palturaarredondamento))/1000000;
        edtquantidade_pedido.Text:=formata_valor(CurrToStr(pquantidade));
   end
   Else edtquantidade_pedido.Text:='0';

end;

procedure TFOrdemServico.lbconcluidoClick(Sender: TObject);
begin
    {  with ObjORDEMSERVICO do
      begin
              if(lbCodigo.Caption='')
              then Exit;
              Status:=dsEdit;
              ZerarTabela;
              if(LocalizaCodigo(lbCodigo.caption)=false)
              then Exit;
              TabelaparaObjeto;
              if(Get_CONCLUIDO = 'S') then
              begin
                  MensagemErro('Esta OS ja esta Conclu�da');
                  exit;
              end;

              Submit_CONCLUIDO('S');
              if(Salvar(true)=false)
              then MensagemErro('erro ao concluir OS');
              ObjetoParaControles;


      end;   }
end;

procedure TFOrdemServico.edtVidroDblClick(Sender: TObject);
var
  FpesquisaLocal:Tfpesquisa;
begin

end;

procedure TFOrdemServico.edtServicoVidroDblClick(Sender: TObject);
var
  FpesquisaLocal:Tfpesquisa;
begin

end;

procedure TFOrdemServico.edtVidroExit(Sender: TObject);
begin
        if(edtproduto.text='')
        then Exit;

        with ObjMATERIAISOS do
        begin
              if(Objvidro.LocalizaCodigo(edtproduto.text)=false)
              then begin
                    edtproduto.Text:='';
                    lbLbNomeProduto.caption:='';
              end
              else
              begin
                    Objvidro.ZerarTabela;
                    Objvidro.TabelaparaObjeto;
                    lbLbNomeProduto.Caption:=objvidro.Get_Descricao;
                    edtvalor_pedido.Text:=formata_valor(Objvidro.Get_PrecoCusto);
              end;
        end;
end;

procedure TFOrdemServico.edtServicoVidroExit(Sender: TObject);
begin
       if(edtservico.Text='')
       then Exit;

       with ObjServicoos do
       begin
             if(Objservico.LocalizaCodigo(edtservico.text)=false) then
             begin
                    edtservico.text:='';
                    lbnomeservico.Caption:='';
             end
             else begin
                    Objservico.ZerarTabela;
                    Objservico.TabelaparaObjeto;
                    lbnomeservico.Caption:=Objservico.Get_Descricao;
                    edtvalorservico.Text:=formata_valor(Objservico.Get_PrecoCusto);
             end;
       end;
end;

procedure TFOrdemServico.lbNumPedidoClick(Sender: TObject);
var
  Fpedido:TFPEDIDO;
begin
      Fpedido:=TFPEDIDO.Create(nil);
      try
            Fpedido.Tag:=StrToInt(lbNumPedido.Caption);
            Fpedido.ShowModal;
      finally
            FreeAndNil(Fpedido);
      end;

end;

procedure TFOrdemServico.dbgrdServicosKeyPress(Sender: TObject;
  var Key: Char);
begin
        edtpesquisa.Enabled:=True;
        edtpesquisa.Visible:=True;
        pnl6.Visible:=True;
        edtpesquisa.Text:=edtpesquisa.Text+ Key;
        edtpesquisa.SetFocus;
        
        if(edtpesquisa.Text<>' ')then
        edtpesquisa.SelStart:=Length(edtpesquisa.text);
        CampoPesquisaServico:=dbgrdServicos.SelectedField.FieldName;

end;

procedure TFOrdemServico.edtpesquisaKeyPress(Sender: TObject;
  var Key: Char);
var
  Where:string;
begin
       if(CampoPesquisaServico = 'SERVICO')
       then CampoPesquisaServico:='tabservicosos.descricao';

       if(CampoPesquisaServico = 'CODIGO')
       then CampoPesquisaServico:='tabservicosos.Codigo';

       if(CampoPesquisaServico = 'FUNCIONARIO')
       then CampoPesquisaServico:= 'tabfuncionarios.nome' ;

       where:='where UPPER('+CampoPesquisaServico +')like'+#39+'%'+edtpesquisa.Text+'%'+#39 ;
       ObjServicoos.Resgata_ServicoOS(lbCodigo.Caption,Where);

        {if(Key=#13) then
        begin
              if(edtpesquisa.text='') then
              begin
                    ObjServicoos.Resgata_ServicoOS(lbCodigo.Caption,'');
                    edtpesquisa.Enabled:=False;
                    edtpesquisa.Visible:=False;
                    pnl6.Visible:=False;
                    edtpesquisa.Text:='';
                    Exit;
              end;
              //codificando para os nomes correto na query de pesquisa
              if(CampoPesquisaServico = 'SERVICO')
              then CampoPesquisaServico:='tabservicosos.descricao';

              if(CampoPesquisaServico = 'CODIGO')
              then CampoPesquisaServico:='tabservicosos.Codigo';

              if(CampoPesquisaServico = 'FUNCIONARIO')
              then CampoPesquisaServico:= 'tabfuncionarios.nome' ;

              where:='where UPPER('+CampoPesquisaServico +')like'+#39+'%'+edtpesquisa.Text+'%'+#39 ;
              ObjServicoos.Resgata_ServicoOS(lbCodigo.Caption,Where);
              edtpesquisa.Enabled:=False;
              edtpesquisa.Visible:=False;
              pnl6.Visible:=False;
              edtpesquisa.Text:='';
        End;  }

end;

procedure TFOrdemServico.edtpesquisaExit(Sender: TObject);
begin
     edtpesquisa.Enabled:=False;
     edtpesquisa.Visible:=False;
     edtpesquisa.Text:='';
     pnl6.Visible:=False;

end;

procedure TFOrdemServico.dbgrdMateriasKeyPress(Sender: TObject;
  var Key: Char);
begin
        EdtPesquisaVidroOS.Enabled:=True;
        EdtPesquisaVidroOS.Visible:=True;
        pnl7.Visible:=True;
        EdtPesquisaVidroOS.Text:=edtpesquisa.Text+ Key;
        EdtPesquisaVidroOS.SetFocus;
        if(edtPesquisaVidroOS.Text<>' ') then
        edtPesquisaVidroOS.SelStart:=Length(edtPesquisaVidroOS.text);
        CampoPesquisaVidro:=dbgrdMaterias.SelectedField.FieldName;
end;



procedure TFOrdemServico.edtPesquisaVidroOSExit(Sender: TObject);
begin
       EdtPesquisaVidroOS.Visible:=False;
       pnl7.Visible:=False;
       EdtPesquisaVidroOS.text:='';
end;

procedure TFOrdemServico.edtPesquisaVidroOSKeyPress(Sender: TObject;
  var Key: Char);
var
  where:string;
begin
        if(CampoPesquisaVidro = 'CODIGO')
        then CampoPesquisaVidro:='tabmateriaisos.Codigo';
        where:='where UPPER('+CampoPesquisaVidro +')like'+#39+'%'+EdtPesquisaVidroOS.Text+'%'+#39 ;
        ObjMATERIAISOS.ResgataMateriais(lbCodigo.Caption,Where);


        {if(Key=#13) then
        begin
              if(EdtPesquisaVidroOS.text='') then
              begin
                    ObjMATERIAISOS.ResgataMateriais(lbCodigo.Caption,'');
                    EdtPesquisaVidroOS.Enabled:=False;
                    EdtPesquisaVidroOS.Visible:=False;
                    pnl7.Visible:=False;
                    EdtPesquisaVidroOS.Text:='';
                    Exit;
              end;
              //codificando para os nomes correto na query de pesquisa

              if(CampoPesquisaVidro = 'CODIGO')
              then CampoPesquisaVidro:='tabmateriaisos.Codigo';

              where:='where UPPER('+CampoPesquisaVidro +')like'+#39+'%'+EdtPesquisaVidroOS.Text+'%'+#39 ;
              ObjMATERIAISOS.ResgataMateriais(lbCodigo.Caption,Where);
              EdtPesquisaVidroOS.Enabled:=False;
              EdtPesquisaVidroOS.Visible:=False;
              pnl7.Visible:=False;
              EdtPesquisaVidroOS.Text:='';
        End; }

end;



procedure TFOrdemServico.edtPesquisaServicoVidroKeyPress(Sender: TObject;
  var Key: Char);
var
  where:string;
begin

end;

procedure TFOrdemServico.btopcoesClick(Sender: TObject);
begin
      self.ObjORDEMSERVICO.BotaoOpcoes(lbCodigo.Caption);
end;

procedure TFOrdemServico.btrelatorioClick(Sender: TObject);
begin
      self.ObjORDEMSERVICO.Imprime(lbCodigo.Caption);
end;

procedure TFOrdemServico.bt1Click(Sender: TObject);
begin
      FAjuda.PassaAjuda('ORDEM DE MEDI��O');
      FAjuda.ShowModal;
end;

procedure TFOrdemServico.edtClienteDblClick(Sender: TObject);
var
  key:Word;
  Shift:TShiftState;
begin
  key:=VK_F9;
  edtClienteKeyDown(Sender, key, Shift);
end;

procedure TFOrdemServico.edtVendedorDblClick(Sender: TObject);
var
  key:Word;
  Shift:TShiftState;
begin
  key:=VK_F9;
  edtVendedorKeyDown(Sender, key, Shift);
end;

{ == Rodolfo == }
procedure TFOrdemServico.edtLargServExit(Sender: TObject);
var
pquantidade,paltura,plargura,plarguraarredondamento,palturaarredondamento:Currency;
begin
   if (edtAltServ.Text <> '') and (edtLargServ.Text <> '') then
   Begin
        Try
           paltura:=strtocurr(tira_ponto(edtAltServ.Text));
        Except
              paltura:=0;
        End;

        Try
           plargura:=strtocurr(tira_ponto(edtLargServ.Text));
        Except
            plargura:=0;
        End;

        FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
        FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);

        pquantidade:=((plargura+plarguraarredondamento)*(paltura+palturaarredondamento))/1000000;
        edtquantidadeservico.Text:=formata_valor(CurrToStr(pquantidade));
   end
   Else edtquantidadeservico.Text:='0';

end;
{ === }

{Rodolfo}
procedure TFOrdemServico.edtAltServExit(Sender: TObject);
begin
      edtLargServExit (Sender);
end;

{Rodolfo}
procedure TFOrdemServico.rbSimClick(Sender: TObject);
begin
    //edtLargServ.Enabled:=True;
    //edtAltServ.Enabled:=True;
    edtquantidadeservico.Enabled := False;
    //edtAltServ.Color:=clWhite;
    //edtLargServ.Color:=clWhite;
    edtquantidadeservico.Color := cl3DLight;

    edtLargServ.Visible:= True;
    edtAltServ.Visible := True;
    lblAltura.Visible := True;
    lblLarg.Visible := True;

    lb2.Left:= 261;
    edtquantidadeservico.Left := 299;

    lb3.Left:=380;
    edtvalorservico.Left:=419;
end;

{Rodolfo}
procedure TFOrdemServico.rbNaoClick(Sender: TObject);
begin
    edtquantidadeservico.Enabled := True;
    edtquantidadeservico.Color := clWhite;

    edtLargServ.Visible:= False;
    edtAltServ.Visible := False;
    lblAltura.Visible := False;
    lblLarg.Visible := False;

    lb2.Left:= 4;
    edtquantidadeservico.Left := 51;

    lb3.Left:=128;
    edtvalorservico.Left:=177;
end;

{Rodolfo}
procedure TFOrdemServico.edtAltServKeyPress(Sender: TObject;
  var Key: Char);
begin
    edtClienteKeyPress(Sender, Key);
end;

{Rodolfo}
procedure TFOrdemServico.edtLargServKeyPress(Sender: TObject;
  var Key: Char);
begin
    edtClienteKeyPress(Sender, Key);
end;

{Rodolfo}
procedure TFOrdemServico.edtquantidadeservicoKeyPress(Sender: TObject;
  var Key: Char);
begin
    edtClienteKeyPress(Sender, Key);
end;

{Rodolfo}
procedure TFOrdemServico.edtvalorservicoKeyPress(Sender: TObject;
  var Key: Char);
begin
    edtClienteKeyPress(Sender, Key);
end;

{Rodolfo}
procedure TFOrdemServico.edtservicoClick(Sender: TObject);
begin
    auxStr := lbNomeServico.Caption;
end;



procedure TFOrdemServico.STRGVidrosServicosDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}    
begin
    if((ARow <> 0) and (ACol = 0))
    then begin
        if(STRGVidrosServicos.Cells[ACol,ARow] = '            X' ) or (STRGVidrosServicos.Cells[ACol,ARow] = 'X' )then
        begin
              Ilprodutos.Draw(STRGVidrosServicos.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 6); //check box marcado
        end
        else
        begin
              if(STRGVidrosServicos.Cells[ACol,ARow] = '            x' ) then
              begin
                  Ilprodutos.Draw(STRGVidrosServicos.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 2); //check box marcado       6
              end
              ELSE begin
                  if(STRGVidrosServicos.Cells[ACol,ARow] <> '             ' ) then
                  begin
                      Ilprodutos.Draw(STRGVidrosServicos.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 5); //check box marcado       6
                  end
                  //else Ilprodutos.Draw(STRGVidrosServicos.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 5); //check box sem marcar    5
              end

        end;

    end;
   { if((ARow <> 0) and (ACol = 4))
    then begin
       if(STRGVidrosServicos.Cells[ACol,ARow] = '            x' ) then
       begin
            Ilprodutos.Draw(STRGVidrosServicos.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 2); //check box marcado       6
       end
    end; }
end;

procedure TFOrdemServico.STRGVidrosServicosDblClick(Sender: TObject);
begin
    if (TStringGrid(sender).cells[0,TStringgrid(sender).row]='')
    Then TStringGrid(sender).cells[0,TStringgrid(sender).row]:='            X'
    Else
    begin
       if (TStringGrid(sender).cells[0,TStringgrid(sender).row]='            X')
       Then TStringGrid(sender).cells[0,TStringgrid(sender).row]:='';
    end
end;

procedure TFOrdemServico.MontaStringGrid;
var
  Query:TIBQuery;
begin
  Query:=TIBQuery.Create(nil);
  Query.Database:=FDataModulo.IBDatabase;
  try
     with STRGListaVidros do
     begin
        RowCount:= 2;
        ColCount:= 3;
        FixedRows:=1;
        FixedCols:=0;
        ColWidths[0]  := 50;
        ColWidths[1]  := 350;
        ColWidths[2]  := 0;
        Cells[1,0]    := 'VIDRO';
        Cells[2,0]    := 'CODIGO';
        if(lbCodigo.Caption<>'0') and (lbCodigo.Caption<>'') then
        begin
            Query.Close;
            Query.sql.Clear;
            Query.sql.Add('select tabmateriaisos.codigo,vidro,tabvidro.descricao from tabmateriaisos');
            Query.sql.Add('join tabvidro on tabvidro.codigo=tabmateriaisos.vidro');
            Query.sql.Add('where ordemservico='+lbCodigo.caption);
            Query.Open;
            while not Query.Eof do
            begin
                RowCount:=RowCount+1;
                Cells[2,RowCount-2]:=Query.fieldbyname('codigo').AsString;
                Cells[1,RowCount-2]:=Query.fieldbyname('descricao').AsString;
                Query.Next;
            end;

        end;
        RowCount:=RowCount-1;
     end;

     with STRGVidrosServicos do
     begin
        RowCount:= 2;
        ColCount:= 3;
        FixedRows:=1;
        FixedCols:=0;
        ColWidths[0]  := 50;
        ColWidths[1]  := 350;
        ColWidths[2]  := 0;
        Cells[1,0]    := 'SERVICO';
        Cells[2,0]    := 'CODIGO';

        if(lbCodigo.Caption<>'0') and (lbCodigo.Caption<>'') then
        begin
            Query.Close;
            Query.sql.Clear;
            Query.sql.Add('select * from tabservicosos');
            Query.sql.Add('where ordemservico='+lbCodigo.caption);
            Query.Open;
            while not Query.Eof do
            begin
                RowCount:=RowCount+1;
                Cells[2,RowCount-2]:=Query.fieldbyname('codigo').AsString;
                Cells[1,RowCount-2]:=Query.fieldbyname('descricao').AsString;
                Query.Next;
            end;

        end;
        RowCount:=RowCount-1;

     end;
  
  finally
      FreeAndNil(Query);
  end;

end;

procedure TFOrdemServico.MenuItem1Click(Sender: TObject);
var
    Query:TIBQuery;
    i:Integer;
    contadorordem:Integer;
begin
  query:=TIBQuery.Create(nil);
  Query.Database:=FDataModulo.IBDatabase ;
  try
      if(lbCodigo.Caption='')
      then Exit;

      Query.Close;
      Query.SQL.Clear;
      Query.SQL.Add('delete from tabvidroservicoos');
      Query.SQL.Add('where ordemdeservico='+lbCodigo.Caption);
      Query.SQL.Add('and vidroos='+STRGListaVidros.Cells[2,STRGListaVidros.Row]);
      Query.SQL.Add('and concluido=''N'' ');
      Query.ExecSQL;
      with ObjVidroServico do
      begin
         contadorordem:=1;
         for i:=1 to STRGVidrosServicos.RowCount-1 do
         begin
            if(STRGVidrosServicos.Cells[0,i]='            X') or (STRGVidrosServicos.Cells[0,i]='            x') or (STRGVidrosServicos.Cells[0,i]='             ')  then
            begin
               Query.Close;
               Query.SQL.Clear;
               Query.SQL.Add('select * from tabvidroservicoos');
               Query.SQL.Add('where ordemdeservico='+lbCodigo.Caption);
               Query.SQL.Add('and vidroos='+STRGListaVidros.Cells[2,STRGListaVidros.Row]);
               Query.SQL.Add('and servicoos='+STRGVidrosServicos.Cells[2,i]);
               Query.Open;

               Query.Last;
               If(query.RecordCount=0) then
               begin
                   Status:=dsInsert;
                   Submit_CODIGO('0');
                   Submit_ORDEMDESERVICO(lbCodigo.Caption);
                   Submit_VIDROOS(STRGListaVidros.Cells[2,STRGListaVidros.Row]);
                   Submit_SERVICOOS(STRGVidrosServicos.Cells[2,i]);
                   Submit_FUNCIONARIO('');
                   Submit_Concluido('N');
                   Submit_OrdemExecucao(IntToStr(contadorordem));
                   contadorordem:=contadorordem+1;
                   if(Salvar(True)=false)then
                   begin
                        MensagemErro('Erro na Grava��o do servi�o para o vidro');
                        Exit;
                   end;
               end;

            end;
         end;
         FDataModulo.IBTransaction.CommitRetaining;
         verificaosconcluida;
         MostrarApenasosServiosdoVidro1Click(Sender);
      end;
  finally
      freeandnil(query);
  end;


end;

procedure TFOrdemServico.MostrarTodososServios1Click(Sender: TObject);
var
    Query:TIBQuery;
    i:Integer;
begin
  query:=TIBQuery.Create(nil);
  Query.Database:=FDataModulo.IBDatabase ;
  try
     //Mostra todos os servi�os...
     with STRGVidrosServicos do
     begin
        RowCount:= 2;
        ColCount:= 3;
        FixedRows:=1;
        FixedCols:=0;
        ColWidths[0]  := 50;
        ColWidths[1]  := 350;
        ColWidths[2]  := 0;
        Cells[1,0]    := 'SERVICO';
        Cells[2,0]    := 'CODIGO';

        if(lbCodigo.Caption<>'0') and (lbCodigo.Caption<>'') then
        begin
            Query.Close;
            Query.sql.Clear;
            Query.sql.Add('select * from tabservicosos');
            Query.sql.Add('where ordemservico='+lbCodigo.caption);
            Query.Open;
            while not Query.Eof do
            begin
                RowCount:=RowCount+1;
                Cells[2,RowCount-2]:=Query.fieldbyname('codigo').AsString;
                Cells[1,RowCount-2]:=Query.fieldbyname('descricao').AsString;
                Query.Next;
            end;

        end;
        RowCount:=RowCount-1;

        for i:=1 to RowCount-1 do
        begin
            Query.Close;
            Query.sql.Clear;
            Query.sql.Add('select * from tabvidroservicoos');
            Query.sql.Add('where VIDROOS='+STRGListaVidros.Cells[2,STRGListaVidros.row]);
            Query.sql.Add('and SERVICOOS='+Cells[2,i]);
            Query.Open;
            if(Query.recordcount>0) then
            begin
                Cells[0,i]:='            X';
            end
            else
                Cells[0,i]:='';
        end;

     end;
  finally
     FreeAndNil(Query);
  end;


end;

procedure TFOrdemServico.MostrarTodososServios2Click(Sender: TObject);
begin
      MostrarTodososServios1Click(Sender);
end;

procedure TFOrdemServico.MostrarApenasosServiosdoVidro1Click(
  Sender: TObject);
var
    Query:TIBQuery;
begin
  query:=TIBQuery.Create(nil);
  Query.Database:=FDataModulo.IBDatabase ;
  try
     //Mostra apenas os servi�os no vidro...

     with STRGVidrosServicos do
     begin
        RowCount:= 2;
        ColCount:= 5;
        FixedRows:=1;
        FixedCols:=0;
        ColWidths[0]  := 50;
        ColWidths[1]  := 350;
        ColWidths[2]  := 0;
        ColWidths[3]  := 0;
        ColWidths[4]  := 0;
        Cells[1,0]    := 'SERVICO';
        Cells[2,0]    := 'CODIGO';
        Cells[3,0]    := 'CODIGO2';   //Codigo na tabvidroservico
        Cells[4,0]    := 'ORDEM';

        Cells[0,1]:='';
        Cells[2,1]:='';
        Cells[1,1]:='';
        Cells[3,1]:='';
        Cells[4,1]:='';

        if(STRGListaVidros.Cells[2,STRGListaVidros.Row]<>'0') and (STRGListaVidros.Cells[2,STRGListaVidros.Row]<>'') then
        begin
            Query.Close;
            Query.sql.Clear;
            Query.sql.Add('select tabvidroservicoos.*,tabservicosos.codigo as codigoservico,tabservicosos.descricao from tabvidroservicoos');
            Query.SQL.Add('join tabservicosos on tabservicosos.codigo=tabvidroservicoos.servicoos');
            Query.sql.Add('where VIDROOS='+STRGListaVidros.Cells[2,STRGListaVidros.Row]);
            Query.SQL.Add('and tabvidroservicoos.ordemdeservico='+lbcodigo.Caption);
            Query.Open;
            while not Query.Eof do
            begin
                RowCount:=RowCount+1;
                Cells[0,RowCount-2]:='';
                Cells[2,RowCount-2]:='';
                Cells[1,RowCount-2]:='';
                if(Query.FieldByName('concluido').asstring='N')
                then Cells[0,RowCount-2]:='             '
                else Cells[0,RowCount-2]:='            x';
                Cells[4,RowCount-2]:=Query.fieldbyname('ORDEMEXECUCAO').AsString;
                Cells[3,RowCount-2]:=Query.fieldbyname('codigo').AsString;
                Cells[2,RowCount-2]:=Query.fieldbyname('codigoservico').AsString;
                Cells[1,RowCount-2]:=Query.fieldbyname('descricao').AsString;
                Query.Next;
            end;

        end;
        RowCount:=RowCount-1;

     end;
  finally
     FreeAndNil(Query);
  end;

end;

procedure TFOrdemServico.MostrarApenasosServiosdoVidro2Click(
  Sender: TObject);
begin
    MostrarApenasosServiosdoVidro1Click(Sender);
end;

procedure TFOrdemServico.STRGListaVidrosClick(Sender: TObject);
begin
    MostrarApenasosServiosdoVidro1Click(Sender);
end;

procedure TFOrdemServico.MarcarTodos1Click(Sender: TObject);
var
  i:integer;
begin
    for i:=1 to STRGVidrosServicos.RowCount-1 do
    begin
       if(STRGVidrosServicos.Cells[0,i]='             ')
       then Exit;

       STRGVidrosServicos.Cells[0,i]:='            X'
    end;
end;

procedure TFOrdemServico.Desmarcartodos1Click(Sender: TObject);
var
  i:integer;
begin
    for i:=1 to STRGVidrosServicos.RowCount-1 do
    begin
       if(STRGVidrosServicos.Cells[0,i]='             ')
       then Exit;
       STRGVidrosServicos.Cells[0,i]:=''
    end;

end;

procedure TFOrdemServico.STRGVidrosServicosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_F5) then
  begin
    if(lbCodigo.Caption='')
    then Exit;

    if(STRGVidrosServicos.ColCount<4)
    then Exit;

    if(STRGListaVidros.RowCount=1) and (STRGListaVidros.Row>0)
    then Exit;

    If (STRGVidrosServicos.RowCount=1) and(STRGVidrosServicos.Row>0)
    Then exit;

    with ObjVidroServico do
    begin
      ZerarTabela;
      ObjServicoos.ZerarTabela;
      ObjMATERIAISOS.ZerarTabela;
      if(LocalizaCodigo(STRGVidrosServicos.Cells[3,STRGVidrosServicos.Row])=False)
      then begin
        Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
        exit;
      end;
      TabelaparaObjeto;
      if(Get_Concluido='N') then
      begin
        if (messagedlg('Deseja Concluir o servi�o '+STRGVidrosServicos.Cells[1,STRGVidrosServicos.Row] +' no material '+STRGListaVidros.Cells[1,STRGListaVidros.Row] +'?',mtconfirmation,[mbyes,mbno],0)=mrno)
        then exit ;
        Status:=dsEdit;
        Submit_Concluido('S');
        if(Salvar(True)=false)
        then MensagemErro('N�o foi poss�vel concluir esse servi�o');
        MostrarApenasosServiosdoVidro1Click(Sender);
      end
      else
      begin
        if (messagedlg('Deseja Reabrir o servi�o '+STRGVidrosServicos.Cells[1,STRGVidrosServicos.Row] +' no material '+STRGListaVidros.Cells[1,STRGListaVidros.Row] +'?',mtconfirmation,[mbyes,mbno],0)=mrno)
        then exit ;
        Status:=dsEdit;
        Submit_Concluido('N');
        if(Salvar(True)=false)
        then MensagemErro('N�o foi poss�vel concluir esse servi�o');
        MostrarApenasosServiosdoVidro1Click(Sender);
      end;

    end;
    if(ObjORDEMSERVICO.VerificaOSConcluida(lbCodigo.Caption)=true)then
    begin
      with ObjORDEMSERVICO do
      begin
        Status:=dsEdit;
        ZerarTabela;
        LocalizaCodigo(lbCodigo.Caption);
        TabelaparaObjeto;
        Submit_CODIGO(lbCodigo.Caption);
        Submit_CONCLUIDO('S');
        Salvar(true);
        LocalizaCodigo(lbCodigo.Caption);
        self.TabelaParaControles;
      end;
    end
    else
    begin
      with ObjORDEMSERVICO do
      begin
        Status:=dsEdit;
        ZerarTabela;
        LocalizaCodigo(lbCodigo.Caption);
        TabelaparaObjeto;
        Submit_CODIGO(lbCodigo.Caption);
        Submit_CONCLUIDO('N');
        Salvar(true);
        LocalizaCodigo(lbCodigo.Caption);
        self.TabelaParaControles;
      end;
    end;
  end;
end;

procedure TFOrdemServico.verificaosconcluida;
begin
    if(ObjORDEMSERVICO.VerificaOSConcluida(lbCodigo.Caption)=true)then
    begin
      with ObjORDEMSERVICO do
      begin
        Status:=dsEdit;
        ZerarTabela;
        LocalizaCodigo(lbCodigo.Caption);
        TabelaparaObjeto;
        Submit_CODIGO(lbCodigo.Caption);
        Submit_CONCLUIDO('S');
        Salvar(true);
        LocalizaCodigo(lbCodigo.Caption);
        self.TabelaParaControles;
      end;
    end
    else
    begin
      with ObjORDEMSERVICO do
      begin
        Status:=dsEdit;
        ZerarTabela;
        LocalizaCodigo(lbCodigo.Caption);
        TabelaparaObjeto;
        Submit_CODIGO(lbCodigo.Caption);
        Submit_CONCLUIDO('N');
        Salvar(true);
        LocalizaCodigo(lbCodigo.Caption);
        self.TabelaParaControles;
      end;
    end;

end;

procedure TFOrdemServico.STRGListaVidrosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_F5) then
  begin
     MenuItem1Click(Sender);
  end;
end;

procedure TFOrdemServico.AlterarCor1Click(Sender: TObject);
var
  key:Word;
  shift:TShiftState;
begin
  key:=VK_F5;
  STRGVidrosServicosKeyDown(Sender,key,shift);
end;

procedure TFOrdemServico.OrdenarExecuodosServios1Click(Sender: TObject);
begin
    //Ordenar os servi�os pra execu��o


end;

procedure TFOrdemServico.edtCodigoBarrasKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8), Chr(13)]) then
    begin
        Key:= #0;
    end;
    if Key =#13 then
    begin
      Self.VerificaCodigoBarras;
      edtCodigoBarras.text:='';
      edtCodigoBarras.SetFocus;
    end;
end;

procedure TFOrdemServico.VerificaCodigoBarras;
var
  Query:TIBQuery;
begin
  query:=TIBQuery.Create(nil);
  Query.Database:=FDataModulo.IBDatabase;

  try
    with Query do
    begin
      Close;
      SQL.Clear;
      SQL.Add('select tabmateriaisos.codigobarras,tabvidro.descricao,tabservicosos.descricao as nomeservico,tabvidroservicoos.ordemexecucao');
      SQL.Add(',tabvidroservicoos.codigo as codigoservico from tabmateriaisos');
      SQL.Add('join tabvidroservicoos on tabvidroservicoos.vidroos=tabmateriaisos.codigo');
      SQL.Add('join tabvidro on tabvidro.codigo=tabmateriaisos.vidro');
      SQL.Add('join tabservicosos on tabservicosos.codigo=tabvidroservicoos.servicoos');
      SQL.Add('where codigobarras='+edtCodigoBarras.Text);
      SQL.Add('and tabmateriaisos.ordemservico='+lbCodigo.Caption);
      SQL.Add('and tabvidroservicoos.concluido=''N''');
      SQL.Add('order by tabvidroservicoos.ordemexecucao');
      
      Open;
      if(recordcount >0)then
      begin
          CriaCodigo(fieldbyname('codigobarras').AsString,img1.Canvas);
          lbNomedoVidro.Caption:=fieldbyname('descricao').AsString;
          lbNomedoServico.Caption:=fieldbyname('nomeservico').AsString;
          ConcluirServico(fieldbyname('codigoservico').AsString);
      end
      else MensagemAviso('Codigo de Barras invalido ou Todos os servi�os nesse vidro foram concluidos!');
    end;
  finally
    FreeAndNil(Query);
  end;


end;

procedure TFOrdemServico.Moverpracima1Click(Sender: TObject);
var
  Aux:TStringList;
begin
  aux:=TStringList.Create;
  try
    if((STRGVidrosServicos.Row-1)=0)
    then Exit;

    aux.Add(STRGVidrosServicos.Cells[0,STRGVidrosServicos.row]);
    aux.Add(STRGVidrosServicos.Cells[1,STRGVidrosServicos.row]);
    aux.Add(STRGVidrosServicos.Cells[2,STRGVidrosServicos.row]);

    STRGVidrosServicos.Cells[0,STRGVidrosServicos.row]:= STRGVidrosServicos.Cells[0,STRGVidrosServicos.row-1];
    STRGVidrosServicos.Cells[1,STRGVidrosServicos.row]:= STRGVidrosServicos.Cells[1,STRGVidrosServicos.row-1];
    STRGVidrosServicos.Cells[2,STRGVidrosServicos.row]:= STRGVidrosServicos.Cells[2,STRGVidrosServicos.row-1];

    STRGVidrosServicos.Cells[0,STRGVidrosServicos.row-1]:= Aux[0];
    STRGVidrosServicos.Cells[1,STRGVidrosServicos.row-1]:= Aux[1];
    STRGVidrosServicos.Cells[2,STRGVidrosServicos.row-1]:= Aux[2];

  finally
    Aux.Free;
  end;

end;

procedure TFOrdemServico.MoverpraBaixo1Click(Sender: TObject);
var
  Aux:TStringList;
begin
  aux:=TStringList.Create;
  try
    if((STRGVidrosServicos.Row+1)>=STRGVidrosServicos.RowCount)
    then Exit;

    aux.Add(STRGVidrosServicos.Cells[0,STRGVidrosServicos.row]);
    aux.Add(STRGVidrosServicos.Cells[1,STRGVidrosServicos.row]);
    aux.Add(STRGVidrosServicos.Cells[2,STRGVidrosServicos.row]);

    STRGVidrosServicos.Cells[0,STRGVidrosServicos.row]:= STRGVidrosServicos.Cells[0,STRGVidrosServicos.row+1];
    STRGVidrosServicos.Cells[1,STRGVidrosServicos.row]:= STRGVidrosServicos.Cells[1,STRGVidrosServicos.row+1];
    STRGVidrosServicos.Cells[2,STRGVidrosServicos.row]:= STRGVidrosServicos.Cells[2,STRGVidrosServicos.row+1];

    STRGVidrosServicos.Cells[0,STRGVidrosServicos.row+1]:= Aux[0];
    STRGVidrosServicos.Cells[1,STRGVidrosServicos.row+1]:= Aux[1];
    STRGVidrosServicos.Cells[2,STRGVidrosServicos.row+1]:= Aux[2];

  finally
    Aux.Free;
  end;

end;

procedure TFOrdemServico.MoverPLinha1Click(Sender: TObject);
var
  Aux:TStringList;
  NumLinha:string;
begin
  aux:=TStringList.Create;

  InputQuery('Ir para Linha','Linha',NumLinha);

  try
    if((StrToInt(NumLinha))>=STRGVidrosServicos.RowCount) or ((StrToInt(NumLinha))<=0)
    then Exit;

    aux.Add(STRGVidrosServicos.Cells[0,STRGVidrosServicos.row]);
    aux.Add(STRGVidrosServicos.Cells[1,STRGVidrosServicos.row]);
    aux.Add(STRGVidrosServicos.Cells[2,STRGVidrosServicos.row]);

    STRGVidrosServicos.Cells[0,STRGVidrosServicos.row]:= STRGVidrosServicos.Cells[0,(StrToInt(NumLinha))];
    STRGVidrosServicos.Cells[1,STRGVidrosServicos.row]:= STRGVidrosServicos.Cells[1,(StrToInt(NumLinha))];
    STRGVidrosServicos.Cells[2,STRGVidrosServicos.row]:= STRGVidrosServicos.Cells[2,(StrToInt(NumLinha))];

    STRGVidrosServicos.Cells[0,(StrToInt(NumLinha))]:= Aux[0];
    STRGVidrosServicos.Cells[1,(StrToInt(NumLinha))]:= Aux[1];
    STRGVidrosServicos.Cells[2,(StrToInt(NumLinha))]:= Aux[2];
  finally
    Aux.Free;
  end;

end;

procedure TFOrdemServico.ConcluirServico(codigoservico:string);
begin
  if(codigoservico<>'') then
  begin
    if(lbCodigo.Caption='')
    then Exit;

    with ObjVidroServico do
    begin
      ZerarTabela;
      ObjServicoos.ZerarTabela;
      ObjMATERIAISOS.ZerarTabela;
      if(LocalizaCodigo(codigoservico)=False)
      then begin
        Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
        exit;
      end;
      TabelaparaObjeto;
      if(Get_Concluido='N') then
      begin
        //if (messagedlg('Deseja Concluir o servi�o '+STRGVidrosServicos.Cells[1,STRGVidrosServicos.Row] +' no material '+STRGListaVidros.Cells[1,STRGListaVidros.Row] +'?',mtconfirmation,[mbyes,mbno],0)=mrno)
        //then exit ;
        Status:=dsEdit;
        Submit_Concluido('S');
        if(Salvar(True)=false)
        then MensagemErro('N�o foi poss�vel concluir o servi�o');
        //MostrarApenasosServiosdoVidro1Click(Sender);
      end
      else
      begin
        //if (messagedlg('Deseja Reabrir o servi�o '+STRGVidrosServicos.Cells[1,STRGVidrosServicos.Row] +' no material '+STRGListaVidros.Cells[1,STRGListaVidros.Row] +'?',mtconfirmation,[mbyes,mbno],0)=mrno)
        //then exit ;
        Status:=dsEdit;
        Submit_Concluido('N');
        if(Salvar(True)=false)
        then MensagemErro('N�o foi poss�vel concluir esse servi�o');
        //MostrarApenasosServiosdoVidro1Click(Sender);
      end;

    end;
    if(ObjORDEMSERVICO.VerificaOSConcluida(lbCodigo.Caption)=true)then
    begin
      with ObjORDEMSERVICO do
      begin
        Status:=dsEdit;
        ZerarTabela;
        LocalizaCodigo(lbCodigo.Caption);
        TabelaparaObjeto;
        Submit_CODIGO(lbCodigo.Caption);
        Submit_CONCLUIDO('S');
        Salvar(true);
        LocalizaCodigo(lbCodigo.Caption);
        self.TabelaParaControles;
      end;
    end
    else
    begin
      with ObjORDEMSERVICO do
      begin
        Status:=dsEdit;
        ZerarTabela;
        LocalizaCodigo(lbCodigo.Caption);
        TabelaparaObjeto;
        Submit_CODIGO(lbCodigo.Caption);
        Submit_CONCLUIDO('N');
        Salvar(true);
        LocalizaCodigo(lbCodigo.Caption);
        self.TabelaParaControles;
      end;
    end;
  end;
end;

procedure TFOrdemServico.AtualizarServiosParaoVidro1Click(Sender: TObject);
begin
    MenuItem1Click(Sender);
end;

procedure TFOrdemServico.MenuItem2Click(Sender: TObject);
begin
    ObjORDEMSERVICO.ImprimeCodigosBarras(lbCodigo.Caption,dbgrdMaterias.DataSource.DataSet.fieldbyname('codigo').asstring);
end;

end.
