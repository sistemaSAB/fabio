unit UENTRADAPRODUTOS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjObjetosEntrada,
  UessencialGlobal, Tabs, Grids, UTitulo_novo,UFORNECEDOR,IBQuery,UpesquisaMenu,UAjuda, UobjMATERIAISENTRADA,
  IBCustomDataSet, ImgList, pngimage, xmldom, XMLIntf, msxmldom, XMLDoc,UobjExtraiXML;


type
  TFENTRADAPRODUTOS = class(TForm)
    pnl1: TPanel;
    StrGridStrgProdutos: TStringGrid;
    pnlDadosPersiana: TPanel;
    edtpesquisa_STRG_GRID: TMaskEdit;
    pnlrodape: TPanel;
    shp1: TShape;
    imgrodape: TImage;
    lbTotalProdutos: TLabel;
    lb31: TLabel;
    lbquantidade: TLabel;
    lb32: TLabel;
    lbLbtitulo1: TLabel;
    lb29: TLabel;
    bt1: TBitBtn;
    lbNomegrupo: TLabel;
    lbNomediametro: TLabel;
    pnlbotes: TPanel;
    btAjuda: TSpeedButton;
    lbnomeformulario: TLabel;
    btnovo: TBitBtn;
    btSalvar: TBitBtn;
    btalterar: TBitBtn;
    btpesquisar: TBitBtn;
    btcancelar: TBitBtn;
    btexcluir: TBitBtn;
    btopcoes: TBitBtn;
    btrelatorios: TBitBtn;
    btsair: TBitBtn;
    pnl2: TPanel;
    lb1: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb5: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    lb22: TLabel;
    lbConcluido: TLabel;
    lb24: TLabel;
    lb8: TLabel;
    lbLbVALORFRETE: TLabel;
    lbLbDESCONTOS: TLabel;
    lbLbOUTROSGASTOS: TLabel;
    lb9: TLabel;
    lb4: TLabel;
    lb23: TLabel;
    lb30: TLabel;
    shp2: TShape;
    lb33: TLabel;
    edtCodigo: TEdit;
    edtData: TMaskEdit;
    edtEMISSAONF: TMaskEdit;
    edtNF: TEdit;
    edtFornecedor: TEdit;
    mmoobservacoes: TMemo;
    edtDESCONTOS: TEdit;
    edtVALORFRETE: TEdit;
    edtOUTROSGASTOS: TEdit;
    edtvaloripi: TEdit;
    grpGroupCreditos: TGroupBox;
    lb10: TLabel;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    edtcreditoicms: TEdit;
    edtcreditopis: TEdit;
    edtcreditocofins: TEdit;
    edtcreditoipi: TEdit;
    edticmssubstituto: TEdit;
    chkCheckFreteFornecedor: TCheckBox;
    chkCheckIcmsFrete: TCheckBox;
    edtVALORPRODUTOS: TMaskEdit;
    edtValorFinal: TMaskEdit;
    edtModeloNF: TEdit;
    edtcfop: TEdit;
    lb45: TLabel;
    edtChaveAcesso: TEdit;
    lb47: TLabel;
    edtBASECALCULOICMS: TEdit;
    lb48: TLabel;
    edtVALORICMS: TEdit;
    lb49: TLabel;
    edtBASECALCULOICMSSUBSTITUICAO: TEdit;
    lb46: TLabel;
    edtValorSeguro: TEdit;
    Label1: TLabel;
    edtSerieNF: TEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    lb21: TLabel;
    lb27: TLabel;
    lb28: TLabel;
    lb25: TLabel;
    lb26: TLabel;
    lb36: TLabel;
    lb37: TLabel;
    lb38: TLabel;
    lb39: TLabel;
    lb40: TLabel;
    lb18: TLabel;
    lb19: TLabel;
    lb35: TLabel;
    lb41: TLabel;
    lb16: TLabel;
    lb42: TLabel;
    lbPorcentagemReducao: TLabel;
    cbb_ep: TComboBox;
    edtproduto_ep: TEdit;
    edtcor_ep: TEdit;
    edtgrupopersiana_EP: TEdit;
    edtdiametropersiana_EP: TEdit;
    edtEdtCFOPProduto: TEdit;
    edtEdtST_A: TEdit;
    edtEdtST_B: TEdit;
    edtCSOSN: TEdit;
    edtEdtUnidade: TEdit;
    edtquantidade_ep: TEdit;
    edtvalor_ep: TEdit;
    edtdesconto: TEdit;
    edtBaseCalculoICMS_p: TEdit;
    edtcreditoicms_produtos: TEdit;
    edtValorICMS_p: TEdit;
    edtPorcentagemReducao: TEdit;
    TabSheet2: TTabSheet;
    lb14: TLabel;
    Label2: TLabel;
    lb15: TLabel;
    Label3: TLabel;
    edtcreditocofins_produtos: TEdit;
    edtPercentualCofins: TEdit;
    edtcreditopis_produtos: TEdit;
    edtPercentualPis: TEdit;
    TabSheet3: TTabSheet;
    lb17: TLabel;
    lb44: TLabel;
    lb20: TLabel;
    edtcreditoipi_produtos: TEdit;
    edtValorIPI_p: TEdit;
    edtipipago_produtos: TEdit;
    TabSheet4: TTabSheet;
    lb43: TLabel;
    edtValorFreteProduto: TEdit;
    Panel1: TPanel;
    edtordeminsercao_ep: TEdit;
    edtcodigo_ep: TEdit;
    lbTipoCampoProduto: TListBox;
    btGravar1: TBitBtn;
    btcancelar1: TBitBtn;
    btxcluir: TBitBtn;
    lbnomeproduto: TLabel;
    lbnomeCor: TLabel;
    Label4: TLabel;
    edtValorOutros: TEdit;
    query: TIBQuery;
    lbVerificado: TLabel;
    imgVerificado: TImage;
    ImageList1: TImageList;
    imgValidaSped: TImage;
    XMLDocument1: TXMLDocument;
    TabSheet5: TTabSheet;
    edtAliquotaST: TEdit;
    edtBCICMSST: TEdit;
    edtValorICMSST: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    edtCstPis: TEdit;
    edtCstCofins: TEdit;
    edtCstIPI: TEdit;
    Label9: TLabel;
    Label10: TLabel;
    edtBCIPI: TEdit;
    panelTotalRestante: TPanel;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    lbTotalRestante: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    lbProdutoXml: TLabel;
    imgOk: TImage;
    Label13: TLabel;

    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtFORNECEDORExit(Sender: TObject);
    procedure EdtFORNECEDORKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure mmoobservacoesKeyPress(Sender: TObject; var Key: Char);
    procedure btopcoesClick(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure edtproduto_epKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtproduto_epExit(Sender: TObject);
    procedure cbb_epChange(Sender: TObject);
    procedure edtcor_epKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcor_epExit(Sender: TObject);
    procedure edtgrupopersiana_EPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtgrupopersiana_EPExit(Sender: TObject);
    procedure edtdiametropersiana_EPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtdiametropersiana_EPExit(Sender: TObject);
    procedure BtCancelar_EPClick(Sender: TObject);
    procedure btGravar_EPClick(Sender: TObject);
    procedure StrGridStrgProdutosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure StrGridStrgProdutosKeyPress(Sender: TObject; var Key: Char);
    procedure edtpesquisa_STRG_GRIDKeyPress(Sender: TObject;
      var Key: Char);
    procedure StrGridStrgProdutosEnter(Sender: TObject);
    procedure StrGridStrgProdutosDblClick(Sender: TObject);
    procedure BtExcluir_EpClick(Sender: TObject);
    procedure edtvalor_epKeyPress(Sender: TObject; var Key: Char);

    procedure StrGridStrgProdutosDrawCell(Sender: TObject; ACol,
      ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure lbLbtitulo1MouseLeave(Sender: TObject);
    procedure lbLbtitulo1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure imgrodapeMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbLbtitulo1Click(Sender: TObject);
    procedure edtFornecedorKeyPress(Sender: TObject; var Key: Char);
    procedure edtproduto_epKeyPress(Sender: TObject; var Key: Char);
    procedure edtcor_epKeyPress(Sender: TObject; var Key: Char);
    procedure edtgrupopersiana_EPKeyPress(Sender: TObject; var Key: Char);
    procedure edtdiametropersiana_EPKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtFornecedorDblClick(Sender: TObject);
    procedure btAjudaClick(Sender: TObject);
    procedure edtModeloNFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcfopKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEdtCFOPProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEdtST_AKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEdtST_BKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCSOSNKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtVALORPRODUTOSKeyPress(Sender: TObject; var Key: Char);
    procedure lbNomeFornecedorMouseLeave(Sender: TObject);
    procedure lbNomeFornecedorMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure lbLbModeloNfClick(Sender: TObject);
    procedure lb34Click(Sender: TObject);
    procedure edtEdtUnidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtModeloNFDblClick(Sender: TObject);
    procedure edtcfopDblClick(Sender: TObject);
    procedure edtproduto_epDblClick(Sender: TObject);
    procedure edtgrupopersiana_EPDblClick(Sender: TObject);
    procedure edtdiametropersiana_EPDblClick(Sender: TObject);
    procedure edtcor_epDblClick(Sender: TObject);
    procedure edtEdtCFOPProdutoDblClick(Sender: TObject);
    procedure edtEdtST_ADblClick(Sender: TObject);
    procedure edtEdtST_BDblClick(Sender: TObject);
    procedure edtCSOSNDblClick(Sender: TObject);
    procedure edtEdtUnidadeDblClick(Sender: TObject);
    procedure cbb_epKeyPress(Sender: TObject; var Key: Char);
    procedure edtDataKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEdtUnidadeKeyPress(Sender: TObject; var Key: Char);
    procedure edtEMISSAONFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbNomeFornecedorClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtPercentualCofinsKeyPress(Sender: TObject; var Key: Char);
    procedure edtPercentualPisKeyPress(Sender: TObject; var Key: Char);
    procedure edtValorOutrosKeyPress(Sender: TObject; var Key: Char);
    procedure imgValidaSpedClick(Sender: TObject);
    procedure edtBaseCalculoICMS_pExit(Sender: TObject);
    procedure edtcreditoicms_produtosExit(Sender: TObject);
    procedure edtPorcentagemReducaoExit(Sender: TObject);
    procedure edtValorICMS_pExit(Sender: TObject);
    procedure edtChaveAcessoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtAliquotaSTKeyPress(Sender: TObject; var Key: Char);
    procedure edtBCICMSSTKeyPress(Sender: TObject; var Key: Char);
    procedure edtValorICMSSTKeyPress(Sender: TObject; var Key: Char);
    procedure edtAliquotaSTExit(Sender: TObject);
    procedure edtBCICMSSTExit(Sender: TObject);
    procedure edtValorICMSSTExit(Sender: TObject);
    procedure edtCstPisKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCstCofinsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCstIPIKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtdescontoExit(Sender: TObject);
    procedure edtPercentualPisExit(Sender: TObject);
    procedure edtcreditopis_produtosExit(Sender: TObject);
    procedure edtPercentualCofinsExit(Sender: TObject);
    procedure edtcreditocofins_produtosExit(Sender: TObject);
    procedure edtcreditoipi_produtosExit(Sender: TObject);
    procedure edtValorIPI_pExit(Sender: TObject);
    procedure edtipipago_produtosExit(Sender: TObject);
    procedure edtValorFreteProdutoExit(Sender: TObject);
    procedure edtValorOutrosExit(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure edtvalor_epExit(Sender: TObject);


  private

    indexItem:Smallint;
    contProdutos:Smallint;

    Usouf9napesquisa:Boolean;
    ObjObjetosEntrada:TObjObjetosEntrada;
    PMostraTelaAlteracaoPreco,PalteraCusto,PConsideraSOmenteVidroINcolor,PConsideraSomenteAnodizadoFosco, PConsideraSomentePinturaFosca:Boolean;
    objExtraiXML:TobjExtraiXML;
    
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    Procedure AtualizaGrid;
    procedure habilitaCamposProduto;
    procedure DesabilitaCamposProduto;
    function AumentaEstoque(Produto,TabMaterial,quantidade:string):Boolean;
    function ValidaUnidadeMedida:Boolean;
    function VerificaPrecoTotalProdutos(ExcetoLinha:integer):Boolean;
    function validaSped():boolean;
    function validaSpedItens():boolean;
    function verificaEntrada():Boolean;
    function excluirItens():boolean;
    procedure limpaCamposProduto();
    procedure setImgVerificado(imgIndex:SmallInt);
    procedure getItemXML();


    { Private declarations }
  public
    { Public declarations }
  end;

var
  FENTRADAPRODUTOS: TFENTRADAPRODUTOS;


implementation

uses Upesquisa, UessencialLocal, UobjDiverso_EP, UobjFERRAGEM_EP,
  UobjKitBox_EP, UobjPerfilado_EP, UobjPERSIANA_EP, UobjVidro_EP,
  Uformata_String_Grid, UobjDIVERSO, UobjFERRAGEMCOR, UobjKITBOXCOR,
  UobjPERFILADOCOR, UDataModulo, UalteraPrecoDiverso, UobjDIVERSOCOR,
  UalteraPrecoFerragem, UAlteraPrecoKitbox, UAlteraPrecoPerfilado,
  UAlteraPrecoVidro, UAlteraPrecoPErsiana, UescolheImagemBotao, Uprincipal,
  UobjCSOSN, UobjENTRADAPRODUTOS, UAcertoUnidadeEntrada, UModeloDeNF,
  UCFOP, UFornecedor2, UFERRAGEM, UDIVERSO, UVIDRO, UPERFILADO, UPERSIANA,
  UKITBOX, StrUtils, UComponentesNfe, UobjIMPOSTO_PIS,
  UobjIMPOSTO_COFINS, UobjIMPOSTO_IPI;

{$R *.dfm}


procedure TFENTRADAPRODUTOS.FormCreate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjObjetosEntrada:=TObjObjetosEntrada.create(self)
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     PConsideraSomenteAnodizadoFosco:=False;
     if (ObjParametroGlobal.ValidaParametro('CONSIDERA SOMENTE "ANODIZADO FOSCO" PARA ATUALIZAR O CUSTO DO PERFILADO NA COMPRA')=True)
     Then Begin
               if (ObjParametroGlobal.get_valor='SIM')
               Then PConsideraSomenteAnodizadoFosco:=True;
     End;


     PConsideraSomentePinturaFosca:=False;
     if (ObjParametroGlobal.ValidaParametro('CONSIDERA SOMENTE "PINTURA FOSCA" PARA ATUALIZAR O CUSTO DO KITBOX NA COMPRA')=True)
     Then Begin
               if (ObjParametroGlobal.get_valor='SIM')
               Then PConsideraSomentePinturaFosca:=True;
     End;

     PConsideraSOmenteVidroINcolor:=False;
     if (ObjParametroGlobal.ValidaParametro('CONSIDERA SOMENTE "INCOLOR" PARA ATUALIZAR O CUSTO DO VIDRO NA COMPRA')=True)
     Then Begin
               if (ObjParametroGlobal.get_valor='SIM')
               Then PConsideraSOmenteVidroINcolor:=True;
     End;

     PAlteraCusto:=False;
     if (ObjParametroGlobal.ValidaParametro('ALTERA O CUSTO NA COMPRA')=True)
     Then Begin
               if (ObjParametroGlobal.get_valor='SIM')
               Then PalteraCusto:=True;
     End;

     PMostraTelaAlteracaoPreco:=False;
     if (ObjParametroGlobal.ValidaParametro('MOSTRA TELA DE ALTERACAO DO PRECO NA COMPRA')=True)
     Then Begin
               if (ObjParametroGlobal.get_valor='SIM')
               Then PMostraTelaAlteracaoPreco:=True;
     End;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE ENTRADA DE PRODUTOS')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     FescolheImagemBotao.PegaFiguraBotaopequeno(BtGravar1,'BOTAOINSERIR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btCancelar1,'BOTAOCANCELARPRODUTO.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btxcluir,'BOTAORETIRAR.BMP');
     pnlDadosPersiana.Visible:=false;
     Self.AtualizaGrid;

end;



procedure TFENTRADAPRODUTOS.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  If (Self.ObjObjetosEntrada=Nil) Then
    exit;

  If (Self.ObjObjetosEntrada.ObjEntradaProdutos.status<>dsinactive) Then
  Begin
    Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
    abort;
    exit;
  End;

  FreeAndNil(objExtraiXML);
  Self.ObjObjetosEntrada.free;
  Action := caFree;
  Self := nil;

end;


procedure TFENTRADAPRODUTOS.btnovoClick(Sender: TObject);
begin

     {verifica se a entrada � valida para gerar o sped}
     if utilizaSpedFiscal then
       if not verificaEntrada then
          Exit;

     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     DesabilitaCamposProduto;
     Self.AtualizaGrid;

     setImgVerificado(0);

     edtcodigo.text:='0';
     EdtVALORFINAL.text:='0';
     EdtDESCONTOS.text:='0';
     EdtVALORFRETE.text:='0';
     EdtOUTROSGASTOS.text:='0';
     lbConcluido.Caption:='Aberto';
     lbConcluido.Visible:=True;

     edtcodigo.enabled:=False;
     EdtVALORFINAL.enabled:=False;

     edtcreditoicms.Text:='0';
     edtcreditopis.Text:='0';
     edtcreditocofins.Text:='0';
     edtcreditoipi.Text:='0';

     edtcreditoicms.enabled:=False;
     edtcreditopis.enabled:=False;
     edtcreditocofins.enabled:=False;
     edtcreditoipi.enabled:=False;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorios.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false ;
     imgValidaSped.visible := False;

     ObjObjetosEntrada.ObjEntradaProdutos.status:=dsInsert;

     PageControl1.TabIndex := 0;

     edtModeloNF.SetFocus;
     panelTotalRestante.Visible := false;
     imgOk.Visible := False;


end;

procedure TFENTRADAPRODUTOS.btSalvarClick(Sender: TObject);
var
  countItens:Smallint;
  nfRepetida:string;
begin

  If ObjObjetosEntrada.ObjEntradaProdutos.Status=dsInactive Then
    exit;

  if (Trim(edtFornecedor.Text) = '') then
  begin
    ShowMessage('Fornecedor vazio');
    Exit;
  end;


  if (ObjObjetosEntrada.ObjEntradaProdutos.Status = dsInsert) then
  begin

    query.Database := FDataModulo.IBDatabase;
    query.Close;

    query.SQL.Text :=
    'select e.codigo '+
    'from tabentradaprodutos e '+
    'where nf=:nf and e.fornecedor=:codigo';

    query.Params[0].AsString := edtNF.Text;
    query.Params[1].AsInteger := StrToInt(edtFornecedor.Text);

    query.Open;
    query.First;

    nfRepetida := '';
    while (not query.Eof) do
    begin
      nfRepetida := nfRepetida + query.Fields[0].AsString+'/';
      query.Next;
    end;

    if (nfRepetida <> '') then
    begin
      nfRepetida[Length(nfRepetida)] := #0;
      MensagemAviso('Ja existe esta nf cadastrada. Entrada: '+nfRepetida);
      Exit;
    end;

  end;

  If ControlesParaObjeto=False Then
  Begin
    Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
    exit;
  End;

  If (ObjObjetosEntrada.ObjEntradaProdutos.salvar(true)=False) Then
    exit;

  edtCodigo.text:=ObjObjetosEntrada.ObjEntradaProdutos.Get_codigo;
  habilita_botoes(Self);
  desabilita_campos(Self);

  ObjObjetosEntrada.ObjEntradaProdutos.LocalizaCodigo(edtcodigo.text);
  ObjObjetosEntrada.ObjEntradaProdutos.TabelaparaObjeto;
  Self.TabelaParaControles;

  btnovo.Visible :=true;
  btalterar.Visible:=true;
  btpesquisar.Visible:=true;
  btrelatorios.Visible:=true;
  btexcluir.Visible:=true;
  btsair.Visible:=true;
  btopcoes.Visible :=true;
  imgValidaSped.Visible := true;
  habilitaCamposProduto;

  if(panelTotalRestante.Visible) then
  begin
    indexItem := 0;
    getItemXML();
  end;

  imgOk.Visible := False;

end;

procedure TFENTRADAPRODUTOS.btAlterarClick(Sender: TObject);
begin

    IF (ObjObjetosEntrada.ObjEntradaProdutos.Get_concluido='S') or (ObjObjetosEntrada.ObjEntradaProdutos.TITULO.Get_CODIGO<>'')
    then
    Begin
      Messagedlg('Esta Entrada j� foi conclu�da, n�o � poss�vel alter�-la',mtinformation,[mbok],0);
      exit;
    end;


    If (ObjObjetosEntrada.ObjEntradaProdutos.Status=dsinactive) and (EdtCodigo.text<>'') Then
    Begin
        habilita_campos(Self);
        limpaedit(pnl1);
        DesabilitaCamposProduto;
        EdtCodigo.enabled:=False;
        lbConcluido.Visible:=True;
        EdtVALORFINAL.enabled:=False;

        edtcreditoicms.enabled:=False;
        edtcreditopis.enabled:=False;
        edtcreditocofins.enabled:=False;
        edtcreditoipi.enabled:=False;


        ObjObjetosEntrada.ObjEntradaProdutos.Status:=dsEdit;
        edtdata.setfocus;
        btSalvar.enabled:=True;
        BtCancelar.enabled:=True;
        btpesquisar.enabled:=True;
        btnovo.Visible :=false;
        btalterar.Visible:=false;
        btpesquisar.Visible:=false;
        btrelatorios.Visible:=false;
        btexcluir.Visible:=false;
        btsair.Visible:=false;
        btopcoes.Visible :=false;
        imgValidaSped.Visible := False;
    End;

end;

procedure TFENTRADAPRODUTOS.btCancelarClick(Sender: TObject);
begin
     ObjObjetosEntrada.ObjEntradaProdutos.cancelar;
     panelTotalRestante.Visible := False;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorios.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     imgValidaSped.Visible:=True;
     Self.AtualizaGrid;
     PageControl1.TabIndex := 0;
     imgOk.Visible := False;

end;

procedure TFENTRADAPRODUTOS.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
           //limpaedit(Self);


            FpesquisaLocal.NomeCadastroPersonalizacao:='TFENTRADAPRODUTOS.btPesquisarClick';
            If (FpesquisaLocal.PreparaPesquisa(ObjObjetosEntrada.ObjEntradaProdutos.Get_pesquisa,ObjObjetosEntrada.ObjEntradaProdutos.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjObjetosEntrada.ObjEntradaProdutos.status<>dsinactive
                                  then exit;

                                  If (ObjObjetosEntrada.ObjEntradaProdutos.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjObjetosEntrada.ObjEntradaProdutos.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;
                                  panelTotalRestante.Visible := False;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFENTRADAPRODUTOS.btExcluirClick(Sender: TObject);
begin

  setImgVerificado(0);

  if (ObjObjetosEntrada.ObjEntradaProdutos.status<>dsinactive) or (Edtcodigo.text='') then
    exit;

  if (lbConcluido.Caption = 'Conclu�do') then
  begin
    ShowMessage('Entrada concluida n�o pode ser excluida');
    Exit;
  end;

  if (ObjObjetosEntrada.ObjEntradaProdutos.LocalizaCodigo(edtcodigo.text)=False) then
  begin
    Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
    exit;
  end;

  if (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno) then
    exit;

  excluirItens();
  if not (ObjObjetosEntrada.ObjEntradaProdutos.exclui(edtcodigo.text,True)) then
  begin
    Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
    exit;
  end;

  limpaedit(Self);
  Self.limpaLabels;
  panelTotalRestante.Visible := False;
  Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
  imgOk.Visible := False;

end;

procedure TFENTRADAPRODUTOS.btRelatorioClick(Sender: TObject);
begin
    ObjObjetosEntrada.Imprime(edtcodigo.text);
end;

procedure TFENTRADAPRODUTOS.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFENTRADAPRODUTOS.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFENTRADAPRODUTOS.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFENTRADAPRODUTOS.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin

    If (ssCtrl in Shift) and (Key = vk_return) then
    begin
      if (edtproduto_ep.Enabled) then
      begin
        if (edtproduto_ep.Text <> '') then
          btGravar_EPClick(btGravar1);
      end;
    end;


    if Key=VK_Escape then
      Self.Close;
    if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

           FAjuda.PassaAjuda('ENTRADA DE PRODUTOS');
           FAjuda.ShowModal;
    end;

end;

procedure TFENTRADAPRODUTOS.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFENTRADAPRODUTOS.ControlesParaObjeto: Boolean;
begin

  Try
    With ObjObjetosEntrada.ObjEntradaProdutos do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        FORNECEDOR.Submit_codigo(edtFORNECEDOR.text);
        CFOP.Submit_CODIGO(edtcfop.Text);
        MODELONF.Submit_CODIGO(edtModeloNF.Text);
        Submit_DATA(edtDATA.text);
        TITULO.Submit_codigo(lbLbtitulo1.Caption);
        Submit_VALORPRODUTOS(edtVALORPRODUTOS.text);
        Submit_DESCONTOS(edtDESCONTOS.text);
        Submit_OBSERVACOES(mmoOBSERVACOES.text);
        Submit_NF(edtNF.text);
        Submit_EMISSAONF(edtEMISSAONF.text);

        if(lbConcluido.Caption = 'Aberto')then
          Submit_CONCLUIDO('N')
        else
          Submit_CONCLUIDO('S');

        Submit_VALORFRETE(edtVALORFRETE.text);
        Submit_OUTROSGASTOS(edtOUTROSGASTOS.text);
        Submit_IcmsSubstituto(edtIcmsSubstituto.text);
        Submit_VALORICMS(edtVALORICMS.Text);
        Submit_BASECALCULOICMS(edtBASECALCULOICMS.Text);
        Submit_BASECALCULOICMSSUBSTITUICAO(edtBASECALCULOICMSSUBSTITUICAO.Text);
        //Submit_VALORSEGURO(edtValorSeguro.Text);

        Submit_valoripi     (edtvaloripi     .text);
        Submit_CreditoICMS  (edtCreditoICMS  .text);
        Submit_CreditoPIS   (edtCreditoPIS   .text);
        Submit_CreditoCOFINS(edtCreditoCOFINS.text);
        Submit_CreditoIPI   (edtCreditoIPI   .text);

        if(chkCheckFreteFornecedor.Checked=True)
        then Submit_FreteFornecedor('S')
        else Submit_FreteFornecedor('N');

        submit_spedvalido('N');

        if(chkCheckIcmsFrete.Checked=True)
        then Submit_IcmsSobreFrete('S')
        else Submit_IcmsSobreFrete('N');

        Submit_ChaveNfe(edtChaveAcesso.Text);
        Submit_serienf(edtSerieNF.Text);


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFENTRADAPRODUTOS.LimpaLabels;
begin
     //lbConcluido.Caption:='';
     lbLbtitulo1.Caption:='';
     lbnomeproduto.Caption:='';
     lbProdutoXml.Caption:='';
     lbnomecor.Caption:='';
     lbnomegrupo.Caption:='';
     lbNomeDiametro.Caption:='';
end;

function TFENTRADAPRODUTOS.ObjetoParaControles: Boolean;
begin
  Try
     With ObjObjetosEntrada.ObjEntradaProdutos do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtFORNECEDOR.text:=FORNECEDOR.Get_codigo;
        edtModeloNF.Text:=MODELONF.Get_CODIGO;
        edtcfop.Text:=CFOP.Get_CODIGO;
        EdtDATA.text:=Get_DATA;
        lbLbtitulo1.Caption:=TITULO.Get_codigo;;
        EdtVALORPRODUTOS.text:=Get_VALORPRODUTOS;
        EdtDESCONTOS.text:=Get_DESCONTOS;
        EdtVALORFINAL.text:=Get_VALORFINAL;
        mmoOBSERVACOES.text:=Get_OBSERVACOES;
        EdtNF.text:=Get_NF;
        EdtEMISSAONF.text:=Get_EMISSAONF;

        if(Get_CONCLUIDO= 'N') then
          lbConcluido.Caption := 'Aberto'
        else
          lbConcluido.Caption := 'Conclu�do';

        EdtVALORFRETE.text:=Get_VALORFRETE;
        EdtOUTROSGASTOS.text:=Get_OUTROSGASTOS;
        EdtIcmsSubstituto.text:=Get_IcmsSubstituto;
        edtVALORICMS.Text:=Get_VALORICMS;
        edtBASECALCULOICMS.Text:=Get_BASECALCULOICMS;
        edtBASECALCULOICMSSUBSTITUICAO.Text:=Get_BASECALCULOICMSSUBSTITUICAO;
        //edtValorSeguro.Text:=Get_VALORSEGURO;

        if(Get_icmssobrefrete='S')
        then chkCheckIcmsFrete.Checked:=True
        else chkCheckIcmsFrete.Checked:=False;

        if(Get_FreteFornecedor='S')
        then chkCheckFreteFornecedor.Checked:=True
        else chkCheckFreteFornecedor.Checked:=False;

        edtvaloripi.text      := Get_valoripi;
        edtCreditoICMS.text   := Get_CreditoICMS;
        edtCreditoPIS.text    := Get_CreditoPIS;
        edtCreditoCOFINS.text := Get_CreditoCOFINS;
        edtCreditoIPI.text    := Get_CreditoIPI;
        edtChaveAcesso.Text   := Get_ChaveNfe;
        edtSerieNF.Text       := Get_serienf;

        if (get_spedvalido = 'S') then
          setImgVerificado(1)
        else
          setImgVerificado(0);


        if (EdtCODIGO.Text='') or (ObjObjetosEntrada.ObjEntradaProdutos.Status=dsinsert)
        Then Begin

                          exit;
        end;

        lbnomeproduto.caption:='';
        lbProdutoXml.Caption:='';
        lbnomecor.caption    :='';
        Lbnomegrupo.caption    :='';
        LbNomeDiametro.caption    :='';
        Usouf9napesquisa:=False;


        if (lbConcluido.Caption ='N')  or (lbConcluido.Caption = 'Aberto') then
        Begin//soh habilita para entradas nao concluidas
          cbb_ep.ItemIndex:=0;
        end;

        Self.Atualizagrid;
        habilitaCamposProduto;

        result:=True;

     End;
  Except
        Result:=False;
  End;
end;

function TFENTRADAPRODUTOS.TabelaParaControles: Boolean;
begin
     If (ObjObjetosEntrada.ObjEntradaProdutos.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFENTRADAPRODUTOS.EdtFORNECEDORExit(Sender: TObject);
begin
     ObjObjetosEntrada.ObjEntradaProdutos.EdtFORNECEDORExit(sender,nil);
end;

procedure TFENTRADAPRODUTOS.EdtFORNECEDORKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjObjetosEntrada.ObjEntradaProdutos.EdtFORNECEDORKeyDown(sender,key,shift,nil);
end;

procedure TFENTRADAPRODUTOS.mmoobservacoesKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then mmoobservacoes.setfocus;
end;

procedure TFENTRADAPRODUTOS.btopcoesClick(Sender: TObject);
begin
  
     ObjObjetosEntrada.ObjEntradaProdutos.Opcoes(EdtCodigo.text);

     if (edtcodigo.text<>'')
     and (ObjObjetosEntrada.ObjEntradaProdutos.Status=dsinactive)
     Then Begin
               ObjObjetosEntrada.ObjEntradaProdutos.LocalizaCodigo(edtcodigo.text);
               ObjObjetosEntrada.ObjEntradaProdutos.TabelaparaObjeto;
               Self.TabelaParaControles;
     End;
end;

procedure TFENTRADAPRODUTOS.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     //if (newtab=0)//Principal
     //Then Begin
               if (edtcodigo.Text<>'')
               Then Begin
                         if (Self.ObjObjetosEntrada.ObjEntradaProdutos.LocalizaCodigo(EdtCODIGO.Text)=True)
                         then Begin
                                   Self.ObjObjetosEntrada.ObjEntradaProdutos.TabelaparaObjeto;
                                   Self.ObjetoParaControles;
                         End;
               End;


               exit;
    // End;

     //if (newtab=1)//produtos
    // Then Begin
               if (EdtCODIGO.Text='') or (ObjObjetosEntrada.ObjEntradaProdutos.Status=dsinsert)
               Then Begin
                          AllowChange:=False;
                          exit;
               end;

               lbnomeproduto.caption:='';
               lbProdutoXml.Caption:='';
               lbnomecor.caption    :='';
               Lbnomegrupo.caption    :='';
               LbNomeDiametro.caption    :='';
               Usouf9napesquisa:=False;

               if (lbConcluido.Caption ='N')
               Then Begin//soh habilita para entradas nao concluidas
                         cbb_ep.ItemIndex:=0;
                         cbb_epChange(sender);
               end;

               Self.Atualizagrid;
               exit;
     //End;

end;

procedure TFENTRADAPRODUTOS.edtproduto_epKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

  if Key<>Vk_f9
  Then exit;

  edtproduto_ep.Text:='';

Try

  Case cbb_ep.ItemIndex of

     0:Begin//diversos
            ObjObjetosEntrada.ObjDiverso_ep.DiversoCor.EdtDiversoKeyDown(sender,key,shift,lbnomeproduto);
     End;
     1:Begin//ferragem
            ObjObjetosEntrada.Objferragem_ep.FerragemCor.EdtFerragemKeyDown(sender,key,shift,lbnomeproduto);
     End;
     2:Begin//KitBox
            ObjObjetosEntrada.Objkitbox_ep.KitBoxCor.EdtKitBoxKeyDown(sender,key,shift,lbnomeproduto);
     End;
     3:Begin//Perfilado
            ObjObjetosEntrada.Objperfilado_ep.PerfiladoCor.EdtPerfiladoKeyDown(sender,key,shift,lbnomeproduto);
     End;
     4:Begin//Persiana
            ObjObjetosEntrada.ObjPersiana_ep.PersianaGrupoDiametroCor.EdtPersianaKeyDown(sender,key,shift,lbnomeproduto);
     End;
     5:Begin//Vidro
            ObjObjetosEntrada.Objvidro_ep.VidroCor.EdtVidroKeyDown(sender,key,shift,lbnomeproduto);
     End;

  End;

Finally
       if (edtproduto_ep.Text<>'')
       Then Usouf9napesquisa:=True;//significa que ele retorna o codigo
End;

end;

procedure TFENTRADAPRODUTOS.edtproduto_epExit(Sender: TObject);
begin
     lbnomeproduto.caption:='';

     if (edtproduto_ep.Text='')
     Then exit;


  Case cbb_ep.ItemIndex of
     0:Begin//diversos
            if (Usouf9napesquisa=False)//digitou a referencia
            Then Begin
                      if (ObjObjetosEntrada.ObjDiverso_ep.DiversoCor.Diverso.LocalizaReferencia(edtproduto_ep.Text)=False)
                      Then edtproduto_ep.Text:=''
                      Else Begin
                                ObjObjetosEntrada.ObjDiverso_ep.DiversoCor.Diverso.TabelaparaObjeto;
                                edtproduto_ep.Text:=ObjObjetosEntrada.ObjDiverso_ep.DiversoCor.Diverso.get_codigo;
                      End;
            End;

            ObjObjetosEntrada.ObjDiverso_ep.DiversoCor.EdtDiversoExit(sender,lbnomeproduto);

     End;
     1:Begin//ferragem
            if (Usouf9napesquisa=False)//digitou a referencia
            Then Begin
                      if (ObjObjetosEntrada.Objferragem_ep.ferragemCor.ferragem.LocalizaReferencia(edtproduto_ep.Text)=False)
                      Then edtproduto_ep.Text:=''
                      Else Begin
                                ObjObjetosEntrada.Objferragem_ep.ferragemCor.ferragem.TabelaparaObjeto;
                                edtproduto_ep.Text:=ObjObjetosEntrada.Objferragem_ep.ferragemCor.ferragem.get_codigo;
                      End;
            End;
            ObjObjetosEntrada.Objferragem_ep.FerragemCor.EdtFerragemExit(sender,lbnomeproduto);
     End;
     2:Begin//KitBox
            if (Usouf9napesquisa=False)//digitou a referencia
            Then Begin
                      if (ObjObjetosEntrada.Objkitbox_ep.kitboxCor.kitbox.LocalizarReferencia(edtproduto_ep.Text)=False)
                      Then edtproduto_ep.Text:=''
                      Else Begin
                                ObjObjetosEntrada.Objkitbox_ep.kitboxCor.kitbox.TabelaparaObjeto;
                                edtproduto_ep.Text:=ObjObjetosEntrada.Objkitbox_ep.kitboxCor.kitbox.get_codigo;
                      End;
            End;
            ObjObjetosEntrada.Objkitbox_ep.KitBoxCor.EdtKitBoxExit(sender,lbnomeproduto);
     End;
     3:Begin//Perfilado
            if (Usouf9napesquisa=False)//digitou a referencia
            Then Begin
                      if (ObjObjetosEntrada.Objperfilado_ep.perfiladoCor.perfilado.LocalizaReferencia(edtproduto_ep.Text)=False)
                      Then edtproduto_ep.Text:=''
                      Else Begin
                                ObjObjetosEntrada.Objperfilado_ep.perfiladoCor.perfilado.TabelaparaObjeto;
                                edtproduto_ep.Text:=ObjObjetosEntrada.Objperfilado_ep.perfiladoCor.perfilado.get_codigo;
                      End;
            End;
            ObjObjetosEntrada.Objperfilado_ep.PerfiladoCor.EdtPerfiladoExit(sender,lbnomeproduto);
     End;
     4:Begin//Persiana
            if (Usouf9napesquisa=False)//digitou a referencia
            Then Begin
                      if (ObjObjetosEntrada.Objpersiana_ep.PersianaGrupoDiametroCor.persiana.LocalizaReferencia(edtproduto_ep.Text)=False)
                      Then edtproduto_ep.Text:=''
                      Else Begin
                                ObjObjetosEntrada.Objpersiana_ep.PersianaGrupoDiametroCor.persiana.TabelaparaObjeto;
                                edtproduto_ep.Text:=ObjObjetosEntrada.Objpersiana_ep.PersianaGrupoDiametroCor.persiana.get_codigo;
                      End;
            End;
            ObjObjetosEntrada.ObjPersiana_ep.PersianaGrupoDiametroCor.EdtPersianaExit(sender,lbnomeproduto);
     End;
     5:Begin//Vidro
            if (Usouf9napesquisa=False)//digitou a referencia
            Then Begin
                      if (ObjObjetosEntrada.Objvidro_ep.vidroCor.vidro.LocalizaReferencia(edtproduto_ep.Text)=False)
                      Then edtproduto_ep.Text:=''
                      Else Begin
                                ObjObjetosEntrada.Objvidro_ep.vidroCor.vidro.TabelaparaObjeto;
                                edtproduto_ep.Text:=ObjObjetosEntrada.Objvidro_ep.vidroCor.vidro.get_codigo;
                      End;
            End;
            ObjObjetosEntrada.Objvidro_ep.VidroCor.EdtVidroExit(sender,lbnomeproduto);
     End;
  End;
  Usouf9napesquisa:=False;

end;

procedure TFENTRADAPRODUTOS.cbb_epChange(Sender: TObject);
begin
     edtproduto_ep.text:='';
     if (cbb_ep.ItemIndex=4)//persiana
     then begin
               PnlDadosPersiana.visible:=true;
               limpaedit(PnlDadosPersiana);
     End
     Else Begin
               PnlDadosPersiana.visible:=False;
               limpaedit(PnlDadosPersiana);
     End;
end;

procedure TFENTRADAPRODUTOS.edtcor_epKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
     ObjObjetosEntrada.ObjDiverso_ep.DiversoCor.Cor.EdtCorKeyDown(sender,key,shift,lbnomecor,edtproduto_ep.text,cbb_ep.text);
end;

procedure TFENTRADAPRODUTOS.edtcor_epExit(Sender: TObject);
begin
ObjObjetosEntrada.ObjDiverso_ep.DiversoCor.EdtCorExit(sender,lbnomecor);
end;

procedure TFENTRADAPRODUTOS.edtgrupopersiana_EPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjObjetosEntrada.ObjPersiana_ep.PersianaGrupoDiametroCor.EdtGrupoPersianaKeyDown(sender,key,shift,lbnomegrupo,edtproduto_ep.text,edtcor_ep.text);
end;

procedure TFENTRADAPRODUTOS.edtgrupopersiana_EPExit(Sender: TObject);
begin
    ObjObjetosEntrada.ObjPersiana_ep.PersianaGrupoDiametroCor.EdtGrupoPersianaExit(sender,lbnomegrupo);
end;

procedure TFENTRADAPRODUTOS.edtdiametropersiana_EPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjObjetosEntrada.ObjPersiana_ep.PersianaGrupoDiametroCor.EdtDiametroKeyDown(sender,key,shift,lbnomediametro,edtproduto_ep.text,edtcor_ep.text,edtgrupopersiana_EP.text);
end;

procedure TFENTRADAPRODUTOS.edtdiametropersiana_EPExit(Sender: TObject);
begin
    ObjObjetosEntrada.ObjPersiana_ep.PersianaGrupoDiametroCor.EdtDiametroExit(sender,lbnomediametro);
    if LbNomeDiametro.caption<>''
    Then LbNomeDiametro.caption:=LbNomeDiametro.caption+' mm'; 
end;

procedure TFENTRADAPRODUTOS.BtCancelar_EPClick(Sender: TObject);
var
pcodigocombo:integer;
begin
     PcodigoCombo:=cbb_ep.ItemIndex;
     limpaedit(pnl1);
     limpaedit(PnlDadosPersiana);
     lbnomeproduto.caption:='';
     lbProdutoXml.Caption:='';
     lbnomecor.caption:='';
     Lbnomegrupo.caption:='';
     LbNomeDiametro.caption:='';
     edtproduto_ep.setfocus;
     cbb_ep.ItemIndex:=PcodigoCombo;
     cbb_epChange(sender);
     limpaCamposProduto;
end;

function TFENTRADAPRODUTOS.ValidaUnidadeMedida:Boolean;
begin

end;

procedure TFENTRADAPRODUTOS.btGravar_EPClick(Sender: TObject);
var
PcodigoCombo:Integer;
pcodigoprimario,Pcodigo:string;
ExcetoLinha:integer;
begin

    (*CODIFICA��O DOS MATERIAS  TABELA TABMATERIAISENTRADA}
    Projeto=''
    Ferragem=1
    Perfialdo=2
    vidro=3
    kitbox=4
    persiana=5
    diverso=6*)

  if utilizaSpedFiscal then
    if not validaSpedItens then
      Exit;

  if (edtcodigo_ep.text = '') or (edtcodigo_ep.text = '0') then
    ExcetoLinha := -1
  Else
    ExcetoLinha:=StrGridStrgProdutos.Row;

  PcodigoCombo:=cbb_ep.ItemIndex;

  if Trim(edtproduto_ep.Text) = ''  then
  begin
    MensagemAviso('Selecione um item');
    edtproduto_ep.SetFocus;
    exit;
  end;

  if trim(edtcor_ep.Text) = '' then
  begin
    MensagemAviso('Selecione uma Cor');
    edtcor_ep.SetFocus;
    exit;
  end;

  if (StrToCurrDef(edtPercentualPis.Text,0) > 0) then
    if (edtCstPis.Text = '') then
    begin
      MensagemAviso('CST PIS n�o pode estar vazio');
      edtCstPis.SetFocus;
      Exit;
    end;

  if (StrToCurrDef(edtPercentualCofins.Text,0) > 0) then
    if (edtCstCofins.Text = '') then
    begin
      MensagemAviso('CST COFINS n�o pode estar vazio');
      edtCstCofins.SetFocus;
      Exit;
    end;

  if (StrToCurrDef(edtcreditoipi_produtos.Text,0) > 0) then
    if (edtCstIPI.Text = '') then
    begin
      MensagemAviso('CST IPI n�o pode estar vazio');
      edtCstIPI.SetFocus;
      Exit;
    end;

  if (StrToCurrDef(edtvalor_ep.Text,0) = 0) then
  begin
    MensagemAviso('Valor deve ser maior que Zero');
    edtvalor_ep.SetFocus;
    Exit;
  end;


  if (StrToCurrDef(edtquantidade_ep.Text,0) = 0) then
  begin
    MensagemAviso('Quantidade deve ser maior que Zero');
    edtquantidade_ep.SetFocus;
    Exit;
  end;

  if(VerificaPrecoTotalProdutos(ExcetoLinha)=True) then
  begin
     MensagemAviso('O "Valor total dos produtos" inserido difere do valor total dos produtos informado no cabe�alho da nota');
     Exit;
  end;


 try

  exec_sql('update tabentradaprodutos set spedvalido=''N'' where codigo =  '+edtCodigo.Text);
  FDataModulo.IBTransaction.CommitRetaining;
  setImgVerificado(0);

  Case cbb_ep.ItemIndex of                           
     0:Begin//diversos
            With ObjObjetosEntrada.ObjDiverso_ep do
            Begin
                 ZerarTabela;

                 if (DiversoCor.Localiza_Diverso_Cor(edtproduto_ep.Text,edtcor_ep.text)=False)
                 Then Begin
                           Messagedlg('N�o foi Encontrado esse Diverso nessa cor!',mterror,[mbok],0);
                           exit;
                 End;

                 DiversoCor.TabelaparaObjeto;
                 PCodigoPrimario:=DiversoCor.get_codigo;

                 status:=dsInactive;
                 if (edtcodigo_ep.text='')
                 or (edtcodigo_ep.text='0')
                 then begin
                          Status:=dsinsert;
                          Submit_Codigo('0');
                          Submit_OrdemInsercao('0');
                 End
                 Else Begin
                           status:=dsedit;
                           Submit_codigo(edtcodigo_ep.text);
                           Submit_OrdemInsercao(edtordeminsercao_ep.Text);
                 End;

                 Submit_Quantidade(edtquantidade_ep.Text);
                 Submit_Valor(edtvalor_ep.text);
                 Entrada.Submit_CODIGO(EdtCODIGO.Text);

                 Submit_CreditoIpi(edtcreditoipi_produtos.Text);
                 Submit_IPIpago(edtIPIpago_produtos.Text);
                 Submit_CreditoICMS(edtcreditoicms_produtos.Text);
                 Submit_CreditoPIS(edtcreditopis_produtos.Text);
                 Submit_CreditoCOFINS(edtcreditocofins_produtos.Text);
                 CFOP.Submit_CODIGO(edtEdtCFOPProduto.Text);
                 CSOSN.Submit_codigo(edtCSOSN.Text);
                 STA.Submit_CODIGO(edtEdtST_A.Text);
                 STB.Submit_CODIGO(edtEdtST_B.Text);
                 Submit_Desconto(virgulaparaponto(edtdesconto.Text) );
                 Submit_Unidade(edtEdtUnidade.Text);
                 Submit_BCIcms(edtBaseCalculoICMS_p.text);
                 Submit_ValorIcms(edtValorICMS_p.Text);
                 Submit_ValorFrete(edtValorFreteProduto.Text);
                 Submit_ValorIpi(edtValorIPI_p.Text);
                 Submit_RedBaseIcms(edtPorcentagemReducao.Text);
                 submit_ppis(edtPercentualPis.Text);
                 submit_pcofins(edtPercentualCofins.Text);
                 submit_valoroutros(edtValorOutros.Text);

                 submit_aliquotast(edtAliquotaST.Text);
                 submit_bcicmsst(edtBCICMSST.Text);
                 submit_valoricmsst(edtValorICMSST.Text);

                 submit_cstpis(edtCstPis.Text);
                 submit_cstcofins(edtCstCofins.Text);
                 submit_cstipi(edtCstIPI.Text);
                 submit_bcipi(edtBCIPI.Text);

                 if(DiversoCor.Diverso.Get_Unidade <>edtEdtUnidade.Text) then
                 begin
                      //Fazendo a convers�o de unidades
                      MensagemAviso('A unidade de medida informada na entrada difere da unidade de medida '+
                      'informada no cadastro de diverso, ser� realizada a convers�o automatica de '+
                      edtEdtUnidade.Text + ' para '+DiversoCor.Diverso.Get_Unidade);

                      FAcertoUnidadeEntrada:=TFAcertoUnidadeEntrada.Create(nil);
                      try
                         FAcertoUnidadeEntrada.PassaObjetos(DiversoCor.Diverso.Get_Codigo,edtEdtUnidade.Text,diversocor.Diverso.Get_Unidade,'Diverso',edtquantidade_ep.text,'');
                         FAcertoUnidadeEntrada.ShowModal;

                         if(FAcertoUnidadeEntrada.tag=0)
                         then Exit;

                         Submit_UnidadeConversao(DiversoCor.Diverso.Get_Unidade);
                         Submit_QuantidadeConversao(CurrToStr(FAcertoUnidadeEntrada.QuantidadeConvertida));

                      finally
                          FreeAndNil(FAcertoUnidadeEntrada);
                      end;

                 end;

                 //Aqui Salva e aumenta o estoque pra esse material
                 if (Salvar(True)=False)
                 Then Begin
                           edtproduto_ep.SetFocus;
                           messagedlg('N�o foi poss�vel Gravar',mterror,[mbok],0);
                           exit;
                 End;

                 Pcodigo:=DiversoCor.diverso.Get_Codigo;

                 if (PalteraCusto=True)
                 Then ObjObjetosEntrada.AtualizaCusto('TABDIVERSO',pcodigo,edtvalor_ep.Text);

                 if (PMostraTelaAlteracaoPreco=True)
                 Then Begin
                           With FalteraPrecoDiverso do
                           Begin
                               lbPrecoPago.caption:=formata_valor(edtvalor_ep.Text);
                               ObjdiversoCor:=DiversoCor;
                               EdtPrecoCusto.Text:=DiversoCor.Diverso.Get_PRECOCUSTO;
                               EdtPorcentagemInstalado.Text:=DiversoCor.Diverso.Get_PORCENTAGEMINSTALADO;
                               EdtPorcentagemFornecido.Text:=DiversoCor.Diverso.Get_PORCENTAGEMFORNECIDO;
                               EdtPorcentagemRetirado.Text:=DiversoCor.Diverso.Get_PORCENTAGEMRETIRADO;
                               EdtPrecoVendaInstalado.Text:=DiversoCor.Diverso.Get_PRECOVENDAINSTALADO;
                               EdtPrecoVendaFornecido.Text:=DiversoCor.Diverso.Get_PRECOVENDAFORNECIDO;
                               EdtPrecoVendaRetirado.Text:=DiversoCor.Diverso.Get_PRECOVENDARETIRADO ;

                               EdtPorcentagemAcrescimo.text:=DiversoCor.Get_PorcentagemAcrescimo;
                               EdtAcrescimoExtra.text:=DiversoCor.Get_AcrescimoExtra;
                               EdtPorcentagemAcrescimoFinal.text:=DiversoCor.Get_PorcentagemAcrescimoFinal;

                               edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               

                               showmodal;

                               if (tag=0)
                               Then exit;

                               ObjdiversoCor.LocalizaCodigo(pcodigoprimario);
                               ObjdiversoCor.TabelaparaObjeto;
                               ObjdiversoCor.Diverso.Status:=dsEdit;
                               ObjdiversoCor.Diverso.Submit_PrecoCusto(tira_ponto(edtPrecoCusto.text));
                               ObjdiversoCor.Diverso.Submit_PORCENTAGEMINSTALADO(tira_ponto(EdtPorcentagemInstalado.Text));
                               ObjdiversoCor.Diverso.Submit_PORCENTAGEMFORNECIDO(tira_ponto(EdtPorcentagemFornecido.Text));
                               ObjdiversoCor.Diverso.Submit_PORCENTAGEMRETIRADO(tira_ponto(EdtPorcentagemRetirado.Text));
                               ObjdiversoCor.Diverso.Submit_precovendaINSTALADO(tira_ponto(EdtprecovendaInstalado.Text));
                               ObjdiversoCor.Diverso.Submit_precovendaFORNECIDO(tira_ponto(EdtprecovendaFornecido.Text));
                               ObjdiversoCor.Diverso.Submit_precovendaRETIRADO(tira_ponto(EdtprecovendaRetirado.Text));
                               
                               if (ObjdiversoCor.Diverso.Salvar(true)=False)
                               Then Begin
                                         MensagemErro('Erro na tentativa de alterar os valores de custo e pre�o de venda');
                                         exit;
                               End;


                               ObjdiversoCor.Status:=dsEdit;
                               ObjdiversoCor.Submit_PorcentagemAcrescimo(EdtPorcentagemAcrescimo.Text);
                               ObjdiversoCor.Submit_AcrescimoExtra(edtAcrescimoExtra.text);

                               if (ObjdiversoCor.Salvar(True)=False)
                               Then Begin
                                         MensagemErro('Erro na tentativa de gravar a porcentagem de acr�scimo para a cor');
                                         exit;
                               End;


                           End;
                 End;

                 PageControl1.TabIndex := 0;
                 Self.Atualizagrid;
                 BtCancelar_EPClick(sender);
                 edtproduto_ep.SetFocus;
            End;
     End;
     1:Begin//Ferragem
            With ObjObjetosEntrada.Objferragem_ep do
            Begin
                 ZerarTabela;
                 if (FerragemCor.Localiza_Ferragem_Cor(edtproduto_ep.Text,edtcor_ep.text)=False)
                 Then Begin
                           Messagedlg('N�o foi Encontrado esse Diverso nessa cor!',mterror,[mbok],0);
                           exit;
                 End;
                 FerragemCor.TabelaparaObjeto;
                 PCodigoPrimario:=FerragemCor.get_codigo;
                 
                 status:=dsInactive;
                 if (edtcodigo_ep.text='')
                 or (edtcodigo_ep.text='0')
                 then begin
                          Status:=dsinsert;
                          Submit_Codigo('0');
                          Submit_OrdemInsercao('0');
                 End
                 Else Begin
                           status:=dsedit;
                           Submit_codigo(edtcodigo_ep.text);
                           Submit_OrdemInsercao(edtordeminsercao_ep.Text);
                 End;

                 Submit_Quantidade(edtquantidade_ep.Text);
                 Submit_Valor(edtvalor_ep.text);;
                 Entrada.Submit_CODIGO(EdtCODIGO.Text);

                 Submit_CreditoIpi(edtcreditoipi_produtos.Text);
                 Submit_IPIpago(edtIPIpago_produtos.Text);
                 Submit_CreditoICMS(edtcreditoicms_produtos.Text);
                 Submit_CreditoPIS(edtcreditopis_produtos.Text);
                 Submit_CreditoCOFINS(edtcreditocofins_produtos.Text);
                 CFOP.Submit_CODIGO(edtEdtCFOPProduto.Text);
                 CSOSN.Submit_codigo(edtCSOSN.Text);
                 STA.Submit_CODIGO(edtEdtST_A.Text);
                 STB.Submit_CODIGO(edtEdtST_B.text);
                 Submit_Desconto(virgulaparaponto(edtdesconto.text));
                 Submit_Unidade(edtEdtUnidade.Text);
                 Submit_BCIcms(edtBaseCalculoICMS_p.text);
                 Submit_ValorIcms(edtValorICMS_p.Text);
                 Submit_ValorFrete(edtValorFreteProduto.Text);
                 Submit_ValorIpi(edtValorIPI_p.Text);
                 Submit_RedBaseIcms(edtPorcentagemReducao.Text);
                 submit_ppis(edtPercentualPis.Text);
                 submit_pcofins(edtPercentualCofins.Text);
                 submit_valoroutros(edtValorOutros.Text);

                 submit_aliquotast(edtAliquotaST.Text);
                 submit_bcicmsst(edtBCICMSST.Text);
                 submit_valoricmsst(edtValorICMSST.Text);

                 submit_cstpis(edtCstPis.Text);
                 submit_cstcofins(edtCstCofins.Text);
                 submit_cstipi(edtCstIPI.Text);
                 submit_bcipi(edtBCIPI.Text);

                 if(FerragemCor.Ferragem.Get_Unidade <>edtEdtUnidade.Text) then
                 begin
                      //Fazendo a convers�o de unidades
                      MensagemAviso('A unidade de medida informada na entrada difere da unidade de medida '+
                      'informada no cadastro de Ferragens, ser� realizada a convers�o automatica de '+
                      edtEdtUnidade.Text + ' para '+FerragemCor.Ferragem.Get_Unidade);

                      FAcertoUnidadeEntrada:=TFAcertoUnidadeEntrada.Create(nil);
                      try
                         if(edtEdtUnidade.Text='KG')
                         then FAcertoUnidadeEntrada.PassaObjetos(FerragemCor.Ferragem.Get_Codigo,edtEdtUnidade.Text,FerragemCor.Ferragem.Get_Unidade,'Ferragem',edtquantidade_ep.text,FerragemCor.Ferragem.Get_Peso)
                         else FAcertoUnidadeEntrada.PassaObjetos(FerragemCor.Ferragem.Get_Codigo,edtEdtUnidade.Text,FerragemCor.Ferragem.Get_Unidade,'Ferragem',edtquantidade_ep.text,'');
                         FAcertoUnidadeEntrada.ShowModal;

                         if(FAcertoUnidadeEntrada.tag=0)
                         then Exit;

                         Submit_UnidadeConversao(FerragemCor.Ferragem.Get_Unidade);
                         Submit_Quantidade(edtquantidade_ep.Text);
                         Submit_QuantidadeConversao(CurrToStr(FAcertoUnidadeEntrada.QuantidadeConvertida));
                      finally
                          FreeAndNil(FAcertoUnidadeEntrada);
                      end;

                 end;



                 if (Salvar(True)=False)
                 Then Begin
                           edtproduto_ep.SetFocus;
                           messagedlg('N�o foi poss�vel Gravar',mterror,[mbok],0);
                           exit;
                 End;
                 Pcodigo:=FerragemCor.Ferragem.Get_Codigo;

                 if (PalteraCusto=True)
                 Then ObjObjetosEntrada.AtualizaCusto('TABFERRAGEM',pcodigo,edtvalor_ep.Text);

                 if (PMostraTelaAlteracaoPreco=True)
                 Then Begin
                           With FalteraPrecoferragem do
                           Begin
                               lbPrecoPago.caption:=formata_valor(edtvalor_ep.Text);
                               ObjferragemCor:=ferragemCor;
                               EdtPrecoCusto.Text:=ferragemCor.ferragem.Get_PRECOCUSTO;
                               EdtPorcentagemInstalado.Text:=ferragemCor.ferragem.Get_PORCENTAGEMINSTALADO;
                               EdtPorcentagemFornecido.Text:=ferragemCor.ferragem.Get_PORCENTAGEMFORNECIDO;
                               EdtPorcentagemRetirado.Text:=ferragemCor.ferragem.Get_PORCENTAGEMRETIRADO;
                               EdtPrecoVendaInstalado.Text:=ferragemCor.ferragem.Get_PRECOVENDAINSTALADO;
                               EdtPrecoVendaFornecido.Text:=ferragemCor.ferragem.Get_PRECOVENDAFORNECIDO;
                               EdtPrecoVendaRetirado.Text:=ferragemCor.ferragem.Get_PRECOVENDARETIRADO ;

                               EdtPorcentagemAcrescimo.text:=ferragemCor.Get_PorcentagemAcrescimo;
                               EdtAcrescimoExtra.text:=ferragemCor.Get_AcrescimoExtra;
                               EdtPorcentagemAcrescimoFinal.text:=ferragemCor.Get_PorcentagemAcrescimoFinal;

                               edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               
                               
                               showmodal;

                               if (tag=0)
                               Then exit;

                               ObjferragemCor.LocalizaCodigo(pcodigoprimario);
                               ObjferragemCor.TabelaparaObjeto;
                               ObjferragemCor.ferragem.Status:=dsEdit;
                               ObjferragemCor.ferragem.Submit_PrecoCusto(tira_ponto(edtPrecoCusto.text));
                               ObjferragemCor.ferragem.Submit_PORCENTAGEMINSTALADO(tira_ponto(EdtPorcentagemInstalado.Text));
                               ObjferragemCor.ferragem.Submit_PORCENTAGEMFORNECIDO(tira_ponto(EdtPorcentagemFornecido.Text));
                               ObjferragemCor.ferragem.Submit_PORCENTAGEMRETIRADO(tira_ponto(EdtPorcentagemRetirado.Text));
                               ObjferragemCor.ferragem.Submit_precovendaINSTALADO(tira_ponto(EdtprecovendaInstalado.Text));
                               ObjferragemCor.ferragem.Submit_precovendaFORNECIDO(tira_ponto(EdtprecovendaFornecido.Text));
                               ObjferragemCor.ferragem.Submit_precovendaRETIRADO(tira_ponto(EdtprecovendaRetirado.Text));
                               if (ObjferragemCor.ferragem.Salvar(true)=False)
                               Then Begin
                                         MensagemErro('Erro na tentativa de alterar os valores de custo e pre�o de venda');
                                         exit;
                               End;

                               ObjferragemCor.Status:=dsEdit;
                               ObjferragemCor.Submit_PorcentagemAcrescimo(EdtPorcentagemAcrescimo.Text);
                               ObjferragemCor.Submit_AcrescimoExtra(edtAcrescimoExtra.text);
                               ObjferragemCor.Submit_ValorExtra(EdtValorExtra.Text);
                               ObjferragemCor.Submit_ValorFinal(EdtValorFinal.Text);

                               if (ObjferragemCor.Salvar(True)=False)
                               Then Begin
                                         MensagemErro('Erro na tentativa de gravar a porcentagem de acr�scimo para a cor');
                                         exit;
                               End;


                           End;
                 End;

                 PageControl1.TabIndex := 0;
                 Self.Atualizagrid;
                 BtCancelar_EPClick(sender);
                 edtproduto_ep.SetFocus;
            End;
     End;
     2:Begin//KitBox
            With ObjObjetosEntrada.Objkitbox_ep do
            Begin
                 ZerarTabela;
                 if (KitBoxCor.Localiza_kitBox_Cor(edtproduto_ep.Text,edtcor_ep.text)=False)
                 Then Begin
                           Messagedlg('N�o foi Encontrado esse KitBox nessa cor!',mterror,[mbok],0);
                           exit;
                 End;
                 KitBoxCor.TabelaparaObjeto;
                 PCodigoPrimario:=KitBoxCor.get_codigo;

                 status:=dsInactive;
                 if (edtcodigo_ep.text='')
                 or (edtcodigo_ep.text='0')
                 then begin
                          Status:=dsinsert;
                          Submit_Codigo('0');
                          Submit_OrdemInsercao('0');
                 End
                 Else Begin
                           status:=dsedit;
                           Submit_codigo(edtcodigo_ep.text);
                           Submit_OrdemInsercao(edtordeminsercao_ep.Text);
                 End;


                 Submit_Quantidade(edtquantidade_ep.Text);
                 Submit_Valor(edtvalor_ep.text);
                 Entrada.Submit_CODIGO(EdtCODIGO.Text);

                 Submit_CreditoIpi(edtcreditoipi_produtos.Text);
                 Submit_IPIpago(edtIPIpago_produtos.Text);
                 Submit_CreditoICMS(edtcreditoicms_produtos.Text);
                 Submit_CreditoPIS(edtcreditopis_produtos.Text);
                 Submit_CreditoCOFINS(edtcreditocofins_produtos.Text);
                 CFOP.Submit_CODIGO(edtEdtCFOPProduto.Text);
                 CSOSN.Submit_codigo(edtCSOSN.Text);
                 STA.Submit_CODIGO(edtEdtST_A.Text);
                 STB.Submit_CODIGO(edtEdtST_B.text);
                 Submit_Desconto(virgulaparaponto(edtdesconto.text));
                 Submit_Unidade(edtEdtUnidade.Text);
                 Submit_BCIcms(edtBaseCalculoICMS_p.text);
                 Submit_ValorIcms(edtValorICMS_p.Text);
                 Submit_ValorFrete(edtValorFreteProduto.Text);
                 Submit_ValorIpi(edtValorIPI_p.Text);
                 Submit_RedBaseIcms(edtPorcentagemReducao.Text);
                 submit_ppis(edtPercentualPis.Text);
                 submit_pcofins(edtPercentualCofins.Text);
                 submit_valoroutros(edtValorOutros.Text);

                 submit_aliquotast(edtAliquotaST.Text);
                 submit_bcicmsst(edtBCICMSST.Text);
                 submit_valoricmsst(edtValorICMSST.Text);

                 submit_cstpis(edtCstPis.Text);
                 submit_cstcofins(edtCstCofins.Text);
                 submit_cstipi(edtCstIPI.Text);
                 submit_bcipi(edtBCIPI.Text);

                 if(KitBoxCor.KitBox.Get_Unidade <>edtEdtUnidade.Text) then
                 begin
                      //Fazendo a convers�o de unidades
                      MensagemAviso('A unidade de medida informada na entrada difere da unidade de medida '+
                      'informada no cadastro de KitBox, ser� realizada a convers�o automatica de '+
                      edtEdtUnidade.Text + ' para '+KitBoxCor.KitBox.Get_Unidade);

                      FAcertoUnidadeEntrada:=TFAcertoUnidadeEntrada.Create(nil);
                      try
                         if(edtEdtUnidade.Text='KG')
                         then FAcertoUnidadeEntrada.PassaObjetos(KitBoxCor.KitBox.Get_Codigo,edtEdtUnidade.Text,KitBoxCor.KitBox.Get_Unidade,'KitBox',edtquantidade_ep.text,KitBoxCor.KitBox.Get_Peso)
                         else FAcertoUnidadeEntrada.PassaObjetos(KitBoxCor.KitBox.Get_Codigo,edtEdtUnidade.Text,KitBoxCor.KitBox.Get_Unidade,'KitBox',edtquantidade_ep.text,'');


                         FAcertoUnidadeEntrada.ShowModal;

                         if(FAcertoUnidadeEntrada.tag=0)
                         then Exit;

                         Submit_UnidadeConversao(KitBoxCor.KitBox.Get_Unidade);
                         Submit_Quantidade(edtquantidade_ep.Text);
                         Submit_QuantidadeConversao(CurrToStr(FAcertoUnidadeEntrada.QuantidadeConvertida));

                      finally
                          FreeAndNil(FAcertoUnidadeEntrada);
                      end;

                 end;

                 if (Salvar(True)=False)
                 Then Begin
                           edtproduto_ep.SetFocus;
                           messagedlg('N�o foi poss�vel Gravar',mterror,[mbok],0);
                           exit;
                 End;
                 Pcodigo:=KitBoxCor.KitBox.Get_Codigo;

                 if (PalteraCusto=True)
                 Then Begin
                         if (PConsideraSomentePinturaFosca=True)
                         Then Begin
                                 //segundo camila (28/08) somente o pintura fosco deve ser considerado
                                 //para atualizar o custo
                                 if (KitBoxCor.Cor.Get_Descricao='PINTURA FOSCA')
                                 Then ObjObjetosEntrada.AtualizaCusto('TABKITBOX',pcodigo,edtvalor_ep.Text);
                         End
                         else ObjObjetosEntrada.AtualizaCusto('TABKITBOX',pcodigo,edtvalor_ep.Text);
                 End;

                 if (PMostraTelaAlteracaoPreco=True)
                 Then Begin
                           With FalteraPrecokitbox do
                           Begin
                               lbPrecoPago.caption:=formata_valor(edtvalor_ep.Text);
                               ObjkitboxCor:=kitboxCor;
                               EdtPrecoCusto.Text:=kitboxCor.kitbox.Get_PRECOCUSTO;
                               EdtPorcentagemInstalado.Text:=kitboxCor.kitbox.Get_PORCENTAGEMINSTALADO;
                               EdtPorcentagemFornecido.Text:=kitboxCor.kitbox.Get_PORCENTAGEMFORNECIDO;
                               EdtPorcentagemRetirado.Text:=kitboxCor.kitbox.Get_PORCENTAGEMRETIRADO;
                               EdtPrecoVendaInstalado.Text:=kitboxCor.kitbox.Get_PRECOVENDAINSTALADO;
                               EdtPrecoVendaFornecido.Text:=kitboxCor.kitbox.Get_PRECOVENDAFORNECIDO;
                               EdtPrecoVendaRetirado.Text:=kitboxCor.kitbox.Get_PRECOVENDARETIRADO ;

                               EdtPorcentagemAcrescimo.text:=kitboxCor.Get_PorcentagemAcrescimo;
                               EdtAcrescimoExtra.text:=kitboxCor.Get_AcrescimoExtra;
                               EdtPorcentagemAcrescimoFinal.text:=kitboxCor.Get_PorcentagemAcrescimoFinal;

                               edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               
                               
                               showmodal;

                               if (tag=0)
                               Then exit;

                               ObjkitboxCor.LocalizaCodigo(pcodigoprimario);
                               ObjkitboxCor.TabelaparaObjeto;
                               ObjkitboxCor.kitbox.Status:=dsEdit;
                               ObjkitboxCor.kitbox.Submit_PrecoCusto(tira_ponto(edtPrecoCusto.text));
                               ObjkitboxCor.kitbox.Submit_PORCENTAGEMINSTALADO(tira_ponto(EdtPorcentagemInstalado.Text));
                               ObjkitboxCor.kitbox.Submit_PORCENTAGEMFORNECIDO(tira_ponto(EdtPorcentagemFornecido.Text));
                               ObjkitboxCor.kitbox.Submit_PORCENTAGEMRETIRADO(tira_ponto(EdtPorcentagemRetirado.Text));
                               ObjkitboxCor.kitbox.Submit_precovendaINSTALADO(tira_ponto(EdtprecovendaInstalado.Text));
                               ObjkitboxCor.kitbox.Submit_precovendaFORNECIDO(tira_ponto(EdtprecovendaFornecido.Text));
                               ObjkitboxCor.kitbox.Submit_precovendaRETIRADO(tira_ponto(EdtprecovendaRetirado.Text));
                               if (ObjkitboxCor.kitbox.Salvar(true)=False)
                               Then Begin
                                         MensagemErro('Erro na tentativa de alterar os valores de custo e pre�o de venda');
                                         exit;
                               End;

                               ObjkitboxCor.Status:=dsEdit;
                               ObjkitboxCor.Submit_PorcentagemAcrescimo(EdtPorcentagemAcrescimo.Text);
                               ObjkitboxCor.Submit_AcrescimoExtra(edtAcrescimoExtra.text);
                               ObjkitboxCor.Submit_ValorExtra(EdtValorExtra.Text);
                               ObjkitboxCor.Submit_ValorFinal(EdtValorFinal.Text);

                               if (ObjkitboxCor.Salvar(True)=False)
                               Then Begin
                                         MensagemErro('Erro na tentativa de gravar a porcentagem de acr�scimo para a cor');
                                         exit;
                               End;


                           End;
                 End;

                 PageControl1.TabIndex := 0;
                 Self.Atualizagrid;
                 BtCancelar_EPClick(sender);
                 edtproduto_ep.SetFocus;
            End;
     End;
     3:Begin//Perfilado
            With ObjObjetosEntrada.Objperfilado_ep do
            Begin
                 ZerarTabela;
                 if (PerfiladoCor.Localiza_Perfilado_Cor(edtproduto_ep.Text,edtcor_ep.text)=False)
                 Then Begin
                           Messagedlg('N�o foi Encontrado esse Perfilado nessa cor!',mterror,[mbok],0);
                           exit;
                 End;
                 PerfiladoCor.TabelaparaObjeto;
                 PCodigoPrimario:=perfiladoCor.get_codigo;

                 status:=dsInactive;
                 if (edtcodigo_ep.text='')
                 or (edtcodigo_ep.text='0')
                 then begin
                          Status:=dsinsert;
                          Submit_Codigo('0');
                          Submit_OrdemInsercao('0');
                 End
                 Else Begin
                           status:=dsedit;
                           Submit_codigo(edtcodigo_ep.text);
                           Submit_OrdemInsercao(edtordeminsercao_ep.Text);
                 End;


                 Submit_Quantidade(edtquantidade_ep.Text);
                 Submit_Valor(edtvalor_ep.text);
                 Entrada.Submit_CODIGO(EdtCODIGO.Text);

                 Submit_CreditoIpi(edtcreditoipi_produtos.Text);
                 Submit_IPIpago(edtIPIpago_produtos.Text);
                 Submit_CreditoICMS(edtcreditoicms_produtos.Text);
                 Submit_CreditoPIS(edtcreditopis_produtos.Text);
                 Submit_CreditoCOFINS(edtcreditocofins_produtos.Text);
                 CFOP.Submit_CODIGO(edtEdtCFOPProduto.Text);
                 CSOSN.Submit_codigo(edtCSOSN.Text);
                 STA.Submit_CODIGO(edtEdtST_A.Text);
                 STB.Submit_CODIGO(edtEdtST_B.text);
                 Submit_Desconto(virgulaparaponto(edtdesconto.text));
                 Submit_Unidade(edtEdtUnidade.Text);
                 Submit_BCIcms(edtBaseCalculoICMS_p.text);
                 Submit_ValorIcms(edtValorICMS_p.Text);
                 Submit_ValorFrete(edtValorFreteProduto.Text);
                 Submit_ValorIpi(edtValorIPI_p.Text);
                 Submit_RedBaseIcms(edtPorcentagemReducao.Text);
                 submit_ppis(edtPercentualPis.Text);
                 submit_pcofins(edtPercentualCofins.Text);
                 submit_valoroutros(edtValorOutros.Text);

                 submit_aliquotast(edtAliquotaST.Text);
                 submit_bcicmsst(edtBCICMSST.Text);
                 submit_valoricmsst(edtValorICMSST.Text);

                 submit_cstpis(edtCstPis.Text);
                 submit_cstcofins(edtCstCofins.Text);
                 submit_cstipi(edtCstIPI.Text);
                 submit_bcipi(edtBCIPI.Text);

                 if(PerfiladoCor.Perfilado.Get_Unidade <>edtEdtUnidade.Text) then
                 begin
                      //Fazendo a convers�o de unidades
                      MensagemAviso('A unidade de medida informada na entrada difere da unidade de medida '+
                      'informada no cadastro de Perfilado, ser� realizada a convers�o automatica de '+
                      edtEdtUnidade.Text + ' para '+PerfiladoCor.Perfilado.Get_Unidade);

                      FAcertoUnidadeEntrada:=TFAcertoUnidadeEntrada.Create(nil);
                      try
                         if(edtEdtUnidade.Text='KG')
                         then FAcertoUnidadeEntrada.PassaObjetos(PerfiladoCor.Perfilado.Get_Codigo,edtEdtUnidade.Text,PerfiladoCor.Perfilado.Get_Unidade,'Perfilado',edtquantidade_ep.text,PerfiladoCor.Perfilado.Get_Peso)
                         else FAcertoUnidadeEntrada.PassaObjetos(PerfiladoCor.Perfilado.Get_Codigo,edtEdtUnidade.Text,PerfiladoCor.Perfilado.Get_Unidade,'Perfilado',edtquantidade_ep.text,'');


                         FAcertoUnidadeEntrada.ShowModal;

                         if(FAcertoUnidadeEntrada.tag=0)
                         then Exit;

                         Submit_UnidadeConversao(PerfiladoCor.Perfilado.Get_Unidade);
                         Submit_Quantidade(edtquantidade_ep.Text);
                         Submit_QuantidadeConversao(CurrToStr(FAcertoUnidadeEntrada.QuantidadeConvertida));

                      finally
                          FreeAndNil(FAcertoUnidadeEntrada);
                      end;

                 end;

                 if (Salvar(True)=False)
                 Then Begin
                           edtproduto_ep.SetFocus;
                           messagedlg('N�o foi poss�vel Gravar',mterror,[mbok],0);
                           exit;
                 End;
                 Pcodigo:=PerfiladoCor.Perfilado.Get_Codigo;

                 if (PalteraCusto=True)
                 Then BEgin
                        if (PConsideraSomenteAnodizadoFosco=True)
                         Then Begin
                                 //Conforme Camila (28/08) somente o anodizado fosco pode ser levado em consideracao no custo
                                 if (perfiladocor.cor.Get_Descricao='ANODIZADO FOSCO')
                                 Then ObjObjetosEntrada.AtualizaCusto('TABPERFILADO',pcodigo,edtvalor_ep.Text);
                         End
                         else ObjObjetosEntrada.AtualizaCusto('TABPERFILADO',pcodigo,edtvalor_ep.Text);
                 End;

                 if (PMostraTelaAlteracaoPreco=True)
                 Then Begin
                           With FalteraPrecoperfilado do
                           Begin
                               lbPrecoPago.caption:=formata_valor(edtvalor_ep.Text);
                               ObjperfiladoCor:=perfiladoCor;
                               EdtPrecoCusto.Text:=perfiladoCor.perfilado.Get_PRECOCUSTO;
                               EdtPorcentagemInstalado.Text:=perfiladoCor.perfilado.Get_PORCENTAGEMINSTALADO;
                               EdtPorcentagemFornecido.Text:=perfiladoCor.perfilado.Get_PORCENTAGEMFORNECIDO;
                               EdtPorcentagemRetirado.Text:=perfiladoCor.perfilado.Get_PORCENTAGEMRETIRADO;
                               EdtPrecoVendaInstalado.Text:=perfiladoCor.perfilado.Get_PRECOVENDAINSTALADO;
                               EdtPrecoVendaFornecido.Text:=perfiladoCor.perfilado.Get_PRECOVENDAFORNECIDO;
                               EdtPrecoVendaRetirado.Text:=perfiladoCor.perfilado.Get_PRECOVENDARETIRADO ;

                               EdtPorcentagemAcrescimo.text:=perfiladoCor.Get_PorcentagemAcrescimo;
                               EdtAcrescimoExtra.text:=perfiladoCor.Get_AcrescimoExtra;
                               EdtPorcentagemAcrescimoFinal.text:=perfiladoCor.Get_PorcentagemAcrescimoFinal;

                               edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               
                               
                               showmodal;

                               if (tag=0)
                               Then exit;

                               ObjperfiladoCor.LocalizaCodigo(pcodigoprimario);
                               ObjperfiladoCor.TabelaparaObjeto;
                               ObjperfiladoCor.perfilado.Status:=dsEdit;
                               ObjperfiladoCor.perfilado.Submit_PrecoCusto(tira_ponto(edtPrecoCusto.text));
                               ObjperfiladoCor.perfilado.Submit_PORCENTAGEMINSTALADO(tira_ponto(EdtPorcentagemInstalado.Text));
                               ObjperfiladoCor.perfilado.Submit_PORCENTAGEMFORNECIDO(tira_ponto(EdtPorcentagemFornecido.Text));
                               ObjperfiladoCor.perfilado.Submit_PORCENTAGEMRETIRADO(tira_ponto(EdtPorcentagemRetirado.Text));
                               ObjperfiladoCor.perfilado.Submit_precovendaINSTALADO(tira_ponto(EdtprecovendaInstalado.Text));
                               ObjperfiladoCor.perfilado.Submit_precovendaFORNECIDO(tira_ponto(EdtprecovendaFornecido.Text));
                               ObjperfiladoCor.perfilado.Submit_precovendaRETIRADO(tira_ponto(EdtprecovendaRetirado.Text));
                               if (ObjperfiladoCor.perfilado.Salvar(true)=False)
                               Then Begin
                                         MensagemErro('Erro na tentativa de alterar os valores de custo e pre�o de venda');
                                         exit;
                               End;

                               ObjperfiladoCor.Status:=dsEdit;
                               ObjperfiladoCor.Submit_PorcentagemAcrescimo(EdtPorcentagemAcrescimo.Text);
                               ObjperfiladoCor.Submit_AcrescimoExtra(edtAcrescimoExtra.text);
                               ObjperfiladoCor.Submit_ValorExtra(EdtValorExtra.Text);
                               ObjperfiladoCor.Submit_ValorFinal(EdtValorFinal.Text);

                               if (ObjperfiladoCor.Salvar(True)=False)
                               Then Begin
                                         MensagemErro('Erro na tentativa de gravar a porcentagem de acr�scimo para a cor');
                                         exit;
                               End;


                           End;
                 End;

                 PageControl1.TabIndex := 0;
                 Self.Atualizagrid;
                 BtCancelar_EPClick(sender);
                 edtproduto_ep.SetFocus;
            End;
     End;

     4:Begin//Persiana
            With ObjObjetosEntrada.ObjPersiana_ep do
            Begin
                 ZerarTabela;
                 if (PersianaGrupoDiametroCor.Localiza_persiana_grupo_diametro_Cor(edtproduto_ep.Text,edtgrupopersiana_EP.text,edtdiametropersiana_EP.text,edtcor_ep.text)=False)
                 Then Begin
                           Messagedlg('N�o foi Encontrado essa Persiana com essas caracter�sticas de cor, grupo e di�metro!',mterror,[mbok],0);
                           exit;
                 End;
                 PersianaGrupoDiametroCor.TabelaparaObjeto;
                 PCodigoPrimario:=PersianaGrupoDiametroCor.get_codigo;

                 status:=dsInactive;
                 if (edtcodigo_ep.text='')
                 or (edtcodigo_ep.text='0')
                 then begin
                          Status:=dsinsert;
                          Submit_Codigo('0');
                          Submit_OrdemInsercao('0');
                 End
                 Else Begin
                           status:=dsedit;
                           Submit_codigo(edtcodigo_ep.text);
                           Submit_OrdemInsercao(edtordeminsercao_ep.Text);
                 End;


                 Submit_Quantidade(edtquantidade_ep.Text);
                 Submit_Valor(edtvalor_ep.text);
                 Entrada.Submit_CODIGO(EdtCODIGO.Text);

                 Submit_CreditoIpi(edtcreditoipi_produtos.Text);
                 Submit_IPIpago(edtIPIpago_produtos.Text);
                 Submit_CreditoICMS(edtcreditoicms_produtos.Text);
                 Submit_CreditoPIS(edtcreditopis_produtos.Text);
                 Submit_CreditoCOFINS(edtcreditocofins_produtos.Text);
                 CFOP.Submit_CODIGO(edtEdtCFOPProduto.Text);
                 CSOSN.Submit_codigo(edtCSOSN.Text);
                 STA.Submit_CODIGO(edtEdtST_A.text);
                 STB.Submit_CODIGO(edtEdtST_B.Text);
                 Submit_Desconto(virgulaparaponto(edtdesconto.Text));
                 Submit_Unidade(edtEdtUnidade.Text);
                 CFOP.Submit_CODIGO(edtEdtCFOPProduto.Text);
                 CSOSN.Submit_codigo(edtCSOSN.Text);
                 STA.Submit_CODIGO(edtEdtST_A.Text);
                 STB.Submit_CODIGO(edtEdtST_B.text);
                 Submit_Unidade(edtEdtUnidade.Text);
                 Submit_BCIcms(edtBaseCalculoICMS_p.text);
                 Submit_ValorIcms(edtValorICMS_p.Text);
                 Submit_ValorFrete(edtValorFreteProduto.Text);
                 Submit_ValorIpi(edtValorIPI_p.Text);
                 Submit_RedBaseIcms(edtPorcentagemReducao.Text);
                 submit_ppis(edtPercentualPis.Text);
                 submit_pcofins(edtPercentualCofins.Text);
                 submit_valoroutros(edtValorOutros.Text);

                 submit_aliquotast(edtAliquotaST.Text);
                 submit_bcicmsst(edtBCICMSST.Text);
                 submit_valoricmsst(edtValorICMSST.Text);

                 submit_cstpis(edtCstPis.Text);
                 submit_cstcofins(edtCstCofins.Text);
                 submit_cstipi(edtCstIPI.Text);
                 submit_bcipi(edtBCIPI.Text);

                 //Estudar o caso de Persianas
                 {if( <>edtEdtUnidade.Text) then
                 begin
                      //Fazendo a convers�o de unidades
                      MensagemAviso('A unidade de medida informada na entrada difere da unidade de medida '+
                      'informada no cadastro de Persiana, ser� realizada a convers�o automatica de '+
                      edtEdtUnidade.Text + ' para '+PerfiladoCor.Perfilado.Get_Unidade);

                      FAcertoUnidadeEntrada:=TFAcertoUnidadeEntrada.Create(nil);
                      try
                         FAcertoUnidadeEntrada.PassaObjetos(PersianaGrupoDiametroCor.Persiana.Get_Codigo,edtEdtUnidade.Text,PersianaGrupoDiametroCor.Persiana.Get_Unidade,'Persiana',edtquantidade_ep.text);
                         FAcertoUnidadeEntrada.ShowModal;

                         if(FAcertoUnidadeEntrada.tag=0)
                         then Exit;

                      finally
                          FreeAndNil(FAcertoUnidadeEntrada);
                      end;

                 end; }
                 
                 if (Salvar(True)=False)
                 Then Begin
                           edtproduto_ep.SetFocus;
                           messagedlg('N�o foi poss�vel Gravar',mterror,[mbok],0);
                           exit;
                 End;
                  Pcodigo:=PersianaGrupoDiametroCor.Get_Codigo;

                  if (PalteraCusto=True)
                  Then ObjObjetosEntrada.AtualizaCusto('TABPERSIANAGRUPODIAMETROCOR',pcodigo,edtvalor_ep.Text);
                  
                  if (PMostraTelaAlteracaoPreco=True)
                  Then Begin
                           With FalteraPrecopersiana do
                           Begin
                               lbPrecoPago.caption:=formata_valor(edtvalor_ep.Text);
                               Objpersianagrupodiametrocor:=persianagrupodiametrocor;
                               edtprecocusto.text:=persianagrupodiametrocor.Get_PRECOCUSTO;

                               EdtPorcentagemInstaladoGDC.Text:=persianagrupodiametrocor.Get_PORCENTAGEMINSTALADO;
                               EdtPorcentagemFornecidoGDC.Text:=persianagrupodiametrocor.Get_PORCENTAGEMFORNECIDO;
                               EdtPorcentagemRetiradoGDC.Text:=persianagrupodiametrocor.Get_PORCENTAGEMRETIRADO;
                               EdtPrecoVendaInstaladoGDC.Text:=persianagrupodiametrocor.Get_PRECOVENDAINSTALADO;
                               EdtPrecoVendaFornecidoGDC.Text:=persianagrupodiametrocor.Get_PRECOVENDAFORNECIDO;
                               EdtPrecoVendaRetiradoGDC.Text:=persianagrupodiametrocor.Get_PRECOVENDARETIRADO ;

                               showmodal;

                               if (tag=0)
                               Then exit;

                               Objpersianagrupodiametrocor.LocalizaCodigo(pcodigoprimario);
                               Objpersianagrupodiametrocor.TabelaparaObjeto;
                               Objpersianagrupodiametrocor.Status:=dsEdit;
                               Objpersianagrupodiametrocor.Submit_PrecoCusto(tira_ponto(edtPrecoCusto.text));
                               Objpersianagrupodiametrocor.Submit_PORCENTAGEMINSTALADO(tira_ponto(EdtPorcentagemInstaladoGDC.Text));
                               Objpersianagrupodiametrocor.Submit_PORCENTAGEMFORNECIDO(tira_ponto(EdtPorcentagemFornecidoGDC.Text));
                               Objpersianagrupodiametrocor.Submit_PORCENTAGEMRETIRADO(tira_ponto(EdtPorcentagemRetiradoGDC.Text));
                               (*Objpersianagrupodiametrocor.Submit_precovendaINSTALADO(tira_ponto(EdtprecovendaInstalado.Text));
                               Objpersianagrupodiametrocor.Submit_precovendaFORNECIDO(tira_ponto(EdtprecovendaFornecido.Text));
                               Objpersianagrupodiametrocor.Submit_precovendaRETIRADO(tira_ponto(EdtprecovendaRetirado.Text));*)
                               if (Objpersianagrupodiametrocor.Salvar(true)=False)
                               Then Begin
                                         MensagemErro('Erro na tentativa de alterar os valores de custo e pre�o de venda');
                                         exit;
                               End;

                           End;
                 End;

                 PageControl1.TabIndex := 0;
                 Self.Atualizagrid;
                 BtCancelar_EPClick(sender);
                 edtproduto_ep.SetFocus;
            End;

     End;

     5:Begin//Vidro
            With ObjObjetosEntrada.ObjVidro_ep do
            Begin
                 ZerarTabela;
                 if (VidroCor.Localiza_Vidro_Cor(edtproduto_ep.Text,edtcor_ep.text)=False)
                 Then Begin
                           Messagedlg('N�o foi Encontrado esse Vidro nessa Cor !',mterror,[mbok],0);
                           exit;
                 End;
                 VidroCor.TabelaparaObjeto;
                 PCodigoPrimario:=VidroCor.get_codigo;
                 

                 status:=dsInactive;
                 if (edtcodigo_ep.text='')
                 or (edtcodigo_ep.text='0')
                 then begin
                          Status:=dsinsert;
                          Submit_Codigo('0');
                          Submit_OrdemInsercao('0');
                 End
                 Else Begin
                           status:=dsedit;
                           Submit_codigo(edtcodigo_ep.text);
                           Submit_OrdemInsercao(edtordeminsercao_ep.Text);
                 End;


                 Submit_Quantidade(edtquantidade_ep.Text);
                 Submit_Valor(edtvalor_ep.text);
                 Entrada.Submit_CODIGO(EdtCODIGO.Text);

                 Submit_CreditoIpi(edtcreditoipi_produtos.Text);
                 Submit_IPIpago(edtIPIpago_produtos.Text);
                 Submit_CreditoICMS(edtcreditoicms_produtos.Text);
                 Submit_CreditoPIS(edtcreditopis_produtos.Text);
                 Submit_CreditoCOFINS(edtcreditocofins_produtos.Text);
                 CFOP.Submit_CODIGO(edtEdtCFOPProduto.Text);
                 CSOSN.Submit_codigo(edtCSOSN.Text);
                 STA.Submit_CODIGO(edtEdtST_A.Text);
                 STB.Submit_CODIGO(edtEdtST_B.text);
                 Submit_Desconto(virgulaparaponto(edtdesconto.text));
                 Submit_Unidade(edtEdtUnidade.Text);
                 Submit_BCIcms(edtBaseCalculoICMS_p.text);
                 Submit_ValorIcms(edtValorICMS_p.Text);
                 Submit_ValorFrete(edtValorFreteProduto.Text);
                 Submit_ValorIpi(edtValorIPI_p.Text);
                 Submit_RedBaseIcms(edtPorcentagemReducao.Text);
                 submit_ppis(edtPercentualPis.Text);
                 submit_pcofins(edtPercentualCofins.Text);
                 submit_valoroutros(edtValorOutros.Text);

                 submit_aliquotast(edtAliquotaST.Text);
                 submit_bcicmsst(edtBCICMSST.Text);
                 submit_valoricmsst(edtValorICMSST.Text);

                 submit_cstpis(edtCstPis.Text);
                 submit_cstcofins(edtCstCofins.Text);
                 submit_cstipi(edtCstIPI.Text);
                 submit_bcipi(edtBCIPI.Text);

                 if(VidroCor.Vidro.Get_Unidade <>edtEdtUnidade.Text) then
                 begin
                      //Fazendo a convers�o de unidades
                      MensagemAviso('A unidade de medida informada na entrada difere da unidade de medida '+
                      'informada no cadastro de Vidros, ser� realizada a convers�o automatica de '+
                      edtEdtUnidade.Text + ' para '+VidroCor.Vidro.Get_Unidade);

                      FAcertoUnidadeEntrada:=TFAcertoUnidadeEntrada.Create(nil);
                      try

                         if(edtEdtUnidade.Text='KG')
                         then FAcertoUnidadeEntrada.PassaObjetos(VidroCor.Vidro.Get_Codigo,edtEdtUnidade.Text,VidroCor.Vidro.Get_Unidade,'Vidro',edtquantidade_ep.text,VidroCor.Vidro.Get_Peso)
                         else FAcertoUnidadeEntrada.PassaObjetos(VidroCor.Vidro.Get_Codigo,edtEdtUnidade.Text,VidroCor.Vidro.Get_Unidade,'Vidro',edtquantidade_ep.text,'');
                         FAcertoUnidadeEntrada.ShowModal;

                         if(FAcertoUnidadeEntrada.tag=0)
                         then Exit;

                         Submit_UnidadeConversao(VidroCor.Vidro.Get_Unidade);
                         Submit_Quantidade(edtquantidade_ep.Text);
                         Submit_QuantidadeConversao(CurrToStr(FAcertoUnidadeEntrada.QuantidadeConvertida));

                      finally
                          FreeAndNil(FAcertoUnidadeEntrada);
                      end;

                 end ;

                 if (Salvar(True)=False)
                 Then Begin
                           edtproduto_ep.SetFocus;
                           messagedlg('N�o foi poss�vel Gravar',mterror,[mbok],0);
                           exit;
                 End;
                 Pcodigo:=VidroCor.Vidro.Get_Codigo;

                 if (PalteraCusto=True)
                 Then BEgin
                         if (PConsideraSOmenteVidroINcolor=True)
                         Then BEgin
                                 //A pedido da Camila(28/08), a unica cor que pode influenciar no custo
                                 //� o incolor pois � o vidro mais barato, as outras cores
                                 //serao reajustadas pelo percentual
                                 if (uppercase(trim(VidroCor.Cor.Get_Descricao))='INCOLOR')
                                 Then ObjObjetosEntrada.AtualizaCusto('TABVIDRO',pcodigo,edtvalor_ep.Text);
                         End
                         else ObjObjetosEntrada.AtualizaCusto('TABVIDRO',pcodigo,edtvalor_ep.Text);
                 End;

                 if (PMostraTelaAlteracaoPreco=True)
                 Then Begin
                           With FalteraPrecovidro do
                           Begin
                               lbPrecoPago.caption:=formata_valor(edtvalor_ep.Text);
                               ObjvidroCor:=vidroCor;
                               EdtPrecoCusto.Text:=vidroCor.vidro.Get_PRECOCUSTO;
                               EdtPorcentagemInstalado.Text:=vidroCor.vidro.Get_PORCENTAGEMINSTALADO;
                               EdtPorcentagemFornecido.Text:=vidroCor.vidro.Get_PORCENTAGEMFORNECIDO;
                               EdtPorcentagemRetirado.Text:=vidroCor.vidro.Get_PORCENTAGEMRETIRADO;
                               EdtPrecoVendaInstalado.Text:=vidroCor.vidro.Get_PRECOVENDAINSTALADO;
                               EdtPrecoVendaFornecido.Text:=vidroCor.vidro.Get_PRECOVENDAFORNECIDO;
                               EdtPrecoVendaRetirado.Text:=vidroCor.vidro.Get_PRECOVENDARETIRADO ;

                               EdtPorcentagemAcrescimo.text:=vidroCor.Get_PorcentagemAcrescimo;
                               EdtAcrescimoExtra.text:=vidroCor.Get_AcrescimoExtra;
                               EdtPorcentagemAcrescimoFinal.text:=vidroCor.Get_PorcentagemAcrescimoFinal;

                               edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
                               
                               
                               showmodal;

                               if (tag=0)
                               Then exit;

                               ObjvidroCor.LocalizaCodigo(pcodigoprimario);
                               ObjvidroCor.TabelaparaObjeto;
                               ObjvidroCor.vidro.Status:=dsEdit;
                               ObjvidroCor.vidro.Submit_PrecoCusto(tira_ponto(edtPrecoCusto.text));
                               ObjvidroCor.vidro.Submit_PORCENTAGEMINSTALADO(tira_ponto(EdtPorcentagemInstalado.Text));
                               ObjvidroCor.vidro.Submit_PORCENTAGEMFORNECIDO(tira_ponto(EdtPorcentagemFornecido.Text));
                               ObjvidroCor.vidro.Submit_PORCENTAGEMRETIRADO(tira_ponto(EdtPorcentagemRetirado.Text));
                               ObjvidroCor.vidro.Submit_precovendaINSTALADO(tira_ponto(EdtprecovendaInstalado.Text));
                               ObjvidroCor.vidro.Submit_precovendaFORNECIDO(tira_ponto(EdtprecovendaFornecido.Text));
                               ObjvidroCor.vidro.Submit_precovendaRETIRADO(tira_ponto(EdtprecovendaRetirado.Text));
                               if (ObjvidroCor.vidro.Salvar(true)=False)
                               Then Begin
                                         MensagemErro('Erro na tentativa de alterar os valores de custo e pre�o de venda');
                                         exit;
                               End;

                               ObjvidroCor.Status:=dsEdit;
                               ObjvidroCor.Submit_PorcentagemAcrescimo(EdtPorcentagemAcrescimo.Text);
                               ObjvidroCor.Submit_AcrescimoExtra(edtAcrescimoExtra.text);
                               ObjvidroCor.Submit_ValorExtra(EdtValorExtra.Text);
                               ObjvidroCor.Submit_ValorFinal(EdtValorFinal.Text);
                               ObjvidroCor.Submit_ValorExtra(EdtValorExtra.Text);
                               ObjvidroCor.Submit_AcrescimoIcms(edtacrescimoicms.Text);
                               ObjvidroCor.Submit_PorcentagemAcrescimoIcms(edtporcentagemacrescimoicms.text);
                               ObjvidroCor.Submit_ValorFinal(EdtValorFinal.Text);

                               if (ObjvidroCor.Salvar(True)=False)
                               Then Begin
                                         MensagemErro('Erro na tentativa de gravar a porcentagem de acr�scimo para a cor');
                                         exit;
                               End;


                           End;
                 End;

                 PageControl1.TabIndex := 0;
                 Self.Atualizagrid;
                 BtCancelar_EPClick(sender);
                 edtproduto_ep.SetFocus;
            End;
     End;

  End;


  if (panelTotalRestante.Visible) then
  begin

    if (indexItem < (contProdutos-1)) then
    begin
      indexItem := indexItem + 1;
      lbTotalRestante.Caption :=  IntToStr(indexItem+1)+' de '+IntToStr(contProdutos);
      getItemXML();
    end
    else
    begin
      imgOk.Visible := true;
    end;
    
  end;


 Finally

  cbb_ep.ItemIndex:=PcodigoCombo;
  cbb_epChange(sender);

 End;

 PageControl1.TabIndex:=0;

end;                                                                                                                                          

procedure TFENTRADAPRODUTOS.AtualizaGrid;
var
temp1,temp2:String;
begin
   ObjObjetosEntrada.Atualizagrid(StrGridStrgProdutos,edtcodigo.text,lbtipocampoproduto.items);
   StrGridStrgProdutos.Row:=StrGridStrgProdutos.RowCount-1;
   edtpesquisa_STRG_GRID.enabled:=True;
   edtpesquisa_STRG_GRID.Visible:=False;
   StrGridStrgProdutos.Col:=7;//ordem de insercao
   Ordena_StringGrid(StrGridStrgProdutos,LbtipoCampoProduto.Items);
   StrGridStrgProdutos.Col:=0;

   ObjObjetosEntrada.RetornaQuantidade_e_soma_Produtos(EdtCODIGO.text,temp1,temp2);
   LbtotalProdutos.caption:=temp2;
   Lbquantidade.caption:=temp1;
end;

procedure TFENTRADAPRODUTOS.StrGridStrgProdutosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Tag:string;
begin
  if (StrGridStrgProdutos.cells[0,1]='')
  Then exit;

  if (key=vk_f12)
  Then Ordena_StringGrid(StrGridStrgProdutos,LbtipoCampoProduto.Items);

  if (Key=13) then
  begin
    with ObjObjetosEntrada do
    begin
      if (StrGridStrgProdutos.Cells[0,StrGridStrgProdutos.row]='DIVERSO')
      Then Begin
         tag:=get_campoTabela('diverso','codigo','tabdiversocor',StrGridStrgProdutos.Cells[19,StrGridStrgProdutos.row]);
         chamaFormulario(TFDIVERSO,self,Tag);
      end;
      if (StrGridStrgProdutos.Cells[0,StrGridStrgProdutos.row]='FERRAGEM')
      Then Begin
         tag:=get_campoTabela('FERRAGEM','codigo','TABFERRAGEMCOR',StrGridStrgProdutos.Cells[19,StrGridStrgProdutos.row]);
         chamaFormulario(TFFERRAGEM,self,Tag);
      end;
      if (StrGridStrgProdutos.Cells[0,StrGridStrgProdutos.row]='VIDRO')
      Then Begin
         tag:=get_campoTabela('VIDRO','codigo','TABVIDROCOR',StrGridStrgProdutos.Cells[19,StrGridStrgProdutos.row]);
         chamaFormulario(TFVIDRO,self,Tag);
      end;
      if (StrGridStrgProdutos.Cells[0,StrGridStrgProdutos.row]='PERFILADO')
      Then Begin
         tag:=get_campoTabela('PERFILADO','codigo','TABPERFILADOCOR',StrGridStrgProdutos.Cells[19,StrGridStrgProdutos.row]);
         chamaFormulario(TFPERFILADO,self,Tag);
      end;
      if (StrGridStrgProdutos.Cells[0,StrGridStrgProdutos.row]='PERSIANA')
      Then Begin
         tag:=get_campoTabela('PERSIANA','codigo','TABPERSIANAGRUPODIAMETROCOR',StrGridStrgProdutos.Cells[19,StrGridStrgProdutos.row]);
         chamaFormulario(TFPERSIANA,self,Tag);
      end;
      if (StrGridStrgProdutos.Cells[0,StrGridStrgProdutos.row]='KITBOX')
      Then Begin
         tag:=get_campoTabela('KITBOX','codigo','TABKITBOXCOR',StrGridStrgProdutos.Cells[19,StrGridStrgProdutos.row]);
         chamaFormulario(TFKITBOX,self,Tag);
      end;
    end
  end;

end;

procedure TFENTRADAPRODUTOS.StrGridStrgProdutosKeyPress(Sender: TObject;
  var Key: Char);
begin
     if (key=#32)
     Then Begin
               if (StrGridStrgProdutos.cells[0,1]='')
               Then exit;
               PreparaPesquisa_StringGrid(StrGridStrgProdutos,edtpesquisa_STRG_GRID,LbtipoCampoProduto.Items);
     End;

end;

procedure TFENTRADAPRODUTOS.edtpesquisa_STRG_GRIDKeyPress(
  Sender: TObject; var Key: Char);
begin
   if key =#27//ESC
   then Begin
           edtpesquisa_STRG_GRID.Visible := false;
           StrGridStrgProdutos.SetFocus;
           exit;
   End;

   if key=#13
   Then Begin
           Pesquisa_StringGrid(StrGridStrgProdutos,edtpesquisa_STRG_GRID,LbtipoCampoproduto.Items);
           StrGridStrgProdutos.SetFocus;
   End;

end;

procedure TFENTRADAPRODUTOS.StrGridStrgProdutosEnter(Sender: TObject);
begin
     edtpesquisa_STRG_GRID.visible:=False;
end;

procedure TFENTRADAPRODUTOS.StrGridStrgProdutosDblClick(Sender: TObject);
begin
     //alterar

     //N�o permite alterar, se quiser alterar, excluir e colocar novamente
    // exit;

    PageControl1.TabIndex := 0;

     if (StrGridStrgProdutos.Cells[6,1]='')//codigo vazio
     Then exit;

     if(lbConcluido.Caption='Conclu�do')
     then
     begin
      exit;
     end;


     with ObjObjetosEntrada do
     begin
          if (StrGridStrgProdutos.Cells[0,StrGridStrgProdutos.row]='DIVERSO')
          Then Begin
                    if (ObjDiverso_ep.LocalizaCodigo(StrGridStrgProdutos.Cells[6,StrGridStrgProdutos.row])=False)
                    Then exit;
                    ObjDiverso_ep.TabelaparaObjeto;

                    limpaedit(PnlDadosPersiana);

                    cbb_ep.ItemIndex:=0;
                    cbb_epChange(sender);

                    edtcodigo_ep.Text       :=ObjDiverso_ep.Get_CODIGO;
                    edtordeminsercao_ep.text:=ObjDiverso_ep.Get_OrdemInsercao;
                    edtproduto_ep.text      :=ObjDiverso_ep.DiversoCor.Diverso.Get_Referencia;
                    edtcor_ep.Text          :=ObjDiverso_ep.DiversoCor.Cor.Get_Codigo;
                    edtquantidade_ep.text   :=ObjDiverso_ep.get_quantidade;
                    edtvalor_ep.text        :=ObjDiverso_ep.Get_Valor;

                    edtcreditoicms_produtos.Text:=ObjDiverso_ep.Get_CreditoICMS;
                    edtcreditoipi_produtos.Text:=ObjDiverso_ep.Get_Creditoipi;
                    edtIPIpago_produtos.Text:=ObjDiverso_ep.Get_IPIpago;
                    edtcreditopis_produtos.Text:=ObjDiverso_ep.Get_Creditopis;
                    edtPercentualCofins.Text := ObjDiverso_ep.get_pcofins;
                    edtPercentualPis.Text := ObjDiverso_ep.get_ppis;
                    edtcreditocofins_produtos.Text:=ObjDiverso_ep.Get_Creditocofins;
                    edtEdtCFOPProduto.Text:=ObjDiverso_ep.CFOP.Get_CODIGO;
                    edtEdtST_A.Text:=ObjDiverso_ep.STA.Get_CODIGO;
                    edtEdtST_B.text:=ObjDiverso_ep.STB.Get_CODIGO;
                    edtCSOSN.Text:=ObjDiverso_ep.CSOSN.Get_codigo;
                    edtdesconto.Text:=ObjDiverso_ep.Get_Desconto;
                    edtEdtUnidade.Text:=ObjDiverso_ep.Get_Unidade;
                    edtBaseCalculoICMS_p.Text:=ObjDiverso_ep.Get_BCIcms;
                    edtValorICMS_p.Text:=ObjDiverso_ep.Get_ValorIcms;
                    edtValorFreteProduto.Text:=ObjDiverso_ep.Get_ValorFrete;
                    edtValorIPI_p.Text:=ObjDiverso_ep.Get_ValorIPI;
                    edtPorcentagemReducao.Text:=ObjDiverso_ep.Get_RedBaseIcms;

                    edtAliquotaST.Text  := ObjDiverso_ep.get_aliquotast;
                    edtBCICMSST.Text    := ObjDiverso_ep.get_bcicmsst;
                    edtValorICMSST.Text := ObjDiverso_ep.get_valoricmsst;
                    edtValorOutros.Text := ObjDiverso_ep.get_valoroutros;

                    edtCstIPI.Text    := ObjDiverso_ep.get_cstipi;
                    edtCstPis.Text    := ObjDiverso_ep.get_cstpis;
                    edtCstCofins.Text := ObjDiverso_ep.get_cstcofins;

                    edtBCIPI.Text := ObjDiverso_ep.get_bcipi;

                    edtproduto_ep.SetFocus;
          End;

          if (strgridStrgProdutos.Cells[0,strgridStrgProdutos.row]='FERRAGEM')
          Then Begin
                    if (ObjFERRAGEM_ep.LocalizaCodigo(strgridStrgProdutos.Cells[6,strgridStrgProdutos.row])=False)
                    Then exit;
                    ObjFERRAGEM_ep.TabelaparaObjeto;

                    limpaedit(PnlDadosPersiana);

                    cbb_ep.ItemIndex:=1;
                    cbb_epChange(sender);

                    edtcodigo_ep.Text       :=ObjFERRAGEM_ep.Get_CODIGO;
                    edtordeminsercao_ep.text:=ObjFERRAGEM_ep.Get_OrdemInsercao;
                    edtproduto_ep.text      :=ObjFERRAGEM_ep.FERRAGEMCor.FERRAGEM.Get_Referencia;
                    edtcor_ep.Text          :=ObjFERRAGEM_ep.FERRAGEMCor.Cor.Get_Codigo;
                    edtquantidade_ep.text   :=ObjFERRAGEM_ep.get_quantidade;
                    edtvalor_ep.text        :=ObjFERRAGEM_ep.Get_Valor;

                    edtcreditoicms_produtos.Text:=Objferragem_ep.Get_CreditoICMS;
                    edtcreditoipi_produtos.Text:=Objferragem_ep.Get_Creditoipi;
                    edtIPIpago_produtos.Text:=Objferragem_ep.Get_IPIpago;
                    edtcreditopis_produtos.Text:=Objferragem_ep.Get_Creditopis;
                    edtcreditocofins_produtos.Text:=Objferragem_ep.Get_Creditocofins;
                    edtPercentualCofins.Text := Objferragem_ep.get_pcofins;
                    edtPercentualPis.Text := Objferragem_ep.get_ppis;

                    edtEdtCFOPProduto.Text:=Objferragem_ep.CFOP.Get_CODIGO;
                    edtEdtST_A.Text:=Objferragem_ep.STA.Get_CODIGO;
                    edtEdtST_B.Text:=Objferragem_ep.STB.Get_CODIGO;
                    edtCSOSN.Text:=Objferragem_ep.CSOSN.Get_codigo;
                    edtdesconto.Text:=Objferragem_ep.Get_Desconto;
                    edtEdtUnidade.Text:=Objferragem_ep.Get_Unidade;
                    edtBaseCalculoICMS_p.Text:=Objferragem_ep.Get_BCIcms;
                    edtValorICMS_p.Text:=Objferragem_ep.Get_ValorIcms;
                    edtValorFreteProduto.Text:=Objferragem_ep.Get_ValorFrete;
                    edtValorIPI_p.Text:=Objferragem_ep.Get_ValorIPI;
                    edtPorcentagemReducao.Text:=Objferragem_ep.Get_RedBaseIcms;
                    edtValorOutros.Text := Objferragem_ep.get_valoroutros;

                    edtAliquotaST.Text  := Objferragem_ep.get_aliquotast;
                    edtBCICMSST.Text    := Objferragem_ep.get_bcicmsst;
                    edtValorICMSST.Text := Objferragem_ep.get_valoricmsst;

                    edtCstIPI.Text    := Objferragem_ep.get_cstipi;
                    edtCstPis.Text    := Objferragem_ep.get_cstpis;
                    edtCstCofins.Text := Objferragem_ep.get_cstcofins;

                    edtBCIPI.Text := Objferragem_ep.get_bcipi;

                    edtproduto_ep.SetFocus;
          End;

          if (strgridStrgProdutos.Cells[0,strgridStrgProdutos.row]='KITBOX')
          Then Begin
                    if (Objkitbox_ep.LocalizaCodigo(strgridStrgProdutos.Cells[6,strgridStrgProdutos.row])=False)
                    Then exit;
                    Objkitbox_ep.TabelaparaObjeto;

                    limpaedit(PnlDadosPersiana);

                    cbb_ep.ItemIndex:=2;
                    cbb_epChange(sender);

                    edtcodigo_ep.Text       :=Objkitbox_ep.Get_CODIGO;
                    edtordeminsercao_ep.text:=Objkitbox_ep.Get_OrdemInsercao;
                    edtproduto_ep.text      :=Objkitbox_ep.kitboxCor.kitbox.Get_Referencia;
                    edtcor_ep.Text          :=Objkitbox_ep.kitboxCor.Cor.Get_Codigo;
                    edtquantidade_ep.text   :=Objkitbox_ep.get_quantidade;
                    edtvalor_ep.text        :=Objkitbox_ep.Get_Valor;

                    edtcreditoicms_produtos.Text:=Objkitbox_ep.Get_CreditoICMS;
                    edtcreditoipi_produtos.Text:=Objkitbox_ep.Get_Creditoipi;
                    edtIPIpago_produtos.Text:=Objkitbox_ep.Get_IPIpago;
                    edtcreditopis_produtos.Text:=Objkitbox_ep.Get_Creditopis;
                    edtcreditocofins_produtos.Text:=Objkitbox_ep.Get_Creditocofins;

                    edtPercentualCofins.Text := Objkitbox_ep.get_pcofins;
                    edtPercentualPis.Text := Objkitbox_ep.get_ppis;

                    edtEdtCFOPProduto.Text:=Objkitbox_ep.CFOP.Get_CODIGO;
                    edtEdtST_A.Text:=Objkitbox_ep.STA.Get_CODIGO;
                    edtEdtST_B.Text:=Objkitbox_ep.STB.Get_CODIGO;
                    edtCSOSN.Text:=Objkitbox_ep.CSOSN.Get_codigo;
                    edtdesconto.Text:=Objkitbox_ep.Get_Desconto;
                    edtEdtUnidade.text:=Objkitbox_ep.Get_Unidade;
                    edtBaseCalculoICMS_p.Text:=Objkitbox_ep.Get_BCIcms;
                    edtValorICMS_p.Text:=Objkitbox_ep.Get_ValorIcms;
                    edtValorFreteProduto.Text:=Objkitbox_ep.Get_ValorFrete;
                    edtValorIPI_p.Text:=Objkitbox_ep.Get_ValorIPI;
                    edtPorcentagemReducao.Text:=Objkitbox_ep.Get_RedBaseIcms;
                    edtValorOutros.Text := Objkitbox_ep.get_valoroutros;

                    edtAliquotaST.Text  := Objkitbox_ep.get_aliquotast;
                    edtBCICMSST.Text    := Objkitbox_ep.get_bcicmsst;
                    edtValorICMSST.Text := Objkitbox_ep.get_valoricmsst;

                    edtCstIPI.Text    := Objkitbox_ep.get_cstipi;
                    edtCstPis.Text    := Objkitbox_ep.get_cstpis;
                    edtCstCofins.Text := Objkitbox_ep.get_cstcofins;

                    edtBCIPI.Text := Objkitbox_ep.get_bcipi;

                    edtproduto_ep.SetFocus;
          End;

          if (strgridStrgProdutos.Cells[0,strgridStrgProdutos.row]='PERFILADO')
          Then Begin
                    if (ObjPERFILADO_ep.LocalizaCodigo(strgridStrgProdutos.Cells[6,strgridStrgProdutos.row])=False)
                    Then exit;
                    ObjPERFILADO_ep.TabelaparaObjeto;

                    //limpaedit(PanelProdutos);
                    limpaedit(PnlDadosPersiana);

                    cbb_ep.ItemIndex:=3;
                    cbb_epChange(sender);

                    edtcodigo_ep.Text       :=ObjPERFILADO_ep.Get_CODIGO;
                    edtordeminsercao_ep.text:=ObjPERFILADO_ep.Get_OrdemInsercao;
                    edtproduto_ep.text      :=ObjPERFILADO_ep.PERFILADOCor.PERFILADO.Get_Referencia;
                    edtcor_ep.Text          :=ObjPERFILADO_ep.PERFILADOCor.Cor.Get_Codigo;
                    edtquantidade_ep.text   :=ObjPERFILADO_ep.get_quantidade;
                    edtvalor_ep.text        :=ObjPERFILADO_ep.Get_Valor;

                    edtcreditoicms_produtos.Text:=Objperfilado_ep.Get_CreditoICMS;
                    edtcreditoipi_produtos.Text:=Objperfilado_ep.Get_Creditoipi;
                    edtIPIpago_produtos.Text:=Objperfilado_ep.Get_IPIpago;
                    edtcreditopis_produtos.Text:=Objperfilado_ep.Get_Creditopis;
                    edtcreditocofins_produtos.Text:=Objperfilado_ep.Get_Creditocofins;

                    edtPercentualCofins.Text := Objperfilado_ep.get_pcofins;
                    edtPercentualPis.Text := Objperfilado_ep.get_ppis;

                    edtEdtCFOPProduto.Text:=Objperfilado_ep.CFOP.Get_CODIGO;
                    edtEdtST_A.Text:=Objperfilado_ep.STA.Get_CODIGO;
                    edtEdtST_B.Text:=Objperfilado_ep.STB.Get_CODIGO;
                    edtCSOSN.Text:=Objperfilado_ep.CSOSN.Get_codigo;
                    edtdesconto.Text:=Objperfilado_ep.Get_Desconto;
                    edtEdtUnidade.Text:=Objperfilado_ep.Get_Unidade;
                    edtBaseCalculoICMS_p.Text:=ObjPERFILADO_ep.Get_BCIcms;
                    edtValorICMS_p.Text:=ObjPERFILADO_ep.Get_ValorIcms;
                    edtValorFreteProduto.Text:=ObjPERFILADO_ep.Get_ValorFrete;
                    edtValorIPI_p.Text:=ObjPERFILADO_ep.Get_ValorIPI;
                    edtPorcentagemReducao.Text:=ObjPERFILADO_ep.Get_RedBaseIcms;

                    edtValorOutros.Text := ObjPERFILADO_ep.get_valoroutros;


                    edtAliquotaST.Text  := ObjPERFILADO_ep.get_aliquotast;
                    edtBCICMSST.Text    := ObjPERFILADO_ep.get_bcicmsst;
                    edtValorICMSST.Text := ObjPERFILADO_ep.get_valoricmsst;

                    edtCstIPI.Text    := ObjPERFILADO_ep.get_cstipi;
                    edtCstPis.Text    := ObjPERFILADO_ep.get_cstpis;
                    edtCstCofins.Text := ObjPERFILADO_ep.get_cstcofins;

                    edtBCIPI.Text := Objperfilado_ep.get_bcipi;

                    edtproduto_ep.SetFocus;
          End;

          if (strgridStrgProdutos.Cells[0,strgridStrgProdutos.row]='PERSIANA')
          Then Begin
                    if (ObjPERSIANA_ep.LocalizaCodigo(strgridStrgProdutos.Cells[6,strgridStrgProdutos.row])=False)
                    Then exit;
                    ObjPERSIANA_ep.TabelaparaObjeto;

                    //limpaedit(PanelProdutos);
                    limpaedit(PnlDadosPersiana);

                    cbb_ep.ItemIndex:=4;
                    cbb_epChange(sender);

                    edtcodigo_ep.Text       :=ObjPERSIANA_ep.Get_CODIGO;
                    edtordeminsercao_ep.text:=ObjPERSIANA_ep.Get_OrdemInsercao;
                    edtproduto_ep.text      :=ObjPERSIANA_ep.PersianaGrupoDiametroCor.PERSIANA.Get_Referencia;
                    edtcor_ep.Text          :=ObjPERSIANA_ep.PersianaGrupoDiametroCor.Cor.Get_Codigo;
                    edtquantidade_ep.text   :=ObjPERSIANA_ep.get_quantidade;
                    edtvalor_ep.text        :=ObjPERSIANA_ep.Get_Valor;

                    edtgrupopersiana_EP.text   :=ObjPersiana_ep.PersianaGrupoDiametroCor.GrupoPersiana.Get_Codigo;
                    edtdiametropersiana_EP.text:=ObjPersiana_ep.PersianaGrupoDiametroCor.Diametro.Get_Codigo;

                    edtcreditoicms_produtos.Text:=Objpersiana_ep.Get_CreditoICMS;
                    edtcreditoipi_produtos.Text:=Objpersiana_ep.Get_Creditoipi;
                    edtIPIpago_produtos.Text:=Objpersiana_ep.Get_IPIpago;
                    edtcreditopis_produtos.Text:=Objpersiana_ep.Get_Creditopis;
                    edtcreditocofins_produtos.Text:=Objpersiana_ep.Get_Creditocofins;

                    edtPercentualCofins.Text := ObjPersiana_ep.get_pcofins;
                    edtPercentualPis.Text := ObjPersiana_ep.get_ppis;

                    edtEdtCFOPProduto.Text:=ObjPersiana_ep.CFOP.Get_CODIGO;
                    edtEdtST_A.Text:=ObjPersiana_ep.STA.Get_CODIGO;
                    edtEdtST_B.Text:=ObjPersiana_ep.STA.Get_CODIGO;
                    edtCSOSN.Text:=ObjPersiana_ep.CSOSN.Get_codigo;
                    edtdesconto.Text:=ObjPersiana_ep.Get_Desconto;
                    edtEdtUnidade.Text:=ObjPersiana_ep.Get_Unidade;
                    edtBaseCalculoICMS_p.Text:=ObjPERSIANA_ep.Get_BCIcms;
                    edtValorICMS_p.Text:=ObjPERSIANA_ep.Get_ValorIcms;
                    edtValorFreteProduto.Text:=ObjPERSIANA_ep.Get_ValorFrete;
                    edtValorIPI_p.Text:=ObjPERSIANA_ep.Get_ValorIPI;
                    edtPorcentagemReducao.Text:=ObjPERSIANA_ep.Get_RedBaseIcms;

                    edtValorOutros.Text := ObjPERSIANA_ep.get_valoroutros;

                    edtAliquotaST.Text  := ObjPERSIANA_ep.get_aliquotast;
                    edtBCICMSST.Text    := ObjPERSIANA_ep.get_bcicmsst;
                    edtValorICMSST.Text := ObjPERSIANA_ep.get_valoricmsst;

                    edtCstIPI.Text    := ObjPERSIANA_ep.get_cstipi;
                    edtCstPis.Text    := ObjPERSIANA_ep.get_cstpis;
                    edtCstCofins.Text := ObjPERSIANA_ep.get_cstcofins;

                    edtBCIPI.Text := ObjPersiana_ep.get_bcipi;

                    edtproduto_ep.SetFocus;
          End;

          if (strgridStrgProdutos.Cells[0,strgridStrgProdutos.row]='VIDRO')
          Then Begin
                    if (ObjVIDRO_ep.LocalizaCodigo(strgridStrgProdutos.Cells[6,strgridStrgProdutos.row])=False)
                    Then exit;
                    ObjVIDRO_ep.TabelaparaObjeto;

                    //limpaedit(PanelProdutos);
                    limpaedit(PnlDadosPersiana);

                    cbb_ep.ItemIndex:=5;
                    cbb_epChange(sender);

                    edtcodigo_ep.Text       :=ObjVIDRO_ep.Get_CODIGO;
                    edtordeminsercao_ep.text:=ObjVIDRO_ep.Get_OrdemInsercao;
                    edtproduto_ep.text      :=ObjVIDRO_ep.VIDROCor.VIDRO.Get_Referencia;
                    edtcor_ep.Text          :=ObjVIDRO_ep.VIDROCor.Cor.Get_Codigo;
                    edtquantidade_ep.text   :=ObjVIDRO_ep.get_quantidade;
                    edtvalor_ep.text        :=ObjVIDRO_ep.Get_Valor;

                    edtcreditoicms_produtos.Text:=Objvidro_ep.Get_CreditoICMS;
                    edtcreditoipi_produtos.Text:=Objvidro_ep.Get_Creditoipi;
                    edtIPIpago_produtos.Text:=Objvidro_ep.Get_IPIpago;
                    edtcreditopis_produtos.Text:=Objvidro_ep.Get_Creditopis;
                    edtcreditocofins_produtos.Text:=Objvidro_ep.Get_Creditocofins;

                    edtPercentualCofins.Text := Objvidro_ep.get_pcofins;
                    edtPercentualPis.Text := Objvidro_ep.get_ppis;

                    edtEdtCFOPProduto.Text:=Objvidro_ep.CFOP.Get_CODIGO;
                    edtEdtST_A.Text:=Objvidro_ep.STA.Get_CODIGO;
                    edtEdtST_B.Text:=Objvidro_ep.STB.Get_CODIGO;
                    edtCSOSN.Text:=Objvidro_ep.CSOSN.Get_codigo;
                    edtdesconto.Text:=Objvidro_ep.Get_Desconto;
                    edtEdtUnidade.Text:=Objvidro_ep.Get_Unidade;
                    edtBaseCalculoICMS_p.Text:=ObjVIDRO_ep.Get_BCIcms;
                    edtValorICMS_p.Text:=ObjVIDRO_ep.Get_ValorIcms;
                    edtValorFreteProduto.Text:=ObjVIDRO_ep.Get_ValorFrete;
                    edtValorIPI_p.Text:=ObjVIDRO_ep.Get_ValorIPI;
                    edtPorcentagemReducao.Text:=ObjVIDRO_ep.Get_RedBaseIcms;

                    edtValorOutros.Text := ObjVIDRO_ep.get_valoroutros;

                    edtAliquotaST.Text  := ObjVIDRO_ep.get_aliquotast;
                    edtBCICMSST.Text    := ObjVIDRO_ep.get_bcicmsst;
                    edtValorICMSST.Text := ObjVIDRO_ep.get_valoricmsst;

                    edtCstIPI.Text    := ObjVIDRO_ep.get_cstipi;
                    edtCstPis.Text    := ObjVIDRO_ep.get_cstpis;
                    edtCstCofins.Text := ObjVIDRO_ep.get_cstcofins;

                    edtBCIPI.Text := Objvidro_ep.get_bcipi;

                    edtproduto_ep.SetFocus;
          End;

     End;

     desabilita_campos(Self);
     habilitaCamposProduto;


end;

procedure TFENTRADAPRODUTOS.BtExcluir_EpClick(Sender: TObject);
var
  QueryDeleta:TIBQuery;
begin

     if(lbConcluido.Caption='Conclu�do')then
      Exit;

     QueryDeleta:=TIBQuery.Create(nil);
     QueryDeleta.Database:=FDataModulo.IBDatabase;

     try

         if (Trim(strgridStrgProdutos.Cells[6,strgridStrgProdutos.row])='') Then
          exit;

         //If (Messagedlg('Tem certeza que deseja Excluir o(a) '+strgridStrgProdutos.Cells[0,strgridStrgProdutos.row]+' C�digo '+strgridStrgProdutos.Cells[6,strgridStrgProdutos.row]+' ?',mtconfirmation,[mbyes,mbno],0)=mrno) Then
          //exit;

         if (strgridStrgProdutos.Cells[0,strgridStrgProdutos.row]='DIVERSO') Then
         Begin

          if (ObjObjetosEntrada.ObjDiverso_ep.Exclui(strgridStrgProdutos.Cells[6,strgridStrgProdutos.row],True)=False) Then
          Begin
            Messagedlg('Erro na tentativa de Excluir o DIVERSO c�digo '+strgridStrgProdutos.Cells[6,strgridStrgProdutos.row],mterror,[mbok],0);
            exit;
          End;

          Self.AtualizaGrid;
          limpaedit(pnl1);
          limpaCamposProduto;
          LimpaLabels;
          
         End
         else
         if (strgridStrgProdutos.Cells[0,strgridStrgProdutos.row]='FERRAGEM') Then
         Begin

          if (ObjObjetosEntrada.ObjFERRAGEM_ep.Exclui(strgridStrgProdutos.Cells[6,strgridStrgProdutos.row],True)=False) Then
          Begin
            Messagedlg('Erro na tentativa de Excluir o FERRAGEM c�digo '+strgridStrgProdutos.Cells[6,strgridStrgProdutos.row],mterror,[mbok],0);
            exit;
          end;

          Self.AtualizaGrid;
          limpaedit(pnl1);
          limpaCamposProduto;
          LimpaLabels;

         End
         else
         if (strgridStrgProdutos.Cells[0,strgridStrgProdutos.row]='KITBOX') Then
         Begin

          if (ObjObjetosEntrada.ObjKITBOX_ep.Exclui(strgridStrgProdutos.Cells[6,strgridStrgProdutos.row],True)=False) Then
          Begin
            Messagedlg('Erro na tentativa de Excluir o KITBOX c�digo '+strgridStrgProdutos.Cells[6,strgridStrgProdutos.row],mterror,[mbok],0);
            exit;
          End;

          Self.AtualizaGrid;
          limpaedit(pnl1);
          limpaCamposProduto;
          LimpaLabels;

         End
         else
         if (strgridStrgProdutos.Cells[0,strgridStrgProdutos.row]='PERFILADO') Then
         Begin

          if (ObjObjetosEntrada.ObjPERFILADO_ep.Exclui(strgridStrgProdutos.Cells[6,strgridStrgProdutos.row],True)=False) Then
          Begin
            Messagedlg('Erro na tentativa de Excluir o PERFILADO c�digo '+strgridStrgProdutos.Cells[6,strgridStrgProdutos.row],mterror,[mbok],0);
            exit;
          End;

          Self.AtualizaGrid;
          limpaedit(pnl1);
          limpaCamposProduto;
          LimpaLabels;

         End
         else
         if (strgridStrgProdutos.Cells[0,strgridStrgProdutos.row]='PERSIANA') Then
         Begin

          if (ObjObjetosEntrada.ObjPERSIANA_ep.Exclui(strgridStrgProdutos.Cells[6,strgridStrgProdutos.row],True)=False) Then
          Begin
            Messagedlg('Erro na tentativa de Excluir o PERSIANA c�digo '+strgridStrgProdutos.Cells[6,strgridStrgProdutos.row],mterror,[mbok],0);
            exit;
          End;

          Self.AtualizaGrid;
          limpaedit(pnl1);
          limpaCamposProduto;
          LimpaLabels;
          
         End
         else
         if (strgridStrgProdutos.Cells[0,strgridStrgProdutos.row]='VIDRO') Then
         Begin

          if (ObjObjetosEntrada.ObjVIDRO_ep.Exclui(strgridStrgProdutos.Cells[6,strgridStrgProdutos.row],True)=False) Then
          Begin
            Messagedlg('Erro na tentativa de Excluir o VIDRO c�digo '+strgridStrgProdutos.Cells[6,strgridStrgProdutos.row],mterror,[mbok],0);
            exit;
          End;

          Self.AtualizaGrid;
          limpaedit(pnl1);
          limpaCamposProduto;
          LimpaLabels;
          
         End;

         exec_sql('update tabentradaprodutos set spedvalido=''N'' where codigo =  '+edtCodigo.Text);
         FDataModulo.IBTransaction.CommitRetaining;
         setImgVerificado(0);

         imgOk.Visible := False;

     finally
      FreeAndNil(QueryDeleta);
     end;


end;

procedure TFENTRADAPRODUTOS.edtvalor_epKeyPress(Sender: TObject;
  var Key: Char);
begin

    if not (key in['0'..'9',',',#8]) then

      if (key = '.') then
        Key := ','
      else
        Key := #0;

end;

procedure TFENTRADAPRODUTOS.habilitaCamposProduto;
begin
   if(lbConcluido.Caption='Conclu�do')
   then
   begin
         DesabilitaCamposProduto;
         exit;
   end;
    cbb_ep.Enabled:=True;
    edtproduto_ep.Enabled:=True;
    edtcor_ep.Enabled:=True;
    edtgrupopersiana_EP.Enabled:=True;
    edtdiametropersiana_EP.Enabled:=True;
    edtcreditocofins_produtos.Enabled:=True;
    edtcreditopis_produtos.Enabled:=True;
    edtcreditoicms_produtos.Enabled:=True;
    edtPercentualPis.Enabled:=True;
    edtCstPis.Enabled:=True;
    edtCstCofins.Enabled:=True;
    edtPercentualCofins.Enabled:=True;
    edtValorOutros.Enabled := True;
    edtcreditoipi_produtos.Enabled:=True;
    edtdiametropersiana_EP.Enabled:=True;
    edtquantidade_ep.Enabled:=True;
    edtvalor_ep.Enabled:=True;
    edtipipago_produtos.Enabled:=True;
    edtEdtCFOPProduto.Enabled:=True;
    edtEdtST_A.enabled:=True;
    edtEdtST_B.Enabled:=True;
    edtCSOSN.Enabled:=True;
    edtdesconto.Enabled:=True;
    edtEdtUnidade.Enabled:=True;
    edtBaseCalculoICMS_p.Enabled:=True;
    edtValorICMS_p.Enabled:=True;
    edtAliquotaST.Enabled:=True;
    edtValorICMSST.Enabled:=True;
    edtBCICMSST.Enabled:=true;
    edtValorFreteProduto.Enabled:=True;
    edtValorIPI_p.Enabled:=True;
    edtPorcentagemReducao.Enabled:=True;
    edtCstIPI.Enabled:=True;
    edtBCIPI.Enabled:=True;

end;

procedure TFENTRADAPRODUTOS.DesabilitaCamposProduto;
begin
    cbb_ep.Enabled:=False;
    edtproduto_ep.Enabled:=False;
    edtcor_ep.Enabled:=False;
    edtgrupopersiana_EP.Enabled:=False;
    edtdiametropersiana_EP.Enabled:=False;
    edtcreditocofins_produtos.Enabled:=False;
    edtcreditopis_produtos.Enabled:=False;
    edtcreditoicms_produtos.Enabled:=False;
    edtcreditoipi_produtos.Enabled:=False;
    edtPercentualCofins.Enabled:=False;
    edtValorOutros.Enabled:=False;
    edtPercentualPis.Enabled:=False;
    edtdiametropersiana_EP.Enabled:=False;
    edtquantidade_ep.Enabled:=False;
    edtvalor_ep.Enabled:=False;
    edtipipago_produtos.Enabled:=False;
    edtEdtCFOPProduto.Enabled:=False;
    edtEdtST_A.enabled:=False;
    edtEdtST_B.Enabled:=False;
    edtCSOSN.Enabled:=False;
    edtdesconto.Enabled:=False;
    edtEdtUnidade.Enabled:=False;
    edtBaseCalculoICMS_p.Enabled:=False;
    edtValorICMS_p.Enabled:=False;
    edtValorFreteProduto.Enabled:=False;
    edtValorIPI_p.Enabled:=False;
    edtPorcentagemReducao.Enabled:=False;
    edtCstPis.Enabled:=False;
    edtCstCofins.Enabled:=False;
    edtCstIPI.Enabled:=False;
    edtBCIPI.Enabled:=false;
end;



procedure TFENTRADAPRODUTOS.StrGridStrgProdutosDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}

begin
          Exit;
          //ZEBRANDO A STRIGRID
          if ((ARow mod 2)=0) then
          begin
               StrGridStrgProdutos.Canvas.Brush.Color :=CORGRIDZEBRADOGLOBAL1;
               StrGridStrgProdutos.Canvas.Font.Color := clBlack;
          end
          else
          begin
             StrGridStrgProdutos.Canvas.Brush.Color := clWhite;
             StrGridStrgProdutos.Canvas.Font.Color := clBlack;
          end;
          StrGridStrgProdutos.Canvas.TextRect(Rect, Rect.Left + LM, Rect.Top + TM, StrGridStrgProdutos.Cells[Acol,Arow]);


end;

procedure TFENTRADAPRODUTOS.lbLbtitulo1MouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFENTRADAPRODUTOS.lbLbtitulo1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
       ScreenCursorProc(crHandPoint);
end;

procedure TFENTRADAPRODUTOS.imgrodapeMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
         ScreenCursorProc(crDefault);
end;

procedure TFENTRADAPRODUTOS.lbLbtitulo1Click(Sender: TObject);
var
Tmptitulo:TFtitulo_novo;
begin
      If (edtCodigo.Text='' )
      Then Exit;

      if (lbLbtitulo1.Caption='' )
      Then begin
                MensagemErro('N�o existe T�tulo para essa Compra!');
                Exit;
      End;

      Try
            Tmptitulo:=TFTitulo_novo.create(nil);
      Except
            Messagedlg('Erro na tentativa de Criar o Formul�rio de T�tulo',mterror,[mbok],0);
            exit;
      End;
      Try
            TmpTitulo.quitaPrimeiraParcela:=False;
            TmpTitulo.LocalizaCodigoInicial(lbLbtitulo1.Caption);
            TmpTitulo.showmodal;
      Finally
            Freeandnil(TmpTitulo);
      End;
end;

procedure TFENTRADAPRODUTOS.edtFornecedorKeyPress(Sender: TObject;
  var Key: Char);
begin
      if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFENTRADAPRODUTOS.edtproduto_epKeyPress(Sender: TObject;
  var Key: Char);
begin
        if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFENTRADAPRODUTOS.edtcor_epKeyPress(Sender: TObject;
  var Key: Char);
begin
        if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFENTRADAPRODUTOS.edtgrupopersiana_EPKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFENTRADAPRODUTOS.edtdiametropersiana_EPKeyPress(Sender: TObject;
  var Key: Char);
begin
         if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFENTRADAPRODUTOS.edtFornecedorDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Ffornecedor:TFFORNECEDOR;
begin
  Try
    Fpesquisalocal:=Tfpesquisa.create(Nil);
    Ffornecedor:=TFFORNECEDOR.Create(nil);
    If (FpesquisaLocal.PreparaPesquisa('Select * from tabfornecedor','',Ffornecedor)=True)
    Then
    Begin
      Try
        If (FpesquisaLocal.showmodal=mrok)
        Then
        Begin
          EdtFornecedor.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
        End;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End; 
  Finally
    FreeandNil(FPesquisaLocal);
    FreeAndNil(Ffornecedor);
  End;
end;
//Entrda de produtos -  Aumenta estoque....
function TFENTRADAPRODUTOS.AumentaEstoque(Produto,TabMaterial,quantidade:string):Boolean;
var
   Query:TIBQuery;
begin
     try
       Query:=TIBQuery.Create(nil);
       Query.Database:=FDataModulo.IBDatabase;
     except

     end;

     try
            with query  do
            begin
                  close;
                  sql.Clear;
                  sql.Add('update '+TabMaterial+' set estoque=estoque+'+quantidade) ;
                  SQL.add('where codigo='+produto) ;
                  InputBox('','',sql.Text);
                  ExecSQL;
                  //FDataModulo.IBTransaction.CommitRetaining;
            end;
     finally

     end;



end;

procedure TFENTRADAPRODUTOS.btAjudaClick(Sender: TObject);
begin
      FAjuda.PassaAjuda('CADASTRO DE PROJETOS');
      FAjuda.ShowModal;
end;

procedure TFENTRADAPRODUTOS.edtModeloNFKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjObjetosEntrada.ObjEntradaProdutos.MODELONF.edtModeloNFKeyDown(Sender,Key,Shift,nil);
end;

procedure TFENTRADAPRODUTOS.edtcfopKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjObjetosEntrada.ObjEntradaProdutos.CFOP.EdtCFOPKeyDown(Sender,Key,Shift,nil);
end;

procedure TFENTRADAPRODUTOS.edtEdtCFOPProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  ObjObjetosEntrada.ObjEntradaProdutos.CFOP.EdtCFOPKeyDown(Sender,Key,Shift);
end;

procedure TFENTRADAPRODUTOS.edtEdtST_AKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjObjetosEntrada.ObjDiverso_ep.STA.EdtSTAKeyDown(sender,Key,Shift);
     self.edtEdtST_A.Hint:=get_campoTabela('descricao','codigo','tabtabelaa_st',self.edtEdtST_A.Text);
end;

procedure TFENTRADAPRODUTOS.edtEdtST_BKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjObjetosEntrada.ObjDiverso_ep.STB.EdtSTBKeyDown(Sender,Key,Shift);
     self.edtEdtST_B.Hint:=get_campoTabela('descricao','codigo','tabtabelab_st', self.edtEdtST_B.Text);
end;

procedure TFENTRADAPRODUTOS.edtCSOSNKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjObjetosEntrada.ObjDiverso_ep.CSOSN.edtCSOSNkeydown(Sender,Key,Shift);
     self.edtCSOSN.Hint:=get_campoTabela('descricao','codigo','TABCSOSN',self.edtCSOSN.Text);
end;

procedure TFENTRADAPRODUTOS.edtVALORPRODUTOSKeyPress(Sender: TObject;
  var Key: Char);
begin
      if not (Key in['0'..'9',Chr(8),',']) then
    begin
        Key:= #0;
    end;
end;

procedure TFENTRADAPRODUTOS.lbNomeFornecedorMouseLeave(Sender: TObject);
begin
  TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFENTRADAPRODUTOS.lbNomeFornecedorMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFENTRADAPRODUTOS.lbLbModeloNfClick(Sender: TObject);
var
  Fmodelonf:TFModeloDeNF;
begin
    try
      FModeloNF:=TFModeloDeNF.Create(nil);
    except
      Exit;
    end;

    try
      if(edtModeloNF.Text='')
      then Exit;

      FModeloNF.Tag:=StrToInt(edtModeloNF.Text);
      FModelonf.showmodal;
    finally
      FreeAndNil(Fmodelonf);
    end;


end;

procedure TFENTRADAPRODUTOS.lb34Click(Sender: TObject);
var
  FCfop:TFCFOP;
begin
  try
    FCfop:=TFCFOP.Create(nil);
  except
    Exit;
  end;

  try
    if(edtcfop.Text='')
    then Exit;

    FCfop.tag:=StrToInt(edtcfop.Text);
    FCfop.ShowModal;
  finally
    FreeAndNil(FCfop);
  end;


end;

procedure TFENTRADAPRODUTOS.edtEdtUnidadeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
  If (key <>vk_f9)
  Then exit;

  Try
    Fpesquisalocal:=Tfpesquisa.create(Nil);

    If (FpesquisaLocal.PreparaPesquisa('SELECT * FROM TABUNIDADEMEDIDA','',nil)=True)
    Then Begin
      Try
        If (FpesquisaLocal.showmodal=mrok)Then
        Begin
          TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('sigla').asstring; 
        End;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;
  Finally
    FreeandNil(FPesquisaLocal);
  End;
end;

procedure TFENTRADAPRODUTOS.edtModeloNFDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
   key:=VK_F9; 
   ObjObjetosEntrada.ObjEntradaProdutos.MODELONF.edtModeloNFKeyDown(Sender,Key,Shift,nil);
end;

procedure TFENTRADAPRODUTOS.edtcfopDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
  key:=VK_F9;
  ObjObjetosEntrada.ObjEntradaProdutos.CFOP.EdtCFOPKeyDown(Sender,Key,Shift,nil);
end;

procedure TFENTRADAPRODUTOS.edtproduto_epDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
  key:=VK_F9;
  edtproduto_epKeyDown(sender,Key,Shift);
end;

procedure TFENTRADAPRODUTOS.edtgrupopersiana_EPDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
  key:=VK_F9;
  ObjObjetosEntrada.ObjPersiana_ep.PersianaGrupoDiametroCor.EdtGrupoPersianaKeyDown(sender,key,shift,lbnomegrupo,edtproduto_ep.text,edtcor_ep.text);
end;

procedure TFENTRADAPRODUTOS.edtdiametropersiana_EPDblClick(
  Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
  key:=VK_F9;
  ObjObjetosEntrada.ObjPersiana_ep.PersianaGrupoDiametroCor.EdtDiametroKeyDown(sender,key,shift,lbnomediametro,edtproduto_ep.text,edtcor_ep.text,edtgrupopersiana_EP.text);
end;

procedure TFENTRADAPRODUTOS.edtcor_epDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
  key:=VK_F9;
  ObjObjetosEntrada.ObjDiverso_ep.DiversoCor.Cor.EdtCorKeyDown(sender,key,shift,lbnomecor,edtproduto_ep.text,cbb_ep.text);
end;

procedure TFENTRADAPRODUTOS.edtEdtCFOPProdutoDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
  key:=VK_F9;
  ObjObjetosEntrada.ObjEntradaProdutos.CFOP.EdtCFOPKeyDown(Sender,Key,Shift);
end;

procedure TFENTRADAPRODUTOS.edtEdtST_ADblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
  key:=VK_F9;
  ObjObjetosEntrada.ObjDiverso_ep.STA.EdtSTAKeyDown(sender,Key,Shift);
  self.edtEdtST_A.Hint:=get_campoTabela('descricao','codigo','tabtabelaa_st',self.edtEdtST_A.Text);
end;

procedure TFENTRADAPRODUTOS.edtEdtST_BDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
  key:=VK_F9;
  ObjObjetosEntrada.ObjDiverso_ep.STB.EdtSTBKeyDown(Sender,Key,Shift);
  self.edtEdtST_B.Hint:=get_campoTabela('descricao','codigo','tabtabelab_st', self.edtEdtST_B.Text);
end;

procedure TFENTRADAPRODUTOS.edtCSOSNDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
  key:=VK_F9;
  ObjObjetosEntrada.ObjDiverso_ep.CSOSN.edtCSOSNkeydown(Sender,Key,Shift);
  self.edtCSOSN.Hint:=get_campoTabela('descricao','codigo','TABCSOSN',self.edtCSOSN.Text);
end;

procedure TFENTRADAPRODUTOS.edtEdtUnidadeDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
  key:=VK_F9;
  edtEdtUnidadeKeyDown(SENDER,KEY,Shift);
end;

procedure TFENTRADAPRODUTOS.cbb_epKeyPress(Sender: TObject; var Key: Char);
begin
  kEY:=#0;
end;

procedure TFENTRADAPRODUTOS.edtDataKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(key=vk_Space)
  then edtData.Text:=DateToStr(Now);
end;

procedure TFENTRADAPRODUTOS.edtEdtUnidadeKeyPress(Sender: TObject;
  var Key: Char);
begin
  key:=#0;
end;

procedure TFENTRADAPRODUTOS.edtEMISSAONFKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if(key=vk_Space)
  then edtEMISSAONF.Text:=DateToStr(Now);
end;


function TFENTRADAPRODUTOS.VerificaPrecoTotalProdutos(ExcetoLinha:integer):Boolean;
var
  i:Integer;
  ValorTotal,valorProduto,quantidade:Currency;
begin
  Result:=False;
  ValorTotal:=0;
  for i:=1 to StrGridStrgProdutos.RowCount-1 do
  begin
    if(i<>ExcetoLinha)then
    begin
      if(StrGridStrgProdutos.Cells[5,i]<>'')
      then ValorTotal:=ValorTotal+(StrToCurr(tira_ponto(StrGridStrgProdutos.Cells[5,i])));
    end;
  end;

  //ValorTotal := ValorTotal + strtocurr(tira_ponto((formata_valor((StrToCurr(edtvalor_ep.text)*StrToCurr(edtquantidade_ep.Text))))));

  valorProduto := StrToCurrDef(format_si(format_db(edtvalor_ep.Text)),0);
  quantidade   := StrToCurrDef(format_si(format_db(edtquantidade_ep.Text)),0);
  ValorTotal := ValorTotal + (valorProduto*quantidade);

  {29/07/2013 - celio = problema ao comparar os valores. No caso de compra por nfe, os valores dos produtos podem ter at� 4 casas
  decimais, mas ao comparar com o valor inserido no cabe�alho que possui somente 2 casas decimais, o valor inserido sempre ser�
  maior por alguns d�cimos. EX: valor dos produtos na nota: 2.176,51, vamos obtido pela soma: 2.176,5157
  **solu��o: truncar o valor em 2 casas decimais para compara��o}
  ValorTotal := StrToCurr(formata_valor(CurrToStr(ValorTotal),2,False));
  if(ValorTotal>StrToCurr(tira_ponto(formata_valor(edtVALORPRODUTOS.text))))
  then Result:=True;
//  ShowMessage(CurrToStr(ValorTotal));
end;

procedure TFENTRADAPRODUTOS.lbNomeFornecedorClick(Sender: TObject);
var
  FFornecedor:TFFORNECEDOR;
begin
   try
     FFornecedor:=TFFORNECEDOR.Create(nil);
   except
     Exit;
   end;

   try
     if(EdtFornecedor.Text='')
     then Exit;

     FFornecedor.Tag:=StrToInt(EdtFornecedor.Text);
     FFornecedor.ShowModal;
   finally
     FreeAndNil(FFornecedor);
   end;


end;

procedure TFENTRADAPRODUTOS.FormShow(Sender: TObject);
begin
  PageControl1.TabIndex := 0;
  setImgVerificado(0);
end;

procedure TFENTRADAPRODUTOS.limpaCamposProduto;
begin

    lbProdutoXml.Caption := '';
    edtproduto_ep.text:='';
    edtcor_ep.text:='';
    edtgrupopersiana_EP.text:='';
    edtdiametropersiana_EP.text:='';
    edtcreditocofins_produtos.text:='';
    edtcreditopis_produtos.text:='';
    edtcreditoicms_produtos.text:='';
    edtPercentualPis.text:='';
    edtPercentualCofins.text:='';
    edtValorOutros.Text:='';
    edtcreditoipi_produtos.text:='';
    edtdiametropersiana_EP.text:='';
    edtquantidade_ep.text:='';
    edtvalor_ep.text:='';
    edtipipago_produtos.text:='';

    if not (panelTotalRestante.Visible) then
    begin
      edtEdtCFOPProduto.text:='';
    end;


    edtEdtST_A.text:='';
    edtEdtST_B.text:='';
    edtCSOSN.text:='';
    edtdesconto.text:='';
    edtEdtUnidade.text:='';
    edtBaseCalculoICMS_p.text:='';
    edtValorICMS_p.text:='';
    edtValorFreteProduto.text:='';
    edtValorIPI_p.text:='';
    edtPorcentagemReducao.text:='';

end;

procedure TFENTRADAPRODUTOS.edtPercentualCofinsKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (key in['0'..'9',',',#8]) then

      if (key = '.') then
        Key := ','
      else
        Key := #0;
end;

procedure TFENTRADAPRODUTOS.edtPercentualPisKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (key in['0'..'9',',',#8]) then

      if (key = '.') then
        Key := ','
      else
        Key := #0;
end;

function TFENTRADAPRODUTOS.validaSped: boolean;
var
  msgErro,numNfe:string;
  VL_BC_ICMS_ITENS,VL_BC_ICMS_TOTAIS,
  VL_BC_ICMSST_ITENS,VL_BC_ICMSST_TOTAIS,
  VL_ICMSST_ITENS,VL_ICMSST_TOTAIS,
  VL_ICMS_ITENS,VL_ICMS_TOTAIS,
  VL_OUTROS_ITENS,VL_OUTROS_TOTAIS,
  VL_IPI_ITENS,VL_IPI_TOTAIS
  //VL_SEGURO_ITENS,VL_SEGURO_TOTAIS
  :Currency;
begin

  msgErro := '';

  try

    Screen.Cursor := crHourGlass;
    
    if (edtModeloNF.Text = '2') then
    begin

      if (Length(edtChaveAcesso.Text) <> 44) then
          msgErro := msgErro + #10#13+' - Chave da nfe inv�lida.';

    end;

    //validando numero da nf-e conforme chave acesso
    numNfe := Copy(edtChaveAcesso.Text,26,9);
    numNfe := tirazeroesquerda(numNfe);

    if (edtModeloNF.Text = '2') then
      if (numNfe <> tirazeroesquerda(edtNF.Text)) then
        msgErro := msgErro + #10#13+' - N�mero da nf informado n�o confere com a chave de acesso.';


    {VALOR BC ICMS}

    VL_BC_ICMS_ITENS  := StrToCurrDef( get_campoTabela('sum(bc_icms) as vl','entrada','proc_materiais_ep',edtCodigo.Text,'vl'),0 );
    VL_BC_ICMS_TOTAIS := StrToCurrDef(edtBASECALCULOICMS.text,0);

    if (VL_BC_ICMS_ITENS <> VL_BC_ICMS_TOTAIS) then
      msgErro := msgErro + #10#13+' - A soma do campo BC_ICMS dos itens deve ser igual ao BC_ICMS da nota fiscal.';

    {VALOR ICMS}

    VL_ICMS_ITENS  := StrToCurrDef( get_campoTabela('sum(valor_icms) as vl','entrada','proc_materiais_ep',edtCodigo.Text,'vl'),0 );
    VL_ICMS_TOTAIS := StrToCurrDef(edtVALORICMS.text,0);

    if (VL_ICMS_ITENS <> VL_ICMS_TOTAIS) then
      msgErro := msgErro + #10#13+' - A soma do campo VL_ICMS dos itens deve ser igual ao VL_ICMS da nota fiscal.';


    {VALOR BC ICMSST}

    VL_BC_ICMSST_ITENS   := StrToCurrDef(get_campoTabela('sum(bcicmsst) as vl','entrada','proc_materiais_ep',edtCodigo.Text,'vl'),0 );
    VL_BC_ICMSST_TOTAIS  := StrToCurrDef(edtBASECALCULOICMSSUBSTITUICAO.Text,0);

    if (VL_BC_ICMSST_ITENS <> VL_BC_ICMSST_TOTAIS) then
      msgErro := msgErro + #10#13+' - A soma do campo BC ICMSST dos itens deve ser igual ao BC ICMSST da nota fiscal.';

    {VALOR ICMSST}

    VL_ICMSST_ITENS  := StrToCurrDef(get_campoTabela('sum(valoricmsst) as vl','entrada','proc_materiais_ep',edtCodigo.Text,'vl'),0 );
    VL_ICMSST_TOTAIS := StrToCurrDef(edticmssubstituto.Text,0);

    if (VL_ICMSST_ITENS <> VL_ICMSST_TOTAIS) then
      msgErro := msgErro + #10#13+' - A soma do campo VALOR ICMSST dos itens deve ser igual ao VALOR ICMSST da nota fiscal.';

    {VALOR OUTROS}

    VL_OUTROS_ITENS  := StrToCurrDef(get_campoTabela('sum(valoroutros) as vl','entrada','proc_materiais_ep',edtCodigo.Text,'vl'),0 );
    VL_OUTROS_TOTAIS := StrToCurrDef(edtOUTROSGASTOS.Text,0);

    if (VL_OUTROS_ITENS <> VL_OUTROS_TOTAIS) then
      msgErro := msgErro + #10#13+' - A soma do campo VALOROUTROS dos itens deve ser igual ao VALOROUTROS da nota fiscal.';


    {VALOR IPI}

    VL_IPI_ITENS  := StrToCurrDef(get_campoTabela('sum(valor_ipi) as vl','entrada','proc_materiais_ep',edtCodigo.Text,'vl'),0 );
    VL_IPI_TOTAIS := StrToCurrDef(edtvaloripi.Text,0);

    if (VL_IPI_ITENS <> VL_IPI_TOTAIS) then
      msgErro := msgErro + #10#13+' - A soma do campo VALOR IPI dos itens deve ser igual ao VALOR IPI da nota fiscal.';

    (*
    {VALOR SEGURO}
    VL_SEGURO_ITENS  := StrToCurrDef(get_campoTabela('sum(valorseguro) as vl','entrada','proc_materiais_ep',edtCodigo.Text,'vl'),0);
    VL_SEGURO_TOTAIS := StrToCurrDef(edtValorSeguro.Text,0);

    if (VL_SEGURO_ITENS <> VL_SEGURO_TOTAIS) then
      msgErro := msgErro + #10#13+' - A soma do campo VALORSEGURO dos itens deve ser igual ao VALORSEGURO da nota fiscal.'; *)

    if (msgErro <> '') then
      MensagemErro('Erros foram encontrados: '+msgErro);

    Result := (msgErro = '');

    if (result) then
    begin
      exec_sql('update tabentradaprodutos set spedvalido = ''S'' where codigo = '+edtCodigo.Text);
      setImgVerificado(1);
    end
    else
    begin
      exec_sql('update tabentradaprodutos set spedvalido = ''N'' where codigo = '+edtCodigo.Text);
      setImgVerificado(0);
    end;

    FDataModulo.IBTransaction.CommitRetaining;

  finally
    Screen.Cursor := crDefault;
  end;

end;

function TFENTRADAPRODUTOS.validaSpedItens: boolean;
var
  msgErro:string;
  nomeproduto:string;
begin

  try

    Screen.Cursor := crHourGlass;
    
    nomeproduto := Copy(cbb_ep.Text,1,3) + edtproduto_ep.Text+'COR'+edtcor_ep.Text;

    if (edtEdtST_B.Text <> '') and (edtcsosn.Text <> '') then
      msgErro := msgErro + #10#13+' - CST e CSOSN n�o podem estar preenchidos ao mesmo tempo. Produto: '+nomeproduto;

    if (edtEdtST_B.Text = '') and (edtcsosn.Text = '') then
      msgErro := msgErro + #10#13+' - CST ou CSOSN devem estar preenchidos. Produto: '+nomeproduto;

    if (edtEdtST_B.Text = '0') then
    begin

      if (StrToCurrDef(edtcreditoicms_produtos.Text,0) <= 0) or (StrToCurrDef(edtBaseCalculoICMS_p.text,0) <= 0) or (StrToCurrDef(edtValorICMS_p.Text,0) <= 0) then
        msgErro := msgErro + #10#13+' - Para CST 0 os seguintes campos devem ser maiores que zero:  % ICMS, BC_ICMS e VALOR ICMS. Produto: '+nomeproduto;

    end
    else if (edtEdtST_B.Text = '60') then
    begin

      if (StrToCurr(edtcreditoicms_produtos.Text) > 0) or (StrToCurr(edtBaseCalculoICMS_p.Text) > 0) or (StrToCurr(edtValorICMS_p.Text) > 0) then
        msgErro := msgErro + #10#13+' - Para CST 60 os seguintes campos devem ser iguais a zero:  % ICMS, BC_ICMS e VALOR ICMS. Produto: '+nomeproduto;

    end;

    if ( (StrToCurrDef(edtBaseCalculoICMS_p.Text,0) > 0) or (StrToCurrDef(edtcreditoicms_produtos.Text,0) > 0) or (StrToCurrDef(edtValorICMS_p.Text,0) > 0)  ) then
    begin

      if (StrToCurrDef(edtBaseCalculoICMS_p.Text,0) <= 0) then
        msgErro := msgErro + #10#13+' - BC ICMS deve ser preenchido'
      else if (StrToCurrDef(edtcreditoicms_produtos.Text,0) <= 0) then
        msgErro := msgErro + #10#13+' - % ICMS deve ser preenchido'
      else if (StrToCurrDef(edtValorICMS_p.Text,0) <= 0) then
        msgErro := msgErro + #10#13+' - VALOR ICMS deve ser preenchido';

    end;

    if (edtEdtCFOPProduto.Text <> '') then
    begin

      if (edtEdtCFOPProduto.Text[1] <> '1') and (edtEdtCFOPProduto.Text[1] <> '2') and (edtEdtCFOPProduto.Text[1] <> '3') then
        msgErro := msgErro + #10#13+' - CFOP inv�lido para entrada de mercadorias. Nos itens';

    end
    else
      msgErro := msgErro + #10#13+' - CFOP n�o informado';

    if (edtcfop.Text <> '') then
    begin

      if (edtcfop.Text[1] <> '1') and (edtcfop.Text[1] <> '2') and (edtcfop.Text[1] <> '3') then
        msgErro := msgErro + #10#13+' - CFOP inv�lido para entrada de mercadorias. No cabe�alho da nota';

    end;

    if (StrToCurrDef(edtValorIPI_p.Text,0) > 0) then
    begin

      if (edtCstIPI.Text = '') then
        msgErro := msgErro + #10#13+' - CST IPI deve ser preenchido';

      if (StrToCurrDef(edtcreditoipi_produtos.Text,0) <= 0) then
        msgErro := msgErro + #10#13+' - Cr�d. IPI % deve ser preenchido';

    end;

    if (msgErro <> '') then
      MensagemErro('Erros foram encontrados: '+msgErro);

    Result := (msgErro = '');

  finally
    Screen.Cursor := crDefault;
  end

end;

procedure TFENTRADAPRODUTOS.edtValorOutrosKeyPress(Sender: TObject;var Key: Char);
begin
    if not (key in['0'..'9',',',#8]) then

      if (key = '.') then
        Key := ','
      else
        Key := #0;
end;



function TFENTRADAPRODUTOS.verificaEntrada: Boolean;
var
  msgErro,codigos:string;
begin
  msgErro := '';
  codigos := '';
  query.Database := FDataModulo.IBDatabase;
  query.Close;
  query.SQL.Clear;

  query.SQL.Text :=
  'select e.codigo '+
  'from tabentradaprodutos e '+
  'where (spedvalido <> ''X'') and (spedvalido = ''N'') ';

  query.Active := True;
  query.FetchAll;

  if (query.RecordCount > 0) then
  begin

    msgErro := 'Voc� deve validar as seguintes entradas antes de prosseguir: ';

    query.First;
    while not query.Eof do
    begin
      codigos := codigos + query.Fields[0].AsString+', ';
      query.Next;
    end;

    msgErro := msgErro + Copy(codigos,1,length(codigos)-2) + '.';

    ShowMessage(msgErro);

    Result := False;

  end
  else
    result := True;


end;

procedure TFENTRADAPRODUTOS.setImgVerificado(imgIndex:SmallInt);
begin

  imgVerificado.Picture := nil;
  ImageList1.GetBitmap(imgIndex,imgVerificado.Picture.Bitmap);
  Application.ProcessMessages;

  if (imgIndex = 0) then
    lbVerificado.caption := 'Entrada inv�lida para Sped'
  else
    lbVerificado.caption := 'Entrada v�lida para Sped';

end;

procedure TFENTRADAPRODUTOS.imgValidaSpedClick(Sender: TObject);
var
  contItens:SmallInt;
begin

  if (edtCodigo.Text <> '') then
  begin

    contItens := StrToInt(get_campoTabela('COUNT(codigo) total','entrada','proc_materiais_ep',edtCodigo.Text,'total'));

    if edtCodigo.Text = '' then
      ShowMessage('Voc� deve pesquisar uma entrada')
    else
    if (contItens <= 0) then
      ShowMessage('Voc� deve inserir itens para a entrada antes de validar')
    else
      validaSped;

  end


end;

procedure TFENTRADAPRODUTOS.edtBaseCalculoICMS_pExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtcreditoicms_produtosExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtPorcentagemReducaoExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtValorICMS_pExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtChaveAcessoKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
  openDialog1:TOpenDialog;
begin


  if key = vk_f9 then
  begin

    if (edtModeloNF.Text <> '2') then
    begin
      MensagemAviso('Dispon�vel apenas para modelo NF-e');
      Exit;
    end;

    openDialog1 := TOpenDialog.Create(nil);
    openDialog1.Filter := 'Arquivos XML (*.xml)';

    if openDialog1.Execute() then
    begin

      if (objExtraiXML <> nil) then
        FreeAndNil(objExtraiXML);


      objExtraiXML := TobjExtraiXML.create;

      objExtraiXML.submit_caminhoArquivo (openDialog1.FileName);
      objExtraiXML.submit_XMLDocument (Self.XMLDocument1);

      objExtraiXML.retornaDados ();
      contProdutos := objExtraiXML.get_totalprodutos ();
      lbTotalRestante.Caption := IntToStr(indexItem+1)+' de '+IntToStr(contProdutos);

      edtChaveAcesso.text := RetornaValorCampos (openDialog1.FileName,'chNFe');
      edtNF.Text          := objExtraiXML.IDE.nNF;
      edtSerieNF.Text     := objExtraiXML.IDE.serie;
      edtEMISSAONF.Text   := fDate(objExtraiXML.IDE.dEmi);
      edtData.Text        := DateToStr(date);

      edtVALORPRODUTOS.Text               := format_si(objExtraiXML.ICMSTot.vProd);
      edtValorFinal.Text                  := format_si(objExtraiXML.ICMSTot.vNF);
      edtVALORFRETE.Text                  := format_si(objExtraiXML.ICMSTot.vFrete);
      edtDESCONTOS.Text                   := format_si(objExtraiXML.ICMSTot.vDesc);
      edtOUTROSGASTOS.Text                := format_si(objExtraiXML.ICMSTot.vOutro);
      edtvaloripi.Text                    := format_si(objExtraiXML.ICMSTot.vIPI);
      edtBASECALCULOICMS.Text             := format_si(objExtraiXML.ICMSTot.vBC);
      edtVALORICMS.Text                   := format_si(objExtraiXML.ICMSTot.vICMS);
      edtBASECALCULOICMSSUBSTITUICAO.Text := format_si(objExtraiXML.ICMSTot.vBCST);
      edticmssubstituto.Text              := format_si(objExtraiXML.ICMSTot.vST);

      panelTotalRestante.Visible := True;
      imgOk.Visible := False;

    end

  end;


end;

procedure TFENTRADAPRODUTOS.edtAliquotaSTKeyPress(Sender: TObject; var Key: Char);
begin
    if not (key in['0'..'9',',',#8]) then

      if (key = '.') then
        Key := ','
      else
        Key := #0;
end;

procedure TFENTRADAPRODUTOS.edtBCICMSSTKeyPress(Sender: TObject; var Key: Char);
begin
    if not (key in['0'..'9',',',#8]) then

      if (key = '.') then
        Key := ','
      else
        Key := #0;
end;


procedure TFENTRADAPRODUTOS.edtValorICMSSTKeyPress(Sender: TObject; var Key: Char);
begin
    if not (key in['0'..'9',',',#8]) then

      if (key = '.') then
        Key := ','
      else
        Key := #0;
end;

procedure TFENTRADAPRODUTOS.edtAliquotaSTExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtBCICMSSTExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtValorICMSSTExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtCstPisKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
  objImpostoPis:TObjIMPOSTO_PIS;
begin

  if Key = vk_f9 then
  begin
    objImpostoPis := TObjIMPOSTO_PIS.Create;
    objImpostoPis.EdtSTKeyDown(Sender,Key,Shift,nil);
    objImpostoPis.Free;
  end;

end;

procedure TFENTRADAPRODUTOS.edtCstCofinsKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
  objImpostoCofins:TObjIMPOSTO_COFINS;
begin

  if Key = vk_f9 then
  begin
    objImpostoCofins := TObjIMPOSTO_COFINS.Create;
    objImpostoCofins.EdtSTKeyDown(Sender,Key,Shift,nil);
    objImpostoCofins.Free;
  end;

end;

procedure TFENTRADAPRODUTOS.edtCstIPIKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
  objImpostoIPI:TObjIMPOSTO_IPI;
begin

  if Key = vk_f9 then
  begin
    objImpostoIPI := TObjIMPOSTO_IPI.Create;
    objImpostoIPI.EdtSTKeyDown(Sender,Key,Shift,nil);
    objImpostoIPI.Free;
  end;

end;

procedure TFENTRADAPRODUTOS.edtdescontoExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';

  if (TEdit(Sender).Text <> '') then
    TEdit(Sender).Text := tira_ponto(formata_valor_4_casas(TEdit(Sender).Text));
end;

procedure TFENTRADAPRODUTOS.edtPercentualPisExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtcreditopis_produtosExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtPercentualCofinsExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtcreditocofins_produtosExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtcreditoipi_produtosExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtValorIPI_pExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtipipago_produtosExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtValorFreteProdutoExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.edtValorOutrosExit(Sender: TObject);
begin
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '0,00';
end;

procedure TFENTRADAPRODUTOS.getItemXML;
begin

  if Assigned(objExtraiXML.produtos[indexItem]) then
  begin

    limpaCamposProduto;

    lbTotalRestante.Caption := IntToStr(indexItem+1)+' de '+IntToStr(contProdutos);

    //PRODUTOS/ICMS

    lbProdutoXml.Caption   := objExtraiXML.produtos[indexItem].xProd;
    edtEdtUnidade.Text     := objExtraiXML.produtos[indexItem].uCom;
    edtquantidade_ep.Text  := formata_valor_4_casas(format_si(objExtraiXML.produtos[indexItem].qCom));
    edtvalor_ep.Text       := formata_valor_4_casas(format_si(objExtraiXML.produtos[indexItem].vUnCom));
    edtdesconto.Text       := formata_valor_4_casas(format_si(objExtraiXML.produtos[indexItem].vDesc));
    edtEdtST_A.Text        := objExtraiXML.produtos[indexItem].ICMS.orig;

    if (objExtraiXML.Emit.CRT = '1') then
      edtCSOSN.Text   := objExtraiXML.produtos[indexItem].ICMS.CSOSN
    else
    begin
      edtEdtST_B.Text := objExtraiXML.produtos[indexItem].ICMS.CST;
      if (edtEdtST_B.Text = '00')then
        edtEdtST_B.Text := '0';
    end;

    edtBaseCalculoICMS_p.Text    := format_si(objExtraiXML.produtos[indexItem].ICMS.vBC);
    edtcreditoicms_produtos.Text := format_si(objExtraiXML.produtos[indexItem].ICMS.pICMS);
    edtPorcentagemReducao.Text   := format_si(objExtraiXML.produtos[indexItem].ICMS.pRedBC);
    edtValorICMS_p.Text          := format_si(objExtraiXML.produtos[indexItem].ICMS.vICMS);

    //ICMS ST

    edtAliquotaST.Text   := format_si(objExtraiXML.produtos[indexItem].ICMS.pICMSST);
    edtBCICMSST.Text     := format_si(objExtraiXML.produtos[indexItem].ICMS.vBCST);
    edtValorICMSST.Text  := format_si(objExtraiXML.produtos[indexItem].ICMS.vICMSST);

    //PIS/COFINS

    edtPercentualPis.Text        := format_si(objExtraiXML.produtos[indexItem].PIS.pPIS);
    edtcreditopis_produtos.Text  := format_si(objExtraiXML.produtos[indexItem].PIS.vPIS);

    edtPercentualCofins.Text         :=  format_si(objExtraiXML.produtos[indexItem].COFINS.pCOFINS);
    edtcreditocofins_produtos.Text   :=  format_si(objExtraiXML.produtos[indexItem].COFINS.vCOFINS);

    //IPI

    edtBCIPI.Text               := format_si(objExtraiXML.produtos[indexItem].IPI.IPITrib.vBC);
    edtcreditoipi_produtos.Text := format_si(objExtraiXML.produtos[indexItem].IPI.IPITrib.pIPI);
    edtValorIPI_p.Text          := format_si(objExtraiXML.produtos[indexItem].IPI.IPITrib.vIPI);

    //OUTROS

    edtValorFreteProduto.Text   := format_si(objExtraiXML.produtos[indexItem].vFrete);
    edtValorOutros.Text         := format_si(objExtraiXML.produtos[indexItem].vOutro);

  end;

end;

procedure TFENTRADAPRODUTOS.SpeedButton4Click(Sender: TObject);
begin

  if (indexItem < contProdutos-1) then
  begin
    indexItem := indexItem + 1;
    getItemXML();
  end;

end;

procedure TFENTRADAPRODUTOS.SpeedButton3Click(Sender: TObject);
begin

  if (indexItem > 0) then
  begin
    indexItem := indexItem - 1;
    getItemXML();
  end;

end;

procedure TFENTRADAPRODUTOS.edtvalor_epExit(Sender: TObject);
begin
  if (TEdit(Sender).Text <> '') then
    TEdit(Sender).Text := tira_ponto(formata_valor_4_casas(TEdit(Sender).Text));
end;

function TFENTRADAPRODUTOS.excluirItens: boolean;
var
  i:Integer;
begin

  try

    Screen.Cursor := crHourGlass;

    for i:=1 to StrGridStrgProdutos.RowCount-1 do
    begin

      if (Trim(StrGridStrgProdutos.Cells[6,i]) <> '') then
      begin
        StrGridStrgProdutos.Row := 1;
        Application.ProcessMessages;
        BtExcluir_EpClick(btexcluir);
      end;

    end;

  finally
    Screen.Cursor := crDefault;
  end;


end;

end.
