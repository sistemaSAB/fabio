unit UobjHORAEXTRA;
Interface
Uses UobjFuncionarios,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal;

Type
   TObjHORAEXTRA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               FUNCIONARIO:TOBJFUNCIONARIOS ;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_VALOR(parametro: string);
                Function Get_VALOR: string;
                Procedure Submit_DATA(parametro: string);
                Function Get_DATA: string;
                Procedure Submit_DATASALARIO(parametro: string);
                Function Get_DATASALARIO: string;
                procedure EdtFUNCIONARIOExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFUNCIONARIOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtFUNCIONARIOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
                //CODIFICA DECLARA GETSESUBMITS


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               VALOR:string;
               DATA:string;
               DATASALARIO:string;
//CODIFICA VARIAVEIS PRIVADAS






               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Procedure ImprimeAnalitico;
                Procedure ImprimeSintetico;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls

, UMenuRelatorios, UReltxtRDPRINT,rdprint;


{ TTabTitulo }


Function  TObjHORAEXTRA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('FUNCIONARIO').asstring<>'')
        Then Begin
                 If (Self.FUNCIONARIO.LocalizaCodigo(FieldByName('FUNCIONARIO').asstring)=False)
                 Then Begin
                          Messagedlg('Funcion�rio N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.FUNCIONARIO.TabelaparaObjeto;
        End;
        Self.VALOR:=fieldbyname('VALOR').asstring;
        Self.DATA:=fieldbyname('DATA').asstring;
        Self.DATASALARIO:=fieldbyname('DATASALARIO').asstring;
//CODIFICA TABELAPARAOBJETO





        result:=True;
     End;
end;


Procedure TObjHORAEXTRA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('FUNCIONARIO').asstring:=Self.FUNCIONARIO.GET_CODIGO;
        ParamByName('VALOR').asstring:=virgulaparaponto(Self.VALOR);
        ParamByName('DATA').asstring:=Self.DATA;
        ParamByName('DATASALARIO').asstring:=Self.DATASALARIO;
//CODIFICA OBJETOPARATABELA





  End;
End;

//***********************************************************************

function TObjHORAEXTRA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjHORAEXTRA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        FUNCIONARIO.ZerarTabela;
        VALOR:='';
        DATA:='';
        DATASALARIO:='';
//CODIFICA ZERARTABELA





     End;
end;

Function TObjHORAEXTRA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (FUNCIONARIO.get_codigo='')
      Then Mensagem:=mensagem+'/Funcion�rio';
      If (VALOR='')
      Then Mensagem:=mensagem+'/Valor';
      If (DATA='')
      Then Mensagem:=mensagem+'/Data';
      If (DATASALARIO='')
      Then Mensagem:=mensagem+'/Data do Sal�rio';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjHORAEXTRA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.FUNCIONARIO.LocalizaCodigo(Self.FUNCIONARIO.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Funcion�rio n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjHORAEXTRA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.FUNCIONARIO.Get_Codigo<>'')
        Then Strtoint(Self.FUNCIONARIO.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Funcion�rio';
     End;
     try
        Strtofloat(Self.VALOR);
     Except
           Mensagem:=mensagem+'/Valor';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjHORAEXTRA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.DATA);
     Except
           Mensagem:=mensagem+'/Data';
     End;
     try
        Strtodate(Self.DATASALARIO);
     Except
           Mensagem:=mensagem+'/Data do Sal�rio';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjHORAEXTRA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjHORAEXTRA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro HORAEXTRA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,FUNCIONARIO,VALOR,DATA,DATASALARIO');
           SQL.ADD(' from  TABHORAEXTRA');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjHORAEXTRA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjHORAEXTRA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjHORAEXTRA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.FUNCIONARIO:=TOBJFUNCIONARIOS .create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABHORAEXTRA(CODIGO,FUNCIONARIO,VALOR,DATA');
                InsertSQL.add(' ,DATASALARIO)');
                InsertSQL.add('values (:CODIGO,:FUNCIONARIO,:VALOR,:DATA,:DATASALARIO');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABHORAEXTRA set CODIGO=:CODIGO,FUNCIONARIO=:FUNCIONARIO');
                ModifySQL.add(',VALOR=:VALOR,DATA=:DATA,DATASALARIO=:DATASALARIO');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABHORAEXTRA where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjHORAEXTRA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjHORAEXTRA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewHORAEXTRA');
     Result:=Self.ParametroPesquisa;
end;

function TObjHORAEXTRA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de HORAEXTRA ';
end;


function TObjHORAEXTRA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENHORAEXTRA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENHORAEXTRA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjHORAEXTRA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.FUNCIONARIO.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjHORAEXTRA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjHORAEXTRA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjHORAEXTRA.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjHORAEXTRA.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjHORAEXTRA.Submit_VALOR(parametro: string);
begin
        Self.VALOR:=Parametro;
end;
function TObjHORAEXTRA.Get_VALOR: string;
begin
        Result:=Self.VALOR;
end;
procedure TObjHORAEXTRA.Submit_DATA(parametro: string);
begin
        Self.DATA:=Parametro;
end;
function TObjHORAEXTRA.Get_DATA: string;
begin
        Result:=Self.DATA;
end;
procedure TObjHORAEXTRA.Submit_DATASALARIO(parametro: string);
begin
        Self.DATASALARIO:=Parametro;
end;
function TObjHORAEXTRA.Get_DATASALARIO: string;
begin
        Result:=Self.DATASALARIO;
end;
//CODIFICA GETSESUBMITS


procedure TObjHORAEXTRA.EdtFUNCIONARIOExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.FUNCIONARIO.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.FUNCIONARIO.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.FUNCIONARIO.GET_NOME;
End;
procedure TObjHORAEXTRA.EdtFUNCIONARIOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.FUNCIONARIO.Get_Pesquisa,Self.FUNCIONARIO.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FUNCIONARIO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.FUNCIONARIO.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FUNCIONARIO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjHORAEXTRA.EdtFUNCIONARIOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.FUNCIONARIO.Get_Pesquisa,Self.FUNCIONARIO.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FUNCIONARIO.RETORNACAMPOCODIGO).asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

//CODIFICA EXITONKEYDOWN

procedure TObjHORAEXTRA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJHORAEXTRA';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Horas Extra - Anal�tico');
                items.add('Horas Extra - Sint�tico');

          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:Self.ImprimeAnalitico;
            1:Self.ImprimeSintetico;
          End;
     end;

end;



procedure TObjHORAEXTRA.ImprimeAnalitico;
var
PData1,PData2,PDataSalario,PFuncionario:string;
Psomafuncionario,Psomatotal:currency;
PseparadoFuncionario:Boolean;
begin
     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Ordenado por Data');
          RgOpcoes.Items.add('Separado por funcion�rio');
          Showmodal;
          if (Tag=0)
          Then exit;

          if (RgOpcoes.ItemIndex=1)
          Then PseparadoFuncionario:=true
          Else PseparadoFuncionario:=False;
     End;




     With FfiltroImp do
     begin

          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;
          Grupo04.Enabled:=true;
          LbGrupo01.caption:='Data Inicial';
          LbGrupo02.caption:='Data Final';
          LbGrupo03.caption:='Data do Sal�rio';
          LbGrupo04.caption:='Funcion�rio';
          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;
          edtgrupo03.EditMask:=MascaraData;
          edtgrupo04.OnKeyDown:=Self.edtFuncionarioKeyDown;
          
          Showmodal;

          if (Tag=0)
          Then exit;

          Pdata1:='';
          Pdata2:='';
          PDataSalario:='';
          PFuncionario:='';

          Try
             if (Trim(comebarra(edtgrupo01.Text))<>'')
             Then Begin
                      strtodate(edtgrupo01.Text);
                      Pdata1:=edtgrupo01.Text;
             End;
          Except

          End;

          Try
             if (Trim(comebarra(edtgrupo02.Text))<>'')
             Then Begin
                      strtodate(edtgrupo02.Text);
                      Pdata2:=edtgrupo02.Text;
             End;
          Except

          End;

          Try
             if (Trim(comebarra(edtgrupo03.Text))<>'')
             Then Begin
                      strtodate(edtgrupo03.Text);
                      PDataSalario:=edtgrupo03.Text;
             End;
          Except

          End;

          Try
             if (edtgrupo04.text<>'')
             Then begin
                     strtoint(edtgrupo04.Text);
                     PFuncionario:=edtgrupo04.Text;
                     if (Self.FUNCIONARIO.LocalizaCodigo(PFuncionario)=False)
                     then Begin
                             Messagedlg('Funcion�rio n�o localizado',mterror,[mbok],0);
                             exit;
                     End;
                     Self.FUNCIONARIO.TabelaparaObjeto;
             End;
          Except

          End;

     End;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select * from ViewHoraExtra');
          sql.add('where codigo<>-100');//apenas where

          if (PData1<>'')
          Then sql.add('and Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39);

          if (PData2<>'')
          Then sql.add('and Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39);

          if (PDataSalario<>'')
          Then sql.add('and DataSalario='+#39+formatdatetime('mm/dd/yyyy',strtodate(PDataSalario))+#39);

          if (PFuncionario<>'')
          Then sql.add('and Funcionario='+PFuncionario);

          if (PseparadoFuncionario=True)
          Then sql.add('order by Funcionario,Data,DataSalario')
          Else sql.add('order by Data,DataSalario');
          
          open;

          if (recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma Informa��o foi selecionada',mtInformation,[mbok],0);
                    exit;
          End;
          With FreltxtRDPRINT do
          Begin

              ConfiguraImpressao;
              RDprint.Abrir;
              if (RDprint.Setup=False)
              Then Begin
                        RDprint.Fechar;
                        exit;
              End;
              LinhaLocal:=3;
              RDprint.ImpC(LinhaLocal,45,'RELAT�RIO DE HORAS EXTRAS',[negrito]);
              IncrementaLinha(2);

              if (PData1<>'')
              Then Begin
                        if (PData2='')
                        then RDprint.ImpF(LinhaLocal,1,'Data: '+Pdata1,[negrito])
                        Else RDprint.ImpF(LinhaLocal,1,'Data: '+Pdata1+' a '+Pdata2,[negrito]);
                        IncrementaLinha(1);
              End;

              if (PDataSalario<>'')
              Then Begin
                        RDprint.ImpF(LinhaLocal,1,'Data do Sal�rio: '+PDataSalario,[negrito]);
                        IncrementaLinha(1);
              End;

              if (PFuncionario<>'')
              Then Begin
                        RDprint.ImpF(LinhaLocal,1,'Funcion�rio: '+'-'+PFuncionario+Self.FUNCIONARIO.Get_Nome,[negrito]);
                        IncrementaLinha(1);
              End;
              //cabecalho das colunas
              IncrementaLinha(1);
              RDprint.ImpF(LinhaLocal,1,CompletaPalavra('FUNCION�RIO',30,' ')+' '+
                                   CompletaPalavra('DATA',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda('VALOR',12,' ')+' '+
                                   CompletaPalavra('DATA SAL.',10,' ')+' '+
                                   CompletaPalavra('COD.FOLHA',9,' ')+' '+
                                   CompletaPalavra('CODIGO',6,' '),[negrito]);
              IncrementaLinha(1);
              RDprint.Imp(LinhaLocal,1,CompletaPalavra('_',90,'_'));
              IncrementaLinha(2);
              PFuncionario:=Fieldbyname('funcionario').asstring;
              PsomaFuncionario:=0;
              PsomaTotal:=0;

              While not(eof) do
              begin
                   if ((PFuncionario<>Fieldbyname('funcionario').asstring)
                   and (PseparadoFuncionario=true))
                   Then Begin//trocou o funcionario
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,1,'TOTAL DO FUNCION�RIO'+CompletaPalavra_a_Esquerda(Formata_valor(PsomaFuncionario),34,' '),[negrito]);
                             IncrementaLinha(2);
                             PFuncionario:=Fieldbyname('funcionario').asstring;
                             PsomaFuncionario:=0;
                   End;

                   VerificaLinha;
                   RDprint.Imp(LinhaLocal,1,CompletaPalavra(fieldbyname('FUNCIONaRIO').asstring+'-'+fieldbyname('nome').asstring,30,' ')+' '+
                                   CompletaPalavra(fieldbyname('DATA').asstring,10,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR').asstring),12,' ')+' '+
                                   CompletaPalavra(fieldbyname('DATASALario').asstring,10,' ')+' '+
                                   CompletaPalavra_a_Esquerda(fieldbyname('folhapagamento').asstring,9,' ')+' '+
                                   CompletaPalavra_a_Esquerda(fieldbyname('codigo').asstring,6,' '));
                  IncrementaLinha(1);
                  Psomafuncionario:=Psomafuncionario+fieldbyname('valor').asfloat;
                  Psomatotal:=Psomatotal+fieldbyname('valor').asfloat;
                  Self.Objquery.Next;
              End;//while

              if (PseparadoFuncionario=true)
              Then Begin//trocou o funcionario
                        VerificaLinha;
                        RDprint.Impf(LinhaLocal,1,'TOTAL DO FUNCION�RIO'+CompletaPalavra_a_Esquerda(Formata_valor(PsomaFuncionario),34,' '),[negrito]);
                        IncrementaLinha(2);
                        PFuncionario:=Fieldbyname('funcionario').asstring;
                        PsomaFuncionario:=0;
              End;


              VerificaLinha;
              RDprint.Imp(LinhaLocal,1,CompletaPalavra('_',90,'_'));
              IncrementaLinha(1);

              VerificaLinha;
              RDprint.Impf(LinhaLocal,1,'SOMA TOTAL R$ '+Formata_valor(Psomatotal),[negrito]);
              IncrementaLinha(1);

              
              FreltxtRDPRINT.RDprint.Fechar;
          End;

     End;


end;

procedure TObjHORAEXTRA.ImprimeSintetico;
begin

end;

end.



