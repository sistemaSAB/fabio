unit UobjObjetosEntrada;
interface
uses rdprint,stdctrls,windows,controls,classes,dialogs,Grids,Uessencialglobal,UobjDiverso_ep,UobjPersiana_ep,uobjferragem_ep,uobjperfilado_ep,
uobjvidro_ep,uobjkitbox_ep,Uobjentradaprodutos,ibquery;

Type
    TobjObjetosEntrada=class
          Public

                ObjEntradaProdutos:TobjEntradaProdutos;
                ObjDiverso_ep   :TObjDiverso_ep   ;
                ObjPersiana_ep  :TObjPersiana_ep  ;
                Objferragem_ep  :TObjferragem_ep  ;
                Objperfilado_ep :TObjperfilado_ep ;
                Objvidro_ep     :TObjvidro_ep     ;
                Objkitbox_ep    :TObjkitbox_ep    ;

                constructor create(Owner:TComponent);
                destructor  free;
                Procedure Atualizagrid(PStringGrid:TStringGrid;Pentrada:string;PtipoColunas:TStrings);
                Procedure RetornaQuantidade_e_soma_Produtos(Pentrada:String; var Pquantidade:String;var Pvalor: String);
                Function  AtualizaCusto(tabela: String;Pcodigo: string;Ppreco:String): Boolean;
                procedure Imprime(Pcodigo: string);
                procedure EdtCodigoMaterialKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
          Private
              Objquery:TibQuery;
              Owner:TComponent;
              
              Procedure ImprimeCompraporProduto;
              procedure ImprimeCompraporProduto_com_creditos;


    End;

implementation

uses UDataModulo, SysUtils, UMenuRelatorios, UFiltraImp, Upesquisa,
  UReltxtRDPRINT;

{ TobjObjetosEntrada }

function TobjObjetosEntrada.AtualizaCusto(tabela: String;Pcodigo: string;Ppreco:String): Boolean;
var
pprecoatual:currency;
begin
     result:=False;
     //atualiza o preco de custo do produto comprado
     With Self.Objquery do
     begin
          close;
          sql.clear;
          sql.add('Select precocusto from '+tabela+' where codigo='+Pcodigo);
          open;
          PprecoAtual:=fieldbyname('precocusto').asfloat;

          if (pprecoatual<strtofloat(ppreco))
          Then Begin
                  close;
                  sql.clear;
                  sql.add('Update '+tabela+' set precocusto='+virgulaparaponto(ppreco));
                  sql.add('where codigo='+Pcodigo);
                  try
                      open;
                      FdataModulo.IBTransaction.CommitRetaining;
                      result:=True;
                  Except
                        mensagemerro('Erro na tentativa de atualizar o pre�o do(a) '+tabela);
                        exit;
                  End;
          End;
     End;
end;


procedure TobjObjetosEntrada.Atualizagrid(PStringGrid: TStringGrid;
  Pentrada: string;PtipoColunas:TStrings);
var
cont:integer;
begin
     PStringGrid.ColCount:=25;
     PStringGrid.RowCount:=2;
     PStringGrid.FixedRows:=1;
     PStringGrid.FixedCols:=0;
     PStringGrid.Cols[0].clear;
     PStringGrid.cols[1].clear;



     For cont:=0 to PStringGrid.colcount-1 do
     Begin
          PStringGrid.cols[cont].clear;
     End;

     ptipocolunas.clear;

     ptipocolunas.add('STRING');
     PStringGrid.CELLS[0,0]:='TIPO';
     ptipocolunas.add('string');
     PStringGrid.Cells[1,0]:='PRODUTO';
     ptipocolunas.add('string');
     PStringGrid.CELLS[2,0]:='COR';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[3,0]:='QUANTIDADE';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[4,0]:='VALOR';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[5,0]:='VALOR FINAL';
     ptipocolunas.add('integer');
     PStringGrid.CELLS[6,0]:='CODIGO';
     ptipocolunas.add('integer');
     PStringGrid.CELLS[7,0]:='ORDEM';

     ptipocolunas.add('decimal');
     PStringGrid.CELLS[8,0]:='CR.IPI';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[9,0]:='CR.ICMS';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[10,0]:='CR.PIS';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[11,0]:='CR.COFINS';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[12,0]:='IPI PG';

     ptipocolunas.add('decimal');
     PStringGrid.CELLS[13,0]:='CFOP';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[14,0]:='CSOSN';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[15,0]:='ORIGEM';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[16,0]:='CST';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[17,0]:='DESCONTO';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[18,0]:='UNIDADE';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[19,0]:='CODIGOMATERIAlCOR';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[20,0]:='BC.ICMS';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[21,0]:='VALOR ICMS';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[22,0]:='VALOR FRETE';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[23,0]:='VALOR IPI';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[24,0]:='RED.BASE.ICMS';



     with PStringGrid do
     begin
          ColWidths[0]:=100;
          ColWidths[1]:=300;
          ColWidths[2]:=100;
          ColWidths[3]:=100;
          ColWidths[4]:=100;
          ColWidths[5]:=100;
          ColWidths[6]:=50;
          ColWidths[7]:=100;
          ColWidths[8]:=100;
          ColWidths[9]:=100;
          ColWidths[10]:=100;
          ColWidths[11]:=100;
          ColWidths[12]:=100;
          ColWidths[13]:=100;
          ColWidths[14]:=100;
          ColWidths[15]:=100;
          ColWidths[16]:=100;
          ColWidths[17]:=100;
          ColWidths[18]:=100;
          ColWidths[19]:=50;
          ColWidths[20]:=100;
          ColWidths[21]:=100;
          ColWidths[22]:=100;
          ColWidths[23]:=100;
          ColWidths[24]:=100;
     end;

      //AjustaLArguraColunaGrid(PStringGrid);

     if (Pentrada='')
     Then Exit;

     If (Self.ObjEntradaProdutos.LocalizaCodigo(Pentrada)=False)
     then Begin
               Messagedlg('Entrada n�o localizado',mterror,[mbok],0);
               exit;
     End;


     With Self.Objquery do
     Begin
          //Selecionando diversos 
          close;
          sql.clear;
          sql.add('Select  TabDiverso_EP.codigo,TabDiverso_EP.ordeminsercao,TabDiverso_EP.quantidade,TabDiverso_EP.valor,TabDiverso_EP.valorfinal,');
          SQL.Add('TabDiverso_EP.bcicms,TabDiverso_EP.valoricms,TabDiverso_EP.valorfrete,TabDiverso_EP.valoripi,TabDiverso_EP.redbaseicms') ;
          sql.add(',tabCor.Descricao as NomeCor,TabDiverso.Descricao as NOME,TabDiverso_EP.creditoipi,TabDiverso_EP.ipipago,TabDiverso_EP.creditoicms,TabDiverso_EP.creditopis,TabDiverso_EP.creditocofins');
          sql.add(',TabDiverso_EP.cfop,TabDiverso_EP.csosn,TabDiverso_EP.csta,TabDiverso_EP.cstb,TabDiverso_EP.unidade,TabDiverso_EP.Desconto,TabDiverso_EP.Diversocor from TabDiverso_EP');
          sql.add('join tabDiversoCor on tabDiverso_ep.DiversoCor=TabDiversoCor.codigo');
          sql.add('join tabdiverso on TabDiversoCor.Diverso=TabDiverso.codigo');
          sql.add('join tabCor on TabDiversoCor.Cor=tabCor.codigo');
          sql.add('where TabDiverso_Ep.Entrada='+Pentrada);
          sql.add('order by TabDiverso_EP.OrdemInsercao');
          open;
          While not(eof) do
          Begin
               PStringGrid.Cells[0,PStringGrid.rowcount-1]:='DIVERSO';
               PStringGrid.Cells[1,PStringGrid.rowcount-1]:=Fieldbyname('nome').asstring;
               PStringGrid.Cells[2,PStringGrid.rowcount-1]:=Fieldbyname('nomecor').asstring;
               PStringGrid.Cells[3,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('quantidade').asstring),12,' ');
               PStringGrid.Cells[4,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('valor').asstring),12,' ');
               PStringGrid.Cells[5,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('valorfinal').asstring),12,' ');
               PStringGrid.Cells[6,PStringGrid.rowcount-1]:=Fieldbyname('codigo').asstring;
               PStringGrid.Cells[7,PStringGrid.rowcount-1]:=Fieldbyname('ordeminsercao').asstring;

               PStringGrid.Cells[8,PStringGrid.rowcount-1]:=Fieldbyname('creditoipi').asstring;
               PStringGrid.Cells[9,PStringGrid.rowcount-1]:=Fieldbyname('creditoicms').asstring;
               PStringGrid.Cells[10,PStringGrid.rowcount-1]:=Fieldbyname('creditopis').asstring;
               PStringGrid.Cells[11,PStringGrid.rowcount-1]:=Fieldbyname('creditocofins').asstring;
               PStringGrid.Cells[12,PStringGrid.rowcount-1]:=Fieldbyname('ipipago').asstring;
               PStringGrid.Cells[13,PStringGrid.rowcount-1]:=Fieldbyname('CFOP').asstring;
               PStringGrid.Cells[14,PStringGrid.rowcount-1]:=Fieldbyname('CSOSN').asstring;
               PStringGrid.Cells[15,PStringGrid.rowcount-1]:=Fieldbyname('CSTA').asstring;
               PStringGrid.Cells[16,PStringGrid.rowcount-1]:=Fieldbyname('CSTB').asstring;
               PStringGrid.Cells[18,PStringGrid.rowcount-1]:=Fieldbyname('UNIDADE').asstring;
               PStringGrid.Cells[17,PStringGrid.rowcount-1]:=Fieldbyname('DESCONTO').asstring;
               PStringGrid.Cells[20,PStringGrid.rowcount-1]:=Fieldbyname('BCICMS').asstring;
               PStringGrid.Cells[21,PStringGrid.rowcount-1]:=Fieldbyname('VALORICMS').asstring;
               PStringGrid.Cells[22,PStringGrid.rowcount-1]:=Fieldbyname('VALORFRETE').asstring;
               PStringGrid.Cells[23,PStringGrid.rowcount-1]:=Fieldbyname('VALORIPI').asstring;
               PStringGrid.Cells[24,PStringGrid.rowcount-1]:=Fieldbyname('REDBASEICMS').asstring;
               PStringGrid.Cells[19,PStringGrid.rowcount-1]:=Fieldbyname('DIVERSOCOR').asstring;

               PStringGrid.RowCount:=PStringGrid.rowcount+1;
               next;
          End;

          //Selecionando ferragem 
          close;
          sql.clear;
          sql.add('Select  Tabferragem_EP.codigo,Tabferragem_EP.ordeminsercao,Tabferragem_EP.quantidade,Tabferragem_EP.valor,Tabferragem_EP.valorfinal,');
          SQL.Add('Tabferragem_EP.bcicms,Tabferragem_EP.valoricms,Tabferragem_EP.valorfrete,Tabferragem_EP.valoripi,Tabferragem_EP.redbaseicms') ;
          sql.add(',tabCor.Descricao as NomeCor,Tabferragem.Descricao as NOME,Tabferragem_EP.creditoipi,Tabferragem_EP.creditoicms,Tabferragem_EP.creditopis,Tabferragem_EP.creditocofins,Tabferragem_EP.ipipago');
          sql.add(',Tabferragem_EP.cfop,Tabferragem_EP.csosn,Tabferragem_EP.csta,Tabferragem_EP.cstb,Tabferragem_EP.unidade,Tabferragem_EP.desconto,TABFERRAGEM_EP.FERRAGEMCOR from Tabferragem_EP');
          sql.add('join tabferragemCor on tabferragem_ep.ferragemCor=TabferragemCor.codigo');
          sql.add('join tabferragem on TabferragemCor.ferragem=Tabferragem.codigo');
          sql.add('join tabCor on TabferragemCor.Cor=tabCor.codigo');
          sql.add('where Tabferragem_Ep.Entrada='+Pentrada);
          sql.add('order by Tabferragem_EP.OrdemInsercao');
          open;
          While not(eof) do
          Begin
               PStringGrid.Cells[0,PStringGrid.rowcount-1]:='FERRAGEM';
               PStringGrid.Cells[1,PStringGrid.rowcount-1]:=Fieldbyname('nome').asstring;
               PStringGrid.Cells[2,PStringGrid.rowcount-1]:=Fieldbyname('nomecor').asstring;
               PStringGrid.Cells[3,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('quantidade').asstring),12,' ');
               PStringGrid.Cells[4,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('valor').asstring),12,' ');
               PStringGrid.Cells[5,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('valorfinal').asstring),12,' ');
               PStringGrid.Cells[6,PStringGrid.rowcount-1]:=Fieldbyname('codigo').asstring;
               PStringGrid.Cells[7,PStringGrid.rowcount-1]:=Fieldbyname('ordeminsercao').asstring;

               PStringGrid.Cells[8,PStringGrid.rowcount-1]:=Fieldbyname('creditoipi').asstring;
               PStringGrid.Cells[9,PStringGrid.rowcount-1]:=Fieldbyname('creditoicms').asstring;
               PStringGrid.Cells[10,PStringGrid.rowcount-1]:=Fieldbyname('creditopis').asstring;
               PStringGrid.Cells[11,PStringGrid.rowcount-1]:=Fieldbyname('creditocofins').asstring;
               PStringGrid.Cells[12,PStringGrid.rowcount-1]:=Fieldbyname('IPIpago').asstring;
               PStringGrid.Cells[13,PStringGrid.rowcount-1]:=Fieldbyname('CFOP').asstring;
               PStringGrid.Cells[14,PStringGrid.rowcount-1]:=Fieldbyname('CSOSN').asstring;
               PStringGrid.Cells[15,PStringGrid.rowcount-1]:=Fieldbyname('CSTA').asstring;
               PStringGrid.Cells[16,PStringGrid.rowcount-1]:=Fieldbyname('CSTB').asstring;
               PStringGrid.Cells[18,PStringGrid.rowcount-1]:=Fieldbyname('UNIDADE').asstring;
               PStringGrid.Cells[17,PStringGrid.rowcount-1]:=Fieldbyname('DESCONTO').asstring;
               PStringGrid.Cells[20,PStringGrid.rowcount-1]:=Fieldbyname('BCICMS').asstring;
               PStringGrid.Cells[21,PStringGrid.rowcount-1]:=Fieldbyname('VALORICMS').asstring;
               PStringGrid.Cells[22,PStringGrid.rowcount-1]:=Fieldbyname('VALORFRETE').asstring;
               PStringGrid.Cells[23,PStringGrid.rowcount-1]:=Fieldbyname('VALORIPI').asstring;
               PStringGrid.Cells[24,PStringGrid.rowcount-1]:=Fieldbyname('REDBASEICMS').asstring;
               PStringGrid.Cells[19,PStringGrid.rowcount-1]:=Fieldbyname('FERRAGEMCOR').asstring;


               PStringGrid.RowCount:=PStringGrid.rowcount+1;
               next;
          End;

          //Selecionando kitbox
          close;
          sql.clear;
          sql.add('Select  Tabkitbox_EP.codigo,Tabkitbox_EP.ordeminsercao,Tabkitbox_EP.quantidade,Tabkitbox_EP.valor,Tabkitbox_EP.valorfinal,');
          SQL.Add('Tabkitbox_EP.bcicms,Tabkitbox_EP.valoricms,Tabkitbox_EP.valorfrete,Tabkitbox_EP.valoripi,Tabkitbox_EP.redbaseicms') ;
          sql.add(',tabCor.Descricao as NomeCor,Tabkitbox.Descricao as NOME,Tabkitbox_EP.creditoipi,Tabkitbox_EP.creditoicms,Tabkitbox_EP.creditopis,Tabkitbox_EP.creditocofins,Tabkitbox_EP.ipipago');
          sql.add(',Tabkitbox_EP.cfop,Tabkitbox_EP.csosn,Tabkitbox_EP.csta,Tabkitbox_EP.cstb,Tabkitbox_EP.unidade,Tabkitbox_EP.desconto,TABKITBOX_EP.KITBOXCOR from Tabkitbox_EP');
          sql.add('join tabkitboxCor on tabkitbox_ep.kitboxCor=TabkitboxCor.codigo');
          sql.add('join tabkitbox on TabkitboxCor.kitbox=Tabkitbox.codigo');
          sql.add('join tabCor on TabkitboxCor.Cor=tabCor.codigo');
          sql.add('where Tabkitbox_Ep.Entrada='+Pentrada);
          sql.add('order by Tabkitbox_EP.OrdemInsercao');
          open;
          While not(eof) do
          Begin
               PStringGrid.Cells[0,PStringGrid.rowcount-1]:='KITBOX';
               PStringGrid.Cells[1,PStringGrid.rowcount-1]:=Fieldbyname('nome').asstring;
               PStringGrid.Cells[2,PStringGrid.rowcount-1]:=Fieldbyname('nomecor').asstring;
               PStringGrid.Cells[3,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('quantidade').asstring),12,' ');
               PStringGrid.Cells[4,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('valor').asstring),12,' ');
               PStringGrid.Cells[5,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('valorfinal').asstring),12,' ');
               PStringGrid.Cells[6,PStringGrid.rowcount-1]:=Fieldbyname('codigo').asstring;
               PStringGrid.Cells[7,PStringGrid.rowcount-1]:=Fieldbyname('ordeminsercao').asstring;

               PStringGrid.Cells[8,PStringGrid.rowcount-1]:=Fieldbyname('creditoipi').asstring;
               PStringGrid.Cells[9,PStringGrid.rowcount-1]:=Fieldbyname('creditoicms').asstring;
               PStringGrid.Cells[10,PStringGrid.rowcount-1]:=Fieldbyname('creditopis').asstring;
               PStringGrid.Cells[11,PStringGrid.rowcount-1]:=Fieldbyname('creditocofins').asstring;
               PStringGrid.Cells[12,PStringGrid.rowcount-1]:=Fieldbyname('IPIpago').asstring;
               PStringGrid.Cells[13,PStringGrid.rowcount-1]:=Fieldbyname('CFOP').asstring;
               PStringGrid.Cells[14,PStringGrid.rowcount-1]:=Fieldbyname('CSOSN').asstring;
               PStringGrid.Cells[15,PStringGrid.rowcount-1]:=Fieldbyname('CSTA').asstring;
               PStringGrid.Cells[16,PStringGrid.rowcount-1]:=Fieldbyname('CSTB').asstring;
               PStringGrid.Cells[18,PStringGrid.rowcount-1]:=Fieldbyname('UNIDADE').asstring;
               PStringGrid.Cells[17,PStringGrid.rowcount-1]:=Fieldbyname('DESCONTO').asstring;
               PStringGrid.Cells[20,PStringGrid.rowcount-1]:=Fieldbyname('BCICMS').asstring;
               PStringGrid.Cells[21,PStringGrid.rowcount-1]:=Fieldbyname('VALORICMS').asstring;
               PStringGrid.Cells[22,PStringGrid.rowcount-1]:=Fieldbyname('VALORFRETE').asstring;
               PStringGrid.Cells[23,PStringGrid.rowcount-1]:=Fieldbyname('VALORIPI').asstring;
               PStringGrid.Cells[24,PStringGrid.rowcount-1]:=Fieldbyname('REDBASEICMS').asstring;
               PStringGrid.Cells[19,PStringGrid.rowcount-1]:=Fieldbyname('KITBOXCOR').asstring;


               PStringGrid.RowCount:=PStringGrid.rowcount+1;
               next;
          End;

          //Selecionando Perfilado
          close;
          sql.clear;
          sql.add('Select  TabPerfilado_EP.codigo,TabPerfilado_EP.ordeminsercao,TabPerfilado_EP.quantidade,TabPerfilado_EP.valor,TabPerfilado_EP.valorfinal,');
          SQL.Add('TabPerfilado_EP.bcicms,TabPerfilado_EP.valoricms,TabPerfilado_EP.valorfrete,TabPerfilado_EP.valoripi,TabPerfilado_EP.redbaseicms') ;
          sql.add(',tabCor.Descricao as NomeCor,TabPerfilado.Descricao as NOME,Tabperfilado_EP.creditoipi,Tabperfilado_EP.creditoicms,Tabperfilado_EP.creditopis,Tabperfilado_EP.creditocofins,Tabperfilado_EP.ipipago');
          sql.add(',TabPerfilado_EP.cfop,TabPerfilado_EP.csosn,TabPerfilado_EP.csta,TabPerfilado_EP.cstb,TabPerfilado_EP.unidade,TabPerfilado_EP.desconto,TABPERFILADO_EP.PERFILADOCOR from TabPerfilado_EP');
          sql.add('join tabPerfiladoCor on tabPerfilado_ep.PerfiladoCor=TabPerfiladoCor.codigo');
          sql.add('join tabPerfilado on TabPerfiladoCor.Perfilado=TabPerfilado.codigo');
          sql.add('join tabCor on TabPerfiladoCor.Cor=tabCor.codigo');
          sql.add('where TabPerfilado_Ep.Entrada='+Pentrada);
          sql.add('order by TabPerfilado_EP.OrdemInsercao');
          open;
          While not(eof) do
          Begin
               PStringGrid.Cells[0,PStringGrid.rowcount-1]:='PERFILADO';
               PStringGrid.Cells[1,PStringGrid.rowcount-1]:=Fieldbyname('nome').asstring;
               PStringGrid.Cells[2,PStringGrid.rowcount-1]:=Fieldbyname('nomecor').asstring;
               PStringGrid.Cells[3,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('quantidade').asstring),12,' ');
               PStringGrid.Cells[4,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('valor').asstring),12,' ');
               PStringGrid.Cells[5,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('valorfinal').asstring),12,' ');
               PStringGrid.Cells[6,PStringGrid.rowcount-1]:=Fieldbyname('codigo').asstring;
               PStringGrid.Cells[7,PStringGrid.rowcount-1]:=Fieldbyname('ordeminsercao').asstring;

               PStringGrid.Cells[8,PStringGrid.rowcount-1]:=Fieldbyname('creditoipi').asstring;
               PStringGrid.Cells[9,PStringGrid.rowcount-1]:=Fieldbyname('creditoicms').asstring;
               PStringGrid.Cells[10,PStringGrid.rowcount-1]:=Fieldbyname('creditopis').asstring;
               PStringGrid.Cells[11,PStringGrid.rowcount-1]:=Fieldbyname('creditocofins').asstring;
               PStringGrid.Cells[12,PStringGrid.rowcount-1]:=Fieldbyname('IPIpago').asstring;
               PStringGrid.Cells[13,PStringGrid.rowcount-1]:=Fieldbyname('CFOP').asstring;
               PStringGrid.Cells[14,PStringGrid.rowcount-1]:=Fieldbyname('CSOSN').asstring;
               PStringGrid.Cells[15,PStringGrid.rowcount-1]:=Fieldbyname('CSTA').asstring;
               PStringGrid.Cells[16,PStringGrid.rowcount-1]:=Fieldbyname('CSTB').asstring;
               PStringGrid.Cells[18,PStringGrid.rowcount-1]:=Fieldbyname('UNIDADE').asstring;
               PStringGrid.Cells[17,PStringGrid.rowcount-1]:=Fieldbyname('DESCONTO').asstring;
               PStringGrid.Cells[20,PStringGrid.rowcount-1]:=Fieldbyname('BCICMS').asstring;
               PStringGrid.Cells[21,PStringGrid.rowcount-1]:=Fieldbyname('VALORICMS').asstring;
               PStringGrid.Cells[22,PStringGrid.rowcount-1]:=Fieldbyname('VALORFRETE').asstring;
               PStringGrid.Cells[23,PStringGrid.rowcount-1]:=Fieldbyname('VALORIPI').asstring;
               PStringGrid.Cells[24,PStringGrid.rowcount-1]:=Fieldbyname('REDBASEICMS').asstring;
               PStringGrid.Cells[19,PStringGrid.rowcount-1]:=Fieldbyname('PERFILADOCOR').asstring;


               PStringGrid.RowCount:=PStringGrid.rowcount+1;
               next;
          End;

          //Selecionando Persianas
          close;
          sql.clear;
          sql.add('Select  Tabpersiana_EP.codigo,Tabpersiana_EP.ordeminsercao,');
          sql.add('Tabpersiana_EP.quantidade,');
          sql.add('Tabpersiana_EP.valor,Tabpersiana_EP.valorfinal,');
          sql.add('tabCor.Descricao as NomeCor');
          SQL.Add(',Tabpersiana_EP.bcicms,Tabpersiana_EP.valoricms,Tabpersiana_EP.valorfrete,Tabpersiana_EP.valoripi,Tabpersiana_EP.redbaseicms') ;
          sql.add(',Tabpersiana.NOME,Tabpersiana_EP.creditoipi,Tabpersiana_EP.creditoicms,Tabpersiana_EP.creditopis,Tabpersiana_EP.creditocofins,Tabpersiana_EP.ipipago');
          sql.add(',Tabpersiana_EP.cfop,Tabpersiana_EP.csosn,Tabpersiana_EP.csta,Tabpersiana_EP.cstb,Tabpersiana_EP.unidade,Tabpersiana_EP.desconto,TABPERSIANA_EP.PERSIANAGRUPODIAMETROCOR from TabPersiana_EP');
          sql.add('join tabpersianaGrupoDiametroCor on tabpersiana_ep.persianagrupodiametroCor=TabpersianagrupodiametroCor.codigo');
          sql.add('join tabpersiana on TabpersianagrupodiametroCor.persiana=Tabpersiana.codigo');
          sql.add('join tabCor on TabpersianagrupodiametroCor.Cor=tabCor.codigo');
          sql.add('where Tabpersiana_EP.Entrada='+Pentrada);
          sql.add('order by Tabpersiana_EP.OrdemInsercao');

          open;
          While not(eof) do
          Begin
               PStringGrid.Cells[0,PStringGrid.rowcount-1]:='PERSIANA';
               PStringGrid.Cells[1,PStringGrid.rowcount-1]:=Fieldbyname('nome').asstring;
               PStringGrid.Cells[2,PStringGrid.rowcount-1]:=Fieldbyname('nomecor').asstring;
               PStringGrid.Cells[3,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('quantidade').asstring),12,' ');
               PStringGrid.Cells[4,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('valor').asstring),12,' ');
               PStringGrid.Cells[5,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('valorfinal').asstring),12,' ');
               PStringGrid.Cells[6,PStringGrid.rowcount-1]:=Fieldbyname('codigo').asstring;
               PStringGrid.Cells[7,PStringGrid.rowcount-1]:=Fieldbyname('ordeminsercao').asstring;

               PStringGrid.Cells[8,PStringGrid.rowcount-1]:=Fieldbyname('creditoipi').asstring;
               PStringGrid.Cells[9,PStringGrid.rowcount-1]:=Fieldbyname('creditoicms').asstring;
               PStringGrid.Cells[10,PStringGrid.rowcount-1]:=Fieldbyname('creditopis').asstring;
               PStringGrid.Cells[11,PStringGrid.rowcount-1]:=Fieldbyname('creditocofins').asstring;
               PStringGrid.Cells[12,PStringGrid.rowcount-1]:=Fieldbyname('IPIpago').asstring;
               PStringGrid.Cells[13,PStringGrid.rowcount-1]:=Fieldbyname('CFOP').asstring;
               PStringGrid.Cells[14,PStringGrid.rowcount-1]:=Fieldbyname('CSOSN').asstring;
               PStringGrid.Cells[15,PStringGrid.rowcount-1]:=Fieldbyname('CSTA').asstring;
               PStringGrid.Cells[16,PStringGrid.rowcount-1]:=Fieldbyname('CSTB').asstring;
               PStringGrid.Cells[18,PStringGrid.rowcount-1]:=Fieldbyname('UNIDADE').asstring;
               PStringGrid.Cells[17,PStringGrid.rowcount-1]:=Fieldbyname('DESCONTO').asstring;
               PStringGrid.Cells[20,PStringGrid.rowcount-1]:=Fieldbyname('BCICMS').asstring;
               PStringGrid.Cells[21,PStringGrid.rowcount-1]:=Fieldbyname('VALORICMS').asstring;
               PStringGrid.Cells[22,PStringGrid.rowcount-1]:=Fieldbyname('VALORFRETE').asstring;
               PStringGrid.Cells[23,PStringGrid.rowcount-1]:=Fieldbyname('VALORIPI').asstring;
               PStringGrid.Cells[24,PStringGrid.rowcount-1]:=Fieldbyname('REDBASEICMS').asstring;
               PStringGrid.Cells[19,PStringGrid.rowcount-1]:=Fieldbyname('PERSIANAGRUPODIAMETROCOR').asstring;


               PStringGrid.RowCount:=PStringGrid.rowcount+1;
               next;
          End;

          //Selecionando vidro
          close;
          sql.clear;
          sql.add('Select  Tabvidro_EP.codigo,Tabvidro_EP.ordeminsercao,Tabvidro_EP.quantidade,Tabvidro_EP.valor,Tabvidro_EP.valorfinal');
          SQL.Add(',Tabvidro_EP.bcicms,Tabvidro_EP.valoricms,Tabvidro_EP.valorfrete,Tabvidro_EP.valoripi,Tabvidro_EP.redbaseicms') ;
          sql.add(',tabCor.Descricao as NomeCor,Tabvidro.Descricao as NOME,Tabvidro_EP.creditoipi,Tabvidro_EP.creditoicms,Tabvidro_EP.creditopis,Tabvidro_EP.creditocofins,Tabvidro_EP.Ipipago');
          sql.add(',Tabvidro_EP.cfop,Tabvidro_EP.csosn,Tabvidro_EP.csta,Tabvidro_EP.cstb,Tabvidro_EP.unidade,Tabvidro_EP.desconto,TABVIDRO_EP.VIDROCOR from Tabvidro_EP');
          sql.add('join tabvidroCor on tabvidro_ep.vidroCor=TabvidroCor.codigo');
          sql.add('join tabvidro on TabvidroCor.vidro=Tabvidro.codigo');
          sql.add('join tabCor on TabvidroCor.Cor=tabCor.codigo');
          sql.add('where Tabvidro_Ep.Entrada='+Pentrada);
          sql.add('order by Tabvidro_EP.OrdemInsercao');
          open;
          While not(eof) do
          Begin
               PStringGrid.Cells[0,PStringGrid.rowcount-1]:='VIDRO';
               PStringGrid.Cells[1,PStringGrid.rowcount-1]:=Fieldbyname('nome').asstring;
               PStringGrid.Cells[2,PStringGrid.rowcount-1]:=Fieldbyname('nomecor').asstring;
               PStringGrid.Cells[3,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('quantidade').asstring),12,' ');
               PStringGrid.Cells[4,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('valor').asstring),12,' ');
               PStringGrid.Cells[5,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor_4_casas(Fieldbyname('valorfinal').asstring),12,' ');
               PStringGrid.Cells[6,PStringGrid.rowcount-1]:=Fieldbyname('codigo').asstring;
               PStringGrid.Cells[7,PStringGrid.rowcount-1]:=Fieldbyname('ordeminsercao').asstring;

               PStringGrid.Cells[8,PStringGrid.rowcount-1]:=Fieldbyname('creditoipi').asstring;
               PStringGrid.Cells[9,PStringGrid.rowcount-1]:=Fieldbyname('creditoicms').asstring;
               PStringGrid.Cells[10,PStringGrid.rowcount-1]:=Fieldbyname('creditopis').asstring;
               PStringGrid.Cells[11,PStringGrid.rowcount-1]:=Fieldbyname('creditocofins').asstring;
               PStringGrid.Cells[12,PStringGrid.rowcount-1]:=Fieldbyname('IPIpago').asstring;
               PStringGrid.Cells[13,PStringGrid.rowcount-1]:=Fieldbyname('CFOP').asstring;
               PStringGrid.Cells[14,PStringGrid.rowcount-1]:=Fieldbyname('CSOSN').asstring;
               PStringGrid.Cells[15,PStringGrid.rowcount-1]:=Fieldbyname('CSTA').asstring;
               PStringGrid.Cells[16,PStringGrid.rowcount-1]:=Fieldbyname('CSTB').asstring;
               PStringGrid.Cells[18,PStringGrid.rowcount-1]:=Fieldbyname('UNIDADE').asstring;
               PStringGrid.Cells[17,PStringGrid.rowcount-1]:=Fieldbyname('DESCONTO').asstring;
               PStringGrid.Cells[20,PStringGrid.rowcount-1]:=Fieldbyname('BCICMS').asstring;
               PStringGrid.Cells[21,PStringGrid.rowcount-1]:=Fieldbyname('VALORICMS').asstring;
               PStringGrid.Cells[22,PStringGrid.rowcount-1]:=Fieldbyname('VALORFRETE').asstring;
               PStringGrid.Cells[23,PStringGrid.rowcount-1]:=Fieldbyname('VALORIPI').asstring;
               PStringGrid.Cells[24,PStringGrid.rowcount-1]:=Fieldbyname('REDBASEICMS').asstring;               
               PStringGrid.Cells[19,PStringGrid.rowcount-1]:=Fieldbyname('VIDROCOR').asstring;


               PStringGrid.RowCount:=PStringGrid.rowcount+1;
               next;
          End;
     End;

     if not((PStringGrid.RowCount=2) and (PStringGrid.cells[5,1]=''))//ta vazio o grid
     then PStringGrid.RowCount:=PStringGrid.rowcount-1;

     //AjustaLArguraColunaGrid(PStringGrid);
End;

constructor TobjObjetosEntrada.create;
begin
    Self.Owner := Owner;
    ObjEntradaProdutos:=TobjEntradaProdutos.create;
    ObjDiverso_ep     :=TObjDiverso_ep     .create(Self.Owner);
    ObjPersiana_ep    :=TObjPersiana_ep    .create(Self.Owner);
    Objferragem_ep    :=TObjferragem_ep    .create(Self.Owner);
    Objperfilado_ep   :=TObjperfilado_ep   .create(Self.Owner);
    Objvidro_ep       :=TObjvidro_ep       .create(Self.Owner);
    Objkitbox_ep      :=TObjkitbox_ep      .Create(Self.Owner);
    Objquery:=TibQuery.create(nil);
    Objquery.Database:=FDataModulo.IBDatabase;
end;

destructor TobjObjetosEntrada.free;
begin
     Freeandnil(Objquery);
     ObjEntradaProdutos.Free;
     ObjDiverso_ep     .free;
     ObjPersiana_ep    .free;
     Objferragem_ep    .free;
     Objperfilado_ep   .free;
     Objvidro_ep       .free;
     Objkitbox_ep      .free;
end;

procedure TobjObjetosEntrada.RetornaQuantidade_e_soma_Produtos(Pentrada:String;
  var Pquantidade:String;var Pvalor: String);
var
TmpSoma,TmpQuantidade:Currency;
begin
     Pquantidade:='0';
     Pvalor:='0';


     if (Pentrada='')
     then exit;

     With Self do
     begin
          TmpSoma:=0;
          TmpQuantidade:=0;
          ObjDiverso_ep.RetornaQuantidade_e_Soma(Pentrada,Tmpquantidade,TmpSoma);
          Try
            Pquantidade:=FloatToStr(strtofloat(Pquantidade)+TmpQuantidade);
          Except
          End;

          Try
            Pvalor:=FloatToStr(strtofloat(Pvalor)+TmpSoma);
          Except
          End;

          TmpSoma:=0;
          TmpQuantidade:=0;
          ObjPersiana_ep.RetornaQuantidade_e_Soma(Pentrada,Tmpquantidade,TmpSoma);
          Try
            Pquantidade:=FloatToStr(strtofloat(Pquantidade)+TmpQuantidade);
          Except
          End;

          Try
            Pvalor:=FloatToStr(strtofloat(Pvalor)+TmpSoma);
          Except
          End;
          TmpSoma:=0;
          TmpQuantidade:=0;
          Objferragem_ep.RetornaQuantidade_e_Soma(Pentrada,Tmpquantidade,TmpSoma);
          Try
            Pquantidade:=FloatToStr(strtofloat(Pquantidade)+TmpQuantidade);
          Except
          End;

          Try
            Pvalor:=FloatToStr(strtofloat(Pvalor)+TmpSoma);
          Except
          End;

          TmpSoma:=0;
          TmpQuantidade:=0;
          Objperfilado_ep.RetornaQuantidade_e_Soma(Pentrada,Tmpquantidade,TmpSoma);
          Try
            Pquantidade:=FloatToStr(strtofloat(Pquantidade)+TmpQuantidade);
          Except
          End;

          Try
            Pvalor:=FloatToStr(strtofloat(Pvalor)+TmpSoma);
          Except
          End;

          TmpSoma:=0;
          TmpQuantidade:=0;
          Objvidro_ep.RetornaQuantidade_e_Soma(Pentrada,Tmpquantidade,TmpSoma);
          Try
            Pquantidade:=FloatToStr(strtofloat(Pquantidade)+TmpQuantidade);
          Except
          End;

          Try
            Pvalor:=FloatToStr(strtofloat(Pvalor)+TmpSoma);
          Except
          End;

          TmpSoma:=0;
          TmpQuantidade:=0;
          Objkitbox_ep.RetornaQuantidade_e_Soma(Pentrada,Tmpquantidade,TmpSoma);
          Try
            Pquantidade:=FloatToStr(strtofloat(Pquantidade)+TmpQuantidade);
          Except
          End;

          Try
            Pvalor:=FloatToStr(strtofloat(Pvalor)+TmpSoma);
          Except
          End;

          Pquantidade:=formata_valor(pquantidade);
          Pvalor:=formata_valor_4_casas(pvalor);
     End;
end;


procedure TobjObjetosEntrada.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJENTRADAPRODUTOS';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Compras por per�odo e produto');
                items.add('Compras por per�odo e produto (com cr�dito de impostos)');

          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:Self.ImprimeCompraporProduto;
            1:Self.ImprimeCompraporProduto_com_Creditos;
          End;
     end;

end;
procedure TobjObjetosEntrada.ImprimeCompraporProduto;
var
Pdatainicial,pdatafinal:String;
Pmaterial,pOrdem,pnomematerialescolhido:String;
Pcodigomaterial:string;
psomavalortotal,psomaquantidadetotal,psomaquantidade,psomavalor:Currency;
begin

{
     *******relatorio****
***intervalo de dados***

datainicial
datafinal

material
8mm cor
}

     Pmaterial:='';
     pcodigomaterial:='';
     pnomematerialescolhido:='';
     pdatainicial:='';
     pdatafinal:='';

     With FfiltroImp do
     Begin
          caption:='Filtro de Dados';
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          grupo07.Enabled:=true;
          Grupo13.Enabled:=True;
          
          LbGrupo01.Caption:='Data Inicial da Compra';
          LbGrupo02.Caption:='Data Final da Compra';
          LbGrupo07.caption:='Cadastro e produto';
          lbgrupo13.Caption:='Ordenado por:';

          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;

          ComboGrupo07.items.clear;
          ComboGrupo07.items.add('FERRAGEM');
          ComboGrupo07.items.add('PERFILADO');
          ComboGrupo07.items.add('VIDRO');
          ComboGrupo07.items.add('KITBOX');
          ComboGrupo07.items.add('PERSIANA');
          ComboGrupo07.items.add('DIVERSO');

          edtgrupo07.OnKeyDown:=Self.edtCodigomaterialKeyDown;
          edtgrupo07.Color:=$005CADFE;

          ComboGrupo13.items.clear;
          ComboGrupo13.items.add('Data, Cadastro');
          ComboGrupo13.items.add('Cadastro, Data');
          edtgrupo13.Visible:=False;

          try
            Showmodal;
          Finally
                 edtgrupo13.Visible:=true;
          End;

          if (tag=0)
          then exit;


          if (trim(comebarra(edtgrupo01.text))<>'')
          Then Begin
                    try
                       strtodate(edtgrupo01.Text);
                       Pdatainicial:=edtgrupo01.Text;
                    except
                          mensagemerro('Data inicial inv�lida');
                          exit;
                    End;
          End;

          if (trim(comebarra(edtgrupo02.text))<>'')
          Then Begin
                    try
                       strtodate(edtgrupo02.Text);
                       PdataFinal:=edtgrupo02.Text;
                    except
                          mensagemerro('Data final inv�lida');
                          exit;
                    End;
          End;
     

         if ((FfiltroImp.ComboGrupo07.Text='FERRAGEM')
           or(FfiltroImp.ComboGrupo07.Text='PERFILADO')
          or (FfiltroImp.ComboGrupo07.Text='VIDRO')
          or (FfiltroImp.ComboGrupo07.Text='KITBOX')
          or (FfiltroImp.ComboGrupo07.Text='PERSIANA')
          or (FfiltroImp.ComboGrupo07.Text='DIVERSO'))
         then Pmaterial:=FfiltroImp.ComboGrupo07.Text
         else Pmaterial:='';

         if (ComboGrupo13.Text='Data, Cadastro')
         or (ComboGrupo13.Text='Cadastro, Data')
         then pOrdem:=ComboGrupo13.text
         else pOrdem:='';

         Pcodigomaterial:=edtgrupo07.Text;
     end;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select PEP.descricao,PEP.referencia,PEP.nomecor,TEP.data,PEP.quantidade,PEP.quantidade,PEP.valor,PEP.valorfinal,PEP.cadastro,PEP.entrada from proc_entrada_produtos PEP');
          sql.add('join tabentradaprodutos TEP on TEP.codigo=PEP.entrada');
          sql.add('WHERE 1=1');

          if Pmaterial<>''
          then SQL.Add('and pep.CADASTRO='#39+uppercase(pmaterial)+#39);

          if Pcodigomaterial<>''
          then sql.add('AND PEP.CODIGOCOR='+pcodigomaterial);

          if (Pdatainicial<>'')
          then sql.add('AND TEP.DATA>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatainicial))+#39);

          if (pdatafinal<>'')
          Then sql.add('AND TEP.DATA<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatafinal))+#39);

          if pOrdem<>''
          then sql.add('order by '+pOrdem);

          open;

          last;

          if (recordcount=0)
          then Begin
                    mensagemaviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;
          pnomematerialescolhido:=fieldbyname('descricao').asstring;
          first;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;

               rdprint.TamanhoQteColunas:=130;
               rdprint.FonteTamanhoPadrao:=S17cpp;
               
               LinhaLocal:=3;
               RDprint.Abrir;

               if (RDprint.Setup=False)
               then begin
                         rdprint.Fechar;
                         exit;
               end;

               rdprint.ImpC(linhalocal,65,'COMPRAS POR PRODUTO',[negrito]);
               incrementalinha(2);

               if (Pmaterial<>'')
               then Begin
                          RDprint.ImpF(linhalocal,1,'CADASTRO: '+pmaterial,[negrito]);
                          IncrementaLinha(1);
               End;

               if (Pcodigomaterial<>'')
               then Begin
                          RDprint.ImpF(linhalocal,1,'PRODUTO: '+Fieldbyname('referencia').asstring+' - '+pnomematerialescolhido+' - '+Fieldbyname('nomecor').asstring,[negrito]);
                          IncrementaLinha(1);
               End;


               if (Pdatainicial<>'') or (Pdatafinal<>'')
               Then begin
                         RDprint.ImpF(linhalocal,1,'Per�odo : '+Pdatainicial+' a '+pdatafinal,[negrito]);
                         IncrementaLinha(1);
               End;
               IncrementaLinha(1);
               //titulo das colunas
               RDprint.ImpF(linhalocal,1,CompletaPalavra('CADASTRO',9,' ')+' '+
                                         CompletaPalavra('ENTR',6,' ')+' '+
                                         CompletaPalavra('DESCRI��O',35,' ')+' '+
                                         CompletaPalavra('COR',20,' ')+' '+
                                         CompletaPalavra('DATA',10,' ')+' '+
                                         CompletaPalavra_a_Esquerda('QUANTIDADE',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR FINAL',12,' '),[negrito]);
               IncrementaLinha(1);
               Desenhalinha;

               psomaquantidade:=0;
               psomavalor:=0;

               psomavalortotal:=0;
               psomaquantidadetotal:=0;

               while not(Self.Objquery.eof) do
               Begin
                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('CADASTRO').asstring,9,' ')+' '+
                                             CompletaPalavra(fieldbyname('entrada').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('descricao').asstring,35,' ')+' '+
                                             CompletaPalavra(fieldbyname('nomecor').asstring,20,' ')+' '+
                                             CompletaPalavra(fieldbyname('data').asstring,10,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('quantidade').asstring),12,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),12,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorfinal').asfloat),12,' '));
                    IncrementaLinha(1);

                    
                    psomaquantidade:=psomaquantidade+1;
                    psomavalor:=psomavalor+Fieldbyname('valor').asfloat;

                    psomavalortotal:=psomavalortotal+Fieldbyname('valorfinal').asfloat;
                    psomaquantidadetotal:=psomaquantidadetotal+Fieldbyname('quantidade').asfloat;

                    Self.Objquery.next;
               end;
               DesenhaLinha;


               //Somei os valor unitarios e dividi pela quantidade compra e n�o a qtde de pe�as
               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'M�DIA DO VALOR UNIT�RIO POR QTDE DE COMPRAS : '+formata_valor((psomavalor/psomaquantidade)),[negrito]);
               incrementalinha(1);

               //somei o valor total e dividi pela qtde de pe�as e n�o o de compras
               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'M�DIA DO VALOR FINAL POR QUANTIDADE COMPRADA: '+formata_valor((psomavalortotal/psomaquantidadetotal)),[negrito]);
               rdprint.Fechar;
          End;
     End;


end;



procedure TobjObjetosEntrada.ImprimeCompraporProduto_com_creditos;
var
Pdatainicial,pdatafinal:String;
Pmaterial,pnomematerialescolhido:String;
Pcodigomaterial:string;
psomavalortotal,psomaquantidadetotal,psomaquantidade,psomavalor:Currency;
psomacreditoicms,psomacreditoipi,psomacreditopis,psomacreditocofins:currency;
psomacreditoicms_un,psomacreditoipi_un,psomacreditopis_un,psomacreditocofins_un:currency;
begin

{
     *******relatorio****
***intervalo de dados***

datainicial
datafinal

material
8mm cor
}

     Pmaterial:='';
     pcodigomaterial:='';
     pnomematerialescolhido:='';
     pdatainicial:='';
     pdatafinal:='';

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          grupo07.Enabled:=true;
          
          LbGrupo01.Caption:='Data Inicial da Compra';
          LbGrupo02.Caption:='Data Final da Compra';
          LbGrupo07.caption:='Cadastro e produto';

          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;

          ComboGrupo07.items.clear;
          ComboGrupo07.items.add('FERRAGEM');
          ComboGrupo07.items.add('PERFILADO');
          ComboGrupo07.items.add('VIDRO');
          ComboGrupo07.items.add('KITBOX');
          ComboGrupo07.items.add('PERSIANA');
          ComboGrupo07.items.add('DIVERSO');

          edtgrupo07.OnKeyDown:=Self.edtCodigomaterialKeyDown;
          edtgrupo07.color:=$005CADFE;

          Showmodal;

          if (tag=0)
          then exit;


          if (trim(comebarra(edtgrupo01.text))<>'')
          Then Begin
                    try
                       strtodate(edtgrupo01.Text);
                       Pdatainicial:=edtgrupo01.Text;
                    except
                          mensagemerro('Data inicial inv�lida');
                          exit;
                    End;
          End;

          if (trim(comebarra(edtgrupo02.text))<>'')
          Then Begin
                    try
                       strtodate(edtgrupo02.Text);
                       PdataFinal:=edtgrupo02.Text;
                    except
                          mensagemerro('Data final inv�lida');
                          exit;
                    End;
          End;

         if ((FfiltroImp.ComboGrupo07.Text='FERRAGEM')
           or(FfiltroImp.ComboGrupo07.Text='PERFILADO')
          or (FfiltroImp.ComboGrupo07.Text='VIDRO')
          or (FfiltroImp.ComboGrupo07.Text='KITBOX')
          or (FfiltroImp.ComboGrupo07.Text='PERSIANA')
          or (FfiltroImp.ComboGrupo07.Text='DIVERSO'))
         then Pmaterial:=FfiltroImp.ComboGrupo07.Text
         else Begin
                   MensagemErro('� necess�rio escolher um material');
                   exit;
         End;

         Pcodigomaterial:=edtgrupo07.Text;
     end;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select PEP.descricao,PEP.referencia,PEP.nomecor,TEP.data,PEP.quantidade,PEP.quantidade,PEP.valor,PEP.valorfinal,');
          sql.add('PEP.creditoicms,PEP.creditoipi,PEP.Creditopis,Pep.creditocofins,');
          sql.add('PEP.creditoicms_un,PEP.creditoipi_un,PEP.Creditopis_un,Pep.creditocofins_un');
          sql.add('from proc_entrada_produtos PEP');
          sql.add('join tabentradaprodutos TEP on TEP.codigo=PEP.entrada');
          sql.add('WHERE pep.CADASTRO='#39+uppercase(pmaterial)+#39);
          sql.add('AND PEP.CODIGOCOR='+pcodigomaterial);

          if (Pdatainicial<>'')
          then sql.add('AND TEP.DATA>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatainicial))+#39);

          if (pdatafinal<>'')
          Then sql.add('AND TEP.DATA<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatafinal))+#39);

          sql.add('order by TEP.Data'); 

          open;

          last;

          if (recordcount=0)
          then Begin
                    mensagemaviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;
          pnomematerialescolhido:=fieldbyname('descricao').asstring;
          first;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;

               LinhaLocal:=3;
               RDprint.Abrir;

               if (RDprint.Setup=False)
               then begin
                         rdprint.Fechar;
                         exit;
               end;

               rdprint.ImpC(linhalocal,45,'COMPRAS POR PRODUTO',[negrito]);
               incrementalinha(2);

               if (Pmaterial<>'')
               then Begin
                          RDprint.ImpF(linhalocal,1,'CADASTRO: '+pmaterial,[negrito]);
                          IncrementaLinha(1);
               End;

               if (Pcodigomaterial<>'')
               then Begin
                          RDprint.ImpF(linhalocal,1,'PRODUTO: '+Fieldbyname('referencia').asstring+' - '+pnomematerialescolhido+' - '+Fieldbyname('nomecor').asstring,[negrito]);
                          IncrementaLinha(1);
               End;


               if (Pdatainicial<>'') or (Pdatafinal<>'')
               Then begin
                         RDprint.ImpF(linhalocal,1,'Per�odo : '+Pdatainicial+' a '+pdatafinal,[negrito]);
                         IncrementaLinha(1);
               End;
               IncrementaLinha(1);
               //titulo das colunas
               RDprint.ImpF(linhalocal,1,CompletaPalavra('DATA',10,' ')+' '+
                                         CompletaPalavra_a_Esquerda('QUANTIDADE',10,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR FINAL',12,' ')+
                                         CompletaPalavra_a_Esquerda('CR�D.ICMS',12,' ')+
                                         CompletaPalavra_a_Esquerda('CR�D.IPI',12,' ')+
                                         CompletaPalavra_a_Esquerda('CR�D.PIS',12,' ')+
                                         CompletaPalavra_a_Esquerda('CR�D.COFINS',12,' ')
                                         ,[negrito]);
               IncrementaLinha(1);
               Desenhalinha;

               psomaquantidade:=0;
               psomavalor:=0;

               psomavalortotal:=0;
               psomaquantidadetotal:=0;

               psomacreditoicms:=0;
               psomacreditoipi:=0;
               psomacreditopis:=0;
               psomacreditocofins:=0;

               psomacreditoicms_un:=0;
               psomacreditoipi_un:=0;
               psomacreditopis_un:=0;
               psomacreditocofins_un:=0;

               while not(Self.Objquery.eof) do
               Begin

                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('data').asstring,10,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('quantidade').asstring),10,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorfinal').asfloat),12,' ')+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('creditoicms').asfloat),12,' ')+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('creditoipi').asfloat),12,' ')+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('creditopis').asfloat),12,' ')+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('creditocofins').asfloat),12,' ')
                                              );
                    IncrementaLinha(1);

                    
                    psomaquantidade:=psomaquantidade+1;
                    psomavalor:=psomavalor+Fieldbyname('valor').asfloat;

                    psomavalortotal:=psomavalortotal+Fieldbyname('valorfinal').asfloat;
                    psomaquantidadetotal:=psomaquantidadetotal+Fieldbyname('quantidade').asfloat;

                    psomacreditoicms:=psomacreditoicms+Fieldbyname('creditoicms').asfloat;
                    psomacreditoipi:=psomacreditoipi+Fieldbyname('creditoipi').asfloat;
                    psomacreditopis:=psomacreditopis+Fieldbyname('creditopis').asfloat;
                    psomacreditocofins:=psomacreditocofins+Fieldbyname('creditocofins').asfloat;

                    psomacreditoicms_un:=psomacreditoicms_un+Fieldbyname('creditoicms_un').asfloat;
                    psomacreditoipi_un:=psomacreditoipi_un+Fieldbyname('creditoipi_un').asfloat;
                    psomacreditopis_un:=psomacreditopis_un+Fieldbyname('creditopis_un').asfloat;
                    psomacreditocofins_un:=psomacreditocofins_un+Fieldbyname('creditocofins_un').asfloat;



                    Self.Objquery.next;
               end;
               DesenhaLinha;

               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'SOMA VALOR FINAL: '+formata_valor(psomavalortotal),[negrito]);
               incrementalinha(1);

               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'SOMA QUANTIDADE: '+formata_valor(psomaquantidadetotal),[negrito]);
               incrementalinha(1);

               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'SOMA CR�DITO DE ICMS: '+formata_valor(psomacreditoicms),[negrito]);
               incrementalinha(1);

               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'SOMA CR�DITO DE IPI: '+formata_valor(psomacreditoipi),[negrito]);
               incrementalinha(1);

               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'SOMA CR�DITO DE PIS: '+formata_valor(psomacreditopis),[negrito]);
               incrementalinha(1);

               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'SOMA CR�DITO DE COFINS: '+formata_valor(psomacreditocofins),[negrito]);
               incrementalinha(1);


               //Somei os valor unitarios e dividi pela quantidade compra e n�o a qtde de pe�as
               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'M�DIA DO VALOR UNIT�RIO POR QTDE DE COMPRAS : '+formata_valor(((psomavalor-psomacreditoicms_un-psomacreditoipi_un-psomacreditopis_un-psomacreditocofins_un)/psomaquantidade)),[negrito]);
               incrementalinha(1);

               //somei o valor total e dividi pela qtde de pe�as e n�o o de compras
               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'M�DIA DO VALOR FINAL POR QUANTIDADE COMPRADA: '+formata_valor(((psomavalortotal-psomacreditoicms-psomacreditoipi-psomacreditopis-psomacreditocofins)/psomaquantidadetotal)),[negrito]);
               rdprint.Fechar;
          End;
     End;


end;


procedure TobjObjetosEntrada.EdtCodigoMaterialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
FpesquisaXX:Tfpesquisa;
comandotemp:string;
begin
     if (key<>vk_f9)
     then exit;

              //usado no Rel imprime_quantidade_e_media_valor_produto
     if ((FfiltroImp.ComboGrupo07.Text='FERRAGEM')
       or(FfiltroImp.ComboGrupo07.Text='PERFILADO')
      or (FfiltroImp.ComboGrupo07.Text='VIDRO')
      or (FfiltroImp.ComboGrupo07.Text='KITBOX')
      or (FfiltroImp.ComboGrupo07.Text='DIVERSO'))
     then comandotemp:='Select * from VIEW'+FfiltroImp.ComboGrupo07.Text+'COR'
     Else Begin
             if (FfiltroImp.ComboGrupo07.Text='PERSIANA')
             then comandotemp:='Select * from VIEW'+FfiltroImp.ComboGrupo07.Text+'GRUPODIAMETROCOR'
             else Begin
                      MensagemAviso('Escolha o cadastro');
                      exit;
              End;
     End;

     try
        FpesquisaXX:=Tfpesquisa.create(nil);
     except
           Mensagemerro('Erro na tentativa de criar o formul�rio de pesquisa');
           exit;
     End;

     Try
        if (FpesquisaXX.PreparaPesquisa(comandotemp,'Pesquisa de '+FfiltroImp.ComboGrupo07.Text,nil)=true)
        then Begin
                  Try
                     If (FpesquisaXX.showmodal=mrok)
                     Then Begin
                              TEdit(Sender).text:=FpesquisaXX.DBGrid.DataSource.DataSet.fieldbyname('codigo').asstring;
                     End;
                  Finally
                        FpesquisaXX.QueryPesq.close;
                  End;
        End;
     finally
            freeandnil(FpesquisaXX);
     End;


End;

end.

