object FENTRADAPRODUTOS: TFENTRADAPRODUTOS
  Left = 603
  Top = 192
  Width = 982
  Height = 728
  Caption = 'Entrada de Produtos'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    974
    697)
  PixelsPerInch = 96
  TextHeight = 13
  object lbNomegrupo: TLabel
    Left = 832
    Top = 288
    Width = 63
    Height = 13
    Caption = 'lbNomegrupo'
    Visible = False
  end
  object lbNomediametro: TLabel
    Left = 832
    Top = 304
    Width = 66
    Height = 13
    Caption = 'nomediametro'
    Visible = False
  end
  object pnl1: TPanel
    Left = 0
    Top = 284
    Width = 974
    Height = 208
    Align = alTop
    BevelOuter = bvNone
    Color = 14024703
    TabOrder = 0
    object pnlDadosPersiana: TPanel
      Left = 0
      Top = 28
      Width = 305
      Height = 24
      BevelOuter = bvNone
      Color = 14024703
      TabOrder = 0
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 974
      Height = 163
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'PRODUTOS/ICMS'
        object lb21: TLabel
          Left = 10
          Top = 15
          Width = 46
          Height = 14
          Caption = 'Categoria'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb27: TLabel
          Left = 10
          Top = 46
          Width = 37
          Height = 14
          Caption = 'Produto'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb28: TLabel
          Left = 77
          Top = 46
          Width = 17
          Height = 14
          Caption = 'Cor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb25: TLabel
          Left = 496
          Top = 46
          Width = 30
          Height = 14
          Caption = 'Grupo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb26: TLabel
          Left = 586
          Top = 46
          Width = 42
          Height = 14
          Caption = 'Di'#226'metro'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb36: TLabel
          Left = 678
          Top = 46
          Width = 27
          Height = 14
          Caption = 'CFOP'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb37: TLabel
          Left = 10
          Top = 94
          Width = 34
          Height = 14
          Caption = 'Origem'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb38: TLabel
          Left = 77
          Top = 94
          Width = 20
          Height = 14
          Caption = 'CST'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb39: TLabel
          Left = 157
          Top = 94
          Width = 36
          Height = 14
          Caption = 'CSOSN'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb40: TLabel
          Left = 157
          Top = 46
          Width = 39
          Height = 14
          Caption = 'Unidade'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb18: TLabel
          Left = 227
          Top = 46
          Width = 55
          Height = 14
          Caption = 'Quantidade'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb19: TLabel
          Left = 320
          Top = 46
          Width = 25
          Height = 14
          Caption = 'Valor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb35: TLabel
          Left = 410
          Top = 46
          Width = 46
          Height = 14
          Caption = 'Desconto'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb41: TLabel
          Left = 227
          Top = 95
          Width = 41
          Height = 14
          Caption = 'BC ICMS'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb16: TLabel
          Left = 320
          Top = 95
          Width = 66
          Height = 14
          Caption = 'Cr'#233'd. ICMS %'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb42: TLabel
          Left = 496
          Top = 95
          Width = 52
          Height = 14
          Caption = 'Valor ICMS'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lbPorcentagemReducao: TLabel
          Left = 410
          Top = 95
          Width = 66
          Height = 14
          Caption = 'Red. BC ICMS'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lbnomeproduto: TLabel
          Left = 339
          Top = 5
          Width = 217
          Height = 13
          AutoSize = False
          Caption = 'Produto xxxxxxxxxxxxxxxxxxxxxxx'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbnomeCor: TLabel
          Left = 606
          Top = 5
          Width = 215
          Height = 13
          AutoSize = False
          Caption = 'Cor xxxxxxxxxxxxxxxxxxxxxxxxxxx'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label11: TLabel
          Left = 240
          Top = 5
          Width = 99
          Height = 13
          Caption = 'Produto sistema: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label12: TLabel
          Left = 240
          Top = 27
          Width = 71
          Height = 13
          Caption = 'Produto xml:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbProdutoXml: TLabel
          Left = 339
          Top = 27
          Width = 217
          Height = 13
          AutoSize = False
          Caption = 'Produto xxxxxxxxxxxxxxxxxxxxxxx'
        end
        object Label13: TLabel
          Left = 576
          Top = 5
          Width = 24
          Height = 13
          Caption = 'Cor:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object cbb_ep: TComboBox
          Left = 71
          Top = 7
          Width = 145
          Height = 22
          Font.Charset = ANSI_CHARSET
          Font.Color = 5066061
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ItemHeight = 14
          ParentFont = False
          TabOrder = 0
          Text = '   '
          OnChange = cbb_epChange
          OnKeyPress = cbb_epKeyPress
          Items.Strings = (
            'Diversos'
            'Ferragem'
            'KitBox'
            'Perfilado'
            'Persiana'
            'Vidro')
        end
        object edtproduto_ep: TEdit
          Left = 10
          Top = 60
          Width = 55
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          Text = 'edtproduto_ep'
          OnDblClick = edtproduto_epDblClick
          OnExit = edtproduto_epExit
          OnKeyDown = edtproduto_epKeyDown
          OnKeyPress = edtproduto_epKeyPress
        end
        object edtcor_ep: TEdit
          Left = 77
          Top = 60
          Width = 70
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          Text = 'Edit1'
          OnDblClick = edtcor_epDblClick
          OnExit = edtcor_epExit
          OnKeyDown = edtcor_epKeyDown
          OnKeyPress = edtcor_epKeyPress
        end
        object edtgrupopersiana_EP: TEdit
          Left = 496
          Top = 60
          Width = 70
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          Text = 'edtgrupopersiana_EP'
          OnDblClick = edtgrupopersiana_EPDblClick
          OnExit = edtgrupopersiana_EPExit
          OnKeyDown = edtgrupopersiana_EPKeyDown
          OnKeyPress = edtgrupopersiana_EPKeyPress
        end
        object edtdiametropersiana_EP: TEdit
          Left = 586
          Top = 60
          Width = 70
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
          Text = 'Edit1'
          OnDblClick = edtdiametropersiana_EPDblClick
          OnExit = edtdiametropersiana_EPExit
          OnKeyDown = edtdiametropersiana_EPKeyDown
          OnKeyPress = edtdiametropersiana_EPKeyPress
        end
        object edtEdtCFOPProduto: TEdit
          Left = 678
          Top = 60
          Width = 42
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 30
          ParentFont = False
          TabOrder = 9
          OnDblClick = edtEdtCFOPProdutoDblClick
          OnKeyDown = edtEdtCFOPProdutoKeyDown
          OnKeyPress = edtproduto_epKeyPress
        end
        object edtEdtST_A: TEdit
          Left = 10
          Top = 108
          Width = 55
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 10
          OnDblClick = edtEdtST_ADblClick
          OnKeyDown = edtEdtST_AKeyDown
          OnKeyPress = edtproduto_epKeyPress
        end
        object edtEdtST_B: TEdit
          Left = 77
          Top = 108
          Width = 70
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 11
          OnDblClick = edtEdtST_BDblClick
          OnKeyDown = edtEdtST_BKeyDown
          OnKeyPress = edtproduto_epKeyPress
        end
        object edtCSOSN: TEdit
          Left = 157
          Top = 108
          Width = 53
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 12
          OnDblClick = edtCSOSNDblClick
          OnKeyDown = edtCSOSNKeyDown
          OnKeyPress = edtproduto_epKeyPress
        end
        object edtEdtUnidade: TEdit
          Left = 157
          Top = 60
          Width = 53
          Height = 20
          CharCase = ecUpperCase
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 3
          OnDblClick = edtEdtUnidadeDblClick
          OnKeyDown = edtEdtUnidadeKeyDown
          OnKeyPress = edtEdtUnidadeKeyPress
        end
        object edtquantidade_ep: TEdit
          Left = 227
          Top = 60
          Width = 70
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          Text = 'Edit1'
          OnKeyPress = edtvalor_epKeyPress
        end
        object edtvalor_ep: TEdit
          Left = 320
          Top = 60
          Width = 70
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 11
          ParentFont = False
          TabOrder = 5
          Text = 'Edit1'
          OnExit = edtvalor_epExit
          OnKeyPress = edtvalor_epKeyPress
        end
        object edtdesconto: TEdit
          Left = 410
          Top = 60
          Width = 70
          Height = 20
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 6
          OnExit = edtdescontoExit
          OnKeyPress = edtvalor_epKeyPress
        end
        object edtBaseCalculoICMS_p: TEdit
          Left = 227
          Top = 109
          Width = 70
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 13
          OnExit = edtBaseCalculoICMS_pExit
          OnKeyPress = edtvalor_epKeyPress
        end
        object edtcreditoicms_produtos: TEdit
          Left = 320
          Top = 109
          Width = 70
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 14
          OnExit = edtcreditoicms_produtosExit
          OnKeyPress = edtvalor_epKeyPress
        end
        object edtValorICMS_p: TEdit
          Left = 496
          Top = 109
          Width = 70
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 16
          OnExit = edtValorICMS_pExit
          OnKeyPress = edtvalor_epKeyPress
        end
        object edtPorcentagemReducao: TEdit
          Left = 410
          Top = 109
          Width = 70
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 15
          OnExit = edtPorcentagemReducaoExit
          OnKeyPress = edtvalor_epKeyPress
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'ICMS ST'
        ImageIndex = 4
        object Label5: TLabel
          Left = 13
          Top = 14
          Width = 67
          Height = 14
          Caption = '% Aliquota ST'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 113
          Top = 14
          Width = 57
          Height = 14
          Caption = 'BC ICMS ST'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 213
          Top = 14
          Width = 56
          Height = 14
          Caption = 'R$ ICMS ST'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object edtAliquotaST: TEdit
          Left = 13
          Top = 28
          Width = 65
          Height = 19
          TabOrder = 0
          OnExit = edtAliquotaSTExit
          OnKeyPress = edtAliquotaSTKeyPress
        end
        object edtBCICMSST: TEdit
          Left = 113
          Top = 28
          Width = 60
          Height = 19
          TabOrder = 1
          OnExit = edtBCICMSSTExit
          OnKeyPress = edtBCICMSSTKeyPress
        end
        object edtValorICMSST: TEdit
          Left = 213
          Top = 28
          Width = 60
          Height = 19
          TabOrder = 2
          OnExit = edtValorICMSSTExit
          OnKeyPress = edtValorICMSSTKeyPress
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'PIS/COFINS'
        ImageIndex = 1
        object lb14: TLabel
          Left = 267
          Top = 63
          Width = 82
          Height = 14
          Caption = 'Cr'#233'd. COFINS R$'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 195
          Top = 63
          Width = 43
          Height = 14
          Caption = '% cofins'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb15: TLabel
          Left = 87
          Top = 63
          Width = 60
          Height = 14
          Caption = 'Cr'#233'd. PIS R$'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 13
          Top = 63
          Width = 27
          Height = 14
          Caption = '% pis'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 13
          Top = 14
          Width = 137
          Height = 13
          Caption = 'CST PIS           CST COFINS'
        end
        object edtcreditocofins_produtos: TEdit
          Left = 267
          Top = 80
          Width = 76
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          OnExit = edtcreditocofins_produtosExit
          OnKeyPress = edtvalor_epKeyPress
        end
        object edtPercentualCofins: TEdit
          Left = 195
          Top = 80
          Width = 58
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnExit = edtPercentualCofinsExit
          OnKeyPress = edtPercentualCofinsKeyPress
        end
        object edtcreditopis_produtos: TEdit
          Left = 87
          Top = 80
          Width = 76
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          OnExit = edtcreditopis_produtosExit
          OnKeyPress = edtvalor_epKeyPress
        end
        object edtPercentualPis: TEdit
          Left = 13
          Top = 80
          Width = 58
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          OnExit = edtPercentualPisExit
          OnKeyPress = edtPercentualPisKeyPress
        end
        object edtCstPis: TEdit
          Left = 13
          Top = 28
          Width = 41
          Height = 19
          Color = 6073854
          TabOrder = 0
          OnKeyDown = edtCstPisKeyDown
        end
        object edtCstCofins: TEdit
          Left = 88
          Top = 28
          Width = 65
          Height = 19
          Color = 6073854
          TabOrder = 1
          OnKeyDown = edtCstCofinsKeyDown
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'IPI'
        ImageIndex = 2
        object lb17: TLabel
          Left = 107
          Top = 63
          Width = 52
          Height = 14
          Caption = 'Cr'#233'd. IPI %'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb44: TLabel
          Left = 175
          Top = 63
          Width = 36
          Height = 14
          Caption = 'Valor IP'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb20: TLabel
          Left = 250
          Top = 63
          Width = 50
          Height = 14
          Caption = 'Ipi Pago %'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 13
          Top = 14
          Width = 37
          Height = 13
          Caption = 'CST IPI'
        end
        object Label10: TLabel
          Left = 13
          Top = 64
          Width = 77
          Height = 13
          Caption = 'Base calculo IPI'
        end
        object edtcreditoipi_produtos: TEdit
          Left = 107
          Top = 80
          Width = 50
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          OnExit = edtcreditoipi_produtosExit
          OnKeyPress = edtvalor_epKeyPress
        end
        object edtValorIPI_p: TEdit
          Left = 175
          Top = 80
          Width = 54
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          OnExit = edtValorIPI_pExit
          OnKeyPress = edtvalor_epKeyPress
        end
        object edtipipago_produtos: TEdit
          Left = 250
          Top = 80
          Width = 70
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 11
          ParentFont = False
          TabOrder = 4
          OnExit = edtipipago_produtosExit
          OnKeyPress = edtvalor_epKeyPress
        end
        object edtCstIPI: TEdit
          Left = 13
          Top = 28
          Width = 55
          Height = 19
          Color = 6073854
          TabOrder = 0
          OnKeyDown = edtCstIPIKeyDown
        end
        object edtBCIPI: TEdit
          Left = 13
          Top = 81
          Width = 81
          Height = 19
          TabOrder = 1
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'OUTROS'
        ImageIndex = 3
        object lb43: TLabel
          Left = 13
          Top = 14
          Width = 51
          Height = 14
          Caption = 'Valor frete'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 96
          Top = 14
          Width = 65
          Height = 13
          Caption = 'Outros gastos'
        end
        object edtValorFreteProduto: TEdit
          Left = 13
          Top = 28
          Width = 52
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnExit = edtValorFreteProdutoExit
          OnKeyPress = edtvalor_epKeyPress
        end
        object edtValorOutros: TEdit
          Left = 96
          Top = 28
          Width = 73
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnExit = edtValorOutrosExit
          OnKeyPress = edtValorOutrosKeyPress
        end
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 163
      Width = 974
      Height = 45
      Align = alClient
      BevelOuter = bvNone
      Color = 14024703
      TabOrder = 2
      DesignSize = (
        974
        45)
      object edtordeminsercao_ep: TEdit
        Left = 755
        Top = 24
        Width = 70
        Height = 19
        TabStop = False
        Anchors = [akRight, akBottom]
        TabOrder = 0
        Text = 'Edit1'
        Visible = False
      end
      object edtcodigo_ep: TEdit
        Left = 683
        Top = 24
        Width = 70
        Height = 19
        TabStop = False
        Anchors = [akRight, akBottom]
        TabOrder = 1
        Text = 'edtcodigo_ep'
        Visible = False
      end
      object lbTipoCampoProduto: TListBox
        Left = 640
        Top = 13
        Width = 33
        Height = 30
        Anchors = [akRight, akBottom]
        ItemHeight = 13
        TabOrder = 2
        Visible = False
      end
      object btGravar1: TBitBtn
        Left = 842
        Top = 3
        Width = 42
        Height = 39
        Anchors = [akRight, akBottom]
        Caption = '&i'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -1
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        OnClick = btGravar_EPClick
      end
      object btcancelar1: TBitBtn
        Left = 884
        Top = 3
        Width = 39
        Height = 39
        Anchors = [akRight, akBottom]
        Caption = '&C'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -1
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        OnClick = BtCancelar_EPClick
      end
      object btxcluir: TBitBtn
        Left = 923
        Top = 3
        Width = 40
        Height = 39
        Anchors = [akRight, akBottom]
        Caption = '&r'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -1
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
        OnClick = BtExcluir_EpClick
      end
      object panelTotalRestante: TPanel
        Left = 64
        Top = 3
        Width = 249
        Height = 36
        BevelOuter = bvNone
        Color = 14024703
        TabOrder = 6
        Visible = False
        object SpeedButton3: TSpeedButton
          Left = 36
          Top = 12
          Width = 16
          Height = 16
          Cursor = crHandPoint
          Flat = True
          Glyph.Data = {
            AE040000424DAE0400000000000036040000280000000A0000000A0000000100
            08000000000078000000120B0000120B000000010000030000006E6E6E00FFFF
            FF00000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000010101010100
            010100016C65010101010001010000016C65010101000101000000016C650101
            00010100000000016C65010001010000000000016C6501010001010000000001
            6C65010101000101000000016C65010101010001010000016C65010101010100
            010100016C65010101010101010101016C65}
          OnClick = SpeedButton3Click
        end
        object SpeedButton4: TSpeedButton
          Left = 53
          Top = 12
          Width = 16
          Height = 16
          Cursor = crHandPoint
          Flat = True
          Glyph.Data = {
            AE040000424DAE0400000000000036040000280000000A0000000A0000000100
            08000000000078000000120B0000120B000000010000030000006E6E6E00FFFF
            FF00000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000010001010001
            0101010100000100000101000101010100000100000001010001010100000100
            0000000101000101000001000000000001010001000001000000000101000101
            0000010000000101000101010000010000010100010101010000010001010001
            010101010000010101010101010101010000}
          OnClick = SpeedButton4Click
        end
        object lbTotalRestante: TLabel
          Left = 81
          Top = 8
          Width = 44
          Height = 20
          Caption = '1 de 7'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object imgOk: TImage
          Left = 141
          Top = 2
          Width = 46
          Height = 30
          Picture.Data = {
            0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000002000
            0000200806000000737A7AF40000001974455874536F6674776172650041646F
            626520496D616765526561647971C9653C000004094944415478DAED97BF6F1C
            5510C7E7FDD8DBFBE9F3512041090DF923ECC4C1922D421A2240A240420828A0
            4054D0910651912A144491106D08D8074841FC07484093388E85E424A68DC0E7
            D8B7BE7D6F86EFECDE59477020015FDCF874CFB77BF7DECC67BE6F66DEDA8808
            1DE6CB1C011C0128C0EB3F6E521C30396BC819431EC3394362888230059662DC
            CD99B682500D6BEAF8DD594B1E6B8AA1F7C6EED9D061718D37E17D2AF1E6C9C4
            D90B4C65C0958AA5F79F4A260FE02CCD366AFEDBC4995608FC4E88725E891E09
            006CCCD66A6EC926B6A3515B04CF83F8363E3E9D3800EEE7D3AAFB9ABD6DE6F0
            A8C227B0575510E6B7BCB79F4D06A0743E57A9BA25389FDA85E7C065C279D86B
            E04FD39212BDF4C1D3C9A50307C0F56C5275DDE86D7B97757D193DAC5105F36A
            70EE03AFE2FAF487C7AABFDE0FC000401E06C0159F760ECE2F076F3B99AE51E7
            185A091E8EAB1A581E5725978576C3DF3E7BACBACF1658838A31E701F003D65E
            7A7005ECB30EB2E7D636339142F63272ECBD3AD739795CA35C16A1D63A00E8EC
            33F702207064E71749EA5EC1C23EE7FC42CE7CE55F019C3961AB7E7960CD54A6
            B2636EE1DC68E295B22781AF9AC0A7E1E32660693F00E3BCFD9C53F76A4EA56C
            55A16D1BF84C60FEFEBE00CECCD8D4773367A6FBF1AFB26BE435005422AFC0CE
            22D4DDF0A658F3370023512E0E12F7DA36EE2146215DCD216B0D653EF2CB6820
            DD7B01547649DDE5CC9A763F6ABE48E15C231F255C1AF8868BB200A56E15B9B2
            1FC09B3FF5DE8D86CEFD81087AB91486CAB281114C6E1AEA57985FDC1EF077BD
            1180B727E17C69C798561FEAE43C728E88E1B8AE0091D71C2207E8FA5EB2EE0B
            F04BAFB39BF3979B4227D5C10020712C8114A265CC8E0CE21928700500C79170
            CB3B887C0713F3E19EDB62BEA10694AB47B99A447E0E7BBEA17BFE8F006FFCDC
            A37E16B4692CF7884EDC8DBA0D423C3C28D5A842A4227738C8C7C69AF790704F
            F487C9394A3895BDA123CAF58ACAEE68A36C4C0F0410298FDCCE88BED9229AD1
            BDDE1D42A87137CC688D521B9B6ED378C215CE5529961B955094DA4DADFD8702
            1844A6C80208C17698F92DDCEF420D1E9DDD63E7B80C47E11C4E9A6A94E57A1A
            E514AA635D9DFC2700953446696546967E474E8C9488C3964A6330EA3CB57BCE
            AFD598165199BF8D4AEDFF00680B6EF7899601715C21324DCCB1A72757643B24
            C7E132CDB25A635940E4B79DF6FA0302D0763ADD27E90262665395C0EFA39C48
            0BE7963A226BC8786D32EBDA980E1A4073A20588AFEE08CDF7F0057A51919053
            88FC31A16B7596E7B5BD96E7C80400425109DC1A6EC79C965E0AA31D9695BAC8
            A2459D5B333A112704A07B0F88764EA63B3034EBA3AC56D0E1F0CC77CB0E1FBF
            1E010046E4C725C82728FE8FE07CA57CEA3D008043FFBFE008E0305F7F0277B8
            B4EE044B797D0000000049454E44AE426082}
          Visible = False
        end
      end
    end
  end
  object StrGridStrgProdutos: TStringGrid
    Left = 0
    Top = 492
    Width = 974
    Height = 155
    Align = alClient
    DefaultRowHeight = 19
    FixedCols = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = 5066061
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    Options = [goFixedHorzLine, goHorzLine, goColMoving]
    ParentFont = False
    TabOrder = 1
    OnDblClick = StrGridStrgProdutosDblClick
    OnDrawCell = StrGridStrgProdutosDrawCell
    OnEnter = StrGridStrgProdutosEnter
    OnKeyDown = StrGridStrgProdutosKeyDown
    OnKeyPress = StrGridStrgProdutosKeyPress
    ColWidths = (
      64
      64
      64
      64
      64)
  end
  object edtpesquisa_STRG_GRID: TMaskEdit
    Left = 2
    Top = 608
    Width = 121
    Height = 19
    Anchors = [akLeft]
    TabOrder = 2
    Text = 'edtpesquisa_STRG_GRID_CP'
    OnKeyPress = edtpesquisa_STRG_GRIDKeyPress
  end
  object pnlrodape: TPanel
    Left = 0
    Top = 647
    Width = 974
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clSilver
    TabOrder = 3
    DesignSize = (
      974
      50)
    object shp1: TShape
      Left = 1
      Top = 31
      Width = 1800
      Height = 1
      Pen.Color = clWhite
    end
    object imgrodape: TImage
      Left = 0
      Top = 0
      Width = 974
      Height = 50
      Align = alClient
      Stretch = True
      OnMouseMove = imgrodapeMouseMove
    end
    object lbTotalProdutos: TLabel
      Left = 83
      Top = 16
      Width = 124
      Height = 17
      Alignment = taRightJustify
      Caption = 'lbTotalprodutos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = 17
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb31: TLabel
      Left = 240
      Top = 16
      Width = 124
      Height = 16
      Caption = 'Quantidade Total'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 14803425
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbquantidade: TLabel
      Left = 377
      Top = 16
      Width = 101
      Height = 17
      HelpType = htKeyword
      Alignment = taRightJustify
      Caption = 'lbquantidade'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = 17
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb32: TLabel
      Left = 755
      Top = 15
      Width = 49
      Height = 19
      Caption = 'T'#237'tulo:'
      Font.Charset = ANSI_CHARSET
      Font.Color = 14803425
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbtitulo1: TLabel
      Left = 839
      Top = 14
      Width = 70
      Height = 18
      Cursor = crHandPoint
      Caption = 'LbTitulo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnClick = lbLbtitulo1Click
      OnMouseMove = lbLbtitulo1MouseMove
      OnMouseLeave = lbLbtitulo1MouseLeave
    end
    object lb29: TLabel
      Left = 11
      Top = 16
      Width = 65
      Height = 16
      Caption = 'Produtos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 14803425
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object bt1: TBitBtn
      Left = 1121
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 974
    Height = 60
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 4
    DesignSize = (
      974
      60)
    object btAjuda: TSpeedButton
      Left = 914
      Top = -8
      Width = 53
      Height = 80
      Cursor = crHandPoint
      Hint = 'Ajuda'
      Anchors = [akTop, akRight]
      Flat = True
      Glyph.Data = {
        4A0D0000424D4A0D0000000000003600000028000000240000001F0000000100
        180000000000140D0000D8030000D80300000000000000000000FEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFAF6FBF9F2FEFEF4FFFFFAFF
        FCFDFFF9FFFEFEFDFCFFFFFEFEFFFDFEFCFEFEF8FFFFFBFEFCFFFFFCFFFDF6FB
        FFF7F8FFF7FBFFFBFBFDF7FFFFFCFFFFFEF7FDFCFBFFFFFFFBFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFF9FD
        F8FDFFFFFDFBFFFFFBFFFFFDFEFFFCFCFFFEFFFEFDFFFCF9FBF0EFF1F0EBEDF9
        F1F2F6F8F9F1FFFFF4FFFFFEFCFFFFFDFFFFFAFFFBFCFFFEFDFFFFFAFDFAFCFD
        F7FFFFFFFCFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFFFCFFFFFCFEFFFDFCFFFFFFFFFDFFF2E4E6DEBCBCCF
        9F99EB8688E68081EA7E7DF68686FE9092FDA2A5F7C3C3FFF0EDFFFAFFF8FFFF
        FFFDFFFFF5FDFCFDFFF6FFFFFBF8FAFFFDFEFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFBFFFCFEFFFBFFFCE7
        E1DCC29E9EBF7579DC7677F07B76FF8783FF8F88FD9388FB8F84FF837BFF7977
        F77576F98084FBB6B9F2F2ECE8FFFAF8FDFCFFFEFFFAFCFDFFFCFEFCFEFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFEFDFFFAFCD7B8BBB17879C87772F08E88FE9893F58E8CE7797DD76F70
        D26867DC716EE88581F69A95F99591FE8686FF7173F28484FFD9D4FFFFFBFDF8
        F7FFFCFFFFFEFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFF9B99D9CA76B6CDA8786F99B96E17D79
        B6484C98222FAB1122AA0F1EA5131FA41D25AA2226C53938E6625BFF9085FD9B
        91FF7B7CF2777BF4C8C7F8FFFEF9FEFDFFFEFFFFFBFCFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFC0A0A1A7706D
        E6928CFC9691B3484A87192589112398182BA32E31DC6F71EB8082CC5050BB28
        26D32A22EB2F24FE3727F36D69FF9D97FB7D83F7707EFEC5CDFFFEFFFFFDFDED
        FFFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFCFBFDFFFDFFFFFBFFFAFFFFF2FAFA
        FFFEFEDCBDBEA66D6BDB9092ED909996303C770E1B8D1E2C9B1C2BA11425D96F
        75FDA8B0FFA2B0FFA0AAC83F43BC2F26D73927FF3929FF2B1CEC5650FF9996FE
        7D7AFD7F7EFADFDBFFFEFFF4FEFEFEFDFFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFF
        FDF9FEFFFDFFF7FAFEFAFFFFF4E6E7A67F81CF8B8CEC9A9F92323C7B121D9123
        2F921C27A5232EA71C27DE837EFFB7B5F9A9AEFDB0B3BF4343B42821C9372BDA
        3326E33626CB2728CA5556FF9893FB7777DE9A9BFFFEFEFFFCFDFEFDFFFEFDFF
        FEFDFFFEFDFFFEFEFEFEFDFFFFFBFFFFFBFFFAFBFFFFFFFFC3AEADA77678F8A4
        A9A9515776131B8D202898242BA3282CA72629A82627B55047EBADA2FFBCB5CD
        7974A92E2AA13026A5372BBD332DBF3528BD2B2DA51E26DF7372FF918FE27377
        F0CCCCFFFFFEFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFAF9FBFFFDFFFFFAFFFFFE
        FFF5F0EFA38384DB9FA0E78A91791D22852428992E309E2827A72C28AC3028AB
        3229A3271FA13E36B4413E9E332CB03129A73126A23127B0322EA93026AD3131
        A91925B63239F2948FFD7C7FE9989BFBFFF9FEFDFFFEFDFFFEFDFFFEFDFFFFFE
        FEFCFBFDFFFEFFFFFDFFFFFEFFDACECEAE8687F0ABAEB156597E24248B2C2992
        2A25A93730A93328AE3B2EA33222D35E59EA928BF28C87E27D74AC362BA93A2A
        A23729A5342AA3342CA62728B02931A71821D8716FFF8A8ADE7D7FEFECE7FEFD
        FFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFFFDFCFEFFFCFEFFFEFFC0B3B1BF9392F0
        A5A78F3B35842A2396352B9F372AA93C2EAC3D2DAC3C2AA1311FD16D69F8CABF
        FBC0B7FCABA3AA3A2EA23827A33928A9372AA6352BAE302BA72C2EAA1922C550
        51FF9692DC7475E0D2D3FFFEFFFEFDFFFEFDFFFEFDFFFFFDFEFEFDFFFEFDFFFF
        FDFFFFFEFEB6A9A7CB9D9CEC9E9F8335288E3527A0392AAD4130A83C2AA73B29
        A93927AE3827CE6C66FFCDC1FABAB5FFBFB6D46B62AD3126B43427B13A2BA933
        27A93327A02A25AC1F28AF373BFC9D94E7797BD1B8BCFFFEFFFFFEFFFEFDFFFE
        FDFFFFFDFEFCFCFCFDFCFEFFFEFFFDFBFBB5A8A6DBAEABE19192813522983C29
        A73B29AF3E2AA83B26AA3F2AAD3B2AAF3224B05A4EFFE0D0FFCEC6F7BEB5FFBE
        B3DE8C7BA44434A33628AD3C2CAD3726A8342DA7262DAE2E33FA9B92E27A7BD4
        ABB3FFFEFFFFFEFFFEFDFFFEFDFFFBFCFFF9FEFFFFFCFDFBFFFFFCF9FBC9AAAB
        DEB1ADDF9A91813628B03C2BAB3D25A7412AA6362AB3362EA13726AE3C25A833
        24D79987FFF4E4F9D3CEFEBEBEFFC9C3EC9E91AD4436B137299C3B27AA3526B1
        2F28A4302FF7969AE67F86C7AAA5FFFEFFFFFEFFFEFDFFFEFDFFFAFBFFFCFEFF
        FFFEFFFCFEFFFCFEFFCEB4B4E6B6B2E6A99F98392AA13426B2382C9E3E2EAA3E
        33AD4338A74539A24536AB443B9B3D38CF8A87FFEBE6FEDED9F1C0B8FFC9BDEC
        8B81AA372AA73B2AA83F2CA52B1FA6403BF9A1A1E57E86C9B3AEFFFEFFFFFEFF
        FEFDFFFEFDFFFAFDFFFDFCFEFFFEFFFDFCFEFAFFFFD3BEC0ECB5B2F5C2B8AB40
        329F3D31B84D49A24F47AD554EAE574DAE5D55B1655FAD5654B34E50A13B40B9
        7B7BFFE1DBFED7CFF9CAC2FFBCB5BD5146AD3627A3402AA83526AD544AFEACAB
        D5787FD4C7C5FFFEFFFEFDFFFEFDFFFEFDFFFBFFFFFFFBFDFFFDFDFFFDFFF9FE
        FFDFD1D2E7B1B0FFDACEC26B61A74C45A55C54A85A54D3958FF2B8B3F9C2BFF4
        B4B3AD5352AB5C59AC5553A54343F7C0BBFDE0D9FFCCCAFBC9C9CC6A60B03429
        9F3923A63624C9776BFFADACC67E84E4E0DFFEFDFFFEFDFFFEFDFFFEFDFFFAFE
        FFFFFCFFFCFCFCFFFEFFFBFEFFF4ECEDE2B7B4FFDBD2DAB0ABA6504AA86153AB
        514AD59594FFF8F7FEF1EFFAD4D2C469649C5148A85950A14B45ECBBB3FBE1DA
        FFCED0FFCFD6D06E66AB352AAD3B2A9F3726F7A89DFBA4A7BB8E91F2F7F6FEFD
        FFFEFDFFFEFDFFFEFDFFFCFBFDFFFEFFFCFEFEFEFDFFFDFCFFFFFEFFE2C7C3F3
        CAC1EDDEDCBD7672AD5144CE6459D7797AF6F5F1F4FFFEF9F3EEE9B4AAB16860
        B45E58C99087FAD7CDFFDCD6F8D5D2FECBCFBE5A55A23127A12F22CB7264FFC3
        BBD28B8ECFB7B9FAFFFFFEFDFFFEFDFFFEFDFFFEFDFFFEFBFDFFFFFFFCFEFEFB
        FDFEFFFAFFFFFEFFEBE5DEF2C4BDFFE5E8EFC8C0AE6053E17164F47773F0BBB8
        FDFEFAFDFFFFFFF8F1FCD6D4FFCDCCFFDDD8FFDED8FFE0DAFFDDD7E7A1A2A339
        32A7352EAC4A3EF8B8ADECB2ADB68E90FAECEEFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFDFFFCFCFCFAFFFEFAFDFFFFFBFFFFFDFDFBFFF9FFCFC9FFC7CCFFF9EF
        E8BFB0D66D5FFF7C70F9807EEACECDFFFBFFFDFCFFF9FEFFFDFFFFFFE4E5FFE2
        DFF6E7DEEDB6AFB34648A1312BB03E37FCAA9FF7CDC1BC8F8CD5C7C8FFFDFFFC
        FEFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFFFEFFFFFEFF
        FDFCFEFCFBFDFCC8C8F6D7D6FFFCF6F0C9C1E57F7AFF7673F98481ECA5A1E6D6
        D7FCF1F3F9F1F1ECD7D9F0C1C3D78A8DA84646A03232A54B4BECABAAFFD1CFC9
        A09EC9ADACFEF9F8FBFFFFFCFCFCFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFF
        FDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFDFCFEFFF4EEF8CDCAFDD8D4FFFBF4FDE6
        E4FBA1A6FB7375FD6F68EA6E5CCE6855B76958AE6458A5514B993E3AA04D4BC8
        7F7BFCC3C1FFCBCCDAA4A4BFAEABF3F9F4FFFFFEFFFCFDFCFEFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFEFDFFFFFEFFF4FF
        FFFBF2EFF6D6D1FDD9D3FFEFE8FCFBF1FFE6DCF6BBB2EE9285D07C70B76F65B2
        6F66C0807BDDA39EF3C1BBFFD6CFF6C6C5D4ACADCEBBB8F9FAF6F7FFFDFAFCFC
        FFFEFFFCF9FBFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFC
        FEFDFCFEFEFDFFFFFEFFFFFAFCFBFFFFF9F9F9FCE0DFFFD3CCFEDDD4F8ECE6F8
        F6F5F5F8F6F4EFEEF6E4E5FADBDCFFDBDCFFD9D8FBCAC8F0C0BCD7B5B6E1D8D5
        FAFFFEFFFFFFFEFCFCFAFFFFF8FDFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFFFFFBFFFBFCFDFDFFFD
        FBFFFFEEF1FFE2E1FFD3D2FFD0D2FFD2CFFFD1CDFFD2CFFFCFCFFFC9C9F2BFC3
        E9C4C6EED6D8F5F7F7FFFBFCFFFEFFFFFCFEFDFFFFF9FCFFFCFBFFFFFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFFFEFDFFFEFDFFFDFCFEFDFCFEFD
        FCFEFFFBFFF0FDFBF1FFFBFFFEFFFFFDFFF9FEFFF9FCFAFEF2EEF9EAE1F4E5DC
        F3E3DCF3E3DDF6E7E5FCF1F3FEFAFFF9FCFFF4FEFEFFFEFFFFF7FAFFFDFFFCF9
        FBFFFDFFFFFCFFF5F8FCFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFF7F9FFFDFEFFFFFBF8FFFDF9FDFBFAF1FBFB
        F6FFFFFBFDFEFBFEFFFFFCFFFFFDFFFFFEFFFBFFFFF8FFFFFDFEFFFFFAFFFFFD
        FFFAFEFFF8FFFFF9FBFCFFFDFFFFF9FCFBFAFCFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFCFFFEFF
        F7FDFFF5FEFFFBFFFFFFFFFEFFFCFDFFFCFFFFFAFFFFFCFFF9FEFDF9FFFDFBFF
        FEF9FCFAFBFBFBFFFFFFFFFCFEFEFDFFFBFAFCFBFFFFF5FDFDF7FFFFFDFFFFFF
        FCFFFEFDFFFEFDFFFEFDFFFEFDFF}
      OnClick = btAjudaClick
    end
    object lbnomeformulario: TLabel
      Left = 534
      Top = 8
      Width = 271
      Height = 32
      Align = alCustom
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Entrada de Produtos'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object imgValidaSped: TImage
      Left = 392
      Top = -1
      Width = 51
      Height = 52
      Picture.Data = {
        0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000003C00
        00003C08060000003AFCD9720000000473424954080808087C08648800000009
        7048597300000B1200000B1201D2DD7EFC0000001C74455874536F6674776172
        650041646F62652046697265776F726B7320435336E8BCB28C000006EA494441
        5478DAED997B6C53551CC7BF7DDDDBF6F6DDB1756BB7765BC75E3CC6780E541E
        823141DDD4609437FE61E01F5980C4205151FE34C6F187FF1AFEC1C418050C2A
        C68C75C01860201BE33526DB8075035DBB96AE7BB0ADF59ECB6E7BDBAD1022D8
        3BE82F39B9F777EE3DEDEF73CEF7FC7ACEA924CC1A9E2393A4809F714B013FEB
        96027ED62D05FCACDB63036BB55A0483C164C71D3186611008049E1EB0542A45
        7EE59E88AF90CBA0A4146C9183A615A015F2189FDC737534EB538AE833EAC17B
        F4049F6FFFE05EE893F728B6C8D81884F18442A1E4014700053E0F24048E02C5
        FB0F07269F29954892074CB170F1804F125839896A920ECC05A288038CF3C995
        A2E48F041E930CA3C1F707FAC2FF6089F125546457886F84FF0BA0700403610F
        6ADC9FC0A2CF8085B1E0A2F7024AE9D9F878CE673069F422038E49380F936C62
        E01AF71EE45AB291AF2DC445CF6598952634F6BAB0D9B8051BE66F8874AA6880
        958F004AE493FBE67BA7705A7A14AF3BDEC691F6A3F0DEF7C1CA583122EF4769
        B0187B5FF93CD2A9531E382CB98F2FBDD55857B41E97FAAEA3CDDF06B9540E1B
        0B7CE7FE2D540C5660C78A9DD0306AF101271A414E8E09FC83DEAF916D33C2A6
        C9C571B78BED8030F40A3D8C8C1E976F5DC047E64FF162F962E8F53AEE3B24C9
        04E6037F5C606E11C1D6758DB6E277E9B75857B809AE9E93AC94BD50CA94C8D3
        39D0E23D8FB97D0BB171CEFBB0665AB8111625F0A4233A9EC985C01259080706
        F66175C94A0443C368F6B68092C9B9B9EB1FF1C273A3173B7276C3E974C26CD4
        47BE23E9C00482977622C0894B4739CE0EFE866056075665BF8ADA9EE3AC9443
        30D006E8680D1ADB1AB071742B1695552033233DA65345072C938D72508C5293
        1078187E1C947D810FCBABD1EC6B867BD00DB542855C9D1D67DD8D28EE2AC73B
        33D7212B33131AB532E63B44014C40248A21749BEAA149BFCFCA558A31BF0A73
        43956CF2314F5832FE30F21516159740A56470C67306945C01873607BEC13EF8
        AE06B1CDB6130EBB0386F144254AE076CB7758525E8492AC59088E0EA02BD089
        A6B66B5818588B4CBD2302DC11BA80364B2DAA9C6FE284E70446C2C3B0682C30
        527A34B49CC67B43DB30B3643632A64D8B2E64E8E834110FF0ACFDD8F5C26ED4
        769F827FF81EECBA6C0C85EFE15CFB39BCECDB0A87A988957B08DF53FBB0BE7C
        0D3C631EB40FDE8051A543AE3617A73B1B907375162A4BD72093CDCA6A15CD7E
        BEC88085BB9C5BE5DF604BF956D4BA4FA07FB41FBD435EA43146E419B271DDDD
        8A72DF5BE8A15B612D9561816D014E794E42A9A0B879FB77F02E3ACFF4E25DF3
        077038ECD0E9B4D1058CA053891F1F4FD280BB337E85739E0AE95A3B6ABB5C18
        0C0D80A154701AF3E0D0DB70B2FD3818B51A3BE7EFC2595F2382E100B2B41930
        280C3876B616ABBD9B307BC65C98C84F10AD103F302D97A2C3F40B64257791CE
        D8D135701B1269182A8A42163B4767988BE160B2D12FEDC7B5812B30AB0D2834
        4C47DDF53A189BA66365FE6B5C5656D1740C70FC5E5914C0FC0A8B56C8D0A63B
        8270710F3B87F37167F82EA43240A3542143930EBBD6867E765ECB1512149A0B
        D0EDEFC615D76D54EAB6C06AB58E67657E6B390580F9AC7A59FD13468A3A3123
        6D0E7CA33E84A463D0AA9450D32A165E8D1C9D153A4A8363A7EB5071BB0AA5C5
        6548339B22805302F8C14140F4B0AD45F923860B3BB0D8FA023CA31E6E29A953
        3130B059B9C8E444437B23A8866CACC87F03A63433D44A2AE17996B05393064C
        7E1E62D7CC5160EE04939DD32DF29F315672138B72962010F2B18B93100A8D05
        E81BF0E252FD2DAC92AF85CD9603AD969908187F150330BFEB51C6655561D2B9
        865A8CCE6CC3BC9CF960542A28E572B8CE37A2AC75350A0A4A58299B27079C32
        C09140A3FE5F121702CE4B28B44D4777AF1B9A3F9D5890B512D3D2D35929D3D1
        D1A5053BAD785F2CC09301C6FBE4FECEC815787113FAA134E43125C8CCB242AB
        616277515305F8E1FF26C41EFBC82464492A6561359303F25BCB715FA8225103
        4F765219B3954C34A29300F3AAF9DF819FBB3FD3A6BAA580C5667BF7EE8DB9A6
        8093095C5D5D8DFDFBF7C36EB7A3A6A606555555686A6AC2810307B86B7D7D3D
        962E5D8AC3870FC3603070F5A40DDF560847DA9377CACACAB8223A6097CBC505
        CD03FA7C3ECE27F5CB972FC7A14387B06CD9326CDEBC99BBF2F7E43931E29376
        C4484709EBC97BA20326800E87832B244052C808093B82EF18123C0F48DE27D6
        D9D9C9B5E12D5ECAA203E6A1895C090801208507E4474C081C0F49E0F951163D
        300FCAC31220D201BCA4EBEAEAB8B92894342F5DA204E2F3F376CA489A044A12
        935EAFE782E7E72891340121CFB66FDFCE3D23C62727BFDF1F53CF27335127AD
        44162FE9645B0AF8491B913A99D344966230D1AFB452C029E014700A38059CEC
        2052C04FD1FE05B4B6CC7A736E33180000000049454E44AE426082}
      OnClick = imgValidaSpedClick
    end
    object btnovo: TBitBtn
      Left = 0
      Top = 0
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btnovoClick
    end
    object btSalvar: TBitBtn
      Left = 100
      Top = 0
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btSalvarClick
    end
    object btalterar: TBitBtn
      Left = 50
      Top = 0
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btAlterarClick
    end
    object btpesquisar: TBitBtn
      Left = 250
      Top = 0
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btPesquisarClick
    end
    object btcancelar: TBitBtn
      Left = 150
      Top = 0
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btCancelarClick
    end
    object btexcluir: TBitBtn
      Left = 200
      Top = 0
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btExcluirClick
    end
    object btopcoes: TBitBtn
      Left = 349
      Top = 0
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btopcoesClick
    end
    object btrelatorios: TBitBtn
      Left = 300
      Top = 0
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btRelatorioClick
    end
    object btsair: TBitBtn
      Left = 447
      Top = 0
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btSairClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 60
    Width = 974
    Height = 224
    Align = alTop
    BevelOuter = bvNone
    Color = 10643006
    TabOrder = 5
    DesignSize = (
      974
      224)
    object lb1: TLabel
      Left = 13
      Top = 6
      Width = 33
      Height = 14
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb2: TLabel
      Left = 536
      Top = 44
      Width = 22
      Height = 14
      Caption = 'Data'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb3: TLabel
      Left = 667
      Top = 7
      Width = 140
      Height = 14
      Caption = 'Frete para o Fornecedor?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb5: TLabel
      Left = 459
      Top = 44
      Width = 40
      Height = 14
      Caption = 'Emiss'#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb6: TLabel
      Left = 181
      Top = 44
      Width = 53
      Height = 14
      Caption = 'Nota Fiscal'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb7: TLabel
      Left = 93
      Top = 44
      Width = 56
      Height = 14
      Caption = 'Fornecedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb22: TLabel
      Left = 10
      Top = 158
      Width = 84
      Height = 14
      Caption = 'Observa'#231#245'es......'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbConcluido: TLabel
      Left = 827
      Top = 9
      Width = 131
      Height = 23
      Align = alCustom
      Caption = 'LbConcluido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb24: TLabel
      Left = 668
      Top = 23
      Width = 110
      Height = 14
      Caption = 'Icms sobre o frete?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb8: TLabel
      Left = 467
      Top = 133
      Width = 90
      Height = 14
      Caption = 'Valor do Icms Sub.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbVALORFRETE: TLabel
      Left = 320
      Top = 104
      Width = 68
      Height = 14
      Caption = 'Valor do Frete'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbDESCONTOS: TLabel
      Left = 467
      Top = 104
      Width = 52
      Height = 14
      Caption = 'Descontos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbOUTROSGASTOS: TLabel
      Left = 649
      Top = 104
      Width = 71
      Height = 14
      Caption = 'Outros Gastos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb9: TLabel
      Left = 804
      Top = 104
      Width = 53
      Height = 14
      Caption = 'Valor do IPI'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb4: TLabel
      Left = 10
      Top = 104
      Width = 92
      Height = 14
      Caption = 'Valor dos Produtos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb23: TLabel
      Left = 183
      Top = 104
      Width = 50
      Height = 14
      Caption = 'Valor Final'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb30: TLabel
      Left = 93
      Top = 6
      Width = 50
      Height = 14
      Caption = 'Modelo NF'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object shp2: TShape
      Left = -91
      Top = 86
      Width = 1053
      Height = 1
      Anchors = [akLeft, akTop, akRight]
      Pen.Color = clWhite
    end
    object lb33: TLabel
      Left = 13
      Top = 44
      Width = 27
      Height = 14
      Caption = 'CFOP'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb45: TLabel
      Left = 181
      Top = 5
      Width = 57
      Height = 14
      Caption = 'Chave NF-e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb47: TLabel
      Left = 10
      Top = 133
      Width = 65
      Height = 14
      Caption = 'BC ICMS.......:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb48: TLabel
      Left = 183
      Top = 133
      Width = 52
      Height = 14
      Caption = 'Valor ICMS'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb49: TLabel
      Left = 320
      Top = 133
      Width = 59
      Height = 14
      Caption = 'Base Subst.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
    end
    object lb46: TLabel
      Left = 649
      Top = 135
      Width = 35
      Height = 14
      Caption = 'Seguro'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label1: TLabel
      Left = 247
      Top = 44
      Width = 25
      Height = 14
      Caption = 'Serie'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbVerificado: TLabel
      Left = 624
      Top = 198
      Width = 126
      Height = 16
      Caption = 'Verificado para sped'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object imgVerificado: TImage
      Left = 592
      Top = 190
      Width = 24
      Height = 24
      Transparent = True
    end
    object edtCodigo: TEdit
      Left = 13
      Top = 20
      Width = 54
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 0
    end
    object edtData: TMaskEdit
      Left = 537
      Top = 59
      Width = 61
      Height = 20
      EditMask = '!99/99/9999;1;_'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 8
      Text = '  /  /    '
      OnKeyDown = edtDataKeyDown
    end
    object edtEMISSAONF: TMaskEdit
      Left = 459
      Top = 59
      Width = 62
      Height = 20
      EditMask = '!99/99/9999;1;_'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 7
      Text = '  /  /    '
      OnKeyDown = edtEMISSAONFKeyDown
    end
    object edtNF: TEdit
      Left = 181
      Top = 59
      Width = 54
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 5
      OnKeyPress = edtFornecedorKeyPress
    end
    object edtFornecedor: TEdit
      Left = 93
      Top = 59
      Width = 55
      Height = 20
      Color = 6073854
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 4
      OnDblClick = edtFornecedorDblClick
      OnExit = EdtFORNECEDORExit
      OnKeyDown = EdtFORNECEDORKeyDown
      OnKeyPress = edtFornecedorKeyPress
    end
    object mmoobservacoes: TMemo
      Left = 107
      Top = 160
      Width = 456
      Height = 59
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      Lines.Strings = (
        'memoobservacoes')
      MaxLength = 500
      ParentFont = False
      TabOrder = 22
      OnKeyPress = mmoobservacoesKeyPress
    end
    object edtDESCONTOS: TEdit
      Left = 573
      Top = 99
      Width = 70
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 14
      OnKeyPress = edtVALORPRODUTOSKeyPress
    end
    object edtVALORFRETE: TEdit
      Left = 392
      Top = 100
      Width = 70
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 13
      OnKeyPress = edtVALORPRODUTOSKeyPress
    end
    object edtOUTROSGASTOS: TEdit
      Left = 725
      Top = 100
      Width = 70
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 15
      OnKeyPress = edtVALORPRODUTOSKeyPress
    end
    object edtvaloripi: TEdit
      Left = 881
      Top = 99
      Width = 70
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 16
      OnKeyPress = edtVALORPRODUTOSKeyPress
    end
    object grpGroupCreditos: TGroupBox
      Left = 587
      Top = 269
      Width = 326
      Height = 61
      Caption = 'Cr'#233'ditos'
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 23
      Visible = False
      object lb10: TLabel
        Left = 7
        Top = 16
        Width = 24
        Height = 14
        Caption = 'ICMS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object lb11: TLabel
        Left = 83
        Top = 16
        Width = 15
        Height = 14
        Caption = 'PIS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object lb12: TLabel
        Left = 159
        Top = 16
        Width = 37
        Height = 14
        Caption = 'COFINS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object lb13: TLabel
        Left = 234
        Top = 16
        Width = 10
        Height = 14
        Caption = 'IPI'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object edtcreditoicms: TEdit
        Left = 7
        Top = 32
        Width = 70
        Height = 20
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
      end
      object edtcreditopis: TEdit
        Left = 83
        Top = 32
        Width = 70
        Height = 20
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
      end
      object edtcreditocofins: TEdit
        Left = 159
        Top = 32
        Width = 70
        Height = 20
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
      end
      object edtcreditoipi: TEdit
        Left = 234
        Top = 32
        Width = 70
        Height = 20
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
      end
    end
    object edticmssubstituto: TEdit
      Left = 573
      Top = 129
      Width = 70
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 20
      OnKeyPress = edtVALORPRODUTOSKeyPress
    end
    object chkCheckFreteFornecedor: TCheckBox
      Left = 650
      Top = 7
      Width = 13
      Height = 13
      TabStop = False
      TabOrder = 9
    end
    object chkCheckIcmsFrete: TCheckBox
      Left = 650
      Top = 22
      Width = 13
      Height = 13
      TabStop = False
      TabOrder = 10
    end
    object edtVALORPRODUTOS: TMaskEdit
      Left = 107
      Top = 100
      Width = 70
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      OnKeyPress = edtVALORPRODUTOSKeyPress
    end
    object edtValorFinal: TMaskEdit
      Left = 240
      Top = 100
      Width = 70
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 12
      OnKeyPress = edtVALORPRODUTOSKeyPress
    end
    object edtModeloNF: TEdit
      Left = 93
      Top = 20
      Width = 55
      Height = 20
      Color = 6073854
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnDblClick = edtModeloNFDblClick
      OnKeyDown = edtModeloNFKeyDown
      OnKeyPress = edtFornecedorKeyPress
    end
    object edtcfop: TEdit
      Left = 13
      Top = 59
      Width = 55
      Height = 20
      Color = 6073854
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnDblClick = edtcfopDblClick
      OnKeyDown = edtcfopKeyDown
      OnKeyPress = edtFornecedorKeyPress
    end
    object edtChaveAcesso: TEdit
      Left = 181
      Top = 20
      Width = 415
      Height = 20
      Color = 6073854
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnKeyDown = edtChaveAcessoKeyDown
    end
    object edtBASECALCULOICMS: TEdit
      Left = 107
      Top = 130
      Width = 71
      Height = 20
      Hint = 'Base de calculo do ICMS'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 17
      OnKeyPress = edtVALORPRODUTOSKeyPress
    end
    object edtVALORICMS: TEdit
      Left = 240
      Top = 130
      Width = 70
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 18
      OnKeyPress = edtVALORPRODUTOSKeyPress
    end
    object edtBASECALCULOICMSSUBSTITUICAO: TEdit
      Left = 392
      Top = 130
      Width = 70
      Height = 20
      Hint = 'Base de calculo do ICMS ST'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 19
      OnKeyPress = edtVALORPRODUTOSKeyPress
    end
    object edtValorSeguro: TEdit
      Left = 693
      Top = 129
      Width = 70
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 21
      Visible = False
      OnKeyPress = edtVALORPRODUTOSKeyPress
    end
    object edtSerieNF: TEdit
      Left = 247
      Top = 60
      Width = 57
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
    end
  end
  object query: TIBQuery
    Left = 904
    Top = 212
  end
  object ImageList1: TImageList
    Height = 24
    Width = 24
    Left = 800
    Top = 228
    Bitmap = {
      494C010102000400040018001800FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000600000001800000001002000000000000024
      000000000000000000000000000000000000A2663E00A2663E00A2663E00A266
      3E009961450088585300A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00895852009A61
      4500A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00817F3E004AAE420047B547006E96
      4600A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E00A2663E009961
      4500382C9C002424B5004D378300A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00503981003B3BC9004439
      A3009A624500A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E0077823A0040B9440074D2750084DD870059C7
      5F00798B4400A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E0099614500392E
      9D003636C4003333C5002F2FBE004E378300A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00503981004545D5005C5CF3006161
      F300453AA3009A624500A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E007480360037B43A006FCD6D0075D3750079D97B0086E0
      8B004DB84C0096714000A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E0099614500382D9D003535
      C3001919BC000B0BBA003232C6003030BF004E378300A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E004F3881004343D2005555EE003C3CF1004E4E
      F7006565F700453AA3009A624500A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00688331002FAF300066C662006BCB680059CA590063D2650087E0
      8B0077D87D0059A44500A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E004136A0007777DB001717
      BA000606B7000808B9000D0DBC003434C8003131C0004E378300A2663E00A266
      3E00A2663E00A2663E004F3881003F3FCF005050E8003535E9003838EE003C3C
      F3004D4DF6005F5FF1003F349E00A2663E00A2663E00A2663E00A2663E00A266
      3E004A8C260026AA26005DBF57005AC154004BBF470054C753005ECE5F0071D9
      74008EE5930059C75F00778A4200A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E003333BA008888E2005454
      D1000606B6000808B9000A0ABB000F0FBE003535CA003232C2004E388300A266
      3E00A2663E004F3882003C3CCC004A4AE1002E2EE1003030E5003434EA003737
      ED004040F0005D5DF1002E2EBB00A2663E00A2663E00A2663E00A2663E00448A
      1F002AA9270059BA500050B948003FB5380048BC430053C450005CCC5C0063D3
      660080DF85008CE5920049B6480096703F00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E006E4969003F3FC0008989
      E2005454D2000707B8000909BA000B0BBD001111C0003737CC003434C3004E38
      83004E3882003939C8004545DC002828DA002929DD002D2DE2003030E5003A3A
      E8005959ED003535C300704B6600A2663E00A2663E00A2663E003F86190022A4
      1D0052B4470047B23D0034AC2A003CB3340047BA410069CA670084D7830064D1
      650069D66C008BE48F0077D97D0056A34100A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E006E4969003F3F
      BF008989E3005555D3000909BA000B0BBC000D0DBE001212C2003838CE003535
      C4003737C6004141D7002222D3002222D6002626DA002929DD003333E1005454
      E7003333C200704B6700A2663E00A2663E00A2663E003D86170020A21B004BAE
      3E003FAB33002AA41E0031AA260040B3370064C45E0079CE76009DDE9C0081D7
      810061D1630072DA76008EE5930055C55B00758A3F00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E00A2663E006E49
      68003F3FC0008989E3005656D4000A0ABB000C0CBD000E0EC0001414C4003A3A
      CF003C3CD2001C1CCD001C1CD0001F1FD3002222D6002D2DDA004F4FE2003232
      C000704B6700A2663E00A2663E00A2663E00517F1F0038AD350078C16E003FA8
      3100219D120027A21A0035AA2A005DBD550071C86E0021A923007BCE7A009CDE
      9B006AD16A0062D264007EDD820088E28D0045B3440095703E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E00A2663E00A266
      3E006F4968004040C0008A8AE3005656D5000B0BBC000D0DBF000F0FC1001515
      C5001717C7001717CA001A1ACC001C1CD0002727D4004A4ADD003030BE00704B
      6700A2663E00A2663E00A2663E00A2663E000A9404008DCB850064B7570042A9
      34002EA11F0034A6270058B74E0052B94E0022A01C006D7D300029AC2B0093D9
      92008DD98C005ECD5E0064D2660083DE860070D4750053A13D00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E00A2663E00A266
      3E00A2663E006F4968004040C0008A8AE3005757D5000C0CBD000E0EBF001010
      C2001212C4001515C7001717CA002222CE004646D7002D2DBD00704A6700A266
      3E00A2663E00A2663E00A2663E00A2663E000294000096CD8F006FBC640044A9
      360053B147006BBD61004EB54900249312008F6D3800A2663E005C852B0058BF
      58009BDC990074D1720059CB590067D2690084DD86004EC1530073893E00A266
      3E00A2663E00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E006F4968004040C0007B7BDE001414BE000C0CBE000E0E
      C0001010C2001212C4001D1DC9004242D3002C2CBB00704A6700A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00148F08007AC2740095CD8C008BC9
      81008ECC86004CB548001E8F0C0098693A00A2663E00A2663E00996A3B00279B
      1C0080CF7E0095D992005BC8590057C856006ED26E007BD77D0040B13E009470
      3E00A2663E00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E006F4968002D2DBD003030C4000C0CBB000B0BBC000C0C
      BE000E0EC0001010C2001515C4003A3ACF003535C300704A6700A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00846F320014960D0042AE3E004BB2
      47001CA21B003388130098693A00A2663E00A2663E00A2663E00A2663E007E75
      330034B0350097D893007BCF77004EC24B0055C6530074D2740064CA65004FA0
      3A00A2663E00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E00A2663E00A266
      3E00A2663E006E4969004343C3004F4FCD001D1DBE000A0ABA000909BA000B0B
      BC000C0CBE000E0EBF000F0FC1001414C3003838CE003333C3006F4A6700A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E0098693A00657827005B7B
      2300846F3200A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E003E8F200069C467009BD998006AC8660055C251005DC75A0079D277004ABC
      4D0071873B00A2663E00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E00A2663E00A266
      3E006E4969004545C4005656CE003737C5003333C5003232C5002424C2002323
      C2002727C5000C0CBD000D0DBE000E0EC0001212C2003737CC003232C1006F4A
      6700A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00906D380023A11D0098D6940094D691005EC258005DC3580071CB6D007BCF
      78003CAE390094703D00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E00A2663E006E49
      69004646C4005959CE003A3AC5003636C4003535C5003333C5003A3AC7005D5D
      D2009797E7005858D4002B2BC6002626C5002626C6002929C7004949D0003F3F
      C5006F4A6800A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00617F290045B544009FD89A007ECC790057BD50005BBF540077CB
      720063C461004C9D3700A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E006E4969004949
      C5005D5DCF003E3EC6003A3AC4003838C4003636C4003C3CC6005B5BD0004646
      C1005151C6009C9CE8005858D3003030C6002F2FC7002F2FC7003232C8005050
      D0004141C4006F4A6800A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E002994170079C8750099D493005FBE570053B94B005DBE
      550077C9710043B744006F873900A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E006E4869005050C7006060
      CF004242C7003E3EC5003B3BC5003A3AC4003F3FC6005B5BCF003E3EBF006F49
      68006F4968005252C6009C9CE8005959D3003030C6003030C6003030C6003232
      C7005151CF004242C4006F496800A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00857134001DA51E009BD4950087CC80004EB544004FB6
      450063BE5B006FC468003AAC3700936F3C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E005454C900A2A2E9005151
      CB004141C6004040C6003E3EC5004343C7005E5ECF003F3FC0006E496900A266
      3E00A2663E006F4968005252C6009C9CE8005A5AD3003232C5003232C5003131
      C5003636C7005A5AD1003434BC00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E004C85200052B84F0098D2910068BE5F004BB1
      3F004DB342006BC0620059BB5500509733000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E004338A2009A9AE6009B9B
      E7004F4FCA004141C6004747C8006161D0004141C1006E496900A2663E00A266
      3E00A2663E00A2663E006E4968005252C6009D9DE8005B5BD2003333C5003333
      C5004C4CCC005454CC003D319D00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E0098693B001A9C140086CA7F0090CD88004EB1
      420047AE3B0053B348006DBE630027AC2A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00996145004338A2009A9A
      E6009B9BE7005656CD006464D1004444C2006E496900A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E006E4968005353C7009E9EE8005C5CD2004E4E
      CC005555CC003D319D0099614500A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E0071772C002AA9290096D08E007AC3
      700048AC3A0050B043006ABC5F0022A924000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E00996145004338
      A2009A9AE600A2A2E9004E4EC6006E496900A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E006E4969006565CF009F9FE8007E7E
      DB003D319E0099614500A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E002C8E130060BB5A0096CE
      8D008CCA83008BC9810074C36E003F9728000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E00A2663E009961
      45003F34A1003D3DC0006E486900A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E006E4969004B4BC4004136
      9F0099614500A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E0098693A0022910F003CAF
      390050B64C0042B24000279B1C00887238000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E00A266
      3E00A2663E00A2663E00A2663E00A2663E00A2663E00A2663E0098693A00677A
      2900558223006A7B2C00A2663E00A2663E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000060000000180000000100010000000000200100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000}
  end
  object XMLDocument1: TXMLDocument
    Left = 872
    Top = 228
    DOMVendorDesc = 'MSXML'
  end
end
