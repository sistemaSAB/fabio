unit UobjPEDIDO_PEDIDO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJCLIENTE
,UOBJVENDEDOR,uobjarquiteto,UCLIENTE,UARQUITETO,UVENDEDOR,UObjPrazoPagamento;

Type
   TObjPEDIDO_PEDIDO=class

          Public
                //ObjDatasource                               :TDataSource;
               Status                                      :TDataSetState;
               SqlInicial                                  :String[200];
               Cliente:TOBJCLIENTE;
               Vendedor:TOBJVENDEDOR;
               Arquiteto:TobjArquiteto;
               //Titulo:TOBJTITULO;
               PrazoPagamento:TObjPrazoPagamento;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;overload;
                Function    Salvar(ComCommit:Boolean;Pverificadesconto:boolean):Boolean;overload;

                
                Function    LocalizaCodigo(Parametro:string) :boolean;
                function    LocalizaTitulo(parametro: string): boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_PesquisaConcluido           :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Descricao(parametro: string);
                Function Get_Descricao: string;
                Procedure Submit_Complemento(parametro: string);
                Function Get_Complemento: string;
                 //ADD email(jonatan)
                Procedure Submit_Email(parametro: string);
                Function Get_Email: string;

                Procedure Submit_Obra(parametro: string);
                Function Get_Obra: string;
                Procedure Submit_Data(parametro: string);
                Function Get_Data: string;
                Procedure Submit_Concluido(parametro: string);
                Function Get_Concluido: string;

                Procedure Submit_valortitulo(Parametro:string);
                Function  Get_valortitulo:string;

                Procedure Submit_Observacao(parametro: String);
                Function Get_Observacao: String;

                Procedure Submit_ValorComissaoArquiteto(parametro:string);
                Function get_ValorComissaoArquiteto:string;

                Function Get_ValorTotal:string;
                Function Get_ValorAcrescimo:string;
                Function Get_ValorDesconto:string;
                Function Get_ValorFinal:string;
                Function Get_Titulo:string;
                Function Get_AlteradoporTroca:string;
                Function Get_ValorBonificacaoCliente:string;
                Function Get_BonificacaoGerada:string;

                function Get_PorcentagemArquiteto:string;
                procedure Submit_PorcentagemArquiteto(parametro:string);

                procedure Submit_DataOrcamento(parametro:string);
                function Get_DataOrcameto:string;

                procedure Submit_DataEntrega(parametro:string);
                Function Get_DataEntrega:string;


                Procedure Submit_ValorTotal(parametro:string);
                Procedure Submit_ValorAcrescimo(parametro:string);
                Procedure Submit_ValorDesconto(parametro:string);
                Procedure Submit_Titulo(parametro:string);
                Procedure Submit_AlteradoporTroca(parametro:string);
                Procedure Submit_ValorBonificacaoCliente(Parametro:string);
                Procedure Submit_BonificacaoGerada(parametro:string);
                procedure submit_operacao(operacao:string);

                function Get_CodigoNFCe: string;
                function Get_GeraCupom: string;
                function Get_NumeroNFCe: string;
                function Get_SerieNFCe: string;
                function get_operacao:string;
                procedure Submit_CodigoNFCe(parametro: String);
                procedure Submit_GeraCupom(parametro: String);
                procedure Submit_NumeroNFCe(parametro: String);
                procedure Submit_SerieNFCe(parametro: String);

                procedure EdtClienteExit(Sender: TObject;LABELNOME:TLABEL);

                procedure EdtClienteKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtClienteKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
                procedure EdtClienteDblClick(Sender: TObject);

                procedure EdtVendedorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtVendedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtVendedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;

                procedure EdtarquitetoExit(Sender: TObject; LABELNOME: TLABEL);
                procedure EdtarquitetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);overload;
                procedure EdtarquitetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;


                Function PrimeiroRegistro:Boolean;
                Function UltimoRegistro:Boolean;
                Function ProximoRegisto(PCodigo:Integer):Boolean;
                Function RegistoAnterior(PCodigo:Integer):Boolean;
                Function ExcluiRelacionamentos(PPedido:string):Boolean;
                Function VerificaPedidoEmRomaneio(PPedido:string):Boolean;
                function Get_Campo(Pcampo: string): String;
                function AtualizaGeraCupom(pPedido: string): boolean;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Titulo:string;
               Descricao:string;
               Complemento:string;
               Obra:string;
               Data:string;
               Concluido:string;
               ValorTotal:string;
               ValorAcrescimo:string;
               ValorDesconto:string;
               ValorBonificacaoCliente:string;
               ValorFinal:string;
               Observacao : string;
               ValorComissaoArquiteto:string;
               BonificacaoGerada:string;
               AlteradoporTroca:string;
               valortitulo:string;
               Email:string;
               PorcentagemComissaoaArquiteto:string;
               DataOrcamento:string;
               DataEntrega:string;
               operacao:string;

               GeraCupom, NumeroNFCe, CodigoNFCe, SerieNFCe:string;


               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
               function  Verificavalordesconto: boolean;
               function  VerificaBonificacao: boolean;




                //procedure RetornarVenda(pcodigo: string);
   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios;


{ TTabTitulo }


Function  TObjPEDIDO_PEDIDO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Descricao:=fieldbyname('Descricao').asstring;
        Self.Complemento:=fieldbyname('Complemento').asstring;
        Self.Email:=fieldbyname('Email').asstring;
        Self.Obra:=fieldbyname('Obra').asstring;
        Self.ValorComissaoArquiteto:=Fieldbyname('ValorComissaoArquiteto').asstring;
        Self.AlteradoporTroca:=Fieldbyname('AlteradoporTroca').asstring;

        If(FieldByName('Cliente').asstring<>'')
        Then Begin
                 If (Self.Cliente.LocalizaCodigo(FieldByName('Cliente').asstring)=False)
                 Then Begin
                          Messagedlg('Cliente N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Cliente.TabelaparaObjeto;
        End;

        If(FieldByName('arquiteto').asstring<>'')
        Then Begin
                 If (Self.arquiteto.LocalizaCodigo(FieldByName('arquiteto').asstring)=False)
                 Then Begin
                          Messagedlg('Arquiteto N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.arquiteto.TabelaparaObjeto;
        End;

        If(FieldByName('Vendedor').asstring<>'')
        Then Begin
                 If (Self.Vendedor.LocalizaCodigo(FieldByName('Vendedor').asstring)=False)
                 Then Begin
                          Messagedlg('Vendedor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Vendedor.TabelaparaObjeto;
        End;

        Self.Titulo:=fieldbyname('Titulo').asstring;
        Self.Data:=fieldbyname('Data').asstring;
        Self.Concluido:=fieldbyname('Concluido').asstring;
        Self.ValorTotal     :=Fieldbyname('ValorTotal').asstring;
        Self.ValorAcrescimo :=Fieldbyname('ValorAcrescimo').asstring;
        Self.ValorDesconto  :=Fieldbyname('ValorDesconto').asstring;
        Self.ValorBonificacaoCliente:=Fieldbyname('ValorBonificacaoCliente').asstring;
        Self.ValorFinal     :=Fieldbyname('ValorFinal').asstring;
        Self.Observacao     :=Fieldbyname('Observacao').AsString;
        Self.BonificacaoGerada:=Fieldbyname('BonificacaoGerada').asstring;
        Self.valortitulo:=FieldByname('valortitulo').asstring;
        self.PorcentagemComissaoaArquiteto:=fieldbyname('porcentagemcomissaoarquiteto').AsString;
        self.DataOrcamento:=fieldbyname('dataorcamento').AsString;
        self.DataEntrega:=fieldbyname('DataEntrega').AsString;
        if(FieldByName('prazopagamento').asstring<>'') then
        begin
          if not(Self.PrazoPagamento.LocalizaCodigo(FieldByName('prazopagamento').asstring)) then
          begin
            MensagemErro('Prazo de Pagamento N�o encontrado!');
            Self.ZerarTabela;
            result:=False;
            exit;
          end
          else
            Self.PrazoPagamento.TabelaparaObjeto;
        end;

        self.GeraCupom:=fieldbyname('geracupom').AsString;
        self.NumeroNFCe:=fieldbyname('NumeroNFCe').AsString;
        self.CodigoNFCe:=fieldbyname('CodigoNFCe').AsString;
        self.SerieNFCe:=fieldbyname('SerieNFCe').AsString;
        self.operacao:=fieldbyname('operacao').AsString;
        result:=True;
     End;
end;


Procedure TObjPEDIDO_PEDIDO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Descricao').asstring:=Self.Descricao;
        ParamByName('Complemento').asstring:=Self.Complemento;
        ParamByName('Email').asstring:=Self.Email;
        ParamByName('Obra').asstring:=Self.Obra;
        ParamByName('Cliente').asstring:=Self.Cliente.GET_CODIGO;
        ParamByName('arquiteto').asstring:=Self.arquiteto.GET_CODIGO;
        ParamByName('Vendedor').asstring:=Self.Vendedor.GET_CODIGO;
        ParamByName('Titulo').asstring:=Self.Get_Titulo;
        ParamByName('Data').asstring:=Self.Data;
        ParamByName('Concluido').asstring:=Self.Concluido;
        ParamByName('ValorTotal').asstring      :=virgulaparaponto(Self.ValorTotal     );
        ParamByName('ValorAcrescimo').asstring  :=virgulaparaponto(Self.ValorAcrescimo );
        ParamByName('ValorDesconto').asstring   :=virgulaparaponto(Self.ValorDesconto  );
        ParamByName('ValorBonificacaoCliente').asstring:=VirgulaParaPonto(Self.ValorBonificacaoCliente);
        Parambyname('Observacao').AsString      :=Self.Observacao;
        ParamByName('ValorComissaoArquiteto').asstring:=virgulaparaponto(Self.ValorComissaoArquiteto);
        ParamByName('BonificacaoGerada').asstring:=Virgulaparaponto(Self.BonificacaoGerada);
        ParamByName('AlteradoporTroca').asstring:=Self.AlteradoporTroca;
        ParamByName('valortitulo').asstring:=virgulaparaponto(Self.valortitulo);
        ParamByName('porcentagemcomissaoarquiteto').AsString:=Self.PorcentagemComissaoaArquiteto;
        ParamByName('dataorcamento').AsString:=Self.DataOrcamento;
        ParamByName('DataEntrega').AsString:=self.DataEntrega;
        ParamByName('PrazoPagamento').asstring:=Self.PrazoPagamento.GET_CODIGO;
        ParamByName('GeraCupom').asstring := Self.GeraCupom;
        ParamByName('CodigoNFCe').asstring := Self.CodigoNFCe;
        ParamByName('SerieNFCe').asstring := Self.SerieNFCe;
        ParamByName('NumeroNFCe').asstring := Self.NumeroNFCe;
        ParamByName('operacao').AsString := self.operacao;       
  End;
End;

//***********************************************************************

function TObjPEDIDO_PEDIDO.Salvar(ComCommit:Boolean):Boolean;
begin
     //salvo verificando o desconto
     result:=Self.Salvar(ComCommit,true);
End;

function TObjPEDIDO_PEDIDO.Salvar(ComCommit:Boolean;Pverificadesconto:boolean):Boolean;
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
     result:=False;

     if (Self.VerificaBrancos=True)
     Then exit;

     if (Self.VerificaNumericos=False)
     Then Exit;

     if (Self.VerificaData=False)
     Then Exit;

     if (Self.VerificaFaixa=False)
     Then Exit;

     if (Self.VerificaRelacionamentos=False)
     Then Exit;

     if (Pverificadesconto=True)
     then begin
               if (Self.Verificavalordesconto=False)
               then exit;
     End;

     //if (self.VerificaBonificacao=False)
     //Then exit;


     If Self.LocalizaCodigo(Self.CODIGO)=False
     Then Begin
               if(Self.Status=dsedit)
               Then Begin
                         Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                         exit;
               End;
     End
     Else Begin
               if(Self.Status=dsinsert)
               Then Begin
                         Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                         exit;
               End;
     End;

     if (Self.status=dsinsert)
     Then Begin
               Self.Objquery.SQL.Clear;
               Self.Objquery.SQL.text:=Self.InsertSql.Text;
               if (Self.Codigo='0')
               Then Self.codigo:=Self.Get_NovoCodigo;
     End
     Else Begin
               if (Self.Status=dsedit)
               Then Begin
                         Self.Objquery.SQL.Clear;
                         Self.Objquery.SQL.text:=Self.ModifySQl.Text;
               End
               Else Begin
                         Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                         exit;
               End;
     End;

     Self.ObjetoParaTabela;

     Try
        Self.Objquery.ExecSQL;

     Except
        if (Self.Status=dsInsert)
        Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
        Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0);
        exit;
     End;

     If ComCommit=True
     Then FDataModulo.IBTransaction.CommitRetaining;

     Self.status          :=dsInactive;
     result:=True;
end;

procedure TObjPEDIDO_PEDIDO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Descricao:='';
        Complemento:='';
        Obra:='';
        Cliente.ZerarTabela;
        Arquiteto.ZerarTabela;
        Vendedor.ZerarTabela;
        Titulo:='';
        Data:='';
        Concluido:='';
        ValorTotal:='';
        ValorAcrescimo:='';
        ValorDesconto:='';
        ValorBonificacaoCliente:='';
        Observacao:='';
        ValorComissaoArquiteto:='';
        AlteradoporTroca:='';
        BonificacaoGerada:='';
        valortitulo:='';
        Email := '';
        DataOrcamento:='';
        DataEntrega:='';
        PrazoPagamento.ZerarTabela;
        GeraCupom := '';
        NumeroNFCe := '';
        CodigoNFCe := '';
        SerieNFCe := '';
        operacao := '';        
     End;
end;

Function TObjPEDIDO_PEDIDO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/C�digo';

      if (ValorTotal='')
      Then Mensagem:=mensagem+'/Valor Total';

      if (ValorAcrescimo='')
      Then Mensagem:=Mensagem+'/Valor Acr�scimo';

      if (ValorDesconto='')
      Then Mensagem:=Mensagem+'/Valor Desconto';

      if (ValorBonificacaoCliente='')
      Then Self.ValorBonificacaoCliente:='0';

      if (ValorComissaoArquiteto='')
      Then ValorComissaoArquiteto:='0';

      if (AlteradoporTroca='')
      then AlteradoporTroca:='N';

      if (BonificacaoGerada='')
      Then BonificacaoGerada:='0';

      if (valortitulo='')
      Then valortitulo:='0';

      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPEDIDO_PEDIDO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     If (Self.Cliente.LocalizaCodigo(Self.Cliente.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Cliente n�o Encontrado!';

     if (Self.Arquiteto.Get_CODIGO<>'')
     Then Begin
               If (Self.arquiteto.LocalizaCodigo(Self.arquiteto.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Arquiteto n�o Encontrado!';
     End;


      If (Self.Vendedor.LocalizaCodigo(Self.Vendedor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Vendedor n�o Encontrado!';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjPEDIDO_PEDIDO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.Cliente.Get_Codigo<>'')
        Then Strtoint(Self.Cliente.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Cliente';
     End;

     try
        If (Self.arquiteto.Get_Codigo<>'')
        Then Strtoint(Self.arquiteto.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Arquiteto';
     End;

     try
        If (Self.Vendedor.Get_Codigo<>'')
        Then Strtoint(Self.Vendedor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Vendedor';
     End;

     try
        If (Self.Titulo<>'')
        Then Strtoint(Self.Titulo);
     Except
           Mensagem:=mensagem+'/T�tulo';
     End;


     try
        Strtofloat(Self.ValorTotal);
     Except
           Mensagem:=mensagem+'/Valor Total';
     End;

     try
        Strtofloat(Self.ValorAcrescimo);
     Except
           Mensagem:=mensagem+'/Valor Acr�scimos';
     End;

     try
        Strtofloat(Self.ValorDesconto);
     Except
           Mensagem:=mensagem+'/Valor Desconto';
     End;

     Try
           StrtoFloat(Self.ValorBonificacaoCliente);
     Except
           Mensagem:=Mensagem+'/ Valor de Bonifica��o de Cliente';
     End;

     try
        Strtofloat(self.ValorComissaoArquiteto);

        if (Strtofloat(self.ValorComissaoArquiteto)>0)
        and (Self.Arquiteto.Get_CODIGO='')
        then Begin
                  Messagedlg('N�o � poss�vel determinar uma faixa de comiss�o de arquiteto sem escolher um  arquiteto',mterror,[mbok],0);
                  exit;
        End;

     except
           mensagem:=Mensagem+'/ Valor da Comiss�o do Arquiteto';
     End;

     Try
        Strtofloat(BonificacaoGerada);
     Except
        mensagem:=mensagem+'/Valor da Bonifica��o Gerada';
     End;

     Try
        Strtofloat(valortitulo);
     Except
        Mensagem:=Mensagem+'/Valor do T�tulo';
     End;

//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPEDIDO_PEDIDO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.Data);
     Except
           Mensagem:=mensagem+'/Data';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPEDIDO_PEDIDO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';

        if ((AlteradoporTroca<>'S') and (AlteradoporTroca<>'N'))
        Then Self.AlteradoporTroca:='N';

//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

Function TobjPedido_Pedido.Get_Campo(Pcampo:string):String;
Begin
     result:='';
     Try
       if (Self.Objquery.Active=True)
       Then result:=Self.Objquery.fieldbyname(pcampo).asstring;
     Except
     End;
End;


function TObjPEDIDO_PEDIDO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PEDIDO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select TB.Codigo,TB.Descricao,TB.Complemento,TB.Obra,TB.Cliente,TB.Arquiteto,TB.Vendedor,TB.Titulo,tb.porcentagemcomissaoarquiteto');
           SQL.ADD(' ,TB.Data,TB.Concluido,TB.valortotal,TB.valoracrescimo,TB.valordesconto,TB.ValorBonificacaoCliente,TB.BonificacaoGerada,TB.valorfinal');
           SQL.ADD(' ,TB.ValorComissaoArquiteto,TB.valortitulo,TB.Observacao, TB.AlteradoporTroca,TB.email');

           SQL.ADD(',DataEntrega,TB.porcentagemcomissaoarquiteto,TB.dataorcamento,tb.prazopagamento');
           SQL.ADD(' ,GeraCupom, NumeroNFCe, CodigoNFCe, SerieNFCe,operacao');
           sql.add(' from  TabPedido TB');
           SQL.ADD('left join tabcliente TC on TC.codigo = TB.cliente');
           SQL.ADD(' WHERE TB.codigo='+parametro);

//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;



function TObjPEDIDO_PEDIDO.LocalizaTitulo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PEDIDO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select TB.Codigo,TB.Descricao,TB.Complemento,TB.Obra,TB.Cliente,TB.Arquiteto,TB.Vendedor,TB.Titulo,tb.porcentagemcomissaoarquiteto');
           SQL.ADD(' ,TB.Data,TB.Concluido,TB.valortotal,TB.valoracrescimo,TB.valordesconto,TB.ValorBonificacaoCliente,TB.BonificacaoGerada,TB.valorfinal,TB.ValorComissaoArquiteto,TB.valortitulo,TB.Observacao, TB.AlteradoporTroca,TC.email');
           SQL.ADD(' ,TB.dataorcamento,DataEntrega,tb.prazopagamento');
           SQL.ADD(' ,GeraCupom, NumeroNFCe, CodigoNFCe, SerieNFCe,operacao');
           SQL.Add(' from  TabPedido TB');
           SQL.ADD('left join tabcliente TC on TC.codigo = TB.cliente');
           SQL.ADD(' WHERE TB.Titulo='+parametro);

//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPEDIDO_PEDIDO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPEDIDO_PEDIDO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPEDIDO_PEDIDO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);   //criando componentes IBDataser
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Cliente:=TOBJCLIENTE.create;
        Self.arquiteto:=TOBJarquiteto.create;
        Self.Vendedor:=TOBJVENDEDOR.create;
        //Self.Titulo:=TOBJTITULO.create;
        self.PrazoPagamento := TObjPrazoPagamento.Create;


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabPedido(Codigo,Descricao,Complemento,Obra');
                InsertSQL.add(' ,Cliente,Arquiteto,Vendedor,Titulo,Data,Concluido,valortotal,valortitulo,valoracrescimo,valordesconto,ValorBonificacaoCliente, BonificacaoGerada,ValorComissaoArquiteto,Observacao,email,AlteradoporTroca,porcentagemcomissaoarquiteto,dataorcamento');
                InsertSql.Add(',DataEntrega,prazopagamento');
                InsertSQL.add(' ,GeraCupom, NumeroNFCe, CodigoNFCe, SerieNFCe,operacao');
                InsertSQL.add(')');
                InsertSQL.add('values (:Codigo,:Descricao,:Complemento,:Obra,:Cliente,:Arquiteto');
                InsertSQL.add(' ,:Vendedor,:Titulo,:Data,:Concluido,:valortotal,:valortitulo,:valoracrescimo,:valordesconto,:ValorBonificacaoCliente,:BonificacaoGerada,:ValorComissaoArquiteto,:Observacao,:email,:AlteradoporTroca,:porcentagemcomissaoarquiteto,:dataorcamento');
                InsertSql.Add(',:DataEntrega,:prazopagamento');
                InsertSQL.add(' ,:GeraCupom, :NumeroNFCe, :CodigoNFCe, :SerieNFCe,:operacao');
                InsertSQL.add(')');


                //CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabPedido set Codigo=:Codigo,Descricao=:Descricao');
                ModifySQL.add(',Complemento=:Complemento,Obra=:Obra,Cliente=:Cliente,Arquiteto=:Arquiteto');
                ModifySQL.add(',Vendedor=:Vendedor,Titulo=:Titulo,Data=:Data,Concluido=:Concluido');
                ModifySQL.add(',valortotal=:valortotal,valoracrescimo=:valoracrescimo,valordesconto=:valordesconto,');
                ModifySQL.add('ValorBonificacaoCliente=:ValorBonificacaoCliente,BonificacaoGerada=:BonificacaoGerada,ValorComissaoArquiteto=:ValorComissaoArquiteto,valortitulo=:valortitulo,Observacao=:Observacao,');
                ModifySQL.add('Email=:email,AlteradoporTroca=:AlteradoporTroca, porcentagemcomissaoarquiteto=:porcentagemcomissaoarquiteto, dataorcamento=:dataorcamento,DataEntrega=:DataEntrega');
                ModifySQL.add(',prazopagamento=:prazopagamento');
                ModifySQL.add(',GeraCupom=:GeraCupom, NumeroNFCe=:NumeroNFCe, CodigoNFCe=:CodigoNFCe, SerieNFCe=:SerieNFCe,operacao=:operacao');
                
                ModifySQL.add(' where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabPedido where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPEDIDO_PEDIDO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPEDIDO_PEDIDO.Get_Pesquisa: TStringList;
begin
  if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('VISUALIZAR TODOS OS OR�AMENTOS')=False) then
  begin
    Self.ParametroPesquisa.clear;
    Self.ParametroPesquisa.add('Select * from ViewPedido where userc='+#39+ObjUsuarioGlobal.Get_nome+#39);
    Result:=Self.ParametroPesquisa;
  end
  else
  begin
    Self.ParametroPesquisa.clear;
    Self.ParametroPesquisa.add('Select * from ViewPedido');
    Result:=Self.ParametroPesquisa;
  end;
end;

function TObjPEDIDO_PEDIDO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de PEDIDO ';
end;


function TObjPEDIDO_PEDIDO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENPEDIDO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPEDIDO_PEDIDO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Cliente.FREE;
    Self.Arquiteto.Free;
    Self.Vendedor.FREE;
    Self.PrazoPagamento.Free;
    
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPEDIDO_PEDIDO.RetornaCampoCodigo: string;
begin
      result:='Codigo';
end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPEDIDO_PEDIDO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPEDIDO_PEDIDO.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjPEDIDO_PEDIDO.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjPEDIDO_PEDIDO.Submit_Descricao(parametro: string);
begin
        Self.Descricao:=Parametro;
end;
function TObjPEDIDO_PEDIDO.Get_Descricao: string;
begin
        Result:=Self.Descricao;
end;
procedure TObjPEDIDO_PEDIDO.Submit_Complemento(parametro: string);
begin
        Self.Complemento:=Parametro;
end;


function TObjPEDIDO_PEDIDO.Get_Complemento: string;
begin
        Result:=Self.Complemento;
end;

procedure TObjPEDIDO_PEDIDO.Submit_Email(parametro:string);
begin
       Self.Email :=Parametro;
end;

function TObjPEDIDO_PEDIDO.Get_Email: string;
begin
      Result:=Self.Email;
end;

procedure TObjPEDIDO_PEDIDO.Submit_Obra(parametro: string);
begin
        Self.Obra:=Parametro;
end;
function TObjPEDIDO_PEDIDO.Get_Obra: string;
begin
        Result:=Self.Obra;
end;
procedure TObjPEDIDO_PEDIDO.Submit_Data(parametro: string);
begin
        Self.Data:=Parametro;
end;
function TObjPEDIDO_PEDIDO.Get_Data: string;
begin
        Result:=Self.Data;
end;
procedure TObjPEDIDO_PEDIDO.Submit_Concluido(parametro: string);
begin
        Self.Concluido:=Parametro;
end;
function TObjPEDIDO_PEDIDO.Get_Concluido: string;
begin
        Result:=Self.Concluido;
end;
//CODIFICA GETSESUBMITS


procedure TObjPEDIDO_PEDIDO.EdtClienteExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Cliente.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Cliente.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Cliente.GET_NOME;
End;

{Rodolfo}
procedure TObjPedido_Pedido.EdtClienteDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
     key:=VK_F9;
     EdtClienteKeyDown(Sender, Key, Shift);
end;
{==Rodolfo}

procedure TObjPEDIDO_PEDIDO.EdtClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState; LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Fcliente:TFCLIENTE;
begin

     If (key <> vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fcliente:=TFCLIENTE.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cliente.Get_Pesquisa2,Self.Cliente.Get_TituloPesquisa,Fcliente)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cliente.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fcliente);
     End;
end;


procedure TObjPEDIDO_PEDIDO.EdtClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fcliente:TFCLIENTE;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            fcliente:=TFCLIENTE.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cliente.Get_Pesquisa2,Self.Cliente.Get_TituloPesquisa,Fcliente)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cliente.RETORNACAMPOCODIGO).asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fcliente);
     End;
end;


//******************

procedure TObjPEDIDO_PEDIDO.EdtarquitetoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.arquiteto.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.arquiteto.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.arquiteto.GET_NOME;
End;

procedure TObjPEDIDO_PEDIDO.EdtarquitetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Farquiteto:TFARQUITETO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Farquiteto:=TFARQUITETO.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.arquiteto.Get_Pesquisa,Self.arquiteto.Get_TituloPesquisa,Farquiteto)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.arquiteto.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.arquiteto.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.arquiteto.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Farquiteto);
     End;
end;

procedure TObjPEDIDO_PEDIDO.EdtarquitetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Farquiteto:TFARQUITETO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Farquiteto:=TFARQUITETO.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.arquiteto.Get_Pesquisa,Self.arquiteto.Get_TituloPesquisa,Farquiteto)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.arquiteto.RETORNACAMPOCODIGO).asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Farquiteto);
     End;
end;

procedure TObjPEDIDO_PEDIDO.EdtVendedorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Vendedor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Vendedor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Vendedor.GET_NOME;
End;
procedure TObjPEDIDO_PEDIDO.EdtVendedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Fvendedor:TFVENDEDOR;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fvendedor:=TFVENDEDOR.Create(nil);

           // If (FpesquisaLocal.PreparaPesquisa(Self.Vendedor.Get_Pesquisa,Self.Vendedor.Get_TituloPesquisa,Fvendedor)=True)
           If (FpesquisaLocal.PreparaPesquisa('Select * from TabVENDEDOR where ativo=''S'' ',Self.Vendedor.Get_TituloPesquisa,Fvendedor)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Vendedor.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;

                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fvendedor);
     End;
end;

procedure TObjPEDIDO_PEDIDO.EdtVendedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fvendedor:TFVENDEDOR;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fvendedor:=TFVENDEDOR.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Vendedor.Get_Pesquisa,Self.Vendedor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Vendedor.RETORNACAMPOCODIGO).asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fvendedor);
     End;
end;

//CODIFICA EXITONKEYDOWN



procedure TObjPEDIDO_PEDIDO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPEDIDO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



function TObjPEDIDO_PEDIDO.Get_ValorTotal: string;
begin
     Result:=Self.ValorTotal;
end;

procedure TObjPEDIDO_PEDIDO.Submit_ValorAcrescimo(parametro: string);
begin
     Self.ValorAcrescimo:=parametro;
end;

procedure TObjPEDIDO_PEDIDO.Submit_ValorDesconto(parametro: string);
begin
     Self.ValorDesconto:=parametro;
end;

procedure TObjPEDIDO_PEDIDO.Submit_ValorTotal(parametro: string);
begin
     Self.ValorTotal:=parametro;
end;
function TObjPEDIDO_PEDIDO.Get_ValorAcrescimo: string;
begin
     Result:=Self.ValorAcrescimo;
end;

function TObjPEDIDO_PEDIDO.Get_ValorDesconto: string;
begin
     Result:=Self.ValorDesconto;
end;

function TObjPEDIDO_PEDIDO.Get_ValorFinal: string;
begin
     result:=Self.ValorFinal;
end;



function TObjPEDIDO_PEDIDO.PrimeiroRegistro: Boolean;
Var MenorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabPedido') ;
           Open;

           if (FieldByName('MenorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MenorCodigo:=fieldbyname('MenorCodigo').AsString;
           Self.LocalizaCodigo(MenorCodigo);

           Result:=true;
     end;
end;

function TObjPEDIDO_PEDIDO.UltimoRegistro: Boolean;
Var MaiorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabPedido') ;
           Open;

           if (FieldByName('MaiorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MaiorCodigo:=fieldbyname('MaiorCodigo').AsString;
           Self.LocalizaCodigo(MaiorCodigo);

           Result:=true;
     end;
end;

function TObjPEDIDO_PEDIDO.ProximoRegisto(PCodigo:Integer): Boolean;
Var MaiorValor:Integer;
begin
     Result:=false;

     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabPedido') ;
           Open;

           MaiorValor:=fieldbyname('MaiorCodigo').AsInteger;
     end;

     if (MaiorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Inc(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MaiorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Inc(PCodigo,1);
     end;

     Result:=true;
end;

function TObjPEDIDO_PEDIDO.RegistoAnterior(PCodigo: Integer): Boolean;
Var MenorValor:Integer;
begin
     Result:=false;
     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabPedido') ;
           Open;

           MenorValor:=fieldbyname('MenorCodigo').AsInteger;
     end;

     if (MenorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Dec(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MenorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Dec(PCodigo,1);
     end;

     Result:=true;
end;

function TObjPEDIDO_PEDIDO.ExcluiRelacionamentos(PPedido: string): Boolean;
Var
    QueryExclui,QueryMedidas,QueryNota  : TIBQuery;
    Nota:string;

begin
try
      Result:=false;
      try
           QueryExclui:=TIBQuery.Create(nil);
           QueryExclui.Database:=FDataModulo.IBDatabase;

           QueryNota:=TIBQuery.Create(nil);
           QueryNota.Database:=FDataModulo.IBDatabase;

           QueryMedidas:=TIBQuery.Create(nil);
           QueryMedidas.Database:=FDataModulo.IBDatabase;
      except
           MensagemErro('Erro ao tentar criar a Query Local');
           exit;
      end;

      //*************** TABORDEMMEDICAO************************************************
      try
          QueryExclui.Close;
          QueryExclui.SQL.Add('update tabordemmedicao set pedido = null where pedido='+PPedido);
          QueryExclui.ExecSQL;
      except
          FDataModulo.IBTransaction.RollbackRetaining;
          Result:=false;
          exit;
      end;

      //******* Descubro todos os projetos para esse pedido
      With  Self.Objquery  do
      Begin
           
           Close;
           Sql.Clear;
           Sql.Add('Select TabPedido_Proj.Codigo as PedidoProjeto from TabPedido_Proj');
           Sql.Add('Where TabPedido_Proj.Pedido = '+PPedido);
           Open;   

           While not (Eof) do
           Begin
                // Para cada PedidoProjeto vou excluindo seus relacionamentos
              {  //**************** TABORDEMSERVICO ****************************
                try


                      QueryExclui.Close;
                      QueryExclui.SQL.Add('delete from tabordemservico where pedido='+PPedido);
                      QueryExclui.ExecSQL;
                except
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;        }

                //**************** TABESTOQUE *********************************
                try
                      QueryNota.Close;
                      QueryNota.Sql.Clear;
                      QueryNota.SQL.Add('select codigo from tabnotafiscal where numpedido='+QuotedStr(PPedido));
                      QueryNota.Open;

                      nota:=QueryNota.fieldbyname('codigo').AsString;
                      while not QueryNota.Eof do
                      begin
                          QueryExclui.Close;
                          QueryExclui.Sql.Clear;
                          QueryExclui.SQL.Add('update tabestoque set ferragem_pp = null');
                          QueryExclui.SQL.Add('Where notafiscal = '+nota);
                          QueryExclui.ExecSQL;

                          QueryExclui.Close;
                          QueryExclui.Sql.Clear;
                          QueryExclui.SQL.Add('update tabestoque set perfilado_pp = null');
                          QueryExclui.SQL.Add('Where notafiscal = '+nota);
                          QueryExclui.ExecSQL;

                          QueryExclui.Close;
                          QueryExclui.Sql.Clear;
                          QueryExclui.SQL.Add('update tabestoque set persiana_pp = null');
                          QueryExclui.SQL.Add('Where notafiscal = '+nota);
                          QueryExclui.ExecSQL;

                          QueryExclui.Close;
                          QueryExclui.Sql.Clear;
                          QueryExclui.SQL.Add('update tabestoque set vidro_pp = null');
                          QueryExclui.SQL.Add('Where notafiscal = '+nota); 
                          QueryExclui.ExecSQL;

                          QueryExclui.Close;
                          QueryExclui.Sql.Clear;
                          QueryExclui.SQL.Add('update tabestoque set diverso_pp = null');
                          QueryExclui.SQL.Add('Where notafiscal = '+nota);
                          QueryExclui.ExecSQL;

                          QueryExclui.Close;
                          QueryExclui.Sql.Clear;
                          QueryExclui.SQL.Add('update tabestoque set kitbox_pp = null');
                          QueryExclui.SQL.Add('Where notafiscal = '+nota);
                          QueryExclui.ExecSQL;

                          QueryExclui.Close;
                          QueryExclui.Sql.Clear;
                          QueryExclui.SQL.Add('update tabestoque set observacao = ''EXCLUS�O DE PEDIDO COM NOTAFISCAL CANCELADA ('+PPedido+')'' ');
                          QueryExclui.SQL.Add('Where notafiscal = '+nota);
                          QueryExclui.ExecSQL;

                          QueryNota.Next;
                      end;


                except
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;
                //*************** Tabmateriaistroca ***************************
                try
                      QueryExclui.Close;
                      QueryExclui.Sql.Clear;
                      QueryExclui.SQL.Add('Delete from Tabmateriaistroca');
                      QueryExclui.SQL.Add('Where PedidoProjeto = '+fieldbyname('PedidoProjeto').AsString);
                      QueryExclui.ExecSQL;
                except
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;
                //*************** Tabmateriaisdevolucao ***************************
                try
                      QueryExclui.Close;
                      QueryExclui.Sql.Clear;
                      QueryExclui.SQL.Add('Delete from Tabmateriaisdevolucao');
                      QueryExclui.SQL.Add('Where Pedido = '+PPedido);
                      QueryExclui.ExecSQL;
                except
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;

                //*************** Tablancamentocredito***************************
                try
                      QueryExclui.Close;
                      QueryExclui.Sql.Clear;
                      QueryExclui.SQL.Add('Delete from Tablancamentocredito');
                      QueryExclui.SQL.Add('Where Pedido = '+PPedido);
                      QueryExclui.ExecSQL;
                except
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;

                //*************** Ferragem_PP ***************************
                try
                      QueryExclui.Close;
                      QueryExclui.Sql.Clear;
                      QueryExclui.SQL.Add('Delete from TabFerragem_PP');
                      QueryExclui.SQL.Add('Where PedidoProjeto = '+fieldbyname('PedidoProjeto').AsString);
                      QueryExclui.ExecSQL;
                except
                      MensagemErro('Erro ao tentar excluir as Ferragens do Pedido_Projeto '+fieldbyname('PedidoProjeto').AsString);
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;

                //*************** Perfilado_PP ***************************
                try
                      QueryExclui.Close;
                      QueryExclui.Sql.Clear;
                      QueryExclui.SQL.Add('Delete from TabPerfilado_PP');
                      QueryExclui.SQL.Add('Where PedidoProjeto = '+fieldbyname('PedidoProjeto').AsString);
                      QueryExclui.ExecSQL;
                except
                      MensagemErro('Erro ao tentar excluir os Perfilados do Pedido_Projeto '+fieldbyname('PedidoProjeto').AsString);
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;

                //*************** Vidro_PP ***************************
                try
                      QueryExclui.Close;
                      QueryExclui.Sql.Clear;
                      QueryExclui.SQL.Add('Delete from TabVidro_PP');
                      QueryExclui.SQL.Add('Where PedidoProjeto = '+fieldbyname('PedidoProjeto').AsString);
                      QueryExclui.ExecSQL;
                except
                      MensagemErro('Erro ao tentar excluir os Vidros do Pedido_Projeto '+fieldbyname('PedidoProjeto').AsString);
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;

                //*************** KitBox_PP ***************************
                try
                      QueryExclui.Close;
                      QueryExclui.Sql.Clear;
                      QueryExclui.SQL.Add('Delete from TabKitBox_PP');
                      QueryExclui.SQL.Add('Where PedidoProjeto = '+fieldbyname('PedidoProjeto').AsString);
                      QueryExclui.ExecSQL;
                except
                      MensagemErro('Erro ao tentar excluir os KitBox do Pedido_Projeto '+fieldbyname('PedidoProjeto').AsString);
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;

                //*************** Servico_PP ***************************
                try
                      QueryExclui.Close;
                      QueryExclui.Sql.Clear;
                      QueryExclui.SQL.Add('Delete from TabServico_PP');
                      QueryExclui.SQL.Add('Where PedidoProjeto = '+fieldbyname('PedidoProjeto').AsString);
                      QueryExclui.ExecSQL;
                except
                      MensagemErro('Erro ao tentar excluir os Servico do Pedido_Projeto '+fieldbyname('PedidoProjeto').AsString);
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;

                //*************** Persiana_PP ***************************
                try
                      QueryExclui.Close;
                      QueryExclui.Sql.Clear;
                      QueryExclui.SQL.Add('Delete from TabPersiana_PP ');
                      QueryExclui.SQL.Add('Where PedidoProjeto = '+fieldbyname('PedidoProjeto').AsString);
                      QueryExclui.ExecSQL;
                except
                      MensagemErro('Erro ao tentar excluir as Persiana do Pedido_Projeto '+fieldbyname('PedidoProjeto').AsString);
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;

                //*************** Diverso_PP ***************************
                try
                      QueryExclui.Close;
                      QueryExclui.Sql.Clear;
                      QueryExclui.SQL.Add('Delete from TabDiverso_PP');
                      QueryExclui.SQL.Add('Where PedidoProjeto = '+fieldbyname('PedidoProjeto').AsString);
                      QueryExclui.ExecSQL;
                except
                      MensagemErro('Erro ao tentar excluir os Diverso do Pedido_Projeto '+fieldbyname('PedidoProjeto').AsString);
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;


                //**************** Medidas_Proj ***************************
                try
                      QueryExclui.Close;
                      QueryExclui.Sql.Clear;
                      QueryExclui.SQL.Add('Select TabMedidas_proj.Codigo from TabMedidas_Proj');
                      QueryExclui.SQL.Add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabMedidas_Proj.PedidoProjeto');
                      QueryExclui.SQL.Add('Join TabPedido on TabPedido.codigo = TabPedido_proj.pedido');
                      QueryExclui.SQL.Add('Where TabPedido.Codigo = '+PPedido);
                      QueryExclui.Open;
                      While not (QueryExclui.Eof)do
                      Begin
                           QueryMedidas.Close;
                           QueryMedidas.SQL.Clear;
                           QueryMedidas.SQL.Add('Delete from TabMedidas_proj Where Codigo = '+QueryExclui.fieldbyname('Codigo').AsString);
                           QueryMedidas.ExecSQL;
                           QueryExclui.Next;
                      end;
                except
                      MensagemErro('Erro ao tentar excluir os Diverso do Pedido_Projeto '+fieldbyname('PedidoProjeto').AsString);
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;
                //******************COMPONENTES*******************************
                Try
                      queryexclui.close;
                      queryexclui.sql.clear;
                      queryexclui.sql.add('Delete from tabcomponente_pp where pedidoprojeto='+fieldbyname('PedidoProjeto').AsString);
                      QueryExclui.ExecSQL;
                except
                      MensagemErro('Erro ao tentar os componentes do Pedido_Projeto '+fieldbyname('PedidoProjeto').AsString);
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Result:=false;
                      exit;
                end;
                //*****proximo pedido projeto*******
                Next;
           end;

           // Com os relacionametos _PP excluidos, agora vou excluir os Pedido_Proj
           try
                Close;
                Sql.Clear;
                Sql.Add('Delete from TabPedido_Proj');
                Sql.Add('Where TabPedido_Proj.Pedido = '+PPedido);
                Sql.Add('and pedidoprojetoprincipal is not null');//excluo primeiro os replicados
                ExecSQL;

                Close;//excluo o resto
                Sql.Clear;
                Sql.Add('Delete from TabPedido_Proj');
                Sql.Add('Where TabPedido_Proj.Pedido = '+PPedido);
                ExecSQL;
           except
                MensagemErro('Erro ao tentar excluir os projetos');
                FDataModulo.IBTransaction.RollbackRetaining;
                Result:=false;
                exit;
           end;

           Result:=true;

      end;

finally
      FreeAndNil(QueryExclui);
      FreeAndNil(QueryMedidas);
end;
end;

function TObjPEDIDO_PEDIDO.VerificaPedidoEmRomaneio(PPedido: string): Boolean;
Var Mensagem : String;
begin
      Result := False;
      With Self.Objquery do
      Begin
           // Como o Romaneio est� dentro do Pedido Projeto eu descubro
           Close;
           Sql.Clear;
           Sql.Add('Select TabRomaneio.Codigo from TABPEDIDOPROJETOROMANEIO');
           Sql.Add('Join TabRomaneio on TabRomaneio.Codigo = TABPEDIDOPROJETOROMANEIO.Romaneio');
           Sql.Add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TABPEDIDOPROJETOROMANEIO.PedidoProjeto');
           Sql.Add('Join TabPedido on TabPedido.Codigo =  TabPedido_Proj.Pedido');
           Sql.Add('Where TabPedido.Codigo = '+PPedido);
           Open;

           if  (RecordCount = 0)then
           Begin
                Result:=false;
                exit;
           end;

           Mensagem:='';
           While not (eof) do
           Begin
                Mensagem:=fieldbyname('Codigo').AsString+#13;
                Next;
           end;

           MensagemErro('Este Pedido foi utilizado no(s) Romaneio(s):'+#13+Mensagem+#13+'Retorne esses Romanieis antes de Excluir este pedido.');
           Result:=true;

      end;
end;

function TObjPEDIDO_PEDIDO.Get_Observacao: String;
begin
    Result:=Self.Observacao;
end;

procedure TObjPEDIDO_PEDIDO.Submit_Observacao(parametro: String);
begin
    Self.Observacao:=parametro;
end;



function TObjPEDIDO_PEDIDO.Get_Titulo: string;
begin
     Result:=Self.Titulo;
end;

procedure TObjPEDIDO_PEDIDO.Submit_Titulo(parametro: string);
begin
     Self.Titulo:=parametro;
end;

function TObjPEDIDO_PEDIDO.Verificavalordesconto: boolean;
var
PvalorBonificacao,Pbonificacaogerada,Pdesconto,pvalor,pacrescimo:Currency;
begin
     Result:=False;

     Try
        Pdesconto:=Strtofloat(Self.ValorDesconto);
     Except
        Pdesconto:=0;
     end;

     Try
        Pacrescimo:=Strtofloat(Self.ValorAcrescimo);
     Except
        Pacrescimo:=0;
     end;

     Try
        Pvalor:=Strtofloat(Self.ValorTotal);
     Except
        Pvalor:=0;
     end;


     Try
        PvalorBonificacao:=Strtofloat(Self.ValorBonificacaoCliente);
     Except
        PvalorBonificacao:=0;
     end;

     Try
        Pbonificacaogerada:=Strtofloat(Self.Bonificacaogerada);
     Except
        Pbonificacaogerada:=0;
     end;

     if (Pvalor>0)
     Then Begin
             if (((pvalor+pacrescimo+Pbonificacaogerada)-(Pdesconto+PValorBonificacao))<0)
             Then Begin
                     Messagedlg('O Desconto n�o pode ser superior ao Valor do Pedido',mterror,[mbok],0);
                     exit;
             End;
     End;
     Result:=True;
end;

function TObjPEDIDO_PEDIDO.get_ValorComissaoArquiteto: string;
begin
     result:=Self.ValorComissaoArquiteto;
end;

procedure TObjPEDIDO_PEDIDO.Submit_ValorComissaoArquiteto(
  parametro: string);
begin
     Self.ValorComissaoArquiteto:=parametro;
end;

function TObjPEDIDO_PEDIDO.Get_AlteradoporTroca: string;
begin
     Result:=Self.AlteradoporTroca;
end;

procedure TObjPEDIDO_PEDIDO.Submit_AlteradoporTroca(parametro: string);
begin
     Self.AlteradoporTroca:=parametro;
end;

function TObjPEDIDO_PEDIDO.Get_ValorBonificacaoCliente: string;
begin
     Result:=Self.ValorBonificacaoCliente;
end;

procedure TObjPEDIDO_PEDIDO.Submit_ValorBonificacaoCliente(
  Parametro: string);
begin
     Self.ValorBonificacaoCliente:=Parametro;
end;

function TObjPEDIDO_PEDIDO.VerificaBonificacao: boolean;
var
PvalorBonificacao:currency;
PcreditoCliente:Currency;
begin
     Result:=False;
     
     Try
        PValorBonificacao:=Strtofloat(Self.ValorBonificacaoCliente);
     Except
           PvalorBonificacao:=0;
     End;

     Try
        PcreditoCliente:=Strtofloat(Self.Cliente.Get_ValorCredito);
     Except
        PcreditoCliente:=0;
     End;

     if (PvalorBonificacao>PcreditoCliente)
     Then Begin
               MensagemErro('A bonifica��o est� superior ao cr�dito do Cliente'+#13+'Cr�dito Atual '+formata_valor(PcreditoCliente));
               exit;
     End;
     Result:=True;
end;

function TObjPEDIDO_PEDIDO.Get_PesquisaConcluido: TStringList;
begin
     Self.ParametroPesquisa.clear;
     //Self.ParametroPesquisa.add('Select tabcliente.nome,tabcliente.codigo as tabcliente.codigo,tabpedido.* from TabPEDIDO join tabcliente on tabcliente.codigo=tabpedido.cliente Where Concluido=''S'' ');
     Self.ParametroPesquisa.add('Select * from ViewPedido Where Concluido=''S'' ');
     Result:=Self.ParametroPesquisa;

end;

function TObjPEDIDO_PEDIDO.Get_BonificacaoGerada: string;
begin
     Result:=Self.BonificacaoGerada;
end;

procedure TObjPEDIDO_PEDIDO.Submit_BonificacaoGerada(parametro: string);
begin
     Self.BonificacaoGerada:=parametro;
end;

function TObjPEDIDO_PEDIDO.Get_valortitulo: string;
begin
     Result:=Self.Valortitulo;
end;

procedure TObjPEDIDO_PEDIDO.Submit_valortitulo(Parametro: string);
begin
     Self.valortitulo:=parametro;
end;

function TObjPEDIDO_PEDIDO.Get_PorcentagemArquiteto: string;
begin
    Result:=Self.PorcentagemComissaoaArquiteto;
end;

procedure TObjPEDIDO_PEDIDO.Submit_PorcentagemArquiteto(parametro:string);
begin
    Self.PorcentagemComissaoaArquiteto:=parametro;
end;

function TObjPEDIDO_PEDIDO.Get_DataOrcameto:string;
begin
    Result:=Self.DataOrcamento;
end;

procedure TObjPEDIDO_PEDIDO.Submit_DataOrcamento(parametro:string);
begin
    self.DataOrcamento:=parametro;
end;

function TObjPEDIDO_PEDIDO.Get_DataEntrega:string;
begin
    Result:=Self.DataEntrega;
end;

procedure TObjPEDIDO_PEDIDO.Submit_DataEntrega(parametro:string);
begin
    Self.DataEntrega:=parametro;
end;

function TObjPEDIDO_PEDIDO.AtualizaGeraCupom(pPedido : string): boolean;
var
  qry : TIBQuery;
begin
  Result := false;
  if(pPedido = '') then
    Exit;
  qry := TIBQuery.Create(nil);
  qry.Database := FDataModulo.IBDatabase;
  try
    qry.Close;
    qry.SQL.Clear;
    qry.SQL.Add('UPDATE TABPEDIDO SET GERACUPOM =' + QuotedStr('S'));
    qry.SQL.Add('WHERE CODIGO=' + pPedido);
    try
      qry.ExecSQL;
    except
      on e:Exception do
        MensagemErro('Erro ao atualizar pedido : ' + ppedido + 'para emiss�o de Cupom Fiscal / NFC-e');
    end;
  finally
    if(qry <> nil) then
      FreeAndNil(qry);
  end;
  FDataModulo.IBTransaction.CommitRetaining;
  Result := True;
end;

function TObjPEDIDO_PEDIDO.Get_CodigoNFCe: string;
begin
  Result := self.CodigoNFCe;
end;

function TObjPEDIDO_PEDIDO.Get_GeraCupom: string;
begin
  Result := self.GeraCupom;
end;

function TObjPEDIDO_PEDIDO.Get_NumeroNFCe: string;
begin
  Result := self.NumeroNFCe;
end;

function TObjPEDIDO_PEDIDO.Get_SerieNFCe: string;
begin
  Result := self.SerieNFCe;
end;

procedure TObjPEDIDO_PEDIDO.Submit_CodigoNFCe(parametro: String);
begin
  self.CodigoNFCe := parametro;
end;

procedure TObjPEDIDO_PEDIDO.Submit_GeraCupom(parametro: String);
begin
  self.GeraCupom := parametro;
end;

procedure TObjPEDIDO_PEDIDO.Submit_NumeroNFCe(parametro: String);
begin
  self.NumeroNFCe := parametro;
end;

procedure TObjPEDIDO_PEDIDO.Submit_SerieNFCe(parametro: String);
begin
  self.SerieNFCe := parametro;
end;
function TObjPEDIDO_PEDIDO.get_operacao: string;
begin
  result := self.operacao;
end;

procedure TObjPEDIDO_PEDIDO.submit_operacao(operacao: string);
begin
  self.operacao := operacao;
end;

end.





