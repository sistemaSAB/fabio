unit UCONTADOR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjCONTADOR,
  jpeg;

type
  TFCONTADOR = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    LbNOME: TLabel;
    EdtNOME: TEdit;
    LbCPF: TLabel;
    LbCRC: TLabel;
    EdtCRC: TEdit;
    LbCNPJ: TLabel;
    LbCEP: TLabel;
    EdtCEP: TEdit;
    LbENDERECO: TLabel;
    EdtENDERECO: TEdit;
    LbNUMERO: TLabel;
    EdtNUMERO: TEdit;
    LbCOMPLEMENTO: TLabel;
    EdtCOMPLEMENTO: TEdit;
    LbBAIRRO: TLabel;
    EdtBAIRRO: TEdit;
    LbFONE: TLabel;
    LbFAX: TLabel;
    LbEMAIL: TLabel;
    EdtEMAIL: TEdit;
    LbCIDADE: TLabel;
    EdtCIDADE: TEdit;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    LbNomeCIDADE: TLabel;
    edtCPF: TMaskEdit;
    edtCNPJ: TMaskEdit;
    edtFone: TMaskEdit;
    EdtFAX: TMaskEdit;
    procedure edtCIDADEKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtCIDADEExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjCONTADOR:TObjCONTADOR;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCONTADOR: TFCONTADOR;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCONTADOR.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjCONTADOR do
    Begin
        Submit_Codigo(edtCodigo.text);
        Submit_NOME(edtNOME.text);
        Submit_CPF(edtCPF.text);
        Submit_CRC(edtCRC.text);
        Submit_CNPJ(edtCNPJ.text);
        Submit_CEP(edtCEP.text);
        Submit_ENDERECO(edtENDERECO.text);
        Submit_NUMERO(edtNUMERO.text);
        Submit_COMPLEMENTO(edtCOMPLEMENTO.text);
        Submit_BAIRRO(edtBAIRRO.text);
        Submit_FONE(edtFONE.text);
        Submit_FAX(edtFAX.text);
        Submit_EMAIL(edtEMAIL.text);
        CIDADE.Submit_codigo(edtCIDADE.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCONTADOR.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCONTADOR do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtNOME.text:=Get_NOME;
        EdtCPF.text:=Get_CPF;
        EdtCRC.text:=Get_CRC;
        EdtCNPJ.text:=Get_CNPJ;
        EdtCEP.text:=Get_CEP;
        EdtENDERECO.text:=Get_ENDERECO;
        EdtNUMERO.text:=Get_NUMERO;
        EdtCOMPLEMENTO.text:=Get_COMPLEMENTO;
        EdtBAIRRO.text:=Get_BAIRRO;
        EdtFONE.text:=Get_FONE;
        EdtFAX.text:=Get_FAX;
        EdtEMAIL.text:=Get_EMAIL;
        EdtCIDADE.text:=CIDADE.Get_codigo;
        LbNomeCIDADE.Caption:=CIDADE.Get_NOME;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCONTADOR.TabelaParaControles: Boolean;
begin
     If (Self.ObjCONTADOR.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFCONTADOR.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCONTADOR=Nil)
     Then exit;

     If (Self.ObjCONTADOR.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjCONTADOR.free;
end;

procedure TFCONTADOR.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFCONTADOR.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjCONTADOR.status:=dsInsert;
     edtNOME.setfocus;

end;


procedure TFCONTADOR.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCONTADOR.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjCONTADOR.Status:=dsEdit;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtNOME.setfocus;
                
    End;


end;

procedure TFCONTADOR.btgravarClick(Sender: TObject);
begin

     if (Self.ObjCONTADOR.Status=dsInactive) then exit;

     if not (ValidaCPF(self.edtCPF.Text)) then
     begin
      MensagemAviso('Informe um CPF v�lido');
      Exit;
     end;

     if not (ValidaCNPJ(self.edtCNPJ.Text)) then
     begin
      MensagemAviso('Informe um CNPJ v�lido');
      Exit;
     end;

     If not (ControlesParaObjeto) then
     begin

      Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
      exit;
      
     end;

     if not (Self.ObjCONTADOR.salvar(true)) then exit;

     edtCodigo.text := Self.ObjCONTADOR.Get_codigo();
     Self.ObjCONTADOR.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;

     mostra_botoes(Self);
     desabilita_campos(Self);
    
end;

procedure TFCONTADOR.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     If (Self.ObjCONTADOR.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCONTADOR.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCONTADOR.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;

end;

procedure TFCONTADOR.btcancelarClick(Sender: TObject);
begin
     Self.ObjCONTADOR.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFCONTADOR.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFCONTADOR.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCONTADOR.Get_pesquisa,Self.ObjCONTADOR.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCONTADOR.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCONTADOR.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjCONTADOR.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFCONTADOR.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFCONTADOR.FormShow(Sender: TObject);
begin

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjCONTADOR:=TObjCONTADOR.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
    
end;
procedure TFCONTADOR.edtCIDADEKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjCONTADOR.edtCIDADEkeydown(sender,key,shift,lbnomeCIDADE);
end;
 
procedure TFCONTADOR.edtCIDADEExit(Sender: TObject);
begin
    ObjCONTADOR.edtCIDADEExit(sender,lbnomeCIDADE);
end;



end.

