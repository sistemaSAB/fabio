unit UObjCFOP;
Interface
Uses Classes,Db,UessencialGlobal,Ibcustomdataset,stdctrls,Upesquisa,windows;

Type
   TObjCFOP=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO:string;
                Procedure Submit_CODIGO(parametro:string);
                //Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                function Get_NOME: string;
                procedure Submit_NOME(parametro: string);
                procedure EdtCFOPKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                 procedure EdtCFOPKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;


         Private
               ObjDataset:Tibdataset;

               CODIGO                  :string;
               NOME                    :string;
               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls;


{ TTabTitulo }


Function  TObjCFOP.TabelaparaObjeto:Boolean;//ok
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.NOME:=fieldbyname('NOME').asstring;
        result:=True;
     End;
end;

procedure TObjCFOP.EdtCFOPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Get_Pesquisa,Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjCFOP.EdtCFOPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Get_Pesquisa,Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.RETORNACAMPOCODIGO).asstring;

                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;



Procedure TObjCFOP.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
        fieldbyname('CODIGO').asstring:=Self.CODIGO;
        fieldbyname('NOME').asstring:=Self.NOME;
  End;
End;

//***********************************************************************

function TObjCFOP.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjCFOP.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO:='';
        Self.NOME:='';
     End;
end;

Function TObjCFOP.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjCFOP.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjCFOP.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjCFOP.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjCFOP.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';


     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjCFOP.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;

                SelectSql.clear;
                SelectSQL.ADD('Select CODIGO,NOME  from  TabCFOP');
                SelectSQL.ADD(' WHERE Codigo='+parametro);


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjCFOP.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCFOP.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCFOP.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        ZerarTabela;

        With Self.ObjDataset do
        Begin
              SelectSql.clear;
                SelectSQL.ADD('Select CODIGO,NOME');
                SelectSQL.ADD(' from  TabCFOP');
                SelectSQL.ADD(' WHERE Codigo=0');
                InsertSql.clear;
                InsertSQL.add('Insert Into TabCFOP(CODIGO,NOME)');
                InsertSQL.add('values (:CODIGO,:NOME)');
                ModifySQL.clear;
                ModifySQL.add('Update TabCFOP set CODIGO=:CODIGO,NOME=:NOME where Codigo=:Codigo');
                DeleteSql.clear;
                DeleteSql.add('Delete from TabCFOP where Codigo=:Codigo ');
                RefreshSQL.clear;
                RefreshSQL.ADD('Select CODIGO,NOME from  TabCFOP');
                RefreshSQL.ADD(' WHERE Codigo=0');
                open;
                Self.ObjDataset.First ;
                Self.status:=dsInactive;
        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjCFOP.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjCFOP.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjCFOP.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCFOP.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCFOP');
     Result:=Self.ParametroPesquisa;
end;

function TObjCFOP.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de CFOP ';
end;



destructor TObjCFOP.Free;
begin
   Freeandnil(Self.ObjDataset);
   Freeandnil(Self.ParametroPesquisa);

end;

function TObjCFOP.RetornaCampoCodigo: string;
begin
     result:='codigo';
end;

function TObjCFOP.RetornaCampoNome: string;
begin
     result:='nome';
end;



procedure TObjCFOP.Submit_NOME(parametro: string);
begin
        Self.NOME:=Parametro;
end;
function TObjCFOP.Get_NOME: string;
begin
        Result:=Self.NOME;
end;


end.
