unit Uobjferragem_ICMS;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UOBJTIPOCLIENTE
,UOBJferragem
,UOBJIMPOSTO_ICMS,
GRIDS,uobjmaterial_ICMS;
//USES_INTERFACE




Type
   TObjferragem_ICMS=class(TObjMaterial_ICMS)

          Public
                Constructor Create(Owner:TComponent);override;
                Destructor  Free;override;
                Function    Salvar(ComCommit:Boolean)       :Boolean;override;
                Function    LocalizaCodigo(Parametro:String) :boolean;override;
                Function    Exclui(Pcodigo:String;ComCommit:boolean)            :Boolean;override;
                Function    Get_Pesquisa                    :TStringList;override;
                Function    Get_TituloPesquisa              :String;override;

                Function   TabelaparaObjeto:Boolean;override;
                Procedure   ZerarTabela;override;
                Procedure   Cancelar;override;
                Procedure   Commit;override;

                Function  Get_NovoCodigo:String;override;
                Function  RetornaCampoCodigo:String;override;
                Function  RetornaCampoNome:String;override;

                Procedure Submit_CodigoMaterial(parametro:String);override;
                Function  Get_CodigoMaterial:String;override;

                Function  Get_NomeMaterial:String;override;

                procedure EdtMaterialExit(Sender: TObject;LABELNOME:TLABEL);override;
                procedure EdtMaterialKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);override;



                Procedure RetornaImposto(PCodigo:String;STRGRID:TStringGrid);override;
                function Localiza(Pestado,PtipoCliente,PcodigoMaterial,POperacao:String): boolean;override;
                Function  ValidaPermissaoImpostos:boolean;override;
                function RetornaMateriais(PMaterialAtual: String): boolean;override;

         Private

               InsertSql,DeleteSql,ModifySQl:TStringList;
               ferragem:TOBJferragem;


               ParametroPesquisa:TStringList;

                Function  verificaduplicidade:boolean;
                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;


   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UTIPOCLIENTE, UIMPOSTO_ICMS;





Function  TObjferragem_ICMS.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        result:=False;
        
        Self.ZerarTabela;

        if (inherited TabelaparaObjeto=false)
        Then exit;

        If(FieldByName('ferragem').asstring<>'')
        Then Begin
                 If (Self.ferragem.LocalizaCodigo(FieldByName('ferragem').asstring)=False)
                 Then Begin
                          Messagedlg('ferragem N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.ferragem.TabelaparaObjeto;
        End;

        result:=True;
     End;
end;


Procedure TObjferragem_ICMS.ObjetoparaTabela;//ok
begin
  With Objquery do
  Begin
        inherited ObjetoparaTabela;

        ParamByName('ferragem').asstring:=Self.ferragem.GET_CODIGO;
  End;
End;

//***********************************************************************

function TObjferragem_ICMS.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;

  if (Self.verificaduplicidade=False)
  Then Exit;

  If Self.LocalizaCodigo(Self.CODIGO)=False
  Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
  End
  Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
  End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       on e:exception do
       Begin
           if (Self.Status=dsInsert)
           Then Messagedlg('Erro na  tentativa de Inserir '+#13+e.message,mterror,[mbok],0)
           Else Messagedlg('Erro na  tentativa de Editar '+#13+E.message,mterror,[mbok],0);
           exit;
       End;
 End;

 atualizaVersao(Get_CodigoMaterial);

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjferragem_ICMS.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        inherited ZerarTabela;
        ferragem.ZerarTabela;
     End;
end;

Function TObjferragem_ICMS.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
      mensagem:=inherited VerificaBrancos;

      if (ferragem.Get_codigo='')
      Then mensagem:=mensagem+'/ferragem';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
  result:=true;
end;


function TObjferragem_ICMS.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     Result:=False;
     mensagem:=inherited verificarelacionamentos;


     If (Self.ferragem.LocalizaCodigo(Self.ferragem.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ ferragem n�o Encontrado!';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
     End;
     
     result:=true;
End;

function TObjferragem_ICMS.VerificaNumericos: Boolean;
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:=inherited VerificaNUmericos;

     try
        If (Self.ferragem.Get_Codigo<>'')
        Then Strtoint(Self.ferragem.Get_Codigo);
     Except
           Mensagem:=mensagem+'/ferragem';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;
end;

function TObjferragem_ICMS.VerificaData: Boolean;
var
Mensagem:string;
begin
     Result:=False;
     mensagem:=inherited VerificaData;

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjferragem_ICMS.VerificaFaixa: boolean;
var
   Mensagem:string;
begin
     Result:=False;
     With Self do
     Begin
          Mensagem:=Inherited VerificaFaixa;

          If mensagem<>''
          Then Begin
                   Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                   exit;
          End;
        result:=true;
     End;
end;

function TObjferragem_ICMS.LocalizaCodigo(parametro: String): boolean;//ok
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro ferragem ICMS vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select '+Self.MaterialSelectSql.text+',ferragem');
           SQL.ADD(' from  TABferragem_ICMS');
           SQL.ADD(' WHERE codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


function TObjferragem_ICMS.Localiza(Pestado,PtipoCliente,PcodigoMaterial,POperacao:String): boolean;//ok
begin

      //Um Caso a Parte na OO, pois nao compensava fazer uma funcao soh pra preencher 2 parametros
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select * from  TABferragem_ICMS');
           SQL.ADD(' WHERE ferragem=:ferragem and Estado=:Estado and TipoCliente=:TipoCliente and operacao=:operacao');

           parambyname('ferragem').asstring:=pcodigomaterial;
           parambyname('estado').asstring:=pestado;
           parambyname('tipocliente').asstring:=ptipocliente;
           parambyname('operacao').asstring:=poperacao;

           
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjferragem_ICMS.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjferragem_ICMS.Exclui(Pcodigo: String;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjferragem_ICMS.create(Owner:TComponent);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin

        Self.Owner := Owner;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;

        Self.ferragem:=TOBJferragem.create;

        inherited;

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABferragem_ICMS('+MATERIALInsertSql.text+',ferragem)');
                InsertSQL.add('values ('+MATERIALInsertSqlValues.Text+',:ferragem)');

                ModifySQL.clear;
                ModifySQL.add('Update TABferragem_ICMS set ferragem=:ferragem,'+MATERIALModifySQl.Text);
                ModifySQL.add('where codigo=:codigo');

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABferragem_ICMS where codigo=:codigo ');

                Self.status          :=dsInactive;
        End;

end;
procedure TObjferragem_ICMS.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjferragem_ICMS.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from Tabferragem_ICMS');
     Result:=Self.ParametroPesquisa;
end;

function TObjferragem_ICMS.Get_TituloPesquisa: String;
begin
     Result:=' Pesquisa de ferragem_ICMS ';
end;


function TObjferragem_ICMS.Get_NovoCodigo: String;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENferragem_ICMS,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjferragem_ICMS.Free;
begin
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.ferragem.FREE;

    inherited;
end;

function TObjferragem_ICMS.RetornaCampoCodigo: String;
begin
      result:='codigo';
end;

function TObjferragem_ICMS.RetornaCampoNome: String;
begin
      result:='';
end;

procedure TObjferragem_ICMS.EdtMaterialExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.ferragem.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.ferragem.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.ferragem.Get_Descricao;
End;
procedure TObjferragem_ICMS.EdtMaterialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.ferragem.Get_Pesquisa,Self.ferragem.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ferragem.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.ferragem.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ferragem.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjferragem_ICMS.RetornaImposto(PCodigo: String;
  STRGRID: TStringGrid);
var
cont,cont2:integer;
begin
     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select tabferragem_icms.Estado,tabtipocliente.nome as TipoCliente,');
          sql.add('tabferragem_icms.codigo,tabferragem_icms.imposto,tabferragem_icms.imposto_ipi,tabferragem_icms.imposto_pis,tabferragem_icms.imposto_cofins,taboperacaonf.nome as operacao');
          sql.add('from Tabferragem_ICMS join tabtipocliente on tabtipocliente.codigo=tabferragem_icms.tipocliente');
          sql.add('join tabimposto_icms on tabferragem_icms.imposto=tabimposto_icms.codigo');
          sql.add('join taboperacaonf on tabferragem_icms.operacao=taboperacaonf.codigo');
          sql.add('Where Tabferragem_ICMS.ferragem='+Pcodigo);
          sql.add('order by Tabferragem_ICMS.Estado');
          open;
          last;
          STRGRID.RowCount:=1;
          STRGRID.ColCount:=1;
          STRGRID.Cols[0].clear;

          if (recordcount=0)
          Then exit;
          first;

          STRGRID.ColCount:=fields.Count;
          STRGRID.RowCount:=RecordCount+1;
          STRGRID.FixedRows:=1;


          for cont:=0 to Fields.count -1 do
          Begin
               STRGRID.Cells[cont,0]:=Fields[cont].DisplayName;
          End;

          cont2:=1;
          While not(eof) do
          Begin
               for cont:=0 to Fields.count -1 do
               Begin
                   STRGRID.Cells[cont,cont2]:=Fields[cont].AsString;
               End;
               cont2:=cont2+1;
               next;
          End;
          AjustaLArguraColunaGrid(STRGRID);
     End;
end;

function TObjferragem_ICMS.Get_CodigoMaterial: String;
begin
     Result:=Self.ferragem.Get_Codigo;
end;

procedure TObjferragem_ICMS.Submit_CodigoMaterial(parametro: String);
begin
     Self.ferragem.Submit_Codigo(parametro);
end;

function TObjferragem_ICMS.Get_NomeMaterial: String;
begin
     Result:=Self.ferragem.Get_Descricao;
end;


function TObjferragem_ICMS.verificaduplicidade: boolean;
begin
     result:=False;
     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select * from tabferragem_icms where tipocliente=:tipocliente and ferragem=:ferragem and estado=:estado and operacao=:operacao');
          ParamByName('tipocliente').asstring:=Self.tipocliente.Get_CODIGO;
          ParamByName('ferragem').asstring:=Self.ferragem.Get_Codigo;
          ParamByName('estado').asstring:=Self.ESTADO;
          ParamByName('operacao').asstring:=Self.operacao.Get_CODIGO;

          open;

          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;

          if (fieldbyname('codigo').asstring<>Self.CODIGO)//mesmo que seja insercao ou edicao tem q verificar
          Then Begin
                    MensagemErro('J� existe um registro para com estas informa��es (Estado, Tipo de Cliente, Opera��o e Produto). C�digo '+fieldbyname('codigo').asstring);
                    exit;
          End
          Else result:=True;
     End;
end;

function TObjferragem_ICMS.ValidaPermissaoImpostos: boolean;
begin
   result:=ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('ALTERAR IMPOSTOS DA FERRAGEM');
end;

function TObjferragem_ICMS.RetornaMateriais(PMaterialAtual:String): boolean;//ok
begin

      //Um Caso a Parte na Or.Obj. pois nao compensava fazer uma funcao soh pra preencher 2 parametros
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select');
           SQL.ADD('tabferragem.codigo,');
           SQL.ADD('tabferragem.descricao,');
           SQL.ADD('tabferragem.referencia,');
           SQL.ADD('tabferragem.grupoferragem as GRUPO,');
           SQL.ADD('tabgrupoferragem.nome as NOMEGRUPO');
           SQL.ADD('from tabferragem');
           SQL.ADD('join tabgrupoferragem on tabferragem.grupoferragem=tabgrupoferragem.codigo');
           SQL.ADD('where tabferragem.codigo<>:pcodigo');
           SQL.ADD('order by tabferragem.grupoferragem,tabferragem.descricao');
           ParamByName('PCODIGO').AsString:=PMaterialAtual;
           Open;
       End;
end;


end.



