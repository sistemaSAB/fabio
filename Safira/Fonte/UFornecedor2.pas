unit UFornecedor2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjFornecedor;

type
  TFfornecedor2 = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    EdtFantasia: TEdit;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    LbRazaoSocial: TLabel;
    LbFantasia: TLabel;
    LbCGC: TLabel;
    EdtCGC: TEdit;
    LbIE: TLabel;
    EdtRazaoSocial: TEdit;
    EdtIE: TEdit;
    ComboEstado: TComboBox;
    EdtEndereco: TEdit;
    LbEstado: TLabel;
    EdtCidade: TEdit;
    LbEndereco: TLabel;
    LbCidade: TLabel;
    Label2: TLabel;
    LbFone: TLabel;
    LbContato: TLabel;
    LbEmail: TLabel;
    EdtDataCadastro: TMaskEdit;
    LbDataCadastro: TLabel;
    EdtEmail: TEdit;
    EdtContato: TEdit;
    EdtFone: TEdit;
    LbFax: TLabel;
    EdtFax: TEdit;
    edtBairro: TEdit;
    MemoObservacoes: TMemo;
    LbNomePrazoPagamento: TLabel;
    Label3: TLabel;
    LbPrazoPagamento: TLabel;
    EdtPrazoPagamento: TEdit;
    EdtCEP: TEdit;
    LbCEP: TLabel;
    LbContaCorrente: TLabel;
    EdtContaCorrente: TEdit;
    EdtAgencia: TEdit;
    EdtBanco: TEdit;
    EdtCodigoPlanoContas: TEdit;
    edtRamoAtividade: TEdit;
    LbNomeRamoAtividade: TLabel;
    LbRamoAtividade: TLabel;
    LbCodigoPlanoContas: TLabel;
    LbBanco: TLabel;
    LbAgencia: TLabel;
//DECLARA COMPONENTES
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtRamoAtividadeExit(Sender: TObject);
    procedure edtRamoAtividadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtCodigoPlanoContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtPrazoPagamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
         ObjFornecedor:TObjFornecedor;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Ffornecedor2: TFfornecedor2;


implementation

uses UessencialGlobal, Upesquisa, UPLanodeContas, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFfornecedor2.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjFornecedor do
    Begin
        Submit_Codigo(edtCodigo.text);
        Submit_RazaoSocial(edtRazaoSocial.text);
        Submit_Fantasia(edtFantasia.text);
        Submit_CGC(edtCGC.text);
        Submit_IE(edtIE.text);
        Submit_Endereco(edtEndereco.text);
        Submit_Fone(edtFone.text);
        Submit_Fax(edtFax.text);
        Submit_Cidade(edtCidade.text);
        Submit_Estado(ComboEstado.text);
        Submit_Contato(edtContato.text);
        Submit_Email(edtEmail.text);
        Submit_DataCadastro(edtDataCadastro.text);
        RamoAtividade.Submit_codigo(edtRamoAtividade.text);
        Submit_CodigoPlanoContas(edtCodigoPlanoContas.text);
        Submit_Banco(edtBanco.text);
        Submit_Agencia(edtAgencia.text);
        Submit_ContaCorrente(edtContaCorrente.text);
        Submit_Bairro(edtBairro.text);
        Submit_CEP(edtCEP.text);
        Submit_Observacoes(MemoObservacoes.text);
        PrazoPagamento.Submit_codigo(edtPrazoPagamento.text);
//CODIFICA SUBMITS
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFfornecedor2.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjFornecedor do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtRazaoSocial.text:=Get_RazaoSocial;
        EdtFantasia.text:=Get_Fantasia;
        EdtCGC.text:=Get_CGC;
        EdtIE.text:=Get_IE;
        EdtEndereco.text:=Get_Endereco;
        EdtFone.text:=Get_Fone;
        EdtFax.text:=Get_Fax;
        EdtCidade.text:=Get_Cidade;
        ComboEstado.text:=Get_Estado;
        EdtContato.text:=Get_Contato;
        EdtEmail.text:=Get_Email;
        EdtDataCadastro.text:=Get_DataCadastro;
        EdtRamoAtividade.text:=RamoAtividade.Get_codigo;
        EdtCodigoPlanoContas.text:=Get_CodigoPlanoContas;
        EdtBanco.text:=Get_Banco;
        EdtAgencia.text:=Get_Agencia;
        EdtContaCorrente.text:=Get_ContaCorrente;
        EdtBairro.text:=Get_Bairro;
        EdtCEP.text:=Get_CEP;
        MemoObservacoes.text:=Get_Observacoes;
        EdtPrazoPagamento.text:=PrazoPagamento.Get_codigo;
//CODIFICA GETS
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFfornecedor2.TabelaParaControles: Boolean;
begin
     If (Self.ObjFornecedor.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFfornecedor2.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjFornecedor:=TObjFornecedor.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

procedure TFfornecedor2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjFornecedor=Nil)
     Then exit;

If (Self.ObjFornecedor.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjFornecedor.free;
end;

procedure TFfornecedor2.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFfornecedor2.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjFornecedor.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjFornecedor.status:=dsInsert;
     Guia.pageindex:=0;
     EdtRazaoSocial.setfocus;

end;


procedure TFfornecedor2.btalterarClick(Sender: TObject);
begin
    If (Self.ObjFornecedor.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjFornecedor.Status:=dsEdit;
                guia.pageindex:=0;
                EdtRazaoSocial.setfocus;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;


end;

procedure TFfornecedor2.btgravarClick(Sender: TObject);
begin

     If Self.ObjFornecedor.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjFornecedor.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjFornecedor.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFfornecedor2.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjFornecedor.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjFornecedor.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjFornecedor.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFfornecedor2.btcancelarClick(Sender: TObject);
begin
     Self.ObjFornecedor.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFfornecedor2.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFfornecedor2.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjFornecedor.Get_pesquisa,Self.ObjFornecedor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjFornecedor.status<>dsinactive
                                  then exit;

                                  If (Self.ObjFornecedor.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjFornecedor.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFfornecedor2.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFfornecedor2.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT

procedure TFfornecedor2.edtRamoAtividadeExit(Sender: TObject);
begin
Self.ObjFornecedor.EdtRamoAtividadeExit(Sender, LbNomeRamoAtividade);
end;

procedure TFfornecedor2.edtRamoAtividadeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   Self.ObjFornecedor.EdtRamoAtividadeKeyDown(Sender,Key, Shift,LbNomeRamoAtividade);
end;

procedure TFfornecedor2.EdtCodigoPlanoContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FplanodeCOntas:TFplanodeCOntas;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FplanodeCOntas:=TFplanodeCOntas.create(NIl);
            If (FpesquisaLocal.PreparaPesquisa(ObjLanctoPortadorGlobal.Portador.PlanodeContas.Get_Pesquisa,ObjLanctoPortadorGlobal.Portador.PlanodeContas.Get_TituloPesquisa,FplanodeCOntas)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then EdtCodigoPlanoContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FplanodeCOntas);
     End;


end;

procedure TFfornecedor2.EdtPrazoPagamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   Self.ObjFornecedor.EdtPrazoPagamentoKeyDown(Sender, key, Shift, LbNomePrazoPagamento);
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(Self.ObjFornecedor.OBJETO.Get_Pesquisa,Self.ObjFornecedor.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjFornecedor.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjFornecedor.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjFornecedor.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
