object FErrosDesativarMaterial: TFErrosDesativarMaterial
  Left = 528
  Top = 197
  Width = 668
  Height = 500
  Caption = 'Projetos Usando o Material '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 660
    Height = 61
    Align = alTop
    Color = 10643006
    TabOrder = 0
    object lbErro: TLabel
      Left = 104
      Top = 8
      Width = 425
      Height = 48
      Alignment = taCenter
      AutoSize = False
      Caption = 'Lista de Projetos usando este Material'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 391
    Width = 660
    Height = 78
    Align = alBottom
    TabOrder = 1
    object ImgRodape: TImage
      Left = 1
      Top = 1
      Width = 658
      Height = 76
      Align = alClient
      Stretch = True
    end
    object lbCancelar: TLabel
      Left = 12
      Top = 42
      Width = 100
      Height = 29
      Cursor = crHandPoint
      Caption = 'Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnClick = lbCancelarClick
      OnMouseMove = lbExcluirMouseMove
      OnMouseLeave = lbExcluirMouseLeave
    end
    object lbExcluir: TLabel
      Left = 12
      Top = 10
      Width = 233
      Height = 29
      Cursor = crHandPoint
      Caption = 'Excluir dos Projetos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnClick = lbExcluirClick
      OnMouseMove = lbExcluirMouseMove
      OnMouseLeave = lbExcluirMouseLeave
    end
  end
  object pnl3: TPanel
    Left = 0
    Top = 61
    Width = 660
    Height = 330
    Align = alClient
    Color = clWhite
    TabOrder = 2
    object mmoErro: TMemo
      Left = 1
      Top = 1
      Width = 658
      Height = 328
      Cursor = crIBeam
      Align = alClient
      BevelEdges = []
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Courier New'
      Font.Style = [fsBold]
      Lines.Strings = (
        'Cliente:123456789x123456789x123456789x123456789'
        'G')
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
      OnKeyDown = mmoErroKeyDown
    end
  end
end
