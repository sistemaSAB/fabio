object Form1: TForm1
  Left = 647
  Top = 252
  Width = 814
  Height = 438
  Caption = 'Form1'
  Color = 10643006
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbLbObservacao: TLabel
    Left = 13
    Top = 154
    Width = 77
    Height = 13
    Caption = 'Observa'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbLbValor: TLabel
    Left = 13
    Top = 133
    Width = 47
    Height = 13
    Caption = 'Cr'#233'dito'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbLbHistorico: TLabel
    Left = 13
    Top = 110
    Width = 58
    Height = 13
    Caption = 'Hist'#243'rico'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbLbCLIENTE: TLabel
    Left = 13
    Top = 87
    Width = 45
    Height = 13
    Caption = 'Cliente'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbLbCODIGO: TLabel
    Left = 13
    Top = 64
    Width = 44
    Height = 13
    Caption = 'C'#243'digo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbLbNomeCLIENTE: TLabel
    Left = 164
    Top = 87
    Width = 45
    Height = 13
    Caption = 'Cliente'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb1: TLabel
    Left = 587
    Top = 63
    Width = 37
    Height = 13
    Caption = 'T'#237'tulo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object shp1: TShape
    Left = 0
    Top = 153
    Width = 765
    Height = 1
    Pen.Color = clWhite
  end
  object lbTitulo: TLabel
    Left = 667
    Top = 58
    Width = 59
    Height = 18
    Cursor = crHandPoint
    Caption = 'lbTitulo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object pnlrodape: TPanel
    Left = 0
    Top = 350
    Width = 798
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    DesignSize = (
      798
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 798
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 499
      Top = 16
      Width = 287
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X CREDITO cadastrados'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object mmoObservacao: TMemo
    Left = 13
    Top = 171
    Width = 553
    Height = 148
    Ctl3D = False
    Lines.Strings = (
      'MemoObservacao')
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 1
  end
  object edtEdtValor: TEdit
    Left = 87
    Top = 130
    Width = 132
    Height = 19
    Ctl3D = False
    MaxLength = 15
    ParentCtl3D = False
    TabOrder = 2
  end
  object edtEdtHistorico: TEdit
    Left = 87
    Top = 107
    Width = 691
    Height = 19
    Ctl3D = False
    MaxLength = 2000
    ParentCtl3D = False
    TabOrder = 3
  end
  object edtEdtCLIENTE: TEdit
    Left = 87
    Top = 84
    Width = 70
    Height = 19
    Color = 6073854
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    MaxLength = 9
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 4
  end
  object edtEdtCODIGO: TEdit
    Left = 87
    Top = 61
    Width = 70
    Height = 19
    Ctl3D = False
    MaxLength = 9
    ParentCtl3D = False
    TabOrder = 5
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 798
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 6
    object lbnomeformulario: TLabel
      Left = 668
      Top = 0
      Width = 130
      Height = 50
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Cr'#233'dito'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -32
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btsair: TBitBtn
      Left = 393
      Top = -3
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
    end
    object btopcoes: TBitBtn
      Left = 344
      Top = -3
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
    object btrelatorios: TBitBtn
      Left = 295
      Top = -3
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
    end
    object btpesquisar: TBitBtn
      Left = 246
      Top = -3
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object btexcluir: TBitBtn
      Left = 197
      Top = -3
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
    end
    object btcancelar: TBitBtn
      Left = 148
      Top = -3
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
    object btgravar: TBitBtn
      Left = 99
      Top = -3
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object btalterar: TBitBtn
      Left = 50
      Top = -3
      Width = 50
      Height = 52
      Caption = 's'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object btBtnovo: TBitBtn
      Left = 1
      Top = -3
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object pnl1: TPanel
    Left = 572
    Top = 172
    Width = 205
    Height = 149
    BevelOuter = bvNone
    Color = 10643006
    TabOrder = 7
    object lb2: TLabel
      Left = 27
      Top = 11
      Width = 119
      Height = 13
      Caption = 'Cr'#233'ditos Lan'#231'ados'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb3: TLabel
      Left = 28
      Top = 29
      Width = 16
      Height = 13
      Caption = 'R$'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb4: TLabel
      Left = 27
      Top = 59
      Width = 122
      Height = 13
      Caption = 'Cr'#233'ditos Utilizados'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbCreditoUtilizados: TLabel
      Left = 51
      Top = 77
      Width = 84
      Height = 13
      Caption = '1.255.236,53'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb5: TLabel
      Left = 27
      Top = 107
      Width = 115
      Height = 13
      Caption = 'Cr'#233'ditos Restante'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbCreditoRestantes: TLabel
      Left = 51
      Top = 125
      Width = 84
      Height = 13
      Caption = '1.255.236,53'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbCreditoLancados: TLabel
      Left = 51
      Top = 29
      Width = 84
      Height = 13
      Caption = '1.255.236,53'
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lb6: TLabel
      Left = 28
      Top = 125
      Width = 16
      Height = 13
      Caption = 'R$'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb7: TLabel
      Left = 28
      Top = 77
      Width = 16
      Height = 13
      Caption = 'R$'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
end
