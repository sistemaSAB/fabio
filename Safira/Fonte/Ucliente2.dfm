object Fcliente2: TFcliente2
  Left = 706
  Top = 309
  Width = 792
  Height = 509
  Caption = 'Cadastro de Clientes - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 381
    Width = 776
    Height = 90
    Align = alBottom
    Color = 3355443
    TabOrder = 1
    object Btnovo: TBitBtn
      Left = 3
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Novo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 115
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Alterar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 227
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Gravar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 339
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 3
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Pesquisar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 115
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Relat'#243'rios'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object btexcluir: TBitBtn
      Left = 227
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Excluir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 339
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Sair'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btsairClick
    end
  end
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 776
    Height = 381
    Align = alClient
    PageIndex = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Principal'
      object LbCodigo: TLabel
        Left = 3
        Top = 25
        Width = 40
        Height = 13
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbSexo: TLabel
        Left = 508
        Top = 101
        Width = 29
        Height = 13
        Caption = 'Sexo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNome: TLabel
        Left = 5
        Top = 99
        Width = 33
        Height = 13
        Caption = 'Nome'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbFantasia: TLabel
        Left = 5
        Top = 123
        Width = 47
        Height = 13
        Caption = 'Fantasia'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbRG_IE: TLabel
        Left = 304
        Top = 149
        Width = 42
        Height = 13
        Caption = 'RG / IE'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbCPF_CGC: TLabel
        Left = 5
        Top = 147
        Width = 62
        Height = 13
        Caption = 'CPF / CGC'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbRamoAtividade: TLabel
        Left = 5
        Top = 172
        Width = 90
        Height = 13
        Caption = 'Ramo Atividade'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeRamoAtividade: TLabel
        Left = 176
        Top = 173
        Width = 90
        Height = 13
        Caption = 'Ramo Atividade'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Lbemail: TLabel
        Left = 5
        Top = 196
        Width = 36
        Height = 13
        Caption = 'e-mail'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbFone: TLabel
        Left = 5
        Top = 220
        Width = 27
        Height = 13
        Caption = 'Fone'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbCelular: TLabel
        Left = 251
        Top = 217
        Width = 41
        Height = 13
        Caption = 'Celular'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbFax: TLabel
        Left = 435
        Top = 219
        Width = 20
        Height = 13
        Caption = 'Fax'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 5
        Top = 243
        Width = 45
        Height = 13
        Caption = 'Contato'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 5
        Top = 261
        Width = 68
        Height = 13
        Caption = 'Observa'#231#227'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object EdtCodigo: TEdit
        Left = 99
        Top = 25
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object GroupBox3: TGroupBox
        Left = 100
        Top = 46
        Width = 93
        Height = 50
        Caption = 'Pessoa'
        TabOrder = 1
        object RadioPessoaFisica: TRadioButton
          Left = 8
          Top = 13
          Width = 77
          Height = 17
          Caption = 'F'#237'sica'
          TabOrder = 0
        end
        object RadioPessoaJuridica: TRadioButton
          Left = 8
          Top = 32
          Width = 74
          Height = 17
          Caption = 'Jur'#237'dica'
          TabOrder = 1
        end
      end
      object EdtNome: TEdit
        Left = 99
        Top = 99
        Width = 406
        Height = 19
        MaxLength = 100
        TabOrder = 2
      end
      object ComboSexo: TComboBox
        Left = 540
        Top = 97
        Width = 41
        Height = 21
        ItemHeight = 13
        TabOrder = 3
        Items.Strings = (
          'M'
          'F')
      end
      object EdtFantasia: TEdit
        Left = 99
        Top = 123
        Width = 485
        Height = 19
        MaxLength = 100
        TabOrder = 4
      end
      object EdtRG_IE: TEdit
        Left = 350
        Top = 147
        Width = 235
        Height = 19
        MaxLength = 25
        TabOrder = 6
        Text = 'x'
      end
      object EdtCPF_CGC: TMaskEdit
        Left = 99
        Top = 147
        Width = 141
        Height = 19
        MaxLength = 25
        TabOrder = 5
      end
      object EdtRamoAtividade: TEdit
        Left = 99
        Top = 171
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 7
        OnExit = EdtRamoAtividadeExit
        OnKeyDown = EdtRamoAtividadeKeyDown
      end
      object Edtemail: TEdit
        Left = 99
        Top = 195
        Width = 486
        Height = 19
        MaxLength = 50
        TabOrder = 8
      end
      object EdtFone: TEdit
        Left = 99
        Top = 219
        Width = 142
        Height = 19
        MaxLength = 25
        TabOrder = 9
      end
      object EdtCelular: TEdit
        Left = 299
        Top = 219
        Width = 126
        Height = 19
        MaxLength = 25
        TabOrder = 10
      end
      object EdtFax: TEdit
        Left = 459
        Top = 219
        Width = 126
        Height = 19
        MaxLength = 25
        TabOrder = 11
      end
      object edtContato: TEdit
        Left = 99
        Top = 242
        Width = 486
        Height = 19
        MaxLength = 50
        TabOrder = 12
      end
      object MemoObservacao: TMemo
        Left = 99
        Top = 264
        Width = 486
        Height = 60
        TabOrder = 13
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Endere'#231'o'
      object Label5: TLabel
        Left = 444
        Top = 34
        Width = 45
        Height = 13
        Caption = 'N'#250'mero'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbEnderecoCobranca: TLabel
        Left = 10
        Top = 34
        Width = 53
        Height = 13
        Caption = 'Endere'#231'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 10
        Top = 60
        Width = 80
        Height = 13
        Caption = 'Complemento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbBairroCobranca: TLabel
        Left = 10
        Top = 86
        Width = 35
        Height = 13
        Caption = 'Bairro'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbCidadeCobranca: TLabel
        Left = 10
        Top = 112
        Width = 40
        Height = 13
        Caption = 'Cidade'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbCEPCobranca: TLabel
        Left = 10
        Top = 138
        Width = 23
        Height = 13
        Caption = 'CEP'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbEstadoCobranca: TLabel
        Left = 185
        Top = 138
        Width = 38
        Height = 13
        Caption = 'Estado'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object edtNumero: TEdit
        Left = 492
        Top = 33
        Width = 98
        Height = 19
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 50
        ParentFont = False
        TabOrder = 1
      end
      object ComboEndereco: TComboBox
        Left = 94
        Top = 32
        Width = 345
        Height = 21
        CharCase = ecUpperCase
        ItemHeight = 13
        MaxLength = 50
        TabOrder = 0
      end
      object EdtComplemento: TEdit
        Left = 94
        Top = 58
        Width = 497
        Height = 19
        TabOrder = 2
      end
      object EdtCEP: TEdit
        Left = 94
        Top = 138
        Width = 80
        Height = 19
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 5
      end
      object ComboEstado: TComboBox
        Left = 230
        Top = 138
        Width = 41
        Height = 21
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 6
        Items.Strings = (
          'AC'
          'AL'
          'AM'
          'AP'
          'BA'
          'CE'
          'DF'
          'ES'
          'GO'
          'MA'
          'MG'
          'MS'
          'MT'
          'PA'
          'PB'
          'PE'
          'PI'
          'PR'
          'RJ'
          'RN'
          'RO'
          'RR'
          'RS'
          'SC'
          'SE'
          'SP'
          'TO')
      end
      object ComboCidade: TComboBox
        Left = 94
        Top = 111
        Width = 239
        Height = 21
        CharCase = ecUpperCase
        ItemHeight = 13
        TabOrder = 4
      end
      object ComboBairro: TComboBox
        Left = 94
        Top = 85
        Width = 238
        Height = 21
        CharCase = ecUpperCase
        ItemHeight = 13
        TabOrder = 3
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&3 - Cr'#233'dito'
      object LbDataUltimaCompra: TLabel
        Left = 7
        Top = 4
        Width = 117
        Height = 13
        Caption = 'Data Ultima Compra'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbValorUltimaCompra: TLabel
        Left = 7
        Top = 28
        Width = 120
        Height = 13
        Caption = 'Valor Ultima Compra'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbDataMaiorFatura: TLabel
        Left = 279
        Top = 4
        Width = 102
        Height = 13
        Caption = 'Data Maior Fatura'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbValorMaiorFatura: TLabel
        Left = 279
        Top = 28
        Width = 105
        Height = 13
        Caption = 'Valor Maior Fatura'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbSituacaoSerasa: TLabel
        Left = 10
        Top = 60
        Width = 93
        Height = 13
        Caption = 'Situa'#231#227'o Serasa'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 10
        Top = 279
        Width = 83
        Height = 13
        Caption = 'Data Cadastro'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbAtivo: TLabel
        Left = 10
        Top = 252
        Width = 29
        Height = 13
        Caption = 'Ativo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbPermiteVendaAPrazo: TLabel
        Left = 10
        Top = 228
        Width = 131
        Height = 13
        Caption = 'Permite Venda a Prazo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbDataNascimento: TLabel
        Left = 10
        Top = 204
        Width = 97
        Height = 13
        Caption = 'Data Nascimento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbContaCorrente: TLabel
        Left = 10
        Top = 180
        Width = 89
        Height = 13
        Caption = 'Conta Corrente'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbAgencia: TLabel
        Left = 10
        Top = 156
        Width = 45
        Height = 13
        Caption = 'Ag'#234'ncia'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbBanco: TLabel
        Left = 10
        Top = 132
        Width = 35
        Height = 13
        Caption = 'Banco'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbCodigoPlanoContas: TLabel
        Left = 10
        Top = 108
        Width = 119
        Height = 13
        Caption = 'C'#243'digo Plano Contas'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbLimiteCredito: TLabel
        Left = 10
        Top = 84
        Width = 80
        Height = 13
        Caption = 'Limite Cr'#233'dito'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomePlanodeContas: TLabel
        Left = 224
        Top = 110
        Width = 131
        Height = 13
        Caption = 'LbNomePlanodeContas'
      end
      object EdtDataUltimaCompra: TMaskEdit
        Left = 148
        Top = 3
        Width = 65
        Height = 19
        EditMask = '!99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 0
        Text = '  /  /    '
      end
      object EdtValorUltimaCompra: TEdit
        Left = 148
        Top = 27
        Width = 66
        Height = 19
        MaxLength = 9
        TabOrder = 1
      end
      object EdtDataMaiorFatura: TMaskEdit
        Left = 413
        Top = 3
        Width = 70
        Height = 19
        EditMask = '!99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 2
        Text = '  /  /    '
      end
      object EdtValorMaiorFatura: TEdit
        Left = 413
        Top = 27
        Width = 71
        Height = 19
        MaxLength = 9
        TabOrder = 3
      end
      object EdtSituacaoSerasa: TEdit
        Left = 151
        Top = 58
        Width = 235
        Height = 19
        MaxLength = 30
        TabOrder = 4
      end
      object EdtLimiteCredito: TEdit
        Left = 151
        Top = 82
        Width = 67
        Height = 19
        MaxLength = 9
        TabOrder = 5
      end
      object EdtCodigoPlanoContas: TEdit
        Left = 151
        Top = 106
        Width = 67
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 6
        OnExit = EdtCodigoPlanoContasExit
        OnKeyDown = EdtCodigoPlanoContasKeyDown
      end
      object EdtBanco: TEdit
        Left = 151
        Top = 131
        Width = 235
        Height = 19
        MaxLength = 30
        TabOrder = 7
      end
      object EdtAgencia: TEdit
        Left = 151
        Top = 155
        Width = 235
        Height = 19
        MaxLength = 30
        TabOrder = 8
      end
      object EdtContaCorrente: TEdit
        Left = 151
        Top = 179
        Width = 235
        Height = 19
        MaxLength = 30
        TabOrder = 9
      end
      object EdtDataNascimento: TMaskEdit
        Left = 151
        Top = 203
        Width = 65
        Height = 19
        EditMask = '!99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 10
        Text = '  /  /    '
      end
      object ComboPermiteVendaAPrazo: TComboBox
        Left = 151
        Top = 227
        Width = 39
        Height = 21
        ItemHeight = 13
        TabOrder = 11
        Items.Strings = (
          'S'
          'N')
      end
      object ComboAtivo: TComboBox
        Left = 151
        Top = 252
        Width = 39
        Height = 21
        ItemHeight = 13
        TabOrder = 12
        Items.Strings = (
          'S'
          'N')
      end
      object EdtDataCadastro: TMaskEdit
        Left = 151
        Top = 280
        Width = 67
        Height = 19
        EditMask = '!99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 13
        Text = '  /  /    '
      end
    end
  end
end
