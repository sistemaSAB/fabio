object FGRUPOPERSIANA: TFGRUPOPERSIANA
  Left = 611
  Top = 228
  Width = 767
  Height = 402
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'GRUPO DE PERSIANA'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object imgrodape: TImage
    Left = 0
    Top = 331
    Width = 751
    Height = 33
    Align = alBottom
  end
  object lbLbReferencia: TLabel
    Left = 19
    Top = 83
    Width = 53
    Height = 14
    Caption = 'Refer'#234'ncia'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object lbLbNome: TLabel
    Left = 19
    Top = 122
    Width = 27
    Height = 14
    Caption = 'Nome'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object lb5: TLabel
    Left = 19
    Top = 160
    Width = 78
    Height = 14
    Caption = 'Plano de Contas'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object lbNomePlanoDeContas: TLabel
    Left = 97
    Top = 178
    Width = 493
    Height = 13
    AutoSize = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clAqua
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btPrimeiro: TSpeedButton
    Left = 292
    Top = 284
    Width = 41
    Height = 28
    Flat = True
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333FFF3333333333333707333333333333F777F3333333333370
      9033333333F33F7737F33333373337090733333337F3F7737733333330037090
      73333333377F7737733333333090090733333333373773773333333309999073
      333333337F333773333333330999903333333333733337F33333333099999903
      33333337F3333F7FF33333309999900733333337333FF7773333330999900333
      3333337F3FF7733333333309900333333333337FF77333333333309003333333
      333337F773333333333330033333333333333773333333333333333333333333
      3333333333333333333333333333333333333333333333333333}
    NumGlyphs = 2
    OnClick = btPrimeiroClick
  end
  object btAnterior: TSpeedButton
    Left = 333
    Top = 284
    Width = 40
    Height = 28
    Flat = True
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      3333333333333FF3333333333333003333333333333F77F33333333333009033
      333333333F7737F333333333009990333333333F773337FFFFFF330099999000
      00003F773333377777770099999999999990773FF33333FFFFF7330099999000
      000033773FF33777777733330099903333333333773FF7F33333333333009033
      33333333337737F3333333333333003333333333333377333333333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333}
    NumGlyphs = 2
    OnClick = btAnteriorClick
  end
  object btProximo: TSpeedButton
    Left = 374
    Top = 284
    Width = 40
    Height = 27
    Flat = True
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333FF3333333333333003333
      3333333333773FF3333333333309003333333333337F773FF333333333099900
      33333FFFFF7F33773FF30000000999990033777777733333773F099999999999
      99007FFFFFFF33333F7700000009999900337777777F333F7733333333099900
      33333333337F3F77333333333309003333333333337F77333333333333003333
      3333333333773333333333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333}
    NumGlyphs = 2
    OnClick = btProximoClick
  end
  object btUltimo: TSpeedButton
    Left = 415
    Top = 284
    Width = 40
    Height = 27
    Flat = True
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      33333333333333333333333333333333333333FF333333333333370733333333
      33333777F33333333333309073333333333337F77F3333F33333370907333733
      3333377F77F337F3333333709073003333333377F77F77F33333333709009033
      333333377F77373F33333333709999033333333377F3337F3333333330999903
      3333333337333373F333333309999990333333337FF33337F333333700999990
      33333337773FF3373F333333330099990333333333773FF37F33333333330099
      033333333333773F73F3333333333300903333333333337737F3333333333333
      0033333333333333773333333333333333333333333333333333}
    NumGlyphs = 2
    OnClick = btUltimoClick
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 751
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    DesignSize = (
      751
      57)
    object lbnomeformulario: TLabel
      Left = 497
      Top = 3
      Width = 85
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Grupo de '
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigo: TLabel
      Left = 620
      Top = 17
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb1: TLabel
      Left = 497
      Top = 25
      Width = 84
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Persianas'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object btrelatorio: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btRelatorioClick
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btopcoesClick
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btPesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btExcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btCancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btSalvarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btAlterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btNovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btSairClick
      Spacing = 0
    end
  end
  object edtReferencia: TEdit
    Left = 19
    Top = 97
    Width = 70
    Height = 19
    MaxLength = 20
    TabOrder = 1
  end
  object edtNome: TEdit
    Left = 19
    Top = 136
    Width = 400
    Height = 19
    MaxLength = 50
    TabOrder = 2
  end
  object edtPlanodeContas: TEdit
    Left = 19
    Top = 175
    Width = 70
    Height = 19
    Color = 6073854
    MaxLength = 9
    TabOrder = 3
    OnDblClick = edtPlanodeContasDblClick
    OnExit = edtPlanodeContasExit
    OnKeyDown = edtPlanodeContasKeyDown
  end
  object grp1: TGroupBox
    Left = 261
    Top = 208
    Width = 180
    Height = 43
    Caption = 'Controla por M'#178
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 4
    object rbRadioControlaMetroQuadradoSIM: TRadioButton
      Left = 8
      Top = 16
      Width = 52
      Height = 17
      Caption = 'SIM'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object rbRadioControlaMetroQuadradoNAo: TRadioButton
      Left = 88
      Top = 16
      Width = 52
      Height = 17
      Caption = 'N'#195'O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  object grp2: TGroupBox
    Left = 20
    Top = 207
    Width = 211
    Height = 106
    Caption = 'Margens'
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 5
    object lbLbPorcentagemInstalado: TLabel
      Left = 11
      Top = 23
      Width = 43
      Height = 14
      Caption = 'Instalado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbPorcentagemFornecido: TLabel
      Left = 11
      Top = 50
      Width = 48
      Height = 14
      Caption = 'Fornecido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbPorcentagemRetirado: TLabel
      Left = 11
      Top = 78
      Width = 40
      Height = 14
      Caption = 'Retirado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb2: TLabel
      Left = 158
      Top = 83
      Width = 10
      Height = 14
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb3: TLabel
      Left = 158
      Top = 53
      Width = 10
      Height = 14
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb4: TLabel
      Left = 158
      Top = 25
      Width = 10
      Height = 14
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object edtPorcentagemRetirado: TEdit
      Left = 85
      Top = 79
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
    end
    object edtPorcentagemFornecido: TEdit
      Left = 85
      Top = 49
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
    end
    object edtPorcentagemInstalado: TEdit
      Left = 85
      Top = 22
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
    end
  end
end
