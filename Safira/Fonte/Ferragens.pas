//Unit que cria ferragens estaticas do sistema

unit Ferragens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, jpeg;

type
  TForm9 = class(TForm)
    PanelPrinc: TPanel;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    Image8: TImage;
    Image9: TImage;
    Image10: TImage;
    Image11: TImage;
    Image12: TImage;
    Image13: TImage;
    Image14: TImage;
    Image15: TImage;
    Image16: TImage;
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure Image3Click(Sender: TObject);
    procedure Image4Click(Sender: TObject);
    procedure Image5Click(Sender: TObject);
    procedure Image6Click(Sender: TObject);
    procedure Image7Click(Sender: TObject);
    procedure Image8Click(Sender: TObject);
    procedure Image9Click(Sender: TObject);
    procedure Image10Click(Sender: TObject);
    procedure Image11Click(Sender: TObject);
    procedure Image12Click(Sender: TObject);
    procedure Image13Click(Sender: TObject);
    procedure Image14Click(Sender: TObject);
    procedure Image15Click(Sender: TObject);
    procedure Image16Click(Sender: TObject);
  private
  public
     tipoFerragem : Integer;
  end;

var
  Form9: TForm9;

implementation

{$R *.dfm}

//Id das imagens que � repassada a Unit DesenhoPrincipal
//quando � clicado em cima de uma das imagens estaticas
procedure TForm9.Image1Click(Sender: TObject);
begin
     tipoFerragem := 4;  //primeira figura da tela
end;

procedure TForm9.Image2Click(Sender: TObject);
begin
    tipoFerragem := 5;  //segunda figura da tela
end;

procedure TForm9.Image3Click(Sender: TObject);
begin
    tipoFerragem := 6;  //terceira figura da tela
end;

procedure TForm9.Image4Click(Sender: TObject);
begin
    tipoFerragem := 7;  //quarta figura da tela
end;

procedure TForm9.Image5Click(Sender: TObject);
begin
    tipoFerragem := 8;  //quinta figura da tela
end;

procedure TForm9.Image6Click(Sender: TObject);
begin
     tipoFerragem := 9;   //sexta figura da tela
end;

procedure TForm9.Image7Click(Sender: TObject);
begin
    tipoFerragem := 10;    //setima figura da tela
end;

procedure TForm9.Image8Click(Sender: TObject);
begin
    tipoFerragem := 11;     //oitava figura da tela
end;

procedure TForm9.Image9Click(Sender: TObject);
begin
    tipoFerragem := 12;     //nona figura da tela
end;

procedure TForm9.Image10Click(Sender: TObject);
begin
    tipoFerragem := 13;     //decima figura da tela
end;

procedure TForm9.Image11Click(Sender: TObject);
begin
     tipoFerragem := 14;     //decima primeira figura da tela
end;

procedure TForm9.Image12Click(Sender: TObject);
begin
    tipoFerragem := 15;
end;

procedure TForm9.Image13Click(Sender: TObject);
begin
    tipoFerragem := 16;
end;

procedure TForm9.Image14Click(Sender: TObject);
begin
    tipoFerragem := 17;
end;

procedure TForm9.Image15Click(Sender: TObject);
begin
    tipoFerragem := 18;
end;

procedure TForm9.Image16Click(Sender: TObject);
begin
     tipoFerragem := 19;
end;

end.
