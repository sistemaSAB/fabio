object FNfeDigitada: TFNfeDigitada
  Left = 553
  Top = 201
  Width = 966
  Height = 713
  Caption = 'Nota fiscal eletr'#244'nica digitada - Exclaim Tecnologia'
  Color = clBtnFace
  Constraints.MinHeight = 496
  Constraints.MinWidth = 888
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pgcGuia: TPageControl
    Left = 0
    Top = 55
    Width = 958
    Height = 571
    ActivePage = tsNFeDigitada
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    Images = il2
    ParentFont = False
    Style = tsFlatButtons
    TabOrder = 0
    OnChange = pgcGuiaChange
    object tsNFeDigitada: TTabSheet
      Caption = 'NF-e Digitada'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ImageIndex = 7
      ParentFont = False
      object panelCabecalho: TPanel
        Left = 0
        Top = 0
        Width = 950
        Height = 180
        Align = alTop
        Color = clSilver
        Constraints.MinHeight = 100
        TabOrder = 0
        DesignSize = (
          950
          180)
        object imagemFundo: TImage
          Left = 1
          Top = 1
          Width = 948
          Height = 178
          Align = alClient
          Stretch = True
        end
        object Label1: TLabel
          Left = 9
          Top = 13
          Width = 117
          Height = 16
          Caption = 'Cliente.................:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 9
          Top = 62
          Width = 108
          Height = 16
          Caption = 'Transportadora..:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 347
          Top = 138
          Width = 85
          Height = 16
          Caption = 'Data emiss'#227'o'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 108
          Top = 138
          Width = 87
          Height = 16
          Caption = 'Data de saida'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 633
          Top = 76
          Width = 147
          Height = 16
          Anchors = [akRight, akBottom]
          Caption = 'Informa'#231#245'es adicionais'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 10
          Top = 138
          Width = 88
          Height = 16
          Caption = 'Hora de saida'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbNomeCliente: TLabel
          Left = 210
          Top = 13
          Width = 343
          Height = 16
          Cursor = crHandPoint
          AutoSize = False
          Caption = 'lbnomeCliente'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = lbNomeClienteClick
          OnMouseMove = lbNomeClienteMouseMove
          OnMouseLeave = lbNomeClienteMouseLeave
        end
        object lbNomeTransportadora: TLabel
          Left = 210
          Top = 65
          Width = 343
          Height = 16
          Cursor = crHandPoint
          AutoSize = False
          Caption = 'lbnomeTransportadora'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = lbNomeTransportadoraClick
          OnMouseMove = lbNomeTransportadoraMouseMove
          OnMouseLeave = lbNomeTransportadoraMouseLeave
        end
        object Label18: TLabel
          Left = 633
          Top = 34
          Width = 51
          Height = 16
          Anchors = [akRight, akBottom]
          AutoSize = False
          Caption = 'NF-e...:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbNFE: TLabel
          Left = 687
          Top = 34
          Width = 70
          Height = 16
          Cursor = crHandPoint
          Hint = 'Abre o m'#243'dulo de nota fiscal eletr'#244'nica'
          Anchors = [akRight, akBottom]
          Caption = '0000000001'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = lbNFEClick
          OnMouseMove = lbNFEMouseMove
          OnMouseLeave = lbNFEMouseLeave
        end
        object Label25: TLabel
          Left = 633
          Top = 54
          Width = 50
          Height = 16
          Anchors = [akRight, akBottom]
          AutoSize = False
          Caption = 'Nota...:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          PopupMenu = popMenu1
        end
        object lbNota: TLabel
          Left = 687
          Top = 54
          Width = 81
          Height = 16
          Cursor = crHandPoint
          Anchors = [akRight, akBottom]
          AutoSize = False
          Caption = '0000000001'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = lbNotaClick
          OnMouseMove = lbNotaMouseMove
          OnMouseLeave = lbNotaMouseLeave
        end
        object Label14: TLabel
          Left = 9
          Top = 38
          Width = 112
          Height = 16
          Caption = 'Fornecedor.........:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbNomeFornecedor: TLabel
          Left = 210
          Top = 39
          Width = 122
          Height = 16
          Cursor = crHandPoint
          Caption = 'lbNomeFornecedor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          OnClick = lbNomeFornecedorClick
          OnMouseMove = lbNomeFornecedorMouseMove
          OnMouseLeave = lbNomeFornecedorMouseLeave
        end
        object lboperacao: TLabel
          Left = 210
          Top = 91
          Width = 169
          Height = 17
          AutoSize = False
          Caption = 'Opera'#231#227'o de Venda'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lb1: TLabel
          Left = 9
          Top = 87
          Width = 62
          Height = 16
          Caption = 'Opera'#231#227'o'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbBaixaEstoque: TLabel
          Left = 26
          Top = 114
          Width = 91
          Height = 16
          Cursor = crHandPoint
          Caption = 'Baixa estoque'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lb2: TLabel
          Left = 283
          Top = 114
          Width = 109
          Height = 16
          Cursor = crHandPoint
          Caption = 'Ind. Pagamento'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label15: TLabel
          Left = 148
          Top = 114
          Width = 125
          Height = 16
          Caption = 'Imposto autom'#225'tico'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label19: TLabel
          Left = 232
          Top = 138
          Width = 106
          Height = 16
          Caption = 'Hora de emiss'#227'o'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbEnderecoEntrega: TLabel
          Left = 632
          Top = 13
          Width = 117
          Height = 16
          Cursor = crHelp
          Caption = 'Novo End. Entrega'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = lbEnderecoEntregaClick
        end
        object memoInformacoes: TMemo
          Left = 632
          Top = 92
          Width = 314
          Height = 85
          Hint = 'Informa'#231#245'es complementares da Nf-e'
          Anchors = [akRight, akBottom]
          BorderStyle = bsNone
          Ctl3D = False
          ParentCtl3D = False
          ParentShowHint = False
          ScrollBars = ssBoth
          ShowHint = True
          TabOrder = 11
        end
        object edtCliente: TEdit
          Left = 132
          Top = 10
          Width = 75
          Height = 20
          Hint = 'Cliente destinat'#225'rio'
          Color = 6073854
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnExit = edtClienteExit
          OnKeyDown = edtClienteKeyDown
          OnKeyPress = edtClienteKeyPress
        end
        object edtTransportadora: TEdit
          Left = 132
          Top = 62
          Width = 75
          Height = 20
          Hint = 'Transportadora'
          Color = 6073854
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnExit = edtTransportadoraExit
          OnKeyDown = edtTransportadoraKeyDown
          OnKeyPress = edtTransportadoraKeyPress
        end
        object edtDataEmissao: TMaskEdit
          Left = 347
          Top = 155
          Width = 89
          Height = 20
          Hint = 'Data de emiss'#227'o do documento fiscal'
          Ctl3D = False
          EditMask = '!99/99/9999;1;_'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 10
          Text = '  /  /    '
          OnKeyDown = edtDataEmissaoKeyDown
        end
        object edtDataSaida: TMaskEdit
          Left = 108
          Top = 155
          Width = 90
          Height = 20
          Hint = 'Data de saida da mercadoria'
          Ctl3D = False
          EditMask = '!99/99/9999;1;_'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
          Text = '  /  /    '
          OnKeyDown = edtDataSaidaKeyDown
        end
        object edtHoraSaida: TMaskEdit
          Left = 10
          Top = 155
          Width = 85
          Height = 20
          Hint = 'Hora de saida da mercadoria'
          Ctl3D = False
          EditMask = '!90:00:00;_'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 8
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          Text = '  :  :  '
          OnKeyDown = edtHoraSaidaKeyDown
        end
        object edtFornecedor: TEdit
          Left = 132
          Top = 36
          Width = 75
          Height = 20
          Color = 6073854
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          OnExit = edtFornecedorExit
          OnKeyDown = edtFornecedorKeyDown
          OnKeyPress = edtFornecedorKeyPress
        end
        object chkBaixaEstoque: TCheckBox
          Left = 10
          Top = 114
          Width = 13
          Height = 16
          BiDiMode = bdLeftToRight
          Color = clSilver
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentColor = False
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          TabOrder = 4
        end
        object ComboIndPag1: TComboBox
          Left = 394
          Top = 112
          Width = 90
          Height = 22
          ItemHeight = 14
          ItemIndex = 0
          TabOrder = 6
          Text = '0 - A vista'
          Items.Strings = (
            '0 - A vista'
            '1 - A prazo'
            '2 - Outros')
        end
        object edtoperacao: TEdit
          Left = 132
          Top = 88
          Width = 74
          Height = 20
          Color = 6073854
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 3
          OnExit = edtoperacaoExit
          OnKeyDown = edtoperacaoKeyDown
        end
        object checkImpostoAutomatico: TCheckBox
          Left = 132
          Top = 114
          Width = 13
          Height = 16
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
        end
        object edtHoraEmissao: TMaskEdit
          Left = 232
          Top = 155
          Width = 104
          Height = 20
          Ctl3D = False
          EditMask = '!90:00:00;_'
          MaxLength = 8
          ParentCtl3D = False
          TabOrder = 9
          Text = '  :  :  '
          OnKeyDown = edtHoraEmissaoKeyDown
        end
      end
      object panelProdutos: TPanel
        Left = 0
        Top = 180
        Width = 950
        Height = 120
        Align = alTop
        Color = 14024703
        Constraints.MaxHeight = 200
        Constraints.MinHeight = 48
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 1
        DesignSize = (
          950
          120)
        object Label7: TLabel
          Left = 11
          Top = 47
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Item'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label8: TLabel
          Left = 175
          Top = 47
          Width = 42
          Height = 16
          Caption = 'Quant.'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label9: TLabel
          Left = 276
          Top = 47
          Width = 53
          Height = 16
          Caption = 'Valor R$'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label10: TLabel
          Left = 404
          Top = 47
          Width = 78
          Height = 16
          Caption = 'Desconto R$'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbDescricaoProduto: TLabel
          Left = 98
          Top = 73
          Width = 524
          Height = 16
          AutoSize = False
          Caption = 
            'descri'#231#227'o produto xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' +
            'xxxx'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label20: TLabel
          Left = 11
          Top = 73
          Width = 69
          Height = 16
          Caption = 'descri'#231#227'o.:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label22: TLabel
          Left = 11
          Top = 97
          Width = 65
          Height = 16
          Caption = 'Cor..........:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label23: TLabel
          Left = 659
          Top = 47
          Width = 53
          Height = 16
          Caption = 'Frete R$'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object v: TLabel
          Left = 11
          Top = 11
          Width = 28
          Height = 16
          Caption = 'Tipo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object label55: TLabel
          Left = 98
          Top = 47
          Width = 22
          Height = 16
          Caption = 'Cor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbCor: TLabel
          Left = 98
          Top = 96
          Width = 521
          Height = 13
          AutoSize = False
          Caption = 
            'cor do produto xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' +
            'xxxx'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Image1: TImage
          Left = 757
          Top = 67
          Width = 20
          Height = 20
          Cursor = crHandPoint
          Picture.Data = {
            0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000001000
            00001008060000001FF3FF61000000097048597300000B1200000B1201D2DD7E
            FC00000A4F6943435050686F746F73686F70204943432070726F66696C650000
            78DA9D53675453E9163DF7DEF4424B8880944B6F5215082052428B801491262A
            2109104A8821A1D91551C1114545041BC8A088038E8E808C15512C0C8A0AD807
            E421A28E83A3888ACAFBE17BA36BD6BCF7E6CDFEB5D73EE7ACF39DB3CF07C008
            0C9648335135800CA9421E11E083C7C4C6E1E42E40810A2470001008B3642173
            FD230100F87E3C3C2B22C007BE000178D30B0800C04D9BC0301C87FF0FEA4299
            5C01808401C07491384B08801400407A8E42A600404601809D98265300A00400
            60CB6362E300502D0060277FE6D300809DF8997B01005B94211501A091002013
            65884400683B00ACCF568A450058300014664BC43900D82D00304957664800B0
            B700C0CE100BB200080C00305188852900047B0060C8232378008499001446F2
            573CF12BAE10E72A00007899B23CB9243945815B082D710757572E1E28CE4917
            2B14366102619A402EC27999193281340FE0F3CC0000A0911511E083F3FD78CE
            0EAECECE368EB60E5F2DEABF06FF226262E3FEE5CFAB70400000E1747ED1FE2C
            2FB31A803B06806DFEA225EE04685E0BA075F78B66B20F40B500A0E9DA57F370
            F87E3C3C45A190B9D9D9E5E4E4D84AC4425B61CA577DFE67C25FC057FD6CF97E
            3CFCF7F5E0BEE22481325D814704F8E0C2CCF44CA51CCF92098462DCE68F47FC
            B70BFFFC1DD322C44962B9582A14E35112718E449A8CF332A52289429229C525
            D2FF64E2DF2CFB033EDF3500B06A3E017B912DA85D6303F64B27105874C0E2F7
            0000F2BB6FC1D4280803806883E1CF77FFEF3FFD47A02500806649927100005E
            44242E54CAB33FC708000044A0812AB0411BF4C1182CC0061CC105DCC10BFC60
            36844224C4C24210420A64801C726029AC82422886CDB01D2A602FD4401D34C0
            51688693700E2EC255B80E3D700FFA61089EC128BC81090441C808136121DA88
            01628A58238E08179985F821C14804128B2420C9881451224B91354831528A54
            2055481DF23D720239875C46BA913BC8003282FC86BC47319481B2513DD40CB5
            43B9A8371A8446A20BD06474319A8F16A09BD072B41A3D8C36A1E7D0AB680FDA
            8F3E43C730C0E8180733C46C302EC6C342B1382C099363CBB122AC0CABC61AB0
            56AC03BB89F563CFB17704128145C0093604774220611E4148584C584ED848A8
            201C243411DA093709038451C2272293A84BB426BA11F9C4186232318758482C
            23D6128F132F107B8843C437241289433227B9900249B1A454D212D246D26E52
            23E92CA99B34481A2393C9DA646BB20739942C202BC885E49DE4C3E433E41BE4
            21F25B0A9D624071A4F853E22852CA6A4A19E510E534E5066598324155A39A52
            DDA8A15411358F5A42ADA1B652AF5187A81334759A39CD8316494BA5ADA295D3
            1A681768F769AFE874BA11DD951E4E97D057D2CBE947E897E803F4770C0D8615
            83C7886728199B18071867197718AF984CA619D38B19C754303731EB98E7990F
            996F55582AB62A7C1591CA0A954A9526951B2A2F54A9AAA6AADEAA0B55F355CB
            548FA95E537DAE46553353E3A909D496AB55AA9D50EB531B5367A93BA887AA67
            A86F543FA47E59FD890659C34CC34F43A451A0B15FE3BCC6200B6319B3782C21
            6B0DAB86758135C426B1CDD97C762ABB98FD1DBB8B3DAAA9A13943334A3357B3
            52F394663F07E39871F89C744E09E728A797F37E8ADE14EF29E2291BA6344CB9
            31655C6BAA96979658AB48AB51AB47EBBD36AEEDA79DA6BD45BB59FB810E41C7
            4A275C2747678FCE059DE753D953DDA70AA7164D3D3AF5AE2EAA6BA51BA1BB44
            77BF6EA7EE989EBE5E809E4C6FA7DE79BDE7FA1C7D2FFD54FD6DFAA7F5470C58
            06B30C2406DB0CCE183CC535716F3C1D2FC7DBF151435DC34043A561956197E1
            8491B9D13CA3D5468D460F8C69C65CE324E36DC66DC6A326062621264B4DEA4D
            EE9A524DB9A629A63B4C3B4CC7CDCCCDA2CDD699359B3D31D732E79BE79BD79B
            DFB7605A785A2CB6A8B6B86549B2E45AA659EEB6BC6E855A3959A558555A5DB3
            46AD9DAD25D6BBADBBA711A7B94E934EAB9ED667C3B0F1B6C9B6A9B719B0E5D8
            06DBAEB66DB67D6167621767B7C5AEC3EE93BD937DBA7D8DFD3D070D87D90EAB
            1D5A1D7E73B472143A563ADE9ACE9CEE3F7DC5F496E92F6758CF10CFD833E3B6
            13CB29C4699D539BD347671767B97383F3888B894B82CB2E973E2E9B1BC6DDC8
            BDE44A74F5715DE17AD2F59D9BB39BC2EDA8DBAFEE36EE69EE87DC9FCC349F29
            9E593373D0C3C843E051E5D13F0B9F95306BDFAC7E4F434F8167B5E7232F632F
            9157ADD7B0B7A577AAF761EF173EF63E729FE33EE33C37DE32DE595FCC37C0B7
            C8B7CB4FC36F9E5F85DF437F23FF64FF7AFFD100A78025016703898141815B02
            FBF87A7C21BF8E3F3ADB65F6B2D9ED418CA0B94115418F82AD82E5C1AD2168C8
            EC90AD21F7E798CE91CE690E85507EE8D6D00761E6618BC37E0C278587855786
            3F8E7088581AD131973577D1DC4373DF44FA449644DE9B67314F39AF2D4A352A
            3EAA2E6A3CDA37BA34BA3FC62E6659CCD5589D58496C4B1C392E2AAE366E6CBE
            DFFCEDF387E29DE20BE37B17982FC85D7079A1CEC2F485A716A92E122C3A9640
            4C884E3894F041102AA8168C25F21377258E0A79C21DC267222FD136D188D843
            5C2A1E4EF2482A4D7A92EC91BC357924C533A52CE5B98427A990BC4C0D4CDD9B
            3A9E169A76206D323D3ABD31839291907142AA214D93B667EA67E66676CBAC65
            85B2FEC56E8BB72F1E9507C96BB390AC05592D0AB642A6E8545A28D72A07B267
            655766BFCD89CA3996AB9E2BCDEDCCB3CADB90379CEF9FFFED12C212E192B6A5
            864B572D1D58E6BDAC6A39B23C7179DB0AE315052B865606AC3CB88AB62A6DD5
            4FABED5797AE7EBD267A4D6B815EC1CA82C1B5016BEB0B550AE5857DEBDCD7ED
            5D4F582F59DFB561FA869D1B3E15898AAE14DB1797157FD828DC78E51B876FCA
            BF99DC94B4A9ABC4B964CF66D266E9E6DE2D9E5B0E96AA97E6970E6E0DD9DAB4
            0DDF56B4EDF5F645DB2F97CD28DBBB83B643B9A3BF3CB8BC65A7C9CECD3B3F54
            A454F454FA5436EED2DDB561D7F86ED1EE1B7BBCF634ECD5DB5BBCF7FD3EC9BE
            DB5501554DD566D565FB49FBB3F73FAE89AAE9F896FB6D5DAD4E6D71EDC703D2
            03FD07230EB6D7B9D4D51DD23D54528FD62BEB470EC71FBEFE9DEF772D0D360D
            558D9CC6E223704479E4E9F709DFF71E0D3ADA768C7BACE107D31F761D671D2F
            6A429AF29A469B539AFB5B625BBA4FCC3ED1D6EADE7AFC47DB1F0F9C343C5979
            4AF354C969DAE982D39367F2CF8C9D959D7D7E2EF9DC60DBA2B67BE763CEDF6A
            0F6FEFBA1074E1D245FF8BE73BBC3BCE5CF2B874F2B2DBE51357B8579AAF3A5F
            6DEA74EA3CFE93D34FC7BB9CBB9AAEB95C6BB9EE7ABDB57B66F7E91B9E37CEDD
            F4BD79F116FFD6D59E393DDDBDF37A6FF7C5F7F5DF16DD7E7227FDCECBBBD977
            27EEADBC4FBC5FF440ED41D943DD87D53F5BFEDCD8EFDC7F6AC077A0F3D1DC47
            F7068583CFFE91F58F0F43058F998FCB860D86EB9E383E3939E23F72FDE9FCA7
            43CF64CF269E17FEA2FECBAE17162F7EF8D5EBD7CED198D1A197F29793BF6D7C
            A5FDEAC0EB19AFDBC6C2C61EBEC97833315EF456FBEDC177DC771DEFA3DF0F4F
            E47C207F28FF68F9B1F553D0A7FB93199393FF040398F3FC63332DDB000002DF
            4944415478DA7D935D48935118C7FFE77DB7A99B737E371B321A5A9AA617CDD0
            B298BBB022B4B208522CC98C0CA1C0E8228388128A6E84A86E24D128C208B32E
            BCF0C20FA60DCAACD0CAB49A8A663A3F369DBAB6BD6FCFBB5A69840FFC39EF79
            CE39BFF37C9C9789A288801D3C765247C315523E29E6B77B82D444BA4C9AC20A
            6B6AA8050B000A4A4E158982703B384CA7516BD310AC8E83200858768E6361B2
            0FEEF9F129995C516E2CAC7962AD3FB31A40379F651C5F1393B81BBAA4EDC835
            46C0181F04D05AB76D192D96114C7EEDC59CAD9DA0DE223AFBF00FE040716982
            52ADEE0F5D9FAD88DDB80B86C4081CDA224318FF2BCC392FD060756374680CD3
            5FBAF063A6D7E5723A13A4D402805B5A434A45547211D4D1E1F07A7DF078BC10
            7C221803388E03C73370F4EDF86687C3D644D1F45513E052003098B4E368822A
            3E0B8228C0BDECF1E7BECA882497F3E0A85C4BDF5FE353D7831EF21A0300AF31
            AF92671A03165D4BF05104FF33B982234810BC8E31BC6BB9217523D60F387CE2
            B43B23FF9C42546E80C7EB21F920630218C5ECF309908291D208512A282D1EC2
            C2185E3EAD965A1BE707149494BD49C9294E8FDF9C8559A7802019A050317828
            5E5110C1BBE187101172198FE1B7560C58EE7590C71448E19A6E535A95B9B0C2
            DF36271D98A555DA0BAA23A2C82793AA49C683E1D9DDAB70CD8C5EA0E9CD0020
            922683C6BCB2C80CF33628950C8E259152A1DB290D958260049A99033A9B9FC3
            666D8472E485D965C8695BF990F611A439D55CCCA79B7662AB8E2146CE20BDD1
            E979119D23407F572B062D8D50D92C30E9EDE8FC80ACFA5776AB1401328FDF01
            3DCF3DB4BF2E44A3D546EBD310B64EEF2FDEFCD430ECC37D589C188472AC0772
            D767D47674A3AE7C2F4AEFDB991FB0C2424992438A484BF2057EA6D0A1D601BD
            7CB45D6432A42446627FE5753CAA3E8F7F016BDAACE5B129316AB12D96BA94DB
            F011178F14FCFD1BD732C658320D1A52786A9C3A3B53B754F59E4FF1AFFD0443
            65394158712C650000000049454E44AE426082}
          Transparent = True
          OnClick = Image1Click
        end
        object Label12: TLabel
          Left = 559
          Top = 47
          Width = 36
          Height = 16
          Caption = 'CFOP'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object edtProduto: TEdit
          Left = 45
          Top = 44
          Width = 47
          Height = 20
          Color = 6073854
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
          OnExit = edtProdutoExit
          OnKeyDown = edtProdutoKeyDown
          OnKeyPress = edtProdutoKeyPress
        end
        object edtQuantidade: TEdit
          Left = 221
          Top = 44
          Width = 45
          Height = 20
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 3
          OnKeyPress = edtQuantidadeKeyPress
        end
        object edtValorproduto: TEdit
          Left = 334
          Top = 44
          Width = 65
          Height = 20
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 4
          OnExit = edtValorprodutoExit
          OnKeyPress = edtValorprodutoKeyPress
        end
        object edtDesconto: TEdit
          Left = 485
          Top = 44
          Width = 55
          Height = 20
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 5
          OnExit = edtDescontoExit
          OnKeyPress = edtDescontoKeyPress
        end
        object btGravarProduto: TBitBtn
          Left = 817
          Top = 16
          Width = 39
          Height = 39
          Anchors = [akRight]
          TabOrder = 8
          OnClick = btGravarProdutoClick
        end
        object btCancelarProduto: TBitBtn
          Left = 857
          Top = 16
          Width = 39
          Height = 39
          Anchors = [akRight]
          TabOrder = 9
        end
        object btExcluirProduto: TBitBtn
          Left = 897
          Top = 16
          Width = 39
          Height = 39
          Anchors = [akRight]
          TabOrder = 10
          OnClick = btExcluirProdutoClick
        end
        object edtValorFrete: TEdit
          Left = 715
          Top = 44
          Width = 51
          Height = 20
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 7
          OnExit = edtValorFreteExit
          OnKeyPress = edtValorFreteKeyPress
        end
        object comboTipo: TComboBox
          Left = 47
          Top = 6
          Width = 188
          Height = 22
          CharCase = ecUpperCase
          Color = 6073854
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ItemHeight = 14
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnEnter = comboTipoEnter
          OnExit = comboTipoExit
          OnKeyDown = comboTipoKeyDown
          Items.Strings = (
            ''
            'FERRAGEM'
            'PERFILADO'
            'VIDRO'
            'KITBOX'
            'PERSIANA'
            'DIVERSO')
        end
        object edtCor: TEdit
          Left = 123
          Top = 44
          Width = 44
          Height = 20
          Color = 6073854
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 2
          OnExit = edtCorExit
          OnKeyDown = edtCorKeyDown
        end
        object edtcfop: TEdit
          Left = 598
          Top = 44
          Width = 51
          Height = 20
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 6
          OnKeyDown = edtcfopKeyDown
        end
      end
      object DBGridProdutos: TDBGrid
        Left = 0
        Top = 300
        Width = 950
        Height = 239
        Align = alClient
        BorderStyle = bsNone
        Ctl3D = False
        DataSource = dsProdutos
        Font.Charset = ANSI_CHARSET
        Font.Color = 5066061
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = [fsBold]
        OnCellClick = DBGridProdutosCellClick
        OnDrawColumnCell = DBGridProdutosDrawColumnCell
        OnDblClick = DBGridProdutosDblClick
        OnKeyDown = DBGridProdutosKeyDown
      end
    end
    object tsProximoNNfe: TTabSheet
      Caption = 'Pr'#243'ximo N'#250'mero NF-e'
      ImageIndex = 5
      object panelProximoNNfe: TPanel
        Left = 0
        Top = 0
        Width = 950
        Height = 539
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object bt_ok: TSpeedButton
          Left = 396
          Top = 70
          Width = 73
          Height = 25
          Caption = 'Salvar'
          Flat = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Glyph.Data = {
            46060000424D4606000000000000360400002800000016000000160000000100
            08000000000010020000120B0000120B0000000100004F000000099B4A00F0F0
            F0008CD0AA0050AC7900D7D7D7006AD0970023B16300CCCCCC003DD27F00FAFE
            FB000BB45600A6D5BB0099B5A600E6E6E60032AA6800BADCC9000EA853001AC4
            660087E9B3005FB084007AD1A1004AD58700C5E9D500B9E0CA00F7F7F700D5EF
            E0000AA550009EC9B10069DE9C00DEDEDE0022D873003AB872000BBB5A00E1FA
            EC004FC28300ADF3CB0029C86F00BAF5D4005DD7930019AD5B003FCF7F0064BC
            8B0022BF6800EFFCF500DDEEE4000AAD530080DDA90099E4BA008BBFA20010BC
            5D00EBF7F000CDEBDB009FD7B80039C0750041D3820072DBA10029B266000AA1
            4D007ED8A60034B56E0070D19C004AB47A000BB6570024C76C00D3F8E400FFFF
            FF008DE7B5001EB561000DAC5400F5FDF9003DAC6F0086CFA7004CD58900C5ED
            D700E7F6ED002BC6700044D1830040BA76000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000414141414141
            414141410909414141414141414141416E6741411801010D1D04040707070704
            041D0D0D011809416E6741411801010D0D1D04070C0704041D1D0D0101184141
            6E6741414141414141091B032930414141414141414141416E67414141414141
            2C130512123D0F4141414141414141416E6741414141410F3D1C1C1C1C1C0E2C
            41414141414141416E67414141410B1F48483608084C4C464541414141414141
            6E674141411706241131313131313F2A29414141414141416E6741413206313E
            4427272D2020203144344141414141416E674141472D27022C41093329440A0A
            0A271941414141416E6741414D1F4A41414141414116382D2D2D3B0941414141
            6E6741410532414141414141414132221A1A1A14414141416E67414141414141
            41414141414141093C1A1A10494141416E674141414141414141414141414141
            413A1A00350941416E67414141414141414141414141414141412E39392F4141
            6E6741414141414141414141414141414141413700432B416E67414141414141
            414141414141414141414141260042416E674141414141414141414141414141
            4141414145284B416E6741414141414141414141414141414141414141211E40
            6E67414141414141414141414141414141414141414123256E67414141414141
            414141414141414141414141414141416E674141414141414141414141414141
            41414141414141416E67}
          ParentFont = False
          OnClick = bt_okClick
        end
        object Label32: TLabel
          Left = 297
          Top = 13
          Width = 31
          Height = 14
          Caption = 'Status'
        end
        object Label31: TLabel
          Left = 100
          Top = 13
          Width = 63
          Height = 14
          Caption = 'Numero NF-e'
        end
        object Label30: TLabel
          Left = 12
          Top = 13
          Width = 13
          Height = 14
          Caption = 'NF'
        end
        object comboStatus: TComboBox
          Left = 297
          Top = 29
          Width = 172
          Height = 22
          ItemHeight = 14
          TabOrder = 3
          Text = 'Aberta para uso'
          OnKeyDown = comboStatusKeyDown
          Items.Strings = (
            'I - Impresa'
            'N - Nao utilizada/Aberta'
            'C - Cancelada'
            'Z - Inutilizada'
            'P - Processada'
            'T - Conting'#234'ncia ')
        end
        object edtNumeroNFEAte: TEdit
          Left = 186
          Top = 29
          Width = 81
          Height = 22
          TabOrder = 2
          OnChange = edtNumeroNFEAteChange
        end
        object edtNumeroNFEde: TEdit
          Left = 100
          Top = 29
          Width = 81
          Height = 22
          TabOrder = 1
        end
        object edtNF1: TEdit
          Left = 12
          Top = 29
          Width = 65
          Height = 22
          TabOrder = 0
        end
      end
    end
    object tsConfAarquivo: TTabSheet
      Caption = 'Configura'#231#227'o Arquivo sqlNFe'
      ImageIndex = 5
      object pageArquivoConf: TPageControl
        Left = 0
        Top = 0
        Width = 950
        Height = 539
        ActivePage = TabSheet6
        Align = alClient
        Images = il2
        Style = tsFlatButtons
        TabOrder = 0
        OnChange = pageArquivoConfChange
        object TabSheet5: TTabSheet
          Caption = 'Geral'
          ImageIndex = 5
          object Labe45: TLabel
            Left = 21
            Top = 225
            Width = 62
            Height = 13
            Caption = 'Certificado'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object btCertificado: TSpeedButton
            Left = 566
            Top = 238
            Width = 21
            Height = 22
            Flat = True
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
              07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
              0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
              33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
            OnClick = btCertificadoClick
          end
          object SpeedButton7: TSpeedButton
            Left = 555
            Top = 282
            Width = 78
            Height = 29
            Caption = 'Salvar'
            Flat = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            Glyph.Data = {
              46060000424D4606000000000000360400002800000016000000160000000100
              08000000000010020000120B0000120B0000000100004F000000099B4A00F0F0
              F0008CD0AA0050AC7900D7D7D7006AD0970023B16300CCCCCC003DD27F00FAFE
              FB000BB45600A6D5BB0099B5A600E6E6E60032AA6800BADCC9000EA853001AC4
              660087E9B3005FB084007AD1A1004AD58700C5E9D500B9E0CA00F7F7F700D5EF
              E0000AA550009EC9B10069DE9C00DEDEDE0022D873003AB872000BBB5A00E1FA
              EC004FC28300ADF3CB0029C86F00BAF5D4005DD7930019AD5B003FCF7F0064BC
              8B0022BF6800EFFCF500DDEEE4000AAD530080DDA90099E4BA008BBFA20010BC
              5D00EBF7F000CDEBDB009FD7B80039C0750041D3820072DBA10029B266000AA1
              4D007ED8A60034B56E0070D19C004AB47A000BB6570024C76C00D3F8E400FFFF
              FF008DE7B5001EB561000DAC5400F5FDF9003DAC6F0086CFA7004CD58900C5ED
              D700E7F6ED002BC6700044D1830040BA76000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000414141414141
              414141410909414141414141414141416E6741411801010D1D04040707070704
              041D0D0D011809416E6741411801010D0D1D04070C0704041D1D0D0101184141
              6E6741414141414141091B032930414141414141414141416E67414141414141
              2C130512123D0F4141414141414141416E6741414141410F3D1C1C1C1C1C0E2C
              41414141414141416E67414141410B1F48483608084C4C464541414141414141
              6E674141411706241131313131313F2A29414141414141416E6741413206313E
              4427272D2020203144344141414141416E674141472D27022C41093329440A0A
              0A271941414141416E6741414D1F4A41414141414116382D2D2D3B0941414141
              6E6741410532414141414141414132221A1A1A14414141416E67414141414141
              41414141414141093C1A1A10494141416E674141414141414141414141414141
              413A1A00350941416E67414141414141414141414141414141412E39392F4141
              6E6741414141414141414141414141414141413700432B416E67414141414141
              414141414141414141414141260042416E674141414141414141414141414141
              4141414145284B416E6741414141414141414141414141414141414141211E40
              6E67414141414141414141414141414141414141414123256E67414141414141
              414141414141414141414141414141416E674141414141414141414141414141
              41414141414141416E67}
            ParentFont = False
            OnClick = SpeedButton7Click
          end
          object GroupBox4: TGroupBox
            Left = 16
            Top = 20
            Width = 298
            Height = 175
            Caption = 'WebService'
            TabOrder = 0
            object Label37: TLabel
              Left = 8
              Top = 19
              Width = 121
              Height = 13
              Caption = 'Selecione UF de Destino:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object ckVisualizar: TCheckBox
              Left = 8
              Top = 129
              Width = 122
              Height = 17
              Caption = 'Visualizar Mensagem'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
            object cbUF: TComboBox
              Left = 8
              Top = 35
              Width = 275
              Height = 24
              Style = csDropDownList
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = []
              ItemHeight = 16
              ItemIndex = 11
              ParentFont = False
              TabOrder = 1
              Text = 'MS'
              Items.Strings = (
                'AC'
                'AL'
                'AP'
                'AM'
                'BA'
                'CE'
                'DF'
                'ES'
                'GO'
                'MA'
                'MT'
                'MS'
                'MG'
                'PA'
                'PB'
                'PR'
                'PE'
                'PI'
                'RJ'
                'RN'
                'RS'
                'RO'
                'RR'
                'SC'
                'SP'
                'SE'
                'TO')
            end
            object rgTipoAmb: TRadioGroup
              Left = 8
              Top = 68
              Width = 275
              Height = 52
              Caption = 'Selecione o Ambiente de Destino'
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'HOMOLOGACAO'
                'PRODUCAO')
              TabOrder = 2
            end
            object ckZeraImpostoSimples: TCheckBox
              Left = 136
              Top = 129
              Width = 148
              Height = 17
              Caption = 'Zera imposto no simples'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
            end
            object ckExpandirLogoMarca: TCheckBox
              Left = 136
              Top = 152
              Width = 148
              Height = 17
              Caption = 'Expandir logo marca danfe'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
            end
          end
          object rgFormaEmissao: TRadioGroup
            Left = 322
            Top = 20
            Width = 265
            Height = 108
            Caption = 'Forma de Emiss'#227'o'
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'NORMAL ON-LINE'
              'CONTINGENCIA'
              'SCAN'
              'DPEC'
              'FSDA')
            TabOrder = 1
          end
          object rgTipoDanfe: TRadioGroup
            Left = 322
            Top = 133
            Width = 265
            Height = 62
            Caption = 'DANFE'
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'RETRATO'
              'PAISAGEM')
            TabOrder = 2
          end
          object edtCertificado: TEdit
            Left = 21
            Top = 239
            Width = 540
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
          end
        end
        object TabSheet6: TTabSheet
          Caption = 'Caminhos XML'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ImageIndex = 5
          ParentFont = False
          object Label33: TLabel
            Left = 37
            Top = 22
            Width = 64
            Height = 14
            Caption = 'Logo Marca'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object sbtLogoMarca: TSpeedButton
            Left = 438
            Top = 35
            Width = 21
            Height = 22
            Flat = True
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
              07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
              0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
              33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
            OnClick = sbtLogoMarcaClick
          end
          object btSalvarResp: TSpeedButton
            Left = 436
            Top = 85
            Width = 21
            Height = 22
            Flat = True
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
              07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
              0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
              33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
            OnClick = btSalvarRespClick
          end
          object Label34: TLabel
            Left = 37
            Top = 112
            Width = 70
            Height = 14
            Caption = 'Arquivo XML'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object SpeedButton2: TSpeedButton
            Left = 436
            Top = 127
            Width = 21
            Height = 22
            Flat = True
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
              07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
              0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
              33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
            OnClick = SpeedButton2Click
          end
          object Label35: TLabel
            Left = 37
            Top = 154
            Width = 81
            Height = 14
            Caption = 'Carta corre'#231#227'o'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object SpeedButton3: TSpeedButton
            Left = 436
            Top = 169
            Width = 21
            Height = 22
            Flat = True
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
              07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
              0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
              33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
            OnClick = SpeedButton3Click
          end
          object Label36: TLabel
            Left = 37
            Top = 200
            Width = 79
            Height = 14
            Caption = 'Cancelamento'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object SpeedButton4: TSpeedButton
            Left = 436
            Top = 215
            Width = 21
            Height = 22
            Flat = True
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
              07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
              0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
              33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
            OnClick = SpeedButton4Click
          end
          object Label38: TLabel
            Left = 37
            Top = 243
            Width = 61
            Height = 14
            Caption = 'Inutiliza'#231#227'o'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object SpeedButton5: TSpeedButton
            Left = 436
            Top = 258
            Width = 21
            Height = 22
            Flat = True
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
              07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
              0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
              33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
            OnClick = SpeedButton5Click
          end
          object Label39: TLabel
            Left = 37
            Top = 337
            Width = 105
            Height = 14
            Caption = 'Arquivo rave (*.rav)'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object SpeedButton6: TSpeedButton
            Left = 436
            Top = 352
            Width = 21
            Height = 22
            Flat = True
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
              07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
              0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
              33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
            OnClick = SpeedButton6Click
          end
          object lbCaminhoXML: TLabel
            Left = 19
            Top = 126
            Width = 17
            Height = 23
            Caption = 'l'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clYellow
            Font.Height = -21
            Font.Name = 'Wingdings'
            Font.Style = [fsBold]
            ParentFont = False
            Layout = tlBottom
          end
          object SpeedButton8: TSpeedButton
            Left = 425
            Top = 408
            Width = 76
            Height = 26
            Caption = 'Salvar'
            Flat = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            Glyph.Data = {
              46060000424D4606000000000000360400002800000016000000160000000100
              08000000000010020000120B0000120B0000000100004F000000099B4A00F0F0
              F0008CD0AA0050AC7900D7D7D7006AD0970023B16300CCCCCC003DD27F00FAFE
              FB000BB45600A6D5BB0099B5A600E6E6E60032AA6800BADCC9000EA853001AC4
              660087E9B3005FB084007AD1A1004AD58700C5E9D500B9E0CA00F7F7F700D5EF
              E0000AA550009EC9B10069DE9C00DEDEDE0022D873003AB872000BBB5A00E1FA
              EC004FC28300ADF3CB0029C86F00BAF5D4005DD7930019AD5B003FCF7F0064BC
              8B0022BF6800EFFCF500DDEEE4000AAD530080DDA90099E4BA008BBFA20010BC
              5D00EBF7F000CDEBDB009FD7B80039C0750041D3820072DBA10029B266000AA1
              4D007ED8A60034B56E0070D19C004AB47A000BB6570024C76C00D3F8E400FFFF
              FF008DE7B5001EB561000DAC5400F5FDF9003DAC6F0086CFA7004CD58900C5ED
              D700E7F6ED002BC6700044D1830040BA76000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000414141414141
              414141410909414141414141414141416E6741411801010D1D04040707070704
              041D0D0D011809416E6741411801010D0D1D04070C0704041D1D0D0101184141
              6E6741414141414141091B032930414141414141414141416E67414141414141
              2C130512123D0F4141414141414141416E6741414141410F3D1C1C1C1C1C0E2C
              41414141414141416E67414141410B1F48483608084C4C464541414141414141
              6E674141411706241131313131313F2A29414141414141416E6741413206313E
              4427272D2020203144344141414141416E674141472D27022C41093329440A0A
              0A271941414141416E6741414D1F4A41414141414116382D2D2D3B0941414141
              6E6741410532414141414141414132221A1A1A14414141416E67414141414141
              41414141414141093C1A1A10494141416E674141414141414141414141414141
              413A1A00350941416E67414141414141414141414141414141412E39392F4141
              6E6741414141414141414141414141414141413700432B416E67414141414141
              414141414141414141414141260042416E674141414141414141414141414141
              4141414145284B416E6741414141414141414141414141414141414141211E40
              6E67414141414141414141414141414141414141414123256E67414141414141
              414141414141414141414141414141416E674141414141414141414141414141
              41414141414141416E67}
            ParentFont = False
            OnClick = SpeedButton8Click
          end
          object Label46: TLabel
            Left = 37
            Top = 289
            Width = 67
            Height = 14
            Caption = 'XML Backup'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object SpeedButton9: TSpeedButton
            Left = 436
            Top = 304
            Width = 21
            Height = 22
            Flat = True
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
              07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
              0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
              33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
            OnClick = SpeedButton9Click
          end
          object Label48: TLabel
            Left = 64
            Top = 417
            Width = 142
            Height = 16
            Cursor = crHandPoint
            Caption = 'Abrir arquivo conf (sqlNFE.txt)'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -12
            Font.Name = 'Arial Narrow'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Label48Click
          end
          object Image2: TImage
            Left = 34
            Top = 411
            Width = 25
            Height = 23
            Picture.Data = {
              0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000001800
              0000180806000000E0773DF80000000467414D410000AFC837058AE900000019
              74455874536F6674776172650041646F626520496D616765526561647971C965
              3C000001DC4944415478DAED95BD4E024110C7675534D2482131C6183F624282
              09818A0A6D7C049FC0C617B0D34434DAD8D9D9C08B58E10BD05991103B08C89D
              E7DD7127DCAE33EB9D2E72E7074AC724FFCC6E6EF8FF7667F70E26848071069B
              002680FF0594CB77454CA7217597A893607270B043B5DF9A53DD6780C8E5D686
              0AABD5074A57BEC28C1E158FAF0199CC2AD8F6CB9049ADD6FC6957CE50C54840
              3ABD0296E5488D12EDB6255B1F0948A596C134BB52A3C4D393FB35606B6B099E
              9F6DE8744CE8F5FE74C3CE10501C026C6E26C1306CB9D5DDDDF4C8EE95CA3DED
              800D01363692D06868E0BA3D2814B611640095704F00C701D5734EE2EF633553
              2DE546438F022C42BDDE96ABB72C17DBD5554C0701A1E6386EE1A2225B9448C4
              C1715CC8E753A069A6BCB28179601AB683004041E747071D7AC89469F5B4725D
              B77F6DAEEBF29ACA776100C0188352A9221616E290CDAECBDE9BA6AB1873F0BC
              0F738FCB66CB676A18469716C8FC431E003004F0B7DE3BD06C1A91E6EA79A861
              9A0EF4FBBD8BC3C3BD224EF14E08A102A66E6E6ECF63B1D9E351AFA6A6B5AE8F
              8EF62F88857A416F3E00C034839A47C55173FE7CDA173D678A1FFD90A33C5F7D
              14BDC6368A3E03FDCF00F00DA6143125B390450B05247C9008DA230DC7FD87F3
              0A0314E3E00D26F1970000000049454E44AE426082}
          end
          object Image3: TImage
            Left = 35
            Top = 448
            Width = 25
            Height = 17
            Picture.Data = {
              0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000001000
              00001008060000001FF3FF610000000473424954080808087C08648800000277
              4944415478DAA5935D48536118C7FFEFE6E6CE9C1EC96DD59C4E6AE5C98F65B1
              EC46C4E8C214CAC82EFA0021BAD210A22E92202223102203898C48C9BCB16E0A
              25EC53A10CCA596D9A8B69F8B195EEA8AC4DD3ED6CC7733AA730BCB0C87AE17D
              79F9F33C3F9EE77DFE2FC17F2E221F131D27DA298ADA2BDF797E71E6E3F06869
              61F503C75F03029D55DDC9B4AE6849E4B8A8188E44C99F121716229DE6F2E652
              F26A7B7C658A2DF3149598605D4DE9E1B9F94F7E97A79EBC3EA3F6659A340610
              45FCAA9A17056EC01B99266FCE267A98548DE55F001F7C612FE93BAFEF950036
              E939560780C80DFA22FDE4ED45F38B2DA9F1F992F20B20880ACC1A8AA1B41483
              686888D3FD500EB5408BC07202E7FECCF512679DB59D31A98A25412DAB8B0281
              37FD340CB63210251015A2489220F34116DCA36330A8A6960051CF64EC09715E
              CE69CB32C51D904A52C9AA87CF4552D12568B55AF882E370F9DEA1DC7E088410
              8CBD7F8C0DA317A08A53C80E880DB1FC7DE26A28B8956D565788E24F400F5F02
              CBCE8370B27D980DCF820DFAB1D5BC0DC6A4B558A3D603CF2B909AAC9481B161
              966F25CE3BC7EB732CBA6A88629C0CE8184B83C9568846F7157CFD16803F3809
              2B9D893C931DBBD3F6801EB9018B5E231540F81196BB46BA6E56D66E3425D648
              2DFC709E639C28D6E79529689AC64B5F179A1C8D683BD221F1450C0D38C48C68
              8FA0D3E9640B8B5F02913AC230CC3A294F1A239432203B3D41BFFF6855039365
              4B0EC466E09E1A4009B30FA16010CFEE5EBDD7DAE96A591A96B49D2BFABDF6E4
              61BBD1BAA3392D6373AE86A2109A99087BFBBBAFCF396FD79C7B28F0CB637FFB
              617615E423DD486D4AD10A2921767CB0E9A9776EA5B8EFBEB4ED190EBAA5D800
              00000049454E44AE426082}
          end
          object Label49: TLabel
            Left = 64
            Top = 448
            Width = 56
            Height = 16
            Cursor = crHandPoint
            Caption = 'Criar Pastas'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -12
            Font.Name = 'Arial Narrow'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Label49Click
          end
          object lbCartaCorrecao: TLabel
            Left = 19
            Top = 168
            Width = 17
            Height = 23
            Caption = 'l'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clYellow
            Font.Height = -21
            Font.Name = 'Wingdings'
            Font.Style = [fsBold]
            ParentFont = False
            Layout = tlBottom
          end
          object lbCancelamento: TLabel
            Left = 19
            Top = 214
            Width = 17
            Height = 23
            Caption = 'l'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clYellow
            Font.Height = -21
            Font.Name = 'Wingdings'
            Font.Style = [fsBold]
            ParentFont = False
            Layout = tlBottom
          end
          object lbInutilizacao: TLabel
            Left = 19
            Top = 257
            Width = 17
            Height = 23
            Caption = 'l'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clYellow
            Font.Height = -21
            Font.Name = 'Wingdings'
            Font.Style = [fsBold]
            ParentFont = False
            Layout = tlBottom
          end
          object lbBackup: TLabel
            Left = 19
            Top = 303
            Width = 17
            Height = 23
            Caption = 'l'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clYellow
            Font.Height = -21
            Font.Name = 'Wingdings'
            Font.Style = [fsBold]
            ParentFont = False
            Layout = tlBottom
          end
          object lbArquivoRave: TLabel
            Left = 19
            Top = 351
            Width = 17
            Height = 23
            Caption = 'l'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clYellow
            Font.Height = -21
            Font.Name = 'Wingdings'
            Font.Style = [fsBold]
            ParentFont = False
            Layout = tlBottom
          end
          object edtLogoMarca: TEdit
            Left = 37
            Top = 36
            Width = 396
            Height = 22
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object edtPathLogs: TEdit
            Left = 37
            Top = 86
            Width = 396
            Height = 22
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
          object ckSalvarResp: TCheckBox
            Left = 37
            Top = 68
            Width = 246
            Height = 15
            Caption = 'Salvar Arquivos de Envio e Resposta'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
          end
          object edtCaminhoXML: TEdit
            Left = 37
            Top = 128
            Width = 396
            Height = 22
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
          end
          object edtCartaCorrecao: TEdit
            Left = 37
            Top = 170
            Width = 396
            Height = 22
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
          end
          object edtCancelamento: TEdit
            Left = 37
            Top = 216
            Width = 396
            Height = 22
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
          end
          object edtInutilizacao: TEdit
            Left = 37
            Top = 259
            Width = 396
            Height = 22
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 6
          end
          object edtArquivoRave: TEdit
            Left = 37
            Top = 353
            Width = 396
            Height = 22
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 8
          end
          object edtXMLBackup: TEdit
            Left = 37
            Top = 305
            Width = 396
            Height = 22
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 7
          end
        end
        object TabSheet7: TTabSheet
          Caption = 'SQL'#39's'
          ImageIndex = 5
          object PageControl2: TPageControl
            Left = 0
            Top = 0
            Width = 942
            Height = 507
            Align = alClient
            TabOrder = 0
          end
          object rgSql: TRadioGroup
            Left = 17
            Top = 10
            Width = 289
            Height = 473
            Items.Strings = (
              'IDE'
              'INFADIC'
              'EMITENTE'
              'ENDERECO EMITENTE'
              'DESTINATARIO CLIENTE'
              'DESTINATARIO FORNECEDOR'
              'ENDERECO DESTINATARIO CLIENTE'
              'ENDERECO DESTINATARIO FORNECEDOR'
              'TRANSPORTE'
              'TRANSPORTA'
              'VEICULO TRANSPORTADORA'
              'VOLUMES TRANSPORTADOS'
              'PRODUTOS'
              'ICMS NORMAL'
              'ICMS ST'
              'PIS'
              'COFINS'
              'IPI'
              'COBRANCA FATURA'
              'COBRANCA DUPLICATA'
              'PROXIMO NUMERO'
              'CODIGO UF EMITENTE'
              'CODIGO CIDADE EMITENTE'
              'EVENTO CCE')
            TabOrder = 1
            OnClick = rgSqlClick
          end
          object memoSQL: TMemo
            Left = 313
            Top = 15
            Width = 614
            Height = 419
            BevelOuter = bvRaised
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = [fsBold]
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 2
          end
          object Panel6: TPanel
            Left = 321
            Top = 457
            Width = 609
            Height = 32
            BevelOuter = bvNone
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 3
            object btSalvarSql: TSpeedButton
              Left = 512
              Top = 0
              Width = 94
              Height = 27
              Caption = 'Salvar'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              Glyph.Data = {
                46060000424D4606000000000000360400002800000016000000160000000100
                08000000000010020000120B0000120B0000000100004F000000099B4A00F0F0
                F0008CD0AA0050AC7900D7D7D7006AD0970023B16300CCCCCC003DD27F00FAFE
                FB000BB45600A6D5BB0099B5A600E6E6E60032AA6800BADCC9000EA853001AC4
                660087E9B3005FB084007AD1A1004AD58700C5E9D500B9E0CA00F7F7F700D5EF
                E0000AA550009EC9B10069DE9C00DEDEDE0022D873003AB872000BBB5A00E1FA
                EC004FC28300ADF3CB0029C86F00BAF5D4005DD7930019AD5B003FCF7F0064BC
                8B0022BF6800EFFCF500DDEEE4000AAD530080DDA90099E4BA008BBFA20010BC
                5D00EBF7F000CDEBDB009FD7B80039C0750041D3820072DBA10029B266000AA1
                4D007ED8A60034B56E0070D19C004AB47A000BB6570024C76C00D3F8E400FFFF
                FF008DE7B5001EB561000DAC5400F5FDF9003DAC6F0086CFA7004CD58900C5ED
                D700E7F6ED002BC6700044D1830040BA76000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000414141414141
                414141410909414141414141414141416E6741411801010D1D04040707070704
                041D0D0D011809416E6741411801010D0D1D04070C0704041D1D0D0101184141
                6E6741414141414141091B032930414141414141414141416E67414141414141
                2C130512123D0F4141414141414141416E6741414141410F3D1C1C1C1C1C0E2C
                41414141414141416E67414141410B1F48483608084C4C464541414141414141
                6E674141411706241131313131313F2A29414141414141416E6741413206313E
                4427272D2020203144344141414141416E674141472D27022C41093329440A0A
                0A271941414141416E6741414D1F4A41414141414116382D2D2D3B0941414141
                6E6741410532414141414141414132221A1A1A14414141416E67414141414141
                41414141414141093C1A1A10494141416E674141414141414141414141414141
                413A1A00350941416E67414141414141414141414141414141412E39392F4141
                6E6741414141414141414141414141414141413700432B416E67414141414141
                414141414141414141414141260042416E674141414141414141414141414141
                4141414145284B416E6741414141414141414141414141414141414141211E40
                6E67414141414141414141414141414141414141414123256E67414141414141
                414141414141414141414141414141416E674141414141414141414141414141
                41414141414141416E67}
              ParentFont = False
              OnClick = btSalvarSqlClick
            end
            object ckQuebraLinha: TCheckBox
              Left = 8
              Top = 8
              Width = 185
              Height = 17
              Caption = 'Quebra autom'#225'tica de linha'
              Checked = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              State = cbChecked
              TabOrder = 0
              OnClick = ckQuebraLinhaClick
            end
          end
        end
      end
    end
  end
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 958
    Height = 55
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 1
    DesignSize = (
      958
      55)
    object lbnomeformulario: TLabel
      Left = 460
      Top = 13
      Width = 172
      Height = 28
      Caption = 'NF-e digitada'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -23
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlBottom
      WordWrap = True
    end
    object lbCodigo: TLabel
      Left = 772
      Top = 14
      Width = 129
      Height = 27
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = '0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -25
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlBottom
    end
    object bt1: TSpeedButton
      Left = 902
      Top = -17
      Width = 53
      Height = 80
      Cursor = crHandPoint
      Hint = 'Ajuda'
      Anchors = [akTop, akRight]
      Flat = True
      Glyph.Data = {
        4A0D0000424D4A0D0000000000003600000028000000240000001F0000000100
        180000000000140D0000D8030000D80300000000000000000000FEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFAF6FBF9F2FEFEF4FFFFFAFF
        FCFDFFF9FFFEFEFDFCFFFFFEFEFFFDFEFCFEFEF8FFFFFBFEFCFFFFFCFFFDF6FB
        FFF7F8FFF7FBFFFBFBFDF7FFFFFCFFFFFEF7FDFCFBFFFFFFFBFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFF9FD
        F8FDFFFFFDFBFFFFFBFFFFFDFEFFFCFCFFFEFFFEFDFFFCF9FBF0EFF1F0EBEDF9
        F1F2F6F8F9F1FFFFF4FFFFFEFCFFFFFDFFFFFAFFFBFCFFFEFDFFFFFAFDFAFCFD
        F7FFFFFFFCFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFFFCFFFFFCFEFFFDFCFFFFFFFFFDFFF2E4E6DEBCBCCF
        9F99EB8688E68081EA7E7DF68686FE9092FDA2A5F7C3C3FFF0EDFFFAFFF8FFFF
        FFFDFFFFF5FDFCFDFFF6FFFFFBF8FAFFFDFEFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFBFFFCFEFFFBFFFCE7
        E1DCC29E9EBF7579DC7677F07B76FF8783FF8F88FD9388FB8F84FF837BFF7977
        F77576F98084FBB6B9F2F2ECE8FFFAF8FDFCFFFEFFFAFCFDFFFCFEFCFEFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFEFDFFFAFCD7B8BBB17879C87772F08E88FE9893F58E8CE7797DD76F70
        D26867DC716EE88581F69A95F99591FE8686FF7173F28484FFD9D4FFFFFBFDF8
        F7FFFCFFFFFEFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFF9B99D9CA76B6CDA8786F99B96E17D79
        B6484C98222FAB1122AA0F1EA5131FA41D25AA2226C53938E6625BFF9085FD9B
        91FF7B7CF2777BF4C8C7F8FFFEF9FEFDFFFEFFFFFBFCFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFC0A0A1A7706D
        E6928CFC9691B3484A87192589112398182BA32E31DC6F71EB8082CC5050BB28
        26D32A22EB2F24FE3727F36D69FF9D97FB7D83F7707EFEC5CDFFFEFFFFFDFDED
        FFFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFCFBFDFFFDFFFFFBFFFAFFFFF2FAFA
        FFFEFEDCBDBEA66D6BDB9092ED909996303C770E1B8D1E2C9B1C2BA11425D96F
        75FDA8B0FFA2B0FFA0AAC83F43BC2F26D73927FF3929FF2B1CEC5650FF9996FE
        7D7AFD7F7EFADFDBFFFEFFF4FEFEFEFDFFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFF
        FDF9FEFFFDFFF7FAFEFAFFFFF4E6E7A67F81CF8B8CEC9A9F92323C7B121D9123
        2F921C27A5232EA71C27DE837EFFB7B5F9A9AEFDB0B3BF4343B42821C9372BDA
        3326E33626CB2728CA5556FF9893FB7777DE9A9BFFFEFEFFFCFDFEFDFFFEFDFF
        FEFDFFFEFDFFFEFEFEFEFDFFFFFBFFFFFBFFFAFBFFFFFFFFC3AEADA77678F8A4
        A9A9515776131B8D202898242BA3282CA72629A82627B55047EBADA2FFBCB5CD
        7974A92E2AA13026A5372BBD332DBF3528BD2B2DA51E26DF7372FF918FE27377
        F0CCCCFFFFFEFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFAF9FBFFFDFFFFFAFFFFFE
        FFF5F0EFA38384DB9FA0E78A91791D22852428992E309E2827A72C28AC3028AB
        3229A3271FA13E36B4413E9E332CB03129A73126A23127B0322EA93026AD3131
        A91925B63239F2948FFD7C7FE9989BFBFFF9FEFDFFFEFDFFFEFDFFFEFDFFFFFE
        FEFCFBFDFFFEFFFFFDFFFFFEFFDACECEAE8687F0ABAEB156597E24248B2C2992
        2A25A93730A93328AE3B2EA33222D35E59EA928BF28C87E27D74AC362BA93A2A
        A23729A5342AA3342CA62728B02931A71821D8716FFF8A8ADE7D7FEFECE7FEFD
        FFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFFFDFCFEFFFCFEFFFEFFC0B3B1BF9392F0
        A5A78F3B35842A2396352B9F372AA93C2EAC3D2DAC3C2AA1311FD16D69F8CABF
        FBC0B7FCABA3AA3A2EA23827A33928A9372AA6352BAE302BA72C2EAA1922C550
        51FF9692DC7475E0D2D3FFFEFFFEFDFFFEFDFFFEFDFFFFFDFEFEFDFFFEFDFFFF
        FDFFFFFEFEB6A9A7CB9D9CEC9E9F8335288E3527A0392AAD4130A83C2AA73B29
        A93927AE3827CE6C66FFCDC1FABAB5FFBFB6D46B62AD3126B43427B13A2BA933
        27A93327A02A25AC1F28AF373BFC9D94E7797BD1B8BCFFFEFFFFFEFFFEFDFFFE
        FDFFFFFDFEFCFCFCFDFCFEFFFEFFFDFBFBB5A8A6DBAEABE19192813522983C29
        A73B29AF3E2AA83B26AA3F2AAD3B2AAF3224B05A4EFFE0D0FFCEC6F7BEB5FFBE
        B3DE8C7BA44434A33628AD3C2CAD3726A8342DA7262DAE2E33FA9B92E27A7BD4
        ABB3FFFEFFFFFEFFFEFDFFFEFDFFFBFCFFF9FEFFFFFCFDFBFFFFFCF9FBC9AAAB
        DEB1ADDF9A91813628B03C2BAB3D25A7412AA6362AB3362EA13726AE3C25A833
        24D79987FFF4E4F9D3CEFEBEBEFFC9C3EC9E91AD4436B137299C3B27AA3526B1
        2F28A4302FF7969AE67F86C7AAA5FFFEFFFFFEFFFEFDFFFEFDFFFAFBFFFCFEFF
        FFFEFFFCFEFFFCFEFFCEB4B4E6B6B2E6A99F98392AA13426B2382C9E3E2EAA3E
        33AD4338A74539A24536AB443B9B3D38CF8A87FFEBE6FEDED9F1C0B8FFC9BDEC
        8B81AA372AA73B2AA83F2CA52B1FA6403BF9A1A1E57E86C9B3AEFFFEFFFFFEFF
        FEFDFFFEFDFFFAFDFFFDFCFEFFFEFFFDFCFEFAFFFFD3BEC0ECB5B2F5C2B8AB40
        329F3D31B84D49A24F47AD554EAE574DAE5D55B1655FAD5654B34E50A13B40B9
        7B7BFFE1DBFED7CFF9CAC2FFBCB5BD5146AD3627A3402AA83526AD544AFEACAB
        D5787FD4C7C5FFFEFFFEFDFFFEFDFFFEFDFFFBFFFFFFFBFDFFFDFDFFFDFFF9FE
        FFDFD1D2E7B1B0FFDACEC26B61A74C45A55C54A85A54D3958FF2B8B3F9C2BFF4
        B4B3AD5352AB5C59AC5553A54343F7C0BBFDE0D9FFCCCAFBC9C9CC6A60B03429
        9F3923A63624C9776BFFADACC67E84E4E0DFFEFDFFFEFDFFFEFDFFFEFDFFFAFE
        FFFFFCFFFCFCFCFFFEFFFBFEFFF4ECEDE2B7B4FFDBD2DAB0ABA6504AA86153AB
        514AD59594FFF8F7FEF1EFFAD4D2C469649C5148A85950A14B45ECBBB3FBE1DA
        FFCED0FFCFD6D06E66AB352AAD3B2A9F3726F7A89DFBA4A7BB8E91F2F7F6FEFD
        FFFEFDFFFEFDFFFEFDFFFCFBFDFFFEFFFCFEFEFEFDFFFDFCFFFFFEFFE2C7C3F3
        CAC1EDDEDCBD7672AD5144CE6459D7797AF6F5F1F4FFFEF9F3EEE9B4AAB16860
        B45E58C99087FAD7CDFFDCD6F8D5D2FECBCFBE5A55A23127A12F22CB7264FFC3
        BBD28B8ECFB7B9FAFFFFFEFDFFFEFDFFFEFDFFFEFDFFFEFBFDFFFFFFFCFEFEFB
        FDFEFFFAFFFFFEFFEBE5DEF2C4BDFFE5E8EFC8C0AE6053E17164F47773F0BBB8
        FDFEFAFDFFFFFFF8F1FCD6D4FFCDCCFFDDD8FFDED8FFE0DAFFDDD7E7A1A2A339
        32A7352EAC4A3EF8B8ADECB2ADB68E90FAECEEFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFDFFFCFCFCFAFFFEFAFDFFFFFBFFFFFDFDFBFFF9FFCFC9FFC7CCFFF9EF
        E8BFB0D66D5FFF7C70F9807EEACECDFFFBFFFDFCFFF9FEFFFDFFFFFFE4E5FFE2
        DFF6E7DEEDB6AFB34648A1312BB03E37FCAA9FF7CDC1BC8F8CD5C7C8FFFDFFFC
        FEFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFFFEFFFFFEFF
        FDFCFEFCFBFDFCC8C8F6D7D6FFFCF6F0C9C1E57F7AFF7673F98481ECA5A1E6D6
        D7FCF1F3F9F1F1ECD7D9F0C1C3D78A8DA84646A03232A54B4BECABAAFFD1CFC9
        A09EC9ADACFEF9F8FBFFFFFCFCFCFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFF
        FDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFDFCFEFFF4EEF8CDCAFDD8D4FFFBF4FDE6
        E4FBA1A6FB7375FD6F68EA6E5CCE6855B76958AE6458A5514B993E3AA04D4BC8
        7F7BFCC3C1FFCBCCDAA4A4BFAEABF3F9F4FFFFFEFFFCFDFCFEFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFEFDFFFFFEFFF4FF
        FFFBF2EFF6D6D1FDD9D3FFEFE8FCFBF1FFE6DCF6BBB2EE9285D07C70B76F65B2
        6F66C0807BDDA39EF3C1BBFFD6CFF6C6C5D4ACADCEBBB8F9FAF6F7FFFDFAFCFC
        FFFEFFFCF9FBFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFC
        FEFDFCFEFEFDFFFFFEFFFFFAFCFBFFFFF9F9F9FCE0DFFFD3CCFEDDD4F8ECE6F8
        F6F5F5F8F6F4EFEEF6E4E5FADBDCFFDBDCFFD9D8FBCAC8F0C0BCD7B5B6E1D8D5
        FAFFFEFFFFFFFEFCFCFAFFFFF8FDFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFFFFFBFFFBFCFDFDFFFD
        FBFFFFEEF1FFE2E1FFD3D2FFD0D2FFD2CFFFD1CDFFD2CFFFCFCFFFC9C9F2BFC3
        E9C4C6EED6D8F5F7F7FFFBFCFFFEFFFFFCFEFDFFFFF9FCFFFCFBFFFFFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFFFEFDFFFEFDFFFDFCFEFDFCFEFD
        FCFEFFFBFFF0FDFBF1FFFBFFFEFFFFFDFFF9FEFFF9FCFAFEF2EEF9EAE1F4E5DC
        F3E3DCF3E3DDF6E7E5FCF1F3FEFAFFF9FCFFF4FEFEFFFEFFFFF7FAFFFDFFFCF9
        FBFFFDFFFFFCFFF5F8FCFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFF7F9FFFDFEFFFFFBF8FFFDF9FDFBFAF1FBFB
        F6FFFFFBFDFEFBFEFFFFFCFFFFFDFFFFFEFFFBFFFFF8FFFFFDFEFFFFFAFFFFFD
        FFFAFEFFF8FFFFF9FBFCFFFDFFFFF9FCFBFAFCFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFCFFFEFF
        F7FDFFF5FEFFFBFFFFFFFFFEFFFCFDFFFCFFFFFAFFFFFCFFF9FEFDF9FFFDFBFF
        FEF9FCFAFBFBFBFFFFFFFFFCFEFEFDFFFBFAFCFBFFFFF5FDFDF7FFFFFDFFFFFF
        FCFFFEFDFFFEFDFFFEFDFFFEFDFF}
      ParentShowHint = False
      ShowHint = True
      OnClick = bt1Click
    end
    object Btnovo: TBitBtn
      Left = 1
      Top = -3
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 51
      Top = -3
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 101
      Top = -3
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = -3
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = -3
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btexcluirClick
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = -3
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = -3
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object btOpcoes: TBitBtn
      Left = 351
      Top = -3
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btOpcoesClick
    end
    object BtSair: TBitBtn
      Left = 401
      Top = -3
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = BtSairClick
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 626
    Width = 958
    Height = 56
    Align = alBottom
    Color = clSilver
    TabOrder = 2
    DesignSize = (
      958
      56)
    object imagemRodape: TImage
      Left = 1
      Top = 1
      Width = 956
      Height = 54
      Align = alClient
      Stretch = True
    end
    object lbl8: TLabel
      Left = 12
      Top = 7
      Width = 84
      Height = 16
      Caption = 'Produtos..: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbTotalProdutos: TLabel
      Left = 94
      Top = 7
      Width = 91
      Height = 16
      AutoSize = False
      Caption = '0000000,00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label11: TLabel
      Left = 191
      Top = 30
      Width = 84
      Height = 16
      Caption = 'NF............:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbtotalNota: TLabel
      Left = 278
      Top = 30
      Width = 91
      Height = 16
      AutoSize = False
      Caption = '0000000,00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label17: TLabel
      Left = 510
      Top = 7
      Width = 57
      Height = 16
      AutoSize = False
      Caption = 'Frete..:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbTotalFrete: TLabel
      Left = 568
      Top = 7
      Width = 82
      Height = 16
      AutoSize = False
      Caption = '000000,00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 38144
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label21: TLabel
      Left = 192
      Top = 7
      Width = 91
      Height = 16
      AutoSize = False
      Caption = 'Desconto..:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbTotalDesconto: TLabel
      Left = 278
      Top = 7
      Width = 89
      Height = 16
      AutoSize = False
      Caption = '0000000,00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 38144
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label24: TLabel
      Left = 822
      Top = 7
      Width = 114
      Height = 33
      Cursor = crHandPoint
      Hint = 'Gera a nota fiscal eletr'#244'nica e imprime o DANFE'
      Anchors = [akTop, akRight]
      Caption = 'Gera NF-e'
      Font.Charset = ANSI_CHARSET
      Font.Color = 38144
      Font.Height = -29
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      PopupMenu = menuNFE
      ShowHint = True
      OnClick = Label24Click
    end
    object Label13: TLabel
      Left = 382
      Top = 7
      Width = 41
      Height = 16
      AutoSize = False
      Caption = 'IPI..:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbTotalIpi: TLabel
      Left = 423
      Top = 7
      Width = 82
      Height = 16
      AutoSize = False
      Caption = '000000,00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 38144
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label16: TLabel
      Left = 8
      Top = 31
      Width = 78
      Height = 16
      AutoSize = False
      Caption = 'ICMS ST...:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbTotalICMSST: TLabel
      Left = 91
      Top = 31
      Width = 87
      Height = 16
      AutoSize = False
      Caption = '0000000,00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 38144
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object dsProdutos: TDataSource
    Left = 824
    Top = 399
  end
  object menuItem: TPopupMenu
    OnPopup = menuItemPopup
    Left = 800
    Top = 422
    object Servios1: TMenuItem
      Caption = 'Servi'#231'o'
    end
    object Produto1: TMenuItem
      Caption = 'Produto'
    end
  end
  object menuNFE: TPopupMenu
    Images = il2
    Left = 856
    Top = 398
    object Consultarservio1: TMenuItem
      Caption = 'Consultar servi'#231'o'
      ImageIndex = 44
      OnClick = Consultarservio1Click
    end
    object Consultardoarquivo1: TMenuItem
      Caption = 'Consultar do arquivo'
      ImageIndex = 42
      OnClick = Consultardoarquivo1Click
    end
    object Consultapelachavedeacesso1: TMenuItem
      Caption = 'Consulta pela chave de acesso'
      ImageIndex = 43
      OnClick = Consultapelachavedeacesso1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Recuperaxmlduplicidade1: TMenuItem
      Caption = 'Recupera xml duplicidade'
      OnClick = Recuperaxmlduplicidade1Click
    end
  end
  object PopUpMenu2: TPopupMenu
    Left = 896
    Top = 394
    object NFeNova1: TMenuItem
      AutoCheck = True
      Caption = 'NF-e Nova'
      Checked = True
    end
  end
  object il2: TImageList
    Left = 840
    Top = 437
    Bitmap = {
      494C01012D003100040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000D0000000010020000000000000D0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D5D5D500C6C6C600B6B6
      B60081869000043464002F373A002F373A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C2C6
      CE0004346400053F7A00DDE1C0002F373A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B9B6B1008B877E007E796D007F796F008D897F00BFBDB700C5CDD500013E
      7B00053F7A001764A5009FD7F400135C9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D1CFCC00706A
      5E007B6C5900A98B6D00C8A27B00CAA47D00AB8F70007F705C005A585200273D
      5C0004326100CAF3FD00135C9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0CDCB00635D5000C09F
      7C00FDDFAB00FEFADE00FFFFF300FFFFF200FEF2DF00FED4A100B9997600635D
      5100273D5C00135C9C00C9D0DB00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000006C675B00C8AA8400FEEC
      B900FFF6C300FEEDB400FDE9AF00FEEBB200FEF1BD00FEF0C100FEE0AF00B191
      71005B595100C5CBD30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADA9A20091846B00FDDEAD00FCDD
      AD00FCD69F00FCD68E00FCD99200FCD88F00FBD7A200FCD8A600FCD8A600FDD8
      A900806F5B00B5B3AE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000807C7000C2A17D00FBCB9A00F9BD
      8900F9C19000FACC9600F9CE9100F8CC9000F8C59300F8C39100FAC49100FDC8
      9600C599770089837A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006A645600D8B08900F9BA8800F4B6
      8300F1B27E00EFB38200F0B58300EEB28000EEB07E00F0B28000F6BC8900F9B9
      8700DEAD8500706A5E0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000635D5000D6AA8400F8B98500EFAE
      7B00ECAA7800ECAA7800ECAA7800ECAA7800ECAA7800ECAA7800EEB07D00F5B5
      8200DAA67E006E695D0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000077726700BC987800F5B58300F3B4
      8100EEAC7A00ECAA7800ECAA7800ECAA7800ECAA7800EDAB7800F0B07D00F0AF
      7F00BE987600807C710000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A3A098008E7F6800E9A87B00F9BC
      8800F6B88400F2B37F00F1B07D00F1B17E00F3B38000F4B68200F4B78400E09E
      75008F7E6600ADA9A20000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E6E400716D5C00AC846600EAAA
      7C00FBCF9E00FFFCF100FFFFFF00FFFFFF00FFFBEF00F8C39100DD9F7400A27D
      6100756F60000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B9B6B100665F5100AD84
      6600E7AC8100F8E0C700FFFDFB00FEFCFB00F6DCC100D8A07800A77F6200675F
      5200C6C5BF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C4BF006A66
      59007E6B5800AC836500C5906D00C38E6B00A98063007D6A57006E695D00CCCA
      C500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009E9A92006E695D00635D5000635D5000706A5E00A29F9600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000069A5
      65003585320069A5650000000000000000000000000000000000000000000000
      00000000000000000000000000006ABAC70038A1B30038A1B30038A1B30038A1
      B30038A1B30038A1B30038A1B3006ABAC7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003C9A
      32002BDF1A003C9A320000000000000000000000000000000000000000000000
      000000000000000000000000000063BFCD0034CBDB0029DBE90028DAE90079ED
      F50028DAE90028DAE9002ECADA0063BFCD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D3D3D3006C6C6C0061616100D2D2D2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000414140009B9B9800000000000000000000000000000000000000
      000000000000000000000000000000000000000000006CB965003DA132003DA1
      32003DE22C003DA132003DA132006CB965000000000000000000000000000000
      0000000000000000000000000000C4E7EC0047BECD0042D9E00033D5DD000000
      000033D5DD0034D5DD0041BCCD00C4E7EC00BABABA007D7E7E00828282008282
      82008282820082828200808080007F80800080828200828282007A7B7B008F8F
      8F00616161001B1B1B002B2B2B00ACACAC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000414141007B7A7A00FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000039A42D0052E7410052E7
      410052E7410052E7410052E741003DA831000000000000000000000000000000
      0000000000000000000000000000000000005BB8C60064D3DE0046D3D70075B4
      B50040D0D20045C9D00063C2D00000000000B3B3B300F4F5F600E8E9E900E6E7
      E700E0E1E100D2D2D300C5C5C500C3C4C400CACBCB00D7D8D800CDCECE003D3D
      3D00191919004141410031313100D6D6D6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004343
      42006F6F6D00F1F0EE0000000000000000000000000000000000D8D8D800C6C6
      C600ABABAB008F8F8F008080800078787800787878003A842F0029981B002F9F
      210066EB550038A72A003CAB2E006EC164000000000000000000D8D8D800C6C6
      C600ABABAB008F8F8F008080800078787800577E820042AEBE007CE4F1000000
      000068D8E7004ABDCD00AECACE0000000000B5B5B500F3F4F400C3C4C400B7B8
      B800959696007B7B7B0071717100707070007474740081818100636464001818
      1800404040002A2A2A00CECECE0000000000B4B4B400B4B4B400B4B4B400B4B4
      B400B4B4B400B4B4B400B4B4B400B4B4B400B4B4B400B4B4B400343434005F5D
      5D00DCDAD900B6B6B600B4B4B400B4B4B4000000000000000000C5C5C5008484
      84008B8B8B00B5B5B500909090005D5D5D005D5D5D0090909000B5B5B5002B9E
      1C0075EE640036A9260000000000000000000000000000000000C5C5C5008484
      84008B8B8B00B5B5B500909090005D5D5D005B5E5E00349BAA0090E7F0005555
      55007FDCE6004BB4C3000000000000000000B5B5B500F7F8F800D2D2D3009D9D
      9D0092929300D2D2D300FCFDFF00FFFFFF00F0F1F300B1B1B200363635002222
      22002C2C2C00D1D1D1000000000000000000B4B4B400FBF9F900FBF9F900FBF9
      F900F4F2F200DCDADA00EDEBEB00F9F7F700FBF9F90048474700504F4D00DCDA
      D900FBFAFA00FBF9F900FBF9F900B4B4B40000000000A5A5A5007F7F7F00C9C9
      C900E7E7E700A2A2A200645D5D009E8484009E848400645D5D00A2A2A20069C2
      5D0038AF290040923500A5A5A5000000000000000000A5A5A5007F7F7F00C9C9
      C900E7E7E700A2A2A200645D5D009E8484009E8484004B7980005EC1CD00ABF5
      FC0065CCD80059898F00A5A5A50000000000B5B5B500F9FAFA00A1A0A0009493
      9400F2F1F200DFD8D200B6A99600AC9C8400C4B9AB00F0EDEB00B9B8BA004848
      4800CBCBCB00000000000000000000000000B4B4B400FBF9F900253A38004851
      50008C7F7800A288800097857D0076787600151C1A005B5B5A00CACFCD007396
      8A0035554E0042534F00FBF9F900B4B4B400BEBEBE007E7E7E00E1E1E100EAEA
      EA00EAEAEA0064646400816F6F007A6767007A676700816F6F0064646400EAEA
      EA00EAEAEA00E1E1E1007E7E7E00BEBEBE00BEBEBE007E7E7E00E1E1E100EAEA
      EA00EAEAEA0064646400816F6F007A6767007A6767007E7172002795A400A6EE
      F4005AC8D700D9DFE0007E7E7E00BEBEBE00B5B5B500F5F5F5009C9C9D00E3E3
      E400D5D0C700B09C7E00CAB08700D2B88D00C1A88300B3A49000F2F0EE00A6A6
      A600CDCECE00000000000000000000000000B4B4B400E3E2E20095878200FFFD
      EA00FFFFFF00FFFFFF00FFFFFF00FFF4DF00958C8800494047002C2334006750
      74003F313A005D4D6200FBF9F900B4B4B4005F5F5F0094949400F0F0F000F0F0
      F000F0F0F00043434300BAB2B200352E2E00352E2E006D5F5F0043434300F0F0
      F000F0F0F000F0F0F000949494005F5F5F005F5F5F0094949400F0F0F000F0F0
      F000F0F0F00043434300BAB2B200352E2E00352E2E006D5F5F002A6D770050C7
      D80096D9E300F0F0F000949494005F5F5F00B5B5B500EFEFEF008E8D8E00FEFE
      FF00B6AB9B00C1A78200D2B48700D1B38700CDB18400AA937000C9C3BB00E4E5
      E700ACACAC00000000000000000000000000B3B3B3007E7C7C00FFF9E500FFFE
      FB00F1F1E000DFE0E400F7F7F700F1F2F200E4C7AD00686059006E665D00534E
      73005859890040456B00FBF9F900B4B4B400B7B7B70073737300E7E7E700F6F6
      F600F6F6F600BDBDBD00FFFFFF009E9B9B00413C3C004C4646005C5C5C00F6F6
      F600F6F6F600E7E7E70073737300B7B7B700B7B7B70073737300E7E7E700F6F6
      F600F6F6F600BDBDBD00FFFFFF009E9B9B00413C3C004C4646005C5C5C00F6F6
      F600F6F6F600E7E7E70073737300B7B7B700B5B5B500F0F0F0009F9F9F00FFFF
      FF00CCC5BA00E7D8C100E7D0AD00DFC59D00D4B99000A68D6900AFA79B00FFFF
      FF00A7A7A700000000000000000000000000ACACAC00A0938F00FFF8F300FFF5
      E500FFF5D100C7C8D600EFF1F500B6A09F00FFF8EA00A8968700EAE6AB00FFD6
      9900B69A76004C443A00FBF9F900B4B4B400000000008D8D8D0060606000CBCB
      CB00FBFBFB00A6A6A6009B9B9B00383737003837370038383800A6A6A600FBFB
      FB00CBCBCB00606060008D8D8D0000000000000000008D8D8D0060606000CBCB
      CB00FBFBFB00A6A6A6009B9B9B00383737003837370038383800A6A6A600FBFB
      FB00CBCBCB00606060008D8D8D0000000000B9B9B900F6F6F60095949400FFFF
      FF00C5BFB600E8E3DA00EDE4D700E8DCC900D3C3AC00A5988300C4BFB800E7E8
      E900B8B8B8000000000000000000000000009D9D9D00AD9E9A00FDEFE300FCE5
      D000FFEAD000FCE9D500B8BCCD00F3DAC800FFF7E500A5846F00DCA77D00FFCD
      9500FFE09F00FFDE9800FBF9F900B4B4B4000000000000000000AFAFAF004E4E
      4E0066666600ADADAD007F7F7F0042424200424242007F7F7F00ADADAD006666
      66004E4E4E00AFAFAF0000000000000000000000000000000000AFAFAF004E4E
      4E0066666600ADADAD007F7F7F0042424200424242007F7F7F00ADADAD006666
      66004E4E4E00AFAFAF000000000000000000BBBBBB00FFFFFF00BABABA00F1F1
      F100DCD8D300D1CCC500E8E4DD00E2DED500C9C3BB00B8B1AA00F4F1F000B9BB
      BB00DADADA00000000000000000000000000ACACAC008E868400FCEDE100F8DD
      C400F8DDC300FFE4CB00FFF0E000FFECDA00F9DEC8008C6E5800DFA97800F2B4
      7E00F3B47F00F6B27A00FBF9F900B4B4B4000000000000000000000000000000
      0000B0B0B0006A6A6A00484848003636360036363600484848006A6A6A00B0B0
      B000000000000000000000000000000000000000000000000000000000000000
      0000B0B0B0006A6A6A00484848003636360036363600484848006A6A6A00B0B0
      B00000000000000000000000000000000000BFBFBF00FFFFFF00B8B7B700ADAC
      AC00FEFEFE00DEDAD500D5D1CB00DBD9D600CEC9C300F2F1EE00E4E4E400BCBC
      BC0000000000000000000000000000000000B3B3B3009A989800E7C5AF00FBEE
      E400F6D7BE00F6DBC600FAE1CD00F7DBC500F9E3D3007E685800EBA87100ECA7
      7200EDAB7100ECA56B00FBF9F900B4B4B4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C3C3C300FFFFFF00FBFBFB00D8D8
      D800C0C0C000F0F0F000FFFFFF00FFFFFF00FBFBFB00CBCBCC00C1C1C1000000
      000000000000000000000000000000000000B4B4B400F3F0F00081614D00FDF7
      F300FDF7F300F3D7C300F6E0D100F5E0D100795A4600C48B6200E1996700E29A
      6600E49D6800E0956200FBF9F900B4B4B4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C600FFFFFF00FFFFFF00FFFF
      FF00EFEFEF00CFCFCF00B5B5B5008F8F8F00C1C1C10000000000000000000000
      000000000000000000000000000000000000B4B4B400FBF9F900EDEBEB00A8A6
      A60083808000908E8D008F8B8B008A898A00DAD7D700FBF9F900FBF9F900FBF9
      F900FBF9F900FBF9F900FBF9F900B4B4B4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C8C8C800BCBCBC00BDBDBD00BDBD
      BD00BDBDBD00BEBEBE00B2B2B200E4E4E4000000000000000000000000000000
      000000000000000000000000000000000000B4B4B400B4B4B400B4B4B400B3B3
      B300AEAEAD00A3A2A200ACACAC00B2B2B200B4B4B400B4B4B400B4B4B400B4B4
      B400B4B4B400B4B4B400B4B4B400B4B4B4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006969690000000000000000000000000000000000E0E0E000EEEEED00EFEF
      EF00F0F0F000F1F1F100F2F2F200F2F2F200EEEEEE00E4E4E400C9C9C9000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C9493007D6B6900735E5A00715E5600725C
      560072625E00827A7800BFBFBF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000008080800797979000000000000000000DDDDDD00F3F3F300F8F8
      F800F9F9F900FAFAFA00FCFCFC00FCFCFC00F6F6F600E8E8E800D0D0D000C5C5
      C5000000000000000000000000000000000000000000848E840052675200475E
      4700485E480066665A0091655400D0A59900E7C7B600EED3BD00F0D1B400EEBE
      9800E7AD8500C88A67007C4D3700847F7D00000000006C4E31006C4E31006C4E
      31006C4E31006C4E31006C4E31006C4E31006C4E31006C4E31006C4E31006C4E
      31006C4E31006C4E31006C4E3100000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B3B3B300000000000000
      00000000000000000000000000000000000000000000DDDDDD00F3F3F300F8F8
      F800F9F9F900FAFAFA00FBFBFB00FCFCFC00F8F8F800EDEDED00D2D2D200E8E8
      E800C4C4C400000000000000000000000000448B480076C078009BDC9A008AE3
      910072E089009CA55C00FFB28100FFFFFF00FFFFFF00FFFFF600FFF7E000FFCA
      9600FFC08700FFCE9300FFA76F0076564E00000000006C4E3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006C4E3100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B3B3B3000000
      000000000000050505008D8D8D000000000000000000DDDDDD00F2F2F200F7F7
      F700F4F5F3008CB9800080B3720080B371007FB170007BAE6C0073A56300709E
      6000D2D2D100C5C5C500000000000000000030BF4800C1FFCE00E6FFE40077E7
      950056E789006EC36E00DC672B00FFBFA200FFF4E100FFE2C000FECB9D00F488
      4A00F0783A00F87F4200CB562700A39A9600000000006C4E3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006C4E3100000000000000000000000000000000000000
      000000000000000000000000000000000000B3B3B30000000000000000000000
      00007979790000000000000000000000000000000000DDDDDD00F2F2F200F6F6
      F600EBEFEA0048AB460038AD410038AD410037AC400037AC3F0037AC3E0036A0
      2D00E4E5E400EAEAEA00DFDFDF000000000015A22F00AAE9B700E0FEDE003FBF
      610016B0440018B74C008F652100F7836000FFE4C200FFD19700F9AA6100DE62
      2C00DB5A2800E95F2A00863D280000000000000000006C4E3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006C4E3100000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B3B3B300000000000000
      00000000000000000000000000000000000000000000DDDDDD00F1F1F100DDE6
      DC0077B9790040B452003EBD5D003EBC5D003EBC5C003EBC5C0043BB5C004FB1
      5300F6F6F600F6F6F600DDDDDD0000000000149A2A00AEE7B800E5FDE4003EB5
      5B0016A43B0017B048003C852900D0592D00FCCCA800FFDBA300F6A76300D557
      2700DB552700C1431D009485820000000000000000006C4E3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006C4E3100000000000000000000000000000000000000
      0000000000000000000000000000B3B3B3000000000000000000000000000000
      00000000000000000000000000000000000000000000DDDDDD00F0F0F00097CC
      9B0049CD750040CA6E003ECA6D003ECA6D003EC96D003EC86B007FC17D00BDDE
      BA00FCFCFC00FAFAFA00DDDDDD000000000011912400B0E4BA00ECFDEA003CAE
      5400129A30001BA43E000D8D250080350700F69B7900FFE0A700F29F6000CB49
      2100D6482100843322000000000000000000000000006C4E3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006C4E3100000000000000000000000000000000000000
      000000000000000000000000000000000000B3B3B30000000000000000000000
      00000000000000000000000000000000000000000000DDDDDD00EFEFEE009FD2
      A600A9E8BC0095E4AD0079DF9A005FD986004AD477003CD06A0076C07C0098CD
      9B00FAFAFA00F9F9F900DDDDDD00000000000F8A1D00B2E0BA00F2FCF10038A5
      4D000C8F2400189B32000285200053311C00D3594100FFDBA000EC995D00C23A
      1A00B8311500866F6D000000000000000000000000006C4E3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006C4E3100000000000000000000000000000000000000
      00000000000000000000B3B3B300000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DDDDDD00EEEEED00E6ED
      E7009FD3A7008AD59900A1E3B1008ADD9E0073D68B005FD17B004BC968003ABA
      5200F5F7F500F8F8F800DDDDDD00000000000F811600B4DDBB00F9FEF800349B
      44000583170013902700057F19006A675B00AA382F00FDBC8400E6955D00BD2B
      12007F221500000000000000000000000000000000006C4E3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006C4E3100000000000000000000000000000000007979
      7900000000000000000000000000B3B3B3000000000000000000000000000000
      00000000000000000000000000000000000000000000DDDDDD00ECECEC00EFEF
      EF00E8EDE90093D5A000C6EBCB00B0E3B70098DBA20083D48F006FCE7D0051BE
      6200F5F6F500F7F7F700DDDDDD00000000000D760F00B8D7BB00FFFDFE003993
      4200087812000F7F1A00017311007A796C00CE8E8E00E4855800E7925D00A516
      080080676700000000000000000000000000000000006C4E3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006C4E310000000000000000008D8D8D00050505000000
      000000000000B3B3B30000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DDDDDD00EBEBEA00EEEE
      ED00EDEFED00AEDBBA00A5D9B30090D49C00BCE4BE0074C88200ADDCB600AADB
      B400F7F7F700F5F5F500DDDDDD00000000002089250081D48A007CD6830034AE
      3D002FAA380034AD400010861B006168580000000000D0543E00E37B4C007113
      0F0000000000000000000000000000000000000000006C4E31006C4E31006C4E
      31006C4E31006C4E31006C4E31006C4E31006C4E31006C4E31006C4E31006C4E
      31006C4E31006C4E31006C4E3100000000000000000000000000000000000000
      00000000000000000000B3B3B300000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DDDDDD00E9E9E800ECEC
      EB00EEEEED00EFEFEE00E6EDE8009AD8AC00ACDFB90095D5A600F4F4F400F5F5
      F400F5F5F500F3F3F300DDDDDD00000000002B9836002FCA4A002AD14A003ADB
      5A0040DF5F003DD95A001E8B2B009A9F9A0000000000D47B7700B03D2500745B
      5A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000079797900080808000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DDDDDD00E7E7E600EAEA
      E900ECECEB00EDEDEC00EEEEEE00EEEFEE00E9EEEA00F1F1F000F2F2F100F2F2
      F200F2F2F200F1F1F000DDDDDD0000000000C6DBC60066A36900479950004499
      4E0047904F0059995F00ACC1AD000000000000000000EFD4D4009B4F4D00CCCB
      CB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006969
      6900000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E0E0E000E5E5E400E5E5
      E400E7E7E600E7E7E700E9E9E700EAEAE900EBEBEA00ECECEB00ECECEC00ECEC
      EC00EDEDEC00EEEEEE00DFDFDF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000ECECEC00EBEB
      EB00EBEBEB00EBEBEB00EBEBEB00EBEBEB00EBEBEB00EBEBEB00EBEBEB00EBEB
      EB00EBEBEB00ECECEC0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CBCBCB00CBCBCB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D8D9DB00B3B5B80094989B007F8387007F83870094989B00B3B5B800D8D9
      DB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009DE18F0079E76300A7EB9800DEF0DA00C2974200B68D3D00CBCBCB000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EDEEEE00B3B6
      B8009C9EA200C0C2C400E2E2E400F7F7F800F7F7F800E2E2E400C0C2C4009C9E
      A200B3B6B800EDEEEE0000000000000000000000000000000000000000000000
      000000000000AC9D8E0080685100694D3100694D310080685100AC9D8E000000
      0000000000000000000000000000000000000000000000000000DADADA00D4D4
      D400CCCCC800CDCDCD00D4D4D400D6D6D6000000000000000000000000008FD2
      7B0040D8190086F56A009FF48A00D9F5D20000000000CC9E4500B68D3D00CBCB
      CB00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EDEEEE00A7AAAD00C0C2
      C300F9F9F900FCFCFC00F1F1F100E5E4E400EAE9E900F8F8F800FEFEFE00F9F9
      F900C0C2C300A7AAAD00EDEEEE0000000000000000000000000000000000C3B6
      AB00694D3100A38A7200B89F8700BCA38B00BEA58D00B99E8300A4896D00694D
      3100C3B6AB00000000000000000000000000000000000000000000000000E9E8
      E500EAE8E200E5E3D800F0F0EB000000000000000000000000000000000062CD
      3B0060E337007AEE5700B7F0A600000000000000000000000000CB9E4500B68D
      3D00CBCBCB000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B3B6B800C0C2C300FCFC
      FC00E7E7E700CECECE00D5D4D400DBDADA00E2E0E000E9E7E700EFEEEE00FAFA
      FA00FDFDFD00C0C2C300B3B6B800000000000000000000000000C3B6AB00785C
      4000B6A08A00B79E8700B3987F00B59B8100B89D8300BA9F8500BCA18700BBA0
      84007D604400C3B6AB0000000000000000000000000000000000C7C6C700BAB9
      BF00EAEAEB00F0EEE600E3E1D200EDECE40000000000000000000000000061C5
      34005ADC280068DF3D00D1F2C70000000000000000000000000000000000CB9E
      4500B68D3D00CBCBCB0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8D9DB009C9EA200F9F9F900E8E8
      E800CDCDCD00CDCDCD00D3D2D200DAD8D800E0DEDE00E6E4E400EBEAEA00EFEE
      EE00F9F8F800F9F9F9009C9EA200D8D9DB000000000000000000694D3100B39D
      8700B8A18B00AE947A00B0967C00B3987E00B59A8000B79C8200BA9F8400BCA1
      8600BA9F8400694D3100000000000000000000000000E6E5E200A5A4AC008F8E
      9A00B9B8C000E9E9E900EAE9E100D8D5C500DDDBD00000000000000000006CBF
      3B0055D01B0066D53400D9F1CF00000000000000000000000000000000000000
      0000CB9E4500B68D3D00CBCBCB00000000000000000000000000000000000000
      000000000000000000000000000000000000B3B5B800C0C2C400FDFCFC00D2D1
      D100CECECE00CCCCCC00D0D0D000D7D6D600DDDBDB00E2E0E000E6E5E500EAE8
      E800EBEAEA00FEFEFE00C0C2C400B3B5B80000000000AC9D8E00A8927E00B39D
      8800A98F7600AC917800AE937A00B0957C00B2987E00B59A8000B79C8200B99E
      8400BCA18600A4886D00AC9D8E0000000000F5F4F000ECECE700D7D6DB00A2A0
      AA008B8A9400B1B0B400E6E5E600E0DED700BDBBAD00BEBCB300D9DCD10074B8
      430063C8270071CE3D00CCEABB00000000000000000000000000000000000000
      000000000000CB9E4500B68D3D00CBCBCB000000000000000000000000000000
      00000000000000000000000000000000000094989B00E2E2E400F4F4F400D4D3
      D300CFCFCF00CDCDCD00CDCDCD00D3D2D200D8D7D700DDDBDB00E1DFDF00E3E1
      E100E4E3E300F6F6F600E2E2E40094989B00000000008A745F00BDAC9C00A48A
      7200A78C7400A98E7600A3886F00B69F8900C1AC9800C3AD9A00C3AF9B00C6B1
      9D00C8B39E00BFA790008068510000000000F2F2ED00E5E2D600F0EFEB00D5D4
      D80097969B007E7C80009E9D9D00D1D1D300CCCAC700BDBAB000B8BBA20078AC
      400098D36C0093D26600BFE1A800000000000000000000000000000000000000
      00000000000000000000CB9E4500B68D3D00CBCBCB0000000000000000000000
      0000000000000000000000000000000000007F838700F7F7F800EDECEC00DDDD
      DD00D2D2D200CECECE00CCCCCC00CECECE00D3D2D200D7D6D600DAD9D900DDDB
      DB00DEDCDC00E6E5E500F7F7F8007F83870000000000755A4100C2B1A300A287
      7000A48A7200A68C740094785E00684B2F00684B2F00684B2F00684B2F00684B
      2F00684B2F00684B2F00684B2F000000000000000000E7E5DC00DBD8CB00EEED
      E900CCCCCD00848383006A69660083827D00C2C1BF00EBEBE300A7BA7F0098C0
      6700B2D58E00BCDBA10000000000000000000000000000000000000000000000
      0000000000000000000000000000CB9E4500B68D3D00CBCBCB00000000000000
      0000000000000000000000000000000000007F838700F7F7F800EFEDED00E4E3
      E300DFDFDF00D5D5D500CECECE00CCCCCC00CECECE00D1D1D100D4D3D300D6D5
      D500D7D6D600E0E0E000F7F7F8007F83870000000000755A4100C0B0A1009F85
      6D00A1876F00A489710091775C00684B2F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D8D6CA00D4D2
      C500E6E5E200B3B3B3006D6C66005C5B4F006F715B009FB2780090B55800AAC7
      8300DAE7CB000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CB9E4500B68D3D00CBCBCB000000
      00000000000000000000000000000000000094989B00E2E2E400F8F8F800E7E6
      E600E3E2E200E0DFDF00DDDCDC00D8D7D700D4D4D400D0D0D000D2D2D200D3D3
      D300D7D7D700F3F3F300E2E2E40094989B000000000080685100BDADA0009C82
      6B009F856D00A1876F008F745C00684B2F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F0F0EC00C3C1
      B400BFBCB200D1D0D10099999100646552007B9161007EA954008DB36B00E4EB
      DC00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CB9E4500B68D3D00CBCB
      CB0000000000000000000000000000000000B3B5B800C0C2C400FEFDFD00E9E7
      E700E6E5E500E2E1E100DFDFDF00DEDDDD00DDDCDC00DCDCDC00DCDCDC00DCDC
      DC00DCDCDC00FDFDFD00C0C2C400B3B5B80000000000AC9D8E00AF9D8D00B4A1
      91009C826B009E846D008E725A00684B2F000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DDDD
      D700A4A29800C7C5BD00E1E0DC00878C7D0091A18600A5BA9600DEE5D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CB9E4500B68D
      3D00CBCBCB00000000000000000000000000D8D9DB009C9EA200F9F9F900F4F3
      F300E9E7E700E6E5E500E3E2E200E0E0E000DFDEDE00DEDDDD00DDDDDD00DDDD
      DD00EEEEEE00F9F9F9009C9EA200D8D9DB000000000000000000755A4100C2B2
      A600A8927D009C826A008C715700684B2F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C4C4BD00C7C5B800A7AA9D00535C4A008D928500E4E5E000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CB9E
      4500B68D3D00CBCBCB00000000000000000000000000B3B6B800C0C2C300FCFC
      FC00F4F3F300E8E7E700E8E6E600E5E4E400E3E2E200E1E0E000E0E0E000F0EF
      EF00FCFCFC00C0C2C300B3B6B800000000000000000000000000C3B6AB008770
      5900C2B2A600AE9A87008F755D00684B2F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F2F2EF00C3C2B60068705B0080877500E4E4DF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CB9E4500B68D3D00CBCBCB000000000000000000EDEEEE00A7AAAD00C0C2
      C300F9F9F900FEFDFD00F8F7F700EEEDED00EEECEC00F7F7F700FDFDFD00F9F9
      F900C0C2C300A7AAAD00EDEEEE0000000000000000000000000000000000C3B6
      AB00755A4100A28D7A00A28D7A00684B2F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EDEDEA00D4D6CF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CB9E4500B58D3D00CBCBCB000000000000000000EDEEEE00B3B6
      B8009C9EA200C0C2C400E2E2E400F7F7F800F7F7F800E2E2E400C0C2C4009C9E
      A200B3B6B800EDEEEE0000000000000000000000000000000000000000000000
      000000000000AC9D8E0080685100694D31000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CB9D4400C29742000000000000000000000000000000
      0000D8D9DB00B3B5B80094989B007F8387007F83870094989B00B3B5B800D8D9
      DB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00F3F3F30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A8937E00AF8D6C00AF8D6C00AF8D6C00AF8D6C00AF8D6C00AF8D
      6C00AF8D6C00AF8D6C00A7927F00DCDCDC000000000000000000000000000000
      000000000000BBBBBB00C5C5C500C5C5C500C5C5C500C5C5C500BDBCBA00CBCB
      CB0000000000000000000000000000000000000000000000000000000000FEFE
      FE00F7F7F700EBEBEB00F2F2F20000000000FDFDFD0000000000FEFEFE00FDFD
      FD00FEFEFE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BD9C7B00FDF4E100FBF2DD00FAF1DC00FAF1DB00FAF0DB00F9EF
      DA00F9EED900FCF2DF00BD9C7B0000000000373737002B2B2B00292929002727
      270022222200FFFFFF00FDFDFD00FDFDFD00FEFEFE00FCFCFC00E2E0DE00D9D9
      D900B9B9B90000000000000000000000000000000000FCFEFE00FBFDFD00EEFE
      E600F8E8F300FFF5FC00E6DAD600E4D7C700FCF0CE00FFFFF900FFFFF700FFFF
      F300FFFCFF00FFFFF200F2FBFE00FFFBFF000000000000000000000000000000
      0000D8D5D400D7D4D400D7D4D400D7D4D400D7D4D400D7D4D400D7D4D400D8D4
      D400DCD9D8000000000000000000000000000000000000000000000000000000
      000000000000BFA08100FBF2DD00F6EDD300F5ECD200F4EAD000F3E9CF00F3E7
      CD00F2E5CB00F8EDD800C9AA8A0000000000454545004A4A4A00474747004444
      44003B3B3B00FFFFFF00FBFBFB00FCFCFC00FDFDFD00FDFDFD00DDDDDD00FDFD
      FD00DBDBDB00AFB1AF000000000000000000000000000000000000000000DED9
      E800860D7F00951CA2008D269F00E6CFFF00F2DFBE00F1ECAD00E8C47800D297
      4100B78A3900E8DCB800FCFFFB00FFFBFF000000000000000000000000009975
      4C00916A3F00A4856300A4846300A3846300A3846300A4846300A38360009A79
      520096795800DAD8D500000000000000000000000000A8937E00AF8D6C00AF8D
      6C00AF8D6C00AC7F5100FAF1DC00F5ECD200F4EAD000F3E9CF00F3E7CD00F2E5
      CB00F1E4CA00F8ECD700CDAF8F00000000004A4A4A004F4F4F004C4C4C004949
      490040404000FFFFFF00FBFBFB00FCFCFC00FDFDFD00FEFEFE00E4E4E400FEFE
      FE00FDFDFD00DBDBDB00C4C4C40000000000FDFFFF00FFFEFF00E6DBDD007321
      92008C23BA003D3FAB006067D6005C4DC000F6E1FF00F5DC9E00FAE49D00D49F
      4400D0A04000F7E3B300FDFFFC00FFFBFF000000000000000000000000009562
      2900B9966E00D4D2CF00CECAC600CECBC700CECBC700CECAC600D6D2CE00CFB5
      9800A6815800D7D3D000000000000000000000000000BD9C7B00FDF4E100FBF2
      DD00FAF1DC00D1AE8600FAF1DB00F4EAD000F3E9CF00F3E7CD00F2E5CB00F1E4
      CA00F0E1C700F7EBD500D1B29500000000004E4E4E0054545400515151004E4E
      4E0044444400FFFFFF00FAFAFA00FBFBFB00FDFDFD00FEFEFE00F0F0F000DCDD
      DC00DDDEDD00EBEBEA00DAD9D80000000000FDFFFF00FFFEFF00EBE0E200A00F
      C9006E66F3006073EC003651C000810EBC00C281D000E6E89600FFE0A000D9AD
      5000EEB05200FAEAC500F8FEFF00FFFCFF000000000000000000000000009E66
      2800C19C7300CCCDCE00C4C4C400C4C4C400C4C4C400C3C4C400D0D1D100C099
      6D00925A1E00D8D5D000000000000000000000000000C9AA8A00FBF2DD00F6ED
      D300F5ECD200D1AF8400FAF0DB00F3E9CF00F3E7CD00F2E5CB00F1E4CA00F0E1
      C700EFDFC500F7E9D400D4B79800000000005353530059595900565656005353
      53004A4A4A00FFFFFF00FAFAFA00FBFBFB00FCFCFC00FDFDFD00FEFEFE00FEFE
      FE00FEFEFE00FEFEFD00FEFDFC00C2C2C2000000000000000000DCDCDC007537
      AF003C74E3004D47BC006B53D5009A37CD00FFE3FF00FFDFB200EFD88A00F1C3
      5E00E9C95E00FFF7DF00FEFCFF00FDFFFC00000000000000000000000000A86B
      2800C79F7200CBCCCD00C2C2C200C2C3C300C2C3C300C2C2C200CFCFD000C79C
      6D009B5F1E00D8D5D000000000000000000000000000CDAF8F00FAF1DC00F5EC
      D200F4EAD000D3B18700F9EFDA00F3E7CD00F2E5CB00F1E4CA00F0E1C700EFDF
      C500ECDAC000F5E6D000D7BA9C0000000000585858005E5E5E005B5B5B005959
      59004E4E4E00FFFFFF00F9F9F900FAFAFA00FBFBFB00FCFCFC00FDFDFD00FDFD
      FD00FEFEFE00FFFFFF00FFFFFF00C2C2C2000000000000000000FEFEFE00FFF9
      FF00AB5ABB009957D4009961C000DEC8F900B8D6CB00ACE6C300D7F1BB00F4FF
      CA00EBE7B700FFFFF300FFFEFF00FFFFFC00000000000000000000000000B170
      2800CEA37300C9C8C800C0BEBD00C0BFBD00C0BFBD00C0BEBD00CCCCCB00CE9F
      6B00A4641E00D9D5D000000000000000000000000000D1B29500FAF1DB00F4EA
      D000F3E9CF00D5B38900F9EED900F2E5CB00F1E4CA00F0E1C700EFDFC500ECDA
      C000E9D3B900F3E3CD00D9BDA000000000005E5E5E0063636300616161005E5E
      5E0053535300FFFFFF00F7F7F700F9F9F900FAFAFA00FBFBFB00FCFCFC00FCFC
      FC00FCFCFC00FFFFFF00FFFFFF00C2C2C200000000000000000000000000EFFF
      FC00FFFEFA00FFF7FF00FFF5FF00A4DFD10072DAC30057D4AC003DAA7C0026A0
      7200F2FFFA00FAFEF800FEFCFF00FFFFFB00000000000000000000000000BE7C
      3300CE8D4600D3A16A00D09F6800D09F6800CF9E6800CE9D6700D2A06A00CB86
      3A00AF6B2200D9D5D000000000000000000000000000D4B79800FAF0DB00F3E9
      CF00F3E7CD00D6B48A00F8EDD800F1E4CA00F0E1C700EFDFC500ECDAC000E9D3
      B900E6CEB400F2E1CC00DCC0A500000000006464640068686800666666006363
      630058585800FFFFFF00F6F6F500F8F8F700F9F9F900FAFAFA00FBFBFB00FBFB
      FB00FBFBFB00FFFFFF00FFFFFF00C2C2C200FFFEFF00FFFFFE00FBFDFF00FFFF
      FC00FFFFFB00DBCAB000FEFEFE00F4FFFF00DEFFED0086D3A70064BD920050B1
      8F00FFFFFE00000000000000000000000000000000000000000000000000CC8D
      4A00D28633006D441700341F080038220900503110005233100052331000C57D
      2E00BC7A3200D9D5D000000000000000000000000000D7BA9C00F9EFDA00F3E7
      CD00F2E5CB00D8B68C00F8EDD700F0E1C700EFDFC500ECDAC000E9D3B900C2A4
      8500C2A48500C2A48500C4A4830000000000696969006E6E6E006B6B6B006868
      68005C5C5C00FFFFFF00F4F4F300F6F6F500F7F7F700F9F9F900FAFAFA00FAFA
      FA00FAFAFA00FFFFFF00FFFFFF00C2C2C200FFFEFF0000000000FFFFFE00F8FF
      FF00FDDFA600FFE29B00F5EBC300FAD6A600F8C66E00EBAE5800EEFFE300EBFB
      FF00FEFFFB00000000000000000000000000000000000000000000000000D598
      5400DE923F005A3A16000F080000170D020098642A00AC71300041290F00CC87
      3900C88A4600D9D5D100000000000000000000000000D9BDA000F9EED900F2E5
      CB00F1E4CA00D9B68D00F7EBD600EFDFC500ECDAC000E9D3B900E6CEB400D2B6
      9A00FFF6E500E0C6AB00F3E9E000000000006E6E6E0073737300707070006D6D
      6D0061616100FFFFFF00F2F2F100F4F4F300F5F5F500F6F6F600F7F7F700F8F8
      F800F9F9F900FFFFFF00FFFFFF00C2C2C200FBFFFF0000000000FFFFF600FCEC
      B700E9AD4D00D2A04600DDAC4000E3AE4600C6A04000F1E2C100FFFFFE000000
      0000FFFFFE00000000000000000000000000000000000000000000000000DDA1
      5E00E99E4A00734A1C002F190200361F0500B4773500C7853C005D3A1400D992
      4400D1975600DEDAD600000000000000000000000000DCC0A500F8EDD800F1E4
      CA00F0E1C700D9B78D00FBF0DD00F6E9D400F4E6D000F3E2CD00F2E1CC00DCC2
      A700E1C7AE00F3EBE00000000000000000007474740078787800757575007272
      720066666600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00C3C3C300FBFEFF00FFFEFF00FEFAE700FCD2
      9100DCA13E00DBA54600E1A53B00F7D58F00F5E7CA00FFFFF700000000000000
      000000000000000000000000000000000000000000000000000000000000DEAC
      7400E4A45F0090673A006043230063462500835D33008760340079572F00D69C
      5B00DABA980000000000000000000000000000000000DEC3A800F8EDD700F0E1
      C700EFDFC500DEBF9900D7B18800C0966900C0966900C0966900C1946800E2C9
      AF00F4ECE3000000000000000000000000007E7E7E007D7D7D00747474006565
      6500636363005858580056565600535353005151510057575700595959005959
      5900C6C6C600C6C6C600C6C6C60000000000FBFFFE00FEFCFB00FFFCF700FFFC
      E500ECC07300FFD98700E5CFA500FFFFF200FAFDFF00FBFDFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E0C6AB00F7EBD600EFDF
      C500ECDAC000E9D3B900E6CEB400D2B69A00FFF6E500E0C6AB00F3E9E0000000
      000000000000000000000000000000000000969696009B9B9B0092929200EFEF
      EF00EEEEEE00EDEDED00ECECEC00EBEBEB00EAEAEA0080808000848484008888
      880000000000000000000000000000000000FDFFFE00FDFFFC00FDFFFF00FDFE
      FF00FFFBE400FBDEAB00FFFFFC0000000000FFFEFF00FFFEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E1C7AE00FBF0DD00F6E9
      D400F4E6D000F3E2CD00F2E1CC00DCC2A700E1C7AE00F3EBE000000000000000
      000000000000000000000000000000000000000000000000000000000000AEAE
      AE00AEAEAE00AEAEAE00AEAEAE00AEAEAE00AEAEAE0000000000000000000000
      000000000000000000000000000000000000FDFEFF00FDFFFC00FBFFFF00FFFF
      F900F1FDFF00FFFFED00FDFFFF0000000000FDFFFF00FAFEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E9D6C300E2C9AF00E2C9
      AF00E2C9AF00E2C9AF00E2C9AF00E2C9AF00F4ECE30000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EEEE
      EE00D7D7D700D5D5D500D3D3D300D1D1D100EDEDED0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D9D9D900D4D4D400D5D5D500DADADA00946F6000854022008D645000D8D0
      CD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000053575500535755005357
      5500535755005357550053575500535755005357550053575500535755005357
      5500535755005357550053575500000000000000000000000000000000000000
      000000000000000000000000000000000000894324008A422200894222009865
      4C00000000000000000000000000000000000000000000000000C2C0C000A5A5
      A200BBBBBA00000000000000000000000000000000000000000000000000BBBB
      BA00A5A5A200C0C0C00000000000000000000000000000000000000000000000
      0000D9D9D900CECECE00BAC4C7008DBAC400AEBDC200C4C4C400CCCCCC00D6D6
      D6000000000000000000000000000000000092928E0053575500CBD3CF00CFD7
      D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7
      D300CFD7D300CFD7D3005357550092928E000000000000000000854122000000
      0000000000000000000000000000A2715B008B422200884122008C442400EBE4
      E10000000000000000000000000000000000000000006E6C6800434850005D67
      72004B525E006F6D6900000000000000000000000000000000006D6D69004E53
      5F00646B7200474A50006E6B690000000000000000000000000000000000D3D3
      D300C3C3C300B4B4B40056B6C80041C5DB0034BBD4005CBBCD0078ACB800BFBF
      BF00D0D0D0000000000000000000000000004547430053575500CBD3CF003634
      2E0036342E0036342E0036342E0036342E0036342E0036342E0036342E003634
      2E0036342E00CFD7D30053575500454743000000000000000000854122008456
      43000000000000000000000000008B4323008B4222008C442400B88D77000000
      00000000000000000000000000000000000099979400395081006B96F00079A7
      FF00658FF600415494009C9B980000000000000000009C9B9800495C97007BA2
      F7008DB6FF007AA2F2003F56840099979400000000000000000000000000D7D7
      D700C8C8C80083B4BE003BC5DB0024C3DA001AC2DB0030C2D9004CCBDF0040B4
      CB00AEC9CF00000000000000000000000000565A580053575500565A5800565B
      5900565B5900565B5900565B5900565B5900565B5900565B5900565B5900565B
      5900565B5900565B590053575500565A58000000000000000000854122008641
      21009A6B5400000000009C5E41008C4323008B4222008E462600000000000000
      000000000000000000000000000000000000797978002958D1003066FF003664
      F800315FFF002242CF00848483000000000000000000848383003D5EDA003E72
      FF004779FF004780FF002664DB00797978000000000000000000000000000000
      0000000000005BC3D50018C2DB001DC3DD0036D2E80024C1DA000FB8D3003BCF
      E2003FBDD500AEDCE6000000000000000000858A8800FFFFFF00CFD7D300CFD7
      D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7
      D300CFD7D300CFD7D300FFFFFF00858A88000000000000000000854122008541
      220084402100AB775F008C4424008D4222008D442400CFAB9800000000000000
      000000000000000000000000000000000000AEAEAB0033407800223AC4003248
      D7002236C2002F366D008D8D8B0000000000000000008D8D8C00434B78003D58
      D4004865E800254ACE00293F7B00AEAEAB000000000000000000000000000000
      0000000000002EC1D9001ACDE60025D1EB0020C5E0001DC1DB001EC6DC0028BE
      D7005BCEE0007CD0E1000000000000000000858A8800FFFFFF00D2D9D6003634
      2E0036342E0036342E0036342E0036342E0036342E0036342E0036342E003634
      2E0036342E0073AEE600FFFFFF00858A8800000000000000000095522E008E47
      2500873F2000904624008F452400904423009148280000000000000000000000
      000000000000000000000000000000000000000000007D7C77005F5E65006465
      780048484F002D2C2400A1A1A1000000000000000000A1A1A1002C2B25004C4C
      5200686A7D005C5D66007C7A7500000000000000000000000000000000000000
      0000C5E8F0003BD2E8002FD9F10037DAF20024CDE80016C5DE001FC0D80061CE
      E000BCE5EE00000000000000000000000000858A8800FFFFFF00DBE0DE003836
      310050504B0050504B0050504B0050504B0050504B0050504B0050504B005050
      4B0038363100DBE0DE00FFFFFF00858A8800000000000000000095522E00964C
      28009043220095452300994724009B4A2600A4522D00A4522D00AA5D3800AC60
      3B00B26F52000000000000000000000000000000000077777700727270008A8A
      85003E3E3A0011111100A6A6A6000000000000000000A9A9A900131313003939
      37008888830073726F0077777700000000000000000000000000000000000000
      00008FBBD50050E2F6004FE3F80044DCF30025D4EC001AC5DD0059CADE00D4ED
      F30000000000000000000000000000000000858A8800FFFFFF00E3E7E6003F3E
      3A006B6D68006B6D68006B6D68006B6D68006B6D68006B6D68006B6D68006B6D
      68003F3E3A00E3E7E600FFFFFF00858A8800000000000000000095522E009D51
      2C00974623009B4824009D4925009F4A2600A04C2700A14D2800AE643F00D0A6
      8D00000000000000000000000000000000000000000068686800656565008A8A
      8A0061616100111111002A2A2A00CDCDCD00D6D6D6002C2C2C00121212005959
      59007D7D7D00676767006363630000000000000000000000000000000000ADAD
      CF000000AA003058B80069CFEA004CE6FB0033D7ED0055C8DD00000000000000
      000000000000000000000000000000000000858A8800FFFFFF00ECEEEE004546
      4300858985008589850085898500858985008589850085898500858985008589
      850045464300ECEEEE00FFFFFF00858A8800000000000000000095522E00A95D
      3400A3532C00A6562D00A7582E00A85A3000A95B3100BC7F5D00E3CBBD000000
      00000000000000000000000000000000000000000000A8A8A800545454007474
      740060606000161616000A0A0A006F6F6F00797979000A0A0A00161616005656
      560066666600555555009F9F9F00000000000000000000000000000000005CC3
      D900216FB800272CC3003B3ECE003779BE007ED3E40000000000000000000000
      000000000000000000000000000000000000898E8C00F8F8F800FFFFFF004C4E
      4C004C4E4C004C4E4C004C4E4C004C4E4C004C4E4C004C4E4C004C4E4C004C4E
      4C004C4E4C00FFFFFF00F8F8F800898E8C000000000000000000A15E3700B46D
      3F00AF653700B1683900B36B3B00B8734300CB997800ECDDD700000000000000
      00000000000000000000000000000000000000000000000000006D6D6D005F5F
      5F004E4E4E0042424200333333004F4F4F005454540032323200414141004D4D
      4D005F5F5F006D6D6D00DCDCDC000000000000000000000000000000000070C8
      DC0050D3E9006DD4ED00375FB8004040A8000000000000000000000000000000
      000000000000000000000000000000000000C9CBCA00898E8C00858A8800696E
      6C00FFFFFF00E0E5E200DFE5E200DFE4E100DEE4E000DDE3E000DDE2DF00FFFF
      FF00696E6C00858A8800898E8C00C9CBCA000000000000000000A9673D00BA76
      4700B46D3C00B6703E00BD7C4D00D1A58A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000969696008F8F
      8F0077777700666666005D5D5D0047474700484848005D5D5D00646464007373
      7300909090009595950000000000000000000000000000000000000000005693
      BA000E76AD0046B2CF0058CAE000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000041423D008086
      8300FFFFFF00E8ECEA00E7EBE900989D9B00989D9B00E5EAE700E5E9E700FFFF
      FF008086830041423D0000000000000000000000000000000000AF6D4200C07F
      4E00B8734100C78E6400D9B6A200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C9C
      9C00A2A2A2008F8F8F008080800067676700636363007E7E7E008A8A8A009B9B
      9B009B9B9B0000000000000000000000000000000000000000009CBED4002182
      BE001978B100C9DBE600D5EEF300000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004E514E00979D
      9A00FFFFFF00F0F3F1009A9E9D009A9E9C009A9E9C00999E9C00EDF0EE00FFFF
      FF00979D9A004E514E0000000000000000000000000000000000B4724500C78D
      6000C7936F00E2C8B90000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CECE
      CE00696969007E7E7E00A3A3A3008F8F8F0086868600A2A2A2007B7B7B006B6B
      6B00C7C7C70000000000000000000000000000000000DBE6ED00398ABD00137E
      C1005899C1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A5A7A600AEB5
      B200FFFFFF009CA09E009CA09E009BA09E009BA09E009B9F9E009B9F9E00FFFF
      FF00AEB5B200A5A7A60000000000000000000000000000000000B4724500D5AD
      9300EDE0D7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009E9E9E0086868600C5C5C500B1B1B100ADADAD00C8C8C800868686009C9C
      9C000000000000000000000000000000000000000000ABC7D9005192BA003585
      B700AECADB000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B5BC
      B900FEFEFE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFE
      FE00B5BCB9000000000000000000000000000000000000000000A4725C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007BABC90096BB
      D200000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C8CD
      CB00B5BCB900B5BCB900B5BCB900B5BCB900B5BCB900B5BCB900B5BCB900B5BC
      B900C7CCCA000000000000000000000000000000000000000000000000000000
      00000000000000000000DEDEDE00DEDEDE00DEDEDE00DEDEDE00000000000000
      000000000000000000000000000000000000767676002A2A2A002E2E2E006262
      6200000000000000000000000000000000000000000000000000000000000000
      00005D5D5D002E2E2E0029292900878787000D7C9100087A8F00087A8F00087A
      8F00087A8F00087A8F00087A8F00087A8F00087A8F00087A8F00087A8F001481
      9600C3E1EA000000000000000000000000000000000000000000000000000000
      0000000000008080800080808000808080008080800080808000808080008080
      8000000000000000000000000000000000000000000000000000000000000000
      000000000000B6B6B6009E9E9E009E9E9E009E9E9E009E9E9E00B6B6B6000000
      0000000000000000000000000000000000009999990001010100010101000C0C
      0C00B7B7B700DCDCDC00DDDDDD000000000000000000DBDBDB00DEDEDE00B2B2
      B200050505000101010001010100B0B0B00007798E00A3E6FF0051D0FF0041CA
      FF0042CAFF0042CAFF0042CAFF0042CAFF0042CAFF0041CAFF0044CAFF0087E4
      FF004597A7000000000000000000000000000000000000000000000000000000
      00000000000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000DEDEDE000066B4000071CF000073CF000076CF000078CF000070B400DEDE
      DE00000000000000000000000000000000000000000017171700010101000101
      0100898989000000000000000000000000000000000000000000000000007B7B
      7B0001010100010101002B2B2B0000000000087A8F004AA6BC0054D1FF0015BC
      FB0014BBFB0015BBFB0015BBFB0015BBFB0015BBFB0015BBFB0014BCFB003AC8
      FE0075C7DF00C5E2EA0000000000000000000000000000000000000000000000
      00008080800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00808080000000000000000000000000000000000000000000000000000000
      0000DEDEDE00006ECF00007CF300007EF3000081F3000082F3000078CF00DEDE
      DE0000000000000000000000000000000000000000005F5F5F00010101000101
      010031313100A8A8A800A8A8A800A6A6A600A6A6A600A9A9A900A3A3A3001D1D
      1D0001010100010101007D7D7D0000000000087A8F00239DBF0089DCF8002DC7
      FE001CC1FD001EC1FD001EC1FD001EC1FD001EC1FD001EC1FD001DC1FD0023C3
      FD0080E2FF004697A60000000000000000000000000000000000000000000000
      000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00808080000000000000000000000000000000000000000000000000000000
      0000DEDEDE00006DCF00007BF300007CF300007FF3000081F3000077CF00DEDE
      DE000000000000000000000000000000000000000000AFAFAF00010101000101
      0100010101000000000000000000000000000000000000000000000000000101
      01000101010009090900CFCFCF0000000000087A8F0036C9FF0050A8B9005FD9
      FF0025C7FD0025C7FD0025C8FD0025C8FD0025C8FD0025C8FD0025C8FD0024C7
      FD0048D2FF0071C4DA00CBE5EB00000000000000000000000000000000008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008080800000000000000000000000000000000000000000000000
      0000DEDEDE00006BCF000079F300007BF300007EF300007FF3000074CF00DEDE
      DE000000000000000000000000000000000000000000000000002A2A2A000101
      0100010101000101010001010100010101000101010001010100010101000101
      010001010100575757000000000000000000087A8F0041D1FF000E94BC0090E3
      FB0039D1FD002BCDFC002DCEFC002DCEFC002DCEFC002DCEFC002DCEFC002CCD
      FC0031CEFD0088E8FF004897A800000000000000000000000000808080008080
      8000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00808080000000000000000000000000000000000000000000DEDE
      DE00C6C6C600006ACF000077F3000079F300007BF300007CF3000072CF00C6C6
      C600DEDEDE000000000000000000000000000000000000000000898989000101
      0100000000000606060040404000484848004949490038383800000000000000
      000000000000B6B6B6000000000000000000087A8F0045D1FF001AC3FB0058AB
      BD0069DFFF0037D3FE0036D2FE0036D2FE0036D2FE0036D2FE0036D2FE0036D2
      FE0035D1FE0057DBFF0074C6DD00CFE7EA00000000000000000080808000FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008080800000000000000000000000000000000000BEBEBE00A2A2
      A20096969600006CD8002086F5000A7DF3000079F300007BF3000073D8009696
      9600A1A1A100BDBDBD0000000000000000000000000000000000D9D9D9000E0E
      0E000A0A0A0005050500BCBCBC00000000000000000094949400070707000707
      07002C2C2C00000000000000000000000000087A8F0047D4FF0026CDFF001399
      BA00A6E6FA008AE4FE0087E4FD0088E4FD0088E4FD0088E4FD0087E4FD0086E5
      FF0089E7FF008EE7FF00C0F7FF004F9BAC000000000000000000808080008080
      80008080800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000000000000000000000000000000000000058AB000063
      CB000067D800006FEA0064AAF80056A4F700419AF6002E90F5000076EA000072
      D800006ECB000064AC0000000000000000000000000000000000000000004E4E
      4E00202020001C1C1C006B6B6B0000000000000000003D3D3D00212121001616
      16007E7E7E00000000000000000000000000087A8F004CD6FF002ACDFD002BCA
      F90009819900087A8F00087A8F00087A8F00087A8F00087A8F00087A8F001080
      96007CB8B4005AA88800268B9B00278B9B000000000000000000000000000000
      00000000000080808000FFFFFF0080808000FFFFFF0080808000FFFFFF008080
      8000FFFFFF0080808000000000000000000000000000000000006C99C4003D8B
      E20076B1F60075B1F90072B0F8006CAEF80065ACF8005DA7F70054A4F7004A9E
      F4002585E0005D95C00000000000000000000000000000000000000000009C9C
      9C00292929003C3C3C002828280000000000BFBFBF00272727003B3B3B001919
      1900CFCFCF00000000000000000000000000087A8F004FD8FF002DCEFE0030CF
      FE0031D2FF0030D1FF0034D3FF004CD9FF0053DDFF0053DDFF006BE4FF0069BC
      DF004AB3620042C94A0000000000000000000000000000000000000000000000
      00000000000080808000FFFFFF0080808000FFFFFF0080808000FFFFFF008080
      8000808080000000000000000000000000000000000000000000000000006C99
      C500448FE30082B7F7007EB7F90078B3F90070B0F80067ACF8005DA6F5002E87
      E1006096C4000000000000000000000000000000000000000000000000000000
      00002D2D2D005757570034343400A8A8A8007D7D7D0051515100424242004646
      4600000000000000000000000000000000001784990076E4FF0036D1FE0032D0
      FE0030D0FE0037D1FE0071E2FF001D899F00087A8F00087A8F0018829B002EA4
      690066E6680068EB6D0079C78E00000000000000000000000000000000000000
      00000000000080808000FFFFFF0080808000FFFFFF0080808000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006D99C6004A92E3008ABCF70083B9F9007AB4F90070AEF500368AE1006498
      C400000000000000000000000000000000000000000000000000000000000000
      00005E5E5E005A5A5A0066666600484848004C4C4C007272720033333300A5A5
      A50000000000000000000000000000000000AAD2DA002D93A8007EE7FF0054DC
      FF0053DCFF007AE7FF002C93A900BCDCE00000000000000000008ED29F0074DF
      74006EE1720064DF690030BD3F00000000000000000000000000000000000000
      00000000000080808000FFFFFF00808080008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006E9BC6004B92E3008CBCF70083B8F7003E8CE2006999C4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BCBCBC00434343008C8C8C003C3C3C00626262007F7F7F003B3B3B000000
      00000000000000000000000000000000000000000000CBE6EC0014819500087A
      8F00087A8F0016829800C5E0E5000000000000000000000000006AC1770037A6
      470067D16D004BC355002AB03E0069BE78000000000000000000000000000000
      00000000000080808000FFFFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000709BC6004A90E000458CE0006E9BC600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000051515100999999008E8E8E009C9C9C006C6C6C007E7E7E000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C8E9D90029A731008DD0A200C6E7D400D1EBDB0096D3A7005BBD
      690088D9890035B1460000000000000000000000000000000000000000000000
      00000000000080808000FFFFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000078A2CD0078A2CD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000898989008F8F8F00C4C4C400BBBBBB003B3B3B00D0D0D0000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BAE1C5005BBD65009CD89C009DD8A100B4E2B400B2E3
      B2007FCD830082CA910000000000000000000000000000000000000000000000
      00000000000080808000FFFFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D8D8D800555555008A8A8A006D6D6D0058585800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A4D8B20065BE76005ABA6B0059B9
      6900A5D9AF000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C2C2C20070707000B0B0B0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E9BDA600E0A07000E9BD
      A600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C5C5C5009696960056565600494949000000000000000000000000000000
      0000000000000000000000000000C0C0C000BFBFBF0000000000000000000000
      00000000000000000000000000000000000000000000E0A07000E9B58E00E0A0
      700000000000A7A7A7002A2A2A00232323001D1D1D0016161600121212000C0C
      0C00070707000303030000000000000000000000000000000000000000000000
      00004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      2200000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C5C5
      C500969696005656560057575700DEDEDE000000000000000000000000000000
      00000000000000000000B6B6B6002E76A1002B76A100B4B4B400000000000000
      00000000000000000000000000000000000000000000E9BDA700E0A07000E9BD
      A600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB12200000000000000000000000000000000000000000000000000C8C2
      BD00978B8100928476009F918300A2948800AFA59B00D6D2CE00DFDFDF009696
      96005656560057575700E0E0E000000000000000000000000000000000000000
      000000000000B8B8B8003176A600007ADB00007CDC002879AA00B4B4B4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB12200000000000000000000000000000000009E948B008F80
      7100C3B8AD00E8E1DC00EBE1DA00ECDDD300DAC7BA00B3A496007C7065007877
      770057575700E0E0E00000000000000000000000000000000000000000000000
      0000B9B9B9003676A4000077DD000080F0000083F000007EE0002879AA00B4B4
      B400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000000000009C948A009E908200EEEB
      E700FFFFFF00FFFEFD00FFFBFA00FFF8F500FFEEE600F0D9CB00A19183007970
      6600DDDDDD00000000000000000000000000000000000000000000000000BBBB
      BB003B75A3000074DC00007DF0000080F3000082F3000083F000007EDF002A78
      A800B5B5B50000000000000000000000000000000000EDC8B100E4AB8200EBC2
      AB00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000D9D6D30085756600E8E1DC00FFFE
      FD00FFFEFD00FFFCFB00FFFBF900FFF9F600FFF4EF00FFE9DE00F4D8C900AD9C
      8D00D1CEC8000000000000000000000000000000000000000000C0C0C0004075
      A1000070DB000078EF00007CF300007FF3000080F3000082F3000082F000007D
      DF002E78A700BCBCBC00000000000000000000000000E8B69200EDC3A400E6AD
      830000000000B9B9B900575757005050500049494900424242003B3B3B003333
      33002C2C2C0025252500000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB1220000000000000000009C948A00B4A69A00FEF9F600FFFB
      F900FFFBFA00FFFBF900FFF9F700FFF8F400FFF6F100FFECE300FFE4D700CBB1
      9E009F948A0000000000000000000000000000000000000000005787B100227F
      DE00278AF1000B7DF400007AF300007DF300007EF300007FF3000081F3000081
      F000007BDE004186B100000000000000000000000000F0D0B800E9B89400EECA
      B100000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB1220000000000000000008A7E7200D3C6BC00FFF7F300FFF8
      F600FFF9F600FFF9F600FFF8F400FFF6F200FFF5F000FFEADF00FFE2D400DABA
      A70088796B000000000000000000000000000000000000000000005BAB000066
      CB00006CD8000073EA003996F600208BF5000B82F400007DF300007BEA000076
      D8000072CC000069AE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000877A6D00E2D4CB00FFF4EF00FFF6
      F200DD764A00D06D4300C3643B00B75B3400A9522C00FFE9DF00FFE1D200D8B7
      A300837364000000000000000000000000000000000000000000000000000000
      0000DEDEDE00006CD8006DB1F8006AB0F8005DAAF700469FF6000073D800DEDE
      DE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000A2988F00DACBC000FFF2EC00FFF4
      EF00FFF4EF00FFF0E900FFEBE100FFECE300FFF1EA00FFF1EB00FEDDCE00CAA6
      8F007D6E5F000000000000000000000000000000000000000000000000000000
      0000DEDEDE000068CF0079B5F90076B5F90072B4F8006BB1F800006DCF00DEDE
      DE000000000000000000000000000000000000000000F1D2BC00EBC09F00F1D2
      BC00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000B7B1AB00C1B2A600FEEFE800FFED
      E400FFEAE000FFEEE600FFF2EC00FFF2EC00FFF1EB00FFF1E900F0CBB600AA8D
      7700A0958C000000000000000000000000000000000000000000000000000000
      0000DEDEDE000065CF0085BCF90082BAF9007CB8F90075B5F900006BCF00DEDE
      DE000000000000000000000000000000000000000000EBC09F00F1CFB400EBC0
      9F0000000000C4C4C40076767600737373006F6F6F006A6A6A00646464005F5F
      5F00585858005252520000000000000000000000000000000000000000004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB12200000000000000000000000000EDEBE900A4998E00E3D3C900FEF0
      E900FFF2EB00FFF2EB00FFF1EB00FFF1EA00FFF0E900F8E3D700C49C83008575
      6500D9D6D3000000000000000000000000000000000000000000000000000000
      0000DEDEDE000064CF0091C1FA008CBFFA0086BCF9007EB8F9000068CF00DEDE
      DE000000000000000000000000000000000000000000F1D2BC00EBC09F00F1D2
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB1220000000000000000000000000000000000C8C3BD00B2A69B00E1D0
      C500F8E8E000FDEDE500FDEEE600FBEBE200F4E2D800CAAD99008C796900A49C
      9500000000000000000000000000000000000000000000000000000000000000
      0000000000000062CF009AC5FA0096C4FA008FC0FA0086BBF9000066CF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C9C4BE00A89E
      9400C3B5AA00D6C5B900DCC9BD00CFBBAD00B6A598008A7C6E00AEA69F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000059B4000061CF000062CF000063CF000064CF00005CB4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F0EF
      EE00C3BDB700B5AEA600B3A79B00ADA49B00B3ACA500DDDAD700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DBDBDB00CBCBCB00CBCBCB00CBCB
      CB00CBCBCB00CBCBCB00CBCBCB00CBCBCB00CBCBCB00DBDBDB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C5D6DD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FDFDFD00FBFBFC00F8F8FA00F9F9FB00FDFCFD00FDFDFD000000
      0000000000000000000000000000000000008383830063646400636363006464
      6400656565006565650064646400636363006364640083838300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B2C4CC005E8392002E697F00557C8C00BECFD600000000000000
      000000000000000000000000000000000000000000000000000000000000F6F7
      F700E0E2E300C2BDB400B9AD8D00B9B08400BAB08800BFB49E00CFCCCB00E8E9
      EB00F6F6F70000000000000000000000000066656500706E6D00797574007B77
      760066656500696968007B77760079767400716E6E0060626600000000000000
      0000000000000000000000000000000000009B9B9B004E4E4E000B0B0B005050
      50007B7B7B009B9B9B00BBBBBB00D8D8D80000000000D5D5D500B7B7B7007A7A
      7A000E0E0E004F4F4F00606060009B9B9B00000000000000000000000000A6BA
      C200587783004C657400738899003D748D00627C9200436273007D9BA900DFE9
      ED00000000000000000000000000000000000000000000000000000000000000
      000091795C00E3983900FEC05400FECD5D00FFD26000FABC5200C38537009892
      8B000000000000000000000000000000000070707000A19F9D007C7877006666
      66005555550054555500656464007C787700A19F9E00595D6500CBCBCB00CBCB
      CB00CBCBCB00CBCBCB00CBCBCB00DBDBDB00000000001E1E1E00CCCCCC001E1E
      1E00000000000000000000000000000000000000000000000000000000001E1E
      1E00CCCCCC001E1E1E000000000000000000000000009DB1BA004E6C7B004760
      72007A8A9B0098A3B300A2B6CB00427892008EA8C500879CB700546E82004B6F
      7F00A9C0C900000000000000000000000000000000000000000000000000B0A5
      9700F2761800EA701300E77F1F00E4AB7E00EA8D3000E87E1A00F77D1900CB75
      2C00DEDEDE0000000000000000000000000066666500C4C2C100656565007474
      74003131310041414100797979005B5B5B00C0BEBE0057575600B67C0800B67B
      0600B47A0700B47B0900B67E0E00BD984F0000139D00000F7F0000021300000F
      7F0000139D006C7376006C7376006C7376006C7376006C7376006C737600575D
      60000D0E0E00575D60006C7376006C7376009FB3BB00354F61006F8097006F7D
      9300667182007E88980099AFC6003F758E008CA5C3006C92AF0052819D007286
      9E003E627300C2CCD0000000000000000000000000000000000000000000AC61
      3000C23E0000C43C0000CF440000ECD4CA00F2D9CB00CC450600C1370000DC5A
      0B00AA938100000000000000000000000000C4C4C4006F6F6D0081817F00C3C2
      C1008E8D8D008E8D8D00C1C0C0006C6C6B006B6C6B00C8CACD00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B67E0E00006AE200006AE200006AE200006A
      E200006AE2006C737600D8D8D800AEAEAE00AEAEAE00AEAEAE00AEAEAE00AEAE
      AE00AEAEAE00AEAEAE00BEBEBE007B81830096A9B1004F637700728199005E6C
      80007A8BA5008597B200849DB8003C718B0084A0BF004D7994002E657F008399
      B60047657900B5C2C50000000000000000000000000000000000C5C3BF00932F
      0600992C0500B1491D00AE230000E4C1B300F1E9E900E7BFB100AB360700A029
      00009E613E0000000000000000000000000000000000000000006E6C6C00E9EE
      F000312F2F002B2A2900E3E9ED00807F8000E2E2E200F5F4F400FAFBFD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B47B0900006AE200006AE200006AE200006A
      E200006AE2006C737600D8D8D800B2B2B200AFAFAF00AEAEAE00AEAEAE00AEAE
      AE00AEAEAE00AEAEAE00BEBEBE0084898A0096A8B000485C70007F91AE008397
      B40072839C005D6B7F00758EAA00396D880087A2C200547A960021546B006F88
      A30048657700D3DDE30000000000000000000000000000000000A59B93006A0D
      00007B251500D6CFCF00A8543900D8B4AE00AA5A4B00F0E1DD00C38F82006C08
      00007D311900000000000000000000000000000000000000000000000000AFCC
      DB006EAFD0005392B60083835F00FFFFFF00C2BEBD00BEBBBB00D5D4D500EDED
      EE00ECEBED00EBEBED00FFFFFF00B47B0800006AE20044C4FF0044C4FF0044C4
      FF00117BDF006C737600DADADA00B6B6B600B2B2B200AFAFAF00AEAEAE00AEAE
      AE00AEAEAE00AEAEAE00BEBEBE008D90920096A8B000435669006E7E98006372
      89006A7B940065758D007892B100376A85007C99BA007A96B500406680006982
      9D0045637500D7E1E50000000000000000000000000000000000998981006E26
      2000681D140098615A00DED7D900EBE2E100E6D8D500D3B7B600610E08004900
      0000631A0C00E0D8D60000000000000000000000000000000000D4D4D50088CC
      EE0080C1E4006FAFD2005B95B300FBFFFF00DBDBE300D6D9E700D5D7E600D3D6
      E500D2D5E300D1D4DF00FFFFFF00B47B0900006AE200000D1A00000000000000
      0000004FA8006C737600DBDBDB00B9B9B900B6B6B600B2B2B200AFAFAF00AEAE
      AE00AEAEAE00AEAEAE00BEBEBE009598990096A7AE00485A6B005E6B80004C59
      6C00798DAB00889EBF00819CBE00366882007090B0006989A7006F8AA8007589
      A500425F7100D7E1E500000000000000000000000000000000009A887F00914C
      44009448410099362C00B96B6500FBFFFF00F1DCDC009A2F26007F1A15006006
      06005E0F0500DFD1CF00000000000000000000000000000000003A5A7F00A8EF
      FF008FD2F1007EBFDF006CAFD3004C6B9C00FFD40800F8CE0E00F6CC0F00F6CC
      0E00F6CB0C00F5C90200FFFFFF00B47B0A005EA0EB0000428C00000000000000
      0000004FA8006C737600DEDEDE00BCBCBC00B9B9B900B6B6B600B2B2B200AFAF
      AF00AEAEAE00AEAEAE00BEBEBE009E9FA00094A5AD00556573008C9AAE008A9E
      BC0090A8CC0086A0C4005D83A100255B74006285A4007C95B6006884A1005872
      8C003D5A6B00D7E1E40000000000000000000000000000000000A18C8400B35A
      5000BB534A00C44C4300D3A8A600F7FAFB00F7F1F100D0797200BF453A00AB46
      3E0092332400E4D4D300000000000000000000000000000000000A3B6B006AAE
      D20074ADD10088C6E50075B4D70000165600FCD84100ECCD4200E9CA4200E9CA
      4200E8CA4100E8C93900FFFFFF00B47B0A00000000005EA0EB00006AE200006A
      E200006AE2006C737600E1E1E100C0C0C000BCBCBC00B9B9B900B6B6B600B2B2
      B200AFAFAF00AEAEAE00BEBEBE00A6A7A70093A5AD0059687500A4B0C2008FA4
      BC005A7E99002C5E7700255F79002F6B8700225F7900386883005D7C99006377
      93003C586900D6E0E40000000000000000000000000000000000B0999100D269
      5D00D3625700D3B4B300E2C0BF00EDCDCB00EEA5A000F9E9E900DE8A8300CC51
      4800BE5B48000000000000000000000000000000000000000000114574002D79
      A9004E8AB900517AA7002442710006295F00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B47B09000000000000000000000000000000
      000000000000AEAEAE00E4E4E400E2E2E200E0E0E000DFDFDF00DDDDDD00DBDB
      DB00DADADA00D8D8D800E0E0E000ADADAD0094A5AD003F5664004C6D81002351
      670024577000487695007195B900819FC500618DAF0034708C00215E7700325F
      780033566900D7E0E40000000000000000000000000000000000C5B3AF00E876
      6600E67A7300DDB0AF00F8695C00F2CAC800F0A59F00F9EAE900F4999300E75F
      5500D16C590000000000000000000000000000000000000000008197AE00277A
      AD003F89BA003B7AAA00123E70008897B100C7C2BF00BEBABA00E1E1E300FFFF
      FF00FEEFAC00F9D94400FFFFFF00B47B0A000000000000000000000000000000
      000000000000AEAEAE00AEAEAE00AEAEAE00AEAEAE00AEAEAE00AEAEAE00AEAE
      AE00AEAEAE00AEAEAE00AEAEAE00AEAEAE009CADB600113D5100174156004D72
      90007998BE0089A4CC008AA5CD008AA5CD008CA6CF00819FC700527E9E00225F
      78002E677D00D9E2E60000000000000000000000000000000000E5DDDC00E37C
      6900FF847A00FE7B6F00FD796B00F4D5D300F8FBFC00F69D9500FF6B5E00FF7D
      7000DE8676000000000000000000000000000000000000000000000000007193
      B0001B5E92001152890055513600FFFFFF00C0BDBC00BEBCBB00BCBABA00B9B7
      BD00F5E18000E3B70000FFFFFF00B47C0B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099ABB40046606F00374E
      6300596F8C00768DB100809AC1007D96BC006981A2004A657F0040627500678E
      9E00B0C9D200000000000000000000000000000000000000000000000000C98B
      7E00FFA29400FF988A00FEA68F00EDD5D100F5C2B300FE927D00FF8C8000FD91
      7700EBBDB8000000000000000000000000000000000000000000000000000000
      00000000000000000000C8840300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B67E0F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000ADBB
      C00059717E00324B5D003A51670036506300536F7D0094ACB500D5E1E5000000
      000000000000000000000000000000000000000000000000000000000000ECDF
      DE00DC856D00FFCFB600FEE4C200F3DEBE00FFE8BF00FFDABA00FFB79800E583
      7300000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D2AA5C00B77F0E00B57C0900B57B0800B57B0900B57B
      0900B47B0900B57C0A00B67E0F00CDA85F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B7C5CA007F949E00B9C7CD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DA9B8E00EBB39500F6D1AB00F3C8A300EFA38900EEADA4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E1E4E600D2D9DE0000000000000000000000
      000000000000000000000000000000000000BB8A5E00BB8A5E00BB8A5E00BB8A
      5E00BB8A5E00BB8A5E00BB8A5E00BB8A5E00BB8A5E00BB8A5E00F3FBF400C4B7
      9C00CBAF8F00BB8A5E000000000000000000000000000000000000000000B4D1
      E00086BBD6006AABCD005DA3C800559EC400559EC3005EA4C8006BABCC0087BA
      D500B6D2E1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DBE0E400A0A5A800B4B4B500BDBDBD00A4AAAF00A7B5C0000000
      000000000000000000000000000000000000BC8B5E00FFFFFE00FFFCF700FFFE
      F500FFFEF600FFFBF400FFF7EE00FFF3E800FFF7EC00FFF8EA00F3FBF4004468
      4600DEE9DF00CCBDA400000000000000000000000000A8CBDD000D7BB300007A
      B900007EBF00007FC000007FC000007FC000007FC000007FC000007FC000007F
      C000007CBC000F7EB700AFD2E40000000000759BAF004A8EB1004A8EB1004A8E
      B1004A8EB1004A8EB1004A8EB1004A8EB1004A8EB1004A8EB1004A8EB1004A8E
      B1004A8EB1004A8EB1004A8EB100759BAF0000000000C2C2C200BFBFBF00BFBF
      BF00B3BABE00A1A5A700CBCBCB00B7C6B9008AB08F00D8D8D800C8C8C9008F9F
      AC00BFBFBF00BFBFBF00C0C0C00000000000BE8C5E00FFFFFE00FDFBF500FEFD
      F400FFFEF600FFFBF400F7F7F100EAEEE700E7ECE500E7ECE400EFF7EF002650
      280035663A00C3D2C200E4ECE4000000000000000000B0D2E4000079B700007C
      BC000080C2000080C3000080C3000080C3000080C3000080C3000080C3000080
      C300007EBF00007CBD00B2D6E90000000000579DC10086CFF00082CBED0082CB
      ED0082CBED0082CBED0082CBED0082CBED0082CBED0082CBED0082CBED0082CB
      ED0082CBED0082CBED0086CFF000579DC100B1CADC0077B3DF0075B3DF0075B3
      DF007594AA00C5C5C500D4D4D40092B7970029823400DCDCDC00DBDBDB00A4AC
      B3006AA7D30076B3DF0076B3E000B3CDE000BF8E5F00FFFFFE0078432B00FCFE
      F2007B432B007B432B00F5F9F5004A6C4B00547B5700547C5700517A54003B7E
      44004EA85D0033773C00A6BBA800E8F0E9000000000000000000127EB700007D
      BD000081C5000081C5000081C5000081C5000081C5000081C5000081C5000081
      C500007FC0000F83C000000000000000000064ABCF0086CFEE007DC8E8007DC8
      E8007DC8E8007DC8E8007DC8E8007DC8E8007DC8E8007DC8E8007DC8E8007DC8
      E8007DC8E8007DC8E80086CFEE0064ABCF00A6C9E3006FC0FA006EC0FC006EC0
      FC008BA0AF00C9CEC90056A4600044984F002B88370052975B0086B18B00CECE
      CE0066ACDD006EC0FC006FC0FA00A8CDE800C18F5F00FFFDFA00F7F9F400FDFE
      FA00FFFFFC00FFFFFC00F5F9F500214B23006DBF770072C67E006CC3790067C8
      780059C36B0047AD5A00296F3300839D850000000000000000009FC7DD00057A
      B700007FC1000081C4000081C5000082C6000082C6000082C6000082C6000081
      C4000481C0009ACCE400000000000000000069B0D4008AD3F00082CCEB0082CC
      EB0082CCEB0082CCEB0082CCEB0082CCEB0082CCEB0082CCEB0082CCEB0082CC
      EB0082CCEB0082CCEB008AD3F00069B0D40086BAE10044ADFA0041ADFB0041AD
      FB008AA9BF00DEE0DE00AAC9AE009CC0A00084B08900B5CCB800CCDACE00E0E0
      E0003E9DE00041ADFB0044ADFA0087BDE400C3916000FFFFFC0078462D00FCFE
      F3007B432B007B432B00F5F9F500234D250082E08E0082E08E007ADA87006DD1
      7C005DC66F004CB760002E7D3B0099AF9A000000000000000000000000000000
      00007DB9D9001C88C100238EC6005BA0C800519AC4001488C4001A8CC8007ABC
      DE00000000000000000000000000000000006DB5D8008FD7F20087D0ED0087D0
      ED0087D0ED0087D0ED0087D0ED0087D0ED0087D0ED0087D0ED0087D0ED0087D0
      ED0087D0ED0087D0ED008FD7F2006DB5D80072B1DE0032A5F8002FA5FA002FA5
      FA005CA1D300E1E1E100EBEBEB00CCDBCE009ABE9F00F2F2F200F3F3F300ACC2
      D3002DA1F5002FA5FA0032A5F80072B2E100C4926000FFFFFC00F7F8EF00FDFE
      F300FFFFFF00FFFFFF00F5F9F500244E26004D8E53004C8E53004B8E520058AA
      63005FC26F0040914D009FB7A200EFF6EF000000000000000000000000000000
      00000000000000000000C4DBEC00B5C7D800ADC0D300BBD4E600000000000000
      00000000000000000000000000000000000071B7DB0094DBF4008DD5F0008DD5
      F0008DD5F0008DD5F0008DD5F0008DD5F0008DD5F0008DD5F0008DD5F0008DD5
      F0008DD5F0008DD5F00094DBF40071B7DB0065AADC002CA2F60029A2F80029A2
      F8002AA0F5009DBED700ECECEC00ECEFED00E6EDE700F5F5F500DEE5EA003F9D
      E30029A2F80029A2F8002CA2F60064AADD00C7936100FFFEF6007A432B00FFFF
      F5007B432B007B432B00F5F9F500DDE5DD00DAD5CC00D7D5CC00CACDC2003062
      33003F7F4600ABC3AE00D4E0D500000000000000000000000000000000000000
      00000000000000000000C8D7E500ADC8E100A9C5DE00C7D5E100000000000000
      00000000000000000000000000000000000075BBDF0099E0F60092DAF30092DA
      F30092DAF30092DAF30092DAF30092DAF30092DAF30092DAF30092DAF30092DA
      F30092DAF30092DAF30099E0F60075BBDF00519FD700279FF400239EF600239E
      F600239EF600259DF4006CB2E600B3D1E900C2D9EA0094C4E900359EEC00239E
      F600239EF600239EF600279FF400519ED600C8956100FFFFFB00FFFFF500FFFF
      F500FFFFF500FFFFF500FAFCF400FCFDF400FDFEF500FDFEF500F9FBF1003459
      3500B3C5B400CCCFBE0000000000000000000000000000000000000000000000
      00000000000000000000BAD5EF00C0DBF400C3DDF400BFD6EB00000000000000
      00000000000000000000000000000000000079BEE2009FE5F90098DFF60098DF
      F60098DFF60098DFF60098DFF60098DFF60098DFF60098DFF60098DFF60098DF
      F60098DFF60098DFF6009FE5F90079BEE2004399D500219BF2001D9AF4001D9A
      F4001D9AF4001D9AF4001D9AF4001D9AF4001D9AF4001D9AF4001D9AF4001D9A
      F4001D9AF4001D9AF400219BF2004396D200CA966200FFFFFC007B432B00F7F2
      E8007B432B007B432B007B432B00FFFFFC007B432B007B432B0075432A009DB1
      9D00CFDACD00BB8D620000000000000000000000000000000000000000000000
      000000000000DFECF700C3DDF400D1E5F700D3E6F700CBE1F500EEF4FA000000
      0000000000000000000000000000000000007CC1E400A3E9FB009DE3F9009DE3
      F9009DE3F9009DE3F9009DE3F9009DE3F900A3E9FA00A3E9FA00A3E9FA00A3E9
      FA00A3E9FA00A3E9FA00A6ECFB007CC1E400308CCF001A96EF001796F1001796
      F1001796F1001796F1001796F1001796F1001796F1001796F1001796F1001796
      F1001796F1001796F1001A96EF002E8ACB00CC986200FFFBF800FFFFFC00FFFF
      FC00FFFFFC00FFFFFC00FFFFFC00FFFFFC00FFFFFC00FFFFFC00FCFDF900FCF3
      E900FFF9F300BB8A5E0000000000000000000000000000000000000000000000
      000000000000B8D3EA00CEE2F500DAE9F700DCEBF800D7E8F700CDE0F0000000
      0000000000000000000000000000000000007FC4E600A8EDFD00A2E7FB00A2E7
      FB00A2E7FB00A2E7FB00A2E7FB00ABF0FD008CD0ED0081C5E70081C5E70081C5
      E70081C5E70081C5E70081C5E7007FC4E6002084CC001392ED001092EF001092
      EF001092EF001092EF001092EF001092EF001092EF001092EF001092EF001092
      EF001092EF001092EF001392ED002080C700CD996300FFFFFC00FFFFFC00FFFF
      FC00FFFFFC00FFFFFC00FFFFFC00FFFFFC00FFFFFC00FFFFFC00D5D5D200C4C7
      C100DDDBDA00BB8A5E0000000000000000000000000000000000000000000000
      00000000000088A2C400D0E3F500DBEAF700E1EDF800DAE9F7009BB1CE000000
      00000000000000000000000000000000000081C6E900AEF3FF00ABF0FE00ABF0
      FE00ABF0FE00ABF0FE00AEF3FF008FD3EF008FD3EF00ABF0FE00ABF0FE00ABF0
      FE00ABF0FE00ABF0FE00AEF3FF0081C6E90097A9E5004B74DC00396DE100396D
      E100396DE100396DE100396DE100396DE100396DE100396DE100396DE100396D
      E100396DE100396DE1004B74DC0097A9E300CF9A6300FFFBF800FFF9F300FFF9
      F3000000A1000000A1000000A1000000A10000009E00FFFFFF00DDDBDA00DDDB
      DA00F5F7F800BB8A5E0000000000000000000000000000000000000000000000
      0000000000005F79A100CCE1F400D4E3F200CDDCEE00C0D2E90062769D000000
      000000000000000000000000000000000000A3D5EF0083C8EB0083C8EB0083C8
      EB0083C8EB0083C8EB0083C8EB0083C8EB00FEFEFD00F8F8F300F0F0E600E9E9
      DB00FEC94100F4B62E0083C8EB00A3D5EF00000000008DC7F00076C6FE0076C6
      FE0076C6FE006FBDF5006AB7EE006AB7EE006AB7EE006AB7EE006AB7EE006AB7
      EE006AB7EE006AB7EE008DC2EA0000000000D09C6400FFFBF800FEF5EF00FFFF
      F500FFFFF500FFFFF500FFFFF500FFFFF500FFFFF500FFFFF500E9E5D500F9FC
      FD00E7D7CA00D2B59E0000000000000000000000000000000000000000000000
      0000000000005E6F9400556D96002E4778002B4883002C4A85008392B1000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D3EBF70085C9EC0085C9EC0085C9EC0085C9
      EC0085C9EC0085C9EC00D3EBF700000000000000000096C8EC0074BCEF0074BC
      EF007ABBEA00D9E7F00000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D29D6400FFFFFF00FFFBF800FFFB
      F800FFFFFE00FFFFFE00FFFFFE00FFFFFE00FFFFFE00FFFFFF00EEEEEE00EDE0
      D800C9A78C000000000000000000000000000000000000000000000000000000
      000000000000D8DBE400374E7B00284379002F4E8D004E689E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D4A06700D19C6400CF9A6300CC98
      6200CA966200C8946100C4926000C2906000BF8E5F00BD8C5E00C2976F00CCAB
      9100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B9C0D100BFC7D90000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6D6D6009C9C9C009C9C9C00D5D5D500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D5D5D500C6C6C600B6B6
      B60081869000043464002F373A002F373A0000000000B4B4B4008B8B8B008B8B
      8B008B8B8B008B8B8B008B8B8B008B8B8B008B8B8B008B8B8B008B8B8B008B8B
      8B008B8B8B00B4B4B4000000000000000000BCBCBC009090900099999900A1A1
      A100A9A9A900AFAFAF00AAAAAA00B8B8B800B8B8B800ABABAB00BABABA00B5B5
      B500ADADAD00A6A6A6009F9F9F00C0C0C0000000000000000000000000000000
      0000C7C7C700CFCFCF0097979700BEBEBE00BEBEBE0097979700CFCFCF00C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C2C6
      CE0004346400053F7A00DDE1C0002F373A00000000007E7E7E00D4D4D400D4D4
      D400D4D4D400D4D4D400D4D4D400D4D4D400D4D4D400D4D4D400D4D4D400D4D4
      D400D4D4D4007E7E7E000000000000000000E4E4E4009D9D9D00ABABAB00B3B3
      B300BDBDBD00C6C6C600CFCFCF00D9D9D900D9D9D900D3D3D300D2D2D200CACA
      CA00C4C4C400BBBBBB00ACACAC00E6E6E6000000000000000000C7C7C7008F8F
      8F009B9B9B0085858500B4B4B400D6D6D600D6D6D600B6B6B600858585009B9B
      9B008F8F8F00C7C7C70000000000000000000000000000000000000000000000
      0000B9B6B1008B877E007E796D007F796F008D897F00BFBDB700C5CDD500013E
      7B00053F7A001764A5009FD7F400135C9C000000000090909000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0090909000000000000000000000000000AEAEAE00B5B5B500C3C3
      C300BFBFBF00C6C6C600C6C6C600C5C5C500C5C5C500C6C6C600C6C6C600C1C1
      C100BEBEBE00B2B2B200B8B8B800000000000000000000000000A5A5A500DBDB
      DB00E6E6E600C2C2C200BEBEBE00CACACA00CACACA00BEBEBE00C2C2C200E6E6
      E600DBDBDB00A5A5A50000000000000000000000000000000000D1CFCC00706A
      5E007B6C5900A98B6D00C8A27B00CAA47D00AB8F70007F705C005A585200273D
      5C0004326100CAF3FD00135C9C00000000000000000090909000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0090909000000000000000000000000000CDCDCD00BABABA00C5C5
      C500C8C8C800C5C5C500C9C9C900C6C6C600C8C8C800C7C7C700C7C7C700CACA
      CA00C4C4C400C0C0C000D3D3D3000000000000000000000000009B9B9B00EBEB
      EB00C4C4C400D6D6D600E7E7E700D3D3D300D1D1D100E7E7E700D6D6D600C4C4
      C400EBEBEB009B9B9B00000000000000000000000000D0CDCB00635D5000C09F
      7C00FDDFAB00FEFADE00FFFFF300FFFFF200FEF2DF00FED4A100B9997600635D
      5100273D5C00135C9C00C9D0DB00000000000000000090909000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0090909000000000000000000000000000EDEDED00C8C8C800CECE
      CE00CECECE00D0D0D000D0D0D000D3D3D300D5D5D500D3D3D300D2D2D200D1D1
      D100CFCFCF00CDCDCD00EFEFEF000000000000000000DBDBDB0098989800C5C5
      C500DFDFDF00D5D5D50094949400BCBCBC00BDBDBD0094949400D5D5D500DFDF
      DF00C5C5C50098989800DBDBDB0000000000000000006C675B00C8AA8400FEEC
      B900FFF6C300FEEDB400FDE9AF00FEEBB200FEF1BD00FEF0C100FEE0AF00B191
      71005B595100C5CBD30000000000000000000000000090909000FFFFFF00BBBB
      BB00D7D7D700BBBBBB00BBBBBB00D7D7D700BBBBBB00D7D7D700BBBBBB00BBBB
      BB00FFFFFF00909090000000000000000000000000000000000074818E005F78
      8F006E859A007A8FA3008398A900879AAA00889CAC00889EAE008199AB007591
      A50065849A007F8F9A0000000000000000000000000096969600B6B6B600C7C7
      C700F0F0F00094949400E7E7E7000000000000000000E7E7E70094949400F0F0
      F000C7C7C700B6B6B6009696960000000000ADA9A20091846B00FDDEAD00FCDD
      AD00FCD69F00FCD68E00FCD99200FCD88F00FBD7A200FCD8A600FCD8A600FDD8
      A900806F5B00B5B3AE0000000000000000000000000090909000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0090909000000000000000000000000000000000006F87A3003864
      9A00446DA400547BAE006388B6006C92BC006D95BE006493BD00558DBA003E82
      B4002775AC006F96B2000000000000000000C2C2C200CCCCCC00F7F7F700E9E9
      E900D8D8D800AFAFAF0000000000000000000000000000000000AFAFAF00D8D8
      D800E9E9E900F7F7F700CCCCCC00C2C2C200807C7000C2A17D00FBCB9A00F9BD
      8900F9C19000FACC9600F9CE9100F8CC9000F8C59300F8C39100FAC49100FDC8
      9600C599770089837A0000000000000000000000000090909000FFFFFF00BBBB
      BB00BBBBBB00D7D7D700BBBBBB00D7D7D700BBBBBB00BBBBBB00D7D7D700BBBB
      BB00FFFFFF0090909000000000000000000000000000000000007387A4002855
      97003666A9004676B5005081BB005587BF005588C0004D85BF004280BD003479
      BA00226EAE006F95B3000000000000000000C8C8C800CFCFCF00FCFCFC00F1F1
      F100DBDBDB00A2A2A20000000000000000000000000000000000A2A2A200DBDB
      DB00F1F1F100FCFCFC00CFCFCF00C8C8C8006A645600D8B08900F9BA8800F4B6
      8300F1B27E00EFB38200F0B58300EEB28000EEB07E00F0B28000F6BC8900F9B9
      8700DEAD8500706A5E0000000000000000000000000090909000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0090909000000000000000000000000000000000007689AA003364
      B200356FC500417ECF00468AD2004691D5004593D700428ED6003685D4002C7D
      CC00246CB1007293B20000000000000000000000000097979700C0C0C000CFCF
      CF00FFFFFF0096969600ADADAD000000000000000000ADADAD0096969600FFFF
      FF00CFCFCF00C0C0C0009797970000000000635D5000D6AA8400F8B98500EFAE
      7B00ECAA7800ECAA7800ECAA7800ECAA7800ECAA7800ECAA7800EEB07D00F5B5
      8200DAA67E006E695D0000000000000000000000000090909000FFFFFF00BBBB
      BB00BBBBBB00D7D7D700BBBBBB00BBBBBB00D7D7D700BBBBBB00D7D7D700BBBB
      BB00FFFFFF0090909000000000000000000000000000000000007389B0004071
      C6003D7CE4004893EF0053AAED005AB7EA005EBBE90062B6E8004096E4003085
      DF002974C3007192B70000000000000000000000000000000000A8A8A800D3D3
      D300FFFFFF00E7E7E70097979700868686008686860097979700E7E7E700FFFF
      FF00D3D3D300A8A8A800000000000000000077726700BC987800F5B58300F3B4
      8100EEAC7A00ECAA7800ECAA7800ECAA7800ECAA7800EDAB7800F0B07D00F0AF
      7F00BE987600807C710000000000000000000000000090909000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009090900000000000000000000000000000000000718AB3004172
      C700498AEA0053A5F0005CBCEE0060C9EB0065CDEA006DC7E9004FA4E2004189
      DD003274C7007392BA00000000000000000000000000000000009E9E9E00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00E9E9E900E9E9E900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00A0A0A0000000000000000000A3A098008E7F6800E9A87B00F9BC
      8800F6B88400F2B37F00F1B07D00F1B17E00F3B38000F4B68200F4B78400E09E
      75008F7E6600ADA9A20000000000000000000000000090909000FFFFFF00DBDB
      DB00EAEAEA00DBDBDB00EAEAEA00DBDBDB00EAEAEA00DBDBDB00EAEAEA00DBDB
      DB00FFFFFF009090900000000000000000000000000000000000738EBA004478
      D100599BEF005FB5F00068CCED006ADBEC006DDCEB0075D1E9005BADE100508F
      DC00417AD4007794C10000000000000000000000000000000000B3B3B300F3F3
      F300FFFFFF00D8D8D800D8D8D800FFFFFF00FFFFFF00D8D8D800D8D8D800FFFF
      FF00F3F3F300B3B3B3000000000000000000E7E6E400716D5C00AC846600EAAA
      7C00FBCF9E00FFFCF100FFFFFF00FFFFFF00FFFBEF00F8C39100DD9F7400A27D
      6100756F6000000000000000000000000000000000007E7E7E00FFFFFF007A7A
      7A00BCBCBC007A7A7A00B8B8B8007A7A7A00B8B8B8007A7A7A00B8B8B8007A7A
      7A00FFFFFF007E7E7E00000000000000000000000000000000007792C000477A
      D2005F9BE30068B6E90073CDE70077DBE7007ADCE6007ACFE30062ADDC00568F
      D600467AD1007B97C50000000000000000000000000000000000E5E5E500B8B8
      B8009E9E9E00BBBBBB00C4C4C400FFFFFF00FFFFFF00C4C4C400BBBBBB009E9E
      9E00B8B8B800E5E5E500000000000000000000000000B9B6B100665F5100AD84
      6600E7AC8100F8E0C700FFFDFB00FEFCFB00F6DCC100D8A07800A77F6200675F
      5200C6C5BF0000000000000000000000000000000000B7B7B7007A7A7A00C7C7
      C7007A7A7A00C7C7C7007A7A7A00C7C7C7007A7A7A00C7C7C7007A7A7A00C7C7
      C7007A7A7A00B7B7B70000000000000000000000000000000000D2D2D200DBDB
      DB00E4E4E400EAEAEA00EDEDED00EFEFEF00F0F0F000EFEFEF00EEEEEE00EAEA
      EA00E2E2E200D9D9D90000000000000000000000000000000000000000000000
      0000000000000000000097979700CFCFCF00CFCFCF0097979700000000000000
      0000000000000000000000000000000000000000000000000000C6C4BF006A66
      59007E6B5800AC836500C5906D00C38E6B00A98063007D6A57006E695D00CCCA
      C5000000000000000000000000000000000000000000DADADA007A7A7A00FFFF
      FF007A7A7A00FFFFFF007A7A7A00FFFFFF007A7A7A00FFFFFF007A7A7A00FFFF
      FF007A7A7A00DADADA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D1D1D100D1D1D10000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009E9A92006E695D00635D5000635D5000706A5E00A29F9600000000000000
      0000000000000000000000000000000000000000000000000000D1D1D1008989
      8900A9A9A90089898900A9A9A9008A8A8A00A9A9A90089898900A9A9A9008989
      8900D1D1D1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E1E1E1000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009F9F9F0000000000000000000000000000000000000000000000
      0000BFBFBF000000000000000000000000000000000089898900858585008181
      8100808080007D7D7D007E7E7E007E7E7E007E7D7D007E7E7E00828181000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D2D2D200BAB9B700ABA9A400908F8D008A898400B1B0
      AF00000000000000000000000000000000000000000000000000000000000000
      000000000000DFDFDF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000040404000000000000000000000000000000000000000000BFBF
      BF0004040400000000000000000000000000AFAEAD00D5D4D3008F8F8E00B0B0
      B0008C8C8C00B5B5B50095949400BCBCBC008E8D8D00AFB0AE00BDBBBC008F8F
      8E00CECECE00D5D6D600C3C3C300E5E5E5000000000000000000ECECEC00B8B8
      B700989898007D7D7D0071717100817F7B00A3A097009A99960093928D00A7A7
      9F00C8C8C6000000000000000000000000000000000000000000000000000000
      00009A9B9A00B5B4B4008F8F8F0092929100B1B1B100DDDDDD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000929292001515150000000000000000000000000000000000000000000000
      000008080800D1D1D1000000000000000000C6C6C600DBDBDA00C5C7C5009696
      9600C0BFC0009B9D9B00C5C5C50099999900B9B9B900C4C5C600D7D7D600B1AF
      B000999A9A008C8E900097989800929292000000000000000000E5E5E400B0AF
      AD0090908F008A8A89008F8F8F008C8B8B008A8A8900878684008D8C8900A1A2
      9D00C7C7C6000000000000000000000000000000000000000000000000009999
      9900C5C4C400C8C8C900C9C9CA00CACACA00C2C2C200B0AFAF00878887008686
      8600ADADAD00D5D5D5000000000000000000000000000000000000000000A3A3
      A3000D0D0D00A3A3A30000000000000000000000000000000000000000000000
      000048484800676767000000000000000000E7E7E600BCBBBB00CCCCCB00F7F7
      F600D5D5D500C7C7C700C7C7C700D5D5D500FBFBFC00CACACA00BDBDBD00D6D6
      D600CDCECE00A89B9900CDCECE00ACACAC00D2D1CF00A9A8A6009F9D9A00ABA9
      A600B2B1AF00D1D1D000B7B7B6009E9E9C0072727100525252007C7C7C00B4B3
      B100C1BDB700C3C1BE00000000000000000000000000000000009A9A9A00A2A2
      A3009999990095969500A3A3A300B3B4B300B3B3B300B1B1B100BDBDBD00D7D8
      D700DDDEDE00B8B9B80095959500979797000000000000000000878787001111
      1100A4A4A4000000000000000000000000000000000000000000000000000000
      0000C2C2C2001111110000000000000000000000000000000000000000009999
      9A004748480000000000000000004E4E4F00A7A7A7000000000000000000D8D9
      D800CCCFD000BCA79200C3C5C700A9A9A900C3C1BE00ADA8A300B0ACA700A5A2
      9E004948470080818200C5C5C500BCB7B20090837400816B5100C3A57B00D5AE
      7400DBAE6700C3B29700000000000000000000000000ACACAC00A9A8AA009598
      9B0084878A00818285007B7C7E007A7B7C00838483008E8F8E008C8D8D00B4B4
      B400E2E2E200E7E7E700E5E5E400EBEBEB00000000000000000016161600C3C3
      C300000000000000000000000000000000000000000000000000000000000000
      0000000000004E4E4E006C6C6C0000000000ACAEAE008E9193007E8385007074
      78006165690052565B0050565A005F6567006B6F7400777B7E007C7E7E007677
      7700D4D5D5009C939400CFD0D000A9A9A900CCCAC800B8AFA400B7B1A900B8B6
      B200666563006C513B00C58C5900CE894400D4873200DD903400E29C4000E5A5
      4800E7AC4F00D0B58B00000000000000000000000000949492008F7A5D008D7B
      62009688760091887D00918D8700909191008B8E900085898D007A7D80009B9C
      9C00CBCBCC00DEDEDE00000000000000000000000000D3D3D3001A1A1A00D3D3
      D300000000000000000000000000000000000000000000000000000000000000
      000000000000B7B7B7002727270000000000B1B3B500FDFCFB00F9F2EB00FCF6
      EF00FCF6F000FDF7F000FDF7F000FBF6EF00FAF4EE00F8F1EC00FCFBFA009EA0
      A200D4D5D60090919200DBDBDB00AAAAAA00D3D1CF0097959200A7A6A200C5C3
      BF00A8A8A600A2724C00D3722000D3762000D57B2000D8812100DA892500DE92
      2D00E29B3400D1A96A00EEEEEE000000000000000000B2B0AE00745836007C60
      3D008166420084684300755934006F573600745F440078685300877B6B00BDBB
      B800F5F5F5000000000000000000000000000000000000000000717171001F1F
      1F003A3A3A00D5D5D50000000000000000000000000000000000000000000000
      000000000000000000001F1F1F0000000000ADAEB000E5DAD200904F1D007B35
      0000823D0300823E04008845060086430500793500005D1B0000DBD1CC00A3A7
      AB00DDDEDE0091919100E1E1E100ABABAB00DBDBD800C9C7C400CAC8C500CBCA
      C700CFD0CD00C6AC9800D27A3600D1732300D3772100D57C2000D8812000DA87
      2000DD8C1F00D49E5000E8E6E3000000000000000000A8A49E00614623008269
      4900755E3D008A705000917856007E654200563B1900694E2A00AE916800D8D4
      CD00000000000000000000000000000000000000000000000000000000007474
      74002F2F2F000000000000000000000000000000000000000000000000000000
      00009F9F9F00000000002323230000000000ADAEAF00EAE2DC00EEE0D3007D3E
      0600894E1800864E1B008A4F1B0091521D0088481900692B0800DDD4CF00A4A8
      AC00E1E2E30093939300E6E6E600A7A7A700C7C6C50098979600878B8600BCBE
      B900DBDBD900D7CAC000DA9A6C00D7854500D6803700D7813000D8852D00DB8C
      3100DF953800D9A05400E3DDD5000000000000000000B0AAA200B69B7600BDA4
      8300B59C7C00A1886800AB927200A9906F00C1A88700D7BF9E00D0B49100D9D6
      D200000000000000000000000000000000000000000000000000000000003333
      33009F9F9F0000000000ADADAD00000000000000000079797900000000000000
      000028282800929292002828280000000000ABACAE00EAE0D600B7926A00975C
      1100985E1D008E571D008B521D00945821008F511F0071320B00DDD4CF00ABB0
      B500EEF0F2009E9E9E00F3F3F300AFAFAF009E9E9E005150500052585200B2B6
      B100E0E0DE00CFCAC400DAA78400DEA17400D9986400D4945D00D59B6400D7A5
      7000D9B07F00DBBE9800E8E5DF000000000000000000B6ADA000CFB59100D7BE
      9D00E4CBAA00D8BF9E00DBC2A100E4CBAA00E2C9A900D9BE9E00D3B89500E0DE
      DC00000000000000000000000000000000000000000000000000000000002C2C
      2C00CACACA00949494002C2C2C0000000000000000002C2C2C00CACACA00BCBC
      BC002C2C2C002C2C2C009393930000000000A3A5A600F0E9DD00D9CBAE00C094
      2300BD8F3100AA7A2C00956222008E541A00874A190076360C00E0D6D000AFB6
      BA00F4F6F600A5A5A500FEFEFE00B0B0B000A2A2A100575656005C625C00B1B6
      B000A7A7A6005A59580089796F00D2C2B600E9DFD600ECE5DF00F0ECE700F4F2
      F0000000000000000000000000000000000000000000B8AB9900C9AF8B00D4BA
      9A00DCC3A200D2B89800CFB69500D4BC9B00DCC3A300DAC29F00D8BF9E00E5E5
      E400000000000000000000000000000000000000000000000000000000007E7E
      7E0031313100313131004949490031313100313131003C3C3C00313131003131
      3100898989000000000000000000000000009F9FA200F0EADE00C0A66D00CBA5
      4300C89D4400BB8C4000AD7D3B00A5713500995C2300813C0500E1D6CE00ADB3
      B800C2C2C30081818100C4C4C400B5B5B500C8C8C6009494920091928F009797
      9500535352002D2D2D0037383800D0D0D0000000000000000000000000000000
      000000000000000000000000000000000000E7E7E800C2B29D00D1B79400DEC5
      A100DDC29D00D1B79100D2B79400D1B69200D5BA9800DBC19D00DAC2A300ECEC
      EC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000096959500F2EEE400F8F5EF00F4EE
      DF00F2EADE00F0E8DC00F0E7DC00EFE7DC00EFE6DB00EBE0D600E2D8D000A5AA
      AC000000000000000000000000000000000000000000B0B0B0003E3E3E002020
      20001F1F1F002E2E2E0037373700D0D0D0000000000000000000000000000000
      00000000000000000000000000000000000000000000CACAC900CAC8C600C9C4
      C000C9C4BC00C8BDB200CBBDAB00D4C2AA00D6C2A700DAC2A200DAC5A900F0F0
      F000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009B9B9C00A29F9A00A8A69E00ABAB
      AA00AEAEAE00B0AFB000B3B4B300B6B6B600B9B8B800BEBEBE00B0AFAD00AFB1
      B100000000000000000000000000000000000000000000000000C4C4C4005D5D
      5D00363636004343430060606000DCDCDC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EBECEC00E0E0E000D6D6D600D0CFCF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CACACA00DDDDDD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000D00000000100010000000000800600000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF80000000000000FFE0000000000000
      F000000000000000C00100000000000080010000000000008003000000000000
      0003000000000000000300000000000000030000000000000003000000000000
      0003000000000000000300000000000000070000000000008007000000000000
      C00F000000000000F03F000000000000FFE3FE00FFFFFFFFFFE3FE00FFF0FFF9
      FF80FE000000FFF1FF80FF010000FFE3C000C00100010000C003C00300030000
      8001800100070000000000000007000000000000000700000000000000070000
      8001800100070000C003C00300070000F00FF00F000F0000FFFFFFFF001F0000
      FFFFFFFF007F0000FFFFFFFF00FF0000FFFFFFFFE07FFFFFFFFFFFE7801FFE01
      FFFFFFE1800F80008001FF8080070000BFFDFF8180030000BFFDFF2780010001
      BFFDFF3F80010001BFFDFE7F80010003BFFDFE7F80010003BFFDFCFF80010007
      BFFDE4FF80010007BFFD81FF8001008F800101FF8001008FFFFF87FF8001018F
      FFFFE7FF8001FFFFFFFFFFFFC003FFFFFFFF3FFFF00FFFFFFFF01FFFC003F81F
      C0E08FFF8001E007E1E1C7FF8001C003C0E1E3FF0000C0038061F1FF00008001
      0001F8FF000080010001FC7F000080018003FE3F000080FFC007FF1F000080FF
      C00FFF8F000080FFE01FFFC70000C0FFF03FFFE38001C0FFF07FFFF18001E0FF
      F9FFFFF8C003F8FFFFFFFFFCF00FFFFFE003FFFFF800F80FE147FFFFF8010007
      8000F007F8010003E000E003800100010000E003800100010000E00380010000
      C000E00380010000C000E00380010000E000E003800100000007E00380010000
      4007E003800100004017E00380030000003FE00780070001003FFFFF801F000F
      013FFFFF803FE07F013FFFFF807FE07FF00FFFFFFFFF8001FF0FC7E3F00F0000
      DE0F83C1E0070000CE1F0180E0070000C43F0180F8030000C03F0180F8030000
      C07F8181F0070000C0078181F00F0000C00F8001E03F0000C01F8001E07F0000
      C03FC001E0FF0000C0FFC003E1FFC003C1FFE007C1FFC003C3FFE00787FFC003
      C7FFF00F87FFE007DFFFFFFFCFFFE007FC3F0FF00007F80FF81F01800007F80F
      F00F87E10003F007F00F80010003F007F00F80010001E003F00FC0030001C003
      E007C0030000C003C003C1870000C003C003E1870000F803C003E1070003F807
      E007F00F0001F81FF00FF00F00C1F87FF81FF01F81C0F8FFFC3FF81FF803F8FF
      FE7FF81FFC03F8FFFFFFF83FFF07FDFFFFFFFFFFFFF8FFFF8FFFFFFFFFF0FE7F
      8803F00FFFE0FC3F8FFFE007E001F81FFFFFC003C003F00FFFFFC0038007E007
      8FFFC0030007C0038803C0030007C0038FFFC0030007C003FFFFC0030007F00F
      FFFFC0030007F00F8FFFC0030007F00F8803E0070007F00F8FFFE007800FF81F
      FFFFFFFFC01FF81FFFFFFFFFE03FFFFF003FFFFFFEFFF81F003FFFFFF83FE007
      003F0080E00FF00F00008FE38007E007000000000003E007000000000003C007
      C00000000003C007E00000000003C003C00000000003C003C00000000003C003
      C00080000003C007C000F8000003C007C000F8000003C007E000FFFF8007E007
      FC00FFFFE01FE00FFC00FFFFF8FFF81FFFFFFE7F0003E007FFFFF81F00038001
      0000800100018001000000000000C003000000000000C003000000000000F00F
      000000000000FC3F000000000001FC3F000000000003FC3F000000000003F81F
      000000000003F81F000000000003F81F000000000003F81F000080010003F81F
      FE0183FF0007F83FFFFFFFFF000FFE7FFFFFFC3FFF8080030000F00FFFE08003
      0000C003F00080038001C003C00180038001C003800180038001800180038003
      C003818100038003C00303C000038003C00303C000038003C003818100038003
      C003C00300038003C003C00300038003C003C00300078003C003C00380078003
      C003FC3FC00F8003FFFFFE7FF03FC007FFFFFFFFFFDFFFFFFBF7801FFC0FFBFF
      FBE70000C007F03FF3F30000C007E003E3F300000003C000C7F3E06000038000
      CFF90000000380038FF9000000018007C3FD00000001800FE7F500000001800F
      E5B100000001800FE1810000000F800FE007000000FF000FFFFF000F80FF800F
      FFFF000FC0FFFE1FFFFFFFFFF3FFFFFF00000000000000000000000000000000
      000000000000}
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 105
    OnTimer = Timer1Timer
    Left = 912
    Top = 428
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 872
    Top = 428
  end
  object popMenu1: TPopupMenu
    Left = 892
    Top = 139
    object VincularNF1: TMenuItem
      Caption = 'Vincular NF-e'
      OnClick = VincularNF1Click
    end
  end
end
