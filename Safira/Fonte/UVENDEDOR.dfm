object FVENDEDOR: TFVENDEDOR
  Left = -10
  Top = 98
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsNone
  Caption = 'VENDEDOR'
  ClientHeight = 409
  ClientWidth = 792
  Color = 13421772
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object ImageGuiaPrincipal: TImage
    Left = 25
    Top = -1
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaPrincipalClick
  end
  object ImageGuiaExtra: TImage
    Left = 25
    Top = 76
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaExtraClick
  end
  object btFechar: TSpeedButton
    Left = 754
    Top = 6
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btFecharClick
  end
  object btMinimizar: TSpeedButton
    Left = 754
    Top = 32
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btMinimizarClick
  end
  object lbAjuda: TLabel
    Left = 649
    Top = 351
    Width = 50
    Height = 13
    Caption = 'Ajuda...'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btAjuda: TSpeedButton
    Left = 719
    Top = 336
    Width = 28
    Height = 38
    Flat = True
  end
  object PainelExtra: TPanel
    Left = 40
    Top = 27
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 0
    object ImagePainelExtra: TImage
      Left = 0
      Top = 14
      Width = 98
      Height = 352
    end
  end
  object PainelPrincipal: TPanel
    Left = 40
    Top = -13
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 1
    object ImagePainelPrincipal: TImage
      Left = 1
      Top = 14
      Width = 98
      Height = 352
    end
    object btNovo: TSpeedButton
      Left = 15
      Top = 25
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btNovoClick
    end
    object btSalvar: TSpeedButton
      Left = 15
      Top = 66
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSalvarClick
    end
    object btAlterar: TSpeedButton
      Left = 15
      Top = 107
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btAlterarClick
    end
    object btCancelar: TSpeedButton
      Left = 15
      Top = 148
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btCancelarClick
    end
    object btSair: TSpeedButton
      Left = 15
      Top = 311
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSairClick
    end
    object btRelatorio: TSpeedButton
      Left = 15
      Top = 270
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btRelatorioClick
    end
    object btExcluir: TSpeedButton
      Left = 15
      Top = 229
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btExcluirClick
    end
    object btPesquisar: TSpeedButton
      Left = 15
      Top = 188
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      Layout = blGlyphRight
      OnClick = btPesquisarClick
    end
  end
  object Notebook: TNotebook
    Left = 139
    Top = 0
    Width = 612
    Height = 337
    TabOrder = 3
    object TPage
      Left = 0
      Top = 0
      Caption = 'Default'
      object Bevel1: TBevel
        Left = 0
        Top = 0
        Width = 612
        Height = 337
        Align = alClient
        Shape = bsFrame
        Style = bsRaised
      end
      object LbCodigo: TLabel
        Left = 3
        Top = 29
        Width = 40
        Height = 13
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNome: TLabel
        Left = 3
        Top = 53
        Width = 33
        Height = 13
        Caption = 'Nome'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbSexo: TLabel
        Left = 3
        Top = 77
        Width = 29
        Height = 13
        Caption = 'Sexo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbEmail: TLabel
        Left = 3
        Top = 101
        Width = 36
        Height = 13
        Caption = 'E-mail'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbFone: TLabel
        Left = 3
        Top = 125
        Width = 27
        Height = 13
        Caption = 'Fone'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbCelular: TLabel
        Left = 3
        Top = 149
        Width = 41
        Height = 13
        Caption = 'Celular'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbDataCadastro: TLabel
        Left = 3
        Top = 173
        Width = 79
        Height = 13
        Caption = 'DataCadastro'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbDataNascimento: TLabel
        Left = 3
        Top = 197
        Width = 97
        Height = 13
        Caption = 'Data Nascimento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbAtivo: TLabel
        Left = 3
        Top = 221
        Width = 29
        Height = 13
        Caption = 'Ativo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbObservacao: TLabel
        Left = 3
        Top = 245
        Width = 68
        Height = 13
        Caption = 'Observa'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 185
        Top = 32
        Width = 165
        Height = 13
        Caption = 'C'#243'digo Cadastro Funcion'#225'rio'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 169
        Top = 224
        Width = 195
        Height = 13
        Caption = 'Faixa de Desconto para Comiss'#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object EdtCodigo: TEdit
        Left = 112
        Top = 29
        Width = 70
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object EdtNome: TEdit
        Left = 112
        Top = 53
        Width = 489
        Height = 19
        MaxLength = 100
        TabOrder = 2
      end
      object EdtEmail: TEdit
        Left = 112
        Top = 101
        Width = 398
        Height = 19
        MaxLength = 50
        TabOrder = 4
      end
      object EdtFone: TEdit
        Left = 112
        Top = 125
        Width = 198
        Height = 19
        MaxLength = 25
        TabOrder = 5
      end
      object EdtCelular: TEdit
        Left = 112
        Top = 149
        Width = 198
        Height = 19
        MaxLength = 25
        TabOrder = 6
      end
      object EdtDataCadastro: TMaskEdit
        Left = 112
        Top = 173
        Width = 120
        Height = 19
        EditMask = '!99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 7
        Text = '  /  /    '
      end
      object EdtDataNascimento: TMaskEdit
        Left = 112
        Top = 197
        Width = 121
        Height = 19
        EditMask = '!99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 8
        Text = '  /  /    '
      end
      object ComboSexo: TComboBox
        Left = 112
        Top = 75
        Width = 44
        Height = 21
        ItemHeight = 13
        TabOrder = 3
        Items.Strings = (
          'M'
          'F')
      end
      object ComboAtivo: TComboBox
        Left = 112
        Top = 221
        Width = 44
        Height = 21
        ItemHeight = 13
        TabOrder = 9
        Items.Strings = (
          'S'
          'N')
      end
      object MemoObservacao: TMemo
        Left = 112
        Top = 246
        Width = 493
        Height = 85
        TabOrder = 11
      end
      object edtcodigofuncionario: TEdit
        Left = 355
        Top = 29
        Width = 70
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 1
        OnExit = edtcodigofuncionarioExit
        OnKeyDown = edtcodigofuncionarioKeyDown
      end
      object edtfaixadesconto: TEdit
        Left = 370
        Top = 221
        Width = 70
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 10
        OnExit = edtfaixadescontoExit
        OnKeyDown = edtfaixadescontoKeyDown
      end
    end
  end
  object Guia: TTabSet
    Left = 141
    Top = 1
    Width = 607
    Height = 22
    BackgroundColor = 6710886
    DitherBackground = False
    EndMargin = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentBackground = True
    StartMargin = -3
    SelectedColor = 15921906
    Tabs.Strings = (
      '&1 - Principal')
    TabIndex = 0
    UnselectedColor = 13421772
    OnClick = GuiaClick
  end
end
