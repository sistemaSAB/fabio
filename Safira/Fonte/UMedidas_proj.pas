unit UMedidas_proj;

interface

uses
  db,uessencialglobal,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Grids, DBGrids,uobjmedidas_proj_PEDIDO;

type
  TFmedidas_proj = class(TForm)
    PanelComponentes: TPanel;
    Label5: TLabel;
    EdtAlturaComponente: TEdit;
    Label6: TLabel;
    edtlarguracomponente: TEdit;
    Label7: TLabel;
    ComboComponente: TComboBox;
    btcancelarcomponente: TBitBtn;
    edtcomponente: TEdit;
    edtcodigo: TEdit;
    pnlMedidasUnicasDuplas: TPanel;
    panelMedidaFrontal: TPanel;
    grpMedidasFrontais: TGroupBox;
    lb1: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    edtLarguraFrontal: TEdit;
    edtAlturaFrontal: TEdit;
    panelMedidaLateral: TPanel;
    grp1: TGroupBox;
    lb5: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    lb8: TLabel;
    edtlarguralateral: TEdit;
    edtalturalateral: TEdit;
    pnl1: TPanel;
    bt1: TBitBtn;
    bt2: TBitBtn;
    dbgridMedidas: TDBGrid;
    btgravarcomponente: TBitBtn;
    procedure btcancelarClick(Sender: TObject);
    procedure btokClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtAlturaFrontalKeyPress(Sender: TObject; var Key: Char);
    procedure btgravarcomponenteClick(Sender: TObject);
    procedure dbgridMedidasDblClick(Sender: TObject);
    procedure btcancelarcomponenteClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure lb10MouseLeave(Sender: TObject);
    procedure lb10MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lb9MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lb9MouseLeave(Sender: TObject);
  private
    { Private declarations }
    TMPObjMedidas_Pro:TobjMedidas_proj_PEDIDO;
    PcodigoPedidoProjeto:string;
  public
    { Public declarations }
    Function PassaObjeto(Pobjeto:tobjMedidas_Proj_PEDIDO):boolean;
  end;

var
  Fmedidas_proj: TFmedidas_proj;

implementation

uses UessencialLocal, UDataModulo;

{$R *.dfm}

procedure TFmedidas_proj.btcancelarClick(Sender: TObject);
begin
     Self.tag:=0;
     Self.Close;
end;

procedure TFmedidas_proj.btokClick(Sender: TObject);
begin
     Self.tag:=1;
     Self.Close;
end;

procedure TFmedidas_proj.FormShow(Sender: TObject);
begin
     Self.Tag:=0;

     if (Self.PanelMedidaFrontal.Visible=True)
     Then EdtAlturaFrontal.SetFocus;

     if (Self.PanelComponentes.Visible=True)
     Then EdtAlturaComponente.setfocus;


end;

procedure TFmedidas_proj.edtAlturaFrontalKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8,','])
     Then if Key='.'
          Then Key:=','
          Else Key:=#0;
end;

{Function TFmedidas_proj.PassaObjeto(Pobjeto: tobjMedidas_Proj):boolean;
begin
     Self.TMPObjMedidas_Pro:=Pobjeto;
     Self.DbGridMedidas.DataSource:=Self.TMPObjMedidas_Pro.ObjDatasource;
     Self.PcodigoPedidoProjeto:=Pobjeto.pedidoprojeto.get_codigo;
     edtcodigo.Enabled:=False;
     edtcodigo.TEXT:='';
     edtlarguracomponente.TEXT:='';
     EdtAlturaComponente.TEXT:='';
     edtcomponente.Enabled:=False;
     //preenchendo o combobox
     ComboComponente.Enabled:=False;
     ComboComponente.Items.clear;
     Self.TMPObjMedidas_Pro.PedidoProjeto.Projeto.RetornaComponentes(Pobjeto.pedidoprojeto.projeto.Get_Codigo,ComboComponente.Items);
     if (ComboComponente.Items.Count=0)
     then Begin
               Messagedlg('Nenhum componente foi selecionado para este projeto, para o tipo de eixo escolhido � necess�rio ter componentes para c�lculo do vidro',mterror,[mbok],0);
               exit;
     End;
     ComboComponente.ItemIndex:=0;
     edtcomponente.Text:=ComboComponente.Text;

     Self.TMPObjMedidas_Pro.RetornaMedidas(PcodigoPedidoProjeto);


     result:=true;
end;
}
Function TFmedidas_proj.PassaObjeto(Pobjeto: tobjMedidas_Proj_PEDIDO):boolean;
begin
     Self.TMPObjMedidas_Pro:=Pobjeto;
     Self.DbGridMedidas.DataSource:=Self.TMPObjMedidas_Pro.ObjDatasource;
     Self.PcodigoPedidoProjeto:=Pobjeto.pedidoprojeto.Get_Codigo;
     edtcodigo.Enabled:=False;
     edtcodigo.TEXT:='';
     edtlarguracomponente.TEXT:='';
     EdtAlturaComponente.TEXT:='';
     edtcomponente.Enabled:=False;
     
     //preenchendo o combobox
     ComboComponente.Enabled:=False;
     ComboComponente.Items.clear;
     Self.TMPObjMedidas_Pro.PedidoProjeto.Projeto.RetornaComponentes(Pobjeto.pedidoprojeto.projeto.Get_Codigo,ComboComponente.Items);
     if (ComboComponente.Items.Count=0)
     then Begin
               Messagedlg('Nenhum componente foi selecionado para este projeto, para o tipo de eixo escolhido � necess�rio ter componentes para c�lculo do vidro',mterror,[mbok],0);
               exit; 
     End;
     ComboComponente.ItemIndex:=0;
     edtcomponente.Text:=ComboComponente.Text;

     Self.TMPObjMedidas_Pro.RetornaMedidas(PcodigoPedidoProjeto);


     result:=true;
end;


procedure TFmedidas_proj.btgravarcomponenteClick(Sender: TObject);
var
paltura,plargura,PalturaArredondamento,PlarguraArredondamento:currency;
begin
     //gravando na tabela de medidas

     if (ComboComponente.Text='')
     and (edtcodigo.text='')
     then exit;

     With Self.TMPObjMedidas_Pro do
     begin
          ZerarTabela;
          if (edtcodigo.Text='')
          Then Begin
                   status:=dsinsert;
                   Submit_CODIGO(Get_NovoCodigo);
                   Submit_Componente(ComboComponente.Text);
          End
          Else Begin
                    if (LocalizaCodigo(edtcodigo.text)=false)
                    Then begin
                              Messagedlg('Componente n�o encontrado para altera��o!',mterror,[mbok],0);
                              exit;
                    End;
                    TabelaparaObjeto;
                    Status:=dsedit;
                    Submit_Componente(edtcomponente.Text);
          End;
          PedidoProjeto.Submit_Codigo(self.PcodigoPedidoProjeto);

          Submit_Altura(EdtAlturaComponente.Text);
          Submit_Largura(edtlarguracomponente.text);
          
          Try
             Plargura:=strtocurr(edtlarguracomponente.text);
             PlarguraArredondamento:=0;
             paltura:=strtocurr(EdtAlturaComponente.Text);
             PalturaArredondamento:=0;
          Except
                Messagedlg('Medidas Inv�lidas',mterror,[mbok],0);
                exit;
          End;

          if (FDataModulo.UtilizaArredondamentoGlobal5cm)
          Then Begin
                     FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
                     FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);
          End;

          Submit_AlturaArredondamento(currtostr(PalturaArredondamento));
          Submit_larguraArredondamento(currtostr(PlarguraArredondamento));

          if (salvar(false)=False)
          Then Begin
                  edtalturacomponente.setfocus;
                  exit;
          End;


          EdtAlturaComponente.Text:='';
          edtlarguracomponente.Text:='';

          if (edtcodigo.text='')
          Then Begin
                    if   (ComboComponente.ItemIndex<>(ComboComponente.Items.count-1))
                    Then  ComboComponente.ItemIndex:=ComboComponente.ItemIndex+1
                    Else  ComboComponente.Text:='';
          End;
          edtcodigo.text:='';
          edtcomponente.text:=ComboComponente.Text;

          RetornaMedidas(PcodigoPedidoProjeto);
          formatadbgrid(DbGridMedidas);
          EdtAlturaComponente.setfocus;
     End;
     
end;

procedure TFmedidas_proj.dbgridMedidasDblClick(Sender: TObject);
begin
     if (Self.DbGridMedidas.DataSource.DataSet.Active=False)
     Then exit;

     if (Self.DbGridMedidas.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Self.TMPObjMedidas_Pro.LocalizaCodigo(Self.DbGridMedidas.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               messagedlg('Componente n�o encontrado na tabela de componentes do pedido',mterror,[mbok],0);
               exit;
     End;

     Self.TMPObjMedidas_Pro.TabelaparaObjeto;

     edtcodigo.Text:=Self.TMPObjMedidas_Pro.get_codigo;
     edtcomponente.Text:=Self.TMPObjMedidas_Pro.Get_Componente;
     edtlarguracomponente.text:=Self.TMPObjMedidas_Pro.Get_Largura;
     EdtAlturaComponente.Text:=Self.TMPObjMedidas_Pro.Get_Altura;
     EdtAlturaComponente.setfocus;



end;

procedure TFmedidas_proj.btcancelarcomponenteClick(Sender: TObject);
begin
     edtcodigo.Text:='';
     edtcomponente.Text:='';
     EdtAlturaComponente.Text:='';
     edtlarguracomponente.Text:='';
     edtcomponente.Text:=ComboComponente.Text;
end;

procedure TFmedidas_proj.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Perform(Wm_NextDlgCtl,0,0); 
end;

procedure TFmedidas_proj.FormCreate(Sender: TObject);
begin
ColocaUpperCaseEdit(Self);
end;

procedure TFmedidas_proj.lb10MouseLeave(Sender: TObject);
begin
       TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFmedidas_proj.lb10MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
        TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFmedidas_proj.lb9MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFmedidas_proj.lb9MouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

end.
