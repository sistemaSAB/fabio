unit UGRUPOPERSIANA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjPERSIANAGrupoDiametroCor,
  UessencialGlobal, Tabs,UPLanodeContas;

type
  TFGRUPOPERSIANA = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    edtReferencia: TEdit;
    lbLbReferencia: TLabel;
    edtNome: TEdit;
    lbLbNome: TLabel;
    lb5: TLabel;
    edtPlanodeContas: TEdit;
    grp1: TGroupBox;
    rbRadioControlaMetroQuadradoSIM: TRadioButton;
    rbRadioControlaMetroQuadradoNAo: TRadioButton;
    grp2: TGroupBox;
    lbLbPorcentagemInstalado: TLabel;
    lbLbPorcentagemFornecido: TLabel;
    lbLbPorcentagemRetirado: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    edtPorcentagemRetirado: TEdit;
    edtPorcentagemFornecido: TEdit;
    edtPorcentagemInstalado: TEdit;
    lbNomePlanoDeContas: TLabel;
    btPrimeiro: TSpeedButton;
    btAnterior: TSpeedButton;
    btProximo: TSpeedButton;
    btUltimo: TSpeedButton;

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    
    procedure SpeedButton1Click(Sender: TObject);
    procedure edtPlanodeContasExit(Sender: TObject);
    procedure edtPlanodeContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure edtPlanodeContasDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
  private
    ObjPERSIANAGrupoDiametroCor:TObjPERSIANAGrupoDiametroCor;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FGRUPOPERSIANA: TFGRUPOPERSIANA;


implementation

uses Upesquisa, UessencialLocal, UMenuRelatorios, UDataModulo,
  UescolheImagemBotao;

{$R *.dfm}


procedure TFGRUPOPERSIANA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjPERSIANAGrupoDiametroCor.GRUPOPERSIANA=Nil)
     Then exit;

     If (Self.ObjPERSIANAGrupoDiametroCor.GRUPOPERSIANA.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjPERSIANAGrupoDiametroCor.free;
    Action := caFree;
    Self := nil;
end;


procedure TFGRUPOPERSIANA.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbCodigo.caption:='0';
     //edtcodigo.text:=ObjPERSIANAGrupoDiametroCor.Grupo.Get_novocodigo;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjPERSIANAGrupoDiametroCor.GrupoPersiana.status:=dsInsert;
      btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

     EdtReferencia.setfocus;

end;

procedure TFGRUPOPERSIANA.btSalvarClick(Sender: TObject);
begin

     If ObjPERSIANAGrupoDiametroCor.GrupoPersiana.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjPERSIANAGrupoDiametroCor.GrupoPersiana.salvar(true)=False)
     Then exit;

     lbCodigo.caption:=ObjPERSIANAGrupoDiametroCor.GrupoPersiana.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFGRUPOPERSIANA.btAlterarClick(Sender: TObject);
begin
    If (ObjPERSIANAGrupoDiametroCor.GrupoPersiana.Status=dsinactive) and (lbCodigo.caption<>'')
    Then Begin
                habilita_campos(Self);

                ObjPERSIANAGrupoDiametroCor.GrupoPersiana.Status:=dsEdit;
                EdtReferencia.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                 btnovo.Visible :=false;
               btalterar.Visible:=false;
               btpesquisar.Visible:=false;
               btrelatorio.Visible:=false;
               btexcluir.Visible:=false;
               btsair.Visible:=false;
               btopcoes.Visible :=false;
          End;

end;

procedure TFGRUPOPERSIANA.btCancelarClick(Sender: TObject);
begin
     ObjPERSIANAGrupoDiametroCor.GrupoPersiana.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     lbCodigo.caption:='';
end;

procedure TFGRUPOPERSIANA.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjPERSIANAGrupoDiametroCor.GrupoPersiana.Get_pesquisa,ObjPERSIANAGrupoDiametroCor.GrupoPersiana.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjPERSIANAGrupoDiametroCor.GrupoPersiana.status<>dsinactive
                                  then exit;

                                  If (ObjPERSIANAGrupoDiametroCor.GrupoPersiana.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjPERSIANAGrupoDiametroCor.GrupoPersiana.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFGRUPOPERSIANA.btExcluirClick(Sender: TObject);
begin
     If (ObjPERSIANAGrupoDiametroCor.GrupoPersiana.status<>dsinactive) or (lbCodigo.caption='')
     Then exit;

     If (ObjPERSIANAGrupoDiametroCor.GrupoPersiana.LocalizaCodigo(lbCodigo.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjPERSIANAGrupoDiametroCor.GrupoPersiana.exclui(lbCodigo.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFGRUPOPERSIANA.btRelatorioClick(Sender: TObject);
begin
///    ObjPERSIANAGrupoDiametroCor.GrupoPersiana.Imprime(lbCodigo.caption);
end;

procedure TFGRUPOPERSIANA.btSairClick(Sender: TObject);
begin
    Self.close;
end;


procedure TFGRUPOPERSIANA.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFGRUPOPERSIANA.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFGRUPOPERSIANA.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;


end;

procedure TFGRUPOPERSIANA.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFGRUPOPERSIANA.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjPERSIANAGrupoDiametroCor.GrupoPersiana do
    Begin
        Submit_Codigo(lbCodigo.caption);
        Submit_Referencia(edtReferencia.text);
        Submit_Nome(edtNome.text);

        if (rbRadioControlaMetroQuadradoSIM.Checked = true)then
        Submit_ControlaPorMetroQuadrado('S')
        else if (rbRadioControlaMetroQuadradoNAo.Checked = true)then
        Submit_ControlaPorMetroQuadrado('N');

        Submit_PorcentagemInstalado(EdtPorcentagemInstalado.Text);
        Submit_PorcentagemFornecido(EdtPorcentagemFornecido.Text);
        Submit_PorcentagemRetirado(EdtPorcentagemRetirado.Text);

        PlanoDeContas.Submit_CODIGO(EdtPlanodeContas.Text);        


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFGRUPOPERSIANA.LimpaLabels;
begin
     lbNomePlanoDeContas.Caption:='';
end;

function TFGRUPOPERSIANA.ObjetoParaControles: Boolean;
begin
  Try
     With ObjPERSIANAGrupoDiametroCor.GrupoPersiana do
     Begin
        lbCodigo.caption:=Get_Codigo;
        EdtReferencia.text:=Get_Referencia;
        EdtNome.text:=Get_Nome;

        if (Get_ControlaPorMetroQuadrado = 'S')then
        rbRadioControlaMetroQuadradoSIM.Checked:=true
        else if (Get_ControlaPorMetroQuadrado = 'N')then
        rbRadioControlaMetroQuadradoNAO.Checked:=true;

        EdtPorcentagemInstalado.Text:=Get_PorcentagemInstalado;
        EdtPorcentagemFornecido.Text:=Get_PorcentagemFornecido;
        EdtPorcentagemRetirado.Text:=Get_PorcentagemRetirado;
        EdtPlanodeContas.Text:=PlanoDeContas.Get_CODIGO;
        lbNomePlanoDeContas.Caption:='TIPO = '+ObjPERSIANAGrupoDiametroCor.GrupoPersiana.PlanoDeContas.Get_Tipo+' | ';
        lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' CLASSIF. = '+ObjPERSIANAGrupoDiametroCor.GrupoPersiana.PlanoDeContas.Get_Classificacao+' | ';
        lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' NOME = '+ObjPERSIANAGrupoDiametroCor.GrupoPersiana.PlanoDeContas.Get_Nome;
        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFGRUPOPERSIANA.TabelaParaControles: Boolean;
begin
     If (ObjPERSIANAGrupoDiametroCor.GrupoPersiana.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;


procedure TFGRUPOPERSIANA.SpeedButton1Click(Sender: TObject);
begin

     With FmenuRelatorios do
     Begin
          //era no titulo antes
          NomeObjeto:='UObjGrupoPersiana';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Atualiza Margens');
                Items.Add('Atualiza Pre�os');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0 :  ObjPERSIANAGrupoDiametroCor.AtualizaMargens(lbCodigo.caption);
                1 :  ObjPERSIANAGrupoDiametroCor.AtualizaPrecos(lbCodigo.caption);
          End;
     end;

end;

procedure TFGRUPOPERSIANA.edtPlanodeContasExit(Sender: TObject);
begin
    ObjPERSIANAGrupoDiametroCor.GrupoPersiana.EdtGrupoPlanoDeContasExit(Sender, lbNomePlanoDeContas);
    lbNomePlanoDeContas.Caption:='TIPO = '+ObjPERSIANAGrupoDiametroCor.GrupoPersiana.PlanoDeContas.Get_Tipo+' | ';
    lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' CLASSIF. = '+ObjPERSIANAGrupoDiametroCor.GrupoPersiana.PlanoDeContas.Get_Classificacao+' | ';
    lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' NOME = '+ObjPERSIANAGrupoDiametroCor.GrupoPersiana.PlanoDeContas.Get_Nome;

end;

procedure TFGRUPOPERSIANA.edtPlanodeContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjPERSIANAGrupoDiametroCor.GrupoPersiana.EdtGrupoPlanoDeContasKeyDown(Sender, Key, Shift, lbNomePlanoDeContas);
end;

procedure TFGRUPOPERSIANA.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjPERSIANAGrupoDiametroCor.GrupoPersiana.PrimeiroRegistro = false)then
    exit;

    ObjPERSIANAGrupoDiametroCor.GrupoPersiana.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOPERSIANA.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigo.caption='')then
   lbCodigo.caption:='0';

    if  (ObjPERSIANAGrupoDiametroCor.GrupoPersiana.RegistoAnterior(StrToInt(lbCodigo.caption)) = false)then
    exit;

    ObjPERSIANAGrupoDiametroCor.GrupoPersiana.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOPERSIANA.btProximoClick(Sender: TObject);
begin
    if (lbCodigo.caption='')then
    lbCodigo.caption:='0';

    if  (ObjPERSIANAGrupoDiametroCor.GrupoPersiana.ProximoRegisto(StrToInt(lbCodigo.caption)) = false)then
    exit;

    ObjPERSIANAGrupoDiametroCor.GrupoPersiana.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOPERSIANA.btUltimoClick(Sender: TObject);
begin
    if  (ObjPERSIANAGrupoDiametroCor.GrupoPersiana.UltimoRegistro = false)then
    exit;

    ObjPERSIANAGrupoDiametroCor.GrupoPersiana.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOPERSIANA.edtPlanodeContasDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FplanoContas:TFplanodeContas;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FplanoContas:=TFplanodeContas.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabplanodecontas','',FplanoContas)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtPlanodeContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbNomePlanodeContas.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FplanoContas);

     End;
end;

procedure TFGRUPOPERSIANA.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     ColocaUpperCaseEdit(Self);

     Try
        Self.ObjPERSIANAGrupoDiametroCor:=TObjPERSIANAGrupoDiametroCor.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

    FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
    FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
    lbCodigo.caption:='';

     habilita_botoes(Self);
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE GRUPO DE PERSIANA')=False)
     Then desab_botoes(Self);
     if(Tag<>0)
    then begin
         if(ObjPERSIANAGrupoDiametroCor.GrupoPersiana.LocalizaCodigo(IntToStr(Tag))=True)then
         begin
              ObjPERSIANAGrupoDiametroCor.GrupoPersiana.TabelaparaObjeto;
              self.ObjetoParaControles;
         end;

    end;
end;

procedure TFGRUPOPERSIANA.btopcoesClick(Sender: TObject);
begin
  With FmenuRelatorios do
  Begin
    NomeObjeto:='UOBJGRUPOFERRAGEM';

    With RgOpcoes do
    Begin
      items.clear;
      Items.Add('Atualiza Pre�o de Custo');
      Items.Add('Atualiza Margens');
    End;

    showmodal;

    If (Tag=0)//indica botao cancel ou fechar
    Then exit;

    Case RgOpcoes.ItemIndex of
      0 : ObjPERSIANAGrupoDiametroCor.AtualizaPrecos(lbCodigo.Caption);
      1 : ObjPERSIANAGrupoDiametroCor.AtualizaMargens(lbCodigo.Caption);
    End;
  end;
end;

end.
