unit UobjSERVICO_PP_Pedido;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJSERVICO;

Type
   TObjSERVICO_PP_PEDIDO=class

          Public
                ObjDataSource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                //PedidoProjeto:TOBJPEDIDO_PROJ;
                Servico:TOBJSERVICO;
                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure ResgataSERVICO_PP(PPedido, PPEdidoProjeto: string);


                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Quantidade(parametro: string);
                Function Get_Quantidade: string;
                Procedure Submit_Valor(parametro: string);
                Function Get_Valor: string;
                Function Get_ValorFinal: string;
                Function Get_PedidoProjeto:string;
                Procedure Submit_PedidoProjeto(parametro:string);

                {Rodolfo==}
                procedure Submit_Altura(parametro: string);
                procedure Submit_Largura(parametro: string);
                procedure Submit_ControlaPorMilimetro(parametro: string);
                procedure Submit_controlaM2(parametro:string);
                function Get_controlaM2:string;
                Function Get_Altura:string;
                Function Get_Largura:string;
                Function Get_ControlaPorMilimetro:string;
                {==Rodolfo}

                procedure EdtServicoExit(Sender: TObject;Var PEdtCodigo:TEdit;LABELNOME:TLABEL);
                procedure EdtServicoKeyDown(Sender: TObject; Var PEdtCodigo:TEdit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                Function DeletaServico_PP(PPedidoProjeto:string):Boolean;
                Function PreencheServicoPP(PPedidoProjeto:string;var PValor:Currency;PprojetoemBRanco:Boolean;Paltura,Plargura:string):Boolean;

                Function Soma_por_PP(PpedidoProjeto:string):Currency;
                Procedure RetornaDadosRel(PpedidoProjeto:string;STrDados:TStrings;ComValor:Boolean);
                Procedure RetornaDadosRel2(PpedidoProjeto:string;STrDados:TStrings;ComValor:Boolean);
                Procedure RetornaDadosRelProjetoBranco(PpedidoProjeto:string;STrDados:TStrings; Pquantidade:integer;Var PTotal:Currency);

         Private
               Objquery:Tibquery;
               ObjQueryPesquisa:TIBquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Quantidade:string;
               Valor:string;
               ValorFinal:string;
               PedidoProjeto:string;
               ParametroPesquisa:TStringList;

               {Rodolfo}
               Altura : string;
               Largura : string;
               ControlaPorMilimetro : string;
               CONTROLAM2:string;
               {Rodolfo}

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UCalculaformula_Perfilado;

Function  TObjSERVICO_PP_PEDIDO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.PedidoProjeto:=fieldbyname('PedidoProjeto').asstring;

        If(FieldByName('Servico').asstring<>'')
        Then Begin
                 If (Self.Servico.LocalizaCodigo(FieldByName('Servico').asstring)=False)
                 Then Begin
                          Messagedlg('Servico N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Servico.TabelaparaObjeto;
        End;
        Self.Quantidade:=fieldbyname('Quantidade').asstring;
        Self.Valor:=fieldbyname('Valor').asstring;
        Self.ValorFinal:=fieldbyname('ValorFinal').asstring;

        Self.Altura := FieldbyName('Altura').Asstring; {Rodolfo}
        Self.Largura := FieldbyName('Largura').Asstring;  {Rodolfo}
        Self.ControlaPorMilimetro := FieldbyName('ControlaPorMilimetro').Asstring; {Rodolfo}
        CONTROLAM2:=fieldbyname('CONTROLAM2').AsString;
        
        result:=True;
     End;
end;


Procedure TObjSERVICO_PP_PEDIDO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:= Self.Codigo;
        ParamByName('PedidoProjeto').asstring:= Self.PedidoProjeto;
        ParamByName('Servico').asstring:= Self.Servico.GET_CODIGO;
        ParamByName('Quantidade').asstring:= virgulaparaponto(Self.Quantidade);
        ParamByName('Valor').asstring:= virgulaparaponto(Self.Valor);
        ParamByName('Altura').AsString := Self.Altura; {Rodolfo}
        ParamByName('Largura').asString := self.Largura;  {Rodolfo}
        ParamByName('ControlaPorMilimetro').AsString := self.ControlaPorMilimetro; {Rodolfo}
        ParamByName('CONTROLAM2').AsString:=Self.CONTROLAM2;
  End;
End;

//***********************************************************************

function TObjSERVICO_PP_PEDIDO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjSERVICO_PP_PEDIDO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        PedidoProjeto:='';
        Servico.ZerarTabela;
        Quantidade:='';
        Valor:='';
        valorFinal:='';
        Altura := '';  {Rodolfo}
        Largura := ''; {Rodolfo}
        CONTROLAM2:='';
     End;
end;

Function TObjSERVICO_PP_PEDIDO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjSERVICO_PP_PEDIDO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Servico.LocalizaCodigo(Self.Servico.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Servico n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjSERVICO_PP_PEDIDO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.PedidoProjeto<>'')
        Then Strtoint(Self.PedidoProjeto);
     Except
           Mensagem:=mensagem+'/PedidoProjeto';
     End;
     try
        If (Self.Servico.Get_Codigo<>'')
        Then Strtoint(Self.Servico.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Servico';
     End;
     try
        Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjSERVICO_PP_PEDIDO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjSERVICO_PP_PEDIDO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjSERVICO_PP_PEDIDO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro SERVICO_PP vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,PedidoProjeto,Servico,Quantidade,Valor,ValorFinal');
           SQL.Add(', Altura, Largura, ControlaPorMilimetro,CONTROLAM2');  {Rodolfo}
           SQL.ADD(' from  TABSERVICO_PP');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjSERVICO_PP_PEDIDO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjSERVICO_PP_PEDIDO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjSERVICO_PP_PEDIDO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDataSource.DataSet:=Self.ObjQueryPesquisa;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        //Self.PedidoProjeto:=TOBJPEDIDO_PROJ.create;
        Self.Servico:=TOBJSERVICO.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABSERVICO_PP(Codigo,PedidoProjeto,Servico');
                InsertSQL.add(' ,Quantidade,Valor, Altura, Largura, ControlaPorMilimetro,CONTROLAM2)');   {Rodolfo}
                InsertSQL.add('values (:Codigo,:PedidoProjeto,:Servico,:Quantidade');
                InsertSQL.add(' ,:Valor, :Altura, :Largura, :ControlaPorMilimetro,:CONTROLAM2)');    {Rodolfo}
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABSERVICO_PP set Codigo=:Codigo,PedidoProjeto=:PedidoProjeto');
                ModifySQL.add(',Servico=:Servico,Quantidade=:Quantidade,Valor=:Valor');
                ModifySQl.Add(', Altura=:Altura, Largura=:Largura, ControlaPorMilimetro=:ControlaPorMilimetro'); {Rodolfo}
                ModifySQL.add(',CONTROLAM2=:CONTROLAM2 where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABSERVICO_PP where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjSERVICO_PP_PEDIDO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjSERVICO_PP_PEDIDO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabSERVICO_PP');
     Result:=Self.ParametroPesquisa;
end;

function TObjSERVICO_PP_PEDIDO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de SERVICO_PP ';
end;


function TObjSERVICO_PP_PEDIDO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENSERVICO_PP,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENSERVICO_PP,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjSERVICO_PP_PEDIDO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(Self.ObjQueryPesquisa);
    Freeandnil(Self.ObjDataSource);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //Self.PedidoProjeto.FREE;
    Self.Servico.FREE;
//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjSERVICO_PP_PEDIDO.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjSERVICO_PP_PEDIDO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjSERVICO_PP_PEDIDO.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;

function TObjSERVICO_PP_PEDIDO.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjSERVICO_PP_PEDIDO.Submit_Quantidade(parametro: string);
begin
        Self.Quantidade:=Parametro;
end;

function TObjSERVICO_PP_PEDIDO.Get_Quantidade: string;
begin
        Result:=Self.Quantidade;
end;

procedure TObjSERVICO_PP_PEDIDO.Submit_Valor(parametro: string);
begin
        Self.Valor:=Parametro;
end;

function TObjSERVICO_PP_PEDIDO.Get_Valor: string;
begin
        Result:=Self.Valor;
end;

{Rodolfo==}
procedure TObjSERVICO_PP_PEDIDO.Submit_Altura(parametro: string);
begin
        Self.Altura:=Parametro;
end;
function TObjSERVICO_PP_PEDIDO.Get_Altura: string;
begin
        Result:=Self.Altura;
end;

procedure TObjSERVICO_PP_PEDIDO.Submit_Largura(parametro: string);
begin
        Self.Largura:=Parametro;
end;
function TObjSERVICO_PP_PEDIDO.Get_Largura: string;
begin
        Result:=Self.Largura;
end;

procedure TObjSERVICO_PP_PEDIDO.Submit_ControlaPorMilimetro(parametro: string);
begin
        Self.ControlaPorMilimetro:=Parametro;
end;
function TObjSERVICO_PP_PEDIDO.Get_ControlaPorMilimetro: string;
begin
        Result:=Self.ControlaPorMilimetro;
end;

procedure TObjSERVICO_PP_PEDIDO.Submit_controlaM2(parametro:string);
begin
  Self.CONTROLAM2:=parametro;
end;

function TObjSERVICO_PP_PEDIDO.Get_controlaM2:string;
begin
  Result:=CONTROLAM2;
end;

{==Rodolfo}
//CODIFICA GETSESUBMITS

procedure TObjSERVICO_PP_PEDIDO.EdtServicoExit(Sender: TObject;Var PEdtCodigo:TEdit;  LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Servico.localizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;

     Self.Servico.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Servico.Get_Descricao;
     PEdtCodigo.Text:=Self.Servico.Get_Codigo;

End;

procedure TObjSERVICO_PP_PEDIDO.EdtServicoKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Servico.Get_Pesquisa,Self.Servico.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 LABELNOME.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('Descricao').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjSERVICO_PP_PEDIDO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJSERVICO_PP';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjSERVICO_PP_PEDIDO.ResgataSERVICO_PP(PPedido, PPEdidoProjeto: string);
begin
       With Self.ObjQueryPesquisa do
       Begin
           Close;
           SQL.Clear;
           Sql.add('Select TabServico.Referencia,TabServico.Descricao as Servico,');
           Sql.add('TabServico_PP.Quantidade,');
           Sql.add('TabServico_PP.Valor,TabServico_PP.ValorFinal,TabPedido_Proj.Codigo as PedidoProjeto,');
           Sql.add('TabServico_PP.Codigo');
           Sql.add('from TabServico_PP');
           Sql.add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabServico_PP.PedidoProjeto');
           Sql.add('Join TabPedido on TabPedido.Codigo = TabPedido_Proj.Pedido');
           Sql.add('Join TabServico on TabServico.Codigo = TabServico_PP.Servico');

           Sql.add('Where TabPedido_Proj.Pedido = '+PPedido);
           if (PpedidoProjeto <> '' )
           then Sql.add('and   TabPedido_Proj.Codigo ='+PPedidoProjeto);

           if (ppedido='')
           Then exit;

           Open;

       end;
end;

function TObjSERVICO_PP_PEDIDO.DeletaServico_PP(PPedidoProjeto:string): Boolean;
begin
     Result:=False;
     With Objquery  do
     Begin
        Close;
        SQL.Clear;
        Sql.Add('Delete from TabServico_PP where PedidoProjeto='+ppedidoprojeto);
        Try
            execsql;
        Except
              On E:Exception do
              Begin
                   Messagedlg('Erro na tentativa de Excluir os Servicos do Projeto '+#13+E.message,mterror,[mbok],0);
                   exit;
              End;
        End;
     end;
     Result:=true;
end;

function TObjSERVICO_PP_PEDIDO.PreencheServicoPP(PPedidoProjeto: string; var PValor: Currency;PprojetoemBRanco:Boolean;Paltura,Plargura:string): Boolean;
var
PtipoPedido:String;
PcodigoProjeto:string;
Pquantidade,quantidadem2:Currency;
begin
    Result:=false;
    Pvalor:=0;

    //Localizo o Pedido Projeto
    {if (Self.PedidoProjeto.LocalizaCodigo(PPedidoProjeto)=false)
    then exit;
    Self.PedidoProjeto.TabelaparaObjeto;}
    //**********************************

    With ObjQueryPesquisa do
    Begin
         Close;
         Sql.clear;
         sql.add('Select TabPedido_Proj.Projeto,TabPedido_Proj.tipoPedido From tabPedido_proj');
         sql.add('where Tabpedido_proj.codigo='+PPedidoProjeto);
         open;
         PtipoPedido:=Fieldbyname('tipopedido').asstring;
         PcodigoProjeto:=Fieldbyname('projeto').asstring;


        //Apaga Apenas as Servicos ligadas a este Pedido Projeto
        Self.DeletaServico_PP(PPedidoProjeto);
        if (PprojetoemBRanco=True)
        Then Begin
                  result:=True;
                  exit;
        End;

        //Esse SQL Retorna o Codigo da Servico,o PReco, e a quantidade (cadastrada no projeto)

        Close;
        SQL.Clear;
        SQL.Add(' Select TabServico.Codigo as Servico, TabServico_proj.Quantidade,');
        SQL.Add('TabServico_proj.largura,TabServico_proj.altura,TabServico_proj.CONTROLAPORMILIMETRO,controlam2,');

        if (PTipoPedido='INSTALADO')
        Then SQL.Add('tabServico.PRecoVendaInstalado as Preco');

        if (PTipoPedido='RETIRADO')
        Then SQL.Add('tabServico.PRecoVendaretirado as Preco');

        if (PTipoPedido='FORNECIDO')
        Then SQL.Add('tabServico.PRecoVendafornecido as Preco');

        SQL.Add('from TabServico');
        SQL.Add('Join TabServico_Proj on TabServico_Proj.Servico = TabServico.Codigo');
        SQL.Add('Where TabServico_Proj.PRojeto = '+PcodigoProjeto);
        Open;

        // Preenchendo a TabFerragm_PP
        While not (eof) do
        Begin
            Self.Status:=dsInsert;
            Self.Submit_Codigo(Self.Get_NovoCodigo);
            Self.Submit_PedidoProjeto(PPedidoProjeto);
            if(fieldbyname('CONTROLAPORMILIMETRO').AsString='SIM')then
            begin
                  Fcalculaformula_Perfilado.QuantidadeExecucaoRecursiva:=0;

                  Fcalculaformula_Perfilado.BTLIMPARTUDOClick(nil);
                  Fcalculaformula_Perfilado.edtvariavel.Text:='QTDE';

                  Fcalculaformula_Perfilado.edtformulaaltura.TEXT:=fieldbyname('altura').asstring;
                  Fcalculaformula_Perfilado.edtformulalargura.text:=fieldbyname('largura').asstring;


                  Fcalculaformula_Perfilado.edtaltura.text:=Paltura;
                  Fcalculaformula_Perfilado.edtlargura.text:=Plargura;

                  //Fcalculaformula_Perfilado.edtalturalateral.text:=PAlturaLateral;
                  //Fcalculaformula_Perfilado.edtlarguralateral.text:=PLarguraLateral;

                  Fcalculaformula_Perfilado.btinsereformulaClick(nil);


                  if (Fcalculaformula_Perfilado.Calcula_Area=False)
                  Then Begin
                            Messagedlg('Erro no C�lcula da F�rmula do Servi�o!',mterror,[mbok],0);
                            exit;
                  End;
                  //tenho a primeira linha dos dois string list com os resultados, basta soma-los
                  //Fcalculaformula_Perfilado.SHOWMODAL;

                  Pquantidade:=0;
                  Try
                      Pquantidade:=strtofloat(Fcalculaformula_Perfilado.lbaltura.Items[0])+strtofloat(Fcalculaformula_Perfilado.lblargura.Items[0]);
                  Except
                      Messagedlg('Erro na F�rmula',mterror,[mbok],0);
                      exit;
                  End;
                  //essa qtde esta em milimetros, mas preciso converte-la
                  //para metro pois o valor � calculado por metro

                  Try
                     Pquantidade:=Pquantidade/1000;
                  Except
                        Pquantidade:=0;
                  End;
                  Self.Quantidade:=FloatToStr(Pquantidade);
                  Submit_ControlaPorMilimetro('SIM');
            end
            else
            begin
               if(fieldbyname('controlam2').AsString='S') then
               begin
                   quantidadem2:= StrToCurr(Paltura)*StrToCurr(Plargura);
                   quantidadem2:=(quantidadem2/1000)/1000;
                   Self.Quantidade:=CurrToStr(quantidadem2);
                   Submit_controlaM2('S');
               end
               else
               begin
                  Self.Quantidade:=fieldbyname('Quantidade').AsString;
                  Submit_controlaM2('N');
               end;
            end;

            Self.Servico.Submit_Codigo(fieldbyname('Servico').AsString);
            Self.Submit_Valor(fieldbyname('Preco').AsString);
            if Self.Salvar(false)=false then
            Begin
                 Result:=false;
                 exit;
            end;
            Next;
        end;
        pvalor:=0;
        Pvalor:=Self.Soma_por_PP(PPedidoProjeto);
        result:=true;
    end;

end;

function TObjSERVICO_PP_PEDIDO.Get_ValorFinal: string;
begin
     Result:=Self.ValorFinal;
end;


function TObjSERVICO_PP_PEDIDO.Soma_por_PP(PpedidoProjeto: string): Currency;
begin

     Self.Objquery.close;
     Self.Objquery.sql.clear;
     Self.Objquery.sql.add('Select sum(valorfinal) as SOMA from TabServico_PP where PedidoProjeto='+PpedidoProjeto);
     Try
         Self.Objquery.open;
         Result:=Self.Objquery.fieldbyname('soma').asfloat;
     Except
           Result:=0;
     End;

end;

procedure TObjSERVICO_PP_PEDIDO.RetornaDadosRel(PpedidoProjeto: string; STrDados: TStrings;ComValor:Boolean);
begin
     if (PpedidoProjeto='')
     Then exit;

     With Self.ObjQueryPesquisa do
     Begin
          close;
          SQL.clear;
          sql.add('Select codigo from TabServico_pp where PedidoProjeto='+PpedidoProjeto);
          open;
          first;

          if (RecordCount = 0)then
          exit;

          While not(eof) do
          Begin
               if(ComValor=True) then
               begin

                   Self.LocalizaCodigo(fieldbyname('codigo').asstring);
                   Self.TabelaparaObjeto;
                   STrDados.Add(CompletaPalavra(Self.Servico.Get_Referencia,27,' ')+' '+
                                CompletaPalavra(Self.Servico.Get_Descricao,45,' ')+' '+
                                CompletaPalavra(Self.Get_Quantidade,6,' ')+' '+
                                CompletaPalavra_a_Esquerda(formata_valor(Self.Get_Valor),10,' ')+' '+
                                CompletaPalavra_a_Esquerda(formata_valor(StrToCurr(Self.Get_Quantidade)*StrToCurr(Self.Get_Valor)),8,' '));

               end
               else
               begin
                   Self.LocalizaCodigo(fieldbyname('codigo').asstring);
                   Self.TabelaparaObjeto;
                   STrDados.Add(CompletaPalavra(Self.Servico.Get_Referencia,27,' ')+' '+
                                CompletaPalavra(Self.Servico.Get_Descricao,45,' ')+' '+
                                CompletaPalavra(Self.Get_Quantidade,6,' '));
               end;

               next;
          end;
          if(ComValor=True) then
          begin
              close;
              sql.clear;
              sql.add('Select sum(ValorFinal) as SOMA FROM TabServico_PP where PedidoProjeto='+PpedidoProjeto);
              open;
              STrDados.Add('?'+CompletaPalavra_a_Esquerda('Total SERVI�OS: '+formata_valor(fieldbyname('soma').asstring),100,' '));
              STrDados.Add(' ');
          end;
     End;

end;

procedure TObjSERVICO_PP_PEDIDO.RetornaDadosRelProjetoBranco(
  PpedidoProjeto: string; STrDados: TStrings;pquantidade:integer;var PTotal: Currency);
begin
  if (PpedidoProjeto='') then
    exit;

  with Self.ObjQueryPesquisa do
  begin
    Close;
    SQL.Clear;
    SQL.Text := ' select s.referencia, s.descricao, '+
                ' sp.valor, sum(sp.quantidade) * cast(:qtde as numeric(9,2)) as quantidade, '+
                ' sum(sp.quantidade) * cast(:qtde as numeric(9,2)) * sp.valor as mult '+
                ' from tabservico_pp sp '+
                ' inner join tabservico s on (sp.servico = s.codigo) '+
                ' where sp.pedidoprojeto = :pedidoprojeto '+
                ' group by 1,2,3 ';

    ParamByName('qtde').AsInteger := pquantidade;
    ParamByName('pedidoprojeto').AsString := PpedidoProjeto;
    Open;
    First;
    if (RecordCount = 0)then
      Exit;

    // Cabe�alho ITEM , DESCRICAO, QTDE, VALOR
    STrDados.Add('?'+CompletaPalavra('�TEM',8,' ')+' '+
                     CompletaPalavra('  COR',14,' ')+' '+
                     CompletaPalavra('   DESCRI��O',39,' ')+' '+
                     '          QTDE      VAL.UN     VALOR');

    while not(eof) do
    begin
      //Self.LocalizaCodigo(fieldbyname('codigo').asstring);
      //Self.TabelaparaObjeto;
      StrDados.Add(CompletaPalavra(fieldbyname('referencia').asstring,26,' ')+' '+
                   CompletaPalavra(fieldbyname('descricao').asstring,44,' ')+' '+
                   CompletaPalavra(fieldbyname('quantidade').asstring,6,' ')+' '+
                   CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),10,' ')+' '+
                   CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('mult').asstring),10,' '));
      Next;
    end;
    //*******************************************************
    close;
    sql.clear;
    sql.add('Select sum(ValorFinal) as SOMA FROM TabServico_PP where PedidoProjeto='+PpedidoProjeto);
    open;

    PTotal:=PTotal+(fieldbyname('soma').AsCurrency*pquantidade);
  end;
end;

function TObjSERVICO_PP_PEDIDO.Get_PedidoProjeto: string;
begin
     Result:=Self.PedidoProjeto;
end;

procedure TObjSERVICO_PP_PEDIDO.Submit_PedidoProjeto(parametro: string);
begin
     Self.PedidoProjeto:=parametro;
end;

{Rodolfo
procedure TObjSERVICO_PP.EdtServicoExit(Sender: TObject;Var PEdtCodigo:TEdit; LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Servico.LocalizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     
     Self.Servico.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Servico.Get_Descricao;
     PEdtCodigo.Text:=Self.Servico.Get_Codigo;
End;  }

procedure TObjSERVICO_PP_PEDIDO.RetornaDadosRel2(PpedidoProjeto: string;
  STrDados: TStrings; ComValor: Boolean);
begin
  if (PpedidoProjeto='') then
    exit;

  with Self.ObjQueryPesquisa do
  begin
    Close;
    SQL.Clear;
    SQL.Text := ' select s.referencia, s.descricao, '+
                ' sp.valor, sum(sp.quantidade) as quantidade, '+
                ' sum(sp.quantidade) * sp.valor as mult '+
                ' from tabservico_pp sp '+
                ' inner join tabservico s on (sp.servico = s.codigo) '+
                ' where sp.pedidoprojeto = :pedidoprojeto '+
                ' group by 1,2,3 ';

    ParamByName('pedidoprojeto').AsString := PpedidoProjeto;
    Open;
    First;
    if (RecordCount = 0)then
      Exit;

    while not(eof) do
    begin
      if ComValor then
      begin
        StrDados.Add(CompletaPalavra(fieldbyname('referencia').asstring,26,' ')+' '+
                     CompletaPalavra(fieldbyname('descricao').asstring,44,' ')+' '+
                     CompletaPalavra(fieldbyname('quantidade').asstring,6,' ')+' '+
                     CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),10,' ')+' '+
                     CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('mult').asstring),10,' '));
      end
      else begin
        StrDados.Add(CompletaPalavra(fieldbyname('referencia').asstring,27,' ')+' '+
                     CompletaPalavra(fieldbyname('descricao').asstring,45,' ')+' '+
                     CompletaPalavra(fieldbyname('quantidade').asstring,6,' '));
      end;
      Next;
    end;
    //*******************************************************
    if ComValor then
    begin
      close;
      sql.clear;
      sql.add('Select sum(ValorFinal) as SOMA FROM TabServico_PP where PedidoProjeto='+PpedidoProjeto);
      open;
      STrDados.Add('?'+CompletaPalavra_a_Esquerda('Total SERVI�OS: '+formata_valor(fieldbyname('soma').asstring),100,' '));
      STrDados.Add(' ');
    end;
  end;
end;

end.



