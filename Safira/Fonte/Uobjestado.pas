unit Uobjestado;
Interface
Uses Ibcustomdataset,IBStoredProc,Db,UessencialGlobal,classes,stdctrls,windows,
UDatamodulo,Ibquery,Dialogs,Controls,SysUtils,Upesquisa;

Type
   TObjEstado=class

          Public
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                Function    Salvar                          :Boolean;overload;
                Function    Salvar(ComCommit:Boolean)       :Boolean;overload;
                Procedure   TabelaparaObjeto                        ;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string)            :Boolean;
                Procedure   ZerarTabela;
                Procedure  Cancelar;
                Procedure  Commit;
                Function   Get_Pesquisa                         :string;
                Function   Get_TituloPesquisa                         :string;
                Constructor Create;
                Destructor  Free;

                Function Get_CODIGO           :string;
                Function Get_NOme             :string;
                Function Get_Aliquota         :string;
                Procedure Submit_Aliquota         (parametro:string);
                Procedure Submit_CODIGO         (parametro:string);
                Procedure Submit_NOME           (parametro:string);

                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                //***************171007*celio
                procedure EdtEstadoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);



         Private
               ObjDataset:Tibdataset;

               CODIGO           :String[02];
               NOME             :String[40];
               Aliquota         :string;


                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation



{ TTabTitulo }


Procedure  TObjEstado.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.CODIGO           :=FieldByName('CODIGO').asstring;
        Self.NOME             :=FieldByName('NOME').asstring;
        Self.Aliquota         :=FieldByName('Aliquota').asstring;
     End;
end;


Procedure TObjEstado.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring    := Self.CODIGO           ;
      FieldByName('NOME').asstring      :=Self.NOME              ;
      FieldByName('Aliquota').asstring  :=Self.Aliquota          ;
  End;
End;

//***********************************************************************

function TObjEstado.Salvar: Boolean;
begin
     Result:=Self.Salvar(true);
end;

function TObjEstado.Salvar(ComCommit: Boolean): Boolean;
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  If Self.LocalizaCodigo(Self.CODIGO)=False
  Then Begin
              if(Self.Status=dsedit)
              Then Begin
                        Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                        result:=False;
                        exit;
                   End
       End
  Else Begin
            If (Self.Status=dsinsert)
            Then Begin
                        Messagedlg('O registro j� est� cadastrado!',mterror,[mbok],0);
                        result:=False;
                        exit;
                 End;
       End;


 if Self.status=dsinsert
 then  Self.ObjDataset.Insert//libera para insercao
 Else  Self.ObjDataset.edit;//se for edicao libera para tal
 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 if (ComCommit=True)
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjEstado.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO           :='';
        Self.NOME             :='';
        Self.Aliquota         :='';
     End;
end;

Function TObjEstado.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';
       If (nome='')
       Then Mensagem:=mensagem+'/Nome';
       If (Aliquota='')
       Then Mensagem:=mensagem+'/Al�quota';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjEstado.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     Try
        {If (Self.CODCURSO.LocalizaCodigo(Self.codcurso.Get_CODIGO)=False)
        Then Mensagem:='/ Curso n�o Encontrado!'
        Else Begin
                  Self.CODCURSO.TabelaparaObjeto;
                  If Self.CODCURSO.Get_Ativo='N'
                  Then Mensagem:='/Situa��o do Curso Inv�lida para a Turma!';
             End;}

        If (mensagem<>'')
        Then Begin
                  Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
                  result:=False;
                  exit;
             End;
        result:=true;
     Finally

     End;
End;

function TObjEstado.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Real;
   Mensagem:string;
begin
     Mensagem:='';
     Try
        reais:=Strtofloat(Self.Aliquota);
     Except
           Mensagem:=Mensagem+'\Al�quota';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjEstado.VerificaData: Boolean;
var
Datas:Tdatetime;
Mensagem:string;
begin
     mensagem:='';

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m datas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjEstado.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjEstado.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,nome,aliquota from tabestado where upper(codigo)='+#39+uppercase(parametro)+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjEstado.Cancelar;
begin
     FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

function TObjEstado.Exclui(Pcodigo: string): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjEstado.create;
begin

        ZerarTabela;
        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select codigo,nome,aliquota from tabestado where codigo=''MS''');


                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' insert into tabestado (codigo,nome,aliquota) values (:codigo,:nome,:aliquota)');

                ModifySQL.clear;
                ModifySQL.add('  Update Tabestado set codigo=:codigo,nome=:nome,aliquota=:aliquota where codigo=:codigo');


                DeleteSQL.clear;
                DeleteSQL.add('delete from tabestado where codigo=:codigo ');

                RefreshSQL.clear;
                RefreshSQL.add('Select codigo,nome,aliquota from tabestado where codigo=''MS''');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjEstado.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjEstado.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjEstado.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjEstado.Get_Pesquisa: string;
begin
     Result:='Select * from TabEstado';
end;

function TObjEstado.Get_TituloPesquisa: string;
begin
     Result:='Pesquisa de Estado ';
end;

function TObjEstado.Get_NOme: string;
begin
     Result:=Self.Nome;
end;

procedure TObjEstado.Submit_NOME(parametro: string);
begin
     Self.nome:=parametro;
end;

destructor TObjEstado.Free;
begin
    Freeandnil(Self.ObjDataset);
end;

function TObjEstado.Get_Aliquota: string;
begin
     result:=Self.Aliquota;
end;

procedure TObjEstado.Submit_Aliquota(parametro: string);
begin
     Self.Aliquota:=parametro;
end;

function TObjEstado.RetornaCampoCodigo: string;
begin
        result:='codigo';
end;

function TObjEstado.RetornaCampoNome: string;
begin
        result:='nome';
end;


procedure TObjEstado.EdtEstadoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,Self.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;

end;

end.
