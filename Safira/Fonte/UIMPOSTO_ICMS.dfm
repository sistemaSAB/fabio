object FIMPOSTO_ICMS: TFIMPOSTO_ICMS
  Left = 466
  Top = 47
  Width = 901
  Height = 608
  Caption = 'Cadastro de Imposto  - ICMS - EXCLAIM TECNOLOGIA'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lb1: TLabel
    Left = 19
    Top = 461
    Width = 178
    Height = 13
    Caption = 'F'#243'rmula Valor do Imposto (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb2: TLabel
    Left = 19
    Top = 421
    Width = 96
    Height = 13
    Caption = 'F'#243'rmula BC (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbIVA_ST: TLabel
    Left = 427
    Top = 369
    Width = 207
    Height = 13
    Caption = #205'ndice de valor agregado - IVA (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbPAUTA_ST: TLabel
    Left = 699
    Top = 369
    Width = 61
    Height = 13
    Caption = 'Pauta (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbALIQUOTA_ST: TLabel
    Left = 275
    Top = 370
    Width = 75
    Height = 13
    Caption = 'Al'#237'quota (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbPERC_REDUCAO_BC_ST: TLabel
    Left = 19
    Top = 370
    Width = 203
    Height = 13
    Caption = '% de Red. da Base de C'#225'lculo (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbMODALIDADE_ST: TLabel
    Left = 19
    Top = 330
    Width = 79
    Height = 13
    Caption = 'Modalide (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb3: TLabel
    Left = 19
    Top = 293
    Width = 149
    Height = 13
    Caption = 'F'#243'rmula Valor do Imposto'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb4: TLabel
    Left = 19
    Top = 253
    Width = 67
    Height = 13
    Caption = 'F'#243'rmula BC'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbIVA: TLabel
    Left = 433
    Top = 210
    Width = 179
    Height = 13
    Caption = #205'ndice de valor agregado (IVA)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbPAUTA: TLabel
    Left = 697
    Top = 210
    Width = 32
    Height = 13
    Caption = 'Pauta'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbALIQUOTA: TLabel
    Left = 275
    Top = 210
    Width = 46
    Height = 13
    Caption = 'Al'#237'quota'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbPERC_REDUCAO_BC: TLabel
    Left = 19
    Top = 210
    Width = 174
    Height = 13
    Caption = '% de Red. da Base de C'#225'lculo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbMODALIDADE: TLabel
    Left = 19
    Top = 164
    Width = 64
    Height = 13
    Caption = 'Modalidade'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb5: TLabel
    Left = 19
    Top = 118
    Width = 108
    Height = 13
    Caption = 'Situa'#231#227'o Tribut'#225'ria'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb6: TLabel
    Left = 19
    Top = 70
    Width = 89
    Height = 13
    Caption = 'CFOP de Venda'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbnomeCFOP_ICMS: TLabel
    Left = 87
    Top = 91
    Width = 66
    Height = 13
    Caption = 'lbnomecfop'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbNomeSTA_ICMS: TLabel
    Left = 126
    Top = 137
    Width = 103
    Height = 13
    AutoSize = False
    Caption = 'Sit. Trib. Tabela B'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbNomeSTB_ICMS: TLabel
    Left = 238
    Top = 137
    Width = 103
    Height = 13
    Caption = 'Sit. Trib. Tabela B'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object edtFORMULA_VALOR_IMPOSTO_ST_ICMS: TEdit
    Left = 19
    Top = 476
    Width = 551
    Height = 19
    TabOrder = 0
  end
  object edtformula_bc_st_ICMS: TEdit
    Left = 19
    Top = 436
    Width = 551
    Height = 19
    TabOrder = 1
    Text = 'edtformula_BC'
  end
  object edtIVA_ST_ICMS: TEdit
    Left = 467
    Top = 385
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 2
  end
  object edtPAUTA_ST_ICMS: TEdit
    Left = 699
    Top = 385
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 3
  end
  object edtPERC_REDUCAO_BC_ST_ICMS: TEdit
    Left = 19
    Top = 386
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 4
  end
  object edtALIQUOTA_ST_ICMS: TEdit
    Left = 275
    Top = 386
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 5
  end
  object cbbComboMODALIDADE_ST_ICMS: TComboBox
    Left = 19
    Top = 343
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 6
    Text = '   '
    Items.Strings = (
      '0 - Pre'#231'o tabelado ou m'#225'ximo sugerido;'
      '1 - Lista Negativa (valor);'
      '2 - Lista Positiva (valor);'
      '3 - Lista Neutra (valor);'
      '4 - Margem Valor Agregado (%);'
      '5 - Pauta (valor);')
  end
  object edtFORMULA_VALOR_IMPOSTO_ICMS: TEdit
    Left = 19
    Top = 308
    Width = 551
    Height = 19
    TabOrder = 7
  end
  object edtformula_BC_ICMS: TEdit
    Left = 19
    Top = 268
    Width = 551
    Height = 19
    TabOrder = 8
    Text = 'edtformula_BC_ICMS'
  end
  object edtIVA_ICMS: TEdit
    Left = 471
    Top = 226
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 9
  end
  object edtPAUTA_ICMS: TEdit
    Left = 697
    Top = 226
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 10
  end
  object edtALIQUOTA_ICMS: TEdit
    Left = 245
    Top = 226
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 11
  end
  object edtPERC_REDUCAO_BC_ICMS: TEdit
    Left = 19
    Top = 226
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 12
  end
  object cbbComboMODALIDADE_ICMS: TComboBox
    Left = 19
    Top = 179
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 13
    Text = '   '
    Items.Strings = (
      '0 - Margem Valor Agregado (%);'
      '1 - Pauta (Valor);'
      '2 - Pre'#231'o Tabelado M'#225'x. (valor);'
      '3 - valor da opera'#231#227'o.')
  end
  object edtSTA_ICMS: TEdit
    Left = 19
    Top = 134
    Width = 40
    Height = 19
    Color = 6073854
    MaxLength = 9
    TabOrder = 14
    OnDblClick = edtSTA_ICMSDblClick
    OnExit = edtSTA_ICMSExit
    OnKeyDown = edtSTA_ICMSKeyDown
  end
  object edtCFOP_ICMS: TEdit
    Left = 22
    Top = 88
    Width = 60
    Height = 19
    Color = 6073854
    MaxLength = 9
    TabOrder = 15
    OnDblClick = edtCFOP_ICMSDblClick
    OnExit = edtCFOP_ICMSExit
    OnKeyDown = edtCFOP_ICMSKeyDown
  end
  object edtSTB_ICMS: TEdit
    Left = 78
    Top = 134
    Width = 40
    Height = 19
    Color = 6073854
    MaxLength = 9
    TabOrder = 16
    OnDblClick = edtSTB_ICMSDblClick
    OnExit = edtSTB_ICMSExit
    OnKeyDown = edtSTB_ICMSKeyDown
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 885
    Height = 54
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 17
    DesignSize = (
      885
      54)
    object lbnomeformulario: TLabel
      Left = 631
      Top = 3
      Width = 104
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Cadastro de'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigo: TLabel
      Left = 746
      Top = 9
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb7: TLabel
      Left = 647
      Top = 25
      Width = 48
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'ICMS'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
      Spacing = 0
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 522
    Width = 885
    Height = 48
    Align = alBottom
    Color = clMedGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clSilver
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 18
    DesignSize = (
      885
      48)
    object imgrodape: TImage
      Left = 1
      Top = 0
      Width = 883
      Height = 47
      Align = alBottom
      Stretch = True
    end
    object lb9: TLabel
      Left = 524
      Top = 11
      Width = 234
      Height = 20
      Anchors = [akTop, akRight]
      Caption = 'Existem X ICMS cadastrados'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
end
