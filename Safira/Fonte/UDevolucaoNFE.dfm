object fDevolucaoNFE: TfDevolucaoNFE
  Left = 427
  Top = 147
  Width = 955
  Height = 709
  Caption = 'Devolu'#231#227'o NF-e'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 281
    Width = 939
    Height = 5
    Cursor = crVSplit
    Align = alTop
    Color = clWhite
    ParentColor = False
    ResizeStyle = rsLine
  end
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 939
    Height = 55
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 460
      Top = 16
      Width = 202
      Height = 28
      Caption = 'Devolu'#231#227'o NF-e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -23
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlBottom
      WordWrap = True
    end
    object lbCodigo: TLabel
      Left = 813
      Top = 0
      Width = 126
      Height = 55
      Align = alRight
      Alignment = taRightJustify
      Caption = '000000000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -25
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
    end
    object Btnovo: TBitBtn
      Left = 1
      Top = -3
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 51
      Top = -3
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 101
      Top = -3
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = -3
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = -3
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btexcluirClick
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = -3
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = -3
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object btOpcoes: TBitBtn
      Left = 351
      Top = -3
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
    end
    object BtSair: TBitBtn
      Left = 401
      Top = -3
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = BtSairClick
    end
  end
  object pnlCabecalho: TPanel
    Left = 0
    Top = 55
    Width = 939
    Height = 226
    Align = alTop
    BevelOuter = bvNone
    Color = 8539648
    TabOrder = 1
    DesignSize = (
      939
      226)
    object lbBaixaEstoque: TLabel
      Left = 31
      Top = 150
      Width = 91
      Height = 16
      Cursor = crHandPoint
      Caption = 'Baixa estoque'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb2: TLabel
      Left = 143
      Top = 150
      Width = 109
      Height = 16
      Cursor = crHandPoint
      Caption = 'Ind. Pagamento'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 15
      Top = 180
      Width = 88
      Height = 16
      Caption = 'Hora de saida'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 130
      Top = 180
      Width = 87
      Height = 16
      Caption = 'Data de saida'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 251
      Top = 180
      Width = 85
      Height = 16
      Caption = 'Data emiss'#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label18: TLabel
      Left = 574
      Top = 14
      Width = 51
      Height = 16
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = 'NF-e...:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbNFE: TLabel
      Left = 628
      Top = 14
      Width = 70
      Height = 16
      Cursor = crHandPoint
      Hint = 'Abre o m'#243'dulo de nota fiscal eletr'#244'nica'
      Anchors = [akTop, akRight]
      Caption = '0000000001'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object Label25: TLabel
      Left = 575
      Top = 47
      Width = 50
      Height = 16
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = 'Nota...:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbNota: TLabel
      Left = 628
      Top = 47
      Width = 81
      Height = 16
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = '0000000001'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 575
      Top = 97
      Width = 147
      Height = 16
      Anchors = [akTop, akRight]
      Caption = 'Informa'#231#245'es adicionais'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 15
      Top = 23
      Width = 45
      Height = 16
      Caption = 'Cliente'
      FocusControl = DBCliente
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 15
      Top = 50
      Width = 73
      Height = 16
      Caption = 'Fornecedor'
      FocusControl = DBFornecedor
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 15
      Top = 78
      Width = 63
      Height = 16
      Caption = 'Transport.'
      FocusControl = DBTransp
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbcliente: TLabel
      Left = 168
      Top = 23
      Width = 50
      Height = 16
      Caption = 'lbcliente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbfornecedor: TLabel
      Left = 168
      Top = 50
      Width = 76
      Height = 16
      Caption = 'lbfornecedor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbtransportadora: TLabel
      Left = 168
      Top = 78
      Width = 99
      Height = 16
      Caption = 'lbtransportadora'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 15
      Top = 106
      Width = 58
      Height = 16
      Caption = 'NF-e Ref.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbChaveRef: TLabel
      Left = 168
      Top = 106
      Width = 321
      Height = 16
      AutoSize = False
      Caption = '5555555555555555555555555555555555555555555555'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      PopupMenu = PopupMenu1
      OnContextPopup = lbChaveRefContextPopup
    end
    object edtHoraSaida: TMaskEdit
      Left = 18
      Top = 197
      Width = 87
      Height = 20
      Hint = 'Hora de saida da mercadoria'
      Ctl3D = False
      EditMask = '!90:00:00;_'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 8
      ParentCtl3D = False
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      Text = '  :  :  '
      OnKeyDown = edtHoraSaidaKeyDown
    end
    object edtDataSaida: TMaskEdit
      Left = 132
      Top = 197
      Width = 90
      Height = 20
      Hint = 'Data de saida da mercadoria'
      Ctl3D = False
      EditMask = '!99/99/9999;1;_'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      Text = '  /  /    '
      OnKeyDown = edtDataSaidaKeyDown
    end
    object edtDataEmissao: TMaskEdit
      Left = 253
      Top = 197
      Width = 89
      Height = 20
      Hint = 'Data de emiss'#227'o do documento fiscal'
      Ctl3D = False
      EditMask = '!99/99/9999;1;_'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      Text = '  /  /    '
      OnKeyDown = edtDataEmissaoKeyDown
    end
    object chkBaixaEstoque: TCheckBox
      Left = 15
      Top = 151
      Width = 13
      Height = 14
      BiDiMode = bdLeftToRight
      Color = clBtnFace
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentColor = False
      ParentCtl3D = False
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
      TabOrder = 4
    end
    object DBCliente: TDBEdit
      Left = 96
      Top = 19
      Width = 54
      Height = 20
      Color = 6073854
      Ctl3D = False
      DataField = 'CLIENTE'
      DataSource = ds1
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnExit = DBClienteExit
      OnKeyDown = DBClienteKeyDown
    end
    object DBFornecedor: TDBEdit
      Left = 96
      Top = 46
      Width = 54
      Height = 20
      Color = 6073854
      Ctl3D = False
      DataField = 'FORNECEDOR'
      DataSource = ds1
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      OnExit = DBFornecedorExit
      OnKeyDown = DBFornecedorKeyDown
    end
    object DBTransp: TDBEdit
      Left = 96
      Top = 74
      Width = 54
      Height = 20
      Color = 6073854
      Ctl3D = False
      DataField = 'TRANSPORTADORA'
      DataSource = ds1
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      OnExit = DBTranspExit
      OnKeyDown = DBTranspKeyDown
    end
    object DBChaveRef: TDBEdit
      Left = 96
      Top = 102
      Width = 55
      Height = 20
      Color = 6073854
      Ctl3D = False
      DataField = 'CHAVE_DEVOLUCAO'
      DataSource = ds1
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      OnExit = DBChaveRefExit
      OnKeyDown = DBChaveRefKeyDown
    end
    object DBMemo1: TDBMemo
      Left = 577
      Top = 112
      Width = 361
      Height = 105
      Anchors = [akTop, akRight]
      BorderStyle = bsNone
      DataField = 'INFORMACOES'
      DataSource = ds1
      ScrollBars = ssBoth
      TabOrder = 8
    end
    object ComboIndPag: TComboBox
      Left = 256
      Top = 149
      Width = 97
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 9
      Text = '0 - A vista'
      Items.Strings = (
        '0 - A vista'
        '1 - A prazo'
        '2 - Outros')
    end
  end
  object pnlProdutos: TPanel
    Left = 0
    Top = 286
    Width = 939
    Height = 103
    Align = alTop
    BevelOuter = bvNone
    Color = clInfoBk
    TabOrder = 2
    DesignSize = (
      939
      103)
    object v: TLabel
      Left = 4
      Top = 11
      Width = 28
      Height = 16
      Caption = 'Tipo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 8
      Top = 40
      Width = 28
      Height = 16
      Alignment = taRightJustify
      Caption = 'Item'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object label55: TLabel
      Left = 80
      Top = 40
      Width = 22
      Height = 16
      Caption = 'Cor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 158
      Top = 40
      Width = 42
      Height = 16
      Caption = 'Quant.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 261
      Top = 40
      Width = 53
      Height = 16
      Caption = 'Valor R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 394
      Top = 40
      Width = 78
      Height = 16
      Caption = 'Desconto R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label23: TLabel
      Left = 651
      Top = 40
      Width = 53
      Height = 16
      Caption = 'Frete R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 6
      Top = 62
      Width = 62
      Height = 16
      Caption = 'Descri'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbDescricaoProduto: TLabel
      Left = 73
      Top = 62
      Width = 275
      Height = 16
      Caption = 'descri'#231#227'o produto xxxxxxxxxxxxxxxxxxxxxxxx'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label22: TLabel
      Left = 6
      Top = 85
      Width = 22
      Height = 16
      Caption = 'Cor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbCor: TLabel
      Left = 73
      Top = 85
      Width = 276
      Height = 16
      Caption = 'cor do produto xxxxxxxxxxxxxxxxxxxxxxxxxxx'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 543
      Top = 40
      Width = 36
      Height = 16
      Caption = 'CFOP'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object comboTipo: TComboBox
      Left = 35
      Top = 6
      Width = 188
      Height = 22
      CharCase = ecUpperCase
      Color = 6073854
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ItemHeight = 14
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnEnter = comboTipoEnter
      OnExit = comboTipoExit
      OnKeyDown = comboTipoKeyDown
      Items.Strings = (
        ''
        'FERRAGEM'
        'PERFILADO'
        'VIDRO'
        'KITBOX'
        'PERSIANA'
        'DIVERSO')
    end
    object edtProduto: TEdit
      Left = 38
      Top = 37
      Width = 39
      Height = 19
      Color = 6073854
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 1
      OnExit = edtProdutoExit
      OnKeyDown = edtProdutoKeyDown
      OnKeyPress = edtProdutoKeyPress
    end
    object edtCor: TEdit
      Left = 107
      Top = 37
      Width = 43
      Height = 19
      Color = 6073854
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 2
      OnExit = edtCorExit
      OnKeyDown = edtCorKeyDown
    end
    object edtQuantidade: TEdit
      Left = 204
      Top = 37
      Width = 45
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 3
      OnKeyPress = edtQuantidadeKeyPress
    end
    object edtValorproduto: TEdit
      Left = 317
      Top = 37
      Width = 65
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 4
      OnKeyPress = edtValorprodutoKeyPress
    end
    object edtDesconto: TEdit
      Left = 475
      Top = 37
      Width = 55
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 5
      OnKeyPress = edtDescontoKeyPress
    end
    object edtValorFrete: TEdit
      Left = 712
      Top = 37
      Width = 51
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 7
      OnKeyPress = edtValorFreteKeyPress
    end
    object btGravarProduto: TBitBtn
      Left = 816
      Top = 7
      Width = 39
      Height = 39
      Anchors = [akRight]
      TabOrder = 8
      OnClick = btGravarProdutoClick
    end
    object btCancelarProduto: TBitBtn
      Left = 856
      Top = 7
      Width = 39
      Height = 39
      Anchors = [akRight]
      TabOrder = 9
      OnClick = btCancelarProdutoClick
    end
    object btExcluirProduto: TBitBtn
      Left = 896
      Top = 7
      Width = 39
      Height = 39
      Anchors = [akRight]
      TabOrder = 10
      OnClick = btExcluirProdutoClick
    end
    object edtcfop: TEdit
      Left = 582
      Top = 37
      Width = 51
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 6
    end
  end
  object DBGridProdutos: TDBGrid
    Left = 0
    Top = 389
    Width = 939
    Height = 215
    Align = alClient
    BorderStyle = bsNone
    Ctl3D = False
    DataSource = ds2
    Font.Charset = ANSI_CHARSET
    Font.Color = 5066061
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 652
    Width = 939
    Height = 19
    Panels = <>
  end
  object Panel1: TPanel
    Left = 0
    Top = 604
    Width = 939
    Height = 48
    Align = alBottom
    BevelOuter = bvNone
    Color = 8539648
    TabOrder = 5
    DesignSize = (
      939
      48)
    object lbGera: TLabel
      Left = 783
      Top = 8
      Width = 147
      Height = 29
      Cursor = crHandPoint
      Hint = 'Gera a nota fiscal eletr'#244'nica e imprime o DANFE'
      Anchors = [akTop, akRight]
      Caption = 'Gera devolu'#231#227'o'
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -24
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      Transparent = True
      OnClick = lbGeraClick
    end
    object Label13: TLabel
      Left = 15
      Top = 7
      Width = 62
      Height = 16
      Caption = 'Produto..:'
      FocusControl = DBCliente
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbTotalProdutos: TLabel
      Left = 80
      Top = 7
      Width = 81
      Height = 16
      Caption = '000000000,00'
      FocusControl = DBCliente
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label16: TLabel
      Left = 166
      Top = 7
      Width = 70
      Height = 16
      Caption = 'Desconto..:'
      FocusControl = DBCliente
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbTotalDescontos: TLabel
      Left = 239
      Top = 7
      Width = 81
      Height = 16
      Caption = '000000000,00'
      FocusControl = DBCliente
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label19: TLabel
      Left = 326
      Top = 7
      Width = 78
      Height = 16
      Caption = 'Nota fiscal..:'
      FocusControl = DBCliente
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbTotalNF: TLabel
      Left = 408
      Top = 7
      Width = 81
      Height = 16
      Caption = '000000000,00'
      FocusControl = DBCliente
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 590
      Top = 7
      Width = 56
      Height = 16
      Caption = 'Impostos'
      FocusControl = DBCliente
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Image2: TImage
      Left = 652
      Top = 4
      Width = 33
      Height = 33
      Cursor = crHandPoint
      Picture.Data = {
        0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000002000
        0000200806000000737A7AF4000006404944415478DAC5976B68536718C79F73
        CDAD494ED25C5A5BAB5557ADA2EDAC5A113753D4944ABD6EAD768AA4732AA343
        2D82B8E9D40ACAA61F14655FDCA5DD9731E88715DC4084D10AEA975DD431B739
        710BD6DA26697369D25CCE2567CF9BA84CA792CC767BE121879373F2FC9EFF73
        79DF50F09475F66CB740D38C87E398B568AED25227B02C03898408C9642ACC30
        740FC3309F37362EE9831758AAAA02F5E4CD9327BFD8834E0F97953905BD5E97
        7908180DC8AC1ECC6C12743A0D288A02F1788AC0F4214C7B7D7DEDF571013876
        ACAB53104C1EABB500528C562DAD98A19496172B462D287A0680A3818AC5801E
        B837C88E8546181DCF412432164EA79576B77B51D70B011C3AF471E7B469259E
        D1D118D8E7CD97A7564F97742CA83A1A408BA64100169F93D1526980440AE8A1
        DFFEE04DAC4287C3319065A5CEED5E98574A1E01ECDFFFD11E93C9788A6569D0
        2F58254EAFB4C8055A0012358F4620587C92A2C84B08A16621920A5091DB7735
        364EA1070787C3F87D392A11CE0BA0BDFDB48005F56771B14D908AE7C825F3E7
        8A261D8081188F91A3738E7900405E822C80A464212209A0A49BBF68A7387414
        4274D5D72F6ACD0B60D7AE537B8A8AACA786E25A75CAAAE6A46004B5E0218006
        0150770E3D337F035008005101F3319602880EC5D90AB8C78742312C50D98245
        99930A1980B6B693BD25250E57C0542517555589063D40467E342C78D0F25908
        CC4EF625028091A7244C0102249200D810A0BB73839939B940832AB436342CCE
        A92033003B777EA8DA6C025CE2D65D595AEBA82834838100E81E02A0732C7660
        E86C0DA4D52C80286521B0183326F77BA5A56592796060F834CE87F69C01F6ED
        FB4855191ECEF89ACF11072B17E8A72F9CC9CEB20B60D1A2630D2AC03E50209A
        001F294254C72962F429115510B300A940485DEE1C34F87CC1BEB56B5FA9CB19
        60EFDE33AAC45BE09C7FCD3913967DA189062B9A4360F8294EC6A2D350C021C0
        4844F511F9252C80A4A882536026DBCC308B61C0890312D470485DE11830F8FD
        C1BEF5EB97E50EB063C707AA56AB816EB5B5CB6CE245BB1901CC0C588D3498F5
        D896BA6C1D10053200287B1C1DC6E22A44C6D2981ACA693652F3CCD17EAEF1A5
        B1D2FE7E5F4F5353DDFA9C01366F3E142A2CB4083F70CB2F2AF60A2F46063681
        CE28211400909644BE8C0A044044E7712CBC481C201455613892067F5801B7E5
        D6EAF92572B1DF3FD2B169D3CA233903B4B4BCDF8945E8B9AF147B63B35EBBE8
        B4B2282F0D760BA00A9081202D496601C97F5222D1030463242D0081B00AF1B1
        B4713977B9C58483231C8ED66DD952DF9733C0C68D075D98825E6038506A3C5F
        0B931CF78B2C143811C021208409DB52979D88E9ECF48308713E0AE00F010CA1
        5943BFBA2A8D910A9F6FC4DBDADA589E8BF34700E4A2B9F940AF20185D09CE32
        52DAF0D6F9223B2D16D9008A10C2862A18C848A6B2294820C428A62180D10F06
        71100D042A4C819F5C545A81544A6CDFB66DF5E9BC019A9ADEABE679B6D7642A
        10D246E7C8FC4D5BCF173B402CB102F62280199FC2F904183C44D12278114015
        066E8D540CFF78798960C47D91E380E7192F7640FE0A64210E78789EC3EDD800
        2270624D83FBEAE21595BF5BB10091030C905520444C06E377DF5EAF097BEF56
        70D81EB22C03393B44A3F18EB6B60D3915E03F001EA4C285078CAFEC762B6E50
        14A80C27CE9833DD3B7B7679548727A25070147C81E0A4E1FB81E202DC289238
        85C8C104C1C1EDAE850B17AE82267AB3AFC1D4598D9DD33365B7AF352F00B2B0
        28D1397D4AA3E13D644493139024C9C0611F1A0C5A74C6632B4A245A2FC7D11D
        B80155E14F452449395C55CEC23CF133A8DDE086E0B5F310F387BB9E07F15480
        876BCB9623649B5E8767842AFCAC2667429665C3F87903EFF59C38F1CE63C7B0
        B75BDEECAD775E72AD78A3090C35AB01C2572078F1D3E7423C1720DF75F6755B
        68C3325970CE7D1598A93889272F4288AB08F1C93321C615E0E46A6B676569D4
        5357AB07CDD46539418C2BC0BF811877807C212604201F880903781C428710AE
        C720FCDF9C836470B4BD6CF7D0E909037816844C17C2CF5F1E8778D9D68E251B
        DF3D32A1004F4224CC8BE0DAF7F7805F7C1C6E0D321DDBB7AF9978808710338A
        E29E888CDBEBCB07E14E8023B7CB8F1EDDEEFD4F001E40ECB94D2D157CD44CB2
        77F474771FBF9E29C2CCBFDFFF71FD05CA5CEBDF82CFABFA0000000049454E44
        AE426082}
    end
  end
  object dts1: TIBDataSet
    Database = FDataModulo.IBDatabase
    Transaction = FDataModulo.IBTransaction
    DeleteSQL.Strings = (
      'delete from TABDEVOLUCAONFE'
      'where'
      '  CODIGO = :OLD_CODIGO')
    InsertSQL.Strings = (
      'insert into TABDEVOLUCAONFE'
      
        '  (AVISTA, BAIXAESTOQUE, CHAVE_DEVOLUCAO, CLIENTE, CODIGO, CRT, ' +
        'DATAC, '
      
        '   DATAEMISSAO, DATAM, DATASAIDA, FORNECEDOR, HORASAIDA, ICMSSOB' +
        'REFRETE, '
      '   INDPAG, INFORMACOES, NOTA, TRANSPORTADORA, USERC, USERM)'
      'values'
      
        '  (:AVISTA, :BAIXAESTOQUE, :CHAVE_DEVOLUCAO, :CLIENTE, :CODIGO, ' +
        ':CRT, :DATAC, '
      
        '   :DATAEMISSAO, :DATAM, :DATASAIDA, :FORNECEDOR, :HORASAIDA, :I' +
        'CMSSOBREFRETE, '
      
        '   :INDPAG, :INFORMACOES, :NOTA, :TRANSPORTADORA, :USERC, :USERM' +
        ')')
    RefreshSQL.Strings = (
      'Select '
      '  CODIGO,'
      '  NOTA,'
      '  TRANSPORTADORA,'
      '  FORNECEDOR,'
      '  DATAEMISSAO,'
      '  DATASAIDA,'
      '  HORASAIDA,'
      '  USERC,'
      '  USERM,'
      '  DATAC,'
      '  DATAM,'
      '  INFORMACOES,'
      '  CLIENTE,'
      '  BAIXAESTOQUE,'
      '  ICMSSOBREFRETE,'
      '  AVISTA,'
      '  CRT,'
      '  INDPAG,'
      '  CHAVE_DEVOLUCAO'
      'from TABDEVOLUCAONFE '
      'where'
      '  CODIGO = :CODIGO')
    SelectSQL.Strings = (
      'select *'
      'from TABDEVOLUCAONFE')
    ModifySQL.Strings = (
      'update TABDEVOLUCAONFE'
      'set'
      '  AVISTA = :AVISTA,'
      '  BAIXAESTOQUE = :BAIXAESTOQUE,'
      '  CHAVE_DEVOLUCAO = :CHAVE_DEVOLUCAO,'
      '  CLIENTE = :CLIENTE,'
      '  CODIGO = :CODIGO,'
      '  CRT = :CRT,'
      '  DATAC = :DATAC,'
      '  DATAEMISSAO = :DATAEMISSAO,'
      '  DATAM = :DATAM,'
      '  DATASAIDA = :DATASAIDA,'
      '  FORNECEDOR = :FORNECEDOR,'
      '  HORASAIDA = :HORASAIDA,'
      '  ICMSSOBREFRETE = :ICMSSOBREFRETE,'
      '  INDPAG = :INDPAG,'
      '  INFORMACOES = :INFORMACOES,'
      '  NOTA = :NOTA,'
      '  TRANSPORTADORA = :TRANSPORTADORA,'
      '  USERC = :USERC,'
      '  USERM = :USERM'
      'where'
      '  CODIGO = :OLD_CODIGO')
    GeneratorField.Field = 'CODIGO'
    GeneratorField.Generator = 'GENDEVOLUCAONFE'
    GeneratorField.ApplyEvent = gamOnPost
    Left = 360
    Top = 199
    object dts1CODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = '"TABDEVOLUCAONFE"."CODIGO"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object dts1NOTA: TIntegerField
      FieldName = 'NOTA'
      Origin = '"TABDEVOLUCAONFE"."NOTA"'
    end
    object dts1TRANSPORTADORA: TIntegerField
      FieldName = 'TRANSPORTADORA'
      Origin = '"TABDEVOLUCAONFE"."TRANSPORTADORA"'
    end
    object dts1FORNECEDOR: TIntegerField
      FieldName = 'FORNECEDOR'
      Origin = '"TABDEVOLUCAONFE"."FORNECEDOR"'
    end
    object dts1DATAEMISSAO: TDateField
      FieldName = 'DATAEMISSAO'
      Origin = '"TABDEVOLUCAONFE"."DATAEMISSAO"'
    end
    object dts1DATASAIDA: TDateField
      FieldName = 'DATASAIDA'
      Origin = '"TABDEVOLUCAONFE"."DATASAIDA"'
    end
    object dts1HORASAIDA: TTimeField
      FieldName = 'HORASAIDA'
      Origin = '"TABDEVOLUCAONFE"."HORASAIDA"'
    end
    object dts1INFORMACOES: TIBStringField
      FieldName = 'INFORMACOES'
      Origin = '"TABDEVOLUCAONFE"."INFORMACOES"'
      Size = 1000
    end
    object dts1CLIENTE: TIntegerField
      FieldName = 'CLIENTE'
      Origin = '"TABDEVOLUCAONFE"."CLIENTE"'
    end
    object dts1BAIXAESTOQUE: TIBStringField
      FieldName = 'BAIXAESTOQUE'
      Origin = '"TABDEVOLUCAONFE"."BAIXAESTOQUE"'
      Size = 1
    end
    object dts1ICMSSOBREFRETE: TIBStringField
      FieldName = 'ICMSSOBREFRETE'
      Origin = '"TABDEVOLUCAONFE"."ICMSSOBREFRETE"'
      Size = 1
    end
    object dts1AVISTA: TIBStringField
      FieldName = 'AVISTA'
      Origin = '"TABDEVOLUCAONFE"."AVISTA"'
      Size = 1
    end
    object dts1CRT: TIBStringField
      FieldName = 'CRT'
      Origin = '"TABDEVOLUCAONFE"."CRT"'
      Size = 1
    end
    object dts1INDPAG: TIBStringField
      FieldName = 'INDPAG'
      Origin = '"TABDEVOLUCAONFE"."INDPAG"'
      Size = 1
    end
    object dts1CHAVE_DEVOLUCAO: TIBStringField
      FieldName = 'CHAVE_DEVOLUCAO'
      Origin = '"TABDEVOLUCAONFE"."CHAVE_DEVOLUCAO"'
      Size = 50
    end
  end
  object queryInsereProduto: TIBQuery
    Database = FDataModulo.IBDatabase
    Transaction = FDataModulo.IBTransaction
    Left = 768
    Top = 303
  end
  object ds2: TDataSource
    DataSet = dts2
    Left = 464
    Top = 358
  end
  object ds1: TDataSource
    DataSet = dts1
    Left = 392
    Top = 200
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.XML'
    Filter = 'XML de NF-e|*.XML'
    Left = 872
    Top = 63
  end
  object PopupMenu1: TPopupMenu
    Left = 840
    Top = 63
    object CriarReferencia1: TMenuItem
      Caption = 'Criar Refer'#234'ncia'
      OnClick = CriarReferencia1Click
    end
  end
  object dts2: TIBDataSet
    Database = FDataModulo.IBDatabase
    Transaction = FDataModulo.IBTransaction
    SelectSQL.Strings = (
      'select * '
      'from viewproddevolucaonfe v'
      'where v.nfedevolucao =:codigo')
    Left = 432
    Top = 358
  end
end
