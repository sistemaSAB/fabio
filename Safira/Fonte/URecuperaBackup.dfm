object FrecuperaBackup: TFrecuperaBackup
  Left = 87
  Top = 218
  Width = 437
  Height = 192
  Caption = 'Recupera Backup'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BtGrupo: TBitBtn
    Left = 0
    Top = 16
    Width = 225
    Height = 49
    Caption = 'Grupo'
    TabOrder = 0
    OnClick = BtGrupoClick
  end
  object Btpersiana: TBitBtn
    Left = 0
    Top = 64
    Width = 225
    Height = 49
    Caption = 'Persiana'
    TabOrder = 1
    OnClick = BtpersianaClick
  end
  object BtPersianaGrupo: TBitBtn
    Left = 0
    Top = 113
    Width = 225
    Height = 49
    Caption = 'Persiana Grupo Diametro Cor'
    TabOrder = 2
    OnClick = BtPersianaGrupoClick
  end
  object Button1: TButton
    Left = 225
    Top = 16
    Width = 201
    Height = 49
    Caption = 'Abrir Base Backup'
    TabOrder = 3
    OnClick = Button1Click
  end
  object OpenDialog: TOpenDialog
  end
  object IBQuery: TIBQuery
    Left = 392
    Top = 72
  end
  object IBQueryBackup: TIBQuery
    Left = 360
    Top = 72
  end
end
