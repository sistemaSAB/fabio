unit UrelPedidoCompraPadrao;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls,jpeg;

type
  TTQRPedidoCompraPadrao = class(TQuickRep)
    qrbnd1: TQRBand;
    qrbnd2: TQRBand;
    qrshp2: TQRShape;
    qrshp3: TQRShape;
    lBDataPedidoCompra: TQRLabel;
    imgLogoDistribuidor: TQRImage;
    qrshp6: TQRShape;
    qrshp7: TQRShape;
    qrshp8: TQRShape;
    qrshp9: TQRShape;
    lB4: TQRLabel;
    lB5: TQRLabel;
    qrshp10: TQRShape;
    qrshp11: TQRShape;
    qrshp12: TQRShape;
    qrshp13: TQRShape;
    qrshp14: TQRShape;
    qrshp15: TQRShape;
    qrshp16: TQRShape;
    qrshp17: TQRShape;
    qrshp18: TQRShape;
    qrshp19: TQRShape;
    qrshp20: TQRShape;
    lB6: TQRLabel;
    lB7: TQRLabel;
    lB8: TQRLabel;
    lB9: TQRLabel;
    lB10: TQRLabel;
    lB11: TQRLabel;
    lB12: TQRLabel;
    lB13: TQRLabel;
    lB14: TQRLabel;
    qrshp5: TQRShape;
    qrshp4: TQRShape;
    lB2: TQRLabel;
    lB3: TQRLabel;
    lB1: TQRLabel;
    qrsysdt1: TQRSysData;
    lB15: TQRLabel;
    lB16: TQRLabel;
    lB17: TQRLabel;
    lB18: TQRLabel;
    lB19: TQRLabel;
    lBX1: TQRLabel;
    lBX2: TQRLabel;
    lBX3: TQRLabel;
    lBX4: TQRLabel;
    lBX5: TQRLabel;
    lBX6: TQRLabel;
    lBX7: TQRLabel;
    lBNomeDistribuidor: TQRLabel;
    lBPedidoCompra: TQRLabel;
    qrmObservacao: TQRMemo;
    lB21: TQRLabel;
    QrStrBandDados: TQRStringsBand;
    lBLBDADOS: TQRLabel;
    BandaDetail: TQRBand;
    BandaSubDetail: TQRSubDetail;
    lB22: TQRLabel;
    lB23: TQRLabel;
    lB24: TQRLabel;
    lB25: TQRLabel;
    lB26: TQRLabel;
    lB30: TQRLabel;
    lB32: TQRLabel;
    lB33: TQRLabel;
    lB27: TQRLabel;
    lB28: TQRLabel;
    lB34: TQRLabel;
    lB35: TQRLabel;
    lB36: TQRLabel;
    lB37: TQRLabel;
    lB38: TQRLabel;
    lB39: TQRLabel;
    lBie: TQRLabel;
    lB41: TQRLabel;
    lB42: TQRLabel;
    lB44: TQRLabel;
    qrshp44: TQRShape;
    qrshp45: TQRShape;
    qrshp46: TQRShape;
    lB45: TQRLabel;
    lB46: TQRLabel;
    lB47: TQRLabel;
    qrshp47: TQRShape;
    lB48: TQRLabel;
    qrchldbndChildBand1: TQRChildBand;
    qrshp56: TQRShape;
    lB20: TQRLabel;
    qrshp57: TQRShape;
    lB58: TQRLabel;
    lB59: TQRLabel;
    qrshp58: TQRShape;
    qrshp59: TQRShape;
    lB60: TQRLabel;
    lB61: TQRLabel;
    QrStrBand1: TQRStringsBand;
    lB53: TQRLabel;
    img2: TQRImage;
    lBNomeEmpresa: TQRLabel;
    lB54: TQRLabel;
    lBEndereco: TQRLabel;
    lBNumero: TQRLabel;
    lBBairro: TQRLabel;
    lBCidade: TQRLabel;
    lBFone: TQRLabel;
    lBCep: TQRLabel;
    lBcliente: TQRLabel;
    lBEnderecoCliente: TQRLabel;
    lBCidadeCliente: TQRLabel;
    lBCNPJ: TQRLabel;
    lBNumeroCliente: TQRLabel;
    lBBairroCliente: TQRLabel;
    lBCEPCliente: TQRLabel;
    lBFoneCliente: TQRLabel;
    lBCPF: TQRLabel;
    lB31: TQRLabel;
    lB52: TQRLabel;
    lB55: TQRLabel;
    lB56: TQRLabel;
    lBCorFerragem: TQRLabel;
    lBcorperfilado: TQRLabel;
    img1: TQRImage;
    lBXESP1: TQRLabel;
    lBXESP2: TQRLabel;
    lBXESP4: TQRLabel;
    lBXESP3: TQRLabel;
    qrshp1: TQRShape;
    lB29: TQRLabel;
    qrlblFaxCliente: TQRLabel;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    procedure lBLBDADOSPrint(sender: TObject; var Value: String);
    procedure lB53Print(sender: TObject; var Value: String);

  private

  public

  end;

var
  TQRPedidoCompraPadrao: TTQRPedidoCompraPadrao;

implementation

{$R *.DFM}

procedure TTQRPedidoCompraPadrao.lBLBDADOSPrint(sender: TObject;
  var Value: String);
var
apoio:string;
posicao:integer;
begin
     LBlbdados.font.Color:=clBLACK;
     LBLBDADOS.Font.Style:=[];
     LBLBDADOS.Font.size:=9;

     case QrStrBandDados.item[1] of
     '?':Begin//negrito
              LBLBDADOS.Font.Style:=[fsbold];
              value:=copy(QrStrBandDados.Item,2,length(QrStrBandDados.item));
         end;
     '�':Begin//cor

              apoio:=copy(QrStrBandDados.Item,2,length(QrStrBandDados.item));
              posicao:=pos('�',apoio);
              If (posicao=0)//tem que ter o fecho e entre eles a cor
              Then value:=QrStrBandDados.item
              Else Begin//s� 3 cores, vermelho,verde e azul
                        apoio:=copy(apoio,1,posicao-1);
                        if UPPERCASE(apoio)='VERMELHO'
                        tHEN LBlbdados.font.Color:=CLRED
                        eLSE
                                IF UPPERCASE(APOIO)='VERDE'
                                THEN LBlbdados.font.Color:=clGreen
                                ELSE
                                        IF UPPERCASE(APOIO)='AZUL'
                                        THEN LBlbdados.font.Color:=clBlue
                                        ELSE LBlbdados.font.Color:=clBLACK;


                        //pegando os dados fora a cor
                        value:=copy(QrStrBandDados.Item,posicao+2,length(QrStrBandDados.item));
              End;
         End;
     Else Begin
                value:=QrStrBandDados.item;
     End;

     end;


end;



procedure TTQRPedidoCompraPadrao.lB53Print(sender: TObject;
  var Value: String);
var
apoio:string;
posicao:integer;
begin
     lB53.font.Color:=clBLACK;
     lB53.Font.Style:=[];
     lB53.Font.size:=9;

     case QrStrBand1.item[1] of
     '?':Begin//negrito
              lB53.Font.Style:=[fsbold];
              value:=copy(QrStrBand1.Item,2,length(QrStrBand1.item));
         end;
     '�':Begin//cor

              apoio:=copy(QrStrBand1.Item,2,length(QrStrBand1.item));
              posicao:=pos('�',apoio);
              If (posicao=0)//tem que ter o fecho e entre eles a cor
              Then value:=QrStrBand1.item
              Else Begin//s� 3 cores, vermelho,verde e azul
                        apoio:=copy(apoio,1,posicao-1);
                        if UPPERCASE(apoio)='VERMELHO'
                        tHEN lB53.font.Color:=CLRED
                        eLSE
                                IF UPPERCASE(APOIO)='VERDE'
                                THEN lB53.font.Color:=clGreen
                                ELSE
                                        IF UPPERCASE(APOIO)='AZUL'
                                        THEN lB53.font.Color:=clBlue
                                        ELSE lB53.font.Color:=clBLACK;


                        //pegando os dados fora a cor
                        value:=copy(QrStrBand1.Item,posicao+2,length(QrStrBand1.item));
              End;
         End;
     Else Begin
                value:=QrStrBand1.item;
     End;

     end;


end;

end.
