unit UReltxtRDPRINT;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, DBTables, RDprint,printers;

type
  TFreltxtRDPRINT = class(TForm)
    botaoimprimir: TBitBtn;
    RDprint: TRDprint;
    procedure RDprintBeforeNewPage(Sender: TObject; Pagina: Integer);
    procedure RDprintNewPage(Sender: TObject; Pagina: Integer);

  private
    { Private declarations }

  public
    { Public declarations }

    ImprimeCabecalho:boolean;
    Procedure ConfiguraImpressao;
    Procedure VerificaLinha(Componente:TrdPrint;out linha:integer);
  end;

var
  FreltxtRDPRINT: TFreltxtRDPRINT;

  cabecalho:String;
implementation

uses UDataModulo, UessencialGlobal;

{$R *.DFM}


procedure TFreltxtRDPRINT.ConfiguraImpressao;
begin

     rdprint.OpcoesPreview.PreviewZoom:=91;
     rdprint.OpcoesPreview.Preview := True;
     rdprint.TitulodoRelatorio:='';
     rdprint.CaptionSetup:='';
     rdprint.TestarPorta:=False;
     rdprint.FonteTamanhoPadrao:=S12cpp;
     rdprint.Orientacao:=poPortrait;
     ImprimeCabecalho:=True;
     cabecalho:='';

     if (ObjParametroGlobal.LocalizaNome('CABECALHO NOS RELATORIOS')=FALSE)
     Then cabecalho:='EXCLAIM TECNOLOGIA - BY EXCLAIM TECNOLOGIA'
     Else Begin
               ObjParametroGlobal.TabelaparaObjeto;
               cabecalho:=ObjParametroGlobal.get_valor;
     End;
     cabecalho:=cabecalho+'  - '+datetostr(now)+'-'+timetostr(now);
     
     if (ObjParametroGlobal.LocalizaNome('QUANTIDADE DE COLUNAS PAPEL RETRATO')=FALSE)
     Then Begin
                rdprint.TamanhoQteColunas:=96;
                Messagedlg('O Par�metro "QUANTIDADE DE COLUNAS PAPEL RETRATO" n�o foi encontrado!'+#13+'Ser� usado 93 linhas',mterror,[mbok],0);
     End
     Else Begin
               ObjParametroGlobal.TabelaparaObjeto;
               TRY
               rdprint.TamanhoQteColunas:=STRTOINT(ObjParametroGlobal.get_valor);
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "QUANTIDADE DE COLUNAS PAPEL RETRATO"'+#13+'Ser� usado 96 colunas',mterror,[mbok],0);
                     rdprint.TamanhoQteColunas:=96;
               END;
     End;

     if (ObjParametroGlobal.LocalizaNome('QUANTIDADE DE LINHAS PAPEL RETRATO')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "QUANTIDADE DE LINHAS PAPEL RETRATO" n�o foi encontrado!'+#13+'Ser� usado 66 linhas',mterror,[mbok],0);
                rdprint.TamanhoQteLinhas:=66;
     End
     Else Begin
               ObjParametroGlobal.TabelaparaObjeto;
               TRY
                 rdprint.TamanhoQteLinhas:=STRTOINT(ObjParametroGlobal.get_valor);
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "QUANTIDADE DE LINHAS PAPEL RETRATO"'+#13+'Ser� usado 66 linhas',mterror,[mbok],0);
                     rdprint.TamanhoQteLinhas:=66;
               END;
     End;

     if (ObjParametroGlobal.LocalizaNome('PORTA DE IMPRESS�O')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "PORTA DE IMPRESS�O" n�o foi encontrado!'+#13+'Ser� usado "LPT1"',mterror,[mbok],0);
               rdprint.PortaComunicacao:='LPT1';
     End
     Else Begin
               ObjParametroGlobal.TabelaparaObjeto;
               TRY
                 rdprint.PortaComunicacao:=ObjParametroGlobal.get_valor;
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "PORTA DE IMPRESS�O"'+#13+'Ser� usado  "LPT1"',mterror,[mbok],0);
                     rdprint.PortaComunicacao:='LPT1';
               END;
     End;

     if (ObjParametroGlobal.LocalizaNome('ACENTUA��O NOS RELAT�RIOS?')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "ACENTUA��O NOS RELAT�RIOS?" n�o foi encontrado!'+#13+'Ser� usado "N�O"',mterror,[mbok],0);
               rdprint.Acentuacao:=SemAcento;
     End
     Else Begin
               ObjParametroGlobal.TabelaparaObjeto;
               IF (ObjParametroGlobal.get_valor='SIM')
               THEN rdprint.Acentuacao:=Transliterate
               Else rdprint.Acentuacao:=SemAcento;
     End;

     if (ObjParametroGlobal.LocalizaNome('USA GERENCIADOR DE IMPRESS�O DO WINDOWS')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "USA GERENCIADOR DE IMPRESS�O DO WINDOWS" n�o foi encontrado!'+#13+'Ser� usado "SIM"',mterror,[mbok],0);
               rdprint.UsaGerenciadorImpr:=True;
     End
     Else Begin
               ObjParametroGlobal.TabelaparaObjeto;
               if (ObjParametroGlobal.get_valor='SIM')
               Then Begin
                      rdprint.TestarPorta:=False;//naum pode testar a porta se usar o gerenciador
                      rdprint.UsaGerenciadorImpr:=True
               End
               Else Begin
                  If (ObjParametroGlobal.get_valor='N�O') or (ObjParametroGlobal.get_valor='NAO')
                  Then Begin
                            rdprint.testarporta:=True;
                            rdprint.UsaGerenciadorImpr:=False;
                  End
                  Else Begin
                            Messagedlg('Valor inv�lido no par�metro "USA GERENCIADOR DE IMPRESS�O DO WINDOWS"'+#13+'Ser� usado  "SIM"',mterror,[mbok],0);
                            rdprint.TestarPorta:=False;
                            rdprint.UsaGerenciadorImpr:=true;
                  End;
               End;
     End;

end;

procedure TFreltxtRDPRINT.RDprintBeforeNewPage(Sender: TObject;
  Pagina: Integer);
begin
     // Rodap�...
{     rdprint.imp (64,01,'===============================================================================================');
     rdprint.impf(65,01,'Deltress Inform�tica Ltda',[italico]);
     rdprint.impf(65,65,'Demonstra��o RdPrint 3.0',[comp17]);
     rdprint.imp (66, conta * 3,inttostr(conta));}
end;

procedure TFreltxtRDPRINT.RDprintNewPage(Sender: TObject;
  Pagina: Integer);
begin
     if (ImprimeCabecalho=True)
     Then Begin
           // Cabe�alho...
           rdprint.imp (01,01,cabecalho);

           if (RDprint.FonteTamanhoPadrao=s20cpp)//160 colunas
           Then rdprint.impD(02,160,'P�gina: ' + formatfloat('000',pagina),[])
           Else rdprint.impf(02,82,'P�gina: ' + formatfloat('000',pagina),[]);
     End;
     
     //Linha  := 8;
end;

procedure TFreltxtRDPRINT.VerificaLinha(Componente: TrdPrint;
  out linha: integer);
begin
     if (linha>=(Componente.TamanhoQteLinhas-3))
     Then Begin
               linha:=3;
               componente.Novapagina;
     End;
end;

end.

