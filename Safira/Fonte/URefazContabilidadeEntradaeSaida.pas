unit URefazContabilidadeEntradaeSaida;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, Buttons,ibquery;

type
  TFRefazContabilidadeEntradaeSaida = class(TForm)
    edtdatainicial: TMaskEdit;
    edtdatafinal: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    EdtRomaneio: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRefazContabilidadeEntradaeSaida: TFRefazContabilidadeEntradaeSaida;

implementation

uses UobjENTRADAPRODUTOS, UessencialGlobal, UobjPedidoObjetos, UDataModulo,
  UMostraBarraProgresso, 
  UobjExportaContabilidade, UFiltraImp;

{$R *.dfm}

procedure TFRefazContabilidadeEntradaeSaida.BitBtn1Click(Sender: TObject);
var
ObjEntradaProdutos:TObjENTRADAPRODUTOS;
begin
     Try
        ObjEntradaProdutos:=TObjENTRADAPRODUTOS.create;
     Except
        MensagemErro('Erro na tentativa de Criar o Objeto de Entrada de Produtos');
        exit;
     End;

     Try
        ObjEntradaProdutos.RefazContabilidade(edtdatainicial.text,edtdatafinal.text);
     Finally
            ObjEntradaProdutos.Free;
     End;

end;

procedure TFRefazContabilidadeEntradaeSaida.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFRefazContabilidadeEntradaeSaida.FormActivate(Sender: TObject);
begin
     edtdatainicial.setfocus;
end;

procedure TFRefazContabilidadeEntradaeSaida.BitBtn2Click(Sender: TObject);
var
ObjpedidoObjetos:TObjpedidoObjetos;
begin
     Try
        ObjpedidoObjetos:=TObjpedidoObjetos.create(Self);
     Except
        MensagemErro('Erro na tentativa de Criar o Objeto de Pedidos');
        exit;
     End;

     Try
        ObjpedidoObjetos.RefazContabilidade(edtdatainicial.text,edtdatafinal.text);
     Finally
            ObjpedidoObjetos.Free;
     End;

end;

procedure TFRefazContabilidadeEntradaeSaida.BitBtn3Click(Sender: TObject);
var
ObjpedidoObjetos:TObjpedidoObjetos;
begin
     Try
        ObjpedidoObjetos:=TObjpedidoObjetos.create(self);
     Except
        MensagemErro('Erro na tentativa de Criar o Objeto de Pedidos');
        exit;
     End;

     Try
        ObjpedidoObjetos.ApagaContabilidadeNaoConcluidos(edtdatainicial.text,edtdatafinal.text);
     Finally
            ObjpedidoObjetos.Free;
     End;

end;

procedure TFRefazContabilidadeEntradaeSaida.BitBtn4Click(Sender: TObject);
var
QueryLocal,QueryLocal2:Tibquery;
ObjPedidoObjetos:TObjPedidoObjetos;
ObjexportaContabilidade:TObjExportaContabilidade;
Pdata:String;
begin

     try
        if (edtromaneio.text='')
        then begin
                strtodate(edtdatainicial.Text);
                strtodate(edtdatafinal.Text);
        End;
     Except
        mensagemerro('Datas Inv�lidas');
        exit;
     End;

     Try
        QueryLocal:=Tibquery.create(nil);
        QueryLocal.Database:=FdataModulo.Ibdatabase;

        QueryLocal2:=Tibquery.create(nil);
        QueryLocal2.Database:=FdataModulo.Ibdatabase;

        ObjPedidoObjetos:=TObjPedidoObjetos.create(self);
        ObjexportaContabilidade:=TObjExportaContabilidade.create;
     except
        Mensagemerro('Erro na tentativa de Criar a Query');
        exit;
     End;

     Try
        With QueryLocal do
        begin
             pdata:='';
             
             close;
             sql.clear;
             sql.add('Select codigo,dataentrega from tabromaneio where concluido=''S''  ');
             if (EdtRomaneio.text='')
             then Begin
                        sql.add('and dataentrega>='+#39+formatdatetime('mm/dd/yyyy',strtodate(edtdatainicial.text))+#39);
                        sql.add('and dataentrega<='+#39+formatdatetime('mm/dd/yyyy',strtodate(edtdatafinal.text))+#39);
             End
             Else begin
                      sql.add('and codigo='+EdtRomaneio.text);
                      
                      With FfiltroImp do
                      Begin
                           DesativaGrupos;
                           Grupo01.Enabled:=True;
                           LbGrupo01.caption:='Data';
                           edtgrupo01.editmask:=MascaraData;
                           showmodal;
                           if (tag=0)
                           then exit;


                           if (Validadata(1,pdata,false)=False)
                           then exit;
                      End;
             End;
             
             open;
             last;
             FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
             FMostraBarraProgresso.Lbmensagem.caption:='Refazendo Romaneios';
             first;


             While not(eof) do
             begin
                  FMostraBarraProgresso.IncrementaBarra1(1);
                  FMostraBarraProgresso.Show;
                  Application.ProcessMessages;

                  if (ObjexportaContabilidade.excluiporgerador('OBJROMANEIO',fieldbyname('codigo').asstring)=False)
                  then Begin
                            Mensagemerro('Erro na tentativa de excluir a contabilidade do Romaneio '+fieldbyname('codigo').asstring);
                            exit;
                  End;

                  QueryLocal2.close;
                  QueryLocal2.sql.clear;
                  QueryLocal2.sql.add('Select TabPedidoProjetoromaneio.pedidoprojeto');
                  QueryLocal2.sql.add('from TabPedidoProjetoromaneio');
                  QueryLocal2.sql.add('where TabPedidoProjetoRomaneio.Romaneio='+Fieldbyname('codigo').asstring);
                  QueryLocal2.open;
                  QueryLocal2.first;

                  While not(QueryLocal2.eof) do
                  Begin

                       if (EdtRomaneio.Text='')
                       then Pdata:=fieldbyname('dataentrega').asstring;

                       if (ObjPedidoObjetos.ExportaContabilidade_financeiro_Romaneio(QueryLocal2.Fieldbyname('pedidoprojeto').asstring,Fieldbyname('codigo').asstring,pdata)=False)
                       Then Begin
                                 MensagemErro('Erro na tentativa de exportar a contabilidade do financeiro do Romaneio '+fieldbyname('codigo').asstring);
                                 exit;
                       End;
                  
                       if (ObjPedidoObjetos.ExportaContabilidade_Estoque_Romaneio(QueryLocal2.Fieldbyname('pedidoprojeto').asstring,Fieldbyname('codigo').asstring,pdata)=False)
                       Then Begin
                                 MensagemErro('Erro na tentativa de exportar a contabilidade do estoque do Romaneio '+fieldbyname('codigo').asstring);
                                 exit;
                       End;
                       QueryLocal2.next;     
                  End;

                  next;
             End;

             fdatamodulo.ibtransaction.CommitRetaining;
             mensagemaviso('Conclu�do');
        End;
     Finally
            fdatamodulo.ibtransaction.RollbackRetaining;
            freeandnil(querylocal);
            freeandnil(querylocal2);
            ObjPedidoObjetos.free;
            ObjexportaContabilidade.free;
     End;

end;

end.
