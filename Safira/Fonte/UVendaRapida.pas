unit UVendaRapida;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ExtCtrls, Buttons,UFiltraImp,UessencialGlobal,UobjPedidoObjetos,DB, UMostraBarraProgresso,UPEDIDO,
  ImgList, Menus;

type
  TFVendaRapida = class(TForm)
    pnlbotes: TPanel;
    pnl2: TPanel;
    Img1: TImage;
    lbdiasemanarodape: TLabel;
    lbdatarodape: TLabel;
    lb6: TLabel;
    STRGMateriais: TStringGrid;
    pnl6: TPanel;
    lbDia: TLabel;
    edtVendedor: TEdit;
    edtCliente: TEdit;
    lb1: TLabel;
    lb2: TLabel;
    lbNomeVendedor: TLabel;
    lbNomeCliente: TLabel;
    bt1: TSpeedButton;
    lb8: TLabel;
    lbdesagendar: TLabel;
    lbconcluir: TLabel;
    lb3: TLabel;
    lbValorVenda: TLabel;
    lb4: TLabel;
    lbTotalDesconto: TLabel;
    lb5: TLabel;
    lbTotalAcrescimo: TLabel;
    lb11: TLabel;
    COMBOTipo: TComboBox;
    lbValorTotal: TLabel;
    lb9: TLabel;
    lb7: TLabel;
    ilProdutos: TImageList;
    pnl3: TPanel;
    btProcessar: TSpeedButton;
    pmPopUpCadastros: TPopupMenu;
    MenuItem4: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure STRGMateriaisKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure STRGMateriaisKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtVendedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClienteDblClick(Sender: TObject);
    procedure edtVendedorDblClick(Sender: TObject);
    procedure lbdesagendarClick(Sender: TObject);
    procedure lbEnterMouseLeave(Sender: TObject);
    procedure lbEnterMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbconcluirClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtClienteExit(Sender: TObject);
    procedure COMBOTipoExit(Sender: TObject);
    procedure COMBOTipoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtVendedorExit(Sender: TObject);
    procedure STRGMateriaisDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure edtVendedorKeyPress(Sender: TObject; var Key: Char);
    procedure edtClienteKeyPress(Sender: TObject; var Key: Char);
    procedure STRGMateriaisDblClick(Sender: TObject);
    procedure bt1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure STRGMateriaisKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btProcessarClick(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
  private
     ObjPedidoObjetos:TObjPedidoObjetos;

     Procedure InserirMateriais;
     procedure PesquisaTipoMateriaisKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
     procedure PesquisaMateriaisKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
     procedure PesquisaCorMateriaisKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
     procedure LimpaLinhaSelecionadaStrgrid(ApartirDe:string);
     procedure Delete;
     procedure RetornaPrecoFerragem;
     procedure RetornaPrecoVidro;
     procedure RetornaPrecoDiverso;
     procedure RetornaPrecoPerfilado;
     procedure RetornaPrecoPersiana;
     procedure RetornaPrecoKitBox;
     procedure RetornaPrecoServico;
     procedure GerarOrcamento;
     procedure AtualizarLabels;
     procedure GeraStringGrid;
     procedure ValidaDadosLinhas;

  public
    { Public declarations }

  end;

var
  FVendaRapida: TFVendaRapida;

implementation

uses UescolheImagemBotao, Upesquisa, UDataModulo, UobjPEDIDO_PROJ_PEDIDO,
  UAjuda, UpesquisaMenu, Uprincipal;

{$R *.dfm}

procedure TFVendaRapida.FormShow(Sender: TObject);
begin
     FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');
     Self.ObjPedidoObjetos:=TObjPedidoObjetos.Create(self);
     lbdatarodape.Caption:=FormatDateTime('dd "de" mmmm "de" yyyy', Now);
     lbdiasemanarodape.Caption:= DiaSemana(Now);
     GeraStringGrid;
end;

procedure TFVendaRapida.InserirMateriais;
begin
      with FfiltroImp do
      begin
             DesativaGrupos;
      end;
end;

procedure TFVendaRapida.STRGMateriaisKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   PrecoFinal:Currency;
begin

   //Delete
   if(Key=46)then
   begin
       Delete;
       AtualizarLabels; //Atualiza valores finais no rodap�
   end;

   //Tecla para cima
   if(Key=38) then
   begin
          if(STRGMateriais.Cells[0,STRGMateriais.RowCount-1]='') and (STRGMateriais.Row>1) then
          begin
                 STRGMateriais.RowCount:=STRGMateriais.RowCount-1;
                 STRGMateriais.col:=0;
          end;
   end;

   if(Key=VK_F5)
   then GerarOrcamento;

   if(key=40) then
   begin
      //Atualiza valores finais no rodap�
      AtualizarLabels;
      ValidaDadosLinhas;

      //Atualizando o valor final do material em questa�o
      if(STRGMateriais.Cells[6,STRGMateriais.Row]<>'') and(STRGMateriais.Cells[3,STRGMateriais.Row]<>'') and(STRGMateriais.Cells[7,STRGMateriais.Row]<>'') and(STRGMateriais.Cells[8,STRGMateriais.Row]<>'') then
      begin
          PrecoFinal:=((StrToCurr(STRGMateriais.Cells[6,STRGMateriais.Row]) * StrToCurr(STRGMateriais.Cells[3,STRGMateriais.Row]))-StrToCurr(STRGMateriais.Cells[7,STRGMateriais.Row]))+StrToCurr(STRGMateriais.Cells[8,STRGMateriais.Row]) ;
          STRGMateriais.Cells[12,STRGMateriais.Row]:=formata_valor(PrecoFinal) ;
      end;


      //Caso coluna seja = 11, ou seja esteja na ultima coluna
      if(STRGMateriais.Row = STRGMateriais.RowCount-1 )then
      begin
          //Se a primeira coluna da linha estiver vazia, logo, n�o
          if(STRGMateriais.Cells[0,STRGMateriais.RowCount-1]='') then
          begin
                 STRGMateriais.col:=0;
                 Exit;
          end;

          STRGMateriais.RowCount:=STRGMateriais.RowCount+1;
          STRGMateriais.Row:=STRGMateriais.RowCount-1;
          STRGMateriais.col:=0;
      end
   end;

   if(Key<>37) and (Key<>38) and (Key<>39) and (Key<>40) then
   begin
      //Se for enter, cria nova linha para adicionar materiais
      if(key=13) then
      begin
          //Atualiza valores finais no rodap�
          AtualizarLabels;
          ValidaDadosLinhas;
          STRGMateriaisDblClick(Sender);

          //Atualizando o valor final do material em questa�o
          if(STRGMateriais.Cells[6,STRGMateriais.Row]<>'') and(STRGMateriais.Cells[3,STRGMateriais.Row]<>'') and(STRGMateriais.Cells[7,STRGMateriais.Row]<>'') and(STRGMateriais.Cells[8,STRGMateriais.Row]<>'') then
          begin
              PrecoFinal:=((StrToCurr(STRGMateriais.Cells[6,STRGMateriais.Row]) * StrToCurr(STRGMateriais.Cells[3,STRGMateriais.Row]))-StrToCurr(STRGMateriais.Cells[7,STRGMateriais.Row]))+StrToCurr(STRGMateriais.Cells[8,STRGMateriais.Row]) ;
              STRGMateriais.Cells[12,STRGMateriais.Row]:=formata_valor(PrecoFinal) ;
          end;

          //Caso coluna seja = 11, ou seja esteja na ultima coluna
          if(STRGMateriais.col=11)then
          begin
             //Se a primeira coluna da linha estiver vazia, logo, n�o
             if(STRGMateriais.Cells[0,STRGMateriais.RowCount-1]='') then
             begin
                 STRGMateriais.col:=0;
                 Exit;
             end;

             STRGMateriais.RowCount:=STRGMateriais.RowCount+1;
             STRGMateriais.Row:=STRGMateriais.RowCount-1;
             STRGMateriais.col:=0;


          end
          else STRGMateriais.col:= STRGMateriais.col+1;
      end;

      //Caso seja F9
      if(Key= VK_F9) then
      begin
          //Verifica coluna v�lida para pesquisa
          if(STRGMateriais.Col = 0)then
          begin
              LimpaLinhaSelecionadaStrgrid('0');
              PesquisaTipoMateriaisKeyDown(Sender,Key,Shift);
          end;
          if(STRGMateriais.Col = 1)then
          begin
               LimpaLinhaSelecionadaStrgrid('1');
               PesquisaMateriaisKeyDown(Sender,Key,Shift);
          end;

          if(STRGMateriais.Col = 2) then
          begin
               LimpaLinhaSelecionadaStrgrid('2');
               PesquisaCorMateriaisKeyDown(Sender,Key,Shift);
          end;
      end
      else
      begin
            //se tecla for diferente de F9
            //Verifica coluna v�lida para edi��o
            if(STRGMateriais.Col=3) or (STRGMateriais.Col=7) or (STRGMateriais.Col=8) then
            begin
                 STRGMateriais.Options:= STRGMateriais.Options + [goEditing];
            end
            else
            begin
                  if(STRGMateriais.Col=4) or (STRGMateriais.Col=5) then
                  begin
                       //Habilito campo altura/largura para edi��o se for o material vidro
                       if(STRGMateriais.Cells[0,STRGMateriais.Row]='VIDRO') or (STRGMateriais.Cells[0,STRGMateriais.Row]='PERFILADO')
                       then  STRGMateriais.Options:= STRGMateriais.Options + [goEditing]
                       else  STRGMateriais.Options:= STRGMateriais.Options - [goEditing];

                       //caso seja o material persiana, verifico se a persiana escolhida � controlada
                       //por metro quadrado, caso seja, habilito para campo altura;largura para edi��o
                       if(STRGMateriais.Cells[0,STRGMateriais.Row]='PERSIANA') then
                       begin
                            if (Self.ObjPedidoObjetos.ObjPersiana_PP.PersianaGrupoDiametroCor.GrupoPersiana.Get_ControlaPorMetroQuadrado = 'S')
                            then STRGMateriais.Options:= STRGMateriais.Options + [goEditing];
                       end;

                       //caso seja o material diverso, verifico se o diverso escolhido � controlado
                       //por metro quadrado, caso seja, habilito para campo altura;largura para edi��o
                       if(STRGMateriais.Cells[0,STRGMateriais.Row]='DIVERSO') then
                       begin
                            if (Self.ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.Diverso.Get_ContralaPorMetroQuadrado = 'S')
                            then STRGMateriais.Options:= STRGMateriais.Options + [goEditing];
                       end;
                  end
                  else
                  begin
                       Key:=0;
                       STRGMateriais.Options:= STRGMateriais.Options - [goEditing];
                  end;
            end;
      end;
   end;
end;

procedure TFVendaRapida.PesquisaTipoMateriaisKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('select * from tabtipomateriaisvenda','',nil)=True) Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)Then
                        Begin
                                STRGMateriais.Cells[0,STRGMateriais.Row]:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;

end;

procedure TFVendaRapida.PesquisaMateriaisKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   SQL:string;
begin
     Try
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='')
            then Exit;

            Fpesquisalocal:=Tfpesquisa.create(Nil);

            if(STRGMateriais.Cells[0,STRGMateriais.Row]='FERRAGEM')
            then  SQL:='Select * from tabferragem';
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='DIVERSO')
            then  SQL:='select * from tabdiverso';
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='PERFILADO')
            then  SQL:='select * from tabperfilado';
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='PERSIANA')
            then  SQL:='select * from tabpersiana';
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='VIDRO')
            then  SQL:='select * from tabvidro';
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='KITBOX')
            then  SQL:='Select * from tabkitbox';
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='SERVICO')
            then  SQL:='Select * from tabservico';

            If (FpesquisaLocal.PreparaPesquisa(SQL,'',nil)=True) Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)Then
                        Begin
                                if(STRGMateriais.Cells[0,STRGMateriais.Row]<>'PERSIANA')
                                then STRGMateriais.Cells[1,STRGMateriais.Row]:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring
                                else STRGMateriais.Cells[1,STRGMateriais.Row]:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                                STRGMateriais.Cells[10,STRGMateriais.Row]:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TFVendaRapida.PesquisaCorMateriaisKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   SQL:string;
begin
     Try
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='') or (STRGMateriais.Cells[1,STRGMateriais.Row]='') or (STRGMateriais.Cells[10,STRGMateriais.Row]='')
            then Exit;


            Fpesquisalocal:=Tfpesquisa.create(Nil);

            //***Montando o SQL de pesquisa
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='FERRAGEM') then
            begin
                 SQL:='Select tabcor.descricao,tabferragemcor.*';
                 SQL:=SQL +' from tabferragemcor';
                 SQL:=SQL +' join tabcor on tabcor.codigo=tabferragemcor.cor';
                 SQL:=SQL +' where ferragem='+STRGMateriais.Cells[10,STRGMateriais.Row];

            end;

            if(STRGMateriais.Cells[0,STRGMateriais.Row]='DIVERSO') then
            begin
                 SQL:='Select tabcor.descricao,tabdiversocor.*';
                 SQL:=SQL +' from tabdiversocor';
                 SQL:=SQL +' join tabcor on tabcor.codigo=tabdiversocor.cor';
                 SQL:=SQL +' where diverso='+STRGMateriais.Cells[10,STRGMateriais.Row];
            end;
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='PERFILADO') then
            begin
                 SQL:='Select tabcor.descricao,tabperfiladocor.*';
                 SQL:=SQL +' from tabperfiladocor';
                 SQL:=SQL +' join tabcor on tabcor.codigo=tabperfiladocor.cor';
                 SQL:=SQL +' where perfilado='+STRGMateriais.Cells[10,STRGMateriais.Row];
            end;
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='PERSIANA')then
            begin
                   SQL:='Select tabcor.descricao,tabpersianagrupodiametrocor.*';
                 SQL:=SQL +' from tabpersianagrupodiametrocor';
                 SQL:=SQL +' join tabcor on tabcor.codigo=tabpersianagrupodiametrocor.cor';
                 SQL:=SQL +' where persiana='+STRGMateriais.Cells[10,STRGMateriais.Row];
            end;
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='VIDRO') then
            begin
                 SQL:='Select tabcor.descricao,tabvidrocor.*';
                 SQL:=SQL +' from tabvidrocor';
                 SQL:=SQL +' join tabcor on tabcor.codigo=tabvidrocor.cor';
                 SQL:=SQL +' where vidro='+STRGMateriais.Cells[10,STRGMateriais.Row];
            end;
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='KITBOX')then
            begin
                 SQL:='Select tabcor.descricao,tabkitboxcor.*';
                 SQL:=SQL +' from tabkitboxcor';
                 SQL:=SQL +' join tabcor on tabcor.codigo=tabkitboxcor.cor';
                 SQL:=SQL +' where kitbox='+STRGMateriais.Cells[10,STRGMateriais.Row];
            end;
            if(STRGMateriais.Cells[0,STRGMateriais.Row]='SERVICO')then
            begin
                RetornaPrecoServico;
                STRGMateriais.Cells[3,STRGMateriais.Row]:= '1';
                STRGMateriais.Cells[7,STRGMateriais.Row]:= '0,00';
                STRGMateriais.Cells[8,STRGMateriais.Row]:= '0,00';
                STRGMateriais.Cells[12,STRGMateriais.Row]:=formata_valor(STRGMateriais.Cells[6,STRGMateriais.Row]);
                Exit;
            end;

            If (FpesquisaLocal.PreparaPesquisa(SQL,'',nil)=True) Then
            Begin
                      Try
                          If (FpesquisaLocal.showmodal=mrok)Then
                          Begin
                                  STRGMateriais.Cells[2,STRGMateriais.Row]:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;
                                  STRGMateriais.Cells[11,STRGMateriais.Row]:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

                                  if(STRGMateriais.Cells[0,STRGMateriais.Row]='FERRAGEM')
                                  then  RetornaPrecoFerragem;
                                  if(STRGMateriais.Cells[0,STRGMateriais.Row]='DIVERSO')
                                  then  RetornaPrecoDiverso;
                                  if(STRGMateriais.Cells[0,STRGMateriais.Row]='PERFILADO')
                                  then  RetornaPrecoPerfilado;
                                  if(STRGMateriais.Cells[0,STRGMateriais.Row]='PERSIANA')
                                  then  RetornaPrecoPersiana;
                                  if(STRGMateriais.Cells[0,STRGMateriais.Row]='VIDRO')
                                  then  RetornaPrecoVidro;
                                  if(STRGMateriais.Cells[0,STRGMateriais.Row]='KITBOX')
                                  then  RetornaPrecoKitBox;

                                  STRGMateriais.Cells[3,STRGMateriais.Row]:= '1';
                                  STRGMateriais.Cells[7,STRGMateriais.Row]:= '0,00';
                                  STRGMateriais.Cells[8,STRGMateriais.Row]:= '0,00';
                                  STRGMateriais.Cells[12,STRGMateriais.Row]:=formata_valor(STRGMateriais.Cells[6,STRGMateriais.Row]);

                          End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFVendaRapida.LimpaLinhaSelecionadaStrgrid(ApartirDe:string);
begin
   with STRGMateriais do
   begin
         // 0 = Limpa todas as celulas, � quando troca o tipo do material
         // 1 = Limpa a partir do material escolhido, � quando troca o material
         // 2 = Limpa a partir da cor do material, � quando troca a cor do material 

         if(ApartirDe<>'1') and (ApartirDe<>'2')
         then  Cells[0,STRGMateriais.Row] := '';

         if(ApartirDe<>'2')then
         begin
             Cells[1,STRGMateriais.Row] := '';
             Cells[10,STRGMateriais.Row] :='';
         end;

         Cells[2,STRGMateriais.Row] := '';
         Cells[3,STRGMateriais.Row] := '';
         Cells[4,STRGMateriais.Row] := '';
         Cells[5,STRGMateriais.Row] := '';
         Cells[6,STRGMateriais.Row] := '';
         Cells[7,STRGMateriais.Row] := '';
         Cells[8,STRGMateriais.Row] := '';
         Cells[9,STRGMateriais.Row] := '';
         Cells[11,STRGMateriais.Row]:= '';
         Cells[11,STRGMateriais.Row]:= '';
         Cells[12,STRGMateriais.Row]:= '';
   end;
end;

procedure TFVendaRapida.STRGMateriaisKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8),',']) then
       begin
             Key:= #0;
       end;

end;

procedure TFVendaRapida.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     If (self.ObjPedidoObjetos=Nil)
     Then exit;
end;

procedure TFVendaRapida.edtVendedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin 
   if(Key=13)
   then edtCliente.SetFocus;

   Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtVendedorKeyDown(Sender, key, Shift, LbNomeVendedor);
end;

procedure TFVendaRapida.edtClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if(Key=13)
   then  COMBOTipo.SetFocus;
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtClienteKeyDown(Sender, Key, Shift, LbNomeCliente);
end;

procedure TFVendaRapida.edtClienteDblClick(Sender: TObject);
var
  key:Word;
  Shift:TShiftState;
begin
    key:=VK_F9;
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtClienteKeyDown(Sender, Key, Shift, LbNomeCliente);
end;

procedure TFVendaRapida.edtVendedorDblClick(Sender: TObject);
var
  key:Word;
  Shift:TShiftState;
begin
    key:=VK_F9;
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtVendedorKeyDown(Sender, key, Shift, LbNomeVendedor);
end;


procedure TFVendaRapida.Delete;
var
    nlinha: integer;
begin
      if(STRGMateriais.Row=0)then
      begin
         exit;
      end;


      if STRGMateriais.RowCount = 1 then
      begin
            STRGMateriais.Cells[0,STRGMateriais.Row] := '';
            STRGMateriais.Cells[1,STRGMateriais.Row] := '';
            STRGMateriais.Cells[2,STRGMateriais.Row] := '';
            STRGMateriais.Cells[3,STRGMateriais.Row] := '';
            STRGMateriais.Cells[4,STRGMateriais.Row] := '';
            STRGMateriais.Cells[5,STRGMateriais.Row] := '';
            STRGMateriais.Cells[6,STRGMateriais.Row] := '';
            STRGMateriais.Cells[7,STRGMateriais.Row] := '';
            STRGMateriais.Cells[8,STRGMateriais.Row] := '';
            STRGMateriais.Cells[9,STRGMateriais.Row] := '';
            STRGMateriais.Cells[10,STRGMateriais.Row] := '';
            STRGMateriais.Cells[11,STRGMateriais.Row] := '';
            STRGMateriais.Cells[12,STRGMateriais.Row] :='' ;
      end;
      if STRGMateriais.RowCount = 2 then
      begin
            STRGMateriais.Cells[0,STRGMateriais.Row] := '';
            STRGMateriais.Cells[1,STRGMateriais.Row] := '';
            STRGMateriais.Cells[2,STRGMateriais.Row] := '';
            STRGMateriais.Cells[3,STRGMateriais.Row] := '';
            STRGMateriais.Cells[4,STRGMateriais.Row] := '';
            STRGMateriais.Cells[5,STRGMateriais.Row] := '';
            STRGMateriais.Cells[6,STRGMateriais.Row] := '';
            STRGMateriais.Cells[7,STRGMateriais.Row] := '';
            STRGMateriais.Cells[8,STRGMateriais.Row] := '';
            STRGMateriais.Cells[9,STRGMateriais.Row] := '';
            STRGMateriais.Cells[10,STRGMateriais.Row] := '';
            STRGMateriais.Cells[11,STRGMateriais.Row] := '';
            STRGMateriais.Cells[12,STRGMateriais.Row] :='' ;
            Exit;
      end;
      if STRGMateriais.RowCount-1 = STRGMateriais.Row then
      begin
            STRGMateriais.Cells[0,STRGMateriais.Row] := '';
            STRGMateriais.Cells[1,STRGMateriais.Row] := '';
            STRGMateriais.Cells[2,STRGMateriais.Row] := '';
            STRGMateriais.Cells[3,STRGMateriais.Row] := '';
            STRGMateriais.Cells[4,STRGMateriais.Row] := '';
            STRGMateriais.Cells[5,STRGMateriais.Row] := '';
            STRGMateriais.Cells[6,STRGMateriais.Row] := '';
            STRGMateriais.Cells[7,STRGMateriais.Row] := '';
            STRGMateriais.Cells[8,STRGMateriais.Row] := '';
            STRGMateriais.Cells[9,STRGMateriais.Row] := '';
            STRGMateriais.Cells[10,STRGMateriais.Row] := '';
            STRGMateriais.Cells[11,STRGMateriais.Row] := '';
            STRGMateriais.Cells[12,STRGMateriais.Row] :='' ;

      end;

     if (STRGMateriais.RowCount > 1) and (STRGMateriais.RowCount <> STRGMateriais.Row) then
      begin

              {Se o numero de linha for igual ao numero de ao numero da linha ent�o ele simplesmente limpa as c�lulas, ou entao ele
              llimpa as celulas da linhas selecionada e copia o conteudo da proxima, e assim por diante ate chegar na ultima, que ser� excluida.}
              STRGMateriais.Cells[0,STRGMateriais.Row] := '';
              STRGMateriais.Cells[1,STRGMateriais.Row] := '';
              STRGMateriais.Cells[2,STRGMateriais.Row] := '';
              STRGMateriais.Cells[3,STRGMateriais.Row] := '';
              STRGMateriais.Cells[4,STRGMateriais.Row] := '';
              STRGMateriais.Cells[5,STRGMateriais.Row] := '';
              STRGMateriais.Cells[6,STRGMateriais.Row] := '';
              STRGMateriais.Cells[7,STRGMateriais.Row] := '';
              STRGMateriais.Cells[8,STRGMateriais.Row] := '';
              STRGMateriais.Cells[9,STRGMateriais.Row] := '';
              STRGMateriais.Cells[10,STRGMateriais.Row] := '';
              STRGMateriais.Cells[11,STRGMateriais.Row] := '';
              STRGMateriais.Cells[12,STRGMateriais.Row] :='' ;

              nlinha := STRGMateriais.Row;

              while STRGMateriais.Row <> STRGMateriais.RowCount do
              begin
                  if STRGMateriais.Cells[1,(nlinha + 1)] <> '' then
                    begin
                          STRGMateriais.Cells[0,STRGMateriais.Row] := STRGMateriais.Cells[0,(nlinha +1)];
                          STRGMateriais.Cells[1,STRGMateriais.Row] := STRGMateriais.Cells[1,(nlinha +1)];
                          STRGMateriais.Cells[2,STRGMateriais.Row] := STRGMateriais.Cells[2,(nlinha +1)];
                          STRGMateriais.Cells[3,STRGMateriais.Row] := STRGMateriais.Cells[3,(nlinha +1)];
                          STRGMateriais.Cells[4,STRGMateriais.Row] := STRGMateriais.Cells[4,(nlinha +1)];
                          STRGMateriais.Cells[5,STRGMateriais.Row] := STRGMateriais.Cells[5,(nlinha +1)];
                          STRGMateriais.Cells[6,STRGMateriais.Row] := STRGMateriais.Cells[6,(nlinha +1)];
                          STRGMateriais.Cells[7,STRGMateriais.Row] := STRGMateriais.Cells[7,(nlinha +1)];
                          STRGMateriais.Cells[8,STRGMateriais.Row] := STRGMateriais.Cells[8,(nlinha +1)];
                          STRGMateriais.Cells[9,STRGMateriais.Row] := STRGMateriais.Cells[9,(nlinha +1)];
                          STRGMateriais.Cells[10,STRGMateriais.Row] := STRGMateriais.Cells[10,(nlinha +1)];
                          STRGMateriais.Cells[11,STRGMateriais.Row] := STRGMateriais.Cells[11,(nlinha +1)];
                          STRGMateriais.Cells[12,STRGMateriais.Row] := STRGMateriais.Cells[12,(nlinha +1)];
                    end
                  else
                  begin
                          STRGMateriais.RowCount := STRGMateriais.RowCount - 1;
                          STRGMateriais.Cells[0,nlinha] := '';
                          STRGMateriais.Cells[1,nlinha] := '';
                          STRGMateriais.Cells[2,nlinha] := '';
                          STRGMateriais.Cells[3,nlinha] := '';
                          STRGMateriais.Cells[4,nlinha] := '';
                          STRGMateriais.Cells[5,nlinha] := '';
                          STRGMateriais.Cells[6,nlinha] := '';
                          STRGMateriais.Cells[7,nlinha] := '';
                          STRGMateriais.Cells[8,nlinha] := '';
                          STRGMateriais.Cells[9,nlinha] := '';
                          STRGMateriais.Cells[10,nlinha] := '';
                          STRGMateriais.Cells[11,nlinha] := '';
                          STRGMateriais.Cells[12,nlinha] := '';
                          exit;

                  end;
                  STRGMateriais.Row := STRGMateriais.Row + 1;
                  nlinha := STRGMateriais.Row;
              end;
      end;
end;

procedure TFVendaRapida.lbdesagendarClick(Sender: TObject);
begin
      Delete;
      AtualizarLabels; //Atualiza valores finais no rodap�
end;

procedure TFVendaRapida.lbEnterMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFVendaRapida.lbEnterMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFVendaRapida.RetornaPrecoFerragem;
begin
     if(STRGMateriais.Cells[11,STRGMateriais.Row]='')
     then Exit;

     if (ObjPedidoObjetos.ObjFerragem_PP.FerragemCor.LocalizaCodigo(STRGMateriais.Cells[11,STRGMateriais.Row])=False)
     Then exit;
     ObjPedidoObjetos.ObjFerragem_PP.FerragemCor.TabelaparaObjeto;

     if (COMBOTipo.Text='Instalado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendaInstalado)+((strtofloat(ObjPedidoObjetos.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendaInstalado)*strtofloat(ObjPedidoObjetos.ObjFerragem_PP.FerragemCor.get_PorcentagemAcrescimoFinal))/100));


     if (COMBOTipo.Text='Retirado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendaretirado)+((strtofloat(ObjPedidoObjetos.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendaretirado)*strtofloat(ObjPedidoObjetos.ObjFerragem_PP.FerragemCor.get_PorcentagemAcrescimoFinal))/100));


     if (COMBOTipo.Text='Fornecido')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendafornecido)+((strtofloat(ObjPedidoObjetos.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendafornecido)*strtofloat(ObjPedidoObjetos.ObjFerragem_PP.FerragemCor.get_PorcentagemAcrescimoFinal))/100));

end;

procedure TFVendaRapida.RetornaPrecoVidro;
begin
     if(STRGMateriais.Cells[11,STRGMateriais.Row]='')
     then Exit;

     if (ObjPedidoObjetos.ObjVidro_PP.VidroCor.LocalizaCodigo(STRGMateriais.Cells[11,STRGMateriais.Row])=False)
     Then exit;
     ObjPedidoObjetos.ObjVidro_PP.VidroCor.TabelaparaObjeto;

     if (COMBOTipo.Text='Instalado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjVidro_PP.VidroCor.Vidro.Get_PrecoVendaInstalado)+((strtofloat(ObjPedidoObjetos.ObjVidro_PP.VidroCor.Vidro.Get_PrecoVendaInstalado)*strtofloat(ObjPedidoObjetos.ObjVidro_PP.VidroCor.get_PorcentagemAcrescimoFinal))/100));


     if (COMBOTipo.Text='Retirado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjVidro_PP.VidroCor.Vidro.Get_PrecoVendaretirado)+((strtofloat(ObjPedidoObjetos.ObjVidro_PP.VidroCor.Vidro.Get_PrecoVendaretirado)*strtofloat(ObjPedidoObjetos.ObjVidro_PP.VidroCor.get_PorcentagemAcrescimoFinal))/100));


     if (COMBOTipo.Text='Fornecido')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjVidro_PP.VidroCor.Vidro.Get_PrecoVendafornecido)+((strtofloat(ObjPedidoObjetos.ObjVidro_PP.VidroCor.Vidro.Get_PrecoVendafornecido)*strtofloat(ObjPedidoObjetos.ObjVidro_PP.VidroCor.get_PorcentagemAcrescimoFinal))/100));


end;

procedure TFVendaRapida.RetornaPrecoDiverso;
begin
     if(STRGMateriais.Cells[11,STRGMateriais.Row]='')
     then Exit;

     if (ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.LocalizaCodigo(STRGMateriais.Cells[11,STRGMateriais.Row])=False)
     Then exit;
     ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.TabelaparaObjeto;

     if (COMBOTipo.Text='Instalado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.Diverso.Get_PrecoVendaInstalado)+((strtofloat(ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.Diverso.Get_PrecoVendaInstalado)*strtofloat(ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.get_PorcentagemAcrescimoFinal))/100));


     if (COMBOTipo.Text='Retirado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.Diverso.Get_PrecoVendaretirado)+((strtofloat(ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.Diverso.Get_PrecoVendaretirado)*strtofloat(ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.get_PorcentagemAcrescimoFinal))/100));


     if (COMBOTipo.Text='Fornecido')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.Diverso.Get_PrecoVendafornecido)+((strtofloat(ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.Diverso.Get_PrecoVendafornecido)*strtofloat(ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.get_PorcentagemAcrescimoFinal))/100));

end;

procedure TFVendaRapida.RetornaPrecoPerfilado;
begin
     if(STRGMateriais.Cells[11,STRGMateriais.Row]='')
     then Exit;


     if (ObjPedidoObjetos.ObjPerfilado_PP.PerfiladoCor.LocalizaCodigo(STRGMateriais.Cells[11,STRGMateriais.Row])=False)
     Then exit;
     ObjPedidoObjetos.ObjPerfilado_PP.PerfiladoCor.TabelaparaObjeto;

     if (COMBOTipo.Text='Instalado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_PrecoVendaInstalado)+((strtofloat(ObjPedidoObjetos.ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_PrecoVendaInstalado)*strtofloat(ObjPedidoObjetos.ObjPerfilado_PP.PerfiladoCor.get_PorcentagemAcrescimoFinal))/100));


     if (COMBOTipo.Text='Retirado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_PrecoVendaretirado)+((strtofloat(ObjPedidoObjetos.ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_PrecoVendaretirado)*strtofloat(ObjPedidoObjetos.ObjPerfilado_PP.PerfiladoCor.get_PorcentagemAcrescimoFinal))/100));


     if (COMBOTipo.Text='Fornecido')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_PrecoVendafornecido)+((strtofloat(ObjPedidoObjetos.ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_PrecoVendafornecido)*strtofloat(ObjPedidoObjetos.ObjPerfilado_PP.PerfiladoCor.get_PorcentagemAcrescimoFinal))/100));

end;

procedure TFVendaRapida.RetornaPrecoPersiana;
begin
     if(STRGMateriais.Cells[11,STRGMateriais.Row]='')
     then Exit;

     if(ObjPedidoObjetos.ObjPersiana_PP.PersianaGrupoDiametroCor.LocalizaCodigo(STRGMateriais.Cells[11,STRGMateriais.Row])=False)
     then Exit;
     ObjPedidoObjetos.ObjPersiana_PP.PersianaGrupoDiametroCor.TabelaparaObjeto;

     if (COMBOTipo.Text='Instalado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(floattostr(strtofloat(ObjPedidoObjetos.ObjPersiana_PP.PersianaGrupoDiametroCor.Get_PrecoVendaInstalado)));


     if (COMBOTipo.Text='Retirado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(floattostr(strtofloat(ObjPedidoObjetos.ObjPersiana_PP.PersianaGrupoDiametroCor.Get_PrecoVendaretirado)));


     if (COMBOTipo.Text='Fornecido')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(floattostr(strtofloat(ObjPedidoObjetos.ObjPersiana_PP.PersianaGrupoDiametroCor.Get_PrecoVendafornecido)));


end;

procedure TFVendaRapida.RetornaPrecoServico;
begin
     if(STRGMateriais.Cells[10,STRGMateriais.Row]='')
     then Exit;

     if (ObjPedidoObjetos.ObjServico_PP.Servico.LocalizaCodigo(STRGMateriais.Cells[10,STRGMateriais.Row])=False)
     Then exit;
     ObjPedidoObjetos.ObjServico_PP.Servico.TabelaparaObjeto;

     if (COMBOTipo.Text='Instalado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjServico_PP.Servico.Get_PrecoVendaInstalado));


     if (COMBOTipo.Text='Retirado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjServico_PP.Servico.Get_PrecoVendaretirado));


     if (COMBOTipo.Text='Fornecido')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjServico_PP.Servico.Get_PrecoVendafornecido));

end;

procedure TFVendaRapida.RetornaPrecoKitBox;
begin
     if(STRGMateriais.Cells[11,STRGMateriais.Row]='')
     then Exit;

     if (ObjPedidoObjetos.ObjKitBox_PP.KitBoxCor.LocalizaCodigo(STRGMateriais.Cells[11,STRGMateriais.Row])=False)
     Then exit;
     ObjPedidoObjetos.ObjKitBox_PP.KitBoxCor.TabelaparaObjeto;

     if (COMBOTipo.Text='Instalado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjKitBox_PP.KitBoxCor.KitBox.Get_PrecoVendaInstalado)+((strtofloat(ObjPedidoObjetos.ObjKitBox_PP.KitBoxCor.kitBox.Get_PrecoVendaInstalado)*strtofloat(ObjPedidoObjetos.ObjKitBox_PP.KitBoxCor.get_PorcentagemAcrescimoFinal))/100));


     if (COMBOTipo.Text='Retirado')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjKitBox_PP.KitBoxCor.KitBox.Get_PrecoVendaretirado)+((strtofloat(ObjPedidoObjetos.ObjKitBox_PP.KitBoxCor.kitBox.Get_PrecoVendaretirado)*strtofloat(ObjPedidoObjetos.ObjKitBox_PP.KitBoxCor.get_PorcentagemAcrescimoFinal))/100));


     if (COMBOTipo.Text='Fornecido')
     Then STRGMateriais.Cells[6,STRGMateriais.Row]:=Formata_Valor(strtofloat(ObjPedidoObjetos.ObjKitBox_PP.KitBoxCor.kitBox.Get_PrecoVendafornecido)+((strtofloat(ObjPedidoObjetos.ObjKitBox_PP.KitBoxCor.kitBox.Get_PrecoVendafornecido)*strtofloat(ObjPedidoObjetos.ObjKitBox_PP.KitBoxCor.get_PorcentagemAcrescimoFinal))/100));

end;

procedure TFVendaRapida.lbconcluirClick(Sender: TObject);
begin
    GerarOrcamento;
end;

procedure TFVendaRapida.GerarOrcamento;
var
  CodigoPedido,CodigoProjetoBranco,CodigoPedidoProjeto:string;
  Cont:Integer;
  Fpedido:TFpedido;
begin
     //gerar um or�amento com os materiais escolhidos

     //A partir dos materiais escolhidos, preciso gerar um pedido
     //Gravar os materias no pedido...


      if(edtCliente.Text='')then
      begin
          MensagemAviso('� preciso escolher um cliente');
          Exit;
      end;

      if(edtVendedor.Text='')then
      begin
          MensagemAviso('� preciso escolher um vendedor');
          Exit;
      end;

      AtualizarLabels;
      ValidaDadosLinhas;

      {
      *************************************************************
             Primeiro gero um pedido
      *************************************************************
      }

      FMostraBarraProgresso.ConfiguracoesIniciais(STRGMateriais.RowCount-1,STRGMateriais.RowCount-1);
      FMostraBarraProgresso.lbmensagem.caption:='Gerando Or�amento';
      FMostraBarraProgresso.Show;
      Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Status:=dsInsert;
      With Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO do
      Begin

            Submit_Codigo('0');
            Submit_Descricao('Pedido R�pido para '+lbNomeCliente.Caption);
            Submit_Complemento('');
            Submit_Obra('');
            Submit_Email(''); //email
            Cliente.Submit_codigo(edtCliente.text);
            Arquiteto.Submit_CODIGO('');
            Vendedor.Submit_codigo(edtVendedor.text);
            Submit_Titulo('');
            Submit_Data(DateToStr(Now));
            Submit_DataOrcamento(DateToStr(Now)) ;
            Submit_Concluido('N');
            Submit_Observacao('');
            Submit_ValorTotal(tira_ponto(lbValorVenda.Caption));
            Submit_Valordesconto(tira_ponto(lbTotalDesconto.Caption));
            Submit_Valoracrescimo('0,00');
            Submit_PorcentagemArquiteto('');
            Submit_ValorComissaoArquiteto('');
            Submit_ValorBonificacaoCliente('');
            Submit_BonificacaoGerada('');
            Submit_PorcentagemArquiteto('0');

            If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.salvar(false)=False)
            Then exit;

            CodigoPedido:=ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_codigo;
      end;
      {
      *************************************************************
      Depois insiro um projeto em branco nesse pedido
      *************************************************************
      }
      With Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto do
      Begin

          //Grava na Tabela Pedido projeto, os dados iniciais
          ZerarTabela;
          Status:=dsInsert;
          if(ObjParametroGlobal.ValidaParametro('CODIGO DO PROJETO EM BRANCO')=false)then
          begin
                MensagemErro('Paramentro "CODIGO DO PROJETO EM BRANCO" n�o encontrado');
                CodigoProjetoBranco:='1';
          end
          else CodigoProjetoBranco:=ObjParametroGlobal.Get_Valor;

          Submit_Codigo('0');
          Pedido.Submit_Codigo(CodigoPedido);
          Projeto.Submit_Codigo(CodigoProjetoBranco);
          CorFerragem.Submit_Codigo('0');
          CorPerfilado.Submit_Codigo('0');
          CorVidro.Submit_Codigo('0');
          CorKitBox.Submit_Codigo('0');
          CorDiverso.Submit_Codigo('0');
          Submit_Local('');
          Submit_observacao('');
          Submit_TipoPedido(UpperCase(COMBOTipo.Text));
          Submit_ValorTotal (tira_ponto(lbValorVenda.Caption));
          Submit_ValorAcrescimo(lbTotalAcrescimo.Caption);
          Submit_ValorDesconto('0,00');
          if (Salvar(false)=False)
          Then Begin
                exit;
          End;
          CodigoPedidoProjeto:=Get_Codigo;

          if (Self.ObjPedidoObjetos.AtualizaValorPedido(CodigoPedido)=false)then
          Begin
               MensagemErro('N�o foi poss�vel Atualizar o Pedido');
               FDataModulo.IBTransaction.RollbackRetaining;
               exit;
          end;    


      End;

      {
      *************************************************************
      Por ultimo eu insiro os materiais da venda
      *************************************************************
      }

      Cont:=1;
      while Cont< STRGMateriais.RowCount do
      begin
            FMostraBarraProgresso.IncrementaBarra1(1);
            FMostraBarraProgresso.Show;
            if(STRGMateriais.Cells[0,Cont]='FERRAGEM') then
            begin
                  With Self.ObjPedidoObjetos.ObjFerragem_PP do
                  Begin
                      if(STRGMateriais.Cells[11,cont]='')then
                      begin
                            MensagemErro('Cor do material inv�lido na linha '+inttostr(cont));
                            FMostraBarraProgresso.Close;
                            exit;
                      end ;

                      if(STRGMateriais.Cells[3,cont]='')then
                      begin
                            MensagemErro('Quantidade inv�lida na linha '+inttostr(cont));
                            FMostraBarraProgresso.Close;
                            exit;
                      end;

                      ZerarTabela;
                      Status:=dsInsert;
                      Submit_Codigo('0');
                      Submit_PedidoProjeto(CodigoPedidoProjeto);
                      FerragemCor.Submit_Codigo(STRGMateriais.Cells[11,cont]);
                      Submit_Quantidade(STRGMateriais.Cells[3,cont]);
                      Submit_Valor(STRGMateriais.Cells[6,cont]);

                      if (Salvar(False)=False)
                      Then Begin
                                      exit;
                      End;

                  end;

            end;
            if(STRGMateriais.Cells[0,Cont]='DIVERSO') then
            begin
                  if(STRGMateriais.Cells[11,cont]='')then
                  begin
                        MensagemErro('Cor do material inv�lido na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end ;
                  if(STRGMateriais.Cells[3,cont]='')then
                  begin
                        MensagemErro('Quantidade inv�lida na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end;
                  With Self.ObjPedidoObjetos.ObjDiverso_PP do
                  Begin
                        ZerarTabela;
                        Status:=dsInsert;
                        Submit_Codigo('0');
                        Submit_PedidoProjeto(CodigoPedidoProjeto);
                        DiversoCor.Submit_Codigo(STRGMateriais.Cells[11,cont]);
                        Submit_Quantidade(STRGMateriais.Cells[3,cont]);
                        Submit_Valor(STRGMateriais.Cells[6,cont]);
                        Submit_Largura(STRGMateriais.Cells[5,cont]);
                        Submit_Altura(STRGMateriais.Cells[4,cont]);

                        if (Salvar(False)=False)Then
                        Begin
                                exit;
                        End;
                  end;
            end ;
            if(STRGMateriais.Cells[0,Cont]='VIDRO') then
            begin
                  if(STRGMateriais.Cells[11,cont]='')then
                  begin
                        MensagemErro('Cor do material inv�lido na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end ;

                  if(STRGMateriais.Cells[3,cont]='')then
                  begin
                        MensagemErro('Quantidade inv�lida na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end;

                  if(STRGMateriais.Cells[5,cont]='') or (STRGMateriais.Cells[4,cont]='') then
                  begin
                        MensagemErro('Altura/Largura inv�lida na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end;

                 //Arrumar, passar altura e largura
                  With Self.ObjPedidoObjetos.ObjVidro_PP do
                  Begin
                      ZerarTabela;
                      Status:=dsInsert;
                      Submit_Codigo('0');
                      Submit_PedidoProjeto(CodigoPedidoProjeto);
                      VidroCor.Submit_Codigo(STRGMateriais.Cells[11,cont]);
                      Submit_Quantidade(STRGMateriais.Cells[3,cont]);
                      Submit_Valor(STRGMateriais.Cells[6,cont]);
                      Submit_Largura(STRGMateriais.Cells[5,cont]);
                      Submit_Altura(STRGMateriais.Cells[4,cont]);

                      if (Salvar(False)=False) Then
                      Begin
                          exit;
                      End;
                  end;
            end ;
            if(STRGMateriais.Cells[0,Cont]='PERFILADO') then
            begin
                  if(STRGMateriais.Cells[11,cont]='')then
                  begin
                        MensagemErro('Cor do material inv�lido na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end ;

                  if(STRGMateriais.Cells[3,cont]='')then
                  begin
                        MensagemErro('Quantidade inv�lida na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end;
                  With Self.ObjPedidoObjetos.ObjPerfilado_PP do
                  Begin
                      ZerarTabela;
                      Status:=dsInsert;
                      Submit_Codigo('0');
                      Submit_PedidoProjeto(CodigoPedidoProjeto);
                      PerfiladoCor.Submit_Codigo(STRGMateriais.Cells[11,cont]);
                      Submit_Quantidade(STRGMateriais.Cells[3,cont]);
                      Submit_Valor(STRGMateriais.Cells[6,cont]);

                      if (Salvar(False)=False)
                      Then Begin
                                exit;
                      End;
                  end;
            end;
            if(STRGMateriais.Cells[0,Cont]='PERSIANA') then
            begin
                  if(STRGMateriais.Cells[11,cont]='')then
                  begin
                        MensagemErro('Cor do material inv�lido na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end ;

                  if(STRGMateriais.Cells[3,cont]='')then
                  begin
                        MensagemErro('Quantidade inv�lida na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end;

                 { if(STRGMateriais.Cells[5,cont]='') or (STRGMateriais.Cells[4,cont]='') then
                  begin
                        MensagemErro('Altura/Largura inv�lida na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end;       }

                  With Self.ObjPedidoObjetos.ObjPersiana_PP do
                  Begin
                      ZerarTabela;
                      Status:=dsInsert;
                      Submit_Codigo('0');
                      Submit_PedidoProjeto(CodigoPedidoProjeto);
                      PersianaGrupoDiametroCor.Submit_Codigo(STRGMateriais.Cells[11,cont]);
                      Submit_Quantidade(STRGMateriais.Cells[3,cont]);
                      Submit_Valor(STRGMateriais.Cells[6,cont]);
                      Submit_Largura(STRGMateriais.Cells[5,cont]);
                      Submit_Altura(STRGMateriais.Cells[4,cont]);

                      if (Salvar(False)=False) Then
                      Begin
                          exit;
                      End;
                  end;
            end ;
            if(STRGMateriais.Cells[0,Cont]='KITBOX') then
            begin
                  if(STRGMateriais.Cells[11,cont]='')then
                  begin
                        MensagemErro('Cor do material inv�lido na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end ;

                  if(STRGMateriais.Cells[3,cont]='')then
                  begin
                        MensagemErro('Quantidade inv�lida na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end;
                  With Self.ObjPedidoObjetos.ObjKitBox_PP do
                  Begin
                      ZerarTabela;
                      Status:=dsInsert;
                      Submit_Codigo('0');
                      Submit_PedidoProjeto(CodigoPedidoProjeto);
                      KitBoxCor.Submit_Codigo(STRGMateriais.Cells[11,cont]);
                      Submit_Quantidade(STRGMateriais.Cells[3,cont]);
                      Submit_Valor(STRGMateriais.Cells[6,cont]);
                      if (Salvar(False)=False)
                      Then Begin
                                exit;
                      End;
                  end;
            end ;
            if(STRGMateriais.Cells[0,Cont]='SERVICO') then
            begin
                  if(STRGMateriais.Cells[10,cont]='')then
                  begin
                        MensagemErro('Codigo do Servi�o invalido na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end ;
                  if(STRGMateriais.Cells[3,cont]='')then
                  begin
                        MensagemErro('Quantidade inv�lida na linha '+inttostr(cont));
                        FMostraBarraProgresso.Close;
                        exit;
                  end;
                  self.ObjPedidoObjetos.ObjServico_PP.ZerarTabela;
                  Self.ObjPedidoObjetos.ObjServico_PP.Status:=dsInsert;
                  self.ObjPedidoObjetos.ObjServico_PP.Submit_Codigo('0');
                  self.ObjPedidoObjetos.ObjServico_PP.Submit_PedidoProjeto(CodigoPedidoProjeto);
                  self.ObjPedidoObjetos.ObjServico_PP.Servico.Submit_Codigo(STRGMateriais.Cells[10,cont]);
                  Self.ObjPedidoObjetos.ObjServico_PP.Submit_Valor(STRGMateriais.Cells[6,cont]);
                  Self.ObjPedidoObjetos.ObjServico_PP.Submit_Quantidade(STRGMateriais.Cells[3,cont]);
                  if(Self.ObjPedidoObjetos.ObjServico_PP.Salvar(False)=False) then
                  begin
                       exit;
                  end;
            end;

            inc(Cont,1);
      end;
      FMostraBarraProgresso.Close;
      MensagemSucesso('Or�amento Gerado com sucesso!!!');
      FDataModulo.IBTransaction.CommitRetaining;

      edtCliente.Text:='';
      edtVendedor.Text:='';
      lbNomeCliente.Caption:='';
      lbNomeVendedor.Caption:='';
      lbTotalDesconto.Caption:='';
      lbTotalAcrescimo.Caption:='';
      lbValorVenda.Caption:='';
      lbValorTotal.Caption:='';

      LimpaStringGrid(STRGMateriais);
      GeraStringGrid;

      Try
          Fpedido:=TFpedido.Create(nil);
      Except
          Messagedlg('Erro na tentativa de Criar o Cadastro de Pedido',mterror,[mbok],0);
          exit;
      End;

      try
          if(CodigoPedido='')
          then Exit;
          Fpedido.Tag:=StrToInt(CodigoPedido);
          Fpedido.ShowModal;
      finally
          FreeAndNil(Fpedido);
      end;



end;

procedure TFVendaRapida.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if (Key=#27)
   Then Self.Close;
end;

procedure TFVendaRapida.edtClienteExit(Sender: TObject);
begin
      Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtClienteExit(Sender, LbNomeCliente);
end;

procedure TFVendaRapida.COMBOTipoExit(Sender: TObject);
begin
     //
end;

procedure TFVendaRapida.COMBOTipoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if(Key=13)
    then  STRGMateriais.SetFocus;
end;

procedure  TFVendaRapida.AtualizarLabels;
var
  Cont:Integer;
  Acres,Desc:Currency;
  pquantidade,paltura,plargura,plarguraarredondamento,palturaarredondamento:Currency;
begin
      cont:=1;
      lbValorVenda.Caption:='0,00';
      lbTotalDesconto.Caption:='0,00';
      lbTotalAcrescimo.Caption:='0,00';
      lbValorTotal.Caption:='0,00' ;
      while Cont<STRGMateriais.RowCount do
      begin
            //Atualizando o pre�o da venda
            if(STRGMateriais.Cells[6,cont] ='') or (STRGMateriais.Cells[3,cont] ='')
            then Exit;

            lbValorVenda.Caption:=formata_valor(StrToFloat(tira_ponto(lbValorVenda.Caption))+(StrToFloat(STRGMateriais.Cells[6,cont]) * StrToFloat(STRGMateriais.Cells[3,Cont])));

            lbTotalDesconto.Caption:= formata_valor(StrToFloat(lbTotalDesconto.Caption)+StrToFloat(STRGMateriais.Cells[7,cont]));

            lbTotalAcrescimo.Caption:= formata_valor(StrToFloat(lbTotalAcrescimo.Caption )+StrToFloat(STRGMateriais.Cells[8,cont]));

            STRGMateriais.Cells[7,cont]:=formata_valor(STRGMateriais.Cells[7,cont]);
            STRGMateriais.Cells[8,cont]:=formata_valor(STRGMateriais.Cells[8,cont]);

            if (STRGMateriais.Cells[4,cont] <> '') and (STRGMateriais.Cells[5,cont] <> '') then
            Begin
                  Try
                     paltura:=strtocurr(tira_ponto(STRGMateriais.Cells[4,cont]));
                  Except
                        paltura:=0;
                  End;

                  Try
                     plargura:=strtocurr(tira_ponto(STRGMateriais.Cells[5,cont]));
                  Except
                      plargura:=0;
                  End;

                  FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
                  FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);

                  pquantidade:=((plargura+plarguraarredondamento)*(paltura+palturaarredondamento))/1000000;

                  if(StrToCurr(get_campoTabela('areaminima','codigo','tabvidro',STRGMateriais.Cells[10,cont]))>pquantidade)
                  then pquantidade:=StrToCurr(get_campoTabela('areaminima','codigo','tabvidro',STRGMateriais.Cells[10,cont]));

                  STRGMateriais.Cells[3,cont]:=formata_valor(CurrToStr(pquantidade));
            end;
            Inc(Cont,1);
      end;

      lbValorTotal.Caption:='0';

      if(StrToCurr(lbTotalAcrescimo.Caption)>0)
      then Acres:=StrToCurr(lbTotalAcrescimo.Caption);

      if(StrToCurr(lbTotalDesconto.Caption)>0)
      then Desc:=StrToCurr(lbTotalDesconto.Caption);

      Acres:=Acres-Desc;

      lbValorTotal.caption:=formata_valor(StrToFloat(tira_ponto(lbValorVenda.Caption))+Acres);


end;


procedure TFVendaRapida.edtVendedorExit(Sender: TObject);
begin
      Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtVendedorExit(Sender, LbNomeVendedor);
end;

procedure TFVendaRapida.STRGMateriaisDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
       //Draw(1,1, Image1.Picture.Graphic);
       //StretchDraw(Rect,);
       //STRGMateriais.Canvas.StretchDraw(Sender.CellRect(Col,Row);,Img1.Picture.Graphic);
       //STRGMateriais.Canvas.Draw(Rect.Left, Rect.Top, Img1.Picture.Graphic);
        if((ARow <> 0) and (ACol = 13))
        then begin
            if(STRGMateriais.Cells[ACol,ARow] = '            x' ) then
            begin
                 Ilprodutos.Draw(STRGMateriais.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 3); //check box marcado       6
            end
            else
            begin
                if(STRGMateriais.Cells[ACol,ARow] = '            v' ) then
                begin
                     Ilprodutos.Draw(STRGMateriais.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 2); //check box sem marcar    5
                end;
            end;
        end;

        {if gdSelected in State then
        begin
            { Cor do fonte e de fundo para linhas e colunas selecionadas }
           { STRGMateriais.Canvas.Font.Color := clWhite;
            STRGMateriais.Canvas.Brush.Color := $00D5FFFF;
        end   }

end;

procedure TFVendaRapida.edtVendedorKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (Key in['0'..'9',Chr(8),',']) then
    begin
        Key:= #0;
    end;
end;

procedure TFVendaRapida.edtClienteKeyPress(Sender: TObject; var Key: Char);
begin
     if not (Key in['0'..'9',Chr(8),',']) then
    begin
        Key:= #0;
    end;
end;

procedure TFVendaRapida.GeraStringGrid;
begin
     with STRGMateriais do
     begin
           RowCount:= 2;
           ColCount:= 14;
           FixedRows:=1;
           FixedCols:=0;

           ColWidths[0] := 200;
           ColWidths[1] := 350;
           ColWidths[2] := 200;
           ColWidths[3] := 100;
           ColWidths[4] := 100;
           ColWidths[5] := 100;
           ColWidths[6] := 100;
           ColWidths[7] := 100;
           ColWidths[8] := 100;
           ColWidths[9] := 50;
           ColWidths[10] := 80;
           ColWidths[11] := 80;
           ColWidths[12] := 100;
           ColWidths[13] := 50;

           Cells[0,0] := 'TIPO';
           Cells[1,0] := 'MATERIAL';
           Cells[2,0] := 'COR';
           Cells[3,0] := 'QUANTIDADE';
           Cells[4,0] := 'ALTURA';
           Cells[5,0] := 'LARGURA';
           Cells[6,0] := 'PRE�O';
           Cells[7,0] := 'DESCONTO' ;
           Cells[8,0] := 'ACR�SCIMO';
           Cells[9,0] := 'C�DIGO';
           Cells[10,0] :='COD MATERIAL';
           Cells[11,0] :='COD COR';
           Cells[12,0] :='VALOR FINAL';
           Cells[13,0] :='STATUS';

     end;
end;


procedure TFVendaRapida.STRGMateriaisDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
      if(STRGMateriais.Col = 0)then
      begin
           LimpaLinhaSelecionadaStrgrid('0');
           PesquisaTipoMateriaisKeyDown(Sender,Key,Shift);
           STRGMateriais.Col:=STRGMateriais.Col+1;
      end;
      if(STRGMateriais.Col = 1)then
      begin
          LimpaLinhaSelecionadaStrgrid('1');
          PesquisaMateriaisKeyDown(Sender,Key,Shift);
          STRGMateriais.Col:=STRGMateriais.Col+1;
      end; 
      if(STRGMateriais.Col = 2) then
      begin
          LimpaLinhaSelecionadaStrgrid('2');
          PesquisaCorMateriaisKeyDown(Sender,Key,Shift);
          STRGMateriais.Col:=STRGMateriais.Col+1;     
          AtualizarLabels;
      end;

end;

procedure TFVendaRapida.bt1Click(Sender: TObject);
begin
     FAjuda.PassaAjuda('PEDIDO R�PIDO');
     FAjuda.ShowModal;
end;

procedure TFVendaRapida.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
     if (Key = VK_F1) then
    begin

      try
        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('PEDIDO R�PIDO');
         FAjuda.ShowModal;
    end;
end;

//r4mr
procedure TFVendaRapida.STRGMateriaisKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   //Tecla para direita - abre a busca igualmente o duplo click - r4mr
  { if(Key=13) then
   begin
       STRGMateriaisDblClick(Sender);
   end;  }
end;

procedure TFVendaRapida.ValidaDadosLinhas;
var
  cont:Integer;
begin
      if(STRGMateriais.Cells[0,STRGMateriais.Row]='FERRAGEM') then
      begin
            STRGMateriais.Cells[13,STRGMateriais.Row]:='            v';
            if(STRGMateriais.Cells[11,STRGMateriais.Row]='')then
            begin
                 STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end ;

            if(STRGMateriais.Cells[3,STRGMateriais.Row]='')then
            begin
                 STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end;

      end;
      if(STRGMateriais.Cells[0,STRGMateriais.Row]='DIVERSO') then
      begin
            STRGMateriais.Cells[13,STRGMateriais.Row]:='            v';
            if(STRGMateriais.Cells[11,STRGMateriais.Row]='')then
            begin
                 STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end ;
            if(STRGMateriais.Cells[3,STRGMateriais.Row]='')then
            begin
                 STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end;

      end ;
      if(STRGMateriais.Cells[0,STRGMateriais.Row]='VIDRO') then
      begin
            STRGMateriais.Cells[13,STRGMateriais.Row]:='            v';
            if(STRGMateriais.Cells[11,STRGMateriais.Row]='')then
            begin
                 STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end ;

            if(STRGMateriais.Cells[3,STRGMateriais.Row]='')then
            begin
                STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end;

            if(STRGMateriais.Cells[5,STRGMateriais.Row]='') or (STRGMateriais.Cells[4,STRGMateriais.Row]='') then
            begin
                STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end;

      end ;
      if(STRGMateriais.Cells[0,STRGMateriais.Row]='PERFILADO') then
      begin
            STRGMateriais.Cells[13,STRGMateriais.Row]:='            v';
            if(STRGMateriais.Cells[11,STRGMateriais.Row]='')then
            begin
                 STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end ;

            if(STRGMateriais.Cells[3,STRGMateriais.Row]='')then
            begin
                STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end;

      end;
      if(STRGMateriais.Cells[0,STRGMateriais.Row]='PERSIANA') then
      begin
            STRGMateriais.Cells[13,STRGMateriais.Row]:='            v';
            if(STRGMateriais.Cells[11,STRGMateriais.Row]='')then
            begin
                STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end ;

            if(STRGMateriais.Cells[3,STRGMateriais.Row]='')then
            begin
                STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end;


      end ;
      if(STRGMateriais.Cells[0,STRGMateriais.Row]='KITBOX') then
      begin
            STRGMateriais.Cells[13,STRGMateriais.Row]:='            v';
            if(STRGMateriais.Cells[11,STRGMateriais.Row]='')then
            begin
                STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end ;

            if(STRGMateriais.Cells[3,STRGMateriais.Row]='')then
            begin
                STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end;

      end ;
      if(STRGMateriais.Cells[0,STRGMateriais.Row]='SERVICO') then
      begin
            STRGMateriais.Cells[13,STRGMateriais.Row]:='            v';
            if(STRGMateriais.Cells[10,STRGMateriais.Row]='')then
            begin
                 STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end ;

            if(STRGMateriais.Cells[3,STRGMateriais.Row]='')then
            begin
                 STRGMateriais.Cells[13,STRGMateriais.Row]:='            x';
            end;

      end;

end;

procedure TFVendaRapida.btProcessarClick(Sender: TObject);
begin

     GerarOrcamento;
end;

procedure TFVendaRapida.MenuItem4Click(Sender: TObject);
var
  Quantidade:string;
  cont:Integer;
begin
    if(STRGMateriais.Cells[0,STRGMateriais.Row]='')
    then Exit;

    //replica o material
    InputQuery('Quantidade','Quantidade',Quantidade);

    for cont:=1 to StrToInt(Quantidade) do
    begin
          STRGMateriais.RowCount:=STRGMateriais.RowCount+1;
          STRGMateriais.Cells[0,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[0,STRGMateriais.Row];
          STRGMateriais.Cells[1,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[1,STRGMateriais.Row];
          STRGMateriais.Cells[2,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[2,STRGMateriais.Row];
          STRGMateriais.Cells[3,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[3,STRGMateriais.Row];
          STRGMateriais.Cells[4,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[4,STRGMateriais.Row];
          STRGMateriais.Cells[5,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[5,STRGMateriais.Row];
          STRGMateriais.Cells[6,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[6,STRGMateriais.Row];
          STRGMateriais.Cells[7,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[7,STRGMateriais.Row];
          STRGMateriais.Cells[8,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[8,STRGMateriais.Row];
          STRGMateriais.Cells[9,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[9,STRGMateriais.Row];
          STRGMateriais.Cells[10,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[10,STRGMateriais.Row];
          STRGMateriais.Cells[11,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[11,STRGMateriais.Row];
          STRGMateriais.Cells[12,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[12,STRGMateriais.Row];
          STRGMateriais.Cells[13,STRGMateriais.RowCount-1]:=STRGMateriais.Cells[13,STRGMateriais.Row];
    end;
     AtualizarLabels;
end;

end.
