unit UPesquisaProjNovo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons,DB,IBQuery,UDataModulo,UpesquisaProjeto,UessencialGlobal;

type
  TFpesquisaProjNovo = class(TForm)
    img1: TImage;
    img2: TImage;
    img3: TImage;
    img4: TImage;
    img5: TImage;
    img6: TImage;
    img7: TImage;
    bvl1: TBevel;
    bvl2: TBevel;
    bvl3: TBevel;
    bvl4: TBevel;
    bvl5: TBevel;
    bvl6: TBevel;
    bt1: TSpeedButton;
    bt2: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure but2Click(Sender: TObject);
    procedure but1Click(Sender: TObject);
    procedure img2MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure img3MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure img4MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure img5MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure img6MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure img7MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure bt3Click(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);


  private
      ListaDeImagens:Tstrings;
      ListaDeReferencias:TStrings;
      ultimaimagemlista:Integer;
      function ResgataLocalDesenhos: string;
      function ResgataDesenho(PReferencia : string): string;

  public
    { Public declarations }
  end;

var
  FpesquisaProjNovo: TFpesquisaProjNovo;

implementation

{$R *.dfm}

procedure TFpesquisaProjNovo.FormShow(Sender: TObject);
var
  i:integer;
  Query:TIBQuery;
  Desenho:string;
begin
     try
         self.ListaDeImagens:= TStringList.Create;
     except

     end;

     try
          Query:=TIBQuery.Create(nil);
          Query.Database:=FDataModulo.IBDatabase;
     except

     end;
      ListaDeImagens.Clear;
     with Query do
     begin
          Close;
          sql.Clear;
          sql.Add('select * from tabprojeto');
          Open;
          while not Eof do
          begin
               Desenho:=ResgataDesenho(fieldbyname('Referencia').AsString);
               if(Desenho<>'') then
               ListaDeImagens.Add(Desenho);
               Next;

          end;
          ShowMessage(IntToStr(recordcount));
     end;



     //ListaDeImagens.Add('D:\Programa��o\SVN\Safira\Fonte\desenhosglassbox\desenhosglassbox\001.bmp');

     img2.Picture.LoadFromFile(ListaDeImagens[0]);
     img3.Picture.LoadFromFile(ListaDeImagens[1]);
     img4.Picture.LoadFromFile(ListaDeImagens[2]);
     img5.Picture.LoadFromFile(ListaDeImagens[3]);
     img6.Picture.LoadFromFile(ListaDeImagens[4]);
     img7.Picture.LoadFromFile(ListaDeImagens[5]);

     self.ultimaimagemlista:=5;
     bt1.Enabled:=False;
     bvl2.Visible:=False;
     bvl3.Visible:=False;
     bvl4.Visible:=False;
     bvl5.Visible:=False;
     bvl6.Visible:=False;
end;

procedure TFpesquisaProjNovo.but2Click(Sender: TObject);
begin
     img2.Picture.LoadFromFile(ListaDeImagens[ultimaimagemlista+1]);
     img3.Picture.LoadFromFile(ListaDeImagens[ultimaimagemlista+2]);
     img4.Picture.LoadFromFile(ListaDeImagens[ultimaimagemlista+3]);
     img5.Picture.LoadFromFile(ListaDeImagens[ultimaimagemlista+4]);
     img6.Picture.LoadFromFile(ListaDeImagens[ultimaimagemlista+5]);
     img7.Picture.LoadFromFile(ListaDeImagens[ultimaimagemlista+6]);
     self.ultimaimagemlista:=ultimaimagemlista+6;
     if(ultimaimagemlista=ListaDeImagens.Count)
     then bt2.Enabled:=False;
     bt1.Enabled:=True;
end;

procedure TFpesquisaProjNovo.but1Click(Sender: TObject);
begin
     img2.Picture.LoadFromFile(ListaDeImagens[ultimaimagemlista-11]);
     img3.Picture.LoadFromFile(ListaDeImagens[ultimaimagemlista-10]);
     img4.Picture.LoadFromFile(ListaDeImagens[ultimaimagemlista-9]);
     img5.Picture.LoadFromFile(ListaDeImagens[ultimaimagemlista-8]);
     img6.Picture.LoadFromFile(ListaDeImagens[ultimaimagemlista-7]);
     img7.Picture.LoadFromFile(ListaDeImagens[ultimaimagemlista-6]);
     self.ultimaimagemlista:=ultimaimagemlista-6;
     if(ultimaimagemlista=5)
     then bT1.Enabled:=False;
     bt2.Enabled:=True;

end;


procedure TFpesquisaProjNovo.img2MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
      img1.Picture:=img2.Picture;
      bvl2.Visible:=False;
      bvl1.Visible:=True;
      bvl3.Visible:=False;
      bvl4.Visible:=False;
      bvl5.Visible:=False;
      bvl6.Visible:=False;
      ScreenCursorProc(crHandPoint);
end;

procedure TFpesquisaProjNovo.img3MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
      img1.Picture:=img3.Picture;
      bvl1.Visible:=False;
      bvl2.Visible:=True;
      bvl4.Visible:=False;
      bvl3.Visible:=False;
      bvl5.Visible:=False;
      bvl6.Visible:=False;
      ScreenCursorProc(crHandPoint);
end;

procedure TFpesquisaProjNovo.img4MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
      img1.Picture:=img4.Picture;
      bvl2.Visible:=False;
      bvl3.Visible:=True;
      bvl4.Visible:=False;
      bvl1.Visible:=False;
      bvl5.Visible:=False;
      bvl6.Visible:=False;
      ScreenCursorProc(crHandPoint);
end;

procedure TFpesquisaProjNovo.img5MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
     img1.Picture:=img5.Picture;
     bvl2.Visible:=False;
      bvl4.Visible:=True;
      bvl3.Visible:=False;
      bvl1.Visible:=False;
      bvl5.Visible:=False;
      bvl6.Visible:=False;
      ScreenCursorProc(crHandPoint);
end;

procedure TFpesquisaProjNovo.img6MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
      img1.Picture:=img6.Picture;
      bvl2.Visible:=False;
      bvl5.Visible:=True;
      bvl3.Visible:=False;
      bvl1.Visible:=False;
      bvl6.Visible:=False;
      bvl4.Visible:=False;
      ScreenCursorProc(crHandPoint);
end;

procedure TFpesquisaProjNovo.img7MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
      img1.Picture:=img7.Picture;
      bvl2.Visible:=False;
      bvl6.Visible:=True;
      bvl3.Visible:=False;
      bvl1.Visible:=False;
      bvl5.Visible:=False;
      bvl4.Visible:=False;
      ScreenCursorProc(crHandPoint);
end;

procedure TFpesquisaProjNovo.bt3Click(Sender: TObject);
begin
    SELF.Close;
end;

procedure TFpesquisaProjNovo.FormMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ScreenCursorProc(crDefault);
end;

function TFpesquisaProjNovo.ResgataLocalDesenhos: string;
begin
     result:='';
     If (ObjParametroGlobal.ValidaParametro('CAMINHO DOS DESENHOS')=FALSE)
     Then Begin
               Messagedlg('Par�metro n�o encontrado "CAMINHO DOS DESENHOS"!', mterror,[mbok],0);
               exit;
     End;
     result:= VerificaBarraFinalDiretorio( ObjParametroGlobal.Get_Valor );
end;

function TFpesquisaProjNovo.ResgataDesenho(PReferencia : string): string;
Var LocalDesenho :string;
begin
     LocalDesenho:='';
     LocalDesenho:=Self.ResgataLocalDesenhos+PReferencia+'.bmp';
     if (FileExists(LocalDesenho)=true)then
     Result:=LocalDesenho
     else
     Result:='';
end;

end.
