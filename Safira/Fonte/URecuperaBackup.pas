unit URecuperaBackup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, IBCustomDataSet, IBQuery;

type
  TFrecuperaBackup = class(TForm)
    BtGrupo: TBitBtn;
    Btpersiana: TBitBtn;
    BtPersianaGrupo: TBitBtn;
    OpenDialog: TOpenDialog;
    Button1: TButton;
    IBQuery: TIBQuery;
    IBQueryBackup: TIBQuery;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtGrupoClick(Sender: TObject);
    procedure BtpersianaClick(Sender: TObject);
    procedure BtPersianaGrupoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrecuperaBackup: TFrecuperaBackup;

implementation

uses UDataModulo, UessencialGlobal, UMostraBarraProgresso;

{$R *.dfm}

procedure TFrecuperaBackup.Button1Click(Sender: TObject);
begin
     if (OpenDialog.Execute=False)
     Then exit;

     FDataModulo.IBDatabaseBackup.DatabaseName:=OpenDialog.FileName;
     FDataModulo.IBDatabaseBackup.Params.Clear;
     FDataModulo.IBDatabaseBackup.Params.text:=FDataModulo.IBDatabase.Params.text;
     FDataModulo.IBDatabaseBackup.Open;

     Self.IBQuery.Database:=FDataModulo.IBDatabase;
     Self.IBQueryBackup.Database:=FDataModulo.IBDatabaseBackup;

     Btpersiana.Enabled:=True;
     BtGrupo.Enabled:=True;
     BtPersianaGrupo.Enabled:=True;


end;

procedure TFrecuperaBackup.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    FDataModulo.IBDatabaseBackup.close;
    Self.ibquery.close;
    Self.IBQueryBackup.close;
end;

procedure TFrecuperaBackup.BtGrupoClick(Sender: TObject);
begin
     With Self.IBQueryBackup do
     Begin
          close;
          SQL.clear;
          sql.add('Select * from TabGrupoPersiana');
          open;

          While not(eof) do
          Begin
               IBQuery.close;
               IBQuery.sql.clear;
               IBQuery.sql.add('Select * from TabGrupoPersiana Where codigo='+Fieldbyname('codigo').asstring);
               IBQuery.open;
               if (IBQuery.recordcount>0)
               Then begin//edi��o
                        IBQuery.close;
                        IBQuery.SQL.clear;
                        IBQuery.SQL.add('Update TabGrupoPersiana set Codigo=:Codigo,Referencia=:Referencia');
                        IBQuery.SQL.add(',Nome=:Nome,ControlaPorMetroQuadrado=:ControlaPorMetroQuadrado');
                        IBQuery.SQl.Add(',PorcentagemInstalado=:PorcentagemInstalado, PorcentagemFornecido=:PorcentagemFornecido');
                        IBQuery.SQL.add(',PorcentagemRetirado=:PorcentagemRetirado,PlanoDeContas=:PlanoDeContas');
                        IBQuery.SQL.add('where Codigo='+Fieldbyname('codigo').asstring);

               End
               Else Begin//insercao
                        IBQuery.close;
                        IBQuery.SQL.clear;
                        IBQuery.SQL.add('Insert Into TabGrupoPersiana(Codigo,Referencia,Nome');
                        IBQuery.SQL.add(' ,ControlaPorMetroQuadrado,PorcentagemInstalado, PorcentagemFornecido, PorcentagemRetirado, PlanoDeContas)');
                        IBQuery.SQL.add('values (:Codigo,:Referencia,:Nome,:ControlaPorMetroQuadrado,:PorcentagemInstalado, :PorcentagemFornecido, :PorcentagemRetirado,:PlanoDeContas');
                        IBQuery.SQL.add(' )');
               End;

               IBQuery.ParamByName('Codigo').asstring:=Fieldbyname('Codigo').asstring;
               IBQuery.ParamByName('Referencia').asstring:=Fieldbyname('Referencia').asstring;
               IBQuery.ParamByName('Nome').asstring:=Fieldbyname('Nome').asstring;
               IBQuery.ParamByName('ControlaPorMetroQuadrado').asstring:=Fieldbyname('ControlaPorMetroQuadrado').asstring;
               IBQuery.ParamByName('PorcentagemInstalado').AsString:=Fieldbyname('PorcentagemInstalado').asstring;
               IBQuery.ParamByName('PorcentagemFornecido').AsString:=Fieldbyname('PorcentagemFornecido').asstring;
               IBQuery.ParamByName('PorcentagemRetirado').AsString:=Fieldbyname('PorcentagemRetirado').asstring;
               IBQuery.ParamByName('PlanoDeContas').AsString:=Fieldbyname('PlanoDeContas').asstring;
               IBQuery.ExecSQL;

               next;
          End;//while

          FDataModulo.IBTransaction.CommitRetaining;
          Showmessage('Conclu�do com Sucesso');

     End;
end;

procedure TFrecuperaBackup.BtpersianaClick(Sender: TObject);
begin
     With Self.IBQueryBackup do
     Begin
          close;
          SQL.clear;
          sql.add('Select * from TabPersiana');
          open;

          While not(eof) do
          Begin
               IBQuery.close;
               IBQuery.sql.clear;
               IBQuery.sql.add('Select * from TabPersiana Where codigo='+Fieldbyname('codigo').asstring);
               IBQuery.open;
               if (IBQuery.recordcount>0)
               Then begin//edi��o
                        IBQuery.close;
                        IBQuery.SQL.clear;
                        IBQuery.SQL.add('Update TabPersiana set Codigo=:Codigo,Referencia=:Referencia');
                        IBQuery.SQL.add(',Nome=:Nome');
                        IBQuery.SQL.add(',IsentoICMS_Estado=:IsentoICMS_Estado,SubstituicaoICMS_Estado=:SubstituicaoICMS_Estado');
                        IBQuery.SQL.add(',ValorPauta_Sub_Trib_Estado=:ValorPauta_Sub_Trib_Estado');
                        IBQuery.SQL.add(',Aliquota_ICMS_Estado=:Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado=:Reducao_BC_ICMS_Estado');
                        IBQuery.SQL.add(',Aliquota_ICMS_Cupom_Estado=:Aliquota_ICMS_Cupom_Estado');
                        IBQuery.SQL.add(',IsentoICMS_ForaEstado=:IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado=:SubstituicaoICMS_ForaEstado');
                        IBQuery.SQL.add(',ValorPauta_Sub_Trib_ForaEstado=:ValorPauta_Sub_Trib_ForaEstado');
                        IBQuery.SQL.add(',Aliquota_ICMS_ForaEstado=:Aliquota_ICMS_ForaEstado');
                        IBQuery.SQL.add(',Reducao_BC_ICMS_ForaEstado=:Reducao_BC_ICMS_ForaEstado');
                        IBQuery.SQL.add(',Aliquota_ICMS_Cupom_ForaEstado=:Aliquota_ICMS_Cupom_ForaEstado');
                        IBQuery.SQL.add(',SituacaoTributaria_TabelaA=:SituacaoTributaria_TabelaA');
                        IBQuery.SQL.add(',SituacaoTributaria_TabelaB=:SituacaoTributaria_TabelaB');
                        IBQuery.SQL.add(',ClassificacaoFiscal=:ClassificacaoFiscal,Fornecedor=:Fornecedor');
                        IBQuery.SQL.add('where Codigo='+Fieldbyname('codigo').asstring);


               End
               Else Begin//insercao
                        IBQuery.close;
                        IBQuery.SQL.clear;
                        IBQuery.SQL.add('Insert Into TabPersiana(Codigo,Referencia,Nome,IsentoICMS_Estado');
                        IBQuery.SQL.add(' ,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado');
                        IBQuery.SQL.add(' ,Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado');
                        IBQuery.SQL.add(' ,IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado');
                        IBQuery.SQL.add(' ,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
                        IBQuery.SQL.add(' ,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado');
                        IBQuery.SQL.add(' ,SituacaoTributaria_TabelaA,SituacaoTributaria_TabelaB');
                        IBQuery.SQL.add(' ,ClassificacaoFiscal, Fornecedor)');
                        IBQuery.SQL.add('values (:Codigo,:Referencia,:Nome,:IsentoICMS_Estado');
                        IBQuery.SQL.add(' ,:SubstituicaoICMS_Estado,:ValorPauta_Sub_Trib_Estado');
                        IBQuery.SQL.add(' ,:Aliquota_ICMS_Estado,:Reducao_BC_ICMS_Estado,:Aliquota_ICMS_Cupom_Estado');
                        IBQuery.SQL.add(' ,:IsentoICMS_ForaEstado,:SubstituicaoICMS_ForaEstado');
                        IBQuery.SQL.add(' ,:ValorPauta_Sub_Trib_ForaEstado,:Aliquota_ICMS_ForaEstado');
                        IBQuery.SQL.add(' ,:Reducao_BC_ICMS_ForaEstado,:Aliquota_ICMS_Cupom_ForaEstado');
                        IBQuery.SQL.add(' ,:SituacaoTributaria_TabelaA,:SituacaoTributaria_TabelaB');
                        IBQuery.SQL.add(' ,:ClassificacaoFiscal,:Fornecedor)');
               End;

               Ibquery.ParamByName('Codigo').asstring:=fieldbyname('Codigo').asstring;
               Ibquery.ParamByName('Referencia').asstring:=fieldbyname('Referencia').asstring;
               Ibquery.ParamByName('Nome').asstring:=fieldbyname('Nome').asstring;
               Ibquery.ParamByName('IsentoICMS_Estado').asstring:=fieldbyname('IsentoICMS_Estado').asstring;
               Ibquery.ParamByName('SubstituicaoICMS_Estado').asstring:=fieldbyname('SubstituicaoICMS_Estado').asstring;
               Ibquery.ParamByName('ValorPauta_Sub_Trib_Estado').asstring:=virgulaparaponto(fieldbyname('ValorPauta_Sub_Trib_Estado').asstring);
               Ibquery.ParamByName('Aliquota_ICMS_Estado').asstring:=virgulaparaponto(fieldbyname('Aliquota_ICMS_Estado').asstring);
               Ibquery.ParamByName('Reducao_BC_ICMS_Estado').asstring:=virgulaparaponto(fieldbyname('Reducao_BC_ICMS_Estado').asstring);
               Ibquery.ParamByName('Aliquota_ICMS_Cupom_Estado').asstring:=virgulaparaponto(fieldbyname('Aliquota_ICMS_Cupom_Estado').asstring);
               Ibquery.ParamByName('IsentoICMS_ForaEstado').asstring:=fieldbyname('IsentoICMS_ForaEstado').asstring;
               Ibquery.ParamByName('SubstituicaoICMS_ForaEstado').asstring:=fieldbyname('SubstituicaoICMS_ForaEstado').asstring;
               Ibquery.ParamByName('ValorPauta_Sub_Trib_ForaEstado').asstring:=virgulaparaponto(fieldbyname('ValorPauta_Sub_Trib_ForaEstado').asstring);
               Ibquery.ParamByName('Aliquota_ICMS_ForaEstado').asstring:=virgulaparaponto(fieldbyname('Aliquota_ICMS_ForaEstado').asstring);
               Ibquery.ParamByName('Reducao_BC_ICMS_ForaEstado').asstring:=virgulaparaponto(fieldbyname('Reducao_BC_ICMS_ForaEstado').asstring);
               Ibquery.ParamByName('Aliquota_ICMS_Cupom_ForaEstado').asstring:=virgulaparaponto(fieldbyname('Aliquota_ICMS_Cupom_ForaEstado').asstring);
               Ibquery.ParamByName('SituacaoTributaria_TabelaA').asstring:=fieldbyname('SituacaoTributaria_TabelaA').asstring;
               Ibquery.ParamByName('SituacaoTributaria_TabelaB').asstring:=fieldbyname('SituacaoTributaria_TabelaB').asstring;
               Ibquery.ParamByName('ClassificacaoFiscal').asstring:=fieldbyname('ClassificacaoFiscal').asstring;
               Ibquery.ParamByName('Fornecedor').AsString:=fieldbyname('Fornecedor').asstring;

                IBQuery.ExecSQL;
                next;
          End;//while

          FDataModulo.IBTransaction.CommitRetaining;
          Showmessage('Conclu�do com Sucesso');

     End;

end;

procedure TFrecuperaBackup.BtPersianaGrupoClick(Sender: TObject);
begin


     With Self.IBQueryBackup do
     Begin
          close;
          SQL.clear;
          sql.add('Select * from TabPersianaGrupoDiametroCor');
          open;
          last;
          FMostraBarraProgresso.BarradeProgresso.MaxValue:=Recordcount;
          FMostraBarraProgresso.BarradeProgresso.MinValue:=0;
          FMostraBarraProgresso.BarradeProgresso.Progress:=0;
          first;
          FMostraBarraProgresso.show;
          While not(eof) do
          Begin
               FMostraBarraProgresso.BarradeProgresso.Progress:=FMostraBarraProgresso.BarradeProgresso.Progress+1;
               FMostraBarraProgresso.BarradeProgresso.repaint;

               IBQuery.close;
               IBQuery.sql.clear;
               IBQuery.sql.add('Select * from TabPersianaGrupoDiametroCor Where codigo='+Fieldbyname('codigo').asstring);
               IBQuery.open;
               if (IBQuery.recordcount>0)
               Then begin//edi��o
                        IBQuery.close;
                        IBQuery.SQL.clear;
                        IBQuery.SQL.add('Update TABPERSIANAGRUPODIAMETROCOR set Codigo=:Codigo');
                        IBQuery.SQL.add(',Persiana=:Persiana,GrupoPersiana=:GrupoPersiana,Cor=:Cor');
                        IBQuery.SQL.add(',Diametro=:Diametro,PrecoCusto=:PrecoCusto');
                        IBQuery.SQL.add(',PorcentagemInstalado=:PorcentagemInstalado,PorcentagemFornecido=:PorcentagemFornecido');
                        IBQuery.SQL.add(',PorcentagemRetirado=:PorcentagemRetirado');
                        IBQuery.SQL.add('where Codigo='+Fieldbyname('codigo').asstring);
               End
               Else Begin//insercao
                        IBQuery.close;
                        IBQuery.SQL.clear;
                        IBQuery.SQL.add('Insert Into TABPERSIANAGRUPODIAMETROCOR(Codigo,Persiana');
                        IBQuery.SQL.add(' ,GrupoPersiana,Cor,Diametro,Estoque,PrecoCusto,PorcentagemInstalado');
                        IBQuery.SQL.add(' ,PorcentagemFornecido,PorcentagemRetirado)');
                        IBQuery.SQL.add('values (:Codigo,:Persiana,:GrupoPersiana,:Cor,:Diametro');
                        IBQuery.SQL.add(' ,:Estoque,:PrecoCusto,:PorcentagemInstalado,:PorcentagemFornecido');
                        IBQuery.SQL.add(' ,:PorcentagemRetirado)');
                        IBQuery.ParamByName('Estoque').asstring:=virgulaparaponto(Fieldbyname('Estoque').asstring);
               End;

                IBQuery.ParamByName('Codigo').asstring:=Fieldbyname('Codigo').asstring;
                IBQuery.ParamByName('Persiana').asstring:=Fieldbyname('Persiana').asstring;
                IBQuery.ParamByName('GrupoPersiana').asstring:=Fieldbyname('GrupoPersiana').asstring;
                IBQuery.ParamByName('Cor').asstring:=Fieldbyname('Cor').asstring;
                IBQuery.ParamByName('Diametro').asstring:=Fieldbyname('Diametro').asstring;
                IBQuery.ParamByName('PrecoCusto').asstring:=virgulaparaponto(Fieldbyname('PrecoCusto').asstring);
                IBQuery.ParamByName('PorcentagemInstalado').asstring:=virgulaparaponto(Fieldbyname('PorcentagemInstalado').asstring);
                IBQuery.ParamByName('PorcentagemFornecido').asstring:=virgulaparaponto(Fieldbyname('PorcentagemFornecido').asstring);
                IBQuery.ParamByName('PorcentagemRetirado').asstring:=virgulaparaponto(Fieldbyname('PorcentagemRetirado').asstring);
                IBQuery.ExecSQL;
                next;
          End;//while
          FMostraBarraProgresso.close;

          FDataModulo.IBTransaction.CommitRetaining;
          Showmessage('Conclu�do com Sucesso');

     End;
end;

procedure TFrecuperaBackup.FormShow(Sender: TObject);
begin
     Btpersiana.Enabled:=False;
     BtGrupo.Enabled:=False;
     BtPersianaGrupo.Enabled:=False;
end;

end.
