unit uReferenciaNFe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons,uobjgeranfe,UObjEntradaProdutos,
  Mask;

type
  TfReferenciaNFE = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    edtChaveAcesso: TEdit;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    edtEntrada: TEdit;
    Label3: TLabel;
    edtCUF: TEdit;
    Label9: TLabel;
    edtNumeroNF: TEdit;
    Label4: TLabel;
    edtAMM: TMaskEdit;
    Label5: TLabel;
    edtCNPJ: TEdit;
    Label6: TLabel;
    edtModelo: TEdit;
    GroupBox3: TGroupBox;
    memoInformacoesComplementares: TMemo;
    Panel2: TPanel;
    radioTipoNFE: TRadioGroup;
    imgRodape: TImage;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    procedure edtEntradaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn2Click(Sender: TObject);
    procedure edtEntradaExit(Sender: TObject);
    procedure edtEntradaKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtChaveAcessoExit(Sender: TObject);
  private

      function validaDados():Boolean;

  public

    objGeraNFe:Tobjgeranfe;
    objEntrada:TObjEntradaProdutos;
    codigoNfeDigitada:string;

    nfeRef:Boolean;
    dadosAdicionais:string;

    function get_nfeRef:Boolean;
    function get_dadosAdicionais:string;
    procedure zeraCampos();


  end;

var
  fReferenciaNFE: TfReferenciaNFE;


implementation

uses Upesquisa, UEntradaProdutos, UessencialGlobal, UObjFornecedor,
  UescolheImagemBotao, UobjNFE, UDataModulo;

{$R *.dfm}

procedure TfReferenciaNFE.FormShow(Sender: TObject);
begin

  objGeraNFe:=Tobjgeranfe.Create();
  objEntrada:=TObjEntradaProdutos.Create();

  Self.zeraCampos();

  self.edtChaveAcesso.OnKeyDown:=self.ObjgeraNfe.Nfe.extraiChaveKeyDown;
  {self.edtChaveAcesso.OnKeyDown:=Self.objgeranfe.Nfe.edtNfeKeyDownChave;}

  FescolheImagemBotao.PegaFiguraImagem(imgRodape,'RODAPE');

  if codigoNfeDigitada <> '' then
  begin

    edtChaveAcesso.Text := get_campoTabela('chave_devolucao','codigo','tabnfedigitada',codigoNfeDigitada);
    edtChaveAcessoExit(edtChaveAcesso);

    if Trim(edtChaveAcesso.Text) <> '' then
      radioTipoNFE.ItemIndex := 1;

  end;

end;

procedure TfReferenciaNFE.FormClose(Sender: TObject;var Action: TCloseAction);
begin

  self.objGeraNFe.free;
  self.objEntrada.Free;

end;

procedure TfReferenciaNFE.BitBtn1Click(Sender: TObject);
begin

  if (Trim (memoInformacoesComplementares.Text) = '') then
  begin

    MensagemAviso ('Necessario informa��o complementar');
    Exit;

  end;

  nfeRef:=False;

  if not (self.validaDados()) then
    Exit;

  if (edtChaveAcesso.Text <> '') then
  begin

    nfeRef:=True;

    if codigoNfeDigitada <> '' then
    begin

      FDataModulo.unicQuery.SQL.Text := 'update tabnfedigitada set chave_devolucao=:chave where codigo=:codigo';

      FDataModulo.unicQuery.Params[0].AsString := edtChaveAcesso.Text;
      FDataModulo.unicQuery.Params[1].AsInteger := StrToInt(codigoNfeDigitada);

      FDataModulo.unicQuery.ExecSQL;
      FDataModulo.unicQuery.Transaction.Commit;

    end;

  end else if (edtNumeroNF.Text <> '') then
  begin

    nfeRef:=False;

  end;

  self.Tag:=0;
  self.Close;

end;

procedure TfReferenciaNFE.edtEntradaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FentradaProdutosX:TFentradaprodutos;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FentradaProdutosX:=TFentradaprodutos.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(objEntrada.Get_Pesquisa,objEntrada.Get_TituloPesquisa,FentradaProdutosX)=True) Then
            Begin

                Try

                  If (FpesquisaLocal.showmodal=mrok) then
                  Begin
                    TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(objEntrada.RETORNACAMPOCODIGO).asstring;
                  End;

                Finally

                  FpesquisaLocal.QueryPesq.close;

                End;

            End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FentradaProdutosX);
     End;

end;

function TfReferenciaNFE.get_nfeRef: Boolean;
begin

  result:=self.nfeRef;

end;

procedure TfReferenciaNFE.BitBtn2Click(Sender: TObject);
begin

  self.Tag:=-1;
  self.Close;

end;

function TfReferenciaNFE.get_dadosAdicionais: string;
begin

  result:=self.memoInformacoesComplementares.Lines.Text;

end;

procedure TfReferenciaNFE.zeraCampos;
begin

  self.edtChaveAcesso.Text:='';
  self.edtCUF.Text:='';
  self.edtAMM.Text:='';
  self.edtCNPJ.Text:='';
  self.edtModelo.Text:='';
  self.edtNumeroNF.Text:='';
  self.dadosAdicionais:='';
  self.edtEntrada.Text:='';

end;

procedure TfReferenciaNFE.edtEntradaExit(Sender: TObject);
begin

  if (Trim(self.edtEntrada.Text) = '') then
    Exit;

  objEntrada.LocalizaCodigo(self.edtEntrada.Text);
  objEntrada.TabelaparaObjeto;

  if (Trim(objEntrada.Fornecedor.get_codigoCidade()) = '') then
  begin

    MensagemAviso('C�digo da cidade invalido para fornecedor de codigo: '+objEntrada.Fornecedor.Get_CODIGO());
    self.zeraCampos();
    Exit;

  end;


  edtCUF.Text      := (objEntrada.Fornecedor.get_codigoCidade()[1]+objEntrada.Fornecedor.get_codigoCidade()[2]);

  try
    edtAMM.Text      :=  FormatDateTime('yy/mm',StrToDate (objEntrada.Get_EmissaoNF()));
  except
    MensagemAviso('Data de emiss�o da NF invalida para a entrada: '+edtEntrada.Text);
    Exit;
  end;

  edtCNPJ.Text     := RetornaSoNumeros(objEntrada.Fornecedor.Get_CGC());
  edtModelo.Text   := '1';
  edtNumeroNF.Text := objEntrada.Get_NF ();

  if (edtNumeroNF.Text <> '') then
  begin

    if (radioTipoNFE.ItemIndex = 0) then
      memoInformacoesComplementares.Lines.Text:=dadosAdicionais + #13 + 'Nota complementar referente a NF: '+edtNumeroNF.Text
    else
      memoInformacoesComplementares.Lines.Text:=dadosAdicionais + #13 + 'Devolu��o referente a NF: '+edtNumeroNF.Text;

    nfeRef:=False;           

  end;


end;

procedure TfReferenciaNFE.edtEntradaKeyPress(Sender: TObject;
  var Key: Char);
begin

   if not(key in ['0'..'9',#8,#13]) then
    key:=#0;

end;

procedure TfReferenciaNFE.FormKeyPress(Sender: TObject; var Key: Char);
begin

   if (key = #13) then
        Perform(Wm_NextDlgCtl,0,0)
   else
   begin

        if (Key=#27) then
          Self.Close;

   end;

end;

function TfReferenciaNFE.validaDados:Boolean;
var
  msg:string;
begin

  result:=False;
  msg:='';

  if (self.edtChaveAcesso.Text = '') and (self.edtNumeroNF.Text = '') then
    Exit;

  if (self.edtChaveAcesso.Text <> '') and (self.edtNumeroNF.Text <> '') then
  begin

    MensagemAviso('N�o � possivel referenciar NF-e e NF ao mesmo tempo');
    Exit;

  end;

  if (self.edtChaveAcesso.Text = '') then
  begin

    if (self.edtCUF.Text = '') then
      msg:=msg+'C�digo do estado|';

    if (self.edtCNPJ.Text = '') then
      msg:=msg+'CNPJ|';

    if (self.edtModelo.Text = '') then
      msg:=msg+'Modelo NF|';

    if (self.edtNumeroNF.Text = '') then
      msg:=msg+'Numero NF|';

    if (RetornaSoNumeros(self.edtAMM.Text) = '') then
      msg:=msg+'Data emiss�o NF'

  end;

  if (msg <> '') then
  begin
    MensagemAviso('Os seguintes campos cont�m valores invalidos: '+msg);
    Exit;
  end;

  result:=True;

end;

procedure TfReferenciaNFE.edtChaveAcessoExit(Sender: TObject);
begin

  if (edtChaveAcesso.Text <> '') then
  begin

    if (radioTipoNFE.ItemIndex = 0) then
      memoInformacoesComplementares.Lines.Text:=dadosAdicionais + #13 + 'Nota complementar referente a NF-e: '+edtChaveAcesso.Text
    else
      memoInformacoesComplementares.Lines.Text:=dadosAdicionais + #13 + 'Devolu��o referente a NF-e: '+edtChaveAcesso.Text;

  end

end;

end.



