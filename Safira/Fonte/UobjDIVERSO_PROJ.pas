unit UobjDIVERSO_PROJ;
Interface
Uses forms,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,
     UOBJDIVERSO,UOBJPROJETO,UDIVERSO;

Type
   TObjDIVERSO_PROJ=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Diverso:TOBJDIVERSO;
                Projeto:TOBJPROJETO;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Quantidade(parametro: string);
                Function Get_Quantidade: string;
                Procedure Submit_Altura(parametro: string);
                Function Get_Altura: string;
                Procedure Submit_Largura(parametro: string);
                Function Get_Largura: string;

                procedure EdtDiversoExit(Sender: TObject; var PEdtCodigo: TEdit; LABELNOME: TLABEL);
                procedure EdtDiversoKeyDown(Sender: TObject; Var PEdtCodigo :TEdit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtDiversoKeyDown2(Sender: TObject;var Key: Word; Shift: TShiftState;var LABELNOME: string;var TEdit:string;codigoprojeto:string);

                procedure EdtProjetoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                Procedure Resgata_Diverso_Proj(PProjeto : string);
                Function VerificaSeJaExisteEstaDiversoNesteProjeto(PProjeto, PDiverso: string): Boolean;
                function Replica(PProjeto, PnovoProjeto: string): boolean;

                procedure Submit_AlturaCorte(parametro:string);
                procedure Submit_PosicaoCorte(parametro:string);
                procedure Submit_TamanhoCorte(parametro:string);

                function Get_AlturaCorte:string;
                function Get_PosicaoCorte:string;
                function Get_TamanhoCorte:string;

         Private
                Objquery:Tibquery;
                ObjQueryPesquisa:TIBQuery;
                InsertSql,DeleteSql,ModifySQl:TStringList;

                Codigo:string;
                Quantidade:string;
                Altura:string;
                Largura:string;
                AlturaCorte:string;
                PosicaoCorte:string;
                TamanhoCorte:string;

                ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

      End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UMostraBarraProgresso;

Function  TObjDIVERSO_PROJ.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If(FieldByName('Diverso').asstring<>'')
        Then Begin
                 If (Self.Diverso.LocalizaCodigo(FieldByName('Diverso').asstring)=False)
                 Then Begin
                          Messagedlg('Diverso N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Diverso.TabelaparaObjeto;
        End;
        If(FieldByName('Projeto').asstring<>'')
        Then Begin
                 If (Self.Projeto.LocalizaCodigo(FieldByName('Projeto').asstring)=False)
                 Then Begin
                          Messagedlg('Projeto N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Projeto.TabelaparaObjeto;
        End;
        Self.Quantidade:=fieldbyname('Quantidade').asstring;
        Self.Altura:=fieldbyname('Altura').asstring;
        Self.Largura:=fieldbyname('Largura').asstring;

        AlturaCorte:=fieldbyname('alturacorte').AsString;
        PosicaoCorte:=fieldbyname('posicaocorte').asstring;
        TamanhoCorte:=fieldbyname('tamanhocorte').AsString;
//CODIFICA TABELAPARAOBJETO






        result:=True;
     End;
end;


Procedure TObjDIVERSO_PROJ.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Diverso').asstring:=Self.Diverso.GET_CODIGO;
        ParamByName('Projeto').asstring:=Self.Projeto.GET_CODIGO;
        ParamByName('Quantidade').asstring:=virgulaparaponto(Self.Quantidade);
        ParamByName('Altura').asstring:=Self.Altura;
        ParamByName('Largura').asstring:=Self.Largura;
        ParamByName('alturaCorte').AsString:=Self.AlturaCorte;
        ParamByName('tamanhocorte').asstring:=Self.TamanhoCorte;
        ParamByName('posicaocorte').AsString:=Self.PosicaoCorte;
//CODIFICA OBJETOPARATABELA






  End;
End;

//***********************************************************************

function TObjDIVERSO_PROJ.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjDIVERSO_PROJ.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Diverso.ZerarTabela;
        Projeto.ZerarTabela;
        Quantidade:='';
        Altura:='';
        Largura:='';
        alturacorte:='';
        tamanhocorte:='';
        posicaocorte:='';
//CODIFICA ZERARTABELA






     End;
end;

Function TObjDIVERSO_PROJ.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjDIVERSO_PROJ.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Diverso.LocalizaCodigo(Self.Diverso.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Diverso n�o Encontrado!';
      If (Self.Projeto.LocalizaCodigo(Self.Projeto.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Projeto n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjDIVERSO_PROJ.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.Diverso.Get_Codigo<>'')
        Then Strtoint(Self.Diverso.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Diverso';
     End;
     try
        If (Self.Projeto.Get_Codigo<>'')
        Then Strtoint(Self.Projeto.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Projeto';
     End;
     try
        Strtofloat(Self.Quantidade);
     Except
           Mensagem:=mensagem+'/Quantidade';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjDIVERSO_PROJ.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjDIVERSO_PROJ.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjDIVERSO_PROJ.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=False;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro DIVERSO_PROJ vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Diverso,Projeto,Quantidade,Altura,Largura,alturacorte,tamanhocorte,posicaocorte');
           SQL.ADD(' ');
           SQL.ADD(' from  TabDiverso_Proj');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjDIVERSO_PROJ.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjDIVERSO_PROJ.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjDIVERSO_PROJ.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjQueryPesquisa;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Diverso:=TOBJDIVERSO.create;
        Self.Projeto:=TOBJPROJETO.create;

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabDiverso_Proj(Codigo,Diverso,Projeto,Quantidade');
                InsertSQL.add(' ,Altura,Largura,alturacorte,posicaocorte,tamanhocorte)');
                InsertSQL.add('values (:Codigo,:Diverso,:Projeto,:Quantidade,:Altura');
                InsertSQL.add(' ,:Largura,:alturacorte,:posicaocorte,:tamanhocorte)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabDiverso_Proj set Codigo=:Codigo,Diverso=:Diverso');
                ModifySQL.add(',Projeto=:Projeto,Quantidade=:Quantidade,Altura=:Altura');
                ModifySQL.add(',Largura=:Largura,alturacorte=:alturacorte,posicaocorte=:posicaocorte,tamanhocorte=:tamanhocorte');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabDiverso_Proj where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjDIVERSO_PROJ.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjDIVERSO_PROJ.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabDIVERSO_PROJ');
     Result:=Self.ParametroPesquisa;
end;

function TObjDIVERSO_PROJ.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de DIVERSO_PROJ ';
end;


function TObjDIVERSO_PROJ.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENDIVERSO_PROJ,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENDIVERSO_PROJ,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjDIVERSO_PROJ.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ObjQueryPesquisa);
    Freeandnil(Self.ObjDataSource);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Diverso.FREE;
Self.Projeto.FREE;
//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjDIVERSO_PROJ.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjDIVERSO_PROJ.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjDiverso_Proj.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjDiverso_Proj.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjDiverso_Proj.Submit_Quantidade(parametro: string);
begin
        Self.Quantidade:=Parametro;
end;
function TObjDiverso_Proj.Get_Quantidade: string;
begin
        Result:=Self.Quantidade;
end;
procedure TObjDiverso_Proj.Submit_Altura(parametro: string);
begin
        Self.Altura:=Parametro;
end;
function TObjDiverso_Proj.Get_Altura: string;
begin
        Result:=Self.Altura;
end;
procedure TObjDiverso_Proj.Submit_Largura(parametro: string);
begin
        Self.Largura:=Parametro;
end;
function TObjDiverso_Proj.Get_Largura: string;
begin
        Result:=Self.Largura;
end;


procedure TObjDIVERSO_PROJ.EdtProjetoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Projeto.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Projeto.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Projeto.Get_Descricao;
End;
procedure TObjDIVERSO_PROJ.EdtProjetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Projeto.Get_Pesquisa,Self.Projeto.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Projeto.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Projeto.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Projeto.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjDIVERSO_PROJ.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJDIVERSO_PROJ';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjDIVERSO_PROJ.Resgata_Diverso_Proj(PProjeto: string);
begin
     With Self.ObjQueryPesquisa do
     Begin
         close;
         SQL.Clear;
         Sql.Add('Select  TabDiverso.Referencia, TabDiverso.Descricao,');
         Sql.Add('TabDiverso_Proj.Quantidade, TabDiverso_Proj.Codigo');
         Sql.Add('from TabDiverso_Proj');
         Sql.Add('Join  TabDiverso on TabDiverso.Codigo = TabDiverso_Proj.Diverso');
         Sql.Add('Where Projeto = '+PProjeto);
         if (PProjeto = '')
         then exit;
         Open;
     end;

end;

procedure TObjDIVERSO_PROJ.EdtDiversoKeyDown(Sender: TObject;
  var PEdtCodigo: TEdit; var Key: Word; Shift: TShiftState;
  LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Fdiverso:TFDIVERSO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fdiverso:=TFDIVERSO.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('select * from tabdiverso where ativo=''S'' ',Self.Diverso.Get_TituloPesquisa,Fdiverso)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Diverso.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Diverso.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fdiverso);
     End;
end;

procedure TObjDIVERSO_PROJ.EdtDiversoKeyDown2(Sender: TObject;
  var Key: Word; Shift: TShiftState;var LABELNOME:string;var TEdit:string;codigoprojeto:string);
var
   FpesquisaLocal:Tfpesquisa;
   Fdiverso:TFDIVERSO;
   sql:string;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fdiverso:=TFDIVERSO.Create(nil);
            sql:='select * from tabdiverso join tabdiverso_proj on tabdiverso_proj.diverso=tabdiverso.codigo where tabdiverso_proj.projeto='+codigoprojeto;
            If (FpesquisaLocal.PreparaPesquisa(sql,'',Fdiverso)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 If TEdit<>''
                                 Then Begin
                                        If FpesquisaLocal.QueryPesq.fieldbyname('DESCRICAO').asstring<>''
                                        Then LABELNOME:=FpesquisaLocal.QueryPesq.fieldbyname('DESCRICAO').asstring
                                        Else LABELNOME:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fdiverso);
     End;
end;

procedure TObjDIVERSO_PROJ.EdtDiversoExit(Sender: TObject; var PEdtCodigo: TEdit; LABELNOME: TLABEL);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Diverso.LocalizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     Self.Diverso.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Diverso.Get_Descricao;
     PEdtCodigo.Text:=Self.Diverso.Get_Codigo;

end;
function TObjDiverso_PROJ.VerificaSeJaExisteEstaDiversoNesteProjeto(PProjeto, PDiverso: string): Boolean;
Var    QueryLOcal:TIBquery;
begin
try
        Result:=false;
        try
             QueryLOcal:=TIBQuery.Create(nil);
             QueryLOcal.Database:=FDataModulo.IBDatabase;
        except
             MensagemErro('Erro ao tentar criar a QueryLocal');
             exit;
        end;

        With QueryLocal do
        Begin
             Close;
             Sql.Clear;
             Sql.Add('Select Codigo from TabDiverso_Proj');
             Sql.Add('Where Diverso = '+PDiverso);
             Sql.Add('and Projeto = '+PProjeto);
             Open;

             if (RecordCount>0)then
             Result:=true;

        end;

Finally
       FreeAndNil(QueryLOcal);
end;

end;




Function Tobjdiverso_proj.Replica(PProjeto, PnovoProjeto: string):boolean;
var
pcodigoantigo:string;
begin
     result:=false;

     With Self.ObjQueryPesquisa do
     Begin
         close;
         SQL.Clear;
         Sql.Add('Select  Tabdiverso_Proj.Codigo');
         Sql.Add('from    Tabdiverso_Proj');
         Sql.Add('Where   Projeto = '+PProjeto);

         if (PProjeto = '')
         then exit;

         Open;
         last;
         if (recordcount=0)
         Then Begin
                   result:=true;
                   exit;
         End;
         FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
         FMostraBarraProgresso.Lbmensagem.caption:='Replicando Diversos';
         first;
         Try

           While not(eof) do
           begin
                FMostraBarraProgresso.IncrementaBarra1(1);
                FMostraBarraProgresso.Show;
                Application.processmessages;

                Self.ZerarTabela;
                if (Self.LocalizaCodigo(fieldbyname('codigo').asstring)=False)
                then Begin
                          mensagemErro('Diverso_Proj C�digo '+fieldbyname('codigo').asstring+' n�o localizado');
                          exit;
                End;
                Self.TabelaparaObjeto;

                Self.Status:= dsinsert;
                PcodigoAntigo:=Self.codigo;
                Self.Submit_Codigo('0');
                Self.Projeto.Submit_Codigo(PnovoProjeto);
                if (self.Salvar(False)=False)
                Then begin
                          mensagemErro('Erro na tentativa de Replicar o diverso_proj C�digo='+PcodigoAntigo);
                          exit;
                End;

                next;
           End;
           result:=True;
           
         Finally
                FMostraBarraProgresso.Close;
         End;
     end;

end;


procedure TObjDIVERSO_PROJ.Submit_AlturaCorte(parametro:string);
begin
    Self.AlturaCorte:=parametro;
end;

procedure TObjDIVERSO_PROJ.Submit_PosicaoCorte(parametro:string);
begin
    Self.PosicaoCorte:=parametro;
end;

procedure TObjDIVERSO_PROJ.Submit_TamanhoCorte(parametro:string);
begin
    Self.TamanhoCorte:=parametro;
end;

function TObjDIVERSO_PROJ.Get_AlturaCorte:string;
begin
    Result:=AlturaCorte;
end;

function TObjDIVERSO_PROJ.Get_PosicaoCorte:string;
begin
    Result:=PosicaoCorte;
end;

function TObjDIVERSO_PROJ.Get_TamanhoCorte:string;
begin
    Result:=TamanhoCorte;
end;




end.



