unit UPERFILADO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjPERFILADOCOR,
  UessencialGlobal, Tabs, Grids, DBGrids, UFRImposto_ICMS, ComCtrls,uobjperfilado_icms,UGRUPOPERFILADO,UFORNECEDOR
  ,UCOR,UpesquisaMenu, Menus,UFiltraImp,UErrosDesativarMateriais,IBQuery,
  ImgList,ClipBrd;

type
  TFPERFILADO = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb4: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    btAjuda: TSpeedButton;
    Guia: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    ts3: TTabSheet;
    pnl1: TPanel;
    btUltimo: TSpeedButton;
    btProximo: TSpeedButton;
    btAnterior: TSpeedButton;
    btPrimeiro: TSpeedButton;
    grprgmargem: TGroupBox;
    lbLbPorcentagemFornecido: TLabel;
    lbLbPorcentagemRetirado: TLabel;
    lbLbPorcentagemInstalado: TLabel;
    lb1: TLabel;
    lb3: TLabel;
    lb5: TLabel;
    edtPorcentagemInstalado: TEdit;
    edtPorcentagemFornecido: TEdit;
    edtPorcentagemRetirado: TEdit;
    grprgpreco: TGroupBox;
    lbLbPrecoVendaInstalado: TLabel;
    lbLbPrecoVendaFornecido: TLabel;
    lbLbPrecoVendaRetirado: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    lb8: TLabel;
    edtPrecoVendaInstalado: TEdit;
    edtPrecoVendaFornecido: TEdit;
    edtPrecoVendaRetirado: TEdit;
    lb2: TLabel;
    lbLbAreaMinima: TLabel;
    lbLbPrecoCusto: TLabel;
    lbLbPeso: TLabel;
    lbLbFornecedor: TLabel;
    lbLbUnidade: TLabel;
    lbLbDescricao: TLabel;
    lbLbGrupoPerfilado: TLabel;
    lbLbReferencia: TLabel;
    edtReferencia: TEdit;
    edtGrupoPerfilado: TEdit;
    edtDescricao: TEdit;
    edtUnidade: TEdit;
    edtFornecedor: TEdit;
    edtPeso: TEdit;
    edtPrecoCusto: TEdit;
    edtAreaMinima: TEdit;
    chkAtivo: TCheckBox;
    lbAtivoInativo: TLabel;
    pnl3: TPanel;
    lbLbClassificaoFiscal1: TLabel;
    edtClassificacaoFiscal: TEdit;
    panelCor: TPanel;
    lbLbCor: TLabel;
    lbnomecor: TLabel;
    lbEstoque2: TLabel;
    lb9: TLabel;
    lb10: TLabel;
    lb11: TLabel;
    lbCusto: TLabel;
    lb12: TLabel;
    lbestoque: TLabel;
    edtCorReferencia: TEdit;
    edtcodigoCOR: TEdit;
    edtCor: TEdit;
    grpVidros: TGroupBox;
    lbLbPorcentagemAcrescimo: TLabel;
    lbLbAcrescimoExtra: TLabel;
    lbLbPorcentagemAcrescimoFinal: TLabel;
    lb13: TLabel;
    lb14: TLabel;
    edtPorcentagemAcrescimo: TEdit;
    edtAcrescimoExtra: TEdit;
    edtPorcentagemAcrescimoFinal: TEdit;
    edtValorExtra: TEdit;
    edtValorCusto: TEdit;
    edtValorFinal: TEdit;
    edtinstaladofinal: TEdit;
    edtfornecidofinal: TEdit;
    edtretiradofinal: TEdit;
    rg_forma_de_calculo_percentual: TRadioGroup;
    edtclassificacaofiscal_cor: TEdit;
    pnl2: TPanel;
    btgravarcor: TBitBtn;
    btexcluircor: TBitBtn;
    btcancelacor: TBitBtn;
    dbgridCOR: TDBGrid;
    lbNomeGrupoPerfilado: TLabel;
    lbNomeFornecedor: TLabel;
    panelICMSForaEstado1: TPanel;
    lb15: TLabel;
    lb16: TLabel;
    lb17: TLabel;
    lb18: TLabel;
    lb19: TLabel;
    lb20: TLabel;
    lb21: TLabel;
    edtAliquota_ICMS_ForaEstado: TEdit;
    edtReducao_BC_ICMS_ForaEstado: TEdit;
    edtAliquota_ICMS_Cupom_ForaEstado: TEdit;
    COMBOisentoicms_foraestado: TComboBox;
    COMBOSubstituicaoicms_foraestado: TComboBox;
    edtVALORPAUTA_SUB_TRIB_FORAESTADO: TEdit;
    panelICMSEstado1: TPanel;
    lbLbAliquota_ICMS_Estado: TLabel;
    lbLbReducao_BC_ICMS_Estado: TLabel;
    lbLbAliquota_ICMS_Cupom_Estado: TLabel;
    lb22: TLabel;
    lb23: TLabel;
    lb24: TLabel;
    lb25: TLabel;
    edtAliquota_ICMS_Estado: TEdit;
    edtReducao_BC_ICMS_Estado: TEdit;
    edtAliquota_ICMS_Cupom_Estado: TEdit;
    COMBOisentoicms_estado: TComboBox;
    COMBOSUBSTITUICAOICMS_ESTADO: TComboBox;
    edtVALORPAUTA_SUB_TRIB_ESTADO: TEdit;
    lbLbSituacaoTributaria_TabelaA: TLabel;
    edtSituacaoTributaria_TabelaB: TEdit;
    edtSituacaoTributaria_TabelaA: TEdit;
    FRImposto_ICMS1: TFRImposto_ICMS;
    edtpercentualagregado: TEdit;
    lb26: TLabel;
    lb27: TLabel;
    COMBO_KitPerfilado: TComboBox;
    il1: TImageList;
    bvl1: TBevel;
    grp1: TGroupBox;
    lb28: TLabel;
    lb29: TLabel;
    lb30: TLabel;
    edtEspessuraVidro: TEdit;
    edtAltura: TEdit;
    edtLargura: TEdit;
    edtNCM: TEdit;
    Label1: TLabel;
    edtCest: TEdit;
    Label2: TLabel;
    edtEstoqueMinimo: TEdit;
    procedure edtFornecedorExit(Sender: TObject);
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtFornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoPerfiladoExit(Sender: TObject);
    procedure edtGrupoPerfiladoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSituacaoTributaria_TabelaAExit(Sender: TObject);
    procedure edtSituacaoTributaria_TabelaAKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edtSituacaoTributaria_TabelaBExit(Sender: TObject);
    procedure edtSituacaoTributaria_TabelaBKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edtPorcentagemInstaladoExit(Sender: TObject);
    procedure edtPorcentagemFornecidoExit(Sender: TObject);
    procedure edtPorcentagemRetiradoExit(Sender: TObject);
    procedure btGravarCorClick(Sender: TObject);
    procedure BtExcluirCorClick(Sender: TObject);
    procedure BtCancelarCorClick(Sender: TObject);
    procedure edtCorReferenciaExit(Sender: TObject);
    procedure edtCorReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgridCORDblClick(Sender: TObject);
    procedure DBGRIDCORKeyPress(Sender: TObject; var Key: Char);
    procedure edtAcrescimoExtraExit(Sender: TObject);
    procedure edtValorExtraExit(Sender: TObject);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure edtPrecoCustoExit(Sender: TObject);
    procedure edtAreaMinimaExit(Sender: TObject);
    procedure edtPrecoVendaInstaladoExit(Sender: TObject);
    procedure edtPrecoVendaFornecidoExit(Sender: TObject);
    procedure edtPrecoVendaRetiradoExit(Sender: TObject);
    procedure dbgridCORDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure edtGrupoPerfiladoDblClick(Sender: TObject);
    procedure edtFornecedorDblClick(Sender: TObject);
    procedure edtCorReferenciaDblClick(Sender: TObject);
    procedure edtNCMKeyPress(Sender: TObject; var Key: Char);
    procedure lbestoqueMouseLeave(Sender: TObject);
    procedure lbestoqueMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbestoqueClick(Sender: TObject);
    procedure edtquant(Sender: TObject;
  var Key: Char);
    procedure chkAtivoClick(Sender: TObject);
    procedure btAjudaClick(Sender: TObject);
    procedure FRImposto_ICMS1btReplicarPedidoClick(Sender: TObject);
    procedure GuiaChange(Sender: TObject);
    procedure COMBO_KitPerfiladoExit(Sender: TObject);
    procedure COMBO_KitPerfiladoKeyPress(Sender: TObject; var Key: Char);
    procedure edtGrupoPerfiladoKeyPress(Sender: TObject; var Key: Char);
    procedure edtFornecedorKeyPress(Sender: TObject; var Key: Char);
    procedure edtPorcentagemInstaladoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemFornecidoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemRetiradoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPrecoCustoKeyPress(Sender: TObject; var Key: Char);
    procedure lbNomeGrupoPerfiladoClick(Sender: TObject);
    procedure lbNomeGrupoPerfiladoMouseLeave(Sender: TObject);
    procedure lbNomeGrupoPerfiladoMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure lbNomeFornecedorClick(Sender: TObject);
    procedure edtEspessuraVidroKeyPress(Sender: TObject; var Key: Char);
    procedure edtAlturaKeyPress(Sender: TObject; var Key: Char);
    procedure edtLarguraKeyPress(Sender: TObject; var Key: Char);
    procedure edtAreaMinimaKeyPress(Sender: TObject; var Key: Char);
    procedure lbnomecorClick(Sender: TObject);
    procedure edtValorCustoKeyPress(Sender: TObject; var Key: Char);
    procedure edtNCMKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCestKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    ObjPerfiladoCor:TObjPerfiladoCOR;
    AlteraPrecoPeloCusto:Boolean;
    PAlteraCampoEstoque:boolean;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    Procedure ResgataCores;
    Procedure PreparaCor;
  private
    EstadoObjeto : string;
    { Private declarations }
  public
    procedure AbreComCodigo(parametro:string);
  end;

var
  FPERFILADO: TFPERFILADO;


implementation

uses Upesquisa, UessencialLocal, UDataModulo, UobjPERFILADO,
  UobjGRUPOPERFILADO, UescolheImagemBotao, Uprincipal, UAjuda, UobjCEST;

{$R *.dfm}


procedure TFPERFILADO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjPerfiladoCor.Perfilado=Nil)
     Then exit;

     If (Self.ObjPerfiladoCor.Perfilado.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Try
      Self.ObjPerfiladoCor.free;
    Except
          on e:exception do
          Begin
               mensagemerro(e.message);
          End;
    End;

    try
        FRImposto_ICMS1.ObjMaterial_ICMS.free;
    Except
          on e:exception do
          Begin
               mensagemerro(e.message);
          End;
    End;

    self.Tag:=0;

end;
procedure TFPERFILADO.btNovoClick(Sender: TObject);
begin
     Self.limpaLabels;
     limpaedit(Self);

     habilita_campos(Self);
     desab_botoes(Self);
     chkAtivo.Checked:=True;
     lbAtivoInativo.Caption:='Ativo';

     lbCodigo.Caption:='0';
     EDTAREAMINIMA.TEXT:='0';


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjPerfiladoCor.Perfilado.status:=dsInsert;
     Guia.TabIndex:=0;
     EdtReferencia.setfocus;

      if (AlteraPrecoPeloCusto=True)
     Then Begin
              EdtPrecoVendaInstalado.Enabled:=false;
              EdtPrecoVendaRetirado.Enabled:=false;
              EdtPrecoVendaFornecido.Enabled:=false;
     End;



     Self.EstadoObjeto:='INSER��O';

     EdtSituacaoTributaria_TabelaA.Text:='1';
     EdtSituacaoTributaria_TabelaB.Text:='1';
     EdtClassificacaoFiscal.Text:='1';

     comboisentoicms_estado.Text:='N�O';
     comboSUBSTITUICAOICMS_ESTADO.Text:='SIM';
     edtVALORPAUTA_SUB_TRIB_ESTADO.Text:='0';
     EdtAliquota_ICMS_Estado.Text:='0';
     EdtReducao_BC_ICMS_Estado.Text:='0';
     EdtAliquota_ICMS_Cupom_Estado.Text:='0';

     comboisentoicms_foraestado.Text:='N�O';
     comboSUBSTITUICAOICMS_foraESTADO.Text:='SIM';
     edtVALORPAUTA_SUB_TRIB_foraESTADO.Text:='0';
     EdtAliquota_ICMS_foraEstado.Text:='0';
     EdtReducao_BC_ICMS_foraEstado.Text:='0';
     EdtAliquota_ICMS_Cupom_foraEstado.Text:='0';

          btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

     edtPrecoCusto.Text:='0';
     edtPorcentagemInstalado.Text:='0';
     edtPorcentagemFornecido.text:='0';
     edtPorcentagemRetirado.Text:='0';



end;

procedure TFPERFILADO.btSalvarClick(Sender: TObject);
begin

     If ObjPerfiladoCor.Perfilado.Status=dsInactive
     Then exit;

     If (EdtReferencia.Text<>'')then begin
        If (ObjPerfiladoCor.Perfilado.VerificaReferencia(ObjPerfiladoCor.Perfilado.Status, lbCodigo.Caption, EdtReferencia.Text)=true) then    {rodolfo}
        Begin
         MensagemErro('Refer�ncia j� existe.');
         exit;
        end;
     end;


     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjPerfiladoCor.Perfilado.salvar(true)=False)
     Then exit;

     lbCodigo.Caption:=ObjPerfiladoCor.Perfilado.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     EdtPrecoVendaInstalado.Enabled:=false;
     EdtPrecoVendaRetirado.Enabled:=false;
     EdtPrecoVendaFornecido.Enabled:=false;

     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

     if (Self.EstadoObjeto='INSER��O')then
     if (ObjPerfiladoCor.CadastraPerfiladoEmTodasAsCores(lbCodigo.Caption)=false)then
     Begin
         MensagemErro('Erro ao tentar cadastrar essa Perfilado nas cores escolhidas.');
         FDataModulo.IBTransaction.RollbackRetaining;
         Self.EstadoObjeto:='';
         exit;
     end
     Else MensagemAviso('Cores Cadastradas com Sucesso !');

     FDataModulo.IBTransaction.CommitRetaining;

     Self.EstadoObjeto:='';

     ObjperfiladoCor.perfilado.LocalizaCodigo(lbCodigo.Caption);
     ObjperfiladoCor.perfilado.TabelaparaObjeto;
     self.ObjetoParaControles;
        btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
      btopcoes.Visible :=true;


end;

procedure TFPERFILADO.btAlterarClick(Sender: TObject);
begin
    if(lbCodigo.Caption='')
    then exit;


    If (ObjPerfiladoCor.Perfilado.Status=dsinactive) and (lbCodigo.Caption<>'') and (guia.TabIndex=0)
    Then
    Begin
                habilita_campos(Self);
                ObjPerfiladoCor.Perfilado.Status:=dsEdit;
                guia.TabIndex:=0;
                EdtReferencia.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;

                 if (AlteraPrecoPeloCusto=True)
                 Then Begin
                          EdtPrecoVendaInstalado.Enabled:=false;
                          EdtPrecoVendaRetirado.Enabled:=false;
                          EdtPrecoVendaFornecido.Enabled:=false;
                 End;

                ObjPerfiladoCor.Perfilado.ReferenciaAnterior:=EdtReferencia.Text;
                Self.EstadoObjeto:='EDI��O';
                     btnovo.Visible :=false;
                 btalterar.Visible:=false;
                 btpesquisar.Visible:=false;
                 btrelatorio.Visible:=false;
                 btexcluir.Visible:=false;
                 btsair.Visible:=false;
                 btopcoes.Visible :=false;

    End;

end;

procedure TFPERFILADO.btCancelarClick(Sender: TObject);
begin
     ObjPerfiladoCor.Perfilado.cancelar;
     EdtPrecoVendaInstalado.Enabled:=false;
     EdtPrecoVendaRetirado.Enabled:=false;
     EdtPrecoVendaFornecido.Enabled:=false;
     lbCodigo.Caption:='';
     Self.limpaLabels;
     limpaedit(Self);

     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     grp1.Visible:=False;


end;

procedure TFPERFILADO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjPerfiladoCor.Perfilado.Get_pesquisa,ObjPerfiladoCor.Perfilado.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjPerfiladoCor.Perfilado.status<>dsinactive
                                  then exit;

                                  If (ObjPerfiladoCor.Perfilado.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjPerfiladoCor.Perfilado.ZERARTABELA;
                                  Guia.TabIndex:=0;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFPERFILADO.btExcluirClick(Sender: TObject);
begin
     If (ObjPerfiladoCor.Perfilado.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (ObjPerfiladoCor.Perfilado.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjPerfiladoCor.Perfilado.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFPERFILADO.btRelatorioClick(Sender: TObject);
begin
//    ObjPerfiladoCor.Perfilado.Imprime(lbCodigo.Caption);
end;

procedure TFPERFILADO.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFPERFILADO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFPERFILADO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFPERFILADO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    If Key=VK_Escape
    Then Self.Close;

      if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('CADASTRO DE PERFILADOS');
         FAjuda.ShowModal;
    end;


end;

procedure TFPERFILADO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFPERFILADO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjPerfiladoCor.Perfilado do
    Begin
        Submit_Codigo(lbCodigo.Caption);
        Submit_Referencia(edtReferencia.text);
        GrupoPerfilado.Submit_codigo(edtGrupoPerfilado.text);
        Submit_Descricao(edtDescricao.text);
        Submit_Unidade(edtUnidade.text);
        Fornecedor.Submit_codigo(edtFornecedor.text);
        Submit_Peso(edtPeso.text);
        Submit_PrecoCusto(tira_ponto(edtPrecoCusto.text));

        Submit_PorcentagemInstalado(tira_ponto(edtPorcentagemInstalado.text));
        Submit_PorcentagemFornecido(tira_ponto(edtPorcentagemFornecido.text));
        Submit_PorcentagemRetirado(tira_ponto(edtPorcentagemRetirado.text));

        Submit_precovendaInstalado(tira_ponto(edtprecovendaInstalado.text));
        Submit_precovendaFornecido(tira_ponto(edtprecovendaFornecido.text));
        Submit_precovendaRetirado(tira_ponto(edtprecovendaRetirado.text));
        Submit_AreaMinima(edtAreaMinima.text);
        Submit_ClassificacaoFiscal(edtClassificacaoFiscal.text);

//Campos que nao sao mais usados
        Submit_IsentoICMS_Estado('N');
        Submit_SubstituicaoICMS_Estado('N');
        Submit_ValorPauta_Sub_Trib_Estado('0');
        Submit_Aliquota_ICMS_Estado('0');
        Submit_Reducao_BC_ICMS_Estado('0');
        Submit_Aliquota_ICMS_Cupom_Estado('0');
        Submit_IsentoICMS_ForaEstado('N');
        Submit_SubstituicaoICMS_ForaEstado('N');
        Submit_ValorPauta_Sub_Trib_ForaEstado('0');
        Submit_Aliquota_ICMS_ForaEstado('0');
        Submit_Reducao_BC_ICMS_ForaEstado('0');
        Submit_Aliquota_ICMS_Cupom_ForaEstado('0');
        SituacaoTributaria_TabelaA.Submit_codigo('0');
        SituacaoTributaria_TabelaB.Submit_codigo('0');
        Submit_percentualagregado('0');
        Submit_ipi('0');
        Submit_NCM(edtNCM.Text);
        Submit_cest(edtCest.Text);

        Submit_EspessuraVidro(edtEspessuraVidro.Text);
        Submit_Largura(edtLargura.Text);
        Submit_Altura(edtAltura.Text);
        if(COMBO_KitPerfilado.Text='SIM')
        then Submit_KitPerfilado('S')
        else Submit_KitPerfilado('N');

        if(chkAtivo.Checked=True)
        then Submi_Ativo('S')
        else Submi_Ativo('N');

         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFPERFILADO.LimpaLabels;
begin
      lbestoque.Caption:='';
      lbCodigo.Caption:='';
      lbNomeFornecedor.Caption:='';
end;

function TFPERFILADO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjPerfiladoCor.Perfilado do
     Begin
        lbCodigo.Caption:=Get_Codigo;
        EdtReferencia.text:=Get_Referencia;
        EdtGrupoPerfilado.text:=GrupoPerfilado.Get_codigo;
        LbNomeGrupoPerfilado.Caption:=GrupoPerfilado.Get_Nome;
        EdtDescricao.text:=Get_Descricao;
        EdtUnidade.text:=Get_Unidade;
        EdtFornecedor.text:=Fornecedor.Get_codigo;
        LbNomeFornecedor.Caption:=Fornecedor.Get_RazaoSocial;
        EdtPeso.text:=Get_Peso;
        EdtPrecoCusto.text:=formata_valor(Get_PrecoCusto);
        EdtPorcentagemInstalado.text:=Get_PorcentagemInstalado;
        EdtPorcentagemFornecido.text:=Get_PorcentagemFornecido;
        EdtPorcentagemRetirado.text:=Get_PorcentagemRetirado;
        EdtAreaMinima.text:=Get_AreaMinima;
        ComboIsentoICMS_Estado.text:=Get_IsentoICMS_Estado;
        ComboSubstituicaoICMS_Estado.text:=Get_SubstituicaoICMS_Estado;
        EdtValorPauta_Sub_Trib_Estado.text:=Get_ValorPauta_Sub_Trib_Estado;
        EdtAliquota_ICMS_Estado.text:=Get_Aliquota_ICMS_Estado;
        EdtReducao_BC_ICMS_Estado.text:=Get_Reducao_BC_ICMS_Estado;
        EdtAliquota_ICMS_Cupom_Estado.text:=Get_Aliquota_ICMS_Cupom_Estado;
        ComboIsentoICMS_ForaEstado.text:=Get_IsentoICMS_ForaEstado;
        ComboSubstituicaoICMS_ForaEstado.text:=Get_SubstituicaoICMS_ForaEstado;
        EdtValorPauta_Sub_Trib_ForaEstado.text:=Get_ValorPauta_Sub_Trib_ForaEstado;
        EdtAliquota_ICMS_ForaEstado.text:=Get_Aliquota_ICMS_ForaEstado;
        EdtReducao_BC_ICMS_ForaEstado.text:=Get_Reducao_BC_ICMS_ForaEstado;
        EdtAliquota_ICMS_Cupom_ForaEstado.text:=Get_Aliquota_ICMS_Cupom_ForaEstado;
        EdtSituacaoTributaria_TabelaA.text:=SituacaoTributaria_TabelaA.Get_codigo;
        EdtSituacaoTributaria_TabelaB.text:=SituacaoTributaria_TabelaB.Get_codigo;
        EdtClassificacaoFiscal.text:=Get_ClassificacaoFiscal;
        EdtPrecoVendaInstalado.Text:=Get_PrecoVendaInstalado;
        EdtPrecoVendaRetirado.Text:=Get_PrecoVendaRetirado;
        EdtPrecoVendaFornecido.Text:=Get_PrecoVendaFornecido;
        edtpercentualagregado.Text:=Get_percentualagregado;
        edtNCM.Text:=Get_NCM;
        edtCest.Text := Get_cest;
        edtEspessuraVidro.Text:=Get_EspessuraVidro;
        edtAltura.Text:=Get_Altura;
        edtLargura.Text:=Get_Largura;

        if(Get_KitPerfilado='S') then
        begin
             grp1.Visible:=True;
             COMBO_KitPerfilado.Text:='SIM'
        end
        else
        begin
            grp1.Visible:=false;
            COMBO_KitPerfilado.Text:='N�O'
        end;


        if(Get_Ativo='S') then
        begin
            chkAtivo.Checked:=True;
            lbAtivoInativo.Caption:='Ativo';
        end
        else
        begin
            chkAtivo.Checked:=False;
            lbAtivoInativo.Caption:='Inativo';
        end;

//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFPERFILADO.TabelaParaControles: Boolean;
begin
     If (ObjPerfiladoCor.Perfilado.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFPERFILADO.edtFornecedorExit(Sender: TObject);
begin
   ObjPerfiladoCor.Perfilado.EdtFornecedorExit(Sender,LbNomeFornecedor);
end;

procedure TFPERFILADO.edtFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    ObjPerfiladoCor.Perfilado.EdtFornecedorKeyDown(Sender, key, Shift, LbNomeFornecedor);
end;

procedure TFPERFILADO.edtGrupoPerfiladoExit(Sender: TObject);
begin
    ObjPerfiladoCor.Perfilado.EdtGrupoPerfiladoExit(Sender, LbNomeGrupoPerfilado);
end;

procedure TFPERFILADO.edtGrupoPerfiladoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   ObjPerfiladoCor.Perfilado.EdtGrupoPerfiladoKeyDown(Sender, Key, Shift,LbNomeGrupoPerfilado);
end;

procedure TFPERFILADO.edtSituacaoTributaria_TabelaAExit(Sender: TObject);
begin
   ObjPerfiladoCor.Perfilado.EdtSituacaoTributaria_TabelaAExit(Sender, Nil);
end;

procedure TFPERFILADO.edtSituacaoTributaria_TabelaAKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  ObjPerfiladoCor.Perfilado.EdtSituacaoTributaria_TabelaAKeyDown(Sender, Key, Shift,Nil);
end;

procedure TFPERFILADO.edtSituacaoTributaria_TabelaBExit(Sender: TObject);
begin
   ObjPerfiladoCor.Perfilado.EdtSituacaoTributaria_TabelaBExit(Sender, Nil);
end;

procedure TFPERFILADO.edtSituacaoTributaria_TabelaBKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   ObjPerfiladoCor.Perfilado.EdtSituacaoTributaria_TabelaBKeyDown(Sender, Key, Shift, Nil);
end;

procedure TFPERFILADO.edtPorcentagemInstaladoExit(Sender: TObject);
Var
    PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaInstalado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemInstalado.Text))/100)));
end;

procedure TFPERFILADO.edtPorcentagemFornecidoExit(Sender: TObject);
Var
  PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaFOrnecido.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemFOrnecido.Text))/100)));
end;

procedure TFPERFILADO.edtPorcentagemRetiradoExit(Sender: TObject);
Var
    PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));

    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaRetirado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemRetirado.Text))/100)));
end;

procedure TFPERFILADO.PreparaCor;
begin
     //Resgatando as Cores para essa ferragem
     Self.ResgataCores;
     //habilitando os edits desse panel
     EdtValorCusto.Enabled:=false;
    // habilita_campos(PanelCor);
     EdtPorcentagemAcrescimo.Enabled:=False;
     EdtPorcentagemAcrescimoFinal.Enabled:=False;

     edtinstaladofinal.enabled:=False;
     edtretiradofinal.enabled:=False;
     edtfornecidofinal.enabled:=False;
    { if(PAlteraCampoEstoque=true)
     then EdtEstoque.Enabled:=True
     else
     EdtEstoque.Enabled:=False;    }

     lbnomecor.caption:='';
     LbCusto.caption:='Custo'+#13+'R$ '+EdtPrecoCusto.text;

end;

procedure TFPERFILADO.ResgataCores;
begin
     ObjPerfiladoCor.ResgataCorPerfilado(lbCodigo.Caption);
     formatadbgrid(DBGRIDCOR);
     DBGRIDCOR.Ctl3D:=False;
end;

procedure TFPERFILADO.btGravarCorClick(Sender: TObject);
begin
    With ObjPERFILADOCOR do
    Begin
        ZerarTabela;

        if (edtcodigoCOR.text='')
        or (edtcodigoCOR.text='0')
        Then Begin
                Status:=dsInsert;
                edtcodigoCOR.Text:='0';
        End
        Else Status:=dsEdit;

        Submit_Codigo(edtcodigoCOR.Text);
        Submit_classificacaofiscal(edtclassificacaofiscal_COR.Text);
        Perfilado.Submit_codigo(lbCodigo.Caption);
        Cor.Submit_codigo(edtCor.text);
        Submit_PorcentagemAcrescimo(EdtPorcentagemAcrescimo.Text);
        Submit_AcrescimoExtra(edtAcrescimoExtra.text);
        Submit_ValorExtra(EdtValorExtra.Text);
        Submit_ValorFinal(EdtValorFinal.Text);
        //Submit_Estoque(EdtEstoque.Text);
        Submit_EstoqueMinimo( edtEstoqueMinimo.Text );


        if (Salvar(True)=False)
        Then Begin
                  EdtCorReferencia.SetFocus;
                  exit;
        End;
        lbnomecor.caption:='';
        limpaedit(panelcor);
        EdtCorReferencia.SetFocus;
        Self.ResgataCores;
    End;

end;

procedure TFPERFILADO.BtExcluirCorClick(Sender: TObject);
begin
     if (edtcodigoCOR.Text='')then
     exit;

     If (DBGRIDCOR.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGRIDCOR.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir essa cor da Ferragem Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     ObjPERFILADOCor.Exclui(DBGRIDCOR.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     Self.ResgataCores;
end;

procedure TFPERFILADO.BtCancelarCorClick(Sender: TObject);
begin
     limpaedit(panelcor);
     EdtValorCusto.Clear;
     EdtPorcentagemAcrescimo.Clear;
     EdtValorExtra.Clear;
     EdtAcrescimoExtra.Clear;
     EdtPorcentagemAcrescimoFinal.Clear;
     EdtValorFinal.Clear;;
     lbnomecor.caption:='';
     EdtCorReferencia.SetFocus;
end;

procedure TFPERFILADO.edtCorReferenciaExit(Sender: TObject);
begin
     ObjPerfiladoCor.EdtCor_PorcentagemCor_Exit(Sender,EdtCor, lbnomecor, EdtPorcentagemAcrescimo);
     EdtValorCusto.Enabled:=false;
     EdtValorCusto.Text:=formata_valor(ObjPerfiladoCor.Perfilado.Get_PrecoCusto);

     GrpVidros.Enabled:=true;
     edtacrescimoExtra.enabled:=true;
     edtValorExtra.enabled:=true;
     //EdtValorExtra.SetFocus;
end;

procedure TFPERFILADO.edtCorReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjPerfiladoCOR.EdtCorKeyDown(Sender, EdtCor, Key, Shift, LbNomeCor);
end;

procedure TFPERFILADO.dbgridCORDblClick(Sender: TObject);
begin

     If (DBGRIDCOR.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGRIDCOR.DataSource.DataSet.RecordCount=0)
     Then exit;



     limpaedit(PanelCor);
     lbnomecor.caption:='';
     if (ObjPerfiladoCor.LocalizaCodigo(DBGRIDCOR.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataCores;
               exit;
     End;

     With ObjPerfiladoCor do
     Begin
          TabelaparaObjeto;
          edtcodigoCOR.text:=Get_Codigo;
          edtclassificacaofiscal_COR.text:=Get_classificacaofiscal;
          EdtCor.text:=Cor.Get_codigo;
          EdtCorReferencia.Text:=Cor.Get_Referencia;
          lbnomecor.caption:=Cor.Get_Descricao;
          EdtPorcentagemAcrescimo.text:=Get_PorcentagemAcrescimo;
          lbestoque.Caption:=RetornaEstoque+' '+Perfilado.Get_Unidade;
          edtEstoqueMinimo.Text := Get_EstoqueMinimo;

          EdtAcrescimoExtra.text:=Get_AcrescimoExtra;
          EdtPorcentagemAcrescimoFinal.text:=Get_PorcentagemAcrescimoFinal;

          EdtValorCusto.Enabled:=false;
          EdtValorExtra.Enabled:=true;
          EdtAcrescimoExtra.Enabled:=true;
          EdtValorExtra.Text:=formata_valor(Get_ValorExtra);
          EdtValorFinal.Text:=formata_valor(Get_ValorFinal);
          EdtValorCusto.Text:=formata_valor(Perfilado.Get_PrecoCusto);

          edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
          edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
          edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));

          EdtCorReferencia.SetFocus;
     End;

end;

procedure TFPERFILADO.DBGRIDCORKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key<>#13)
  then Self.DBGRIDCORDblClick(sender);
end;

procedure TFPERFILADO.edtAcrescimoExtraExit(Sender: TObject);
Var PValorCusto, PAcrescimoExtra, PValorExtra :Currency;
begin
    if ((TEdit(Sender).Text=''))then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoCusto.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaInstalado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaFornecido.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaRetirado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtCor.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;

    PValorCusto:=0;
    PAcrescimoExtra:=0;
    PValorExtra:=0;

    try
          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));

          PAcrescimoExtra:=StrTOCurr(tira_ponto(EdtAcrescimoExtra.Text));
          PValorExtra:=((PValorCusto*PAcrescimoExtra)/100);
          EdtValorExtra.Text:=Formata_Valor(PValorExtra);
    except;
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais.');
    end;

    EdtPorcentagemAcrescimoFinal.Text:=CurrToStr(StrToCurr(tira_ponto(EdtPorcentagemAcrescimo.Text))+StrToCurr(tira_ponto(EdtAcrescimoExtra.Text)));

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(tira_ponto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
end;

procedure TFPERFILADO.edtValorExtraExit(Sender: TObject);
Var
PValorCusto, PPorcentagemExtra, PValorExtra : Currency;
begin
    if ((TEdit(Sender).Text=''))then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoCusto.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaInstalado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaFornecido.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaRetirado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtCor.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);

    PValorCusto:=0;
    PValorExtra:=0;
    PPorcentagemExtra:=0;

    try
          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));

          PValorExtra:=StrToCurr(Tira_Ponto(EdtValorExtra.Text));

          PPorcentagemExtra:=(PValorExtra*100)/PValorCusto;
          EdtAcrescimoExtra.Text:=FormatFloat('#,##0.00',PPorcentagemExtra);
    except
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais');
    end;
    //********** Atualiza Valor Final, s� pra mostra porque este campo � Computed By
    EdtPorcentagemAcrescimoFinal.Text :=formata_valor(StrToCurr(Tira_Ponto(EdtPorcentagemAcrescimo.Text))+
                                      StrToCurr(tira_ponto(EdtAcrescimoExtra.Text))
                                      );

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(Tira_POnto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
end;

procedure TFPERFILADO.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjPerfiladoCor.Perfilado.PrimeiroRegistro = false)then
    exit;

    ObjPerfiladoCor.Perfilado.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFPERFILADO.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigo.Caption='')then
    lbCodigo.Caption:='0';

    if  (ObjPerfiladoCor.Perfilado.RegistoAnterior(StrToInt(lbCodigo.Caption)) = false)then
    exit;

    ObjPerfiladoCor.Perfilado.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFPERFILADO.btProximoClick(Sender: TObject);
begin
    if (lbCodigo.Caption='')then
    lbCodigo.Caption:='0';

    if  (ObjPerfiladoCor.Perfilado.ProximoRegisto(StrToInt(lbCodigo.Caption)) = false)then
    exit;

    ObjPerfiladoCor.Perfilado.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFPERFILADO.btUltimoClick(Sender: TObject);
begin
    if  (ObjPerfiladoCor.Perfilado.UltimoRegistro = false)then
    exit;

    ObjPerfiladoCor.Perfilado.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFPERFILADO.edtPrecoCustoExit(Sender: TObject);
begin
      TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
      if(EdtPorcentagemInstalado.Text<>'')
      then EdtPorcentagemInstaladoExit(Sender);
      if(EdtPorcentagemFornecido.Text<>'')
      then EdtPorcentagemFornecidoExit(sender);
      if(EdtPorcentagemRetirado.Text<>'')
      then EdtPorcentagemRetiradoExit(Sender);
end;

procedure TFPERFILADO.edtAreaMinimaExit(Sender: TObject);
begin
//    Guia.TabIndex:=1;
end;

procedure TFPERFILADO.edtPrecoVendaInstaladoExit(Sender: TObject);
Var
    PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemInstalado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaInstalado.Text))/precocusto));
end;

procedure TFPERFILADO.edtPrecoVendaFornecidoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemfornecido.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendafornecido.Text))/precocusto));
end;

procedure TFPERFILADO.edtPrecoVendaRetiradoExit(Sender: TObject);
Var
    PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemretirado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaretirado.Text))/precocusto));
end;

procedure TFPERFILADO.dbgridCORDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGRIDCOR.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGRIDCOR.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGRIDCOR.DefaultDrawDataCell(Rect,DBGRIDCOR.Columns[DataCol].Field, State);
          End;
end;

procedure TFPERFILADO.AbreComCodigo(parametro:string);
begin
    if(ObjPerfiladoCor.Perfilado.LocalizaReferencia(parametro)=True)
    then begin
        ObjPerfiladoCor.Perfilado.TabelaparaObjeto;
        Self.ObjetoParaControles;

    end;
end;

procedure TFPERFILADO.FormShow(Sender: TObject);
begin
      limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.TabIndex:=0;

     ColocaUpperCaseEdit(Self);

     Try
        Self.ObjPerfiladoCor:=TObjPerfiladoCor.create;
        DBGRIDCOR.DataSource:=ObjPerfiladoCor.ObjDatasource;
        FRImposto_ICMS1.ObjMaterial_ICMS:=TObjperfilado_ICMS.Create(self);
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;


     habilita_botoes(Self);
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE PERFILADO')=False)
     Then desab_botoes(Self);

     PAlteraCampoEstoque:=False;
     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('PERMITE ALTERAR O CAMPO ESTOQUE NO CADASTRO DE PERFILADO')=True)
     Then PAlteraCampoEstoque:=true;


     Rg_forma_de_calculo_percentual.itemindex:=0;
     if (ObjParametroGlobal.ValidaParametro('FORMA DE CALCULO DE PERCENTUAL DE ACRESCIMO NAS CORES DO PERFILADO')=True)
     then Begin
               if (ObjParametroGlobal.get_valor='1')
               then Rg_forma_de_calculo_percentual.itemindex:=1; 
     End;
     Rg_forma_de_calculo_percentual.enabled:=False;

     AlteraPrecoPeloCusto:=True;
     if (ObjParametroGlobal.ValidaParametro('CALCULA PRECO DE VENDA DO PERFILADO PELO CUSTO E PERCENTUAIS?')=True)
     then Begin
               if (ObjParametroGlobal.get_valor<>'SIM')
               then AlteraPrecoPeloCusto:=false;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
      FescolheImagemBotao.PegaFiguraBotaopequeno(Btgravarcor,'BOTAOINSERIR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btcancelacor,'BOTAOCANCELARPRODUTO.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btexcluircor,'BOTAORETIRAR.BMP');
     lbCodigo.caption:='';

     if(Tag<>0)
     then begin
          if(ObjPerfiladoCor.Perfilado.LocalizaCodigo(IntToStr(Tag))=True)
          then
          begin
              ObjPerfiladoCor.Perfilado.TabelaparaObjeto;
              self.ObjetoParaControles;
          end;

     end;

end;

procedure TFPERFILADO.edtGrupoPerfiladoDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FgrupoPerfilado:TFGRUPOPERFILADO  ;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            try
                FgrupoPerfilado:=TFGRUPOPERFILADO.Create(nil);
            except

            end;


            If (FpesquisaLocal.PreparaPesquisa('Select * from tabgrupoperfilado','',FgrupoPerfilado)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtGrupoPerfilado.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 LbNomeGrupoPerfilado.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FgrupoPerfilado);

     End;
end;

procedure TFPERFILADO.edtFornecedorDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Ffornecedor:TFFORNECEDOR;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Ffornecedor:=TFFORNECEDOR.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabfornecedor','',Ffornecedor)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtFornecedor.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 LbNomeFornecedor.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('razaosocial').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Ffornecedor);

     End;
end;

procedure TFPERFILADO.edtCorReferenciaDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Fcor:TFCOR ;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fcor:=TFCOR.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabcor','',Fcor)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtCorReferencia.text:=FpesquisaLocal.QueryPesq.fieldbyname('referencia').asstring;
                                 lbnomecor.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fcor);

     End;
end;
procedure TFPERFILADO.edtNCMKeyPress(Sender: TObject; var Key: Char);
begin
      if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFPERFILADO.lbestoqueMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFPERFILADO.lbestoqueMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

//Faz o lan�amento na tabestoque de materiais
procedure TFPERFILADO.lbestoqueClick(Sender: TObject);
begin
     if(ObjParametroGlobal.ValidaParametro('UTILIZA SPED FISCAL')=false) then
     begin
          MensagemAviso('O Parametro "UTILIZA SPED FISCAL" n�o foi encontrado!!!');
          Exit;
     end;
     if(ObjParametroGlobal.Get_Valor='SIM')
     then Exit;

     If(PAlteraCampoEstoque=False)then
     begin
          MensagemAviso('O Usu�rio '+ObjUsuarioGlobal.Get_nome+' n�o tem autoriza��o para alterar o estoque');
          Exit;
     end
     else
     begin
          with FfiltroImp do
          begin
              DesativaGrupos;
              Grupo01.Enabled:=True;
              edtgrupo01.Text:='';
              LbGrupo01.Caption:='QUANTIDADE';
              edtgrupo01.OnKeyPress:=edtquant;

              MensagemAviso('Acerte o estoque: Digite a quantidade a tirar ou acrescentar no estoque');

              ShowModal;

              if(Tag=0)
              then Exit;

              if(edtgrupo01.Text='') or(edtgrupo01.Text='0')
              then Exit;
              
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Status:=dsInsert;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Submit_DATA(FormatDateTime('dd/mm/yyyy',Now));
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('PELO MODO ACERTO DE ESTOQUE (PERFILADO)');
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(edtgrupo01.Text);
              OBJESTOQUEGLOBAL.Submit_PERFILADOCOR(ObjPERFILADOCOR.Get_Codigo);

              if (OBJESTOQUEGLOBAL.Salvar(True)=False)
              Then Begin
                     MensagemErro('Erro na tentativa de salvar o registro de estoque do PERFILADO');
                     exit;
              End;

              lbestoque.Caption:=ObjPERFILADOCOR.RetornaEstoque+' '+ObjPERFILADOCOR.Perfilado.Get_Unidade;


          end;
     end;
end;

procedure TFPERFILADO.edtquant(Sender: TObject;
  var Key: Char);
begin
     if not (Key in['0'..'9',Chr(8),'-',',']) then
    begin
        Key:= #0;
    end;

end;

procedure TFPERFILADO.chkAtivoClick(Sender: TObject);
var
  QueryPesq:TIBQuery;
  FERRODESATIVARMATERIAL:TFErrosDesativarMaterial;
begin
      if(lbCodigo.Caption='') or (lbCodigo.Caption='0')
      then Exit;

      QueryPesq:=TIBQuery.Create(nil);
      QueryPesq.Database:=FDataModulo.IBDatabase;
      FERRODESATIVARMATERIAL:=TFErrosDesativarMaterial.Create(nil);

      try
          if(chkAtivo.Checked=True)
          then lbAtivoInativo.Caption:='Ativo';

          if(chkAtivo.Checked=False) then
          begin
              with QueryPesq do
              begin
                  Close;
                  SQL.Clear;
                  SQL.Add('select * from tabperfilado_proj where perfilado='+lbCodigo.Caption);
                  Open;

                  if(recordcount=0)
                  then lbAtivoInativo.Caption:='Inativo'
                  else
                  begin
                       chkAtivo.Checked:=True;
                       FERRODESATIVARMATERIAL.PassaMaterial(lbCodigo.Caption,EdtDescricao.Text,'Perfilado');
                       FERRODESATIVARMATERIAL.ShowModal;
                  end;

              end;


          end;
      finally
           FreeAndNil(QueryPesq);
           FERRODESATIVARMATERIAL.Free;
      end;



end;

procedure TFPERFILADO.btAjudaClick(Sender: TObject);
begin
     FAjuda.PassaAjuda('CADASTRO DE PERFILADOS');
     FAjuda.ShowModal;
end;

procedure TFPERFILADO.FRImposto_ICMS1btReplicarPedidoClick(
  Sender: TObject);
begin
     FRImposto_ICMS1.Button1Click(Sender);
end;

procedure TFPERFILADO.GuiaChange(Sender: TObject);
begin
     if (Guia.TabIndex=2)//cor
     Then Begin

               if (OBJPERFILADOCOR.Perfilado.Status=dsinsert)
               or (lbCodigo.Caption='')
               Then Begin
                         exit;
               End;
               habilita_campos(panelCor);
               Self.PreparaCor;
               EdtCorReferencia.SetFocus;

     End
     Else
     Begin
             if (Guia.TabIndex=1)//Impostos
             Then Begin
                      if (ObjPerfiladoCor.Perfilado.Status=dsinsert)
                      or (lbCodigo.Caption='')
                      Then Begin
                                exit;
                      End;
                      FRImposto_ICMS1.Acessaframe(lbCodigo.Caption);
             End
             Else Begin
                      Guia.TabIndex:=0;
             End;
     end;
end;

procedure TFPERFILADO.COMBO_KitPerfiladoExit(Sender: TObject);
begin
     if(COMBO_KitPerfilado.Text='SIM')
     then grp1.Visible:=True;
end;

procedure TFPERFILADO.COMBO_KitPerfiladoKeyPress(Sender: TObject;
  var Key: Char);
begin
   key:=#0;
end;

procedure TFPERFILADO.edtGrupoPerfiladoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFPERFILADO.edtFornecedorKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFPERFILADO.edtPorcentagemInstaladoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFPERFILADO.edtPorcentagemFornecidoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFPERFILADO.edtPorcentagemRetiradoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFPERFILADO.edtPrecoCustoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFPERFILADO.lbNomeGrupoPerfiladoClick(Sender: TObject);
var
  FgrupoPerfilado:TFGRUPOPERFILADO;
begin
     try
       FgrupoPerfilado:=TFGRUPOPERFILADO.Create(nil);
     except
       Exit;
     end;

     try
       if(edtGrupoPerfilado.Text='')
       then Exit;

       FgrupoPerfilado.tag:=StrToInt(edtGrupoPerfilado.Text);
       FgrupoPerfilado.ShowModal;

     finally
       FreeAndNil(FgrupoPerfilado);
     end;


end;

procedure TFPERFILADO.lbNomeGrupoPerfiladoMouseLeave(Sender: TObject);
begin
TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFPERFILADO.lbNomeGrupoPerfiladoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFPERFILADO.lbNomeFornecedorClick(Sender: TObject);
var
  FFornecedor:TFFORNECEDOR;
begin
  try
    FFornecedor:=TFFORNECEDOR.Create(nil);
  except
    Exit;
  end;

  try
    if(edtFornecedor.Text='')
    then Exit;

    FFornecedor.tag:=StrToInt(edtFornecedor.Text);
    FFornecedor.ShowModal;

  finally
    FreeAndNil(FFornecedor);
  end;
end;

procedure TFPERFILADO.edtEspessuraVidroKeyPress(Sender: TObject;
  var Key: Char);
begin
      If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFPERFILADO.edtAlturaKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFPERFILADO.edtLarguraKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFPERFILADO.edtAreaMinimaKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFPERFILADO.lbnomecorClick(Sender: TObject);
var
  Fcor:TFCOR;
begin
   try
      Fcor:=TFCOR.Create(nil);
   except
      Exit;
   end;

   try
     if(EdtCor.Text='')
     then Exit;

     Fcor.Tag:=StrToInt(EdtCor.Text);
     fcor.ShowModal;
   finally
     FreeAndNil(Fcor);
   end; 

end;

procedure TFPERFILADO.edtValorCustoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFPERFILADO.edtNCMKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  FDataModulo.edtNCMKeyDown(Sender,Key,Shift);
end;

procedure TFPERFILADO.edtCestKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if Key=vk_f9 then
  begin
    Clipboard.AsText := EDTNCM.Text;
    With TObjCEST.Create do
    begin
      edtCESTkeydown(sender,Key,shift);
      Free;
    end;
  end;
end;

end.

