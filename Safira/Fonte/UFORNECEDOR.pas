unit UFORNECEDOR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls,db, UObjFORNECEDOR,
  UessencialGlobal, Tabs,IBQuery,URAMOATIVIDADE,UPrazoPagamento,UpesquisaMenu,Uportador,
  ComCtrls;

type
  TFFORNECEDOR = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lb1: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    lbCodigoFornecedor: TLabel;
    pnl1: TPanel;
    imgrodape: TImage;
    lb14: TLabel;
    btAjuda: TSpeedButton;
    pgcGuia: TPageControl;
    tsPrincipal: TTabSheet;
    panelPrincipal: TPanel;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    lb15: TLabel;
    lb16: TLabel;
    lb17: TLabel;
    lb18: TLabel;
    lb19: TLabel;
    lb20: TLabel;
    lb21: TLabel;
    lb22: TLabel;
    lb23: TLabel;
    lb24: TLabel;
    lbdatacadastro: TLabel;
    lb26: TLabel;
    lb27: TLabel;
    lb28: TLabel;
    Label1: TLabel;
    lbNomeTipoFornecedor: TLabel;
    lb25: TLabel;
    mmoobservacoes: TMemo;
    edtendereco: TEdit;
    cbbcidade: TComboBox;
    edtestado: TEdit;
    edtCodigoCidade: TEdit;
    cbbpais: TComboBox;
    edtfax: TEdit;
    edtemail: TEdit;
    edtcep: TEdit;
    edtcontato: TEdit;
    edtbairro: TEdit;
    edtie: TEdit;
    edtfantasia: TEdit;
    edtdatacadastro: TMaskEdit;
    edtrazaosocial: TEdit;
    edtfone: TEdit;
    edtTipoFornecedor: TEdit;
    edtNumero: TEdit;
    tsCredito: TTabSheet;
    panel2: TPanel;
    edtagencia: TEdit;
    edtcontacorrente: TEdit;
    lb2: TLabel;
    lb3: TLabel;
    lbnomeramoatividade: TLabel;
    lbprazopagamento: TLabel;
    edtramoatividade: TEdit;
    lb4: TLabel;
    lb5: TLabel;
    edtprazopagamento: TEdit;
    edtcomissao: TEdit;
    lb6: TLabel;
    edtbanco: TEdit;
    edtrepresentante: TEdit;
    edtportador: TEdit;
    edtCodigoPlanoContas: TMaskEdit;
    lb7: TLabel;
    lb8: TLabel;
    lb9: TLabel;
    lb10: TLabel;
    edtCGC: TMaskEdit;
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtRamoAtividadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtPrazoPagamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtRamoAtividadeExit(Sender: TObject);
    procedure EdtCodigoPlanoContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MemoObservacoesExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtRamoAtividadeKeyPress(Sender: TObject; var Key: Char);
    procedure EdtCodigoPlanoContasKeyPress(Sender: TObject; var Key: Char);
    procedure EdtPrazoPagamentoKeyPress(Sender: TObject; var Key: Char);
    procedure edtRamoAtividadeDblClick(Sender: TObject);
    procedure EdtCodigoPlanoContasDblClick(Sender: TObject);
    procedure EdtPrazoPagamentoDblClick(Sender: TObject);
    procedure cbbcidadeExit(Sender: TObject);
    procedure edtestadoExit(Sender: TObject);
    procedure edtTipoFornecedorExit(Sender: TObject);
    procedure edtTipoFornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportadorDblClick(Sender: TObject);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btAjudaClick(Sender: TObject);
    procedure edtTipoFornecedorDblClick(Sender: TObject);
    procedure edtportadorKeyPress(Sender: TObject; var Key: Char);
    procedure edtcgcExit(Sender: TObject);
    procedure lbNomeTipoFornecedorClick(Sender: TObject);
    procedure lbNomeTipoFornecedorMouseLeave(Sender: TObject);
    procedure lbNomeTipoFornecedorMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
  private
         ObjFORNECEDOR:TObjFORNECEDOR;
         
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    procedure MostraQuantidadeCadastrada;
    function  retornaCodigoPais(parametro: string): string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFORNECEDOR: TFFORNECEDOR;


implementation

uses Upesquisa, UessencialLocal, UPLanodeContas, UDataModulo,
  UescolheImagemBotao, Uprincipal, UAjuda, UTIPOCLIENTE;

{$R *.dfm}

procedure TFFORNECEDOR.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjFornecedor=Nil)
     Then exit;

     If (Self.ObjFornecedor.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjFornecedor.free;
    
end;


procedure TFFORNECEDOR.btNovoClick(Sender: TObject);
begin
     pgcGuia.TabIndex:=0;
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);
     lbCodigoFornecedor.Caption:='0';
     //edtcodigo.text:=Self.ObjFornecedor.Get_novocodigo;
     edtcgc.EditMask:=MascaraCNPJ;
     
     EdtCodigoPlanoContas.TEXT:='0';
     EDTRAMOATIVIDADE.TEXT:='';
     EdtPrazoPagamento.TEXT:='';//a vista
     //ComboEstado.text:=ESTADOSISTEMAGLOBAL;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjFornecedor.status:=dsInsert;
     EdtDataCadastro.Text:=DateToStr(Now);
     EdtRazaoSocial.setfocus;

     btalterar.visible:=false;
     btrelatorios.visible:=false;
     btsair.Visible:=false;
     btopcoes.visible:=false;
     btexcluir.Visible:=false;
     btnovo.Visible:=false;
     btopcoes.visible:=false;


end;

procedure TFFORNECEDOR.btSalvarClick(Sender: TObject);
begin

     If Self.ObjFornecedor.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjFornecedor.salvar(true)=False)
     Then exit;

     lbCodigoFornecedor.Caption:=Self.ObjFornecedor.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
     btopcoes.visible:=true;
     MostraQuantidadeCadastrada;
     pgcGuia.TabIndex:=0;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFFORNECEDOR.btAlterarClick(Sender: TObject);
begin
    If (Self.ObjFornecedor.Status=dsinactive) and (lbCodigoFornecedor.Caption<>'')
    Then
    Begin
                pgcGuia.TabIndex:=0;
                habilita_campos(Self);
                Self.ObjFornecedor.Status:=dsEdit;
                EdtRazaoSocial.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btalterar.visible:=false;
                btrelatorios.visible:=false;
                btsair.Visible:=false;
                btopcoes.visible:=false;
                btexcluir.Visible:=false;
                btnovo.Visible:=false;
                btopcoes.visible:=false;
    End;

end;

procedure TFFORNECEDOR.btCancelarClick(Sender: TObject);
begin
     Self.ObjFornecedor.cancelar;
     btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
      btopcoes.visible:=true;
     lbCodigoFornecedor.Caption:='';
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     pgcGuia.TabIndex:=0;
end;

procedure TFFORNECEDOR.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
           edtcgc.EditMask:='';
            If (FpesquisaLocal.PreparaPesquisa(Self.ObjFornecedor.Get_pesquisa,Self.ObjFornecedor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjFornecedor.status<>dsinactive
                                  then exit;

                                  If (Self.ObjFornecedor.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                 Self.ObjFornecedor.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFFORNECEDOR.btExcluirClick(Sender: TObject);
begin
     If (Self.ObjFornecedor.status<>dsinactive) or (lbCodigoFornecedor.Caption='')
     Then exit;

     If (Self.ObjFornecedor.LocalizaCodigo(lbCodigoFornecedor.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjFornecedor.exclui(lbCodigoFornecedor.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
     lbCodigoFornecedor.Caption:='';

end;

procedure TFFORNECEDOR.btRelatorioClick(Sender: TObject);
begin
  ///  Self.ObjFornecedor.Imprime;
end;

procedure TFFORNECEDOR.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFFORNECEDOR.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFFORNECEDOR.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFFORNECEDOR.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    If Key=VK_Escape
    Then Self.Close;
     if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;
    if (Key = VK_F2) then
    begin

           FAjuda.PassaAjuda('CADASTRO DE FORNECEDOR');
           FAjuda.ShowModal;
    end;

end;


procedure TFFORNECEDOR.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;


function TFFORNECEDOR.ControlesParaObjeto: Boolean;
begin
  Try
    With Self.ObjFornecedor do
    Begin
        Submit_Codigo(lbCodigoFornecedor.Caption);
        Submit_RazaoSocial(edtRazaoSocial.text);
        Submit_Fantasia(edtFantasia.text);
        Submit_CGC(edtCGC.text);
        Submit_IE(edtIE.text);
        Submit_Endereco(edtEndereco.text);
        Submit_Fone(edtFone.text);
        Submit_Fax(edtFax.text);
        Submit_Cidade(cbbcidade.Text);
        Submit_Estado(edtestado.text);
        Submit_Contato(edtContato.text);
        Submit_Email(edtEmail.text);
        Submit_DataCadastro(edtDataCadastro.text);
        RamoAtividade.Submit_codigo(edtRamoAtividade.text);
        Submit_CodigoPlanoContas(edtCodigoPlanoContas.text);
        Submit_Banco(edtBanco.text);
        Submit_Agencia(edtAgencia.text);
        Submit_ContaCorrente(edtContaCorrente.text);
        Submit_Bairro(edtBairro.text);
        Submit_CEP(edtCEP.text);
        Submit_codigocidade(edtcodigocidade.text);
        Submit_Observacoes(MmoObservacoes.text);
        PrazoPagamento.Submit_codigo(edtPrazoPagamento.text);
        tipo.Submit_CODIGO(edtTipoFornecedor.Text);
        Submit_CodigoPais(retornaCodigoPais(cbbpais.Text));
        Submit_Numero(edtNumero.Text);

         result:=true;
    End;
  Except
        result:=False;
  End;

end;


procedure TFFORNECEDOR.LimpaLabels;
begin
//LIMPA LABELS
end;


function TFFORNECEDOR.ObjetoParaControles: Boolean;
begin
  Try
     With Self.ObjFornecedor do
     Begin
        lbCodigoFornecedor.Caption:=Get_Codigo;
        EdtRazaoSocial.text:=Get_RazaoSocial;
        EdtFantasia.text:=Get_Fantasia;
        EdtCGC.text:=Get_CGC;
        EdtIE.text:=Get_IE;
        EdtEndereco.text:=Get_Endereco;
        EdtFone.text:=Get_Fone;
        EdtFax.text:=Get_Fax;
        cbbcidade.Text   :=Get_Cidade;
        edtestado.text:=Get_Estado;
        EdtContato.text:=Get_Contato;
        EdtEmail.text:=Get_Email;
        EdtDataCadastro.text:=Get_DataCadastro;
        EdtRamoAtividade.text:=RamoAtividade.Get_codigo;
        EdtCodigoPlanoContas.text:=Get_CodigoPlanoContas;
        EdtBanco.text:=Get_Banco;
        EdtAgencia.text:=Get_Agencia;
        EdtContaCorrente.text:=Get_ContaCorrente;
        EdtBairro.text:=Get_Bairro;
        EdtCEP.text:=Get_CEP;
        Edtcodigocidade.text:=Get_codigocidade;
        MmoObservacoes.text:=Get_Observacoes;
        EdtPrazoPagamento.text:=PrazoPagamento.Get_codigo;
        lbprazopagamento.Caption:=PrazoPagamento.Get_Nome;
        lbNomeTipoFornecedor.Caption:=tipo.Get_NOME;
        edtTipoFornecedor.Text:=tipo.Get_CODIGO;
        edtCodigoCidade.Text:=Get_codigocidade;
        cbbpais.Text:=Get_CodigoPais;
        edtNumero.Text:=Get_Numero;

//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;


function TFFORNECEDOR.TabelaParaControles: Boolean;
begin
     If (Self.ObjFornecedor.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;


procedure TFFORNECEDOR.EdtRamoAtividadeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   Self.ObjFornecedor.EdtRamoAtividadeKeyDown(Sender,Key, Shift,LbNomeRamoAtividade);
end;


procedure TFFORNECEDOR.EdtPrazoPagamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   Self.ObjFornecedor.EdtPrazoPagamentoKeyDown(Sender, key, Shift, LbPrazoPagamento);
end;


procedure TFFORNECEDOR.edtRamoAtividadeExit(Sender: TObject);
begin
   Self.ObjFornecedor.EdtRamoAtividadeExit(Sender, LbNomeRamoAtividade);
end;


procedure TFFORNECEDOR.EdtCodigoPlanoContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FplanodeCOntas:TFplanodeCOntas;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FplanodeCOntas:=TFplanodeCOntas.create(NIl);
            If (FpesquisaLocal.PreparaPesquisa(ObjLanctoPortadorGlobal.Portador.PlanodeContas.Get_Pesquisa,ObjLanctoPortadorGlobal.Portador.PlanodeContas.Get_TituloPesquisa,FplanodeCOntas)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then EdtCodigoPlanoContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FplanodeCOntas);
     End;


end;


procedure TFFORNECEDOR.MemoObservacoesExit(Sender: TObject);
begin
    //Guia.TabIndex:=1;
end;


procedure TFFORNECEDOR.MostraQuantidadeCadastrada;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabfornecedor');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb14.Caption:='Existem '+IntToStr(contador)+' fornecedores cadastrados'
       else
       begin
            lb14.Caption:='Existe '+IntToStr(contador)+' fornecedor cadastrado';
       end;

    finally

    end;


end;


procedure TFFORNECEDOR.FormShow(Sender: TObject);
begin
       //*****estavam no onactivate e on show*****
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     ColocaUpperCaseEdit(Self);

     if not (ObjFornecedor.carregaCidades(cbbcidade)) then
    MensagemAviso('ERRO ao carregar as cidades');

    if not (ObjFornecedor.carregaCodigoPaises(cbbpais)) then
        MensagemAviso('ERRO ao carregar pa�ses');

     Try
        Self.ObjFornecedor:=TObjFornecedor.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     lbCodigoFornecedor.Caption:='';
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');

    if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE FORNECEDOR')=False)
    Then desab_botoes(Self)
    Else habilita_botoes(Self);
    MostraQuantidadeCadastrada;


    if (self.Tag <> 0) then
    begin

      if (Self.ObjFORNECEDOR.LocalizaCodigo (IntToStr (Self.Tag))) then
      begin

        Self.ObjFORNECEDOR.TabelaparaObjeto;
        Self.ObjetoParaControles;
        
      end;

    end;

    pgcGuia.TabIndex:=0;
end;

procedure TFFORNECEDOR.edtRamoAtividadeKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
       begin
              Key:= #0;
       end;
end;

procedure TFFORNECEDOR.EdtCodigoPlanoContasKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
       begin
              Key:= #0;
       end;
end;


procedure TFFORNECEDOR.EdtPrazoPagamentoKeyPress(Sender: TObject;
  var Key: Char);
begin
        if not (Key in['0'..'9',Chr(8)]) then
       begin
              Key:= #0;
       end;
end;


procedure TFFORNECEDOR.edtRamoAtividadeDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FramoAtividade:TFRAMOATIVIDADE ;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FramoAtividade:=TFRAMOATIVIDADE.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabramoatividade','',FramoAtividade)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 edtRamoAtividade.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 LbNomeRamoAtividade.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FramoAtividade);

     End;
end;


procedure TFFORNECEDOR.EdtCodigoPlanoContasDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FplanoContas:TFplanodeContas;
begin

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FplanoContas:=TFplanodeContas.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabplanodecontas','',FplanoContas)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtCodigoPlanoContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                // lbp.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FplanoContas);

     End;
end;


procedure TFFORNECEDOR.EdtPrazoPagamentoDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FprazoPagamento:TFprazoPagamento;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FprazoPagamento:=TFprazoPagamento.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabprazopagamento','',FprazoPagamento)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtPrazoPagamento.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 LbPrazoPagamento.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FprazoPagamento);

     End;
end;


procedure TFFORNECEDOR.cbbcidadeExit(Sender: TObject);
begin
    if (cbbcidade.Text <> '') then
    begin

      edtestado.Text := ObjFornecedor.retornaUF(cbbcidade.Text);

      if (edtestado.Text <> '') then
      begin  
          edtestado.OnExit(edtestado);
          edtcep.Text;
      end;
    end;
end;


procedure TFFORNECEDOR.edtestadoExit(Sender: TObject);
var
   cidade:string;
begin

   cidade := ObjFornecedor.retornaCidade(cbbcidade.Text);
   edtCodigoCidade.Text := ObjFornecedor.retornaCodigoCidade(cidade,edtestado.Text);

end;


function TFFORNECEDOR.retornaCodigoPais(parametro: string):string;
var
  i:Integer;
begin

  Result := '';

  for i:=Pos('|',parametro)+1 to Length (parametro)  do
    Result := Result + parametro[i];

end;


procedure TFFORNECEDOR.edtTipoFornecedorExit(Sender: TObject);
begin

  SELF.ObjFORNECEDOR.EdtTipoExit(sender,lbNomeTipoFornecedor);

end;


procedure TFFORNECEDOR.edtTipoFornecedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

  self.ObjFORNECEDOR.edtTipoKeyDown(Sender,Key,Shift,lbNomeTipoFornecedor);

end;


procedure TFFORNECEDOR.edtportadorDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FPortador:TFportador;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPortador:=TFportador.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabportador','',Fportador)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 edtportador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                // lbp.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fportador);

     End;
end;


procedure TFFORNECEDOR.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FPortador:TFportador;
begin
     If (key <>vk_f9)
     Then exit;
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPortador:=TFportador.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabportador','',Fportador)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 edtportador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                // lbp.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fportador);

     End;
end;


procedure TFFORNECEDOR.btAjudaClick(Sender: TObject);
begin
      FAjuda.PassaAjuda('CADASTRO DE FORNECEDOR');
      FAjuda.ShowModal;
end;

{r4mr}
procedure TFFORNECEDOR.edtTipoFornecedorDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtTipoFornecedorKeyDown(Sender, Key, Shift);
end;

procedure TFFORNECEDOR.edtportadorKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFFORNECEDOR.edtcgcExit(Sender: TObject);
begin
  //Verfica se ja existe algum cliente com o mesmo CPF
  if(VerificaDuplicidadeCPFCGCCadastro('TABFORNECEDOR','CGC',edtcgc.Text,lbCodigoFornecedor.Caption)=True) then
  begin
     If (Messagedlg('Existe um outro FORNECEDOR cadastrado com este mesmo CGC. Deseja gravar este FORNECEDOR com o mesmo CGC?',mtconfirmation,[mbyes,mbno],0)=Mrno) Then
     begin
       edtCGC.Text:='';
       exit;
     end;
  end;
end;

procedure TFFORNECEDOR.lbNomeTipoFornecedorClick(Sender: TObject);
begin
  if(edtTipoFornecedor.Text='')
  then Exit;

  chamaFormulario(TFtipoCliente,self,edtTipoFornecedor.Text);
end;

procedure TFFORNECEDOR.lbNomeTipoFornecedorMouseLeave(Sender: TObject);
begin
  TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFFORNECEDOR.lbNomeTipoFornecedorMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

end.
