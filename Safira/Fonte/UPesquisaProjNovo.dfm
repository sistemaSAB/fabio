object FpesquisaProjNovo: TFpesquisaProjNovo
  Left = 513
  Top = 143
  Width = 914
  Height = 673
  Caption = 'FpesquisaProjNovo'
  Color = 10643006
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnMouseMove = FormMouseMove
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object img1: TImage
    Left = 152
    Top = 64
    Width = 601
    Height = 385
    Stretch = True
  end
  object img2: TImage
    Left = 87
    Top = 504
    Width = 105
    Height = 105
    Stretch = True
    OnMouseMove = img2MouseMove
  end
  object img3: TImage
    Left = 207
    Top = 504
    Width = 105
    Height = 105
    Stretch = True
    OnMouseMove = img3MouseMove
  end
  object img4: TImage
    Left = 327
    Top = 504
    Width = 105
    Height = 105
    Stretch = True
    OnMouseMove = img4MouseMove
  end
  object img5: TImage
    Left = 447
    Top = 504
    Width = 105
    Height = 105
    Stretch = True
    OnMouseMove = img5MouseMove
  end
  object img6: TImage
    Left = 567
    Top = 504
    Width = 105
    Height = 105
    Stretch = True
    OnMouseMove = img6MouseMove
  end
  object img7: TImage
    Left = 687
    Top = 504
    Width = 105
    Height = 105
    Stretch = True
    OnMouseMove = img7MouseMove
  end
  object bvl1: TBevel
    Left = 84
    Top = 498
    Width = 113
    Height = 116
  end
  object bvl2: TBevel
    Left = 203
    Top = 498
    Width = 112
    Height = 116
  end
  object bvl3: TBevel
    Left = 321
    Top = 498
    Width = 113
    Height = 116
  end
  object bvl4: TBevel
    Left = 443
    Top = 499
    Width = 113
    Height = 116
  end
  object bvl5: TBevel
    Left = 562
    Top = 499
    Width = 113
    Height = 116
  end
  object bvl6: TBevel
    Left = 683
    Top = 499
    Width = 113
    Height = 117
  end
  object bt1: TSpeedButton
    Left = 8
    Top = 520
    Width = 65
    Height = 73
    Caption = '<<'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = but1Click
  end
  object bt2: TSpeedButton
    Left = 808
    Top = 517
    Width = 65
    Height = 73
    Caption = '>>'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = but2Click
  end
end
