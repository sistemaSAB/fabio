unit UmostraStringGrid;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, ExtCtrls, FileCtrl, ImgList;

type
  TFmostraStringGrid = class(TForm)
    StringGrid: TStringGrid;
    Panel1: TPanel;
    PanelRodape: TPanel;
    lbNomeTela: TLabel;
    ilProdutos: TImageList;
    pnl6: TPanel;
    lbmensagem: TLabel;
    bt2: TSpeedButton;
    btCancelar: TSpeedButton;
    pnl2: TPanel;
    Img1: TImage;
    btAnterior: TSpeedButton;
    btProximo: TSpeedButton;
    lbRodape: TLabel;
    procedure btcancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtokClick(Sender: TObject);
    procedure StringGridDblClick(Sender: TObject);
    procedure StringGridKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure lbOKMouseLeave(Sender: TObject);
    procedure lbOKMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure StringGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure Configuracoesiniciais;
  end;

var
  FmostraStringGrid: TFmostraStringGrid;

implementation

uses UescolheImagemBotao;

{$R *.dfm}

procedure TFmostraStringGrid.btcancelarClick(Sender: TObject);
begin
     Self.Tag:=0;
     Self.Close;
end;

procedure TFmostraStringGrid.FormShow(Sender: TObject);
begin
     Self.Tag:=0;
     Self.StringGrid.SetFocus;
     FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');

end;

procedure TFmostraStringGrid.BtokClick(Sender: TObject);
begin
     Self.Tag:=1;
     Self.Close;
end;

procedure TFmostraStringGrid.StringGridDblClick(Sender: TObject);
begin
     BtokClick(sender);
end;

procedure TFmostraStringGrid.StringGridKeyPress(Sender: TObject;
  var Key: Char);
begin
     if (key=#13)
     Then BtokClick(sender);
end;

procedure TFmostraStringGrid.Configuracoesiniciais;
begin
     Self.StringGrid.ColCount:=1;
     Self.StringGrid.RowCount:=2;
     Self.StringGrid.FixedCols:=0;
     Self.StringGrid.FixedRows:=1;
     Self.StringGrid.Cols[0].clear;
     Self.caption:='Escolha uma Op��o';
end;

procedure TFmostraStringGrid.FormCreate(Sender: TObject);
begin
     Self.panelrodape.visible:=False;
end;

procedure TFmostraStringGrid.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Self.panelrodape.visible:=False;
end;

procedure TFmostraStringGrid.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if (key=#27)
     then Self.btcancelarClick(sender);

end;

procedure TFmostraStringGrid.lbOKMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFmostraStringGrid.lbOKMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
        TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFmostraStringGrid.StringGridDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
    //pinta o checkbox na stringgrid
    if((ARow <> 0) and (ACol = 0))
    then begin
        if(StringGrid.Cells[ACol,ARow] = '       X' ) or (StringGrid.Cells[ACol,ARow] = 'X' )then
        begin
             Ilprodutos.Draw(StringGrid.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 6); //check box marcado       6
        end
        else Ilprodutos.Draw(StringGrid.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 5); //check box sem marcar    5
    end;

end;

end.
