unit UObjNotaFiscal;
Interface
Uses uobjcliente,uobjFornecedor,windows,StdCtrls,Classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,uobjtransportadora,uobjoperacaonf,uobjnfe;

Type
   TObjNotaFiscal=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Cliente:Tobjcliente;
                Fornecedor:tobjfornecedor;
                Transportadora:tobjtransportadora;
                Operacao:tobjoperacaonf;
                Nfe:TobjNfe;


                Constructor Create;
                Destructor  Free;
                function    Get_NovoLote: string;
                Function    Salvar(ComCommit:Boolean):Boolean;

                Function    LocalizaCodigo(Parametro:string) :boolean;
                function     LocalizaCodigo_por_NFE(parametro: String): boolean;


                Function    ExisteNota(pNumero,pModelo:string):boolean;
                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;overload;
                Function    Get_Pesquisa(PSituacao:string):TStringList;overload;
                Function    Get_TituloPesquisa              :string;

                Function    TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Procedure Submit_CODIGO           (parametro:string);
                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                function Get_Numero: string;
                function Get_Situacao: string;
                function get_nfecomplementar: string;
                function get_nfentrada: string;
                function Get_TIPO: string;
                procedure Submit_Numero(parametro: string);
                procedure Submit_Situacao(parametro: string);
                procedure submit_nfecomplementar(parametro:string);
                procedure submit_nfentrada(parametro:string);
                procedure Submit_TIPO(parametro: string);
                Procedure Submit_NumeroFinal(parametro:string);
                Function  VerificaPermissao:boolean;
                Function  RasuraNotaFiscal(PNumero:string;ComCommit:Boolean): Boolean;
                Function  PegaPrimeiraNotaLivre:string;

                function Get_NumPedido:string;
                procedure Submit_NumPedido(parametro:string);
                procedure Submit_ModeloNF(parametro:string);
                procedure submit_NUMNFEDIGITADA(parametro:string);
                function get_NUMNFEDIGITADA():string;
                function Get_ModeloNF:string;

                Function Get_VALORTOTAL                      :string;
                Function Get_DESCONTO                   :string;
                Function Get_VALORFINAL                      :string;
                Function Get_NATUREZAOPERACAO                :string;
                Function Get_dataemissao                     :string;
                Function Get_BASECALCULOICMS                 :string;
                Function Get_VALORICMS                       :string;
                Function Get_BASECALCULOICMS_SUBSTITUICAO    :string;
                Function Get_VALORICMS_SUBSTITUICAO          :string;
                Function Get_VALORFRETE                      :string;
                Function Get_valorpis                      :string;
                Function Get_valorpis_st                      :string;

                Function Get_valorcofins                      :string;
                Function Get_valorcofins_st                      :string;

                Function Get_VALORSEGURO                     :string;
                Function Get_OUTRASDESPESAS                  :string;
                Function Get_VALORTOTALIPI                   :string;
                Function Get_NomeTransportadora              :string;
                Function Get_FreteporContaTransportadora     :string;
                Function Get_PlacaVeiculoTransportadora      :string;
                Function Get_UFVeiculoTransportadora         :string;
                Function Get_CNPJTransportadora              :string;
                Function Get_EnderecoTransportadora          :string;
                Function Get_MunicipioTransportadora         :string;
                Function Get_UFTransportadora                :string;
                Function Get_IETransportadora                :string;
                Function Get_Quantidade                      :string;
                Function Get_Especie                         :string;
                Function Get_Marca                           :string;
                Function Get_NumeroVolumes                   :string;
                Function Get_PesoBruto                       :string;
                Function Get_PesoLiquido                     :string;
                Function Get_DadosAdicionais                 :String;
                Function Get_OBSERVACOES                     :String;
                Function Get_Lote:string;
                function get_serienf:string;

                Procedure Submit_VALORTOTAL                      (parametro:string);
                Procedure Submit_DESCONTO                        (parametro:string);
                Procedure Submit_NATUREZAOPERACAO                (parametro:string);
                Procedure Submit_dataemissao                       (parametro:string);
                Procedure Submit_BASECALCULOICMS                 (parametro:string);
                Procedure Submit_VALORICMS                       (parametro:string);
                Procedure Submit_BASECALCULOICMS_SUBSTITUICAO    (parametro:string);
                Procedure Submit_VALORICMS_SUBSTITUICAO          (parametro:string);
                Procedure Submit_VALORFRETE                      (parametro:string);
                Procedure Submit_valorpis                      (parametro:string);
                Procedure Submit_valorpis_st                      (parametro:string);

                Procedure Submit_valorcofins                      (parametro:string);
                Procedure Submit_valorcofins_st                      (parametro:string);

                Procedure Submit_VALORSEGURO                     (parametro:string);
                Procedure Submit_OUTRASDESPESAS                  (parametro:string);
                Procedure Submit_VALORTOTALIPI                   (parametro:string);
                Procedure Submit_NomeTransportadora              (parametro:string);
                Procedure Submit_FreteporContaTransportadora     (parametro:string);
                Procedure Submit_PlacaVeiculoTransportadora      (parametro:string);
                Procedure Submit_UFVeiculoTransportadora         (parametro:string);
                Procedure Submit_CNPJTransportadora              (parametro:string);
                Procedure Submit_EnderecoTransportadora          (parametro:string);
                Procedure Submit_MunicipioTransportadora         (parametro:string);
                Procedure Submit_UFTransportadora                (parametro:string);
                Procedure Submit_IETransportadora                (parametro:string);
                Procedure Submit_Quantidade                      (parametro:string);
                Procedure Submit_Especie                         (parametro:string);
                Procedure Submit_Marca                           (parametro:string);
                Procedure Submit_Numerovolumes                   (parametro:string);
                Procedure Submit_PesoBruto                       (parametro:string);
                Procedure Submit_PesoLiquido                     (parametro:string);
                Procedure Submit_DadosAdicionais                 (parametro:String);
                Procedure Submit_OBSERVACOES                     (parametro:String);

                Procedure Submit_Lote(parametro:string);
                procedure Submit_serienf(parametro: string);


                Function get_datasaida:string;
                Function get_Horasaida:string;
                function get_horaemissao:string;

                Procedure Submit_datasaida(parametro:string);
                Procedure Submit_Horasaida(parametro:string);
                procedure submit_horaemissao(parametro:string);

                function Get_BASECALCULOICMS_SUBST_recolhido: string;
                function Get_IcmsRecolhidoPelaEmpresa: string;
                function Get_VALORICMS_SUBST_recolhido: string;
                procedure Submit_BASECALCULOICMS_SUBST_recolhido(parametro: string);
                procedure  Submit_IcmsRecolhidoPelaEmpresa(parametro: string);
                procedure Submit_VALORICMS_SUBST_recolhido(parametro: string);

                Function Get_TabeladePreco:String;
                Procedure Submit_TabeladePreco(parametro:string);


                Function Get_vencimentofatura1:string;
                Function Get_vencimentofatura2:string;
                Function Get_vencimentofatura3:string;
                Function Get_vencimentofatura4:string;
                Function Get_vencimentofatura5:string;

                

                Function Get_valorfatura1:string;
                Function Get_valorfatura2:string;
                Function Get_valorfatura3:string;
                Function Get_valorfatura4:string;
                Function Get_valorfatura5:string;



                Procedure Submit_vencimentofatura1(parametro:string);
                Procedure Submit_vencimentofatura2(parametro:string);
                Procedure Submit_vencimentofatura3(parametro:string);
                Procedure Submit_vencimentofatura4(parametro:string);
                Procedure Submit_vencimentofatura5(parametro:string);

                Procedure Submit_valorfatura1(parametro:string);
                Procedure Submit_valorfatura2(parametro:string);
                Procedure Submit_valorfatura3(parametro:string);
                Procedure Submit_valorfatura4(parametro:string);
                Procedure Submit_valorfatura5(parametro:string);
                procedure Submit_indpag(parametro:string);
                Function Get_indpag:string;


                procedure EdtTransportadoraKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;Pnome:Tlabel);
                procedure EdtTransportadoraExit(Sender: TObject;Pnome:Tlabel);

                procedure EdtClienteKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;Pnome:Tlabel);
                procedure EdtClienteExit(Sender: TObject;Pnome:Tlabel);

                procedure EdtfornecedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;Pnome:Tlabel);
                procedure EdtfornecedorExit(Sender: TObject;Pnome:Tlabel);

                procedure EdtoperacaoExit(Sender: TObject; Pnome: Tlabel);
                procedure EdtoperacaoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; Pnome: Tlabel);overload;
                procedure EdtoperacaoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;

                function Get_DevolucaoCliente: string;
                procedure Submit_DevolucaoCliente(parametro: string);
                procedure EdtNFEExit(Sender: TObject; Pnome: Tlabel);
                procedure EdtNFEKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; Pnome: Tlabel);
                
         Private
               ObjDataset:Tibdataset;

               CODIGO                 :string;

               Numero                 :string;
               Situacao               :string;
               nfecomplementar        :string;
               nfentrada              :string;
               TIPO               :string;
               NUmeroFinal:string;//usado so no objeto
               Lote:string;
               serienf:string;


               datasaida:string;
               horasaida:string;
               horaemissao:string;


               VALORTOTAL:string;
               DESCONTO  :string;
               VALORFINAL:string;

               NATUREZAOPERACAO:string;
               dataemissao:string;
               BASECALCULOICMS:string;
               VALORICMS:string;
               BASECALCULOICMS_SUBSTITUICAO:string;
               VALORICMS_SUBSTITUICAO:string;
               VALORFRETE:string;
               valorpis:string;
               valorpis_st:string;

               valorcofins:string;
               valorcofins_st:string;

               VALORSEGURO:string;
               OUTRASDESPESAS:string;
               VALORTOTALIPI:string;
               NomeTransportadora:string;
               FreteporContaTransportadora:string;
               PlacaVeiculoTransportadora:string;
               UFVeiculoTransportadora:string;
               CNPJTransportadora:string;
               EnderecoTransportadora:string;
               MunicipioTransportadora:string;
               UFTransportadora:string;
               IETransportadora:string;
               Quantidade:string;
               Especie:string;
               Marca:string;
               NumeroVolumes:string;
               PesoBruto:string;
               PesoLiquido:string;
               DadosAdicionais:String;//Str300
               OBSERVACOES:String;//str500
               NumPedido:string;
               modeloNF:string;
               NUMNFEDIGITADA:STRING;
               indpag:string;

               BASECALCULOICMS_SUBST_recolhido: string;
               IcmsRecolhidoPelaEmpresa: string;
               VALORICMS_SUBST_recolhido: string;




               TabeladePreco:string;


               vencimentofatura1:string;
               vencimentofatura2:string;
               vencimentofatura3:string;
               vencimentofatura4:string;
               vencimentofatura5:string;

               valorfatura1:string;
               valorfatura2:string;
               valorfatura3:string;
               valorfatura4:string;
               valorfatura5:string;

               DevolucaoCliente:String;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Function ContaQuantNFLote(PcodigoNF:string):integer;


   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Ibquery,Controls, Upesquisa;



Function  TObjNotaFiscal.TabelaparaObjeto:Boolean;//ok
begin
     With ObjDataset do
     Begin
        result:=False;
        Self.ZerarTabela;

        if (Fieldbyname('transportadora').asstring<>'')
        Then Begin
                  if (Self.Transportadora.LocalizaCodigo(Fieldbyname('transportadora').asstring)=False)
                  Then Begin
                            Messagedlg('Transportadora n�o localizada',mterror,[mbok],0);
                            Self.ZerarTabela;
                            exit;
                  End;
                  Self.Transportadora.TabelaparaObjeto;
        End;


        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.Numero:=fieldbyname('Numero').asstring;
        Self.Situacao:=fieldbyname('Situacao').asstring;
        self.nfecomplementar:=fieldbyname('nfecomplementar').AsString;
        self.nfentrada:=fieldbyname('nfentrada').AsString;

        Self.TIPO:=fieldbyname('TIPO').asstring;
        Self.VALORTOTAL:=fieldbyname('VALORTOTAL').asstring;
        Self.DESCONTO:=fieldbyname('DESCONTO').asstring;
        self.NumPedido:=fieldbyname('NumPedido').AsString;
        self.modeloNF:=fieldbyname('modelo_nf').AsString;
        self.NUMNFEDIGITADA:=fieldbyname('NUMNFEDIGITADA').AsString;

        Self.VALORFINAL:=fieldbyname('VALORFINAL').asstring;
        Self.NATUREZAOPERACAO:=fieldbyname('NATUREZAOPERACAO').asstring;

        Self.dataemissao:=fieldbyname('dataemissao').asstring;
        Self.BASECALCULOICMS:=fieldbyname('BASECALCULOICMS').asstring;
        Self.VALORICMS:=fieldbyname('VALORICMS').asstring;
        Self.BASECALCULOICMS_SUBSTITUICAO:=fieldbyname('BASECALCULOICMS_SUBSTITUICAO').asstring;
        Self.VALORICMS_SUBSTITUICAO:=fieldbyname('VALORICMS_SUBSTITUICAO').asstring;
        Self.VALORFRETE:=fieldbyname('VALORFRETE').asstring;
        Self.valorpis:=fieldbyname('valorpis').asstring;
        Self.valorpis_st:=fieldbyname('valorpis_st').asstring;

        Self.valorcofins:=fieldbyname('valorcofins').asstring;
        Self.valorcofins_st:=fieldbyname('valorcofins_st').asstring;
        Self.VALORSEGURO:=fieldbyname('VALORSEGURO').asstring;
        Self.OUTRASDESPESAS:=fieldbyname('OUTRASDESPESAS').asstring;
        Self.VALORTOTALIPI:=fieldbyname('VALORTOTALIPI').asstring;
        Self.NomeTransportadora:=fieldbyname('NomeTransportadora').asstring;
        Self.FreteporContaTransportadora:=fieldbyname('FreteporContaTransportadora').asstring;
        Self.PlacaVeiculoTransportadora:=fieldbyname('PlacaVeiculoTransportadora').asstring;
        Self.UFVeiculoTransportadora:=fieldbyname('UFVeiculoTransportadora').asstring;
        Self.CNPJTransportadora:=fieldbyname('CNPJTransportadora').asstring;
        Self.EnderecoTransportadora:=fieldbyname('EnderecoTransportadora').asstring;
        Self.MunicipioTransportadora:=fieldbyname('MunicipioTransportadora').asstring;
        Self.UFTransportadora:=fieldbyname('UFTransportadora').asstring;
        Self.IETransportadora:=fieldbyname('IETransportadora').asstring;
        Self.Quantidade:=fieldbyname('Quantidade').asstring;
        Self.Especie:=fieldbyname('Especie').asstring;
        Self.Marca:=fieldbyname('Marca').asstring;
        Self.numeroVolumes:=fieldbyname('numeroVolumes').asstring;
        Self.PesoBruto:=fieldbyname('PesoBruto').asstring;
        Self.PesoLiquido:=fieldbyname('PesoLiquido').asstring;
        Self.DadosAdicionais:=fieldbyname('DadosAdicionais').asstring;
        Self.OBSERVACOES:=fieldbyname('OBSERVACOES').asstring;
        Self.TabeladePreco:=fieldbyname('TabeladePreco').asstring;

        Self.vencimentofatura1:=fieldbyname('vencimentofatura1').asstring;
        Self.vencimentofatura2:=fieldbyname('vencimentofatura2').asstring;
        Self.vencimentofatura3:=fieldbyname('vencimentofatura3').asstring;
        Self.vencimentofatura4:=fieldbyname('vencimentofatura4').asstring;
        Self.vencimentofatura5:=fieldbyname('vencimentofatura5').asstring;

        Self.valorfatura1:=fieldbyname('valorfatura1').asstring;
        Self.valorfatura2:=fieldbyname('valorfatura2').asstring;
        Self.valorfatura3:=fieldbyname('valorfatura3').asstring;
        Self.valorfatura4:=fieldbyname('valorfatura4').asstring;
        Self.valorfatura5:=fieldbyname('valorfatura5').asstring;

        Self.dataSaida:=fieldbyname('Datasaida').asstring;
        Self.HoraSaida:=fieldbyname('Horasaida').asstring;
        Self.horaemissao:=fieldbyname('horaemissao').asstring;
        Self.lote:=Fieldbyname('lote').asstring;
        Self.serienf:=Fieldbyname('serienf').asstring;
        self.indpag:=Fieldbyname('indpag').AsString;

        if (Fieldbyname('Cliente').asstring<>'')
        Then Begin
                  if (Self.Cliente.LocalizaCodigo(Fieldbyname('Cliente').asstring)=False)
                  Then Begin
                            Messagedlg('Cliente n�o localizado',mterror,[mbok],0);
                            Self.ZerarTabela;
                            exit;
                  End;
                  Self.Cliente.TabelaparaObjeto;
        End;

        if (Fieldbyname('NFE').asstring<>'')
        Then Begin
                  if (Self.NFE.LocalizaCodigo(Fieldbyname('NFE').asstring)=False)
                  Then Begin
                            Messagedlg('NFE n�o localizado',mterror,[mbok],0);
                            Self.ZerarTabela;
                            exit;
                  End;
                  Self.NFE.TabelaparaObjeto;
        End;


        if (Fieldbyname('Fornecedor').asstring<>'')
        Then Begin
                  if (Self.Fornecedor.LocalizaCodigo(Fieldbyname('Fornecedor').asstring)=False)
                  Then Begin
                            Messagedlg('Fornecedor n�o localizado',mterror,[mbok],0);
                            Self.ZerarTabela;
                            exit;
                  End;
                  Self.Fornecedor.TabelaparaObjeto;
        End;


        if (Fieldbyname('Operacao').asstring<>'')
        Then Begin
                  if (Self.Operacao.LocalizaCodigo(Fieldbyname('Operacao').asstring)=False)
                  Then Begin
                            Messagedlg('Opera��o n�o localizado',mterror,[mbok],0);
                            Self.ZerarTabela;
                            exit;
                  End;
                  Self.Operacao.TabelaparaObjeto;
        End;

        Self.BASECALCULOICMS_SUBST_recolhido:=Fieldbyname('BASECALCULOICMS_SUBST_recolhido').asstring;
        Self.VALORICMS_SUBST_recolhido:=Fieldbyname('VALORICMS_SUBST_recolhido').asstring;
        Self.IcmsRecolhidoPelaEmpresa:=Fieldbyname('IcmsRecolhidoPelaEmpresa').asstring;
        Self.DevolucaoCliente:=Fieldbyname('DevolucaoCliente').asstring;

        result:=True;
     End;
end;


Procedure TObjNotaFiscal.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin

        VALORTOTAL:=tira_ponto(VALORTOTAL);
       // VALORTOTAL:=virgulaparaponto(VALORTOTAL);

        fieldbyname('CODIGO').asstring:=Self.CODIGO;
        fieldbyname('Numero').asstring:=Self.Numero;
        fieldbyname('Situacao').asstring:=Self.Situacao;
        FieldByName('nfecomplementar').asstring:=self.nfecomplementar;
        FieldByName('nfentrada').asstring:=self.nfentrada;
        fieldbyname('TIPO').asstring:=Self.TIPO;
        FieldByName('VALORTOTAL').asstring:=Self.VALORTOTAL;
        FieldByName('DESCONTO').asstring:=Self.DESCONTO;
        FieldByName('NATUREZAOPERACAO').asstring:=Self.NATUREZAOPERACAO;
        FieldByName('dataemissao').asstring:=Self.dataemissao;
        FieldByName('BASECALCULOICMS').asstring:=Self.BASECALCULOICMS;
        FieldByName('VALORICMS').asstring:=Self.VALORICMS;
        FieldByName('BASECALCULOICMS_SUBSTITUICAO').asstring:=Self.BASECALCULOICMS_SUBSTITUICAO;
        FieldByName('VALORICMS_SUBSTITUICAO').asstring:=Self.VALORICMS_SUBSTITUICAO;
        FieldByName('VALORFRETE').asstring:=Self.VALORFRETE;
        FieldByName('valorpis').asstring:=Self.valorpis;
        FieldByName('valorpis_st').asstring:=Self.valorpis_st;
        FieldByName('NUMPEDIDO').AsString:=Self.NumPedido;

        FieldByName('valorcofins').asstring:=Self.valorcofins;
        FieldByName('valorcofins_st').asstring:=Self.valorcofins_st;
        FieldByName('VALORSEGURO').asstring:=Self.VALORSEGURO;
        FieldByName('OUTRASDESPESAS').asstring:=Self.OUTRASDESPESAS;
        FieldByName('VALORTOTALIPI').asstring:=Self.VALORTOTALIPI;
        FieldByName('NomeTransportadora').asstring:=Self.NomeTransportadora;
        FieldByName('FreteporContaTransportadora').asstring:=Self.FreteporContaTransportadora;
        FieldByName('PlacaVeiculoTransportadora').asstring:=Self.PlacaVeiculoTransportadora;
        FieldByName('UFVeiculoTransportadora').asstring:=Self.UFVeiculoTransportadora;
        FieldByName('CNPJTransportadora').asstring:=Self.CNPJTransportadora;
        FieldByName('EnderecoTransportadora').asstring:=Self.EnderecoTransportadora;
        FieldByName('MunicipioTransportadora').asstring:=Self.MunicipioTransportadora;
        FieldByName('UFTransportadora').asstring:=Self.UFTransportadora;
        FieldByName('IETransportadora').asstring:=Self.IETransportadora;
        FieldByName('Quantidade').asstring:=Self.Quantidade;
        FieldByName('Especie').asstring:=Self.Especie;
        FieldByName('Marca').asstring:=Self.Marca;
        FieldByName('NumeroVolumes').asstring:=Self.NumeroVolumes;
        FieldByName('PesoBruto').asstring:=Self.PesoBruto;
        FieldByName('PesoLiquido').asstring:=Self.PesoLiquido;
        FieldByName('DadosAdicionais').asstring:=Self.DadosAdicionais;
        FieldByName('OBSERVACOES').asstring:=Self.OBSERVACOES;
        FieldByName('TabeladePreco').asstring:=Self.TabeladePreco;

        FieldByName('vencimentofatura1').asstring:=Self.vencimentofatura1;
        FieldByName('vencimentofatura2').asstring:=Self.vencimentofatura2;
        FieldByName('vencimentofatura3').asstring:=Self.vencimentofatura3;
        FieldByName('vencimentofatura4').asstring:=Self.vencimentofatura4;
        FieldByName('vencimentofatura5').asstring:=Self.vencimentofatura5;


        FieldByName('valorfatura1').asstring:=Self.valorfatura1;
        FieldByName('valorfatura2').asstring:=Self.valorfatura2;
        FieldByName('valorfatura3').asstring:=Self.valorfatura3;
        FieldByName('valorfatura4').asstring:=Self.valorfatura4;
        FieldByName('valorfatura5').asstring:=Self.valorfatura5;

        FieldByName('transportadora').asstring:=Self.transportadora.get_codigo;
        FieldByName('cliente').asstring:=Self.cliente.get_codigo;
        FieldByName('NFE').asstring:=Self.NFE.get_codigo;

        FieldByName('Fornecedor').asstring:=Self.Fornecedor.get_codigo;
        FieldByName('operacao').asstring:=Self.operacao.get_codigo;
        FieldByName('datasaida').asstring:=Self.datasaida;
        FieldByName('horasaida').asstring:=Self.horasaida;
        FieldByName('horaemissao').asstring:=Self.horaemissao;
        Fieldbyname('lote').asstring:=Self.lote;
        Fieldbyname('serienf').asstring:=Self.serienf;
        Fieldbyname('BASECALCULOICMS_SUBST_recolhido').asstring:=Self.BASECALCULOICMS_SUBST_recolhido;
        Fieldbyname('VALORICMS_SUBST_recolhido').asstring:=Self.VALORICMS_SUBST_recolhido;
        Fieldbyname('IcmsRecolhidoPelaEmpresa').asstring:=Self.IcmsRecolhidoPelaEmpresa;
        Fieldbyname('DevolucaoCliente').asstring:=Self.DevolucaoCliente;
        FieldByName('modelo_nf').AsString:=self.modeloNF;
        FieldByName('NUMNFEDIGITADA').AsString:=self.NUMNFEDIGITADA;
        Fieldbyname('indpag').AsString:=Self.indpag;
  End;
End;

//***********************************************************************

function TObjNotaFiscal.Salvar(ComCommit:Boolean): Boolean;//Ok
var
cont:Integer;
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
  End;

  For cont:=Strtoint(Self.NUmero) to Strtoint(self.NUmerofinal) do
  Begin
     Self.numero:=Inttostr(cont);

     if (Self.Status=dsinsert)
     Then Self.Codigo:=Self.Get_NovoCodigo;
     
     If Self.Status=dsinsert
     Then Begin
                If Self.ExisteNota(Self.Numero,self.modeloNF)=True
                Then Begin
                         Messagedlg('A Nota '+Self.Numero+' J� est� cadastrada ',mterror,[mbok],0);

                         If (ComCommit=True)
                         Then FDataModulo.IBTransaction.RollbackRetaining;

                         result:=False;
                         exit;
                End;
                If (Self.localizacodigo(Self.codigo)=True)
                Then Begin
                         Messagedlg('Este c�digo j� existe',mterror,[mbok],0);

                         If (ComCommit=True)
                         Then FDataModulo.IBTransaction.RollbackRetaining;

                         result:=False;
                         exit;
                End;
     End;

    if (Self.status = dsinsert) then
    begin
      Self.ObjDataset.Insert;
      FreteporContaTransportadora := '9'; //(sem frete)
      nfentrada:='N';
    end
    Else
    if (Self.Status=dsedit) Then
      Self.ObjDataset.edit//se for edicao libera para tal
    else
    Begin
      mensagemerro('Status Inv�lido na Grava��o');
      exit;
    End;


     Self.ObjetoParaTabela;
     Try
       Self.ObjDataset.Post;
     Except
           on e:exception do
           Begin
                 Messagedlg('Erro ao tentar salvar a Nota '+Self.Numero+#13+E.message,mterror,[mbok],0);
                 
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.RollbackRetaining;
                 result:=False;
                 exit;
           End;
     End;
 End;



 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjNotaFiscal.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO:='';
        Self.Numero:='';
        Self.Situacao:='';
        self.nfecomplementar:='';
        self.nfentrada:='';
        Self.TIPO:='';
        Self.NumeroFinal:='';
        VALORTOTAL:='';
        DESCONTO  :='';
        VALORFINAL:='';
        NATUREZAOPERACAO:='';
        dataemissao:='';
        BASECALCULOICMS:='';
        VALORICMS:='';
        BASECALCULOICMS_SUBSTITUICAO:='';
        VALORICMS_SUBSTITUICAO:='';
        VALORFRETE:='';
        valorpis:='';
        valorpis_st:='';
        numpedido:='';

        valorcofins:='';
        valorcofins_st:='';
        VALORSEGURO:='';
        OUTRASDESPESAS:='';
        VALORTOTALIPI:='';
        NomeTransportadora:='';
        FreteporContaTransportadora:='';
        PlacaVeiculoTransportadora:='';
        UFVeiculoTransportadora:='';
        CNPJTransportadora:='';
        EnderecoTransportadora:='';
        MunicipioTransportadora:='';
        UFTransportadora:='';
        IETransportadora:='';
        Quantidade:='';
        Especie:='';
        Marca:='';
        NumeroVolumes:='';
        PesoBruto:='';
        PesoLiquido:='';
        DadosAdicionais:='';
        OBSERVACOES:='';
        TabeladePreco:='';

        vencimentofatura1:='';
        vencimentofatura2:='';
        vencimentofatura3:='';
        vencimentofatura4:='';
        vencimentofatura5:='';

        valorfatura1:='';
        valorfatura2:='';
        valorfatura3:='';
        valorfatura4:='';
        valorfatura5:='';


        Self.Transportadora.ZerarTabela;
        datasaida:='';
        horasaida:='';
        horaemissao:='';
        cliente.zerartabela;
        Fornecedor.zerartabela;
        NFE.zerartabela;
        operacao.zerartabela;
        lote:='';
        self.serienf:='';
        BASECALCULOICMS_SUBST_recolhido:='';
        VALORICMS_SUBST_recolhido:='';
        IcmsRecolhidoPelaEmpresa:='';
        DevolucaoCliente:='';

        modeloNF:='';
        self.NUMNFEDIGITADA:='';
        indpag:='';
     End;
end;

Function TObjNotaFiscal.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       if (Lote='')
       Then Mensagem:=mensagem+'/Lote';

       if (serienf='')
       Then Mensagem:=mensagem+'/serie';

       if (BASECALCULOICMS_SUBST_recolhido='')
       Then BASECALCULOICMS_SUBST_recolhido:='0';

       if (VALORICMS_SUBST_recolhido='')
       Then VALORICMS_SUBST_recolhido:='0';

       if (IcmsRecolhidoPelaEmpresa='')
       Then IcmsRecolhidoPelaEmpresa:='N';

       if (DevolucaoCliente='')
       Then DevolucaoCliente:='N';

       if (trim(comebarra(vencimentofatura1))='')
       then vencimentofatura1:='';

       if (trim(comebarra(vencimentofatura2))='')
       then vencimentofatura2:='';

       if (trim(comebarra(vencimentofatura3))='')
       then vencimentofatura3:='';

       if (trim(comebarra(vencimentofatura4))='')
       then vencimentofatura4:='';

       if (trim(comebarra(vencimentofatura5))='')
       then vencimentofatura5:='';




  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
  End;
  result:=false;
end;


function TObjNotaFiscal.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';

     if (Self.Transportadora.Get_CODIGO<>'')
     then begin
               if (Self.Transportadora.LocalizaCodigo(Self.Transportadora.Get_CODIGO)=False)
               then Mensagem:=Mensagem+'\Transportadora n�o localizada';
     End;

     if (Self.Cliente.get_codigo<>'')
     Then Begin
              if (Self.Cliente.LocalizaCodigo(Self.cliente.get_codigo)=False)
              Then mensagem:=mensagem+'\Cliente n�o localizado';
     End;

     if (Self.Fornecedor.get_codigo<>'')
     Then Begin
              if (Self.Fornecedor.LocalizaCodigo(Self.Fornecedor.get_codigo)=False)
              Then mensagem:=mensagem+'\Fornecedor n�o localizado';
     End;

     if (Self.NFE.get_codigo<>'')
     Then Begin
              if (Self.NFE.LocalizaCodigo(Self.NFE.get_codigo)=False)
              Then mensagem:=mensagem+'\NFE n�o localizada';
     End;

     if (Self.operacao.get_codigo<>'')
     Then Begin
              if (Self.operacao.LocalizaCodigo(Self.operacao.get_codigo)=False)
              Then mensagem:=mensagem+'\Opera��o n�o localizado';
     End;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjNotaFiscal.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;


     try
        Inteiros:=Strtoint(Self.Numero);
     Except
           Mensagem:=mensagem+'/N�mero da NF';
     End;

     try
        Inteiros:=Strtoint(Self.Lote);
     Except
        Mensagem:=mensagem+'/Lote';
     End;

     try
        Inteiros:=Strtoint(Self.serienf);
     Except
        Mensagem:=mensagem+'/serienf';
     End;

     try
        if (Self.cliente.get_codigo<>'')
        then Inteiros:=Strtoint(Self.cliente.get_codigo);
     Except
           Mensagem:=mensagem+'\Cliente';
     End;

     try
        if (Self.Fornecedor.get_codigo<>'')
        then Inteiros:=Strtoint(Self.Fornecedor.get_codigo);
     Except
           Mensagem:=mensagem+'\Fornecedor';
     End;

     try
        if (Self.NFE.get_codigo<>'')
        then Inteiros:=Strtoint(Self.NFE.get_codigo);
     Except
           Mensagem:=mensagem+'\NFE';
     End;

     try
        if (Self.operacao.get_codigo<>'')
        then Inteiros:=Strtoint(Self.operacao.get_codigo);
     Except
           Mensagem:=mensagem+'\operacao';
     End;

     try
        if (self.Transportadora.Get_CODIGO<>'')
        Then Inteiros:=Strtoint(Self.Transportadora.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Transportadora';
     End;


     
     Try
        if (Self.valorfatura1<>'')
        Then strtocurr(Self.valorfatura1);
     Except
           Mensagem:=mensagem+'/Valor Fatura 1';
     End;

     Try
        if (Self.valorfatura2<>'')
        Then strtocurr(Self.valorfatura2);
     Except
           Mensagem:=mensagem+'/Valor Fatura 2';
     End;

          Try
        if (Self.valorfatura3<>'')
        Then strtocurr(Self.valorfatura3);
     Except
           Mensagem:=mensagem+'/Valor Fatura 3';
     End;


          Try
        if (Self.valorfatura4<>'')
        Then strtocurr(Self.valorfatura4);
     Except
           Mensagem:=mensagem+'/Valor Fatura 4';
     End;


          Try
        if (Self.valorfatura5<>'')
        Then strtocurr(Self.valorfatura5);
     Except
           Mensagem:=mensagem+'/Valor Fatura 5';
     End;


     If (Mensagem='')
     Then Begin
             try
                If (Self.NumeroFinal<>'')
                Then Begin
                        Inteiros:=Strtoint(Self.NumeroFinal);
                        If (strtoint(Self.NumeroFinal)<strtoint(Self.Numero))
                        Then Self.NumeroFinal:=Self.Numero;
                End
                Else Self.NumeroFinal:=Self.Numero; 
             Except
                   Self.NumeroFinal:=Self.Numero;
             End;
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjNotaFiscal.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

     Try
        if (comebarra(trim(Self.datasaida))<>'')
        Then Datas:=strtodate(Self.datasaida)
        Else Self.datasaida:='';
     Except
           Mensagem:=mensagem+'/Data de Saida';
     End;

     Try
        if (comebarra(trim(Self.dataemissao))<>'')
        Then Datas:=strtodate(Self.dataemissao)
        Else Self.dataemissao:='';
     Except
           Mensagem:=mensagem+'/Data de Emiss�o';
     End;


     Try
        if (Self.vencimentofatura1<>'')
        Then strtodate(Self.vencimentofatura1);
     Except
           Mensagem:=mensagem+'/Vencimento Fatura 1';
     End;


     Try
        if (Self.vencimentofatura2<>'')
        Then strtodate(Self.vencimentofatura2);
     Except
           Mensagem:=mensagem+'/Vencimento Fatura 2';
     End;


     Try
        if (Self.vencimentofatura3<>'')
        Then strtodate(Self.vencimentofatura3);
     Except
           Mensagem:=mensagem+'/Vencimento Fatura 3';
     End;


     Try
        if (Self.vencimentofatura4<>'')
        Then strtodate(Self.vencimentofatura4);
     Except
           Mensagem:=mensagem+'/Vencimento Fatura 4';
     End;


     Try
        if (Self.vencimentofatura5<>'')
        Then strtodate(Self.vencimentofatura5);
     Except
           Mensagem:=mensagem+'/Vencimento Fatura 5';
     End;

     Try
        if (comebarra(trim(Self.horasaida))<>'')
        Then Horas:=strtotime(Self.horasaida)
        Else Self.horasaida:='';
     Except
           Mensagem:=mensagem+'/Hora de Sa�da';
     End;

     Try
        if (comebarra(trim(Self.horaemissao))<>'')
        Then Horas:=strtotime(Self.horaemissao)
        Else Self.horaemissao:='';
     Except
           Mensagem:=mensagem+'/Hora de emissao';
     End;

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjNotaFiscal.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If ((Self.Situacao<>'I') and (Self.Situacao<>'R')and
        (Self.Situacao<>'N') and (Self.Situacao<>'C') AND
        (Self.Situacao<>'M') and (Self.Situacao<>'P') and (Self.Situacao<>'T'))
     Then Mensagem:=Mensagem+'\Situa��o Inv�lida!';


     If ((Self.TIPO<>'E') and (Self.TIPO<>'S'))
     Then Mensagem:=Mensagem+'\Tipo Inv�lido!';




     if ((Self.IcmsRecolhidoPelaEmpresa<>'S') and (Self.IcmsRecolhidoPelaEmpresa<>'N'))
     Then Self.IcmsRecolhidoPelaEmpresa:='N';

     if ((Self.DevolucaoCliente<>'S') and (Self.DevolucaoCliente<>'N'))
     Then Self.DevolucaoCliente:='N';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;
function TObjNotaFiscal.ExisteNota(pNumero,pModelo:string): boolean;
begin
       With Self.ObjDataset do
       Begin


           close;
           SelectSQL.Clear;
           SelectSQL.ADD('Select CODIGO,Numero,Nfe,lote,serienf,cliente,Fornecedor,operacao,Situacao,Tipo');
           SelectSQL.ADD(' ,VALORTOTAL,DESCONTO,VALORFINAL');
           SelectSQL.ADD(' ,NATUREZAOPERACAO,dataemissao,datasaida,horasaida,horaemissao,BASECALCULOICMS,VALORICMS,BASECALCULOICMS_SUBSTITUICAO');
           SelectSQL.ADD(' ,VALORICMS_SUBSTITUICAO,VALORFRETE,VALORSEGURO,OUTRASDESPESAS');
           SelectSQL.ADD(' ,VALORTOTALIPI,VALORPIS,VALORPIS_ST,valorcofins,valorcofins_ST,NomeTransportadora,FreteporContaTransportadora');
           SelectSQL.ADD(' ,PlacaVeiculoTransportadora,UFVeiculoTransportadora,CNPJTransportadora');
           SelectSQL.ADD(' ,EnderecoTransportadora,MunicipioTransportadora,UFTransportadora');
           SelectSQL.ADD(' ,IETransportadora,Quantidade,Especie,Marca,NumeroVolumes,PesoBruto,PesoLiquido');
           SelectSQL.ADD(' ,DadosAdicionais,OBSERVACOES,transportadora,BASECALCULOICMS_SUBST_recolhido,VALORICMS_SUBST_recolhido,IcmsRecolhidoPelaEmpresa,DevolucaoCliente,');
           SelectSQL.ADD(' tabeladepreco,vencimentofatura1,vencimentofatura2,vencimentofatura3,vencimentofatura4,vencimentofatura5,valorfatura1,valorfatura2,valorfatura3,valorfatura4,valorfatura5,numpedido,modelo_nf,NUMNFEDIGITADA,indpag,nfecomplementar,nfentrada');


           SelectSQL.ADD(' from  TabNotaFiscal');
           SelectSQL.ADD(' WHERE Numero='+#39+pNumero+#39+' and modelo_nf = '+pModelo);

           Open;

           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjNotaFiscal.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select CODIGO,Numero,Nfe,cliente,Fornecedor,operacao,lote,serienf,Situacao,nfecomplementar,nfentrada,Tipo');
           SelectSQL.ADD(' ,VALORTOTAL,DESCONTO,VALORFINAL');
           SelectSQL.ADD(' ,NATUREZAOPERACAO,dataemissao,datasaida,horasaida,horaemissao,BASECALCULOICMS,VALORICMS,BASECALCULOICMS_SUBSTITUICAO');
           SelectSQL.ADD(' ,VALORICMS_SUBSTITUICAO,VALORFRETE,VALORSEGURO,OUTRASDESPESAS');
           SelectSQL.ADD(' ,VALORTOTALIPI,VALORPIS,VALORPIS_ST,valorcofins,valorcofins_ST,NomeTransportadora,FreteporContaTransportadora');
           SelectSQL.ADD(' ,PlacaVeiculoTransportadora,UFVeiculoTransportadora,CNPJTransportadora');
           SelectSQL.ADD(' ,EnderecoTransportadora,MunicipioTransportadora,UFTransportadora');
           SelectSQL.ADD(' ,IETransportadora,Quantidade,Especie,Marca,NumeroVolumes,PesoBruto,PesoLiquido');
           SelectSQL.ADD(' ,DadosAdicionais,OBSERVACOES,transportadora,BASECALCULOICMS_SUBST_recolhido,VALORICMS_SUBST_recolhido,');
           SelectSQL.ADD('IcmsRecolhidoPelaEmpresa,DevolucaoCliente,tabeladepreco,vencimentofatura1,vencimentofatura2,vencimentofatura3,vencimentofatura4,vencimentofatura5,valorfatura1,valorfatura2,valorfatura3,valorfatura4,');
           SelectSQL.Add('valorfatura5,numpedido,modelo_nf,NUMNFEDIGITADA,indpag');

           SelectSQL.ADD(' ');
           SelectSQL.ADD(' from  TabNotaFiscal');
           SelectSQL.ADD(' WHERE Codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


function TObjNotaFiscal.LocalizaCodigo_por_NFE(parametro: String): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select CODIGO,Numero,Nfe,cliente,Fornecedor,operacao,lote,serienf,Situacao,nfecomplementar,nfentrada,Tipo');
           SelectSQL.ADD(' ,VALORTOTAL,DESCONTO,VALORFINAL');
           SelectSQL.ADD(' ,NATUREZAOPERACAO,dataemissao,datasaida,horasaida,horaemissao,BASECALCULOICMS,VALORICMS,BASECALCULOICMS_SUBSTITUICAO');
           SelectSQL.ADD(' ,VALORICMS_SUBSTITUICAO,VALORFRETE,VALORSEGURO,OUTRASDESPESAS');
           SelectSQL.ADD(' ,VALORTOTALIPI,VALORPIS,VALORPIS_ST,valorcofins,valorcofins_ST,NomeTransportadora,FreteporContaTransportadora');
           SelectSQL.ADD(' ,PlacaVeiculoTransportadora,UFVeiculoTransportadora,CNPJTransportadora');
           SelectSQL.ADD(' ,EnderecoTransportadora,MunicipioTransportadora,UFTransportadora');
           SelectSQL.ADD(' ,IETransportadora,Quantidade,Especie,Marca,NumeroVolumes,PesoBruto,PesoLiquido');
           SelectSQL.ADD(' ,DadosAdicionais,OBSERVACOES,transportadora,BASECALCULOICMS_SUBST_recolhido,VALORICMS_SUBST_recolhido,');
           SelectSQL.ADD('IcmsRecolhidoPelaEmpresa,DevolucaoCliente,tabeladepreco,vencimentofatura1,vencimentofatura2,vencimentofatura3,vencimentofatura4,vencimentofatura5,valorfatura1,valorfatura2,valorfatura3,valorfatura4,valorfatura5');
           SelectSQL.ADD(',numpedido,modelo_nf,NUMNFEDIGITADA,indpag ');
           SelectSQL.ADD(' from  TabNotaFiscal');
           SelectSQL.ADD(' WHERE NFE='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjNotaFiscal.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjNotaFiscal.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     result:=False;
     Try
        if (Self.ContaQuantNFLote(Pcodigo)>1)
        Then Begin
                  Messagedlg('Existe mais de Uma NF com esse mesmo n� de Lote escolha excluir lote',mterror,[mbok],0);
                  exit;
        End;


        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
                 result:=True;
        End;
     Except
     End;
end;


constructor TObjNotaFiscal.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        Self.Transportadora:=Tobjtransportadora.create;
        Self.Cliente:=Tobjcliente.create;
        Self.Operacao:=tobjoperacaonf.create;
        Self.Fornecedor:=tobjfornecedor.create;
        Self.Nfe:=TobjNfe.create;
        Self.ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSql.clear;
                SelectSQL.ADD('Select CODIGO,Numero,Nfe,cliente,Fornecedor,operacao,lote,serienf,Situacao,nfecomplementar,nfentrada,Tipo');
                SelectSQL.ADD(' ,VALORTOTAL,DESCONTO,VALORFINAL');
                SelectSQL.ADD(' ,NATUREZAOPERACAO,dataemissao,datasaida,horasaida,horaemissao,BASECALCULOICMS,VALORICMS,BASECALCULOICMS_SUBSTITUICAO');
                SelectSQL.ADD(' ,VALORICMS_SUBSTITUICAO,VALORFRETE,VALORSEGURO,OUTRASDESPESAS');
                SelectSQL.ADD(' ,VALORTOTALIPI,NomeTransportadora,FreteporContaTransportadora');
                SelectSQL.ADD(' ,PlacaVeiculoTransportadora,UFVeiculoTransportadora,CNPJTransportadora');
                SelectSQL.ADD(' ,EnderecoTransportadora,MunicipioTransportadora,UFTransportadora');
                SelectSQL.ADD(' ,IETransportadora,Quantidade,Especie,Marca,NumeroVolumes,PesoBruto,PesoLiquido');
                SelectSQL.ADD(' ,DadosAdicionais,OBSERVACOES,transportadora,BASECALCULOICMS_SUBST_recolhido,VALORICMS_SUBST_recolhido,IcmsRecolhidoPelaEmpresa,DevolucaoCliente');
                SelectSQL.ADD(',tabeladepreco,vencimentofatura1,vencimentofatura2,vencimentofatura3,vencimentofatura4,vencimentofatura5,valorfatura1,valorfatura2,valorfatura3,valorfatura4,valorfatura5,VALORPIS,valorpis_st,valorcofins,');
                SelectSQL.Add('valorcofins_st, numpedido,modelo_nf,NUMNFEDIGITADA,indpag from  TabNotaFiscal');
                SelectSQL.ADD(' WHERE Codigo=:Codigo');

                InsertSql.clear;
                InsertSQL.add('Insert Into TabNotaFiscal(CODIGO,cliente,Fornecedor,operacao,Numero,Nfe,Lote,serienf,Situacao,nfecomplementar,nfentrada,Tipo');
                InsertSQL.add(' ,VALORTOTAL,DESCONTO');
                InsertSQL.add(' ,NATUREZAOPERACAO,dataemissao,datasaida,horasaida,horaemissao');
                InsertSQL.add(' ,BASECALCULOICMS,VALORICMS,BASECALCULOICMS_SUBSTITUICAO');
                InsertSQL.add(' ,VALORICMS_SUBSTITUICAO,VALORFRETE,VALORSEGURO,OUTRASDESPESAS');
                InsertSQL.add(' ,VALORTOTALIPI,NomeTransportadora,FreteporContaTransportadora');
                InsertSQL.add(' ,PlacaVeiculoTransportadora,UFVeiculoTransportadora');
                InsertSQL.add(' ,CNPJTransportadora,EnderecoTransportadora,MunicipioTransportadora');
                InsertSQL.add(' ,UFTransportadora,IETransportadora,Quantidade,Especie');
                InsertSQL.add(' ,Marca,NumeroVolumes,PesoBruto,PesoLiquido,DadosAdicionais');
                InsertSQL.add(' ,OBSERVACOES,transportadora,BASECALCULOICMS_SUBST_recolhido,VALORICMS_SUBST_recolhido,IcmsRecolhidoPelaEmpresa,DevolucaoCliente,tabeladepreco,');
                InsertSQL.add(' vencimentofatura1,vencimentofatura2,vencimentofatura3,vencimentofatura4,vencimentofatura5,valorfatura1,valorfatura2,valorfatura3,valorfatura4,valorfatura5,VALORPIS,valorpis_st,valorcofins,valorcofins_st,numpedido,modelo_nf,NUMNFEDIGITADA,indpag)');
                InsertSQL.add('values (:CODIGO,:cliente,:Fornecedor,:operacao,:Numero,:Nfe,:Lote,:serienf,:Situacao,:nfecomplementar,:nfentrada,:Tipo');
                InsertSQL.add(' ,:Valortotal,:DESCONTO');
                InsertSQL.add(' ,:NATUREZAOPERACAO,:dataemissao,:datasaida,:horasaida,:horaemissao');
                InsertSQL.add(' ,:BASECALCULOICMS,:VALORICMS,:BASECALCULOICMS_SUBSTITUICAO');
                InsertSQL.add(' ,:VALORICMS_SUBSTITUICAO,:VALORFRETE,:VALORSEGURO,:OUTRASDESPESAS');
                InsertSQL.add(' ,:VALORTOTALIPI,:NomeTransportadora,:FreteporContaTransportadora');
                InsertSQL.add(' ,:PlacaVeiculoTransportadora,:UFVeiculoTransportadora');
                InsertSQL.add(' ,:CNPJTransportadora,:EnderecoTransportadora,:MunicipioTransportadora');
                InsertSQL.add(' ,:UFTransportadora,:IETransportadora,:Quantidade,:Especie');
                InsertSQL.add(' ,:Marca,:Numerovolumes,:PesoBruto,:PesoLiquido,:DadosAdicionais');
                InsertSQL.add(' ,:OBSERVACOES,:transportadora,:BASECALCULOICMS_SUBST_recolhido,:VALORICMS_SUBST_recolhido,:IcmsRecolhidoPelaEmpresa,:DevolucaoCliente,:tabeladepreco,');
                InsertSQL.add(':vencimentofatura1,:vencimentofatura2,:vencimentofatura3,:vencimentofatura4,:vencimentofatura5,:valorfatura1,:valorfatura2,:valorfatura3,:valorfatura4,:valorfatura5,:VALORPIS,:valorpis_st,:valorcofins,:valorcofins_st,:numpedido,:modelo_nf,:NUMNFEDIGITADA');
                InsertSQL.Add(',:indpag)');




                ModifySQL.clear;
                ModifySQL.add('Update TabNotaFiscal set NFE=:NFe,CODIGO=:CODIGO,Numero=:Numero,lote=:Lote,serienf=:serienf');
                ModifySQL.add(',Situacao=:Situacao,nfecomplementar=:nfecomplementar,nfentrada=:nfentrada,Tipo=:Tipo');
                ModifySQL.add(',VALORTOTAL=:VALORTOTAL,DESCONTO=:DESCONTO');
                ModifySQL.add(',NATUREZAOPERACAO=:NATUREZAOPERACAO');
                ModifySQL.add(',dataemissao=:dataemissao,BASECALCULOICMS=:BASECALCULOICMS');
                ModifySQL.add(',VALORICMS=:VALORICMS,BASECALCULOICMS_SUBSTITUICAO=:BASECALCULOICMS_SUBSTITUICAO');
                ModifySQL.add(',VALORICMS_SUBSTITUICAO=:VALORICMS_SUBSTITUICAO,VALORFRETE=:VALORFRETE,VALORPIS=:VALORPIS,valorpis_st=:valorpis_st,valorcofins=:valorcofins,valorcofins_st=:valorcofins_st');
                ModifySQL.add(',VALORSEGURO=:VALORSEGURO,OUTRASDESPESAS=:OUTRASDESPESAS');
                ModifySQL.add(',VALORTOTALIPI=:VALORTOTALIPI,NomeTransportadora=:NomeTransportadora');
                ModifySQL.add(',FreteporContaTransportadora=:FreteporContaTransportadora');
                ModifySQL.add(',PlacaVeiculoTransportadora=:PlacaVeiculoTransportadora');
                ModifySQL.add(',UFVeiculoTransportadora=:UFVeiculoTransportadora,CNPJTransportadora=:CNPJTransportadora');
                ModifySQL.add(',EnderecoTransportadora=:EnderecoTransportadora,MunicipioTransportadora=:MunicipioTransportadora');
                ModifySQL.add(',UFTransportadora=:UFTransportadora,IETransportadora=:IETransportadora');
                ModifySQL.add(',Quantidade=:Quantidade,Especie=:Especie,Marca=:Marca');
                ModifySQL.add(',NumeroVolumes=:Numerovolumes,PesoBruto=:PesoBruto,PesoLiquido=:PesoLiquido');
                ModifySQL.add(',DadosAdicionais=:DadosAdicionais,OBSERVACOES=:OBSERVACOES,transportadora=:transportadora,cliente=:cliente,Fornecedor=:Fornecedor,operacao=:operacao,datasaida=:datasaida,horasaida=:horasaida,horaemissao=:horaemissao');
                ModifySQL.add(',BASECALCULOICMS_SUBST_recolhido=:BASECALCULOICMS_SUBST_recolhido,VALORICMS_SUBST_recolhido=:VALORICMS_SUBST_recolhido,IcmsRecolhidoPelaEmpresa=:IcmsRecolhidoPelaEmpresa,DevolucaoCliente=:DevolucaoCliente');
                ModifySQL.add(',tabeladepreco=:tabeladepreco,vencimentofatura1=:vencimentofatura1,vencimentofatura2=:vencimentofatura2,vencimentofatura3=:vencimentofatura3,vencimentofatura4=:vencimentofatura4,vencimentofatura5=:vencimentofatura5');
                ModifySQL.add(',valorfatura1=:valorfatura1,valorfatura2=:valorfatura2,valorfatura3=:valorfatura3,valorfatura4=:valorfatura4,valorfatura5=:valorfatura5, numpedido=:numpedido,modelo_nf=:modelo_nf,NUMNFEDIGITADA=:NUMNFEDIGITADA');
                ModifySQL.add(',indpag=:indpag where Codigo=:Codigo');



                DeleteSql.clear;
                DeleteSql.add('Delete from TabNotaFiscal where Codigo=:Codigo ');

                RefreshSQL.clear;
                RefreshSQL.ADD('Select CODIGO,cliente,Fornecedor,operacao,Numero,Nfe,lote,serienf,Situacao,nfecomplementar,nfentrada,Tipo');
                RefreshSQL.ADD(' ,VALORTOTAL,DESCONTO,VALORFINAL');
                RefreshSQL.ADD(' ,NATUREZAOPERACAO,dataemissao,datasaida,horasaida,horaemissao,BASECALCULOICMS,VALORICMS,BASECALCULOICMS_SUBSTITUICAO');
                RefreshSQL.ADD(' ,VALORICMS_SUBSTITUICAO,VALORFRETE,VALORSEGURO,OUTRASDESPESAS');
                RefreshSQL.ADD(' ,VALORTOTALIPI,NomeTransportadora,FreteporContaTransportadora');
                RefreshSQL.ADD(' ,PlacaVeiculoTransportadora,UFVeiculoTransportadora,CNPJTransportadora');
                RefreshSQL.ADD(' ,EnderecoTransportadora,MunicipioTransportadora,UFTransportadora');
                RefreshSQL.ADD(' ,IETransportadora,Quantidade,Especie,Marca,NumeroVolumes,PesoBruto,PesoLiquido');
                RefreshSQL.ADD(' ,DadosAdicionais,OBSERVACOES,transportadora,BASECALCULOICMS_SUBST_recolhido,VALORICMS_SUBST_recolhido,IcmsRecolhidoPelaEmpresa,DevolucaoCliente,tabeladepreco,');
                RefreshSQL.ADD('vencimentofatura1,vencimentofatura2,vencimentofatura3,vencimentofatura4,vencimentofatura5,valorfatura1,valorfatura2,valorfatura3,valorfatura4,valorfatura5,VALORPIS,valorpis_st,valorcofins,valorcofins_st,numpedido,modelo_nf,NUMNFEDIGITADA');
                RefreshSQL.ADD(' ,indpag from  TabNotaFiscal');
                RefreshSQL.ADD(' WHERE Codigo=0');
        End;    

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjNotaFiscal.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjNotaFiscal.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjNotaFiscal.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjNotaFiscal.Get_Pesquisa(PSituacao: string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabNotaFiscal where situacao='#39+Psituacao+#39);
     Result:=Self.ParametroPesquisa;
end;

function TObjNotaFiscal.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabNotaFiscal');
     Result:=Self.ParametroPesquisa;
end;

function TObjNotaFiscal.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de NotaFiscal ';
end;



{
function TObjNotaFiscal.Get_PesquisaTurma: string;
begin
     Result:=Self.CodTurma.get_pesquisa;
end;

function TObjNotaFiscal.Get_TituloPesquisaTurma: string;
begin
     Result:=Self.Codturma.get_titulopesquisa;
end;
}

function TObjNotaFiscal.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_NOTAFISCAL';
           
           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o NotaFiscal',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


Destructor TObjNotaFiscal.Free;
Begin
     Freeandnil(Self.ObjDataset);
     Freeandnil(Self.ParametroPesquisa);
     Self.Transportadora.free;
     Self.Cliente.free;
     Self.Fornecedor.free;
     Self.Operacao.Free;
     Self.Nfe.free;
End;

function TObjNotaFiscal.RetornaCampoCodigo: string;
begin
     result:='codigo';
end;

function TObjNotaFiscal.RetornaCampoNome: string;
begin
     result:='';
end;

procedure TObjNotaFiscal.Submit_Numero(parametro: string);
begin
        Self.Numero:=Parametro;
end;
function TObjNotaFiscal.Get_Numero: string;
begin
        Result:=Self.Numero;
end;
procedure TObjNotaFiscal.Submit_Situacao(parametro: string);
begin
        Self.Situacao:=Parametro;
end;
function TObjNotaFiscal.Get_Situacao: string;
begin
        Result:=Self.Situacao;
end;



procedure TObjNotaFiscal.Submit_TIPO(parametro: string);
begin
        Self.TIPO:=Parametro;
end;
function TObjNotaFiscal.Get_TIPO: string;
begin
        Result:=Self.TIPO;
end;


procedure TObjNotaFiscal.Submit_NumeroFinal(parametro: string);
begin
     Self.numerofinal:=Parametro;
end;


function TObjNotaFiscal.VerificaPermissao: boolean;
var
TmpNivelUsuario,TmpNIvelPermissao:integer;
begin
     result:=False;
     iF (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE NOTA FISCAL')=fALSE)
     Then exit
     Else result:=True;
END;

function TObjNotaFiscal.RasuraNotaFiscal(PNumero:string;ComCommit:Boolean): Boolean;
begin
     result:=False;
     If (Self.LocalizaCodigo(Pnumero)=False)
     Then Begin
                Messagedlg('Nota N�o encontrada!',mterror,[mbok],0);
                exit;
     End;
     Self.TabelaparaObjeto;
     Self.Status:=dsedit;
     Self.Submit_Situacao('R');//rasurada
     Result:=Self.Salvar(ComCommit);
end;


function TObjNotaFiscal.PegaPrimeiraNotaLivre: string;
begin
     With Self.Objdataset do
     Begin
          close;                                                      
          SelectSQL.clear;
          SelectSQL.add('Select min(codigo) as MINIMO from TabNotaFiscal where situacao=''N'' ');
          Open;
          result:=fieldbyname('minimo').asstring;
     End;


end;


procedure TObjNotaFiscal.Submit_VALORTOTAL(parametro: string);
begin
        Self.VALORTOTAL:=Parametro;
end;
function TObjNotaFiscal.Get_VALORTOTAL: string;
begin
        Result:=Self.VALORTOTAL;
end;
function TObjNotaFiscal.Get_VALORFINAL: string;
begin
        Result:=Self.VALORFINAL;
end;
procedure TObjNotaFiscal.Submit_NATUREZAOPERACAO(parametro: string);
begin
        Self.NATUREZAOPERACAO:=Parametro;
end;
function TObjNotaFiscal.Get_NATUREZAOPERACAO: string;
begin
        Result:=Self.NATUREZAOPERACAO;
end;
procedure TObjNotaFiscal.Submit_dataemissao(parametro: string);
begin
        Self.dataemissao:=Parametro;
end;
function TObjNotaFiscal.Get_dataemissao: string;
begin
        Result:=Self.dataemissao;
end;
procedure TObjNotaFiscal.Submit_BASECALCULOICMS(parametro: string);
begin
        Self.BASECALCULOICMS:=Parametro;
end;
function TObjNotaFiscal.Get_BASECALCULOICMS: string;
begin
        Result:=Self.BASECALCULOICMS;
end;
procedure TObjNotaFiscal.Submit_VALORICMS(parametro: string);
begin
        Self.VALORICMS:=Parametro;
end;
function TObjNotaFiscal.Get_VALORICMS: string;
begin
        Result:=Self.VALORICMS;
end;
procedure TObjNotaFiscal.Submit_BASECALCULOICMS_SUBSTITUICAO(parametro: string);
begin
        Self.BASECALCULOICMS_SUBSTITUICAO:=Parametro;
end;
function TObjNotaFiscal.Get_BASECALCULOICMS_SUBSTITUICAO: string;
begin
        Result:=Self.BASECALCULOICMS_SUBSTITUICAO;
end;
procedure TObjNotaFiscal.Submit_VALORICMS_SUBSTITUICAO(parametro: string);
begin
        Self.VALORICMS_SUBSTITUICAO:=Parametro;
end;
function TObjNotaFiscal.Get_VALORICMS_SUBSTITUICAO: string;
begin
        Result:=Self.VALORICMS_SUBSTITUICAO;
end;
procedure TObjNotaFiscal.Submit_VALORFRETE(parametro: string);
begin
        Self.VALORFRETE:=Parametro;
end;
function TObjNotaFiscal.Get_VALORFRETE: string;
begin
        Result:=Self.VALORFRETE;
end;





procedure TObjNotaFiscal.Submit_valorpis(parametro: string);
begin
        Self.valorpis:=Parametro;
end;
function TObjNotaFiscal.Get_valorpis: string;
begin
        Result:=Self.valorpis;
end;

procedure TObjNotaFiscal.Submit_valorpis_st(parametro: string);
begin
        Self.valorpis_st:=Parametro;
end;
function TObjNotaFiscal.Get_valorpis_st: string;
begin
        Result:=Self.valorpis_st;
end;

procedure TObjNotaFiscal.Submit_valorcofins(parametro: string);
begin
        Self.valorcofins:=Parametro;
end;
function TObjNotaFiscal.Get_valorcofins: string;
begin
        Result:=Self.valorcofins;
end;

procedure TObjNotaFiscal.Submit_valorcofins_st(parametro: string);
begin
        Self.valorcofins_st:=Parametro;
end;
function TObjNotaFiscal.Get_valorcofins_st: string;
begin
        Result:=Self.valorcofins_st;
end;


procedure TObjNotaFiscal.Submit_VALORSEGURO(parametro: string);
begin
        Self.VALORSEGURO:=Parametro;
end;
function TObjNotaFiscal.Get_VALORSEGURO: string;
begin
        Result:=Self.VALORSEGURO;
end;
procedure TObjNotaFiscal.Submit_OUTRASDESPESAS(parametro: string);
begin
        Self.OUTRASDESPESAS:=Parametro;
end;
function TObjNotaFiscal.Get_OUTRASDESPESAS: string;
begin
        Result:=Self.OUTRASDESPESAS;
end;
procedure TObjNotaFiscal.Submit_VALORTOTALIPI(parametro: string);
begin
        Self.VALORTOTALIPI:=Parametro;
end;
function TObjNotaFiscal.Get_VALORTOTALIPI: string;
begin
        Result:=Self.VALORTOTALIPI;
end;
procedure TObjNotaFiscal.Submit_NomeTransportadora(parametro: string);
begin
        Self.NomeTransportadora:=Parametro;
end;
function TObjNotaFiscal.Get_NomeTransportadora: string;
begin
        Result:=Self.NomeTransportadora;
end;
procedure TObjNotaFiscal.Submit_FreteporContaTransportadora(parametro: string);
begin
        Self.FreteporContaTransportadora:=Parametro;
end;
function TObjNotaFiscal.Get_FreteporContaTransportadora: string;
begin
        Result:=Self.FreteporContaTransportadora;
end;
procedure TObjNotaFiscal.Submit_PlacaVeiculoTransportadora(parametro: string);
begin
        Self.PlacaVeiculoTransportadora:=Parametro;
end;
function TObjNotaFiscal.Get_PlacaVeiculoTransportadora: string;
begin
        Result:=Self.PlacaVeiculoTransportadora;
end;
procedure TObjNotaFiscal.Submit_UFVeiculoTransportadora(parametro: string);
begin
        Self.UFVeiculoTransportadora:=Parametro;
end;
function TObjNotaFiscal.Get_UFVeiculoTransportadora: string;
begin
        Result:=Self.UFVeiculoTransportadora;
end;
procedure TObjNotaFiscal.Submit_CNPJTransportadora(parametro: string);
begin
        Self.CNPJTransportadora:=Parametro;
end;
function TObjNotaFiscal.Get_CNPJTransportadora: string;
begin
        Result:=Self.CNPJTransportadora;
end;
procedure TObjNotaFiscal.Submit_EnderecoTransportadora(parametro: string);
begin
        Self.EnderecoTransportadora:=Parametro;
end;
function TObjNotaFiscal.Get_EnderecoTransportadora: string;
begin
        Result:=Self.EnderecoTransportadora;
end;
procedure TObjNotaFiscal.Submit_MunicipioTransportadora(parametro: string);
begin
        Self.MunicipioTransportadora:=Parametro;
end;
function TObjNotaFiscal.Get_MunicipioTransportadora: string;
begin
        Result:=Self.MunicipioTransportadora;
end;
procedure TObjNotaFiscal.Submit_UFTransportadora(parametro: string);
begin
        Self.UFTransportadora:=Parametro;
end;
function TObjNotaFiscal.Get_UFTransportadora: string;
begin
        Result:=Self.UFTransportadora;
end;
procedure TObjNotaFiscal.Submit_IETransportadora(parametro: string);
begin
        Self.IETransportadora:=Parametro;
end;
function TObjNotaFiscal.Get_IETransportadora: string;
begin
        Result:=Self.IETransportadora;
end;
procedure TObjNotaFiscal.Submit_Quantidade(parametro: string);
begin
        Self.Quantidade:=Parametro;
end;
function TObjNotaFiscal.Get_Quantidade: string;
begin
        Result:=Self.Quantidade;
end;
procedure TObjNotaFiscal.Submit_Especie(parametro: string);
begin
        Self.Especie:=Parametro;
end;
function TObjNotaFiscal.Get_Especie: string;
begin
        Result:=Self.Especie;
end;
procedure TObjNotaFiscal.Submit_Marca(parametro: string);
begin
        Self.Marca:=Parametro;
end;
function TObjNotaFiscal.Get_Marca: string;
begin
        Result:=Self.Marca;
end;
procedure TObjNotaFiscal.Submit_Numerovolumes(parametro: string);
begin
        Self.NumeroVolumes:=Parametro;
end;
function TObjNotaFiscal.Get_Numerovolumes: string;
begin
        Result:=Self.NumeroVolumes;
end;
procedure TObjNotaFiscal.Submit_PesoBruto(parametro: string);
begin
        Self.PesoBruto:=Parametro;
end;
function TObjNotaFiscal.Get_PesoBruto: string;
begin
        Result:=Self.PesoBruto;
end;
procedure TObjNotaFiscal.Submit_PesoLiquido(parametro: string);
begin
        Self.PesoLiquido:=Parametro;
end;
function TObjNotaFiscal.Get_PesoLiquido: string;
begin
        Result:=Self.PesoLiquido;
end;
procedure TObjNotaFiscal.Submit_DadosAdicionais(parametro: String);
begin
        Self.DadosAdicionais:=Parametro;
end;
function TObjNotaFiscal.Get_DadosAdicionais: String;
begin
        Result:=Self.DadosAdicionais;
end;
procedure TObjNotaFiscal.Submit_OBSERVACOES(parametro: string);
begin
        Self.OBSERVACOES:=Parametro;
end;
function TObjNotaFiscal.Get_OBSERVACOES: String;
begin
        Result:=Self.OBSERVACOES;
end;


procedure TObjNotaFiscal.Submit_TabeladePreco(parametro: string);
begin
        Self.TabeladePreco:=Parametro;
end;
function TObjNotaFiscal.Get_TabeladePreco: String;
begin
        Result:=Self.TabeladePreco;
end;



procedure TObjNotaFiscal.Submit_DESCONTO(parametro: string);
begin
     Self.Desconto:=Parametro;
end;

function TObjNotaFiscal.Get_DESCONTO: string;
begin
      RESULT:=Self.Desconto;
end;

procedure TObjNotaFiscal.EdtTransportadoraKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState;Pnome:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Transportadora.get_pesquisa,Self.Transportadora.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                  Pnome.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;

procedure TObjNotaFiscal.EdtTransportadoraExit(Sender: TObject;
  Pnome: Tlabel);
begin
     Pnome.caption:='';
     
     if (Tedit(Sender).text='')
     Then exit;

     if (Self.Transportadora.LocalizaCodigo(Tedit(sender).text)=False)
     Then Begin
               Tedit(sender).text:='';
               exit;
     End;
     Self.Transportadora.TabelaparaObjeto;
     pnome.caption:=Self.Transportadora.Get_NOME;

end;


function TObjNotaFiscal.get_datasaida: string;
begin
     Result:=Self.datasaida;
end;

function TObjNotaFiscal.get_Horasaida: string;
begin
     Result:=Self.horasaida;
end;

procedure TObjNotaFiscal.Submit_datasaida(parametro: string);
begin
     Self.datasaida:=parametro;
end;

procedure TObjNotaFiscal.Submit_Horasaida(parametro: string);
begin
     Self.horasaida:=parametro;
end;

procedure TObjNotaFiscal.EdtClienteExit(Sender: TObject; Pnome: Tlabel);
begin
     Pnome.caption:='';

     if (Tedit(Sender).text='')
     Then exit;

     if (Self.Cliente.LocalizaCodigo(Tedit(sender).text)=False)
     Then Begin
               Tedit(sender).text:='';
               exit;
     End;
     Self.Cliente.TabelaparaObjeto;
     pnome.caption:=Self.Cliente.Get_NOME;

end;

procedure TObjNotaFiscal.EdtClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState; Pnome: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabcliente where ativo=''S'' ',Self.Cliente.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                  Pnome.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;
//*********

procedure TObjNotaFiscal.EdtFornecedorExit(Sender: TObject; Pnome: Tlabel);
begin
     Pnome.caption:='';

     if (Tedit(Sender).text='')
     Then exit;

     if (Self.Fornecedor.LocalizaCodigo(Tedit(sender).text)=False)
     Then Begin
               Tedit(sender).text:='';
               exit;
     End;
     Self.Fornecedor.TabelaparaObjeto;
     pnome.caption:=Self.Fornecedor.Get_RazaoSocial;

end;


procedure TObjNotaFiscal.EdtNFEExit(Sender: TObject; Pnome: Tlabel);
begin
     Pnome.caption:='';

     if (Tedit(Sender).text='')
     Then exit;

     if (Self.NFE.LocalizaCodigo(Tedit(sender).text)=False)
     Then Begin
               Tedit(sender).text:='';
               exit;
     End;
     Self.NFE.TabelaparaObjeto;

end;

procedure TObjNotaFiscal.EdtFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState; Pnome: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Fornecedor.get_pesquisa,Self.Fornecedor.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                  Pnome.caption:=FpesquisaLocal.QueryPesq.fieldbyname('razaosocial').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;

procedure TObjNotaFiscal.EdtNFEKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState; Pnome: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.NFE.get_pesquisa,Self.NFE.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                  Pnome.caption:=FpesquisaLocal.QueryPesq.fieldbyname('razaosocial').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;


procedure TObjNotaFiscal.EdtoperacaoExit(Sender: TObject; Pnome: Tlabel);
begin
     Pnome.caption:='';

     if (Tedit(Sender).text='')
     Then exit;

     if (Self.operacao.LocalizaCodigo(Tedit(sender).text)=False)
     Then Begin
               Tedit(sender).text:='';
               exit;
     End;
     Self.operacao.TabelaparaObjeto;
     pnome.caption:=Self.operacao.Get_NOME;

end;

procedure TObjNotaFiscal.EdtoperacaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Begin
     Self.EdtoperacaoKeyDown(Sender,Key,Shift,nil);
End;

procedure TObjNotaFiscal.EdtoperacaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState; Pnome: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.operacao.get_pesquisa,Self.operacao.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                  Pnome.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;

function TObjNotaFiscal.Get_Lote: string;
begin
     Result:=Self.Lote;
end;

procedure TObjNotaFiscal.Submit_Lote(parametro: string);
begin
     Self.Lote:=Parametro;
end;

procedure TObjNotaFiscal.Submit_serienf(parametro: string);
begin
     Self.serienf:=Parametro;
end;

function TObjNotaFiscal.Get_NovoLote: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENLOTENOTAFISCAL,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


function TObjNotaFiscal.ContaQuantNFLote(PcodigoNF: string): integer;
begin
     Result:=0;

     if (Self.LocalizaCodigo(PcodigoNF)=False)
     Then Begin
               MEssagedlg('NF n�o localizada',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;

     if (Self.Lote='0')
     Then exit;

     With Self.ObjDataset do
     BEgin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select count(Codigo) as CONTA from TabNotaFiscal where Lote='+Self.Lote);
          open;
          Result:=Fieldbyname('conta').asinteger;
     End;
end;


function TObjNotaFiscal.Get_BASECALCULOICMS_SUBST_recolhido: string;
begin
     Result:=Self.BASECALCULOICMS_SUBST_recolhido;
end;

function TObjNotaFiscal.Get_VALORICMS_SUBST_recolhido: string;
begin
     Result:=Self.VALORICMS_SUBST_recolhido;
end;

procedure TObjNotaFiscal.Submit_BASECALCULOICMS_SUBST_recolhido(
  parametro: string);
begin
     Self.BASECALCULOICMS_SUBST_recolhido:=parametro;
end;

procedure TObjNotaFiscal.Submit_VALORICMS_SUBST_recolhido(
  parametro: string);
begin
     Self.VALORICMS_SUBST_recolhido:=parametro;
end;


function TObjNotaFiscal.Get_IcmsRecolhidoPelaEmpresa: string;
begin
     Result:=Self.IcmsRecolhidoPelaEmpresa;
end;

procedure TObjNotaFiscal.Submit_IcmsRecolhidoPelaEmpresa(
  parametro: string);
begin
     Self.IcmsRecolhidoPelaEmpresa:=parametro;
end;


function TObjNotaFiscal.Get_DevolucaoCliente: string;
begin
     Result:=Self.DevolucaoCliente;
end;

procedure TObjNotaFiscal.Submit_DevolucaoCliente(
  parametro: string);
begin
     Self.DevolucaoCliente:=parametro;
end;


function TObjNotaFiscal.Get_valorfatura1: string;
begin
     Result:=Self.valorfatura1;
end;

function TObjNotaFiscal.Get_valorfatura2: string;
begin
     Result:=Self.valorfatura2;
end;

function TObjNotaFiscal.Get_valorfatura3: string;
begin
     Result:=Self.valorfatura3;
end;

function TObjNotaFiscal.Get_valorfatura4: string;
begin
     Result:=Self.valorfatura4;
end;

function TObjNotaFiscal.Get_valorfatura5: string;
begin
     Result:=Self.valorfatura5;
end;

function TObjNotaFiscal.Get_vencimentofatura1: string;
begin
     Result:=Self.vencimentofatura1;
end;

function TObjNotaFiscal.Get_vencimentofatura2: string;
begin
     Result:=Self.vencimentofatura2;
end;

function TObjNotaFiscal.Get_vencimentofatura3: string;
begin
     Result:=Self.vencimentofatura3;
end;

function TObjNotaFiscal.Get_vencimentofatura4: string;
begin
     Result:=Self.vencimentofatura4;
end;

function TObjNotaFiscal.Get_vencimentofatura5: string;
begin
     Result:=Self.vencimentofatura5;
end;

procedure TObjNotaFiscal.Submit_valorfatura1(parametro: string);
begin
     Self.valorfatura1:=parametro;
end;

procedure TObjNotaFiscal.Submit_valorfatura2(parametro: string);
begin
     Self.valorfatura2:=parametro;
end;

procedure TObjNotaFiscal.Submit_valorfatura3(parametro: string);
begin
     Self.valorfatura3:=parametro;
end;

procedure TObjNotaFiscal.Submit_valorfatura4(parametro: string);
begin
     Self.valorfatura4:=parametro;
end;

procedure TObjNotaFiscal.Submit_valorfatura5(parametro: string);
begin
     Self.valorfatura5:=parametro;
end;

procedure TObjNotaFiscal.Submit_vencimentofatura1(parametro: string);
begin
     Self.vencimentofatura1:=parametro;
end;

procedure TObjNotaFiscal.Submit_vencimentofatura2(parametro: string);
begin
     Self.vencimentofatura2:=parametro;
end;

procedure TObjNotaFiscal.Submit_vencimentofatura3(parametro: string);
begin
     Self.vencimentofatura3:=parametro;
end;

procedure TObjNotaFiscal.Submit_vencimentofatura4(parametro: string);
begin
     Self.vencimentofatura4:=parametro;
end;

procedure TObjNotaFiscal.Submit_vencimentofatura5(parametro: string);
begin
     Self.vencimentofatura5:=parametro;
end;

procedure TObjNotaFiscal.Submit_NumPedido(parametro:string);
begin
    self.NumPedido:=parametro;
end;

function TObjNotaFiscal.Get_NumPedido:string;
begin
    Result:=self.NumPedido;
end;


function TObjNotaFiscal.Get_ModeloNF:string;
begin
    Result:=self.modeloNF;
end;

procedure TObjNotaFiscal.Submit_ModeloNF(parametro:String);
begin
    self.modeloNF:=parametro;
end;


function TObjNotaFiscal.get_NUMNFEDIGITADA: string;
begin

  Result := self.NUMNFEDIGITADA;

end;

procedure TObjNotaFiscal.submit_NUMNFEDIGITADA(parametro: string);
begin

  self.NUMNFEDIGITADA := parametro;

end;

procedure TObjNotaFiscal.Submit_indpag(parametro:string);
begin
  Self.indpag:=parametro;
end;

function TObjNotaFiscal.Get_indpag:string;
begin
  Result:=Self.indpag;
end;


procedure TObjNotaFiscal.submit_nfecomplementar(parametro: string);
begin
  self.nfecomplementar := parametro;
end;

function TObjNotaFiscal.get_nfecomplementar: string;
begin
  Result := nfecomplementar;
end;

function TObjNotaFiscal.get_serienf: string;
begin
  Result := self.serienf;
end;

function TObjNotaFiscal.get_horaemissao: string;
begin
  result := self.horaemissao;
end;

procedure TObjNotaFiscal.submit_horaemissao(parametro: string);
begin
  self.horaemissao := parametro;
end;

procedure TObjNotaFiscal.submit_nfentrada(parametro: string);
begin
  self.nfentrada:=parametro;
end;

function TObjNotaFiscal.get_nfentrada: string;
begin
  Result := self.nfentrada;
end;

end.



