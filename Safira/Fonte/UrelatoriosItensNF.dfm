object FrelatoriosItensNF: TFrelatoriosItensNF
  Left = 189
  Top = 181
  Width = 385
  Height = 124
  Caption = 'Relat'#243'rio de Itens em NF'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 7
    Top = 5
    Width = 37
    Height = 13
    Caption = 'Material'
  end
  object BtImprimir: TBitBtn
    Left = 162
    Top = 5
    Width = 115
    Height = 41
    Caption = 'Imprimir'
    TabOrder = 0
    OnClick = BtImprimirClick
  end
  object ComboMaterial: TComboBox
    Left = 4
    Top = 19
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    Text = '   '
    Items.Strings = (
      'Diversos'
      'Ferragem'
      'KitBox'
      'Perfilado'
      'Persiana'
      'Vidro')
  end
end
