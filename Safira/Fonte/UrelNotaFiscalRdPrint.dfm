object FrelNotaFiscalRdPrint: TFrelNotaFiscalRdPrint
  Left = 591
  Top = 116
  Width = 812
  Height = 580
  Caption = 'FrelNotaFiscalRdPrint'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  WindowState = wsMaximized
  OnDragDrop = FormDragDrop
  OnDragOver = FormDragOver
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbNaturezaOperacao: TLabel
    Left = 10
    Top = 25
    Width = 89
    Height = 12
    Caption = 'NATUREZA.OPER.'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbcfop: TLabel
    Left = 190
    Top = 25
    Width = 27
    Height = 12
    Caption = 'CFOP'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbInscrEstSubstTribut: TLabel
    Left = 340
    Top = 25
    Width = 87
    Height = 12
    Caption = 'IE.SUBST.TRIBUT'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbDataEmissao: TLabel
    Left = 680
    Top = 70
    Width = 63
    Height = 12
    Caption = 'DT EMISS'#195'O'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbFonefaxDestRemete: TLabel
    Left = 190
    Top = 135
    Width = 63
    Height = 12
    Caption = 'FONE.REMET'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbNomeDestinatRemete: TLabel
    Left = 10
    Top = 70
    Width = 65
    Height = 12
    Caption = 'NOME.REMET'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbCnpjDestRemete: TLabel
    Left = 190
    Top = 70
    Width = 61
    Height = 12
    Caption = 'CNPJ.REMET'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbUfDestRemete: TLabel
    Left = 340
    Top = 135
    Width = 49
    Height = 12
    Caption = 'UF.REMET'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbEnderecoDestRemete: TLabel
    Left = 10
    Top = 100
    Width = 89
    Height = 12
    Caption = 'ENDERECO.REMET'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbBairroDestRemete: TLabel
    Left = 190
    Top = 100
    Width = 78
    Height = 12
    Caption = 'BAIRRO.REMET'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbCepDestRemete: TLabel
    Left = 340
    Top = 100
    Width = 55
    Height = 12
    Caption = 'CEP.REMET'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbMunicipioDestRemete: TLabel
    Left = 10
    Top = 135
    Width = 94
    Height = 12
    Caption = 'MUNICIPIO.REMET'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbIeDestRemete: TLabel
    Left = 500
    Top = 135
    Width = 47
    Height = 12
    Caption = 'IE.REMET'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbDataSaida: TLabel
    Left = 680
    Top = 80
    Width = 50
    Height = 12
    Caption = 'DT SA'#205'DA'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbHorasaida: TLabel
    Left = 680
    Top = 90
    Width = 67
    Height = 12
    Caption = 'HORA SA'#205'DA'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbcontrolesup: TLabel
    Left = 690
    Top = 5
    Width = 67
    Height = 12
    Caption = 'N'#186' NF.SUPER.'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbsaidaentrada: TLabel
    Left = 500
    Top = 5
    Width = 7
    Height = 12
    Caption = 'X'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbbasecalculo: TLabel
    Left = 10
    Top = 290
    Width = 47
    Height = 12
    Caption = 'BASECAL'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbValoricms: TLabel
    Left = 190
    Top = 290
    Width = 41
    Height = 12
    Caption = 'VL.ICMS'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbBaseCalculoICMSSubstituicao: TLabel
    Left = 340
    Top = 290
    Width = 55
    Height = 12
    Caption = 'BASE.ICMS'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LBValorICMSSubstituicao: TLabel
    Left = 460
    Top = 290
    Width = 64
    Height = 12
    Caption = 'VL.ICMS.SUB'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbValorFrete: TLabel
    Left = 10
    Top = 330
    Width = 44
    Height = 12
    Caption = 'VLFRETE'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbValorSeguro: TLabel
    Left = 190
    Top = 330
    Width = 32
    Height = 12
    Caption = 'VLSEG'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbOutrasDespesas: TLabel
    Left = 340
    Top = 330
    Width = 66
    Height = 12
    Caption = 'VL.OUT.DESP'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbValorTotalIPI: TLabel
    Left = 460
    Top = 330
    Width = 50
    Height = 12
    Caption = 'VLTOTIPI'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbValorTotalNota: TLabel
    Left = 580
    Top = 330
    Width = 76
    Height = 12
    Caption = 'VLTOTALNOTA'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbNomeTransportadora: TLabel
    Left = 10
    Top = 380
    Width = 42
    Height = 12
    Caption = 'NOMETR'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbFreteporcontaTransportadora: TLabel
    Left = 190
    Top = 380
    Width = 72
    Height = 12
    Caption = 'FRETETRANSP'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbPlacaVeiculoTransportadora: TLabel
    Left = 340
    Top = 380
    Width = 59
    Height = 12
    Caption = 'PLACAVEIC'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbUFTransportadora: TLabel
    Left = 500
    Top = 380
    Width = 54
    Height = 12
    Caption = 'UFTRANSP'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbCnpjTransportadora: TLabel
    Left = 660
    Top = 380
    Width = 66
    Height = 12
    Caption = 'CNPJTRANSP'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbEnderecoTransportadora: TLabel
    Left = 10
    Top = 405
    Width = 33
    Height = 12
    Caption = 'ENDTR'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbMunicipioTransportadora: TLabel
    Left = 190
    Top = 405
    Width = 46
    Height = 12
    Caption = 'MUNICTR'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbUFEnderecoTransportadora: TLabel
    Left = 340
    Top = 405
    Width = 46
    Height = 12
    Caption = 'UFENDTR'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LBIETransportadora: TLabel
    Left = 540
    Top = 405
    Width = 24
    Height = 12
    Caption = 'IETR'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbQuantidadeTransportadora: TLabel
    Left = 10
    Top = 430
    Width = 36
    Height = 12
    Caption = 'QUANT'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbEspecieTransportadora: TLabel
    Left = 190
    Top = 430
    Width = 42
    Height = 12
    Caption = 'ESPECIE'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbMarcaTransportadora: TLabel
    Left = 340
    Top = 430
    Width = 37
    Height = 12
    Caption = 'MARCA'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbNumeroTransportadora: TLabel
    Left = 460
    Top = 430
    Width = 63
    Height = 12
    Caption = 'NUMTRANSP'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lBPesoBrutoTransportadora: TLabel
    Left = 580
    Top = 430
    Width = 47
    Height = 12
    Caption = 'PESOBTR'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object LbPesoLiquidoTransportadora: TLabel
    Left = 660
    Top = 430
    Width = 46
    Height = 12
    Caption = 'PESOLTR'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object ValorTotalProdutos: TLabel
    Left = 580
    Top = 290
    Width = 70
    Height = 12
    Caption = 'VALORTOTAL'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbcontroleinf: TLabel
    Left = 690
    Top = 470
    Width = 52
    Height = 12
    Caption = 'N'#186'NF.INFE'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object MEMOPRODUTOS: TLabel
    Left = 10
    Top = 220
    Width = 86
    Height = 12
    Caption = 'MEMOPRODUTOS'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object MEMOINFORMACOESCOMPLEMENTARES: TLabel
    Left = 10
    Top = 480
    Width = 200
    Height = 12
    Caption = 'MEMOINFORMACOESCOMPLEMENTARES'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbvencimentofatura1: TLabel
    Left = 11
    Top = 156
    Width = 58
    Height = 12
    Caption = 'VENCTOFT1'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbvalorfatura1: TLabel
    Left = 12
    Top = 172
    Width = 30
    Height = 12
    Caption = 'VLFt1'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbvencimentofatura2: TLabel
    Left = 91
    Top = 156
    Width = 58
    Height = 12
    Caption = 'VENCTOFT2'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbvalorfatura2: TLabel
    Left = 92
    Top = 172
    Width = 30
    Height = 12
    Caption = 'VLFt2'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbvencimentofatura3: TLabel
    Left = 171
    Top = 156
    Width = 58
    Height = 12
    Caption = 'VENCTOFT3'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbvalorfatura3: TLabel
    Left = 172
    Top = 172
    Width = 30
    Height = 12
    Caption = 'VLFt3'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbvencimentofatura4: TLabel
    Left = 251
    Top = 156
    Width = 58
    Height = 12
    Caption = 'VENCTOFT4'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbvalorfatura4: TLabel
    Left = 252
    Top = 172
    Width = 30
    Height = 12
    Caption = 'VLFt4'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbvencimentofatura5: TLabel
    Left = 323
    Top = 156
    Width = 58
    Height = 12
    Caption = 'VENCTOFT5'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object lbvalorfatura5: TLabel
    Left = 324
    Top = 172
    Width = 30
    Height = 12
    Caption = 'VLFt5'
    Color = clMenuText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = lbcontrolesupClick
    OnMouseMove = lbcontrolesupMouseMove
  end
  object PanelPOsicoes: TPanel
    Left = 554
    Top = 222
    Width = 116
    Height = 75
    DockSite = True
    DragMode = dmAutomatic
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBtnFace
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnDockOver = PanelPOsicoesDockOver
    object LbColuna: TLabel
      Left = 4
      Top = 7
      Width = 40
      Height = 13
      Caption = 'Coluna'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblinha: TLabel
      Left = 4
      Top = 31
      Width = 32
      Height = 13
      Caption = 'Linha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbQtdCaracteres: TLabel
      Left = 4
      Top = 55
      Width = 55
      Height = 13
      Caption = 'Qt.Carac.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtleft: TEdit
      Left = 57
      Top = 3
      Width = 52
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      BorderStyle = bsNone
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 9
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 0
      Text = 'EDTLEFT'
      OnKeyPress = edtleftKeyPress
    end
    object edttop: TEdit
      Left = 57
      Top = 27
      Width = 52
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      BorderStyle = bsNone
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 9
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 1
      Text = 'EDTTOP'
      OnKeyPress = edttopKeyPress
    end
    object edtquantidade: TEdit
      Left = 57
      Top = 51
      Width = 52
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      BorderStyle = bsNone
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 9
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 2
      Text = 'EDTTOP'
      OnKeyPress = edtquantidadeKeyPress
    end
  end
  object ComponenteRdPrint: TRDprint
    ImpressoraPersonalizada.NomeImpressora = 'Modelo Personalizado - (Epson)'
    ImpressoraPersonalizada.AvancaOitavos = '27 48'
    ImpressoraPersonalizada.AvancaSextos = '27 50'
    ImpressoraPersonalizada.SaltoPagina = '12'
    ImpressoraPersonalizada.TamanhoPagina = '27 67 66'
    ImpressoraPersonalizada.Negrito = '27 69'
    ImpressoraPersonalizada.Italico = '27 52'
    ImpressoraPersonalizada.Sublinhado = '27 45 49'
    ImpressoraPersonalizada.Expandido = '27 14'
    ImpressoraPersonalizada.Normal10 = '18 27 80'
    ImpressoraPersonalizada.Comprimir12 = '18 27 77'
    ImpressoraPersonalizada.Comprimir17 = '27 80 27 15'
    ImpressoraPersonalizada.Comprimir20 = '27 77 27 15'
    ImpressoraPersonalizada.Reset = '27 80 18 20 27 53 27 70 27 45 48'
    ImpressoraPersonalizada.Inicializar = '27 64'
    OpcoesPreview.PaginaZebrada = False
    OpcoesPreview.Remalina = False
    OpcoesPreview.CaptionPreview = 'Rdprint Preview'
    OpcoesPreview.PreviewZoom = 100
    OpcoesPreview.CorPapelPreview = clWhite
    OpcoesPreview.CorLetraPreview = clBlack
    OpcoesPreview.Preview = False
    OpcoesPreview.BotaoSetup = Ativo
    OpcoesPreview.BotaoImprimir = Ativo
    OpcoesPreview.BotaoGravar = Ativo
    OpcoesPreview.BotaoLer = Ativo
    OpcoesPreview.BotaoProcurar = Ativo
    Margens.Left = 10
    Margens.Right = 10
    Margens.Top = 10
    Margens.Bottom = 10
    Autor = Deltress
    RegistroUsuario.NomeRegistro = 'Exclaim Tecnologia'
    RegistroUsuario.SerieProduto = 'SINGLE-0706/01006'
    RegistroUsuario.AutorizacaoKey = 'BTXV-5778-EZCC-2224-OESY'
    About = 'RDprint 4.0c - Registrado'
    Acentuacao = Transliterate
    CaptionSetup = 'Rdprint Setup'
    TitulodoRelatorio = 'Gerado por RDprint'
    UsaGerenciadorImpr = False
    CorForm = clBtnFace
    CorFonte = clBlack
    Impressora = Epson
    Mapeamento.Strings = (
      '//--- Grafico Compativel com Windows/USB ---//'
      '//'
      'GRAFICO=GRAFICO'
      'HP=GRAFICO'
      'DESKJET=GRAFICO'
      'LASERJET=GRAFICO'
      'INKJET=GRAFICO'
      'STYLUS=GRAFICO'
      'EPL=GRAFICO'
      'USB=GRAFICO'
      '//'
      '//--- Linha Epson Matricial 9 e 24 agulhas ---//'
      '//'
      'EPSON=EPSON'
      'GENERICO=EPSON'
      'LX-300=EPSON'
      'LX-810=EPSON'
      'FX-2170=EPSON'
      'FX-1170=EPSON'
      'LQ-1170=EPSON'
      'LQ-2170=EPSON'
      'OKIDATA=EPSON'
      '//'
      '//--- Rima e Emilia ---//'
      '//'
      'RIMA=RIMA'
      'EMILIA=RIMA'
      '//'
      '//--- Linha HP/Xerox padr'#227'o PCL ---//'
      '//'
      'PCL=HP'
      '//'
      '//--- Impressoras 40 Colunas ---//'
      '//'
      'DARUMA=BOBINA'
      'SIGTRON=BOBINA'
      'SWEDA=BOBINA'
      'BEMATECH=BOBINA')
    PortaComunicacao = 'LPT1'
    MostrarProgresso = True
    TamanhoQteLinhas = 66
    TamanhoQteColunas = 80
    TamanhoQteLPP = Seis
    NumerodeCopias = 1
    FonteTamanhoPadrao = S05cpp
    FonteEstiloPadrao = []
    Orientacao = poPortrait
    Left = 40
    Top = 65530
  end
  object MainMenu1: TMainMenu
    Left = 8
    Top = 65530
    object Opes1: TMenuItem
      Caption = 'Op'#231#245'es'
      object ini1: TMenuItem
        Caption = 'Carrega Ini'
        OnClick = ini1Click
      end
      object IMPRIMEFOLHADEGUIA1: TMenuItem
        Caption = 'Imprime Folha Guia'
        OnClick = IMPRIMEFOLHADEGUIA1Click
      end
      object SalvaPosies1: TMenuItem
        Caption = 'Salva Posi'#231#245'es'
        OnClick = SalvaPosies1Click
      end
      object ImprimeTeste1: TMenuItem
        Caption = 'Imprime Teste'
      end
    end
  end
end
