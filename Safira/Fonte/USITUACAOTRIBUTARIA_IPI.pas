unit USITUACAOTRIBUTARIA_IPI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjSITUACAOTRIBUTARIA_IPI,
  jpeg;

type
  TFSITUACAOTRIBUTARIA_IPI = class(TForm)
    lbLbNOME: TLabel;
    edtNOME: TEdit;
    imgrodape: TImage;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb7: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjSITUACAOTRIBUTARIA_IPI:TObjSITUACAOTRIBUTARIA_IPI;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSITUACAOTRIBUTARIA_IPI: TFSITUACAOTRIBUTARIA_IPI;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFSITUACAOTRIBUTARIA_IPI.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjSITUACAOTRIBUTARIA_IPI do
    Begin
        Submit_CODIGO(lbCodigo.Caption);
        Submit_NOME(edtNOME.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFSITUACAOTRIBUTARIA_IPI.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjSITUACAOTRIBUTARIA_IPI do
     Begin
        lbCodigo.Caption:=Get_CODIGO;
        EdtNOME.text:=Get_NOME;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFSITUACAOTRIBUTARIA_IPI.TabelaParaControles: Boolean;
begin
     If (Self.ObjSITUACAOTRIBUTARIA_IPI.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFSITUACAOTRIBUTARIA_IPI.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(imgrodape,'RODAPE');


     Try
        Self.ObjSITUACAOTRIBUTARIA_IPI:=TObjSITUACAOTRIBUTARIA_IPI.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
end;

procedure TFSITUACAOTRIBUTARIA_IPI.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjSITUACAOTRIBUTARIA_IPI=Nil)
     Then exit;

If (Self.ObjSITUACAOTRIBUTARIA_IPI.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjSITUACAOTRIBUTARIA_IPI.free;
end;

procedure TFSITUACAOTRIBUTARIA_IPI.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFSITUACAOTRIBUTARIA_IPI.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
    
     lbCodigo.Caption:='0';

     
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjSITUACAOTRIBUTARIA_IPI.status:=dsInsert;
     btnovo.Visible:=false;
     btalterar.Visible:=False;
     btexcluir.Visible:=false;
     btrelatorios.Visible:=false;
     btopcoes.Visible:=false;
     btsair.Visible:=False;
     btpesquisar.visible:=False;


end;


procedure TFSITUACAOTRIBUTARIA_IPI.btalterarClick(Sender: TObject);
begin
    If (Self.ObjSITUACAOTRIBUTARIA_IPI.Status=dsinactive) and (lbCodigo.Caption<>'')
    Then Begin
                habilita_campos(Self);
                Self.ObjSITUACAOTRIBUTARIA_IPI.Status:=dsEdit;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtNOME.setfocus;
                btnovo.Visible:=false;
                 btalterar.Visible:=False;
                 btexcluir.Visible:=false;
                 btrelatorios.Visible:=false;
                 btopcoes.Visible:=false;
                 btsair.Visible:=False;
                 btpesquisar.visible:=False;

          End;


end;

procedure TFSITUACAOTRIBUTARIA_IPI.btgravarClick(Sender: TObject);
begin

     If Self.ObjSITUACAOTRIBUTARIA_IPI.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjSITUACAOTRIBUTARIA_IPI.salvar(true)=False)
     Then exit;

     lbCodigo.Caption:=Self.ObjSITUACAOTRIBUTARIA_IPI.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
      btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;

     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFSITUACAOTRIBUTARIA_IPI.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjSITUACAOTRIBUTARIA_IPI.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (Self.ObjSITUACAOTRIBUTARIA_IPI.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjSITUACAOTRIBUTARIA_IPI.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFSITUACAOTRIBUTARIA_IPI.btcancelarClick(Sender: TObject);
begin
     Self.ObjSITUACAOTRIBUTARIA_IPI.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
      btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;


end;

procedure TFSITUACAOTRIBUTARIA_IPI.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFSITUACAOTRIBUTARIA_IPI.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjSITUACAOTRIBUTARIA_IPI.Get_pesquisa,Self.ObjSITUACAOTRIBUTARIA_IPI.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjSITUACAOTRIBUTARIA_IPI.status<>dsinactive
                                  then exit;

                                  If (Self.ObjSITUACAOTRIBUTARIA_IPI.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjSITUACAOTRIBUTARIA_IPI.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFSITUACAOTRIBUTARIA_IPI.LimpaLabels;
begin
    lbCodigo.Caption:='';
end;

procedure TFSITUACAOTRIBUTARIA_IPI.FormShow(Sender: TObject);
begin
    // PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjSITUACAOTRIBUTARIA_IPI.OBJETO.Get_Pesquisa,Self.ObjSITUACAOTRIBUTARIA_IPI.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjSITUACAOTRIBUTARIA_IPI.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjSITUACAOTRIBUTARIA_IPI.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname
(Self.ObjSITUACAOTRIBUTARIA_IPI.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
