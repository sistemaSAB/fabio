object FalteraPrecoFerragem: TFalteraPrecoFerragem
  Left = 635
  Top = 354
  Width = 746
  Height = 237
  Caption = 'Altera'#231#227'o de Pre'#231'o  - FERRAGEM'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LbPrecoCusto: TLabel
    Left = 4
    Top = 16
    Width = 85
    Height = 14
    Caption = 'Pre'#231'o de Custo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 513
    Top = 7
    Width = 82
    Height = 16
    Caption = 'Pre'#231'o Pago: '
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbprecopago: TLabel
    Left = 629
    Top = 7
    Width = 82
    Height = 16
    Alignment = taRightJustify
    Caption = 'Pre'#231'o Pago: '
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GroupBoxMargem: TGroupBox
    Left = 4
    Top = 35
    Width = 206
    Height = 81
    Caption = 'Margem'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object LbPorcentagemFornecido: TLabel
      Left = 11
      Top = 36
      Width = 48
      Height = 14
      Caption = 'Fornecido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object LbPorcentagemRetirado: TLabel
      Left = 11
      Top = 56
      Width = 40
      Height = 14
      Caption = 'Retirado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object LbPorcentagemInstalado: TLabel
      Left = 11
      Top = 15
      Width = 43
      Height = 14
      Caption = 'Instalado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 158
      Top = 15
      Width = 10
      Height = 14
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 158
      Top = 36
      Width = 10
      Height = 14
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 158
      Top = 56
      Width = 10
      Height = 14
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object EdtPorcentagemInstalado: TEdit
      Left = 85
      Top = 11
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnExit = EdtPorcentagemInstaladoExit
    end
    object EdtPorcentagemFornecido: TEdit
      Left = 85
      Top = 32
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      OnExit = EdtPorcentagemFornecidoExit
    end
    object EdtPorcentagemRetirado: TEdit
      Left = 85
      Top = 52
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      OnExit = EdtPorcentagemRetiradoExit
    end
  end
  object EdtPrecoCusto: TEdit
    Left = 104
    Top = 14
    Width = 80
    Height = 19
    Ctl3D = False
    MaxLength = 9
    ParentCtl3D = False
    TabOrder = 0
  end
  object GroupBoxPrecoVenda: TGroupBox
    Left = 220
    Top = 36
    Width = 206
    Height = 81
    Caption = 'Pre'#231'o Venda'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object LbPrecoVendaInstalado: TLabel
      Left = 11
      Top = 15
      Width = 43
      Height = 14
      Caption = 'Instalado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object LbPrecoVendaFornecido: TLabel
      Left = 11
      Top = 38
      Width = 48
      Height = 14
      Caption = 'Fornecido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object LbPrecoVendaRetirado: TLabel
      Left = 11
      Top = 61
      Width = 40
      Height = 14
      Caption = 'Retirado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 79
      Top = 15
      Width = 13
      Height = 14
      Caption = 'R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 79
      Top = 38
      Width = 13
      Height = 14
      Caption = 'R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 79
      Top = 61
      Width = 13
      Height = 14
      Caption = 'R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object EdtPrecoVendaInstalado: TEdit
      Left = 115
      Top = 11
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnExit = EdtPrecoVendaInstaladoExit
    end
    object EdtPrecoVendaFornecido: TEdit
      Left = 115
      Top = 34
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      OnExit = EdtPrecoVendaFornecidoExit
    end
    object EdtPrecoVendaRetirado: TEdit
      Left = 115
      Top = 57
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      OnExit = EdtPrecoVendaRetiradoExit
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 121
    Width = 730
    Height = 78
    Align = alBottom
    TabOrder = 3
    object Label8: TLabel
      Left = 308
      Top = 11
      Width = 50
      Height = 14
      Caption = 'Instalado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 308
      Top = 31
      Width = 55
      Height = 14
      Caption = 'Fornecido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 308
      Top = 52
      Width = 46
      Height = 14
      Caption = 'Retirado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Rg_forma_de_calculo_percentual: TRadioGroup
      Left = 465
      Top = 4
      Width = 180
      Height = 33
      Caption = 'Calcular % de Acr'#233'scimo'
      Columns = 2
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ItemIndex = 0
      Items.Strings = (
        'Custo'
        'Valor Final')
      ParentFont = False
      TabOrder = 0
    end
    object edtinstaladofinal: TEdit
      Left = 370
      Top = 8
      Width = 74
      Height = 19
      Color = 15663069
      Ctl3D = False
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
    end
    object edtfornecidofinal: TEdit
      Left = 370
      Top = 28
      Width = 74
      Height = 19
      Color = 15663069
      Ctl3D = False
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
    end
    object edtretiradofinal: TEdit
      Left = 370
      Top = 49
      Width = 74
      Height = 19
      Color = 15663069
      Ctl3D = False
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
    end
    object btgravar: TButton
      Left = 468
      Top = 46
      Width = 75
      Height = 25
      Caption = '&Gravar'
      TabOrder = 5
      OnClick = btgravarClick
    end
    object btcancelar: TButton
      Left = 548
      Top = 46
      Width = 75
      Height = 25
      Caption = '&Cancelar'
      TabOrder = 6
      OnClick = btcancelarClick
    end
    object GroupBoxVidros: TGroupBox
      Left = 8
      Top = 11
      Width = 233
      Height = 56
      Caption = 'Margem de lucro'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      object LbPorcentagemAcrescimo: TLabel
        Left = 2
        Top = 14
        Width = 30
        Height = 14
        Caption = '% Cor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbAcrescimoExtra: TLabel
        Left = 115
        Top = 14
        Width = 38
        Height = 14
        Caption = '% Extra'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbPorcentagemAcrescimoFinal: TLabel
        Left = 171
        Top = 14
        Width = 35
        Height = 14
        Caption = '% Final'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 59
        Top = 14
        Width = 41
        Height = 14
        Caption = 'R$ Extra'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object EdtPorcentagemAcrescimo: TEdit
        Left = 3
        Top = 28
        Width = 54
        Height = 19
        Color = 15663069
        Ctl3D = False
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
      end
      object EdtAcrescimoExtra: TEdit
        Left = 115
        Top = 28
        Width = 54
        Height = 19
        Color = 15663069
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
        OnExit = EdtAcrescimoExtraExit
      end
      object EdtPorcentagemAcrescimoFinal: TEdit
        Left = 171
        Top = 28
        Width = 54
        Height = 19
        Color = 15663069
        Ctl3D = False
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
      end
      object EdtValorExtra: TEdit
        Left = 59
        Top = 28
        Width = 54
        Height = 19
        Color = 15663069
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        OnExit = EdtValorExtraExit
      end
    end
  end
end
