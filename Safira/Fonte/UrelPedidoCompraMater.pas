unit UrelPedidoCompraMater;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls;

type
  TTQRUrelPedidoCompraMater = class(TQuickRep)
    Bandaqrbnd1: TQRBand;
    lB1: TQRLabel;
    qrsysdt1: TQRSysData;
    BandaDetail: TQRBand;
    qrshp44: TQRShape;
    qrshp45: TQRShape;
    qrshp46: TQRShape;
    lB45: TQRLabel;
    lB46: TQRLabel;
    lB47: TQRLabel;
    qrshp1: TQRShape;
    lB29: TQRLabel;
    QrStrBandDados: TQRStringsBand;
    lBLBDADOS: TQRLabel;
    BandaSubDetail: TQRSubDetail;
    lB22: TQRLabel;
    lB23: TQRLabel;
    lB24: TQRLabel;
    lB25: TQRLabel;
    lB26: TQRLabel;
    lB30: TQRLabel;
    lB32: TQRLabel;
    lB33: TQRLabel;
    lB44: TQRLabel;
    lBNomeEmpresa: TQRLabel;
    lBEndereco: TQRLabel;
    lBNumero: TQRLabel;
    lBBairro: TQRLabel;
    lBCidade: TQRLabel;
    lBFone: TQRLabel;
    lBCep: TQRLabel;
    Bandaqrbnd2: TQRBand;
    qrshp2: TQRShape;
    qrshp3: TQRShape;
    lBDataPedidoCompra: TQRLabel;
    imgLogoDistribuidor: TQRImage;
    qrshp6: TQRShape;
    qrshp7: TQRShape;
    lB4: TQRLabel;
    lB5: TQRLabel;
    qrshp5: TQRShape;
    qrshp4: TQRShape;
    lB2: TQRLabel;
    lB3: TQRLabel;
    lBNomeDistribuidor: TQRLabel;
    lBPedidoCompra: TQRLabel;
    qrmObservacao: TQRMemo;
    lB21: TQRLabel;
    qrshp8: TQRShape;
    lB7: TQRLabel;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    procedure lBLBDADOSPrint(sender: TObject; var Value: String);
  private

  public

  end;
              
var
  TQRUrelPedidoCompraMater: TTQRUrelPedidoCompraMater;

implementation

{$R *.DFM}


procedure TTQRUrelPedidoCompraMater.lBLBDADOSPrint(sender: TObject;
  var Value: String);
var
apoio:string;
posicao:integer;
begin
     LBlbdados.font.Color:=clBLACK;
     LBLBDADOS.Font.Style:=[];
     LBLBDADOS.Font.size:=9;

     case QrStrBandDados.item[1] of
     '?':Begin//negrito
              LBLBDADOS.Font.Style:=[fsbold];
              value:=copy(QrStrBandDados.Item,2,length(QrStrBandDados.item));
         end;
     '�':Begin//cor

              apoio:=copy(QrStrBandDados.Item,2,length(QrStrBandDados.item));
              posicao:=pos('�',apoio);
              If (posicao=0)//tem que ter o fecho e entre eles a cor
              Then value:=QrStrBandDados.item
              Else Begin//s� 3 cores, vermelho,verde e azul
                        apoio:=copy(apoio,1,posicao-1);
                        if UPPERCASE(apoio)='VERMELHO'
                        tHEN LBlbdados.font.Color:=CLRED
                        eLSE
                                IF UPPERCASE(APOIO)='VERDE'
                                THEN LBlbdados.font.Color:=clGreen
                                ELSE
                                        IF UPPERCASE(APOIO)='AZUL'
                                        THEN LBlbdados.font.Color:=clBlue
                                        ELSE LBlbdados.font.Color:=clBLACK;


                        //pegando os dados fora a cor
                        value:=copy(QrStrBandDados.Item,posicao+2,length(QrStrBandDados.item));
              End;
         End;
     Else Begin
                value:=QrStrBandDados.item;
     End;

     end;


end;


end.
