unit UobjFERRAGEM_PROJ;
Interface
Uses forms,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJFERRAGEM,UOBJPROJETO,UFERRAGEM;

Type
   TObjFERRAGEM_PROJ=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Ferragem:TOBJFERRAGEM;
                Projeto:TOBJPROJETO;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_FormulaAltura(parametro: string);
                Function Get_FormulaAltura: string;
                Procedure Submit_FormulaLargura(parametro: string);
                Function Get_FormulaLargura: string;
                Procedure Submit_Quantidade(parametro: string);
                Function Get_Quantidade: string;
                procedure EdtFerragemExit(Sender: TObject;Var PEdtCodigo :TEdit;LABELNOME:TLABEL);
                procedure EdtFerragemKeyDown(Sender: TObject; Var PEdtCodigo :TEdit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtFerragemKeyDown2(Sender: TObject; Var PEdtCodigo :string; var Key: Word;
                Shift: TShiftState;var LABELNOME:string;CodigoProjeto:string;var referencia:string);
                procedure EdtProjetoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                Procedure Resgata_Ferragem_Proj(PProjeto : string);

                Function  Replica(PProjeto : string;PnovoProjeto:string):boolean;

                procedure Submit_AlturaCorte(parametro:string);
                procedure Submit_PosicaoCorte(parametro:string);
                procedure Submit_TamanhoCorte(parametro:String);

                function Get_AlturaCorte:string;
                function Get_PosicaoCorte:string;
                function Get_TamanhoCorte:string;

                procedure Submit_Ligadacomponente(parametro:string);
                procedure Submit_ComponenteLigacao(parametro:string);

                function Get_Ligadacomponente:string;
                function Get_ComponenteLigacao:string;

                Function VerificaSeJaExisteEstaFerragemNesteProjeto(PProjeto, PFerragem:string):Boolean;

         Private
               Objquery:Tibquery;
               ObjQueryPesquisa : TIBQuery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               FormulaAltura:string;
               FormulaLargura:string;
               Quantidade:string;
               AlturaCorte:string;
               PosicaoCorte:string;
               TamanhoCorte:string;
               ligadacomponente:string;
               ComponenteLigacao:string;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UMostraBarraProgresso;


{ TTabTitulo }


Function  TObjFERRAGEM_PROJ.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If(FieldByName('Ferragem').asstring<>'')
        Then Begin
                 If (Self.Ferragem.LocalizaCodigo(FieldByName('Ferragem').asstring)=False)
                 Then Begin
                          Messagedlg('Ferragem N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Ferragem.TabelaparaObjeto;
        End;
        If(FieldByName('Projeto').asstring<>'')
        Then Begin
                 If (Self.Projeto.LocalizaCodigo(FieldByName('Projeto').asstring)=False)
                 Then Begin
                          Messagedlg('Projeto N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Projeto.TabelaparaObjeto;
        End;
        Self.FormulaAltura:=fieldbyname('FormulaAltura').asstring;
        Self.FormulaLargura:=fieldbyname('FormulaLargura').asstring;
        Self.Quantidade:=fieldbyname('Quantidade').asstring;
        self.AlturaCorte:=fieldbyname('alturacorte').AsString;
        self.PosicaoCorte:=fieldbyname('posicaocorte').AsString;
        self.TamanhoCorte:=fieldbyname('tamanhocorte').AsString;
        self.ligadacomponente:=fieldbyname('ligadacomponente').AsString;
        Self.ComponenteLigacao:=fieldbyname('componenteligacao').AsString;

//CODIFICA TABELAPARAOBJETO






        result:=True;
     End;
end;


Procedure TObjFERRAGEM_PROJ.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Ferragem').asstring:=Self.Ferragem.GET_CODIGO;
        ParamByName('Projeto').asstring:=Self.Projeto.GET_CODIGO;
        ParamByName('FormulaAltura').asstring:=Self.FormulaAltura;
        ParamByName('FormulaLargura').asstring:=Self.FormulaLargura;
        ParamByName('Quantidade').asstring:=virgulaparaponto(Self.Quantidade);
        ParamByName('alturacorte').AsString:=AlturaCorte;
        ParamByName('posicaocorte').AsString:=PosicaoCorte;
        ParamByName('tamanhocorte').AsString:=TamanhoCorte;
        ParamByName('ligadacomponente').AsString:=ligadacomponente;
        ParamByName('ComponenteLigacao').AsString:=ComponenteLigacao;
//CODIFICA OBJETOPARATABELA






  End;
End;

//***********************************************************************

function TObjFERRAGEM_PROJ.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjFERRAGEM_PROJ.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Ferragem.ZerarTabela;
        Projeto.ZerarTabela;
        FormulaAltura:='';
        FormulaLargura:='';
        Quantidade:='';
        alturacorte:='';
        PosicaoCorte:='';
        TamanhoCorte:='';
        ComponenteLigacao:='';
        ligadacomponente:='';
//CODIFICA ZERARTABELA






     End;
end;

Function TObjFERRAGEM_PROJ.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjFERRAGEM_PROJ.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Ferragem.LocalizaCodigo(Self.Ferragem.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Ferragem n�o Encontrado!';
      If (Self.Projeto.LocalizaCodigo(Self.Projeto.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Projeto n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjFERRAGEM_PROJ.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.Ferragem.Get_Codigo<>'')
        Then Strtoint(Self.Ferragem.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Ferragem';
     End;
     try
        If (Self.Projeto.Get_Codigo<>'')
        Then Strtoint(Self.Projeto.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Projeto';
     End;
     try
        Strtofloat(Self.Quantidade);
     Except
           Mensagem:=mensagem+'/Quantidade';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjFERRAGEM_PROJ.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjFERRAGEM_PROJ.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjFERRAGEM_PROJ.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro FERRAGEM_PROJ vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Ferragem,Projeto,FormulaAltura,FormulaLargura,Quantidade,alturacorte,tamanhocorte,posicaocorte,ComponenteLigacao,ligadacomponente');
           SQL.ADD(' ');
           SQL.ADD(' from  TABFERRAGEM_PROJ');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjFERRAGEM_PROJ.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjFERRAGEM_PROJ.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjFERRAGEM_PROJ.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjQueryPesquisa;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Ferragem:=TOBJFERRAGEM.create;
        Self.Projeto:=TOBJPROJETO.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABFERRAGEM_PROJ(Codigo,Ferragem,Projeto');
                InsertSQL.add(' ,FormulaAltura,FormulaLargura,Quantidade,alturacorte,tamanhocorte,posicaocorte,ComponenteLigacao,ligadacomponente)');
                InsertSQL.add('values (:Codigo,:Ferragem,:Projeto,:FormulaAltura,:FormulaLargura');
                InsertSQL.add(' ,:Quantidade,:alturacorte,:tamanhocorte,:posicaocorte,:ComponenteLigacao,:ligadacomponente)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABFERRAGEM_PROJ set Codigo=:Codigo,Ferragem=:Ferragem');
                ModifySQL.add(',Projeto=:Projeto,FormulaAltura=:FormulaAltura,FormulaLargura=:FormulaLargura');
                ModifySQL.add(',Quantidade=:Quantidade,alturacorte=:alturacorte,tamanhocorte=:tamanhocorte,posicaocorte=:posicaocorte');
                ModifySQL.add(',ComponenteLigacao=:ComponenteLigacao,ligadacomponente=:ligadacomponente where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABFERRAGEM_PROJ where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjFERRAGEM_PROJ.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjFERRAGEM_PROJ.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabFERRAGEM_PROJ');
     Result:=Self.ParametroPesquisa;
end;

function TObjFERRAGEM_PROJ.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de FERRAGEM_PROJ ';
end;


function TObjFERRAGEM_PROJ.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENFERRAGEM_PROJ,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENFERRAGEM_PROJ,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjFERRAGEM_PROJ.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Freeandnil(Self.ObjQueryPesquisa);
    Freeandnil(Self.ObjDataSource);
    Self.Ferragem.FREE;
    Self.Projeto.FREE;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjFERRAGEM_PROJ.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjFERRAGEM_PROJ.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjFerragem_Proj.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjFerragem_Proj.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjFerragem_Proj.Submit_FormulaAltura(parametro: string);
begin
        Self.FormulaAltura:=Parametro;
end;
function TObjFerragem_Proj.Get_FormulaAltura: string;
begin
        Result:=Self.FormulaAltura;
end;
procedure TObjFerragem_Proj.Submit_FormulaLargura(parametro: string);
begin
        Self.FormulaLargura:=Parametro;
end;
function TObjFerragem_Proj.Get_FormulaLargura: string;
begin
        Result:=Self.FormulaLargura;
end;
procedure TObjFerragem_Proj.Submit_Quantidade(parametro: string);
begin
        Self.Quantidade:=Parametro;
end;
function TObjFerragem_Proj.Get_Quantidade: string;
begin
        Result:=Self.Quantidade;
end;
//CODIFICA GETSESUBMITS


procedure TObjFERRAGEM_PROJ.EdtFerragemExit(Sender: TObject;Var PEdtCodigo :TEdit; LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Ferragem.LocalizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     Self.Ferragem.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Ferragem.Get_Descricao;
     PEdtCodigo.Text:=Self.Ferragem.Get_Codigo;

End;
procedure TObjFERRAGEM_PROJ.EdtFerragemKeyDown(Sender: TObject; Var PEdtCodigo :TEdit; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Fferragem:TFFERRAGEM;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fferragem:=TFFERRAGEM.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Ferragem.Get_Pesquisa2,Self.Ferragem.Get_TituloPesquisa,Fferragem)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                { If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Ferragem.RETORNACAMPONOME<>''

                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Ferragem.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End; }
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fferragem);
     End;
end;

procedure TObjFERRAGEM_PROJ.EdtFerragemKeyDown2(Sender: TObject; Var PEdtCodigo :string; var Key: Word;
  Shift: TShiftState;var LABELNOME:string;CodigoProjeto:string;var referencia:string);
var
   FpesquisaLocal:Tfpesquisa;
   Fferragem:TFFERRAGEM;
   SQL:string;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fferragem:=TFFERRAGEM.Create(nil);
            sql:='SELECT * FROM TABFERRAGEM JOIN TABFERRAGEM_PROJ on TABFERRAGEM.codigo=TABFERRAGEM_PROJ.ferragem WHERE TABFERRAGEM_PROJ.PROJETO='+CodigoProjeto;
            If (FpesquisaLocal.PreparaPesquisa(SQL,Self.Ferragem.Get_TituloPesquisa,Fferragem)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 PEdtCodigo:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If PEdtCodigo<>''
                                 Then Begin
                                        If Self.Ferragem.RETORNACAMPONOME<>''

                                        Then LABELNOME:=FpesquisaLocal.QueryPesq.fieldbyname('DESCRICAO').asstring
                                        Else LABELNOME:='';
                                        referencia:=FpesquisaLocal.querypesq.fieldbyname('referencia').AsString;
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fferragem);
     End;
end;
procedure TObjFERRAGEM_PROJ.EdtProjetoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Projeto.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Projeto.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Projeto.Get_Descricao;
End;
procedure TObjFERRAGEM_PROJ.EdtProjetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Projeto.Get_Pesquisa,Self.Projeto.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Projeto.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Projeto.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Projeto.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjFERRAGEM_PROJ.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJFERRAGEM_PROJ';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjFERRAGEM_PROJ.Resgata_Ferragem_Proj(PProjeto: string);
begin
     With Self.ObjQueryPesquisa do
     Begin
         close;
         SQL.Clear;
         Sql.Add('Select  TabFerragem.Referencia, TabFerragem.Descricao,');
         Sql.Add('TabFerragem_Proj.Quantidade, TabFerragem_Proj.Codigo');
         Sql.Add('from TabFerragem_Proj');
         Sql.Add('Join  TabFerragem on TabFerragem.Codigo = TabFerragem_Proj.Ferragem');
         Sql.Add('Where Projeto = '+PProjeto);
        
         if (PProjeto = '')
         then exit;
         Open;
     end;
end;

function TObjFERRAGEM_PROJ.VerificaSeJaExisteEstaFerragemNesteProjeto(PProjeto, PFerragem: string): Boolean;
Var    QueryLOcal:TIBquery;
begin
try
        Result:=false;
        try
             QueryLOcal:=TIBQuery.Create(nil);
             QueryLOcal.Database:=FDataModulo.IBDatabase;
        except
             MensagemErro('Erro ao tentar criar a QueryLocal');
             exit;
        end;

        With QueryLocal do
        Begin
             Close;
             Sql.Clear;
             Sql.Add('Select Codigo from TabFerragem_Proj');
             Sql.Add('Where Ferragem = '+PFerragem);
             Sql.Add('and Projeto = '+PProjeto);
             Open;

             if (RecordCount>0)then
             Result:=true;

        end;

Finally
       FreeAndNil(QueryLOcal);
end;

end;

Function TObjFERRAGEM_PROJ.Replica(PProjeto, PnovoProjeto: string):boolean;
var
pcodigoantigo:string;
begin
     result:=false;
     With Self.ObjQueryPesquisa do
     Begin
         close;
         SQL.Clear;
         Sql.Add('Select  TabFerragem_Proj.Codigo');
         Sql.Add('from    TabFerragem_Proj');
         Sql.Add('Where   Projeto = '+PProjeto);

         if (PProjeto = '')
         then exit;

         Open;
         last;
         if (recordcount=0)
         Then Begin
                   result:=true;
                   exit;
         End;
         FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
         FMostraBarraProgresso.Lbmensagem.caption:='Replicando Ferragens';
         first;
         Try
         
           While not(eof) do
           begin
                FMostraBarraProgresso.IncrementaBarra1(1);
                FMostraBarraProgresso.Show;
                Application.processmessages;

                Self.ZerarTabela;
                if (Self.LocalizaCodigo(fieldbyname('codigo').asstring)=False)
                then Begin
                          mensagemErro('Ferragem_Proj C�digo '+fieldbyname('codigo').asstring+' n�o localizada');
                          exit;
                End;
                Self.TabelaparaObjeto;

                Self.Status:= dsinsert;
                PcodigoAntigo:=Self.codigo;
                Self.Submit_Codigo('0');
                Self.Projeto.Submit_Codigo(PnovoProjeto);
                if (self.Salvar(False)=False)
                Then begin
                          mensagemErro('Erro na tentativa de Replicar a ferragem_proj C�digo='+PcodigoAntigo);
                          exit;
                End;

                next;
           End;
           result:=True;
           
         Finally
                FMostraBarraProgresso.Close;
         End;
     end;

end;

procedure TObjFERRAGEM_PROJ.Submit_AlturaCorte(parametro:string);
begin
    Self.AlturaCorte:=parametro;
end;

procedure TObjFERRAGEM_PROJ.Submit_PosicaoCorte(parametro:string);
begin
    self.PosicaoCorte:=parametro;
end;

procedure TObjFERRAGEM_PROJ.Submit_TamanhoCorte(parametro:string);
begin
    self.TamanhoCorte:=parametro;
end;

function TObjFERRAGEM_PROJ.Get_AlturaCorte:string;
begin
    Result:=AlturaCorte;
end;

function TObjFERRAGEM_PROJ.Get_PosicaoCorte:string;
begin
    Result:=PosicaoCorte;
end;

function TObjFERRAGEM_PROJ.Get_TamanhoCorte:string;
begin
    Result:=TamanhoCorte;
end;

procedure TObjFERRAGEM_PROJ.Submit_Ligadacomponente(parametro:string);
begin
    Self.ligadacomponente:=parametro;
end;

procedure TObjFERRAGEM_PROJ.Submit_ComponenteLigacao(parametro:string);
begin
    Self.ComponenteLigacao:=parametro;
end;

function TObjFERRAGEM_PROJ.Get_Ligadacomponente:string;
begin
    Result:=ligadacomponente;
end;

function TObjFERRAGEM_PROJ.Get_ComponenteLigacao:string;
begin
    Result:=ComponenteLigacao;
end;

end.



