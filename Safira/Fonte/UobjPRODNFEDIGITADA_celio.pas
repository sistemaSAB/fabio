{jonas neuberger 29/04/2011 08:51}

unit UobjPRODNFEDIGITADA;

Interface

Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,
     UOBJNFEDIGITADA,UOBJFERRAGEM,UOBJPERFILADO,UOBJDIVERSO,UOBJVIDRO,
     UOBJCOMPONENTE,UOBJKITBOX,UOBJPERSIANA,UobjNotaFiscalObjetos,UobjCOR,UobjTransmiteNFE;

Type
   TObjPRODNFEDIGITADA=class

          Public

            Status:TDataSetState;
            SqlInicial:String;
            NFEDIGITADA:TOBJNFEDIGITADA;
            FERRAGEM:TOBJFERRAGEM;
            PERFILADO:TOBJPERFILADO;
            DIVERSO:TOBJDIVERSO;
            VIDRO:TOBJVIDRO;
            COMPONENTE:TOBJCOMPONENTE;
            KITBOX:TOBJKITBOX;
            PERSIANA:TOBJPERSIANA;
            COR:TObjCOR;

            ObjNotafiscalObjetos:TObjNotafiscalObjetos;
            objTransNFE:TObjTransmiteNFE;
            queryProdutos :TIBQuery;

            Constructor Create(Owner:TComponent);
            Destructor  Free;
            Function    Salvar(ComCommit:Boolean)       :Boolean;
            Function    LocalizaCodigo(Parametro:string) :boolean;
            Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
            Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
            Function    Get_Pesquisa                    :TStringList;
            Function    Get_TituloPesquisa              :string;

            Function   TabelaparaObjeto:Boolean;
            Procedure  ZerarTabela;
            Procedure  Cancelar;
            Procedure  Commit;

            Function  Get_NovoCodigo:string;
            Function  RetornaCampoCodigo:string;
            Function  RetornaCampoNome:string;
            Procedure Imprime(Pcodigo:string);

            Procedure Submit_CODIGO(parametro: string);
            Function Get_CODIGO: string;
            Procedure Submit_QUANTIDADE(parametro: string);
            Function Get_QUANTIDADE: string;
            Procedure Submit_VALOR(parametro: string);
            Function Get_VALOR: string;
            Procedure Submit_DESCONTO(parametro: string);
            Function Get_DESCONTO: string;
            Procedure Submit_VALORFRETE(parametro: string);
            Function Get_VALORFRETE: string;
            Procedure Submit_VALORSEGURO(parametro: string);
            Function Get_VALORSEGURO: string;
            Procedure Submit_VALORTOTAL(parametro: string);
            Function Get_VALORTOTAL: string;
            Procedure Submit_VALORFINAL(parametro: string);
            procedure submit_valoripi(p:string);
            procedure submit_vaoricms_st(p:string);
            Function Get_VALORFINAL: string;
            function get_DESCRICAO:string;
            function get_valoripi:string;
            function get_valoricms_st:string;
            function get_MATERIAL:string;
            function get_cfop:string;

            procedure submit_MATERIAL(parametro:string);
            procedure submit_DESCRICAO(parametro:string);
            procedure submit_nfeEntrada(valor:Boolean);
            procedure submit_nfeDevolucao(valor:boolean);
            procedure submit_nfeComplementar(valor:Boolean);
            procedure submit_cfop(p:string);


            procedure desativaGrid();
            procedure retornaProdutos  (pCodigo: string);
            procedure excluiProdutos   (pNfeDigitada:string);
            procedure opcoes(pCodigo:string);
            function get_valorTotal_2():string;
            function get_valorFinal_2():string;
            function gravaMateriaisVendidos_2(pNfedigitada,operacao:string):Boolean;
            function gravaMateriaisImpostoManual(pNfedigitada:string):Boolean;

            procedure pesquisaCor(codigoFerragem,chaveEstrangeira,chavePrimaria,tabela:string;LABELNOME:TLabel;sender:TObject);
            function  geraNotaFiscalParaNFe(PnotaFiscal:TStringList;pNfeDigitada:string;operacao:string;Corte:Integer;pModoContingencia:Boolean = false):Boolean;
            function get_proximaNFE():string;
            function estornaMateriaisVenda(pnfeDigitada:string):Boolean;



            procedure EdtNFEDIGITADAExit(Sender: TObject;LABELNOME:TLABEL);
            procedure EdtNFEDIGITADAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
            procedure EdtFERRAGEMExit(Sender: TObject;LABELNOME:TLABEL);

            procedure EdtFERRAGEMKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);


            procedure EdtPERFILADOExit(Sender: TObject;LABELNOME:TLABEL);
            procedure EdtPERFILADOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
            procedure EdtDIVERSOExit(Sender: TObject;LABELNOME:TLABEL);
            procedure EdtDIVERSOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
            procedure EdtVIDROExit(Sender: TObject;LABELNOME:TLABEL);
            procedure EdtVIDROKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
            procedure EdtCOMPONENTEExit(Sender: TObject;LABELNOME:TLABEL);
            procedure EdtCOMPONENTEKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
            procedure EdtKITBOXExit(Sender: TObject;LABELNOME:TLABEL);
            procedure EdtKITBOXKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
            procedure EdtPERSIANAExit(Sender: TObject;LABELNOME:TLABEL);
            procedure EdtPERSIANAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
            procedure EdtCFOPKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);

          Private

            Objquery:Tibquery;
            InsertSql,DeleteSql,ModifySQl,ParametroPesquisa:TStringList;
            Owner:TComponent;
            
            CODIGO:string;
            QUANTIDADE:string;
            VALOR:string;
            DESCONTO:string;
            VALORFRETE:string;
            VALORSEGURO:string;
            VALORTOTAL:string;
            VALORFINAL:string;
            VALORIPI:string;
            VALORICMS_ST:string;
            DataC:string;
            UserC:string;
            DataM:string;
            UserM:string;
            DESCRICAO:string;
            MATERIAL:string;
            CFOP:string;

            nfeDevolucao:Boolean;
            NfeEntrada:Boolean;
            nfeComplementar:Boolean;

            {referencia}
            cUF_ref:string;
            AAMM_ref:string;
            CNPJ_ref:string;
            modelo_ref:string;
            serie_ref:string;
            nNF_ref:string;
            refNFe_ref:string;
            tipoRef:string;
            adicional_ref:string;

            calculatotaltributos:Boolean;

            Function  VerificaBrancos:Boolean;
            Function  VerificaRelacionamentos:Boolean;
            Function  VerificaNumericos:Boolean;
            Function  VerificaData:Boolean;
            Function  VerificaFaixa:boolean;
            Procedure ObjetoparaTabela;


            procedure geraNfeDevolucao (pcodigo:string);
            procedure geraNFeEntrada   (pCodigo:string);
            procedure geraNfeComplementar  (pcodigo:string);


End;


implementation

uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
     UmenuRelatorios, UFERRAGEM, UPERFILADO, UDIVERSO, UVIDRO, UCOMPONENTE,
     UKITBOX, UPERSIANA,objmateriaisvenda, Uobjferragem_icms,
  UOBJPERFILADO_ICMS, UobjVIDRO_ICMS, UOBJKITBOX_ICMS, uobjpersiana_icms,
  uobjdiverso_icms, UCOR, uReferenciaNFe, UobjNFE;


Function  TObjPRODNFEDIGITADA.TabelaparaObjeto:Boolean;
begin

  With Objquery do
  Begin
    Self.ZerarTabela;
    Self.CODIGO:=fieldbyname('CODIGO').asstring;
    If(FieldByName('NFEDIGITADA').asstring<>'')
    Then Begin
           If (Self.NFEDIGITADA.LocalizaCodigo(FieldByName('NFEDIGITADA').asstring)=False)
           Then Begin
                    Messagedlg('NFEDIGITADA N�o encontrado(a)!',mterror,[mbok],0);
                    Self.ZerarTabela;
                    result:=False;
                    exit;
           End
           Else Self.NFEDIGITADA.TabelaparaObjeto;
    End;
    If(FieldByName('NFEDIGITADA').asstring<>'')
    Then Begin
           If (Self.NFEDIGITADA.LocalizaCodigo(FieldByName('NFEDIGITADA').asstring)=False)
           Then Begin
                    Messagedlg('NFEDIGITADA N�o encontrado(a)!',mterror,[mbok],0);
                    Self.ZerarTabela;
                    result:=False;
                    exit;
           End
           Else Self.NFEDIGITADA.TabelaparaObjeto;
    End;
    If(FieldByName('FERRAGEM').asstring<>'')
    Then Begin
           If (Self.FERRAGEM.LocalizaCodigo(FieldByName('FERRAGEM').asstring)=False)
           Then Begin
                    Messagedlg('FERRAGEM N�o encontrado(a)!',mterror,[mbok],0);
                    Self.ZerarTabela;
                    result:=False;
                    exit;
           End
           Else Self.FERRAGEM.TabelaparaObjeto;
    End;
    If(FieldByName('PERFILADO').asstring<>'')
    Then Begin
           If (Self.PERFILADO.LocalizaCodigo(FieldByName('PERFILADO').asstring)=False)
           Then Begin
                    Messagedlg('PERFILADO N�o encontrado(a)!',mterror,[mbok],0);
                    Self.ZerarTabela;
                    result:=False;
                    exit;
           End
           Else Self.PERFILADO.TabelaparaObjeto;
    End;
    If(FieldByName('DIVERSO').asstring<>'')
    Then Begin
           If (Self.DIVERSO.LocalizaCodigo(FieldByName('DIVERSO').asstring)=False)
           Then Begin
                    Messagedlg('DIVERSO N�o encontrado(a)!',mterror,[mbok],0);
                    Self.ZerarTabela;
                    result:=False;
                    exit;
           End
           Else Self.DIVERSO.TabelaparaObjeto;
    End;
    If(FieldByName('VIDRO').asstring<>'')
    Then Begin
           If (Self.VIDRO.LocalizaCodigo(FieldByName('VIDRO').asstring)=False)
           Then Begin
                    Messagedlg('VIDRO N�o encontrado(a)!',mterror,[mbok],0);
                    Self.ZerarTabela;
                    result:=False;
                    exit;
           End
           Else Self.VIDRO.TabelaparaObjeto;
    End;
    If(FieldByName('COMPONENTE').asstring<>'')
    Then Begin
           If (Self.COMPONENTE.LocalizaCodigo(FieldByName('COMPONENTE').asstring)=False)
           Then Begin
                    Messagedlg('COMPONENTE N�o encontrado(a)!',mterror,[mbok],0);
                    Self.ZerarTabela;
                    result:=False;
                    exit;
           End
           Else Self.COMPONENTE.TabelaparaObjeto;
    End;
    If(FieldByName('KITBOX').asstring<>'')
    Then Begin
           If (Self.KITBOX.LocalizaCodigo(FieldByName('KITBOX').asstring)=False)
           Then Begin
                    Messagedlg('KITBOX N�o encontrado(a)!',mterror,[mbok],0);
                    Self.ZerarTabela;
                    result:=False;
                    exit;
           End
           Else Self.KITBOX.TabelaparaObjeto;
    End;

    If(FieldByName('PERSIANA').asstring<>'')
    Then Begin

      If (Self.PERSIANA.LocalizaCodigo(FieldByName('PERSIANA').asstring)=False)
      Then Begin
              Messagedlg('PERSIANA N�o encontrado(a)!',mterror,[mbok],0);
              Self.ZerarTabela;
              result:=False;
              exit;
      End
      Else Self.PERSIANA.TabelaparaObjeto;
      
    End;

    If(FieldByName('COR').asstring<>'')
    Then Begin

      If (Self.PERSIANA.LocalizaCodigo(FieldByName('COR').asstring)=False)
      Then Begin
              Messagedlg('COR N�o encontrado(a)!',mterror,[mbok],0);
              Self.ZerarTabela;
              result:=False;
              exit;
      End
      Else Self.cor.TabelaparaObjeto;

    End;
    
    Self.QUANTIDADE:=fieldbyname('QUANTIDADE').asstring;
    Self.VALOR:=fieldbyname('VALOR').asstring;
    Self.DESCONTO:=fieldbyname('DESCONTO').asstring;
    Self.VALORFRETE:=fieldbyname('VALORFRETE').asstring;
    Self.VALORSEGURO:=fieldbyname('VALORSEGURO').asstring;
    Self.VALORTOTAL:=fieldbyname('VALORTOTAL').asstring;
    Self.VALORFINAL:=fieldbyname('VALORFINAL').asstring;

    Self.VALORIPI:=fieldbyname('VALORIPI').asstring;
    Self.VALORICMS_ST:=fieldbyname('VALORICMS_ST').asstring;

    self.DESCRICAO:=fieldbyname('DESCRICAO').AsString;
    self.MATERIAL:=fieldbyname('MATERIAL').AsString;
    self.CFOP:=fieldbyname('CFOP').AsString;

    result:=True;
    
  End;
  
end;


Procedure TObjPRODNFEDIGITADA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('NFEDIGITADA').asstring:=Self.NFEDIGITADA.GET_CODIGO;
        ParamByName('FERRAGEM').asstring:=Self.FERRAGEM.GET_CODIGO;
        ParamByName('PERFILADO').asstring:=Self.PERFILADO.GET_CODIGO;
        ParamByName('DIVERSO').asstring:=Self.DIVERSO.GET_CODIGO;
        ParamByName('VIDRO').asstring:=Self.VIDRO.GET_CODIGO;
        ParamByName('COMPONENTE').asstring:=Self.COMPONENTE.GET_CODIGO;
        ParamByName('KITBOX').asstring:=Self.KITBOX.GET_CODIGO;
        ParamByName('PERSIANA').asstring:=Self.PERSIANA.GET_CODIGO;
        ParamByName('COR').AsString:=Self.COR.Get_Codigo;
        ParamByName('QUANTIDADE').asstring:=virgulaparaponto(Self.QUANTIDADE);
        ParamByName('VALOR').asstring:=virgulaparaponto(Self.VALOR);
        ParamByName('DESCONTO').asstring:=virgulaparaponto(Self.DESCONTO);
        ParamByName('VALORFRETE').asstring:=virgulaparaponto(Self.VALORFRETE);
        ParamByName('VALORSEGURO').asstring:=virgulaparaponto(Self.VALORSEGURO);
        ParamByName('VALORTOTAL').asstring:=virgulaparaponto(Self.VALORTOTAL);
        ParamByName('VALORFINAL').asstring:=virgulaparaponto(Self.VALORFINAL);

        ParamByName('valoripi').asstring:=virgulaparaponto(Self.VALORIPI);
        ParamByName('valoricms_st').asstring:=virgulaparaponto(Self.VALORICMS_ST);

        ParamByName('DESCRICAO').AsString:=self.DESCRICAO;
        ParamByName('MATERIAL').AsString:=self.MATERIAL;
        ParamByName('cfop').AsString:=self.CFOP;



  End;
End;

//***********************************************************************

function TObjPRODNFEDIGITADA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;

 Self.ObjetoParaTabela;

 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPRODNFEDIGITADA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        NFEDIGITADA.ZerarTabela;
        FERRAGEM.ZerarTabela;
        PERFILADO.ZerarTabela;
        DIVERSO.ZerarTabela;
        VIDRO.ZerarTabela;
        COMPONENTE.ZerarTabela;
        KITBOX.ZerarTabela;
        PERSIANA.ZerarTabela;
        self.COR.ZerarTabela;
        QUANTIDADE:='';
        VALOR:='';
        DESCONTO:='';
        VALORFRETE:='';
        VALORSEGURO:='';
        VALORTOTAL:='';
        VALORFINAL:='';
        VALORIPI:='';
        VALORICMS_ST:='';
        self.DESCRICAO:='';
        self.MATERIAL:='';
        Self.CFOP:='';

     End;
end;

Function TObjPRODNFEDIGITADA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
  
      If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';


  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPRODNFEDIGITADA.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

    {If (Self.NFEDIGITADA.LocalizaCodigo(Self.NFEDIGITADA.Get_CODIGO)=False)
    Then Mensagem:=mensagem+'/ NFEDIGITADA n�o Encontrado!';

    If (Self.FERRAGEM.LocalizaCodigo(Self.FERRAGEM.Get_CODIGO)=False)
    Then Mensagem:=mensagem+'/ FERRAGEM n�o Encontrado!';

    If (Self.PERFILADO.LocalizaCodigo(Self.PERFILADO.Get_CODIGO)=False)
    Then Mensagem:=mensagem+'/ PERFILADO n�o Encontrado!';

    If (Self.DIVERSO.LocalizaCodigo(Self.DIVERSO.Get_CODIGO)=False)
    Then Mensagem:=mensagem+'/ DIVERSO n�o Encontrado!';

    If (Self.VIDRO.LocalizaCodigo(Self.VIDRO.Get_CODIGO)=False)
    Then Mensagem:=mensagem+'/ VIDRO n�o Encontrado!';

    If (Self.COMPONENTE.LocalizaCodigo(Self.COMPONENTE.Get_CODIGO)=False)
    Then Mensagem:=mensagem+'/ COMPONENTE n�o Encontrado!';

    If (Self.KITBOX.LocalizaCodigo(Self.KITBOX.Get_CODIGO)=False)
    Then Mensagem:=mensagem+'/ KITBOX n�o Encontrado!';

    If (Self.PERSIANA.LocalizaCodigo(Self.PERSIANA.Get_CODIGO)=False)
    Then Mensagem:=mensagem+'/ PERSIANA n�o Encontrado!';


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;}

     result:=true;


End;

function TObjPRODNFEDIGITADA.VerificaNumericos: Boolean;
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
     try
        If (Self.NFEDIGITADA.Get_Codigo<>'')
        Then Strtoint(Self.NFEDIGITADA.Get_Codigo);
     Except
           Mensagem:=mensagem+'/NFEDIGITADA';
     End;

     try
        If (Self.FERRAGEM.Get_Codigo<>'')
        Then Strtoint(Self.FERRAGEM.Get_Codigo);
     Except
           Mensagem:=mensagem+'/FERRAGEM';
     End;
     try
        If (Self.PERFILADO.Get_Codigo<>'')
        Then Strtoint(Self.PERFILADO.Get_Codigo);
     Except
           Mensagem:=mensagem+'/PERFILADO';
     End;
     try
        If (Self.DIVERSO.Get_Codigo<>'')
        Then Strtoint(Self.DIVERSO.Get_Codigo);
     Except
           Mensagem:=mensagem+'/DIVERSO';
     End;
     try
        If (Self.VIDRO.Get_Codigo<>'')
        Then Strtoint(Self.VIDRO.Get_Codigo);
     Except
           Mensagem:=mensagem+'/VIDRO';
     End;
     try
        If (Self.COMPONENTE.Get_Codigo<>'')
        Then Strtoint(Self.COMPONENTE.Get_Codigo);
     Except
           Mensagem:=mensagem+'/COMPONENTE';
     End;
     try
        If (Self.KITBOX.Get_Codigo<>'')
        Then Strtoint(Self.KITBOX.Get_Codigo);
     Except
           Mensagem:=mensagem+'/KITBOX';
     End;
     try
        If (Self.PERSIANA.Get_Codigo<>'')
        Then Strtoint(Self.PERSIANA.Get_Codigo);
     Except
           Mensagem:=mensagem+'/PERSIANA';
     End;

     try
        If (Self.COR.Get_Codigo<>'')
        Then Strtoint(Self.COR.Get_Codigo);
     Except
           Mensagem:=mensagem+'/COR';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPRODNFEDIGITADA.VerificaData: Boolean;
var
Mensagem:string;
begin

   Result:=False;
   mensagem:='';

   If Mensagem<>''
   Then Begin
         Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
         exit;
   End;
   result:=true;

end;

function TObjPRODNFEDIGITADA.VerificaFaixa: boolean;
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPRODNFEDIGITADA.LocalizaCodigo(parametro: string): boolean;
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PRODNFEDIGITADA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,NFEDIGITADA,FERRAGEM,PERFILADO,DIVERSO');
           SQL.ADD(' ,VIDRO,COMPONENTE,KITBOX,PERSIANA,COR,QUANTIDADE,VALOR,DESCONTO');
           SQL.ADD(' ,VALORFRETE,VALORSEGURO,VALORTOTAL,VALORFINAL,valoripi,valoricms_st,DataC');
           SQL.ADD(' ,UserC,DataM,UserM');
           SQL.ADD(' from  TABPRODNFEDIGITADA');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPRODNFEDIGITADA.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjPRODNFEDIGITADA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjPRODNFEDIGITADA.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjPRODNFEDIGITADA.create(Owner:TComponent);
begin

        Self.Owner := Owner;
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        self.queryProdutos:=TIBQuery.Create(nil);
        self.queryProdutos.Database:=FDataModulo.IBDatabase;
        self.ObjNotafiscalObjetos:=TObjNotafiscalObjetos.Create(Self.Owner);

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.NFEDIGITADA:=TOBJNFEDIGITADA .create;
        Self.FERRAGEM:=TOBJFERRAGEM       .create;
        Self.PERFILADO:=TOBJPERFILADO      .create;
        Self.DIVERSO:=TOBJDIVERSO        .create;
        Self.VIDRO:=TOBJVIDRO          .create;
        Self.COMPONENTE:=TOBJCOMPONENTE     .create;
        Self.KITBOX:=TOBJKITBOX         .create;
        Self.PERSIANA:=TOBJPERSIANA       .create;
        self.COR:=TObjCOR.Create;

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABPRODNFEDIGITADA(CODIGO,NFEDIGITADA');
                InsertSQL.add(' ,FERRAGEM,PERFILADO,DIVERSO,VIDRO,COMPONENTE,KITBOX');
                InsertSQL.add(' ,PERSIANA,COR,QUANTIDADE,VALOR,DESCONTO,VALORFRETE');
                InsertSQL.add(' ,VALORSEGURO,VALORTOTAL,VALORFINAL,DESCRICAO,MATERIAL,valoripi,valoricms_st,cfop)');
                InsertSQL.add('values (:CODIGO,:NFEDIGITADA,:FERRAGEM');
                InsertSQL.add(' ,:PERFILADO,:DIVERSO,:VIDRO,:COMPONENTE,:KITBOX');
                InsertSQL.add(' ,:PERSIANA,:COR,:QUANTIDADE,:VALOR,:DESCONTO,:VALORFRETE');
                InsertSQL.add(' ,:VALORSEGURO,:VALORTOTAL,:VALORFINAL,:DESCRICAO,:MATERIAL,:valoripi,:valoricms_st,:cfop)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABPRODNFEDIGITADA set CODIGO=:CODIGO');
                ModifySQL.add(',NFEDIGITADA=:NFEDIGITADA,FERRAGEM=:FERRAGEM,PERFILADO=:PERFILADO');
                ModifySQL.add(',DIVERSO=:DIVERSO,VIDRO=:VIDRO,COMPONENTE=:COMPONENTE');
                ModifySQL.add(',KITBOX=:KITBOX,PERSIANA=:PERSIANA,COR=:COR');
                ModifySQL.add(',QUANTIDADE=:QUANTIDADE,VALOR=:VALOR,DESCONTO=:DESCONTO');
                ModifySQL.add(',VALORFRETE=:VALORFRETE,VALORSEGURO=:VALORSEGURO,VALORTOTAL=:VALORTOTAL');
                ModifySQL.add(',VALORFINAL=:VALORFINAL,DESCRICAO=:DESCRICAO,MATERIAL=:MATERIAL,valoripi=:valoripi,valoricms_st=:valoricms_st,cfop=:cfop');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABPRODNFEDIGITADA where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPRODNFEDIGITADA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPRODNFEDIGITADA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPRODNFEDIGITADA');
     Result:=Self.ParametroPesquisa;
end;

function TObjPRODNFEDIGITADA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de PRODNFEDIGITADA ';
end;


function TObjPRODNFEDIGITADA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPRODNFEDIGITADA,1) CODIGO FROM RDB$DATABASE');

           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPRODNFEDIGITADA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    FreeAndNil(self.queryProdutos);
    Self.NFEDIGITADA.FREE;
    Self.FERRAGEM.FREE;
    Self.PERFILADO.FREE;
    Self.DIVERSO.FREE;
    Self.VIDRO.FREE;
    Self.COMPONENTE.FREE;
    Self.KITBOX.FREE;
    Self.PERSIANA.FREE;
    SELF.COR.Free;
    self.ObjNotafiscalObjetos.Free;
    FreeAndNil(objTransNFE);

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPRODNFEDIGITADA.RetornaCampoCodigo: string;
begin
      result:='codigo';

end;


function TObjPRODNFEDIGITADA.RetornaCampoNome: string;
begin
      result:='';

end;

procedure TObjPRODNFEDIGITADA.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjPRODNFEDIGITADA.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjPRODNFEDIGITADA.Submit_QUANTIDADE(parametro: string);
begin
        Self.QUANTIDADE:=Parametro;
end;
function TObjPRODNFEDIGITADA.Get_QUANTIDADE: string;
begin
        Result:=Self.QUANTIDADE;
end;
procedure TObjPRODNFEDIGITADA.Submit_VALOR(parametro: string);
begin
        Self.VALOR:=Parametro;
end;
function TObjPRODNFEDIGITADA.Get_VALOR: string;
begin
        Result:=Self.VALOR;
end;
procedure TObjPRODNFEDIGITADA.Submit_DESCONTO(parametro: string);
begin
  if (parametro = '') then
    parametro:='0';
        Self.DESCONTO:=Parametro;
end;
function TObjPRODNFEDIGITADA.Get_DESCONTO: string;
begin
        Result:=Self.DESCONTO;
end;
procedure TObjPRODNFEDIGITADA.Submit_VALORFRETE(parametro: string);
begin

  if (parametro = '') then
    parametro:='0';
    
  Self.VALORFRETE:=Parametro;
end;
function TObjPRODNFEDIGITADA.Get_VALORFRETE: string;
begin
        Result:=Self.VALORFRETE;
end;
procedure TObjPRODNFEDIGITADA.Submit_VALORSEGURO(parametro: string);
begin
  if (parametro = '') then
    parametro:='0';
        Self.VALORSEGURO:=Parametro;
end;
function TObjPRODNFEDIGITADA.Get_VALORSEGURO: string;
begin
        Result:=Self.VALORSEGURO;
end;
procedure TObjPRODNFEDIGITADA.Submit_VALORTOTAL(parametro: string);
begin
        Self.VALORTOTAL:=Parametro;
end;
function TObjPRODNFEDIGITADA.Get_VALORTOTAL: string;
begin
        Result:=Self.VALORTOTAL;
end;
procedure TObjPRODNFEDIGITADA.Submit_VALORFINAL(parametro: string);
begin
        Self.VALORFINAL:=Parametro;
end;
function TObjPRODNFEDIGITADA.Get_VALORFINAL: string;
begin
        Result:=Self.VALORFINAL;
end;



procedure TObjPRODNFEDIGITADA.EdtNFEDIGITADAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;

begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.NFEDIGITADA.Get_Pesquisa,Self.NFEDIGITADA.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.NFEDIGITADA.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.NFEDIGITADA.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.NFEDIGITADA.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjPRODNFEDIGITADA.EdtNFEDIGITADAExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.NFEDIGITADA.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.NFEDIGITADA.tabelaparaobjeto;
     
End;

procedure TObjPRODNFEDIGITADA.EdtFERRAGEMExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.FERRAGEM.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.FERRAGEM.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.FERRAGEM.Get_Descricao;
End;

procedure TObjPRODNFEDIGITADA.EdtFERRAGEMKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FFERRAGEM       :TFFERRAGEM;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FFERRAGEM       :=TFFERRAGEM       .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.FERRAGEM.Get_Pesquisa,Self.FERRAGEM.Get_TituloPesquisa,FFERRAGEM)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FERRAGEM.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.FERRAGEM.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FERRAGEM.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FFERRAGEM       );
     End;
end;



procedure TObjPRODNFEDIGITADA.EdtPERFILADOExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PERFILADO.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PERFILADO.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.PERFILADO.Get_Descricao;
End;

procedure TObjPRODNFEDIGITADA.EdtPERFILADOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPERFILADO      :TFPERFILADO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPERFILADO      :=TFPERFILADO      .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PERFILADO.Get_Pesquisa,Self.PERFILADO.Get_TituloPesquisa,FPERFILADO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PERFILADO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.PERFILADO.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PERFILADO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FPERFILADO      );
     End;
end;
procedure TObjPRODNFEDIGITADA.EdtDIVERSOExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.DIVERSO.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.DIVERSO.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.DIVERSO.Get_Descricao;
End;

procedure TObjPRODNFEDIGITADA.EdtDIVERSOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FDIVERSO        :TFDIVERSO ;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FDIVERSO        :=TFDIVERSO        .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.DIVERSO.Get_Pesquisa,Self.DIVERSO.Get_TituloPesquisa,FDIVERSO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.DIVERSO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.DIVERSO.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.DIVERSO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FDIVERSO        );
     End;
end;
procedure TObjPRODNFEDIGITADA.EdtVIDROExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.VIDRO.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.VIDRO.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.VIDRO.Get_Descricao;
End;

procedure TObjPRODNFEDIGITADA.EdtVIDROKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FVIDRO          :TFVIDRO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FVIDRO          :=TFVIDRO          .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.VIDRO.Get_Pesquisa,Self.VIDRO.Get_TituloPesquisa,FVIDRO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.VIDRO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.VIDRO.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.VIDRO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FVIDRO          );
     End;
end;
procedure TObjPRODNFEDIGITADA.EdtCOMPONENTEExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.COMPONENTE.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.COMPONENTE.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.COMPONENTE.Get_Descricao;
End;

procedure TObjPRODNFEDIGITADA.EdtCOMPONENTEKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FCOMPONENTE     :TFCOMPONENTE     ;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCOMPONENTE     :=TFCOMPONENTE     .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.COMPONENTE.Get_Pesquisa,Self.COMPONENTE.Get_TituloPesquisa,FCOMPONENTE)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.COMPONENTE.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.COMPONENTE.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.COMPONENTE.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCOMPONENTE     );
     End;
end;

procedure TObjPRODNFEDIGITADA.EdtCFOPKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabcfop','Pesquisa de CFOP',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;


                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPRODNFEDIGITADA.EdtKITBOXExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.KITBOX.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.KITBOX.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.KITBOX.Get_Descricao;
End;

procedure TObjPRODNFEDIGITADA.EdtKITBOXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FKITBOX         :TFKITBOX;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FKITBOX         :=TFKITBOX         .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.KITBOX.Get_Pesquisa,Self.KITBOX.Get_TituloPesquisa,FKITBOX)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.KITBOX.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.KITBOX.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.KITBOX.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FKITBOX         );
     End;
end;
procedure TObjPRODNFEDIGITADA.EdtPERSIANAExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PERSIANA.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PERSIANA.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.PERSIANA.GET_NOME;
End;
procedure TObjPRODNFEDIGITADA.EdtPERSIANAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPERSIANA       :TFPERSIANA       ;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPERSIANA       :=TFPERSIANA       .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PERSIANA.Get_Pesquisa,Self.PERSIANA.Get_TituloPesquisa,FPERSIANA)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PERSIANA.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.PERSIANA.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PERSIANA.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FPERSIANA       );
     End;
end;



procedure TObjPRODNFEDIGITADA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPRODNFEDIGITADA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



procedure TObjPRODNFEDIGITADA.desativaGrid;
begin

  self.retornaProdutos ('-1');

end;

procedure TObjPRODNFEDIGITADA.retornaProdutos(pCodigo: string);
begin

  self.queryProdutos.Close;
  self.queryProdutos.sql.Clear;

  Self.queryProdutos.SQL.Add ('select v.*');
  Self.queryProdutos.SQL.Add ('from viewProdNFEDigitada v');
  Self.queryProdutos.SQL.Add ('where v.nfedigitada = '+pCodigo);

  self.queryProdutos.Open;

end;

procedure TObjPRODNFEDIGITADA.excluiProdutos(pNfeDigitada: string);
begin

  with self.Objquery do
  begin

    Close;
    SQL.Clear;
    sql.Add ('delete from TABPRODNFEDIGITADA');
    sql.Add ('where NFEDIGITADA = '+pNfeDigitada);

    try
      ExecSQL;
    except
      FDataModulo.IBTransaction.RollbackRetaining;
    end;

  end;

end;

function TObjPRODNFEDIGITADA.get_DESCRICAO: string;
begin

  result:=self.DESCRICAO;

end;

function TObjPRODNFEDIGITADA.get_MATERIAL: string;
begin

  result:=self.MATERIAL;

end;

procedure TObjPRODNFEDIGITADA.submit_DESCRICAO(parametro: string);
begin

  self.DESCRICAO:=parametro;

end;

procedure TObjPRODNFEDIGITADA.submit_MATERIAL(parametro: string);
begin

  self.MATERIAL:=parametro;

end;

procedure TObjPRODNFEDIGITADA.opcoes(pCodigo: string);
var
  strNota:TStringList;
  pNFe:string;
  pNF:string;
  reciboEnvio:string;
begin

  With FOpcaorel do
  Begin
      RgOpcoes.Items.clear;
      RgOpcoes.Items.add('Gera Nf-e devolu��o');{0}
      RgOpcoes.Items.add('Gera Nf-e entrada');{1}
      RgOpcoes.Items.add('Gera NF-e complementar');            {2}
      RgOpcoes.Items.add('Gera NF-e em modo de conting�ncia'); {3}
      RgOpcoes.Items.add('Transmitir NF-e em conting�ncia');   {4}
      showmodal;

      if (tag=0)
      Then exit;

      case RgOpcoes.ItemIndex of

        0:begin

          self.geraNFeDevolucao(pCodigo);

        end;

        1:begin

          self.geraNFeEntrada(pCodigo);

        end;

        2:begin

          self.geraNfeComplementar(pCodigo);

        end;
        3:begin
          strNota := TStringList.Create;
          try
            if (pCodigo<>'') and (self.NfeDigitada.status=dsinactive) then
            begin

              self.submit_nfeEntrada(false);
              self.submit_nfeDevolucao(false);
              self.submit_nfeComplementar(false);
              strNota.Add (self.get_proximaNFE());
              if (self.GeraNotaFiscalParaNFE(strNota,pcodigo,'1',999,True)=false) then
              begin
                MensagemSucesso('N�o foi possivel gerar NF-e');
                FDataModulo.IBTransaction.RollbackRetaining
              end
              else
              begin
                MensagemSucesso('NF-e gerada em modo de conting�ncia');
                FDataModulo.IBTransaction.CommitRetaining;
              end;

            end;
          finally
            FreeAndNil (strNota);
          end;
        end;
        4:begin
          NfeDigitada.LocalizaCodigo(pcodigo);
          NfeDigitada.tabelaparaObjeto;

          pNF := NfeDigitada.nota.Get_CODIGO;
          pNFe := NfeDigitada.NOTA.Nfe.Get_CODIGO;

          NfeDigitada.nota.Nfe.LocalizaCodigo(pNFe);
          NfeDigitada.nota.Nfe.TabelaparaObjeto;

          if not objTransNFE.transmiteXMLContingente(reciboEnvio,NfeDigitada.nota.Nfe.get_arquivo_xml) then
          begin
            MensagemSucesso('NF-e n�o transmitida');
            FDataModulo.IBTransaction.RollbackRetaining;
          end
          else
          begin
            {editando a NF-e para G}
            NfeDigitada.nota.Nfe.Status := dsEdit;
            NfeDigitada.nota.Nfe.Submit_STATUSNOTA('G');
            NfeDigitada.nota.Nfe.Submit_ReciboEnvio(reciboEnvio);

            if not NfeDigitada.nota.Nfe.Salvar(false) then
            begin
              MensagemErro('Erro ao editar nfe para gerada');
              Exit;
            end;

            {editando a NF}
            NFEDIGITADA.NOTA.Status:=dsEdit;
            NfeDigitada.nota.LocalizaCodigo(pNF);
            NfeDigitada.nota.TabelaparaObjeto;
            NfeDigitada.nota.Submit_Situacao('I');

            if not NfeDigitada.nota.Salvar(false) then
            begin
              MensagemErro('Erro ao editar NF para gerada');
              Exit;
            end;
            MensagemSucesso('NF-e transmitida com sucesso');
            FDataModulo.IBTransaction.CommitRetaining;
          end;

        end;



      End;
  End;

end;

procedure TObjPRODNFEDIGITADA.geraNfeComplementar(pcodigo: string);
var
  formReferencia:TfReferenciaNFE;
  strNota:TStringList;
begin

  formReferencia:=TfReferenciaNFE.Create(nil);
  strNota := TStringList.Create;


  try
    formReferencia.ShowModal;

    if (formReferencia.Tag = -1) then
      Exit;


    self.adicional_ref   :=formReferencia.get_dadosAdicionais;                                                                
    self.refNFe_ref      :=formReferencia.edtChaveAcesso.Text;                                                                

    if (self.refNFe_ref = '') then
    begin

      MensagemAviso('Uma NF-e complementar deve ser refer�nciada por uma NF-e (c�digo fiscal (55))');
      Exit;

    end;

    if (pcodigo<>'') and (self.NfeDigitada.status=dsinactive) then
    begin
      strNota.Add (self.get_proximaNFE());
      self.NfeEntrada:=False;
      Self.nfeDevolucao:=False;
      self.nfeComplementar:=True;

      if (self.GeraNotaFiscalParaNFE(strNota,pcodigo,'1',999)=false) then
      begin
        MensagemSucesso('N�o foi possivel gerar NF-e');
        FDataModulo.IBTransaction.RollbackRetaining
      end
      else
      begin
        MensagemSucesso('NF-e gerada');
        FDataModulo.IBTransaction.CommitRetaining;
      end;

    end;

  finally
    FreeAndNil (strNota);
    FreeAndNil(formReferencia);

  end;


end;


function TObjPRODNFEDIGITADA.get_valorTotal_2: string;
begin

  Result := CurrToStr (StrToCurr (tira_ponto(Get_Quantidade)) * StrToCurr (tira_ponto(get_valor)));

end;

function TObjPRODNFEDIGITADA.get_valorFinal_2: string;
begin

  Result := CurrToStr (strtocurr (tira_ponto(Get_Quantidade)) * StrToCurr (tira_ponto(Get_Valor)) - StrToCurr (tira_ponto(Get_Desconto ())) );

end;

procedure TObjPRODNFEDIGITADA.geraNfeDevolucao(pcodigo: string);
var
  statusnfe:Integer;
  adicionalNFE:string;
  formReferencia:TfReferenciaNFE;
  strNota:TStringList;
  msg:string;
  impostoAutomatico:string;
begin

   formReferencia:=TfReferenciaNFE.Create(nil);
   
   try

    formReferencia.ShowModal;

    if (formReferencia.Tag = -1) then
    begin

      Self.nfeDevolucao:=False;
      self.nfeComplementar:=False;
      self.NfeEntrada:=False;
      Exit;

    end;

    self.adicional_ref:=formReferencia.get_dadosAdicionais();
    self.refNFe_ref:='';


    if (formReferencia.get_nfeRef) then
    begin

      self.refNFe_ref:=formReferencia.edtChaveAcesso.Text;
      tipoRef := '55';

    end
    else
    begin

     tipoRef := '1';
     self.cUF_ref:=formReferencia.edtCUF.Text;
     self.AAMM_ref:=formReferencia.edtAMM.Text;
     self.CNPJ_ref:=formReferencia.edtCNPJ.Text;
     self.modelo_ref:=formReferencia.edtModelo.Text;
     self.serie_ref:='0';
     self.nNF_ref:=formReferencia.edtNumeroNF.Text;

    end;

     if (pcodigo<>'') and (self.NfeDigitada.status=dsinactive) then
     begin

        strNota := TStringList.Create;
        strNota.Add (self.get_proximaNFE());
        self.NfeEntrada:=False;
        self.nfeComplementar:=False;
        self.nfeDevolucao:=true;
        try

          if (self.GeraNotaFiscalParaNFE(strNota,pcodigo,'3',999)=false) then
          begin
            MensagemErro('Erro na gera��o da NF-e de Devolu��o');
            FDataModulo.IBTransaction.RollbackRetaining
          end
          else
          begin
            FDataModulo.IBTransaction.CommitRetaining;
          end;

        finally
          FreeAndNil (strNota);
        end;
        
     end;

   finally
    FreeAndNil(freFerenciaNFE);
   end;


end;

function TObjPRODNFEDIGITADA.gravaMateriaisImpostoManual(pNfedigitada: string): Boolean;
var
  objVenda,ObjICMS:TObject;
begin

  result := False;

  {FERRAGEM--------------}

  Objquery.Active:=false;
  Objquery.SQL.Text :=

  'select '+

  'f.descricao,f.unidade,prod.ferragem,f.ncm,prod.quantidade,prod.valorfinal, '+
  'prod.valor,f.referencia,prod.cor,f.peso,f.classificacaofiscal,prod.desconto, '+
  'prod.valorfrete,prod.valorseguro,prod.percentualipi,prod.valorbasecalculo,prod.percentualicms,prod.valoricms, '+
  'prod.percentualreducaobc,prod.percentualpis,prod.valorpis,prod.percentualcofins,prod.valorcofins, '+
  'prod.percentualreducaobc_st,prod.valorbasecalculo_st,prod.percentualicms_st,prod.valoricms_st, '+
  'prod.valoripi,prod.valorbasecalculo_ipi,prod.valoroutros,prod.bcpis,prod.bccofins,prod.cstpis,prod.cstcofins,prod.cstipi, '+
  'prod.cst,prod.percentualtributo,prod.vtottrib,prod.csosn,prod.cfop '+

  'from tabferragem f '+
  'join tabprodnfedigitada prod on prod.ferragem = f.codigo '+
  'where prod.nfedigitada = '+pNfedigitada;

  try

    Objquery.Active := True;

    objVenda := TObjMateriaisVenda.create;

    while not (Objquery.Eof) do
    begin

      TObjMateriaisVenda(objVenda).state:=dsInsert;
      TObjMateriaisVenda(objVenda).Submit_Codigo             (TObjMateriaisVenda(objVenda).Get_NovoCodigo());
      TObjMateriaisVenda(objVenda).Submit_Descricao          (Objquery.fieldbyname('DESCRICAO').AsString);
      TObjMateriaisVenda(objVenda).Submit_Unidade            (Objquery.fieldbyname('UNIDADE').AsString);
      TObjMateriaisVenda(objVenda).Submit_Ferragem           (Objquery.fieldbyname('ferragem').AsString);
      TObjMateriaisVenda(objVenda).Submit_NCM                (Objquery.fieldbyname('NCM').AsString);
      TObjMateriaisVenda(objVenda).Submit_QUANTIDADE         (Objquery.fieldbyname('quantidade').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALORFINAL         (Objquery.fieldbyname('valorfinal').AsString);
      TObjMateriaisVenda(objVenda).Submit_ValorUnitario      (Objquery.fieldbyname('valor').AsString);
      TObjMateriaisVenda(objVenda).Submit_Desconto           (Objquery.fieldbyname('DESCONTO').AsString);
      TObjMateriaisVenda(objVenda).Nfedigitada.Submit_CODIGO (pNfedigitada);

      TObjMateriaisVenda(objVenda).Submit_Vidro      ('');
      TObjMateriaisVenda(objVenda).Submit_Perfilado  ('');
      TObjMateriaisVenda(objVenda).Submit_Diverso    ('');
      TObjMateriaisVenda(objVenda).Submit_Componente ('');
      TObjMateriaisVenda(objVenda).Submit_KitBox     ('');
      TObjMateriaisVenda(objVenda).Submit_Persiana   ('');
      TObjMateriaisVenda(objVenda).Submit_indpag     (get_campoTabela('indpag','codigo','tabnfedigitada',pNfedigitada));


      TObjMateriaisVenda(objVenda).Submit_Referencia     (Objquery.fieldbyname('referencia').AsString);
      TObjMateriaisVenda(objVenda).Submit_Cor            (get_campoTabela('descricao','codigo','tabcor',Objquery.fieldbyname('cor').AsString));
      TObjMateriaisVenda(objVenda).Submit_CodigoCor      (Objquery.fieldbyname('cor').AsString);
      TObjMateriaisVenda(objVenda).Submit_PesoUnitario   (Objquery.fieldbyname('peso').AsString);
      TObjMateriaisVenda(objVenda).Submit_Material       ('1');


      TObjMateriaisVenda(objVenda).Submit_VALORFRETE         (Objquery.fieldbyname('valorfrete').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALORSEGURO        (Objquery.fieldbyname('valorseguro').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualipi      (Objquery.fieldbyname('percentualipi').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_ICMS            (Objquery.fieldbyname('valorbasecalculo').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualicms     (Objquery.fieldbyname('percentualicms').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_ICMS         (Objquery.fieldbyname('valoricms').AsString);
      TObjMateriaisVenda(objVenda).Submit_REDUCAOBASECALCULO (Objquery.fieldbyname('percentualreducaobc').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualpis      (Objquery.fieldbyname('percentualpis').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_PIS          (Objquery.fieldbyname('valorpis').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualcofins   (Objquery.fieldbyname('percentualcofins').AsString);

      TObjMateriaisVenda(objVenda).Submit_VALOR_COFINS          (Objquery.fieldbyname('valorcofins').AsString);
      TObjMateriaisVenda(objVenda).submit_reducaobasecalculo_st (Objquery.fieldbyname('percentualreducaobc_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_ICMS_ST            (Objquery.fieldbyname('valorbasecalculo_st').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualicms_st     (Objquery.fieldbyname('percentualicms_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_ICMS_ST         (Objquery.fieldbyname('valoricms_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_IPI             (Objquery.fieldbyname('valoripi').AsString);

      TObjMateriaisVenda(objVenda).Submit_BC_IPI      (Objquery.fieldbyname('valorbasecalculo_ipi').AsString);
      TObjMateriaisVenda(objVenda).submit_valoroutros (Objquery.fieldbyname('valoroutros').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_PIS      (Objquery.fieldbyname('bcpis').AsString);

      TObjMateriaisVenda(objVenda).Submit_BC_COFINS  (Objquery.fieldbyname('bccofins').AsString);
      TObjMateriaisVenda(objVenda).submit_cstpis     (Objquery.fieldbyname('cstpis').AsString);
      TObjMateriaisVenda(objVenda).submit_cstcofins  (Objquery.fieldbyname('cstcofins').AsString);
      TObjMateriaisVenda(objVenda).Submit_cstipi     (Objquery.fieldbyname('cstipi').AsString);

      TObjMateriaisVenda(objVenda).Submit_SITUACAOTRIBUTARIA_TABELAA (get_campoTabela('SITUACAOTRIBUTARIA_TABELAA','codigo','TABFERRAGEM',Objquery.fieldbyname('ferragem').AsString));
      TObjMateriaisVenda(objVenda).Submit_SITUACAOTRIBUTARIA_TABELAB (Objquery.fieldbyname('cst').AsString);
      TObjMateriaisVenda(objVenda).Submit_percentualtributo          (Objquery.fieldbyname('percentualtributo').AsString);
      TObjMateriaisVenda(objVenda).Submit_VTOTTRIB                   (Objquery.fieldbyname('vtottrib').AsString);
      TObjMateriaisVenda(objVenda).submit_CSOSN                      (Objquery.fieldbyname('csosn').AsString);
      TObjMateriaisVenda(objVenda).Submit_CFOP                       (Objquery.fieldbyname('cfop').AsString);

      TObjMateriaisVenda(objVenda).Submit_NotaFiscal('');
      TObjMateriaisVenda(objVenda).Submit_classificacaofiscal     (Objquery.fieldbyname('classificacaofiscal').AsString);

      if not (TObjMateriaisVenda(objVenda).Salvar(false)) then
      begin
       messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem '+Objquery.fieldbyname('ferragem').asstring,mterror,[mbok],0);
       exit;
      End;

      Objquery.Next;

    end;


    {PERFILADO--------------}
    Objquery.Active:=false;
    Objquery.SQL.Text :=

    'select '+

    'p.descricao,p.unidade,prod.perfilado,p.ncm,prod.quantidade,prod.valorfinal, '+
    'prod.valor,p.referencia,prod.cor,p.peso,p.classificacaofiscal,prod.desconto, '+

    'prod.valorfrete,prod.valorseguro,prod.percentualipi,prod.valorbasecalculo,prod.percentualicms,prod.valoricms, '+
    'prod.percentualreducaobc,prod.percentualpis,prod.valorpis,prod.percentualcofins,prod.valorcofins, '+
    'prod.percentualreducaobc_st,prod.valorbasecalculo_st,prod.percentualicms_st,prod.valoricms_st, '+
    'prod.valoripi,prod.valorbasecalculo_ipi,prod.valoroutros,prod.bcpis,prod.bccofins,prod.cstpis,prod.cstcofins,prod.cstipi, '+
    'prod.cst,prod.percentualtributo,prod.vtottrib,prod.csosn,prod.cfop '+

    'from tabperfilado p '+
    'join tabprodnfedigitada prod on prod.perfilado = p.codigo '+
    'where prod.nfedigitada ='+pNfedigitada;


    Objquery.Active := True;

    TObjMateriaisVenda(objVenda).ZerarTabela;

    while not (Objquery.Eof) do
    begin

      TObjMateriaisVenda(objVenda).state:=dsInsert;
      TObjMateriaisVenda(objVenda).Submit_Codigo             (TObjMateriaisVenda(objVenda).Get_NovoCodigo());
      TObjMateriaisVenda(objVenda).Submit_Descricao          (Objquery.fieldbyname('DESCRICAO').AsString);
      TObjMateriaisVenda(objVenda).Submit_Unidade            (Objquery.fieldbyname('UNIDADE').AsString);
      TObjMateriaisVenda(objVenda).Submit_Perfilado          (Objquery.fieldbyname('perfilado').AsString);
      TObjMateriaisVenda(objVenda).Submit_NCM                (Objquery.fieldbyname('NCM').AsString);
      TObjMateriaisVenda(objVenda).Submit_QUANTIDADE         (Objquery.fieldbyname('quantidade').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALORFINAL         (Objquery.fieldbyname('valorfinal').AsString);
      TObjMateriaisVenda(objVenda).Submit_ValorUnitario      (Objquery.fieldbyname('valor').AsString);
      TObjMateriaisVenda(objVenda).Submit_Desconto           (Objquery.fieldbyname('DESCONTO').AsString);
      TObjMateriaisVenda(objVenda).Nfedigitada.Submit_CODIGO (pNfedigitada);

      TObjMateriaisVenda(objVenda).Submit_Vidro      ('');
      TObjMateriaisVenda(objVenda).Submit_Ferragem   ('');
      TObjMateriaisVenda(objVenda).Submit_Diverso    ('');
      TObjMateriaisVenda(objVenda).Submit_Componente ('');
      TObjMateriaisVenda(objVenda).Submit_KitBox     ('');
      TObjMateriaisVenda(objVenda).Submit_Persiana   ('');
      TObjMateriaisVenda(objVenda).Submit_indpag     (get_campoTabela('indpag','codigo','tabnfedigitada',pNfedigitada));


      TObjMateriaisVenda(objVenda).Submit_Referencia     (Objquery.fieldbyname('referencia').AsString);
      TObjMateriaisVenda(objVenda).Submit_Cor            (get_campoTabela('descricao','codigo','tabcor',Objquery.fieldbyname('cor').AsString));
      TObjMateriaisVenda(objVenda).Submit_CodigoCor      (Objquery.fieldbyname('cor').AsString);
      TObjMateriaisVenda(objVenda).Submit_PesoUnitario   (Objquery.fieldbyname('peso').AsString);
      TObjMateriaisVenda(objVenda).Submit_Material       ('2');


      TObjMateriaisVenda(objVenda).Submit_VALORFRETE         (Objquery.fieldbyname('valorfrete').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALORSEGURO        (Objquery.fieldbyname('valorseguro').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualipi      (Objquery.fieldbyname('percentualipi').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_ICMS            (Objquery.fieldbyname('valorbasecalculo').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualicms     (Objquery.fieldbyname('percentualicms').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_ICMS         (Objquery.fieldbyname('valoricms').AsString);
      TObjMateriaisVenda(objVenda).Submit_REDUCAOBASECALCULO (Objquery.fieldbyname('percentualreducaobc').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualpis      (Objquery.fieldbyname('percentualpis').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_PIS          (Objquery.fieldbyname('valorpis').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualcofins   (Objquery.fieldbyname('percentualcofins').AsString);

      TObjMateriaisVenda(objVenda).Submit_VALOR_COFINS          (Objquery.fieldbyname('valorcofins').AsString);
      TObjMateriaisVenda(objVenda).submit_reducaobasecalculo_st (Objquery.fieldbyname('percentualreducaobc_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_ICMS_ST            (Objquery.fieldbyname('valorbasecalculo_st').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualicms_st     (Objquery.fieldbyname('percentualicms_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_ICMS_ST         (Objquery.fieldbyname('valoricms_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_IPI             (Objquery.fieldbyname('valoripi').AsString);

      TObjMateriaisVenda(objVenda).Submit_BC_IPI      (Objquery.fieldbyname('valorbasecalculo_ipi').AsString);
      TObjMateriaisVenda(objVenda).submit_valoroutros (Objquery.fieldbyname('valoroutros').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_PIS      (Objquery.fieldbyname('bcpis').AsString);

      TObjMateriaisVenda(objVenda).Submit_BC_COFINS  (Objquery.fieldbyname('bccofins').AsString);
      TObjMateriaisVenda(objVenda).submit_cstpis     (Objquery.fieldbyname('cstpis').AsString);
      TObjMateriaisVenda(objVenda).submit_cstcofins  (Objquery.fieldbyname('cstcofins').AsString);
      TObjMateriaisVenda(objVenda).Submit_cstipi     (Objquery.fieldbyname('cstipi').AsString);

      TObjMateriaisVenda(objVenda).Submit_SITUACAOTRIBUTARIA_TABELAA (get_campoTabela('SITUACAOTRIBUTARIA_TABELAA','codigo','TABPERFILADO',Objquery.fieldbyname('perfilado').AsString));
      TObjMateriaisVenda(objVenda).Submit_SITUACAOTRIBUTARIA_TABELAB (Objquery.fieldbyname('cst').AsString);
      TObjMateriaisVenda(objVenda).Submit_percentualtributo          (Objquery.fieldbyname('percentualtributo').AsString);
      TObjMateriaisVenda(objVenda).Submit_VTOTTRIB                   (Objquery.fieldbyname('vtottrib').AsString);
      TObjMateriaisVenda(objVenda).submit_CSOSN                      (Objquery.fieldbyname('csosn').AsString);
      TObjMateriaisVenda(objVenda).Submit_CFOP                       (Objquery.fieldbyname('cfop').AsString);

      TObjMateriaisVenda(objVenda).Submit_NotaFiscal('');
      TObjMateriaisVenda(objVenda).Submit_classificacaofiscal     (Objquery.fieldbyname('classificacaofiscal').AsString);

      if not (TObjMateriaisVenda(objVenda).Salvar(false)) then
      begin
        messagedlg('N�o foi poss�vel Gravar referente o perfilado '+Objquery.fieldbyname('perfilado').asstring,mterror,[mbok],0);
        exit;
      End;

      Objquery.Next;

    end;


    {VIDRO-------------------------------}
    Objquery.Active:=false;
    Objquery.SQL.Text :=

    'select '+

    'v.descricao,v.unidade,prod.vidro,v.ncm,prod.quantidade,prod.valorfinal, '+
    'prod.valor,v.referencia,prod.cor,v.peso,v.classificacaofiscal,prod.desconto, '+

    'prod.valorfrete,prod.valorseguro,prod.percentualipi,prod.valorbasecalculo,prod.percentualicms,prod.valoricms, '+
    'prod.percentualreducaobc,prod.percentualpis,prod.valorpis,prod.percentualcofins,prod.valorcofins, '+
    'prod.percentualreducaobc_st,prod.valorbasecalculo_st,prod.percentualicms_st,prod.valoricms_st, '+
    'prod.valoripi,prod.valorbasecalculo_ipi,prod.valoroutros,prod.bcpis,prod.bccofins,prod.cstpis,prod.cstcofins,prod.cstipi, '+
    'prod.cst,prod.percentualtributo,prod.vtottrib,prod.csosn,prod.cfop '+

    'from tabvidro v '+
    'join tabprodnfedigitada prod on prod.vidro = v.codigo '+
    'where prod.nfedigitada ='+pNfedigitada;


    Objquery.Active := True;

    TObjMateriaisVenda(objVenda).ZerarTabela;

    while not (Objquery.Eof) do
    begin

      TObjMateriaisVenda(objVenda).state:=dsInsert;
      TObjMateriaisVenda(objVenda).Submit_Codigo             (TObjMateriaisVenda(objVenda).Get_NovoCodigo());
      TObjMateriaisVenda(objVenda).Submit_Descricao          (Objquery.fieldbyname('DESCRICAO').AsString);
      TObjMateriaisVenda(objVenda).Submit_Unidade            (Objquery.fieldbyname('UNIDADE').AsString);
      TObjMateriaisVenda(objVenda).Submit_Vidro              (Objquery.fieldbyname('vidro').AsString);
      TObjMateriaisVenda(objVenda).Submit_NCM                (Objquery.fieldbyname('NCM').AsString);
      TObjMateriaisVenda(objVenda).Submit_QUANTIDADE         (Objquery.fieldbyname('quantidade').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALORFINAL         (Objquery.fieldbyname('valorfinal').AsString);
      TObjMateriaisVenda(objVenda).Submit_ValorUnitario      (Objquery.fieldbyname('valor').AsString);
      TObjMateriaisVenda(objVenda).Submit_Desconto           (Objquery.fieldbyname('DESCONTO').AsString);
      TObjMateriaisVenda(objVenda).Nfedigitada.Submit_CODIGO (pNfedigitada);

      TObjMateriaisVenda(objVenda).Submit_Perfilado  ('');
      TObjMateriaisVenda(objVenda).Submit_Ferragem   ('');
      TObjMateriaisVenda(objVenda).Submit_Diverso    ('');
      TObjMateriaisVenda(objVenda).Submit_Componente ('');
      TObjMateriaisVenda(objVenda).Submit_KitBox     ('');
      TObjMateriaisVenda(objVenda).Submit_Persiana   ('');
      TObjMateriaisVenda(objVenda).Submit_indpag     (get_campoTabela('indpag','codigo','tabnfedigitada',pNfedigitada));


      TObjMateriaisVenda(objVenda).Submit_Referencia     (Objquery.fieldbyname('referencia').AsString);
      TObjMateriaisVenda(objVenda).Submit_Cor            (get_campoTabela('descricao','codigo','tabcor',Objquery.fieldbyname('cor').AsString));
      TObjMateriaisVenda(objVenda).Submit_CodigoCor      (Objquery.fieldbyname('cor').AsString);
      TObjMateriaisVenda(objVenda).Submit_PesoUnitario   (Objquery.fieldbyname('peso').AsString);
      TObjMateriaisVenda(objVenda).Submit_Material       ('3');


      TObjMateriaisVenda(objVenda).Submit_VALORFRETE         (Objquery.fieldbyname('valorfrete').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALORSEGURO        (Objquery.fieldbyname('valorseguro').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualipi      (Objquery.fieldbyname('percentualipi').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_ICMS            (Objquery.fieldbyname('valorbasecalculo').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualicms     (Objquery.fieldbyname('percentualicms').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_ICMS         (Objquery.fieldbyname('valoricms').AsString);
      TObjMateriaisVenda(objVenda).Submit_REDUCAOBASECALCULO (Objquery.fieldbyname('percentualreducaobc').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualpis      (Objquery.fieldbyname('percentualpis').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_PIS          (Objquery.fieldbyname('valorpis').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualcofins   (Objquery.fieldbyname('percentualcofins').AsString);

      TObjMateriaisVenda(objVenda).Submit_VALOR_COFINS          (Objquery.fieldbyname('valorcofins').AsString);
      TObjMateriaisVenda(objVenda).submit_reducaobasecalculo_st (Objquery.fieldbyname('percentualreducaobc_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_ICMS_ST            (Objquery.fieldbyname('valorbasecalculo_st').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualicms_st     (Objquery.fieldbyname('percentualicms_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_ICMS_ST         (Objquery.fieldbyname('valoricms_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_IPI             (Objquery.fieldbyname('valoripi').AsString);

      TObjMateriaisVenda(objVenda).Submit_BC_IPI      (Objquery.fieldbyname('valorbasecalculo_ipi').AsString);
      TObjMateriaisVenda(objVenda).submit_valoroutros (Objquery.fieldbyname('valoroutros').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_PIS      (Objquery.fieldbyname('bcpis').AsString);

      TObjMateriaisVenda(objVenda).Submit_BC_COFINS  (Objquery.fieldbyname('bccofins').AsString);
      TObjMateriaisVenda(objVenda).submit_cstpis     (Objquery.fieldbyname('cstpis').AsString);
      TObjMateriaisVenda(objVenda).submit_cstcofins  (Objquery.fieldbyname('cstcofins').AsString);
      TObjMateriaisVenda(objVenda).Submit_cstipi     (Objquery.fieldbyname('cstipi').AsString);

      TObjMateriaisVenda(objVenda).Submit_SITUACAOTRIBUTARIA_TABELAA (get_campoTabela('SITUACAOTRIBUTARIA_TABELAA','codigo','TABVIDRO',Objquery.fieldbyname('vidro').AsString));
      TObjMateriaisVenda(objVenda).Submit_SITUACAOTRIBUTARIA_TABELAB (Objquery.fieldbyname('cst').AsString);
      TObjMateriaisVenda(objVenda).Submit_percentualtributo          (Objquery.fieldbyname('percentualtributo').AsString);
      TObjMateriaisVenda(objVenda).Submit_VTOTTRIB                   (Objquery.fieldbyname('vtottrib').AsString);
      TObjMateriaisVenda(objVenda).submit_CSOSN                      (Objquery.fieldbyname('csosn').AsString);
      TObjMateriaisVenda(objVenda).Submit_CFOP                       (Objquery.fieldbyname('cfop').AsString);

      TObjMateriaisVenda(objVenda).Submit_NotaFiscal('');
      TObjMateriaisVenda(objVenda).Submit_classificacaofiscal     (Objquery.fieldbyname('classificacaofiscal').AsString);

      if not (TObjMateriaisVenda(objVenda).Salvar(false)) then
      begin
        messagedlg('N�o foi poss�vel Gravar referente o vidro '+Objquery.fieldbyname('vidro').asstring,mterror,[mbok],0);
        exit;
      End;

      Objquery.Next;

    end;


    {KITBOX------------------------------------------------------}
    Objquery.Active:=false;
    Objquery.SQL.Text :=

    'select '+

    'k.descricao,k.unidade,prod.kitbox,k.ncm,prod.quantidade,prod.valorfinal, '+
    'prod.valor,k.referencia,prod.cor,k.peso,k.classificacaofiscal,prod.desconto, '+

    'prod.valorfrete,prod.valorseguro,prod.percentualipi,prod.valorbasecalculo,prod.percentualicms,prod.valoricms, '+
    'prod.percentualreducaobc,prod.percentualpis,prod.valorpis,prod.percentualcofins,prod.valorcofins, '+
    'prod.percentualreducaobc_st,prod.valorbasecalculo_st,prod.percentualicms_st,prod.valoricms_st, '+
    'prod.valoripi,prod.valorbasecalculo_ipi,prod.valoroutros,prod.bcpis,prod.bccofins,prod.cstpis,prod.cstcofins,prod.cstipi, '+
    'prod.cst,prod.percentualtributo,prod.vtottrib,prod.csosn,prod.cfop '+

    'from tabkitbox k '+
    'join tabprodnfedigitada prod on prod.kitbox = k.codigo '+
    'where prod.nfedigitada ='+pNfedigitada;


    Objquery.Active := True;

    TObjMateriaisVenda(objVenda).ZerarTabela;

    while not (Objquery.Eof) do
    begin

      TObjMateriaisVenda(objVenda).state:=dsInsert;
      TObjMateriaisVenda(objVenda).Submit_Codigo             (TObjMateriaisVenda(objVenda).Get_NovoCodigo());
      TObjMateriaisVenda(objVenda).Submit_Descricao          (Objquery.fieldbyname('DESCRICAO').AsString);
      TObjMateriaisVenda(objVenda).Submit_Unidade            (Objquery.fieldbyname('UNIDADE').AsString);
      TObjMateriaisVenda(objVenda).Submit_KitBox             (Objquery.fieldbyname('kitbox').AsString);
      TObjMateriaisVenda(objVenda).Submit_NCM                (Objquery.fieldbyname('NCM').AsString);
      TObjMateriaisVenda(objVenda).Submit_QUANTIDADE         (Objquery.fieldbyname('quantidade').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALORFINAL         (Objquery.fieldbyname('valorfinal').AsString);
      TObjMateriaisVenda(objVenda).Submit_ValorUnitario      (Objquery.fieldbyname('valor').AsString);
      TObjMateriaisVenda(objVenda).Submit_Desconto           (Objquery.fieldbyname('DESCONTO').AsString);
      TObjMateriaisVenda(objVenda).Nfedigitada.Submit_CODIGO (pNfedigitada);

      TObjMateriaisVenda(objVenda).Submit_Perfilado  ('');
      TObjMateriaisVenda(objVenda).Submit_Ferragem   ('');
      TObjMateriaisVenda(objVenda).Submit_Diverso    ('');
      TObjMateriaisVenda(objVenda).Submit_Componente ('');
      TObjMateriaisVenda(objVenda).Submit_Vidro      ('');
      TObjMateriaisVenda(objVenda).Submit_Persiana   ('');
      TObjMateriaisVenda(objVenda).Submit_indpag     (get_campoTabela('indpag','codigo','tabnfedigitada',pNfedigitada));


      TObjMateriaisVenda(objVenda).Submit_Referencia     (Objquery.fieldbyname('referencia').AsString);
      TObjMateriaisVenda(objVenda).Submit_Cor            (get_campoTabela('descricao','codigo','tabcor',Objquery.fieldbyname('cor').AsString));
      TObjMateriaisVenda(objVenda).Submit_CodigoCor      (Objquery.fieldbyname('cor').AsString);
      TObjMateriaisVenda(objVenda).Submit_PesoUnitario   (Objquery.fieldbyname('peso').AsString);
      TObjMateriaisVenda(objVenda).Submit_Material       ('4');


      TObjMateriaisVenda(objVenda).Submit_VALORFRETE         (Objquery.fieldbyname('valorfrete').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALORSEGURO        (Objquery.fieldbyname('valorseguro').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualipi      (Objquery.fieldbyname('percentualipi').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_ICMS            (Objquery.fieldbyname('valorbasecalculo').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualicms     (Objquery.fieldbyname('percentualicms').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_ICMS         (Objquery.fieldbyname('valoricms').AsString);
      TObjMateriaisVenda(objVenda).Submit_REDUCAOBASECALCULO (Objquery.fieldbyname('percentualreducaobc').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualpis      (Objquery.fieldbyname('percentualpis').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_PIS          (Objquery.fieldbyname('valorpis').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualcofins   (Objquery.fieldbyname('percentualcofins').AsString);

      TObjMateriaisVenda(objVenda).Submit_VALOR_COFINS          (Objquery.fieldbyname('valorcofins').AsString);
      TObjMateriaisVenda(objVenda).submit_reducaobasecalculo_st (Objquery.fieldbyname('percentualreducaobc_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_ICMS_ST            (Objquery.fieldbyname('valorbasecalculo_st').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualicms_st     (Objquery.fieldbyname('percentualicms_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_ICMS_ST         (Objquery.fieldbyname('valoricms_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_IPI             (Objquery.fieldbyname('valoripi').AsString);

      TObjMateriaisVenda(objVenda).Submit_BC_IPI      (Objquery.fieldbyname('valorbasecalculo_ipi').AsString);
      TObjMateriaisVenda(objVenda).submit_valoroutros (Objquery.fieldbyname('valoroutros').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_PIS      (Objquery.fieldbyname('bcpis').AsString);

      TObjMateriaisVenda(objVenda).Submit_BC_COFINS  (Objquery.fieldbyname('bccofins').AsString);
      TObjMateriaisVenda(objVenda).submit_cstpis     (Objquery.fieldbyname('cstpis').AsString);
      TObjMateriaisVenda(objVenda).submit_cstcofins  (Objquery.fieldbyname('cstcofins').AsString);
      TObjMateriaisVenda(objVenda).Submit_cstipi     (Objquery.fieldbyname('cstipi').AsString);

      TObjMateriaisVenda(objVenda).Submit_SITUACAOTRIBUTARIA_TABELAA (get_campoTabela('SITUACAOTRIBUTARIA_TABELAA','codigo','TABKITBOX',Objquery.fieldbyname('kitbox').AsString));
      TObjMateriaisVenda(objVenda).Submit_SITUACAOTRIBUTARIA_TABELAB (Objquery.fieldbyname('cst').AsString);
      TObjMateriaisVenda(objVenda).Submit_percentualtributo          (Objquery.fieldbyname('percentualtributo').AsString);
      TObjMateriaisVenda(objVenda).Submit_VTOTTRIB                   (Objquery.fieldbyname('vtottrib').AsString);
      TObjMateriaisVenda(objVenda).submit_CSOSN                      (Objquery.fieldbyname('csosn').AsString);
      TObjMateriaisVenda(objVenda).Submit_CFOP                       (Objquery.fieldbyname('cfop').AsString);

      TObjMateriaisVenda(objVenda).Submit_NotaFiscal('');
      TObjMateriaisVenda(objVenda).Submit_classificacaofiscal     (Objquery.fieldbyname('classificacaofiscal').AsString);

      if not (TObjMateriaisVenda(objVenda).Salvar(false)) then
      begin
        messagedlg('N�o foi poss�vel Gravar referente o kitbox '+Objquery.fieldbyname('kitbox').asstring,mterror,[mbok],0);
        exit;
      End;

      Objquery.Next;

    end;


    {PERSIANA--------------------------------------}
    
    Objquery.Active:=false;
    Objquery.SQL.Text :=

    'select '+

    'p.nome,prod.persiana,p.ncm,prod.quantidade,prod.valorfinal,prod.valor, '+
    'p.referencia,prod.cor,p.classificacaofiscal,prod.desconto, '+

    'prod.valorfrete,prod.valorseguro,prod.percentualipi,prod.valorbasecalculo,prod.percentualicms,prod.valoricms, '+
    'prod.percentualreducaobc,prod.percentualpis,prod.valorpis,prod.percentualcofins,prod.valorcofins, '+
    'prod.percentualreducaobc_st,prod.valorbasecalculo_st,prod.percentualicms_st,prod.valoricms_st, '+
    'prod.valoripi,prod.valorbasecalculo_ipi,prod.valoroutros,prod.bcpis,prod.bccofins,prod.cstpis,prod.cstcofins,prod.cstipi, '+
    'prod.cst,prod.percentualtributo,prod.vtottrib,prod.csosn,prod.cfop '+

    'from tabpersiana p '+
    'join tabprodnfedigitada prod on prod.persiana = p.codigo '+
    'where prod.nfedigitada ='+pNfedigitada;


    Objquery.Active := True;

    TObjMateriaisVenda(objVenda).ZerarTabela;

    while not (Objquery.Eof) do
    begin

      TObjMateriaisVenda(objVenda).state:=dsInsert;
      TObjMateriaisVenda(objVenda).Submit_Codigo             (TObjMateriaisVenda(objVenda).Get_NovoCodigo());
      TObjMateriaisVenda(objVenda).Submit_Descricao          (Objquery.fieldbyname('nome').AsString);
      TObjMateriaisVenda(objVenda).Submit_Persiana           (Objquery.fieldbyname('persiana').AsString);
      TObjMateriaisVenda(objVenda).Submit_Unidade            ('M�');
      TObjMateriaisVenda(objVenda).Submit_NCM                (Objquery.fieldbyname('NCM').AsString);
      TObjMateriaisVenda(objVenda).Submit_QUANTIDADE         (Objquery.fieldbyname('quantidade').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALORFINAL         (Objquery.fieldbyname('valorfinal').AsString);
      TObjMateriaisVenda(objVenda).Submit_ValorUnitario      (Objquery.fieldbyname('valor').AsString);
      TObjMateriaisVenda(objVenda).Submit_Desconto           (Objquery.fieldbyname('DESCONTO').AsString);
      TObjMateriaisVenda(objVenda).Nfedigitada.Submit_CODIGO (pNfedigitada);

      TObjMateriaisVenda(objVenda).Submit_Perfilado  ('');
      TObjMateriaisVenda(objVenda).Submit_Ferragem   ('');
      TObjMateriaisVenda(objVenda).Submit_Diverso    ('');
      TObjMateriaisVenda(objVenda).Submit_Componente ('');
      TObjMateriaisVenda(objVenda).Submit_Vidro      ('');
      TObjMateriaisVenda(objVenda).Submit_KitBox     ('');
      TObjMateriaisVenda(objVenda).Submit_indpag     (get_campoTabela('indpag','codigo','tabnfedigitada',pNfedigitada));


      TObjMateriaisVenda(objVenda).Submit_Referencia     (Objquery.fieldbyname('referencia').AsString);
      TObjMateriaisVenda(objVenda).Submit_Cor            (get_campoTabela('descricao','codigo','tabcor',Objquery.fieldbyname('cor').AsString));
      TObjMateriaisVenda(objVenda).Submit_CodigoCor      (Objquery.fieldbyname('cor').AsString);
      TObjMateriaisVenda(objVenda).Submit_PesoUnitario   ('0');
      TObjMateriaisVenda(objVenda).Submit_Material       ('5');


      TObjMateriaisVenda(objVenda).Submit_VALORFRETE         (Objquery.fieldbyname('valorfrete').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALORSEGURO        (Objquery.fieldbyname('valorseguro').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualipi      (Objquery.fieldbyname('percentualipi').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_ICMS            (Objquery.fieldbyname('valorbasecalculo').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualicms     (Objquery.fieldbyname('percentualicms').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_ICMS         (Objquery.fieldbyname('valoricms').AsString);
      TObjMateriaisVenda(objVenda).Submit_REDUCAOBASECALCULO (Objquery.fieldbyname('percentualreducaobc').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualpis      (Objquery.fieldbyname('percentualpis').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_PIS          (Objquery.fieldbyname('valorpis').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualcofins   (Objquery.fieldbyname('percentualcofins').AsString);

      TObjMateriaisVenda(objVenda).Submit_VALOR_COFINS          (Objquery.fieldbyname('valorcofins').AsString);
      TObjMateriaisVenda(objVenda).submit_reducaobasecalculo_st (Objquery.fieldbyname('percentualreducaobc_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_ICMS_ST            (Objquery.fieldbyname('valorbasecalculo_st').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualicms_st     (Objquery.fieldbyname('percentualicms_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_ICMS_ST         (Objquery.fieldbyname('valoricms_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_IPI             (Objquery.fieldbyname('valoripi').AsString);

      TObjMateriaisVenda(objVenda).Submit_BC_IPI      (Objquery.fieldbyname('valorbasecalculo_ipi').AsString);
      TObjMateriaisVenda(objVenda).submit_valoroutros (Objquery.fieldbyname('valoroutros').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_PIS      (Objquery.fieldbyname('bcpis').AsString);

      TObjMateriaisVenda(objVenda).Submit_BC_COFINS  (Objquery.fieldbyname('bccofins').AsString);
      TObjMateriaisVenda(objVenda).submit_cstpis     (Objquery.fieldbyname('cstpis').AsString);
      TObjMateriaisVenda(objVenda).submit_cstcofins  (Objquery.fieldbyname('cstcofins').AsString);
      TObjMateriaisVenda(objVenda).Submit_cstipi     (Objquery.fieldbyname('cstipi').AsString);

      TObjMateriaisVenda(objVenda).Submit_SITUACAOTRIBUTARIA_TABELAA (get_campoTabela('SITUACAOTRIBUTARIA_TABELAA','codigo','TABPERSIANA',Objquery.fieldbyname('persiana').AsString));
      TObjMateriaisVenda(objVenda).Submit_SITUACAOTRIBUTARIA_TABELAB (Objquery.fieldbyname('cst').AsString);
      TObjMateriaisVenda(objVenda).Submit_percentualtributo          (Objquery.fieldbyname('percentualtributo').AsString);
      TObjMateriaisVenda(objVenda).Submit_VTOTTRIB                   (Objquery.fieldbyname('vtottrib').AsString);
      TObjMateriaisVenda(objVenda).submit_CSOSN                      (Objquery.fieldbyname('csosn').AsString);
      TObjMateriaisVenda(objVenda).Submit_CFOP                       (Objquery.fieldbyname('cfop').AsString);

      TObjMateriaisVenda(objVenda).Submit_NotaFiscal('');
      TObjMateriaisVenda(objVenda).Submit_classificacaofiscal     (Objquery.fieldbyname('classificacaofiscal').AsString);

      if not (TObjMateriaisVenda(objVenda).Salvar(false)) then
      begin
        messagedlg('N�o foi poss�vel Gravar referente a persiana '+Objquery.fieldbyname('persiana').asstring,mterror,[mbok],0);
        exit;
      End;

      Objquery.Next;

    end;

    {DIVERSO--------------------------}

    Objquery.Active:=false;
    Objquery.SQL.Text :=

    'select '+

    'd.descricao,d.unidade,prod.diverso,d.ncm,prod.quantidade,prod.valorfinal, '+
    'prod.valor,d.referencia,prod.cor,D.classificacaofiscal,prod.desconto, '+

    'prod.valorfrete,prod.valorseguro,prod.percentualipi,prod.valorbasecalculo,prod.percentualicms,prod.valoricms, '+
    'prod.percentualreducaobc,prod.percentualpis,prod.valorpis,prod.percentualcofins,prod.valorcofins, '+
    'prod.percentualreducaobc_st,prod.valorbasecalculo_st,prod.percentualicms_st,prod.valoricms_st, '+
    'prod.valoripi,prod.valorbasecalculo_ipi,prod.valoroutros,prod.bcpis,prod.bccofins,prod.cstpis,prod.cstcofins,prod.cstipi, '+
    'prod.cst,prod.percentualtributo,prod.vtottrib,prod.csosn,prod.cfop '+

    'from tabdiverso d '+
    'join tabprodnfedigitada prod on prod.diverso = d.codigo '+
    'where prod.nfedigitada ='+pNfedigitada;


    Objquery.Active := True;

    TObjMateriaisVenda(objVenda).ZerarTabela;

    while not (Objquery.Eof) do
    begin

      TObjMateriaisVenda(objVenda).state:=dsInsert;
      TObjMateriaisVenda(objVenda).Submit_Codigo             (TObjMateriaisVenda(objVenda).Get_NovoCodigo());
      TObjMateriaisVenda(objVenda).Submit_Descricao          (Objquery.fieldbyname('DESCRICAO').AsString);
      TObjMateriaisVenda(objVenda).Submit_Unidade            (Objquery.fieldbyname('UNIDADE').AsString);
      TObjMateriaisVenda(objVenda).Submit_Diverso            (Objquery.fieldbyname('diverso').AsString);
      TObjMateriaisVenda(objVenda).Submit_NCM                (Objquery.fieldbyname('NCM').AsString);
      TObjMateriaisVenda(objVenda).Submit_QUANTIDADE         (Objquery.fieldbyname('quantidade').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALORFINAL         (Objquery.fieldbyname('valorfinal').AsString);
      TObjMateriaisVenda(objVenda).Submit_ValorUnitario      (Objquery.fieldbyname('valor').AsString);
      TObjMateriaisVenda(objVenda).Submit_Desconto           (Objquery.fieldbyname('DESCONTO').AsString);
      TObjMateriaisVenda(objVenda).Nfedigitada.Submit_CODIGO (pNfedigitada);

      TObjMateriaisVenda(objVenda).Submit_Perfilado  ('');
      TObjMateriaisVenda(objVenda).Submit_Ferragem   ('');
      TObjMateriaisVenda(objVenda).Submit_persiana   ('');
      TObjMateriaisVenda(objVenda).Submit_Componente ('');
      TObjMateriaisVenda(objVenda).Submit_Vidro      ('');
      TObjMateriaisVenda(objVenda).Submit_KitBox     ('');
      TObjMateriaisVenda(objVenda).Submit_indpag     (get_campoTabela('indpag','codigo','tabnfedigitada',pNfedigitada));


      TObjMateriaisVenda(objVenda).Submit_Referencia     (Objquery.fieldbyname('referencia').AsString);
      TObjMateriaisVenda(objVenda).Submit_Cor            (get_campoTabela('descricao','codigo','tabcor',Objquery.fieldbyname('cor').AsString));
      TObjMateriaisVenda(objVenda).Submit_CodigoCor      (Objquery.fieldbyname('cor').AsString);
      TObjMateriaisVenda(objVenda).Submit_Material       ('6');


      TObjMateriaisVenda(objVenda).Submit_VALORFRETE         (Objquery.fieldbyname('valorfrete').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALORSEGURO        (Objquery.fieldbyname('valorseguro').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualipi      (Objquery.fieldbyname('percentualipi').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_ICMS            (Objquery.fieldbyname('valorbasecalculo').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualicms     (Objquery.fieldbyname('percentualicms').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_ICMS         (Objquery.fieldbyname('valoricms').AsString);
      TObjMateriaisVenda(objVenda).Submit_REDUCAOBASECALCULO (Objquery.fieldbyname('percentualreducaobc').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualpis      (Objquery.fieldbyname('percentualpis').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_PIS          (Objquery.fieldbyname('valorpis').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualcofins   (Objquery.fieldbyname('percentualcofins').AsString);

      TObjMateriaisVenda(objVenda).Submit_VALOR_COFINS          (Objquery.fieldbyname('valorcofins').AsString);
      TObjMateriaisVenda(objVenda).submit_reducaobasecalculo_st (Objquery.fieldbyname('percentualreducaobc_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_ICMS_ST            (Objquery.fieldbyname('valorbasecalculo_st').AsString);
      TObjMateriaisVenda(objVenda).submit_percentualicms_st     (Objquery.fieldbyname('percentualicms_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_ICMS_ST         (Objquery.fieldbyname('valoricms_st').AsString);
      TObjMateriaisVenda(objVenda).Submit_VALOR_IPI             (Objquery.fieldbyname('valoripi').AsString);

      TObjMateriaisVenda(objVenda).Submit_BC_IPI      (Objquery.fieldbyname('valorbasecalculo_ipi').AsString);
      TObjMateriaisVenda(objVenda).submit_valoroutros (Objquery.fieldbyname('valoroutros').AsString);
      TObjMateriaisVenda(objVenda).Submit_BC_PIS      (Objquery.fieldbyname('bcpis').AsString);

      TObjMateriaisVenda(objVenda).Submit_BC_COFINS  (Objquery.fieldbyname('bccofins').AsString);
      TObjMateriaisVenda(objVenda).submit_cstpis     (Objquery.fieldbyname('cstpis').AsString);
      TObjMateriaisVenda(objVenda).submit_cstcofins  (Objquery.fieldbyname('cstcofins').AsString);
      TObjMateriaisVenda(objVenda).Submit_cstipi     (Objquery.fieldbyname('cstipi').AsString);

      TObjMateriaisVenda(objVenda).Submit_SITUACAOTRIBUTARIA_TABELAA (get_campoTabela('SITUACAOTRIBUTARIA_TABELAA','codigo','TABDIVERSO',Objquery.fieldbyname('diverso').AsString));
      TObjMateriaisVenda(objVenda).Submit_SITUACAOTRIBUTARIA_TABELAB (Objquery.fieldbyname('cst').AsString);
      TObjMateriaisVenda(objVenda).Submit_percentualtributo          (Objquery.fieldbyname('percentualtributo').AsString);
      TObjMateriaisVenda(objVenda).Submit_VTOTTRIB                   (Objquery.fieldbyname('vtottrib').AsString);
      TObjMateriaisVenda(objVenda).submit_CSOSN                      (Objquery.fieldbyname('csosn').AsString);
      TObjMateriaisVenda(objVenda).Submit_CFOP                       (Objquery.fieldbyname('cfop').AsString);

      TObjMateriaisVenda(objVenda).Submit_NotaFiscal('');
      TObjMateriaisVenda(objVenda).Submit_classificacaofiscal     (Objquery.fieldbyname('classificacaofiscal').AsString);

      if not (TObjMateriaisVenda(objVenda).Salvar(false)) then
      begin
        messagedlg('N�o foi poss�vel Gravar referente o diverso '+Objquery.fieldbyname('diverso').asstring,mterror,[mbok],0);
        exit;
      End;

      Objquery.Next;

    end;

    
  except
    on e:Exception do
    begin
      ShowMessage(e.Message);
      Exit;
    end;
  end;

  Result := True;

end;


function TObjPRODNFEDIGITADA.gravaMateriaisVendidos_2(pNfedigitada,operacao:string): Boolean;
var
  objeto,objeto2:TObject;
  tipoCliente,UFcliente:string;
  percentualtributo:string;
  vtottrib:Currency;
begin


  result:=False;

  if(ObjParametroGlobal.ValidaParametro('NFE calcula total tributos')=false)
  then calculatotaltributos:=False
  else
  begin
        if(ObjParametroGlobal.Get_Valor='SIM')
        then calculatotaltributos:=True
        else calculatotaltributos:=False;
  end;


  try

      NFEDIGITADA.LocalizaCodigo(pNfedigitada);
      NFEDIGITADA.TabelaparaObjeto();
  
      {posso ter um cliente ou ou fornecedor na tabnfedigitada}
      if (NFEDIGITADA.CLIENTE.Get_Codigo() <> '') then
      begin

        tipoCliente:=NFEDIGITADA.CLIENTE.tipocliente.Get_CODIGO();
        UFcliente:=NFEDIGITADA.CLIENTE.Get_Estado();

      end else if (NFEDIGITADA.FORNECEDOR.Get_Codigo() <> '') then
      begin

        tipoCliente:=NFEDIGITADA.FORNECEDOR.tipo.Get_CODIGO();
        UFcliente:=NFEDIGITADA.FORNECEDOR.Get_Estado();

      end;


      {FERRAGEM********************************************************************}
      Objquery.Active:=false;
      Objquery.SQL.Clear;
      Objquery.Sql.add('select f.descricao,f.unidade,prod.ferragem,f.ncm,');
      Objquery.Sql.add('prod.quantidade,prod.valorfinal,prod.valor,');
      Objquery.Sql.add('f.referencia, prod.cor,f.peso,f.classificacaofiscal,prod.desconto');
      Objquery.Sql.add('from tabferragem f');
      Objquery.Sql.add('join tabprodnfedigitada prod on prod.ferragem = f.codigo');
      Objquery.Sql.add('where prod.nfedigitada ='+pNfedigitada);


      try
        Objquery.Active:=True;
      except
        Exit;
      end;


      objeto:=TObjMateriaisVenda.create;

      try

        objeto2:=nil;
        if (Objquery.RecordCount > 0) then
          objeto2:=TObjferragem_ICMS.Create(self.Owner);

        try


          while not (Objquery.Eof) do
          begin

            TObjMateriaisVenda(objeto).state:=dsInsert;
            TObjMateriaisVenda(objeto).Submit_Codigo     (TObjMateriaisVenda(objeto).Get_NovoCodigo());
            TObjMateriaisVenda(objeto).Submit_Descricao  (Objquery.fieldbyname('DESCRICAO').AsString);
            TObjMateriaisVenda(objeto).Submit_Unidade    (Objquery.fieldbyname('UNIDADE').AsString);
            TObjMateriaisVenda(objeto).Submit_Ferragem   (Objquery.fieldbyname('ferragem').AsString);
            TObjMateriaisVenda(objeto).Submit_NCM        (Objquery.fieldbyname('NCM').AsString);
            TObjMateriaisVenda(objeto).Submit_Desconto   (Objquery.fieldbyname('DESCONTO').AsString);

            TObjMateriaisVenda(objeto).Nfedigitada.Submit_CODIGO(pNfedigitada);

            TObjMateriaisVenda(objeto).Submit_Vidro      ('');
            TObjMateriaisVenda(objeto).Submit_Perfilado  ('');
            TObjMateriaisVenda(objeto).Submit_Diverso    ('');
            TObjMateriaisVenda(objeto).Submit_Componente ('');
            TObjMateriaisVenda(objeto).Submit_KitBox     ('');
            TObjMateriaisVenda(objeto).Submit_Persiana   ('');
            TObjMateriaisVenda(objeto).Submit_indpag(get_campoTabela('indpag','codigo','tabnfedigitada',pNfedigitada));

            If not (TObjferragem_ICMS(objeto2).Localiza(ESTADOSISTEMAGLOBAL,tipoCliente,Objquery.fieldbyname('ferragem').AsString,operacao)) then
            begin

              //Se para alguma ferragem ainda n�o foram gravados os impostos, avisa qual ferragem �
              MensagemErro('N�o foi encontrado o imposto de Origem para esta Ferragem, tipo de cliente e opera��o'+#13+
                         'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+Objquery.fieldbyname('ferragem').AsString);
              exit;

            end;
            TObjferragem_ICMS(objeto2).TabelaparaObjeto;

            TObjMateriaisVenda(objeto).Submit_MPOSTO_ICMS_ORIGEM    (TObjferragem_ICMS(objeto2).IMPOSTO.Get_CODIGO);
            TObjMateriaisVenda(objeto).Submit_IMPOSTO_IPI           (TObjferragem_ICMS(objeto2).imposto_ipi.Get_CODIGO);
            TObjMateriaisVenda(objeto).Submit_IMPOSTO_PIS_ORIGEM    (TObjferragem_ICMS(objeto2).imposto_pis.Get_CODIGO);
            TObjMateriaisVenda(objeto).Submit_IMPOSTO_COFINS_ORIGEM (TObjferragem_ICMS(objeto2).imposto_cofins.Get_CODIGO);
            TObjMateriaisVenda(objeto).Submit_CFOP                  (TObjferragem_ICMS(objeto2).IMPOSTO.CFOP.Get_CODIGO);
            TObjMateriaisVenda(objeto).submit_CSOSN                 (TObjferragem_ICMS(objeto2).IMPOSTO.CSOSN.Get_CODIGO);


            if(UFCliente=ESTADOSISTEMAGLOBAL) then
              TObjMateriaisVenda(objeto).Submit_CFOP(TObjFerragem_ICMS(objeto2).IMPOSTO.CFOP.Get_CODIGO)
            else
              TObjMateriaisVenda(objeto).Submit_CFOP(TObjFerragem_ICMS(objeto2).IMPOSTO.Get_CFOP_FORAESTADO);


            if not (tObjFerragem_ICMS(objeto2).Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,Objquery.fieldbyname('ferragem').AsString,operacao)) then
            begin
              MensagemErro('N�o foi encontrado o imposto de Destino para este Ferragem, tipo de cliente e opera��o'+#13+
                             'Estado: '+UFCliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+Objquery.fieldbyname('ferragem').AsString);
              exit;
            end;
            TObjferragem_ICMS(objeto2).TabelaparaObjeto;

            TObjMateriaisVenda(objeto).Submit_IMPOSTO_ICMS_DESTINO   (TObjferragem_ICMS(objeto2).IMPOSTO.Get_CODIGO);
            TObjMateriaisVenda(objeto).Submit_MPOSTO_PIS_DESTINO     (TObjferragem_ICMS(objeto2).imposto_pis.Get_CODIGO);
            TObjMateriaisVenda(objeto).Submit_IMPOSTO_COFINS_DESTINO (TObjferragem_ICMS(objeto2).imposto_cofins.Get_CODIGO);

            TObjMateriaisVenda(objeto).Submit_ALIQUOTA                      (TObjferragem_ICMS(objeto2).IMPOSTO.Get_ALIQUOTA);
            TObjMateriaisVenda(objeto).Submit_REDUCAOBASECALCULO            (TObjferragem_ICMS(objeto2).IMPOSTO.Get_PERC_REDUCAO_BC);
            TObjMateriaisVenda(objeto).Submit_VALORPAUTA                    (TObjferragem_ICMS(objeto2).IMPOSTO.Get_PAUTA);
            TObjMateriaisVenda(objeto).Submit_ALIQUOTACUPOM                 ('0');
            TObjMateriaisVenda(objeto).Submit_PERCENTUALAGREGADO            ('0');
            TObjMateriaisVenda(objeto).Submit_MARGEMVALORAGREGADOCONSUMIDOR ('0');

            if (calculatotaltributos) then
            begin
                try
                    percentualtributo :=  TObjferragem_ICMS(objeto2).IMPOSTO.Get_PERCENTUALTRIBUTO;
                    vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(Objquery.fieldbyname('valorfinal').AsString);
                    TObjMateriaisVenda(objeto).Submit_percentualtributo(percentualtributo);
                    TObjMateriaisVenda(objeto).Submit_VTOTTRIB(CurrToStr(vtottrib));

                except
                    on e:exception do
                    begin
                       ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                            Exit;
                    end
                end
            end;

            //S�o gravados com esses valores por default
            //As bases de calculos e os valores de cada imposto
            //S�o atualizados na fun��o "CalculaBasedeCalculoNovaForma"

           TObjMateriaisVenda(objeto).Submit_VALORFRETE      ('0');
           TObjMateriaisVenda(objeto).Submit_VALORSEGURO     ('0');
           TObjMateriaisVenda(objeto).Submit_BC_ICMS         ('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_ICMS      ('0');
           TObjMateriaisVenda(objeto).Submit_BC_ICMS_ST      ('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_ICMS_ST   ('0');
           TObjMateriaisVenda(objeto).Submit_BC_IPI          ('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_IPI       ('0');
           TObjMateriaisVenda(objeto).Submit_BC_PIS          ('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_PIS       ('0');
           TObjMateriaisVenda(objeto).Submit_BC_PIS_ST       ('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_PIS_ST    ('0');
           TObjMateriaisVenda(objeto).Submit_BC_COFINS       ('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_COFINS    ('0');
           TObjMateriaisVenda(objeto).Submit_BC_COFINS_ST    ('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_COFINS_ST ('0');

           TObjMateriaisVenda(objeto).Submit_SITUACAOTRIBUTARIA_TABELAA(TObjferragem_ICMS(objeto2).IMPOSTO.STA.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_SITUACAOTRIBUTARIA_TABELAB(TObjferragem_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO);

           if(TObjferragem_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO = '30') or (TObjferragem_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='40') or (TObjferragem_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='41') then
            TObjMateriaisVenda(objeto).Submit_ISENTO('S')
           else
            TObjMateriaisVenda(objeto).Submit_ISENTO('N');


           if(TObjFerragem_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO = '30') or (TObjferragem_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='10') or (TObjFerragem_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='60')then
            TObjMateriaisVenda(objeto).Submit_SUBSTITUICAOTRIBUTARIA('S')
           else
            TObjMateriaisVenda(objeto).Submit_SUBSTITUICAOTRIBUTARIA('N');


            TObjMateriaisVenda(objeto).Submit_NotaFiscal('');
            TObjMateriaisVenda(objeto).Submit_classificacaofiscal(Objquery.fieldbyname('classificacaofiscal').AsString);


           TObjMateriaisVenda(objeto).Submit_QUANTIDADE     (Objquery.fieldbyname('QUANTIDADE').AsString);
           TObjMateriaisVenda(objeto).Submit_VALORFINAL     (Objquery.fieldbyname('valorfinal').AsString);
           TObjMateriaisVenda(objeto).Submit_ValorUnitario  (Objquery.fieldbyname('valor').AsString);
           TObjMateriaisVenda(objeto).Submit_Referencia     (Objquery.fieldbyname('referencia').AsString);
           TObjMateriaisVenda(objeto).Submit_Cor(get_campoTabela('descricao','codigo','tabcor',Objquery.fieldbyname('cor').AsString));
           TObjMateriaisVenda(objeto).Submit_CodigoCor(Objquery.fieldbyname('cor').AsString);
           TObjMateriaisVenda(objeto).Submit_PesoUnitario   (Objquery.fieldbyname('peso').AsString);
           TObjMateriaisVenda(objeto).Submit_Material       ('1');

           if not (TObjMateriaisVenda(objeto).Salvar(false)) then
           begin
            messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem '+Objquery.fieldbyname('ferragem').asstring,mterror,[mbok],0);
            exit;
           End;

           Objquery.Next;

          end;

        finally

          if (TObjferragem_ICMS(objeto2) <> nil) then
            TObjferragem_ICMS(objeto2).Free;

        end;

        {PERFILADO*******************************************************************}

        Objquery.Active:=false;
        Objquery.SQL.Clear;
        Objquery.Sql.add('select p.descricao,p.unidade,prod.perfilado,p.ncm,');
        Objquery.Sql.add('prod.quantidade,prod.valorfinal,prod.valor,');
        Objquery.Sql.add('p.referencia,prod.cor,p.peso,p.classificacaofiscal,prod.desconto');
        Objquery.Sql.add('from tabperfilado p');
        Objquery.Sql.add('join tabprodnfedigitada prod on prod.perfilado = p.codigo');
        Objquery.Sql.add('where prod.nfedigitada ='+pNfedigitada);

        try
          Objquery.Active:=True;
        except
          Exit;
        end;


        objeto2:=nil;
        if (Objquery.RecordCount > 0) then
          objeto2:=TObjperfilado_ICMS.Create(Self.Owner);

        try

          while not(Objquery.Eof) do
          begin

           TObjMateriaisVenda(objeto).state:=dsInsert;
           TObjMateriaisVenda(objeto).ZerarTabela;
           TObjMateriaisVenda(objeto).Submit_Codigo     (TObjMateriaisVenda(objeto).Get_NovoCodigo);
           TObjMateriaisVenda(objeto).Submit_Descricao  (Objquery.fieldbyname('DESCRICAO').asstring);
           TObjMateriaisVenda(objeto).Submit_Unidade    (Objquery.fieldbyname('UNIDADE').AsString);
           TObjMateriaisVenda(objeto).Submit_Perfilado  (Objquery.fieldbyname('perfilado').AsString);
           TObjMateriaisVenda(objeto).Submit_NCM        (Objquery.fieldbyname('NCM').AsString);
           TObjMateriaisVenda(objeto).Submit_Desconto   (Objquery.fieldbyname('DESCONTO').AsString);

           TObjMateriaisVenda(objeto).Nfedigitada.Submit_CODIGO(pNfedigitada);

           TObjMateriaisVenda(objeto).Submit_Ferragem   ('');
           TObjMateriaisVenda(objeto).Submit_Vidro      ('');
           TObjMateriaisVenda(objeto).Submit_Diverso    ('');
           TObjMateriaisVenda(objeto).Submit_Componente ('');
           TObjMateriaisVenda(objeto).Submit_KitBox     ('');
           TObjMateriaisVenda(objeto).Submit_Persiana   ('');
           TObjMateriaisVenda(objeto).Submit_indpag(get_campoTabela('indpag','codigo','tabnfedigitada',pNfedigitada));

           If not (TObjperfilado_ICMS(objeto2).Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,Objquery.fieldbyname('perfilado').AsString,operacao)) then
           begin
            MensagemErro('N�o foi encontrado o imposto de Destino para este Ferragem, tipo de cliente e opera��o'+#13+'Estado: '+UFCliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+Objquery.fieldbyname('Codigo').AsString);
            exit;
           End;

           TObjperfilado_ICMS(objeto2).TabelaparaObjeto;

           TObjMateriaisVenda(objeto).Submit_MPOSTO_ICMS_ORIGEM    (TObjperfilado_ICMS(objeto2).IMPOSTO.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_IPI           (TObjperfilado_ICMS(objeto2).imposto_ipi.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_PIS_ORIGEM    (TObjperfilado_ICMS(objeto2).imposto_pis.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_COFINS_ORIGEM (TObjperfilado_ICMS(objeto2).imposto_cofins.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_CFOP                  (TObjperfilado_ICMS(objeto2).IMPOSTO.CFOP.Get_CODIGO);
           TObjMateriaisVenda(objeto).submit_CSOSN                 (TObjperfilado_ICMS(objeto2).IMPOSTO.CSOSN.Get_CODIGO);

           if(UFCliente=ESTADOSISTEMAGLOBAL) then
              TObjMateriaisVenda(objeto).Submit_CFOP(TObjFerragem_ICMS(objeto2).IMPOSTO.CFOP.Get_CODIGO)
           else
              TObjMateriaisVenda(objeto).Submit_CFOP(TObjFerragem_ICMS(objeto2).IMPOSTO.Get_CFOP_FORAESTADO);



           if (TObjPerfilado_ICMS(objeto2).Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,Objquery.fieldbyname('perfilado').AsString,operacao)=False) then
           begin

            MensagemErro('N�o foi encontrado o imposto de Destino para este perfilado, tipo de cliente e opera��o');
            exit;

           End;

           TObjperfilado_ICMS(objeto2).TabelaparaObjeto;

           TObjMateriaisVenda(objeto).Submit_IMPOSTO_ICMS_DESTINO   (TObjperfilado_ICMS(objeto2).IMPOSTO.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_MPOSTO_PIS_DESTINO     (TObjperfilado_ICMS(objeto2).imposto_pis.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_COFINS_DESTINO (tObjPerfilado_ICMS(objeto2).imposto_cofins.Get_CODIGO);

           TObjMateriaisVenda(objeto).Submit_ALIQUOTA                      (TObjperfilado_ICMS(objeto2).IMPOSTO.Get_ALIQUOTA);
           TObjMateriaisVenda(objeto).Submit_REDUCAOBASECALCULO            (TObjperfilado_ICMS(objeto2).IMPOSTO.Get_PERC_REDUCAO_BC);
           TObjMateriaisVenda(objeto).Submit_VALORPAUTA                    (TObjperfilado_ICMS(objeto2).IMPOSTO.Get_PAUTA);

           TObjMateriaisVenda(objeto).Submit_ALIQUOTACUPOM                 ('0');
           TObjMateriaisVenda(objeto).Submit_PERCENTUALAGREGADO            ('0');
           TObjMateriaisVenda(objeto).Submit_MARGEMVALORAGREGADOCONSUMIDOR ('0');


           TObjMateriaisVenda(objeto).Submit_VALORFRETE('0');
           TObjMateriaisVenda(objeto).Submit_VALORSEGURO('0');
           TObjMateriaisVenda(objeto).Submit_BC_ICMS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_ICMS('0');
           TObjMateriaisVenda(objeto).Submit_BC_ICMS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_ICMS_ST('0');
           TObjMateriaisVenda(objeto).Submit_BC_IPI('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_IPI('0');
           TObjMateriaisVenda(objeto).Submit_BC_PIS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_PIS('0');
           TObjMateriaisVenda(objeto).Submit_BC_PIS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_PIS_ST('0');
           TObjMateriaisVenda(objeto).Submit_BC_COFINS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_COFINS('0');
           TObjMateriaisVenda(objeto).Submit_BC_COFINS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_COFINS_ST('0');


           TObjMateriaisVenda(objeto).Submit_SITUACAOTRIBUTARIA_TABELAA(TObjperfilado_ICMS(objeto2).IMPOSTO.STA.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_SITUACAOTRIBUTARIA_TABELAB(TObjperfilado_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO);

           if(TObjperfilado_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO = '30') or (TObjperfilado_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='40') or (TObjperfilado_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='41') then
              TObjMateriaisVenda(objeto).Submit_ISENTO('S')
           else
              TObjMateriaisVenda(objeto).Submit_ISENTO('N');


           if(TObjperfilado_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO = '30') or (TObjperfilado_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='10') or (TObjFerragem_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='60') then
            TObjMateriaisVenda(objeto).Submit_SUBSTITUICAOTRIBUTARIA('S')
           else
            TObjMateriaisVenda(objeto).Submit_SUBSTITUICAOTRIBUTARIA('N');

           TObjMateriaisVenda(objeto).Submit_NotaFiscal('');
           TObjMateriaisVenda(objeto).Submit_classificacaofiscal(Objquery.fieldbyname('classificacaofiscal').AsString);

           TObjMateriaisVenda(objeto).Submit_QUANTIDADE   (Objquery.fieldbyname('QUANTIDADE').AsString);
           TObjMateriaisVenda(objeto).Submit_VALORFINAL   (Objquery.fieldbyname('valorfinal').AsString);
           TObjMateriaisVenda(objeto).Submit_ValorUnitario(Objquery.fieldbyname('valor').AsString);
           TObjMateriaisVenda(objeto).Submit_Referencia   (Objquery.fieldbyname('referencia').AsString);
           TObjMateriaisVenda(objeto).Submit_Material     ('2');
           //TObjMateriaisVenda(objeto).Submit_Cor          (Objquery.fieldbyname('cor').AsString);
           TObjMateriaisVenda(objeto).Submit_Cor(get_campoTabela('descricao','codigo','tabcor',Objquery.fieldbyname('cor').AsString));
           TObjMateriaisVenda(objeto).Submit_CodigoCor(Objquery.fieldbyname('cor').AsString);
           TObjMateriaisVenda(objeto).Submit_PesoUnitario (Objquery.fieldbyname('peso').AsString);

            if (calculatotaltributos) then
            begin
                try
                    percentualtributo :=  TObjperfilado_ICMS(objeto2).IMPOSTO.Get_PERCENTUALTRIBUTO;
                    vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(Objquery.fieldbyname('valorfinal').AsString);
                    TObjMateriaisVenda(objeto).Submit_percentualtributo(percentualtributo);
                    TObjMateriaisVenda(objeto).Submit_VTOTTRIB(CurrToStr(vtottrib));

                except
                    on e:exception do
                    begin
                       ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                            Exit;
                    end
                end
            end;

           if not (TObjMateriaisVenda(objeto).Salvar(false)) then
           begin
            messagedlg('N�o foi poss�vel gravar o perfilado. '+Objquery.fieldbyname('perfilado').asstring,mterror,[mbok],0);
            exit;
           End;

           Objquery.Next;

          end;

        finally

          if (TObjperfilado_ICMS(objeto2) <> nil) then
            TObjperfilado_ICMS(objeto2).Free;

        end;

        {VIDRO***********************************************************************}

        Objquery.Active:=false;
        Objquery.SQL.Clear;
        Objquery.Sql.add('select v.descricao,v.unidade,prod.vidro,v.ncm,');
        Objquery.Sql.add('prod.quantidade,prod.valorfinal,prod.valor,');
        Objquery.Sql.add('v.referencia,prod.cor,v.peso,v.classificacaofiscal,prod.desconto');
        Objquery.Sql.add('from tabvidro v');
        Objquery.Sql.add('join tabprodnfedigitada prod on prod.vidro = v.codigo');
        Objquery.Sql.add('where prod.nfedigitada ='+pNfedigitada);


        try
          Objquery.Active:=True;
        except
          Exit;
        end;

        objeto2:=nil;
        if (Objquery.RecordCount > 0) then
          objeto2:=TObjVIDRO_ICMS.Create(self.Owner);

        try

          while not (Objquery.Eof) do
          begin

           TObjMateriaisVenda(objeto).state:=dsInsert;
           TObjMateriaisVenda(objeto).ZerarTabela;

           TObjMateriaisVenda(objeto).Submit_Codigo    (TObjMateriaisVenda(objeto).Get_NovoCodigo);
           TObjMateriaisVenda(objeto).Nfedigitada.Submit_CODIGO(pNfedigitada);
           TObjMateriaisVenda(objeto).Submit_Descricao (Objquery.fieldbyname('descricao').asstring);
           TObjMateriaisVenda(objeto).Submit_Unidade   (Objquery.fieldbyname('unidade').AsString);

           TObjMateriaisVenda(objeto).Submit_Ferragem   ('');
           TObjMateriaisVenda(objeto).Submit_Perfilado  ('');
           TObjMateriaisVenda(objeto).Submit_Diverso    ('');
           TObjMateriaisVenda(objeto).Submit_Componente ('');
           TObjMateriaisVenda(objeto).Submit_KitBox     ('');
           TObjMateriaisVenda(objeto).Submit_Persiana   ('');

           TObjMateriaisVenda(objeto).Submit_Vidro    (Objquery.fieldbyname('vidro').AsString);
           TObjMateriaisVenda(objeto).Submit_NCM      (Objquery.fieldbyname('NCM').AsString);
           TObjMateriaisVenda(objeto).Submit_Desconto (Objquery.fieldbyname('DESCONTO').AsString);


           If not (TObjVidro_ICMS(objeto2).Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,Objquery.fieldbyname('vidro').AsString,operacao)) then
           Begin

            MensagemErro('N�o foi encontrado o imposto de Origem para este Vidro, tipo de cliente e opera��o');
            exit;

           End;
           TObjVidro_ICMS(objeto2).TabelaparaObjeto;

           TObjMateriaisVenda           (objeto).Submit_MPOSTO_ICMS_ORIGEM    (TObjVidro_ICMS(objeto2).IMPOSTO.Get_CODIGO);
           TObjMateriaisVenda           (objeto).Submit_IMPOSTO_IPI           (TObjVidro_ICMS(objeto2).imposto_ipi.Get_CODIGO);
           TObjMateriaisVenda           (objeto).Submit_IMPOSTO_PIS_ORIGEM    (TObjVidro_ICMS(objeto2).imposto_pis.Get_CODIGO);
           TObjMateriaisVenda           (objeto).Submit_IMPOSTO_COFINS_ORIGEM (TObjVidro_ICMS(objeto2).imposto_cofins.Get_CODIGO);
           TObjMateriaisVenda           (objeto).Submit_CFOP                  (TObjVidro_ICMS(objeto2).IMPOSTO.CFOP.Get_CODIGO);
           TObjMateriaisVenda           (objeto).submit_CSOSN                 (TObjVidro_ICMS(objeto2).IMPOSTO.CSOSN.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_indpag(get_campoTabela('indpag','codigo','tabnfedigitada',pNfedigitada));
           
           if(UFCliente=ESTADOSISTEMAGLOBAL) then
              TObjMateriaisVenda(objeto).Submit_CFOP(TObjFerragem_ICMS(objeto2).IMPOSTO.CFOP.Get_CODIGO)
           else
              TObjMateriaisVenda(objeto).Submit_CFOP(TObjFerragem_ICMS(objeto2).IMPOSTO.Get_CFOP_FORAESTADO);


           if not (TObjVidro_ICMS(objeto2).Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,Objquery.fieldbyname('vidro').AsString,operacao)) Then
           Begin

            MensagemErro('N�o foi encontrado o imposto de Destino para este vidro, tipo de cliente e opera��o');
            exit;

           End;
           TObjVidro_ICMS(objeto2).TabelaparaObjeto;

           TObjMateriaisVenda(objeto).Submit_IMPOSTO_ICMS_DESTINO   (TObjVidro_ICMS(objeto2).IMPOSTO.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_MPOSTO_PIS_DESTINO     (TObjVidro_ICMS(objeto2).imposto_pis.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_COFINS_DESTINO (TObjVidro_ICMS(objeto2).imposto_cofins.Get_CODIGO);

           TObjMateriaisVenda(objeto).Submit_ALIQUOTA                      (TObjVidro_ICMS(objeto2).IMPOSTO.Get_ALIQUOTA);
           TObjMateriaisVenda(objeto).Submit_REDUCAOBASECALCULO            (TObjVidro_ICMS(objeto2).IMPOSTO.Get_PERC_REDUCAO_BC);
           TObjMateriaisVenda(objeto).Submit_ALIQUOTACUPOM                 ('0');
           TObjMateriaisVenda(objeto).Submit_PERCENTUALAGREGADO            ('0');
           TObjMateriaisVenda(objeto).Submit_VALORPAUTA                    (TObjVidro_ICMS(objeto2).IMPOSTO.Get_PAUTA);
           TObjMateriaisVenda(objeto).Submit_MARGEMVALORAGREGADOCONSUMIDOR ('0');


           TObjMateriaisVenda(objeto).Submit_VALORFRETE('0');
           TObjMateriaisVenda(objeto).Submit_VALORSEGURO('0');
           TObjMateriaisVenda(objeto).Submit_BC_ICMS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_ICMS('0');
           TObjMateriaisVenda(objeto).Submit_BC_ICMS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_ICMS_ST('0');
           TObjMateriaisVenda(objeto).Submit_BC_IPI('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_IPI('0');
           TObjMateriaisVenda(objeto).Submit_BC_PIS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_PIS('0');
           TObjMateriaisVenda(objeto).Submit_BC_PIS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_PIS_ST('0');
           TObjMateriaisVenda(objeto).Submit_BC_COFINS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_COFINS('0');
           TObjMateriaisVenda(objeto).Submit_BC_COFINS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_COFINS_ST('0');
           TObjMateriaisVenda(objeto).Submit_ALIQUOTACUPOM('0');

           TObjMateriaisVenda(objeto).Submit_SITUACAOTRIBUTARIA_TABELAA(TObjVidro_ICMS(objeto2).IMPOSTO.STA.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_SITUACAOTRIBUTARIA_TABELAB(TObjVidro_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO);

           if(TObjVidro_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO = '30') or (TObjVidro_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='40') or (TObjVidro_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='41') then
            TObjMateriaisVenda(objeto).Submit_ISENTO('S')
           else
            TObjMateriaisVenda(objeto).Submit_ISENTO('N');


           if(TObjVidro_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO = '30') or (TObjVidro_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='10') or (TObjFerragem_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='60') then
            TObjMateriaisVenda(objeto).Submit_SUBSTITUICAOTRIBUTARIA('S')
           else
            TObjMateriaisVenda(objeto).Submit_SUBSTITUICAOTRIBUTARIA('N');



           TObjMateriaisVenda(objeto).Submit_NotaFiscal('');
           TObjMateriaisVenda(objeto).Submit_classificacaofiscal(Objquery.fieldbyname('classificacaofiscal').AsString);


           TObjMateriaisVenda(objeto).Submit_QUANTIDADE(Objquery.fieldbyname('QUANTIDADE').AsString);
           TObjMateriaisVenda(objeto).Submit_VALORFINAL(Objquery.fieldbyname('valorfinal').AsString);
           TObjMateriaisVenda(objeto).Submit_ValorUnitario(Objquery.fieldbyname('valor').AsString);
           TObjMateriaisVenda(objeto).Submit_Referencia(Objquery.fieldbyname('referencia').AsString);
           TObjMateriaisVenda(objeto).Submit_Material('3');
           //TObjMateriaisVenda(objeto).Submit_Cor(Objquery.fieldbyname('cor').AsString);
           TObjMateriaisVenda(objeto).Submit_Cor(get_campoTabela('descricao','codigo','tabcor',Objquery.fieldbyname('cor').AsString));
           TObjMateriaisVenda(objeto).Submit_CodigoCor(Objquery.fieldbyname('cor').AsString);
           TObjMateriaisVenda(objeto).Submit_PesoUnitario(Objquery.fieldbyname('peso').AsString);

           if (calculatotaltributos) then
            begin
                try
                    percentualtributo :=  TObjVidro_ICMS(objeto2).IMPOSTO.Get_PERCENTUALTRIBUTO;
                    vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(Objquery.fieldbyname('valorfinal').AsString);
                    TObjMateriaisVenda(objeto).Submit_percentualtributo(percentualtributo);
                    TObjMateriaisVenda(objeto).Submit_VTOTTRIB(CurrToStr(vtottrib));

                except
                    on e:exception do
                    begin
                       ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                            Exit;
                    end
                end
            end;

           if not (TObjMateriaisVenda(objeto).Salvar(false)) Then
           Begin
            messagedlg('N�o foi poss�vel Gravar o vidro. '+ObjQuery.fieldbyname('vidro').asstring,mterror,[mbok],0);
            exit;
           End;

           Objquery.Next;

          end;

        finally

          if (TObjVIDRO_ICMS(objeto2) <> nil) then
            TObjVIDRO_ICMS(objeto2).Free;

        end;

        {KITBOX***********************************************************************}

        Objquery.Active:=false;
        Objquery.SQL.Clear;
        Objquery.Sql.add('select k.descricao,k.unidade,prod.kitbox,k.ncm,');
        Objquery.Sql.add('prod.quantidade,prod.valorfinal,prod.valor,');
        Objquery.Sql.add('k.referencia,prod.cor,k.peso,k.classificacaofiscal,prod.desconto');
        Objquery.Sql.add('from tabkitbox k');
        Objquery.Sql.add('join tabprodnfedigitada prod on prod.kitbox = k.codigo');
        Objquery.Sql.add('where prod.nfedigitada ='+pNfedigitada);

        try
          Objquery.Active:=True;
        except
          Exit;
        end;

        objeto2:=nil;
        if (Objquery.RecordCount > 0) then
          objeto2:=TObjkitbox_ICMS.Create(self.Owner);

        try

          while not (Objquery.Eof) do
          begin

           TObjMateriaisVenda(objeto).state:=dsInsert;
           TObjMateriaisVenda(objeto).ZerarTabela;
           TObjMateriaisVenda(objeto).Submit_Codigo(TObjMateriaisVenda(objeto).Get_NovoCodigo);
           TObjMateriaisVenda(objeto).Nfedigitada.Submit_CODIGO(pNfedigitada);
           TObjMateriaisVenda(objeto).Submit_Descricao(Objquery.fieldbyname('DESCRICAO').asstring);
           TObjMateriaisVenda(objeto).Submit_Unidade(Objquery.fieldbyname('UNIDADE').AsString);
           TObjMateriaisVenda(objeto).Submit_KitBox(Objquery.fieldbyname('kitbox').AsString);
           TObjMateriaisVenda(objeto).Submit_NCM(Objquery.fieldbyname('NCM').AsString);
           TObjMateriaisVenda(objeto).Submit_Desconto(Objquery.fieldbyname('DESCONTO').AsString);

           TObjMateriaisVenda(objeto).Submit_Ferragem   ('');
           TObjMateriaisVenda(objeto).Submit_Vidro      ('');
           TObjMateriaisVenda(objeto).Submit_Perfilado  ('');
           TObjMateriaisVenda(objeto).Submit_Diverso    ('');
           TObjMateriaisVenda(objeto).Submit_Componente ('');
           TObjMateriaisVenda(objeto).Submit_Persiana   ('');
           TObjMateriaisVenda(objeto).Submit_indpag(get_campoTabela('indpag','codigo','tabnfedigitada',pNfedigitada));

           If not (TObjKitbox_ICMS(objeto2).Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,Objquery.fieldbyname('kitbox').AsString,operacao)) Then
           Begin

            MensagemErro('N�o foi encontrado o imposto de Origem para este KitBox, tipo de cliente e opera��o');
            exit;

           End;
           TObjKitbox_ICMS(objeto2).TabelaparaObjeto;

           TObjMateriaisVenda(objeto).Submit_MPOSTO_ICMS_ORIGEM     (TObjKitbox_ICMS(objeto2).IMPOSTO.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_IPI            (TObjKitbox_ICMS(objeto2).imposto_ipi.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_PIS_ORIGEM     (TObjKitbox_ICMS(objeto2).imposto_pis.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_COFINS_ORIGEM  (TObjKitbox_ICMS(objeto2).imposto_cofins.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_CFOP                   (TObjKitbox_ICMS(objeto2).IMPOSTO.CFOP.Get_CODIGO);
           TObjMateriaisVenda(objeto).submit_CSOSN                  (TObjKitbox_ICMS(objeto2).IMPOSTO.CSOSN.Get_codigo);


           if(UFCliente=ESTADOSISTEMAGLOBAL) then
              TObjMateriaisVenda(objeto).Submit_CFOP(TObjFerragem_ICMS(objeto2).IMPOSTO.CFOP.Get_CODIGO)
           else
              TObjMateriaisVenda(objeto).Submit_CFOP(TObjFerragem_ICMS(objeto2).IMPOSTO.Get_CFOP_FORAESTADO);

           if not (TObjKitbox_ICMS(objeto2).Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,Objquery.fieldbyname('kitbox').AsString,operacao)) Then
           Begin

            MensagemErro('N�o foi encontrado o imposto de Destino para este KitBox, tipo de cliente e opera��o');
            exit;

           End;

           TObjKitbox_ICMS(objeto2).TabelaparaObjeto;

           TObjMateriaisVenda(objeto).Submit_IMPOSTO_ICMS_DESTINO    (TObjKitbox_ICMS(objeto2).IMPOSTO.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_MPOSTO_PIS_DESTINO      (TObjKitbox_ICMS(objeto2).imposto_pis.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_COFINS_DESTINO  (TObjKitbox_ICMS(objeto2).imposto_cofins.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_ALIQUOTA                (TObjKitbox_ICMS(objeto2).IMPOSTO.Get_ALIQUOTA);
           TObjMateriaisVenda(objeto).Submit_REDUCAOBASECALCULO      (TObjKitbox_ICMS(objeto2).IMPOSTO.Get_PERC_REDUCAO_BC);
           TObjMateriaisVenda(objeto).Submit_VALORPAUTA              (TObjKitbox_ICMS(objeto2).IMPOSTO.Get_PAUTA);

           TObjMateriaisVenda(objeto).Submit_ALIQUOTACUPOM                  ('0');
           TObjMateriaisVenda(objeto).Submit_PERCENTUALAGREGADO             ('0');
           TObjMateriaisVenda(objeto).Submit_MARGEMVALORAGREGADOCONSUMIDOR  ('0');

           TObjMateriaisVenda(objeto).Submit_VALORFRETE('0');
           TObjMateriaisVenda(objeto).Submit_VALORSEGURO('0');
           TObjMateriaisVenda(objeto).Submit_BC_ICMS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_ICMS('0');
           TObjMateriaisVenda(objeto).Submit_BC_ICMS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_ICMS_ST('0');
           TObjMateriaisVenda(objeto).Submit_BC_IPI('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_IPI('0');
           TObjMateriaisVenda(objeto).Submit_BC_PIS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_PIS('0');
           TObjMateriaisVenda(objeto).Submit_BC_PIS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_PIS_ST('0');
           TObjMateriaisVenda(objeto).Submit_BC_COFINS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_COFINS('0');
           TObjMateriaisVenda(objeto).Submit_BC_COFINS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_COFINS_ST('0');

           TObjMateriaisVenda(objeto).Submit_SITUACAOTRIBUTARIA_TABELAA(TObjKitbox_ICMS(objeto2).IMPOSTO.STA.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_SITUACAOTRIBUTARIA_TABELAB(TObjKitbox_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO);

           if (TObjKitbox_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO = '30') or (TObjKitbox_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='40') or (TObjKitbox_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='41') then
            TObjMateriaisVenda(objeto).Submit_ISENTO('S')
           else
            TObjMateriaisVenda(objeto).Submit_ISENTO('N');


           if(TObjKitbox_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO = '30') or (TObjKitbox_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='10') or (TObjFerragem_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='60') then
            TObjMateriaisVenda(objeto).Submit_SUBSTITUICAOTRIBUTARIA('S')
           else
            TObjMateriaisVenda(objeto).Submit_SUBSTITUICAOTRIBUTARIA('N');

            TObjMateriaisVenda(objeto).Submit_NotaFiscal('');
            TObjMateriaisVenda(objeto).Submit_classificacaofiscal(objquery.fieldbyname('classificacaofiscal').AsString);

            TObjMateriaisVenda(objeto).Submit_QUANTIDADE(Objquery.fieldbyname('QUANTIDADE').AsString);
            TObjMateriaisVenda(objeto).Submit_VALORFINAL(Objquery.fieldbyname('valorfinal').AsString);
            TObjMateriaisVenda(objeto).Submit_ValorUnitario(Objquery.fieldbyname('valor').AsString);
            TObjMateriaisVenda(objeto).Submit_Referencia(Objquery.fieldbyname('referencia').AsString);
            TObjMateriaisVenda(objeto).Submit_Material('4');
           //TObjMateriaisVenda(objeto).Submit_Cor(Objquery.fieldbyname('cor').AsString);
            TObjMateriaisVenda(objeto).Submit_Cor(get_campoTabela('descricao','codigo','tabcor',Objquery.fieldbyname('cor').AsString));
            TObjMateriaisVenda(objeto).Submit_CodigoCor(Objquery.fieldbyname('cor').AsString);
            TObjMateriaisVenda(objeto).Submit_PesoUnitario(Objquery.fieldbyname('peso').AsString);

            if (calculatotaltributos) then
            begin
                try
                    percentualtributo :=  TObjKitbox_ICMS(objeto2).IMPOSTO.Get_PERCENTUALTRIBUTO;
                    vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(Objquery.fieldbyname('valorfinal').AsString);
                    TObjMateriaisVenda(objeto).Submit_percentualtributo(percentualtributo);
                    TObjMateriaisVenda(objeto).Submit_VTOTTRIB(CurrToStr(vtottrib));

                except
                    on e:exception do
                    begin
                       ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                            Exit;
                    end
                end
            end;

            if not (TObjMateriaisVenda(objeto).Salvar(false)) Then
            Begin
              messagedlg('N�o foi poss�vel Gravar o Kitbox '+objquery.fieldbyname('kitbox').asstring,mterror,[mbok],0);
              exit;
            End;

            Objquery.Next;

          end;

        finally

          if (TObjKitbox_ICMS(objeto2) <> nil) then
            TObjKitbox_ICMS(objeto2).Free;

        end;

        {PERSIANA***********************************************************************}

        Objquery.Active:=false;
        Objquery.SQL.Clear;
        Objquery.Sql.add('select p.nome,prod.persiana,p.ncm,');
        Objquery.Sql.add('prod.quantidade,prod.valorfinal,prod.valor,');
        Objquery.Sql.add('p.referencia,prod.cor,p.classificacaofiscal,prod.desconto');
        Objquery.Sql.add('from tabpersiana p');
        Objquery.Sql.add('join tabprodnfedigitada prod on prod.persiana = p.codigo');
        Objquery.Sql.add('where prod.nfedigitada ='+pNfedigitada);

        try
          Objquery.Active:=True;
        except
          Exit;
        end;



        objeto2:=nil;
        if (Objquery.RecordCount > 0) then
          objeto2:=TObjpersiana_ICMS.Create(self.Owner);

        try

          while not (Objquery.Eof) do
          begin

           TObjMateriaisVenda(objeto).state:=dsInsert;

           TObjMateriaisVenda(objeto).ZerarTabela;
           TObjMateriaisVenda(objeto).Submit_Codigo(TObjMateriaisVenda(objeto).Get_NovoCodigo);
           TObjMateriaisVenda(objeto).Nfedigitada.Submit_CODIGO(pNfedigitada);
           TObjMateriaisVenda(objeto).Submit_Descricao(Objquery.fieldbyname('nome').asstring);
           TObjMateriaisVenda(objeto).Submit_Ferragem('');
           TObjMateriaisVenda(objeto).Submit_Vidro('');
           TObjMateriaisVenda(objeto).Submit_Perfilado('');
           TObjMateriaisVenda(objeto).Submit_Diverso('');
           TObjMateriaisVenda(objeto).Submit_Componente('');
           TObjMateriaisVenda(objeto).Submit_KitBox('');
           TObjMateriaisVenda(objeto).Submit_Persiana(Objquery.fieldbyname('persiana').AsString);
           TObjMateriaisVenda(objeto).Submit_NCM(Objquery.fieldbyname('NCM').AsString);
           TObjMateriaisVenda(objeto).Submit_Desconto(Objquery.fieldbyname('DESCONTO').AsString);
           TObjMateriaisVenda(objeto).Submit_indpag(get_campoTabela('indpag','codigo','tabnfedigitada',pNfedigitada));

           If not (TObjPersiana_ICMS(objeto2).Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,Objquery.fieldbyname('persiana').AsString,operacao)) Then
           Begin

               MensagemErro('N�o foi encontrado o imposto de Origem para esta Persiana, tipo de cliente e opera��o'+#13+
                            'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+Objquery.fieldbyname('persiana').AsString);
               exit;

           End;



           TObjPersiana_ICMS(objeto2).TabelaparaObjeto;

           TObjMateriaisVenda(objeto).Submit_MPOSTO_ICMS_ORIGEM    (TObjPersiana_ICMS(objeto2).IMPOSTO.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_IPI           (TObjPersiana_ICMS(objeto2).imposto_ipi.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_PIS_ORIGEM    (TObjPersiana_ICMS(objeto2).imposto_pis.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_COFINS_ORIGEM (TObjPersiana_ICMS(objeto2).imposto_cofins.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_CFOP                  (TObjPersiana_ICMS(objeto2).IMPOSTO.CFOP.Get_CODIGO);
           TObjMateriaisVenda(objeto).submit_CSOSN                 (TObjPersiana_ICMS(objeto2).IMPOSTO.CSOSN.Get_CODIGO);


           if(UFCliente=ESTADOSISTEMAGLOBAL) then
              TObjMateriaisVenda(objeto).Submit_CFOP(TObjFerragem_ICMS(objeto2).IMPOSTO.CFOP.Get_CODIGO)
           else
              TObjMateriaisVenda(objeto).Submit_CFOP(TObjFerragem_ICMS(objeto2).IMPOSTO.Get_CFOP_FORAESTADO);

           if not (TObjPersiana_ICMS(objeto2).Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,Objquery.fieldbyname('persiana').AsString,operacao)) Then
           Begin

            MensagemErro('N�o foi encontrado o imposto de Destino para esta Persiana, tipo de cliente e opera��o'+#13+
                          'Estado: '+UFcliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+Objquery.fieldbyname('persiana').AsString);
            exit;

           End;

           TObjPersiana_ICMS(objeto2).TabelaparaObjeto;

           TObjMateriaisVenda(objeto).Submit_IMPOSTO_ICMS_DESTINO(TObjPersiana_ICMS(objeto2).IMPOSTO.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_MPOSTO_PIS_DESTINO(TObjPersiana_ICMS(objeto2).imposto_pis.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_COFINS_DESTINO(TObjPersiana_ICMS(objeto2).imposto_cofins.Get_CODIGO);

           TObjMateriaisVenda(objeto).Submit_ALIQUOTA(TObjPersiana_ICMS(objeto2).IMPOSTO.Get_ALIQUOTA);
           TObjMateriaisVenda(objeto).Submit_REDUCAOBASECALCULO(TObjPersiana_ICMS(objeto2).IMPOSTO.Get_PERC_REDUCAO_BC);
           TObjMateriaisVenda(objeto).Submit_VALORPAUTA(TObjPersiana_ICMS(objeto2).IMPOSTO.Get_PAUTA);

           TObjMateriaisVenda(objeto).Submit_ALIQUOTACUPOM('0');
           TObjMateriaisVenda(objeto).Submit_PERCENTUALAGREGADO('0');
           TObjMateriaisVenda(objeto).Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');

           TObjMateriaisVenda(objeto).Submit_VALORFRETE('0');
           TObjMateriaisVenda(objeto).Submit_VALORSEGURO('0');
           TObjMateriaisVenda(objeto).Submit_BC_ICMS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_ICMS('0');
           TObjMateriaisVenda(objeto).Submit_BC_ICMS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_ICMS_ST('0');
           TObjMateriaisVenda(objeto).Submit_BC_IPI('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_IPI('0');
           TObjMateriaisVenda(objeto).Submit_BC_PIS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_PIS('0');
           TObjMateriaisVenda(objeto).Submit_BC_PIS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_PIS_ST('0');
           TObjMateriaisVenda(objeto).Submit_BC_COFINS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_COFINS('0');
           TObjMateriaisVenda(objeto).Submit_BC_COFINS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_COFINS_ST('0');

           TObjMateriaisVenda(objeto).Submit_SITUACAOTRIBUTARIA_TABELAA(TObjPersiana_ICMS(objeto2).IMPOSTO.STA.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_SITUACAOTRIBUTARIA_TABELAB(TObjPersiana_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO);

           if(TObjPersiana_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO = '30') or (TObjPersiana_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='40') or (TObjPersiana_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='41') then
              TObjMateriaisVenda(objeto).Submit_ISENTO('S')
           else
              TObjMateriaisVenda(objeto).Submit_ISENTO('N');


           if(TObjPersiana_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO = '30') or (TObjPersiana_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='10') or (TObjFerragem_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='60') then
              TObjMateriaisVenda(objeto).Submit_SUBSTITUICAOTRIBUTARIA('S')
           else
               TObjMateriaisVenda(objeto).Submit_SUBSTITUICAOTRIBUTARIA('N');


           TObjMateriaisVenda(objeto).Submit_NotaFiscal('');
           TObjMateriaisVenda(objeto).Submit_classificacaofiscal(Objquery.fieldbyname('classificacaofiscal').AsString);
           TObjMateriaisVenda(objeto).Submit_QUANTIDADE(Objquery.fieldbyname('QUANTIDADE').AsString);
           TObjMateriaisVenda(objeto).Submit_VALORFINAL(Objquery.fieldbyname('valorfinal').AsString);
           TObjMateriaisVenda(objeto).Submit_ValorUnitario(Objquery.fieldbyname('valor').AsString);
           TObjMateriaisVenda(objeto).Submit_Referencia(Objquery.fieldbyname('referencia').AsString);
           TObjMateriaisVenda(objeto).Submit_Material('5');

           {persiana nao possui cor dizendo o jonatan 05/05/2011}
           //Submit_Cor(fieldbyname('cor').AsString);
           TObjMateriaisVenda(objeto).Submit_PesoUnitario('0');

           if (calculatotaltributos) then
            begin
                try
                    percentualtributo :=  TObjPersiana_ICMS(objeto2).IMPOSTO.Get_PERCENTUALTRIBUTO;
                    vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(Objquery.fieldbyname('valorfinal').AsString);
                    TObjMateriaisVenda(objeto).Submit_percentualtributo(percentualtributo);
                    TObjMateriaisVenda(objeto).Submit_VTOTTRIB(CurrToStr(vtottrib));

                except
                    on e:exception do
                    begin
                       ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                            Exit;
                    end
                end
            end;

           if not (TObjMateriaisVenda(objeto).Salvar(false)) then
           Begin
            messagedlg('N�o foi poss�vel Gravar a Persiana'+Objquery.fieldbyname('persiana').asstring,mterror,[mbok],0);
            exit;
           End;

           Objquery.Next;

          end;

        finally

          if (TObjPersiana_ICMS(objeto2) <> nil ) then
            TObjPersiana_ICMS(objeto2).Free;

        end;



        {DIVERSO***********************************************************************}

        Objquery.Active:=false;
        Objquery.SQL.Clear;
        Objquery.Sql.add('select d.descricao,prod.diverso,d.ncm,');
        Objquery.Sql.add('prod.quantidade,prod.valorfinal,prod.valor,');
        Objquery.Sql.add('d.referencia,prod.cor,d.classificacaofiscal,d.unidade,prod.desconto');
        Objquery.Sql.add('from tabdiverso d');
        Objquery.Sql.add('join tabprodnfedigitada prod on prod.diverso = d.codigo');
        Objquery.Sql.add('where prod.nfedigitada ='+pNfedigitada);


        try
          Objquery.Active:=True;
        except
          Exit;
        end;


        objeto2:=nil;
        if (Objquery.RecordCount > 0) then
          objeto2:=TObjdiverso_ICMS.Create(self.Owner);

        try

          while not (Objquery.Eof) do
          begin

            TObjMateriaisVenda(objeto).state:=dsInsert;

           TObjMateriaisVenda(objeto).ZerarTabela;
           TObjMateriaisVenda(objeto).Submit_Codigo    (TObjMateriaisVenda  (objeto).Get_NovoCodigo);
           TObjMateriaisVenda(objeto).Submit_Descricao (Objquery.fieldbyname('descricao').asstring);
           TObjMateriaisVenda(objeto).Submit_Unidade   (Objquery.fieldbyname('unidade').AsString);
           TObjMateriaisVenda(objeto).Submit_Diverso   (Objquery.fieldbyname('diverso').AsString);
           TObjMateriaisVenda(objeto).Submit_NCM       (Objquery.fieldbyname('NCM').AsString);
           TObjMateriaisVenda(objeto).Submit_Desconto  (Objquery.fieldbyname('DESCONTO').AsString);

           TObjMateriaisVenda(objeto).Submit_Ferragem  ('');
           TObjMateriaisVenda(objeto).Submit_Vidro     ('');
           TObjMateriaisVenda(objeto).Submit_Perfilado ('');
           TObjMateriaisVenda(objeto).Submit_Componente('');
           TObjMateriaisVenda(objeto).Submit_KitBox    ('');
           TObjMateriaisVenda(objeto).Submit_Persiana  ('');
           TObjMateriaisVenda(objeto).Nfedigitada.Submit_CODIGO(pNfedigitada);

           If not (TObjDiverso_ICMS(objeto2).Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,Objquery.fieldbyname('diverso').AsString,operacao)) Then
           Begin
            MensagemErro('N�o foi encontrado o imposto de Origem para esta Ferragem, tipo de cliente e opera��o'+#13+
                         'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+objquery.fieldbyname('Codigo').AsString);
            exit;
           End;

           TObjDiverso_ICMS(objeto2).TabelaparaObjeto;

           TObjMateriaisVenda(objeto).Submit_MPOSTO_ICMS_ORIGEM     (TObjDiverso_ICMS(objeto2).IMPOSTO.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_IPI            (TObjDiverso_ICMS(objeto2).imposto_ipi.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_PIS_ORIGEM     (TObjDiverso_ICMS(objeto2).imposto_pis.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_COFINS_ORIGEM  (TObjDiverso_ICMS(objeto2).imposto_cofins.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_CFOP                   (TObjDiverso_ICMS(objeto2).IMPOSTO.CFOP.Get_CODIGO);
           TObjMateriaisVenda(objeto).submit_CSOSN                  (TObjDiverso_ICMS(objeto2).IMPOSTO.CSOSN.Get_CODIGO);


           if(UFCliente=ESTADOSISTEMAGLOBAL) then
              TObjMateriaisVenda(objeto).Submit_CFOP(TObjFerragem_ICMS(objeto2).IMPOSTO.CFOP.Get_CODIGO)
           else
              TObjMateriaisVenda(objeto).Submit_CFOP(TObjFerragem_ICMS(objeto2).IMPOSTO.Get_CFOP_FORAESTADO);



           if not (TObjDiverso_ICMS(objeto2).Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,Objquery.fieldbyname('diverso').AsString,operacao)) then
           Begin

            MensagemErro('N�o foi encontrado o imposto de Destino para este Diverso, tipo de cliente e opera��o'+#13+
                        'Estado: '+UFcliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+objquery.fieldbyname('Codigo').AsString);
            exit;

           End;

           TObjDiverso_ICMS(objeto2).TabelaparaObjeto;

           TObjMateriaisVenda(objeto).Submit_IMPOSTO_ICMS_DESTINO(TObjDiverso_ICMS(objeto2).IMPOSTO.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_MPOSTO_PIS_DESTINO(TObjDiverso_ICMS(objeto2).imposto_pis.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_IMPOSTO_COFINS_DESTINO(tObjDiverso_ICMS(objeto2).imposto_cofins.Get_CODIGO);

           TObjMateriaisVenda(objeto).Submit_ALIQUOTA(TObjDiverso_ICMS(objeto2).IMPOSTO.Get_ALIQUOTA);
           TObjMateriaisVenda(objeto).Submit_REDUCAOBASECALCULO(TObjDiverso_ICMS(objeto2).IMPOSTO.Get_PERC_REDUCAO_BC);
           TObjMateriaisVenda(objeto).Submit_VALORPAUTA(TObjDiverso_ICMS(objeto2).IMPOSTO.Get_PAUTA);

           TObjMateriaisVenda(objeto).Submit_ALIQUOTACUPOM('0');
           TObjMateriaisVenda(objeto).Submit_PERCENTUALAGREGADO('0');
           TObjMateriaisVenda(objeto).Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');


           TObjMateriaisVenda(objeto).Submit_VALORFRETE('0');
           TObjMateriaisVenda(objeto).Submit_VALORSEGURO('0');
           TObjMateriaisVenda(objeto).Submit_BC_ICMS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_ICMS('0');
           TObjMateriaisVenda(objeto).Submit_BC_ICMS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_ICMS_ST('0');
           TObjMateriaisVenda(objeto).Submit_BC_IPI('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_IPI('0');
           TObjMateriaisVenda(objeto).Submit_BC_PIS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_PIS('0');
           TObjMateriaisVenda(objeto).Submit_BC_PIS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_PIS_ST('0');
           TObjMateriaisVenda(objeto).Submit_BC_COFINS('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_COFINS('0');
           TObjMateriaisVenda(objeto).Submit_BC_COFINS_ST('0');
           TObjMateriaisVenda(objeto).Submit_VALOR_COFINS_ST('0');


           TObjMateriaisVenda(objeto).Submit_SITUACAOTRIBUTARIA_TABELAA(TObjDiverso_ICMS(objeto2).IMPOSTO.STA.Get_CODIGO);
           TObjMateriaisVenda(objeto).Submit_SITUACAOTRIBUTARIA_TABELAB(TObjDiverso_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO);

           if(TObjDiverso_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO = '30') or (TObjDiverso_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='40') or (TObjDiverso_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='41') then
              TObjMateriaisVenda(objeto).Submit_ISENTO('S')
           else
              TObjMateriaisVenda(objeto).Submit_ISENTO('N');


           if(TObjDiverso_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO = '30') or (TObjDiverso_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='10')or (TObjFerragem_ICMS(objeto2).IMPOSTO.STB.Get_CODIGO='60') then
               TObjMateriaisVenda(objeto).Submit_SUBSTITUICAOTRIBUTARIA('S')
           else
               TObjMateriaisVenda(objeto).Submit_SUBSTITUICAOTRIBUTARIA('N');



           TObjMateriaisVenda(objeto).Submit_NotaFiscal('');
           TObjMateriaisVenda(objeto).Submit_classificacaofiscal(Objquery.fieldbyname('classificacaofiscal').AsString);


           TObjMateriaisVenda(objeto).Submit_QUANTIDADE(Objquery.fieldbyname('QUANTIDADE').AsString);
           TObjMateriaisVenda(objeto).Submit_VALORFINAL(Objquery.fieldbyname('valorfinal').AsString);
           TObjMateriaisVenda(objeto).Submit_ValorUnitario(Objquery.fieldbyname('valor').AsString);
           TObjMateriaisVenda(objeto).Submit_Referencia(Objquery.fieldbyname('referencia').AsString);
           TObjMateriaisVenda(objeto).Submit_Material('6');

           //TObjMateriaisVenda(objeto).Submit_Cor(Objquery.fieldbyname('cor').AsString);
           TObjMateriaisVenda(objeto).Submit_Cor(get_campoTabela('descricao','codigo','tabcor',Objquery.fieldbyname('cor').AsString));
           TObjMateriaisVenda(objeto).Submit_CodigoCor(Objquery.fieldbyname('cor').AsString);
           
           TObjMateriaisVenda(objeto).Submit_PesoUnitario('0');

            if (calculatotaltributos) then
            begin
                try
                    percentualtributo :=  TObjDiverso_ICMS(objeto2).IMPOSTO.Get_PERCENTUALTRIBUTO;
                    vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(Objquery.fieldbyname('valorfinal').AsString);
                    TObjMateriaisVenda(objeto).Submit_percentualtributo(percentualtributo);
                    TObjMateriaisVenda(objeto).Submit_VTOTTRIB(CurrToStr(vtottrib));

                except
                    on e:exception do
                    begin
                       ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                            Exit;
                    end
                end
            end;

           if not (TObjMateriaisVenda(objeto).Salvar(false)) Then
           Begin
            messagedlg('N�o foi poss�vel Gravar Diverso '+objquery.fieldbyname('diverso').asstring,mterror,[mbok],0);
            exit;
           End;

            Objquery.Next;

          end;

        finally

          if (TObjDiverso_ICMS(objeto2) <> nil) then
            TObjDiverso_ICMS(objeto2).Free;       

        end;

      finally


        TObjMateriaisVenda(objeto).free;

      end;

  except

    Exit;

  end;
                           
  result:=true;


end;

procedure TObjPRODNFEDIGITADA.pesquisaCor(codigoFerragem,chaveEstrangeira,chavePrimaria,tabela:string;LABELNOME:TLabel;sender:TObject);
var
  fpesquisaLocal:TFpesquisa;
begin


  with (Self.Objquery) do
  begin

    Active:=False;
    sql.Clear;
    sql.Add('select cor.codigo as codigo_cor, cor.descricao as nome_cor');
    sql.Add('from '+tabela);
    sql.Add('join '+tabela+'cor '+'on '+tabela+'cor'+'.'+chaveEstrangeira+' = '+tabela+'.'+chavePrimaria);
    sql.Add('join tabcor cor on cor.codigo = '+tabela+'cor .cor');
    sql.Add('where '+tabela+'.codigo = '+codigoFerragem);

  end;

  Try

    Fpesquisalocal:=Tfpesquisa.create(Nil);

    If (FpesquisaLocal.PreparaPesquisa(Objquery.SQL.Text,'CORES ('+uppercase(chaveEstrangeira)+')',nil)) Then
    Begin

      Try

        If (FpesquisaLocal.showmodal=mrok) Then
        Begin

          TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo_cor').asstring;

          if ( LabelNome <> NIl) Then
            LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome_cor').asstring

        End;

      Finally

        FpesquisaLocal.QueryPesq.close;
        
      End;

    End;


    
  finally

    FreeandNil(FPesquisaLocal);

  end;


end;

function TObjPRODNFEDIGITADA.geraNotaFiscalParaNFe(PnotaFiscal:TStringList;pNfeDigitada:string;operacao:string;Corte:Integer;pModoContingencia:Boolean):Boolean;
var
  pCLiente,pFornecedor,sta,stb,pnfe,fraseVtotTrib,infCpl:string;
  valTotTrib,totalProdutos,percentualTotal:Currency;
  impostoautomatico:string;
begin

  Result:=False;

  impostoautomatico := get_campoTabela('impostoautomatico','codigo','TABNFEDIGITADA',pNfeDigitada);

  if impostoautomatico = 'S' then
  begin

    if not (Self.gravaMateriaisVendidos_2(pNfeDigitada,operacao)) then
    begin
      ShowMessage('Erro ao gravar materiais venda');
      Exit;
    end;

    if not (self.ObjNotafiscalObjetos.CalculaBaseDeCalculoNovaForma('','0',pNfeDigitada)) then
    begin
      ShowMessage('Erro ao calcular base de calculo nova forma');
      Exit;
    end;

  end
  else
  begin

    if not (self.gravaMateriaisImpostoManual(pNfeDigitada)) then
    begin
      ShowMessage('Erro ao gravar materiais venda para imposto manual');
      Exit;
    end;

    //FDataModulo.IBTransaction.CommitRetaining;
    //Exit;//tirar

  end;                                                                 


  if not (self.ObjNotafiscalObjetos.GeraNotaFiscalNova(PnotaFiscal,'',operacao,corte,pNfeDigitada)) then
  begin
   ShowMessage('Erro na gera��o da nota fiscal');
   Exit;
  end;

  if (calculatotaltributos) then
  begin
    //alterando as informacoes complementares para vTotTrib > 0
     if ObjParametroGlobal.ValidaParametro('NFE FRASE TOTAL DOS TRIBUTOS') then
    fraseVtotTrib := ObjParametroGlobal.Get_Valor;
    
    if (Pos(':valor',fraseVtotTrib) = 0) or (Pos(':percentual',fraseVtotTrib) = 0) then
    begin
      MensagemErro('Parametro: "NFE FRASE TOTAL DOS TRIBUTOS" inv�lido');
      Exit;
    end;

    valTotTrib := StrToCurrDef(get_campoTabela('SUM(vTotTrib) as vTotTrib','nfedigitada','tabmateriaisvenda',pNfeDigitada,'vTotTrib'),0);
    totalProdutos := StrToCurrDef(get_campoTabela('SUM(valorfinal) as valorfinal','nfedigitada','tabmateriaisvenda',pNfeDigitada,'valorfinal'),0);
    percentualTotal := (valTotTrib*100)/totalProdutos;
    infCpl := '';
    infCpl := ' ';
    infCpl := StringReplace(fraseVtotTrib,':valor',formata_valor(CurrToStr(valTotTrib),2,false),[]);
    infCpl := StringReplace(infCpl,':percentual',formata_valor(CurrToStr(percentualTotal),2,false),[]);

    if not exec_sql('update tabnfedigitada set informacoes = informacoes||'+QuotedStr(infCpl) + ' where codigo = '+pNfeDigitada) then
    begin
      MensagemErro('Erro ao alterar infCpl total dos tributos na tabnfedigitada');
      Exit;
    end;

    if not exec_sql('update tabnotafiscal set dadosadicionais = '''' where dadosadicionais is null and codigo = '+PnotaFiscal[0]) then
    begin
      MensagemErro('Erro ao alterar dadosadicionais na tabnotafiscal');
      Exit;
    end;

    if not exec_sql('update tabnotafiscal set dadosadicionais = dadosadicionais||'+QuotedStr(infCpl)+' where codigo = '+PnotaFiscal[0]) then
    begin
      MensagemErro('Erro ao alterar infCpl total dos tributos na tabnotafiscal');
      Exit;
    end;
  end;
<<<<<<< .mine
  //FDataModulo.IBTransaction.CommitRetaining;

  if nfenova then
  begin

=======
                         
>>>>>>> .r1260
    {NF}
    SELF.ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo (PnotaFiscal[0]);
    SELF.ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
    pnfe := SELF.ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Get_Numero;

    objTransNFE.pCodigoNF := StrToInt(PnotaFiscal[0]);
    objTransNFE.numeroNFE := pnfe;

    objTransNFE.contingencia := pModoContingencia;
    
    if(pModoContingencia=True)
    then exec_sql('update tabnotafiscal set situacao=''T'' where codigo='+PnotaFiscal[0]); 

    if self.NfeEntrada then
      objTransNFE.TipoNF := 'ENTRADA'
    else
      objTransNFE.TipoNF := 'SAIDA';

    if self.nfeDevolucao then
    begin
      objTransNFE.nfeDevolucao := True;
      objTransNFE.modeloRef := StrToInt(self.tiporef);
      objTransNFE.chaveRef  := self.refNFe_ref;
      objTransNFE.cUF_ref   := self.cUF_ref;
      objTransNFE.AAMM_ref  := self.AAMM_ref;
      objTransNFE.CNPJ_ref  := self.CNPJ_ref;
      objTransNFE.serie_ref := self.serie_ref;
      objTransNFE.nNF_ref   := Self.nNF_ref;
      objTransNFE.infComplementares := self.adicional_ref;
      exec_sql('update tabnotafiscal set DADOSADICIONAIS='+#39+SELF.ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Get_DadosAdicionais+' '+adicional_ref+#39+' where codigo='+PnotaFiscal[0]);

    end
    else if self.nfeComplementar then
    begin
      objTransNFE.nfeComplementar := True;
      objTransNFE.infComplementares := self.adicional_ref;
      objTransNFE.chaveRef := self.refNFe_ref;
      exec_sql('update tabnotafiscal set DADOSADICIONAIS='+#39+SELF.ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Get_DadosAdicionais+' '+adicional_ref+#39+' where codigo='+PnotaFiscal[0]);
    end;

    //FDataModulo.IBTransaction.CommitRetaining;
    //Exit;

    if impostoautomatico = 'N' then
      objTransNFE.arquivoSQL := '\sqlManual.txt';
      
    objTransNFE.CRT := get_campoTabela('crt','codigo','TABNFEDIGITADA',pNfeDigitada);

    objTransNFE.Transmite;

    if objTransNFE.strErros.Count > 0 then
    begin
      ShowMessage(objTransNFE.strErros.Text);
      result := False;
    end
    else
    begin
      {editando a NF-e}
      ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.LocalizaCodigo(pnfe);
      ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.TabelaparaObjeto;
      ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Status := dsedit;
      ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Submit_CERTIFICADO  (ObjEmpresaGlobal.get_certificado);
      ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Submit_ReciboEnvio  (objTransNFE.retornaRecibo);
      ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.submit_chave_acesso (objTransNFE.retornaChaveAcesso);
      ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.submit_arquivo_xml  (objTransNFE.retornaArquivoXML);
      ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Submit_ARQUIVO      (objTransNFE.retornaArquivoXML);

      if (pModoContingencia=True) then
        ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.nfe.Submit_STATUSNOTA('T')//contingencia
      else
        ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.nfe.Submit_STATUSNOTA('G');//Gerada

      if objTransNFE.denegada then
         ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.nfe.Submit_STATUSNOTA('D');

      if (ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Salvar(false) = False) then
        MensagemErro('N�o foi poss�vel alterar o status  para "GERADA" na NF-e: '+PnotaFiscal[0]+' contate o suporte'); {marcar manualmente para G}

      {editando a NF}
      ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;
      ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Submit_Codigo(pnfe);

      if (pModoContingencia=True) then
        ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('T')
      else
        ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('I');

      if objTransNFE.denegada then
         ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('D');

      if not (ObjNotafiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Salvar (false)) then
        MensagemErro('N�o foi possivel alterar o status para "I" na NF: '+pnfe+' contate o suporte');{marcar manualmente para I}

      result := True;
      FDataModulo.IBTransaction.CommitRetaining;
      
    end;



  {if not (Self.ObjNotafiscalObjetos.geraNFE_2(PnotaFiscal,'',false,false,pNfeDigitada)) then
  begin
    result:=false;
    Exit;
  end;

  result := true; }

end;

function TObjPRODNFEDIGITADA.get_proximaNFE: string;
begin

  with (self.Objquery) do
  begin

   Close;
   SQL.Clear;
   SQL.Add ('select first 1 codigo,numero');
   SQL.Add ('from tabnotafiscal');
   SQL.Add ('where upper (situacao) = ''N'' and modelo_nf = 2');
   SQL.Add('order by codigo');

   Open;
   First;
   result := fieldbyname('codigo').AsString;

  end;

end;

function TObjPRODNFEDIGITADA.estornaMateriaisVenda(pnfeDigitada: string): Boolean;
begin

   result := False;

   try

     if (pnfeDigitada = '') then
      Exit;

     with (self.Objquery) do
     begin

      Active := false;
      sql.Clear;
      SQL.Add('delete from TABMATERIAISVENDA where nfedigitada = '+pnfeDigitada);
      Active := True;

     end;

   except

    Exit;

   end;

   FDataModulo.IBTransaction.CommitRetaining;
   result := True;


end;

procedure TObjPRODNFEDIGITADA.geraNFeEntrada(pCodigo: string);
var
  strNota:TStringList;
  msg:string;
begin
  if (pcodigo<>'') and (self.NfeDigitada.status=dsinactive) then
  begin
    self.ObjNotafiscalObjetos.NfeEntrada := True;
    strNota := TStringList.Create;
    try
      strNota.Add (self.get_proximaNFE());
      self.nfeDevolucao:=False;
      self.nfeComplementar:=False;
      self.NfeEntrada:=True;

      if (self.GeraNotaFiscalParaNFE(strNota,pcodigo,'2',999)=False) then
      begin
        MensagemErro('Erro ao Gerar NF-e de Entrada');
        FDataModulo.IBTransaction.RollbackRetaining
      end else
      begin
      FDataModulo.IBTransaction.CommitRetaining;
      end;
    finally
      FreeAndNil (strNota);
    end;
  end;

end;

procedure TObjPRODNFEDIGITADA.submit_nfeEntrada(valor: Boolean);
begin

  Self.NfeEntrada:=valor;

end;

procedure TObjPRODNFEDIGITADA.submit_nfeDevolucao(valor: boolean);
begin
self.nfeDevolucao:=valor;
end;

procedure TObjPRODNFEDIGITADA.submit_nfeComplementar(valor: Boolean);
begin
self.nfeComplementar:=valor;
end;

function TObjPRODNFEDIGITADA.get_valoricms_st: string;
begin
  result:= self.VALORICMS_ST;
  if Trim(Result) = '' then
    Result := '0';
end;

function TObjPRODNFEDIGITADA.get_valoripi: string;
begin
  result := self.VALORIPI;
  if Trim(Result) = '' then
    Result := '0';
end;

procedure TObjPRODNFEDIGITADA.submit_valoripi(p: string);
begin

  self.VALORIPI := p;
end;

procedure TObjPRODNFEDIGITADA.submit_vaoricms_st(p: string);
begin
  self.VALORICMS_ST := p;
end;



function TObjPRODNFEDIGITADA.get_cfop: string;
begin
  Result := self.CFOP;
end;

procedure TObjPRODNFEDIGITADA.submit_cfop(p: string);
begin
  self.CFOP := p;
end;

end.



