unit UobjORDEMINSTALACAO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,UobjCOLOCADOR,UMostraBarraProgresso
;
//USES_INTERFACE


Type
   TObjORDEMINSTALACAO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                COLOCADOR                                   :TObjCOLOCADOR;
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;

                Procedure Submit_DATA(parametro: string);
                Function Get_DATA: string;
                procedure EdtColocadorKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtColocadorKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);overload;
                procedure EdtClienteKeyDown(Sender:TObject;var Key: Word; Shifth:TShiftState);
                procedure EdtPedidoProjetoAgendadoKeyDown(Sender:TObject;var Key: Word; Shifth:TShiftState);

                Function LocalizPorData(parametro:string;var parametrocodigo:string;colocador:string):Boolean;

                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               DATA:string;
               DATAC:string;
               DATAM:string;
               USERC:string;
               USERM:string;
//CODIFICA VARIAVEIS PRIVADAS

               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
               Procedure OrdemdeInstalacao(parametro:string);
               Procedure RelacaoOrdensInstalacao(parametro:string);
               procedure EdtClienteAgendaInstalacaoKeyDown(Sender: TObject; var Key: Word;
               Shift: TShiftState);
               procedure RelacaoOrdemInstalacoesReagendadas(parametro:string);
               procedure RelacaoOrdemInstalacoesCanceladas;

   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UReltxtRDPRINT,rdprint, UobjRELPERSREPORTBUILDER;




Function  TObjORDEMINSTALACAO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;

        If(fieldbyname('COLOCADOR').asstring<>'')
        Then Begin
                 If (Self.COLOCADOR.LocalizaCodigo(fieldbyname('COLOCADOR').asstring)=False)
                 Then Begin
                          Messagedlg('PEDIDO N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.COLOCADOR.TabelaparaObjeto;
        End;

        Self.DATA:=fieldbyname('DATA').asstring;

        result:=True;
     End;
end;


Procedure TObjORDEMINSTALACAO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('COLOCADOR').asstring:=Self.COLOCADOR.Get_CODIGO;
        ParamByName('DATA').asstring:=Self.DATA;
  End;
End;

//***********************************************************************

function TObjORDEMINSTALACAO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjORDEMINSTALACAO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        COLOCADOR.ZerarTabela;
        DATA:='';





     End;
end;

Function TObjORDEMINSTALACAO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjORDEMINSTALACAO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjORDEMINSTALACAO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;

//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjORDEMINSTALACAO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.DATA);
     Except
           Mensagem:=mensagem+'/DATA';
     End;

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjORDEMINSTALACAO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjORDEMINSTALACAO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro ORDEMINSTALACAO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,COLOCADOR,DATA,DATAC,DATAM,USERC,USERM');
           SQL.ADD(' from  TABORDEMINSTALACAO');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjORDEMINSTALACAO.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjORDEMINSTALACAO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjORDEMINSTALACAO.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjORDEMINSTALACAO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        COLOCADOR:=TObjCOLOCADOR.Create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABORDEMINSTALACAO(CODIGO,COLOCADOR,DATA');
                InsertSQL.add(' ,DATAC,DATAM,USERC,USERM)');
                InsertSQL.add('values (:CODIGO,:COLOCADOR,:DATA,:DATAC,:DATAM,:USERC');
                InsertSQL.add(' ,:USERM)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABORDEMINSTALACAO set CODIGO=:CODIGO,COLOCADOR=:COLOCADOR');
                ModifySQL.add(',DATA=:DATA,DATAC=:DATAC,DATAM=:DATAM,USERC=:USERC,USERM=:USERM');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABORDEMINSTALACAO where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjORDEMINSTALACAO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjORDEMINSTALACAO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select tabordeminstalacao.codigo,tabordeminstalacao.data, tabordeminstalacao.colocador,tabfuncionarios.nome');
     Self.ParametroPesquisa.Add('from TabORDEMINSTALACAO');
     Self.ParametroPesquisa.Add('join tabcolocador on tabcolocador.codigo=tabordeminstalacao.colocador');
     Self.ParametroPesquisa.Add('join tabfuncionarios on tabfuncionarios.codigo=tabcolocador.funcionario');
     Result:=Self.ParametroPesquisa;
end;

function TObjORDEMINSTALACAO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de ORDEMINSTALACAO ';
end;


function TObjORDEMINSTALACAO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENORDEMINSTALACAO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENORDEMINSTALACAO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjORDEMINSTALACAO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    COLOCADOR.Free;
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjORDEMINSTALACAO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjORDEMINSTALACAO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjORDEMINSTALACAO.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjORDEMINSTALACAO.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;

procedure TObjORDEMINSTALACAO.Submit_DATA(parametro: string);
begin
        Self.DATA:=Parametro;
end;
function TObjORDEMINSTALACAO.Get_DATA: string;
begin
        Result:=Self.DATA;
end;

//CODIFICA EXITONKEYDOWN
procedure TObjORDEMINSTALACAO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJORDEMINSTALACAO';

          With RgOpcoes do
          Begin
                items.clear;
                Items.Add('Rela��o de Instala��es por Colocador');
                Items.Add('Ordem de Instala��o');
                Items.Add('Rela��o de Instala��es Canceladas');
                Items.Add('Rela��o de Instala��es Reagendadas');

          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of

                  0:Self.RelacaoOrdensInstalacao(pcodigo);
                  1:Self.OrdemdeInstalacao(pcodigo);
                  2:self.RelacaoOrdemInstalacoesCanceladas;
                  3:Self.RelacaoOrdemInstalacoesReagendadas(Pcodigo);
          End;
     end;

end;


procedure TObjORDEMINSTALACAO.EdtColocadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Colocador.Get_Pesquisa,Colocador.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Colocador.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjORDEMINSTALACAO.EdtColocadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Colocador.Get_Pesquisa,Colocador.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Colocador.RETORNACAMPOCODIGO).asstring;

                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjORDEMINSTALACAO.LocalizPorData(parametro:string; var parametrocodigo:string;colocador:String):Boolean;
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro DATA da ORDEMINSTALACAO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,COLOCADOR,DATA,DATAC,DATAM,USERC,USERM');
           SQL.ADD(' from  TABORDEMINSTALACAO');
           SQL.ADD(' WHERE DATA='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(parametro))+#39);
           SQL.Add('and colocador='+colocador);

           Open;

           If (recordcount>0) then
           begin
                parametrocodigo:=fieldbyname('codigo').AsString;
                Result:=True;

           end
           Else Result:=False;
       End;
end;

procedure TObjORDEMINSTALACAO.EdtClienteAgendaInstalacaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Pesquisa:string;
begin

     If (key <>vk_f9)
     Then exit;

     Pesquisa:='';
     Pesquisa:=Pesquisa+'select distinct (tabcliente.codigo), tabcliente.nome, tabcliente.endereco, tabcliente.cidade,tabcliente.estado';
     Pesquisa:=Pesquisa+' from tabromaneiosordeminstalacao';
     Pesquisa:=Pesquisa+' join tabpedido_proj on tabpedido_proj.codigo=tabromaneiosordeminstalacao.pedidoprojeto';
     Pesquisa:=Pesquisa+' join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto';
     Pesquisa:=Pesquisa+' join tabordeminstalacao on tabordeminstalacao.codigo=tabromaneiosordeminstalacao.ordeminstalacao';
     Pesquisa:=Pesquisa+' join tabpedido on tabpedido.codigo=tabpedido_proj.pedido';
     Pesquisa:=Pesquisa+' join tabcliente on tabcliente.codigo=tabpedido.cliente';
     Pesquisa:=Pesquisa+' where tabordeminstalacao.codigo='+Get_CODIGO;
     Pesquisa:=Pesquisa+' and tabordeminstalacao.colocador='+colocador.Get_CODIGO;

     
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Pesquisa,'Cliente Agendados',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjORDEMINSTALACAO.OrdemdeInstalacao(parametro:string);
var
  ObjRelPersReportBuilder:TObjRELPERSREPORTBUILDER;
  CAMPOWHERE:String;
begin
      if(parametro='')
      then Exit;

      LocalizaCodigo(parametro);
      TabelaparaObjeto;

      try

            try
                   ObjRelPersreportBuilder:=TObjRELPERSREPORTBUILDER.Create;
            except
                   MensagemErro('Erro ao tentar criar o Objeto ObjRelPersreportBuilder');
                   exit;
            end;

            if (ObjRelPersReportBuilder.LocalizaNOme('ORDEM INSTALACAO') = false)
            then Begin
                       MensagemErro('O Relatorio de ReporteBuilder "ORDEM DE MEDICAO" n�o foi econtrado');
                       exit;
            end;

            with FfiltroImp do
            begin
                DesativaGrupos;

                Grupo01.Enabled:=True;
                LbGrupo01.Caption:='Cliente';
                edtgrupo01.Color:=$005CADFE;
                edtgrupo01.OnKeyDown:=EdtClienteAgendaInstalacaoKeyDown;

                ShowModal;

                if(Tag=0)
                then Exit;

                CAMPOWHERE:=CAMPOWHERE+' WHERE TABORDEMINSTALACAO.codigo='+parametro;
                CAMPOWHERE:=CAMPOWHERE+' AND TABCLIENTE.CODIGO='+edtgrupo01.Text;

                ObjRelPersReportBuilder.TabelaparaObjeto;
                ObjRelPersReportBuilder.SQLCabecalhoPreenchido:=StrReplace(ObjRelPersReportBuilder.Get_SQLCabecalho,':PCODIGO',edtgrupo01.Text);
                ObjRelPersReportBuilder.SQLRepeticaoPreenchido:=StrReplace(ObjRelPersReportBuilder.get_SQLRepeticao,':PCODIGO',CAMPOWHERE);
                //ObjRelPersReportBuilder.SQLRepeticaoPreenchido:= ObjRelPersReportBuilder.SQLRepeticaoPreenchido+'' 



                ObjRelPersReportBuilder.ChamaRelatorio(false);
            end;
      finally
            ObjRelPersreportBuilder.Free;
      end;

end;

procedure TObjORDEMINSTALACAO.EdtClienteKeyDown(Sender:TObject;var Key: Word; Shifth:TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabcliente','Clientes',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjORDEMINSTALACAO.EdtPedidoProjetoAgendadoKeyDown(Sender:TObject;var Key: Word; Shifth:TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Pesquisa:string;
begin
     If (key <>vk_f9)
     Then exit;

     Pesquisa:=Pesquisa+'SELECT TABCLIENTE.CODIGO AS CODIGOCLIENTE';
     Pesquisa:=Pesquisa+' ,TABPROJETO.DESCRICAO AS NOMEPROJETO';
     Pesquisa:=Pesquisa+' ,TABCLIENTE.NOME';
     Pesquisa:=Pesquisa+' ,TABROMANEIOSORDEMINSTALACAO.CONCLUIDO';
     Pesquisa:=Pesquisa+' ,TABROMANEIOSORDEMINSTALACAO.HORARIO';
     Pesquisa:=Pesquisa+' ,TABROMANEIOSORDEMINSTALACAO.TEMPOMEDIOINSTALACAO';
     Pesquisa:=Pesquisa+' ,TABROMANEIOSORDEMINSTALACAO.TEMPOFINAL AS HORARIOTERMINO';
     Pesquisa:=Pesquisa+' ,TABROMANEIOSORDEMINSTALACAO.PEDIDOPROJETO';
     Pesquisa:=Pesquisa+' ,TABROMANEIOSORDEMINSTALACAO.ROMANEIO';
     Pesquisa:=Pesquisa+' ,TABROMANEIOSORDEMINSTALACAO.OBSERVACAO';
     Pesquisa:=Pesquisa+' ,TABORDEMINSTALACAO.DATA';
     Pesquisa:=Pesquisa+' FROM TABROMANEIOSORDEMINSTALACAO';
     Pesquisa:=Pesquisa+' JOIN TABPEDIDO_PROJ ON TABPEDIDO_PROJ.CODIGO=TABROMANEIOSORDEMINSTALACAO.PEDIDOPROJETO';
     Pesquisa:=Pesquisa+' JOIN TABPROJETO ON TABPROJETO.CODIGO=TABPEDIDO_PROJ.PROJETO';
     Pesquisa:=Pesquisa+' JOIN TABORDEMINSTALACAO ON TABORDEMINSTALACAO.CODIGO=TABROMANEIOSORDEMINSTALACAO.ORDEMINSTALACAO';
     Pesquisa:=Pesquisa+' JOIN TABPEDIDO ON TABPEDIDO.CODIGO=TABPEDIDO_PROJ.PEDIDO';
     Pesquisa:=Pesquisa+' join tabcliente on tabcliente.codigo=tabpedido.cliente';
     Pesquisa:=Pesquisa+' --WHERE TABORDEMINSTALACAO.DATA=';
     Pesquisa:=Pesquisa+' --AND TABCLIENTE.CODIGO=1';
     Pesquisa:=Pesquisa+' order by tabordeminstalacao.data';

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Pesquisa,'Pedidos/Projetos Agendados',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('pedidoprojeto').asstring;

                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjORDEMINSTALACAO.RelacaoOrdemInstalacoesCanceladas;
var
  Query:TIBQuery;
  ColocadorCorrente:string;
  ClienteCorrente:string;
  PProjetoCorrente:string;
begin
      Query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;

      try
            with FfiltroImp do
            begin
                 DesativaGrupos;

                 Grupo01.Enabled:=True;
                 LbGrupo01.Caption:='Colocador';
                 edtgrupo01.Color:=$005CADFE;
                 edtgrupo01.OnKeyDown:=EdtColocadorKeyDown;

                 Grupo02.Enabled:=True;
                 LbGrupo02.Caption:='Cliente';
                 edtgrupo02.Color:=$005CADFE;
                 edtgrupo02.OnKeyDown:=EdtClienteKeyDown;

                 Grupo03.Enabled:=True;
                 lbgrupo03.Caption:='Pedido/Projeto';
                 edtgrupo03.Color:=$005CADFE;
                 edtgrupo03.OnKeyDown:=EdtPedidoProjetoAgendadoKeyDown;

                 Grupo04.Enabled:=True;
                 edtgrupo04.EditMask:='99/99/9999';
                 LbGrupo04.Caption:='Data Inicial' ;

                 Grupo05.Enabled:=True;
                 edtgrupo05.EditMask:='99/99/9999';
                 lbgrupo05.Caption:='Data Final';

                 ShowModal;

                 if(Tag=0)
                 then Exit;

            end;

            with Query do
            begin
                 Close;
                 SQL.Clear;
                 SQL.Add('select tabprojeto.codigo as projeto, tabprojeto.descricao, tabpedido_proj.codigo, tabcliente.nome as cliente,');
                 SQL.Add('tabcancelamentoinstalacao.*,tabfuncionarios.nome as nomecolocador,tabcliente.codigo as codigocliente');
                 SQL.Add('from tabcancelamentoinstalacao');
                 SQL.Add('join tabpedido_proj on tabpedido_proj.codigo=tabcancelamentoinstalacao.pedidoprojeto');
                 SQL.Add('join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto');
                 SQL.Add('join tabpedido on tabpedido.codigo=tabpedido_proj.pedido');
                 SQL.Add('join tabcliente on tabcliente.codigo=tabpedido.cliente');
                 SQL.Add('join tabcolocador on tabcolocador.codigo=tabcancelamentoinstalacao.colocador');
                 SQL.Add('join tabfuncionarios on tabfuncionarios.codigo=tabcolocador.funcionario');

                 if(FfiltroImp.edtgrupo01.text<>'')
                 then SQL.Add('and colocador='+#39+FfiltroImp.edtgrupo01.text+#39);
                 if(FfiltroImp.edtgrupo02.text<>'')
                 then SQL.Add('and cliente='+#39+FfiltroImp.edtgrupo02.text+#39);
                 if(FfiltroImp.edtgrupo03.text<>'')
                 then SQL.Add('and pedidoprojeto='+#39+FfiltroImp.edtgrupo03.text+#39);

                 SQL.Add('order by colocador,codigocliente,pedidoprojeto,tabcancelamentoinstalacao.codigo');

                 Open;

                 
                 if(recordcount=0)
                 then Exit;

                 FreltxtRDPRINT.ConfiguraImpressao;
                 FreltxtRDPRINT.LinhaLocal:=3;
                 FreltxtRDPRINT.RDprint.Abrir;
                 if (FreltxtRDPRINT.RDprint.Setup=False) then
                 begin
                      FreltxtRDPRINT.rdprint.Fechar;
                      exit;
                 end;
                 FMostraBarraProgresso.ConfiguracoesIniciais(6,0);
                 FMostraBarraProgresso.lbmensagem.caption:='Gerando Relat�rios';
                 FMostraBarraProgresso.Show;
                 FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,30,'Rela��o de Instala��o Canceladas',[negrito]);
                 FreltxtRDPRINT.incrementalinha(2);
                 FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,1,'Colocador: ',[negrito]);
                 FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,FieldByName('colocador').AsString+' - '+FieldByName('nomecolocador').AsString,[negrito]);
                 FreltxtRDPRINT.incrementalinha(2);
                 ColocadorCorrente:=fieldbyname('colocador').AsString;

                 FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,'Projeto: ',[negrito]);
                 FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,21,FieldByName('projeto').AsString+' - '+FieldByName('descricao').AsString,[negrito]);
                 FreltxtRDPRINT.incrementalinha(1);
                 PProjetoCorrente:=fieldbyname('pedidoprojeto').AsString;

                 FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,'Cliente: ',[negrito]);
                 FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,21,FieldByName('codigocliente').AsString+' - '+FieldByName('cliente').AsString,[negrito]);
                 FreltxtRDPRINT.incrementalinha(2);
                 ClienteCorrente:=fieldbyname('codigocliente').AsString;

                 FreltxtRDPRINT.RDprint.ImpF(FreltxtRDPRINT.linhalocal,1,CompletaPalavra('Data Cancelamento',18,' ')+' '+
                                             CompletaPalavra('Data Instala��o',18,' ')+' '+
                                             CompletaPalavra('Horario',10,' ')+' '+
                                             CompletaPalavra('Motivo Cancelamento',60,' '),[negrito]);
                 FreltxtRDPRINT.incrementalinha(1);

                 while not Eof do
                 begin
                      FMostraBarraProgresso.IncrementaBarra1(1);
                      FMostraBarraProgresso.Show;

                      if(ColocadorCorrente<>FieldByName('colocador').AsString) then
                      begin
                            FreltxtRDPRINT.incrementalinha(2);
                            FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,1,'Colocador: ',[negrito]);
                            FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,FieldByName('colocador').AsString+' - '+FieldByName('nomecolocador').AsString,[negrito]);
                            FreltxtRDPRINT.incrementalinha(2);
                            ColocadorCorrente:=fieldbyname('colocador').AsString;

                            FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,'Projeto: ',[negrito]);
                            FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,21,FieldByName('projeto').AsString+' - '+FieldByName('descricao').AsString,[negrito]);
                            FreltxtRDPRINT.incrementalinha(1);
                            PProjetoCorrente:=fieldbyname('pedidoprojeto').AsString;

                            FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,'Cliente: ',[negrito]);
                            FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,21,FieldByName('codigocliente').AsString+' - '+FieldByName('cliente').AsString,[negrito]);
                            FreltxtRDPRINT.incrementalinha(2);
                            ClienteCorrente:=fieldbyname('codigocliente').AsString;

                            FreltxtRDPRINT.RDprint.ImpF(FreltxtRDPRINT.linhalocal,1,CompletaPalavra('Data Cancelamento',18,' ')+' '+
                                                         CompletaPalavra('Data Instala��o',18,' ')+' '+
                                                         CompletaPalavra('Horario',10,' ')+' '+
                                                         CompletaPalavra('Motivo Cancelamento',60,' '),[negrito]);
                            FreltxtRDPRINT.incrementalinha(1);

                      end
                      else
                      begin
                            if(PProjetoCorrente<>FieldByName('pedidoprojeto').AsString) or (ClienteCorrente<>FieldByName('codigocliente').AsString) then
                            begin
                                  FreltxtRDPRINT.incrementalinha(2);
                                  FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,'Projeto: ',[negrito]);
                                  FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,21,FieldByName('projeto').AsString+' - '+FieldByName('descricao').AsString,[negrito]);
                                  FreltxtRDPRINT.incrementalinha(1);
                                  PProjetoCorrente:=fieldbyname('pedidoprojeto').AsString;

                                  FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,'Cliente: ',[negrito]);
                                  FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,21,FieldByName('codigocliente').AsString+' - '+FieldByName('cliente').AsString,[negrito]);
                                  FreltxtRDPRINT.incrementalinha(2);
                                  ClienteCorrente:=fieldbyname('codigocliente').AsString;

                                  FreltxtRDPRINT.RDprint.ImpF(FreltxtRDPRINT.linhalocal,1,CompletaPalavra('Data Cancelamento',18,' ')+' '+
                                                               CompletaPalavra('Data Instala��o',18,' ')+' '+
                                                               CompletaPalavra('Horario',10,' ')+' '+
                                                               CompletaPalavra('Motivo Cancelamento',60,' '),[negrito]);
                                  FreltxtRDPRINT.incrementalinha(1);
                            end;

                      end;

                      FreltxtRDPRINT.RDprint.ImpF(FreltxtRDPRINT.linhalocal,1,CompletaPalavra(fieldbyname('datacancelamento').AsString,18,' ')+' '+
                                                   CompletaPalavra(fieldbyname('ultimadatainstalacao').AsString,18,' ')+' '+
                                                   CompletaPalavra(fieldbyname('ultimohorarioinstalacao').AsString,10,' ')+' '+
                                                   CompletaPalavra(fieldbyname('justificativa').AsString,60,' '),[negrito]);
                      FreltxtRDPRINT.incrementalinha(1);


                      Next;
                 end;

                 FMostraBarraProgresso.close;
                 FreltxtRDPRINT.RDprint.Fechar;
            end;

      finally
            FreeAndNil(Query);
      end;

end;

procedure TObjORDEMINSTALACAO.RelacaoOrdemInstalacoesReagendadas(parametro:string);
var
   Query:TIBQuery;
   ColocadorCorrente:string;
   ClienteCorrente:string;
   PProjetoCorrente:string;
begin

   Query:=TIBQuery.Create(nil);
   Query.Database:=FDataModulo.IBDatabase;
   try
        with FfiltroImp do
        begin
             DesativaGrupos;


             Grupo01.Enabled:=True;
             LbGrupo01.Caption:='Colocador';
             edtgrupo01.Color:=$005CADFE;
             edtgrupo01.OnKeyDown:=EdtColocadorKeyDown;

             Grupo02.Enabled:=True;
             LbGrupo02.Caption:='Cliente';
             edtgrupo02.Color:=$005CADFE;
             edtgrupo02.OnKeyDown:=EdtClienteKeyDown;

             Grupo03.Enabled:=True;
             lbgrupo03.Caption:='Pedido/Projeto';
             edtgrupo03.Color:=$005CADFE;
             edtgrupo03.OnKeyDown:=EdtPedidoProjetoAgendadoKeyDown;

            { Grupo04.Enabled:=True;
             LbGrupo04.Caption:='Data Inicial';
             edtgrupo04.EditMask:='99/99/9999';


             Grupo05.Enabled:=True;
             LbGrupo05.Caption :='Data Final';
             edtgrupo05.EditMask:='99/99/9999';    }


             ShowModal;

             if(Tag=0)
             then Exit;
        end;

        with Query do
        begin
              Close;
              SQL.Clear;
              SQL.Add('select tabprojeto.codigo as projeto,tabprojeto.descricao,tabpedido_proj.codigo,tabcliente.nome as cliente,tabreagendamentoinstalacao.*,tabfuncionarios.nome as nomecolocador,tabcliente.codigo as codigocliente');
              SQL.Add('from tabreagendamentoinstalacao');
              SQL.Add('join tabpedido_proj on tabpedido_proj.codigo=tabreagendamentoinstalacao.pedidoprojeto');
              SQL.Add('join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto');
              SQL.Add('join tabpedido on tabpedido.codigo=tabpedido_proj.pedido');
              SQL.Add('join tabcliente on tabcliente.codigo= tabpedido.cliente');
              SQL.Add('join tabcolocador on tabcolocador.codigo=tabreagendamentoinstalacao.colocador');
              SQL.Add('join tabfuncionarios on tabfuncionarios.codigo=tabcolocador.funcionario');
              SQL.Add('where tabreagendamentoinstalacao.codigo<>-1');

              if(FfiltroImp.edtgrupo01.text<>'')
              then SQL.Add('and colocador='+#39+FfiltroImp.edtgrupo01.text+#39);
              if(FfiltroImp.edtgrupo02.text<>'')
              then SQL.Add('and cliente='+#39+FfiltroImp.edtgrupo02.text+#39);
              if(FfiltroImp.edtgrupo03.text<>'')
              then SQL.Add('and pedidoprojeto='+#39+FfiltroImp.edtgrupo03.text+#39);

              SQL.Add('order by colocador,codigocliente,pedidoprojeto,tabreagendamentoinstalacao.codigo');

              Open;

              if(recordcount=0)
              then Exit;

              FreltxtRDPRINT.ConfiguraImpressao;
              FreltxtRDPRINT.LinhaLocal:=3;
              FreltxtRDPRINT.RDprint.Abrir;
              if (FreltxtRDPRINT.RDprint.Setup=False) then
              begin
                    FreltxtRDPRINT.rdprint.Fechar;
                    exit;
              end;
              FMostraBarraProgresso.ConfiguracoesIniciais(6,0);
              FMostraBarraProgresso.lbmensagem.caption:='Gerando Relat�rios';
              FMostraBarraProgresso.Show;
              FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,30,'Rela��o de Instala��o Reagendadas',[negrito]);
              FreltxtRDPRINT.incrementalinha(2);
              FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,1,'Colocador: ',[negrito]);
              FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,FieldByName('colocador').AsString+' - '+FieldByName('nomecolocador').AsString,[negrito]);
              FreltxtRDPRINT.incrementalinha(2);
              ColocadorCorrente:=fieldbyname('colocador').AsString;

              FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,'Projeto: ',[negrito]);
              FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,21,FieldByName('projeto').AsString+' - '+FieldByName('descricao').AsString,[negrito]);
              FreltxtRDPRINT.incrementalinha(1);
              PProjetoCorrente:=fieldbyname('pedidoprojeto').AsString;

              FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,'Cliente: ',[negrito]);
              FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,21,FieldByName('codigocliente').AsString+' - '+FieldByName('cliente').AsString,[negrito]);
              FreltxtRDPRINT.incrementalinha(2);
              ClienteCorrente:=fieldbyname('codigocliente').AsString;

              FreltxtRDPRINT.RDprint.ImpF(FreltxtRDPRINT.linhalocal,1,CompletaPalavra('Data Ant',12,' ')+' '+
                                     CompletaPalavra('Nova Data',12,' ')+' '+
                                     CompletaPalavra('Horario Ant.',12,' ')+' '+
                                     CompletaPalavra('Novo Horario',12,' ')+' '+
                                     CompletaPalavra('Motivo Reagendamento',50,' '),[negrito]);
              FreltxtRDPRINT.incrementalinha(1);

              while not Eof do
              begin
                    FMostraBarraProgresso.IncrementaBarra1(1);
                    FMostraBarraProgresso.Show;

                    if(ColocadorCorrente<>FieldByName('colocador').AsString) then
                    begin
                          FreltxtRDPRINT.incrementalinha(2);
                          FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,1,'Colocador: ',[negrito]);
                          FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,FieldByName('colocador').AsString+' - '+FieldByName('nomecolocador').AsString,[negrito]);
                          FreltxtRDPRINT.incrementalinha(2);
                          ColocadorCorrente:=fieldbyname('colocador').AsString;

                          FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,'Projeto: ',[negrito]);
                          FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,21,FieldByName('projeto').AsString+' - '+FieldByName('descricao').AsString,[negrito]);
                          FreltxtRDPRINT.incrementalinha(1);
                          PProjetoCorrente:=fieldbyname('pedidoprojeto').AsString;

                          FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,'Cliente: ',[negrito]);
                          FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,21,FieldByName('codigocliente').AsString+' - '+FieldByName('cliente').AsString,[negrito]);
                          FreltxtRDPRINT.incrementalinha(2);
                          ClienteCorrente:=fieldbyname('codigocliente').AsString;

                          FreltxtRDPRINT.RDprint.ImpF(FreltxtRDPRINT.linhalocal,1,CompletaPalavra('Data Ant',12,' ')+' '+
                                                 CompletaPalavra('Nova Data',12,' ')+' '+
                                                 CompletaPalavra('Horario Ant.',12,' ')+' '+
                                                 CompletaPalavra('Novo Horario',12,' ')+' '+
                                                 CompletaPalavra('Motivo Reagendamento',50,' '),[negrito]);
                          FreltxtRDPRINT.incrementalinha(1);

                    end
                    else
                    begin
                          if(PProjetoCorrente<>FieldByName('pedidoprojeto').AsString) or (ClienteCorrente<>FieldByName('codigocliente').AsString) then
                          begin
                                FreltxtRDPRINT.incrementalinha(2);
                                FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,'Projeto: ',[negrito]);
                                FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,21,FieldByName('projeto').AsString+' - '+FieldByName('descricao').AsString,[negrito]);
                                FreltxtRDPRINT.incrementalinha(1);
                                PProjetoCorrente:=fieldbyname('pedidoprojeto').AsString;

                                FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,'Cliente: ',[negrito]);
                                FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,21,FieldByName('codigocliente').AsString+' - '+FieldByName('cliente').AsString,[negrito]);
                                FreltxtRDPRINT.incrementalinha(2);
                                ClienteCorrente:=fieldbyname('codigocliente').AsString;

                                FreltxtRDPRINT.RDprint.ImpF(FreltxtRDPRINT.linhalocal,1,CompletaPalavra('Data Ant',12,' ')+' '+
                                                       CompletaPalavra('Nova Data',12,' ')+' '+
                                                       CompletaPalavra('Horario Ant.',12,' ')+' '+
                                                       CompletaPalavra('Novo Horario',12,' ')+' '+
                                                       CompletaPalavra('Motivo Reagendamento',50,' '),[negrito]);
                                FreltxtRDPRINT.incrementalinha(1);
                          end;

                    end;

                    FreltxtRDPRINT.RDprint.ImpF(FreltxtRDPRINT.linhalocal,1,CompletaPalavra(fieldbyname('dataanterior').AsString,12,' ')+' '+
                                                CompletaPalavra(fieldbyname('novadata').AsString,12,' ')+' '+
                                                CompletaPalavra(fieldbyname('horarioanterior').AsString,12,' ')+' '+
                                                CompletaPalavra(fieldbyname('novohorario').AsString,12,' ')+' '+
                                                CompletaPalavra(fieldbyname('motivoreagendamento').AsString,50,' '),[negrito]);
                    FreltxtRDPRINT.incrementalinha(1);                                                


                    Next;
              end;

              FMostraBarraProgresso.close;
              FreltxtRDPRINT.RDprint.Fechar;

        end;

   finally
        FreeAndNil(Query);
   end;


end;

procedure TObjORDEMINSTALACAO.RelacaoOrdensInstalacao(parametro:string);
var
  Query:TIBQuery;
  Colocador,DataInicial,DataFinal:string;
  DataCorrente,ColocadorCorrente:string;
begin
   Query:=TIBQuery.Create(nil);
   Query.Database:=FDataModulo.IBDatabase;
   try
        with FfiltroImp do
        begin
              DesativaGrupos;
              Grupo01.Enabled:=True;
              LbGrupo01.Caption:='Colocador';
              edtgrupo01.Color:=$005CADFE;
              edtgrupo01.OnKeyDown:=EdtColocadorKeyDown;

              Grupo02.Enabled:=True;
              LbGrupo02.Caption:='Data Inicial';
              edtgrupo02.EditMask:='99/99/9999';

              Grupo03.Enabled:=True;
              lbgrupo03.Caption:='Data Final';
              edtgrupo03.EditMask:='99/99/9999';

              ShowModal;

              if(Tag=0)
              then Exit;

              Colocador:='';
              DataInicial:='';
              DataFinal:='';
              if(edtgrupo01.Text<>'')
              then ColocadorCorrente:=edtgrupo01.Text;

              if(edtgrupo02.Text<>'  /  /    ')
              then DataInicial:=edtgrupo02.Text
              else DataInicial:='99/99/9999';

              if(edtgrupo03.Text<>'  /  /    ')
              then DataFinal:=edtgrupo03.Text
              else DataFinal:='99/99/9999';

        end;


        with Query do
        begin
              SQL.Clear;
              SQL.Add('select tabromaneiosordeminstalacao.codigo,tabprojeto.descricao,tabromaneiosordeminstalacao.concluido,');
              SQL.Add('tabromaneiosordeminstalacao.horario,tabromaneiosordeminstalacao.tempomedioinstalacao,tabordeminstalacao.colocador');
              SQL.Add(',tabromaneiosordeminstalacao.tempofinal as HorarioTermino,tabromaneiosordeminstalacao.pedidoprojeto,tabromaneiosordeminstalacao.ordeminstalacao');
              SQL.Add(',tabromaneiosordeminstalacao.romaneio,tabordeminstalacao.data,tabromaneiosordeminstalacao.observacao,tabfuncionarios.nome from tabromaneiosordeminstalacao');
              SQL.Add('join tabpedido_proj on tabpedido_proj.codigo=tabromaneiosordeminstalacao.pedidoprojeto');
              SQL.Add('join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto') ;
              SQL.Add('join tabordeminstalacao on tabordeminstalacao.codigo=tabromaneiosordeminstalacao.ordeminstalacao');
              SQL.Add('join tabcolocador on tabcolocador.codigo=tabordeminstalacao.colocador');
              SQL.Add('join tabfuncionarios on tabfuncionarios.codigo=tabcolocador.funcionario');
              SQL.Add('where tabordeminstalacao.data>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(DataInicial))+#39);
              SQL.Add('and tabordeminstalacao.data<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(DataFinal))+#39);
              if(ColocadorCorrente<>'')
              then SQL.Add('and colocador='+ColocadorCorrente);

              SQL.Add('order by colocador,data,horario');

              Open;

              if(recordcount=0) then
              begin
                  MensagemAviso('N�o h� nenhuma instala��o agendada para esta data!!!');
                  Exit;
              end;

              FreltxtRDPRINT.ConfiguraImpressao;
              FreltxtRDPRINT.LinhaLocal:=3;
              FreltxtRDPRINT.RDprint.Abrir;
              if (FreltxtRDPRINT.RDprint.Setup=False) then
              begin
                    FreltxtRDPRINT.rdprint.Fechar;
                    exit;
              end;
              FMostraBarraProgresso.ConfiguracoesIniciais(6,0);
              FMostraBarraProgresso.lbmensagem.caption:='Gerando Relat�rios';
              FMostraBarraProgresso.Show;
              FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,30,'Rela��o de Instala��o por Colocador',[negrito]);
              FreltxtRDPRINT.incrementalinha(2);

              FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,1,FieldByName('nome').AsString,[negrito]);
              FreltxtRDPRINT.incrementalinha(2);
              ColocadorCorrente:=fieldbyname('colocador').AsString;

              FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,1,DiaSemana(StrToDate(FieldByName('data').Asstring))+','+FieldByName('data').Asstring,[negrito]);
              FreltxtRDPRINT.incrementalinha(1) ;
              DataCorrente:=fieldbyname('data').AsString;
              FreltxtRDPRINT.RDprint.ImpF(FreltxtRDPRINT.linhalocal,1,CompletaPalavra('Projeto',35,' ')+' '+
                                     CompletaPalavra('Horario Instala��o',20,' ')+' '+
                                     CompletaPalavra('Dura��o',8,' ')+' '+
                                     CompletaPalavra('Horario T�rmino',20,' ')+' '+
                                     CompletaPalavra('Romaneio',10,' '),[negrito]);
              FreltxtRDPRINT.incrementalinha(1);
              DataCorrente:=fieldbyname('data').AsString;

              while not eof do
              begin
                    if(ColocadorCorrente<>FieldByName('colocador').AsString)then
                    begin
                            FreltxtRDPRINT.incrementalinha(2);
                            FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,1,FieldByName('nome').AsString,[negrito]);
                            FreltxtRDPRINT.incrementalinha(2);
                            ColocadorCorrente:=fieldbyname('colocador').AsString;

                    end;
                    if(DataCorrente<>FieldByName('data').AsString)then
                    begin
                            FreltxtRDPRINT.incrementalinha(1);
                            FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,1,DiaSemana(StrToDate(FieldByName('data').Asstring))+','+FieldByName('data').Asstring,[negrito]);
                            FreltxtRDPRINT.incrementalinha(1);
                            DataCorrente:=fieldbyname('data').AsString;
                            FreltxtRDPRINT.RDprint.ImpF(FreltxtRDPRINT.linhalocal,1,CompletaPalavra('Projeto',35,' ')+' '+
                                          CompletaPalavra('Horario Instala��o',20,' ')+' '+
                                          CompletaPalavra('Dura��o',8,' ')+' '+
                                          CompletaPalavra('Horario T�rmino',20,' ')+' '+
                                          CompletaPalavra('Romaneio',10,' '),[negrito]);
                            FreltxtRDPRINT.incrementalinha(1);
                    end;

                    FreltxtRDPRINT.RDprint.Imp(FreltxtRDPRINT.linhalocal,1,CompletaPalavra(FieldByName('descricao').Asstring,35,' ')+' '+
                                   CompletaPalavra(FieldByName('horario').Asstring,20,' ')+' '+
                                   CompletaPalavra(FieldByName('tempomedioinstalacao').Asstring,8,' ')+' '+
                                   CompletaPalavra(FieldByName('horariotermino').Asstring,20,' ')+' '+
                                   CompletaPalavra(FieldByName('Romaneio').Asstring,10,' '));
                    FreltxtRDPRINT.incrementalinha(1);

                    FMostraBarraProgresso.IncrementaBarra1(1);
                    FMostraBarraProgresso.Show;
                    Next;
              end;
              FMostraBarraProgresso.close;
              FreltxtRDPRINT.RDprint.Fechar;
        end;

   finally
        FreeAndNil(Query);
   end;

end;

end.



