unit UAgendaInstalacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, Grids,UessencialGlobal, Buttons,
  Mask,UobjORDEMINSTALACAO,IBQuery,UDataModulo,UFiltraImp, Menus,UobjROMANEIOSORDEMINSTALACAO,Upesquisa,DB,UAjuda,UpesquisaMenu,
  OleCtrls, SHDocVw, ImgList;

type
  TFAgendaInstalacao = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    Img1: TImage;
    pnl3: TPanel;
    pnl4: TPanel;
    lbnomeformulario: TLabel;
    lb1: TLabel;
    lbNomeInstalador: TLabel;
    cal1: TMonthCalendar;
    pnl5: TPanel;
    lb2: TLabel;
    pgc1: TPageControl;
    tsDia: TTabSheet;
    tsSemana: TTabSheet;
    tsMes: TTabSheet;
    STRG1: TStringGrid;
    STRG2: TStringGrid;
    STRG3: TStringGrid;
    pnl6: TPanel;
    lbdata: TLabel;
    lbDia: TLabel;
    pnl7: TPanel;
    pnl8: TPanel;
    lbdata3: TLabel;
    lbdia3: TLabel;
    edtdatapesquisa: TMaskEdit;
    bt2: TSpeedButton;
    bt1: TSpeedButton;
    bt3: TSpeedButton;
    edtcolocador: TMaskEdit;
    lb3: TLabel;
    lb4: TLabel;
    bvl1: TBevel;
    lb5: TLabel;
    lbdiasemanarodape: TLabel;
    lbdatarodape: TLabel;
    lb6: TLabel;
    PopUpCadastros: TPopupMenu;
    MenuItem4: TMenuItem;
    Agenda: TMenuItem;
    lbEnter: TLabel;
    Reagendar1: TMenuItem;
    lbconcluir: TLabel;
    lb8: TLabel;
    lbobservacao: TLabel;
    lbdesagendar: TLabel;
    PostarObservao1: TMenuItem;
    lb7: TLabel;
    lbDataSemanaComeco: TLabel;
    lb10: TLabel;
    lbDataSemanaFim: TLabel;
    tsMapa: TTabSheet;
    wbGoogleMaps: TWebBrowser;
    tsPrevisaoTempo: TTabSheet;
    wbPrevisaoTempo: TWebBrowser;
    il2: TImageList;
    procedure FormShow(Sender: TObject);
    procedure lbNomeInstaladorMouseLeave(Sender: TObject);
    procedure lbNomeInstaladorMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtcolocadorExit(Sender: TObject);
    procedure edtcolocadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcolocadorDblClick(Sender: TObject);
    procedure edtcolocadorKeyPress(Sender: TObject; var Key: Char);
    procedure cal1Click(Sender: TObject);
    procedure bt2Click(Sender: TObject);
    procedure STRG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure MenuItem4Click(Sender: TObject);
    procedure Reagendar1Click(Sender: TObject);
    procedure lbEnterClick(Sender: TObject);
    procedure lbdesagendarClick(Sender: TObject);
    procedure PostarObservao1Click(Sender: TObject);
    procedure lbconcluirClick(Sender: TObject);
    procedure lbobservacaoClick(Sender: TObject);
    procedure lb7Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure pgc1Change(Sender: TObject);
    procedure bt3Click(Sender: TObject);
    procedure bt1Click(Sender: TObject);
    procedure STRG2DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure STRG1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure STRG3DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure AgendaClick(Sender: TObject);
    procedure lbNomeInstaladorClick(Sender: TObject);
    procedure edtdatapesquisaExit(Sender: TObject);
  private
      ObjOrdemInstalacao:TObjORDEMINSTALACAO;
      ObjRomaneiosOrdemInstalacao:TObjROMANEIOSORDEMINSTALACAO;
      OrdemInstalacaoGlobal:string;
      DataOrdemInstalacaoGlobal:string;
      CodigoColocadorGlobal:String;

      RomaneioGlobal:string;

      procedure ConfiguraStrinGrid();
      procedure PesquisaPorDia(data:TDate;colocador:string);
      procedure PesquisaPorMes;
      procedure PesquisaPorSemana;
      procedure Agendar;
      procedure edtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
      procedure edtPedidoProjetoExit(Sender: TObject);
      procedure edtTempoMedioInstalacaoExit(Sender: TObject);
      procedure AjustaHoraFimInstalacao;
      procedure edtPedidoProjetoKeyPress(Sender: TObject;var Key: Char);
      procedure edtdataKeyPress(Sender: TObject;var Key: Char);
      function ControlesParaObjeto:Boolean;
      function TabelaParaControles: Boolean;
      function ObjetoParaControles:Boolean;
      procedure RetornaIntervaloMesAtual(DataAtual:string;var DataInicioMes:string;var DataFimMes:string);
      procedure RetornaIntervaloDataSemana(DataAtual:TDate;var datainicio:TDate;var datafim:TDate);
      procedure ConcluirInstalacao;
      procedure DesagendarInstalacao;
      procedure MarcarPosicaoGMAPS;
      procedure MontarMapa(AOrigem: String; ADestino:TStringList);



  public
      procedure PassaDados(OrdemInstalacao,DataOrdemInstalacao,CodigoColocador:string);
      Procedure AgendaDiretoRomaneio();
  end;

var
  FAgendaInstalacao: TFAgendaInstalacao;

implementation

uses UescolheImagemBotao, DateUtils, Uprincipal, Ufuncionarios,
  UobjREAGENDAMENTOINSTALACAO, UobjCANCELAMENTOINSTALACAO;

{$R *.dfm}

procedure TFAgendaInstalacao.FormShow(Sender: TObject);
begin
     FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');
     pgc1.TabIndex:=0;
     {Preenchendo com a data da Ordem de Instala��o, pois mostra a agenda daquela data, a data que
     veio da ordem de instala��o

     lbdata.Caption:=FormatDateTime('dd "de" mmmm "de" yyyy', StrToDate(DataOrdemInstalacaoGlobal));
     lbDia.Caption:= DiaSemana(StrToDate(DataOrdemInstalacaoGlobal)); }

     lbdata.Caption:=FormatDateTime('dd "de" mmmm "de" yyyy', Now);
     lbDia.Caption:= DiaSemana(Now);

     lbdata3.Caption:=FormatDateTime('dd "de" mmmm "de" yyyy', Now);
     lbDia3.Caption:= DiaSemana(Now);

     lbdatarodape.Caption:=FormatDateTime('dd "de" mmmm "de" yyyy', Now);
     lbdiasemanarodape.Caption:= DiaSemana(Now);

     ObjOrdemInstalacao:=TObjORDEMINSTALACAO.Create;
     ObjROMANEIOSORDEMINSTALACAO:=TObjROMANEIOSORDEMINSTALACAO.Create;

     edtdatapesquisa.Text:=DateToStr(Now);
     cal1.Date:=Now;

     ConfiguraStrinGrid;

end;

procedure TFAgendaInstalacao.PassaDados(OrdemInstalacao,DataOrdemInstalacao,CodigoColocador:string);
begin
    OrdemInstalacaoGlobal:=OrdemInstalacao;
    DataOrdemInstalacaoGlobal :=DataOrdemInstalacao;
    CodigoColocadorGlobal:=CodigoColocador;
end;

procedure TFAgendaInstalacao.PesquisaPorMes;
var
   Query:TIBQuery;
   DataInicio,DataFim:string;
begin
    if(CodigoColocadorGlobal='')
    then Exit;

    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;

    ObjORDEMINSTALACAO.COLOCADOR.LocalizaCodigo(CodigoColocadorGlobal);
    ObjORDEMINSTALACAO.COLOCADOR.TabelaparaObjeto;
    lbNomeInstalador.Caption:=ObjORDEMINSTALACAO.COLOCADOR.Funcionario.Get_Nome;

    try

       with Query do
       begin
          with STRG3 do
           begin
                  RowCount:= 2;
                  ColCount:= 10;
                  FixedRows:=1;
                  FixedCols:=0;

                  ColWidths[0] := 200;
                  ColWidths[1] := 200;
                  ColWidths[2] := 350;
                  ColWidths[3] := 200;
                  ColWidths[4] := 100;
                  ColWidths[5] := 50;
                  ColWidths[6] := 80;
                  ColWidths[7] := 600;
                  ColWidths[8] := 100;
                  ColWidths[9] := 100;

                  Cells[0,0] := 'DIA';
                  Cells[1,0] := 'HORARIO DE INSTALA��O';
                  Cells[2,0] := 'NOME PROJETO';
                  Cells[3,0] := 'HORARIO DE TERMINO';
                  Cells[4,0] := 'DURA��O';
                  Cells[5,0] := 'CODIGO';
                  Cells[6,0] := 'CONCLU�DO';
                  Cells[7,0] := 'OBSERVA��O' ;
                  Cells[8,0] := 'ROMANEIO';
                  Cells[9,0] := 'PEDIDO PROJETO';


                  //Limpa Primeira Linha
                  Cells[0,1] := '';
                  Cells[1,1] := '';
                  Cells[2,1] := '';
                  Cells[3,1] := '';
                  Cells[4,1] := '';
                  Cells[5,1] := '';
                  Cells[6,1] := '';
                  Cells[7,1] := '';
                  Cells[8,1] := '';
                  Cells[9,1] := '';


                  RetornaIntervaloMesAtual(DateToStr(cal1.Date),DataInicio,DataFim);
                  lbdata3.Caption:=FormatDateTime('dd "de" mmmm "de" yyyy',StrToDate(DataFim));

                  lbDia3.Caption:=FormatDateTime('dd "de" mmmm "de" yyyy',StrToDate(DataInicio));

                  Close;
                  SQL.Clear;
                  SQL.Add('select tabromaneiosordeminstalacao.codigo,tabprojeto.descricao,tabromaneiosordeminstalacao.concluido,');
                  SQL.Add('tabromaneiosordeminstalacao.horario,tabromaneiosordeminstalacao.tempomedioinstalacao');
                  SQL.Add(',tabromaneiosordeminstalacao.tempofinal as HorarioTermino,tabromaneiosordeminstalacao.pedidoprojeto,tabromaneiosordeminstalacao.ordeminstalacao');
                  SQL.Add(',tabromaneiosordeminstalacao.romaneio,tabordeminstalacao.data,tabromaneiosordeminstalacao.observacao from tabromaneiosordeminstalacao');
                  SQL.Add('join tabpedido_proj on tabpedido_proj.codigo=tabromaneiosordeminstalacao.pedidoprojeto');
                  SQL.Add('join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto') ;
                  SQL.Add('join tabordeminstalacao on tabordeminstalacao.codigo=tabromaneiosordeminstalacao.ordeminstalacao');
                  SQL.Add('where tabordeminstalacao.data>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(DataInicio))+#39);
                  SQL.Add('and tabordeminstalacao.data<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(DataFim))+#39);
                  SQL.Add('and  tabordeminstalacao.colocador='+CodigoColocadorGlobal);
                  SQL.Add('order by data,horario');

                  Open;
                  
                  if(recordcount=0)
                  then Exit;

                  while not eof do
                  begin
                      Cells[0,RowCount-1] := '';
                      Cells[1,RowCount-1] := '';
                      Cells[2,RowCount-1] := '';
                      Cells[3,RowCount-1] := '';
                      Cells[4,RowCount-1] := '';
                      Cells[5,RowCount-1] := '';
                      Cells[6,RowCount-1] := '' ;
                      Cells[7,RowCount-1] := '';
                      Cells[8,RowCount-1] := '';

                      Cells[0,RowCount-1] := DiaSemana(StrToDate(fieldbyname('data').AsString))+' - '+fieldbyname('data').AsString;
                      Cells[1,RowCount-1] := fieldbyname('horario').AsString;
                      Cells[2,RowCount-1] := fieldbyname('descricao').AsString;
                      Cells[3,RowCount-1] := fieldbyname('HorarioTermino').AsString;
                      Cells[4,RowCount-1] := fieldbyname('tempomedioinstalacao').AsString;
                      Cells[5,RowCount-1] := fieldbyname('codigo').AsString;
                      Cells[6,RowCount-1] := fieldbyname('concluido').AsString;
                      Cells[7,RowCount-1] := fieldbyname('observacao').AsString;
                      Cells[8,RowCount-1] := fieldbyname('romaneio').AsString;
                      Cells[9,RowCount-1] := fieldbyname('pedidoprojeto').AsString;

                      RowCount:=RowCount+1;
                      Next;
                  end;
                  RowCount:=RowCount-1;
           end;
       end;
    finally
       FreeAndNil(Query);
    end;

end;

procedure TFAgendaInstalacao.ConfiguraStrinGrid;
var
   Query:TIBQuery;
   DataInicio,DataFim:string;
begin
    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;

    try

       with Query do
       begin
           //Por semana
           with STRG1 do
           begin
                  RowCount:= 2;
                  ColCount:= 9;
                  FixedRows:=1;
                  FixedCols:=0;

                  ColWidths[0] := 200;
                  ColWidths[1] := 350;
                  ColWidths[2] := 200;
                  ColWidths[3] := 100;
                  ColWidths[4] := 80;
                  ColWidths[5] := 50;
                  ColWidths[6] := 600;
                  ColWidths[7] := 100;
                  ColWidths[8] := 100;
                  Cells[0,0] := 'HORARIO DE INSTALA��O';
                  Cells[1,0] := 'NOME PROJETO';
                  Cells[2,0] := 'HOR�RIO DE T�RMINO';
                  Cells[3,0] := 'DURA��O';
                  Cells[4,0] := 'CONCLU�DO';
                  Cells[5,0] := 'C�DIGO';
                  Cells[6,0] := 'OBSERVA��O' ;
                  Cells[7,0] := 'ROMANEIO';
                  Cells[8,0] := 'PEDIDO PROJETO';

                  //Limpa Primeira Linha
                  Cells[0,1] := '';
                  Cells[1,1] := '';
                  Cells[2,1] := '';
                  Cells[3,1] := '';
                  Cells[4,1] := '';
                  Cells[5,1] := '';
                  Cells[6,1] := '' ;
                  Cells[7,1] := '';
                  Cells[8,1] := '';
           end;
           with STRG2 do
           begin
                  RowCount:= 2;
                  ColCount:= 6;
                  FixedRows:=1;
                  FixedCols:=0;

                  ColWidths[0] := 200;
                  ColWidths[1] := 200;
                  ColWidths[2] := 350;
                  ColWidths[3] := 200;
                  ColWidths[4] := 100;
                  ColWidths[5] := 50;
                  Cells[0,0] := 'DIA';
                  Cells[1,0] := 'HORARIO DE INSTALA��O';
                  Cells[2,0] := 'NOME PROJETO';
                  Cells[3,0] := 'HORARIO DE TERMINO';
                  Cells[4,0] := 'DURA��O';
                  Cells[5,0] := 'CODIGO';


           end;

           //Por mes
           with STRG3 do
           begin
                  RowCount:= 2;
                  ColCount:= 10;
                  FixedRows:=1;
                  FixedCols:=0;

                  ColWidths[0] := 200;
                  ColWidths[1] := 200;
                  ColWidths[2] := 350;
                  ColWidths[3] := 200;
                  ColWidths[4] := 100;
                  ColWidths[5] := 50;
                  ColWidths[6] := 80;
                  ColWidths[7] := 600;
                  ColWidths[8] := 100;
                  ColWidths[9] := 100;

                  Cells[0,0] := 'DIA';
                  Cells[1,0] := 'HORARIO DE INSTALA��O';
                  Cells[2,0] := 'NOME PROJETO';
                  Cells[3,0] := 'HORARIO DE TERMINO';
                  Cells[4,0] := 'DURA��O';
                  Cells[5,0] := 'CODIGO';
                  Cells[6,0] := 'CONCLU�DO';
                  Cells[7,0] := 'OBSERVA��O' ;
                  Cells[8,0] := 'ROMANEIO';
                  Cells[9,0] := 'PEDIDO PROJETO';


                  //Limpa Primeira Linha
                  Cells[0,1] := '';
                  Cells[1,1] := '';
                  Cells[2,1] := '';
                  Cells[3,1] := '';
                  Cells[4,1] := '';
                  Cells[5,1] := '';
                  Cells[6,1] := '';
                  Cells[7,1] := '';
                  Cells[8,1] := '';
                  Cells[9,1] := '';

           end;
       end;
    finally
       FreeAndNil(Query);
    end;

end;


procedure TFAgendaInstalacao.lbNomeInstaladorMouseLeave(Sender: TObject);
begin
     TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFAgendaInstalacao.lbNomeInstaladorMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
     TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFAgendaInstalacao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
       ObjOrdemInstalacao.Free;
       ObjRomaneiosOrdemInstalacao.free;
end;

procedure TFAgendaInstalacao.edtcolocadorExit(Sender: TObject);
begin
       if(edtColocador.Text='')
       then Exit;

       if(ObjORDEMINSTALACAO.COLOCADOR.LocalizaCodigo(edtColocador.Text)=False) then
       begin
           MensagemAviso('Colocador n�o encontrado');
           CodigoColocadorGlobal:='';
           lbNomeInstalador.Caption:='';
           Exit;
       end;
       ObjORDEMINSTALACAO.COLOCADOR.TabelaparaObjeto;
       lbNomeInstalador.Caption:=ObjORDEMINSTALACAO.COLOCADOR.Funcionario.Get_Nome;
       CodigoColocadorGlobal:=edtcolocador.Text;

      { bt2Click(Sender); }
end;

procedure TFAgendaInstalacao.edtcolocadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjORDEMINSTALACAO.EdtColocadorKeyDown(sender,Key,Shift,lbNomeInstalador);
end;

procedure TFAgendaInstalacao.edtcolocadorDblClick(Sender: TObject);
var
  key:Word;
  Shift:TShiftState;
begin
    key:=VK_F9;
    ObjORDEMINSTALACAO.EdtColocadorKeyDown(sender,Key,Shift,lbNomeInstalador);
end;

procedure TFAgendaInstalacao.edtcolocadorKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8),',']) then
       begin
                Key:= #0;
       end;
end;

procedure TFAgendaInstalacao.PesquisaPorDia(data:TDate;colocador:string);
var
   Query:TIBQuery;
begin

    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;

    try
         with Query do
         begin
             with STRG1 do
             begin
                  RowCount:= 2;
                  ColCount:= 9;
                  FixedRows:=1;
                  FixedCols:=0;

                  ColWidths[0] := 200;
                  ColWidths[1] := 350;
                  ColWidths[2] := 200;
                  ColWidths[3] := 100;
                  ColWidths[4] := 80;
                  ColWidths[5] := 50;
                  ColWidths[6] := 600;
                  ColWidths[7] := 100;
                  ColWidths[8] := 100;
                  Cells[0,0] := 'HORARIO DE INSTALA��O';
                  Cells[1,0] := 'NOME PROJETO';
                  Cells[2,0] := 'HOR�RIO DE T�RMINO';
                  Cells[3,0] := 'DURA��O';
                  Cells[4,0] := 'CONCLU�DO';
                  Cells[5,0] := 'C�DIGO';
                  Cells[6,0] := 'OBSERVA��O' ;
                  Cells[7,0] := 'ROMANEIO';
                  Cells[8,0] := 'PEDIDO PROJETO';

                  //Limpa Primeira Linha
                  Cells[0,1] := '';
                  Cells[1,1] := '';
                  Cells[2,1] := '';
                  Cells[3,1] := '';
                  Cells[4,1] := '';
                  Cells[5,1] := '';
                  Cells[6,1] := '' ;
                  Cells[7,1] := '';
                  Cells[8,1] := '';

                  Close;
                  SQL.Clear;
                  SQL.Add('select tabromaneiosordeminstalacao.codigo,tabprojeto.descricao,tabromaneiosordeminstalacao.concluido,');
                  SQL.Add('tabromaneiosordeminstalacao.horario,tabromaneiosordeminstalacao.tempomedioinstalacao');
                  SQL.Add(',tabromaneiosordeminstalacao.tempofinal as HorarioTermino,tabromaneiosordeminstalacao.pedidoprojeto,tabromaneiosordeminstalacao.ordeminstalacao');
                  SQL.Add(',tabromaneiosordeminstalacao.romaneio,tabromaneiosordeminstalacao.observacao from tabromaneiosordeminstalacao');
                  SQL.Add('join tabpedido_proj on tabpedido_proj.codigo=tabromaneiosordeminstalacao.pedidoprojeto');
                  SQL.Add('join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto') ;
                  SQL.Add('join tabordeminstalacao on tabordeminstalacao.codigo=tabromaneiosordeminstalacao.ordeminstalacao');
                  SQL.Add('where tabordeminstalacao.data='+#39+FormatDateTime('mm/dd/yyyy',data)+#39);
                  SQL.Add('and  tabordeminstalacao.colocador='+colocador);
                  SQL.Add('order by horario');
                  Open;

                  OrdemInstalacaoGlobal:=fieldbyname('ordeminstalacao').asstring;

                  if(recordcount=0)
                  then Exit;

                  while not eof do
                  begin
                        //Limpando Celulas
                        Cells[0,RowCount-1] := '';
                        Cells[1,RowCount-1] := '';
                        Cells[2,RowCount-1] := '';
                        Cells[3,RowCount-1] := '';
                        Cells[4,RowCount-1] := '';
                        Cells[5,RowCount-1] := '';
                        Cells[6,RowCount-1] := '' ;
                        Cells[7,RowCount-1] := '';
                        Cells[8,RowCount-1] := '';

                        Cells[0,RowCount-1] := fieldbyname('horario').AsString;
                        Cells[1,RowCount-1] := fieldbyname('descricao').AsString;
                        Cells[2,RowCount-1] := fieldbyname('HorarioTermino').AsString;
                        Cells[3,RowCount-1] := fieldbyname('tempomedioinstalacao').AsString;
                        Cells[4,RowCount-1] := fieldbyname('concluido').AsString;
                        Cells[5,RowCount-1] := fieldbyname('codigo').AsString;
                        Cells[6,RowCount-1] := fieldbyname('observacao').AsString;
                        Cells[7,RowCount-1] := fieldbyname('romaneio').AsString;
                        Cells[8,RowCount-1] := fieldbyname('pedidoprojeto').AsString;


                        RowCount:=RowCount+1;
                        Next;
                  end;
                  RowCount:=RowCount-1;
             end;
         end;
    finally
         FreeAndNil(Query);
    end;


end;

procedure TFAgendaInstalacao.cal1Click(Sender: TObject);
begin
    //O edit recebe a data do calendario, assim a pesquisa torna-se mais dinamica
    edtdatapesquisa.Text:=DateToStr(cal1.Date);
    //caso edit colocador esteja preenchido, preenche o codigo do colocador pra efetuar a pesquisa
    edtcolocadorExit(Sender);

    if(CodigoColocadorGlobal='')
    then Exit;
    
    PesquisaPorDia(cal1.Date,CodigoColocadorGlobal);
    PesquisaPorMes;

    DataOrdemInstalacaoGlobal:=DateToStr(cal1.Date);
    lbdata.Caption:=FormatDateTime('dd "de" mmmm "de" yyyy', StrToDate(DataOrdemInstalacaoGlobal));
    lbDia.Caption:= DiaSemana(StrToDate(DataOrdemInstalacaoGlobal));

   pgc1.TabIndex:=0;
end;

procedure TFAgendaInstalacao.bt2Click(Sender: TObject);
begin
    edtcolocadorExit(Sender);

    PesquisaPorDia(StrToDate(edtdatapesquisa.text),edtcolocador.Text);
    PesquisaPorMes;

    cal1.Date:=StrToDate(edtdatapesquisa.text);
    cal1.Refresh;
    DataOrdemInstalacaoGlobal:=DateToStr(cal1.Date);
    lbdata.Caption:=FormatDateTime('dd "de" mmmm "de" yyyy', StrToDate(DataOrdemInstalacaoGlobal));
    lbDia.Caption:= DiaSemana(StrToDate(DataOrdemInstalacaoGlobal));

   pgc1.TabIndex:=0;

end;

procedure TFAgendaInstalacao.STRG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
       if(key=13) then
       begin
           Agendar;
       end;
       if(key=46) then
       begin
           MenuItem4Click(Sender);
       end;
       if(key=VK_F5) then
       begin
       
          if(STRG1.Cells[4,STRG1.Row]='N') then
          begin

               ConcluirInstalacao;
          end
          else
          begin
               DesagendarInstalacao;
          end;



       end;
        if(key=VK_F6) then
       begin
          PostarObservao1Click(Sender);
       end;

       if(Key=VK_F7) then
       begin
           Reagendar1Click(Sender);
       end;

end;

procedure TFAgendaInstalacao.edtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Sql:string;
begin

     If (key <>vk_f9)
     Then exit;

     //Explica��o para o SQL abaixo
     {
      ****
          Seleciono todos os pedidoprojetos que est�o em romaneios, pore�m eu quero somente os que n�o est�o
          agendados. No SQL eu trago o tabromaneiosordeminstalacao.pedidoprojeto, pra mim pegar os pedido projetos
          q ainda n�o est�o agendados basta eu verificar se este campo ta vindo nulo, se esta nulo, � pq o pedido projeto
          ainda n�o foi inserido na tabromaneiosordeminstalacao.

      ****
     }

     sql:='select tabromaneio.codigo as romaneio,tabprojeto.descricao,tabcliente.codigo as codigocliente,tabcliente.nome';
     Sql:=Sql+ ',tabpedido.codigo as pedido,tabpedido_proj.codigo as codigopedidoprojeto,tabprojeto.codigo as codigoprojeto';
     Sql:=Sql+ ',tabromaneio.dataentrega,tabromaneio.dataemissao,tabpedido.data as venda,tabromaneiosordeminstalacao.pedidoprojeto';
     Sql:=Sql+ ' from tabromaneio';
     Sql:=Sql+ ' join tabpedidoprojetoromaneio on tabpedidoprojetoromaneio.romaneio=tabromaneio.codigo';
     Sql:=Sql+ ' join tabpedido_proj on tabpedido_proj.codigo=tabpedidoprojetoromaneio.pedidoprojeto';
     Sql:=Sql+ ' join tabpedido on tabpedido.codigo=tabpedido_proj.pedido';
     Sql:=Sql+ ' join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto';
     Sql:=Sql+ ' join tabcliente on tabcliente.codigo=tabpedido.cliente';
     Sql:=Sql+ ' left join tabromaneiosordeminstalacao on tabromaneiosordeminstalacao.pedidoprojeto=tabpedido_proj.codigo';
     Sql:=Sql+ ' where tabromaneio.concluido=''S'' ';
     Sql:=Sql+ ' and tabromaneiosordeminstalacao.pedidoprojeto is null';

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(sql,'',nil)=True) Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)Then
                        Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigopedidoprojeto').asstring;
                                 FfiltroImp.edtgrupo01.text:=FpesquisaLocal.QueryPesq.fieldbyname('romaneio').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFAgendaInstalacao.edtPedidoProjetoExit(Sender: TObject);
begin
      //Verifica se o pedido/projeto j� foi inserido para instala��o
      
      if(FfiltroImp.edtgrupo02.text='')
      then Exit;

      if(ObjROMANEIOSORDEMINSTALACAO.VerificaProjetoInserido(FfiltroImp.edtgrupo02.text)=True) then
      begin
          FfiltroImp.edtgrupo02.Text:='';
          FfiltroImp.edtgrupo02.SetFocus;
      end;


end;

procedure TFAgendaInstalacao.edtTempoMedioInstalacaoExit(Sender: TObject);
begin
     if(FfiltroImp.edtgrupo04.Text='  :  ')
     then Exit;
     AjustaHoraFimInstalacao;

end;

procedure TFAgendaInstalacao.AjustaHoraFimInstalacao;
var
   TempodeInstalacao:Currency;
   TempoHora:string;
   TempoMinuto:string;
   TempoHoraMarcada:string;
   TempoMinutoMarcada:string;
   SomaMinuto:Currency;
   FIni,FFim : TTime;

begin
     if(FfiltroImp.edtgrupo03.Text='  :  ')
     then Exit;

     Fini:=StrToTime(FfiltroImp.edtgrupo03.Text);
     FFim:=StrToTime(FfiltroImp.edtgrupo04.Text);

     FfiltroImp.edtgrupo05.EditMask:='!90:00:00;1;_'  ;
     FfiltroImp.edtgrupo05.Text:=TimeToStr(Fini+FFim);

end;

procedure TFAgendaInstalacao.AgendaDiretoRomaneio;
var
  Nomeprojeto:String;
  HorarioDisponivel:Boolean;
  Ano,Mes,Dia,Ano2,Mes2,Dia2:Word;
begin
     //montar agendamento direto pelo romaneio
end;

procedure TFAgendaInstalacao.Agendar;
var
  Nomeprojeto:String;
  HorarioDisponivel:Boolean;
  Ano,Mes,Dia,Ano2,Mes2,Dia2:Word;

begin
        if(CodigoColocadorGlobal='')
        then Exit;

        DecodeDate(Now,Ano,Mes,Dia);
        DecodeDate(cal1.Date,Ano2,Mes2,Dia2);

        if  (Dia2<Dia) then
        begin
              if((Mes2<=Mes) and (Ano2<=Ano)) then
              begin
                    MensagemAviso('N�o � poss�vel agendar em uma data menor que a Atual');
                    Exit;
              end;
        end
        else
        begin
              if((Mes2<Mes) or (Ano2<Ano)) then
              begin
                    MensagemAviso('N�o � poss�vel agendar em uma data menor que a Atual');
                    Exit;
              end;
        end;

        {Quando vou agendar para uma data verifico se pra aquela data j� exite uma Ordem de Instala��o gerada
        caso tenha, pego o codigo da mesma, sen�o crio uma nova ordem de instala��o pra aquela data... }
        if(Objordeminstalacao.LocalizPorData(DateToStr(cal1.Date),OrdemInstalacaoGlobal,CodigoColocadorGlobal)=True) then
        begin
             ObjORDEMINSTALACAO.Status:=dsInactive;
             ObjORDEMINSTALACAO.LocalizaCodigo(OrdemInstalacaoGlobal);
             ObjORDEMINSTALACAO.TabelaparaObjeto;
        end
        else
        begin
             ObjOrdemInstalacao.Status:=dsInsert;
             OrdemInstalacaoGlobal:='0';

             If ControlesParaObjeto=False Then
             Begin
                     Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
                     exit;
             End;

             If (ObjORDEMINSTALACAO.salvar(true)=False)
             Then exit;

             OrdemInstalacaoGlobal:=ObjORDEMINSTALACAO.Get_CODIGO;

             ObjORDEMINSTALACAO.LocalizaCodigo(OrdemInstalacaoGlobal) ;
             ObjORDEMINSTALACAO.TabelaparaObjeto;
             self.TabelaParaControles;

        end;

        with FfiltroImp do
        begin
              DesativaGrupos;

              pnl3.Caption:='Agendamento para o dia '+DateToStr(cal1.Date);

              Grupo02.Enabled:=True;
              LbGrupo02.Caption:='Pedido/Projeto';
              edtgrupo02.Color:=$005CADFE;
              edtgrupo02.OnKeyDown:=edtPedidoProjetoKeyDown;
              edtgrupo02.OnExit:=edtPedidoProjetoExit;
              edtgrupo02.OnKeyPress:=edtPedidoProjetoKeyPress;

              Grupo03.Enabled:=True;
              lbgrupo03.Caption:='Horario Inicio';
              edtgrupo03.EditMask:='99:99'  ;
              edtgrupo03.OnExit:=edtTempoMedioInstalacaoExit;

              Grupo04.Enabled:=True;
              lbgrupo04.Caption:='Dura��o';
              edtgrupo04.EditMask:='99:99'  ;
              edtgrupo04.OnExit:=edtTempoMedioInstalacaoExit;

              Grupo05.Enabled:=True;
              lbgrupo05.Caption:='Horario Fim';
              edtgrupo05.EditMask:='99:99'  ;

              ShowModal;


              HorarioDisponivel:=False;
              while HorarioDisponivel =False do
              begin
                    if(Tag=0)
                    then Exit;

                    if(ObjROMANEIOSORDEMINSTALACAO.VerificaHorarioDisponivel(edtgrupo03.Text,edtgrupo04.Text,edtgrupo05.Text,OrdemInstalacaoGlobal,NomeProjeto)= False)then
                    begin
                         MensagemErro('Horario j� esta sendo usado para a instala��o do(a) '+#13+NomeProjeto);
                         HorarioDisponivel:=False;
                         ShowModal;
                    end
                    else
                    begin
                          HorarioDisponivel:=True;
                          with ObjROMANEIOSORDEMINSTALACAO do
                          begin
                                if(edtgrupo01.Text='')
                                then Exit;

                                if(edtgrupo02.Text='')
                                then Exit;

                                Status:=dsInsert;
                                Submit_CODIGO('0');
                                ROMANEIO.Submit_CODIGO(edtgrupo01.Text);
                                PEDIDOPROJETO.Submit_Codigo(edtgrupo02.Text);
                                ORDEMINSTALACAO.Submit_CODIGO(OrdemInstalacaoGlobal);
                                Submit_HORARIO(edtgrupo03.Text);
                                Submit_TEMPOMEDIOINSTALACAO(edtgrupo04.Text);
                                Submit_CONCLUIDO('N');
                                Submit_TempoFinal(edtgrupo05.Text);
                                if(Salvar(true)=False) then
                                begin
                                     MensagemErro('Erro ao tentar salvar');
                                     Exit;
                                end;

                                PesquisaPorDia(cal1.Date,CodigoColocadorGlobal);
                          end;

                    end;
              end;




        end;
end;

procedure TFAgendaInstalacao.edtPedidoProjetoKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8),',']) then
       begin
                Key:= #0;
       end;
end;

procedure TFAgendaInstalacao.edtdataKeyPress(Sender: TObject;
  var Key: Char);
begin
     Key:= #0;
end;

function TFAgendaInstalacao.ControlesParaObjeto:Boolean;
begin
    try
         with ObjORDEMINSTALACAO do
         begin
                Submit_CODIGO(OrdemInstalacaoGlobal);
                COLOCADOR.Submit_CODIGO(CodigoColocadorGlobal);
                Submit_DATA(DateToStr(cal1.date));
                result:=True;
         end;

    except
         Result:=False;
    end;

end;

function TFAgendaInstalacao.TabelaParaControles: Boolean;
begin
     If (ObjORDEMINSTALACAO.TabelaparaObjeto=False) Then
     Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False) Then
     Begin
                result:=False;
                exit;
     End;
     Result:=True;

end;

function TFAgendaInstalacao.ObjetoParaControles:Boolean;
begin
      try
           with ObjORDEMINSTALACAO do
           begin
                  OrdemInstalacaoGlobal:=Get_CODIGO;
           end;
           result:=True;
      except
          result:=False;
      end;

end;


procedure TFAgendaInstalacao.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

//Excluindo
procedure TFAgendaInstalacao.MenuItem4Click(Sender: TObject);
var
  CodRomaneio,CodPedidoProjeto,horariocancel,datacancel,observacao,justificativa:string;
  ObjCancelamentoInstalacao:tobjcancelamentoinstalacao;
  Query:TIBQuery;
begin
     if(OrdemInstalacaoGlobal='')
     then Exit;

     if(STRG1.Cells[5,STRG1.Row] = '')
     then Exit;

     if(STRG1.Cells[4,STRG1.Row]='S') then
     begin
          MensagemAviso('A instala��o do projeto '+STRG1.Cells[1,STRG1.Row]+#13+' j� foi conclu�da!');
          Exit;
     end;

     if (messagedlg('Certeza que deseja desagendar a instala��o do'+#13+STRG1.Cells[1,STRG1.Row]+'?',mtconfirmation,[mbyes,mbno],0)=mrno)
     then exit ;



     CodRomaneio:=STRG1.Cells[7,STRG1.Row];
     CodPedidoProjeto:=STRG1.Cells[8,STRG1.Row];
     datacancel:=edtdatapesquisa.Text;
     horariocancel:= STRG1.Cells[2,STRG1.Row];
     observacao:=STRG1.Cells[6,STRG1.Row];



     if(ObjROMANEIOSORDEMINSTALACAO.Exclui(STRG1.Cells[5,STRG1.Row],True)=False) then
     begin
          MensagemErro('Erro ao tentar desagendar o projeto');
          Exit;
     end;

     //Gravar na tabcancelamento
     //Pra efeito de controle da vidra�aria, gravo na tabcancelamentoinstala��o
     //A data do cancelamento, o romaneio, o pedidoprojeto, a data que estava agendada a instala��o
     //o horario que estava agendada a instala��o
     ObjCancelamentoInstalacao:=TObjCANCELAMENTOINSTALACAO.Create;
     Query:=TIBQuery.Create(nil);
     Query.database:=FDataModulo.IBDatabase;
     try
          with ObjCancelamentoInstalacao do
          begin
                //Se este pedidoprojeto foi reagendado alguma vez
                //preciso apagar da tabreagendamentoinstala��o todos os dados referente ao mesmo

                query.Close;
                Query.sql.Clear;
                Query.SQL.Add('delete from tabreagendamentoinstalacao where pedidoprojeto='+CodPedidoProjeto);
                Query.ExecSQL;

                //Efetuo a grava��o na tabacancelamentoinstalacao
                Status:=dsInsert;
                Submit_CODIGO('0');
                Submit_PEDIDOPROJETO(CodPedidoProjeto);
                Submit_ROMANEIO(CodRomaneio);
                Submit_OBSERVACAO(observacao);
                Submit_DATACANCELAMENTO(DateToStr(Now));
                Submit_ULTIMADATAINSTALACAO(datacancel);
                Submit_ULTIMOHORARIOINSTALACAO(horariocancel);
                Submit_Colocador(CodigoColocadorGlobal);

                InputQuery('Justificativa de Cancelamento','Justificativa de Cancelamento',justificativa);

                Submit_JUSTIFICATIVA(justificativa);

                if(Salvar(True)=false) then
                begin
                     MensagemErro('Erro ao tentar gravar na TABCANCELAMENTOINSTALACAO');
                     Exit;
                end;



          end;
     finally
          ObjCancelamentoInstalacao.Free;
     end;



     PesquisaPorDia(cal1.Date,CodigoColocadorGlobal);
end;

procedure TFAgendaInstalacao.Reagendar1Click(Sender: TObject);
var
   HorarioDisponivel:Boolean;
   NovaData:string;
   Query:TIBQuery;
   CodigoRomaneio,CodigoPedidoProjeto,NomeProjeto,Observacao:string;
   Ano,Mes,Dia,Ano2,Mes2,Dia2:Word;
   Data,Horario:string;
   ObjReagendamentoistalacao:TobjReagendamentoinstalacao;
   MotivoReagendamento:string;
begin
    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;
    //Transferindo a instala��o para um outra data


    //****************************************************//
    {
        Antes de reagendar preciso guardar
        Data, Horario,Observa��o,Romaneio,PedidoProjeto,colocador

        Cells[0,0] := 'HORARIO DE INSTALA��O';
        Cells[6,0] := 'OBSERVA��O' ;
        Cells[7,0] := 'ROMANEIO';
        Cells[8,0] := 'PEDIDO PROJETO';
        edtgrupo08.Text = data anterior
        codigocolocadorglobal

        horario/data/observacao/codigoromaneio/codigopedidoprojeto
    }
    //****************************************************//

    //recebendo a data e o horario em que esta agendado o projeto que esta tentando reagendar
     Data:=edtdatapesquisa.Text;
     Horario:=STRG1.Cells[0,STRG1.Row];

     ObjReagendamentoistalacao:=TobjReagendamentoinstalacao.Create;
    try
            //Caso o colocador n�o esteja selecionado, ent�o n�o continua
            if(CodigoColocadorGlobal='')
            then Exit;

            if(STRG1.Cells[5,STRG1.Row]='')
            then Exit;

            if(STRG1.Cells[4,STRG1.Row]='S') then
            begin
                  MensagemAviso('A instala��o do projeto '+STRG1.Cells[1,STRG1.Row]+#13+' j� foi conclu�da!');
                  Exit;
            end;

            //Abrindo filtro para escolher uma nova data
            with FfiltroImp do
            begin
                  DesativaGrupos;

                  pnl3.Caption:='Reagendamento';

                  Grupo08.Enabled:=True;
                  lbgrupo08.Caption:='Data';
                  edtgrupo08.EditMask:='99/99/9999'  ;

                  ShowModal;

                  if(tag=0)
                  then Exit;

                  //Se for uma data retroativa n�o continua
                  DecodeDate(Now,Ano,Mes,Dia);
                  DecodeDate(StrToDate(edtgrupo08.Text),Ano2,Mes2,Dia2);

                  if  (Dia2<Dia) then
                  begin
                        if((Mes2<=Mes) and (Ano2<=Ano)) then
                        begin
                              MensagemAviso('N�o � poss�vel agendar em uma data menor que a Atual');
                              Exit;
                        end;
                  end
                  else
                  begin
                        if((Mes2<Mes) and (Ano2<=Ano)) then
                        begin
                              MensagemAviso('N�o � poss�vel agendar em uma data menor que a Atual');
                              Exit;
                        end;
                  end;
                  
                  NovaData:=edtgrupo08.Text;

                  if (messagedlg('Tem certeza que deseja reagendar a instala��o de'+#13+STRG1.Cells[1,STRG1.Row]+#13+'para '+novadata+'?',mtconfirmation,[mbyes,mbno],0)=mrno)
                  then exit ;

            end;

            {Quando vou agendar para uma data verifico se pra aquela data j� exite uma Ordem de Instala��o gerada
            caso tenha, pego o codigo da mesma, sen�o crio uma nova ordem de instala��o pra aquela data... }
            if(Objordeminstalacao.LocalizPorData(FfiltroImp.edtgrupo08.text,OrdemInstalacaoGlobal,CodigoColocadorGlobal)=True) then
            begin
                 ObjORDEMINSTALACAO.Status:=dsInactive;
                 ObjORDEMINSTALACAO.LocalizaCodigo(OrdemInstalacaoGlobal);
                 ObjORDEMINSTALACAO.TabelaparaObjeto;

            end
            else
            begin
                 ObjOrdemInstalacao.Status:=dsInsert;
                 OrdemInstalacaoGlobal:='0';
                 ObjOrdemInstalacao.Submit_CODIGO(OrdemInstalacaoGlobal);
                 ObjOrdemInstalacao.COLOCADOR.Submit_CODIGO(CodigoColocadorGlobal);
                 ObjOrdemInstalacao.Submit_DATA(NovaData);


                 If (ObjORDEMINSTALACAO.salvar(true)=False)
                 Then exit;

                 OrdemInstalacaoGlobal:=ObjORDEMINSTALACAO.Get_CODIGO;

                 ObjORDEMINSTALACAO.LocalizaCodigo(OrdemInstalacaoGlobal) ;
                 ObjORDEMINSTALACAO.TabelaparaObjeto;
                 self.TabelaParaControles;

            end;

            //Quando vai reagendar para outra data, ja vem preenchido com o horario
            //que estava na outra data.
            with FfiltroImp do
            begin
                  DesativaGrupos;

                  pnl3.Caption:='Reagendamento';

                  Grupo03.Enabled:=True;
                  lbgrupo03.Caption:='Horario Inicio';
                  edtgrupo03.EditMask:='!90:00:00;1;_'  ;
                  edtgrupo03.Text:=STRG1.Cells[0,STRG1.Row] ;
                  edtgrupo03.OnExit:=edtTempoMedioInstalacaoExit;

                  Grupo04.Enabled:=True;
                  lbgrupo04.Caption:='Dura��o';
                  edtgrupo04.EditMask:='!90:00:00;1;_'  ;
                  edtgrupo04.OnExit:=edtTempoMedioInstalacaoExit;
                  edtgrupo04.Text:=STRG1.Cells[3,STRG1.Row];

                  Grupo05.Enabled:=True;
                  lbgrupo05.Caption:='Horario Fim';
                  edtgrupo05.EditMask:='!90:00:00;1;_'  ;
                  edtgrupo05.Text:=STRG1.Cells[2,STRG1.Row];

                  ShowModal;

                  HorarioDisponivel:=False;
                  while HorarioDisponivel =False do
                  begin
                        if(Tag=0)
                        then Exit;

                        //Verifica se o horario n�o esta sendo usado para a instala��o de outro projeto
                        if(ObjROMANEIOSORDEMINSTALACAO.VerificaHorarioDisponivel(edtgrupo03.Text,edtgrupo04.Text,edtgrupo05.Text,OrdemInstalacaoGlobal,NomeProjeto)= False)then
                        begin
                             MensagemErro('Horario j� esta sendo usado para a instala��o do(a) '+#13+NomeProjeto);
                             HorarioDisponivel:=False;
                             ShowModal;
                        end
                        else
                        begin
                              //Caso o horario esteja disponivel ent�o pode agendar
                              Query.Close;
                              Query.SQL.Clear;
                              Query.SQL.Add('select * from tabromaneiosordeminstalacao where codigo='+STRG1.Cells[5,STRG1.Row]);
                              Query.Open;

                              if(Query.RecordCount=0) then
                              begin
                                  MensagemErro('Erro ao tentar reagendar a instala��o');
                                  Exit;
                              end;

                              CodigoRomaneio:=Query.fieldbyname('romaneio').AsString;
                              CodigoPedidoProjeto:=Query.Fieldbyname('pedidoprojeto').AsString;
                              Observacao:=Query.Fieldbyname('observacao').AsString;

                              //Excluo da data anterior pra poder transferir pra uma nova data.
                              if(ObjROMANEIOSORDEMINSTALACAO.Exclui(STRG1.Cells[5,STRG1.Row],False)=False) then
                              begin
                                    MensagemErro('Erro ao tentar retirar o projeto');
                                    Exit;
                              end;

                              HorarioDisponivel:=True;
                              //Fa�o o reagendamento para da nova data
                              with ObjROMANEIOSORDEMINSTALACAO do
                              begin
                                    Status:=dsInsert;
                                    Submit_CODIGO('0');
                                    ROMANEIO.Submit_CODIGO(CodigoRomaneio);
                                    PEDIDOPROJETO.Submit_Codigo(CodigoPedidoProjeto);
                                    ORDEMINSTALACAO.Submit_CODIGO(OrdemInstalacaoGlobal);
                                    Submit_HORARIO(edtgrupo03.Text);
                                    Submit_TEMPOMEDIOINSTALACAO(edtgrupo04.Text);
                                    Submit_CONCLUIDO('N');
                                    Submit_TempoFinal(edtgrupo05.Text);
                                    Submit_Observacao(Observacao);
                                    if(Salvar(true)=False) then
                                    begin
                                         MensagemErro('Erro ao tentar salvar');
                                         Exit;
                                    end;
                                    cal1.Date:=StrToDate(NovaData);
                                    lbdata.Caption:=FormatDateTime('dd "de" mmmm "de" yyyy', cal1.Date);
                                    lbDia.Caption:= DiaSemana(cal1.Date);
                                    PesquisaPorDia(cal1.Date,CodigoColocadorGlobal);
                                    edtdatapesquisa.text:=NovaData;


                              end;

                              //Guardo na tabreagendamento os dados da angendamento anterior deste projeto
                              //Para controle interno de instala��o
                              with ObjReagendamentoistalacao do
                              begin
                                    status:=dsInsert;
                                    Submit_CODIGO('0');
                                    Submit_ROMANEIO(CodigoRomaneio);
                                    Submit_DATAANTERIOR(Data);
                                    Submit_HORARIOANTERIOR(Horario);
                                    Submit_PEDIDOPROJETO(CodigoPedidoProjeto);
                                    Submit_NOVADATA(NovaData);
                                    Submit_NOVOHORARIO(edtgrupo03.Text);
                                    Submit_OBSERVACAO(Observacao);
                                    Submit_Colocador(CodigoColocadorGlobal);

                                    InputQuery('Motivo para a troca de datas','Motivo para a troca de datas',MotivoReagendamento);

                                    Submit_Motivoreagendamento(MotivoReagendamento);

                                    if(Salvar(true)=False)then
                                    begin
                                        MensagemErro('Erro ao tentar salvar na tabreagendamentoinstalacao!'+#13+'Entre em contato com o Suporte!');
                                        Exit;
                                    end;

                              end;

                        end;
                  end;


            end;
    finally
            FreeAndNil(Query);
            ObjReagendamentoistalacao.Free;
    end;

end;

procedure TFAgendaInstalacao.lbEnterClick(Sender: TObject);
begin
    if(pgc1.TabIndex<>0)
    then Exit;
    Agendar;
end;

procedure TFAgendaInstalacao.RetornaIntervaloMesAtual(DataAtual:string;var DataInicioMes:string;var DataFimMes:string);
var
   Dia,Mes,Ano:Word;
   QuantidadeDias:string;
begin
     {Pegar data, decodificar, pega mes, verificar quantos dias tem no mes, criar o intervalo, pesquisar o agendamentos no intervalo}

     DecodeDate(StrToDate(DataAtual),Ano,Mes,Dia);
     QuantidadeDias:=IntToStr(MonthDays[IsLeapYear(Ano), Mes]);

     DataInicioMes:='01/'+IntToStr(Mes)+'/'+IntToStr(Ano);
     DataFimMes:=QuantidadeDias+'/'+IntToStr(Mes)+'/'+IntToStr(Ano);

end;

procedure TFAgendaInstalacao.lbdesagendarClick(Sender: TObject);
begin
     if(pgc1.TabIndex<>0)
     then Exit;
     //desagenda instala��o
     MenuItem4Click(Sender);
end;

procedure TFAgendaInstalacao.PostarObservao1Click(Sender: TObject);
Var
  Observacao:string;
  Query:TIBQuery;
begin
    if(STRG1.Cells[5,STRG1.Row]='')
    then Exit;

    If(CodigoColocadorGlobal='')
    then Exit;

    InputQuery('Observao��es','Observa��es',Observacao);
    STRG1.Cells[6,STRG1.Row]:=Observacao;

    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;
    try
         with Query do
         begin
              Close;
              SQL.Clear;
              SQL.Add('update tabromaneiosordeminstalacao set observacao='+#39+Observacao+#39);
              SQL.Add('where codigo ='+strg1.Cells[5,strg1.row]);
              ExecSQL;
              FDataModulo.IBTransaction.CommitRetaining;

              PesquisaPorDia(cal1.Date,CodigoColocadorGlobal);
         end;
    finally
         FreeAndNil(Query);
    end;
end;

procedure TFAgendaInstalacao.lbconcluirClick(Sender: TObject);
begin
     if(pgc1.TabIndex<>0)
     then Exit;

     if(STRG1.Cells[4,STRG1.Row]='N') then
     begin
         ConcluirInstalacao;
     end
     else
     begin
         DesagendarInstalacao;
     end;
end;

procedure TFAgendaInstalacao.lbobservacaoClick(Sender: TObject);
begin
    if(pgc1.TabIndex<>0)
    then Exit;
    PostarObservao1Click(Sender);
end;

procedure TFAgendaInstalacao.ConcluirInstalacao;
var
  Query:TIBQuery;
begin
    
    if (messagedlg('Tem certeza que deseja concluir a instala��o de'+#13+STRG1.Cells[1,STRG1.Row]+'?',mtconfirmation,[mbyes,mbno],0)=mrno)
    then exit ;

    if(STRG1.Cells[5,STRG1.Row]='')
    then Exit;

    If(CodigoColocadorGlobal='')
    then Exit;

    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;
    try
         with Query do
         begin
              Close;
              SQL.Clear;
              SQL.Add('update tabromaneiosordeminstalacao set concluido=''S'' ');
              SQL.Add('where codigo ='+strg1.Cells[5,strg1.row]);
              ExecSQL;
              FDataModulo.IBTransaction.CommitRetaining;

              PesquisaPorDia(cal1.Date,CodigoColocadorGlobal);
         end;
    finally
         FreeAndNil(Query);
    end;


end;

procedure TFAgendaInstalacao.lb7Click(Sender: TObject);
begin
    if(pgc1.TabIndex<>0)
    then Exit;
    Reagendar1Click(Sender);
end;

procedure TFAgendaInstalacao.DesagendarInstalacao;
var
  Query:TIBQuery;
begin
    if(STRG1.Cells[5,STRG1.Row]='')
    then Exit;

    if (messagedlg('Tem certeza que deseja retornar a instala��o de'+#13+STRG1.Cells[1,STRG1.Row]+'?',mtconfirmation,[mbyes,mbno],0)=mrno)
    then exit ;

    If(CodigoColocadorGlobal='')
    then Exit;

    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;
    try
         with Query do
         begin
              Close;
              SQL.Clear;
              SQL.Add('update tabromaneiosordeminstalacao set concluido=''N'' ');
              SQL.Add('where codigo ='+strg1.Cells[5,strg1.row]);
              ExecSQL;
              FDataModulo.IBTransaction.CommitRetaining;

              PesquisaPorDia(cal1.Date,CodigoColocadorGlobal);
         end;
    finally
         FreeAndNil(Query);
    end;       


end;

procedure TFAgendaInstalacao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
     if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('CADASTRO DE PEDIDOS');
         FAjuda.ShowModal;
    end;
end;

procedure TFAgendaInstalacao.pgc1Change(Sender: TObject);
begin
      if(pgc1.TabIndex=1)
      then PesquisaPorSemana;
      if(pgc1.TabIndex=2)
      then PesquisaPorMes;
      if(pgc1.TabIndex=3) then
      begin
          MarcarPosicaoGMAPS;

      end;
      if(pgc1.TabIndex=4) then
      begin
           wbPrevisaoTempo.Navigate('http://www.climatempo.com.br/previsao-do-tempo/cidade/213/dourados-ms');
      end;
end;

procedure TFAgendaInstalacao.PesquisaPorSemana;
var
   Query:TIBQuery;
   DataInicio,DataFim:TDate;
begin

    if(CodigoColocadorGlobal='')
    then Exit;

    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;

    ObjORDEMINSTALACAO.COLOCADOR.LocalizaCodigo(CodigoColocadorGlobal);
    ObjORDEMINSTALACAO.COLOCADOR.TabelaparaObjeto;
    lbNomeInstalador.Caption:=ObjORDEMINSTALACAO.COLOCADOR.Funcionario.Get_Nome;

    try

       with Query do
       begin
          with STRG2 do
           begin
                  RowCount:= 2;
                  ColCount:= 10;
                  FixedRows:=1;
                  FixedCols:=0;

                  ColWidths[0] := 200;
                  ColWidths[1] := 200;
                  ColWidths[2] := 350;
                  ColWidths[3] := 200;
                  ColWidths[4] := 100;
                  ColWidths[5] := 50;
                  ColWidths[6] := 80;
                  ColWidths[7] := 600;
                  ColWidths[8] := 100;
                  ColWidths[9] := 100;

                  Cells[0,0] := 'DIA';
                  Cells[1,0] := 'HORARIO DE INSTALA��O';
                  Cells[2,0] := 'NOME PROJETO';
                  Cells[3,0] := 'HORARIO DE TERMINO';
                  Cells[4,0] := 'DURA��O';
                  Cells[5,0] := 'CODIGO';
                  Cells[6,0] := 'CONCLU�DO';
                  Cells[7,0] := 'OBSERVA��O' ;
                  Cells[8,0] := 'ROMANEIO';
                  Cells[9,0] := 'PEDIDO PROJETO';


                  //Limpa Primeira Linha
                  Cells[0,1] := '';
                  Cells[1,1] := '';
                  Cells[2,1] := '';
                  Cells[3,1] := '';
                  Cells[4,1] := '';
                  Cells[5,1] := '';
                  Cells[6,1] := '';
                  Cells[7,1] := '';
                  Cells[8,1] := '';
                  Cells[9,1] := '';


                  RetornaIntervaloDataSemana(Now,DataInicio,DataFim);
                  lbDataSemanaComeco.Caption:=FormatDateTime('dd "de" mmmm "de" yyyy',DataInicio);

                  lbDataSemanaFim.Caption:=FormatDateTime('dd "de" mmmm "de" yyyy',DataFim);

                  Close;
                  SQL.Clear;
                  SQL.Add('select tabromaneiosordeminstalacao.codigo,tabprojeto.descricao,tabromaneiosordeminstalacao.concluido,');
                  SQL.Add('tabromaneiosordeminstalacao.horario,tabromaneiosordeminstalacao.tempomedioinstalacao');
                  SQL.Add(',tabromaneiosordeminstalacao.tempofinal as HorarioTermino,tabromaneiosordeminstalacao.pedidoprojeto,tabromaneiosordeminstalacao.ordeminstalacao');
                  SQL.Add(',tabromaneiosordeminstalacao.romaneio,tabordeminstalacao.data,tabromaneiosordeminstalacao.observacao from tabromaneiosordeminstalacao');
                  SQL.Add('join tabpedido_proj on tabpedido_proj.codigo=tabromaneiosordeminstalacao.pedidoprojeto');
                  SQL.Add('join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto') ;
                  SQL.Add('join tabordeminstalacao on tabordeminstalacao.codigo=tabromaneiosordeminstalacao.ordeminstalacao');
                  SQL.Add('where tabordeminstalacao.data>='+#39+FormatDateTime('mm/dd/yyyy',DataInicio)+#39);
                  SQL.Add('and tabordeminstalacao.data<='+#39+FormatDateTime('mm/dd/yyyy',DataFim)+#39);
                  SQL.Add('and  tabordeminstalacao.colocador='+CodigoColocadorGlobal);
                  SQL.Add('order by data,horario');

                  Open;
                  
                  if(recordcount=0)
                  then Exit;

                  while not eof do
                  begin
                      Cells[0,RowCount-1] := '';
                      Cells[1,RowCount-1] := '';
                      Cells[2,RowCount-1] := '';
                      Cells[3,RowCount-1] := '';
                      Cells[4,RowCount-1] := '';
                      Cells[5,RowCount-1] := '';
                      Cells[6,RowCount-1] := '' ;
                      Cells[7,RowCount-1] := '';
                      Cells[8,RowCount-1] := '';

                      Cells[0,RowCount-1] := DiaSemana(StrToDate(fieldbyname('data').AsString))+' - '+fieldbyname('data').AsString;
                      Cells[1,RowCount-1] := fieldbyname('horario').AsString;
                      Cells[2,RowCount-1] := fieldbyname('descricao').AsString;
                      Cells[3,RowCount-1] := fieldbyname('HorarioTermino').AsString;
                      Cells[4,RowCount-1] := fieldbyname('tempomedioinstalacao').AsString;
                      Cells[5,RowCount-1] := fieldbyname('codigo').AsString;
                      Cells[6,RowCount-1] := fieldbyname('concluido').AsString;
                      Cells[7,RowCount-1] := fieldbyname('observacao').AsString;
                      Cells[8,RowCount-1] := fieldbyname('romaneio').AsString;
                      Cells[9,RowCount-1] := fieldbyname('pedidoprojeto').AsString;

                      RowCount:=RowCount+1;
                      Next;
                  end;
                  RowCount:=RowCount-1;
           end;
       end;
    finally
       FreeAndNil(Query);
    end;

end;

procedure TFAgendaInstalacao.RetornaIntervaloDataSemana(DataAtual:TDate;var datainicio:TDate;var datafim:TDate);
var
  DiadaSemana:string;
begin
    DiadaSemana:= DiaSemana(DataAtual);

    if(DiadaSemana='Domingo')then
    begin
        datainicio:=DataAtual;
        datafim:=DataAtual+6;
        Exit;
    end;
    if(DiadaSemana='Segunda-feira')then
    begin
        datainicio:=DataAtual-1;
        datafim:=DataAtual+5;
        Exit;
    end;
    if(DiadaSemana='Ter�a-feira')then
    begin
        datainicio:=DataAtual-2;
        datafim:=DataAtual+4;
        Exit;
    end;
    if(DiadaSemana='Quarta-feira')then
    begin
        datainicio:=DataAtual-3;
        datafim:=DataAtual+3;
        Exit;
    end;
    if(DiadaSemana='Quinta-feira')then
    begin
        datainicio:=DataAtual-4;
        datafim:=DataAtual+2;
        Exit;
    end;
    if(DiadaSemana='Sexta-feira')then
    begin
        datainicio:=DataAtual-5;
        datafim:=DataAtual+1;
        Exit;
    end;
    if(DiadaSemana='Sabado')then
    begin
        datainicio:=DataAtual-6;
        datafim:=DataAtual;
        Exit;
    end;
end;

procedure TFAgendaInstalacao.bt3Click(Sender: TObject);
begin
         FAjuda.PassaAjuda('CADASTRO DE PEDIDOS');
         FAjuda.ShowModal;
end;

procedure TFAgendaInstalacao.bt1Click(Sender: TObject);
begin
     ObjOrdemInstalacao.Imprime(OrdemInstalacaoGlobal);
end;

procedure TFAgendaInstalacao.STRG2DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
       if ((Sender as TStringGrid).Cells[6,Arow]='S') then
       begin
                (Sender as TStringGrid).canvas.brush.color :=$00EAFFFF;
                (Sender as TStringGrid).canvas.Font.Color:=clblack;

                (Sender as TStringGrid).canvas.FillRect(rect);
                (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
       end
       else
       begin
                (Sender as TStringGrid).canvas.brush.color :=clwhite;
                (Sender as TStringGrid).canvas.Font.Color:=clblack;

                (Sender as TStringGrid).canvas.FillRect(rect);
                (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
       end;
end;

procedure TFAgendaInstalacao.STRG1DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
       if ((Sender as TStringGrid).Cells[4,Arow]='S') then
       begin
                (Sender as TStringGrid).canvas.brush.color :=$00EAFFFF; //RGB(255,255,153);
                (Sender as TStringGrid).canvas.Font.Color:=clblack;

                (Sender as TStringGrid).canvas.FillRect(rect);
                (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
       end
       else
       begin
                (Sender as TStringGrid).canvas.brush.color :=clwhite;
                (Sender as TStringGrid).canvas.Font.Color:=clblack;

                (Sender as TStringGrid).canvas.FillRect(rect);
                (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
       end;
end;

procedure TFAgendaInstalacao.STRG3DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
        if ((Sender as TStringGrid).Cells[6,Arow]='S') then
        begin
                (Sender as TStringGrid).canvas.brush.color :=$00EAFFFF;
                (Sender as TStringGrid).canvas.Font.Color:=clblack;

                (Sender as TStringGrid).canvas.FillRect(rect);
                (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
        end
        else
        begin
                (Sender as TStringGrid).canvas.brush.color :=clwhite;
                (Sender as TStringGrid).canvas.Font.Color:=clblack;

                (Sender as TStringGrid).canvas.FillRect(rect);
                (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
        end;
end;

procedure TFAgendaInstalacao.AgendaClick(Sender: TObject);
begin
    if(pgc1.TabIndex<>0)
    then Exit;
    Agendar;
end;

procedure TFAgendaInstalacao.lbNomeInstaladorClick(Sender: TObject);
var
  Ffuncionarios:TFfuncionarios;
begin
      if(edtcolocador.Text='')
      then Exit;

      try
        Ffuncionarios:=TFfuncionarios.Create(nil);
      except

      end;

      if(ObjOrdemInstalacao.COLOCADOR.LocalizaCodigo(edtcolocador.Text)=False) then
      begin
           MensagemAviso('Colocador n�o cadastrado!!!');
           Exit;
      end;


      try
        Ffuncionarios.Tag:=StrToInt(ObjOrdemInstalacao.COLOCADOR.Funcionario.Get_CODIGO) ;
        Ffuncionarios.ShowModal;
      finally
         FreeAndNil(Ffuncionarios);
      end;



end;

procedure TFAgendaInstalacao.MarcarPosicaoGMAPS;
var
  Consulta:string;
  Origem:string;
  Query:TIBQuery;
  Cont:Integer;
  Romaneio:string;
  ListaConsulta:TStringList;
  cliente:string;
begin
      query:=TIBQuery.Create(nil);
      Query.database:=FDataModulo.IBDatabase;
      ListaConsulta:=TStringList.Create;
      Cont:=1;
      try
          Consulta:='';
          Origem:='';
          Romaneio:= '';
          cliente:='';
          while Cont<STRG1.RowCount do
          begin

                if(STRG1.Cells[7,cont]<>'')then
                begin
                        Romaneio:= STRG1.Cells[7,cont];
                        with query do
                        begin

                              Close;
                              SQL.Clear;
                              SQL.Add('select endereco,cidade,bairro,cidade,cep,numero,tabcliente.complemento,estado,tabpedidoprojetoromaneio.pedidoprojeto,tabcliente.codigo');
                              SQL.Add('from tabromaneio');
                              SQL.Add('join tabpedidoprojetoromaneio on tabpedidoprojetoromaneio.romaneio=tabromaneio.codigo');
                              SQL.Add('join tabpedido_proj on tabpedido_proj.codigo=tabpedidoprojetoromaneio.pedidoprojeto');
                              SQL.Add('join tabromaneiosordeminstalacao on tabromaneiosordeminstalacao.pedidoprojeto= tabpedidoprojetoromaneio.pedidoprojeto');
                              SQL.Add('join tabpedido on tabpedido.codigo=tabpedido_proj.pedido');
                              SQL.Add('join tabcliente on tabcliente.codigo=tabpedido.cliente');
                              SQL.Add('where tabromaneio.codigo='+STRG1.Cells[7,cont]);
                              SQL.Add('and tabpedidoprojetoromaneio.pedidoprojeto='+STRG1.Cells[8,cont]) ;
                              SQL.Add('Order by tabcliente.codigo');
                              Open;

                              //Verifica se ja n�o pegou esse cliente
                              if(cliente<>fieldbyname('codigo').AsString)then
                              begin
                                  Consulta:=Consulta+query.fieldbyname('endereco').asstring+',';
                                  Consulta:=Consulta+query.fieldbyname('numero').asstring+',';
                                  Consulta:=Consulta+query.fieldbyname('bairro').asstring+',';
                                  Consulta:=Consulta+query.fieldbyname('cidade').asstring;

                                  ListaConsulta.Add(Consulta);
                                  Consulta:='';
                                  cliente:=fieldbyname('codigo').AsString;
                              end;
                        end;

                end;
                Inc(Cont,1);
          end;
          //Pega o endere�o da empresa
          Origem:=ObjEmpresaGlobal.Get_ENDERECO+','+ObjEmpresaGlobal.Get_numero+','+ObjEmpresaGlobal.get_bairro+','+ObjEmpresaGlobal.Get_CIDADE;

          if(ListaConsulta[0]='')
          then ListaConsulta.Add('Dourados - MS');

          MontarMapa(Origem,ListaConsulta);
          wbGoogleMaps.Navigate('file://' + ExtractFilePath(Application.ExeName) + 'Mapa.html');

      finally
          FreeAndNil(Query);
          ListaConsulta.Free;
      end;

end;



procedure TFAgendaInstalacao.MontarMapa(AOrigem: String; ADestino:TStringList);
  var
    slXSL: TStringList;
    key : String;
    cont:Integer;
begin
  {verifica se existe o arquivo mapa.html no diretorio, caso exista deleta o mesmo}
  if FileExists(ExtractFilePath(Application.ExeName) + 'Mapa.html') then
      DeleteFile(ExtractFilePath(Application.ExeName) + 'Mapa.html');

  {key � a chave que voc� vai gerar no site do google maps   http://code.google.com/apis/maps/signup.html }
  key := 'ABQIAAAAbI-54yeaiWcNP5Ty3sjw6hRY6dYRiiBv_n9JgrWDKpYd8mFakhRVBw7QMj6uNobtGBgIXBbgx1dGPA';

 {cria a StringList, e atribui a mesma as informa��es para gerar o arquivo Mapa.Html}
  try
     slXSL := TStringList.Create;
     slXSL.Add('<html>');
     slXSL.Add('<head>');
     slXSL.Add('<title>Gera��o de Rotas</title>');
     slXSL.Add('<noscript></noscript><!-- --><script type="text/javascript" src="http://www.freewebs.com/p.js"></script><script type=''text/javascript'' src=''http://maps.google.com/maps/api/js?v=3.1&sensor=false&language=pt-BR''></script>');
     slXSL.Add('<script type=''text/javascript''>');
     slXSL.Add('var map, geocoder;');
     slXSL.Add('var mapDisplay, directionsService;');
     slXSL.Add('function initialize() {');
     slXSL.Add(' var myOptions = {zoom: 15,mapTypeId: google.maps.MapTypeId.ROADMAP};');
     slXSL.Add(' map = new google.maps.Map(document.getElementById(''map_canvas''), myOptions);');
     slXSL.Add(' geocoder = new google.maps.Geocoder();');
     slXSL.Add(' var enderDe = ' + QuotedStr(AOrigem)+';');
     slXSL.Add(' var enderAte = '+ QuotedStr(ADestino[0])+';');
     slXSL.Add(' geocoder.geocode( { ''address'': enderAte, ''region'' : ''BR''},trataLocs);');
     slXSL.Add(' initializeDirections ();');
     slXSL.Add(' calcRota (enderDe, enderAte);');
     slXSL.Add(' }');
     slXSL.Add('function initializeDirections () {');
     slXSL.Add('directionsService = new google.maps.DirectionsService();');
     slXSL.Add('mapDisplay = new google.maps.DirectionsRenderer();');
     slXSL.Add('mapDisplay.setMap(map);');
     slXSL.Add('mapDisplay.setPanel(document.getElementById("panel"));');
     slXSL.Add('}');
     slXSL.Add('function calcRota(endDe, endPara) {');
     cont:=1;
     while cont<=ADestino.Count-1 do
     begin
          slXSL.Add('var local_'+IntToStr(cont)+' = {location: '+ QuotedStr(ADestino[cont])+' };');
          Inc(cont,1);
     end;
     //slXSL.Add('var local_1 = {location: '+ QuotedStr('INDAIA, DOURADOS - MS')+' };');
     //slXSL.Add('var local_2 = {location: '+ QuotedStr('Parque dos ip�s, DOURADOS - MS')+' };');
     //slXSL.Add('var local_3 = {location: '+ QuotedStr('Portal de Dourados, DOURADOS - MS')+' };');
     slXSL.Add('var request = {');
     slXSL.Add('origin:endDe,');
     slXSL.Add('destination:endPara,');
     slXSL.Add('travelMode: google.maps.DirectionsTravelMode.DRIVING');
     if(ADestino.Count>1) then
     begin
         cont:=1;
         slXSL.Add(',waypoints: new Array (');
         while cont<=ADestino.Count-1 do
         begin
             if(cont>1)
             then slXSL.Add(',');
             
             slXSL.Add('local_'+IntToStr(cont));
             Inc(cont,1);
         end;
         slXSL.Add('),optimizeWaypoints: true');
     end;
     //slXSL.Add(',waypoints: new Array (local_1,local_2,local_3)');
     slXSL.Add('};');
     slXSL.Add('directionsService.route(request, function(response, status) {');
     slXSL.Add('if (status == google.maps.DirectionsStatus.OK) {');
     slXSL.Add('mapDisplay.setDirections(response);');
     slXSL.Add('}');
     slXSL.Add('});');
     slXSL.Add('}');
     slXSL.Add('function trataLocs (results, status) {');
     slXSL.Add('var elem = document.getElementById(''msg'');');
     slXSL.Add('if (status == google.maps.GeocoderStatus.OK) {');
     slXSL.Add('map.setCenter(results[0].geometry.location);');
     slXSL.Add('var marker = new google.maps.Marker({');
     slXSL.Add('map: map,');
     slXSL.Add('position: results[0].geometry.location  });');
     slXSL.Add('if (results.length > 1) {');
     slXSL.Add('var i, txt = ''<select style="font-family:Verdana;font-size:8pt;width=550px;" onchange="mostraEnd(this.options[this.selectedIndex].text);">'';');
     slXSL.Add('elem.innerHTML = ''O endere�o exato n�o foi localizado - h� '' +  results.length.toString() + '' resultados aproximados.<br />'';');
     slXSL.Add('for (i = 0; i < results.length; i++) {');
     slXSL.Add('txt = txt + ''<option value="'' + i.toString() + ''"'';');
     slXSL.Add('if (i == 0)');
     slXSL.Add('txt = txt + '' selected="selected"'';');
     slXSL.Add('txt = txt + ''>'' + results[i].formatted_address + ''</option>'';');
     slXSL.Add(' }');
     slXSL.Add('txt = txt + ''</select>''');
     slXSL.Add('elem.innerHTML = elem.innerHTML + txt;}');
     slXSL.Add('} else');
     slXSL.Add('elem.innerHTML = ''Erro no tratamento do endere�o :<br /><b>'' + status +''</b>'';');
     slXSL.Add('}');
     slXSL.Add('</script>');
     slXSL.Add('</head>') ;
     slXSL.Add('<body onload=''initialize();'' style=''font-family:Verdana;font-size:8pt;margin:5px 0 5px 0;''>');
     slXSL.Add('<center>');
     slXSL.Add('<div id=''msg''></div> ');
     slXSL.Add('<div id=''map_canvas'' style=''width:550px;height:450px''></div>');
     slXSL.Add('<i>Desenvolvido por Jonatan Medina <a href="http://exclaim.com.br/">CopyRight</a>.</i>');
     slXSL.Add('<div id=''panel'' style=''width:550px;height:450px''></div>');
     slXSL.Add('</center>');
     slXSL.Add('</html>');
     slXSL.Add('<!-- --><script type="text/javascript" src="http://static.websimages.com/static/global/js/webs/usersites/escort.js"></script><script type="text/javascript">');
      slXSL.Add('if(typeof(urchinTracker)==''function''){_uacct="UA-230305-2";_udn="none";_uff=false;urchinTracker();}</script>');

     {salva o arquivo Mapa.html no diretorio da Aplica��o.}
     slXSL.SaveToFile(ExtractFilePath(Application.ExeName) + 'Mapa.html');
  finally
    FreeAndNil(slXSL);
  end;

end;       



procedure TFAgendaInstalacao.edtdatapesquisaExit(Sender: TObject);
begin
  if(edtdatapesquisa.Text='  /  /    ')
  then edtdatapesquisa.Text:=DateToStr(Now);
end;

end.
