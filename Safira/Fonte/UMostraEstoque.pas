unit UMostraEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, Grids, Buttons, StdCtrls, ExtCtrls;

type
  TFMostraEstoque = class(TForm)
    panelpnl1: TPanel;
    lbnomeformulario: TLabel;
    lb1: TLabel;
    bt1: TSpeedButton;
    bt3: TSpeedButton;
    lbCodPedido: TLabel;
    STRG1: TStringGrid;
    ilProdutos: TImageList;
    panelpnl2: TPanel;
    img1: TImage;
    panelpnl6: TPanel;
    procedure STRG1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure STRG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    procedure ConfiguraStringgrid;
  end;

var
  FMostraEstoque: TFMostraEstoque;

implementation

uses UescolheImagemBotao, UessencialGlobal, UDIVERSO, UFERRAGEM, UVIDRO,
  UPERFILADO, UPERSIANA, UKITBOX;

{$R *.dfm}

procedure TFMostraEstoque.STRG1DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}    
begin
  if((ARow <> 0) and (ACol = 0))
  then begin
    if(STRG1.Cells[ACol,ARow] = '            X' ) or (STRG1.Cells[ACol,ARow] = 'X' )then
    begin
      Ilprodutos.Draw(STRG1.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 6); //check box marcado
    end
    else
    Ilprodutos.Draw(STRG1.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 5); //check box marcado       6
  end;
end;

procedure TFMostraEstoque.ConfiguraStringgrid;
begin
  with STRG1 do
  begin
    RowCount:=2;
    ColCount:=6;
    ColWidths[0]:= 50;
    ColWidths[1]:= 150;
    ColWidths[2]:= 500;
    ColWidths[3]:= 150;
    ColWidths[4]:= 150;
    ColWidths[5]:= 0;
    Cells[0,0]:='';
    Cells[1,0]:='Tipo';
    Cells[2,0]:='Material';
    Cells[3,0]:='Quantidade Vendida';
    Cells[4,0]:='Estoque';

  end;
end;


procedure TFMostraEstoque.FormShow(Sender: TObject);
begin
  FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');
end;

procedure TFMostraEstoque.STRG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if(Key=13) then
    begin
        if (STRG1.Cells[1,STRG1.row]='DIVERSO')
        Then Begin
           chamaFormulario(TFDIVERSO,self,STRG1.Cells[5,STRG1.row]);
        end;
        if (STRG1.Cells[1,STRG1.row]='FERRAGEM')
        Then Begin
           chamaFormulario(TFFERRAGEM,self,STRG1.Cells[5,STRG1.row]);
        end;
        if (STRG1.Cells[1,STRG1.row]='VIDRO')
        Then Begin
           chamaFormulario(TFVIDRO,self,STRG1.Cells[5,STRG1.row]);
        end;
        if (STRG1.Cells[1,STRG1.row]='PERFILADO')
        Then Begin
           chamaFormulario(TFPERFILADO,self,STRG1.Cells[5,STRG1.row]);
        end;
        if (STRG1.Cells[1,STRG1.row]='PERSIANA')
        Then Begin
           chamaFormulario(TFPERSIANA,self,STRG1.Cells[5,STRG1.row]);
        end;
        if (STRG1.Cells[1,STRG1.row]='KITBOX')
        Then Begin
           chamaFormulario(TFKITBOX,self,STRG1.Cells[5,STRG1.row]);
        end;
    end;



end;

end.
