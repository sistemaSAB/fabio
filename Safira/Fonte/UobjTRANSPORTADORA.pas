unit UobjTRANSPORTADORA;
Interface
Uses Ibquery,windows,Classes,Db,UessencialGlobal;

Type
   TObjTRANSPORTADORA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_NOME(parametro: string);
                Function Get_NOME: string;
                Procedure Submit_PLACA(parametro: string);
                Function Get_PLACA: string;
                Procedure Submit_Observacoes(parametro: String);
                Function Get_Observacoes: String;

                procedure submit_UF(uf:string);
                procedure submit_CPFCNPJ(cpfcnpj:String);
                procedure submit_IE(ie:string);
                procedure submit_Municipio(municipio:string);
                procedure submit_Endereco(endereco:string);
                procedure submit_UFPlaca(ufplaca:string);
                procedure submit_PesoMaximo(pesomaximo:string);
                procedure submit_peso(peso:string);

                function get_UF():string;
                function get_CPFCNPJ():string;
                function get_IE():string;
                function get_Municipio():string;
                function get_Endereco():string;
                function get_UFPlaca():string;
                function get_PesoMaximo():string;
                function get_peso():string;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               NOME:string;
               PLACA:string;
               Observacoes:String;

               UF:string;
               CPFCNPJ:string;
               IE:string;
               MUNICIPIO:string;
               ENDERECO:string;
               UFPLACA:string;
               PESOMAXIMO:string;
               PESO:string;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls
//USES IMPLEMENTATION
, UMenuRelatorios;


{ TTabTitulo }


Function  TObjTRANSPORTADORA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.NOME:=fieldbyname('NOME').asstring;
        Self.PLACA:=fieldbyname('PLACA').asstring;
        Self.Observacoes:=fieldbyname('Observacoes').asstring;

        Self.UF:=fieldbyname('UF').asstring;
        Self.CPFCNPJ:=fieldbyname('CPFCNPJ').asstring;
        Self.IE:=fieldbyname('IE').asstring;
        Self.MUNICIPIO:=fieldbyname('MUNICIPIO').asstring;
        Self.ENDERECO:=fieldbyname('ENDERECO').asstring;
        Self.UFPLACA:=fieldbyname('UFPLACA').asstring;
        Self.PESOMAXIMO:=fieldbyname('PESOMAXIMO').asstring;
        Self.PESO:=fieldbyname('PESO').asstring;

        result:=True;
     End;
end;


Procedure TObjTRANSPORTADORA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin

  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('NOME').asstring:=Self.NOME;
        ParamByName('PLACA').asstring:=Self.PLACA;
        ParamByName('Observacoes').asstring:=Self.Observacoes;

        ParamByName('UF').asstring:=Self.UF;
        ParamByName('CPFCNPJ').asstring:=Self.CPFCNPJ;
        ParamByName('IE').asstring:=Self.IE;
        ParamByName('MUNICIPIO').asstring:=Self.MUNICIPIO;
        ParamByName('ENDERECO').asstring:=Self.ENDERECO;
        ParamByName('UFPLACA').asstring:=Self.UFPLACA;
        ParamByName('PESOMAXIMO').asstring:= virgulaparaponto(Self.PESOMAXIMO);
        ParamByName('PESO').asstring:= virgulaparaponto(Self.PESO);

  End;
  
End;

//***********************************************************************

function TObjTRANSPORTADORA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjTRANSPORTADORA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        NOME:='';
        PLACA:='';
        Observacoes:='';
        UF:='';
        CPFCNPJ:='';
        IE:='';
        MUNICIPIO:='';
        ENDERECO:='';
        UFPLACA:='';
        PESOMAXIMO:='';
        PESO:='';

     End;
end;

Function TObjTRANSPORTADORA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      if (self.CPFCNPJ='') then
        Mensagem:=Mensagem+'/CPFCNPJ';

      if (self.UF='') then
        Mensagem:=Mensagem+'/UF';

      if (self.UFPLACA='') then
        Mensagem:=Mensagem+'/UF PLACA';

      if(Self.IE='') then
        Mensagem:=Mensagem+'IE';

      if (self.NOME='') then
        Mensagem:=Mensagem+'NOME';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjTRANSPORTADORA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjTRANSPORTADORA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjTRANSPORTADORA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjTRANSPORTADORA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjTRANSPORTADORA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro TRANSPORTADORA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,NOME,PLACA,Observacoes,UF,CPFCNPJ,IE,MUNICIPIO,ENDERECO,UFPLACA,PESOMAXIMO,PESO');
           SQL.ADD(' from  TABTRANSPORTADORA');
           SQL.ADD(' WHERE codigo='+parametro);


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjTRANSPORTADORA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjTRANSPORTADORA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjTRANSPORTADORA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABTRANSPORTADORA(CODIGO,NOME,PLACA,Observacoes,UF,CPFCNPJ,IE,MUNICIPIO,ENDERECO,UFPLACA,PESOMAXIMO,PESO');
                InsertSQL.add(' )');
                InsertSQL.add('values (:CODIGO,:NOME,:PLACA,:Observacoes,:UF,:CPFCNPJ,:IE,:MUNICIPIO,:ENDERECO,:UFPLACA,:PESOMAXIMO,:PESO)');


                ModifySQL.clear;
                ModifySQL.add('Update TABTRANSPORTADORA set CODIGO=:CODIGO,NOME=:NOME');
                ModifySQL.add(',PLACA=:PLACA,Observacoes=:Observacoes,UF=:UF,CPFCNPJ=:CPFCNPJ,IE=:IE,MUNICIPIO=:MUNICIPIO,ENDERECO=:ENDERECO,UFPLACA=:UFPLACA,PESOMAXIMO=:PESOMAXIMO,PESO=:PESO');
                ModifySQL.add('where codigo=:codigo');


                DeleteSQL.clear;
                DeleteSql.add('Delete from TABTRANSPORTADORA where codigo=:codigo ');


                Self.status          :=dsInactive;
        End;

end;
procedure TObjTRANSPORTADORA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjTRANSPORTADORA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabTRANSPORTADORA');
     Result:=Self.ParametroPesquisa;
end;

function TObjTRANSPORTADORA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de TRANSPORTADORA ';
end;


function TObjTRANSPORTADORA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENTRANSPORTADORA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENTRANSPORTADORA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjTRANSPORTADORA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjTRANSPORTADORA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjTRANSPORTADORA.RetornaCampoNome: string;
begin
      result:='Nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjTransportadora.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjTransportadora.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjTransportadora.Submit_NOME(parametro: string);
begin
        Self.NOME:=Parametro;
end;
function TObjTransportadora.Get_NOME: string;
begin
        Result:=Self.NOME;
end;
procedure TObjTransportadora.Submit_PLACA(parametro: string);
begin
        Self.PLACA:=Parametro;
end;
function TObjTransportadora.Get_PLACA: string;
begin
        Result:=Self.PLACA;
end;
procedure TObjTransportadora.Submit_Observacoes(parametro: String);
begin
        Self.Observacoes:=Parametro;
end;
function TObjTransportadora.Get_Observacoes: String;
begin
        Result:=Self.Observacoes;
end;



//CODIFICA EXITONKEYDOWN
procedure TObjTRANSPORTADORA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJTRANSPORTADORA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:Begin
            End;
          End;
     end;

end;



function TObjTRANSPORTADORA.get_CPFCNPJ: string;
begin

  result:=self.CPFCNPJ;

end;

function TObjTRANSPORTADORA.get_Endereco: string;
begin

  result:=self.ENDERECO;

end;

function TObjTRANSPORTADORA.get_IE: string;
begin

  result:=self.IE;

end;

function TObjTRANSPORTADORA.get_Municipio: string;
begin

  result:=self.MUNICIPIO;

end;

function TObjTRANSPORTADORA.get_peso: string;
begin

  result:=Self.PESO;

end;

function TObjTRANSPORTADORA.get_PesoMaximo: string;
begin

  result:=self.PESOMAXIMO;

end;

function TObjTRANSPORTADORA.get_UF: string;
begin

  result:=self.UF;

end;

function TObjTRANSPORTADORA.get_UFPlaca: string;
begin

  result:=self.UFPLACA

end;

procedure TObjTRANSPORTADORA.submit_CPFCNPJ(cpfcnpj: String);
begin

  self.CPFCNPJ:=cpfcnpj;

end;

procedure TObjTRANSPORTADORA.submit_Endereco(endereco: string);
begin

  self.ENDERECO:=endereco;

end;

procedure TObjTRANSPORTADORA.submit_IE(ie: string);
begin

  self.IE:=ie;

end;

procedure TObjTRANSPORTADORA.submit_Municipio(municipio: string);
begin

  self.MUNICIPIO:=municipio;

end;

procedure TObjTRANSPORTADORA.submit_peso(peso: string);
begin

  self.PESO:=peso;

end;

procedure TObjTRANSPORTADORA.submit_PesoMaximo(pesomaximo: string);
begin

  self.PESOMAXIMO:=pesomaximo;

end;

procedure TObjTRANSPORTADORA.submit_UF(uf: string);
begin

  self.UF:=uf;

end;

procedure TObjTRANSPORTADORA.submit_UFPlaca(ufplaca: string);
begin

  self.UFPLACA:=ufplaca;

end;

end.



