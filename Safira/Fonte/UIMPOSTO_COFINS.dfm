object FIMPOSTO_COFINS: TFIMPOSTO_COFINS
  Left = 513
  Top = 245
  Width = 829
  Height = 474
  Caption = 'Cadastro de Impostos - COFINS - EXCLAIM TECNOLOGIA'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbLbST: TLabel
    Left = 9
    Top = 72
    Width = 108
    Height = 13
    Caption = 'Situa'#231#227'o Tribut'#225'ria'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbNomeST_COFINS: TLabel
    Left = 132
    Top = 88
    Width = 108
    Height = 13
    Caption = 'Situa'#231#227'o Tribut'#225'ria'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbTIPOCALCULO: TLabel
    Left = 9
    Top = 110
    Width = 88
    Height = 13
    Caption = 'Tipo de C'#225'lculo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbALIQUOTAPERCENTUAL: TLabel
    Left = 177
    Top = 110
    Width = 110
    Height = 13
    Caption = 'Al'#237'quota Percentual'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbALIQUOTAVALOR: TLabel
    Left = 372
    Top = 110
    Width = 102
    Height = 13
    Caption = 'Al'#237'quota em Valor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb2: TLabel
    Left = 9
    Top = 155
    Width = 160
    Height = 13
    Caption = 'F'#243'rmula da Base de C'#225'lculo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb3: TLabel
    Left = 9
    Top = 191
    Width = 149
    Height = 13
    Caption = 'F'#243'rmula Valor do Imposto'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbTIPOCALCULO_ST: TLabel
    Left = 9
    Top = 231
    Width = 117
    Height = 13
    Caption = 'Tipo de C'#225'lculo (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbALIQUOTAVALOR_ST: TLabel
    Left = 348
    Top = 229
    Width = 131
    Height = 13
    Caption = 'Al'#237'quota em Valor (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbALIQUOTAPERCENTUAL_ST: TLabel
    Left = 153
    Top = 229
    Width = 139
    Height = 13
    Caption = 'Al'#237'quota Percentual (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb4: TLabel
    Left = 9
    Top = 277
    Width = 189
    Height = 13
    Caption = 'F'#243'rmula da Base de C'#225'lculo (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb5: TLabel
    Left = 9
    Top = 317
    Width = 178
    Height = 13
    Caption = 'F'#243'rmula Valor do Imposto (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object pnl1: TPanel
    Left = 0
    Top = 388
    Width = 813
    Height = 48
    Align = alBottom
    Color = clMedGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clSilver
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      813
      48)
    object imgrodape: TImage
      Left = 1
      Top = 0
      Width = 811
      Height = 47
      Align = alBottom
      Stretch = True
    end
    object lb9: TLabel
      Left = 452
      Top = 11
      Width = 256
      Height = 20
      Anchors = [akTop, akRight]
      Caption = 'Existem X COFINS cadastrados'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 813
    Height = 54
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 1
    DesignSize = (
      813
      54)
    object lbnomeformulario: TLabel
      Left = 559
      Top = 3
      Width = 104
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Cadastro de'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbcodigo: TLabel
      Left = 674
      Top = 9
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb7: TLabel
      Left = 567
      Top = 25
      Width = 73
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'COFINS'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
      Spacing = 0
    end
  end
  object edtST_COFINS: TEdit
    Left = 9
    Top = 88
    Width = 50
    Height = 19
    Color = 6073854
    MaxLength = 9
    TabOrder = 2
    OnDblClick = edtST_COFINSDblClick
    OnExit = edtST_COFINSExit
    OnKeyDown = edtST_COFINSKeyDown
  end
  object cbbComboTipoCalculo_COFINS: TComboBox
    Left = 9
    Top = 126
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 3
    Text = '  '
    Items.Strings = (
      'Percentual'
      'Valor')
  end
  object edtALIQUOTAPERCENTUAL_COFINS: TEdit
    Left = 177
    Top = 126
    Width = 50
    Height = 19
    MaxLength = 9
    TabOrder = 4
  end
  object edtALIQUOTAVALOR_COFINS: TEdit
    Left = 372
    Top = 126
    Width = 50
    Height = 19
    MaxLength = 9
    TabOrder = 5
  end
  object edtFORMULABASECALCULO_COFINS: TEdit
    Left = 9
    Top = 169
    Width = 480
    Height = 19
    TabOrder = 6
  end
  object edtFORMULA_VALOR_IMPOSTO_COFINS: TEdit
    Left = 9
    Top = 206
    Width = 480
    Height = 19
    TabOrder = 7
  end
  object cbbTIPOCALCULO_ST_COFINS: TComboBox
    Left = 9
    Top = 247
    Width = 50
    Height = 21
    ItemHeight = 13
    TabOrder = 8
    Text = '  '
    Items.Strings = (
      'Percentual'
      'Valor')
  end
  object edtALIQUOTAVALOR_ST_COFINS: TEdit
    Left = 348
    Top = 245
    Width = 50
    Height = 19
    MaxLength = 9
    TabOrder = 9
  end
  object edtALIQUOTAPERCENTUAL_ST_COFINS: TEdit
    Left = 153
    Top = 245
    Width = 50
    Height = 19
    MaxLength = 9
    TabOrder = 10
  end
  object edtFORMULABASECALCULO_ST_COFINS: TEdit
    Left = 9
    Top = 291
    Width = 480
    Height = 19
    TabOrder = 11
  end
  object edtFORMULA_VALOR_IMPOSTO_ST_COFINS: TEdit
    Left = 9
    Top = 335
    Width = 480
    Height = 19
    TabOrder = 12
  end
end
