unit UIE_ST_ESTADO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjIE_ST_ESTADO,
  jpeg;

type
  TFIE_ST_ESTADO = class(TForm)
    edtESTADO: TEdit;
    lbLbESTADO: TLabel;
    edtIE: TEdit;
    lbLbIE: TLabel;
    edtCODIGO: TEdit;
    lbLbCODIGO: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lb1: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btgravar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjIE_ST_ESTADO:TObjIE_ST_ESTADO;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FIE_ST_ESTADO: TFIE_ST_ESTADO;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFIE_ST_ESTADO.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjIE_ST_ESTADO do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_IE(edtIE.text);
        Submit_ESTADO(edtESTADO.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFIE_ST_ESTADO.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjIE_ST_ESTADO do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtIE.text:=Get_IE;
        EdtESTADO.text:=Get_ESTADO;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFIE_ST_ESTADO.TabelaParaControles: Boolean;
begin
     If (Self.ObjIE_ST_ESTADO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFIE_ST_ESTADO.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjIE_ST_ESTADO:=TObjIE_ST_ESTADO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
    
end;

procedure TFIE_ST_ESTADO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjIE_ST_ESTADO=Nil)
     Then exit;

If (Self.ObjIE_ST_ESTADO.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjIE_ST_ESTADO.free;
end;

procedure TFIE_ST_ESTADO.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFIE_ST_ESTADO.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjIE_ST_ESTADO.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjIE_ST_ESTADO.status:=dsInsert;
     edtIE.setfocus;
     btalterar.visible:=false;
     btrelatorios.visible:=false;
     btsair.Visible:=false;
     btopcoes.visible:=false;
     btexcluir.Visible:=false;
     btnovo.Visible:=false;
     btopcoes.visible:=false;


end;


procedure TFIE_ST_ESTADO.btalterarClick(Sender: TObject);
begin
    If (Self.ObjIE_ST_ESTADO.Status=dsinactive) and (EdtCodigo.text<>'')
    Then
    Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjIE_ST_ESTADO.Status:=dsEdit;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtIE.setfocus;
                btalterar.visible:=false;
                btrelatorios.visible:=false;
                btsair.Visible:=false;
                btopcoes.visible:=false;
                btexcluir.Visible:=false;
                btnovo.Visible:=false;
                btopcoes.visible:=false;

                
    End;


end;

procedure TFIE_ST_ESTADO.btgravarClick(Sender: TObject);
begin

     If Self.ObjIE_ST_ESTADO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjIE_ST_ESTADO.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjIE_ST_ESTADO.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
      btopcoes.visible:=true;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFIE_ST_ESTADO.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjIE_ST_ESTADO.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjIE_ST_ESTADO.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjIE_ST_ESTADO.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFIE_ST_ESTADO.btcancelarClick(Sender: TObject);
begin
     Self.ObjIE_ST_ESTADO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
      btopcoes.visible:=true;

end;

procedure TFIE_ST_ESTADO.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFIE_ST_ESTADO.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjIE_ST_ESTADO.Get_pesquisa,Self.ObjIE_ST_ESTADO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjIE_ST_ESTADO.status<>dsinactive
                                  then exit;

                                  If (Self.ObjIE_ST_ESTADO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjIE_ST_ESTADO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFIE_ST_ESTADO.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFIE_ST_ESTADO.FormShow(Sender: TObject);
begin
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');

end;
//CODIFICA ONKEYDOWN E ONEXIT


end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjIE_ST_ESTADO.OBJETO.Get_Pesquisa,Self.ObjIE_ST_ESTADO.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjIE_ST_ESTADO.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjIE_ST_ESTADO.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjIE_ST_ESTADO.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
