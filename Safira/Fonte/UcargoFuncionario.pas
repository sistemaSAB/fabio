unit UcargoFuncionario;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjCargoFuncionario;

type
  TFcargoFuncionario = class(TForm)
    edtnome: TEdit;
    lb1: TLabel;
    imgrodape: TImage;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb2: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FcargoFuncionario: TFcargoFuncionario;
  ObjCargoFuncionario:TObjCargoFuncionario;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, Uprincipal;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFcargoFuncionario.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjCargoFuncionario do
    Begin
         Submit_CODIGO          ( lbCodigo.Caption);
         Submit_Nome(edtnome.text);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFcargoFuncionario.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjCargoFuncionario do
     Begin
        lbCodigo.Caption          :=Get_CODIGO          ;
        edtnome.text            :=Get_nome            ;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFcargoFuncionario.TabelaParaControles: Boolean;
begin
     If (ObjCargoFuncionario.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFcargoFuncionario.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjCargoFuncionario=Nil)
     Then exit;

    If (ObjCargoFuncionario.status<>dsinactive)
    Then
    Begin
              Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
              abort;
              exit;
    End;

    ObjCargoFuncionario.free;
    self.Tag:=0;
end;

procedure TFcargoFuncionario.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFcargoFuncionario.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     
     //edtcodigo.text:='0';
     lbCodigo.Caption:=ObjCargoFuncionario.Get_novocodigo;
     btnovo.Visible:=false;
     btalterar.Visible:=False;
     btexcluir.Visible:=false;
     btrelatorios.Visible:=false;
     btopcoes.Visible:=false;
     btsair.Visible:=False;
     btpesquisar.visible:=False;

     

     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjCargoFuncionario.status:=dsInsert;

     Edtnome.setfocus;

end;


procedure TFcargoFuncionario.btalterarClick(Sender: TObject);
begin
    If (ObjCargoFuncionario.Status=dsinactive) and (lbCodigo.Caption<>'')
    Then
    Begin
                habilita_campos(Self);

                ObjCargoFuncionario.Status:=dsEdit;

                edtnome.setfocus;
               
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btnovo.Visible:=false;
                 btalterar.Visible:=False;
                 btexcluir.Visible:=false;
                 btrelatorios.Visible:=false;
                 btopcoes.Visible:=false;
                 btsair.Visible:=False;
                 btpesquisar.visible:=False;
    End;


end;

procedure TFcargoFuncionario.btgravarClick(Sender: TObject);
begin

     If ObjCargoFuncionario.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjCargoFuncionario.salvar(true)=False)
     Then exit;

     habilita_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFcargoFuncionario.btexcluirClick(Sender: TObject);
begin
     If (ObjCargoFuncionario.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (ObjCargoFuncionario.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjCargoFuncionario.exclui(lbCodigo.Caption,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFcargoFuncionario.btcancelarClick(Sender: TObject);
begin
     ObjCargoFuncionario.cancelar;

     limpaedit(Self);
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;
     lbCodigo.caption:='';

end;

procedure TFcargoFuncionario.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFcargoFuncionario.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjCargoFuncionario.Get_pesquisa,ObjCargoFuncionario.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjCargoFuncionario.status<>dsinactive
                                  then exit;

                                  If (ObjCargoFuncionario.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjCargoFuncionario.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFcargoFuncionario.FormShow(Sender: TObject);
begin
          limpaedit(Self);
          desabilita_campos(Self);
          Try
             ObjCargoFuncionario:=TObjCargoFuncionario.create;
          Except
                 Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
                Self.close;
          End;
          FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
          FescolheImagemBotao.PegaFiguraImagem(imgrodape,'RODAPE');
          lbCodigo.caption:='';
         if(Tag<>0)
         then begin
               if(ObjCargoFuncionario.LocalizaCodigo(IntToStr(Tag))=True)then
               begin
                    ObjCargoFuncionario.TabelaparaObjeto;
                    self.ObjetoParaControles;
               end;

         end;
end;

procedure TFcargoFuncionario.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
        if (Key = VK_F1) then
      Fprincipal.chamaPesquisaMenu();
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjCargoFuncionario.OBJETO.Get_Pesquisa,ObjCargoFuncionario.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(OBJCargoFuncionario.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If OBJCargoFuncionario.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(OBJCargoFuncionario.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
