unit UobjPEDIDOPROJETOPEDIDOCOMPRA;
Interface
Uses forms,rdprint,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal
,UOBJPEDIDOCOMPRA
,UOBJPEDIDO_PROJ,
ComObj,UrelPedidoCompraPadrao,QuickRpt,ExtCtrls,Graphics,UrelPedidoCompraMater;
//USES_INTERFACE




Type
   TObjPEDIDOPROJETOPEDIDOCOMPRA=class

          Public
               ObjDatasource                               :TDataSource;
               Status                                      :TDataSetState;
               SqlInicial                                  :String[200];
               PedidoCompra:TOBJPEDIDOCOMPRA;
               PedidoProjeto:TOBJPEDIDO_PROJ;
               utilizaCusto:Boolean;

//CODIFICA VARIAVEIS PUBLICAS



                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaPedidoProjeto(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);
                procedure Opcoes(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                procedure EdtPedidoCompraExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtPedidoCompraKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtPedidoProjetoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel;Ppedido:string);overload;
                Procedure ResgataPedidoProjetos(Pcodigo:string);
                procedure EdtPedidoProjeto_cadastrados_KeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);

                procedure CalculaFormulasCorte(FormulaAltura:string;FormulaPosicao:string;Altura:Currency;Largura:Currency;var AlturaCorte:string;Var PosicaoCorte:string);
                //CODIFICA DECLARA GETSESUBMITS
                procedure GerarPedidoCompra(PPedidoCompra:string; mostraCusto:boolean);


         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               PedidoCompraRel:TTQRPedidoCompraPadrao;
               PedidoCompraMatAvulsoRel:TTQRUrelPedidoCompraMater;


               CODIGO:string;
               PedidodeCompraExterno:string;
               PedidoProjetoExterno:string;
                //CODIFICA VARIAVEIS PRIVADAS



               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Procedure ImprimePedido_por_Data;

                procedure BandaDetailBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
                procedure BandaDetailBeforePrint2(Sender: TQRCustomBand; var PrintBand: Boolean);
                procedure PreencheOrcamentoRelPedidoCompra();
                procedure PreencheOrcamentoRelPedidoCompra2();
                procedure Preencheimagens;
                function  retornaPalavrasAntesSimboloLocal(str: string): string;
                function  RetornaOperadorMatematico(str:string):string;
                function  RetornaValor(Str:string):string;
                function  RetornaUnidadeMedida(str:string):string;


   End;


implementation
uses Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UPEDIDOCOMPRA, UReltxtRDPRINT, UMostraBarraProgresso,
  UobjVIDROCOR, UopcaoRel, UobjCLIENTE, UobjParametros;





Function  TObjPEDIDOPROJETOPEDIDOCOMPRA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('PedidoCompra').asstring<>'')
        Then Begin
                 If (Self.PedidoCompra.LocalizaCodigo(FieldByName('PedidoCompra').asstring)=False)
                 Then Begin
                          Messagedlg('Pedido de Compra N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PedidoCompra.TabelaparaObjeto;
        End;
        If(FieldByName('PedidoProjeto').asstring<>'')
        Then Begin
                 If (Self.PedidoProjeto.LocalizaCodigo(FieldByName('PedidoProjeto').asstring)=False)
                 Then Begin
                          Messagedlg('Pedido Projeto] N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PedidoProjeto.TabelaparaObjeto;
        End;

        if(FieldByName('pedido').AsString<>'') then
        begin
              if(self.PedidoProjeto.Pedido.LocalizaCodigo(fieldbyname('pedido').AsString)=False)
              then begin
                       Messagedlg('Pedido N�o encontrado(a)!',mterror,[mbok],0);
                       Self.ZerarTabela;
                       result:=False;
                       exit;

              end
              else self.PedidoProjeto.Pedido.TabelaparaObjeto;

        end;
//CODIFICA TABELAPARAOBJETO



        result:=True;
     End;
end;


Procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('PedidoCompra').asstring:=Self.PedidoCompra.GET_CODIGO;
        ParamByName('PedidoProjeto').asstring:=Self.PedidoProjeto.GET_CODIGO;
        ParamByName('pedido').AsString:=self.PedidoProjeto.Pedido.Get_Codigo;
//CODIFICA OBJETOPARATABELA



  End;
End;

//***********************************************************************

function TObjPEDIDOPROJETOPEDIDOCOMPRA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin

              if (Self.localizaPedidoProjeto(Self.PedidoProjeto.get_codigo)=true)
              then Begin
                        mensagemerro('Esse pedido/projeto j� foi inserido no pedido de compra n� '+Self.Objquery.fieldbyname('pedidocompra').asstring);
                        exit;
              End;


              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        PedidoCompra.ZerarTabela;
        PedidoProjeto.ZerarTabela;
        PedidoProjeto.Pedido.ZerarTabela;
//CODIFICA ZERARTABELA



     End;
end;

Function TObjPEDIDOPROJETOPEDIDOCOMPRA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (PedidoCompra.get_Codigo='')
      Then Mensagem:=mensagem+'/Pedido de Compra';
      If (PedidoProjeto.get_Codigo='')
      Then Mensagem:=mensagem+'/Pedido Projeto]';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPEDIDOPROJETOPEDIDOCOMPRA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.PedidoCompra.LocalizaCodigo(Self.PedidoCompra.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Pedido de Compra n�o Encontrado!';
      If (Self.PedidoProjeto.LocalizaCodigo(Self.PedidoProjeto.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Pedido Projeto] n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjPEDIDOPROJETOPEDIDOCOMPRA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.PedidoCompra.Get_Codigo<>'')
        Then Strtoint(Self.PedidoCompra.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Pedido de Compra';
     End;
     try
        If (Self.PedidoProjeto.Get_Codigo<>'')
        Then Strtoint(Self.PedidoProjeto.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Pedido Projeto]';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPEDIDOPROJETOPEDIDOCOMPRA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPEDIDOPROJETOPEDIDOCOMPRA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPEDIDOPROJETOPEDIDOCOMPRA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PEDIDOPROJETOPEDIDOCOMPRA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,pedido,PedidoCompra,PedidoProjeto');
           SQL.ADD(' from  TabPedidoProjetoPedidoCompra');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


function TObjPEDIDOPROJETOPEDIDOCOMPRA.LocalizaPedidoProjeto(parametro: string): boolean;//ok
begin
       result:=False;
       
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro de localiza��o vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,pedido,PedidoCompra,PedidoProjeto');
           SQL.ADD(' from  TabPedidoProjetoPedidoCompra');
           SQL.ADD(' WHERE pedidoprojeto='+parametro);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPEDIDOPROJETOPEDIDOCOMPRA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPEDIDOPROJETOPEDIDOCOMPRA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjqueryPesquisa;


        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.PedidoCompra:=TOBJPEDIDOCOMPRA.create;
        Self.PedidoProjeto:=TOBJPEDIDO_PROJ.create;


        PedidoCompraMatAvulsoRel:=TTQRUrelPedidoCompraMater.create(nil);

//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabPedidoProjetoPedidoCompra(CODIGO,PedidoCompra');
                InsertSQL.add(' ,PedidoProjeto,pedido)');
                InsertSQL.add('values (:CODIGO,:PedidoCompra,:PedidoProjeto,:pedido)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabPedidoProjetoPedidoCompra set CODIGO=:CODIGO');
                ModifySQL.add(',PedidoCompra=:PedidoCompra,PedidoProjeto=:PedidoProjeto,pedido=:pedido');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabPedidoProjetoPedidoCompra where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;
        utilizaCusto := true;

end;
procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPEDIDOPROJETOPEDIDOCOMPRA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPEDIDOPROJETOPEDIDOCOMPRA');
     Result:=Self.ParametroPesquisa;
end;

function TObjPEDIDOPROJETOPEDIDOCOMPRA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de PEDIDOPROJETOPEDIDOCOMPRA ';
end;


function TObjPEDIDOPROJETOPEDIDOCOMPRA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPEDIDOPROJETOPEDIDOCOMPRA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENPEDIDOPROJETOPEDIDOCOMPRA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPEDIDOPROJETOPEDIDOCOMPRA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.objdatasource);
    Freeandnil(Self.Objquerypesquisa);
    Freeandnil(Self.ParametroPesquisa);
    //FreeAndNil(PedidoCompraRel);
    FreeAndNil(PedidoCompraMatAvulsoRel);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.PedidoCompra.FREE;
    Self.PedidoProjeto.FREE;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPEDIDOPROJETOPEDIDOCOMPRA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPEDIDOPROJETOPEDIDOCOMPRA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPedidoProjetoPedidoCompra.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjPedidoProjetoPedidoCompra.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
//CODIFICA GETSESUBMITS


procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.EdtPedidoCompraExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PedidoCompra.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PedidoCompra.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.PedidoCompra.GET_NOME;
End;
procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.EdtPedidoCompraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPEDIDOCOMPRA:TFPEDIDOCOMPRA;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPEDIDOCOMPRA:=TFPEDIDOCOMPRA.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PedidoCompra.Get_Pesquisa,Self.PedidoCompra.Get_TituloPesquisa,FPedidoCompra)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoCompra.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.PedidoCompra.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoCompra.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FPEDIDOCOMPRA);
     End;
end;
procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.EdtPedidoProjetoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PedidoProjeto.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PedidoProjeto.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.PedidoProjeto.GET_NOME;
End;


procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
begin
     Self.EdtPedidoProjetoKeyDown(Sender,Key,Shift,LABELNOME,'');
End;


procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel;Ppedido:string);
var
   FpesquisaLocal:Tfpesquisa;
//   FPEDIDO_PROJ:TFPEDIDO_PROJ;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PedidoProjeto.Get_Pesquisaview(ppedido),Self.PedidoProjeto.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoProjeto.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.PedidoProjeto.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoProjeto.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.EdtPedidoProjeto_cadastrados_KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            Self.ParametroPesquisa.clear;
            Self.ParametroPesquisa.add('Select * from ViewPedidoProjetoPedidoCompra');

            If (FpesquisaLocal.PreparaPesquisa(Self.ParametroPesquisa,'Pesquisa de Pedidos/Projetos por Pedido de Compra',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('pedidoprojeto').asstring;
                         End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPEDIDOPROJETOPEDIDOCOMPRA';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Pedidos por Data');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
                   Self.ImprimePedido_por_Data;
            End;
          End;
     end;

end;



procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.ResgataPedidoProjetos(
  Pcodigo: string);
begin
     if(Pcodigo='')
     then Exit;
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          Sql.add('Select codigo,pedido,pedidocompra,pedidoprojeto,datac,userc,datam,userm from TabPedidoprojetoPedidoCompra where pedidocompra='+pcodigo);
          open;
     End;
end;

procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.ImprimePedido_por_Data;
var
pdatainicial,pdatafinal:Tdate;
pfornecedor:string;
ObjVidroCor:TobjVidroCor;
begin
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;

          LbGrupo01.caption:='Data Inicial';
          LbGrupo02.caption:='Data Final';
          LbGrupo03.caption:='Fornecedor';
          
          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;
          edtgrupo03.OnKeyDown:=Self.pedidocompra.EdtFornecedorKeyDown;

          showmodal;

          if (tag=0)
          Then exit;

          if (Validadata(1,pdatainicial,false)=False)
          Then exit;

          if (Validadata(2,pdatafinal,false)=False)
          Then exit;

          pfornecedor:='';

          Try
             if (edtgrupo03.text<>'')
             Then Begin
                       strtoint(edtgrupo03.text);
                       pfornecedor:=edtgrupo03.text;
                       if (Self.PedidoCompra.Fornecedor.LocalizaCodigo(pfornecedor)=False)
                       Then Begin
                                 Mensagemaviso('Fornecedor n�o localizado');
                                 exit;
                       End;
             End;
          Except
                mensagemerro('C�digo inv�lido para o Fornecedor');
                exit;
          End;
     End;

     Try
        ObjVidroCor:=TobjVidroCor.create;
     Except
           MensagemErro('Erro na tentativa de criar o Objeto de Vidro por Cor');
           exit;
     End;

Try

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select tabpedidocompra.codigo,tabpedidocompra.codigomanual_1,tabpedidocompra.codigomanual_2,tabpedidocompra.codigomanual,tabpedido.cliente,tabvidro_pp.vidrocor,');
          sql.add('sum(tabvidro_pp.quantidade) as soma,TPPPC.pedido');
          sql.add('from tabpedidoprojetopedidocompra TPPPC');
          sql.add('join tabpedidocompra on TPPPC.pedidocompra=Tabpedidocompra.codigo');
          sql.add('join tabpedido_proj on TPPPC.pedidoprojeto=tabpedido_proj.codigo');
          sql.add('join tabpedido on tabpedido_proj.pedido=tabpedido.codigo');
          sql.add('join tabvidro_pp on tabvidro_pp.pedidoprojeto=tabpedido_proj.codigo');
          sql.add('where Tabpedidocompra.data>='+#39+formatdatetime('mm/dd/yyyy',pdatainicial)+#39);
          sql.add('and tabpedidocompra.data<='+#39+formatdatetime('mm/dd/yyyy',pdatafinal)+#39);

          if (Pfornecedor<>'')
          Then sql.add('and Tabpedidocompra.fornecedor='+pfornecedor);

          sql.add('group by tabpedidocompra.codigo,tabpedidocompra.codigomanual_1,tabpedidocompra.codigomanual_2,tabpedidocompra.codigomanual,tabpedido.cliente,tabvidro_pp.vidrocor,TPPPC.pedido');
          sql.add('order by tabpedidocompra.codigomanual_1,tabpedidocompra.codigomanual_2');
          open;
          last;
          if (recordcount=0)
          Then Begin
                    Mensagemaviso('Nenhuma informa��o foi selecionada');
                    exit;
          End;

          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.Lbmensagem.caption:='Gerando Relat�rio';
          first;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               RDprint.TamanhoQteColunas:=130;
               RDprint.FonteTamanhoPadrao:=S17cpp;
               RDprint.Abrir;
               LinhaLocal:=3;
               if (RDprint.Setup=False)
               Then Begin
                         RDprint.Fechar;
                         exit;
               End;
               RDprint.ImpC(linhalocal,65,'PEDIDOS DE COMPRA POR INTERVALO DE DATA - '+DATETOSTR(pdatainicial)+' a '+datetostr(Pdatafinal),[negrito]);
               IncrementaLinha(2);

               if (pfornecedor<>'')
               Then Begin
                         RDprint.Impf(linhalocal,1,'Fornecedor: '+Pfornecedor+'-'+Self.PedidoCompra.Fornecedor.Get_RazaoSocial,[negrito]);
                         IncrementaLinha(2);
               End;
               RDprint.Impf(linhalocal,1,completapalavra('Pedido',8,' ')+' '+
                                         completapalavra('Cliente',30,' ')+' '+
                                         completapalavra('Cidade',15,' ')+' '+
                                         completapalavra('Data',5,' ')+' '+
                                         completapalavra('Vidro',22,' ')+' '+
                                         completapalavra('Cor',10,' ')+' '+
                                         CompletaPalavra_a_Esquerda('M�',10,' ')+' '+
                                         completapalavra('Dt Chg',6,' ')+' '+
                                         completapalavra('Coloca��o',10,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;

               While not (Self.Objquery.eof) do
               Begin
                    FMostraBarraProgresso.IncrementaBarra1(1);
                    FMostraBarraProgresso.Show;
                    application.processmessages;

                    Self.PedidoCompra.LocalizaCodigo(fieldbyname('codigo').asstring);
                    Self.PedidoCompra.TabelaparaObjeto;

                    Self.PedidoProjeto.Pedido.Cliente.LocalizaCodigo(fieldbyname('cliente').asstring);
                    Self.PedidoProjeto.Pedido.Cliente.TabelaparaObjeto;

                    ObjvidroCor.localizacodigo(Fieldbyname('vidrocor').asstring);
                    ObjvidroCor.tabelaparaobjeto;

                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,completapalavra(fieldbyname('pedido').AsString,8,' '){completapalavra(Self.PedidoCompra.Get_CodigoManual,8,' ')}+' '+
                                             completapalavra(Self.PedidoProjeto.Pedido.Cliente.Get_Nome,30,' ')+' '+
                                             completapalavra(Self.PedidoProjeto.Pedido.Cliente.Get_Cidade,15,' ')+' '+
                                             completapalavra(formatdatetime('dd/mm',strtodate(Self.PedidoCompra.Get_Data)),5,' ')+' '+
                                             completapalavra(ObjVidroCor.Vidro.Get_Descricao,22,' ')+' '+
                                             completapalavra(ObjVidroCor.Cor.Get_Descricao,10,' ')+' '+
                                             CompletaPalavra_a_Esquerda(floattostr(fieldbyname('soma').ascurrency),10,' ')+' '+
                                             completapalavra(' ',6,' ')+' '+
                                             completapalavra(' ',10,' '));
                    IncrementaLinha(1);
                    Self.Objquery.Next;
               End;
               DesenhaLinha;
               FMostraBarraProgresso.Close;
               RDprint.Fechar;
          End;
     End;
     
Finally
       ObjVidroCor.free;
End;


end;

procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.Opcoes(Pcodigo:string);
begin
       With FOpcaorel do
       Begin
            RgOpcoes.Items.clear;
            RgOpcoes.Items.add('Gerar Pedido de Compra');


            showmodal;
            if (tag=0)
            Then exit;
            Case RgOpcoes.ItemIndex of
                 0:GerarPedidoCompra(Pcodigo, true);
            End;
       End;
end;

procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.GerarPedidoCompra(PpedidoCompra:string; mostraCusto:boolean);
var
   QueryPesquisa:TIBQuery;
   QueryAux:TIBQuery;
   PLocalLogoTipo: string;
   Espessura:string;
begin
     Try
          QueryPesquisa:=TIBQuery.Create(nil);
          QueryPesquisa.Database:=FDataModulo.IBDatabase;

          QueryAux:=TIBQuery.Create(nil);
          QueryAux.Database:=FDataModulo.IBDatabase;
     Except

     End;

     try


          PedidodeCompraExterno:=PPedidoCompra;
          utilizaCusto := mostraCusto;

          with QueryPesquisa do
          begin
                  //Gerando pedido compra de pedidos projetos
                  Close;
                  sql.Clear;
                  sql.add('select distinct (pp.codigo),F.Fantasia,PC.observacao,PC.codigo as CodigoPedidoCompra,PC.data, cor.descricao, PPPC.pedido ');
                  sql.Add('from tabpedidoprojetopedidocompra PPPC');
                  sql.Add('join tabpedidocompra PC on PPPC.pedidocompra=PC.codigo');
                  SQL.Add('join tabfornecedor F on F.codigo=PC.fornecedor');
                  SQL.Add('join tabpedido_proj PP on PPPC.pedidoprojeto=PP.codigo');
                  SQL.Add('left join tabferragem_pp FPP on PP.codigo=FPP.pedidoprojeto') ;
                  SQL.Add('left join tabpersiana_PP PPP on PPP.pedidoprojeto=PP.codigo');
                  SQL.Add('left join tabperfilado_PP PERPP on PERPP.pedidoprojeto=PP.codigo');
                  SQL.Add('left join tabkitbox_pp KPP on KPP.pedidoprojeto=PP.codigo');
                  SQL.Add('left join tabdiverso_pp DPP on DPP.pedidoprojeto= PP.codigo');
                  SQL.Add('left join tabvidro_pp VPP on VPP.pedidoprojeto=PP.codigo');
                  SQL.Add('left join tabvidrocor vidrocor on vidrocor.codigo=VPP.vidrocor');
                  SQL.Add('left join tabcor cor on cor.codigo=vidrocor.cor');
                  SQL.Add('where PPPC.pedidocompra='+PPedidoCompra);


                  Open;
                  while not eof do
                  begin
                          PedidoCompraRel:=TTQRPedidoCompraPadrao.create(nil);

                          PedidoCompraRel.lBX1.Caption := '';
                          PedidoCompraRel.lBX2.Caption := '';
                          PedidoCompraRel.lBX3.Caption := '';
                          PedidoCompraRel.lBX4.Caption := '';
                          PedidoCompraRel.lBX5.Caption := '';
                          PedidoCompraRel.lBX6.Caption := '';
                          PedidoCompraRel.lBX7.Caption := '';
                          PedidoCompraRel.lBXESP1.caption:='';
                          PedidoCompraRel.lBXESP2.caption:='';
                          PedidoCompraRel.lBXESP3.caption:='';
                          PedidoCompraRel.lBXESP4.caption:='';

                          PedidoProjetoExterno:=fieldbyname('codigo').AsString;

                          QueryAux.Close;
                          QueryAux.SQL.Clear;
                          QueryAux.SQL.Add('select tabvidro.espessura');
                          QueryAux.SQL.Add('from tabvidro');
                          QueryAux.SQL.Add('join tabvidrocor on tabvidrocor.vidro=tabvidro.codigo');
                          QueryAux.SQL.Add('join tabvidro_pp on tabvidro_pp.vidrocor=tabvidrocor.codigo');
                          QueryAux.SQL.Add('join tabpedido_proj on tabpedido_proj.codigo=tabvidro_pp.pedidoprojeto');
                          QueryAux.SQL.Add('where tabpedido_proj.codigo='+PedidoProjetoExterno);
                          QueryAux.Open;

                          Espessura:=QueryAux.fieldbyname('espessura').AsString;

                          PedidoCompraRel.lB16.Caption:=Espessura;
                          PedidoCompraRel.lBXESP1.Caption:='X';

                          PedidoCompraRel.lB17.Caption:=CurrToStr(StrToCurrDef(Espessura,0)+2);
                          PedidoCompraRel.lB18.Caption:=CurrToStr(StrToCurrDef(Espessura,0)+4);
                          PedidoCompraRel.lB19.Caption:=CurrToStr(StrToCurrDef(Espessura,0)+6);

                          PedidoProjeto.Pedido.LocalizaCodigo(fieldbyname('pedido').AsString);
                          PedidoProjeto.Pedido.TabelaparaObjeto;

                          PedidoCompraRel.qrmObservacao.Lines.Text:=fieldbyname('observacao').AsString;
                          PedidoCompraRel.lBNomeDistribuidor.Caption:=fieldbyname('fantasia').AsString;
                          PedidoCompraRel.lbpedidocompra.caption:= FieldByName('codigopedidocompra').asstring;
                          PedidoCompraRel.lBDataPedidoCompra.caption:=Fieldbyname('data').AsString;

                          // Verificando preenchendo a Cor do Vidro...
                          if(PedidoCompraRel.lB8.Caption=tira_espacos_final(fieldbyname('descricao').AsString))
                          then begin
                                 PedidoCompraRel.lBX1.Caption := 'X';
                          end
                          else
                          begin
                              if(PedidoCompraRel.lB9.Caption=tira_espacos_final(fieldbyname('descricao').AsString))
                              then
                              begin
                                     PedidoCompraRel.lBX2.Caption := 'X';
                              end
                              else
                              begin
                                  if(PedidoCompraRel.lB10.Caption=tira_espacos_final(fieldbyname('descricao').AsString))
                                  then
                                  begin
                                       PedidoCompraRel.lBX3.Caption := 'X';
                                  end
                                  else
                                  begin
                                      if(PedidoCompraRel.lB11.Caption=tira_espacos_final(fieldbyname('descricao').AsString))
                                      then
                                      begin
                                            PedidoCompraRel.lBX4.Caption := 'X';
                                      end
                                      else
                                      begin
                                          if(PedidoCompraRel.lB12.Caption=tira_espacos_final(fieldbyname('descricao').AsString))
                                          then
                                          begin
                                                PedidoCompraRel.lBX5.Caption := 'X';
                                          end
                                          else
                                          begin
                                              if(PedidoCompraRel.lB13.Caption=tira_espacos_final(fieldbyname('descricao').AsString))
                                              then
                                              begin
                                                   PedidoCompraRel.lBX6.Caption := 'X';
                                              end
                                              else
                                              begin

                                                      if(PedidoCompraRel.lB14.Caption=tira_espacos_final(fieldbyname('descricao').AsString))
                                                      then
                                                      begin
                                                              PedidoCompraRel.lBX7.Caption := 'X';
                                                      end
                                                      else
                                                      begin
                                                              PedidoCompraRel.lB14.Caption:=fieldbyname('descricao').AsString;
                                                              PedidoCompraRel.lBX7.Caption := 'X';
                                                      end;


                                              end;
                                          end;
                                      end;
                                  end;
                              end;
                          end;
                          
                          PreencheOrcamentoRelPedidoCompra;

                          PLocalLogoTipo:=ExtractFilePath(Application.exename)+'Images\Logo_Distribuidor.jpg';
                          PedidoCompraRel.imgLogoDistribuidor.Picture.LoadFromFile(PLocalLogoTipo);
                          PedidoCompraRel.BandaDetail.BeforePrint:= BandaDetailBeforePrint;
                          Preencheimagens;

                          PedidoCompraRel.Preview;
                          PedidoCompraRel.Free;
                          Next;
                  end;

                  //gerar pedidocompra dos materiais avulsos...


                  Close;
                  SQL.Clear;
                  sql.Add('select viewpedidomateriaisavulso.NOMEMATERIAL,TABCOR.descricao,viewpedidomateriaisavulso.CODIGO,viewpedidomateriaisavulso.PEDIDOCOMPRA,');
                  SQL.Add('viewpedidomateriaisavulso.MATERIALCOR,viewpedidomateriaisavulso.TIPOMATERIAL,viewpedidomateriaisavulso.LARGURA,viewpedidomateriaisavulso.ALTURA,viewpedidomateriaisavulso.QUANTIDADE,');
                  SQL.Add('viewpedidomateriaisavulso.CUSTO');
                  SQL.Add('from viewpedidomateriaisavulso');
                  SQL.Add('join tabcor on tabcor.codigo=viewpedidomateriaisavulso.materialcor');
                  SQL.Add('where pedidocompra='+PPedidoCompra);
                  Open;
                  Last;
                  if(recordcount=0)
                  then Exit;

                  PedidoCompra.LocalizaCodigo(PPedidoCompra);
                  PedidoCompra.TabelaparaObjeto;

                  PedidoCompraMatAvulsoRel.qrmObservacao.Lines.Text:=PedidoCompra.Get_Observacao;
                  PedidoCompraMatAvulsoRel.lBNomeDistribuidor.Caption:=PedidoCompra.Fornecedor.Get_Fantasia;
                  PedidoCompraMatAvulsoRel.lbpedidocompra.caption:= PedidoCompra.Get_CODIGO;
                  PedidoCompraMatAvulsoRel.lBDataPedidoCompra.caption:=PedidoCompra.Get_Data;

                  PLocalLogoTipo:=ExtractFilePath(Application.exename)+'Images\Logo_Distribuidor.jpg';
                  PedidoCompraMatAvulsoRel.imgLogoDistribuidor.Picture.LoadFromFile(PLocalLogoTipo);
                  PreencheOrcamentoRelPedidoCompra2;
                  PedidoCompraMatAvulsoRel.BandaDetail.BeforePrint:= BandaDetailBeforePrint2;

                  PedidoCompraMatAvulsoRel.Preview;

          end;
     finally
          FreeAndNil(QueryPesquisa);
     end;

end;

procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.BandaDetailBeforePrint(Sender: TQRCustomBand;var PrintBand: Boolean);
var

  query,Querybusca:TIBQuery;

  folgaaltura,folgalargura,ValorOrcamento,Valor, custototal, custo:Currency;

  Posicaocorte,alturacorte,tamanhofuro,

  corte,PPedidoPRojeto,altura,largura:string;




begin

  try

    query := TIBQuery.Create(nil);
    query.Database := FDataModulo.IBDatabase;

    Querybusca := TIBQuery.Create(nil);
    Querybusca.Database := FDataModulo.IBDatabase;

  except

  end;

  try

    with query do
    begin

      Close;
      sql.Clear;
      sql.Add('select comp.referencia,comp.codigo,cpp.largura,cpp.altura,comp.descricao,PP.projeto,cp.folgaaltura,cp.folgalargura,vidro.precocusto') ;
      sql.Add('from tabpedidoprojetopedidocompra PPPC') ;
      sql.Add('join tabpedidocompra Pcompra on Pcompra.codigo=PPPC.pedidocompra') ;
      sql.Add('join tabpedido_proj PP on PPPC.pedidoprojeto=PP.codigo');
      sql.Add('join tabcomponente_pp cpp on cpp.pedidoprojeto=pp.codigo') ;
      sql.Add('join tabcomponente comp on  comp.codigo=cpp.componente') ;
      sql.Add('join tabvidro_pp vpp on vpp.pedidoprojeto=pp.codigo');
      sql.Add('join tabvidrocor vidrocor on vidrocor.codigo=vpp.vidrocor');
      sql.Add('join tabvidro vidro on vidro.codigo=vidrocor.vidro');
      SQL.Add('join tabcomponente_proj cp on cp.componente = comp.codigo and cp.projeto = pp.projeto');
      sql.Add('where Pcompra.codigo='+PedidodeCompraExterno);
      sql.Add('and pp.codigo='+PedidoProjetoExterno);
      //InputBox('','',query.SQL.Text);
      Open;
      PedidoCompraRel.QrStrBandDados.Items.Clear;


      //Mostrando os componentes
      //EX: Porta Direita, Porta Esquerda, Fixo, Etc
      corte:='';
      custototal := 0;
      while not eof do
      begin

        Querybusca.Close;
        Querybusca.sql.clear;
        Querybusca.sql.Add('select corte from tabcomponente_proj where tabcomponente_proj.componente='+fieldbyname('codigo').AsString);
        Querybusca.sql.Add('and tabcomponente_proj.projeto='+fieldbyname('projeto').AsString);
        Querybusca.Open;

        if(Querybusca.fieldbyname('corte').AsString='MODELADO') then
          corte:='MD'
        else
        if(Querybusca.fieldbyname('corte').AsString='CAIXILHO') then
          corte:='C'
        else
        if(Querybusca.fieldbyname('corte').AsString='DESPRUMO') then
          corte:='DP'
        else
        if(Querybusca.Fieldbyname('corte').AsString='INSTALA��O') then
          corte:='I';
        custo := fieldbyname('precocusto').AsCurrency;
        if not utilizaCusto then
          custo := 0;
        altura  := CurrToStr( query.fieldbyname('altura').AsCurrency-query.fieldbyname('folgaaltura').AsCurrency );
        largura := CurrToStr( query.fieldbyname('largura').AsCurrency-query.fieldbyname('folgalargura').AsCurrency );
        custototal := custototal + custo;
        PedidoCompraRel.QrStrBandDados.Items.Add (
        CompletaPalavra('',5,' ')+' '+CompletaPalavra('1',7,' ')+' '+
        CompletaPalavra(largura,6,' ')+'X '+CompletaPalavra(altura,6,' ')+' '+CompletaPalavra(fieldbyname('referencia').AsString,10,' ')
        +' '+CompletaPalavra(corte,7,' ')+' '+CompletaPalavra(fieldbyname('descricao').AsString,40,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(custo),10,' '));

        Next;

      end;
      if( custototal> 0 ) then
        PedidoCompraRel.QrStrBandDados.Items.Add (
        CompletaPalavra('',77,' ')+'TOTAL '+CompletaPalavra_a_Esquerda(formata_valor( custototal),16,' '));

      //Selecionando as ferragem que para instalar precisam de cortar algum componente

      Close;
      sql.Clear;
      sql.Add('select  DISTINCT(componente.codigo),componente.descricao as nomecomponente,ferragem.descricao as nomeferragem, ferragem.referencia');
      sql.Add(',ccp.formulaalturacorte,ccp.formulaposicaocorte,ccp.tamanhofuro,ccp.tamanhofuro,cpp.largura,cpp.altura');
      sql.Add('from tabcortecomponente_proj ccp');
      sql.Add('join tabcomponente_proj cp on cp.codigo=ccp.componente_proj');
      sql.Add('join tabferragem ferragem on ferragem.codigo=ccp.ferragem');
      sql.Add('join tabcomponente componente on componente.codigo=cp.componente');
      sql.Add('join tabprojeto projeto on projeto.codigo=cp.projeto');
      sql.Add('join tabpedido_proj pp on pp.projeto=projeto.codigo');
      sql.Add('join tabpedidoprojetopedidocompra ppc on ppc.pedidoprojeto=pp.codigo');
      SQL.Add('join tabpedidocompra on tabpedidocompra.codigo=ppc.pedidocompra');
      SQL.Add('join tabcomponente_pp cpp on cpp.pedidoprojeto=pp.codigo');
      sql.Add('where tabpedidocompra.codigo='+PedidodeCompraExterno);
      sql.Add('and pp.codigo='+PedidoProjetoExterno);
      sql.Add('and componente.codigo=cpp.componente');

      Open;

      {PedidoCompraRel.QrStrBandDados.Items.Add(#13);
      PedidoCompraRel.QrStrBandDados.Items.Add(CompletaPalavra('REFERENCIA',10,' ')+' '+CompletaPalavra('DESCRI��O',40,' ')+' '+
      CompletaPalavra('ALTURA',10,' ')+' '+CompletaPalavra('POSI��O',10,' ')+' '+CompletaPalavra('FURO',7,' ')+' '+CompletaPalavra('COMPONENTE',15,' '));

      while not Eof do
      begin

                CalculaFormulasCorte(fieldbyname('formulaalturacorte').AsString,fieldbyname('formulaposicaocorte').AsString,fieldbyname('altura').AsCurrency,fieldbyname('largura').AsCurrency,alturacorte,Posicaocorte);
                PedidoCompraRel.QrStrBandDados.Items.Add(CompletaPalavra(fieldbyname('referencia').AsString,10,' ')+' '+CompletaPalavra(fieldbyname('nomeferragem').AsString,40,' ')+' '+
                CompletaPalavra(alturacorte,10,' ')+' '+CompletaPalavra(Posicaocorte,10,' ')+' '+CompletaPalavra(fieldbyname('TAMANHOFURO').AsString+' mm.',7,' ')+' '+CompletaPalavra(fieldbyname('nomecomponente').AsString,18,' '));

                Next;
      end;
           }
      Close;
      sql.Clear;
      sql.add('select * from viewmateriaispedidocompra');
      sql.Add('where codigo='+PedidoProjetoExterno);
      sql.Add('and pedidocompra='+PedidodeCompraExterno);
      Open;
      PedidoCompraRel.QrStrBand1.Items.Clear;

      PedidoCompraRel.lBCorFerragem.Caption:=get_campoTabela ('DESCRICAO','CODIGO','TABCOR', fieldbyname('corferragem').AsString);
      PedidoCompraRel.lBcorperfilado.Caption:=get_campoTabela ('DESCRICAO','CODIGO','TABCOR', fieldbyname('corperfilado').AsString);

      PedidoCompraRel.QrStrBand1.Items.Add(CompletaPalavra('',5,' ')+' '+CompletaPalavra('CODIGO',10,' ')+' '+
      CompletaPalavra('DESCRICAO',50,' ')+' '+CompletaPalavra('QUANTIDADE',5,' ')+' '+CompletaPalavra('CUSTO',10,' ')
      +' '+CompletaPalavra('VALOR',10,' '));

      ValorOrcamento:=0;
      while not Eof do
      begin

              if(fieldbyname('DESCRICAO').AsString<>'') then
              begin
                      custo := fieldbyname('CUSTO').AsCurrency;
                      if not utilizaCusto then
                        custo := 0;
                      Valor:=fieldbyname('QUANTIDADEMATERIAL').AsCurrency*custo;

                      PedidoCompraRel.QrStrBand1.Items.Add(CompletaPalavra('',5,' ')+' '+CompletaPalavra(fieldbyname('referencia').AsString,10,' ')+' '+
                      CompletaPalavra(fieldbyname('DESCRICAO').AsString,50,' ')+' '+CompletaPalavra(fieldbyname('QUANTIDADEMATERIAL').AsString,5,' ')+' '+CompletaPalavra(formata_valor(fieldbyname('CUSTO').AsCurrency),10,' ')
                      +' '+CompletaPalavra(formata_valor(Valor),10,' '));

                      ValorOrcamento:=ValorOrcamento+Valor;
                     { if(PedidoCompraRel.lB53.Color=clYellow) then
                      PedidoCompraRel.lB53.Color:=clWhite
                      else PedidoCompraRel.lB53.Color:=clYellow;  }
              end;
              Next;
      end;
      PedidoCompraRel.QrStrBand1.Items.Add(CompletaPalavra('',6,' ')+CompletaPalavra('VALOR TOTAL ',78,' ')+' '+CompletaPalavra(CurrToStr(ValorOrcamento),10,' '));

    end;
    
  finally
    FreeAndNil(query);
  end;

End;

procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.BandaDetailBeforePrint2(Sender: TQRCustomBand;var PrintBand: Boolean);
var
PPedidoPRojeto:string;
query:TIBQuery;
Valor,custo:Currency;
Posicaocorte,alturacorte,tamanhofuro:string;
ValorOrcamento,ValorCustoTotal:Currency;
begin
     try
          query:=TIBQuery.Create(nil);
          query.Database:=FDataModulo.IBDatabase;


     except

     end;

     try
            with query do
            begin
                 Close;
                  SQL.Clear;
                  sql.Add('select viewpedidomateriaisavulso.NOMEMATERIAL,TABCOR.descricao,viewpedidomateriaisavulso.CODIGO,viewpedidomateriaisavulso.PEDIDOCOMPRA,');
                  SQL.Add('viewpedidomateriaisavulso.MATERIALCOR,viewpedidomateriaisavulso.TIPOMATERIAL,viewpedidomateriaisavulso.LARGURA,viewpedidomateriaisavulso.ALTURA,viewpedidomateriaisavulso.QUANTIDADE,');
                  SQL.Add('viewpedidomateriaisavulso.CUSTO');
                  SQL.Add('from viewpedidomateriaisavulso');
                  SQL.Add('join tabcor on tabcor.codigo=viewpedidomateriaisavulso.materialcor');
                  SQL.Add('where pedidocompra='+PedidodeCompraExterno);
                  Open;
                  PedidoCompraMatAvulsoRel.QrStrBandDados.Items.Clear;
                  ValorCustoTotal := 0;
                  while not eof do
                  begin
                       custo := fieldbyname('custo').AsCurrency;
                       if not utilizaCusto then
                        custo := 0;
                       ValorCustoTotal := ValorCustoTotal + ( custo * fieldbyname('QUANTIDADE').AsCurrency );
                       PedidoCompraMatAvulsoRel.QrStrBandDados.Items.Add(CompletaPalavra('',5,' ')+' '+CompletaPalavra(fieldbyname('QUANTIDADE').AsString,7,' ')+' '+
                       CompletaPalavra(fieldbyname('largura').AsString,6,' ')+'X '+CompletaPalavra(fieldbyname('altura').AsString,6,' ')+' '+CompletaPalavra(fieldbyname('TIPOMATERIAL').AsString,10,' ')
                       +' '+CompletaPalavra(fieldbyname('NOMEMATERIAL').AsString,30,' ')+' '+CompletaPalavra(fieldbyname('descricao').AsString,10,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor( custo * fieldbyname('QUANTIDADE').AsCurrency),15,' '));
                       Next;
                  end;
                  if ValorCustoTotal > 0 then
                    PedidoCompraMatAvulsoRel.QrStrBandDados.Items.Add(CompletaPalavra('',76,' ')+'TOTAL '+CompletaPalavra_a_Esquerda(formata_valor( ValorCustoTotal ),15,' '));

            end;
     finally
           FreeAndNil(query);
     end;



End;


procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.PreencheOrcamentoRelPedidoCompra;
begin
      PedidoCompraRel.lBNomeEmpresa.Caption:=ObjEmpresaGlobal.Get_FANTASIA;
      PedidoCompraRel.lBEndereco.Caption:=ObjEmpresaGlobal.Get_ENDERECO;
      PedidoCompraRel.lBCidade.Caption:=ObjEmpresaGlobal.Get_CIDADE;
      PedidoCompraRel.lBBairro.caption:=ObjEmpresaGlobal.get_bairro;
      PedidoCompraRel.lBFone.Caption:=ObjEmpresaGlobal.Get_FONE;
      PedidoCompraRel.lBCep.Caption:=ObjEmpresaGlobal.Get_CEP;
      PedidoCompraRel.lBNumero.Caption:=ObjEmpresaGlobal.Get_numero;
      PedidoCompraRel.lBcliente.Caption:=ObjpedidoProjetoPedidoCompra.PedidoProjeto.Pedido.Cliente.Get_Nome;
      PedidoCompraRel.lBEnderecoCliente.Caption:=PedidoProjeto.Pedido.Cliente.Get_Endereco;
      PedidoCompraRel.lBCidadeCliente.Caption:=PedidoProjeto.pedido.Cliente.Get_Cidade;
      PedidoComprarel.lBBairroCliente.Caption:=PedidoProjeto.Pedido.Cliente.Get_Bairro;
      PedidoCompraRel.lBFoneCliente.Caption:=PedidoProjeto.Pedido.Cliente.Get_Fone;
      PedidoCompraRel.lBNumeroCliente.Caption:=PedidoProjeto.Pedido.Cliente.Get_Numero;
      PedidoComprarel.lBCEPCliente.Caption:=PedidoProjeto.Pedido.Cliente.Get_CEP;
      PedidoCompraRel.lBCPF.Caption:=PedidoProjeto.Pedido.Cliente.Get_CPF_CGC;
      PedidoCompraRel.lBCNPJ.Caption:=PedidoProjeto.Pedido.Cliente.Get_CPF_CGC;
      PedidoCompraRel.lBFoneCliente.Caption:=PedidoProjeto.Pedido.Cliente.Get_Fone;
      PedidoCompraRel.qrlblFaxCliente.Caption:=PedidoProjeto.Pedido.Cliente.Get_Fax;
      PedidoCompraRel.lBie.Caption:=PedidoProjeto.Pedido.Cliente.Get_RG_IE;
end;

procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.PreencheOrcamentoRelPedidoCompra2;
begin
      PedidoCompraMatAvulsoRel.lBNomeEmpresa.Caption:=ObjEmpresaGlobal.Get_FANTASIA;
      PedidoCompraMatAvulsoRel.lBEndereco.Caption:=ObjEmpresaGlobal.Get_ENDERECO;
      PedidoCompraMatAvulsoRel.lBCidade.Caption:=ObjEmpresaGlobal.Get_CIDADE;
      PedidoCompraMatAvulsoRel.lBBairro.caption:=ObjEmpresaGlobal.get_bairro;
      PedidoCompraMatAvulsoRel.lBFone.Caption:=ObjEmpresaGlobal.Get_FONE;
      PedidoCompraMatAvulsoRel.lBCep.Caption:=ObjEmpresaGlobal.Get_CEP;
      PedidoCompraMatAvulsoRel.lBNumero.Caption:=ObjEmpresaGlobal.Get_numero;

      {PedidoCompraMatAvulsoRel.lBcliente.Caption:=ObjpedidoProjetoPedidoCompra.PedidoProjeto.Pedido.Cliente.Get_Nome;
      PedidoCompraMatAvulsoRel.lBEnderecoCliente.Caption:=PedidoProjeto.Pedido.Cliente.Get_Endereco;
      PedidoCompraMatAvulsoRel.lBCidadeCliente.Caption:=PedidoProjeto.pedido.Cliente.Get_Cidade;
     // PedidoCompraRel.lBUFCliente.Caption:=PedidoProjeto.pedido.Cliente.Get_Estado;
      PedidoCompraMatAvulsoRel.lBBairroCliente.Caption:=PedidoProjeto.Pedido.Cliente.Get_Bairro;
      PedidoCompraMatAvulsoRel.lBFoneCliente.Caption:=PedidoProjeto.Pedido.Cliente.Get_Fone;
      PedidoCompraMatAvulsoRel.lBNumeroCliente.Caption:=PedidoProjeto.Pedido.Cliente.Get_Numero;
      PedidoCompraMatAvulsoRel.lBCEPCliente.Caption:=PedidoProjeto.Pedido.Cliente.Get_CEP;
      PedidoCompraMatAvulsoRel.lBCPF.Caption:=PedidoProjeto.Pedido.Cliente.Get_CPF_CGC;
      PedidoCompraMatAvulsoRel.lBCNPJ.Caption:=PedidoProjeto.Pedido.Cliente.Get_CPF_CGC; }
     
end;

procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.Preencheimagens;
var
  Caminho:string;
  Query:TIBQuery;
begin
   Query:=TIBQuery.Create(nil);
   query.Database:=FDataModulo.IBDatabase;
   try
      query.Close;
      query.sql.Clear;
      query.sql.Add('select referencia');
      query.sql.Add('from tabprojeto');
      query.sql.Add('join tabpedido_proj on tabpedido_proj.projeto = tabprojeto.codigo');
      query.sql.Add('join tabpedidoprojetopedidocompra on tabpedidoprojetopedidocompra.pedidoprojeto= tabpedido_proj.codigo');
      query.sql.Add('and tabpedidoprojetopedidocompra.pedidoprojeto='+PedidoProjetoExterno);
      query.Open;
      if (ObjParametroGlobal.ValidaParametro('CAMINHO DOS DESENHOS PARA PEDIDO DE COMPRA')=false)then
      Begin
          MensagemErro('O par�metro "CAMINHO DOS DESENHOS PARA PEDIDO DE COMPRA" n�o foi encontrado.');
          exit;
      end;
      
      Caminho:=PedidoProjeto.Projeto.ResgataDesenho(query.fieldbyname('referencia').AsString);;
      if(FileExists(Caminho)) then
      begin
        PedidoCompraRel.img2.Picture.LoadFromFile(Caminho);
        PedidoCompraRel.img1.Picture.LoadFromFile(ObjParametroGlobal.Get_Valor+PedidoProjetoExterno+'_compra.jpg');
      end;

   finally
      FreeAndNil(Query);
   end;
end;

procedure TObjPEDIDOPROJETOPEDIDOCOMPRA.CalculaFormulasCorte(FormulaAltura:string;FormulaPosicao:string;Altura:Currency;Largura:Currency;var AlturaCorte:string;Var PosicaoCorte:string);
var
  aux1,aux2,aux3:string;
  valoraux:Currency;
begin
    aux1:='';
    aux2:='';
    aux3:='';

    AlturaCorte:=FormulaAltura;
    //antes de fazer as substitui��es na formula, eu pego o valor
    aux3:=RetornaValor(AlturaCorte);
    //substitui a formula
    AlturaCorte:=StrReplace(AlturaCorte,'ALTURA',CurrToStr(Altura));
    AlturaCorte:=StrReplace(AlturaCorte,'LARGURA',CurrToStr(Largura));
    //pego o valor substituido
    aux1:=retornaPalavrasAntesSimboloLocal(AlturaCorte);
    //pego o operador matematico
    aux2:=RetornaOperadorMatematico(AlturaCorte);

    if(aux2<>'') then
    begin
         if(aux2='+')
         then valoraux:=StrToCurr(aux1)+StrToCurr(aux3);
         if(aux2='-')
         then valoraux:=StrToCurr(aux1)-StrToCurr(aux3);
         if(aux2='*')
         then valoraux:=StrToCurr(aux1)*StrToCurr(aux3);
         if(aux2='/')
         then valoraux:=StrToCurr(aux1)/StrToCurr(aux3);
         AlturaCorte:=CurrToStr(valoraux);

    end;
    AlturaCorte:=AlturaCorte+' mm.';

    aux1:='';
    aux2:='';
    aux3:='';

    PosicaoCorte:=FormulaPosicao ;
     //antes de fazer as substitui��es na formula, eu pego o valor
    aux3:=RetornaValor(PosicaoCorte);
    //substitui a formula
    PosicaoCorte:=StrReplace(PosicaoCorte,'ALTURA',CurrToStr(Altura));
    PosicaoCorte:=StrReplace(PosicaoCorte,'LARGURA',CurrToStr(Largura));
    //pego o valor substituido
    aux1:=retornaPalavrasAntesSimboloLocal(PosicaoCorte);
    //pego o operador matematico
    aux2:=RetornaOperadorMatematico(PosicaoCorte);

    if(aux2<>'') then
    begin
         if(aux2='+')
         then valoraux:=StrToCurr(aux1)+StrToCurr(aux3);
         if(aux2='-')
         then valoraux:=StrToCurr(aux1)-StrToCurr(aux3);
         if(aux2='*')
         then valoraux:=StrToCurr(aux1)*StrToCurr(aux3);
         if(aux2='/')
         then valoraux:=StrToCurr(aux1)/StrToCurr(aux3);
         PosicaoCorte:=CurrToStr(valoraux);

    end;
    PosicaoCorte:=PosicaoCorte+' mm.';



end;

function TObjPEDIDOPROJETOPEDIDOCOMPRA.RetornaUnidadeMedida(str:string):string;
var
    i:Integer;
    Aux:string;
begin
      result:='';
      i:=0;
      aux:='';

      if(Str='')
      then Exit;

      while (i<Length(str)+1) do
      begin
              if (Str[i]='c') or(Str[i]='m')
              then aux:=aux+Str[i];
              Inc(i,1);
      end;

      result:=aux;

end;

function TObjPEDIDOPROJETOPEDIDOCOMPRA.RetornaValor(Str:string):string;
var
  i,j:Integer;
  aux:string ;
begin
       Result:='';
       i:=0;
       aux:='';
       if(Str='')
       then Exit;
       while (i <= Length (str)) do
       begin
            if(Str[i] in['0'..'9'])
            then aux:=aux+Str[i];

            Inc(i,1);
       end;

       result:=aux;
end;

function TObjPEDIDOPROJETOPEDIDOCOMPRA.retornaPalavrasAntesSimboloLocal(str: string): string;
var
  i,j:Integer;
  aux:string;
begin

   result:='';

  if (str = '') then
    Exit;

  aux:='';
  
  i:=1;
  while ((str[i] <> '/') and(str[i] <> '-') and (str[i] <> '+') and (str[i] <> '*') and (i <= Length (str))) do
  begin

    aux:=aux+str[i];
    i:=i+1;

  end;
  //aux:=aux+Simbolo;
  i:=Length(aux);
  result:='';

  while ( (aux[i] = ' ') and (i >= 1 ) ) do I:=I-1;


  for j := 1 to i  do
    result:=result+aux[j];


end;

function TObjPEDIDOPROJETOPEDIDOCOMPRA.RetornaOperadorMatematico(str:string):string;
var
  i,j:Integer;
  aux:string ;
begin
      Result:='';
      aux:='';
      i:=0;
      if(Str='')
      then Exit;
      while (i <= Length (str)) do
      begin
          if(Str[i]='/') or (Str[i]='+') or (Str[i]='-') or (Str[i]='*')
          then aux:=Str[i];

          Inc(i,1);
      end;
      Result:=aux;

end;

end.






