unit UCIDADE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjCIDADE,
  UessencialGlobal, UEssencialLocal, Tabs,IBQuery,UopcaoRel, ComCtrls;

type
  TFCIDADE = class(TForm)
    pnl1: TPanel;
    imgrodape: TImage;
    lb14: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    lbCodigoCidade: TLabel;
    edtCodigoCidade: TEdit;
    edtestado: TEdit;
    edtnome: TEdit;
    edtCodigo: TEdit;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    btNovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btCancelar: TBitBtn;
    btexcluir: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btOpcoes: TBitBtn;
    btsair: TBitBtn;
    statusBar1: TStatusBar;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure btOpcoesClick(Sender: TObject);

  private
    ObjCIDADE:TObjCIDADE;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    procedure MostraQuantidadeCadastrada;
    function  AtualizaQuantidade: string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCIDADE: TFCIDADE;


implementation

uses Upesquisa, UDataModulo, UescolheImagemBotao;

{$R *.dfm}


procedure TFCIDADE.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCIDADE=Nil)
     Then exit;

     If (Self.ObjCIDADE.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjCIDADE.free;
   
end;

procedure TFCIDADE.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

      edtcodigo.text:='0';



     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;
     btgravar.Enabled:=True;

     ObjCIDADE.status:=dsInsert;

     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorios.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;



     //comboestado.setfocus;

end;

procedure TFCIDADE.btSalvarClick(Sender: TObject);
begin

     If ObjCIDADE.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjCIDADE.salvar()=False)
     Then exit;

    // lbcodigo.Caption:=ObjCIDADE.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorios.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;
     MostraQuantidadeCadastrada;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCIDADE.btAlterarClick(Sender: TObject);
begin

    If (ObjCIDADE.Status=dsinactive) and (edtCodigo.Text<>'') Then
    Begin

      habilita_campos(Self);

      ObjCIDADE.Status:=dsEdit;

      desab_botoes(Self);
      BtCancelar.enabled:=True;
      btpesquisar.enabled:=True;
      btgravar.Enabled:=True;
      btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorios.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

    End;

end;

procedure TFCIDADE.btCancelarClick(Sender: TObject);
begin
     ObjCIDADE.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
    edtCodigo.text:='';
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorios.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;

end;

procedure TFCIDADE.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjCIDADE.Get_pesquisa,ObjCIDADE.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                             If (FpesquisaLocal.showmodal=mrok)
                             Then Begin
                                    If ObjCIDADE.status<>dsinactive
                                    then exit;

                                    If (ObjCIDADE.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                    Then
                                    Begin
                                              Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                              exit;
                                    End;
                                    ObjCIDADE.ZERARTABELA;
                                    If (TabelaParaControles=False)
                                    Then
                                    Begin
                                              Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                              limpaedit(Self);
                                              Self.limpaLabels;
                                              exit;
                                    End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
            End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFCIDADE.btExcluirClick(Sender: TObject);
begin
     If (ObjCIDADE.status<>dsinactive) or (edtCodigo.Text='')
     Then exit;

     If (ObjCIDADE.LocalizaCodigo(edtCodigo.Text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjCIDADE.exclui(edtCodigo.Text)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCIDADE.btRelatorioClick(Sender: TObject);
begin
   // ObjCIDADE.Imprime(edtCodigo.Text);
end;

procedure TFCIDADE.btSairClick(Sender: TObject);
begin
    Self.close;
end;


procedure TFCIDADE.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFCIDADE.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFCIDADE.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;


end;

procedure TFCIDADE.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFCIDADE.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjCIDADE do
    Begin
        Submit_Codigo(edtCodigo.Text);
        Submit_Nome(edtNome.text);
        submit_codigoCidade(edtCodigoCidade.Text);
        Submit_ESTADO(edtestado.Text);
        //Submit_uf(ComboEstado.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFCIDADE.LimpaLabels;
begin
//LIMPA LABELS
end;

function TFCIDADE.ObjetoParaControles: Boolean;
begin
  Try
     With ObjCIDADE do
     Begin
        edtCodigo.Text:=Get_Codigo;
        EdtNome.text:=Get_Nome;
        edtCodigoCidade.text:=get_codigoCidade;
        edtestado.Text:=Get_ESTADO;

//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFCIDADE.TabelaParaControles: Boolean;
begin
     ObjCIDADE.TabelaparaObjeto;

     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFCIDADE.MostraQuantidadeCadastrada;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabcidade');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb14.Caption:='Existem '+IntToStr(contador)+' cidades cadastrados'
       else
       begin
            lb14.Caption:='Existe '+IntToStr(contador)+' cidade cadastrado';
       end;

    finally

    end;


end;


//CODIFICA ONKEYDOWN E ONEXIT


procedure TFCIDADE.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     ColocaUpperCaseEdit(Self);

     Try
        Self.ObjCIDADE:=TObjCIDADE.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
    edtCodigo.Text:='';

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE CIDADE')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);
     MostraQuantidadeCadastrada;
end;

procedure TFCIDADE.btOpcoesClick(Sender: TObject);
begin
      With FOpcaorel do
      Begin

            RgOpcoes.Items.clear;
            RgOpcoes.Items.add('Importar cidades (*.txt)'); //0

            showmodal;

            if (tag=0)
            Then exit;

            case RgOpcoes.ItemIndex of

                0:begin

                  try

                    Screen.Cursor:=crHourGlass;

                    if (Objcidade.importaCidade(statusBar1)) then
                      FDataModulo.IBTransaction.CommitRetaining
                    else
                    begin

                      FDataModulo.IBTransaction.RollbackRetaining;
                      MensagemAviso('Cidades n�o foram importadas');
                      Exit;

                    end;

                    MensagemAviso('Importa��o concluida com sucesso!');
                    AtualizaQuantidade();

                  finally

                    Screen.Cursor:=crDefault;

                  end;

                end;

            End;

      End;

end;

function TFcidade.AtualizaQuantidade: string;
begin
      result:='Existem '+ ContaRegistros('TABCIDADE','codigo') + ' Cidades Cadastradas';
end;


end.
