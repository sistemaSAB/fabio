unit UAcertaUnidades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, Grids, StdCtrls, ExtCtrls, Menus,IBQuery,UDataModulo,UobjRELACAOMEDIDAS,DB,
  ImgList,UUtils,UessencialGlobal, jpeg;

type
  TFAcertaUnidades = class(TForm)
    panelpnl2: TPanel;
    Img1: TImage;
    panelpnl6: TPanel;
    lbDia: TLabel;
    STRGUnidadesRelacao: TStringGrid;
    panelpnlbotes: TPanel;
    bt1: TSpeedButton;
    lbdesagendar: TLabel;
    lbconcluir: TLabel;
    lb7: TLabel;
    panelpnl3: TPanel;
    btProcessar: TSpeedButton;
    lb11: TLabel;
    COMBOTipo: TComboBox;
    lb8: TLabel;
    PopUpCadastros: TPopupMenu;
    Alterar1: TMenuItem;
    Excluir: TMenuItem;
    ilProdutos: TImageList;
    Img2: TImage;
    Img3: TImage;
    Img4: TImage;
    lb1: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    procedure FormShow(Sender: TObject);
    procedure COMBOTipoExit(Sender: TObject);
    procedure COMBOTipoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure STRGUnidadesRelacaoKeyPress(Sender: TObject; var Key: Char);
    procedure STRGUnidadesRelacaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure STRGUnidadesRelacaoDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btProcessarClick(Sender: TObject);
    procedure STRGUnidadesRelacaoDrawCell(Sender: TObject; ACol,
      ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure ExcluirClick(Sender: TObject);
    procedure Alterar1Click(Sender: TObject);
    procedure COMBOTipoChange(Sender: TObject);
  private
      ObjrelacaoMedidas:TObjRELACAOMEDIDAS;

      procedure PesquisaUnidadesKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
      procedure GravaRelacoes;
      procedure GeraStringGrid;
      procedure CarregaRelacoesMedidas;
      procedure Delete;
      procedure Alterar;
      procedure VerificaDadosInseridos;
  public
    { Public declarations }
  end;

var
  FAcertaUnidades: TFAcertaUnidades;

implementation

uses UescolheImagemBotao, Upesquisa;

{$R *.dfm}

procedure TFAcertaUnidades.FormShow(Sender: TObject);
begin
     ObjrelacaoMedidas:=TObjRELACAOMEDIDAS.Create;

     FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');
     GeraStringGrid;
end;

procedure TFAcertaUnidades.GeraStringGrid;
begin
     with STRGUnidadesRelacao do
     begin
           RowCount:= 2;
           ColCount:= 9;
           FixedRows:=1;
           FixedCols:=0;

           ColWidths[0] := 120;
           ColWidths[1] := 200;
           ColWidths[2] := 120;
           ColWidths[3] := 200;
           ColWidths[4] := 100;
           ColWidths[5] := 0;
           ColWidths[6] := 0;
           ColWidths[7] := 0;
           ColWidths[8] := 50;

           Cells[0,0] := 'UN. ORIGEM';
           Cells[1,0] := 'NOME UNIDADE';
           Cells[2,0] := 'UN. CONVERS�O';
           Cells[3,0] := 'NOME UNIDADE';
           Cells[4,0] := 'EQUIVAL�NCIA ';
           Cells[5,0] := 'UN. OR.';
           Cells[6,0] := 'UN. COV.';
           Cells[7,0] := 'CODIGO';
           Cells[8,0] := '';

     end;
end;

procedure TFAcertaUnidades.COMBOTipoExit(Sender: TObject);
begin
    {STRGUnidadesRelacao.SetFocus;

    CarregaRelacoesMedidas;   }
end;

procedure TFAcertaUnidades.COMBOTipoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
        if(Key=13)
        then COMBOTipoExit(Sender);
end;

procedure TFAcertaUnidades.STRGUnidadesRelacaoKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8),',']) then
       begin
             Key:= #0;
       end;

end;

procedure TFAcertaUnidades.STRGUnidadesRelacaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   //Delete
   if(Key=46)then
   begin
       if(STRGUnidadesRelacao.Cells[7,STRGUnidadesRelacao.Row]<>'') then
       begin
            if (messagedlg('Certeza que deseja a rela�a� entre'+#13+STRGUnidadesRelacao.Cells[0,STRGUnidadesRelacao.Row]+'e '+STRGUnidadesRelacao.Cells[2,STRGUnidadesRelacao.Row]+'?',mtconfirmation,[mbyes,mbno],0)=mrno)
            then exit ;
            if(ObjrelacaoMedidas.Exclui(STRGUnidadesRelacao.Cells[7,STRGUnidadesRelacao.Row],True)=False) then
            begin
                  MensagemErro('Erro ao tentar excluir registro da linha '+IntToStr(STRGUnidadesRelacao.Row));
                  Exit;
            end; 
       end;
       Delete;
   end;

   //Tecla para cima
   if(Key=38) then
   begin
          if(STRGUnidadesRelacao.Cells[0,STRGUnidadesRelacao.RowCount-1]='') and (STRGUnidadesRelacao.Row>1) then
          begin
                 STRGUnidadesRelacao.RowCount:=STRGUnidadesRelacao.RowCount-1;
                 STRGUnidadesRelacao.col:=0;
          end;
   end;

   if(Key=VK_F5)
   then GravaRelacoes;

   if(key=40) then
   begin
      //Atualiza valores finais no rodap�
      //AtualizarLabels;
      //ValidaDadosLinhas;

      //Caso esteja na ultima coluna
      if(STRGUnidadesRelacao.Row = STRGUnidadesRelacao.RowCount-1 )then
      begin
          //Se a primeira coluna da linha estiver vazia, logo, n�o
          if(STRGUnidadesRelacao.Cells[0,STRGUnidadesRelacao.RowCount-1]='') then
          begin
                 STRGUnidadesRelacao.col:=0;
                 Exit;
          end;

          STRGUnidadesRelacao.RowCount:=STRGUnidadesRelacao.RowCount+1;
          STRGUnidadesRelacao.Row:=STRGUnidadesRelacao.RowCount-1;
          STRGUnidadesRelacao.col:=0;
      end
   end;
   
   if(Key= VK_F12) then
   begin
       //Mostra ou esconde codigos
       if(STRGUnidadesRelacao.ColWidths[5]= 0) then
       begin
            STRGUnidadesRelacao.ColWidths[5] := 80;
            STRGUnidadesRelacao.ColWidths[6] := 80;
            STRGUnidadesRelacao.ColWidths[7] := 80;
       end
       else
       begin
            STRGUnidadesRelacao.ColWidths[5] := 0;
            STRGUnidadesRelacao.ColWidths[6] := 0;
            STRGUnidadesRelacao.ColWidths[7] := 0;
       end;
   end;

   if(Key<>37) and (Key<>38) and (Key<>39) and (Key<>40) then
   begin
      //Se for enter, cria nova linha para adicionar materiais
      if(key=13) then
      begin
          //Caso coluna seja = 11, ou seja esteja na ultima coluna
          if(STRGUnidadesRelacao.col=4)then
          begin
             //Se a primeira coluna da linha estiver vazia, logo, n�o
             if(STRGUnidadesRelacao.Cells[0,STRGUnidadesRelacao.RowCount-1]='') then
             begin
                 STRGUnidadesRelacao.col:=0;
                 Exit;
             end;

             STRGUnidadesRelacao.RowCount:=STRGUnidadesRelacao.RowCount+1;
             STRGUnidadesRelacao.Row:=STRGUnidadesRelacao.RowCount-1;
             STRGUnidadesRelacao.col:=0;


          end
          else STRGUnidadesRelacao.col:= STRGUnidadesRelacao.col+1;
      end;

      //Caso seja F9
      if(Key= VK_F9) then
      begin
          //Verifica coluna v�lida para pesquisa
          if(STRGUnidadesRelacao.Col = 0)then
          begin
              PesquisaUnidadesKeyDown(Sender,Key,Shift);
              VerificaDadosInseridos;
          end;
          if(STRGUnidadesRelacao.Col = 1)then
          begin
               PesquisaUnidadesKeyDown(Sender,Key,Shift);
               VerificaDadosInseridos;
          end;

          if(STRGUnidadesRelacao.Col = 2) then
          begin
               PesquisaUnidadesKeyDown(Sender,Key,Shift);
                VerificaDadosInseridos;
          end;

          if(STRGUnidadesRelacao.Col = 3) then
          begin
              PesquisaUnidadesKeyDown(Sender,Key,Shift);
              VerificaDadosInseridos;
          end;
      end
      else
      begin
            //se tecla for diferente de F9
            //Verifica coluna v�lida para edi��o
            if(STRGUnidadesRelacao.Col=4) then
            begin
                 STRGUnidadesRelacao.Options:= STRGUnidadesRelacao.Options + [goEditing];
            end
            else
            begin
                 Key:=0;
                 STRGUnidadesRelacao.Options:= STRGUnidadesRelacao.Options - [goEditing];

            end;
      end;

   end;
end;

procedure TFAcertaUnidades.PesquisaUnidadesKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('select * from tabunidademedida','',nil)=True) Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)Then
                        Begin
                                if(STRGUnidadesRelacao.Col=0) or (STRGUnidadesRelacao.Col=1) then
                                begin
                                    STRGUnidadesRelacao.Cells[0,STRGUnidadesRelacao.Row]:=FpesquisaLocal.QueryPesq.fieldbyname('sigla').asstring;
                                    STRGUnidadesRelacao.Cells[1,STRGUnidadesRelacao.Row]:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;
                                    STRGUnidadesRelacao.Cells[5,STRGUnidadesRelacao.Row]:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                end;
                                if(STRGUnidadesRelacao.Col=2) or (STRGUnidadesRelacao.Col=3) then
                                begin
                                    STRGUnidadesRelacao.Cells[2,STRGUnidadesRelacao.Row]:=FpesquisaLocal.QueryPesq.fieldbyname('sigla').asstring;
                                    STRGUnidadesRelacao.Cells[3,STRGUnidadesRelacao.Row]:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;
                                    STRGUnidadesRelacao.Cells[6,STRGUnidadesRelacao.Row]:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                end;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TFAcertaUnidades.STRGUnidadesRelacaoDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
      //Verifica coluna v�lida para pesquisa
      if(STRGUnidadesRelacao.Col = 0)then
      begin
          PesquisaUnidadesKeyDown(Sender,Key,Shift);
          VerificaDadosInseridos;
      end;
      if(STRGUnidadesRelacao.Col = 1)then
      begin
          PesquisaUnidadesKeyDown(Sender,Key,Shift);
          VerificaDadosInseridos;
      end;

      if(STRGUnidadesRelacao.Col = 2) then
      begin
          PesquisaUnidadesKeyDown(Sender,Key,Shift);
          VerificaDadosInseridos;
      end;

      if(STRGUnidadesRelacao.Col = 3) then
      begin
          PesquisaUnidadesKeyDown(Sender,Key,Shift);
          VerificaDadosInseridos;

      end;
end;

procedure TFAcertaUnidades.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
      ObjrelacaoMedidas.Free;
end;

procedure TFAcertaUnidades.GravaRelacoes;
var
  cont:Integer;
begin
     try
         cont:=1;

         with ObjrelacaoMedidas do
         begin
               while cont<STRGUnidadesRelacao.RowCount do
               begin
                    if(STRGUnidadesRelacao.Cells[5,cont]='') and (STRGUnidadesRelacao.Cells[6,cont]='')
                    then Exit;

                    //Se estiver com X n�o ouve altera��o e ',' tem alguma advertencia
                    if(STRGUnidadesRelacao.Cells[8,cont] <> '            x') and (STRGUnidadesRelacao.Cells[8,cont] <> '            ,')then
                    begin
                          //se estiver vazio � pra inserir
                          if(STRGUnidadesRelacao.Cells[8,cont] = '') then
                          begin
                              Status:=dsInsert;
                              Submit_CODIGO('0');
                          end;
                          if(STRGUnidadesRelacao.Cells[8,cont] = '            .')then
                          begin
                               Status:=dsEdit;
                               Submit_CODIGO(STRGUnidadesRelacao.Cells[7,cont]);
                          end;
                          
                          Submit_TIPOMATERIAL(COMBOTipo.Text);
                          Submit_UNIDADEMEDIDAORIGEM(STRGUnidadesRelacao.Cells[5,Cont]);
                          Submit_UNIDADEMEDIDACONVERSAO(STRGUnidadesRelacao.Cells[6,Cont]);
                          Submit_VALOR(STRGUnidadesRelacao.Cells[4,Cont]);
                          if(Salvar(False)=False)
                          then Exit;                           

                    end;
                    //Limpa linha
                    STRGUnidadesRelacao.Rows[cont].Clear;
                    
                    Inc(cont,1);

               end;
               FDataModulo.IBTransaction.CommitRetaining;

         end;
     finally
         CarregaRelacoesMedidas;
     end;
end;

procedure TFAcertaUnidades.btProcessarClick(Sender: TObject);
begin
    GravaRelacoes;
end;

procedure TFAcertaUnidades.CarregaRelacoesMedidas;
var
   query:TIBQuery;
   Query2:TIBQuery;
begin
     query:=TIBQuery.Create(nil);
     query.Database:=FDataModulo.IBDatabase;

     Query2:=TIBQuery.Create(nil);
     Query2.Database:=FDataModulo.IBDatabase;


     STRGUnidadesRelacao.RowCount:=2;
     STRGUnidadesRelacao.Rows[STRGUnidadesRelacao.Row].Clear;

     try
          with query do
          begin
                Close;
                sql.Clear;
                sql.Add('select tabrelacaomedidas.codigo,tabunidademedida.codigo  as codmedida,unidademedidaorigem,descricao,sigla,valor,unidademedidaconversao from tabrelacaomedidas');
                sql.add('join tabunidademedida on tabunidademedida.codigo=tabrelacaomedidas.unidademedidaorigem');
                sql.Add('where tipomaterial='+#39+COMBOTipo.Text+#39);
                Open;

                while not eof do
                begin
                     STRGUnidadesRelacao.Rows[STRGUnidadesRelacao.Row].Clear;
                     STRGUnidadesRelacao.Cells[7,STRGUnidadesRelacao.Row]:=fieldbyname('codigo').AsString;
                     STRGUnidadesRelacao.Cells[4,STRGUnidadesRelacao.Row]:=fieldbyname('valor').AsString;
                     STRGUnidadesRelacao.Cells[1,STRGUnidadesRelacao.Row]:=fieldbyname('descricao').AsString;
                     STRGUnidadesRelacao.Cells[0,STRGUnidadesRelacao.Row]:=fieldbyname('sigla').AsString;
                     STRGUnidadesRelacao.Cells[5,STRGUnidadesRelacao.Row]:=fieldbyname('codmedida').AsString;
                     query2.Close;
                     query2.SQL.Clear;
                     query2.SQL.Add('select * from tabunidademedida where codigo='+fieldbyname('unidademedidaconversao').AsString);
                     query2.Open;

                     STRGUnidadesRelacao.Cells[3,STRGUnidadesRelacao.Row]:=query2.fieldbyname('descricao').AsString;
                     STRGUnidadesRelacao.Cells[2,STRGUnidadesRelacao.Row]:=query2.fieldbyname('sigla').AsString;
                     STRGUnidadesRelacao.Cells[6,STRGUnidadesRelacao.Row]:=query2.fieldbyname('codigo').AsString;
                     STRGUnidadesRelacao.Cells[8,STRGUnidadesRelacao.Row]:='            x';

                     STRGUnidadesRelacao.RowCount:=STRGUnidadesRelacao.RowCount+1;
                     STRGUnidadesRelacao.Row:=STRGUnidadesRelacao.Row+1;
                     Next;
                end;
                if(STRGUnidadesRelacao.RowCount>2)
                then STRGUnidadesRelacao.RowCount:=STRGUnidadesRelacao.RowCount-1;
          end;
     finally
          FreeAndNil(query);
          FreeAndNil(Query2);
     end;

end;

procedure TFAcertaUnidades.Delete;
var
    nlinha: integer;
begin
      if(STRGUnidadesRelacao.Row=0)then
      begin
         exit;
      end;


      if STRGUnidadesRelacao.RowCount = 1 then
      begin
            STRGUnidadesRelacao.Cells[0,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[1,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[2,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[3,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[4,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[5,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[6,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[7,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[8,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[9,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[10,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[11,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[12,STRGUnidadesRelacao.Row] :='' ;
      end;
      if STRGUnidadesRelacao.RowCount = 2 then
      begin
            STRGUnidadesRelacao.Cells[0,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[1,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[2,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[3,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[4,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[5,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[6,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[7,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[8,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[9,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[10,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[11,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[12,STRGUnidadesRelacao.Row] :='' ;
            Exit;
      end;
      if STRGUnidadesRelacao.RowCount-1 = STRGUnidadesRelacao.Row then
      begin
            STRGUnidadesRelacao.Cells[0,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[1,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[2,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[3,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[4,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[5,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[6,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[7,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[8,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[9,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[10,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[11,STRGUnidadesRelacao.Row] := '';
            STRGUnidadesRelacao.Cells[12,STRGUnidadesRelacao.Row] :='' ;

      end;

     if (STRGUnidadesRelacao.RowCount > 1) and (STRGUnidadesRelacao.RowCount <> STRGUnidadesRelacao.Row) then
      begin

              {Se o numero de linha for igual ao numero de ao numero da linha ent�o ele simplesmente limpa as c�lulas, ou entao ele
              llimpa as celulas da linhas selecionada e copia o conteudo da proxima, e assim por diante ate chegar na ultima, que ser� excluida.}
              STRGUnidadesRelacao.Cells[0,STRGUnidadesRelacao.Row] := '';
              STRGUnidadesRelacao.Cells[1,STRGUnidadesRelacao.Row] := '';
              STRGUnidadesRelacao.Cells[2,STRGUnidadesRelacao.Row] := '';
              STRGUnidadesRelacao.Cells[3,STRGUnidadesRelacao.Row] := '';
              STRGUnidadesRelacao.Cells[4,STRGUnidadesRelacao.Row] := '';
              STRGUnidadesRelacao.Cells[5,STRGUnidadesRelacao.Row] := '';
              STRGUnidadesRelacao.Cells[6,STRGUnidadesRelacao.Row] := '';
              STRGUnidadesRelacao.Cells[7,STRGUnidadesRelacao.Row] := '';
              STRGUnidadesRelacao.Cells[8,STRGUnidadesRelacao.Row] := '';
              STRGUnidadesRelacao.Cells[9,STRGUnidadesRelacao.Row] := '';
              STRGUnidadesRelacao.Cells[10,STRGUnidadesRelacao.Row] := '';
              STRGUnidadesRelacao.Cells[11,STRGUnidadesRelacao.Row] := '';
              STRGUnidadesRelacao.Cells[12,STRGUnidadesRelacao.Row] :='' ;

              nlinha := STRGUnidadesRelacao.Row;

              while STRGUnidadesRelacao.Row <> STRGUnidadesRelacao.RowCount do
              begin
                  if STRGUnidadesRelacao.Cells[1,(nlinha + 1)] <> '' then
                    begin
                          STRGUnidadesRelacao.Cells[0,STRGUnidadesRelacao.Row] := STRGUnidadesRelacao.Cells[0,(nlinha +1)];
                          STRGUnidadesRelacao.Cells[1,STRGUnidadesRelacao.Row] := STRGUnidadesRelacao.Cells[1,(nlinha +1)];
                          STRGUnidadesRelacao.Cells[2,STRGUnidadesRelacao.Row] := STRGUnidadesRelacao.Cells[2,(nlinha +1)];
                          STRGUnidadesRelacao.Cells[3,STRGUnidadesRelacao.Row] := STRGUnidadesRelacao.Cells[3,(nlinha +1)];
                          STRGUnidadesRelacao.Cells[4,STRGUnidadesRelacao.Row] := STRGUnidadesRelacao.Cells[4,(nlinha +1)];
                          STRGUnidadesRelacao.Cells[5,STRGUnidadesRelacao.Row] := STRGUnidadesRelacao.Cells[5,(nlinha +1)];
                          STRGUnidadesRelacao.Cells[6,STRGUnidadesRelacao.Row] := STRGUnidadesRelacao.Cells[6,(nlinha +1)];
                          STRGUnidadesRelacao.Cells[7,STRGUnidadesRelacao.Row] := STRGUnidadesRelacao.Cells[7,(nlinha +1)];
                          STRGUnidadesRelacao.Cells[8,STRGUnidadesRelacao.Row] := STRGUnidadesRelacao.Cells[8,(nlinha +1)];
                          STRGUnidadesRelacao.Cells[9,STRGUnidadesRelacao.Row] := STRGUnidadesRelacao.Cells[9,(nlinha +1)];
                          STRGUnidadesRelacao.Cells[10,STRGUnidadesRelacao.Row] := STRGUnidadesRelacao.Cells[10,(nlinha +1)];
                          STRGUnidadesRelacao.Cells[11,STRGUnidadesRelacao.Row] := STRGUnidadesRelacao.Cells[11,(nlinha +1)];
                          STRGUnidadesRelacao.Cells[12,STRGUnidadesRelacao.Row] := STRGUnidadesRelacao.Cells[12,(nlinha +1)];
                    end
                  else
                  begin
                          STRGUnidadesRelacao.RowCount := STRGUnidadesRelacao.RowCount - 1;
                          STRGUnidadesRelacao.Cells[0,nlinha] := '';
                          STRGUnidadesRelacao.Cells[1,nlinha] := '';
                          STRGUnidadesRelacao.Cells[2,nlinha] := '';
                          STRGUnidadesRelacao.Cells[3,nlinha] := '';
                          STRGUnidadesRelacao.Cells[4,nlinha] := '';
                          STRGUnidadesRelacao.Cells[5,nlinha] := '';
                          STRGUnidadesRelacao.Cells[6,nlinha] := '';
                          STRGUnidadesRelacao.Cells[7,nlinha] := '';
                          STRGUnidadesRelacao.Cells[8,nlinha] := '';
                          STRGUnidadesRelacao.Cells[9,nlinha] := '';
                          STRGUnidadesRelacao.Cells[10,nlinha] := '';
                          STRGUnidadesRelacao.Cells[11,nlinha] := '';
                          STRGUnidadesRelacao.Cells[12,nlinha] := '';
                          exit;

                  end;
                  STRGUnidadesRelacao.Row := STRGUnidadesRelacao.Row + 1;
                  nlinha := STRGUnidadesRelacao.Row;
              end;
      end;
end;


procedure TFAcertaUnidades.STRGUnidadesRelacaoDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
        if((ARow <> 0) and (ACol = 8))
        then begin
            if(STRGUnidadesRelacao.Cells[ACol,ARow] = '            x' ) then
            begin
                 Ilprodutos.Draw(STRGUnidadesRelacao.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 0); //check box marcado       6
            end
            else
            begin
                if(STRGUnidadesRelacao.Cells[ACol,ARow] = '' ) then
                begin
                     Ilprodutos.Draw(STRGUnidadesRelacao.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 5); //check box sem marcar    5
                end
                else
                begin
                      if(STRGUnidadesRelacao.Cells[ACol,ARow] = '            .' ) then
                      begin
                           Ilprodutos.Draw(STRGUnidadesRelacao.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 2); //check box marcado       6
                      end
                      else
                      begin
                            if(STRGUnidadesRelacao.Cells[ACol,ARow] = '            ,' ) then
                            begin
                                 Ilprodutos.Draw(STRGUnidadesRelacao.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 6); //check box marcado       6
                            end
                      end;
                end;
            end;
        end;
end;

procedure TFAcertaUnidades.ExcluirClick(Sender: TObject);
begin
    if(STRGUnidadesRelacao.Cells[7,STRGUnidadesRelacao.Row]<>'') then
    begin
          if(ObjrelacaoMedidas.Exclui(STRGUnidadesRelacao.Cells[7,STRGUnidadesRelacao.Row],False)=False) then
          begin
                  MensagemErro('Erro ao tentar excluir registro da linha '+IntToStr(STRGUnidadesRelacao.Row));
                  Exit;
          end;
    end;
    Delete;
end;

procedure TFAcertaUnidades.Alterar;
begin
    if(STRGUnidadesRelacao.Cells[8,STRGUnidadesRelacao.row] <> '')then
    begin
        STRGUnidadesRelacao.Cells[8,STRGUnidadesRelacao.Row]:= '            .';
    end;
end;


procedure TFAcertaUnidades.Alterar1Click(Sender: TObject);
begin
    Alterar;
end;

procedure TFAcertaUnidades.VerificaDadosInseridos;
var
  Cont:Integer;
begin
    cont:=1;
    while Cont<STRGUnidadesRelacao.RowCount do
    begin
        if(STRGUnidadesRelacao.Cells[0,STRGUnidadesRelacao.Row]=STRGUnidadesRelacao.Cells[0,cont])and
        (STRGUnidadesRelacao.Cells[2,STRGUnidadesRelacao.Row]=STRGUnidadesRelacao.Cells[2,cont])and
        (STRGUnidadesRelacao.Row<>cont) then
        begin
            MensagemAviso('A rela��o entre '+STRGUnidadesRelacao.Cells[0,STRGUnidadesRelacao.Row]+ ' e '+STRGUnidadesRelacao.Cells[2,STRGUnidadesRelacao.Row]+' ja existe!!!');
            STRGUnidadesRelacao.Cells[8,STRGUnidadesRelacao.row] := '            ,';
            Exit;
        end;
        Inc(Cont,1);
    end;
    //STRGUnidadesRelacao.Cells[8,STRGUnidadesRelacao.row] := '';
end;

procedure TFAcertaUnidades.COMBOTipoChange(Sender: TObject);
begin
    STRGUnidadesRelacao.SetFocus;

    CarregaRelacoesMedidas;
end;

end.
