unit UAlteraPrecoVidro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,UobjvidroCor, ExtCtrls, Buttons;

type
  TFAlteraPrecoVidro = class(TForm)
    GroupBoxMargem: TGroupBox;
    LbPorcentagemFornecido: TLabel;
    LbPorcentagemRetirado: TLabel;
    LbPorcentagemInstalado: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdtPorcentagemInstalado: TEdit;
    EdtPorcentagemFornecido: TEdit;
    EdtPorcentagemRetirado: TEdit;
    EdtPrecoCusto: TEdit;
    LbPrecoCusto: TLabel;
    GroupBoxPrecoVenda: TGroupBox;
    LbPrecoVendaInstalado: TLabel;
    LbPrecoVendaFornecido: TLabel;
    LbPrecoVendaRetirado: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdtPrecoVendaInstalado: TEdit;
    EdtPrecoVendaFornecido: TEdit;
    EdtPrecoVendaRetirado: TEdit;
    Label1: TLabel;
    lbprecopago: TLabel;
    Panel1: TPanel;
    Label8: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Rg_forma_de_calculo_percentual: TRadioGroup;
    edtinstaladofinal: TEdit;
    edtfornecidofinal: TEdit;
    edtretiradofinal: TEdit;
    btgravar: TButton;
    btcancelar: TButton;
    GroupBoxVidros: TGroupBox;
    LbPorcentagemAcrescimo: TLabel;
    LbAcrescimoExtra: TLabel;
    LbPorcentagemAcrescimoFinal: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    EdtPorcentagemAcrescimo: TEdit;
    EdtAcrescimoExtra: TEdit;
    EdtPorcentagemAcrescimoFinal: TEdit;
    EdtValorExtra: TEdit;
    edtacrescimoicms: TEdit;
    edtporcentagemacrescimoicms: TEdit;
    procedure FormShow(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure EdtPorcentagemInstaladoExit(Sender: TObject);
    procedure EdtPorcentagemFornecidoExit(Sender: TObject);
    procedure EdtPorcentagemRetiradoExit(Sender: TObject);
    procedure EdtPrecoVendaInstaladoExit(Sender: TObject);
    procedure EdtPrecoVendaFornecidoExit(Sender: TObject);
    procedure EdtPrecoVendaRetiradoExit(Sender: TObject);
    procedure EdtAcrescimoExtraExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtValorExtraExit(Sender: TObject);
    procedure edtacrescimoicmsExit(Sender: TObject);
    procedure edtporcentagemacrescimoicmsExit(Sender: TObject);
  private
    { Private declarations }
        AlteraPrecoPeloCusto:Boolean;

  public
    { Public declarations }
    ObjvidroCor:TobjvidroCor;
  end;

var
  FAlteraPrecoVidro: TFAlteraPrecoVidro;

implementation

uses UDataModulo, UessencialGlobal;

{$R *.dfm}

procedure TFAlteraPrecoVidro.FormShow(Sender: TObject);
begin
    Self.Tag:=0;

     AlteraPrecoPeloCusto:=True;

    if (ObjParametroGlobal.ValidaParametro('CALCULA PRECO DE VENDA DO VIDRO PELO CUSTO E PERCENTUAIS?')=True)
    then Begin
               if (ObjParametroGlobal.get_valor<>'SIM')
               then AlteraPrecoPeloCusto:=false;
    End;

    if (AlteraPrecoPeloCusto=True)
    Then Begin
              GroupBoxPrecoVenda.Enabled:=False;
              GroupBoxMargem.Enabled:=True;
    End
    Else BEgin
              GroupBoxPrecoVenda.Enabled:=True;
              GroupBoxMargem.Enabled:=False;
    End;

    Rg_forma_de_calculo_percentual.itemindex:=0;
    if (ObjParametroGlobal.ValidaParametro('FORMA DE CALCULO DE PERCENTUAL DE ACRESCIMO NAS CORES DO VIDRO')=True)
    then Begin
               if (ObjParametroGlobal.get_valor='1')
               then Rg_forma_de_calculo_percentual.itemindex:=1;
    End;
    Rg_forma_de_calculo_percentual.enabled:=False;
End;

procedure TFAlteraPrecoVidro.btgravarClick(Sender: TObject);
begin
     Self.Tag:=1;
     Self.Close;
end;

procedure TFAlteraPrecoVidro.btcancelarClick(Sender: TObject);
begin
     Self.tag:=0;
     Self.close;
end;

procedure TFAlteraPrecoVidro.EdtPorcentagemInstaladoExit(
  Sender: TObject);
Var
    PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaInstalado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemInstalado.Text))/100)));
end;


procedure TFAlteraPrecoVidro.EdtPorcentagemFornecidoExit(
  Sender: TObject);
Var
    PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaFornecido.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemFornecido.Text))/100)));
end;

procedure TFAlteraPrecoVidro.EdtPorcentagemRetiradoExit(Sender: TObject);
Var
    PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaRetirado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemRetirado.Text))/100)));
end;


procedure TFAlteraPrecoVidro.EdtPrecoVendaInstaladoExit(Sender: TObject);
Var
  PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemInstalado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaInstalado.Text))/precocusto));
end;

procedure TFAlteraPrecoVidro.EdtPrecoVendaFornecidoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemfornecido.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendafornecido.Text))/precocusto));

end;
procedure TFAlteraPrecoVidro.EdtPrecoVendaRetiradoExit(Sender: TObject);
Var
  PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemretirado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaretirado.Text))/precocusto));
end;

procedure TFAlteraPrecoVidro.EdtAcrescimoExtraExit(Sender: TObject);
Var PValorCusto, PAcrescimoExtra, PValorExtra :Currency;
begin
    if ((TEdit(Sender).Text=''))then
    exit;

    PValorCusto:=0;
    PAcrescimoExtra:=0;
    PValorExtra:=0;

    try
          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));

          PAcrescimoExtra:=StrTOCurr(tira_ponto(EdtAcrescimoExtra.Text));
          PValorExtra:=((PValorCusto*PAcrescimoExtra)/100);
          EdtValorExtra.Text:=Formata_Valor(PValorExtra);
    except;
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais.');
    end;

    EdtPorcentagemAcrescimoFinal.Text:=CurrToStr(StrToCurr(tira_ponto(EdtPorcentagemAcrescimo.Text))+StrToCurr(tira_ponto(EdtAcrescimoExtra.Text))+
                                            StrToCurr(tira_ponto(edtporcentagemacrescimoicms.Text)));

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(tira_ponto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));

end;

procedure TFAlteraPrecoVidro.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);

end;

procedure TFAlteraPrecoVidro.EdtValorExtraExit(Sender: TObject);
Var
PValorCusto, PPorcentagemExtra, PValorExtra : Currency;
begin

    if ((TEdit(Sender).Text=''))then
    exit;

    TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);

    PValorCusto:=0;
    PValorExtra:=0;
    PPorcentagemExtra:=0;

    try


          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));

          PValorExtra:=StrToCurr(Tira_Ponto(EdtValorExtra.Text));

          PPorcentagemExtra:=(PValorExtra*100)/PValorCusto;
          EdtAcrescimoExtra.Text:=FormatFloat('#,##0.00',PPorcentagemExtra);

          if (edtacrescimoicms.text='') and (edtporcentagemacrescimoicms.text='')
          then Begin
                    edtacrescimoicms.text:='0';
                    edtporcentagemacrescimoicms.text:='0';
          End;

    except
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais');
    end;



    
    //********** Atualiza Valor Final, s� pra mostra porque este campo � Computed By
    EdtPorcentagemAcrescimoFinal.Text :=formata_valor(StrToCurr(Tira_Ponto(EdtPorcentagemAcrescimo.Text))+
                                      StrToCurr(tira_ponto(EdtAcrescimoExtra.Text))+
                                      StrToCurr(tira_ponto(edtporcentagemacrescimoicms.Text))
                                      );

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(Tira_POnto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));

(*

Antes era calculado sobre o Custo.
Exemplo
O Vidro incolor custa 100,00, sendo assim
os demais ser�o calculados sobre ele
ent�o adiciona-se por exemplo
R$ 30,00 no custo para o vidro verde
sendo assim o calculo deveria ser
custo R$ 130 + %de instalado retirado ou fornecido

tomando com exemplo 10% de percentual

R$ 130 +104% = 265,20

Entao devo na cor devo calcular assim

R$ 30,00 de aumento representam 30% de aumento

sendo assim R$ (100*104%) = 204+30%+(61,20)=265,20 � o preco final do video verde





    if ((TEdit(Sender).Text=''))then
    exit;

    TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);

    PValorCusto:=0;
    PValorExtra:=0;
    PPorcentagemExtra:=0;

    try
          PValorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.Text));
          PValorExtra:=StrToCurr(Tira_Ponto(EdtValorExtra.Text));
          PPorcentagemExtra:=(PValorExtra*100)/PValorCusto;
          EdtAcrescimoExtra.Text:=FormatFloat('#,##0.00',PPorcentagemExtra);
    except
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais');
    end;
    //********** Atualiza Valor Final, s� pra mostra poruque este campo � Computed By
    EdtPorcentagemAcrescimoFinal.Text :=formata_valor(StrToCurr(Tira_Ponto(EdtPorcentagemAcrescimo.Text))+
                                      StrToCurr(tira_ponto(EdtAcrescimoExtra.Text))+
                                      StrToCurr(tira_ponto(edtporcentagemacrescimoicms.Text))
                                      );

    EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(Tira_POnto(EdtPorcentagemAcrescimoFinal.Text))/100));




*)

end;


procedure TFAlteraPrecoVidro.edtacrescimoicmsExit(Sender: TObject);
Var
PValorCusto, PPorcentagemExtraICMS, PValorExtraICMS : Currency;
begin
    if ((TEdit(Sender).Text=''))
    then exit;

    TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);

    PValorCusto:=0;
    PValorExtraICMS:=0;
    PPorcentagemExtraICMS:=0;

    try

          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));

          PValorExtraICMS:=StrToCurr(Tira_Ponto(edtacrescimoicms.Text));
          
          PPorcentagemExtraICMS:=(PValorExtraICMS*100)/PValorCusto;
          edtporcentagemacrescimoicms.Text:=FormatFloat('#,##0.00',PPorcentagemExtraICMS);
    except
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais');
    end;
    //********** Atualiza Valor Final, s� pra mostra poruque este campo � Computed By
    EdtPorcentagemAcrescimoFinal.Text :=formata_valor(StrToCurr(Tira_Ponto(EdtPorcentagemAcrescimo.Text))+
                                      StrToCurr(tira_ponto(EdtAcrescimoExtra.Text))+
                                      StrToCurr(tira_ponto(edtporcentagemacrescimoicms.Text))
                                      );

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(Tira_POnto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));

end;

procedure TFAlteraPrecoVidro.edtporcentagemacrescimoicmsExit(
  Sender: TObject);
Var
PValorCusto, PAcrescimoICMS, PporcentagemICMS :Currency;
begin
    if ((TEdit(Sender).Text=''))then
    exit;

    PValorCusto:=0;
    PAcrescimoICMS:=0;
    PporcentagemICMS:=0;

    try
          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));


          PporcentagemICMS:=StrTOCurr(tira_ponto(edtporcentagemacrescimoicms.Text));
          PAcrescimoICMS:=((PValorCusto*PporcentagemICMS)/100);
          edtacrescimoicms.Text:=Formata_Valor(PAcrescimoICMS);
    except;
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais.');
    end;

    EdtPorcentagemAcrescimoFinal.Text:=CurrToStr(StrToCurr(tira_ponto(EdtPorcentagemAcrescimo.Text))+StrToCurr(tira_ponto(EdtAcrescimoExtra.Text))+
                                       StrToCurr(tira_ponto(edtporcentagemacrescimoicms.Text)));

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(tira_ponto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
end;

end.
