Unit UobjNotaFiscalObjetos;

Interface
Uses
stdctrls,rdprint,dialogs,Grids, Ibquery,windows,Classes,Db,UessencialGlobal,
SysUtils, Messages,  Graphics, Controls,
ExtCtrls, Forms,
UObjFerragem_NF,UObjPerfilado_NF,UObjVidro_NF,UObjKitBox_NF,UObjPersiana_NF,UObjDiverso_NF,uobjnotafiscalcfop,
uobjgeranfe,UObjVidro_ICMS,
UobjFerragem_ICMS,UobjPerfilado_ICMS,UobjKitbox_ICMS,UobjPersiana_ICMS,UObjDiverso_ICMS,UobjMaterial_NF,
uobjie_st_estado,ObjMateriaisVenda,pcnConversao,UObjNotaFiscal,UObjNfeDigitada,UobjCOBRDUPLICATA,XMLDoc;



Type

   TModoConversaoNfe=(MC_Arredonda,MC_Copia2);

   TObjNotafiscalObjetos=class
   
          Public

                ObjNotaFiscalCfop   :TObjNotaFiscalCfop;
                ObjFerragem_NF      :TObjFERRAGEM_NF;
                ObjPerfilado_NF     :TObjPerfilado_NF;
                ObjVidro_NF         :TObjVidro_NF;
                ObjKitBox_NF        :TObjKITBOX_NF;
                ObjPersiana_NF      :TObjPERSIANA_NF;
                ObjDiverso_NF       :TObjDIVERSO_NF;

                ObjVidro_ICMS:TobjVidro_ICMS;
                ObjFerragem_ICMS:TobjFerragem_ICMS;
                ObjPerfilado_ICMS:TobjPerfilado_ICMS;
                ObjKitbox_ICMS:TobjKitbox_ICMS;
                ObjPersiana_ICMS:TobjPersiana_ICMS;
                ObjDiverso_ICMS:TobjDiverso_ICMS;
                CobrDuplicata:TObjCOBRDUPLICATA;

                 devolucao:Boolean;
                 NfeEntrada:Boolean;

                {referencia}
                cUF_ref:string;
                AAMM_ref:string;
                CNPJ_ref:string;
                modelo_ref:string;
                serie_ref:string;
                nNF_ref:string;
                refNFe_ref:string;
                adicional_ref:string;

                //ObjNfeObjetos:TObjNFEObjetos;
                objgeranfe:Tobjgeranfe;

                ObjIe_St_Estado:TobjIe_st_estado;

                Constructor Create(Owner:TComponent);
                Destructor  Free;
                procedure  Atualizagrid(PStringGrid: TStringGrid; PNotaFiscal: string;PtipoColunas: TStrings);
                procedure  ImprimeNFRDPRINT(Pnota: string);

                //*********************antiga*******************
                procedure ImprimeNFRDPRINT_antiga(Pnota: string);

                Procedure  Opcoes(Pnotafiscal:string);
                Procedure  ImprimeItensNF(Pmaterial:String);
                procedure EdtLocalizaProdutoReferenciaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;Ptipo,PnotaFiscal:String);
                Function ImportaPedido(PnotaFiscal:string):boolean;
                procedure GeraNF(PnotaFiscal: string;Pedido:string;Operacao:string;valorpedido:string);
                procedure ImprimeNFRDPRINT_NOTADIGITADAM1(Pnota: string);

                function CalculaBaseDeCalculoNovaForma(CodigoVenda:string;ValorDesconto:string;PnfeDigitada:string = ''):Boolean;

                procedure ImprimeNotaFiscal(Pnota:string);
                //procedure GeraNotaFiscal(PnotaFiscal:string;Pedido:string;Operacao:string;valorpedido:string);
                function  GeraNotaFiscalNova(PnotaFiscal:TStringList;pedido:string;operacao:string;Corte:Integer;PnfeDigitada:string = ''):Boolean;

                function GeraNotaFiscalParaNFE(PnotaFiscal:TStringList;pedido:string;operacao:string;Corte:Integer):Boolean;
                function geraNFE_2 (Pnota:TStringList; pedido: string; PmodoContigencia,PForcaProcessamento: boolean;PnfeDigitada:string = ''): boolean;

                procedure AtualizavaloresTotaisNotaFiscal(Pnota:string);
                procedure CalculaPesoNovaForma(Pnota:string);
                procedure GravaProdutosNF(Ppedido:string;Pnota:string);
                function GravaProdutosNF_NfeDigitada(PNfeDigitada,Pnota:string):Boolean;
                function get_proximaNFE():string; {retorna o codigo da proxima nfe aberta para emiss�o}
                procedure RelacaodeNotasFiscais(Nota:String);
                procedure edtclienteKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure edtnotafiscalKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);

                function verificaNotasAbertas(pNumero: string): Boolean;
                function vinculaNFe_ao_pedido(XMLDocument1:TXMLDocument; codigoCliente,codigoPedido:string):Boolean;

         Private
               Objquery:Tibquery;
               ObjqueryTEMP:Tibquery;
               ModoArredondamento:TModoConversaoNfe;
               Owner:TComponent;

               Procedure AdicionaCfop(Pnotafiscal:string);
               Function  CalculaBasedeCalculo(pnota:string):boolean;
               function  CalculaBasedeCalculo_NOVA(pnota: string): boolean;
               Function  CalculaPeso(pnota:string):boolean;
               Function  CalculaValorTotal(pnota:string):boolean;

               Function  GeraNfe(Pnota:string;PmodoContigencia:boolean;PForcaProcessamento:boolean):boolean;overload;
               Function  GeraNfe(Pnota:string):boolean;overload;

               Function  MenuNfe:boolean;

               Function  Arredonda_NFE(PValor:Currency):Currency;
               Procedure ReimprimeNfe(Pnotafiscal:string);
               Procedure CancelaNfe(Pnotafiscal:string);
               procedure VerificaRetorno(Pnotafiscal: string);
               procedure SalvaXML(Pnotafiscal: string);
               procedure EnviaNfe_Contingencia(Pnotafiscal: string);
               Procedure ExportaCucaFresca;
               procedure LimpaNF(NotaFiscal:string);

               function CalculaDifal(pUF_Destino: string;
                pBC_Icms: Currency; var pPERCENTUAL_ICMS_UF_DEST, pVALOR_ICMS_UF_DEST,
                pVALOR_ICMS_UF_REMET, pPERCENTUAL_ICMS_INTER,
                pPERCENTUAL_ICMS_INTER_PART: currency):boolean;

               function AjustaDescontos(DescontoTotal:Currency;pedido:string):Currency;
               function AjustaAcrescimos(AcrescimoTotal:Currency;pedido:string):Currency;

               function GET_ST_TABELA_A           (orig:string):TpcnOrigemMercadoria;
               function GET_ST_TABELA_B           (CST:string):TpcnCSTIcms;
               function GET_MODALIDADE_BC_ICMS_ST (modBCST :integer):TpcnDeterminacaoBaseIcmsST;
               function GET_MODALIDADE_BC         (ModBC:integer):TpcnDeterminacaoBaseIcms;
               function GET_CST_PIS               (cst:integer):TpcnCstPis;
               function GET_CST_IPI               (cst:integer):TpcnCstIpi;
               function GET_CST_COFINS            (cst:integer):TpcnCstCofins;
               function Valida_dados_NFE          (PObjnotaFiscal: TObjNotaFiscal;pPedido:string;pnfeDigitada:string = ''): Boolean;

               function get_crt(tipoCRT: string): TpcnCRT;
               function get_TIPOCSOSN(tipoCSOSN: string): TpcnCSOSNIcms;


   End;


implementation

uses UDataModulo, UrelNotaFiscalRdPrint, UopcaoRel, UFiltraImp,
  UReltxtRDPRINT, UMostraBarraProgresso, UobjNFE, UmenuNFE,
  UmostraStringList, UCalculaformula_Vidro, UobjIMPOSTO_ICMS,
  UobjIMPOSTO_IPI, UobjIMPOSTO_PIS, UComponentesNfe,
  UobjEMPRESA, UobjCLIENTE, UObjCFOP, pcnNFe, UobjFORNECEDOR, Upesquisa,
  UobjPedidoObjetos, UerrosNFE, UescolheTransportadora, UobjTransmiteNFE,
  UalteraFaturas, UobjIMPOSTO_COFINS, UobjExtraiXML;

constructor TObjNotafiscalObjetos.Create(Owner:TComponent);
begin
  With Self do
  Begin
      Self.Owner := owner;
      Objquery:=Tibquery.create(nil);
      Objquery.database:=FdataMOdulo.Ibdatabase;
      ObjqueryTEMP:=Tibquery.create(nil);
      ObjqueryTEMP.database:=FdataMOdulo.Ibdatabase;

      ObjNotaFiscalCfop   :=TObjNotaFiscalCfop.create;
      ObjFerragem_NF      :=TObjFERRAGEM_NF.create(Self.Owner);
      ObjPerfilado_NF     :=TObjPerfilado_NF.create(Self.Owner);
      ObjVidro_NF         :=TObjVidro_NF.create(self.Owner);
      ObjKitBox_NF        :=TObjKITBOX_NF.create(Self.Owner);
      ObjPersiana_NF      :=TObjPERSIANA_NF.create(Self.Owner);
      ObjDiverso_NF       :=TObjDIVERSO_NF.create(Self.Owner);

      //ObjNfeObjetos       :=TObjNFEObjetos.create;
      objgeranfe          := Tobjgeranfe.create;

      ObjVidro_ICMS:=TobjVidro_ICMS.create(Self.Owner);
      ObjFerragem_ICMS:=TobjFerragem_ICMS.Create(Self.Owner);
      ObjPerfilado_ICMS:=TobjPerfilado_ICMS.Create(Self.Owner);
      ObjKitbox_ICMS:=TobjKitbox_ICMS.Create(Self.Owner);
      ObjPersiana_ICMS:=TobjPersiana_ICMS.Create(Self.Owner);
      ObjDiverso_ICMS:=TobjDiverso_ICMS.Create(Self.Owner);

      ObjIe_St_Estado:=TobjIe_st_estado.create;
      self.CobrDuplicata:=TObjCOBRDUPLICATA.Create;

      Self.ModoArredondamento:=MC_Arredonda;

      if (ObjParametroGlobal.ValidaParametro('MODO DE ARREDONDAMENTO IMPOSTOS NF (0-ARREDONDAMENTO,1-ELIMINA TERC CASA)')=True)
      then Begin
                if (ObjParametroGlobal.Get_Valor='1')
                Then Self.ModoArredondamento:=MC_Copia2; 
      End;

  End;

  self.NfeEntrada := False;
  self.devolucao  := False;

end;

destructor TObjNotafiscalObjetos.Free;
begin
     With Self do
     Begin
          ObjNotaFiscalCfop.free;
          ObjFerragem_NF.free;
          ObjPerfilado_NF.free;
          ObjVidro_NF.free;
          ObjKitBox_NF.free;
          ObjPersiana_NF.free;
          ObjDiverso_NF.free;
          Freeandnil(Self.Objquery);
          Freeandnil(Self.ObjqueryTEMP);
          //ObjNfeObjetos.free;
          Objgeranfe.free;
          ObjVidro_ICMS.free;
          ObjFerragem_ICMS.free;
          ObjPerfilado_ICMS.free;
          ObjKitbox_ICMS.free;
          ObjPersiana_ICMS.free;
          ObjDiverso_ICMS.free;
          ObjIe_St_Estado.free;
          CobrDuplicata.Free;
     End;
end;

procedure TobjNOtaFiscalObjetos.Atualizagrid(PStringGrid: TStringGrid;
  PNotaFiscal: string;PtipoColunas:TStrings);
var
cont:integer;
begin
     PStringGrid.ColCount:=2;
     PStringGrid.RowCount:=2;
     PStringGrid.FixedRows:=1;
     PStringGrid.FixedCols:=0;
     PStringGrid.Cols[0].clear;
     PStringGrid.cols[1].clear;

     if (PNotaFiscal='')
     Then Exit;

     If (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(PNotaFiscal)=False)
     then Begin
               Messagedlg('Nota Fiscal n�o localizada',mterror,[mbok],0);
               exit;
     End;

     Self.CalculaValorTotal(PNotaFiscal);

     //titulo das colunas
     PStringGrid.ColCount:=31;
     For cont:=0 to PStringGrid.colcount-1 do
     Begin
          PStringGrid.cols[cont].clear;
     End;

     ptipocolunas.clear;

     ptipocolunas.add('STRING');
     PStringGrid.CELLS[0,0]:='TIPO';
     ptipocolunas.add('string');
     PStringGrid.Cells[1,0]:='PRODUTO';
     ptipocolunas.add('string');
     PStringGrid.CELLS[2,0]:='COR';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[3,0]:='QUANTIDADE';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[4,0]:='VALOR';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[5,0]:='VALOR FINAL';
     ptipocolunas.add('integer');
     PStringGrid.CELLS[6,0]:='CODIGO';
     ptipocolunas.add('integer');
     PStringGrid.CELLS[7,0]:='CODIGOPRODUTO';
     
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[8,0]:='VL.FRETE';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[9,0]:='VL.SEGURO';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[10,0]:='BC_ICMS';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[11,0]:='VL_ICMS';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[12,0]:='BC_ICMS_ST';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[13,0]:='VL_ICMS_ST';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[14,0]:='BC_IPI';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[15,0]:='VL_IPI';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[16,0]:='BC_PIS';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[17,0]:='VL_PIS';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[18,0]:='BC_PIS_ST';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[19,0]:='VL_PIS_ST';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[20,0]:='BC_COFINS';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[21,0]:='VL_COFINS';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[22,0]:='BC_COFINS_ST';
     ptipocolunas.add('decimal');
     PStringGrid.CELLS[23,0]:='VL_COFINS_ST';
     ptipocolunas.add('integer');
     PStringGrid.CELLS[24,0]:='IMPOSTO_ICMS_ORIGEM';
     ptipocolunas.add('integer');
     PStringGrid.CELLS[25,0]:='IMPOSTO_ICMS_DESTINO';
     ptipocolunas.add('integer');
     PStringGrid.CELLS[26,0]:='IMPOSTO_IPI';
     ptipocolunas.add('integer');
     PStringGrid.CELLS[27,0]:='IMPOSTO_PIS_ORIGEM';
     ptipocolunas.add('integer');
     PStringGrid.CELLS[28,0]:='IMPOSTO_PIS_DESTINO';
     ptipocolunas.add('integer');
     PStringGrid.CELLS[29,0]:='IMPOSTO_COFINS_ORIGEM';
     ptipocolunas.add('integer');
     PStringGrid.CELLS[30,0]:='IMPOSTO_COFINS_DESTINO';






     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select  * from ProcRetornaMateriais_NF('+PNotaFiscal+')');
          open;
          While not(eof) do
          Begin
               PStringGrid.Cells[0,PStringGrid.rowcount-1]:=Fieldbyname('TIpo').asstring;
               PStringGrid.Cells[1,PStringGrid.rowcount-1]:=Fieldbyname('produto').asstring;
               PStringGrid.Cells[2,PStringGrid.rowcount-1]:=Fieldbyname('cor').asstring;
               PStringGrid.Cells[3,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('quantidade').asstring),12,' ');
               PStringGrid.Cells[4,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valor').asstring),12,' ');
               PStringGrid.Cells[5,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valorfinal').asstring),12,' ');
               PStringGrid.Cells[6,PStringGrid.rowcount-1]:=Fieldbyname('codigo').asstring;
               PStringGrid.Cells[7,PStringGrid.rowcount-1]:=Fieldbyname('codigoproduto').asstring;
               PStringGrid.Cells[8,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valorfrete').asstring),12,' ');
               PStringGrid.Cells[9,PStringGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valorseguro').asstring),12,' ');
               PStringGrid.CELLS[10,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('BC_ICMS').asstring),12,' ');
               PStringGrid.CELLS[11,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('VALOR_ICMS').asstring),12,' ');
               PStringGrid.CELLS[12,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('BC_ICMS_ST').asstring),12,' ');
               PStringGrid.CELLS[13,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('VALOR_ICMS_ST').asstring),12,' ');
               PStringGrid.CELLS[14,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('BC_IPI').asstring),12,' ');
               PStringGrid.CELLS[15,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('VALOR_IPI').asstring),12,' ');
               PStringGrid.CELLS[16,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('BC_PIS').asstring),12,' ');
               PStringGrid.CELLS[17,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('VALOR_PIS').asstring),12,' ');
               PStringGrid.CELLS[18,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('BC_PIS_ST').asstring),12,' ');
               PStringGrid.CELLS[19,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('VALOR_PIS_ST').asstring),12,' ');
               PStringGrid.CELLS[20,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('BC_COFINS').asstring),12,' ');
               PStringGrid.CELLS[21,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('VALOR_COFINS').asstring),12,' ');
               PStringGrid.CELLS[22,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('BC_COFINS_ST').asstring),12,' ');
               PStringGrid.CELLS[23,PStringGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('VALOR_COFINS_ST').asstring),12,' ');
               PStringGrid.CELLS[24,PStringGrid.RowCount-1]:=Fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring;
               PStringGrid.CELLS[25,PStringGrid.RowCount-1]:=Fieldbyname('IMPOSTO_ICMS_DESTINO').asstring;
               PStringGrid.CELLS[26,PStringGrid.RowCount-1]:=Fieldbyname('IMPOSTO_IPI').asstring;
               PStringGrid.CELLS[27,PStringGrid.RowCount-1]:=Fieldbyname('IMPOSTO_PIS_ORIGEM').asstring;
               PStringGrid.CELLS[28,PStringGrid.RowCount-1]:=Fieldbyname('IMPOSTO_PIS_DESTINO').asstring;
               PStringGrid.CELLS[29,PStringGrid.RowCount-1]:=Fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring;
               PStringGrid.CELLS[30,PStringGrid.RowCount-1]:=Fieldbyname('IMPOSTO_COFINS_DESTINO').asstring;
               PStringGrid.RowCount:=PStringGrid.rowcount+1;
               next;
          End;
     End;

     if not((PStringGrid.RowCount=2) and (PStringGrid.cells[5,1]=''))//ta vazio o grid
     then PStringGrid.RowCount:=PStringGrid.rowcount-1;

     AjustaLArguraColunaGrid(PStringGrid);
End;


procedure TObjNotaFiscalObjetos.ImprimeNFRDPRINT_antiga(Pnota: string);
var
  EspacoInicial,TamanhoCampo,EspacoCampo,cont,cont3,quant_campos_produto:Integer;

  Pcfop,PNaturezaOperacao,Temp,NomeCampo,Linha:String;
  Qtemp:Tibquery;
  Pisento,PSubstituicao,Paliquota:String;
  temp2:String;
  StrStringList:TStringList;
  TrocaIcmsSubstituicao_recolhido:boolean;

Begin

     Try
         Qtemp:=Tibquery.create(nil);
         Qtemp.database:=Self.Objquery.database;
     Except
           messagedlg('Erro na cria��o de objetos!',mterror,[mbok],0);
     End;

     Try
        StrStringList:=TStringList.Create;
     Except
           MensagemErro('Erro na Cria��o da StringList');
           freeandnil(qtemp);
     end;

Try//try finally da QTemp e da StrStringList

     If (Pnota='')
     Then Begin
               MensagemAviso('Escolha uma Nota Fiscal');
               exit;
     End;

     //Localiza a NF
     If (Self.ObjNotaFiscalCFOP.NOTAFISCAL.LocalizaCodigo(Pnota)=False)
     Then Begin
              Messagedlg('Nota Fiscal n�o Localizada!',mterror,[mbok],0);
              exit;
     End;
     Self.ObjNotaFiscalCFOP.NotaFiscal.TabelaparaObjeto;


     
     if (Self.CalculaBasedeCalculo(pnota)=False)
     Then exit;

     if (self.CalculaPeso(pnota)=False)
     then exit;


     //Localiza a NF
     If (Self.ObjNotaFiscalCFOP.NOTAFISCAL.LocalizaCodigo(Pnota)=False)
     Then Begin
              Messagedlg('Nota Fiscal n�o Localizada!',mterror,[mbok],0);
              exit;
     End;
     Self.ObjNotaFiscalCFOP.NotaFiscal.TabelaparaObjeto;

     if (Self.ObjNotaFiscalCFOP.NotaFiscal.Get_IcmsRecolhidoPelaEmpresa='S')
     Then TrocaIcmsSubstituicao_recolhido:=True
     Else TrocaIcmsSubstituicao_recolhido:=False;


     Self.ObjNotaFiscalCFOP.RetornaCFOP_por_Nota(Pnota,Pcfop,PNaturezaOperacao);

     With FRelNotaFiscalRdPrint do
     Begin

          //***********Recuperando a configura��o de cada label*****************
          CarregaConfiguracoesGerais;
          ComponenteRdPrint.Abrir;

          if (ComponenteRdPrint.Setup=False)
          Then Begin
                    ComponenteRdPrint.Fechar;
                    Exit;
          End;

          Try
             //**************************************CABE�ALHO NF*****************************************************
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbcfop.top/5))),strtoint(floattostr(int(lbcfop.left/5))),completapalavra(Pcfop,lbcfop.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbsaidaentrada.top/5))),strtoint(floattostr(int(lbsaidaentrada.left/5))),completapalavra('X',lbsaidaentrada.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbNaturezaOperacao.top/5))),strtoint(floattostr(int(lbNaturezaOperacao.left/5))),completapalavra(PNaturezaOperacao,lbNaturezaOperacao.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbControleSup.top/5))),strtoint(floattostr(int(lbControleSup.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Numero,lbControleSup.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbControleInf.top/5))),strtoint(floattostr(int(lbControleInf.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Numero,lbControleInf.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbDataEmissao.top/5))),strtoint(floattostr(int(lbDataEmissao.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_dataemissao,lbDataEmissao.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbDataSaida.top/5))),strtoint(floattostr(int(lbDataSaida.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.get_datasaida,lbDataSaida.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbHoraSaida.top/5))),strtoint(floattostr(int(lbHoraSaida.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.get_Horasaida,lbHoraSaida.tag,' '));
             //***********************************DADOS DO CLIENTE****************************************************
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbNomeDestinatRemete.top/5))),strtoint(floattostr(int(lbNomeDestinatRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Nome,lbNomeDestinatRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbCNPJDestRemete.top/5))),strtoint(floattostr(int(lbCNPJDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Cpf_CGC,lbCNPJDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbEnderecoDestRemete.top/5))),strtoint(floattostr(int(lbEnderecoDestRemete.left/5))),completapalavra(self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Endereco+','+self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Numero,lbEnderecoDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbBairroDestRemete.top/5))),strtoint(floattostr(int(lbBairroDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Bairro,lbBairroDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbCepDestRemete.top/5))),strtoint(floattostr(int(lbCepDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Cep,lbCepDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbMunicipioDestRemete.top/5))),strtoint(floattostr(int(lbMunicipioDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Cidade,lbMunicipioDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbFoneFaxDestRemete.top/5))),strtoint(floattostr(int(lbFoneFaxDestRemete.left/5))),completapalavra(self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Fone,lbFoneFaxDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbUFDestRemete.top/5))),strtoint(floattostr(int(lbUFDestRemete.left/5))),completapalavra(self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Estado,lbUFDestRemete.tag,' '));

             //If (Self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Fisica_Juridica='J')
             //Then
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbIeDestRemete.top/5))),strtoint(floattostr(int(lbIeDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Rg_Ie,lbIeDestRemete.tag,' '));

             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura1.top/5))),strtoint(floattostr(int(lbVencimentofatura1.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura1,lbVencimentofatura1.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura2.top/5))),strtoint(floattostr(int(lbVencimentofatura2.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura2,lbVencimentofatura2.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura3.top/5))),strtoint(floattostr(int(lbVencimentofatura3.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura3,lbVencimentofatura3.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura4.top/5))),strtoint(floattostr(int(lbVencimentofatura4.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura4,lbVencimentofatura4.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura5.top/5))),strtoint(floattostr(int(lbVencimentofatura5.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura5,lbVencimentofatura5.tag,' '));

             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura1.top/5))),strtoint(floattostr(int(lbvalorfatura1.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura1),lbvalorfatura1.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura2.top/5))),strtoint(floattostr(int(lbvalorfatura2.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura2),lbvalorfatura2.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura3.top/5))),strtoint(floattostr(int(lbvalorfatura3.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura3),lbvalorfatura3.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura4.top/5))),strtoint(floattostr(int(lbvalorfatura4.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura4),lbvalorfatura4.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura5.top/5))),strtoint(floattostr(int(lbvalorfatura5.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura5),lbvalorfatura5.tag,' '));





             //**************************************ICMS  E OUTROS***************************************************************************
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbbasecalculo.top/5))),strtoint(floattostr(int(lbbasecalculo.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_BaseCalculoICms),lbbasecalculo.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValoricms.top/5))),strtoint(floattostr(int(lbValoricms.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_VALORICMS),lbValoricms.tag,' '));

             if (TrocaIcmsSubstituicao_recolhido=False)
             Then Begin
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbBaseCalculoICMSSubstituicao.top/5))),strtoint(floattostr(int(lbBaseCalculoICMSSubstituicao.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_BASECALCULOICMS_SUBSTITUICAO),lbBaseCalculoICMSSubstituicao.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorICMSSubstituicao.top/5))),strtoint(floattostr(int(lbValorICMSSubstituicao.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_VALORICMS_SUBSTITUICAO),lbValorICMSSubstituicao.tag,' '));
             End
             Else Begin
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbBaseCalculoICMSSubstituicao.top/5))),strtoint(floattostr(int(lbBaseCalculoICMSSubstituicao.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_BASECALCULOICMS_SUBST_recolhido),lbBaseCalculoICMSSubstituicao.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorICMSSubstituicao.top/5))),strtoint(floattostr(int(lbValorICMSSubstituicao.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_VALORICMS_SUBST_recolhido),lbValorICMSSubstituicao.tag,' '));
             End;

             ComponenteRdPrint.Imp(strtoint(floattostr(int(ValorTotalProdutos.top/5))),strtoint(floattostr(int(ValorTotalProdutos.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_ValorTotal),ValorTotalProdutos.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorFrete.top/5))),strtoint(floattostr(int(lbValorFrete.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_ValorFrete),lbValorFrete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorSeguro.top/5))),strtoint(floattostr(int(lbValorSeguro.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_ValorSeguro),lbValorSeguro.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbOutrasDespesas.top/5))),strtoint(floattostr(int(lbOutrasDespesas.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_OutrasDespesas),lbOutrasDespesas.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorTotalIPI.top/5))),strtoint(floattostr(int(lbValorTotalIPI.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_ValorTotalIPI),lbValorTotalIPI.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorTotalNota.top/5))),strtoint(floattostr(int(lbValorTotalNota.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_VALORfinal),lbValorTotalNota.tag,' '));

             //***********************************TRANSPORTADORA*************************************************************************
             if (Self.ObjNotaFiscalCFOP.notafiscal.Transportadora.Get_CODIGO='')
             Then Begin
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbNomeTransportadora.top/5))),strtoint(floattostr(int(lbNomeTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_NomeTransportadora,LbNomeTransportadora.Tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbFreteporcontaTransportadora.top/5))),strtoint(floattostr(int(lbFreteporcontaTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_FreteporContaTransportadora,lbFreteporcontaTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbPlacaVeiculoTransportadora.top/5))),strtoint(floattostr(int(lbPlacaVeiculoTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_PlacaVeiculoTransportadora,lbPlacaVeiculoTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbUFTransportadora.top/5))),strtoint(floattostr(int(lbUFTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_UFVeiculoTransportadora,lbUFTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbCnpjTransportadora.top/5))),strtoint(floattostr(int(lbCnpjTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_CNPJTransportadora,lbCnpjTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbEnderecoTransportadora.top/5))),strtoint(floattostr(int(lbEnderecoTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_EnderecoTransportadora,lbEnderecoTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbMunicipioTransportadora.top/5))),strtoint(floattostr(int(lbMunicipioTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_MunicipioTransportadora,lbMunicipioTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbUFEnderecoTransportadora.top/5))),strtoint(floattostr(int(lbUFEnderecoTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_UFTransportadora,lbUFEnderecoTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbIETransportadora.top/5))),strtoint(floattostr(int(lbIETransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_IETransportadora,lbIETransportadora.tag,' '));
             End;

             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbEspecieTransportadora.top/5))),strtoint(floattostr(int(lbEspecieTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Especie,lbEspecieTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbMarcaTransportadora.top/5))),strtoint(floattostr(int(lbMarcaTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Marca,lbMarcaTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbNumeroTransportadora.top/5))),strtoint(floattostr(int(lbNumeroTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_NumeroVolumes,lbNumeroTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbQuantidadeTransportadora.top/5))),strtoint(floattostr(int(lbQuantidadeTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Quantidade,lbQuantidadeTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbPesoBrutoTransportadora.top/5))),strtoint(floattostr(int(lbPesoBrutoTransportadora.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_PesoBruto),lbPesoBrutoTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbPesoLiquidoTransportadora.top/5))),strtoint(floattostr(int(lbPesoLiquidoTransportadora.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_PesoLiquido),lbPesoLiquidoTransportadora.tag,' '));
             
             // *****************DADOS DO PRODUTO*******************************
             If  (FrelnotafiscalRdPrint.PegaCampoIni('QUANTIDADECAMPOS',Temp)=False)
             Then exit;
             
             Try
                quant_campos_produto:=strtoint(temp);
             Except
                   Messagedlg('Erro na Valor do Campo "QUANTIDADECAMPOS" do arquivo INI',mterror,[mbok],0);
                   exit;
             End;
             
             //*********pegando a quantidade de caracteres no espaco inicial*********
             If(FrelnotafiscalRdPrint.PegaCampoIni('ESPACOINICIAL',Temp)=False)
             Then exit;
             
             Try
                EspacoInicial:=strtoint(temp);
             Except
                   Messagedlg('Erro na Valor do Campo "ESPACOINICIAL" do arquivo INI',mterror,[mbok],0);
                   exit;
             End;

             With Qtemp do
             Begin

                  close;
                  SQL.clear;
                  SQL.add('Select  * from ProcRetornaMateriais_NF('+PNota+')');
                  open;
                  
                  If (Recordcount=0)
                  Then Begin
                            Messagedlg('N�o h� produtos cadastrados para esta NF!',mterror,[mbok],0);
                            exit;
                  End;

                  first;
                  cont3:=0;
                  //esse while percorre produto a produto colocado na nf
                  While not(eof) do
                  Begin
                       linha:=' ';
                       linha:=CompletaPalavra(' ',EspacoInicial,' ');

                       for cont:=1 to quant_campos_produto do
                       Begin
                           //***************************************************

                            //pega o nome do campo
                           If (FrelNotaFiscalRdPrint.PegaCampoIni('CAMPO'+INTTOSTR(CONT),TEMP)=False)
                           Then exit;

                           NomeCampo:=temp;

                           //pega a quant. caracteres do campo
                           If (FrelNotaFiscalRdPrint.pegacampoini(NomeCampo,temp)=False)
                           Then exit;

                           try
                              tamanhocampo:=strtoint(temp);
                           Except
                                 Messagedlg('Erro no valor do Tamanho do Campo->'+NomeCampo,mterror,[mbok],0);
                                 exit;
                           End;

                           //espacocampo
                           //pega a quant. caracteres espaco do campo
                           If (FrelNotaFiscalRdPrint.pegacampoini('ESPACO'+NomeCampo,temp)=False)
                           Then exit;
                           Try
                              espacocampo:=strtoint(temp);
                           Except
                                 Messagedlg('Erro no valor do Tamanho do Espa�o do Campo->'+NomeCampo,mterror,[mbok],0);
                                 exit;
                           End;

                           //Ja tenho o nome do campo, tamanho e o espaco
                           //CAMPOS QUE PODEM SER USADOS
                           //CODIGOPRODUTO
                           //DESCRICAOPRODUTO
                           //CLASSIFICACAOFISCAL
                           //SITUACAOTRIBUTARIA
                           //UNIDADE
                           //QUANTIDADE
                           //VALOR
                           //VALORFINAl
                           //ALIQUOTAICMS
             
                           If (nomecampo='CODIGOPRODUTO')
                           Then Begin
                                    linha:=linha+CompletaPalavra(Fieldbyname('codigoproduto').asstring,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;
             
                           If (nomecampo='DESCRICAOPRODUTO')
                           Then linha:=linha+CompletaPalavra(fieldbyname('produto').asstring,tamanhocampo-20,' ')+'-'+CompletaPalavra(fieldbyname('cor').asstring,19,' ')+completapalavra(' ',espacocampo,' ');
             
                           If (nomecampo='CLASSIFICACAOFISCAL')
                           Then Begin
                                     linha:=linha+CompletaPalavra(fieldbyname('classificacaofiscal').asstring,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;
                           If (nomecampo='SITUACAOTRIBUTARIA')
                           Then Begin
                                     linha:=linha+CompletaPalavra(fieldbyname('situacaotributaria_tabelaA').asstring+AdicionaZero(fieldbyname('situacaotributaria_tabelaB').asstring),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;
             
                           If (nomecampo='UNIDADE')
                           Then Begin
                                     linha:=linha+CompletaPalavra(fieldbyname('unidade').asstring,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;
             
                           If(nomecampo='QUANTIDADE')
                           Then Begin
                                     linha:=linha+CompletaPalavra_a_Esquerda(Fieldbyname('Quantidade').asstring,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           If(nomecampo='VALORUNITARIO')
                           Then Begin
                                     linha:=linha+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('VALOR').asstring),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           if (nomecampo='VALORTOTAL')
                           Then Begin
                                     linha:=linha+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('VALORFINAL').asstring),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           if (nomecampo='IPI')
                           Then Begin
                                     linha:=linha+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('IPI').asstring),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ');
                           End;

                           If (nomecampo='ALIQUOTAICMS')
                           Then Begin
                                     
                                     Pisento:=fieldbyname('isento').asstring;
                                     PSubstituicao:=fieldbyname('substituicaotributaria').asstring;
                                     Paliquota:=fieldbyname('aliquota').asstring;


                                     if (Pisento='S')
                                     Then linha:=linha+CompletaPalavra('00',tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                                     Else linha:=linha+CompletaPalavra(Paliquota,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ');
                           End;

                           if (nomecampo='VALORIPI')
                           Then linha:=linha+CompletaPalavra_a_Esquerda(formata_valor(((Fieldbyname('VALORFINAL').asfloat*Fieldbyname('IPI').asfloat)/100)),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           
                           //***************************************************************
                       End;//for dos campos
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(MEMOPRODUTOS.top/5)))+cont3,strtoint(floattostr(int(MEMOPRODUTOS.left/5))),completapalavra(linha,MEMOPRODUTOS.tag,' '));
                       inc(cont3,1);
                       next;
                  End;//while dos produtos
             
             
                  //******************DESCONTO**********************************
                  If(strtofloat(Self.ObjNotaFiscalCfop.NotaFiscal.get_desconto)>0)
                  Then Begin//tem Desconto Geral
                            LINHA:='';
                            //ANTES DO DESCONTO
                            If (FrelNotaFiscalRdPrint.pegacampoini('ANTESDESCONTO',temp)=False)
                            Then exit;
                            Try
                               espacocampo:=strtoint(temp);
                            Except
                                  Messagedlg('Erro no valor do Tamanho do Espa�o Antes do Desconto ',mterror,[mbok],0);
                                  exit;
                            End;
                            linha:=linha+CompletaPalavra(' ',espacocampo,' ');
                            //desconto
                            If (FrelNotaFiscalRdPrint.pegacampoini('DESCONTO',temp)=False)
                            Then exit;

                            Try
                               espacocampo:=strtoint(temp);
                            Except
                                  Messagedlg('Erro no valor do Tamanho do Espa�o Antes do Desconto ',mterror,[mbok],0);
                                  exit;
                            End;
                            linha:=linha+CompletaPalavra('DESCONTO ',espacocampo,' ');

                            //espaco antes do valor do desconto
                            If (FrelNotaFiscalRdPrint.pegacampoini('DEPOISDESCONTO',temp)=False)
                            Then exit;

                            Try
                               espacocampo:=strtoint(temp);
                            Except
                                  Messagedlg('Erro no valor do Tamanho do Espa�o Antes do Desconto ',mterror,[mbok],0);
                                  exit;
                            End;
                            linha:=linha+CompletaPalavra(' ',espacocampo,' ');
                            //valor do desconto
                            linha:=linha+formata_valor(Self.ObjNotaFiscalCfop.NotaFiscal.get_Desconto);
                            ComponenteRdPrint.Imp(strtoint(floattostr(int(MEMOPRODUTOS.top/5)))+cont3,strtoint(floattostr(int(MEMOPRODUTOS.left/5))),completapalavra(linha,MEMOPRODUTOS.tag,' '));
                  End;
                  
                 //**************DADOS ADICIONAIS*******************************
                 StrStringList.Clear;
                 StrStringList.text:=Self.ObjNotaFiscalCfop.NOTAFISCAL.Get_DadosAdicionais;
                 cont3:=0;
                 for cont:=0 to StrStringList.Count-1 do
                 Begin
             
                      if (length(StrStringList[cont])>MEMOINFORMACOESCOMPLEMENTARES.tag)
                      Then Begin
                                //a linha digitada nao cabe numa unica linha
                                //tenho que quebrar em varias
                                //************************************************
                                Temp2:=StrStringList[cont];
                                While (temp2<>'') do
                                Begin
                                     linha:=temp2;
             
                                     DividirValorCOMSEGUNDASEMTAMANHO(linha,MEMOINFORMACOESCOMPLEMENTARES.tag,temp,temp2);
             
                                     if (length(temp2)>1)
                                     Then Begin
                                               if (temp2[1]=' ')
                                               Then temp2:=copy(temp2,2,length(temp2)-1);
                                     End;
             
                                     if (temp<>'')
                                     Then Begin
                                               componenteRdPrint.Imp(strtoint(floattostr(int(MEMOINFORMACOESCOMPLEMENTARES.top/5)))+cont3,strtoint(floattostr(int(MEMOINFORMACOESCOMPLEMENTARES.left/5))),completapalavra(temp,MEMOINFORMACOESCOMPLEMENTARES.tag,' '));
                                               inc(cont3,1);
                                     End;
                                End;
                                //**************************************************
                      End
                      Else Begin//cabe numa linha apenas
                                componenteRdPrint.Imp(strtoint(floattostr(int(MEMOINFORMACOESCOMPLEMENTARES.top/5)))+cont3,strtoint(floattostr(int(MEMOINFORMACOESCOMPLEMENTARES.left/5))),completapalavra(StrStringList[cont],MEMOINFORMACOESCOMPLEMENTARES.tag,' '));
                                inc(cont3,1);
                      End;
                 End;//for cont:=0...
                 //*************************************************************
                 
             End;//with Qtemp
             
          Finally
             componenterdprint.fechar;
          End;
     End;//With FreltxtRdPrint

Finally
     freeandnil(Qtemp);
     FreeAndNil(StrStringList);
End;

End;

procedure TObjNotaFiscalObjetos.ImprimeNFRDPRINT(Pnota: string);
Begin

     With FOpcaorel do
     Begin
           RgOpcoes.Items.clear;
           RgOpcoes.Items.add('Nota Fiscal');
           RgOpcoes.Items.add('Desativada');
           RgOpcoes.Items.Add('Rela��o de Notas fiscais emitidas') ;
           showmodal;

            Case RgOpcoes.ItemIndex of
               0:Self.ImprimeNFRDPRINT_NOTADIGITADAM1(pnota);
               1:Self.ImprimeNFRDPRINT_antiga(pnota);
               2:Self.RelacaodeNotasFiscais(Pnota);
            End;


     End;
End;



//USADA QUANDO VAI SE FAZER UMA NOTA M1 DIGITADA
//SE GERAR DIRETO PELO PEDIDO USA OUTRO
procedure TObjNotaFiscalObjetos.ImprimeNFRDPRINT_NOTADIGITADAM1(Pnota: string);
var
  EspacoInicial,TamanhoCampo,EspacoCampo,cont,cont3,quant_campos_produto:Integer;

  Pcfop,PNaturezaOperacao,Temp,NomeCampo,Linha:String;
  Qtemp:Tibquery;
  Pisento,PSubstituicao,Paliquota:String;
  temp2:String;
  StrStringList:TStringList;
  TrocaIcmsSubstituicao_recolhido:boolean;

Begin
     //Se quiser ver como era antes pegue a funacao ImprimeNFRDPRINT_antiga

     Try
         Qtemp:=Tibquery.create(nil);
         Qtemp.database:=Self.Objquery.database;
     Except
           messagedlg('Erro na cria��o de objetos!',mterror,[mbok],0);
     End;

     Try
        StrStringList:=TStringList.Create;
     Except
           MensagemErro('Erro na Cria��o da StringList');
           freeandnil(qtemp);
     end;

Try//try finally da QTemp e da StrStringList

     If (Pnota='')
     Then Begin
               MensagemAviso('Escolha uma Nota Fiscal');
               exit;
     End;

     //Localiza a NF
     If (Self.ObjNotaFiscalCFOP.NOTAFISCAL.LocalizaCodigo(Pnota)=False)
     Then Begin
              Messagedlg('Nota Fiscal n�o Localizada!',mterror,[mbok],0);
              exit;
     End;
     Self.ObjNotaFiscalCFOP.NotaFiscal.TabelaparaObjeto;

     if(Self.ObjNotaFiscalCfop.NOTAFISCAL.Get_Situacao= 'N')
     then begin
         if (Self.CalculaBasedeCalculo_NOVA(pnota)=False)
         Then exit;

         if (self.CalculaPeso(pnota)=False)
         then exit;
     end;

     //Localiza a NF
     If (Self.ObjNotaFiscalCFOP.NOTAFISCAL.LocalizaCodigo(Pnota)=False)
     Then Begin
              Messagedlg('Nota Fiscal n�o Localizada!',mterror,[mbok],0);
              exit;
     End;
     Self.ObjNotaFiscalCFOP.NotaFiscal.TabelaparaObjeto;

     //Agora o CFOP � por produto
     pCfop:='';
     With Qtemp do
     Begin
          close;
          SQL.clear;
          SQL.add('Select  * from ProcRetornaMateriais_NF('+PNota+')');
          open;

          StrStringList.clear;
          While not(Qtemp.eof) do
          Begin
               //***************ICMS DESTINO***************
               if (ObjVidro_NF.IMPOSTO_ICMS_DESTINO.LocalizaCodigo(Qtemp.fieldbyname('imposto_icms_DESTINO').asstring)=False)
               Then Begin
                      MensagemErro('O Imposto ICMS DESTINO do produto '+Self.Objquery.fieldbyname('tipo').asstring+'-'+Self.Objquery.fieldbyname('produto').asstring+#13+
                                   'N�o foi encontrado');
                      exit;
               End;
               ObjVidro_NF.IMPOSTO_ICMS_DESTINO.TabelaparaObjeto;

               //Procuro se o CFOP j� n�o existe na lista
               if (StrStringList.IndexOf(fieldbyname('CFOP').AsString)<0)
               Then StrStringList.add(fieldbyname('CFOP').AsString);

               Qtemp.next;
          End;

          Pcfop:='';

          for cont:=0 to StrStringList.Count-1 do
          Begin
               if (cont>0)
               Then Pcfop:=Pcfop+'/';

               Pcfop:=Pcfop+StrStringList[cont];
          End;

     End;



     With FRelNotaFiscalRdPrint do
     Begin

          //***********Recuperando a configura��o de cada label*****************
          CarregaConfiguracoesGerais;
          ComponenteRdPrint.Abrir;

          if (ComponenteRdPrint.Setup=False)
          Then Begin
                    ComponenteRdPrint.Fechar;
                    Exit;
          End;

          Try
             //**************************************CABE�ALHO NF*****************************************************
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbcfop.top/5))),strtoint(floattostr(int(lbcfop.left/5))),completapalavra(Pcfop,lbcfop.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbsaidaentrada.top/5))),strtoint(floattostr(int(lbsaidaentrada.left/5))),completapalavra('X',lbsaidaentrada.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbNaturezaOperacao.top/5))),strtoint(floattostr(int(lbNaturezaOperacao.left/5))),completapalavra(Self.ObjNotaFiscalCfop.NOTAFISCAL.Get_NATUREZAOPERACAO,lbNaturezaOperacao.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbControleSup.top/5))),strtoint(floattostr(int(lbControleSup.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Numero,lbControleSup.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbControleInf.top/5))),strtoint(floattostr(int(lbControleInf.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Numero,lbControleInf.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbDataEmissao.top/5))),strtoint(floattostr(int(lbDataEmissao.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_dataemissao,lbDataEmissao.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbDataSaida.top/5))),strtoint(floattostr(int(lbDataSaida.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.get_datasaida,lbDataSaida.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbHoraSaida.top/5))),strtoint(floattostr(int(lbHoraSaida.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.get_Horasaida,lbHoraSaida.tag,' '));
             //***********************************DADOS DO CLIENTE****************************************************
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbNomeDestinatRemete.top/5))),strtoint(floattostr(int(lbNomeDestinatRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Nome,lbNomeDestinatRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbCNPJDestRemete.top/5))),strtoint(floattostr(int(lbCNPJDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Cpf_CGC,lbCNPJDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbEnderecoDestRemete.top/5))),strtoint(floattostr(int(lbEnderecoDestRemete.left/5))),completapalavra(self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Endereco+','+self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Numero,lbEnderecoDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbBairroDestRemete.top/5))),strtoint(floattostr(int(lbBairroDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Bairro,lbBairroDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbCepDestRemete.top/5))),strtoint(floattostr(int(lbCepDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Cep,lbCepDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbMunicipioDestRemete.top/5))),strtoint(floattostr(int(lbMunicipioDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Cidade,lbMunicipioDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbFoneFaxDestRemete.top/5))),strtoint(floattostr(int(lbFoneFaxDestRemete.left/5))),completapalavra(self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Fone,lbFoneFaxDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbUFDestRemete.top/5))),strtoint(floattostr(int(lbUFDestRemete.left/5))),completapalavra(self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Estado,lbUFDestRemete.tag,' '));

             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbIeDestRemete.top/5))),strtoint(floattostr(int(lbIeDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Rg_Ie,lbIeDestRemete.tag,' '));

             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura1.top/5))),strtoint(floattostr(int(lbVencimentofatura1.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura1,lbVencimentofatura1.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura2.top/5))),strtoint(floattostr(int(lbVencimentofatura2.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura2,lbVencimentofatura2.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura3.top/5))),strtoint(floattostr(int(lbVencimentofatura3.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura3,lbVencimentofatura3.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura4.top/5))),strtoint(floattostr(int(lbVencimentofatura4.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura4,lbVencimentofatura4.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura5.top/5))),strtoint(floattostr(int(lbVencimentofatura5.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura5,lbVencimentofatura5.tag,' '));

             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura1.top/5))),strtoint(floattostr(int(lbvalorfatura1.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura1),lbvalorfatura1.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura2.top/5))),strtoint(floattostr(int(lbvalorfatura2.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura2),lbvalorfatura2.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura3.top/5))),strtoint(floattostr(int(lbvalorfatura3.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura3),lbvalorfatura3.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura4.top/5))),strtoint(floattostr(int(lbvalorfatura4.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura4),lbvalorfatura4.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura5.top/5))),strtoint(floattostr(int(lbvalorfatura5.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura5),lbvalorfatura5.tag,' '));

             
             //**************************************ICMS  E OUTROS***************************************************************************
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbbasecalculo.top/5))),strtoint(floattostr(int(lbbasecalculo.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_BaseCalculoICms),lbbasecalculo.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValoricms.top/5))),strtoint(floattostr(int(lbValoricms.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_VALORICMS),lbValoricms.tag,' '));

             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbBaseCalculoICMSSubstituicao.top/5))),strtoint(floattostr(int(lbBaseCalculoICMSSubstituicao.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_BASECALCULOICMS_SUBST_recolhido),lbBaseCalculoICMSSubstituicao.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorICMSSubstituicao.top/5))),strtoint(floattostr(int(lbValorICMSSubstituicao.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_VALORICMS_SUBST_recolhido),lbValorICMSSubstituicao.tag,' '));


             ComponenteRdPrint.Imp(strtoint(floattostr(int(ValorTotalProdutos.top/5))),strtoint(floattostr(int(ValorTotalProdutos.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_ValorTotal),ValorTotalProdutos.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorFrete.top/5))),strtoint(floattostr(int(lbValorFrete.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_ValorFrete),lbValorFrete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorSeguro.top/5))),strtoint(floattostr(int(lbValorSeguro.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_ValorSeguro),lbValorSeguro.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbOutrasDespesas.top/5))),strtoint(floattostr(int(lbOutrasDespesas.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_OutrasDespesas),lbOutrasDespesas.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorTotalIPI.top/5))),strtoint(floattostr(int(lbValorTotalIPI.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_ValorTotalIPI),lbValorTotalIPI.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorTotalNota.top/5))),strtoint(floattostr(int(lbValorTotalNota.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_VALORfinal),lbValorTotalNota.tag,' '));

             //***********************************TRANSPORTADORA*************************************************************************
             if (Self.ObjNotaFiscalCFOP.notafiscal.Transportadora.Get_CODIGO<>'')
             Then Begin
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbNomeTransportadora.top/5))),strtoint(floattostr(int(lbNomeTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_NomeTransportadora,LbNomeTransportadora.Tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbFreteporcontaTransportadora.top/5))),strtoint(floattostr(int(lbFreteporcontaTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_FreteporContaTransportadora,lbFreteporcontaTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbPlacaVeiculoTransportadora.top/5))),strtoint(floattostr(int(lbPlacaVeiculoTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_PlacaVeiculoTransportadora,lbPlacaVeiculoTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbUFTransportadora.top/5))),strtoint(floattostr(int(lbUFTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_UFVeiculoTransportadora,lbUFTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbCnpjTransportadora.top/5))),strtoint(floattostr(int(lbCnpjTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_CNPJTransportadora,lbCnpjTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbEnderecoTransportadora.top/5))),strtoint(floattostr(int(lbEnderecoTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_EnderecoTransportadora,lbEnderecoTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbMunicipioTransportadora.top/5))),strtoint(floattostr(int(lbMunicipioTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_MunicipioTransportadora,lbMunicipioTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbUFEnderecoTransportadora.top/5))),strtoint(floattostr(int(lbUFEnderecoTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_UFTransportadora,lbUFEnderecoTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbIETransportadora.top/5))),strtoint(floattostr(int(lbIETransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_IETransportadora,lbIETransportadora.tag,' '));
             End;

             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbEspecieTransportadora.top/5))),strtoint(floattostr(int(lbEspecieTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Especie,lbEspecieTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbMarcaTransportadora.top/5))),strtoint(floattostr(int(lbMarcaTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Marca,lbMarcaTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbNumeroTransportadora.top/5))),strtoint(floattostr(int(lbNumeroTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_NumeroVolumes,lbNumeroTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbQuantidadeTransportadora.top/5))),strtoint(floattostr(int(lbQuantidadeTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Quantidade,lbQuantidadeTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbPesoBrutoTransportadora.top/5))),strtoint(floattostr(int(lbPesoBrutoTransportadora.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_PesoBruto),lbPesoBrutoTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbPesoLiquidoTransportadora.top/5))),strtoint(floattostr(int(lbPesoLiquidoTransportadora.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_PesoLiquido),lbPesoLiquidoTransportadora.tag,' '));
             
             // *****************DADOS DO PRODUTO*******************************
             If  (FrelnotafiscalRdPrint.PegaCampoIni('QUANTIDADECAMPOS',Temp)=False)
             Then exit;
             
             Try
                quant_campos_produto:=strtoint(temp);
             Except
                   Messagedlg('Erro na Valor do Campo "QUANTIDADECAMPOS" do arquivo INI',mterror,[mbok],0);
                   exit;
             End;

             //*********pegando a quantidade de caracteres no espaco inicial*********
             If(FrelnotafiscalRdPrint.PegaCampoIni('ESPACOINICIAL',Temp)=False)
             Then exit;

             Try
                EspacoInicial:=strtoint(temp);
             Except
                   Messagedlg('Erro na Valor do Campo "ESPACOINICIAL" do arquivo INI',mterror,[mbok],0);
                   exit;
             End;

             With Qtemp do
             Begin

                  close;
                  SQL.clear;
                  SQL.add('Select  * from ProcRetornaMateriais_NF('+PNota+')');
                  open;

                  If (Recordcount=0)
                  Then Begin
                            Messagedlg('N�o h� produtos cadastrados para esta NF!',mterror,[mbok],0);
                            exit;
                  End;

                  first;
                  cont3:=0;
                  //esse while percorre produto a produto colocado na nf
                  While not(eof) do
                  Begin
                       //***************ICMS ORIGEM***************
                       if (ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.LocalizaCodigo(Qtemp.fieldbyname('imposto_icms_origem').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto ICMS Origem do produto '+Qtemp.fieldbyname('tipo').asstring+'-'+Qtemp.fieldbyname('produto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.TabelaparaObjeto;

                       //***************ICMS DESTINO***************
                       if (ObjVidro_NF.IMPOSTO_ICMS_DESTINO.LocalizaCodigo(Qtemp.fieldbyname('imposto_icms_DESTINO').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto ICMS DESTINO do produto '+Qtemp.fieldbyname('tipo').asstring+'-'+Qtemp.fieldbyname('produto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_ICMS_DESTINO.TabelaparaObjeto;

                       //**************************PIS ORIGEM**********************************
                       if (ObjVidro_NF.IMPOSTO_PIS_ORIGEM.LocalizaCodigo(Qtemp.fieldbyname('imposto_PIS_origem').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto PIS Origem do produto '+Qtemp.fieldbyname('tipo').asstring+'-'+Qtemp.fieldbyname('produto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_PIS_ORIGEM.TabelaparaObjeto;
                  
                       //***************PIS DESTINO***************
                       if (ObjVidro_NF.IMPOSTO_PIS_DESTINO.LocalizaCodigo(Qtemp.fieldbyname('imposto_PIS_DESTINO').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto PIS DESTINO do produto '+Qtemp.fieldbyname('tipo').asstring+'-'+Qtemp.fieldbyname('produto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_PIS_DESTINO.TabelaparaObjeto;

                  
                       //**************************COFINS ORIGEM**********************************
                       if (ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.LocalizaCodigo(Qtemp.fieldbyname('imposto_COFINS_origem').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto COFINS Origem do produto '+Qtemp.fieldbyname('tipo').asstring+'-'+Qtemp.fieldbyname('produto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.TabelaparaObjeto;

                       //***************COFINS DESTINO***************
                       if (ObjVidro_NF.IMPOSTO_COFINS_DESTINO.LocalizaCodigo(Qtemp.fieldbyname('imposto_COFINS_DESTINO').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto COFINS DESTINO do produto '+Qtemp.fieldbyname('tipo').asstring+'-'+Qtemp.fieldbyname('produto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_COFINS_DESTINO.TabelaparaObjeto;

                       //**************IPI****************
                        if (ObjVidro_NF.IMPOSTO_IPI.LocalizaCodigo(Qtemp.fieldbyname('imposto_IPI').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto IPI do produto '+Qtemp.fieldbyname('tipo').asstring+'-'+Qtemp.fieldbyname('produto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_IPI.TabelaparaObjeto;
                       //*********************************************************
                  
                       linha:=' ';
                       linha:=CompletaPalavra(' ',EspacoInicial,' ');

                       for cont:=1 to quant_campos_produto do
                       Begin
                           //***************************************************

                            //pega o nome do campo
                           If (FrelNotaFiscalRdPrint.PegaCampoIni('CAMPO'+INTTOSTR(CONT),TEMP)=False)
                           Then exit;

                           NomeCampo:=temp;

                           //pega a quant. caracteres do campo
                           If (FrelNotaFiscalRdPrint.pegacampoini(NomeCampo,temp)=False)
                           Then exit;

                           try
                              tamanhocampo:=strtoint(temp);
                           Except
                                 Messagedlg('Erro no valor do Tamanho do Campo->'+NomeCampo,mterror,[mbok],0);
                                 exit;
                           End;

                           //espacocampo
                           //pega a quant. caracteres espaco do campo
                           If (FrelNotaFiscalRdPrint.pegacampoini('ESPACO'+NomeCampo,temp)=False)
                           Then exit;
                           Try
                              espacocampo:=strtoint(temp);
                           Except
                                 Messagedlg('Erro no valor do Tamanho do Espa�o do Campo->'+NomeCampo,mterror,[mbok],0);
                                 exit;
                           End;

                           //Ja tenho o nome do campo, tamanho e o espaco
                           //CAMPOS QUE PODEM SER USADOS
                           //CODIGOPRODUTO
                           //DESCRICAOPRODUTO
                           //CLASSIFICACAOFISCAL
                           //SITUACAOTRIBUTARIA
                           //UNIDADE
                           //QUANTIDADE
                           //VALOR
                           //VALORFINAl
                           //ALIQUOTAICMS

                           If (nomecampo='CODIGOPRODUTO')
                           Then Begin
                                    linha:=linha+CompletaPalavra(Fieldbyname('codigoproduto').asstring,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           If (nomecampo='DESCRICAOPRODUTO')
                           Then linha:=linha+CompletaPalavra(fieldbyname('produto').asstring,tamanhocampo-20,' ')+'-'+CompletaPalavra(fieldbyname('cor').asstring,19,' ')+completapalavra(' ',espacocampo,' ');

                           If (nomecampo='CLASSIFICACAOFISCAL')
                           Then Begin
                                     linha:=linha+CompletaPalavra(fieldbyname('classificacaofiscal').asstring,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           If (nomecampo='SITUACAOTRIBUTARIA')
                           Then Begin
                                     linha:=linha+CompletaPalavra(ObjVidro_NF.IMPOSTO_ICMS_DESTINO.STA.Get_CODIGO+AdicionaZero(ObjVidro_NF.IMPOSTO_ICMS_DESTINO.STB.Get_CODIGO),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           If (nomecampo='UNIDADE')
                           Then Begin
                                     linha:=linha+CompletaPalavra(fieldbyname('unidade').asstring,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;
             
                           If(nomecampo='QUANTIDADE')
                           Then Begin
                                     linha:=linha+CompletaPalavra_a_Esquerda(Fieldbyname('Quantidade').asstring,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           If(nomecampo='VALORUNITARIO')
                           Then Begin
                                     linha:=linha+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('VALOR').asstring),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           if (nomecampo='VALORTOTAL')
                           Then Begin
                                     linha:=linha+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('VALORFINAL').asstring),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           if (nomecampo='IPI')
                           Then Begin
                                     linha:=linha+CompletaPalavra_a_Esquerda(formata_valor(ObjVidro_NF.IMPOSTO_IPI.Get_Aliquota),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ');
                           End;

                           If (nomecampo='ALIQUOTAICMS')
                           Then Begin
                                     linha:=linha+CompletaPalavra(ObjVidro_NF.IMPOSTO_ICMS_DESTINO.Get_ALIQUOTA,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ');
                           End;

                           if (nomecampo='VALORIPI')
                           Then linha:=linha+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR_IPI').asstring),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')

                           //***************************************************************
                       End;//for dos campos
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(MEMOPRODUTOS.top/5)))+cont3,strtoint(floattostr(int(MEMOPRODUTOS.left/5))),completapalavra(linha,MEMOPRODUTOS.tag,' '));
                       inc(cont3,1);
                       next;
                  End;//while dos produtos

             
                  //******************DESCONTO**********************************
                  If(strtofloat(Self.ObjNotaFiscalCfop.NotaFiscal.get_desconto)>0)
                  Then Begin//tem Desconto Geral
                            LINHA:='';
                            //ANTES DO DESCONTO
                            If (FrelNotaFiscalRdPrint.pegacampoini('ANTESDESCONTO',temp)=False)
                            Then exit;
                            Try
                               espacocampo:=strtoint(temp);
                            Except
                                  Messagedlg('Erro no valor do Tamanho do Espa�o Antes do Desconto ',mterror,[mbok],0);
                                  exit;
                            End;
                            linha:=linha+CompletaPalavra(' ',espacocampo,' ');
                            //desconto
                            If (FrelNotaFiscalRdPrint.pegacampoini('DESCONTO',temp)=False)
                            Then exit;

                            Try
                               espacocampo:=strtoint(temp);
                            Except
                                  Messagedlg('Erro no valor do Tamanho do Espa�o Antes do Desconto ',mterror,[mbok],0);
                                  exit;
                            End;
                            linha:=linha+CompletaPalavra('DESCONTO ',espacocampo,' ');

                            //espaco antes do valor do desconto
                            If (FrelNotaFiscalRdPrint.pegacampoini('DEPOISDESCONTO',temp)=False)
                            Then exit;

                            Try
                               espacocampo:=strtoint(temp);
                            Except
                                  Messagedlg('Erro no valor do Tamanho do Espa�o Antes do Desconto ',mterror,[mbok],0);
                                  exit;
                            End;
                            linha:=linha+CompletaPalavra(' ',espacocampo,' ');
                            //valor do desconto
                            linha:=linha+formata_valor(Self.ObjNotaFiscalCfop.NotaFiscal.get_Desconto);
                            ComponenteRdPrint.Imp(strtoint(floattostr(int(MEMOPRODUTOS.top/5)))+cont3,strtoint(floattostr(int(MEMOPRODUTOS.left/5))),completapalavra(linha,MEMOPRODUTOS.tag,' '));
                  End;
                  
                 //**************DADOS ADICIONAIS*******************************
                 StrStringList.Clear;
                 StrStringList.text:=Self.ObjNotaFiscalCfop.NOTAFISCAL.Get_DadosAdicionais;
                 cont3:=0;
                 for cont:=0 to StrStringList.Count-1 do
                 Begin

                      if (length(StrStringList[cont])>MEMOINFORMACOESCOMPLEMENTARES.tag)
                      Then Begin
                                //a linha digitada nao cabe numa unica linha
                                //tenho que quebrar em varias
                                //************************************************
                                Temp2:=StrStringList[cont];
                                While (temp2<>'') do
                                Begin
                                     linha:=temp2;

                                     DividirValorCOMSEGUNDASEMTAMANHO(linha,MEMOINFORMACOESCOMPLEMENTARES.tag,temp,temp2);

                                     if (length(temp2)>1)
                                     Then Begin
                                               if (temp2[1]=' ')
                                               Then temp2:=copy(temp2,2,length(temp2)-1);
                                     End;

                                     if (temp<>'')
                                     Then Begin
                                               componenteRdPrint.Imp(strtoint(floattostr(int(MEMOINFORMACOESCOMPLEMENTARES.top/5)))+cont3,strtoint(floattostr(int(MEMOINFORMACOESCOMPLEMENTARES.left/5))),completapalavra(temp,MEMOINFORMACOESCOMPLEMENTARES.tag,' '));
                                               inc(cont3,1);
                                     End;
                                End;
                                //**************************************************
                      End
                      Else Begin//cabe numa linha apenas
                                componenteRdPrint.Imp(strtoint(floattostr(int(MEMOINFORMACOESCOMPLEMENTARES.top/5)))+cont3,strtoint(floattostr(int(MEMOINFORMACOESCOMPLEMENTARES.left/5))),completapalavra(StrStringList[cont],MEMOINFORMACOESCOMPLEMENTARES.tag,' '));
                                inc(cont3,1);
                      End;
                 End;//for cont:=0...
                 //*************************************************************
                 
             End;//with Qtemp
             
          Finally
             componenterdprint.fechar;
          End;
     End;//With FreltxtRdPrint

Finally
     freeandnil(Qtemp);
End;

End;


procedure TObjNotafiscalObjetos.Opcoes(Pnotafiscal:string);
begin
     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Gera Nfe');                                      {0}
          RgOpcoes.Items.add('Menu NFE');                                      {1}
          //RgOpcoes.Items.add('Calcula BC  - NFE (Teste)');                     {2}
          //RgOpcoes.Items.add('Imprime Nfe da Nota Atual');                     {3}
          //RgOpcoes.Items.add('Cancela NFe da Nota Atual');                     {4}
          //RgOpcoes.Items.add('Salva o XML na Nfe Atual');                      {5}
          //RgOpcoes.Items.add('Verifica Retorno de Nfe em Processamento');      {6}
          //RgOpcoes.Items.add('Gera NFE em Modo de Conting�ncia');              {7}
          //RgOpcoes.Items.add('Envia NFE emitida em modo de Conting�ncia');     {8}
          //RgOpcoes.Items.add('Gera Nfe e for�a lote em processamento');        {9}
          RgOpcoes.Items.add('Importa Pedido para NF Atual');                  {10}
          RgOpcoes.Items.add('Exporta Nfs (Sistema Cuca Fresca)');             {11}



          Showmodal;

          if (tag=0)
          then exit;

          Case RgOpcoes.ItemIndex of
            //0:Self.AdicionaCfop(Pnotafiscal);
            0:Self.GeraNfe(PnotaFiscal);
            1:Self.MenuNfe;
            
            2:Self.CalculaBasedeCalculo_NOVA(Pnotafiscal);
            3:Self.ReimprimeNfe(Pnotafiscal);
            4:Self.CancelaNfe(Pnotafiscal);
            5:Self.SalvaXML(Pnotafiscal);
            6:Self.VerificaRetorno(Pnotafiscal);
            7:Self.GeraNfe(PnotaFiscal,True,False);//contingencia
            8:Self.EnviaNFe_Contingencia(PnotaFiscal);
            9:Self.GeraNfe(PnotaFiscal,False,True);//Forca Lote em Processamento
            10:Self.ImportaPedido(PnotaFiscal);
            11:self.ExportaCucaFresca;

          End;
     end;
end;

procedure TObjNotafiscalObjetos.AdicionaCfop(Pnotafiscal: string);
var
pcfop:string;
begin
     if (PNotaFiscal='')
     then Begin
               MensagemAviso('Escolha a Nota Fiscal');
               exit;
     end;

    if (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(Pnotafiscal)=False)
    then begin
              Messagedlg('Nota fiscal n�o encontrada',mterror,[mbok],0);
              exit;
    End;
    Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;

    With FfiltroImp do
    Begin
         DesativaGrupos;
         Grupo01.Enabled:=True;
         edtgrupo01.OnKeyDown:=Self.ObjNotaFiscalCfop.EdtCFOPKeyDown;
         LbGrupo01.caption:='CFOP';
         showmodal;
         if (tag=0)
         Then exit;

         Try
            StrToInt(edtgrupo01.Text);
            
            Pcfop:=edtgrupo01.Text;
            if (Self.ObjNotaFiscalCfop.CFOP.LocalizaCodigo(Pcfop)=False)
            Then Begin
                      MensagemAviso('Cfop n�o encontrado');
                      exit;
            End;
         Except
               MensagemErro('Cfop inv�lido');
               exit;
         End;
    End;

    Self.ObjNotaFiscalCfop.Submit_CODIGO(Self.ObjNotaFiscalCfop.Get_NovoCodigo);
    Self.ObjNotaFiscalCfop.CFOP.TabelaparaObjeto;
    self.ObjNotaFiscalCfop.Status:=dsinsert;

    if (Self.ObjNotaFiscalCfop.Salvar(true)=False)
    Then Begin
              MensagemErro('N�o foi poss�vel salvar o CFOP para esta Nota Fiscal');
              exit;
    End;
    
end;

function TObjNotafiscalObjetos.CalculaBasedeCalculo(pnota: string): boolean;
VAR
CFOPTOTAL,CFOPTRIBUTADOS,CFOPSUBTIUICAO,CFOPECF:string;
TMPBASEC_ICMS_recolhido,TMPVALORICMS_recolhido,PvalorIPI,tmpvalorproduto,TmpPorcentagemDesconto,TmpValorTotalVenda,TMpDescontototal,TMPBASEC_ICMS,TMPVALORICMS,TMPBASEC_ICMSSUBSTITUICAO,TMPVALORICMSSUBSTITUICAO:Currency;
TmpValorIcmsAtual,ipiindividual,percentualagregado,valorprodutoindividual:Currency;
QuantidadeTributado,QuantidadeNaoTributado:integer;
VendaNoestado:boolean;
StrlCfop:TStringList;
cont:integer;
Pcfop,pnaturezaoperacao:string;
Pisento,PSubstituicao,Paliquota,PvalorPauta,Preducao,PaliquotaCupom:String;
PCalculaSobreMVG:boolean;
begin
     //acerta a base de calculo e a aliquota
     //de acordo com os produtos da venda
     result:=False;

     Try
        StrlCfop:=TStringList.create;
        StrlCfop.clear;
     Except
           Messagedlg('Erro na tentativa de cria��o da StringList de CFOPs',mterror,[mbok],0);
           exit;
     End;
Try

     If (Pnota='')
     Then exit;

     //Verificando se a venda existe
     If (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(Pnota)=False)
     Then Begin
               messagedlg('Nota Fiscal n�o localizada!',mterror,[mbok],0);
               exit;
     End;
     Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;


     If  (uppercase(Self.ObjNotaFiscalCfop.NOTAFISCAL.Cliente.Get_Estado)<>ESTADOSISTEMAGLOBAL)
     Then VendaNoestado:=False
     Else VendaNoestado:=True;

     

     //Procurando os parametros de CFOP para Produtos tributados
     //e CFOP para produtos de subtituicao (isento na venda, pago por antecipacao ao estado)

     //Nos tributados existem dois casos os produtos vendidos no estado
     //e os os produtos vendidos para fora do estado
     //sao cfops diferentes

     If (VendaNoestado=True)
     Then Begin
             If (ObjParametroGlobal.ValidaParametro('CODIGO CFOP PARA VENDA DE PRODUTOS TRIBUTADOS')=FALSE)
             Then Begin
                       MEssagedlg('O Par�metro "CODIGO CFOP PARA VENDA DE PRODUTOS TRIBUTADOS" n�o foi localizado!',mterror,[mbok],0);
                       result:=False;
                       exit;
             End;

             CFOPTRIBUTADOS:='';
             CFOPTRIBUTADOS:=ObjParametroGlobal.Get_Valor;
             //**********************SUBSTITUICAO******************************
             If (ObjParametroGlobal.ValidaParametro('CODIGO CFOP PARA VENDA DE PRODUTOS DE SUBSTITUI��O')=FALSE)
             Then Begin
                       MEssagedlg('O Par�metro "CODIGO CFOP PARA VENDA DE PRODUTOS DE SUBSTITUI��O" n�o foi localizado!',mterror,[mbok],0);
                       result:=False;
                       exit;
             End;
             CFOPSUBTIUICAO:='';
             CFOPSUBTIUICAO:=ObjParametroGlobal.Get_Valor;
             //*****************CFOP PARA ECF*********************************
             If (ObjParametroGlobal.ValidaParametro('CODIGO CFOP PARA VENDA DE PRODUTOS DENTRO DO ESTADO EMITIDOS NO ECF')=FALSE)
             Then Begin
                       MEssagedlg('O Par�metro "CODIGO CFOP PARA VENDA DE PRODUTOS DENTRO DO ESTADO EMITIDOS NO ECF" n�o foi localizado!',mterror,[mbok],0);
                       result:=False;
                       exit;
             End;
             CFOPECF:='';
             CFOPECF:=ObjParametroGlobal.Get_Valor;
     End
     Else Begin
             If (ObjParametroGlobal.ValidaParametro('CODIGO CFOP PARA VENDA FORA DO ESTADO DE PRODUTOS TRIBUTADOS')=FALSE)
             Then Begin
                       Messagedlg('O Par�metro "CODIGO CFOP PARA VENDA FORA DO ESTADO DE PRODUTOS TRIBUTADOS" n�o foi localizado!',mterror,[mbok],0);
                       result:=False;
                       exit;
             End;
             //Guardando o Numero do CFOP
             CFOPTRIBUTADOS:='';
             CFOPTRIBUTADOS:=ObjParametroGlobal.Get_Valor;


             If (ObjParametroGlobal.ValidaParametro('CODIGO CFOP PARA VENDA DE PRODUTOS DE SUBSTITUI��O FORA DO ESTADO')=FALSE)
             Then Begin
                       MEssagedlg('O Par�metro "CODIGO CFOP PARA VENDA DE PRODUTOS DE SUBSTITUI��O FORA DO ESTADO" n�o foi localizado!',mterror,[mbok],0);
                       result:=False;
                       exit;
             End;
             //*****Guardando o de Substituicao*******
             CFOPSUBTIUICAO:='';
             CFOPSUBTIUICAO:=ObjParametroGlobal.Get_Valor;
             //*****************CFOP PARA ECF*********************************
             If (ObjParametroGlobal.ValidaParametro('CODIGO CFOP PARA VENDA DE PRODUTOS FORA DO ESTADO EMITIDOS NO ECF')=FALSE)
             Then Begin
                       MEssagedlg('O Par�metro "CODIGO CFOP PARA VENDA DE PRODUTOS FORA DO ESTADO EMITIDOS NO ECF" n�o foi localizado!',mterror,[mbok],0);
                       result:=False;
                       exit;
             End;
             CFOPECF:='';
             CFOPECF:=ObjParametroGlobal.Get_Valor;
     End;

     PCalculaSobreMVG:=False;
     if (ObjParametroGlobal.ValidaParametro('CALCULA BASE DE CALCULO ICMS CONSUMIDOR FINAL PELO MVG SOBRE VALOR E IPI')=False)
     then exit;

     if (ObjParametroGlobal.Get_Valor='SIM')
     Then PCalculaSobreMVG:=True;


     //*******************************************************
     CFOPTOTAL:='';
     //*******************************************************

     With self.Objquery do
     Begin
          Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
          {*******Separando quais CFOP devem ser colocados na NF*****
           se existir icms em algum produto independente do valor
           o CFOP sera a 5102(parametro)
           se naum existir em algum produto sera 5403(parametro)
           caso existam os dois casos preencho os dois
          ***********************************************************}
          {
          //tributados
          CLOSE;
          SQL.clear;
          SQL.add('Select count(codigo) as CONTA');
          SQL.add('from procretornamateriais_nf('+pnota+')');
          if (VendaNoestado=true)
          Then SQL.add('where SubsTituicaoICMS_Estado=''N'' or  SubsTituicaoICMS_Estado is null')
          Else SQL.add('where SubsTituicaoICMS_ForaEstado=''N'' or  SubsTituicaoICMS_ForaEstado is null')

          open;
          QuantidadeTributado:=Fieldbyname('CONTA').asinteger;

          If (Fieldbyname('CONTA').asinteger<>0)
          Then Begin
                    StrlCfop.add(CFOPTRIBUTADOS);
                    CFOPTOTAL:=CFOPTRIBUTADOS;
          End;

          //sem tributacao (substituicao)
          CLOSE;
          SQL.clear;
          CLOSE;
          SQL.clear;
          SQL.add('Select count(codigo) as CONTA');
          SQL.add('from procretornamateriais_nf('+pnota+')');
          if (VendaNoestado=true)
          Then SQL.add('where SubsTituicaoICMS_Estado=''S'' ');
          Else SQL.add('where SubsTituicaoICMS_ForaEstado=''S'' ');
          
          open;
          QuantidadeNaoTributado:=Fieldbyname('CONTA').asinteger;
          If (Fieldbyname('CONTA').asinteger<>0)
          Then BEgin
                    StrlCfop.add(CFOPSUBTIUICAO);
                    If (Cfoptotal<>'')
                    Then CFOPTOTAL:=CFOPTOTAL+'/'+CFOPSUBTIUICAO
                    Else CFOPTOTAL:=CFOPSUBTIUICAO;
          End;}

          //****************BASES DE CALCULO************************************
          
          //Para trabalhar as bases de calculos e descontos totais
          //tenho que selecionar todos os produtos da venda
          //e percorrer um a um
          //primeiro, preciso saber se existe desconto total
          //pois influencia na base de calculo

          //Como Funciona
          //Caso exista desconto TOTAL na nota
          //tenho que recalcular atraves de % os novos valores dos
          //produtos
          //ou seja, os produtos terao novos valores apenas
          //para calculo da base de calculo

          //resgantando o valor do desconto
          Try
             TMpDescontototal:=strtofloat(Self.ObjNotaFiscalCfop.NOTAFISCAL.get_Desconto);
          Except
                TMpdescontoTOTAL:=0;
          End;

          //resgatando o valor total da venda
          Try
             TmpValorTotalVenda:=strtofloat(Self.ObjNotaFiscalCfop.NOTAFISCAL.Get_VALORTOTAL);
          Except
                TmpValorTotalVenda:=0;
          End;
          
          if (TMpDescontototal>0)
          Then Begin
                    //descobrindo o que o desconto
                    //representa em termos de % do valor total
                    TmpPorcentagemDesconto:=(TMpDescontototal*100)/TmpValorTotalVenda;
                    TmpPorcentagemDesconto:=strtofloat(tira_ponto(formata_valor(floattostr(TmpPorcentagemDesconto))));
          End
          Else TmpPorcentagemDesconto:=0;

          //******selecionando todos os produtos da venda*******
          Close;
          SQL.clear;
          SQL.add('Select * from ProcRetornaMateriais_nf('+Pnota+')');
          open;
          first;

          TMPBASEC_ICMS:=0;
          TMPVALORICMS:=0;

          TMPBASEC_ICMS_recolhido:=0;
          TMPVALORICMS_recolhido:=0;
          

          TMPBASEC_ICMSSUBSTITUICAO:=0;
          TMPVALORICMSSUBSTITUICAO:=0;
          PvalorIPI:=0;

          While not(eof) do
          Begin
               //*****acertando o valor do produto******
               If (TMpDescontototal>0)//tem desconto geral
               Then Begin
                         //valor total-(porcentagem do valortotal)
                         //exemplo
                         //100,00 se a porcentagem=10% seria 100-10=90
                         tmpvalorproduto:=fieldbyname('valorfinal').asfloat-((fieldbyname('valorfinal').asfloat*TmpPorcentagemDesconto)/100);
               End
               Else tmpvalorproduto:=fieldbyname('valorfinal').asfloat;


               pvaloripi:=pvaloripi+((tmpvalorproduto*fieldbyname('ipi').asfloat)/100);
               

               //****Casos possiveis*****
               //Isento-> Produto com Aliquota Zero(isento,diferido,nao tributado...)
               //Substituicao->
               //            RegimeEspecial?
               //               Sim->Paga-se o Imposto sobre o valor da pauta e nao sobre o valor vendido, assim entra na parte da NF como BaseCalculoSubstituicao
               //               Nao->Coloco o Valor da Aliquota na Impressao mas nao calculo nada, porque ele pagou na substituicao
               //Tributado
               //            Com Reducao na Base Calculo, calcula-se sobre a aliquota do cupom
               //Exemplo
               //     Aliquota=17%
               //     Reducao=58.824%
               //     Aliquota Cupom (final)= 7%

               Pisento:=fieldbyname('isento').asstring;
               PSubstituicao:=fieldbyname('substituicaotributaria').asstring;
               Paliquota:=fieldbyname('aliquota').asstring;
               PvalorPauta:=fieldbyname('valorpauta').asstring;
               Preducao:=fieldbyname('REDUCAOBASECALCULO').asstring;
               PaliquotaCupom:=fieldbyname('aliquotacupom').asstring;

               //**************Validando os Campos*****************************
               if (Pisento<>'S') and (Pisento<>'N')
               Then PIsento:='N';

               if (PSubstituicao<>'S') and (PSubstituicao<>'N')
               Then PSubstituicao:='N';

               try
                  strtofloat(Paliquota);
               Except
                  Paliquota:='0';
               End;

               try
                  strtofloat(PaliquotaCupom);
               Except
                  PaliquotaCupom:='0';
               End;

               try
                  strtofloat(Preducao);
               Except
                  Preducao:='0';
               End;

               try
                  strtofloat(PvalorPauta);
               Except
                  PvalorPauta:='0';
               End;
               //**************************************************************



               if (Pisento='N')//N�o � isento
               Then Begin
                         
                         if (PSubstituicao='N')//n�o � de substituicao
                         Then Begin

                                   if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Get_IcmsRecolhidoPelaEmpresa='S')
                                   Then Begin //Fornecedores
                                            //Alterado dia 03/04/09 a pedido da camila, conforme email abaixo
                                            (*
                                            Base de c�lculo do ICMS= Valor dos produtos (1800,00) 
                                            Valor do ICMS=base de calculo do ICMS *Aliquota do ICMS (1800,00*17%=306,00)

                                            Base de C�lculo do ICMS Substitui��o= (quantidade de produto * o valor da pauta)
                                            se existir mais de um tipo de produto na NF,
                                            ser� multiplicado a quantidade pela respectiva pauta e somado tudo no final.
                                            Valor do ICMS Substitui��o=(base de c�lculo do icms substitui��o*aliquota de icms)-(valor do ICMS)

                                            *)

                                             //Calculando ja com a reducao da base de calculo SE HOUVER
                                             TMPBASEC_ICMS:=TMPBASEC_ICMS+(tmpvalorproduto-((tmpvalorproduto*Strtofloat(Preducao))/100));
                                             //utilizo o valor sem reducao porque eu utilizo a aliquota do cupom e nao a aliquota inicial
                                             TmpValorIcmsAtual:=((tmpvalorproduto*Strtofloat(PAliquotaCupom))/100);
                                             TMPVALORICMS:=TMPVALORICMS+TmpValorIcmsAtual;
                                             

                                             percentualagregado:=fieldbyname('percentualagregado').asfloat;

                                             if (percentualagregado=0)
                                             Then Begin//faz pela pauta
                                                       (*
                                                       Base de C�lculo do ICMS Substitui��o= (quantidade de produto * o valor da pauta) se existir mais de um tipo de produto na NF, ser� multiplicado a quantidade pela respectiva pauta e somado tudo no final.
                                                       Valor do ICMS Substitui��o=(base de c�lculo do icms substitui��o*aliquota de icms)-(valor do ICMS)
                                                       *)
                                                       valorprodutoindividual:=Fieldbyname('quantidade').ascurrency*strtocurr(PvalorPauta);
                                                       TMPBASEC_ICMS_recolhido:=TMPBASEC_ICMS_recolhido+valorprodutoindividual;
                                                       TMPVALORICMS_recolhido:=TMPVALORICMS_recolhido+((valorprodutoindividual*Strtofloat(PAliquotaCupom))/100) - TmpValorIcmsAtual;
                                             End
                                             Else Begin
                                                       //Faz pelo Valor Agregado

                                                       (*
                                                        = Base de Calculo = (ValorProduto+IPI)*%agregado
                                                        = ValorICMS = Base de Calculo*Aliquota - Valor Icms(ValorProduto*Aliquota)
                                                        *)
                                                        
                                                       //Calculando  (ValorProduto+Ipi)*%AGregado
                                                       Ipiindividual:=(tmpvalorproduto*fieldbyname('ipi').asfloat)/100;
                                                       valorprodutoindividual:=tmpvalorproduto+ipiindividual;
                                                       valorprodutoindividual:=valorprodutoindividual+((valorprodutoindividual*percentualagregado)/100);
                                                       //Base de Calculo
                                                       TMPBASEC_ICMS_recolhido:=TMPBASEC_ICMS_recolhido+valorprodutoindividual;
                                                       TMPVALORICMS_recolhido:=TMPVALORICMS_recolhido+((valorprodutoindividual*Strtofloat(PAliquotaCupom))/100) - TmpValorIcmsAtual;
                                            End;
                                   End
                                   Else Begin//Outros que nao sejam revendedores

                                             if (PCalculaSobreMVG=False)
                                             Then BEgin
                                                        //mantenho essa forma de calculo
                                                        //pois se for comercio � assim que
                                                        //se calcula

                                                        if (StrtoCurr(PvalorPauta)=0)
                                                        Then Begin
                                                                  //Calculo o ICMS pelo Valor do produto e com a aliquota final(cupom)
                                                                  TMPBASEC_ICMS:=TMPBASEC_ICMS+(tmpvalorproduto-((tmpvalorproduto*Strtofloat(Preducao))/100));
                                                                  TMPVALORICMS:=TMPVALORICMS+((tmpvalorproduto*Strtofloat(PAliquotaCupom))/100);
                                                        End
                                                        Else Begin//Utilizo o valor da pauta no lugar do valor do produto
                                                        
                                                                  //calculando a reducao caso EXISTA
                                                                  valorprodutoindividual:=strtofloat(PvalorPauta)-((strtofloat(PvalorPauta)*Strtofloat(Preducao))/100);
                                                                  //Valor do produto * quantidade
                                                                  valorprodutoindividual:=(valorprodutoindividual*fieldbyname('quantidade').ascurrency);
                                                                  TMPBASEC_ICMS:=TMPBASEC_ICMS+valorprodutoindividual;
                                                                  TMPVALORICMS:=TMPVALORICMS+((valorprodutoindividual*Strtofloat(PAliquotaCupom))/100);
                                                        End;
                                             End
                                             Else BEgin
                                                       (*Solicitado a alteracao dia 04/06/2009 pela Genice e sr. Carlos
                                                        em reuniao aqui na empresa
                                                        O calculo do Consumidor final sera
                                                        ((Valor NF +IPI)* Margem Valor Agregado) = BC
                                                        BC * Imposto = Valor ICMS
                                                        *)
                                             
                                                      //Calculando Valor +IPI
                                                       tmpvalorproduto:=tmpvalorproduto+((tmpvalorproduto*fieldbyname('ipi').asfloat)/100);
                                                       //calculando reducao caso haja
                                                       tmpvalorproduto:=(tmpvalorproduto-((tmpvalorproduto*Strtofloat(Preducao))/100));
                                                       //aplicando a margem do valor agregado para consumidor final
                                                       tmpvalorproduto:=tmpvalorproduto+((tmpvalorproduto*fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').asfloat)/100);

                                                       TMPBASEC_ICMS:=TMPBASEC_ICMS+tmpvalorproduto;
                                                       TMPVALORICMS:=TMPVALORICMS+((tmpvalorproduto*Strtofloat(PAliquotaCupom))/100);
                                             End




                                   End;
                         End
                         Else Begin//Substituicao, venda de produto que foi comprado ja com substituicao, nao tem imposto de saida
                                   (*
                                   TMPBASEC_ICMSSUBSTITUICAO:=TMPBASEC_ICMS+(tmpvalorproduto-((tmpvalorproduto*Strtofloat(Preducao))/100));
                                   TMPVALORICMSSUBSTITUICAO:=TMPVALORICMS+((tmpvalorproduto*Strtofloat(PAliquotaCupom))/100);
                                   *)
                         End;
               End
               Else Begin//isento
                        //n�o tributa
               End;
               
               next;
          End;//while

          if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Get_IcmsRecolhidoPelaEmpresa='N')
          Then Begin
                    self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_BASECALCULOICMS_SUBSTITUICAO(floattostr(TMPBASEC_ICMSSUBSTITUICAO));
                    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_ValorICMS_Substituicao(floattostr(TMPVALORICMSSUBSTITUICAO));

                    self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_BASECALCULOICMS_SUBST_recolhido('0');
                    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORICMS_SUBST_recolhido('0');
          End
          Else Begin
                    self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_BASECALCULOICMS_SUBSTITUICAO('0');
                    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_ValorICMS_Substituicao('0');

                    self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_BASECALCULOICMS_SUBST_recolhido(floattostr(TMPBASEC_ICMS_recolhido));
                    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORICMS_SUBST_recolhido(floattostr(TMPVALORICMS_recolhido));
          End;
          
          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_BASECALCULOICMS(floattostr(TMPBASEC_ICMS));
          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORICMS(floattostr(TMPVALORICMS));
          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORTOTALIPI(floattostr(PvalorIPI));
          Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsEdit;
          result:=Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(true);
     End;

Finally
       Freeandnil(StrlCfop);
End;

end;


function TObjNotafiscalObjetos.CalculaBasedeCalculo_NOVA(pnota: string): boolean;
VAR
StrlCfop:TStringList;

Pformula:String;


PvalorIpiProduto,pvaloripitotal:Currency;

PBC_PIS_produto:currency;
PBC_PIS_ST_produto:currency;
PBC_COFINS_produto:currency;
PBC_COFINS_ST_produto:currency;

PvalorPIS_Produto:currency;
PValorPIS_ST_Produto:currency;
PvalorCOFINS_PRODUTO:currency;
PVALORCofins_ST_Produto:currency;

PvalorPIS_Total,PValorPIS_ST_total,
PvalorCOFINS_total,
PVALORCofins_ST_total:Currency;


PvalorTotalFrete,PvalorFreteProduto,PvalorTotalSeguro,PvalorSeguroProduto:Currency;
tmpvalorproduto,TMpDescontototal,TmpValorTotalVenda,TmpPorcentagemDesconto:Currency;

TMPBASEC_ICMS_PRODUTO,TMPVALOR_ICMS_PRODUTO:Currency;
TMPBASEC_ICMS_PRODUTO_ST,TMPVALOR_ICMS_PRODUTO_ST:Currency;
TMPBASEC_ICMS,TMPVALORICMS,TMPVALORICMS_recolhido,TMPBASEC_ICMS_recolhido:currency;
PBaseCalculoIPIProduto:Currency;

  Procedure SubstituiFormulas;
  Begin
       //Funcao dentro de funcao


       //Valores calculados nos produtos

       PFormula:=StringReplace(pformula,'VALORFINAL_PROD',virgulaparaponto(floattostr(tmpvalorproduto)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VALORFRETE_PROD',virgulaparaponto(floattostr(PvalorFreteProduto)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VALORSEGURO_PROD',virgulaparaponto(floattostr(PvalorSeguroProduto)),[rfReplaceAll,rfIgnoreCase]);

       PFormula:=StringReplace(pformula,'VALORIPI_PROD',virgulaparaponto(floattostr(PvalorIpiProduto)),[rfReplaceAll,rfIgnoreCase]);

       PFormula:=StringReplace(pformula,'VALOR_PIS_PROD',virgulaparaponto(FLOATTOSTR(PvalorPIS_Produto)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VALOR_PIS_ST_PROD',virgulaparaponto(FLOATTOSTR(PvalorPIS_ST_Produto)),[rfReplaceAll,rfIgnoreCase]);

       PFormula:=StringReplace(pformula,'VALOR_COFINS_PROD',virgulaparaponto(FLOATTOSTR(PvalorCOFINS_Produto)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VALOR_COFINS_ST_PROD',virgulaparaponto(FLOATTOSTR(PvalorCOFINS_ST_Produto)),[rfReplaceAll,rfIgnoreCase]);

       //ICMS
       PFormula:=StringReplace(pformula,'PERC_REDUCAO_BC_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_PERC_REDUCAO_BC),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTA_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_ALIQUOTA),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'IVA_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_IVA),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'PAUTA_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_PAUTA),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'BC_ICMS',virgulaparaponto(floattostr(TMPBASEC_ICMS_PRODUTO)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VALOR_ICMS',virgulaparaponto(floattostr(TMPVALOR_ICMS_PRODUTO)),[rfReplaceAll,rfIgnoreCase]);
       //ICMS ST
       PFormula:=StringReplace(pformula,'PERC_REDUCAO_BC_ST_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_PERC_REDUCAO_BC_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTA_ST_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_ALIQUOTA_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'IVA_ST_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_IVA_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'PAUTA_ST_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_PAUTA_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'BC_ST_ICMS',virgulaparaponto(floattostr(TMPBASEC_ICMS_PRODUTO_ST)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VALOR_ST_ICMS',virgulaparaponto(floattostr(TMPVALOR_ICMS_PRODUTO_ST)),[rfReplaceAll,rfIgnoreCase]);

       //IPI
       PFormula:=StringReplace(pformula,'BC_IPI',virgulaparaponto(FLOATTOSTR(PBaseCalculoIPIProduto)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTA_IPI',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_IPI.Get_Aliquota),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'QTDE_UP_IPI',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_IPI.Get_QuantidadeTotalUP),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VL_UP_IPI',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_IPI.Get_ValorUnidade),[rfReplaceAll,rfIgnoreCase]);

       //PIS
       PFormula:=StringReplace(pformula,'ALIQUOTAPERCENTUAL_PIS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_PIS_ORIGEM.Get_ALIQUOTAPERCENTUAL),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTAVALOR_PIS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_PIS_ORIGEM.Get_ALIQUOTAVALOR),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'BC_PIS',virgulaparaponto(FLOATTOSTR(PBC_PIS_produto)),[rfReplaceAll,rfIgnoreCase]);
       //PIS ST
       PFormula:=StringReplace(pformula,'ALIQUOTAPERCENTUAL_ST_PIS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_PIS_ORIGEM.Get_ALIQUOTAPERCENTUAL_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTAVALOR_ST_PIS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_PIS_ORIGEM.Get_ALIQUOTAVALOR_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'BC_ST_PIS',virgulaparaponto(FLOATTOSTR(PBC_PIS_ST_produto)),[rfReplaceAll,rfIgnoreCase]);

       //COFINS
       PFormula:=StringReplace(pformula,'ALIQUOTAPERCENTUAL_COFINS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.Get_ALIQUOTAPERCENTUAL),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTAVALOR_COFINS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.Get_ALIQUOTAVALOR),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'BC_COFINS',virgulaparaponto(FLOATTOSTR(PBC_COFINS_produto)),[rfReplaceAll,rfIgnoreCase]);
       //COFINS ST
       PFormula:=StringReplace(pformula,'ALIQUOTAPERCENTUAL_ST_COFINS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.Get_ALIQUOTAPERCENTUAL_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTAVALOR_ST_COFINS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.Get_ALIQUOTAVALOR_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'BC_ST_COFINS',virgulaparaponto(FLOATTOSTR(PBC_COFINS_ST_produto)),[rfReplaceAll,rfIgnoreCase]);


  End;

begin
     //acerta a base de calculo e a aliquota
     //de acordo com os produtos da venda
     result:=False;


     PvalorIpiProduto:=0;
     pvaloripitotal:=0;
     PvalorTotalFrete:=0;
     PvalorFreteProduto:=0;
     PvalorTotalSeguro:=0;
     PvalorSeguroProduto:=0;
     tmpvalorproduto:=0;
     TMpDescontototal:=0;
     TmpValorTotalVenda:=0;
     TmpPorcentagemDesconto:=0;
     TMPBASEC_ICMS_PRODUTO:=0;
     TMPVALOR_ICMS_PRODUTO:=0;
     TMPBASEC_ICMS_PRODUTO_ST:=0;
     TMPVALOR_ICMS_PRODUTO_ST:=0;
     TMPBASEC_ICMS:=0;
     TMPVALORICMS:=0;
     TMPVALORICMS_recolhido:=0;
     TMPBASEC_ICMS_recolhido:=0;

     Try
        StrlCfop:=TStringList.create;
        StrlCfop.clear;
     Except
           Messagedlg('Erro na tentativa de cria��o da StringList de CFOPs',mterror,[mbok],0);
           exit;
     End;
Try

     If (Pnota='')
     Then exit;

     //Verificando se a venda existe
     If (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(Pnota)=False)
     Then Begin
               messagedlg('Nota Fiscal n�o localizada!',mterror,[mbok],0);
               exit;
     End;
     Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;


     With self.Objquery do
     Begin
          Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;

          //resgantando o valor do desconto
          Try
             TMpDescontototal:=strtofloat(Self.ObjNotaFiscalCfop.NOTAFISCAL.get_Desconto);
          Except
                TMpdescontoTOTAL:=0;
          End;

          //resgatando o valor total da venda
          Try

             TmpValorTotalVenda:=strtofloat(Self.ObjNotaFiscalCfop.NOTAFISCAL.Get_VALORTOTAL);
          Except
                TmpValorTotalVenda:=0;
          End;

          if (TMpDescontototal>0)
          Then Begin
                    //descobrindo o que o desconto
                    //representa em termos de % do valor total
                    TmpPorcentagemDesconto:=(TMpDescontototal*100)/TmpValorTotalVenda;
                    TmpPorcentagemDesconto:=strtofloat(tira_ponto(formata_valor(floattostr(TmpPorcentagemDesconto))));
          End
          Else TmpPorcentagemDesconto:=0;

          //******selecionando todos os produtos da venda*******
          Close;
          SQL.clear;
          SQL.add('Select * from ProcRetornaMateriais_nf('+Pnota+')');
          open;
          first;

          PvalorTotalFrete:=0;
          PvalorTotalSeguro:=0;

          //Zerando Impostos TOTAIS
          //ICMS
          TMPBASEC_ICMS:=0;
          TMPVALORICMS:=0;
          //ICMS ST
          TMPVALORICMS_recolhido:=0;
          TMPBASEC_ICMS_recolhido:=0;
          //IPI
          PvalorIPITotal:=0;
          //PIS
          PvalorPIS_Total:=0;
          PvalorPIS_ST_Total:=0;
          //COFINS
          PvalorCOFINS_Total:=0;
          PvalorCOFINS_ST_Total:=0;

          While not(eof) do
          Begin
               tmpvalorproduto:=0;

               PBC_PIS_produto:=0;
               PBC_PIS_ST_produto:=0;
               PBC_COFINS_produto:=0;
               PBC_COFINS_ST_produto:=0;

               PvalorPIS_Produto:=0;
               PValorPIS_ST_Produto:=0;
               PvalorCOFINS_PRODUTO:=0;
               PVALORCofins_ST_Produto:=0;

               PvalorIpiProduto:=0;
               TMPBASEC_ICMS_PRODUTO:=0;
               TMPVALOR_ICMS_PRODUTO:=0;
               TMPBASEC_ICMS_PRODUTO_ST:=0;
               TMPVALOR_ICMS_PRODUTO_ST:=0;



               //*****acertando o valor do produto******
               If (TMpDescontototal>0)//tem desconto geral
               Then Begin
                         //valor total-(porcentagem do valortotal)
                         //exemplo
                         //100,00 se a porcentagem=10% seria 100-10=90
                         tmpvalorproduto:=fieldbyname('valorfinal').asfloat-((fieldbyname('valorfinal').asfloat*TmpPorcentagemDesconto)/100);
               End
               Else tmpvalorproduto:=fieldbyname('valorfinal').asfloat;


               PvalorFreteProduto:=fieldbyname('valorfrete').asfloat;
               PvalorSeguroProduto:=fieldbyname('valorseguro').asfloat;
               PvalorTotalFrete:=PvalorTotalfrete+PvalorfreteProduto;
               PvalorTotalSeguro:=PvalorTotalSeguro+PvalorSeguroProduto;

               //*******************IPI************************************

               if (fieldbyname('IMPOSTO_IPI').asstring='')
               then Begin
                         MensagemErro('O PRODUTO'+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring+' n�o possui o Imposto de IPI cadastrado');
                         exit;
               End;

               if (self.ObjVidro_NF.IMPOSTO_IPI.LocalizaCodigo(fieldbyname('IMPOSTO_IPI').asstring)=False)
               Then Begin

                         MensagemErro('IMPOSTO_IPI C�DIGO '+fieldbyname('imposto_icms_origem').asstring+' N�O ENCONTRADO PARA O PRODUTO'+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
               End;
               self.ObjVidro_NF.IMPOSTO_IPI.TabelaparaObjeto;

               Pformula:=self.ObjVidro_NF.IMPOSTO_IPI.Get_FORMULABASECALCULO;
               SubstituiFormulas;

               Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
               Fcalculaformula_Vidro.RDFormula.Execute;

               if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
               Then Begin
                          mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" da BC do IPI. Express�o Inv�lida'+#13+
                          'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                           exit;
               End;
               PBaseCalculoIPIProduto:=Fcalculaformula_vidro.RDFormula.Resultado;

               //******VALOR DO IPI*****
               Pformula:=self.ObjVidro_NF.IMPOSTO_IPI.Get_formula_valor_imposto;
               SubstituiFormulas;

               Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
               Fcalculaformula_Vidro.RDFormula.Execute;

               if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
               Then Begin
                          mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" do valor do IPI. Express�o Inv�lida'+#13+
                          'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                           exit;
               End;
               PvalorIpiProduto:=Fcalculaformula_vidro.RDFormula.Resultado;



               //******************PIS*********************

               if (fieldbyname('imposto_pis_origem').asstring<>'')
               then Begin
                         if (self.ObjVidro_NF.imposto_pis_origem.LocalizaCodigo(fieldbyname('imposto_pis_origem').asstring)=False)
                         Then Begin
                                   MensagemErro('imposto_pis_origem C�DIGO '+fieldbyname('imposto_icms_origem').asstring+' N�O ENCONTRADO PARA O PRODUTO'+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                         End;
                         self.ObjVidro_NF.imposto_pis_origem.TabelaparaObjeto;

                         Pformula:=self.ObjVidro_NF.imposto_pis_origem.Get_FORMULABASECALCULO;
                         SubstituiFormulas;

                         Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
                         Fcalculaformula_Vidro.RDFormula.Execute;

                         if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
                         Then Begin
                                    mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" da BC do PIS. Express�o Inv�lida'+#13+
                                    'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                                     exit;
                         End;
                         PBC_PIS_produto:=Fcalculaformula_Vidro.RDFormula.Resultado;

                         //******VALOR DO PIS*****
                         Pformula:=self.ObjVidro_NF.imposto_pis_origem.Get_formula_valor_imposto;
                         SubstituiFormulas;

                         Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
                         Fcalculaformula_Vidro.RDFormula.Execute;

                         if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
                         Then Begin
                                    mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" do valor do PIS. Express�o Inv�lida'+#13+
                                    'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                                     exit;
                         End;
                         PvalorPIS_Produto:=Fcalculaformula_Vidro.RDFormula.Resultado;
               End;


                //******************PIS ST*******************

               if (fieldbyname('imposto_pis_destino').asstring<>'')
               then Begin
                        if (self.ObjVidro_NF.imposto_pis_destino.LocalizaCodigo(fieldbyname('imposto_pis_destino').asstring)=False)
                        Then Begin
                                  MensagemErro('imposto_pis_destino C�DIGO '+fieldbyname('imposto_icms_origem').asstring+' N�O ENCONTRADO PARA O PRODUTO'+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                        End;
                        self.ObjVidro_NF.imposto_pis_destino.TabelaparaObjeto;

                        Pformula:=self.ObjVidro_NF.imposto_pis_destino.Get_FORMULABASECALCULO_ST;
                        SubstituiFormulas;

                        Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
                        Fcalculaformula_Vidro.RDFormula.Execute;

                        if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
                        Then Begin
                                   mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" da BC do PIS ST. Express�o Inv�lida'+#13+
                                   'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                                    exit;
                        End;
                        PBC_PIS_ST_produto:=Fcalculaformula_Vidro.RDFormula.Resultado;

                        //******VALOR DO PIS ST*****
                        Pformula:=self.ObjVidro_NF.imposto_pis_destino.Get_formula_valor_imposto_ST;
                        SubstituiFormulas;

                        Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
                        Fcalculaformula_Vidro.RDFormula.Execute;

                        if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
                        Then Begin
                                   mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" do valor do PIS ST. Express�o Inv�lida'+#13+
                                   'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                                    exit;
                        End;
                        PValorPIS_ST_Produto:=Fcalculaformula_Vidro.RDFormula.Resultado;
               End;
              //******************COFINS*********************

               if (fieldbyname('imposto_COFINS_origem').asstring<>'')
               then Begin
                          if (self.ObjVidro_NF.imposto_COFINS_origem.LocalizaCodigo(fieldbyname('imposto_COFINS_origem').asstring)=False)
                          Then Begin
                                    MensagemErro('imposto_COFINS_origem C�DIGO '+fieldbyname('imposto_icms_origem').asstring+' N�O ENCONTRADO PARA O PRODUTO'+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                          End;
                          self.ObjVidro_NF.imposto_COFINS_origem.TabelaparaObjeto;

                          Pformula:=self.ObjVidro_NF.imposto_COFINS_origem.Get_FORMULABASECALCULO;
                          SubstituiFormulas;

                          Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
                          Fcalculaformula_Vidro.RDFormula.Execute;

                          if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
                          Then Begin
                                     mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" da BC do COFINS. Express�o Inv�lida'+#13+
                                     'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                                      exit;
                          End;
                          PBC_COFINS_produto:=Fcalculaformula_Vidro.RDFormula.Resultado;

                          //******VALOR DO COFINS*****
                          Pformula:=self.ObjVidro_NF.imposto_COFINS_origem.Get_formula_valor_imposto;
                          SubstituiFormulas;

                          Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
                          Fcalculaformula_Vidro.RDFormula.Execute;

                          if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
                          Then Begin
                                     mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" do valor do COFINS. Express�o Inv�lida'+#13+
                                     'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                                      exit;
                          End;
                          PvalorCOFINS_Produto:=Fcalculaformula_Vidro.RDFormula.Resultado;
               End;

                //******************COFINS ST*******************

               if (fieldbyname('imposto_COFINS_destino').asstring<>'')
               then Begin
                        if (self.ObjVidro_NF.imposto_COFINS_destino.LocalizaCodigo(fieldbyname('imposto_COFINS_destino').asstring)=False)
                        Then Begin
                                  MensagemErro('imposto_COFINS_destino C�DIGO '+fieldbyname('imposto_icms_origem').asstring+' N�O ENCONTRADO PARA O PRODUTO'+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                        End;
                        self.ObjVidro_NF.imposto_COFINS_destino.TabelaparaObjeto;

                        Pformula:=self.ObjVidro_NF.imposto_COFINS_destino.Get_FORMULABASECALCULO_ST;
                        SubstituiFormulas;

                        Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
                        Fcalculaformula_Vidro.RDFormula.Execute;

                        if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
                        Then Begin
                                   mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" da BC do COFINS ST. Express�o Inv�lida'+#13+
                                   'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                                    exit;
                        End;
                        PBC_COFINS_ST_produto:=Fcalculaformula_Vidro.RDFormula.Resultado;

                        //******VALOR DO COFINS ST*****
                        Pformula:=self.ObjVidro_NF.imposto_COFINS_destino.Get_formula_valor_imposto_ST;
                        SubstituiFormulas;

                        Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
                        Fcalculaformula_Vidro.RDFormula.Execute;

                        if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
                        Then Begin
                                   mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" do valor do COFINS ST. Express�o Inv�lida'+#13+
                                   'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                                    exit;
                        End;
                        PValorCOFINS_ST_Produto:=Fcalculaformula_Vidro.RDFormula.Resultado;
               End;

               //******************ICMS*********************************

               if (fieldbyname('imposto_icms_origem').asstring='')
               then Begin
                         MensagemErro('O PRODUTO'+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring+' n�o possui o Imposto de ICMS de ORIGEM');
                         exit;
               End;

               if (self.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.LocalizaCodigo(fieldbyname('imposto_icms_origem').asstring)=False)
               Then Begin

                         MensagemErro('IMPOSTO ICMS DE ORIGEM C�DIGO '+fieldbyname('imposto_icms_origem').asstring+' N�O ENCONTRADO PARA O PRODUTO'+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
               End;
               self.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.TabelaparaObjeto;

               if (self.ObjVidro_NF.IMPOSTO_ICMS_DESTINO.LocalizaCodigo(fieldbyname('imposto_icms_destino').asstring)=False)
               Then Begin

                         MensagemErro('IMPOSTO ICMS DE DESTINO C�DIGO '+fieldbyname('imposto_icms_destino').asstring+' N�O ENCONTRADO PARA O PRODUTO'+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
               End;
               self.ObjVidro_NF.IMPOSTO_ICMS_DESTINO.TabelaparaObjeto;



               //A Base de Calculo sempre � da Origem
               //***********************FORMULA DA BC DO ICMS**************************
               PFormula:=Self.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_FORMULA_BC;

               SubstituiFormulas;

               Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
               Fcalculaformula_Vidro.RDFormula.Execute;

               if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
               Then Begin
                          mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" da BC do ICMS. Express�o Inv�lida'+#13+
                          'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                           exit;
               End;
               TMPBASEC_ICMS_PRODUTO:=Fcalculaformula_vidro.RDFormula.Resultado;


               //***********************FORMULA DO VALOR DO ICMS**************************
               PFormula:=Self.ObjVidro_NF.IMPOSTO_ICMS_DESTINO.Get_formula_valor_imposto;
               SubstituiFormulas;

               Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
               Fcalculaformula_Vidro.RDFormula.Execute;
               if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
               Then Begin
                          mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" do Valor do ICMS. Express�o Inv�lida'+#13+
                          'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                           exit;
               End;
               TMPVALOR_ICMS_PRODUTO:=Fcalculaformula_vidro.RDFormula.Resultado;
               //***************************************************************



               //A Substitui��o sempre � do Destino
               //***********************FORMULA DA BC DO ICMS ST**************************
               PFormula:=Self.ObjVidro_NF.IMPOSTO_ICMS_DESTINO.Get_FORMULA_BC_ST;
               SubstituiFormulas;

               Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
               Fcalculaformula_Vidro.RDFormula.Execute;

               if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
               Then Begin
                          mensagemerro('N�o � poss�vel calcular a f�rmula da Base de C�lculo ST: '+PfORMULA+#13+'Express�o Inv�lida'+#13+
                          'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                           exit;
               End;

               TMPBASEC_ICMS_PRODUTO_ST:=Fcalculaformula_vidro.RDFormula.Resultado;
               //************************FORMULA DO VALOR DO ICMS ST*************************

               TMPVALOR_ICMS_PRODUTO_ST:=0;

               PFormula:=Self.ObjVidro_NF.IMPOSTO_ICMS_DESTINO.Get_formula_valor_imposto_ST;
               SubstituiFormulas;

               Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
               Fcalculaformula_Vidro.RDFormula.Execute;
               if (Fcalculaformula_Vidro.RDFormula.Falhou=True)
               Then Begin
                          mensagemerro('N�o � poss�vel calcular a f�rmula: '+PfORMULA+#13+'Express�o Inv�lida'+#13+
                          'Produto '+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring);
                           exit;
               End;
               TMPVALOR_ICMS_PRODUTO_ST:=Fcalculaformula_vidro.RDFormula.Resultado;

               //****************************************************************


               //**************Acumulando os valores dos produtos nos totais***********

               //**********************Arredondando**************************
               PvalorIpiProduto:=Self.Arredonda_NFE(PvalorIpiProduto);
               PvalorPIS_Produto:=Self.Arredonda_NFE(PvalorPIS_Produto);
               PValorPIS_ST_Produto:=Self.Arredonda_NFE(PValorPIS_ST_Produto);
               PvalorCOFINS_Produto:=Self.Arredonda_NFE(PvalorCOFINS_Produto);
               PValorCOFINS_ST_Produto:=Self.Arredonda_NFE(PValorCOFINS_ST_Produto);

               TMPBASEC_ICMS_PRODUTO:=Self.Arredonda_NFE(TMPBASEC_ICMS_PRODUTO);
               TMPVALOR_ICMS_PRODUTO:=Self.Arredonda_NFE(TMPVALOR_ICMS_PRODUTO);
               TMPBASEC_ICMS_PRODUTO_ST:=Self.Arredonda_NFE(TMPBASEC_ICMS_PRODUTO_ST);
               TMPVALOR_ICMS_PRODUTO_ST:=Self.Arredonda_NFE(TMPVALOR_ICMS_PRODUTO_ST);
               //***************************************************************

               pvaloripitotal:=pvaloripitotal+PvalorIpiProduto;
               PvalorPIS_Total:=PvalorPIS_Total+PvalorPIS_Produto;
               PValorPIS_ST_total:=PValorPIS_ST_total+PValorPIS_ST_Produto;
               PvalorCOFINS_Total:=PvalorCOFINS_Total+PvalorCOFINS_Produto;
               PValorCOFINS_ST_total:=PValorCOFINS_ST_total+PValorCOFINS_ST_Produto;

               TMPBASEC_ICMS:=TMPBASEC_ICMS + TMPBASEC_ICMS_PRODUTO;
               TMPVALORICMS:=TMPVALORICMS   + TMPVALOR_ICMS_PRODUTO;

               TMPBASEC_ICMS_recolhido:=TMPBASEC_ICMS_recolhido+TMPBASEC_ICMS_PRODUTO_ST;
               TMPVALORICMS_recolhido:=TMPVALORICMS_recolhido+TMPVALOR_ICMS_PRODUTO_ST;

               //****************************************************************************

               IF (fieldbyname('tipo').asstring='VIDRO')
               Then Begin
                         With Self.ObjVidro_NF do
                         begin
                              ZerarTabela;
                              LocalizaCodigo(fieldbyname('codigo').asstring);
                              TabelaparaObjeto;
                              Status:=dsEdit;

                              {
                              Submit_BC_ICMS(formatfloat('0.00',TMPBASEC_ICMS_PRODUTO));
                              Submit_VALOR_ICMS(formatfloat('0.00',TMPVALOR_ICMS_PRODUTO));

                              Submit_BC_ICMS_ST(formatfloat('0.00',TMPBASEC_ICMS_PRODUTO_ST));
                              Submit_VALOR_ICMS_ST(formatfloat('0.00',TMPVALOR_ICMS_PRODUTO_ST));

                              Submit_BC_IPI(formatfloat('0.00',PBaseCalculoIPIProduto));
                              Submit_VALOR_IPI(formatfloat('0.00',PvalorIpiProduto));

                              Submit_BC_PIS(formatfloat('0.00',PBC_PIS_produto));
                              Submit_VALOR_PIS(formatfloat('0.00',PvalorPIS_Produto));

                              Submit_BC_PIS_ST(formatfloat('0.00',PBC_PIS_ST_produto));
                              Submit_VALOR_PIS_ST(formatfloat('0.00',PValorPIS_ST_Produto));

                              Submit_BC_COFINS(formatfloat('0.00',PBC_COFINS_produto));
                              Submit_VALOR_COFINS(formatfloat('0.00',PvalorCOFINS_PRODUTO));

                              Submit_BC_COFINS_ST(formatfloat('0.00',PBC_COFINS_ST_produto));
                              Submit_VALOR_COFINS_ST(formatfloat('0.00',PVALORCofins_ST_Produto));
                              }

                              Submit_BC_ICMS(CurrToStr(TMPBASEC_ICMS_PRODUTO));
                              Submit_VALOR_ICMS(CurrToStr(TMPVALOR_ICMS_PRODUTO));

                              Submit_BC_ICMS_ST(CurrToStr(TMPBASEC_ICMS_PRODUTO_ST));
                              Submit_VALOR_ICMS_ST(CurrToStr(TMPVALOR_ICMS_PRODUTO_ST));

                              Submit_BC_IPI(CurrToStr(PBaseCalculoIPIProduto));
                              Submit_VALOR_IPI(CurrToStr(PvalorIpiProduto));

                              Submit_BC_PIS(CurrToStr(PBC_PIS_produto));
                              Submit_VALOR_PIS(CurrToStr(PvalorPIS_Produto));

                              Submit_BC_PIS_ST(CurrToStr(PBC_PIS_ST_produto));
                              Submit_VALOR_PIS_ST(CurrToStr(PValorPIS_ST_Produto));

                              Submit_BC_COFINS(CurrToStr(PBC_COFINS_produto));
                              Submit_VALOR_COFINS(CurrToStr(PvalorCOFINS_PRODUTO));

                              Submit_BC_COFINS_ST(CurrToStr(PBC_COFINS_ST_produto));
                              Submit_VALOR_COFINS_ST(CurrToStr(PVALORCofins_ST_Produto));

                              if (Salvar(true)=false)
                              then Begin
                                        mensagemerro('Erro na tentativa de editar o VIDRO com os valores dos impostos');
                                        exit;
                              End;
                         End;
               End;

               IF (fieldbyname('tipo').asstring='FERRAGEM')
               Then Begin
                        With Self.ObjFERRAGEM_NF do
                         begin
                              ZerarTabela;
                              LocalizaCodigo(fieldbyname('codigo').asstring);
                              TabelaparaObjeto;
                              Status:=dsEdit;
                              {
                              Submit_BC_ICMS(formatfloat('0.00',TMPBASEC_ICMS_PRODUTO));
                              Submit_VALOR_ICMS(formatfloat('0.00',TMPVALOR_ICMS_PRODUTO));

                              Submit_BC_ICMS_ST(formatfloat('0.00',TMPBASEC_ICMS_PRODUTO_ST));
                              Submit_VALOR_ICMS_ST(formatfloat('0.00',TMPVALOR_ICMS_PRODUTO_ST));

                              Submit_BC_IPI(formatfloat('0.00',PBaseCalculoIPIProduto));
                              Submit_VALOR_IPI(formatfloat('0.00',PvalorIpiProduto));

                              Submit_BC_PIS(formatfloat('0.00',PBC_PIS_produto));
                              Submit_VALOR_PIS(formatfloat('0.00',PvalorPIS_Produto));

                              Submit_BC_PIS_ST(formatfloat('0.00',PBC_PIS_ST_produto));
                              Submit_VALOR_PIS_ST(formatfloat('0.00',PValorPIS_ST_Produto));

                              Submit_BC_COFINS(formatfloat('0.00',PBC_COFINS_produto));
                              Submit_VALOR_COFINS(formatfloat('0.00',PvalorCOFINS_PRODUTO));

                              Submit_BC_COFINS_ST(formatfloat('0.00',PBC_COFINS_ST_produto));
                              Submit_VALOR_COFINS_ST(formatfloat('0.00',PVALORCofins_ST_Produto));
                              }

                              Submit_BC_ICMS(CurrToStr(TMPBASEC_ICMS_PRODUTO));
                              Submit_VALOR_ICMS(CurrToStr(TMPVALOR_ICMS_PRODUTO));

                              Submit_BC_ICMS_ST(CurrToStr(TMPBASEC_ICMS_PRODUTO_ST));
                              Submit_VALOR_ICMS_ST(CurrToStr(TMPVALOR_ICMS_PRODUTO_ST));

                              Submit_BC_IPI(CurrToStr(PBaseCalculoIPIProduto));
                              Submit_VALOR_IPI(CurrToStr(PvalorIpiProduto));

                              Submit_BC_PIS(CurrToStr(PBC_PIS_produto));
                              Submit_VALOR_PIS(CurrToStr(PvalorPIS_Produto));

                              Submit_BC_PIS_ST(CurrToStr(PBC_PIS_ST_produto));
                              Submit_VALOR_PIS_ST(CurrToStr(PValorPIS_ST_Produto));

                              Submit_BC_COFINS(CurrToStr(PBC_COFINS_produto));
                              Submit_VALOR_COFINS(CurrToStr(PvalorCOFINS_PRODUTO));

                              Submit_BC_COFINS_ST(CurrToStr(PBC_COFINS_ST_produto));
                              Submit_VALOR_COFINS_ST(CurrToStr(PVALORCofins_ST_Produto));

                              if (Salvar(true)=false)
                              then Begin
                                        mensagemerro('Erro na tentativa de editar o FERRAGEM com os valores dos impostos');
                                        exit;
                              End;
                         End;
               End;

               IF (fieldbyname('tipo').asstring='PERFILADO')
               Then Begin
                         With Self.ObjPERFILADO_NF do
                         begin
                              ZerarTabela;
                              LocalizaCodigo(fieldbyname('codigo').asstring);
                              TabelaparaObjeto;
                              Status:=dsEdit;
                              {
                              Submit_BC_ICMS(formatfloat('0.00',TMPBASEC_ICMS_PRODUTO));
                              Submit_VALOR_ICMS(formatfloat('0.00',TMPVALOR_ICMS_PRODUTO));

                              Submit_BC_ICMS_ST(formatfloat('0.00',TMPBASEC_ICMS_PRODUTO_ST));
                              Submit_VALOR_ICMS_ST(formatfloat('0.00',TMPVALOR_ICMS_PRODUTO_ST));

                              Submit_BC_IPI(formatfloat('0.00',PBaseCalculoIPIProduto));
                              Submit_VALOR_IPI(formatfloat('0.00',PvalorIpiProduto));

                              Submit_BC_PIS(formatfloat('0.00',PBC_PIS_produto));
                              Submit_VALOR_PIS(formatfloat('0.00',PvalorPIS_Produto));

                              Submit_BC_PIS_ST(formatfloat('0.00',PBC_PIS_ST_produto));
                              Submit_VALOR_PIS_ST(formatfloat('0.00',PValorPIS_ST_Produto));

                              Submit_BC_COFINS(formatfloat('0.00',PBC_COFINS_produto));
                              Submit_VALOR_COFINS(formatfloat('0.00',PvalorCOFINS_PRODUTO));

                              Submit_BC_COFINS_ST(formatfloat('0.00',PBC_COFINS_ST_produto));
                              Submit_VALOR_COFINS_ST(formatfloat('0.00',PVALORCofins_ST_Produto));
                              }

                              Submit_BC_ICMS(CurrToStr(TMPBASEC_ICMS_PRODUTO));
                              Submit_VALOR_ICMS(CurrToStr(TMPVALOR_ICMS_PRODUTO));

                              Submit_BC_ICMS_ST(CurrToStr(TMPBASEC_ICMS_PRODUTO_ST));
                              Submit_VALOR_ICMS_ST(CurrToStr(TMPVALOR_ICMS_PRODUTO_ST));

                              Submit_BC_IPI(CurrToStr(PBaseCalculoIPIProduto));
                              Submit_VALOR_IPI(CurrToStr(PvalorIpiProduto));

                              Submit_BC_PIS(CurrToStr(PBC_PIS_produto));
                              Submit_VALOR_PIS(CurrToStr(PvalorPIS_Produto));

                              Submit_BC_PIS_ST(CurrToStr(PBC_PIS_ST_produto));
                              Submit_VALOR_PIS_ST(CurrToStr(PValorPIS_ST_Produto));

                              Submit_BC_COFINS(CurrToStr(PBC_COFINS_produto));
                              Submit_VALOR_COFINS(CurrToStr(PvalorCOFINS_PRODUTO));

                              Submit_BC_COFINS_ST(CurrToStr(PBC_COFINS_ST_produto));
                              Submit_VALOR_COFINS_ST(CurrToStr(PVALORCofins_ST_Produto));
                                                            
                              if (Salvar(true)=false)
                              then Begin
                                        mensagemerro('Erro na tentativa de editar o PERFILADO com os valores dos impostos');
                                        exit;
                              End;
                         End;
               End;

               IF (fieldbyname('tipo').asstring='KITBOX')
               Then Begin
                        With Self.ObjKITBOX_NF do
                         begin
                              ZerarTabela;
                              LocalizaCodigo(fieldbyname('codigo').asstring);
                              TabelaparaObjeto;
                              Status:=dsEdit;
                              {
                              Submit_BC_ICMS(formatfloat('0.00',TMPBASEC_ICMS_PRODUTO));
                              Submit_VALOR_ICMS(formatfloat('0.00',TMPVALOR_ICMS_PRODUTO));

                              Submit_BC_ICMS_ST(formatfloat('0.00',TMPBASEC_ICMS_PRODUTO_ST));
                              Submit_VALOR_ICMS_ST(formatfloat('0.00',TMPVALOR_ICMS_PRODUTO_ST));

                              Submit_BC_IPI(formatfloat('0.00',PBaseCalculoIPIProduto));
                              Submit_VALOR_IPI(formatfloat('0.00',PvalorIpiProduto));

                              Submit_BC_PIS(formatfloat('0.00',PBC_PIS_produto));
                              Submit_VALOR_PIS(formatfloat('0.00',PvalorPIS_Produto));

                              Submit_BC_PIS_ST(formatfloat('0.00',PBC_PIS_ST_produto));
                              Submit_VALOR_PIS_ST(formatfloat('0.00',PValorPIS_ST_Produto));

                              Submit_BC_COFINS(formatfloat('0.00',PBC_COFINS_produto));
                              Submit_VALOR_COFINS(formatfloat('0.00',PvalorCOFINS_PRODUTO));

                              Submit_BC_COFINS_ST(formatfloat('0.00',PBC_COFINS_ST_produto));
                              Submit_VALOR_COFINS_ST(formatfloat('0.00',PVALORCofins_ST_Produto));
                              }

                              Submit_BC_ICMS(CurrToStr(TMPBASEC_ICMS_PRODUTO));
                              Submit_VALOR_ICMS(CurrToStr(TMPVALOR_ICMS_PRODUTO));

                              Submit_BC_ICMS_ST(CurrToStr(TMPBASEC_ICMS_PRODUTO_ST));
                              Submit_VALOR_ICMS_ST(CurrToStr(TMPVALOR_ICMS_PRODUTO_ST));

                              Submit_BC_IPI(CurrToStr(PBaseCalculoIPIProduto));
                              Submit_VALOR_IPI(CurrToStr(PvalorIpiProduto));

                              Submit_BC_PIS(CurrToStr(PBC_PIS_produto));
                              Submit_VALOR_PIS(CurrToStr(PvalorPIS_Produto));

                              Submit_BC_PIS_ST(CurrToStr(PBC_PIS_ST_produto));
                              Submit_VALOR_PIS_ST(CurrToStr(PValorPIS_ST_Produto));

                              Submit_BC_COFINS(CurrToStr(PBC_COFINS_produto));
                              Submit_VALOR_COFINS(CurrToStr(PvalorCOFINS_PRODUTO));

                              Submit_BC_COFINS_ST(CurrToStr(PBC_COFINS_ST_produto));
                              Submit_VALOR_COFINS_ST(CurrToStr(PVALORCofins_ST_Produto));
                                                            
                              if (Salvar(true)=false)
                              then Begin
                                        mensagemerro('Erro na tentativa de editar o KITBOX com os valores dos impostos');
                                        exit;
                              End;
                         End;
               End;

               IF (fieldbyname('tipo').asstring='PERSIANA')
               Then Begin
                         With Self.ObjPERSIANA_NF do
                         begin
                              ZerarTabela;
                              LocalizaCodigo(fieldbyname('codigo').asstring);
                              TabelaparaObjeto;
                              Status:=dsEdit;

                              {
                              Submit_BC_ICMS(formatfloat('0.00',TMPBASEC_ICMS_PRODUTO));
                              Submit_VALOR_ICMS(formatfloat('0.00',TMPVALOR_ICMS_PRODUTO));

                              Submit_BC_ICMS_ST(formatfloat('0.00',TMPBASEC_ICMS_PRODUTO_ST));
                              Submit_VALOR_ICMS_ST(formatfloat('0.00',TMPVALOR_ICMS_PRODUTO_ST));

                              Submit_BC_IPI(formatfloat('0.00',PBaseCalculoIPIProduto));
                              Submit_VALOR_IPI(formatfloat('0.00',PvalorIpiProduto));

                              Submit_BC_PIS(formatfloat('0.00',PBC_PIS_produto));
                              Submit_VALOR_PIS(formatfloat('0.00',PvalorPIS_Produto));

                              Submit_BC_PIS_ST(formatfloat('0.00',PBC_PIS_ST_produto));
                              Submit_VALOR_PIS_ST(formatfloat('0.00',PValorPIS_ST_Produto));

                              Submit_BC_COFINS(formatfloat('0.00',PBC_COFINS_produto));
                              Submit_VALOR_COFINS(formatfloat('0.00',PvalorCOFINS_PRODUTO));

                              Submit_BC_COFINS_ST(formatfloat('0.00',PBC_COFINS_ST_produto));
                              Submit_VALOR_COFINS_ST(formatfloat('0.00',PVALORCofins_ST_Produto));
                              }

                              Submit_BC_ICMS(CurrToStr(TMPBASEC_ICMS_PRODUTO));
                              Submit_VALOR_ICMS(CurrToStr(TMPVALOR_ICMS_PRODUTO));

                              Submit_BC_ICMS_ST(CurrToStr(TMPBASEC_ICMS_PRODUTO_ST));
                              Submit_VALOR_ICMS_ST(CurrToStr(TMPVALOR_ICMS_PRODUTO_ST));

                              Submit_BC_IPI(CurrToStr(PBaseCalculoIPIProduto));
                              Submit_VALOR_IPI(CurrToStr(PvalorIpiProduto));

                              Submit_BC_PIS(CurrToStr(PBC_PIS_produto));
                              Submit_VALOR_PIS(CurrToStr(PvalorPIS_Produto));

                              Submit_BC_PIS_ST(CurrToStr(PBC_PIS_ST_produto));
                              Submit_VALOR_PIS_ST(CurrToStr(PValorPIS_ST_Produto));

                              Submit_BC_COFINS(CurrToStr(PBC_COFINS_produto));
                              Submit_VALOR_COFINS(CurrToStr(PvalorCOFINS_PRODUTO));

                              Submit_BC_COFINS_ST(CurrToStr(PBC_COFINS_ST_produto));
                              Submit_VALOR_COFINS_ST(CurrToStr(PVALORCofins_ST_Produto));
                                                            
                              if (Salvar(true)=false)
                              then Begin
                                        mensagemerro('Erro na tentativa de editar o PERSIANA com os valores dos impostos');
                                        exit;
                              End;
                         End;
               End;

               IF (fieldbyname('tipo').asstring='DIVERSO')
               Then Begin
                         With Self.ObjDIVERSO_NF do
                         begin
                              ZerarTabela;
                              LocalizaCodigo(fieldbyname('codigo').asstring);
                              TabelaparaObjeto;
                              Status:=dsEdit;
                              {
                              Submit_BC_ICMS(formatfloat('0.00',TMPBASEC_ICMS_PRODUTO));
                              Submit_VALOR_ICMS(formatfloat('0.00',TMPVALOR_ICMS_PRODUTO));

                              Submit_BC_ICMS_ST(formatfloat('0.00',TMPBASEC_ICMS_PRODUTO_ST));
                              Submit_VALOR_ICMS_ST(formatfloat('0.00',TMPVALOR_ICMS_PRODUTO_ST));

                              Submit_BC_IPI(formatfloat('0.00',PBaseCalculoIPIProduto));
                              Submit_VALOR_IPI(formatfloat('0.00',PvalorIpiProduto));

                              Submit_BC_PIS(formatfloat('0.00',PBC_PIS_produto));
                              Submit_VALOR_PIS(formatfloat('0.00',PvalorPIS_Produto));

                              Submit_BC_PIS_ST(formatfloat('0.00',PBC_PIS_ST_produto));
                              Submit_VALOR_PIS_ST(formatfloat('0.00',PValorPIS_ST_Produto));

                              Submit_BC_COFINS(formatfloat('0.00',PBC_COFINS_produto));
                              Submit_VALOR_COFINS(formatfloat('0.00',PvalorCOFINS_PRODUTO));

                              Submit_BC_COFINS_ST(formatfloat('0.00',PBC_COFINS_ST_produto));
                              Submit_VALOR_COFINS_ST(formatfloat('0.00',PVALORCofins_ST_Produto));
                              }

                              Submit_BC_ICMS(CurrToStr(TMPBASEC_ICMS_PRODUTO));
                              Submit_VALOR_ICMS(CurrToStr(TMPVALOR_ICMS_PRODUTO));

                              Submit_BC_ICMS_ST(CurrToStr(TMPBASEC_ICMS_PRODUTO_ST));
                              Submit_VALOR_ICMS_ST(CurrToStr(TMPVALOR_ICMS_PRODUTO_ST));

                              Submit_BC_IPI(CurrToStr(PBaseCalculoIPIProduto));
                              Submit_VALOR_IPI(CurrToStr(PvalorIpiProduto));

                              Submit_BC_PIS(CurrToStr(PBC_PIS_produto));
                              Submit_VALOR_PIS(CurrToStr(PvalorPIS_Produto));

                              Submit_BC_PIS_ST(CurrToStr(PBC_PIS_ST_produto));
                              Submit_VALOR_PIS_ST(CurrToStr(PValorPIS_ST_Produto));

                              Submit_BC_COFINS(CurrToStr(PBC_COFINS_produto));
                              Submit_VALOR_COFINS(CurrToStr(PvalorCOFINS_PRODUTO));

                              Submit_BC_COFINS_ST(CurrToStr(PBC_COFINS_ST_produto));
                              Submit_VALOR_COFINS_ST(CurrToStr(PVALORCofins_ST_Produto));
                                                            
                              if (Salvar(true)=false)
                              then Begin
                                        mensagemerro('Erro na tentativa de editar o DIVERSO com os valores dos impostos');
                                        exit;
                              End;
                         End;
               End;



               next;
          End;//while

          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_BASECALCULOICMS_SUBST_recolhido(formatfloat('0.00',TMPBASEC_ICMS_recolhido));
          Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORICMS_SUBST_recolhido(formatfloat('0.00',TMPVALORICMS_recolhido));

          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_BASECALCULOICMS_SUBSTITUICAO('0');
          Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_ValorICMS_Substituicao('0');

          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_BASECALCULOICMS(formatfloat('0.00',TMPBASEC_ICMS));
          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORICMS(formatfloat('0.00',TMPVALORICMS));

          //Adiciona a Soma de IPI, Frete e Seguro
          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORTOTALIPI(formatfloat('0.00',pvaloripitotal));
          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORFRETE(formatfloat('0.00',PvalorTotalFrete));
          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORseguro(formatfloat('0.00',PvalorTotalseguro));

          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_valorpis(formatfloat('0.00',PvalorPIS_Total));
          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_valorpis_St(formatfloat('0.00',PValorPIS_ST_total));

          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_valorcofins(formatfloat('0.00',Pvalorcofins_Total));
          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_valorcofins_St(formatfloat('0.00',PValorcofins_ST_total));

          Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsEdit;
          result:=Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(true);
     End;

Finally
       Freeandnil(StrlCfop);
End;

end;



function TObjNotafiscalObjetos.CalculaValorTotal(pnota: string): boolean;
begin
     result:=False;

     if (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(pnota)=False)
     Then begin
               mensagemerro('Nota N�o encontrada');
               exit;
     End;
     Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;

     With Self.Objquery do
     Begin
          close;
          SQL.clear;
          SQL.add('Select sum(valorfinal) as SOMA From ProcRetornaMateriais_NF('+pnota+')');
          open;
          Self.ObjNotaFiscalCfop.NOTAFISCAL.status:=dsedit;
          Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_valortotal(fieldbyname('soma').asstring);
          Result:=Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(true);
     End;
end;

procedure TObjNotafiscalObjetos.ImprimeItensNF(Pmaterial:String);
var
Qprodutos:tibquery;
pdatainicial,pdatafinal:tdate;
pproduto:string;
somaquantidade,somavalorfinal:currency;

begin

     With FfiltroImp do
     begin
           DesativaGrupos;
           Grupo01.Enabled:=true;
           Grupo02.Enabled:=true;
           LbGrupo01.caption:='Data Inicial';
           LbGrupo02.caption:='Data Inicial';
           edtgrupo01.EditMask:=MascaraData;
           edtgrupo02.EditMask:=MascaraData;

           showmodal;

           if (tag=0)
           Then exit;

           if (Validadata(1,pdatainicial,false)=False)
           Then Begin
                     mensagemerro('Data inicial inv�lida');
                     exit;
           End;

           if (Validadata(2,pdatafinal,false)=False)
           Then Begin
                     mensagemerro('Data final inv�lida');
                     exit;
           End;

     End;


     Try

         Qprodutos:=Tibquery.create(nil);
         Qprodutos.database:=Self.Objquery.database;
     Except
           messagedlg('Erro na cria��o de objetos!',mterror,[mbok],0);
     End;

try

     //SERA MOSTRADO POR DIA, SENDO ASSIM, POSSO TER MAIS DE UMA NF NO MESMO DIA
     //ENTAO PRECISO SEPARAR POR DIAS DEPOIS FILTRAR QUAIS NFS ESTAVAM NAQUELE DIA

     With Qprodutos do
     Begin
          close;
          SQL.clear;
          SQL.add('Select dataemissao,codigoprodutocor,nomecor,produto,sum(quantidade) as SOMAQUANTIDADE,sum(valorfinal) as SOMAVALORFINAL');
          SQL.add('from procretornamateriais_todas_nf');
          SQL.add('where TIPO='+#39+Pmaterial+#39);
          sql.add('and DATAEMISSAO>='+#39+formatdatetime('mm/dd/yyyy',pdatainicial)+#39);
          sql.add('and DATAEMISSAO<='+#39+formatdatetime('mm/dd/yyyy',pdatafinal)+#39);
          SQL.add('group by codigoprodutocor,produto,nomecor,dataemissao');
          SQL.add('order by codigoprodutocor,produto,nomecor,dataemissao');
          open;
          if (Qprodutos.recordcount=0)
          then Begin
                    mensagemaviso('Nenhum produto foi selecionado');
                    exit;
          End;


          Qprodutos.First;
          With FreltxtRDPRINT do
          Begin
              ConfiguraImpressao;
              RDprint.Abrir;
              linhaLocal:=3;

              if (RDprint.Setup=False)
              then begin
                        RDprint.Fechar;
                        Exit;
              End;

              RDprint.impc(linhalocal,45,'PRODUTOS DE NOTA FISCAIS',[negrito]);
              IncrementaLinha(2);

              VerificaLinha;
              RDprint.impf(linhalocal,10,completapalavra('EMISSAO',10,' ')+' '+
                                        CompletaPalavra_a_Esquerda('QUANTIDADE',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('VALOR FINAL',12,' '),[negrito]);
              IncrementaLinha(1);
              DesenhaLinha;
              IncrementaLinha(1);
              
              Pproduto:='';
              VerificaLinha;
              RDprint.impf(linhalocal,1,completapalavra(fieldbyname('codigoprodutocor').asstring,5,' ')+' '+
                                        completapalavra(fieldbyname('produto').asstring,60,' ')+' '+
                                        completapalavra(fieldbyname('nomecor').asstring,20,' '),[negrito]);
              IncrementaLinha(2);
              Pproduto:=fieldbyname('codigoprodutocor').asstring;
              somaquantidade:=0;
              somavalorfinal:=0;

              While not(Qprodutos.eof) do
              Begin
                   if (Pproduto<>fieldbyname('codigoprodutocor').asstring)
                   Then Begin
                            //totalizando a anterior
                            verificalinha;
                            RDprint.impf(linhalocal,10,completapalavra('',10,' ')+' '+
                                                     completaPalavra_a_Esquerda(formata_valor(somaQUANTIDADE),12,' ')+' '+
                                                     completaPalavra_a_Esquerda(formata_valor(somaVALORFINAL),12,' '),[negrito]);
                            IncrementaLinha(1);
                            somaquantidade:=0;
                            somavalorfinal:=0;


                            IncrementaLinha(1);
                            VerificaLinha;
                            RDprint.impf(linhalocal,1,completapalavra(fieldbyname('codigoprodutocor').asstring,5,' ')+' '+
                                                      completapalavra(fieldbyname('produto').asstring,60,' ')+' '+
                                                      completapalavra(fieldbyname('nomecor').asstring,20,' '),[negrito]);
                            IncrementaLinha(2);
                            Pproduto:=fieldbyname('codigoprodutocor').asstring;
                   End;

                   VerificaLinha;
                   RDprint.imp(linhalocal,10,fieldbyname('dataemissao').asstring+' '+
                   completaPalavra_a_Esquerda(formata_valor(fieldbyname('somaQUANTIDADE').asstring),12,' ')+' '+
                   completaPalavra_a_Esquerda(formata_valor(fieldbyname('somaVALORFINAL').asstring),12,' '));
                   IncrementaLinha(1);

                   somaquantidade:=somaquantidade+fieldbyname('somaQUANTIDADE').asfloat;
                   somavalorfinal:=somavalorfinal+fieldbyname('somavalorfinal').asfloat;

                   Qprodutos.next;
              End;
              verificalinha;
              RDprint.impf(linhalocal,10,completapalavra('',10,' ')+' '+
                                         completaPalavra_a_Esquerda(formata_valor(somaQUANTIDADE),12,' ')+' '+
                                         completaPalavra_a_Esquerda(formata_valor(somaVALORFINAL),12,' '),[negrito]);
              IncrementaLinha(1);
              desenhalinha;

              rdprint.fechar;
          End;
     End;
Finally
       FMostraBarraProgresso.close;
       Freeandnil(Qprodutos);
End;                  

end;

function TObjNotafiscalObjetos.CalculaPeso(pnota: string): boolean;
var
ppesototal:currency;
begin
     result:=False;

     If (Pnota='')
     Then exit;

     //Verificando se a venda existe
     If (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(Pnota)=False)
     Then Begin
               messagedlg('Nota Fiscal n�o localizada!',mterror,[mbok],0);
               exit;
     End;
     Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;


     With self.Objquery do
     Begin
          Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
          //******selecionando todos os produtos da venda*******
          Close;
          SQL.clear;
          SQL.add('Select QUANTIDADE,PESOINDIVIDUAL from ProcRetornaMateriais_nf('+Pnota+')');
          open;
          first;
          Ppesototal:=0;
          While not(eof) do
          Begin
               if (fieldbyname('pesoindividual').asstring<>'')
               Then Begin
                         Try
                            ppesototal:=ppesototal+(fieldbyname('quantidade').ascurrency*fieldbyname('pesoindividual').ascurrency);
                         Except

                         End;
               End;

               next;
          End;

          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_PesoBruto(floattostr(ppesototal));
          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_PesoLiquido(floattostr(ppesototal));
          Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsEdit;
          result:=Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(true);
     End;
end;



function TObjNotafiscalObjetos.GeraNfe(Pnota: string): boolean;
Begin

     Result:=Self.GeraNfe(Pnota,False,False);
End;


function TObjNotafiscalObjetos.MenuNfe: boolean;
begin

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO MENU NFE')=False)
     Then exit;

     FmenuNfe.PassaObjeto(Self);
     FmenuNfe.Showmodal;

end;


function TObjNotafiscalObjetos.Arredonda_NFE(PValor: Currency): Currency;
var
Str,antes,depois:string;
posicao:integer;
begin
     (*
     Esta fun��o ser� chamada para fazer os arredondamento na hora de calcular os impostos
     Percebi que no Sistema Gratuito o arredondamento n�o � utilizado, ou seja
     ele simplesmente elimina a 3� casa
     Um produto de 10,57 * 17% = 1,7969   pelo m�todo de arredondamento o 6 que est� na
     terceira casa transformaria o 1,79 em 1,80,
     por�m ele mant�m o valor de 1,79 no ICMS*)

     //Deixei a crit�rio do Cliente (parametro pesquisado no create deste objeto)

     if (Self.ModoArredondamento=MC_Copia2)
     Then Begin
              {
              Str:=CurrToStr(Pvalor);
              posicao:=pos(',',str);

              if (Posicao=0)//nao tem casa decimal
              Then Begin
                       result:=Pvalor;
                       exit;
              End;

              antes:=copy(str,1,posicao);//copio at� a virgula
              depois:=copy(str,posicao+1,length(str)-posicao);//copio depois da virgula

              if (length(depois)>=2)//se tiver de 2 a mais casas
              then depois:=depois[1]+depois[2]; //copio apenas as 2
              //senao nao preciso mexer

              result:=strtocurr(antes+depois);
              }
              //alterado por celio 21/10/2013, caso o valor ser ex: 193,96, o resultado
              //era 194... nem precisa falar mais nada
              Result := StrToCurr(formata_valor(CurrToStr(PValor),2,false));
     End
     Else Begin
               //arredondamento
               //result:=strtocurr(formatfloat('0.00',pvalor));
               Result := StrToCurr(formata_valor(CurrToStr(PValor),2,true));

     End;


End;

procedure TObjNotafiscalObjetos.ReimprimeNfe(Pnotafiscal: string);
begin
     if (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(Pnotafiscal)=False)
     then Begin
               MensagemErro('Nota Fiscal n�o encontrada');
               exit;
     End;
     Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;

     if (Self.ObjNotaFiscalCfop.NOTAFISCAL.NFE.Get_codigo='')
     then Begin
               mensagemerro('Esta NF n�o est� vinculada a nenhuma NFE');
               exit;
     End;

     if (Self.objgeranfe.Nfe.LocalizaCodigo(Self.ObjNotaFiscalCfop.NOTAFISCAL.NFe.Get_codigo)=False)
     Then Begin
               MensagemErro('Nfe c�digo '+Self.ObjNotaFiscalCfop.NOTAFISCAL.NFE.Get_codigo+' n�o localizada');
               exit;
     End;
     Self.objgeranfe.Nfe.TabelaparaObjeto;

     if (Self.objgeranfe.Nfe.Get_STATUSNOTA='C')
     Then Begin
               if (MensagemPergunta('Esta NFe encontra-se cancelada, certeza que deseja re-imprimir?')=mrno)
               Then exit;
     End;

     if (Self.objgeranfe.Nfe.ExtraiArquivoNFe=False)
     Then exit;

     FmenuNFE.PassaObjeto(Self);
     FmenuNFE.ImprimeNFeArquivo(RetornaPastaSistema+'ARQUIVOB.XML');

end;


procedure TObjNotafiscalObjetos.EnviaNfe_Contingencia(Pnotafiscal: string);
var
precibo,pnfe:String;
PObjNotaFiscal:TObjNotaFiscal;
begin

     PObjNotaFiscal:=Self.ObjNotaFiscalCfop.NOTAFISCAL;
     
     if (PObjNotaFiscal.LocalizaCodigo(Pnotafiscal)=False)
     then Begin
               MensagemErro('Nota Fiscal n�o encontrada');
               exit;
     End;
     PObjNotaFiscal.TabelaparaObjeto;

     if (PObjNotaFiscal.nfe.Get_codigo='')
     then Begin
               mensagemerro('Esta NF n�o est� vinculada a nenhuma NFE');
               exit;
     End;

     if (Self.objgeranfe.Nfe.LocalizaCodigo(PObjNotaFiscal.NFe.Get_codigo)=False)
     Then Begin
               MensagemErro('Nfe c�digo '+PObjNotaFiscal.Nfe.Get_codigo+' n�o localizada');
               exit;
     End;
     Self.objgeranfe.Nfe.TabelaparaObjeto;

     if (Self.objgeranfe.Nfe.Get_STATUSNOTA<>'T')
     Then Begin
               Mensagemerro('Esta NF n�o est� em modo de conting�ncia');
               exit;
     End;

     if (Self.objgeranfe.Nfe.ExtraiArquivoNFe=False)
     Then exit;


     pnfe:=Self.objgeranfe.Nfe.GET_CODIGO;
     
     Self.objgeranfe.ConfiguraComponente;
     FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
     FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie:=ObjEmpresaGlobal.Get_certificado;
     FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(RetornaPastaSistema+'ARQUIVOB.XML');

     Try
        With FcomponentesNfe.ACBrNFe.WebServices do
        Begin
             if not(StatusServico.Executar)
             then raise Exception.Create(StatusServico.Msg);

             Enviar.Lote := '0';

             if not(Enviar.Executar)
             then raise Exception.Create(Enviar.Msg);
        End;
     Except
           on e:Exception do
           Begin
                mensagemerro('Erro na tentativa de enviar a NFE'+#13+E.message);
                exit;
           End;
     End;


     Try


          With FcomponentesNfe.ACBrNFe.WebServices do
          Begin
               Precibo:=Enviar.Recibo;
     
               Retorno.Recibo := Enviar.Recibo;
               if not(Retorno.Executar)
               then raise Exception.Create(Retorno.Msg);
          End;
     Except
           on e:exception do
           Begin
                if (FcomponentesNfe.ACBrNFe.WebServices.Retorno.cStat=105)
                Then Begin
                          //O Stat 105 � lote em processamento
                          MensagemErro('O Lote foi enviado mas continua em processamento, tente novamente mais tarde apenas confirmar esta NF');
     
                          //Marcando que esta Nfe esta em Processamento
                          Self.ObjgeraNfe.Nfe.status:=dsedit;
                          Self.ObjgeraNfe.Nfe.Submit_StatusNota('P');//Processando
                          Self.ObjgeraNfe.Nfe.Submit_reciboenvio(Precibo);
     
                          if (Self.ObjgeraNfe.Nfe.Salvar(true)=False)
                          Then Begin
                                   MensagemErro('N�o foi poss�vel alterar o status  para "PROCESSANDO"  e o Recibo na nota ID '+pnfe+' cancela a Nfe manualmente');
                                   exit;
                          End;
     
                          PObjNotaFiscal.Status:=dsedit;
                          PObjNotaFiscal.Submit_Situacao('P');
                          
                          if (PObjNotaFiscal.Salvar(true)=False)
                          then Begin
                                    MensagemErro('Erro na tentativa de Salvar o N� da Nfe '+pnfe+' na Nota Fiscal do Safira, cancela a Nfe '+PnotaFiscal+' manualmente');
                                    exit;
                          End;
                          //sai daqui
                          exit;
     
                End
                Else Begin
                          //o Erro foi outro n�o foi o Lote em Processamento (105)
                          MensagemErro(e.message);
                          exit;
                End;
           End;
     End;

     Try
       //Se chegou at� aqui � porque a Nfe foi enviada e confirmada
       With FComponentesNfe.ACBrNFe do
       Begin
            if DANFE <> nil
            then begin
                      if NotasFiscais.Items[0].Confirmada
                      then
                      begin
                          Danfe.ProtocoloNFe:=WebServices.Retorno.NFeRetorno.ProtNFe.Items[0].nProt;
                          NotasFiscais.Items[0].Imprimir;
                      end;
            end;
       end;
     Except
           MensagemErro('Erro na tentativa de Imprimir, tente imprimir direto do arquivo');
     End;


     Self.ObjgeraNfe.Nfe.status:=dsedit;
     Self.ObjgeraNfe.Nfe.Submit_StatusNota('G');//Gerada

     if (Self.ObjgeraNfe.Nfe.Salvar(True)=False)
     Then MensagemErro('N�o foi poss�vel alterar o status  para "GERADA" na nota ID '+pnfe+' Cancele a Nfe manualmente ou altere manualmente este status para GERADA');

     If (PObjNotaFiscal.LocalizaCodigo(Pnotafiscal)=False)
     Then Begin
                  Messagedlg('Nota Fiscal n�o Localizada, cancele a Nfe '+pnfe+' pelo arquivo ',mterror,[mbok],0);
                  exit;
     End;
     PObjNotaFiscal.TabelaparaObjeto;
     PObjNotaFiscal.Status:=dsedit;
     PObjNotaFiscal.Nfe.Submit_codigo(pnfe);
     PObjNotaFiscal.Submit_Situacao('I');

     if (PObjNotaFiscal.Salvar(true)=False)
     then Begin
                   MensagemErro('Erro na tentativa de Alterar o Status para Impresso na Nota Fiscal do Safira, fa�a isto manualmente. C�digo da Nota Fiscal '+pnotafiscal);
                   exit;
     End;

     MensagemAviso('Conclu�do');
end;



procedure TObjNotafiscalObjetos.SalvaXML(Pnotafiscal: string);
begin
     if (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(Pnotafiscal)=False)
     then Begin
               MensagemErro('Nota Fiscal n�o encontrada');
               exit;
     End;
     Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;

     if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Get_codigo='')
     then Begin
               mensagemerro('Esta NF n�o est� vinculada a nenhuma NFE');
               exit;
     End;

     if (Self.objgeranfe.Nfe.LocalizaCodigo(Self.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Get_codigo)=False)
     Then Begin
               MensagemErro('Nfe c�digo '+Self.ObjNotaFiscalCfop.NOTAFISCAL.NFe.Get_codigo+' n�o localizada');
               exit;
     End;
     Self.objgeranfe.Nfe.TabelaparaObjeto;

     if (FComponentesNfe.SaveDialog.Execute=False)
     Then exit;

     if (Self.objgeranfe.Nfe.ExtraiArquivoNFe(FComponentesNfe.SaveDialog.FileName)=False)
     Then exit;

end;

procedure TObjNotafiscalObjetos.CancelaNfe(Pnotafiscal: string);
begin
     if (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(Pnotafiscal)=False)
     then Begin
               MensagemErro('Nota Fiscal n�o encontrada');
               exit;
     End;
     Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;

     if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.get_codigo='')
     then Begin
               mensagemerro('Esta NF n�o est� vinculada a nenhuma NFE');
               exit;
     End;

     if (Self.objgeranfe.Nfe.LocalizaCodigo(Self.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.get_codigo)=False)
     Then Begin
               MensagemErro('Nfe c�digo '+Self.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.get_codigo+' n�o localizada');
               exit;
     End;
     Self.objgeranfe.Nfe.TabelaparaObjeto;

     if (Self.objgeranfe.Nfe.ExtraiArquivoNFe=False)
     Then exit;

     FmenuNFE.PassaObjeto(Self);
     FmenuNFE.CancelaNFe(RetornaPastaSistema+'ARQUIVOB.XML');
end;

procedure TObjNotafiscalObjetos.VerificaRetorno(Pnotafiscal: string);
var
Pnfe:String;
PGrava:boolean;
begin
     if (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(Pnotafiscal)=False)
     then Begin
               MensagemErro('Nota Fiscal n�o encontrada');
               exit;
     End;
     Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;

     if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.get_codigo='')
     then Begin
               mensagemerro('Esta NF n�o est� vinculada a nenhuma NFE');
               exit;
     End;

     if (Self.objgeranfe.Nfe.LocalizaCodigo(Self.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.get_codigo)=False)
     Then Begin
               MensagemErro('Nfe c�digo '+Self.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.get_codigo+' n�o localizada');
               exit;
     End;
     Self.objgeranfe.Nfe.TabelaparaObjeto;

     PGrava:=True;
     if (Self.objgeranfe.nfe.Get_STATUSNOTA<>'P')
     Then Begin
               IF (MensagemPergunta('Esta Nfe n�o se encontra com o Status de "P - Processando". Certeza que deseja consultar?')=mrno)
               Then exit;
               PGrava:=False;
     End;

     if (Self.objgeranfe.Nfe.ExtraiArquivoNFe=False)
     Then Begin
               Mensagemerro('N�o foi poss�vel extrair o arquivo XML do BD');
               exit;
     End;

     FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
     Self.objgeranfe.ConfiguraComponente;
     FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie:=ObjEmpresaGlobal.Get_certificado;
     FcomponentesNfe.ACBrNFe.NotasFiscais.Clear;
     FcomponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(RetornaPastaSistema+'ARQUIVOB.XML');


     Try
       With FcomponentesNfe.ACBrNFe.WebServices do
       Begin

             Retorno.Recibo := Self.objgeranfe.nfe.get_reciboenvio;

             if not(Retorno.Executar)
             then raise Exception.Create(Retorno.Msg);
             
       End;
     Except
           on e:exception do
           Begin

                if ((FcomponentesNfe.ACBrNFe.WebServices.Retorno.cStat=105)
                or (pos('LOTE EM PROCESSAMENTO',uppercase(e.message))>0))
                Then Begin
                          //O Stat 105 � lote em processamento
                          MensagemErro('O Lote continua em processamento, tente novamente mais tarde apenas confirmar esta NF');
                          exit;
                End
                Else Begin
                          //o Erro foi outro n�o foi o Lote em Processamento (105)
                          MensagemErro(e.message);

                          //nesta situacao precisarei tirar o status de processando da NF

                          Try
                              Self.ObjgeraNfe.Nfe.status:=dsedit;
                              Self.ObjgeraNfe.Nfe.Submit_StatusNota('A');//Volta a ficar aberta
                              Self.ObjgeraNfe.Nfe.Submit_Arquivo('');

                              if (Self.ObjgeraNfe.Nfe.Salvar(True)=False)
                              Then MensagemErro('N�o foi poss�vel alterar o status  para "GERADA" na nota ID '+pnfe+' Cancele a Nfe manualmente ou altere manualmente este status para GERADA');
               
               
                              Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
                              Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;
                              Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('N');//n�o usada
               
                              if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(true)=False)
                              then Begin
                                            MensagemErro('Erro na tentativa de Salvar a Situa��o para Impressa na Nota Fiscal do Safira, altere manualmente');
                                            exit;
                              End;

                          Finally
                                   FDataModulo.IBTransaction.RollbackRetaining;
                          End;



                          //****
                          exit;
                End;
           End;
     End;

     
     Try
       //Se chegou at� aqui � porque a Nfe foi enviada e confirmada
       With FComponentesNfe.ACBrNFe do
       Begin
            if DANFE <> nil
            then begin
                      if NotasFiscais.Items[0].Confirmada
                      then  begin
                              Danfe.ProtocoloNFe:=WebServices.Retorno.NFeRetorno.ProtNFe.Items[0].nProt;
                              NotasFiscais.Items[0].Imprimir;
                      end;
            end;
       end;
     Except
           MensagemErro('Erro na tentativa de Imprimir, tente imprimir direto do arquivo');
     End;

     if (pgrava)
     Then Begin
               FcomponentesNfe.MemoResp.text:= UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.Retorno.RetWS);
               FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);
               
               Try
                 Self.ObjgeraNfe.Nfe.status:=dsedit;
                 Self.ObjgeraNfe.Nfe.Submit_StatusNota('G');//Gerada
                 Self.ObjgeraNfe.Nfe.Submit_Arquivo(objgeranfe.LocalArquivos+StringReplace(FcomponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID,'Nfe','',[rfIgnoreCase])+'-nfe.xml');
               
               
                 if (Self.ObjgeraNfe.Nfe.Salvar(True)=False)
                 Then MensagemErro('N�o foi poss�vel alterar o status  para "GERADA" na nota ID '+pnfe+' Cancele a Nfe manualmente ou altere manualmente este status para GERADA');
               
               
                 Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
                 Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;
                 Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('I');
               
                 if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(true)=False)
                 then Begin
                               MensagemErro('Erro na tentativa de Salvar a Situa��o para Impressa na Nota Fiscal do Safira, altere manualmente');
                               exit;
                 End;
               
               Finally
                      FDataModulo.IBTransaction.RollbackRetaining;
               End;
     End;
end;

procedure TObjNotafiscalObjetos.EdtLocalizaProdutoReferenciaKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState;Ptipo,PnotaFiscal:String);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            if (PnotaFiscal='')
            then Begin
                      MensagemAviso('Escolha a NF');
                      exit;
            End;

            If (FpesquisaLocal.PreparaPesquisa('Select * from PROCRETORNAMATERIAIS_NF('+PnotaFiscal+') where upper(tipo)='''+UpperCase(Ptipo)+'''','Produtos da NF '+PnotaFiscal,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;
     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjNotafiscalObjetos.ImportaPedido(PnotaFiscal: string): boolean;
var
ObjPedidoObjetos:TObjPedidoObjetos;
ppedido:string;
begin

     if (PnotaFiscal='')
     then Begin
               mensagemaviso('Escolha a Nota Fiscal');
               exit;
     End;

     if (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(PnotaFiscal)=False)
     then Begin
               MensagemErro('Nota Fiscal n�o encontrada');
               exit;
     End;

     Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;

     try
        ObjPedidoObjetos:=TObjPedidoObjetos.Create(Self.Owner);
     Except
           on e:exception do
           begin
                Mensagemerro('Erro na tentativa OBJPEDIDOOBJETOS'+#13+E.message);
                exit; 
           End;
     End;

     Try
         With FfiltroImp do
         Begin
              DesativaGrupos;
              Grupo01.Enabled:=true;
              LbGrupo01.caption:='Pedido N�';
              edtgrupo01.EditMask:='';
              edtgrupo01.OnKeyDown:=ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtPedidoKeyDown;

              Grupo02.Enabled:=True;
              LbGrupo02.caption:='Opera��o';
              edtgrupo02.OnKeyDown:=Self.ObjNotaFiscalCfop.notafiscal.EdtoperacaoKeyDown;
              edtgrupo02.Text:=Self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.Get_CODIGO;

              Showmodal;

              if (tag=0)
              Then exit;

              if (edtgrupo01.Text='')
              then Begin
                        mensagemerro('Escolha o n� do pedido que deseja importar');
                        exit;
              End;

              if (edtgrupo02.Text='')
              then Begin
                        MensagemErro('Escolha uma opera��o para emiss�o da NF');
                        exit;
              End;


              if (ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(edtgrupo01.Text)=False)
              then Begin
                        MensagemErro('Pedido n�o localizado');
                        exit;
              End;
              ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
              ppedido:=ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Codigo;

              
              if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.LocalizaCodigo(edtgrupo02.Text)=False)
              then Begin
                        MensagemErro('Opera��o n�o localizada');
                        exit;
              End;
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.TabelaparaObjeto;

              Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_dataemissao(datetostr(now));
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_datasaida(datetostr(now));
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Horasaida(timetostr(now));
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Cliente.Submit_Codigo(ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Codigo);
              self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_NATUREZAOPERACAO(self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.Get_NOME);
              if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(True)=False)
              then Begin
                        Mensagemerro('Erro na tentativa de Salvar o Cliente na Nota Fiscal');
                        exit;
              End;

              if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.Get_CODIGO='')
              then Begin
                        mensagemerro('Escolha uma opera��o para a nota fiscal');
                        exit;
              End;


              With Self.ObjqueryTEMP do
              Begin

                   //***************FERRAGEM*******************************

                   close;
                   sql.clear;
                   sql.add('Select tabferragem_pp.* from tabferragem_pp');
                   sql.add('join tabpedido_proj on tabferragem_pp.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where tabpedido_proj.pedido='+Ppedido);
                   open;

                   while not(eof) do
                   Begin
                        With Self.ObjFerragem_nf do
                        Begin
                             ZerarTabela;

                             if (FerragemCor.LocalizaCodigo(fieldbyname('ferragemcor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado essa Ferragem nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             FerragemCor.TabelaparaObjeto;

                             notafiscal.LocalizaCodigo(PnotaFiscal);
                             notafiscal.TabelaparaObjeto;

                             status:=dsInactive;
                             Status:=dsinsert;
                             Submit_Codigo('0');

                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);

                             //Procurando o Imposto do Estado de origem
                             if (Self.ObjFerragem_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Ferragemcor.Ferragem.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                             Then Begin
                                       MensagemErro('N�o foi encontrado o imposto de Origem para esta Ferragem, tipo de cliente e opera��o'+#13+
                                                    'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Ferragemcor.Ferragem.Get_Codigo);
                                       exit;
                             End;
                             Self.ObjFerragem_ICMS.TabelaparaObjeto;

                             IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Self.ObjFerragem_ICMS.IMPOSTO.Get_CODIGO);
                             IMPOSTO_IPI.Submit_codigo(Self.ObjFerragem_ICMS.imposto_ipi.Get_CODIGO);
                             IMPOSTO_PIS_ORIGEM.Submit_codigo(Self.ObjFerragem_ICMS.imposto_pis.Get_CODIGO);
                             IMPOSTO_COFINS_ORIGEM.Submit_codigo(Self.ObjFerragem_ICMS.imposto_cofins.Get_CODIGO);
                             CFOP.Submit_CODIGO(Self.ObjFerragem_ICMS.IMPOSTO.CFOP.Get_CODIGO);

                             //Procurando o estado de destino
                             if (Self.ObjFerragem_ICMS.Localiza(NOTAFISCAL.Cliente.Get_Estado,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Ferragemcor.Ferragem.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                             Then Begin
                                       MensagemErro('N�o foi encontrado o imposto de Destino para este Ferragem, tipo de cliente e opera��o'+#13+
                                                    'Estado: '+NOTAFISCAL.Cliente.Get_Estado+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Ferragemcor.Ferragem.Get_Codigo);
                                       exit;
                             End;
                             Self.ObjFerragem_ICMS.TabelaparaObjeto;
                             IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Self.ObjFerragem_ICMS.IMPOSTO.Get_CODIGO);
                             IMPOSTO_PIS_DESTINO.Submit_codigo(Self.ObjFerragem_ICMS.imposto_pis.Get_CODIGO);
                             IMPOSTO_COFINS_DESTINO.Submit_codigo(Self.ObjFerragem_ICMS.imposto_cofins.Get_CODIGO);

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');
                             

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');


                             //******nao usa mais******
                             Submit_valorpauta('0');
                             Submit_ISENTO('N');
                             Submit_SUBSTITUICAOTRIBUTARIA('N');
                             Submit_ALIQUOTA('0');
                             Submit_REDUCAOBASECALCULO('0');
                             Submit_ALIQUOTACUPOM('0');
                             Submit_IPI('0');
                             Submit_percentualagregado('0');
                             Submit_Margemvaloragregadoconsumidor('0');
                             SituacaoTributaria_TabelaA.Submit_CODIGO('');
                             SituacaoTributaria_TabelaB.Submit_CODIGO('');
                             //*********************************


                             Submit_valorfrete('0');
                             Submit_valorseguro('0');

                             Submit_referencia('');

                             if (Salvar(False)=False)
                             Then Begin
                                       messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                                       exit;
                             End;
                        End;

                        next;//pr�xima ferragem do pedido
                   End;//While



                   //*********************DIVERSO******************

                   close;
                   sql.clear;
                   sql.add('Select tabdiverso_pp.* from tabdiverso_pp');
                   sql.add('join tabpedido_proj on tabdiverso_pp.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where Tabpedido_proj.pedido='+Ppedido);
                   open;

                   while not(eof) do
                   Begin
                        With Self.ObjDiverso_nf do
                        Begin
                             ZerarTabela;
                             if (DiversoCor.LocalizaCodigo(fieldbyname('diversocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Diverso nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             DiversoCor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnotafiscal)=False)
                             Then Begin
                                       mensagemerro('Nota Fiscal n�o encontrada');
                                       exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Status:=dsinsert;
                             Submit_Codigo('0');
                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);
                             //Procurando o Imposto do Estado de origem
                             if (Self.ObjDiverso_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Diversocor.Diverso.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                             Then Begin
                                       MensagemErro('N�o foi encontrado o imposto de Origem para este Diverso, tipo de cliente e opera��o'+#13+
                                                    'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Diversocor.Diverso.Get_Codigo);
                                       exit;
                             End;
                             Self.ObjDiverso_ICMS.TabelaparaObjeto;

                             IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Self.ObjDiverso_ICMS.IMPOSTO.Get_CODIGO);
                             IMPOSTO_IPI.Submit_codigo(Self.ObjDiverso_ICMS.imposto_ipi.Get_CODIGO);
                             IMPOSTO_PIS_ORIGEM.Submit_codigo(Self.ObjDiverso_ICMS.imposto_pis.Get_CODIGO);
                             IMPOSTO_COFINS_ORIGEM.Submit_codigo(Self.ObjDiverso_ICMS.imposto_cofins.Get_CODIGO);
                             CFOP.Submit_CODIGO(Self.ObjDiverso_ICMS.IMPOSTO.CFOP.Get_CODIGO);

                             //Procurando o estado de destino
                             if (Self.ObjDiverso_ICMS.Localiza(NOTAFISCAL.Cliente.Get_Estado,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Diversocor.Diverso.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                             Then Begin
                                       MensagemErro('N�o foi encontrado o imposto de Destino para este Diverso, tipo de cliente e opera��o'+#13+
                                                    'Estado: '+NOTAFISCAL.Cliente.Get_Estado+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Diversocor.Diverso.Get_Codigo);
                                       exit;
                             End;
                             Self.ObjDiverso_ICMS.TabelaparaObjeto;
                             IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Self.ObjDiverso_ICMS.IMPOSTO.Get_CODIGO);
                             IMPOSTO_PIS_DESTINO.Submit_codigo(Self.ObjDiverso_ICMS.imposto_pis.Get_CODIGO);
                             IMPOSTO_COFINS_DESTINO.Submit_codigo(Self.ObjDiverso_ICMS.imposto_cofins.Get_CODIGO);

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');
                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');

                           
                             //******nao usa mais******
                             Submit_valorpauta('0');
                             Submit_ISENTO('N');
                             Submit_SUBSTITUICAOTRIBUTARIA('N');
                             Submit_ALIQUOTA('0');
                             Submit_REDUCAOBASECALCULO('0');
                             Submit_ALIQUOTACUPOM('0');
                             Submit_IPI('0');
                             Submit_percentualagregado('0');
                             Submit_Margemvaloragregadoconsumidor('0');
                             SituacaoTributaria_TabelaA.Submit_CODIGO('');
                             SituacaoTributaria_TabelaB.Submit_CODIGO('');
                             //*********************************


                             Submit_valorfrete('0');
                             Submit_valorseguro('0');

                             Submit_referencia('');

                             if (Salvar(False)=False)
                             Then Begin
                                       messagedlg('N�o foi poss�vel Gravar o Diverso',mterror,[mbok],0);
                                       exit;
                             End;

                        End;

                      next;//proximo divero
                   End;//while

                   //*********************KITBOX******************
                   close;
                   sql.clear;
                   sql.add('Select TABKITBOX_PP.* from TABKITBOX_PP');
                   sql.add('join tabpedido_proj on TABKITBOX_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where Tabpedido_proj.pedido='+Ppedido);
                   open;

                   while not(eof) do
                   Begin
                        With Self.ObjKitbox_nf do
                        Begin
                             ZerarTabela;
                             if (KitboxCor.LocalizaCodigo(fieldbyname('kitboxcor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Kitbox nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             KitboxCor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnotafiscal)=False)
                             Then Begin
                                       mensagemerro('Nota Fiscal n�o encontrada');
                                       exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Status:=dsinsert;
                             Submit_Codigo('0');

                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);

                             //Procurando o Imposto do Estado de origem
                             if (Self.ObjKitbox_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Kitboxcor.Kitbox.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                             Then Begin
                                       MensagemErro('N�o foi encontrado o imposto de Origem para este Kitbox, tipo de cliente e opera��o'+#13+
                                                    'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Kitboxcor.Kitbox.Get_Codigo);
                                       exit;
                             End;
                             Self.ObjKitbox_ICMS.TabelaparaObjeto;
                           
                             IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Self.ObjKitbox_ICMS.IMPOSTO.Get_CODIGO);
                             IMPOSTO_IPI.Submit_codigo(Self.ObjKitbox_ICMS.imposto_ipi.Get_CODIGO);
                             IMPOSTO_PIS_ORIGEM.Submit_codigo(Self.ObjKitbox_ICMS.imposto_pis.Get_CODIGO);
                             IMPOSTO_COFINS_ORIGEM.Submit_codigo(Self.ObjKitbox_ICMS.imposto_cofins.Get_CODIGO);
                             CFOP.Submit_CODIGO(Self.ObjKitbox_ICMS.IMPOSTO.CFOP.Get_CODIGO);
                           
                             //Procurando o estado de destino
                             if (Self.ObjKitbox_ICMS.Localiza(NOTAFISCAL.Cliente.Get_Estado,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Kitboxcor.Kitbox.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                             Then Begin
                                       MensagemErro('N�o foi encontrado o imposto de Destino para este Kitbox, tipo de cliente e opera��o'+#13+
                                                    'Estado: '+NOTAFISCAL.Cliente.Get_Estado+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Kitboxcor.Kitbox.Get_Codigo);
                                       exit;
                             End;
                             Self.ObjKitbox_ICMS.TabelaparaObjeto;
                             IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Self.ObjKitbox_ICMS.IMPOSTO.Get_CODIGO);
                             IMPOSTO_PIS_DESTINO.Submit_codigo(Self.ObjKitbox_ICMS.imposto_pis.Get_CODIGO);
                             IMPOSTO_COFINS_DESTINO.Submit_codigo(Self.ObjKitbox_ICMS.imposto_cofins.Get_CODIGO);
                           
                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');
                           
                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');

                           
                             //******nao usa mais******
                             Submit_valorpauta('0');
                             Submit_ISENTO('N');
                             Submit_SUBSTITUICAOTRIBUTARIA('N');
                             Submit_ALIQUOTA('0');
                             Submit_REDUCAOBASECALCULO('0');
                             Submit_ALIQUOTACUPOM('0');
                             Submit_IPI('0');
                             Submit_percentualagregado('0');
                             Submit_Margemvaloragregadoconsumidor('0');
                             SituacaoTributaria_TabelaA.Submit_CODIGO('');
                             SituacaoTributaria_TabelaB.Submit_CODIGO('');
                             //*********************************
                             Submit_valorfrete('0');
                             Submit_valorseguro('0');

                             Submit_referencia('');

                             if (Salvar(False)=False)
                             Then Begin
                                       messagedlg('N�o foi poss�vel Gravar o kitbox',mterror,[mbok],0);
                                       exit;
                             End;
                        End;
                        
                        next;//pr�ximo kitbox do pedido
                   End;//while


                   //*********************PERFILADO******************
                   close;
                   sql.clear;
                   sql.add('Select TABperfilado_PP.* from TABperfilado_PP');
                   sql.add('join tabpedido_proj on TABperfilado_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where Tabpedido_proj.pedido='+Ppedido);
                   open;

                   while not(eof) do
                   Begin
                        With Self.ObjPerfilado_nf do
                        Begin
                             ZerarTabela;
                             if (PerfiladoCor.LocalizaCodigo(fieldbyname('perfiladocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Perfilado nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             PerfiladoCor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnotafiscal)=False)
                             Then Begin
                                       mensagemerro('Nota Fiscal n�o encontrada');
                                       exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Status:=dsinsert;
                             Submit_Codigo('0');

                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);
                             
                             //Procurando o Imposto do Estado de origem
                             if (Self.ObjPerfilado_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Perfiladocor.Perfilado.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                             Then Begin
                                       MensagemErro('N�o foi encontrado o imposto de Origem para este Perfilado, tipo de cliente e opera��o'+#13+
                                                    'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Perfiladocor.Perfilado.Get_Codigo);
                                       exit;
                             End;
                             Self.ObjPerfilado_ICMS.TabelaparaObjeto;

                             IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Self.ObjPerfilado_ICMS.IMPOSTO.Get_CODIGO);
                             IMPOSTO_IPI.Submit_codigo(Self.ObjPerfilado_ICMS.imposto_ipi.Get_CODIGO);
                             IMPOSTO_PIS_ORIGEM.Submit_codigo(Self.ObjPerfilado_ICMS.imposto_pis.Get_CODIGO);
                             IMPOSTO_COFINS_ORIGEM.Submit_codigo(Self.ObjPerfilado_ICMS.imposto_cofins.Get_CODIGO);
                             CFOP.Submit_CODIGO(Self.ObjPerfilado_ICMS.IMPOSTO.CFOP.Get_CODIGO);

                             //Procurando o estado de destino
                             if (Self.ObjPerfilado_ICMS.Localiza(NOTAFISCAL.Cliente.Get_Estado,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Perfiladocor.Perfilado.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                             Then Begin
                                       MensagemErro('N�o foi encontrado o imposto de Destino para este Perfilado, tipo de cliente e opera��o'+#13+
                                                    'Estado: '+NOTAFISCAL.Cliente.Get_Estado+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Perfiladocor.Perfilado.Get_Codigo);
                                       exit;
                             End;
                             Self.ObjPerfilado_ICMS.TabelaparaObjeto;
                             IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Self.ObjPerfilado_ICMS.IMPOSTO.Get_CODIGO);
                             IMPOSTO_PIS_DESTINO.Submit_codigo(Self.ObjPerfilado_ICMS.imposto_pis.Get_CODIGO);
                             IMPOSTO_COFINS_DESTINO.Submit_codigo(Self.ObjPerfilado_ICMS.imposto_cofins.Get_CODIGO);

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');
                           
                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');
                           
                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');
                           
                           
                             //******nao usa mais******
                             Submit_valorpauta('0');
                             Submit_ISENTO('N');
                             Submit_SUBSTITUICAOTRIBUTARIA('N');
                             Submit_ALIQUOTA('0');
                             Submit_REDUCAOBASECALCULO('0');
                             Submit_ALIQUOTACUPOM('0');
                             Submit_IPI('0');
                             Submit_percentualagregado('0');
                             Submit_Margemvaloragregadoconsumidor('0');
                             SituacaoTributaria_TabelaA.Submit_CODIGO('');
                             SituacaoTributaria_TabelaB.Submit_CODIGO('');
                             //*********************************

                             Submit_valorfrete('0');
                             Submit_valorseguro('0');

                             Submit_referencia('');
                             

                             if (Salvar(False)=False)
                             Then Begin
                                       messagedlg('N�o foi poss�vel Gravar o Perfilado',mterror,[mbok],0);
                                       exit;
                             End;
                        End;

                        next;//pr�ximo perfilado do pedido
                   End;//while

                   
                   //***************PERSIANA*******************************
                   close;
                   sql.clear;
                   sql.add('Select TABPERSIANA_PP.* from TABPERSIANA_PP');
                   sql.add('join tabpedido_proj on TABPERSIANA_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where tabpedido_proj.pedido='+Ppedido);
                   open;

                   while not(eof) do
                   Begin
                        With Self.ObjPersiana_nf do
                        Begin
                             ZerarTabela;
                             if (persianagrupodiametrocor.LocalizaCodigo(fieldbyname('persianagrupodiametrocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Persiana nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             persianagrupodiametrocor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnotafiscal)=False)
                             Then Begin
                                       mensagemerro('Nota Fiscal n�o encontrada');
                                       exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Status:=dsinsert;
                             Submit_Codigo('0');
                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);
                             //Procurando o Imposto do Estado de origem
                             if (Self.ObjPersiana_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,persianagrupodiametrocor.Persiana.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                             Then Begin
                                       MensagemErro('N�o foi encontrado o imposto de Origem para este Persiana, tipo de cliente e opera��o'+#13+
                                                    'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+persianagrupodiametrocor.Persiana.Get_Codigo);
                                       exit;
                             End;
                             Self.ObjPersiana_ICMS.TabelaparaObjeto;

                             IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Self.ObjPersiana_ICMS.IMPOSTO.Get_CODIGO);
                             IMPOSTO_IPI.Submit_codigo(Self.ObjPersiana_ICMS.imposto_ipi.Get_CODIGO);
                             IMPOSTO_PIS_ORIGEM.Submit_codigo(Self.ObjPersiana_ICMS.imposto_pis.Get_CODIGO);
                             IMPOSTO_COFINS_ORIGEM.Submit_codigo(Self.ObjPersiana_ICMS.imposto_cofins.Get_CODIGO);
                             CFOP.Submit_CODIGO(Self.ObjPersiana_ICMS.IMPOSTO.CFOP.Get_CODIGO);

                             //Procurando o estado de destino
                             if (Self.ObjPersiana_ICMS.Localiza(NOTAFISCAL.Cliente.Get_Estado,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,persianagrupodiametrocor.Persiana.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                             Then Begin
                                       MensagemErro('N�o foi encontrado o imposto de Destino para este Persiana, tipo de cliente e opera��o'+#13+
                                                    'Estado: '+NOTAFISCAL.Cliente.Get_Estado+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+persianagrupodiametrocor.Persiana.Get_Codigo);
                                       exit;
                             End;
                             Self.ObjPersiana_ICMS.TabelaparaObjeto;
                             IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Self.ObjPersiana_ICMS.IMPOSTO.Get_CODIGO);
                             IMPOSTO_PIS_DESTINO.Submit_codigo(Self.ObjPersiana_ICMS.imposto_pis.Get_CODIGO);
                             IMPOSTO_COFINS_DESTINO.Submit_codigo(Self.ObjPersiana_ICMS.imposto_cofins.Get_CODIGO);

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');
                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');


                             //******nao usa mais******
                             Submit_valorpauta('0');
                             Submit_ISENTO('N');
                             Submit_SUBSTITUICAOTRIBUTARIA('N');
                             Submit_ALIQUOTA('0');
                             Submit_REDUCAOBASECALCULO('0');
                             Submit_ALIQUOTACUPOM('0');
                             Submit_IPI('0');
                             Submit_percentualagregado('0');
                             Submit_Margemvaloragregadoconsumidor('0');
                             SituacaoTributaria_TabelaA.Submit_CODIGO('');
                             SituacaoTributaria_TabelaB.Submit_CODIGO('');
                             //*********************************


                             Submit_valorfrete('0');
                             Submit_valorseguro('0');

                             Submit_referencia('');

                             if (Salvar(False)=False)
                             Then Begin
                                       messagedlg('N�o foi poss�vel Gravar a Persiana',mterror,[mbok],0);
                                       exit;
                             End;
                        End;

                        next;//pr�xima persiana
                   End;

                   //***************VIDRO*******************************
                   close;
                   sql.clear;
                   sql.add('Select TABVIDRO_PP.* from TABVIDRO_PP');
                   sql.add('join tabpedido_proj on TABVIDRO_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where tabpedido_proj.pedido='+Ppedido);
                   open;

                   while not(eof) do
                   Begin
                        With Self.ObjVidro_nf do
                        Begin
                             ZerarTabela;
                             if (VidroCor.LocalizaCodigo(fieldbyname('vidrocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Vidro nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             VidroCor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnotafiscal)=False)
                             Then Begin
                                       mensagemerro('Nota Fiscal n�o encontrada');
                                       exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Status:=dsinsert;
                             Submit_Codigo('0');

                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);
                             //Procurando o Imposto do Estado de origem
                             if (Self.ObjVidro_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Vidrocor.Vidro.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                             Then Begin
                                       MensagemErro('N�o foi encontrado o imposto de Origem para este Vidro, tipo de cliente e opera��o'+#13+
                                                    'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Vidrocor.Vidro.Get_Codigo);
                                       exit;
                             End;
                             Self.ObjVidro_ICMS.TabelaparaObjeto;

                             IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Self.ObjVidro_ICMS.IMPOSTO.Get_CODIGO);
                             IMPOSTO_IPI.Submit_codigo(Self.ObjVidro_ICMS.imposto_ipi.Get_CODIGO);
                             IMPOSTO_PIS_ORIGEM.Submit_codigo(Self.ObjVidro_ICMS.imposto_pis.Get_CODIGO);
                             IMPOSTO_COFINS_ORIGEM.Submit_codigo(Self.ObjVidro_ICMS.imposto_cofins.Get_CODIGO);
                             CFOP.Submit_CODIGO(Self.ObjVidro_ICMS.IMPOSTO.CFOP.Get_CODIGO);

                             //Procurando o estado de destino
                             if (Self.ObjVidro_ICMS.Localiza(NOTAFISCAL.Cliente.Get_Estado,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Vidrocor.Vidro.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                             Then Begin
                                       MensagemErro('N�o foi encontrado o imposto de Destino para este Vidro, tipo de cliente e opera��o'+#13+
                                                    'Estado: '+NOTAFISCAL.Cliente.Get_Estado+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Vidrocor.Vidro.Get_Codigo);
                                       exit;
                             End;
                             Self.ObjVidro_ICMS.TabelaparaObjeto;
                             IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Self.ObjVidro_ICMS.IMPOSTO.Get_CODIGO);
                             IMPOSTO_PIS_DESTINO.Submit_codigo(Self.ObjVidro_ICMS.imposto_pis.Get_CODIGO);
                             IMPOSTO_COFINS_DESTINO.Submit_codigo(Self.ObjVidro_ICMS.imposto_cofins.Get_CODIGO);

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');
                           
                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');
                           
                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');
                           
                           
                             //******nao usa mais******
                             Submit_valorpauta('0');
                             Submit_ISENTO('N');
                             Submit_SUBSTITUICAOTRIBUTARIA('N');
                             Submit_ALIQUOTA('0');
                             Submit_REDUCAOBASECALCULO('0');
                             Submit_ALIQUOTACUPOM('0');
                             Submit_IPI('0');
                             Submit_percentualagregado('0');
                             Submit_Margemvaloragregadoconsumidor('0');
                             SituacaoTributaria_TabelaA.Submit_CODIGO('');
                             SituacaoTributaria_TabelaB.Submit_CODIGO('');

                             Submit_valorfrete('0');
                             Submit_valorseguro('0');
                           
                             Submit_referencia('');

                             if (Salvar(False)=False)
                             Then Begin
                                       messagedlg('N�o foi poss�vel Gravar o Vidro',mterror,[mbok],0);
                                       exit;
                             End;
                        End;


                       next;//pr�ximo vidro
                   End;

              End;//With Self.ObjqueryTemp

              //Concluindo a Importa��o, gravando o desconto
              Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(PnotaFiscal);
              Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_DESCONTO(ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorDesconto);

              if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(True)=False)
              then Begin
                        Mensagemerro('Erro na tentativa de Salvar o desconto na Nota Fiscal');
                        exit;
              End;

              mensagemaviso('Importa��o Conclu�da com Sucesso');

         End;
     Finally
            FDataModulo.IBTransaction.RollbackRetaining;
            ObjPedidoObjetos.Free;
     End;




end;

procedure TObjNotafiscalObjetos.ExportaCucaFresca;
var
Parquivo:TStringList;
PdataInicial,PdataFinal:TDate;
begin
     MensagemAviso('Em Implementa��o');
     exit;

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          LbGrupo01.caption:='Data inicial';
          LbGrupo02.caption:='Data Final';
          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;
          
          Showmodal;

          if (tag=0)
          Then exit;

          if (Validadata(1,pdatainicial,false)=False)
          Then exit;

          if (Validadata(2,pdatafinal,false)=False)
          Then exit;

     End;



     Try
        PArquivo:=TStringList.Create;
     Except
           MensagemErro('Erro na tentativa de criar a StringList "PArquivo"');
           exit;
     End;

     Try


     


     Finally
            Freeandnil(Parquivo);
     End;

end;

//comentarios adicionados por jonatan medina 29/03/2011
//Pnotafiscal = Codigo da nota fiscal a ser emitida
//Pedido = Codigo do pedido quer que a nota fiscal seja gerada
//valorpedido =  valor final do pedido
procedure TObjNotafiscalObjetos.GeraNF(PnotaFiscal: string;Pedido:string;Operacao:string;valorpedido:string);
var
ObjPedidoObjetos:TObjPedidoObjetos;
ppedido:string;
Query:TIBQuery;
begin

     if (PnotaFiscal='')
     then Begin
               mensagemaviso('Escolha a Nota Fiscal');
               exit;
     End;


     //Localizinado uma nota fiscal para ser emitida
     if (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(PnotaFiscal)=False)
     then Begin
               MensagemErro('Nota Fiscal n�o encontrada');
               exit;
     End;

     //carregando as informa��es que ja est�o na nota
     Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;

     try
        ObjPedidoObjetos:=TObjPedidoObjetos.Create(Self.Owner);
     Except
           on e:exception do
           begin
                Mensagemerro('Erro na tentativa OBJPEDIDOOBJETOS'+#13+E.message);
                exit;
           End;
     End;

     try
        Query:=TIBQuery.Create(nil);
        Query.Database:=FDataModulo.IBDatabase;
     except

     end;


     Try
              //Procurando o pedido
              if (ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(pedido)=False)
              then Begin
                        MensagemErro('Pedido n�o localizado');
                        exit;
              End;
              //ao encontrar o pedido, carrega as informa��es do mesmo para o objeto
              ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
              //ppedido recebe o codigo do pedido
              //verificar, pois se ja esta passando o pedido, n�o tem pq buscar novamente o codigo,
              //ou existir uma outra variavel pra guardar o pedido...
              ppedido:=ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Codigo;

              //procura a opera��o na tabela de opera��es (venda/compra/devolu��o)
              //lembrando que os cfop s�o passados automaticamente por parametro
              //por exemplo, opera��o de venda = cfop tal( valor que esta no parametro)
              if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.LocalizaCodigo(Operacao)=False)
              then Begin
                        MensagemErro('Opera��o n�o localizada');
                        exit;
              End;
              //carregando dados da tabopera��o para o objeto
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.TabelaparaObjeto;

              //passando as informa��es para objnotafiscal para que a nota seja montanda
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_dataemissao(datetostr(now));
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_datasaida(datetostr(now));
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Horasaida(timetostr(now));
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Cliente.Submit_Codigo(ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Codigo);
              self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_NATUREZAOPERACAO(self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.Get_NOME);
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORTOTAL(valorpedido);
              self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_NumPedido(ppedido);
              self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('I');


              //salvando dados na tabnotafiscal
              if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(True)=False)
              then Begin
                        Mensagemerro('Erro na tentativa de Salvar o Cliente na Nota Fiscal');
                        exit;
              End;

              if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.Get_CODIGO='')
              then Begin
                        mensagemerro('Escolha uma opera��o para a nota fiscal');
                        exit;
              End;

              //aqui procura todos os materias contidos no pedido, ou seja dos projetos que tem no pedido em quest�o
              //busca quais s�o os materias que comp�e o mesmo pra poder calcular os impostos
              With Self.ObjqueryTEMP do
              Begin

                   //***************FERRAGEM*******************************

                   //procura na tabferragem pedido projeto junto com a tabpedido_projeto
                   //PEDIDOPROJETO,FERRAGEMCOR,QUANTIDADE,VALOR,VALORFINAL(valor*quantidade)

                   close;
                   sql.clear;
                   sql.add('Select tabferragem_pp.* from tabferragem_pp');
                   sql.add('join tabpedido_proj on tabferragem_pp.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where tabpedido_proj.pedido='+Ppedido);
                   open;


                   while not(eof) do
                   Begin
                                With Self.ObjFerragem_nf do
                                Begin
                                     ZerarTabela;

                                     //Procura pela cor da ferragem
                                     if (FerragemCor.LocalizaCodigo(fieldbyname('ferragemcor').asstring)=False)
                                     Then Begin
                                             Messagedlg('N�o foi Encontrado essa Ferragem!',mterror,[mbok],0);
                                             Self.LimpaNF(PnotaFiscal);
                                             exit;
                                     End;

                                     //carrega para o objetos os dados da tabela
                                     //CODIGO,FERRAGEM,COR,PORCENTAGEMACRESCIMO,ESTOQUE,ACRESCIMOEXTRA,PORCENTAGEMACRESCIMOFINAL,
                                     //VALOREXTRA,VALORFINAL,CLASSIFICACAOFISCAL
                                     FerragemCor.TabelaparaObjeto;

                                     //localiza novamente a nota fiscal
                                     notafiscal.LocalizaCodigo(PnotaFiscal);
                                     //transefera dados da tabela para objeto
                                     notafiscal.TabelaparaObjeto;

                                     status:=dsInactive;
                                     Status:=dsinsert;
                                     Submit_Codigo('0');

                                     //Passando a quantidade de ferragem que tem no pedido
                                     Submit_Quantidade(fieldbyname('quantidade').asstring);
                                     //Passando o valor unitario de cada pedido
                                     Submit_Valor(fieldbyname('valor').asstring);

                                     //***ALTERA��O FEITA POR JONATAN MEDINA
                                     //ANTES BUSCAVA OS IMPOSTOS NA HORA DE FOSSE GERAR UMA NOTA FISCAL, O QUE � ERRADO
                                     //O CERTO � GERAR UMA VENDA, E GUARDAR OS IMPOSTOS DOS MATERIAIS, POIS ASSIM N�O TEM O PROBLEMA
                                     //DO USUARIO APOS GERAR UMA VENDA, IR LA NO CADASTRO DO MATERIAL E MUDAR AS INFORMA��ES E A
                                     //NOTA SAIR COM AS NOVAS INFORMA��ES E N�O COM AS INFORMA��ES CORRETAS, Q TEMOS AO GERAR A VENDA
                                     //AGORA AO GERAR UMA VENDA, TODOS OS IMPOSTOS DO MATERIAL/MATERIAIS, S�O GUARDADOS NA TABMATERIAISVENDA
                                     //ASSIM NA HORA DE BUSCAR OS IMPOSTOS, SO BUSCA NESTA TABELA

                                     //Seleciono os dados que est�o na tabmateriaisvenda usando a viewprodutosvenda
                                     //nela eu busco pelo codigo do produto (codigo da ferragem, neste caso)
                                     //e pelo codigo do material (como eu estou procurando por ferragens, aqui o codigo � 1)
                                     Query.Close;
                                     Query.SQL.Clear;
                                     Query.SQL.Add('select * from viewprodutosvenda');
                                     Query.SQL.Add('where codigoprodutoPK ='+Ferragemcor.Ferragem.Get_Codigo);
                                     Query.SQL.Add('and material = 1');
                                     Query.SQL.Add('and pedido='+ppedido);
                                     Query.Open;

                                     IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                     IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                     IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                     IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                     CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                     IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                                     IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                     IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                                     Submit_BC_IPI('0');
                                     Submit_VALOR_IPI('0');

                                     Submit_BC_PIS('0');
                                     Submit_VALOR_PIS('0');
                                     Submit_BC_PIS_ST('0');
                                     Submit_VALOR_PIS_ST('0');

                                     Submit_BC_COFINS('0');
                                     Submit_VALOR_COFINS('0');
                                     Submit_BC_COFINS_ST('0');
                                     Submit_VALOR_COFINS_ST('0');


                                     Submit_BC_IPI('0');
                                     Submit_VALOR_IPI('0');

                                     Submit_BC_PIS('0');
                                     Submit_VALOR_PIS('0');
                                     Submit_BC_PIS_ST('0');
                                     Submit_VALOR_PIS_ST('0');

                                     Submit_BC_COFINS('0');
                                     Submit_VALOR_COFINS('0');
                                     Submit_BC_COFINS_ST('0');
                                     Submit_VALOR_COFINS_ST('0');


                                     //******nao usa mais******
                                     Submit_valorpauta('0');
                                     Submit_ISENTO('N');
                                     Submit_SUBSTITUICAOTRIBUTARIA('N');
                                     Submit_ALIQUOTA('0');
                                     Submit_REDUCAOBASECALCULO('0');
                                     Submit_ALIQUOTACUPOM('0');
                                     Submit_IPI('0');
                                     Submit_percentualagregado('0');
                                     Submit_Margemvaloragregadoconsumidor('0');
                                     SituacaoTributaria_TabelaA.Submit_CODIGO('');
                                     SituacaoTributaria_TabelaB.Submit_CODIGO('');
                                     //*********************************


                                     Submit_valorfrete('0');
                                     Submit_valorseguro('0');

                                     Submit_referencia('');

                                     if (Salvar(False)=False)
                                     Then Begin
                                               messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                                               Self.LimpaNF(PnotaFiscal);
                                               exit;
                                     End;
                                End;
                         //end;

                        next;//pr�xima ferragem do pedido
                   End;//While



                   //*********************DIVERSO******************

                   close;
                   sql.clear;
                   sql.add('Select tabdiverso_pp.* from tabdiverso_pp');
                   sql.add('join tabpedido_proj on tabdiverso_pp.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where Tabpedido_proj.pedido='+Ppedido);
                   open;

                   while not(eof) do
                   Begin
                        With Self.ObjDiverso_nf do
                        Begin
                             ZerarTabela;
                             if (DiversoCor.LocalizaCodigo(fieldbyname('diversocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Diverso nessa cor!',mterror,[mbok],0);
                                      Self.LimpaNF(PnotaFiscal);
                                     exit;
                             End;
                             DiversoCor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnotafiscal)=False)
                             Then Begin
                                       mensagemerro('Nota Fiscal n�o encontrada');
                                       exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Status:=dsinsert;
                             Submit_Codigo('0');
                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);


                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+Diversocor.Diverso.Get_Codigo);
                             Query.SQL.Add('and material = 6');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.Open;

                             IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                             IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                             IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                             IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                             CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                             IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                             IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                             IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');


                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');


                             //******nao usa mais******
                             Submit_valorpauta('0');
                             Submit_ISENTO('N');
                             Submit_SUBSTITUICAOTRIBUTARIA('N');
                             Submit_ALIQUOTA('0');
                             Submit_REDUCAOBASECALCULO('0');
                             Submit_ALIQUOTACUPOM('0');
                             Submit_IPI('0');
                             Submit_percentualagregado('0');
                             Submit_Margemvaloragregadoconsumidor('0');
                             SituacaoTributaria_TabelaA.Submit_CODIGO('');
                             SituacaoTributaria_TabelaB.Submit_CODIGO('');
                             //*********************************


                             Submit_valorfrete('0');
                             Submit_valorseguro('0');
                             Submit_referencia('');

                             if (Salvar(False)=False)
                             Then Begin
                                       messagedlg('N�o foi poss�vel Gravar o Diverso',mterror,[mbok],0);
                                        Self.LimpaNF(PnotaFiscal);
                                       exit;
                             End;

                        End;

                      next;//proximo divero
                   End;//while

                   //*********************KITBOX******************
                   close;
                   sql.clear;
                   sql.add('Select TABKITBOX_PP.* from TABKITBOX_PP');
                   sql.add('join tabpedido_proj on TABKITBOX_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where Tabpedido_proj.pedido='+Ppedido);
                   open;

                   while not(eof) do
                   Begin
                        With Self.ObjKitbox_nf do
                        Begin
                             ZerarTabela;
                             if (KitboxCor.LocalizaCodigo(fieldbyname('kitboxcor').asstring)=False)
                             Then
                             Begin
                                     Messagedlg('N�o foi Encontrado esse Kitbox nessa cor!',mterror,[mbok],0);
                                      Self.LimpaNF(PnotaFiscal);
                                     exit;
                             End;
                             KitboxCor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnotafiscal)=False)
                             Then
                             Begin
                                       mensagemerro('Nota Fiscal n�o encontrada');
                                       exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Status:=dsinsert;
                             Submit_Codigo('0');

                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+Kitboxcor.Kitbox.Get_Codigo);
                             Query.SQL.Add('and material = 4');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.Open;

                             IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                             IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                             IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                             IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                             CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                             IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                             IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                             IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');


                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');


                             //******nao usa mais******
                             Submit_valorpauta('0');
                             Submit_ISENTO('N');
                             Submit_SUBSTITUICAOTRIBUTARIA('N');
                             Submit_ALIQUOTA('0');
                             Submit_REDUCAOBASECALCULO('0');
                             Submit_ALIQUOTACUPOM('0');
                             Submit_IPI('0');
                             Submit_percentualagregado('0');
                             Submit_Margemvaloragregadoconsumidor('0');
                             SituacaoTributaria_TabelaA.Submit_CODIGO('');
                             SituacaoTributaria_TabelaB.Submit_CODIGO('');
                             //*********************************


                             Submit_valorfrete('0');
                             Submit_valorseguro('0');

                             Submit_referencia('');

                             if (Salvar(False)=False)
                             Then
                             Begin
                                       messagedlg('N�o foi poss�vel Gravar o kitbox',mterror,[mbok],0);
                                       Self.LimpaNF(PnotaFiscal);
                                       exit;
                             End;
                        End;
                        next;//pr�ximo kitbox do pedido
                   End;//while


                   //*********************PERFILADO******************
                   close;
                   sql.clear;
                   sql.add('Select TABperfilado_PP.* from TABperfilado_PP');
                   sql.add('join tabpedido_proj on TABperfilado_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where Tabpedido_proj.pedido='+Ppedido);
                   open;

                   while not(eof) do
                   Begin
                        With Self.ObjPerfilado_nf do
                        Begin
                             ZerarTabela;
                             if (PerfiladoCor.LocalizaCodigo(fieldbyname('perfiladocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Perfilado nessa cor!',mterror,[mbok],0);
                                     Self.LimpaNF(PnotaFiscal);
                                     exit;
                             End;
                             PerfiladoCor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnotafiscal)=False)
                             Then Begin
                                       mensagemerro('Nota Fiscal n�o encontrada');
                                       exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Status:=dsinsert;
                             Submit_Codigo('0');

                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+Perfiladocor.Perfilado.Get_Codigo);
                             Query.SQL.Add('and material = 2');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.Open;

                             IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                             IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                             IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                             IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                             CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                             IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                             IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                             IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');


                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');


                             //******nao usa mais******
                             Submit_valorpauta('0');
                             Submit_ISENTO('N');
                             Submit_SUBSTITUICAOTRIBUTARIA('N');
                             Submit_ALIQUOTA('0');
                             Submit_REDUCAOBASECALCULO('0');
                             Submit_ALIQUOTACUPOM('0');
                             Submit_IPI('0');
                             Submit_percentualagregado('0');
                             Submit_Margemvaloragregadoconsumidor('0');
                             SituacaoTributaria_TabelaA.Submit_CODIGO('');
                             SituacaoTributaria_TabelaB.Submit_CODIGO('');
                             //*********************************


                             Submit_valorfrete('0');
                             Submit_valorseguro('0');

                             Submit_referencia('');

                             if (Salvar(False)=False)
                             Then Begin
                                       messagedlg('N�o foi poss�vel Gravar o Perfilado',mterror,[mbok],0);
                                      Self.LimpaNF(PnotaFiscal);
                                       exit;
                             End;
                        End;

                        next;//pr�ximo perfilado do pedido
                   End;//while


                   //***************PERSIANA*******************************
                   close;
                   sql.clear;
                   sql.add('Select TABPERSIANA_PP.* from TABPERSIANA_PP');
                   sql.add('join tabpedido_proj on TABPERSIANA_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where tabpedido_proj.pedido='+Ppedido);
                   open;

                   while not(eof) do
                   Begin
                        With Self.ObjPersiana_nf do
                        Begin
                             ZerarTabela;
                             if (persianagrupodiametrocor.LocalizaCodigo(fieldbyname('persianagrupodiametrocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Persiana nessa cor!',mterror,[mbok],0);
                                      Self.LimpaNF(PnotaFiscal);
                                     exit;
                             End;
                             persianagrupodiametrocor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnotafiscal)=False)
                             Then Begin
                                       mensagemerro('Nota Fiscal n�o encontrada');
                                       exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Status:=dsinsert;
                             Submit_Codigo('0');
                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+persianagrupodiametrocor.Persiana.Get_Codigo);
                             Query.SQL.Add('and material = 5');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.Open;

                             IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                             IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                             IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                             IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                             CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                             IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                             IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                             IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');


                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');


                             //******nao usa mais******
                             Submit_valorpauta('0');
                             Submit_ISENTO('N');
                             Submit_SUBSTITUICAOTRIBUTARIA('N');
                             Submit_ALIQUOTA('0');
                             Submit_REDUCAOBASECALCULO('0');
                             Submit_ALIQUOTACUPOM('0');
                             Submit_IPI('0');
                             Submit_percentualagregado('0');
                             Submit_Margemvaloragregadoconsumidor('0');
                             SituacaoTributaria_TabelaA.Submit_CODIGO('');
                             SituacaoTributaria_TabelaB.Submit_CODIGO('');
                             //*********************************


                             Submit_valorfrete('0');
                             Submit_valorseguro('0');

                             Submit_referencia('');


                             if (Salvar(False)=False)
                             Then Begin
                                       messagedlg('N�o foi poss�vel Gravar a Persiana',mterror,[mbok],0);
                                       exit;
                             End;
                        End;

                        next;//pr�xima persiana
                   End;

                   //***************VIDRO*******************************
                   close;
                   sql.clear;
                   sql.add('Select TABVIDRO_PP.* from TABVIDRO_PP');
                   sql.add('join tabpedido_proj on TABVIDRO_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where tabpedido_proj.pedido='+Ppedido);
                   open;

                   while not(eof) do
                   Begin
                        With Self.ObjVidro_nf do
                        Begin
                             ZerarTabela;
                             if (VidroCor.LocalizaCodigo(fieldbyname('vidrocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Vidro nessa cor!',mterror,[mbok],0);
                                      Self.LimpaNF(PnotaFiscal);
                                     exit;
                             End;
                             VidroCor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnotafiscal)=False)
                             Then Begin
                                       mensagemerro('Nota Fiscal n�o encontrada');
                                       exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Status:=dsinsert;
                             Submit_Codigo('0');

                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+Vidrocor.Vidro.Get_Codigo);
                             Query.SQL.Add('and material = 3');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.Open;

                             IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                             IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                             IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                             IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                             CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                             IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                             IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                             IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');


                             Submit_BC_IPI('0');
                             Submit_VALOR_IPI('0');

                             Submit_BC_PIS('0');
                             Submit_VALOR_PIS('0');
                             Submit_BC_PIS_ST('0');
                             Submit_VALOR_PIS_ST('0');

                             Submit_BC_COFINS('0');
                             Submit_VALOR_COFINS('0');
                             Submit_BC_COFINS_ST('0');
                             Submit_VALOR_COFINS_ST('0');


                             //******nao usa mais******
                             Submit_valorpauta('0');
                             Submit_ISENTO('N');
                             Submit_SUBSTITUICAOTRIBUTARIA('N');
                             Submit_ALIQUOTA('0');
                             Submit_REDUCAOBASECALCULO('0');
                             Submit_ALIQUOTACUPOM('0');
                             Submit_IPI('0');
                             Submit_percentualagregado('0');
                             Submit_Margemvaloragregadoconsumidor('0');
                             SituacaoTributaria_TabelaA.Submit_CODIGO('');
                             SituacaoTributaria_TabelaB.Submit_CODIGO('');
                             //*********************************


                             Submit_valorfrete('0');
                             Submit_valorseguro('0');

                             Submit_referencia('');

                             if (Salvar(False)=False)
                             Then Begin
                                       messagedlg('N�o foi poss�vel Gravar o Vidro',mterror,[mbok],0);
                                        Self.LimpaNF(PnotaFiscal);
                                       exit;
                             End;
                        End;


                       next;//pr�ximo vidro
                   End;

              End;//With Self.ObjqueryTemp

              //Concluindo a Importa��o, gravando o desconto
              Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(PnotaFiscal);
              Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;
              Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_DESCONTO(ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorDesconto);

              if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(True)=False)
              then Begin
                        Mensagemerro('Erro na tentativa de Salvar o desconto na Nota Fiscal');
                        Self.LimpaNF(PnotaFiscal);
                        exit;
              End;



     Finally
            FDataModulo.IBTransaction.RollbackRetaining;
            ObjPedidoObjetos.Free;
            FreeAndNil(Query);
     End;




end;

procedure TObjNotafiscalObjetos.LimpaNF(Notafiscal:string);
var
  Query:TIBQuery;
begin
  try
      Query:=TIBQuery.Create(nil);
      query.Database:=FDataModulo.IBDatabase;
  except

  end;

  try
      with Query do
      begin
         Close;
         sql.Clear;
         SQL.Add('update tabnotafiscal  set numpedido=null,cliente=null,situacao=''N'',valortotal=null,desconto=null,naturezaoperacao=null');
         sql.Add(',datasaida=null,horasaida=null,dataemissao=null,operacao=null where codigo='+NotaFiscal);
         Query.ExecSQL;
         FDataModulo.IBTransaction.CommitRetaining;

         sql.Clear;
         SQL.Add('delete from tabferragem_nf where notafiscal='+NotaFiscal);
         ExecSQL;
         FDataModulo.IBTransaction.CommitRetaining;

         sql.Clear;
         SQL.Add('delete from tabdiverso_nf where notafiscal='+NotaFiscal);
         ExecSQL;
         FDataModulo.IBTransaction.CommitRetaining;

         sql.Clear;
         SQL.Add('delete from tabkitbox_nf where notafiscal='+NotaFiscal);
         ExecSQL;
         FDataModulo.IBTransaction.CommitRetaining;

         sql.Clear;
         SQL.Add('delete from tabperfilado_nf where notafiscal='+NotaFiscal);
         ExecSQL;
         FDataModulo.IBTransaction.CommitRetaining;

         sql.Clear;
         SQL.Add('delete from tabpersiana_nf where notafiscal='+NotaFiscal);
         ExecSQL;
         FDataModulo.IBTransaction.CommitRetaining;

         sql.Clear;
         SQL.Add('delete from tabvidro_nf where notafiscal='+NotaFiscal);
         ExecSQL;
         FDataModulo.IBTransaction.CommitRetaining;



      end;
  finally
    FreeAndNil(Query);
  end;


end;



function TObjNotafiscalObjetos.CalculaDifal(pUF_Destino: string;
  pBC_Icms: Currency; var pPERCENTUAL_ICMS_UF_DEST, pVALOR_ICMS_UF_DEST,
  pVALOR_ICMS_UF_REMET, pPERCENTUAL_ICMS_INTER,
  pPERCENTUAL_ICMS_INTER_PART: currency):boolean;
var
  percentual_diferenca, icms_diferenca: Currency;
  msg_erro: string;
begin
  Result := false;
  
  pPERCENTUAL_ICMS_UF_DEST     := 0;
  pVALOR_ICMS_UF_DEST          := 0;
  pVALOR_ICMS_UF_REMET         := 0;
  pPERCENTUAL_ICMS_INTER       := 0;
  pPERCENTUAL_ICMS_INTER_PART  := 0;

  msg_erro := 'Aten��o: N�o foi poss�vel calcular o Diferencial de Al�quotas para venda interestadual.';
  percentual_diferenca := 0;

  {resgatar o percentual do ICMS interestadual do estado de origem
  resgatar o percentual do ICMS interno no estado de destino
  calcular o valor do icms sobre a diferenca ( ICMS interno dest - ICMS interestadual orig)
  realizar a partilha de acordo com a tabela abaixo:
  Ano   %ICMS DIFAL Origem  %ICMS DIFAL Destino
  2016  60                  40
  2017  40                  60
  2018  20                  80
  2019   0                  100
  }
  try
    //percentual do ICMS interestadual do estado de origem
    pPERCENTUAL_ICMS_INTER := StrToCurr( get_campoTabela('aliquota','','tabicms_interestadual','where uf_origem='+QuotedStr(ObjEmpresaGlobal.Get_ESTADO)+' and uf_destino='+QuotedStr(puf_destino) ) );
  except
    MensagemErro(msg_erro+#13+'Erro ao resgatar Percentual de ICMS Interestadual da UF: ' + ObjEmpresaGlobal.Get_ESTADO);
    Exit;
  end;

  try
    //percentual do ICMS interno no estado de destino
    pPERCENTUAL_ICMS_UF_DEST := StrToCurr( get_campoTabela('aliquota','','tabicms_interestadual','where uf_origem='+QuotedStr( puf_destino )+' and uf_destino='+QuotedStr( puf_destino ) ) );
  except
    MensagemErro(msg_erro+#13+'Erro ao resgatar Percentual de ICMS da UF: ' + puf_destino);
    Exit;
  end;

  try
    percentual_diferenca := pPERCENTUAL_ICMS_UF_DEST - pPERCENTUAL_ICMS_INTER;
  except
    MensagemErro(msg_erro+#13+'Erro ao calcular a diferen�a de al�quotas');
    Exit;
  end;

  try
    icms_diferenca := pBC_Icms * percentual_diferenca/100;
    if( FormatDateTime('yyyy', now) = '2016' ) then
    begin
      pVALOR_ICMS_UF_DEST := icms_diferenca * 0.40;
      pVALOR_ICMS_UF_REMET := icms_diferenca * 0.60;
      pPERCENTUAL_ICMS_INTER_PART := 40;
    end
    else
    if( FormatDateTime('yyyy', now) = '2017' ) then
    begin
      pVALOR_ICMS_UF_DEST := icms_diferenca * 0.60;
      pVALOR_ICMS_UF_REMET := icms_diferenca * 0.40;
      pPERCENTUAL_ICMS_INTER_PART := 60;
    end
    else
    if( FormatDateTime('yyyy', now) = '2018' ) then
    begin
      pVALOR_ICMS_UF_DEST := icms_diferenca * 0.80;
      pVALOR_ICMS_UF_REMET := icms_diferenca * 0.20;
      pPERCENTUAL_ICMS_INTER_PART := 80;
    end
    else
    begin
      //2019 para frente
      pVALOR_ICMS_UF_DEST := icms_diferenca;
      pVALOR_ICMS_UF_REMET := 0;
      pPERCENTUAL_ICMS_INTER_PART := 100;
    end;

    if( ObjEmpresaGlobal.get_Simples = 'S' ) then
      pVALOR_ICMS_UF_REMET := 0;

    //truncando os calores em 2 casas decimais
    pVALOR_ICMS_UF_DEST := StrToCurr( formata_valor( CurrToStr( pVALOR_ICMS_UF_DEST ), 2, true ) );
    pVALOR_ICMS_UF_REMET := StrToCurr( formata_valor( CurrToStr( pVALOR_ICMS_UF_REMET ), 2, true ) );

  except
    on e:Exception do
    begin
      MensagemErro(msg_erro+#13+e.Message);
      exit;
    end;
  end;

  Result := True;
end;


function TObjNotafiscalObjetos.CalculaBaseDeCalculoNovaForma(CodigoVenda:string;ValorDesconto:string;PnfeDigitada:string):boolean;
VAR
  StrlCfop:TStringList;

  query:TIBQuery;

  Pformula:String;

  MateriaisVenda:TObjMateriaisVenda;

  {ICMS}
  TMPBASEC_ICMS_PRODUTO,TMPVALOR_ICMS_PRODUTO:Currency;
  TMPPERC_ICMS,TMPREDUCAOBCICMS:Currency;

  {ICMS ST}
  TMPBASEC_ICMS_PRODUTO_ST,TMPVALOR_ICMS_PRODUTO_ST:Currency;
  TMPPERC_ICMS_ST,TMPREDUCAOBCICMS_ST:currency;

  {PIS}
  PBC_PIS_produto,PvalorPIS_Produto:currency;
  pPis:currency;
  cstPis:string;

  {PIS ST}
  PBC_PIS_ST_produto,PValorPIS_ST_Produto:currency;

  {COFINS}
  PBC_COFINS_produto,PvalorCOFINS_PRODUTO:currency;
  pCofins:currency;
  cstCofins:string;

  {COFINS ST}
  PBC_COFINS_ST_produto,PVALORCofins_ST_Produto:currency;

  {IPI}
  PvalorIpiProduto:Currency;

  {TOTAIS}
  PvalorPIS_Total,PValorPIS_ST_total,
  PvalorCOFINS_total,PVALORCofins_ST_total,pvaloripitotal:Currency;


  PvalorTotalFrete,PvalorFreteProduto,PvalorTotalSeguro,PvalorSeguroProduto:Currency;
  tmpvalorproduto,TMpDescontototal,TmpValorTotalVenda,TmpPorcentagemDesconto:Currency;


  TMPBASEC_ICMS,TMPVALORICMS,TMPVALORICMS_recolhido,TMPBASEC_ICMS_recolhido:currency;
  PBaseCalculoIPIProduto:Currency;
  DESCONTO:Currency;

  VendadeProjeto: Boolean;

  totalItens: Integer;
  somaDescontoItens: Currency;

  PERCENTUAL_ICMS_UF_DEST, VALOR_ICMS_UF_DEST,
  VALOR_ICMS_UF_REMET, PERCENTUAL_ICMS_INTER,
  PERCENTUAL_ICMS_INTER_PART: currency;

  //cliente ou fornecedor
  uf_destinatario,ie_destinatario,tipo_pessoa:string;
  calculoDifal:Boolean;

  Procedure SubstituiFormulas;
  Begin
       //Funcao dentro de funcao

       //Valores calculados nos produtos

       PFormula:=StringReplace(pformula,'VALORFINAL_PROD',virgulaparaponto(floattostr(tmpvalorproduto)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VALORFRETE_PROD',virgulaparaponto(floattostr(PvalorFreteProduto)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VALORSEGURO_PROD',virgulaparaponto(floattostr(PvalorSeguroProduto)),[rfReplaceAll,rfIgnoreCase]);

       PFormula:=StringReplace(pformula,'VALORIPI_PROD',virgulaparaponto(floattostr(PvalorIpiProduto)),[rfReplaceAll,rfIgnoreCase]);

       PFormula:=StringReplace(pformula,'VALOR_PIS_PROD',virgulaparaponto(FLOATTOSTR(PvalorPIS_Produto)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VALOR_PIS_ST_PROD',virgulaparaponto(FLOATTOSTR(PvalorPIS_ST_Produto)),[rfReplaceAll,rfIgnoreCase]);

       PFormula:=StringReplace(pformula,'VALOR_COFINS_PROD',virgulaparaponto(FLOATTOSTR(PvalorCOFINS_Produto)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VALOR_COFINS_ST_PROD',virgulaparaponto(FLOATTOSTR(PvalorCOFINS_ST_Produto)),[rfReplaceAll,rfIgnoreCase]);

       //ICMS
       PFormula:=StringReplace(pformula,'PERC_REDUCAO_BC_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_PERC_REDUCAO_BC),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTA_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_ALIQUOTA),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'IVA_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_IVA),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'PAUTA_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_PAUTA),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'BC_ICMS',virgulaparaponto(floattostr(TMPBASEC_ICMS_PRODUTO)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VALOR_ICMS',virgulaparaponto(floattostr(TMPVALOR_ICMS_PRODUTO)),[rfReplaceAll,rfIgnoreCase]);

       TMPPERC_ICMS     := StrToCurrDef(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_ALIQUOTA,0);
       TMPREDUCAOBCICMS := StrToCurrDef(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_PERC_REDUCAO_BC,0);

       //ICMS ST
       PFormula:=StringReplace(pformula,'PERC_REDUCAO_BC_ST_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_PERC_REDUCAO_BC_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTA_ST_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_ALIQUOTA_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'IVA_ST_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_IVA_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'PAUTA_ST_ICMS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_PAUTA_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'BC_ST_ICMS',virgulaparaponto(floattostr(TMPBASEC_ICMS_PRODUTO_ST)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VALOR_ST_ICMS',virgulaparaponto(floattostr(TMPVALOR_ICMS_PRODUTO_ST)),[rfReplaceAll,rfIgnoreCase]);

       TMPPERC_ICMS_ST      := StrToCurrDef(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_ALIQUOTA_ST,0);
       TMPREDUCAOBCICMS_ST  := StrToCurrDef(sELF.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_PERC_REDUCAO_BC_ST,0);


       //IPI
       PFormula:=StringReplace(pformula,'BC_IPI',virgulaparaponto(FLOATTOSTR(PBaseCalculoIPIProduto)),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTA_IPI',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_IPI.Get_Aliquota),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'QTDE_UP_IPI',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_IPI.Get_QuantidadeTotalUP),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'VL_UP_IPI',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_IPI.Get_ValorUnidade),[rfReplaceAll,rfIgnoreCase]);

       //PIS
       PFormula:=StringReplace(pformula,'ALIQUOTAPERCENTUAL_PIS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_PIS_ORIGEM.Get_ALIQUOTAPERCENTUAL),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTAVALOR_PIS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_PIS_ORIGEM.Get_ALIQUOTAVALOR),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'BC_PIS',virgulaparaponto(FLOATTOSTR(PBC_PIS_produto)),[rfReplaceAll,rfIgnoreCase]);

       pPis := StrToCurrDef(sELF.ObjVidro_NF.IMPOSTO_PIS_ORIGEM.Get_ALIQUOTAPERCENTUAL,0);
       cstPis := sELF.ObjVidro_NF.IMPOSTO_PIS_ORIGEM.ST.Get_CODIGO;

       //PIS ST
       PFormula:=StringReplace(pformula,'ALIQUOTAPERCENTUAL_ST_PIS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_PIS_ORIGEM.Get_ALIQUOTAPERCENTUAL_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTAVALOR_ST_PIS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_PIS_ORIGEM.Get_ALIQUOTAVALOR_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'BC_ST_PIS',virgulaparaponto(FLOATTOSTR(PBC_PIS_ST_produto)),[rfReplaceAll,rfIgnoreCase]);

       //COFINS
       PFormula:=StringReplace(pformula,'ALIQUOTAPERCENTUAL_COFINS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.Get_ALIQUOTAPERCENTUAL),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTAVALOR_COFINS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.Get_ALIQUOTAVALOR),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'BC_COFINS',virgulaparaponto(FLOATTOSTR(PBC_COFINS_produto)),[rfReplaceAll,rfIgnoreCase]);

       pCofins := StrToCurrDef(sELF.ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.Get_ALIQUOTAPERCENTUAL,0);
       cstCofins := sELF.ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.ST.Get_CODIGO;

       //COFINS ST
       PFormula:=StringReplace(pformula,'ALIQUOTAPERCENTUAL_ST_COFINS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.Get_ALIQUOTAPERCENTUAL_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'ALIQUOTAVALOR_ST_COFINS',virgulaparaponto(sELF.ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.Get_ALIQUOTAVALOR_ST),[rfReplaceAll,rfIgnoreCase]);
       PFormula:=StringReplace(pformula,'BC_ST_COFINS',virgulaparaponto(FLOATTOSTR(PBC_COFINS_ST_produto)),[rfReplaceAll,rfIgnoreCase]);


  End;

begin

  result:=False;

  if (PnfeDigitada = '') and (CodigoVenda = '') then
    Exit;


   //acerta a base de calculo e a aliquota
   //de acordo com os produtos da venda

   query := TIBQuery.Create(nil);
   query.Database := FDataModulo.IBDatabase;
   MateriaisVenda := TObjMateriaisVenda.create;

   try

     PvalorIpiProduto:=0;
     pvaloripitotal:=0;
     PvalorTotalFrete:=0;
     PvalorFreteProduto:=0;
     PvalorTotalSeguro:=0;
     PvalorSeguroProduto:=0;
     tmpvalorproduto:=0;
     TMpDescontototal:=0;
     TmpValorTotalVenda:=0;
     TmpPorcentagemDesconto:=0;
     TMPBASEC_ICMS_PRODUTO:=0;
     TMPVALOR_ICMS_PRODUTO:=0;
     TMPBASEC_ICMS_PRODUTO_ST:=0;
     TMPVALOR_ICMS_PRODUTO_ST:=0;
     TMPBASEC_ICMS:=0;
     TMPVALORICMS:=0;
     TMPVALORICMS_recolhido:=0;
     TMPBASEC_ICMS_recolhido:=0;

     try

        Try
          StrlCfop:=TStringList.create;
          StrlCfop.clear;
        Except
          Messagedlg('Erro na tentativa de cria��o da StringList de CFOPs',mterror,[mbok],0);
          exit;
        End;

        if (PnfeDigitada = '') then
        begin

          {Verificando se a venda existe}
          with query do
          begin

            Close;
            sql.Clear;

            SQL.Add('select tabpedido.*,c.estado as uf_destinatario,c.rg_ie as ie_destinatario,c.fisica_juridica as tipo_pessoa from tabpedido');
            sql.Add('inner join tabcliente c on c.codigo=tabpedido.cliente');
            sql.Add('where tabpedido.codigo='+CodigoVenda);

            Open;

            if(recordcount = 0) then
              Exit;

          end;

        end
        else
        begin

          with (query) do
          begin

            Active := False;
            sql.Clear;

            //SQL.Add('select * from tabnfedigitada where codigo = '+PnfeDigitada);

            SQL.Add('select tabnfedigitada.*,');
            SQL.Add('case when (c.estado is not null and c.estado<>'''') then c.estado          else f.estado end as uf_destinatario,');
            sql.Add('case when (c.estado is not null and c.estado<>'''') then c.rg_ie           else f.ie     end as ie_destinatario,');
            sql.Add('case when (c.estado is not null and c.estado<>'''') then c.fisica_juridica else ''J''    end as tipo_pessoa');
            SQL.Add('from tabnfedigitada');
            SQL.Add('left join tabcliente c on c.codigo = tabnfedigitada.cliente');
            SQL.Add('left join tabfornecedor f on f.codigo = tabnfedigitada.fornecedor');
            SQL.Add('where tabnfedigitada.codigo = '+PnfeDigitada);

            active := True;


            if (recordCount = 0) then
              Exit;

          end;

        end;

        uf_destinatario := query.fieldbyname('uf_destinatario').AsString;
        ie_destinatario := query.fieldbyname('ie_destinatario').AsString;
        tipo_pessoa     := query.fieldbyname('tipo_pessoa').AsString;

        calculoDifal := false;
        if (ObjEmpresaGlobal.Get_ESTADO <> uf_destinatario) then
        begin
          calculoDifal := true;
          if (tipo_pessoa='J') and (ie_destinatario<>'') then
            calculoDifal := false;

        end;


        With self.Objquery do
        Begin

          tmpdescontototal := StrToCurr(tira_ponto(ValorDesconto));

          if (PnfeDigitada = '') then
            TmpValorTotalVenda := StrToFloatDef(query.fieldbyname('VALORTOTAL').AsString,0)

          else
            TmpValorTotalVenda := StrToFloatDef(query.fieldbyname('valorProduto').AsString,0);

          if (TMpDescontototal > 0) Then
          Begin
            {descobrindo o que o desconto
            representa em termos de % do valor total}
            TmpPorcentagemDesconto := (TMpDescontototal*100)/TmpValorTotalVenda;
            TmpPorcentagemDesconto := strtofloat(tira_ponto(formata_valor(floattostr(TmpPorcentagemDesconto))));
          End
          Else
            TmpPorcentagemDesconto:=0;

          {selecionando todos os produtos da venda}

          Close;
          SQL.clear;

          if (PnfeDigitada = '') then
            SQL.add('Select * from viewprodutosvenda where pedido='+CodigoVenda)
          else
            SQL.Add('select * from viewprodutosvenda where nfedigitada ='+PnfeDigitada);

          open;
          Last;
          totalItens := Objquery.RecordCount;
          first;


          PvalorTotalFrete:=0;
          PvalorTotalSeguro:=0;
          DESCONTO:=0;

          {Zerando Impostos TOTAIS}

          {ICMS}
          TMPBASEC_ICMS := 0;
          TMPVALORICMS := 0;

          {ICMS ST}
          TMPVALORICMS_recolhido := 0;
          TMPBASEC_ICMS_recolhido := 0;

          {IPI}
          PvalorIPITotal := 0;

          {PIS}
          PvalorPIS_Total := 0;
          PvalorPIS_ST_Total := 0;

          {COFINS}
          PvalorCOFINS_Total := 0;
          PvalorCOFINS_ST_Total := 0;

          {pego o primeiro, pois se o primeiro for um projeto, todos ser�o}

          VendadeProjeto := (Objquery.FieldByName('PROJETO').AsString <> '');
          somaDescontoItens := 0;

          While not(eof) do
          Begin

            tmpvalorproduto := 0;

            PBC_PIS_produto := 0;
            PBC_PIS_ST_produto := 0;
            PBC_COFINS_produto := 0;
            PBC_COFINS_ST_produto := 0;

            PvalorPIS_Produto := 0;
            PValorPIS_ST_Produto := 0;
            PvalorCOFINS_PRODUTO := 0;
            PVALORCofins_ST_Produto := 0;

            PvalorIpiProduto := 0;
            TMPBASEC_ICMS_PRODUTO := 0;
            TMPVALOR_ICMS_PRODUTO := 0;
            TMPBASEC_ICMS_PRODUTO_ST := 0;
            TMPVALOR_ICMS_PRODUTO_ST := 0;

            {acertando o valor do produto}
            if (TMpDescontototal>0) then
            begin
              if(Objquery.RecNo = totalItens) then
              begin
                //como o calculo do desconto envolve percentual pode ser que ao final
                //os a soma do desconto do itens nao coincida com o valor total do desconto
                //entao pego o ultimo e jogo a diferen�a para atingir o desconto total

                //tmpvalorproduto := fieldbyname('valorfinal').asfloat - ((fieldbyname('valorfinal').asfloat*TmpPorcentagemDesconto)/100);
                tmpvalorproduto := fieldbyname('valorfinal').asfloat - ( TMpDescontototal - somaDescontoItens );
                DESCONTO := fieldbyname('valorfinal').asfloat - tmpvalorproduto;
              end
              else
              begin
                tmpvalorproduto := fieldbyname('valorfinal').asfloat - ((fieldbyname('valorfinal').asfloat*TmpPorcentagemDesconto)/100);
                DESCONTO := fieldbyname('valorfinal').asfloat - tmpvalorproduto;
              end;
              somaDescontoItens := somaDescontoItens + DESCONTO;
            end
            else
              tmpvalorproduto := fieldbyname('valorfinal').asfloat;


            PvalorFreteProduto := fieldbyname('valorfrete').asfloat;
            PvalorSeguroProduto := fieldbyname('valorseguro').asfloat;
            PvalorTotalFrete := PvalorTotalfrete+PvalorfreteProduto;
            PvalorTotalSeguro := PvalorTotalSeguro+PvalorSeguroProduto;

            {IPI}

            if (fieldbyname('IMPOSTO_IPI').asstring = '')then
            begin
              MensagemErro('O PRODUTO'+fieldbyname('TIPO').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+'-'+fieldbyname('PRODUTO').asstring+' n�o possui o Imposto de IPI cadastrado');
              exit;
            end;

            if (self.ObjVidro_NF.IMPOSTO_IPI.LocalizaCodigo(fieldbyname('IMPOSTO_IPI').asstring)=False) then
            begin
              MensagemErro('IMPOSTO_IPI C�DIGO N�O ENCONTRADO PARA O PRODUTO');
              Exit;
            end;

            self.ObjVidro_NF.IMPOSTO_IPI.TabelaparaObjeto;

            Pformula:=self.ObjVidro_NF.IMPOSTO_IPI.Get_FORMULABASECALCULO;
            SubstituiFormulas;

            Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
            Fcalculaformula_Vidro.RDFormula.Execute;

            if (Fcalculaformula_Vidro.RDFormula.Falhou=True) then
            begin
              mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" da BC do IPI. Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('PRODUTO').asstring);
              exit;
            end;

            PBaseCalculoIPIProduto:=Fcalculaformula_vidro.RDFormula.Resultado;

            {VALOR DO IPI}

            Pformula := self.ObjVidro_NF.IMPOSTO_IPI.Get_formula_valor_imposto;
            SubstituiFormulas;

            Fcalculaformula_Vidro.RDFormula.Expressao := Pformula;
            Fcalculaformula_Vidro.RDFormula.Execute;

            if (Fcalculaformula_Vidro.RDFormula.Falhou=True) then
            begin
              mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" do valor do IPI. Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
              exit;
            end;

            PvalorIpiProduto:=Fcalculaformula_vidro.RDFormula.Resultado;

            {PIS}

            if (fieldbyname('imposto_pis_origem').asstring<>'') then
            begin

              if (self.ObjVidro_NF.imposto_pis_origem.LocalizaCodigo(fieldbyname('imposto_pis_origem').asstring)=False) then
              begin
                MensagemErro('imposto_pis_origem C�DIGO '+fieldbyname('imposto_icms_origem').asstring+' N�O ENCONTRADO PARA O PRODUTO'+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
                Exit;
              end;

              self.ObjVidro_NF.imposto_pis_origem.TabelaparaObjeto;

              Pformula:=self.ObjVidro_NF.imposto_pis_origem.Get_FORMULABASECALCULO;
              SubstituiFormulas;

              Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
              Fcalculaformula_Vidro.RDFormula.Execute;

              if (Fcalculaformula_Vidro.RDFormula.Falhou=True) then
              begin
               mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" da BC do PIS. Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
               exit;
              end;

              PBC_PIS_produto:=Fcalculaformula_Vidro.RDFormula.Resultado;

              {VALOR DO PIS}
              Pformula := self.ObjVidro_NF.imposto_pis_origem.Get_formula_valor_imposto;
              SubstituiFormulas;

              Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
              Fcalculaformula_Vidro.RDFormula.Execute;

              if (Fcalculaformula_Vidro.RDFormula.Falhou=True) then
              begin
                mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" do valor do PIS. Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
                exit;
              end;
              PvalorPIS_Produto:=Fcalculaformula_Vidro.RDFormula.Resultado;

            end;


            {PIS ST}

            if (fieldbyname('imposto_pis_destino').asstring<>'') then
            begin

              if (self.ObjVidro_NF.imposto_pis_destino.LocalizaCodigo(fieldbyname('imposto_pis_destino').asstring)=False) then
              begin
                MensagemErro('imposto_pis_destino C�DIGO '+fieldbyname('imposto_icms_origem').asstring+' N�O ENCONTRADO PARA O PRODUTO'+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
              end;

              self.ObjVidro_NF.imposto_pis_destino.TabelaparaObjeto;

              Pformula:=self.ObjVidro_NF.imposto_pis_destino.Get_FORMULABASECALCULO_ST;
              SubstituiFormulas;

              Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
              Fcalculaformula_Vidro.RDFormula.Execute;

              if (Fcalculaformula_Vidro.RDFormula.Falhou=True) then
              begin
                mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" da BC do PIS ST. Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
                exit;
              end;

              PBC_PIS_ST_produto:=Fcalculaformula_Vidro.RDFormula.Resultado;

              {VALOR DO PIS ST}
              Pformula:=self.ObjVidro_NF.imposto_pis_destino.Get_formula_valor_imposto_ST;
              SubstituiFormulas;

              Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
              Fcalculaformula_Vidro.RDFormula.Execute;

              if (Fcalculaformula_Vidro.RDFormula.Falhou=True) then
              begin
                mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" do valor do PIS ST. Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
                exit;
              end;

              PValorPIS_ST_Produto:=Fcalculaformula_Vidro.RDFormula.Resultado;
              
            end;

            {COFINS}

            if (fieldbyname('imposto_COFINS_origem').asstring<>'') then
            begin

              if (self.ObjVidro_NF.imposto_COFINS_origem.LocalizaCodigo(fieldbyname('imposto_COFINS_origem').asstring)=False) then
              begin
                MensagemErro('imposto_COFINS_origem C�DIGO '+fieldbyname('imposto_icms_origem').asstring+' N�O ENCONTRADO PARA O PRODUTO'+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
                Exit;
              end;

              self.ObjVidro_NF.imposto_COFINS_origem.TabelaparaObjeto;

              Pformula:=self.ObjVidro_NF.imposto_COFINS_origem.Get_FORMULABASECALCULO;
              SubstituiFormulas;

              Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
              Fcalculaformula_Vidro.RDFormula.Execute;

              if (Fcalculaformula_Vidro.RDFormula.Falhou=True) Then
              Begin
                mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" da BC do COFINS. Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
                exit;
              End;

              PBC_COFINS_produto:=Fcalculaformula_Vidro.RDFormula.Resultado;

              {VALOR DO COFINS}
              Pformula:=self.ObjVidro_NF.imposto_COFINS_origem.Get_formula_valor_imposto;
              SubstituiFormulas;

              Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
              Fcalculaformula_Vidro.RDFormula.Execute;

              if (Fcalculaformula_Vidro.RDFormula.Falhou=True) Then
              Begin
                mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" do valor do COFINS. Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
                exit;
              End;

              PvalorCOFINS_Produto:=Fcalculaformula_Vidro.RDFormula.Resultado;

            End;

            {COFINS ST}

            if (fieldbyname('imposto_COFINS_destino').asstring<>'') then
            Begin

              if (self.ObjVidro_NF.imposto_COFINS_destino.LocalizaCodigo(fieldbyname('imposto_COFINS_destino').asstring)=False) Then
              Begin
                MensagemErro('imposto_COFINS_destino C�DIGO '+fieldbyname('imposto_icms_origem').asstring+' N�O ENCONTRADO PARA O PRODUTO'+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
                Exit;
              end;

              self.ObjVidro_NF.imposto_COFINS_destino.TabelaparaObjeto;

              Pformula:=self.ObjVidro_NF.imposto_COFINS_destino.Get_FORMULABASECALCULO_ST;
              SubstituiFormulas;

              Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
              Fcalculaformula_Vidro.RDFormula.Execute;

              if (Fcalculaformula_Vidro.RDFormula.Falhou=True) Then
              Begin
                mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" da BC do COFINS ST. Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
                exit;
              End;

              PBC_COFINS_ST_produto:=Fcalculaformula_Vidro.RDFormula.Resultado;

              {VALOR DO COFINS ST}
              Pformula:=self.ObjVidro_NF.imposto_COFINS_destino.Get_formula_valor_imposto_ST;
              SubstituiFormulas;

              Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
              Fcalculaformula_Vidro.RDFormula.Execute;

              if (Fcalculaformula_Vidro.RDFormula.Falhou=True) Then
              Begin
                mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" do valor do COFINS ST. Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
                exit;
              End;

              PValorCOFINS_ST_Produto:=Fcalculaformula_Vidro.RDFormula.Resultado;

            End;

            {ICMS}

            if (fieldbyname('imposto_icms_origem').asstring='') then
            Begin
              MensagemErro('O PRODUTO'+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring+' n�o possui o Imposto de ICMS de ORIGEM');
              exit;
            End;

            if (self.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.LocalizaCodigo(fieldbyname('imposto_icms_origem').asstring)=False) Then
            Begin
              MensagemErro('IMPOSTO ICMS DE ORIGEM C�DIGO '+fieldbyname('imposto_icms_origem').asstring+' N�O ENCONTRADO PARA O PRODUTO'+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
              Exit;
            End;

            self.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.TabelaparaObjeto;

            if (self.ObjVidro_NF.IMPOSTO_ICMS_DESTINO.LocalizaCodigo(fieldbyname('imposto_icms_destino').asstring)=False) Then
            Begin
              MensagemErro('IMPOSTO ICMS DE DESTINO C�DIGO '+fieldbyname('imposto_icms_destino').asstring+' N�O ENCONTRADO PARA O PRODUTO'+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
              Exit;
            End;

            self.ObjVidro_NF.IMPOSTO_ICMS_DESTINO.TabelaparaObjeto;

            //A Base de Calculo sempre � da Origem
            //***********************FORMULA DA BC DO ICMS**************************
            PFormula:=Self.ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.Get_FORMULA_BC;

            SubstituiFormulas;

            Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
            Fcalculaformula_Vidro.RDFormula.Execute;

            if (Fcalculaformula_Vidro.RDFormula.Falhou=True) Then
            Begin
              mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" da BC do ICMS. Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
              exit;
            End;

            TMPBASEC_ICMS_PRODUTO:=Fcalculaformula_vidro.RDFormula.Resultado;


            {FORMULA DO VALOR DO ICMS}
            PFormula:=Self.ObjVidro_NF.IMPOSTO_ICMS_DESTINO.Get_formula_valor_imposto;
            SubstituiFormulas;

            Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
            Fcalculaformula_Vidro.RDFormula.Execute;

            if (Fcalculaformula_Vidro.RDFormula.Falhou=True) Then
            Begin
              mensagemerro('N�o � poss�vel calcular a f�rmula: "'+PfORMULA+#13+'" do Valor do ICMS. Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
              exit;
            End;
            TMPVALOR_ICMS_PRODUTO:=Fcalculaformula_vidro.RDFormula.Resultado;



            {FORMULA DA BC DO ICMS ST}
            {A Substitui��o sempre � do Destino}
            PFormula:=Self.ObjVidro_NF.IMPOSTO_ICMS_DESTINO.Get_FORMULA_BC_ST;
            SubstituiFormulas;

            Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
            Fcalculaformula_Vidro.RDFormula.Execute;

            if (Fcalculaformula_Vidro.RDFormula.Falhou=True) Then
            Begin
              mensagemerro('N�o � poss�vel calcular a f�rmula da Base de C�lculo ST: '+PfORMULA+#13+'Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
              exit;
            End;

            TMPBASEC_ICMS_PRODUTO_ST:=Fcalculaformula_vidro.RDFormula.Resultado;

            {FORMULA DO VALOR DO ICMS ST}

            TMPVALOR_ICMS_PRODUTO_ST:=0;

            PFormula:=Self.ObjVidro_NF.IMPOSTO_ICMS_DESTINO.Get_formula_valor_imposto_ST;
            SubstituiFormulas;

            Fcalculaformula_Vidro.RDFormula.Expressao:=Pformula;
            Fcalculaformula_Vidro.RDFormula.Execute;
            if (Fcalculaformula_Vidro.RDFormula.Falhou=True) Then
            Begin
              mensagemerro('N�o � poss�vel calcular a f�rmula: '+PfORMULA+#13+'Express�o Inv�lida'+#13+'Produto '+fieldbyname('material').asstring+'-'+fieldbyname('CODIGOPRODUTO').asstring);
              exit;
            End;

            TMPVALOR_ICMS_PRODUTO_ST:=Fcalculaformula_vidro.RDFormula.Resultado;

            {Acumulando os valores dos produtos nos totais}

            {Arredondando}
            PvalorIpiProduto:=Self.Arredonda_NFE(PvalorIpiProduto);
            PvalorPIS_Produto:=Self.Arredonda_NFE(PvalorPIS_Produto);
            PValorPIS_ST_Produto:=Self.Arredonda_NFE(PValorPIS_ST_Produto);
            PvalorCOFINS_Produto:=Self.Arredonda_NFE(PvalorCOFINS_Produto);
            PValorCOFINS_ST_Produto:=Self.Arredonda_NFE(PValorCOFINS_ST_Produto);

            TMPBASEC_ICMS_PRODUTO:=Self.Arredonda_NFE(TMPBASEC_ICMS_PRODUTO);
            TMPVALOR_ICMS_PRODUTO:=Self.Arredonda_NFE(TMPVALOR_ICMS_PRODUTO);
            TMPBASEC_ICMS_PRODUTO_ST:=Self.Arredonda_NFE(TMPBASEC_ICMS_PRODUTO_ST);
            TMPVALOR_ICMS_PRODUTO_ST:=Self.Arredonda_NFE(TMPVALOR_ICMS_PRODUTO_ST);

            PBC_PIS_produto := Self.Arredonda_NFE(PBC_PIS_produto);
            PBC_PIS_ST_produto := self.Arredonda_NFE(PBC_PIS_ST_produto);
            PBC_COFINS_produto := self.Arredonda_NFE(PBC_COFINS_produto);
            PBC_COFINS_ST_produto := self.Arredonda_NFE(PBC_COFINS_ST_produto);
             //***************************************************************

            pvaloripitotal:=pvaloripitotal+PvalorIpiProduto;
            PvalorPIS_Total:=PvalorPIS_Total+PvalorPIS_Produto;
            PValorPIS_ST_total:=PValorPIS_ST_total+PValorPIS_ST_Produto;
            PvalorCOFINS_Total:=PvalorCOFINS_Total+PvalorCOFINS_Produto;
            PValorCOFINS_ST_total:=PValorCOFINS_ST_total+PValorCOFINS_ST_Produto;

            TMPBASEC_ICMS:=TMPBASEC_ICMS + TMPBASEC_ICMS_PRODUTO;
            TMPVALORICMS:=TMPVALORICMS   + TMPVALOR_ICMS_PRODUTO;

            TMPBASEC_ICMS_recolhido:=TMPBASEC_ICMS_recolhido+TMPBASEC_ICMS_PRODUTO_ST;
            TMPVALORICMS_recolhido:=TMPVALORICMS_recolhido+TMPVALOR_ICMS_PRODUTO_ST;

            {gravando na materias venda}
            with MateriaisVenda do
            begin

              ZerarTabela;
              LocalizaCodigo(fieldbyname('codigo').AsString);
              TabelaParaObjetos;
              state:=dsEdit;

              {ICMS}
              Submit_BC_ICMS(CurrToStr(TMPBASEC_ICMS_PRODUTO));
              Submit_VALOR_ICMS(CurrToStr(TMPVALOR_ICMS_PRODUTO));
              submit_percentualicms(CurrToStr(TMPPERC_ICMS));
              Submit_REDUCAOBASECALCULO(CurrToStr(TMPREDUCAOBCICMS));

              {ICMST ST}
              Submit_BC_ICMS_ST(CurrToStr(TMPBASEC_ICMS_PRODUTO_ST));
              Submit_VALOR_ICMS_ST(CurrToStr(TMPVALOR_ICMS_PRODUTO_ST));
              submit_reducaobasecalculo_st(CurrToStr(TMPREDUCAOBCICMS_ST));
              submit_percentualicms_st(CurrToStr(TMPPERC_ICMS_ST));

              {IPI}
              Submit_BC_IPI(CurrToStr(PBaseCalculoIPIProduto));
              Submit_VALOR_IPI(CurrToStr(PvalorIpiProduto));

              {PIS}
              Submit_BC_PIS(CurrToStr(PBC_PIS_produto));
              Submit_VALOR_PIS(CurrToStr(PvalorPIS_Produto));
              submit_percentualpis(CurrToStr(pPis));
              submit_cstpis(cstPis);

              {PIS ST}
              Submit_BC_PIS_ST(CurrToStr(PBC_PIS_ST_produto));
              Submit_VALOR_PIS_ST(CurrToStr(PValorPIS_ST_Produto));

              {COFINS}
              Submit_BC_COFINS(CurrToStr(PBC_COFINS_produto));
              Submit_VALOR_COFINS(CurrToStr(PvalorCOFINS_PRODUTO));
              submit_percentualcofins(CurrToStr(pCofins));
              submit_cstcofins(cstCofins);

              {COFINS ST}
              Submit_BC_COFINS_ST(CurrToStr(PBC_COFINS_ST_produto));
              Submit_VALOR_COFINS_ST(CurrToStr(PVALORCofins_ST_Produto));

              Submit_VALORFRETE(CurrToStr(PvalorFreteProduto));
              Submit_VALORSEGURO(CurrToStr(PvalorSeguroProduto));

              if (calculoDifal) then
              begin
                CalculaDifal(uf_destinatario,TMPBASEC_ICMS_PRODUTO,PERCENTUAL_ICMS_UF_DEST,
                            VALOR_ICMS_UF_DEST,VALOR_ICMS_UF_REMET,PERCENTUAL_ICMS_INTER,
                            PERCENTUAL_ICMS_INTER_PART);


                //DIFAL
                set_PERCENTUAL_ICMS_UF_DEST    ( CurrToStr  (PERCENTUAL_ICMS_UF_DEST) );
                set_VALOR_ICMS_UF_DEST         ( CurrToStr  (VALOR_ICMS_UF_DEST) );
                set_VALOR_ICMS_UF_REMET        ( CurrToStr  (VALOR_ICMS_UF_REMET) );
                set_PERCENTUAL_ICMS_INTER      ( CurrToStr  (PERCENTUAL_ICMS_INTER) );
                set_PERCENTUAL_ICMS_INTER_PART ( CurrToStr  (PERCENTUAL_ICMS_INTER_PART) );
                set_BC_ICMS_UF_DEST            ( CurrToStr  (TMPBASEC_ICMS_PRODUTO) );

              end;


              if (PnfeDigitada = '') then
                Submit_Desconto(formata_valor(DESCONTO));

              if (Salvar(false)=false) then
              Begin
                mensagemerro('Erro na tentativa de editar o VIDRO com os valores dos impostos');
                exit;
              End;

            end;

            next;

          End;

          DESCONTO:=StrToCurr(tira_ponto(ValorDesconto));

          if (PnfeDigitada = '') then
          begin
            AjustaDescontos(DESCONTO,CodigoVenda);
            if not(VendadeProjeto) then
              AjustaAcrescimos(StrToCurr(get_campoTabela('SUM(VALORACRESCIMO) AS VALORACRESCIMO', 'pedido', 'tabpedido_proj', CodigoVenda,'VALORACRESCIMO')),CodigoVenda);
          end;


        end;
        
     finally
      FreeAndNil(query);
      MateriaisVenda.free;
     end;

   except
     on e:Exception do
     begin
      ShowMessage(e.Message);
      Exit;
     end;
   end;

   result:=True;

end;

procedure TObjNotafiscalObjetos.ImprimeNotaFiscal(Pnota:string);
var
  EspacoInicial,TamanhoCampo,EspacoCampo,cont,cont3,quant_campos_produto:Integer;

  Pcfop,PNaturezaOperacao,Temp,NomeCampo,Linha:String;
  Qtemp:Tibquery;
  Pisento,PSubstituicao,Paliquota:String;
  temp2:String;
  StrStringList:TStringList;
  TrocaIcmsSubstituicao_recolhido:boolean;

Begin
     //Se quiser ver como era antes pegue a funacao ImprimeNFRDPRINT_nova

  Try
         Qtemp:=Tibquery.create(nil);
         Qtemp.database:=Self.Objquery.database;
  Except
           messagedlg('Erro na cria��o de objetos!',mterror,[mbok],0);
  End;

  Try
        StrStringList:=TStringList.Create;
  Except
           MensagemErro('Erro na Cria��o da StringList');
           freeandnil(qtemp);
  end;

  Try//try finally da QTemp e da StrStringList

     If (Pnota='')
     Then Begin
               MensagemAviso('Escolha uma Nota Fiscal');
               exit;
     End;

     //Localizando a notafiscal
     If (Self.ObjNotaFiscalCFOP.NOTAFISCAL.LocalizaCodigo(Pnota)=False)
     Then Begin
              Messagedlg('Nota Fiscal n�o Localizada!',mterror,[mbok],0);
              exit;
     End;
     Self.ObjNotaFiscalCFOP.NotaFiscal.TabelaparaObjeto;

     {AtualizavaloresTotaisNotaFiscal(Pnota);


     //Calculando o peso
     self.CalculaPesoNovaForma(pnota); }


     //Localiza a NF
     If (Self.ObjNotaFiscalCFOP.NOTAFISCAL.LocalizaCodigo(Pnota)=False)
     Then Begin
              Messagedlg('Nota Fiscal n�o Localizada!',mterror,[mbok],0);
              exit;
     End;
     Self.ObjNotaFiscalCFOP.NotaFiscal.TabelaparaObjeto;

     //Agora o CFOP � por produto
     pCfop:='';
     With Qtemp do
     Begin
          close;
          SQL.clear;
          sql.Add('select * from viewprodutosvenda where notafiscal='+Pnota);
          open;

          StrStringList.clear;
          While not(Qtemp.eof) do
          Begin
               //***************ICMS DESTINO***************
               //Pega o imposto icms destino que esta guradado na tabmateriaisvenda
               if (ObjVidro_NF.IMPOSTO_ICMS_DESTINO.LocalizaCodigo(Qtemp.fieldbyname('imposto_icms_DESTINO').asstring)=False)
               Then Begin
                      MensagemErro('O Imposto ICMS DESTINO do produto '+Self.Objquery.fieldbyname('material').asstring+'-'+Self.Objquery.fieldbyname('codigoproduto').asstring+#13+
                                   'N�o foi encontrado');
                      exit;
               End;
               ObjVidro_NF.IMPOSTO_ICMS_DESTINO.TabelaparaObjeto;

               //Procuro se o CFOP j� n�o existe na lista
               {if (StrStringList.IndexOf(objvidro_nf.IMPOSTO_ICMS_DESTINO.CFOP.Get_CODIGO)<0)
               Then StrStringList.add(objvidro_nf.IMPOSTO_ICMS_DESTINO.CFOP.Get_CODIGO); }

               if (StrStringList.IndexOf(fieldbyname('CFOP').AsString)<0)
               Then StrStringList.add(fieldbyname('CFOP').AsString);


               Qtemp.next;
          End;

          Pcfop:='';

          for cont:=0 to StrStringList.Count-1 do
          Begin
               if (cont>0)
               Then Pcfop:=Pcfop+'/';

               Pcfop:=Pcfop+StrStringList[cont];
          End;

     End;



     With FRelNotaFiscalRdPrint do
     Begin

          //***********Recuperando a configura��o de cada label*****************
          CarregaConfiguracoesGerais;
          ComponenteRdPrint.Abrir;

          if (ComponenteRdPrint.Setup=False)
          Then Begin
                    ComponenteRdPrint.Fechar;
                    Exit;
          End;

          Try
             //**************************************CABE�ALHO NF*****************************************************
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbcfop.top/5))),strtoint(floattostr(int(lbcfop.left/5))),completapalavra(Pcfop,lbcfop.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbsaidaentrada.top/5))),strtoint(floattostr(int(lbsaidaentrada.left/5))),completapalavra('X',lbsaidaentrada.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbNaturezaOperacao.top/5))),strtoint(floattostr(int(lbNaturezaOperacao.left/5))),completapalavra(Self.ObjNotaFiscalCfop.NOTAFISCAL.Get_NATUREZAOPERACAO,lbNaturezaOperacao.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbControleSup.top/5))),strtoint(floattostr(int(lbControleSup.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Numero,lbControleSup.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbControleInf.top/5))),strtoint(floattostr(int(lbControleInf.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Numero,lbControleInf.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbDataEmissao.top/5))),strtoint(floattostr(int(lbDataEmissao.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_dataemissao,lbDataEmissao.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbDataSaida.top/5))),strtoint(floattostr(int(lbDataSaida.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.get_datasaida,lbDataSaida.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbHoraSaida.top/5))),strtoint(floattostr(int(lbHoraSaida.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.get_Horasaida,lbHoraSaida.tag,' '));
             //***********************************DADOS DO CLIENTE****************************************************
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbNomeDestinatRemete.top/5))),strtoint(floattostr(int(lbNomeDestinatRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Nome,lbNomeDestinatRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbCNPJDestRemete.top/5))),strtoint(floattostr(int(lbCNPJDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Cpf_CGC,lbCNPJDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbEnderecoDestRemete.top/5))),strtoint(floattostr(int(lbEnderecoDestRemete.left/5))),completapalavra(self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Endereco+','+self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Numero,lbEnderecoDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbBairroDestRemete.top/5))),strtoint(floattostr(int(lbBairroDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Bairro,lbBairroDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbCepDestRemete.top/5))),strtoint(floattostr(int(lbCepDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Cep,lbCepDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbMunicipioDestRemete.top/5))),strtoint(floattostr(int(lbMunicipioDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Cliente.Get_Cidade,lbMunicipioDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbFoneFaxDestRemete.top/5))),strtoint(floattostr(int(lbFoneFaxDestRemete.left/5))),completapalavra(self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Fone,lbFoneFaxDestRemete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbUFDestRemete.top/5))),strtoint(floattostr(int(lbUFDestRemete.left/5))),completapalavra(self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Estado,lbUFDestRemete.tag,' '));

             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbIeDestRemete.top/5))),strtoint(floattostr(int(lbIeDestRemete.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.cliente.Get_Rg_Ie,lbIeDestRemete.tag,' '));

             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura1.top/5))),strtoint(floattostr(int(lbVencimentofatura1.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura1,lbVencimentofatura1.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura2.top/5))),strtoint(floattostr(int(lbVencimentofatura2.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura2,lbVencimentofatura2.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura3.top/5))),strtoint(floattostr(int(lbVencimentofatura3.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura3,lbVencimentofatura3.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura4.top/5))),strtoint(floattostr(int(lbVencimentofatura4.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura4,lbVencimentofatura4.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbVencimentofatura5.top/5))),strtoint(floattostr(int(lbVencimentofatura5.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_vencimentofatura5,lbVencimentofatura5.tag,' '));

             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura1.top/5))),strtoint(floattostr(int(lbvalorfatura1.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura1),lbvalorfatura1.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura2.top/5))),strtoint(floattostr(int(lbvalorfatura2.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura2),lbvalorfatura2.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura3.top/5))),strtoint(floattostr(int(lbvalorfatura3.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura3),lbvalorfatura3.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura4.top/5))),strtoint(floattostr(int(lbvalorfatura4.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura4),lbvalorfatura4.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbvalorfatura5.top/5))),strtoint(floattostr(int(lbvalorfatura5.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.get_valorfatura5),lbvalorfatura5.tag,' '));

             //**************************************ICMS  E OUTROS***************************************************************************
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbbasecalculo.top/5))),strtoint(floattostr(int(lbbasecalculo.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_BaseCalculoICms),lbbasecalculo.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValoricms.top/5))),strtoint(floattostr(int(lbValoricms.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_VALORICMS),lbValoricms.tag,' '));

             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbBaseCalculoICMSSubstituicao.top/5))),strtoint(floattostr(int(lbBaseCalculoICMSSubstituicao.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_BASECALCULOICMS_SUBST_recolhido),lbBaseCalculoICMSSubstituicao.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorICMSSubstituicao.top/5))),strtoint(floattostr(int(lbValorICMSSubstituicao.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_VALORICMS_SUBST_recolhido),lbValorICMSSubstituicao.tag,' '));


             ComponenteRdPrint.Imp(strtoint(floattostr(int(ValorTotalProdutos.top/5))),strtoint(floattostr(int(ValorTotalProdutos.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_ValorTotal),ValorTotalProdutos.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorFrete.top/5))),strtoint(floattostr(int(lbValorFrete.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_ValorFrete),lbValorFrete.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorSeguro.top/5))),strtoint(floattostr(int(lbValorSeguro.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_ValorSeguro),lbValorSeguro.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbOutrasDespesas.top/5))),strtoint(floattostr(int(lbOutrasDespesas.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_OutrasDespesas),lbOutrasDespesas.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorTotalIPI.top/5))),strtoint(floattostr(int(lbValorTotalIPI.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_ValorTotalIPI),lbValorTotalIPI.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbValorTotalNota.top/5))),strtoint(floattostr(int(lbValorTotalNota.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_VALORfinal),lbValorTotalNota.tag,' '));

             //***********************************TRANSPORTADORA*************************************************************************
             if (Self.ObjNotaFiscalCFOP.notafiscal.Transportadora.Get_CODIGO='')
             Then Begin
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbNomeTransportadora.top/5))),strtoint(floattostr(int(lbNomeTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_NomeTransportadora,LbNomeTransportadora.Tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbFreteporcontaTransportadora.top/5))),strtoint(floattostr(int(lbFreteporcontaTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_FreteporContaTransportadora,lbFreteporcontaTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbPlacaVeiculoTransportadora.top/5))),strtoint(floattostr(int(lbPlacaVeiculoTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_PlacaVeiculoTransportadora,lbPlacaVeiculoTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbUFTransportadora.top/5))),strtoint(floattostr(int(lbUFTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_UFVeiculoTransportadora,lbUFTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbCnpjTransportadora.top/5))),strtoint(floattostr(int(lbCnpjTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_CNPJTransportadora,lbCnpjTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbEnderecoTransportadora.top/5))),strtoint(floattostr(int(lbEnderecoTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_EnderecoTransportadora,lbEnderecoTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbMunicipioTransportadora.top/5))),strtoint(floattostr(int(lbMunicipioTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_MunicipioTransportadora,lbMunicipioTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbUFEnderecoTransportadora.top/5))),strtoint(floattostr(int(lbUFEnderecoTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_UFTransportadora,lbUFEnderecoTransportadora.tag,' '));
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(lbIETransportadora.top/5))),strtoint(floattostr(int(lbIETransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_IETransportadora,lbIETransportadora.tag,' '));
             End;

             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbEspecieTransportadora.top/5))),strtoint(floattostr(int(lbEspecieTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Especie,lbEspecieTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbMarcaTransportadora.top/5))),strtoint(floattostr(int(lbMarcaTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Marca,lbMarcaTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbNumeroTransportadora.top/5))),strtoint(floattostr(int(lbNumeroTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_NumeroVolumes,lbNumeroTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbQuantidadeTransportadora.top/5))),strtoint(floattostr(int(lbQuantidadeTransportadora.left/5))),completapalavra(Self.ObjNotaFiscalCFOP.notafiscal.Get_Quantidade,lbQuantidadeTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbPesoBrutoTransportadora.top/5))),strtoint(floattostr(int(lbPesoBrutoTransportadora.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_PesoBruto),lbPesoBrutoTransportadora.tag,' '));
             ComponenteRdPrint.Imp(strtoint(floattostr(int(lbPesoLiquidoTransportadora.top/5))),strtoint(floattostr(int(lbPesoLiquidoTransportadora.left/5))),completapalavra(formata_valor(Self.ObjNotaFiscalCFOP.notafiscal.Get_PesoLiquido),lbPesoLiquidoTransportadora.tag,' '));
             
             // *****************DADOS DO PRODUTO*******************************
             If  (FrelnotafiscalRdPrint.PegaCampoIni('QUANTIDADECAMPOS',Temp)=False)
             Then exit;
             
             Try
                quant_campos_produto:=strtoint(temp);
             Except
                   Messagedlg('Erro na Valor do Campo "QUANTIDADECAMPOS" do arquivo INI',mterror,[mbok],0);
                   exit;
             End;

             //*********pegando a quantidade de caracteres no espaco inicial*********
             If(FrelnotafiscalRdPrint.PegaCampoIni('ESPACOINICIAL',Temp)=False)
             Then exit;

             Try
                EspacoInicial:=strtoint(temp);
             Except
                   Messagedlg('Erro na Valor do Campo "ESPACOINICIAL" do arquivo INI',mterror,[mbok],0);
                   exit;
             End;

             With Qtemp do
             Begin

                  close;
                  SQL.clear;
                  SQL.add('Select  * from viewprodutosvenda where notafiscal='+Pnota);
                  open;

                  If (Recordcount=0)
                  Then Begin
                            Messagedlg('N�o h� produtos cadastrados para esta NF!',mterror,[mbok],0);
                            exit;
                  End;

                  first;
                  cont3:=0;
                  //esse while percorre produto a produto colocado na nf
                  While not(eof) do
                  Begin
                       //***************ICMS ORIGEM***************
                       if (ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.LocalizaCodigo(Qtemp.fieldbyname('imposto_icms_origem').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto ICMS Origem do produto '+Qtemp.fieldbyname('material').asstring+'-'+Qtemp.fieldbyname('codigoproduto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_ICMS_ORIGEM.TabelaparaObjeto;

                       //***************ICMS DESTINO***************
                       if (ObjVidro_NF.IMPOSTO_ICMS_DESTINO.LocalizaCodigo(Qtemp.fieldbyname('imposto_icms_DESTINO').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto ICMS DESTINO do produto '+Qtemp.fieldbyname('material').asstring+'-'+Qtemp.fieldbyname('codigoproduto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_ICMS_DESTINO.TabelaparaObjeto;

                       //**************************PIS ORIGEM**********************************
                       if (ObjVidro_NF.IMPOSTO_PIS_ORIGEM.LocalizaCodigo(Qtemp.fieldbyname('imposto_PIS_origem').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto PIS Origem do produto '+Qtemp.fieldbyname('material').asstring+'-'+Qtemp.fieldbyname('codigoproduto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_PIS_ORIGEM.TabelaparaObjeto;
                  
                       //***************PIS DESTINO***************
                       if (ObjVidro_NF.IMPOSTO_PIS_DESTINO.LocalizaCodigo(Qtemp.fieldbyname('imposto_PIS_DESTINO').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto PIS DESTINO do produto '+Qtemp.fieldbyname('material').asstring+'-'+Qtemp.fieldbyname('codigoproduto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_PIS_DESTINO.TabelaparaObjeto;

                  
                       //**************************COFINS ORIGEM**********************************
                       if (ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.LocalizaCodigo(Qtemp.fieldbyname('imposto_COFINS_origem').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto COFINS Origem do produto '+Qtemp.fieldbyname('material').asstring+'-'+Qtemp.fieldbyname('codigoproduto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_COFINS_ORIGEM.TabelaparaObjeto;

                       //***************COFINS DESTINO***************
                       if (ObjVidro_NF.IMPOSTO_COFINS_DESTINO.LocalizaCodigo(Qtemp.fieldbyname('imposto_COFINS_DESTINO').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto COFINS DESTINO do produto '+Qtemp.fieldbyname('material').asstring+'-'+Qtemp.fieldbyname('codigoproduto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_COFINS_DESTINO.TabelaparaObjeto;

                       //**************IPI****************
                        if (ObjVidro_NF.IMPOSTO_IPI.LocalizaCodigo(Qtemp.fieldbyname('imposto_IPI').asstring)=False)
                       Then Begin
                              MensagemErro('O Imposto IPI do produto '+Qtemp.fieldbyname('material').asstring+'-'+Qtemp.fieldbyname('codigoproduto').asstring+#13+
                                           'N�o foi encontrado');
                              exit;
                       End;
                       ObjVidro_NF.IMPOSTO_IPI.TabelaparaObjeto;
                       //*********************************************************
                  
                       linha:=' ';
                       linha:=CompletaPalavra(' ',EspacoInicial,' ');

                       for cont:=1 to quant_campos_produto do
                       Begin
                           //***************************************************

                            //pega o nome do campo
                           If (FrelNotaFiscalRdPrint.PegaCampoIni('CAMPO'+INTTOSTR(CONT),TEMP)=False)
                           Then exit;

                           NomeCampo:=temp;

                           //pega a quant. caracteres do campo
                           If (FrelNotaFiscalRdPrint.pegacampoini(NomeCampo,temp)=False)
                           Then exit;

                           try
                              tamanhocampo:=strtoint(temp);
                           Except
                                 Messagedlg('Erro no valor do Tamanho do Campo->'+NomeCampo,mterror,[mbok],0);
                                 exit;
                           End;

                           //espacocampo
                           //pega a quant. caracteres espaco do campo
                           If (FrelNotaFiscalRdPrint.pegacampoini('ESPACO'+NomeCampo,temp)=False)
                           Then exit;
                           Try
                              espacocampo:=strtoint(temp);
                           Except
                                 Messagedlg('Erro no valor do Tamanho do Espa�o do Campo->'+NomeCampo,mterror,[mbok],0);
                                 exit;
                           End;

                           //Ja tenho o nome do campo, tamanho e o espaco
                           //CAMPOS QUE PODEM SER USADOS
                           //CODIGOPRODUTO
                           //DESCRICAOPRODUTO
                           //CLASSIFICACAOFISCAL
                           //SITUACAOTRIBUTARIA
                           //UNIDADE
                           //QUANTIDADE
                           //VALOR
                           //VALORFINAl
                           //ALIQUOTAICMS

                           If (nomecampo='CODIGOPRODUTO')
                           Then Begin
                                    linha:=linha+CompletaPalavra(Fieldbyname('codigoproduto').asstring,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           If (nomecampo='DESCRICAOPRODUTO')
                           Then linha:=linha+CompletaPalavra(fieldbyname('descricao').asstring,tamanhocampo-20,' ')+'-'+CompletaPalavra(fieldbyname('cor').asstring,19,' ')+completapalavra(' ',espacocampo,' ');

                           If (nomecampo='CLASSIFICACAOFISCAL')
                           Then Begin
                                     linha:=linha+CompletaPalavra(fieldbyname('classificacaofiscal').asstring,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           If (nomecampo='SITUACAOTRIBUTARIA')
                           Then Begin
                                     linha:=linha+CompletaPalavra(ObjVidro_NF.IMPOSTO_ICMS_DESTINO.STA.Get_CODIGO+AdicionaZero(ObjVidro_NF.IMPOSTO_ICMS_DESTINO.STB.Get_CODIGO),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           If (nomecampo='UNIDADE')
                           Then Begin
                                     linha:=linha+CompletaPalavra(fieldbyname('unidade').asstring,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;
             
                           If(nomecampo='QUANTIDADE')
                           Then Begin
                                     linha:=linha+CompletaPalavra_a_Esquerda(Fieldbyname('Quantidade').asstring,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           If(nomecampo='VALORUNITARIO')
                           Then Begin
                                     linha:=linha+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('VALORUNITARIO').asstring),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           if (nomecampo='VALORTOTAL')
                           Then Begin
                                     linha:=linha+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('VALORFINAL').asstring),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')
                           End;

                           if (nomecampo='IPI')
                           Then Begin
                                     linha:=linha+CompletaPalavra_a_Esquerda(formata_valor(ObjVidro_NF.IMPOSTO_IPI.Get_Aliquota),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ');
                           End;

                           If (nomecampo='ALIQUOTAICMS')
                           Then Begin
                                     linha:=linha+CompletaPalavra(ObjVidro_NF.IMPOSTO_ICMS_DESTINO.Get_ALIQUOTA,tamanhocampo,' ')+completapalavra(' ',espacocampo,' ');
                           End;

                           if (nomecampo='VALORIPI')
                           Then linha:=linha+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR_IPI').asstring),tamanhocampo,' ')+completapalavra(' ',espacocampo,' ')

                           //***************************************************************
                       End;//for dos campos
                       ComponenteRdPrint.Imp(strtoint(floattostr(int(MEMOPRODUTOS.top/5)))+cont3,strtoint(floattostr(int(MEMOPRODUTOS.left/5))),completapalavra(linha,MEMOPRODUTOS.tag,' '));
                       inc(cont3,1);
                       next;
                  End;//while dos produtos

             
                  //******************DESCONTO**********************************
                  If(strtofloat(Self.ObjNotaFiscalCfop.NotaFiscal.get_desconto)>0)
                  Then Begin//tem Desconto Geral
                            LINHA:='';
                            //ANTES DO DESCONTO
                            If (FrelNotaFiscalRdPrint.pegacampoini('ANTESDESCONTO',temp)=False)
                            Then exit;
                            Try
                               espacocampo:=strtoint(temp);
                            Except
                                  Messagedlg('Erro no valor do Tamanho do Espa�o Antes do Desconto ',mterror,[mbok],0);
                                  exit;
                            End;
                            linha:=linha+CompletaPalavra(' ',espacocampo,' ');
                            //desconto
                            If (FrelNotaFiscalRdPrint.pegacampoini('DESCONTO',temp)=False)
                            Then exit;

                            Try
                               espacocampo:=strtoint(temp);
                            Except
                                  Messagedlg('Erro no valor do Tamanho do Espa�o Antes do Desconto ',mterror,[mbok],0);
                                  exit;
                            End;
                            linha:=linha+CompletaPalavra('DESCONTO ',espacocampo,' ');

                            //espaco antes do valor do desconto
                            If (FrelNotaFiscalRdPrint.pegacampoini('DEPOISDESCONTO',temp)=False)
                            Then exit;

                            Try
                               espacocampo:=strtoint(temp);
                            Except
                                  Messagedlg('Erro no valor do Tamanho do Espa�o Antes do Desconto ',mterror,[mbok],0);
                                  exit;
                            End;
                            linha:=linha+CompletaPalavra(' ',espacocampo,' ');
                            //valor do desconto
                            linha:=linha+formata_valor(Self.ObjNotaFiscalCfop.NotaFiscal.get_Desconto);
                            ComponenteRdPrint.Imp(strtoint(floattostr(int(MEMOPRODUTOS.top/5)))+cont3,strtoint(floattostr(int(MEMOPRODUTOS.left/5))),completapalavra(linha,MEMOPRODUTOS.tag,' '));
                  End;
                  
                 //**************DADOS ADICIONAIS*******************************
                 StrStringList.Clear;
                 StrStringList.text:=Self.ObjNotaFiscalCfop.NOTAFISCAL.Get_DadosAdicionais;
                 cont3:=0;
                 for cont:=0 to StrStringList.Count-1 do
                 Begin

                      if (length(StrStringList[cont])>MEMOINFORMACOESCOMPLEMENTARES.tag)
                      Then Begin
                                //a linha digitada nao cabe numa unica linha
                                //tenho que quebrar em varias
                                //************************************************
                                Temp2:=StrStringList[cont];
                                While (temp2<>'') do
                                Begin
                                     linha:=temp2;

                                     DividirValorCOMSEGUNDASEMTAMANHO(linha,MEMOINFORMACOESCOMPLEMENTARES.tag,temp,temp2);

                                     if (length(temp2)>1)
                                     Then Begin
                                               if (temp2[1]=' ')
                                               Then temp2:=copy(temp2,2,length(temp2)-1);
                                     End;

                                     if (temp<>'')
                                     Then Begin
                                               componenteRdPrint.Imp(strtoint(floattostr(int(MEMOINFORMACOESCOMPLEMENTARES.top/5)))+cont3,strtoint(floattostr(int(MEMOINFORMACOESCOMPLEMENTARES.left/5))),completapalavra(temp,MEMOINFORMACOESCOMPLEMENTARES.tag,' '));
                                               inc(cont3,1);
                                     End;
                                End;
                                //**************************************************
                      End
                      Else Begin//cabe numa linha apenas
                                componenteRdPrint.Imp(strtoint(floattostr(int(MEMOINFORMACOESCOMPLEMENTARES.top/5)))+cont3,strtoint(floattostr(int(MEMOINFORMACOESCOMPLEMENTARES.left/5))),completapalavra(StrStringList[cont],MEMOINFORMACOESCOMPLEMENTARES.tag,' '));
                                inc(cont3,1);
                      End;
                 End;//for cont:=0...
                 //*************************************************************

             End;//with Qtemp

          Finally
             componenterdprint.fechar;
          End;
     End;//With FreltxtRdPrint

  Finally
     freeandnil(Qtemp);
  End;

End;

//NOVA FORMA DE GERAR A NOTA FISCAL
//N�o utiliza mais as tabelas tabferragem_nf,tabvidro_nf,tabdiverso_nf,taperfilado_nf,tabpersiana_nf,tabkitbox_nf
//Para realizar os calculos, usa a tabmateriaisvenda, e atualiza essas tabelas apenas pra ter um controle de NF's
{procedure TObjNotafiscalObjetos.GeraNotaFiscal(PnotaFiscal:string;Pedido:string;Operacao:string;valorpedido:string);
var
ObjPedidoObjetos:TObjPedidoObjetos;
ppedido:string;
Query:TIBQuery;

//MateriaisVenda:TObjMateriaisVenda;
begin

     if (PnotaFiscal='')
     then Begin
               mensagemaviso('Escolha a Nota Fiscal');
               exit;
     End;


     //Localizinado uma nota fiscal para ser emitida
     if (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(PnotaFiscal)=False)
     then Begin
               MensagemErro('Nota Fiscal n�o encontrada');
               exit;
     End;

     //carregando as informa��es que ja est�o na nota
     Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;

     try
        ObjPedidoObjetos:=TObjPedidoObjetos.Create;
     Except
           on e:exception do
           begin
                Mensagemerro('Erro na tentativa OBJPEDIDOOBJETOS'+#13+E.message);
                exit;
           End;
     End;

     try
        Query:=TIBQuery.Create(nil);
        Query.Database:=FDataModulo.IBDatabase;

     except

     end;


     Try
        //Procurando o pedido
        if (ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(pedido)=False)
        then Begin
                  MensagemErro('Pedido n�o localizado');
                  exit;
        End;
        //ao encontrar o pedido, carrega as informa��es do mesmo para o objeto
        ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
        ppedido:=ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Codigo;

        //procura a opera��o na tabela de opera��es (venda/compra/devolu��o)
        //lembrando que os cfop s�o passados automaticamente por parametro
        //por exemplo, opera��o de venda = cfop tal( valor que esta no parametro)
        if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.LocalizaCodigo(Operacao)=False)
        then Begin
                  MensagemErro('Opera��o n�o localizada');
                  exit;
        End;
        //carregando dados da tabopera��o para o objeto
        Self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.TabelaparaObjeto;

        //passando as informa��es para objnotafiscal para que a nota seja montanda
        Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;
        Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_dataemissao(datetostr(now));
        Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_datasaida(datetostr(now));
        Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Horasaida(timetostr(now));
        Self.ObjNotaFiscalCfop.NOTAFISCAL.Cliente.Submit_Codigo(ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Codigo);
        self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_NATUREZAOPERACAO(self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.Get_NOME);
        Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORTOTAL(valorpedido);
        self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_NumPedido(ppedido);
        self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('I');


        //salvando dados na tabnotafiscal
        if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(True)=False)
        then Begin
                  Mensagemerro('Erro na tentativa de Salvar o Cliente na Nota Fiscal');
                  exit;
        End;

        if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.Get_CODIGO='')
        then Begin
                  mensagemerro('Escolha uma opera��o para a nota fiscal');
                  exit;
        End;

        //Grava na tabmateriasvenda o numero da nota fiscal para cada produto(material) na venda
        with Query do
        begin
              Close;
              SQL.Clear;
              sql.Add('Update tabmateriaisvenda set notafiscal ='+PnotaFiscal);
              SQL.Add('Where pedido='+Pedido);
              ExecSQL;
              FDataModulo.IBTransaction.CommitRetaining;

        end;


        Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(PnotaFiscal);
        Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
        Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;
        Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_DESCONTO(ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorDesconto);

        if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(True)=False) then
        Begin
            Mensagemerro('Erro na tentativa de Salvar o desconto na Nota Fiscal');
            Self.LimpaNF(PnotaFiscal);
            exit;
        end;



     Finally

        FDataModulo.IBTransaction.RollbackRetaining;
        ObjPedidoObjetos.Free;
        FreeAndNil(Query);
        
     End;


end;  }

{ TODO : gera NF }
function TObjNotafiscalObjetos.GeraNotaFiscalNova(PnotaFiscal:TStringList;pedido:string;operacao:string;Corte:Integer;pNfeDigitada:string):Boolean;
var
  ObjPedidoObjetos:TObjPedidoObjetos;
  objNfeDigitada:TObjNFEDIGITADA;
  ppedido:string;
  Query:TIBQuery;
  Query2:TIBQuery;
  i,cont,i2,cont3:Integer;
  ListaDeValorTotal:TStringList;
  ListaDeProdutos:TStringList;
  ListadeValorTotalDesconto:TStringList;
  Valor, Desconto:Currency;
  formEscolheTransportadora:TFEscolheTransportadora;
  GerarPorProjetos:string;
  DadosAdicionais:string;
  alteraFaturas:TfAlteraFaturas;
  pCodigoCobrFatura,pCodigoDup:string;
  contador:integer;
  indpag:string;
begin

  result:=false;
  
  try
  
    try


      ListaDeValorTotal:=TStringList.Create;
      ListaDeProdutos:=TStringList.Create;
      ListadeValorTotalDesconto:=TStringList.Create;
      Query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;


      valor:=0;
      cont:=0;
      desconto:=0;

      if(ObjParametroGlobal.ValidaParametro('VENDER PROJETOS COMO PRODUTOS')=false) then
      begin
              MensagemErro('Paramentro "VENDER PROJETOS COMO PRODUTOS" n�o encontrado');
              GerarPorProjetos:='NAO';
      end
      else GerarPorProjetos:=ObjParametroGlobal.Get_Valor;

      with (Query) do
      begin

        active:=false;
        SQL.Clear;

        if (PnfeDigitada = '') then
        begin
            if(GerarPorProjetos='NAO') then
            begin
                sql.Add('select * from viewprodutosvenda where pedido='+pedido) ;
                //SQL.Add('and codigoproduto is not null') ; //se for um projeto que esta gravado, n�o posso pegar ele
            end
            else
            begin
                 sql.Add('select * from viewprodutosvenda where pedido='+pedido) ;
                // SQL.Add('and codigoproduto is null') ; //esta pra gerar por projetos, ent�o pego o projeto e nao os materiais
            end;
        end
        else
        begin
            SQL.Add('select * from viewprodutosvenda where nfeDigitada = '+PnfeDigitada);
            sql.Add('and codigoproduto is not null'); //na NFE digitada somente fa�o por materiais... ver depois pra fazer na digitada
        end;

        sql.Add('order by codigo');
        active:=True;

        while not (Eof) do
        begin

          valor := valor + fieldbyname('valorfinal').AsCurrency;
          desconto := desconto + fieldbyname('desconto').AsCurrency;
          inc(cont,1);
          ListaDeProdutos.Add(fieldbyname('codigo').AsString);

          if(cont=Corte) then
          begin

            ListaDeValorTotal.Add(CurrToStr(Valor));
            ListadeValorTotalDesconto.Add(CurrToStr(Desconto));
            cont:=0;
            valor:=0;
            Desconto:=0;

          end;

          if (PnfeDigitada <> '')
          then indpag:=fieldbyname('indpag').AsString;

          Next;

        end;

        if( cont <> 0) then
        begin

          ListaDeValorTotal.Add(CurrToStr(Valor));
          ListadeValorTotalDesconto.Add(CurrToStr(Desconto));
          cont:=0;
          valor:=0;

        end;

      end;

      cont3:=corte;
      desconto:=0;

      if (cont3 > ListaDeProdutos.Count) then
        cont3:=ListaDeProdutos.count;

      for i:=0 to PnotaFiscal.Count-1 do
      begin


        {Localizando uma nota fiscal para ser emitida}
        if (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(PnotaFiscal[i]) = False) then
        Begin
          MensagemErro('Nota Fiscal n�o encontrada');
          exit;
        End;

        {carregando as informa��es que ja est�o na nota}
        if not Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto then
          Exit;

        if (PnfeDigitada = '') then
          ObjPedidoObjetos:=TObjPedidoObjetos.Create(Self.Owner)
        else
          objNfeDigitada:=TObjNFEDIGITADA.Create;


        Try

          if (PnfeDigitada = '') then
          begin

            {Procurando o pedido}
            if (ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(pedido)=False) then
            Begin
              MensagemErro('Pedido n�o localizado');
              exit;
            End;
        
            if not ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto then
              Exit;

            ppedido:=ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Codigo;

          end
          else
          begin

            {procurando a nfe digitada}
            if not (objNfeDigitada.LocalizaCodigo (PnfeDigitada)) then
            begin
              MensagemErro('Nfe digitada n�o encontrada');
              Exit;
            end;

            if not objNfeDigitada.TabelaparaObjeto then
              Exit;

          end;

          {procura a opera��o na tabela de opera��es (venda/compra/devolu��o)}
          {lembrando que os cfop s�o passados automaticamente por parametro}
          {por exemplo, opera��o de venda = cfop tal( valor que esta no parametro)}

          if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.LocalizaCodigo(Operacao)=False) then
          Begin
              MensagemErro('Opera��o n�o localizada');
              exit;
          End;

          {carregando dados da tabopera��o para o objeto}
          if not Self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.TabelaparaObjeto then
            Exit;

          {passando as informa��es para objnotafiscal para que a nota seja montanda}
          Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;
          Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_dataemissao(datetostr(now));
          Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_datasaida(datetostr(now));
          Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Horasaida(timetostr(now));
          self.ObjNotaFiscalCfop.NOTAFISCAL.submit_horaemissao(TimeToStr(now));

          if(indpag= '') then
            indpag :='2';

          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_indpag(indpag);

          if (PnfeDigitada = '') then
          begin

            Self.ObjNotaFiscalCfop.NOTAFISCAL.Cliente.Submit_Codigo(ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Codigo);
            self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_NumPedido(ppedido);
            DadosAdicionais:='';
            InputQuery('Dados adicionais na Nota','Dados adicionais na Nota',DadosAdicionais);
            self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_DadosAdicionais(ObjEmpresaGlobal.Get_FraseNFe+' '+DadosAdicionais);

          end
          else
          begin

            if (objNfeDigitada.CLIENTE.Get_Codigo <> '') then
              self.ObjNotaFiscalCfop.NOTAFISCAL.Cliente.Submit_Codigo(objNfeDigitada.CLIENTE.Get_Codigo)

            else if (objNfeDigitada.FORNECEDOR.Get_Codigo <> '') then
              self.ObjNotaFiscalCfop.NOTAFISCAL.Fornecedor.Submit_Codigo(objNfeDigitada.FORNECEDOR.Get_Codigo)

            else begin
              MensagemErro('Escolha um cliente ou fornecedor');
              Exit;
            end;

            //default
            self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_FreteporContaTransportadora('9');

            if ( objNfeDigitada.TRANSPORTADORA.Get_CODIGO <> '' ) then
            begin

                try

                  formEscolheTransportadora := TFEscolheTransportadora.Create (nil);

                  try

                    formEscolheTransportadora.edttransportadora.Text := objNfeDigitada.TRANSPORTADORA.Get_CODIGO ();
                    formEscolheTransportadora.edttransportadoraExit (formEscolheTransportadora.edttransportadora);
                    formEscolheTransportadora.get_dados (objNfeDigitada.TRANSPORTADORA.Get_CODIGO ());

                    formEscolheTransportadora.ShowModal;

                    if (formEscolheTransportadora.Tag = 1) then
                    begin

                      self.ObjNotaFiscalCfop.NOTAFISCAL.Transportadora.Submit_CODIGO       (formEscolheTransportadora.edttransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_NomeTransportadora          (formEscolheTransportadora.EdtNomeTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_EnderecoTransportadora      (formEscolheTransportadora.EdtEnderecoTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_CNPJTransportadora          (formEscolheTransportadora.EdtCNPJTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_MunicipioTransportadora     (formEscolheTransportadora.EdtMunicipioTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_FreteporContaTransportadora (formEscolheTransportadora.EdtFreteporContaTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_IETransportadora            (formEscolheTransportadora.EdtIETransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_UFTransportadora            (formEscolheTransportadora.EdtUFTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_PlacaVeiculoTransportadora  (formEscolheTransportadora.EdtPlacaVeiculoTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_UFVeiculoTransportadora     (formEscolheTransportadora.EdtUFVeiculoTransportadora.Text);

                      {volumes transportados}
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Quantidade    (formEscolheTransportadora.EdtQuantidade.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Especie       (formEscolheTransportadora.edtEspecie.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Marca         (formEscolheTransportadora.edtMarca.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_PesoBruto     (formEscolheTransportadora.edtPesoBruto.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_PesoLiquido   (formEscolheTransportadora.edtPesoLiquido.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Numerovolumes (formEscolheTransportadora.edtNumeracao.Text); {na nota fiscal do amanda este campo � 'numera��o'. aqui aproveitei o campo 'numeroVolumes'}

                    end;

                  finally

                    FreeAndNil (formEscolheTransportadora);

                  end;

                except

                  MensagemErro ('Erro ao recuperar dados da transportadora');
                  Exit;

                end;

            end;


            self.ObjNotaFiscalCfop.NOTAFISCAL.submit_NUMNFEDIGITADA(PnfeDigitada);
            self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_DadosAdicionais(ObjEmpresaGlobal.Get_FraseNFe+' '+objNfeDigitada.Get_INFORMACOES);

            if not (update_campoTabela('TABNFEDIGITADA','nota',PnotaFiscal[0],'codigo',PnfeDigitada)) then
              Exit;

            if(objNfeDigitada.get_indpag='1') then
            begin

                alteraFaturas:=TfAlteraFaturas.Create(nil);
                alteraFaturas.edtNumeroParcelas.Text := '1';
                alteraFaturas.data := StrToDate(objNfeDigitada.Get_DATAEMISSAO);
                alteraFaturas.edtValorNF.Text :=objNfeDigitada.Get_VALORNF;
                alteraFaturas.ShowModal;

                if alteraFaturas.Tag = 0 then
                  Exit;

                with CobrDuplicata.COBRFATURA do
                begin
                  ZerarTabela;
                  Status := dsInsert;
                  pCodigoCobrFatura := CobrDuplicata.COBRFATURA.Get_NovoCodigo;
                  Submit_Codigo(pCodigoCobrFatura);
                  Submit_nFat(pCodigoCobrFatura);
                  Submit_vOrig(objNfeDigitada.Get_VALORNF);
                  Submit_vDesc('0');
                  Submit_vLiq(objNfeDigitada.Get_VALORNF);
                  CobrDuplicata.COBRFATURA.notafiscal.Submit_CODIGO(PnotaFiscal[0]);
                  if not Salvar(false) then
                  begin
                    MensagemErro('Erro ao gravar Cobran�a fatura');
                    exit;
                  end
                end;

                for contador:=0 to alteraFaturas.listDatas.Items.Count-1 do
                begin

                  CobrDuplicata.ZerarTabela;
                  CobrDuplicata.Status := dsInsert;
                  pCodigoDup := CobrDuplicata.Get_NovoCodigo;
                  CobrDuplicata.Submit_Codigo(pCodigoDup);
                  CobrDuplicata.Submit_nDup(pCodigoDup);
                  CobrDuplicata.Submit_dVenc(alteraFaturas.listDatas.Items[contador]);
                  CobrDuplicata.Submit_vDup(alteraFaturas.ListValores.Items[contador]);
                  CobrDuplicata.COBRFATURA.Submit_Codigo(pCodigoCobrFatura);
                  if not CobrDuplicata.Salvar(false) then
                  begin
                    MensagemErro('Erro ao gravar Cobran�a duplicata');
                    exit;
                  end;

                end;

                FreeAndNil(alteraFaturas);
            end;

              Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_datasaida(objNfeDigitada.Get_DATASAIDA);
              self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_dataemissao(objNfeDigitada.Get_DATAEMISSAO);

          end;

          if ( (PnfeDigitada = '') and (escolheTransportadora) ) then
          begin

                try

                  formEscolheTransportadora := TFEscolheTransportadora.Create (nil);

                  try

                    formEscolheTransportadora.ShowModal;

                    if (formEscolheTransportadora.Tag = 1) then
                    begin

                      self.ObjNotaFiscalCfop.NOTAFISCAL.Transportadora.Submit_CODIGO       (formEscolheTransportadora.edttransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_NomeTransportadora          (formEscolheTransportadora.EdtNomeTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_EnderecoTransportadora      (formEscolheTransportadora.EdtEnderecoTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_CNPJTransportadora          (formEscolheTransportadora.EdtCNPJTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_MunicipioTransportadora     (formEscolheTransportadora.EdtMunicipioTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_FreteporContaTransportadora (formEscolheTransportadora.EdtFreteporContaTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_IETransportadora            (formEscolheTransportadora.EdtIETransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_UFTransportadora            (formEscolheTransportadora.EdtUFTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_PlacaVeiculoTransportadora  (formEscolheTransportadora.EdtPlacaVeiculoTransportadora.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_UFVeiculoTransportadora     (formEscolheTransportadora.EdtUFVeiculoTransportadora.Text);

                      {volumes transportados}
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Quantidade    (formEscolheTransportadora.EdtQuantidade.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Especie       (formEscolheTransportadora.edtEspecie.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Marca         (formEscolheTransportadora.edtMarca.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_PesoBruto     (formEscolheTransportadora.edtPesoBruto.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_PesoLiquido   (formEscolheTransportadora.edtPesoLiquido.Text);
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Numerovolumes (formEscolheTransportadora.edtNumeracao.Text); {na nota fiscal do amanda este campo � 'numera��o'. aqui aproveitei o campo 'numeroVolumes'}

                    end
                    else
                      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_FreteporContaTransportadora('9');

                  finally

                    FreeAndNil (formEscolheTransportadora);

                  end;

                except

                  MensagemErro ('Erro ao recuperar dados da transportadora');
                  Exit;

                end;

          end;


          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_NATUREZAOPERACAO(self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.Get_NOME);

          //27/11/2015
          //self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('I');

          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORTOTAL(ListaDeValorTotal[i]);
          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_DESCONTO((tira_ponto(formata_valor(ListadeValorTotalDesconto[i]))));



          {salvando dados na tabnotafiscal}
          if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(False)=False) then
          Begin
            Mensagemerro('Erro na tentativa de Salvar o Cliente na Nota Fiscal');
            exit;
          End;

          if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.Get_CODIGO='') then
          Begin
            mensagemerro('Escolha uma opera��o para a nota fiscal');
            exit;
          End;

          Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(PnotaFiscal[i]);
          Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
          Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;

          for i2:=cont to cont3 -1 do
          begin

            {Grava na tabmateriasvenda o numero da nota fiscal para cada produto(material) na venda}
            Query.Close;
            Query.SQL.Clear;
            Query.sql.Add('Update tabmateriaisvenda set notafiscal ='+PnotaFiscal[i]);
            Query.SQL.Add('Where codigo='+ListaDeProdutos[i2]);
            Query.ExecSQL;

            {desconto:=desconto+(StrToCurr(ListadeValorTotalDesconto[i2]))}
            {FDataModulo.IBTransaction.CommitRetaining;}

          end;

          desconto:=0;
          cont:=cont3;
          cont3:=cont3+corte;

          if(cont3>ListaDeProdutos.Count) then
            cont3:=ListaDeProdutos.count;


          if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(False)=False) then
          Begin

            Mensagemerro('Erro na tentativa de Salvar o desconto na Nota Fiscal');
            Self.LimpaNF(PnotaFiscal[i]);
            exit;

          End;

          {Somando os valores totais da nota}
          AtualizavaloresTotaisNotaFiscal(PnotaFiscal[i]);

          {Calculando o peso}
          self.CalculaPesoNovaForma(PnotaFiscal[i]);

        Finally

          if (ObjPedidoObjetos <> nil) then
            ObjPedidoObjetos.Free;

          if (objNfeDigitada <> nil) then
            objNfeDigitada.Free;

        End;

      end;

    except
      Exit;
    end;

    result:=True;

  finally
    if Assigned(Query) then
      freeandnil(Query);
  end;

end;

{ TODO : gera NF para nfe }
function TObjNotafiscalObjetos.GeraNotaFiscalParaNFE(PnotaFiscal:TStringList;pedido:string;operacao:string;Corte:Integer):Boolean;
var
 queryDup:TIBQuery;
 QuantidadeParcelas:Integer;
 pCLiente,pFornecedor,sta,stb,pnfe,pCodigoCobrFatura:string;
 objTransNFE:TObjTransmiteNFE;
 pModoContingencia:Boolean;
 calculatotaltributos:Boolean;
 fraseVtotTrib,infCpl,Frase_difal:string;
 valTotTrib,totalProdutos,percentualTotal,TOTAL_VALOR_ICMS_UF_DEST,TOTAL_VALOR_ICMS_UF_REMET:Currency;

begin

  result := False;

  queryDup:=TIBQuery.Create(nil);
  queryDup.Database:=FDataModulo.IBDatabase;

  if not (self.GeraNotaFiscalNova(PnotaFiscal,pedido,operacao,corte)) then
    Exit;

  if(ObjParametroGlobal.ValidaParametro('NFE calcula total tributos')=false)
  then calculatotaltributos:=False
  else
  begin
    if(ObjParametroGlobal.Get_Valor='SIM')
    then calculatotaltributos:=True
    else calculatotaltributos:=False;
  end;


  if (calculatotaltributos) then
  begin
    //alterando as informacoes complementares para vTotTrib > 0
     if ObjParametroGlobal.ValidaParametro('NFE FRASE TOTAL DOS TRIBUTOS') then
    fraseVtotTrib := ObjParametroGlobal.Get_Valor;
    
    if (Pos(':valor',fraseVtotTrib) = 0) or (Pos(':percentual',fraseVtotTrib) = 0) then
    begin
      MensagemErro('Parametro: "NFE FRASE TOTAL DOS TRIBUTOS" inv�lido');
      Exit;
    end;

    valTotTrib := StrToCurrDef(get_campoTabela('SUM(vTotTrib) as vTotTrib','pedido','tabmateriaisvenda',pedido,'vTotTrib'),0);
    totalProdutos := StrToCurrDef(get_campoTabela('SUM(valorfinal) as valorfinal','pedido','tabmateriaisvenda',pedido,'valorfinal'),0);
    percentualTotal := (valTotTrib*100)/totalProdutos;
    infCpl := '';
    infCpl := ' ';
    infCpl := StringReplace(fraseVtotTrib,':valor',formata_valor(CurrToStr(valTotTrib),2,false),[]);
    infCpl := StringReplace(infCpl,':percentual',formata_valor(CurrToStr(percentualTotal),2,false),[]);

    {if not exec_sql('update tabnfedigitada set informacoes = informacoes||'+QuotedStr(infCpl) + ' where codigo = '+pNfeDigitada) then
    begin
      MensagemErro('Erro ao alterar infCpl total dos tributos na tabnfedigitada');
      Exit;
    end; }
    
    if not exec_sql('update tabnotafiscal set dadosadicionais = '''' where dadosadicionais is null and codigo = '+PnotaFiscal[0]) then
    begin
      MensagemErro('Erro ao alterar dadosadicionais na tabnotafiscal');
      Exit;
    end;

    if not exec_sql('update tabnotafiscal set dadosadicionais = dadosadicionais||'+QuotedStr(infCpl)+' where codigo = '+PnotaFiscal[0]) then
    begin
      MensagemErro('Erro ao alterar infCpl total dos tributos na tabnotafiscal');
      Exit;
    end;
  end;

   TOTAL_VALOR_ICMS_UF_DEST  :=  StrToCurrDef( get_campoTabela('sum(VALOR_ICMS_UF_DEST) as soma','notafiscal','tabmateriaisvenda',PnotaFiscal[0],'soma') ,0);
   TOTAL_VALOR_ICMS_UF_REMET :=  StrToCurrDef( get_campoTabela('sum(VALOR_ICMS_UF_REMET) as soma','notafiscal','tabmateriaisvenda',PnotaFiscal[0],'soma') ,0);


  if (TOTAL_VALOR_ICMS_UF_DEST+TOTAL_VALOR_ICMS_UF_REMET > 0) then
  begin
    Frase_Difal := 'INFORMA��ES COMPLEMENTARES: Valores totais do ICMS Interestadual: DIFAL da UF destino R$:valordestino + FCP R$:valorfcp; DIFAL da UF Origem R$:valororigem.';
    Frase_Difal := StringReplace( Frase_Difal, ':valordestino', formata_valor(TOTAL_VALOR_ICMS_UF_DEST) ,[rfReplaceAll]);
    Frase_Difal := StringReplace( Frase_Difal, ':valorfcp',     formata_valor(0)                        ,[rfReplaceAll]);
    Frase_Difal := StringReplace( Frase_Difal, ':valororigem',  formata_valor(TOTAL_VALOR_ICMS_UF_REMET),[rfReplaceAll]);


    exec_sql('update tabnotafiscal set dadosadicionais = dadosadicionais || '+QuotedStr(' '+Frase_Difal) + 'where codigo='+PnotaFiscal[0]);

  end;

  {NF-e nova}
  try


    {todo: faturas NF-e}
    {gravando as duplicatas para nf-e}
    objTransNFE := TObjTransmiteNFE.Create(nil,FDataModulo.IBDatabase);
    queryDup.Active := False;
    queryDup.SQL.Text :=

    'select t.codigo as nfat, p.nome as nomefat, t.valor as vfat, d.codigo as ndup, d.vencimento as ddup, d.valor as vdup, p.parcelas '+
    'from tabpedido v '+
    'inner join tabtitulo t on (t.codigo = v.titulo) '+
    'inner join tabprazopagamento p on (p.codigo = t.prazo) '+
    'inner join tabpendencia d on (d.titulo = t.codigo) '+
    'where v.codigo = '+pedido+' '+
    'order by d.vencimento';

    queryDup.Active := True;
    QueryDup.Last;
    QuantidadeParcelas:=QueryDup.RecordCount;
    queryDup.First;

    with CobrDuplicata.COBRFATURA do
    begin
      ZerarTabela;
      Status := dsInsert;
      pCodigoCobrFatura := CobrDuplicata.COBRFATURA.Get_NovoCodigo;
      Submit_Codigo(pCodigoCobrFatura);
      Submit_nFat(queryDup.fieldbyname('nfat').AsString);
      Submit_vOrig(queryDup.fieldbyname('vfat').AsString);
      Submit_vDesc('0');
      Submit_vLiq(queryDup.fieldbyname('vfat').AsString);
      CobrDuplicata.COBRFATURA.notafiscal.Submit_CODIGO(PnotaFiscal[0]);

      if not Salvar(false) then
      begin
        MensagemErro('Erro ao gravar Cobran�a fatura');
        exit;
      end
    end;
    if QuantidadeParcelas> 1 then
    begin
      while not queryDup.Eof do
      begin
        CobrDuplicata.ZerarTabela;
        CobrDuplicata.Status := dsInsert;
        CobrDuplicata.Submit_Codigo('0');
        CobrDuplicata.Submit_nDup(queryDup.fieldbyname('ndup').AsString);
        CobrDuplicata.Submit_dVenc(queryDup.fieldbyname('ddup').AsString);
        CobrDuplicata.Submit_vDup(queryDup.fieldbyname('vdup').AsString);
        CobrDuplicata.COBRFATURA.Submit_Codigo(pCodigoCobrFatura);
        if not CobrDuplicata.Salvar(false) then
        begin
          MensagemErro('Erro ao gravar Cobran�a duplicata');
          exit;
        end;
        queryDup.Next;
      end;

      if not update_campoTabela('tabnotafiscal','indpag','1','codigo',PnotaFiscal[0]) then
        Exit;

    end
    else begin
       if not update_campoTabela('tabnotafiscal','indpag','0','codigo',PnotaFiscal[0]) then
        Exit;
    end;
   {end duplicatas}

   

    pModoContingencia:=False;

    ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(PnotaFiscal[0]);
    ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
    pnfe := ObjNotaFiscalCfop.NOTAFISCAL.Get_Numero;

    if verificaNotasAbertas(pnfe) then
      Exit;

    objTransNFE.pCodigoNF := StrToInt(PnotaFiscal[0]);
    objTransNFE.numeroNFE := pnfe;
    objTransNFE.TipoNF := 'SAIDA';
    objTransNFE.contingencia := pModoContingencia;
    {referenciado por cupom fiscal}
    objTransNFE.modeloecf := '2D';
    objTransNFE.nECF := get_campoTabela('sequenciaECF','codigo','TABPEDIDO',pedido);
    objTransNFE.nCOO := get_campoTabela('numeroCupom','codigo','TABPEDIDO',pedido);

    //FDataModulo.IBTransaction.CommitRetaining;
    //Result := True;
    //Exit;




    objTransNFE.Transmite;
    if objTransNFE.strErros.Count > 0 then
    begin
      ShowMessage(objTransNFE.strErros.Text);
      result := False;
    end
    else
    begin
      {editando a NF-e}
      ObjNotaFiscalCfop.NOTAFISCAL.Nfe.LocalizaCodigo(pnfe);
      ObjNotaFiscalCfop.NOTAFISCAL.Nfe.TabelaparaObjeto;
      ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Status := dsedit;
      ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Submit_CERTIFICADO  (ObjEmpresaGlobal.get_certificado);
      ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Submit_ReciboEnvio  (objTransNFE.retornaRecibo);
      ObjNotaFiscalCfop.NOTAFISCAL.Nfe.submit_chave_acesso (objTransNFE.retornaChaveAcesso);
      ObjNotaFiscalCfop.NOTAFISCAL.Nfe.submit_arquivo_xml  (objTransNFE.retornaArquivoXML);
      ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Submit_ARQUIVO      (objTransNFE.retornaArquivoXML);

      if (pModoContingencia=True) then
        ObjNotaFiscalCfop.NOTAFISCAL.nfe.Submit_STATUSNOTA('T')//contingencia
      else
        ObjNotaFiscalCfop.NOTAFISCAL.nfe.Submit_STATUSNOTA('G');//Gerada

      if objTransNFE.denegada then
         ObjNotaFiscalCfop.NOTAFISCAL.nfe.Submit_STATUSNOTA('D');

      //celio 27/03 - processamento
      if objTransNFE.nfeProcessamento then
        ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Submit_STATUSNOTA('P'); //em processamento
      //celio

      if (ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Salvar(false) = False) then
        MensagemErro('N�o foi poss�vel alterar o status  para "GERADA" na NF-e: '+PnotaFiscal[0]+' contate o suporte'); {marcar manualmente para G}

      {editando a NF}
      ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;
      ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Submit_Codigo(pnfe);

      if (pModoContingencia=True) then
        ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('T')
      else
        ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('I');

      if objTransNFE.denegada then
          ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('D');

      //celio 27/03 - processamento
      if objTransNFE.nfeProcessamento then
        ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('P'); //em processamento
      //celio

      if not (ObjNotaFiscalCfop.NOTAFISCAL.Salvar (false)) then
        MensagemErro('N�o foi possivel alterar o status para "I" na NF: '+pnfe+' contate o suporte');{marcar manualmente para I}

      result := True;
      FDataModulo.IBTransaction.CommitRetaining;

      if (ObjNotaFiscalCfop.NOTAFISCAL.Get_Situacao = 'T') or (ObjNotaFiscalCfop.NOTAFISCAL.Get_Situacao = 'I') then
        objTransNFE.imprimeDanfe( objTransNFE.retornaArquivoXML )
      else if (ObjNotaFiscalCfop.NOTAFISCAL.Get_Situacao = 'D') then
        ShowMessage('NF-e foi gerada mais esta denegada o uso, n�o podendo ser mais utilizada');

    end;

  finally
    FreeAndNil(queryDup);
    FreeAndNil(objTransNFE);
  end;

end;
{-}



{ TODO : atualizaValoresTotaisNotaFiscal }
procedure TObjNotafiscalObjetos.AtualizavaloresTotaisNotaFiscal(Pnota:string);
var
  query:TIBQuery;
  valortotalIPI:Currency;
  valortotalSEGURO:Currency;
  valortotalFRETE:Currency;
  valortotalPIS:Currency;
  valortotalPISST:Currency;
  valortotalCOFINS:Currency;
  valortotalCOFINSST:Currency;
  TMPBASEC_ICMS:Currency;
  TMPVALORICMS:Currency;
  TMPBASEC_ICMS_recolhido:Currency;
  TMPVALORICMS_recolhido:Currency;

begin
  try
    query:=TIBQuery.Create(nil);
    query.Database:=FDataModulo.IBDatabase;
  except

  end;

  try
    valortotalIPI:=0;
    valortotalSEGURO:=0;
    valortotalFRETE:=0;
    valortotalPIS:=0;
    valortotalPISST:=0;
    valortotalCOFINS:=0;
    valortotalCOFINSST:=0;
    with query do
    begin
      Close;
      SQL.Clear;
      SQL.Add('select * from viewprodutosvenda where notafiscal='+Pnota);
      Open;
      while not Eof do
      begin
           valortotalIPI            := valortotalIPI           + (StrToCurrDef(fieldbyname('valor_ipi').AsString,0));
           valortotalSEGURO         := valortotalSEGURO        + (StrToCurrDef(fieldbyname('valorseguro').AsString,0));
           valortotalFRETE          := valortotalFRETE         + (StrToCurrDef(fieldbyname('valorfrete').AsString,0));
           valortotalPIS            := valortotalPIS           + (StrToCurrDef(fieldbyname('valor_pis').AsString,0));
           valortotalPISST          := valortotalPISST         + (StrToCurrDef(fieldbyname('valor_pis_st').AsString,0));
           valortotalCOFINS         := valortotalCOFINS        + (StrToCurrDef(fieldbyname('valor_cofins').AsString,0));
           valortotalCOFINSST       := valortotalCOFINSST      + (StrToCurrDef(fieldbyname('valor_cofins_st').AsString,0));
           TMPBASEC_ICMS            := TMPBASEC_ICMS           + (StrToCurrDef(fieldbyname('BC_ICMS').AsString,0));
           TMPVALORICMS             := TMPVALORICMS            + (StrToCurrDef(fieldbyname('VALOR_ICMS').AsString,0));
           TMPBASEC_ICMS_recolhido  := TMPBASEC_ICMS_recolhido + (StrToCurrDef(fieldbyname('BC_ICMS_st').AsString,0));
           TMPVALORICMS_recolhido   := TMPVALORICMS_recolhido  + (StrToCurrDef(fieldbyname('VALOR_ICMS_st').AsString,0));
           Next;
      end;
               
      self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(Pnota) ;
      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_BASECALCULOICMS_SUBST_recolhido(formatfloat('0.00',TMPBASEC_ICMS_recolhido));
      Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORICMS_SUBST_recolhido(formatfloat('0.00',TMPVALORICMS_recolhido));

      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_BASECALCULOICMS_SUBSTITUICAO(formatfloat('0.00',TMPBASEC_ICMS_recolhido));
      Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_ValorICMS_Substituicao(formatfloat('0.00',TMPVALORICMS_recolhido));

      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_BASECALCULOICMS(formatfloat('0.00',TMPBASEC_ICMS));
      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORICMS(formatfloat('0.00',TMPVALORICMS));

      //Adiciona a Soma de IPI, Frete e Seguro
      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORTOTALIPI(formatfloat('0.00',valortotalIPI));
      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORFRETE(formatfloat('0.00',valorTotalFrete));
      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORseguro(formatfloat('0.00',valorTotalseguro));

      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_valorpis(formatfloat('0.00',valortotalPIS));
      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_valorpis_St(formatfloat('0.00',valortotalPISST));

      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_valorcofins(formatfloat('0.00',valortotalCOFINS));
      self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_valorcofins_St(formatfloat('0.00',valortotalCOFINSST));

      Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsEdit;
      ObjNotaFiscalCfop.NOTAFISCAL.Salvar(false);

    end;
  finally
    if Assigned(query) then
      FreeAndNil(query);
  end;
end;

procedure TObjNotafiscalObjetos.CalculaPesoNovaForma(Pnota:string);
var
ppesototal:currency;
begin

     If (Pnota='')
     Then exit;

     //Verificando se a venda existe
     If (Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(Pnota)=False)
     Then Begin
               messagedlg('Nota Fiscal n�o localizada!',mterror,[mbok],0);
               exit;
     End;
     Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;


     With self.Objquery do
     Begin
          Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
          //******selecionando todos os produtos da venda*******
          Close;
          SQL.clear;
          SQL.add('Select QUANTIDADE,PESOUNITARIO from viewprodutosvenda where notafiscal='+Pnota);
          open;
          first;
          Ppesototal:=0;
          While not(eof) do
          Begin
               if (fieldbyname('PESOUNITARIO').asstring<>'')
               Then Begin
                         Try
                            ppesototal:=ppesototal+(fieldbyname('quantidade').ascurrency*fieldbyname('PESOUNITARIO').ascurrency);
                         Except

                         End;
               End;

               next;
          End;

          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_PesoBruto(floattostr(ppesototal));
          self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_PesoLiquido(floattostr(ppesototal));
          Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsEdit;
          Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(false);
     End;
end;

Function TObjNotafiscalObjetos.GravaProdutosNF_NfeDigitada(PnfeDigitada,Pnota:string):Boolean;
var
  QryPesquisaItens_QUERY:TIBQuery;
  QryPesquisaItens_QUERYaux:TIBQuery;
  ObjFerragem_NFlocal      :TObjFERRAGEM_NF;
  ObjPerfilado_NFlocal     :TObjPerfilado_NF;
  ObjVidro_NFlocal         :TObjVidro_NF;
  ObjKitBox_NFlocal        :TObjKITBOX_NF;
  ObjPersiana_NFlocal      :TObjPERSIANA_NF;
  ObjDiverso_NFlocal       :TObjDIVERSO_NF;
begin
  Result:=False;
  try
    QryPesquisaItens_QUERY:=TIBQuery.Create(nil);
    QryPesquisaItens_QUERY.Database:=FDataModulo.IBDatabase;
    QryPesquisaItens_QUERYaux:=TIBQuery.Create(nil);
    QryPesquisaItens_QUERYaux.Database:=FDataModulo.IBDatabase;

    ObjFerragem_NFlocal      :=TObjFERRAGEM_NF.create(Self.Owner);
    ObjPerfilado_NFlocal     :=TObjPerfilado_NF.create(Self.Owner);
    ObjVidro_NFlocal         :=TObjVidro_NF.create(Self.Owner);
    ObjKitBox_NFlocal        :=TObjKITBOX_NF.create(Self.Owner);
    ObjPersiana_NFlocal      :=TObjPERSIANA_NF.create(Self.Owner);
    ObjDiverso_NFlocal       :=TObjDIVERSO_NF.create(Self.Owner);
  except
    Exit;
  end;

  try
    QryPesquisaItens_QUERY.Close;
    QryPesquisaItens_QUERY.SQL.Clear;
    QryPesquisaItens_QUERY.SQL.Text:=
    'SELECT *'+
    'FROM TABMATERIAISVENDA '+
    'WHERE NFEDIGITADA=:PNfeDigitada '+
    'AND NOTAFISCAL=:pNota';

    QryPesquisaItens_QUERY.Params[0].AsString:=PNfeDigitada;
    QryPesquisaItens_QUERY.Params[1].AsString:=Pnota;
    QryPesquisaItens_QUERY.Open;

    while not QryPesquisaItens_QUERY.Eof do
    begin
      //FERRAGEM
      if(QryPesquisaItens_QUERY.FieldByName('material').AsString='1') then
      begin
        With ObjFerragem_nflocal do
        Begin
          ZerarTabela;
          QryPesquisaItens_QUERYaux.Close;
          QryPesquisaItens_QUERYaux.SQL.Clear;
          QryPesquisaItens_QUERYaux.SQL.Text:=
          'select codigo from tabferragemcor where cor='+QryPesquisaItens_QUERY.fieldbyname('cor_codigo').AsString+' '+
          'and ferragem='+QryPesquisaItens_QUERY.fieldbyname('ferragem').AsString;
          QryPesquisaItens_QUERYaux.Open;

          if (FerragemCor.LocalizaCodigo(QryPesquisaItens_QUERYaux.fields[0].asstring)=False)
          Then Begin
            Messagedlg('N�o foi Encontrado essa Ferragem!',mterror,[mbok],0);
            exit;
          End;
          if (notafiscal.LocalizaCodigo(pnota)=False)Then
          Begin
            mensagemerro('Nota Fiscal n�o encontrada');
            exit;
          End;
          notafiscal.TabelaparaObjeto;
          FerragemCor.TabelaparaObjeto;
          status:=dsInactive;
          Status:=dsinsert;
          Submit_Codigo('0');
          Submit_Quantidade(QryPesquisaItens_QUERY.fieldbyname('quantidade').asstring);
          Submit_Valor(QryPesquisaItens_QUERY.fieldbyname('valorunitario').asstring);
          IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
          IMPOSTO_IPI.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_IPI').asstring);
          IMPOSTO_PIS_ORIGEM.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
          IMPOSTO_COFINS_ORIGEM.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
          CFOP.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('CFOP').asstring);
          IMPOSTO_ICMS_DESTINO.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
          IMPOSTO_PIS_DESTINO.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
          IMPOSTO_COFINS_DESTINO.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
          Submit_BC_ICMS(QryPesquisaItens_QUERY.fieldbyname('BC_ICMS').AsString);
          Submit_BC_ICMS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_ICMS_ST').AsString);
          Submit_VALOR_ICMS(QryPesquisaItens_QUERY.fieldbyname('VALOR_ICMS').AsString);
          Submit_VALOR_ICMS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_ICMS_ST').AsString);
          Submit_BC_IPI(QryPesquisaItens_QUERY.fieldbyname('BC_IPI').AsString);
          Submit_VALOR_IPI(QryPesquisaItens_QUERY.fieldbyname('VALOR_IPI').AsString);
          Submit_BC_PIS(QryPesquisaItens_QUERY.fieldbyname('BC_PIS').AsString);
          Submit_VALOR_PIS(QryPesquisaItens_QUERY.fieldbyname('VALOR_IPI').AsString);
          Submit_BC_PIS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_PIS_ST').AsString);
          Submit_VALOR_PIS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_PIS_ST').AsString);
          Submit_BC_COFINS(QryPesquisaItens_QUERY.fieldbyname('BC_COFINS').AsString);
          Submit_VALOR_COFINS(QryPesquisaItens_QUERY.fieldbyname('VALOR_COFINS').AsString);
          Submit_BC_COFINS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_COFINS_ST').AsString);
          Submit_VALOR_COFINS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_COFINS_ST').AsString);
          Submit_valorpauta(QryPesquisaItens_QUERY.fieldbyname('VALORPAUTA').AsString);
          Submit_ISENTO(QryPesquisaItens_QUERY.fieldbyname('ISENTO').AsString);
          Submit_SUBSTITUICAOTRIBUTARIA(QryPesquisaItens_QUERY.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
          Submit_ALIQUOTA(QryPesquisaItens_QUERY.fieldbyname('ALIQUOTA').AsString);
          Submit_REDUCAOBASECALCULO(QryPesquisaItens_QUERY.fieldbyname('REDUCAOBASECALCULO').AsString);
          Submit_ALIQUOTACUPOM(QryPesquisaItens_QUERY.fieldbyname('ALIQUOTACUPOM').AsString);
          Submit_IPI('0');
          Submit_percentualagregado(QryPesquisaItens_QUERY.fieldbyname('PERCENTUALAGREGADO').AsString);
          Submit_Margemvaloragregadoconsumidor(QryPesquisaItens_QUERY.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
          SituacaoTributaria_TabelaA.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
          SituacaoTributaria_TabelaB.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
          Submit_valorfrete(QryPesquisaItens_QUERY.fieldbyname('valorfrete').AsString);
          Submit_valorseguro(QryPesquisaItens_QUERY.fieldbyname('valorseguro').AsString);
          Submit_referencia('');
          Submit_desconto(QryPesquisaItens_QUERY.fieldbyname('desconto').AsString);
          CSOSN.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('csosn').AsString);
          submit_UNIDADE(QryPesquisaItens_QUERY.FieldByName('unidade').AsString);
          if (Salvar(False)=False)
          Then Begin
            messagedlg('N�o foi poss�vel Gravar a Ferragem '+QryPesquisaItens_QUERY.fieldbyname('descricao').asstring,mterror,[mbok],0);
            exit;
          End;

        End;
      end;
      //PERFILADO
      if(QryPesquisaItens_QUERY.FieldByName('material').AsString='2') then
      begin
        With ObjPerfilado_NFlocal do
        Begin
          ZerarTabela;
          QryPesquisaItens_QUERYaux.Close;
          QryPesquisaItens_QUERYaux.SQL.Clear;
          QryPesquisaItens_QUERYaux.SQL.Text:=
          'select codigo from tabperfiladocor where cor='+QryPesquisaItens_QUERY.fieldbyname('cor_codigo').AsString+' '+
          'and perfilado='+QryPesquisaItens_QUERY.fieldbyname('perfilado').AsString;
          QryPesquisaItens_QUERYaux.Open;

          if (PerfiladoCor.LocalizaCodigo(QryPesquisaItens_QUERYaux.fields[0].asstring)=False)
          Then Begin
            Messagedlg('N�o foi Encontrado essa Ferragem!',mterror,[mbok],0);
            exit;
          End;
          if (notafiscal.LocalizaCodigo(pnota)=False)Then
          Begin
            mensagemerro('Nota Fiscal n�o encontrada');
            exit;
          End;
          notafiscal.TabelaparaObjeto;
          PerfiladoCor.TabelaparaObjeto;
          status:=dsInactive;
          Status:=dsinsert;
          Submit_Codigo('0');
          Submit_Quantidade(QryPesquisaItens_QUERY.fieldbyname('quantidade').asstring);
          Submit_Valor(QryPesquisaItens_QUERY.fieldbyname('valorunitario').asstring);
          IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
          IMPOSTO_IPI.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_IPI').asstring);
          IMPOSTO_PIS_ORIGEM.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
          IMPOSTO_COFINS_ORIGEM.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
          CFOP.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('CFOP').asstring);
          IMPOSTO_ICMS_DESTINO.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
          IMPOSTO_PIS_DESTINO.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
          IMPOSTO_COFINS_DESTINO.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
          Submit_BC_ICMS(QryPesquisaItens_QUERY.fieldbyname('BC_ICMS').AsString);
          Submit_BC_ICMS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_ICMS_ST').AsString);
          Submit_VALOR_ICMS(QryPesquisaItens_QUERY.fieldbyname('VALOR_ICMS').AsString);
          Submit_VALOR_ICMS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_ICMS_ST').AsString);
          Submit_BC_IPI(QryPesquisaItens_QUERY.fieldbyname('BC_IPI').AsString);
          Submit_VALOR_IPI(QryPesquisaItens_QUERY.fieldbyname('VALOR_IPI').AsString);
          Submit_BC_PIS(QryPesquisaItens_QUERY.fieldbyname('BC_PIS').AsString);
          Submit_VALOR_PIS(QryPesquisaItens_QUERY.fieldbyname('VALOR_IPI').AsString);
          Submit_BC_PIS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_PIS_ST').AsString);
          Submit_VALOR_PIS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_PIS_ST').AsString);
          Submit_BC_COFINS(QryPesquisaItens_QUERY.fieldbyname('BC_COFINS').AsString);
          Submit_VALOR_COFINS(QryPesquisaItens_QUERY.fieldbyname('VALOR_COFINS').AsString);
          Submit_BC_COFINS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_COFINS_ST').AsString);
          Submit_VALOR_COFINS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_COFINS_ST').AsString);
          Submit_valorpauta(QryPesquisaItens_QUERY.fieldbyname('VALORPAUTA').AsString);
          Submit_ISENTO(QryPesquisaItens_QUERY.fieldbyname('ISENTO').AsString);
          Submit_SUBSTITUICAOTRIBUTARIA(QryPesquisaItens_QUERY.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
          Submit_ALIQUOTA(QryPesquisaItens_QUERY.fieldbyname('ALIQUOTA').AsString);
          Submit_REDUCAOBASECALCULO(QryPesquisaItens_QUERY.fieldbyname('REDUCAOBASECALCULO').AsString);
          Submit_ALIQUOTACUPOM(QryPesquisaItens_QUERY.fieldbyname('ALIQUOTACUPOM').AsString);
          Submit_IPI('0');
          Submit_percentualagregado(QryPesquisaItens_QUERY.fieldbyname('PERCENTUALAGREGADO').AsString);
          Submit_Margemvaloragregadoconsumidor(QryPesquisaItens_QUERY.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
          SituacaoTributaria_TabelaA.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
          SituacaoTributaria_TabelaB.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
          Submit_valorfrete(QryPesquisaItens_QUERY.fieldbyname('valorfrete').AsString);
          Submit_valorseguro(QryPesquisaItens_QUERY.fieldbyname('valorseguro').AsString);
          Submit_referencia('');
          Submit_desconto(QryPesquisaItens_QUERY.fieldbyname('desconto').AsString);
          CSOSN.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('csosn').AsString);
          submit_UNIDADE(QryPesquisaItens_QUERY.FieldByName('unidade').AsString);
          if (Salvar(False)=False)
          Then Begin
            messagedlg('N�o foi poss�vel Gravar o perfilado '+QryPesquisaItens_QUERY.fieldbyname('descricao').asstring,mterror,[mbok],0);
            exit;
          End;

        End;
      end;
      //VIDRO
      if(QryPesquisaItens_QUERY.FieldByName('material').AsString='3') then
      begin
        With ObjVidro_NFlocal do
        Begin
          ZerarTabela;
          QryPesquisaItens_QUERYaux.Close;
          QryPesquisaItens_QUERYaux.SQL.Clear;
          QryPesquisaItens_QUERYaux.SQL.Text:=
          'select codigo from tabvidrocor where cor='+QryPesquisaItens_QUERY.fieldbyname('cor_codigo').AsString+' '+
          'and vidro='+QryPesquisaItens_QUERY.fieldbyname('vidro').AsString;
          QryPesquisaItens_QUERYaux.Open;

          if (vidroCor.LocalizaCodigo(QryPesquisaItens_QUERYaux.fields[0].asstring)=False)
          Then Begin
            Messagedlg('N�o foi Encontrado esse vidro!',mterror,[mbok],0);
            exit;
          End;
          if (notafiscal.LocalizaCodigo(pnota)=False)Then
          Begin
            mensagemerro('Nota Fiscal n�o encontrada');
            exit;
          End;
          notafiscal.TabelaparaObjeto;
          vidroCor.TabelaparaObjeto;
          status:=dsInactive;
          Status:=dsinsert;
          Submit_Codigo('0');
          Submit_Quantidade(QryPesquisaItens_QUERY.fieldbyname('quantidade').asstring);
          Submit_Valor(QryPesquisaItens_QUERY.fieldbyname('valorunitario').asstring);
          IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
          IMPOSTO_IPI.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_IPI').asstring);
          IMPOSTO_PIS_ORIGEM.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
          IMPOSTO_COFINS_ORIGEM.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
          CFOP.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('CFOP').asstring);
          IMPOSTO_ICMS_DESTINO.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
          IMPOSTO_PIS_DESTINO.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
          IMPOSTO_COFINS_DESTINO.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
          Submit_BC_ICMS(QryPesquisaItens_QUERY.fieldbyname('BC_ICMS').AsString);
          Submit_BC_ICMS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_ICMS_ST').AsString);
          Submit_VALOR_ICMS(QryPesquisaItens_QUERY.fieldbyname('VALOR_ICMS').AsString);
          Submit_VALOR_ICMS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_ICMS_ST').AsString);
          Submit_BC_IPI(QryPesquisaItens_QUERY.fieldbyname('BC_IPI').AsString);
          Submit_VALOR_IPI(QryPesquisaItens_QUERY.fieldbyname('VALOR_IPI').AsString);
          Submit_BC_PIS(QryPesquisaItens_QUERY.fieldbyname('BC_PIS').AsString);
          Submit_VALOR_PIS(QryPesquisaItens_QUERY.fieldbyname('VALOR_IPI').AsString);
          Submit_BC_PIS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_PIS_ST').AsString);
          Submit_VALOR_PIS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_PIS_ST').AsString);
          Submit_BC_COFINS(QryPesquisaItens_QUERY.fieldbyname('BC_COFINS').AsString);
          Submit_VALOR_COFINS(QryPesquisaItens_QUERY.fieldbyname('VALOR_COFINS').AsString);
          Submit_BC_COFINS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_COFINS_ST').AsString);
          Submit_VALOR_COFINS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_COFINS_ST').AsString);
          Submit_valorpauta(QryPesquisaItens_QUERY.fieldbyname('VALORPAUTA').AsString);
          Submit_ISENTO(QryPesquisaItens_QUERY.fieldbyname('ISENTO').AsString);
          Submit_SUBSTITUICAOTRIBUTARIA(QryPesquisaItens_QUERY.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
          Submit_ALIQUOTA(QryPesquisaItens_QUERY.fieldbyname('ALIQUOTA').AsString);
          Submit_REDUCAOBASECALCULO(QryPesquisaItens_QUERY.fieldbyname('REDUCAOBASECALCULO').AsString);
          Submit_ALIQUOTACUPOM(QryPesquisaItens_QUERY.fieldbyname('ALIQUOTACUPOM').AsString);
          Submit_IPI('0');
          Submit_percentualagregado(QryPesquisaItens_QUERY.fieldbyname('PERCENTUALAGREGADO').AsString);
          Submit_Margemvaloragregadoconsumidor(QryPesquisaItens_QUERY.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
          SituacaoTributaria_TabelaA.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
          SituacaoTributaria_TabelaB.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
          Submit_valorfrete(QryPesquisaItens_QUERY.fieldbyname('valorfrete').AsString);
          Submit_valorseguro(QryPesquisaItens_QUERY.fieldbyname('valorseguro').AsString);
          Submit_referencia('');
          Submit_desconto(QryPesquisaItens_QUERY.fieldbyname('desconto').AsString);
          CSOSN.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('csosn').AsString);
          submit_UNIDADE(QryPesquisaItens_QUERY.FieldByName('unidade').AsString);
          if (Salvar(False)=False)
          Then Begin
            messagedlg('N�o foi poss�vel Gravar o vidro '+QryPesquisaItens_QUERY.fieldbyname('descricao').asstring,mterror,[mbok],0);
            exit;
          End;

        End;
      end;
      //KITBOX
      if(QryPesquisaItens_QUERY.FieldByName('material').AsString='4') then
      begin
        With ObjKitBox_NFlocal do
        Begin
          ZerarTabela;
          QryPesquisaItens_QUERYaux.Close;
          QryPesquisaItens_QUERYaux.SQL.Clear;
          QryPesquisaItens_QUERYaux.SQL.Text:=
          'select codigo from tabkitboxcor where cor='+QryPesquisaItens_QUERY.fieldbyname('cor_codigo').AsString+' '+
          'and kitbox='+QryPesquisaItens_QUERY.fieldbyname('kitbox').AsString;
          QryPesquisaItens_QUERYaux.Open;

          if (kitboxCor.LocalizaCodigo(QryPesquisaItens_QUERYaux.fields[0].asstring)=False)
          Then Begin
            Messagedlg('N�o foi Encontrado esse kitbox!',mterror,[mbok],0);
            exit;
          End;
          if (notafiscal.LocalizaCodigo(pnota)=False)Then
          Begin
            mensagemerro('Nota Fiscal n�o encontrada');
            exit;
          End;
          notafiscal.TabelaparaObjeto;
          kitboxCor.TabelaparaObjeto;
          status:=dsInactive;
          Status:=dsinsert;
          Submit_Codigo('0');
          Submit_Quantidade(QryPesquisaItens_QUERY.fieldbyname('quantidade').asstring);
          Submit_Valor(QryPesquisaItens_QUERY.fieldbyname('valorunitario').asstring);
          IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
          IMPOSTO_IPI.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_IPI').asstring);
          IMPOSTO_PIS_ORIGEM.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
          IMPOSTO_COFINS_ORIGEM.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
          CFOP.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('CFOP').asstring);
          IMPOSTO_ICMS_DESTINO.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
          IMPOSTO_PIS_DESTINO.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
          IMPOSTO_COFINS_DESTINO.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
          Submit_BC_ICMS(QryPesquisaItens_QUERY.fieldbyname('BC_ICMS').AsString);
          Submit_BC_ICMS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_ICMS_ST').AsString);
          Submit_VALOR_ICMS(QryPesquisaItens_QUERY.fieldbyname('VALOR_ICMS').AsString);
          Submit_VALOR_ICMS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_ICMS_ST').AsString);
          Submit_BC_IPI(QryPesquisaItens_QUERY.fieldbyname('BC_IPI').AsString);
          Submit_VALOR_IPI(QryPesquisaItens_QUERY.fieldbyname('VALOR_IPI').AsString);
          Submit_BC_PIS(QryPesquisaItens_QUERY.fieldbyname('BC_PIS').AsString);
          Submit_VALOR_PIS(QryPesquisaItens_QUERY.fieldbyname('VALOR_IPI').AsString);
          Submit_BC_PIS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_PIS_ST').AsString);
          Submit_VALOR_PIS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_PIS_ST').AsString);
          Submit_BC_COFINS(QryPesquisaItens_QUERY.fieldbyname('BC_COFINS').AsString);
          Submit_VALOR_COFINS(QryPesquisaItens_QUERY.fieldbyname('VALOR_COFINS').AsString);
          Submit_BC_COFINS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_COFINS_ST').AsString);
          Submit_VALOR_COFINS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_COFINS_ST').AsString);
          Submit_valorpauta(QryPesquisaItens_QUERY.fieldbyname('VALORPAUTA').AsString);
          Submit_ISENTO(QryPesquisaItens_QUERY.fieldbyname('ISENTO').AsString);
          Submit_SUBSTITUICAOTRIBUTARIA(QryPesquisaItens_QUERY.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
          Submit_ALIQUOTA(QryPesquisaItens_QUERY.fieldbyname('ALIQUOTA').AsString);
          Submit_REDUCAOBASECALCULO(QryPesquisaItens_QUERY.fieldbyname('REDUCAOBASECALCULO').AsString);
          Submit_ALIQUOTACUPOM(QryPesquisaItens_QUERY.fieldbyname('ALIQUOTACUPOM').AsString);
          Submit_IPI('0');
          Submit_percentualagregado(QryPesquisaItens_QUERY.fieldbyname('PERCENTUALAGREGADO').AsString);
          Submit_Margemvaloragregadoconsumidor(QryPesquisaItens_QUERY.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
          SituacaoTributaria_TabelaA.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
          SituacaoTributaria_TabelaB.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
          Submit_valorfrete(QryPesquisaItens_QUERY.fieldbyname('valorfrete').AsString);
          Submit_valorseguro(QryPesquisaItens_QUERY.fieldbyname('valorseguro').AsString);
          Submit_referencia('');
          Submit_desconto(QryPesquisaItens_QUERY.fieldbyname('desconto').AsString);
          CSOSN.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('csosn').AsString);
          submit_UNIDADE(QryPesquisaItens_QUERY.FieldByName('unidade').AsString);
          if (Salvar(False)=False)
          Then Begin
            messagedlg('N�o foi poss�vel Gravar o kitbox '+QryPesquisaItens_QUERY.fieldbyname('descricao').asstring,mterror,[mbok],0);
            exit;
          End;

        End;
      end;
      //PERSIANA
      if(QryPesquisaItens_QUERY.FieldByName('material').AsString='5') then
      begin
        With ObjPersiana_NFlocal do
        Begin
          ZerarTabela;
          QryPesquisaItens_QUERYaux.Close;
          QryPesquisaItens_QUERYaux.SQL.Clear;
          QryPesquisaItens_QUERYaux.SQL.Text:=
          'select codigo from tabPERSIANAGRUPODIAMETROcor where cor='+QryPesquisaItens_QUERY.fieldbyname('cor_codigo').AsString+' '+
          'and PERSIANA='+QryPesquisaItens_QUERY.fieldbyname('persiana').AsString;
          QryPesquisaItens_QUERYaux.Open;

          if (persianagrupodiametrocor.LocalizaCodigo(QryPesquisaItens_QUERYaux.fields[0].asstring)=False)
          Then Begin
            Messagedlg('N�o foi Encontrado essa persiana!',mterror,[mbok],0);
            exit;
          End;
          if (notafiscal.LocalizaCodigo(pnota)=False)Then
          Begin
            mensagemerro('Nota Fiscal n�o encontrada');
            exit;
          End;
          notafiscal.TabelaparaObjeto;
          persianagrupodiametrocor.TabelaparaObjeto;
          status:=dsInactive;
          Status:=dsinsert;
          Submit_Codigo('0');
          Submit_Quantidade(QryPesquisaItens_QUERY.fieldbyname('quantidade').asstring);
          Submit_Valor(QryPesquisaItens_QUERY.fieldbyname('valorunitario').asstring);
          IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
          IMPOSTO_IPI.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_IPI').asstring);
          IMPOSTO_PIS_ORIGEM.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
          IMPOSTO_COFINS_ORIGEM.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
          CFOP.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('CFOP').asstring);
          IMPOSTO_ICMS_DESTINO.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
          IMPOSTO_PIS_DESTINO.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
          IMPOSTO_COFINS_DESTINO.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
          Submit_BC_ICMS(QryPesquisaItens_QUERY.fieldbyname('BC_ICMS').AsString);
          Submit_BC_ICMS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_ICMS_ST').AsString);
          Submit_VALOR_ICMS(QryPesquisaItens_QUERY.fieldbyname('VALOR_ICMS').AsString);
          Submit_VALOR_ICMS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_ICMS_ST').AsString);
          Submit_BC_IPI(QryPesquisaItens_QUERY.fieldbyname('BC_IPI').AsString);
          Submit_VALOR_IPI(QryPesquisaItens_QUERY.fieldbyname('VALOR_IPI').AsString);
          Submit_BC_PIS(QryPesquisaItens_QUERY.fieldbyname('BC_PIS').AsString);
          Submit_VALOR_PIS(QryPesquisaItens_QUERY.fieldbyname('VALOR_IPI').AsString);
          Submit_BC_PIS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_PIS_ST').AsString);
          Submit_VALOR_PIS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_PIS_ST').AsString);
          Submit_BC_COFINS(QryPesquisaItens_QUERY.fieldbyname('BC_COFINS').AsString);
          Submit_VALOR_COFINS(QryPesquisaItens_QUERY.fieldbyname('VALOR_COFINS').AsString);
          Submit_BC_COFINS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_COFINS_ST').AsString);
          Submit_VALOR_COFINS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_COFINS_ST').AsString);
          Submit_valorpauta(QryPesquisaItens_QUERY.fieldbyname('VALORPAUTA').AsString);
          Submit_ISENTO(QryPesquisaItens_QUERY.fieldbyname('ISENTO').AsString);
          Submit_SUBSTITUICAOTRIBUTARIA(QryPesquisaItens_QUERY.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
          Submit_ALIQUOTA(QryPesquisaItens_QUERY.fieldbyname('ALIQUOTA').AsString);
          Submit_REDUCAOBASECALCULO(QryPesquisaItens_QUERY.fieldbyname('REDUCAOBASECALCULO').AsString);
          Submit_ALIQUOTACUPOM(QryPesquisaItens_QUERY.fieldbyname('ALIQUOTACUPOM').AsString);
          Submit_IPI('0');
          Submit_percentualagregado(QryPesquisaItens_QUERY.fieldbyname('PERCENTUALAGREGADO').AsString);
          Submit_Margemvaloragregadoconsumidor(QryPesquisaItens_QUERY.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
          SituacaoTributaria_TabelaA.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
          SituacaoTributaria_TabelaB.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
          Submit_valorfrete(QryPesquisaItens_QUERY.fieldbyname('valorfrete').AsString);
          Submit_valorseguro(QryPesquisaItens_QUERY.fieldbyname('valorseguro').AsString);
          Submit_referencia('');
          Submit_desconto(QryPesquisaItens_QUERY.fieldbyname('desconto').AsString);
          CSOSN.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('csosn').AsString);
          submit_UNIDADE(QryPesquisaItens_QUERY.FieldByName('unidade').AsString);
          if (Salvar(False)=False)
          Then Begin
            messagedlg('N�o foi poss�vel Gravar a Persiana '+QryPesquisaItens_QUERY.fieldbyname('descricao').asstring,mterror,[mbok],0);
            exit;
          End;

        End;
      end;
      //DIVERSO
      if(QryPesquisaItens_QUERY.FieldByName('material').AsString='6') then
      begin
        With ObjDiverso_NFlocal do
        Begin
          ZerarTabela;
          QryPesquisaItens_QUERYaux.Close;
          QryPesquisaItens_QUERYaux.SQL.Clear;
          QryPesquisaItens_QUERYaux.SQL.Text:=
          'select codigo from tabDIVERSOcor where cor='+QryPesquisaItens_QUERY.fieldbyname('cor_codigo').AsString+' '+
          'and diverso='+QryPesquisaItens_QUERY.fieldbyname('diverso').AsString;
          QryPesquisaItens_QUERYaux.Open;

          if (diversoCOR.LocalizaCodigo(QryPesquisaItens_QUERYaux.fields[0].asstring)=False)
          Then Begin
            Messagedlg('N�o foi Encontrado esse diverso!',mterror,[mbok],0);
            exit;
          End;
          if (notafiscal.LocalizaCodigo(pnota)=False)Then
          Begin
            mensagemerro('Nota Fiscal n�o encontrada');
            exit;
          End;
          notafiscal.TabelaparaObjeto;
          diversoCOR.TabelaparaObjeto;
          status:=dsInactive;
          Status:=dsinsert;
          Submit_Codigo('0');
          Submit_Quantidade(QryPesquisaItens_QUERY.fieldbyname('quantidade').asstring);
          Submit_Valor(QryPesquisaItens_QUERY.fieldbyname('valorunitario').asstring);
          IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
          IMPOSTO_IPI.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_IPI').asstring);
          IMPOSTO_PIS_ORIGEM.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
          IMPOSTO_COFINS_ORIGEM.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
          CFOP.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('CFOP').asstring);
          IMPOSTO_ICMS_DESTINO.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
          IMPOSTO_PIS_DESTINO.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
          IMPOSTO_COFINS_DESTINO.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
          Submit_BC_ICMS(QryPesquisaItens_QUERY.fieldbyname('BC_ICMS').AsString);
          Submit_BC_ICMS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_ICMS_ST').AsString);
          Submit_VALOR_ICMS(QryPesquisaItens_QUERY.fieldbyname('VALOR_ICMS').AsString);
          Submit_VALOR_ICMS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_ICMS_ST').AsString);
          Submit_BC_IPI(QryPesquisaItens_QUERY.fieldbyname('BC_IPI').AsString);
          Submit_VALOR_IPI(QryPesquisaItens_QUERY.fieldbyname('VALOR_IPI').AsString);
          Submit_BC_PIS(QryPesquisaItens_QUERY.fieldbyname('BC_PIS').AsString);
          Submit_VALOR_PIS(QryPesquisaItens_QUERY.fieldbyname('VALOR_IPI').AsString);
          Submit_BC_PIS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_PIS_ST').AsString);
          Submit_VALOR_PIS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_PIS_ST').AsString);
          Submit_BC_COFINS(QryPesquisaItens_QUERY.fieldbyname('BC_COFINS').AsString);
          Submit_VALOR_COFINS(QryPesquisaItens_QUERY.fieldbyname('VALOR_COFINS').AsString);
          Submit_BC_COFINS_ST(QryPesquisaItens_QUERY.fieldbyname('BC_COFINS_ST').AsString);
          Submit_VALOR_COFINS_ST(QryPesquisaItens_QUERY.fieldbyname('VALOR_COFINS_ST').AsString);
          Submit_valorpauta(QryPesquisaItens_QUERY.fieldbyname('VALORPAUTA').AsString);
          Submit_ISENTO(QryPesquisaItens_QUERY.fieldbyname('ISENTO').AsString);
          Submit_SUBSTITUICAOTRIBUTARIA(QryPesquisaItens_QUERY.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
          Submit_ALIQUOTA(QryPesquisaItens_QUERY.fieldbyname('ALIQUOTA').AsString);
          Submit_REDUCAOBASECALCULO(QryPesquisaItens_QUERY.fieldbyname('REDUCAOBASECALCULO').AsString);
          Submit_ALIQUOTACUPOM(QryPesquisaItens_QUERY.fieldbyname('ALIQUOTACUPOM').AsString);
          Submit_IPI('0');
          Submit_percentualagregado(QryPesquisaItens_QUERY.fieldbyname('PERCENTUALAGREGADO').AsString);
          Submit_Margemvaloragregadoconsumidor(QryPesquisaItens_QUERY.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
          SituacaoTributaria_TabelaA.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
          SituacaoTributaria_TabelaB.Submit_CODIGO(QryPesquisaItens_QUERY.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
          Submit_valorfrete(QryPesquisaItens_QUERY.fieldbyname('valorfrete').AsString);
          Submit_valorseguro(QryPesquisaItens_QUERY.fieldbyname('valorseguro').AsString);
          Submit_referencia('');
          Submit_desconto(QryPesquisaItens_QUERY.fieldbyname('desconto').AsString);
          CSOSN.Submit_codigo(QryPesquisaItens_QUERY.fieldbyname('csosn').AsString);
          submit_UNIDADE(QryPesquisaItens_QUERY.FieldByName('unidade').AsString);
          if (Salvar(False)=False)
          Then Begin
            messagedlg('N�o foi poss�vel Gravar o Diverso '+QryPesquisaItens_QUERY.fieldbyname('descricao').asstring,mterror,[mbok],0);
            exit;
          End;

        End;
      end;
      QryPesquisaItens_QUERY.Next;
    end;
    //FDataModulo.IBTransaction.CommitRetaining;
    Result:=True;
  finally
    FreeAndNil(QryPesquisaItens_QUERY);
    FreeAndNil(QryPesquisaItens_QUERYaux);
    ObjFerragem_NFlocal.Free;
    ObjPerfilado_NFlocal.Free;
    ObjVidro_NFlocal.Free;
    ObjKitBox_NFlocal.Free;
    ObjPersiana_NFlocal.Free;
    ObjDiverso_NFlocal.Free;
  end;

end;


procedure TObjNotafiscalObjetos.GravaProdutosNF(Ppedido:string;Pnota:string);
var
  ObjPedidoObjetos:TObjPedidoObjetos;
  Query:TIBQuery;
  ObjFerragem_NFlocal      :TObjFERRAGEM_NF;
  ObjPerfilado_NFlocal     :TObjPerfilado_NF;
  ObjVidro_NFlocal         :TObjVidro_NF;
  ObjKitBox_NFlocal        :TObjKITBOX_NF;
  ObjPersiana_NFlocal      :TObjPERSIANA_NF;
  ObjDiverso_NFlocal       :TObjDIVERSO_NF;
begin

  ObjPedidoObjetos := TObjPedidoObjetos.Create(self.Owner);

  Query:=TIBQuery.Create(nil);
  Query.Database:=FDataModulo.IBDatabase;
  ObjFerragem_NFlocal      :=TObjFERRAGEM_NF.create(Self.Owner);
  ObjPerfilado_NFlocal     :=TObjPerfilado_NF.create(Self.Owner);
  ObjVidro_NFlocal         :=TObjVidro_NF.create(Self.Owner);
  ObjKitBox_NFlocal        :=TObjKITBOX_NF.create(Self.Owner);
  ObjPersiana_NFlocal      :=TObjPERSIANA_NF.create(Self.Owner);
  ObjDiverso_NFlocal       :=TObjDIVERSO_NF.create(Self.Owner);



  Try
              //Procurando o pedido

              //aqui procura todos os materias contidos no pedido, ou seja dos projetos que tem no pedido em quest�o
              //busca quais s�o os materias que comp�e o mesmo pra poder calcular os impostos
              With Self.ObjqueryTEMP do
              Begin

                   //***************FERRAGEM*******************************

                   //procura na tabferragem pedido projeto junto com a tabpedido_projeto
                   //PEDIDOPROJETO,FERRAGEMCOR,QUANTIDADE,VALOR,VALORFINAL(valor*quantidade)

                   close;
                   sql.clear;
                   sql.add('Select tabferragem_pp.* from tabferragem_pp');
                   sql.add('join tabpedido_proj on tabferragem_pp.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where tabpedido_proj.pedido='+Ppedido);
                   open;

                try
                   while not(eof) do
                   Begin


                                With ObjFerragem_nflocal do
                                Begin
                                     ZerarTabela;

                                     //Procura pela cor da ferragem
                                     if (FerragemCor.LocalizaCodigo(fieldbyname('ferragemcor').asstring)=False)
                                     Then Begin
                                             Messagedlg('N�o foi Encontrado essa Ferragem!',mterror,[mbok],0);
                                             exit;
                                     End;
                                     if (notafiscal.LocalizaCodigo(pnota)=False)
                                     Then Begin
                                               mensagemerro('Nota Fiscal n�o encontrada');
                                               exit;
                                     End;
                                     notafiscal.TabelaparaObjeto;

                                     //carrega para o objetos os dados da tabela
                                     //CODIGO,FERRAGEM,COR,PORCENTAGEMACRESCIMO,ESTOQUE,ACRESCIMOEXTRA,PORCENTAGEMACRESCIMOFINAL,
                                     //VALOREXTRA,VALORFINAL,CLASSIFICACAOFISCAL
                                     FerragemCor.TabelaparaObjeto;

                                     //***ALTERA��O FEITA POR JONATAN MEDINA
                                     //ANTES BUSCAVA OS IMPOSTOS NA HORA DE FOSSE GERAR UMA NOTA FISCAL, O QUE � ERRADO
                                     //O CERTO � GERAR UMA VENDA, E GUARDAR OS IMPOSTOS DOS MATERIAIS, POIS ASSIM N�O TEM O PROBLEMA
                                     //DO USUARIO APOS GERAR UMA VENDA, IR LA NO CADASTRO DO MATERIAL E MUDAR AS INFORMA��ES E A
                                     //NOTA SAIR COM AS NOVAS INFORMA��ES E N�O COM AS INFORMA��ES CORRETAS, Q TEMOS AO GERAR A VENDA
                                     //AGORA AO GERAR UMA VENDA, TODOS OS IMPOSTOS DO MATERIAL/MATERIAIS, S�O GUARDADOS NA TABMATERIAISVENDA
                                     //ASSIM NA HORA DE BUSCAR OS IMPOSTOS, SO BUSCA NESTA TABELA

                                     //Seleciono os dados que est�o na tabmateriaisvenda usando a viewprodutosvenda
                                     //nela eu busco pelo codigo do produto (codigo da ferragem, neste caso)
                                     //e pelo codigo do material (como eu estou procurando por ferragens, aqui o codigo � 1)
                                     Query.Close;
                                     Query.SQL.Clear;
                                     Query.SQL.Add('select * from viewprodutosvenda');
                                     Query.SQL.Add('where codigoprodutoPK ='+Ferragemcor.Ferragem.Get_Codigo);
                                     Query.SQL.Add('and material = 1');
                                     Query.SQL.Add('and pedido='+ppedido);
                                     Query.SQL.Add('and notafiscal='+Pnota);
                                     Query.Open;
                                     if(Query.RecordCount>0)
                                     then begin
                                         status:=dsInactive;
                                         Status:=dsinsert;
                                         Submit_Codigo('0');

                                         //Passando a quantidade de ferragem que tem no pedido
                                         Submit_Quantidade(fieldbyname('quantidade').asstring);
                                         //Passando o valor unitario de cada pedido
                                         Submit_Valor(fieldbyname('valor').asstring);

                                         IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                         IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                         IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                         IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                         CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                         IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                                         IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                         IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                                         Submit_BC_ICMS(Query.fieldbyname('BC_ICMS').AsString);
                                         Submit_BC_ICMS_ST(Query.fieldbyname('BC_ICMS_ST').AsString);
                                         Submit_VALOR_ICMS(Query.fieldbyname('VALOR_ICMS').AsString);
                                         Submit_VALOR_ICMS_ST(Query.fieldbyname('VALOR_ICMS_ST').AsString);

                                         Submit_BC_IPI(Query.fieldbyname('BC_IPI').AsString);
                                         Submit_VALOR_IPI(Query.fieldbyname('VALOR_IPI').AsString);

                                         Submit_BC_PIS(Query.fieldbyname('BC_PIS').AsString);
                                         Submit_VALOR_PIS(Query.fieldbyname('VALOR_IPI').AsString);
                                         Submit_BC_PIS_ST(Query.fieldbyname('BC_PIS_ST').AsString);
                                         Submit_VALOR_PIS_ST(Query.fieldbyname('VALOR_PIS_ST').AsString);

                                         Submit_BC_COFINS(Query.fieldbyname('BC_COFINS').AsString);
                                         Submit_VALOR_COFINS(Query.fieldbyname('VALOR_COFINS').AsString);
                                         Submit_BC_COFINS_ST(Query.fieldbyname('BC_COFINS_ST').AsString);
                                         Submit_VALOR_COFINS_ST(Query.fieldbyname('VALOR_COFINS_ST').AsString);


                                         Submit_valorpauta(Query.fieldbyname('VALORPAUTA').AsString);
                                         Submit_ISENTO(Query.fieldbyname('ISENTO').AsString);
                                         Submit_SUBSTITUICAOTRIBUTARIA(Query.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
                                         Submit_ALIQUOTA(Query.fieldbyname('ALIQUOTA').AsString);
                                         Submit_REDUCAOBASECALCULO(Query.fieldbyname('REDUCAOBASECALCULO').AsString);
                                         Submit_ALIQUOTACUPOM(Query.fieldbyname('ALIQUOTACUPOM').AsString);
                                         Submit_IPI('0');
                                         Submit_percentualagregado(Query.fieldbyname('PERCENTUALAGREGADO').AsString);
                                         Submit_Margemvaloragregadoconsumidor(Query.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
                                         SituacaoTributaria_TabelaA.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
                                         SituacaoTributaria_TabelaB.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
                                         //*********************************


                                         Submit_valorfrete(Query.fieldbyname('valorfrete').AsString);
                                         Submit_valorseguro(Query.fieldbyname('valorseguro').AsString);
                                         Submit_referencia('');
                                         Submit_desconto(virgulaparaponto(Query.fieldbyname('desconto').AsString));
                                         CSOSN.Submit_codigo(Query.fieldbyname('csosn').AsString);
                                         submit_UNIDADE(Query.FieldByName('unidade').AsString);



                                         if (Salvar(False)=False)
                                         Then Begin
                                                   messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                                                   exit;
                                         End;
                                     end;
                                End;
                         //end;

                        next;//pr�xima ferragem do pedido
                   End;//While
                finally
                    ObjFerragem_NFlocal.Free;
                end;




                   //*********************DIVERSO******************

                   close;
                   sql.clear;
                   sql.add('Select tabdiverso_pp.* from tabdiverso_pp');
                   sql.add('join tabpedido_proj on tabdiverso_pp.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where Tabpedido_proj.pedido='+Ppedido);
                   open;
                try
                   while not(eof) do
                   Begin
                        With ObjDiverso_nflocal do
                        Begin
                             ZerarTabela;
                             if (DiversoCor.LocalizaCodigo(fieldbyname('diversocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Diverso nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             DiversoCor.TabelaparaObjeto;
                             if (notafiscal.LocalizaCodigo(pnota)=False)
                             Then Begin
                                  mensagemerro('Nota Fiscal n�o encontrada');
                                  exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+Diversocor.Diverso.Get_Codigo);
                             Query.SQL.Add('and material = 6');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.SQL.Add('and notafiscal='+Pnota);
                             Query.Open;
                             if(Query.RecordCount>0)
                             then begin
                                    status:=dsInactive;
                                    Status:=dsinsert;
                                    Submit_Codigo('0');

                                    //Passando a quantidade de ferragem que tem no pedido
                                    Submit_Quantidade(fieldbyname('quantidade').asstring);
                                    //Passando o valor unitario de cada pedido
                                    Submit_Valor(fieldbyname('valor').asstring);


                                    IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                    IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                    IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                    IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                    CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                    IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                                    IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                    IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                                    Submit_BC_ICMS(Query.fieldbyname('BC_ICMS').AsString);
                                    Submit_BC_ICMS_ST(Query.fieldbyname('BC_ICMS_ST').AsString);
                                    Submit_VALOR_ICMS(Query.fieldbyname('VALOR_ICMS').AsString);
                                    Submit_VALOR_ICMS_ST(Query.fieldbyname('VALOR_ICMS_ST').AsString);

                                    Submit_BC_IPI(Query.fieldbyname('BC_IPI').AsString);
                                    Submit_VALOR_IPI(Query.fieldbyname('VALOR_IPI').AsString);

                                    Submit_BC_PIS(Query.fieldbyname('BC_PIS').AsString);
                                    Submit_VALOR_PIS(Query.fieldbyname('VALOR_IPI').AsString);
                                    Submit_BC_PIS_ST(Query.fieldbyname('BC_PIS_ST').AsString);
                                    Submit_VALOR_PIS_ST(Query.fieldbyname('VALOR_PIS_ST').AsString);

                                    Submit_BC_COFINS(Query.fieldbyname('BC_COFINS').AsString);
                                    Submit_VALOR_COFINS(Query.fieldbyname('VALOR_COFINS').AsString);
                                    Submit_BC_COFINS_ST(Query.fieldbyname('BC_COFINS_ST').AsString);
                                    Submit_VALOR_COFINS_ST(Query.fieldbyname('VALOR_COFINS_ST').AsString);


                                    Submit_valorpauta(Query.fieldbyname('VALORPAUTA').AsString);
                                    Submit_ISENTO(Query.fieldbyname('ISENTO').AsString);
                                    Submit_SUBSTITUICAOTRIBUTARIA(Query.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
                                    Submit_ALIQUOTA(Query.fieldbyname('ALIQUOTA').AsString);
                                    Submit_REDUCAOBASECALCULO(Query.fieldbyname('REDUCAOBASECALCULO').AsString);
                                    Submit_ALIQUOTACUPOM(Query.fieldbyname('ALIQUOTACUPOM').AsString);
                                    Submit_IPI('0');
                                    Submit_percentualagregado(Query.fieldbyname('PERCENTUALAGREGADO').AsString);
                                    Submit_Margemvaloragregadoconsumidor(Query.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
                                    SituacaoTributaria_TabelaA.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
                                    SituacaoTributaria_TabelaB.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
                                     //*********************************


                                    Submit_valorfrete(Query.fieldbyname('valorfrete').AsString);
                                    Submit_valorseguro(Query.fieldbyname('valorseguro').AsString);
                                    Submit_referencia('');
                                    Submit_desconto(virgulaparaponto(Query.fieldbyname('desconto').AsString));
                                    CSOSN.Submit_codigo(Query.fieldbyname('csosn').AsString);
                                    submit_UNIDADE(Query.FieldByName('unidade').AsString);

                                    if (Salvar(False)=False)
                                    Then Begin
                                               messagedlg('N�o foi poss�vel Gravar o Diverso',mterror,[mbok],0);
                                               exit;
                                    End;
                             end;

                        End;

                      next;//proximo divero
                   End;//while
                
                finally
                     ObjDiverso_NFlocal.Free;
                end;



                   //*********************KITBOX******************
                   close;
                   sql.clear;
                   sql.add('Select TABKITBOX_PP.* from TABKITBOX_PP');
                   sql.add('join tabpedido_proj on TABKITBOX_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where Tabpedido_proj.pedido='+Ppedido);
                   open;
                 try

                   while not(eof) do
                   Begin
                        With ObjKitbox_nflocal do
                        Begin
                             ZerarTabela;
                             if (KitboxCor.LocalizaCodigo(fieldbyname('kitboxcor').asstring)=False)
                             Then
                             Begin
                                     Messagedlg('N�o foi Encontrado esse Kitbox nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             KitboxCor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnota)=False)
                             Then Begin
                                               mensagemerro('Nota Fiscal n�o encontrada');
                                               exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+Kitboxcor.Kitbox.Get_Codigo);
                             Query.SQL.Add('and material = 4');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.SQL.Add('and notafiscal='+Pnota);
                             Query.Open;
                             if(Query.RecordCount>0)
                             then begin
                                    status:=dsInactive;
                                    Status:=dsinsert;
                                    Submit_Codigo('0');

                                    //Passando a quantidade de ferragem que tem no pedido
                                    Submit_Quantidade(fieldbyname('quantidade').asstring);
                                    //Passando o valor unitario de cada pedido
                                    Submit_Valor(fieldbyname('valor').asstring);


                                     IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                     IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                     IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                     IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                     CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                     IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                     IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                     IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                     IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                     CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                     IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                                     IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                     IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                                     Submit_BC_ICMS(Query.fieldbyname('BC_ICMS').AsString);
                                     Submit_BC_ICMS_ST(Query.fieldbyname('BC_ICMS_ST').AsString);
                                     Submit_VALOR_ICMS(Query.fieldbyname('VALOR_ICMS').AsString);
                                     Submit_VALOR_ICMS_ST(Query.fieldbyname('VALOR_ICMS_ST').AsString);

                                     Submit_BC_IPI(Query.fieldbyname('BC_IPI').AsString);
                                     Submit_VALOR_IPI(Query.fieldbyname('VALOR_IPI').AsString);

                                     Submit_BC_PIS(Query.fieldbyname('BC_PIS').AsString);
                                     Submit_VALOR_PIS(Query.fieldbyname('VALOR_IPI').AsString);
                                     Submit_BC_PIS_ST(Query.fieldbyname('BC_PIS_ST').AsString);
                                     Submit_VALOR_PIS_ST(Query.fieldbyname('VALOR_PIS_ST').AsString);

                                     Submit_BC_COFINS(Query.fieldbyname('BC_COFINS').AsString);
                                     Submit_VALOR_COFINS(Query.fieldbyname('VALOR_COFINS').AsString);
                                     Submit_BC_COFINS_ST(Query.fieldbyname('BC_COFINS_ST').AsString);
                                     Submit_VALOR_COFINS_ST(Query.fieldbyname('VALOR_COFINS_ST').AsString);


                                     Submit_valorpauta(Query.fieldbyname('VALORPAUTA').AsString);
                                     Submit_ISENTO(Query.fieldbyname('ISENTO').AsString);
                                     Submit_SUBSTITUICAOTRIBUTARIA(Query.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
                                     Submit_ALIQUOTA(Query.fieldbyname('ALIQUOTA').AsString);
                                     Submit_REDUCAOBASECALCULO(Query.fieldbyname('REDUCAOBASECALCULO').AsString);
                                     Submit_ALIQUOTACUPOM(Query.fieldbyname('ALIQUOTACUPOM').AsString);
                                     Submit_IPI('0');
                                     Submit_percentualagregado(Query.fieldbyname('PERCENTUALAGREGADO').AsString);
                                     Submit_Margemvaloragregadoconsumidor(Query.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
                                     SituacaoTributaria_TabelaA.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
                                     SituacaoTributaria_TabelaB.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
                                             //*********************************


                                     Submit_valorfrete(Query.fieldbyname('valorfrete').AsString);
                                     Submit_valorseguro(Query.fieldbyname('valorseguro').AsString);
                                     Submit_referencia('');
                                     Submit_desconto(virgulaparaponto(Query.fieldbyname('desconto').AsString));
                                     CSOSN.Submit_codigo(Query.fieldbyname('csosn').AsString);
                                     submit_UNIDADE(Query.FieldByName('unidade').AsString);


                                     if (Salvar(False)=False)
                                     Then
                                     Begin
                                               messagedlg('N�o foi poss�vel Gravar o kitbox',mterror,[mbok],0);
                                               exit;
                                     End;
                             end;
                        End;
                        next;//pr�ximo kitbox do pedido
                   End;//while

                 finally
                    ObjKitBox_NFlocal.Free;
                 end;


                   //*********************PERFILADO******************
                   close;
                   sql.clear;
                   sql.add('Select TABperfilado_PP.* from TABperfilado_PP');
                   sql.add('join tabpedido_proj on TABperfilado_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where Tabpedido_proj.pedido='+Ppedido);
                   open;
                try

                   while not(eof) do
                   Begin
                        With ObjPerfilado_nflocal do
                        Begin
                             ZerarTabela;
                             if (PerfiladoCor.LocalizaCodigo(fieldbyname('perfiladocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Perfilado nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             PerfiladoCor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnota)=False)
                             Then Begin
                                  mensagemerro('Nota Fiscal n�o encontrada');
                                  exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+Perfiladocor.Perfilado.Get_Codigo);
                             Query.SQL.Add('and material = 2');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.SQL.Add('and notafiscal='+Pnota);
                             Query.Open;
                             if(Query.RecordCount>0)
                             then begin
                                  status:=dsInactive;
                                  Status:=dsinsert;
                                  Submit_Codigo('0');

                                  //Passando a quantidade de ferragem que tem no pedido
                                  Submit_Quantidade(fieldbyname('quantidade').asstring);
                                  //Passando o valor unitario de cada pedido
                                  Submit_Valor(fieldbyname('valor').asstring);

                                 IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                 IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                 IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                 IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                 CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                 IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                                 IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                 IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                                 Submit_BC_ICMS(Query.fieldbyname('BC_ICMS').AsString);
                                 Submit_BC_ICMS_ST(Query.fieldbyname('BC_ICMS_ST').AsString);
                                 Submit_VALOR_ICMS(Query.fieldbyname('VALOR_ICMS').AsString);
                                 Submit_VALOR_ICMS_ST(Query.fieldbyname('VALOR_ICMS_ST').AsString);

                                 Submit_BC_IPI(Query.fieldbyname('BC_IPI').AsString);
                                 Submit_VALOR_IPI(Query.fieldbyname('VALOR_IPI').AsString);

                                 Submit_BC_PIS(Query.fieldbyname('BC_PIS').AsString);
                                 Submit_VALOR_PIS(Query.fieldbyname('VALOR_IPI').AsString);
                                 Submit_BC_PIS_ST(Query.fieldbyname('BC_PIS_ST').AsString);
                                 Submit_VALOR_PIS_ST(Query.fieldbyname('VALOR_PIS_ST').AsString);

                                 Submit_BC_COFINS(Query.fieldbyname('BC_COFINS').AsString);
                                 Submit_VALOR_COFINS(Query.fieldbyname('VALOR_COFINS').AsString);
                                 Submit_BC_COFINS_ST(Query.fieldbyname('BC_COFINS_ST').AsString);
                                 Submit_VALOR_COFINS_ST(Query.fieldbyname('VALOR_COFINS_ST').AsString);


                                 Submit_valorpauta(Query.fieldbyname('VALORPAUTA').AsString);
                                 Submit_ISENTO(Query.fieldbyname('ISENTO').AsString);
                                 Submit_SUBSTITUICAOTRIBUTARIA(Query.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
                                 Submit_ALIQUOTA(Query.fieldbyname('ALIQUOTA').AsString);
                                 Submit_REDUCAOBASECALCULO(Query.fieldbyname('REDUCAOBASECALCULO').AsString);
                                 Submit_ALIQUOTACUPOM(Query.fieldbyname('ALIQUOTACUPOM').AsString);
                                 Submit_IPI('0');
                                 Submit_percentualagregado(Query.fieldbyname('PERCENTUALAGREGADO').AsString);
                                 Submit_Margemvaloragregadoconsumidor(Query.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
                                 SituacaoTributaria_TabelaA.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
                                 SituacaoTributaria_TabelaB.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
                                         //*********************************


                                 Submit_valorfrete(Query.fieldbyname('valorfrete').AsString);
                                 Submit_valorseguro(Query.fieldbyname('valorseguro').AsString);

                                 Submit_referencia('');
                                 Submit_desconto(virgulaparaponto(Query.fieldbyname('desconto').AsString));
                                 CSOSN.Submit_codigo(Query.fieldbyname('csosn').AsString);
                                 submit_UNIDADE(Query.FieldByName('unidade').AsString);
                                 if (Salvar(False)=False)
                                 Then Begin
                                           messagedlg('N�o foi poss�vel Gravar o Perfilado',mterror,[mbok],0);
                                           exit;
                                 End;
                             end;
                        End;

                        next;//pr�ximo perfilado do pedido
                   End;//while

                finally
                      ObjPerfilado_NFlocal.Free;
                end;


                try

                   //***************PERSIANA*******************************
                   close;
                   sql.clear;
                   sql.add('Select TABPERSIANA_PP.* from TABPERSIANA_PP');
                   sql.add('join tabpedido_proj on TABPERSIANA_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where tabpedido_proj.pedido='+Ppedido);
                   open;

                   while not(eof) do
                   Begin
                        With ObjPersiana_nflocal do
                        Begin
                             ZerarTabela;
                             if (persianagrupodiametrocor.LocalizaCodigo(fieldbyname('persianagrupodiametrocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Persiana nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             persianagrupodiametrocor.TabelaparaObjeto;
                             if (notafiscal.LocalizaCodigo(pnota)=False)
                             Then Begin
                                               mensagemerro('Nota Fiscal n�o encontrada');
                                               exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Status:=dsinsert;
                             Submit_Codigo('0');
                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+persianagrupodiametrocor.Persiana.Get_Codigo);
                             Query.SQL.Add('and material = 5');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.SQL.Add('and notafiscal='+Pnota);
                             Query.Open;
                             if(Query.RecordCount>0)
                             then begin
                                    status:=dsInactive;
                                    Status:=dsinsert;
                                    Submit_Codigo('0');

                                    //Passando a quantidade de ferragem que tem no pedido
                                    Submit_Quantidade(fieldbyname('quantidade').asstring);
                                    //Passando o valor unitario de cada pedido
                                    Submit_Valor(fieldbyname('valor').asstring);
                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                   IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                   CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                                   Submit_BC_ICMS(Query.fieldbyname('BC_ICMS').AsString);
                                   Submit_BC_ICMS_ST(Query.fieldbyname('BC_ICMS_ST').AsString);
                                   Submit_VALOR_ICMS(Query.fieldbyname('VALOR_ICMS').AsString);
                                   Submit_VALOR_ICMS_ST(Query.fieldbyname('VALOR_ICMS_ST').AsString);

                                   Submit_BC_IPI(Query.fieldbyname('BC_IPI').AsString);
                                   Submit_VALOR_IPI(Query.fieldbyname('VALOR_IPI').AsString);

                                   Submit_BC_PIS(Query.fieldbyname('BC_PIS').AsString);
                                   Submit_VALOR_PIS(Query.fieldbyname('VALOR_IPI').AsString);
                                   Submit_BC_PIS_ST(Query.fieldbyname('BC_PIS_ST').AsString);
                                   Submit_VALOR_PIS_ST(Query.fieldbyname('VALOR_PIS_ST').AsString);

                                   Submit_BC_COFINS(Query.fieldbyname('BC_COFINS').AsString);
                                   Submit_VALOR_COFINS(Query.fieldbyname('VALOR_COFINS').AsString);
                                   Submit_BC_COFINS_ST(Query.fieldbyname('BC_COFINS_ST').AsString);
                                   Submit_VALOR_COFINS_ST(Query.fieldbyname('VALOR_COFINS_ST').AsString);


                                   Submit_valorpauta(Query.fieldbyname('VALORPAUTA').AsString);
                                   Submit_ISENTO(Query.fieldbyname('ISENTO').AsString);
                                   Submit_SUBSTITUICAOTRIBUTARIA(Query.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
                                   Submit_ALIQUOTA(Query.fieldbyname('ALIQUOTA').AsString);
                                   Submit_REDUCAOBASECALCULO(Query.fieldbyname('REDUCAOBASECALCULO').AsString);
                                   Submit_ALIQUOTACUPOM(Query.fieldbyname('ALIQUOTACUPOM').AsString);
                                   Submit_IPI('0');
                                   Submit_percentualagregado(Query.fieldbyname('PERCENTUALAGREGADO').AsString);
                                   Submit_Margemvaloragregadoconsumidor(Query.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
                                   SituacaoTributaria_TabelaA.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
                                   SituacaoTributaria_TabelaB.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
                                           //*********************************


                                   Submit_valorfrete(Query.fieldbyname('valorfrete').AsString);
                                   Submit_valorseguro(Query.fieldbyname('valorseguro').AsString);

                                   Submit_desconto(virgulaparaponto(Query.fieldbyname('desconto').AsString));
                                   CSOSN.Submit_codigo(Query.fieldbyname('csosn').AsString);
                                   Submit_referencia('');
                                   submit_UNIDADE(Query.FieldByName('unidade').AsString);
                                   if (Salvar(False)=False)
                                   Then Begin
                                             messagedlg('N�o foi poss�vel Gravar a Persiana',mterror,[mbok],0);
                                             exit;
                                   End;
                             end;
                        End;

                        next;//pr�xima persiana
                   End;

                finally
                    ObjPersiana_NFlocal.Free;
                end;


                   //***************VIDRO*******************************
                   close;
                   sql.clear;
                   sql.add('Select TABVIDRO_PP.* from TABVIDRO_PP');
                   sql.add('join tabpedido_proj on TABVIDRO_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where tabpedido_proj.pedido='+Ppedido);
                   open;
                try

                   while not(eof) do
                   Begin
                        With ObjVidro_nflocal do
                        Begin
                             ZerarTabela;
                             if (VidroCor.LocalizaCodigo(fieldbyname('vidrocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Vidro nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             VidroCor.TabelaparaObjeto;
                             if (notafiscal.LocalizaCodigo(pnota)=False)
                             Then Begin
                                               mensagemerro('Nota Fiscal n�o encontrada');
                                               exit;
                             End;
                             notafiscal.TabelaparaObjeto;        

                             Status:=dsinsert;
                             Submit_Codigo('0');

                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+Vidrocor.Vidro.Get_Codigo);
                             Query.SQL.Add('and material = 3');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.SQL.Add('and notafiscal='+Pnota);
                             Query.Open;
                             if(Query.RecordCount>0)
                             then begin
                                    status:=dsInactive;
                                    Status:=dsinsert;
                                    Submit_Codigo('0');

                                    //Passando a quantidade de ferragem que tem no pedido
                                    Submit_Quantidade(fieldbyname('quantidade').asstring);
                                    //Passando o valor unitario de cada pedido
                                    Submit_Valor(fieldbyname('valor').asstring);


                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                   IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                   CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                                   Submit_BC_ICMS(Query.fieldbyname('BC_ICMS').AsString);
                                   Submit_BC_ICMS_ST(Query.fieldbyname('BC_ICMS_ST').AsString);
                                   Submit_VALOR_ICMS(Query.fieldbyname('VALOR_ICMS').AsString);
                                   Submit_VALOR_ICMS_ST(Query.fieldbyname('VALOR_ICMS_ST').AsString);

                                   Submit_BC_IPI(Query.fieldbyname('BC_IPI').AsString);
                                   Submit_VALOR_IPI(Query.fieldbyname('VALOR_IPI').AsString);

                                   Submit_BC_PIS(Query.fieldbyname('BC_PIS').AsString);
                                   Submit_VALOR_PIS(Query.fieldbyname('VALOR_IPI').AsString);
                                   Submit_BC_PIS_ST(Query.fieldbyname('BC_PIS_ST').AsString);
                                   Submit_VALOR_PIS_ST(Query.fieldbyname('VALOR_PIS_ST').AsString);

                                   Submit_BC_COFINS(Query.fieldbyname('BC_COFINS').AsString);
                                   Submit_VALOR_COFINS(Query.fieldbyname('VALOR_COFINS').AsString);
                                   Submit_BC_COFINS_ST(Query.fieldbyname('BC_COFINS_ST').AsString);
                                   Submit_VALOR_COFINS_ST(Query.fieldbyname('VALOR_COFINS_ST').AsString);


                                   Submit_valorpauta(Query.fieldbyname('VALORPAUTA').AsString);
                                   Submit_ISENTO(Query.fieldbyname('ISENTO').AsString);
                                   Submit_SUBSTITUICAOTRIBUTARIA(Query.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
                                   Submit_ALIQUOTA(Query.fieldbyname('ALIQUOTA').AsString);
                                   Submit_REDUCAOBASECALCULO(Query.fieldbyname('REDUCAOBASECALCULO').AsString);
                                   Submit_ALIQUOTACUPOM(Query.fieldbyname('ALIQUOTACUPOM').AsString);
                                   Submit_IPI('0');
                                   Submit_percentualagregado(Query.fieldbyname('PERCENTUALAGREGADO').AsString);
                                   Submit_Margemvaloragregadoconsumidor(Query.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
                                   SituacaoTributaria_TabelaA.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
                                   SituacaoTributaria_TabelaB.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
                                           //*********************************


                                   Submit_valorfrete(Query.fieldbyname('valorfrete').AsString);
                                   Submit_valorseguro(Query.fieldbyname('valorseguro').AsString);
                                   Submit_desconto(virgulaparaponto(Query.fieldbyname('desconto').AsString));
                                   CSOSN.Submit_codigo(Query.fieldbyname('csosn').AsString);
                                   submit_UNIDADE(Query.FieldByName('unidade').AsString);
                                   Submit_referencia('');

                                   if (Salvar(false)=False)
                                   Then Begin
                                             messagedlg('N�o foi poss�vel Gravar o Vidro',mterror,[mbok],0);
                                             exit;
                                   End;
                             end;
                        End;


                       next;//pr�ximo vidro
                   End;

                finally
                    ObjVidro_NFlocal.Free;
                end;

                   Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(Pnota);
                   Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
                   Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;

                    if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(True)=False)
                    then Begin
                              Mensagemerro('Erro na tentativa de Salvar o desconto na Nota Fiscal');
                              Self.LimpaNF(Pnota);
                              exit;
                    End;


              End;//With Self.ObjqueryTemp


  Finally
            FDataModulo.IBTransaction.RollbackRetaining;
            ObjPedidoObjetos.Free;
            FreeAndNil(Query);
  End;

end;

function TObjNotafiscalObjetos.get_proximaNFE: string;
begin

  with (self.Objquery) do
  begin

   Close;
   SQL.Clear;
   SQL.Add ('select first 1 codigo,numero');
   SQL.Add ('from tabnotafiscal');
   SQL.Add ('where upper (situacao) = ''N'' and modelo_nf = 2');
   SQL.Add('order by codigo');

   Open;
   First;
   result := fieldbyname('codigo').AsString;

  end;

end;

{todo: begin nfe}
function TObjNotafiscalObjetos.geraNFE_2(Pnota:TStringList; pedido: string;PmodoContigencia, PForcaProcessamento: boolean;PnfeDigitada:string): boolean;
var
  PObjNotaFiscal:TObjNotaFiscal;
  infoRef,precibo,cstaux,pNfe:string;
  ST:Boolean;
  statusNfe:Integer;
  QueryDup:TIBQuery;
  QuantidadeParcelas:Integer;

  {icms st}
  totalitens_vBCST,totalItens_vICMS_ST,
  valorICMS_ST,baseCalcICMS_ST:Currency;

  {icms}
  totalItens_vBC,totalItens_vICMS,
  valorICMS,baseCalcICMS:Currency;

  {pis}
  totalItens_vBCPIS, baseCalcPIS,
  totalItens_vPIS,valorPIS:Currency;

  {pis st}
  totalItens_vBCPIS_ST,baseCalcPIS_ST,
  totalItens_vPIS_ST,valorPIS_ST:Currency;

  {cofins st}
  totalItens_vBCCOFINS_ST,baseCalcCOFINS_ST,
  totalItens_vCOFINS_ST,valorCOFINS_ST:Currency;

  {cofins}
  totalItens_vBCCOFINS,baseCalcCOFINS,
  totalItens_vCOFINS,valorCOFINS:Currency;

  {desconto}
  totalDesconto:Currency;

  {total produto}
  valorTotalProduto:Currency;

  {total seguro}
  valorTotalSeguro:Currency;

  {total frete}
  valorTotalFrete:Currency;

  GerarPorProjetos:string;
begin

  
  (*result:=False;
  infoRef := '';

  PObjNotaFiscal:=TObjNotaFiscal.Create;
  QueryDup:=TIBQuery.Create(nil);
  QueryDup.Database:=FDataModulo.IBDatabase;

  try

    try

      If not (PObjNotaFiscal.LocalizaCodigo (Pnota[0])) then
      begin

        MensagemAviso ('Nota Fiscal n�o Localizada!');
        Exit;

      end;

      PObjNotaFiscal.TabelaparaObjeto;
      Pnfe:=PObjNotaFiscal.Get_Numero();


      if not(self.Valida_dados_NFE(PObjnotaFiscal,pedido,PnfeDigitada)) then
        Exit;

      if (PObjnotaFiscal.Nfe.Get_codigo <> '') then
      begin

        mensagemErro('A nota fiscal ja possui uma NFE, n�o � poss�vel gerar outra NFe � partir dela');
        Exit;

      end;

      if not (PObjnotaFiscal.Nfe.LocalizaCodigo(Pnfe)) then
      begin

        MensagemAviso('NF-e Fiscal n�o localizada');
        exit;

      end;

      PObjnotaFiscal.Nfe.TabelaparaObjeto;

      if (PObjnotaFiscal.Nfe.Get_StatusNota <> 'A') then
      begin

        MensagemAviso('A NF-e n�o se encontra aberta para uso. Status Atual '+PObjnotaFiscal.Nfe.Get_STATUSNOTA ());
        exit;

      end;

      if (self.devolucao) then
      begin

            if (Self.refNFe_ref <> '') then {� referencia de nfe, ent�o tem que preencher com a chave de acesso}
            begin

              {para passar uma nfe como referencia a mesma deve estar autorizada,
              n�o podendo estar: cancelada, unitilizada, etc}

              {No caso de localiza��o da NF-e retornar o cStat com os valores �100-Autorizado o Uso�,
              �101-Cancelamento de NF-e Homologado� ou �110-Uso Denegado�.}

              {try
                screen.Cursor:=crHourGlass;
                statusNFE := objgeranfe.consulta_por_chaveAcesso (self.refNFe_ref,false);
              finally
                screen.Cursor:=crDefault;
              end;

              if (statusNFE <> 100) then
              begin

                MensagemAviso ('A NF-e de refer�ncia deve estar autorizada');
                Exit;

              end;     }

              self.objgeranfe.ConfiguraComponente;
              FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
              FComponentesNfe.ACBrNFe.NotasFiscais.Add;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.NFref.Add;

              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.NFref.Items[0].refNFe:=Self.refNFe_ref;

              infoRef := 'Devolu�ao referente a NF-e: '+self.refNFe_ref+#13;

            end else if (self.nNF_ref <> '') then {� referencia de NF modelo 1}
            begin


              self.objgeranfe.ConfiguraComponente;
              FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
              FComponentesNfe.ACBrNFe.NotasFiscais.Add;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.NFref.Add;

              with FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.NFref.Items[0].RefNF do
              begin

                cUF    := StrToInt(self.cUF_ref);
                AAMM   := FormatDateTime('yymm',StrToDate (self.AAMM_ref));
                CNPJ   := RetornaSoNumeros(self.CNPJ_ref);
                modelo := StrToInt(self.modelo_ref);
                serie  := StrToInt(Self.serie_ref);
                nNF    := StrToInt(self.nNF_ref);

                infoRef := 'Devolu��o referente a NF: '+Self.nNF_ref+#13;

              end;

            end;

      end else
      begin

            self.objgeranfe.ConfiguraComponente;
            FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
            FComponentesNfe.ACBrNFe.NotasFiscais.Add;

      end;


      {try
        self.objgeranfe.ConfiguraComponente;
      except
        Exit;
      end;
      FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
      FComponentesNfe.ACBrNFe.NotasFiscais.Add;}

      FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie:=ObjEmpresaGlobal.Get_certificado ();

      if (PmodoContigencia) then
        FComponentesNfe.ACBrNFe.Configuracoes.Geral.FormaEmissao :=  teFSDA;

      {IDE NF-e}
      try

        if (PnfeDigitada = '') then
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.natOp       := LimitaPalavra(get_campoTabela('CFOP','pedido','viewprodutosvenda',pedido),60)
        else
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.natOp       := LimitaPalavra(get_campoTabela('CFOP','nfedigitada','viewprodutosvenda',PnfeDigitada),60);

        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.cNF         := StrToInt (pNfe);
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.nNF         := StrToInt (pNfe);
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.dEmi        := StrToDateTime (PObjnotaFiscal.Get_dataemissao);
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.serie       := 1;
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.cUF         := StrToInt(ObjEmpresaGlobal.Get_codigoestado);
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.cMunFG      := StrToInt(ObjEmpresaGlobal.Get_codigocidade);
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.indPag      := ipOutras;

        if (NfeEntrada) then
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.tpNF     := tnEntrada
        else
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.tpNF     := tnSaida;

      except
        MensagemAviso('Erro ao prencher dados da IDE NF-e');
        Exit;
      end;

      {informa��es complementares da NF-e}
      FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.InfAdic.infCpl  :=  infoRef+PObjnotaFiscal.Get_DadosAdicionais ();

      {EMITENTE}
      try

        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.CNPJCPF           := RetornaSoNumeros(ObjEmpresaGlobal.Get_CNPJ);
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.IE                := ObjEmpresaGlobal.Get_IE;
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.IEST              :='';
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.xNome             := LimitaPalavra (ObjEmpresaGlobal.Get_RAZAOSOCIAL,60);
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.xFant             := LimitaPalavra (ObjEmpresaGlobal.Get_FANTASIA,60);
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.EnderEmit.fone    := ObjEmpresaGlobal.Get_FONE;
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.EnderEmit.CEP     := StrToint(retornasonumeros(ObjEmpresaGlobal.Get_CEP));
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.EnderEmit.xLgr    := LimitaPalavra(ObjEmpresaGlobal.Get_ENDERECO,60);
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.EnderEmit.nro     := ObjEmpresaGlobal.Get_Numero;
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.EnderEmit.xCpl    := LimitaPalavra(ObjEmpresaGlobal.Get_COMPLEMENTO,60);
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.EnderEmit.xBairro := LimitaPalavra(ObjEmpresaGlobal.get_bairro,60);
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.EnderEmit.cMun    := StrToInt(ObjEmpresaGlobal.get_codigoCidade);
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.EnderEmit.xMun    := ObjEmpresaGlobal.Get_CIDADE;
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.EnderEmit.UF      := ObjEmpresaGlobal.Get_ESTADO;
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.enderEmit.cPais      := 1058;
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.enderEmit.xPais      := 'BRASIL';

        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.CRT:=get_crt(ObjEmpresaGlobal.CRT.Get_Codigo);

        {if (ObjEmpresaGlobal.get_Simples() = 'S') then
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.CRT:=crtSimplesNacional
        else
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.CRT:=crtRegimeNormal;}


      except

        MensagemAviso('Erro ao preencher dados do emitente');
        Exit;

      end;

      {DESTINATARIO - pode ser um cliente ou um fornecedor}

     if (PObjNotaFiscal.Cliente.Get_CODIGO () <> '') then
      begin

        try

          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.CNPJCPF           := LimitaPalavra (RetornaSoNUmeros(PObjnotaFiscal.Cliente.Get_Cpf_CGC),14);
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.CEP     := strtoint(RetornaSoNUmeros(PObjnotaFiscal.Cliente.Get_Cep));
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.xLgr    := LimitaPalavra(PObjnotaFiscal.Cliente.Get_Endereco,60);

          if (Trim(PObjNotaFiscal.Cliente.Get_Numero) <> '') then
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.nro     := PObjnotaFiscal.Cliente.get_numero;

          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.xBairro := LimitaPalavra(PObjnotaFiscal.Cliente.Get_Bairro,60);
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.cMun    := strtoint(PObjnotaFiscal.Cliente.Get_codigocidade);
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.xMun    := LimitaPalavra(PObjnotaFiscal.Cliente.Get_Cidade,60);
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.UF      := PObjnotaFiscal.Cliente.Get_Estado;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.Fone    := PObjnotaFiscal.Cliente.Get_Fone;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.xNome             := LimitaPalavra(PObjnotaFiscal.Cliente.Get_Nome,60);
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.cPais   := 1058;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.xPais   := 'BRASIL';

           if(PObjnotaFiscal.Cliente.Get_Fisica_Juridica='J') then
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.IE := PObjnotaFiscal.Cliente.Get_RG_IE
           else
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.IE  := 'ISENTO';

        except

          MensagemAviso('Erro ao preencher dados do destinatario');
          Exit;

        end;
    
      end else
      if (PObjNotaFiscal.Fornecedor.Get_CODIGO () <> '') then
      begin

        try

          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.CNPJCPF           := LimitaPalavra (RetornaSoNUmeros(PObjnotaFiscal.Fornecedor.Get_CGC),14);
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.CEP     := strtoint(RetornaSoNUmeros(PObjnotaFiscal.Fornecedor.Get_CEP));
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.xLgr    := LimitaPalavra(PObjnotaFiscal.Fornecedor.Get_Endereco,60);

          if (Trim(PObjNotaFiscal.Fornecedor.Get_Numero) <> '') then
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.nro     := PObjnotaFiscal.Fornecedor.Get_Numero;

          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.xBairro := LimitaPalavra(PObjnotaFiscal.Fornecedor.Get_Bairro,60);
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.cMun    := strtoint(PObjnotaFiscal.Fornecedor.Get_codigocidade);
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.xMun    := LimitaPalavra(PObjnotaFiscal.Fornecedor.Get_Cidade,60);
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.UF      := PObjnotaFiscal.Fornecedor.Get_Estado;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.Fone    := PObjnotaFiscal.Fornecedor.Get_Fone;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.xNome             := LimitaPalavra(PObjnotaFiscal.Fornecedor.Get_RazaoSocial,60);
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.cPais   := 1058;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest.xPais   := 'BRASIL';

           if(PObjnotaFiscal.Fornecedor.Get_IE <> ''  ) then
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.IE := PObjnotaFiscal.Fornecedor.Get_IE
           else
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.IE  := 'ISENTO';

        except
          MensagemAviso('Erro ao preencher dados do destinatario');
          Exit;
        end

      end else
      begin

        MensagemErro ('Escolha um cliente ou funcion�rio');

      end;

       {TRANSPORTADORA}

       if (PObjnotaFiscal.Get_FreteporContaTransportadora = '2') then
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.modFrete:=mfContaDestinatario
       else
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.modFrete:=mfContaEmitente;

       FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Transporta.CNPJCPF :=PObjnotaFiscal.Get_CNPJTransportadora;
       FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Transporta.xNome   :=PObjnotaFiscal.Get_NomeTransportadora;
       FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Transporta.IE      :=PObjnotaFiscal.Get_IETransportadora;
       FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Transporta.xEnder  :=PObjnotaFiscal.Get_EnderecoTransportadora;
       FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Transporta.xMun    :=PObjnotaFiscal.Get_MunicipioTransportadora;
       FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Transporta.UF      :=PObjnotaFiscal.Get_UFTransportadora;
       FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.veicTransp.placa   :=PObjnotaFiscal.Get_PlacaVeiculoTransportadora;
       FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.veicTransp.UF      :=PObjnotaFiscal.Get_UFVeiculoTransportadora;

       {VOLUMES TRANSPORTADOS}

       try

         FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Vol.Add;

         if (PObjnotaFiscal.Get_Quantidade () <> '') then
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Vol.Items[0].qVol  := StrToInt (PObjnotaFiscal.Get_Quantidade)
         else
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Vol.Items[0].qVol := 0;

         FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Vol.Items[0].esp   := PObjnotaFiscal.Get_Especie;
         FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Vol.Items[0].marca := PObjnotaFiscal.Get_Marca;
         FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Vol.Items[0].nVol  := PObjnotaFiscal.Get_NumeroVolumes;

         if (PObjnotaFiscal.Get_PesoBruto () <> '') then
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Vol.Items[0].pesoB := StrToCurr (PObjnotaFiscal.Get_PesoBruto)
         else
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Vol.Items[0].pesoB := 0;

         if (PObjnotaFiscal.Get_PesoLiquido () <> '') then
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Vol.Items[0].pesoL := StrToCurr (PObjnotaFiscal.Get_PesoLiquido)
         else
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Vol.Items[0].pesoL := 0;

       except

         MensagemAviso('Erro ao preencher dados dos volumes transportados');
         Exit;

       end;

       {Jonatan FATURA/DUPLICATA}
       if(pedido<>'')then
       begin
         try

            queryDup.Active := False;
            queryDup.SQL.Text :=

            'select t.codigo as nfat, p.nome as nomefat, t.valor as vfat, d.codigo as ndup, d.vencimento as ddup, d.saldo as vdup, p.parcelas '+
            'from tabpedido v '+
            'inner join tabtitulo t on (t.codigo = v.titulo) '+
            'inner join tabprazopagamento p on (p.codigo = t.prazo) '+
            'inner join tabpendencia d on (d.titulo = t.codigo) '+
            'where v.codigo = '+pedido+' '+
            'order by d.vencimento';

            queryDup.Active := True;
            QueryDup.Last;
            QuantidadeParcelas:=QueryDup.RecordCount;
            queryDup.First;

            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Cobr.Fat.nFat  := queryDup.fieldbyname('nfat').AsString;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Cobr.Fat.vOrig := queryDup.fieldbyname('vfat').AsCurrency;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Cobr.Fat.vDesc := 0;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Cobr.Fat.vLiq  := queryDup.fieldbyname('vfat').AsCurrency;

            //if queryDup.FieldByName('parcelas').AsString <> '1' then
            if QuantidadeParcelas> 1 then
            begin
              while not queryDup.Eof do
              begin
                FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Cobr.Dup.Add;
                FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Cobr.Dup.Items[queryDup.RecNo-1].nDup := queryDup.fieldbyname('ndup').AsString;
                FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Cobr.Dup.Items[queryDup.RecNo-1].dVenc:= queryDup.fieldbyname('ddup').AsDateTime;
                FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Cobr.Dup.Items[queryDup.RecNo-1].vDup := queryDup.fieldbyname('vdup').AsCurrency;
                queryDup.Next;
              end;
            end;

            {queryDup.First;
            with CobrDuplicata.COBRFATURA do
            begin
              ZerarTabela;
              Status := dsInsert;
              pCodigoCobrFatura := CobrDuplicata.COBRFATURA.Get_NovoCodigo;
              Submit_Codigo(pCodigoCobrFatura);
              Submit_nFat(queryDup.fieldbyname('nfat').AsString);
              Submit_vOrig(queryDup.fieldbyname('vfat').AsString);
              Submit_vDesc('0');
              Submit_vLiq(queryDup.fieldbyname('vfat').AsString);
              CobrDuplicata.COBRFATURA.notafiscal.Submit_CODIGO(strCodigoNotas[0]);
              if not Salvar(false) then
              begin
                MensagemErro('Erro ao gravar Cobran�a fatura');
                exit;
              end
            end;

            if QuantidadeParcelas> 1 then
            begin
              while not queryDup.Eof do
              begin
                CobrDuplicata.ZerarTabela;
                CobrDuplicata.Status := dsInsert;
                CobrDuplicata.Submit_Codigo('0');
                CobrDuplicata.Submit_nDup(queryDup.fieldbyname('ndup').AsString);
                CobrDuplicata.Submit_dVenc(queryDup.fieldbyname('ddup').AsString);
                CobrDuplicata.Submit_vDup(queryDup.fieldbyname('vdup').AsString);
                CobrDuplicata.COBRFATURA.Submit_Codigo(pCodigoCobrFatura);
                if not CobrDuplicata.Salvar(false) then
                begin
                  MensagemErro('Erro ao gravar Cobran�a duplicata');
                  exit;
                end;
                queryDup.Next;
              end;
            end;}
         except
            MensagemAviso('Erro ao preencher as faturas!');
            Exit;
         end;
       end;

      {jonatan}
      if(ObjParametroGlobal.ValidaParametro('VENDER PROJETOS COMO PRODUTOS')=false) then
      begin
              MensagemErro('Paramentro "VENDER PROJETOS COMO PRODUTOS" n�o encontrado');
              GerarPorProjetos:='NAO';
      end
      else GerarPorProjetos:=ObjParametroGlobal.Get_Valor;
      {jonatan}

       {PRODUTOS}
       with self.Objquery do
       begin

        active:=false;
        SQL.Clear;


        {jonatan}
        if (PnfeDigitada = '') then
        begin

          if(GerarPorProjetos='NAO') then
          begin
                  sql.Add('select * from viewprodutosvenda where pedido='+pedido) ;
                  //SQL.Add('and codigoproduto is not null') ; //se for um projeto que esta gravado, n�o posso pegar ele
          end
          else
          begin
                   sql.Add('select * from viewprodutosvenda where pedido='+pedido) ;
                  // SQL.Add('and codigoproduto is null') ; //esta pra gerar por projetos, ent�o pego o projeto e nao os materiais
          end;

        end else
        begin

          SQL.Add('select *');
          SQL.Add('from VIEWPRODUTOSVENDA');
          SQL.Add('where nfedigitada = '+PnfeDigitada);

        end;

        Active:=True;
        First;

        while not (eof) do
        begin

          if (FieldByName('SUBSTITUICAOTRIBUTARIA').AsString = 'S') then
            ST:=True
          else
            ST:=False;

          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Add;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.nItem := RecNo;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.CFOP   := FieldByName('CFOP').AsString;

          if (GerarPorProjetos = 'NAO') then
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.cProd  := FieldByName('CODIGOPRODUTO').AsString
          else
          BEGIN
               if(FieldByName('CODIGOPRODUTO').AsString<>'')
               then FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.cProd  := FieldByName('CODIGOPRODUTO').AsString
               else FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.cProd  := FieldByName('PROJETO').AsString;
          end;


          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.xProd  := LimitaPalavra(FieldByName('DESCRICAO').asstring,50)+' - '+LimitaPalavra(FieldByName('COR').asstring,50);
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.qCom   := fieldbyname('QUANTIDADE').AsCurrency;

          if (FieldByName('UNIDADE').asstring <> '') then
          begin
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.uCom  := fieldbyname('UNIDADE').asstring;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.uTrib := Self.Objquery.fieldbyname('UNIDADE').asstring
          end else
          begin
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.uCom  := 'UN';
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.uTrib := 'UN';
          end;

          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.vUnCom  :=  fieldbyname('VALORUNITARIO').AsCurrency;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.vUnTrib :=  fieldbyname('VALORUNITARIO').AsCurrency;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.vProd   :=  fieldbyname('VALORUNITARIO').AsCurrency * fieldbyname('QUANTIDADE').AsCurrency;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.qTrib   :=  fieldbyname('QUANTIDADE').AsCurrency;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.vDesc   :=  fieldbyname('DESCONTO').AsCurrency;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.NCM     :=  RetornaSoNumeros(fieldbyname('NCM').asstring);
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.vFrete  :=  fieldbyname('VALORFRETE').AsCurrency;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.vSeg    :=  fieldbyname('VALORSEGURO').AsCurrency;


          {TOTAL DESCONTO}
          totalDesconto:=totalDesconto + fieldbyname('DESCONTO').AsCurrency;

          {TOTAL PRODUTO}
          valorTotalProduto:=valorTotalProduto + FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.vProd;

          {TOTAL FRETE}
          valorTotalFrete:=valorTotalFrete + FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.vFrete;

          {TOTAL SEGURO}
          valorTotalSeguro:=valorTotalSeguro + FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Prod.vSeg;

          {IMPOSTO}

          {icms | icms_st}
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.orig    := GET_ST_TABELA_A (self.Objquery.FieldByName('SITUACAOTRIBUTARIA_TABELAA').AsString);

          if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1]   .Imposto.ICMS.CSOSN   := get_TIPOCSOSN(FieldByName('CSOSN').AsString)

          else
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.CST     := GET_ST_TABELA_B (self.Objquery.FieldByName('SITUACAOTRIBUTARIA_TABELAB').AsString);


          cstaux := self.Objquery.FieldByName('SITUACAOTRIBUTARIA_TABELAB').AsString;

          baseCalcICMS_ST :=fieldbyname('BC_ICMS_ST').AsCurrency;
          valorICMS_ST    :=FieldByName('VALOR_ICMS_ST').AsCurrency;

          baseCalcICMS    :=fieldbyname('BC_ICMS').AsCurrency;
          valorICMS       :=FieldByName('VALOR_ICMS').AsCurrency;

           if (ST) then
           begin

            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.modBCST  := GET_MODALIDADE_BC_ICMS_ST (FieldByname('MODALIDADE_ST').AsInteger);
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.pMVAST   := FieldByName('MARGEMVALORAGREGADOCONSUMIDOR').AsCurrency;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.pRedBCST := FieldByName('REDUCAOBASECALCULO').AsCurrency;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.pICMSST  := FieldByName('ALIQUOTAST').AsCurrency;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.vBCST    := baseCalcICMS_ST;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.vICMSST  := valorICMS_ST;

            totalitens_vBCST := totalitens_vBCST + baseCalcICMS_ST;
            totalItens_vICMS_ST:=totalItens_vICMS_ST + valorICMS_ST;

           end else
           begin
       
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.modBC    := GET_MODALIDADE_BC (FieldByName('MODALIDADE').AsInteger);
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.pRedBC   := FieldByName('REDUCAOBASECALCULO').AsCurrency;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.vBC      := baseCalcICMS;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.pICMS := FieldByName('ALIQUOTA').AsCurrency;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.vICMS := valorICMS;

            totalItens_vBC := totalItens_vBC + baseCalcICMS;
            totalItens_vICMS:=totalItens_vICMS + valorICMS;

           end;

           {pis | pis_st}
           FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PIS.CST:=GET_CST_PIS(FieldByName('CST_PIS').AsInteger);

           baseCalcPIS    :=FieldByName('BC_PIS').AsCurrency;
           valorPIS       :=FieldByName('VALOR_PIS').AsCurrency;

           baseCalcPIS_ST :=FieldByName('BC_PIS_ST').AsCurrency;
           valorPIS_ST    :=FieldByName('VALOR_PIS').AsCurrency;

           if (ST) then
           begin

            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PISST.vBc       :=baseCalcPIS_ST;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PISST.vPIS      :=valorPIS_ST;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PISST.pPis      :=FieldByName('ALIQUOTA_PIS').AsCurrency;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PISST.vAliqProd :=0;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PISST.qBCProd   :=0;

            totalItens_vBCPIS_ST := totalItens_vBCPIS_ST + baseCalcPIS_ST;
            totalItens_vPIS_ST   := totalItens_vPIS_ST   + valorPIS_ST;

           end else
           begin

            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PIS.vBC       :=baseCalcPIS;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PIS.vPIS      :=valorPIS;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PIS.pPIS      :=FieldByName('ALIQUOTA_PIS').AsCurrency;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PIS.vAliqProd :=0;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PIS.qBCProd   :=0;

            totalItens_vBCPIS := totalItens_vBCPIS + baseCalcPIS;
            totalItens_vPIS   := totalItens_vPIS   + valorPIS;

           end;

           {cofins | cofins st}

           FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINS.CST := GET_CST_COFINS(FieldByName('CST_COFINS').AsInteger);
           baseCalcCOFINS    :=FieldByName('BC_COFINS').AsCurrency;
           valorCOFINS       :=FieldByName('VALOR_COFINS').AsCurrency;

           baseCalcCOFINS_ST :=FieldByName('BC_COFINS_ST').AsCurrency;
           valorCOFINS_ST    :=FieldByName('VALOR_COFINS_ST').AsCurrency;

           if (ST) then
           begin

            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINSST.vBC       :=baseCalcCOFINS_ST;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINSST.vCOFINS   :=valorCOFINS_ST;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINSST.pCOFINS   :=FieldByName('ALIQUOTA_COFINSST').AsCurrency;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINSST.qBCProd   :=0;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINSST.vAliqProd :=0;

            totalItens_vBCCOFINS_ST:=totalItens_vBCCOFINS_ST + baseCalcCOFINS_ST;
            totalItens_vCOFINS_ST := totalItens_vCOFINS_ST + valorCOFINS_ST;

           end else
           begin

            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINS.vBC       :=baseCalcCOFINS;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINS.vCOFINS   :=valorCOFINS;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINS.pCOFINS   :=FieldByName('ALIQUOTA_COFINS').AsCurrency;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINS.qBCProd   :=0;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINS.vAliqProd :=0;

            totalItens_vBCCOFINS:=totalItens_vBCCOFINS+baseCalcCOFINS;
            totalItens_vCOFINS := totalItens_vCOFINS + valorCOFINS; 

           end;


           if (ObjEmpresaGlobal.get_Simples () = 'S') then
           begin

            if (ST) then
            begin

              {ICMS ST}

              if (cstaux <> '10') and(cstaux <> '30') and (cstaux <> '60') then
              begin

                FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.pMVAST   := 0;
                FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.pRedBCST := 0;
                FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.pICMSST  := 0;
                FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.vBCST    := 0;
                FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.vICMSST  := 0;
                totalitens_vBCST    := 0;
                totalItens_vICMS_ST := 0;

              end;

              {PIS ST}
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PISST.vBc       :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PISST.pPis      :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PISST.vPIS      :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PISST.vAliqProd :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PISST.qBCProd   :=0;

              totalItens_vBCPIS_ST := 0;

              {COFINS ST}
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINSST.vBC       :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINSST.vCOFINS   :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINSST.pCOFINS   :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINSST.qBCProd   :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINSST.vAliqProd :=0;
              totalItens_vBCCOFINS_ST:=0;

            end else
            begin

              {ICMS}
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.pRedBC   := 0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.vBC      := 0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.pICMS := 0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.ICMS.vICMS := 0;
              totalItens_vBC  := 0;
              totalItens_vICMS:=0;

              {PIS}
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PIS.vBC       :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PIS.pPIS      :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PIS.vPIS      :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PIS.vAliqProd :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PIS.qBCProd   :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.PIS.CST       :=pis99;
              totalItens_vBCPIS := 0;;

              {COFINS}
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINS.vBC       :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINS.vCOFINS   :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINS.pCOFINS   :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINS.qBCProd   :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINS.vAliqProd :=0;
              FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det.Items[RecNo-1].Imposto.COFINS.CST       :=cof99;
              totalItens_vBCCOFINS:=0;

            end;

           end;

          Next;
        end;

        {TOTAIS}

        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total.ICMSTot.vDesc := totalDesconto;
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total.ICMSTot.vProd := valorTotalProduto;
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total.ICMSTot.vNF   := (valorTotalProduto - totalDesconto) + StrToCurr(PObjNotaFiscal.Get_VALORTOTALIPI) + StrToCurr(PObjNotaFiscal.Get_VALORFRETE) + StrToCurr(PObjNotaFiscal.Get_VALORICMS_SUBST_recolhido);


        {ICMS}
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total.ICMSTot.vBC:= totalItens_vBC;
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total.ICMSTot.vICMS := totalItens_vICMS;

        {ICMS - ST}
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total.ICMSTot.vBCST := totalitens_vBCST;
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total.ICMSTot.vST   := totalItens_vICMS_ST;

        {PIS}
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total.ICMSTot.vPIS :=  totalItens_vPIS;

        {CALCULO DO COFINS}
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total.ICMSTot.vCOFINS := totalItens_vCOFINS;

        {SEGURO}
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total.ICMSTot.vSeg := valorTotalSeguro;

        {FRETE}
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total.ICMSTot.vFrete := valorTotalFrete;

        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total.ICMSTot.vOutro :=0;
        FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total.ICMSTot.vII:=0;


       end;

       try

        FcomponentesNfe.ACBrNFe.NotasFiscais.Assinar;
        FcomponentesNfe.ACBrNFe.NotasFiscais.Valida;

       except

        on e:exception do
        Begin

          MensagemErro('Erro na tentativa de Assinar e validar'+#13+E.message);
          FDataModulo.IBTransaction.RollbackRetaining;
          exit;

        End;

       end;

       if not (PmodoContigencia) then
       begin

        try

          try

           Screen.Cursor:=crHourGlass;

           Application.ProcessMessages;

           if not (FComponentesNfe.ACBrNFe.WebServices.StatusServico.Executar) then
            raise Exception.Create(FcomponentesNfe.ACBrNFe.WebServices.StatusServico.Msg);

          finally
            Screen.Cursor:=crDefault;
          end;

           FComponentesNfe.ACBrNFe.WebServices.Enviar.Lote := '0';

          try

            Screen.Cursor := crHourGlass;

            ObjParametroGlobal.ValidaParametro('NFE XML DE BACKUP');

            Application.ProcessMessages;

            if not (FComponentesNfe.ACBrNFe.WebServices.Enviar.Executar(ObjParametroGlobal.Get_Valor ())) then
               raise Exception.Create(FcomponentesNfe.ACBrNFe.WebServices.Enviar.Msg);

          finally

            Screen.Cursor := crDefault;
        
          end;

        except

          on e:Exception do
          Begin

            mensagemerro('Erro na tentativa de enviar a NFE'+#13+E.message);
            exit;
          end;

        end;

        try

          precibo := FComponentesNfe.ACBrNFe.WebServices.Enviar.Recibo;

          if (PForcaProcessamento = False) then
          begin

            FComponentesNfe.ACBrNFe.WebServices.Retorno.Recibo := FComponentesNfe.ACBrNFe.WebServices.Enviar.Recibo;

            try

               Screen.Cursor:=crHourGlass;

               try
                     if not (FComponentesNfe.ACBrNFe.WebServices.Retorno.Executar) then
                     raise Exception.Create(FComponentesNfe.ACBrNFe.WebServices.Retorno.Msg);
               except

                    MensagemErro(FComponentesNfe.ACBrNFe.WebServices.Retorno.Msg);
                    FDataModulo.IBTransaction.RollbackRetaining;
                    exit;
               end;
                          
            finally
               Screen.Cursor:=crDefault;
            end;

          end else
          begin

            raise Exception.Create('Lote de processamento for�ado pelo sistema Safira');

          end;

        except

          on e:Exception do
          begin

            {O Stat 105 � lote em processamento algum mano loko leu no forum que teve casos de lote em processamento com status 0 por isso verifiquei a frase lote em processamento}
            if ( (FcomponentesNfe.ACBrNFe.WebServices.Retorno.cStat = 105) or (PForcaProcessamento) or (pos ('LOTE EM PROCESSAMENTO',uppercase (e.message))>0)) then
            begin

                MensagemErro('O Lote foi enviado mas continua em processamento, tente novamente mais tarde apenas para confirmar esta NF');

                PObjNotaFiscal.Nfe.Status := dsEdit;
                PObjNotaFiscal.Nfe.Submit_STATUSNOTA('P');//processando
                PObjNotaFiscal.Nfe.Submit_ARQUIVO (objgeranfe.LocalArquivos+StringReplace (FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID,'Nfe','',[rfIgnoreCase])+'-nfe.xml');
                PObjNotaFiscal.Nfe.submit_chave_acesso (StringReplace (FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID,'Nfe','',[rfIgnoreCase]));
                PObjNotaFiscal.Nfe.submit_arquivo_xml(PObjnotaFiscal.Nfe.GET_CAMINHO_DOS_ARQUIVOS_XML+StringReplace (FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID,'Nfe','',[rfIgnoreCase])+'-NFe.xml');
                //ShowMessage (PObjnotaFiscal.Nfe.GET_CAMINHO_DOS_ARQUIVOS_XML + FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID+'-NFe');

                PObjNotaFiscal.Nfe.Submit_ReciboEnvio (precibo);

                 if (PObjNotaFiscal.Nfe.Salvar(false)=False) then
                 begin

                    MensagemErro('N�o foi poss�vel alterar o status  para "PROCESSANDO"  e o Recibo na nota ID '+pnfe+' cancele a Nfe manualmente');
                    exit;

                 end;

                 {marcando que esta NF tem uma NFE mas esta em Processamento}
                 if (PObjnotaFiscal.LocalizaCodigo(Pnota[0])=False) then
                 begin

                    Messagedlg('Nota Fiscal n�o Localizada!',mterror,[mbok],0);
                    exit;

                 end;

                 PObjnotaFiscal.TabelaparaObjeto;
                 PObjnotaFiscal.Status:=dsEdit;
                 PObjNotaFiscal.Nfe.Submit_CODIGO(pnfe);
                 PObjNotaFiscal.Submit_Situacao('P');

                  if (PObjnotaFiscal.Salvar(false)=False) then
                  begin

                    MensagemErro('Erro na tentativa de Salvar o N� da Nfe '+pnfe+' na Nota Fiscal do safira, cancela a Nfe '+Pnota[0]+' manualmente');
                    exit;

                  end;

                  exit;

            end else
            begin

               {o Erro foi outro n�o foi o Lote em Processamento (105)}
               MensagemErro(e.Message);
               exit;

            end;

          end;

        end;

        Try

            {Se chegou at� aqui � porque a Nfe foi enviada e confirmada}
            if (FComponentesNfe.ACBrNFe.DANFE <> nil) then
            begin

              if FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].Confirmada then
              begin

                FComponentesNfe.ACBrNFe.DANFE.ProtocoloNFe :=  FComponentesNfe.ACBrNFe.WebServices.Retorno.NFeRetorno.ProtNFe.Items[0].nProt;
                FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].Imprimir;

              end;

            end;

        Except

          MensagemErro('Erro na tentativa de Imprimir, tente imprimir direto do arquivo');

        End;


       end;

       FcomponentesNfe.MemoResp.Text:= UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.Retorno.RetWS);
      FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);

      PObjNotaFiscal.Nfe.Status := dsedit;

       PObjNotaFiscal.Nfe.Submit_CERTIFICADO(ObjEmpresaGlobal.get_certificado);
       PObjNotaFiscal.Nfe.Submit_ReciboEnvio (precibo);
       PObjNotaFiscal.Nfe.submit_chave_acesso (StringReplace (FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID,'Nfe','',[rfIgnoreCase]));
       PObjNotaFiscal.Nfe.submit_arquivo_xml(PObjnotaFiscal.Nfe.GET_CAMINHO_DOS_ARQUIVOS_XML+StringReplace (FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID,'Nfe','',[rfIgnoreCase])+'-NFe.xml');
       //ShowMessage (PObjnotaFiscal.Nfe.GET_CAMINHO_DOS_ARQUIVOS_XML+StringReplace (FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID,'Nfe','',[rfIgnoreCase])+'-NFe');

       FComponentesNfe.ACBrNFe.NotasFiscais.SaveToFile(PObjnotaFiscal.Nfe.GET_CAMINHO_DOS_ARQUIVOS_XML());

       if (PmodoContigencia) then
         PObjNotaFiscal.Nfe.Submit_STATUSNOTA('T')//contingencia

       else
         PObjNotaFiscal.Nfe.Submit_STATUSNOTA('G');//Gerada

       PObjNotaFiscal.Nfe.Submit_Arquivo(objgeranfe.LocalArquivos+'\'+StringReplace(FcomponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID,'Nfe','',[rfIgnoreCase])+'-nfe.xml');

       if (PObjNotaFiscal.Nfe.Salvar (True) = False) then
         MensagemErro('N�o foi poss�vel alterar o status  para "GERADA" na nota ID '+pnfe+' Cancele a Nfe manualmente ou altere manualmente este status para GERADA');

       if (PObjnotaFiscal.LocalizaCodigo (Pnota[0]) = False) then
       begin

        MensagemAviso('Nota Fiscal n�o Localizada, cancele a Nfe '+pnfe+' pelo arquivo ');
        exit;

       end;

       PObjNotaFiscal.TabelaparaObjeto;
       PObjNotaFiscal.Status:=dsedit;
       PObjNotaFiscal.Nfe.Submit_Codigo(pnfe);

       if (PmodoContigencia) then
          PObjNotaFiscal.Submit_Situacao('T')

       else
         PObjNotaFiscal.Submit_Situacao('I');

       if (PObjNotaFiscal.Salvar (true) = False) then
       begin

          MensagemErro('Erro na tentativa de Salvar o N� da Nfe '+pnfe+' na Nota Fiscal do safira.');
          exit;

       end;

    except

      Exit;

    end;

  finally
    self.devolucao  := False;
    self.NfeEntrada := False;
    PObjNotaFiscal.Free;
    FreeAndNil(QueryDup);
  end;

  result:=True;  *)

end;
{todo: end nfe}

function TObjNotafiscalObjetos.GeraNfe(Pnota: string; PmodoContigencia,
  PForcaProcessamento: boolean): boolean;
begin

end;


function TObjNotafiscalObjetos.AjustaDescontos(DescontoTotal:Currency;pedido:string):Currency;
var
  Query:TIBQuery;
  Diferenca:Currency;
  Codigo:string;
  NovoDesconto:Currency;
begin
    result:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
      begin
          //CALCULO A SOMA DOS DESCONTO ATE O MOMENTO
          Close ;
          SQL.Clear;
          SQL.Add('select sum(desconto) from tabmateriaisvenda where pedido='+pedido);
          Open;
          //calculo a diferen�a q falta ou sobra para o valor correto
          Diferenca:= FieldByName('sum').AsCurrency-DescontoTotal;
          if(Diferenca=0)
          then Exit;

          //pego o menor valor maior q a diferen�a
          Close;
          sql.Clear;
          sql.Add('select codigo, min(desconto) AS DESCONTO from tabmateriaisvenda where pedido='+pedido);
          sql.Add('and desconto>'+virgulaparaponto(CurrToStr(diferenca)));
          sql.Add('group by codigo');
          Open;
          Last;
          codigo:=fieldbyname('codigo').AsString;
          NovoDesconto:=fieldbyname('desconto').AsCurrency - Diferenca;

          if(Codigo='')
          then exit;
          Close;
          sql.Clear;
          sql.Add('Update tabmateriaisvenda set desconto='+virgulaparaponto(CurrToStr(Novodesconto)));
          SQL.Add('where codigo='+Codigo);
          ExecSQL;
          //FDataModulo.IBTransaction.CommitRetaining;

          result:=Diferenca;
      end;

    finally
         FreeAndNil(Query);
    end;



end;


function TObjNotafiscalObjetos.GET_ST_TABELA_A(orig: string): TpcnOrigemMercadoria;
begin

   if (orig = '0') then
       Result := oeNacional

   else if (orig = '1') then
       Result := oeEstrangeiraImportacaoDireta

   else if (orig = '2') then
       Result := oeEstrangeiraAdquiridaBrasil
   else
      ShowMessage ('Configura��o da CST invalida no cadastro de produtos');

end;

function TObjNotafiscalObjetos.GET_ST_TABELA_B(CST: string): TpcnCSTIcms;
begin

     case StrToInt (CST) of

        0:  result := cst00;
        10: result := cst10;
        20: result := cst20;
        30: result := cst30;
        40: result := cst40;
        41: result := cst41;
        50: result := cst50;
        51: result := cst51;
        60: result := cst60;
        70: result := cst70;
        90: result := cst90;

     else

        begin

          MensagemErro('C�digo da Situa��o Tribut�ria Inv�lida no cadastro  de produto ');
          exit;
        end

     end;

end;

function TObjNotafiscalObjetos.GET_MODALIDADE_BC_ICMS_ST(ModBCST:integer): TpcnDeterminacaoBaseIcmsST;
begin

  case (ModBCST) of
    0 :result:=dbisPrecoTabelado;
    1 :result:=dbisListaNegativa;
    2 :result:=dbisListaPositiva;
    3 :result:=dbisListaNeutra;
    4 :result:=dbisMargemValorAgregado;
    5 :result:=dbisPauta;
  else;
    MensagemAviso('Aten��o: Nenhum valor informado na modalidade de determina��o da BC do ICMS ST. Sera enviado 0-pre�o tabelado');
    result:=dbisPrecoTabelado;
  end;

end;

function TObjNotafiscalObjetos.GET_MODALIDADE_BC(ModBC: integer): TpcnDeterminacaoBaseIcms;
begin

  case (ModBC) of
    0 :result:=dbiMargemValorAgregado;
    1 :result:=dbiPauta;
    2 :result:=dbiPrecoTabelado;
    3 :result:=dbiValorOperacao;
  else;
    MensagemAviso('Aten��o: Nenhum valor informado na modalidade de determina��o da BC do ICMS. Sera enviado 3-valor da opera��o');
    result:=dbiValorOperacao;
  end;

end;

function TObjNotafiscalObjetos.GET_CST_COFINS(cst: integer): TpcnCstCofins;
begin

  case (cst) of
    1:result:=cof01;
    2:result:=cof02;
    3:result:=cof03;
    4:result:=cof04;
    6:result:=cof06;
    7:result:=cof07;
    8:result:=cof08;
    9:result:=cof09;
    49:result:=cof49;
    50:result:=cof50;
    51:result:=cof51;
    52:result:=cof52;
    53:result:=cof53;
    54:result:=cof54;
    55:result:=cof55;
    56:result:=cof56;
    60:result:=cof60;
    61:result:=cof61;
    62:result:=cof62;
    63:result:=cof63;
    64:result:=cof64;
    65:result:=cof65;
    66:result:=cof66;
    67:result:=cof67;
    70:result:=cof70;
    71:result:=cof71;
    72:result:=cof72;
    73:result:=cof73;
    74:result:=cof74;
    75:result:=cof75;
    98:result:=cof98;
    99:result:=cof99;
  else ;
    result:=cof99;
  end;

end;

function TObjNotafiscalObjetos.GET_CST_IPI(cst: integer): TpcnCstIpi;
begin

  case (cst) of
   0:result:=ipi00;
   1:result:=ipi01;
   2:result:=ipi02;
   3:result:=ipi03;
   4:result:=ipi04;
   5:result:=ipi05;
   51:result:=ipi51;
   52:result:=ipi52;
   53:result:=ipi53;
   54:result:=ipi54;
   55:result:=ipi55;
   99:result:=ipi99;
  else ;
    result:=ipi99;
  end;

end;

function TObjNotafiscalObjetos.GET_CST_PIS(cst: integer): TpcnCstPis;
begin

  case (cst) of

    01: result := pis01;
    02: result := pis02;
    03: result := pis03;
    04: result := pis04;
    06: result := pis06;
    07: result := pis07;
    08: result := pis08;
    09: result := pis09;
    49: result := pis49;
    50: result := pis50;
    51: result := pis51;
    52: result := pis52;
    53: result := pis53;
    54: result := pis54;
    55: result := pis55;
    56: result := pis56;
    60: result := pis60;
    61: result := pis61;
    62: result := pis62;
    63: result := pis63;
    64: result := pis64;
    65: result := pis65;
    66: result := pis66;
    67: result := pis67;
    70: result := pis70;
    71: result := pis71;
    72: result := pis72;
    73: result := pis73;
    74: result := pis74;
    75: result := pis75;
    98: result := pis98;
    99: result := pis99;

  else
    result:=pis99;
  end;

end;

function TObjNotafiscalObjetos.get_crt(tipoCRT: string): TpcnCRT;
begin

  if (tipoCRT = '1') then
    result := crtSimplesNacional

  else if (tipoCRT = '2') then
    result := crtSimplesExcessoReceita

  else if (tipoCRT = '3') then
    result := crtRegimeNormal

  else
  begin
    MensagemAviso('Tipo do CRT inv�lido no cadastro da empresa');
  end;

end;

function TObjNotafiscalObjetos.get_TIPOCSOSN(tipoCSOSN: string): TpcnCSOSNIcms;
begin

  case (StrToInt(tipoCSOSN)) of

    101:result := csosn101;
    102:result := csosn102;
    103:Result := csosn103;
    201:Result := csosn201;
    202:Result := csosn202;
    203:Result := csosn203;
    300:Result := csosn300;
    400:Result := csosn400;
    500:Result := csosn500;
    900:Result := csosn900;

  else

    if (tipoCSOSN = '') then
      result := csosnVazio
    else
      ShowMessage('CSOSN inv�lido');

  end;

end;

function TObjNotafiscalObjetos.Valida_dados_NFE(PObjnotaFiscal: TObjNotaFiscal;pPedido:string;pnfeDigitada:string): Boolean;
var
  mensagem:string;
  contErro,contAtencao:Integer;
  ErrosNFE :TFerrosNFE;
  queryLocal:tibquery;
  GerarPorProjetos:string;
begin

  mensagem :='ATEN��O, VERIFIQUE AS SEGUINTES SITUA��ES'+#13;


  contErro   := 0;
  contAtencao:= 0;

  if ( Trim (ObjEmpresaGlobal.get_certificado ()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - N�mero do certificado digital n�o informado no cadastro da empresa';
  end;

  if ( Trim (PObjnotaFiscal.Get_NATUREZAOPERACAO()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Natureza de opera��o n�o informada no cadastro de cfop';
  end;

  if ( Length(PObjnotaFiscal.Get_NATUREZAOPERACAO()) > 60 ) then
  begin
      contAtencao := contAtencao + 1;
      mensagem := mensagem + #13 +'ATEN��O: '+IntToStr(contAtencao)+' - Campo natureza de opera��o ultrapassa o limite reservado (60). O campo ser� abreviado';
  end;

  if ( Trim (PObjnotaFiscal.Get_dataemissao()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Data de emiss�o n�o informada';
  end;

  if ( Trim (ObjEmpresaGlobal.get_codigoEstado()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - C�digo do estado n�o informado no cadatro da empresa';
  end;

  if ( Length (ObjEmpresaGlobal.get_codigoEstado()) > 2) then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - C�digo do estado ultrapassa o limite reservado (2)';
  end;

  if ( Trim (ObjEmpresaGlobal.get_codigoCidade()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - C�digo da cidade n�o informado no cadatro da empresa';
  end;

  if ( Length (ObjEmpresaGlobal.get_codigoCidade()) > 7) then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - C�digo da cidade ultrapassa o limite reservado (7)';
  end;

  {mudar aqui}
  {if ( Trim (ObjEmpresaGlobal.Get_IE()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Inscri��o estadual n�o informada no cadatro da empresa';
  end;}

  {if ( Length (RetornaSoNumeros(ObjEmpresaGlobal.Get_IE())) > 14) then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Inscri��o estadual ultrapassa o limite reservado (1-14)';
  end;}

  if ( Trim (ObjEmpresaGlobal.Get_RAZAOSOCIAL()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Raz�o social n�o informada no cadatro da empresa';
  end;

  if ( Length (ObjEmpresaGlobal.Get_RAZAOSOCIAL()) > 60) then
  begin
      contAtencao := contAtencao + 1;
      mensagem := mensagem + #13 +'ATEN��O: '+IntToStr(contAtencao)+' - Raz�o social ultrapassa o limite reservado (60). O campo ser� abreviado';
  end;

  {if ( Trim (ObjEmpresaGlobal.Get_FANTASIA()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Fantasia n�o informada no cadatro da empresa';
  end;}

  if ( Length (ObjEmpresaGlobal.Get_FANTASIA()) > 60) then
  begin
      contAtencao := contAtencao + 1;
      mensagem := mensagem + #13 +'ATEN��O: '+IntToStr(contAtencao)+' - Fantasia informada no cadastro da empresa ultrapassa o limite reservado (60). O campo ser� abreviado';
  end;

  if ( Trim (ObjEmpresaGlobal.Get_FONE()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Fone n�o informado no cadatro da empresa';
  end;

  if ( Length (ObjEmpresaGlobal.Get_FONE()) > 14) then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Fone informado no cadatro da empresa ultrapassa o limite reservado (14)';
  end;

  if ( Trim (RetornaSoNumeros(ObjEmpresaGlobal.Get_CEP())) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - CEP n�o informado no cadatro da empresa';
  end;

  if ( Length (RetornaSoNumeros (ObjEmpresaGlobal.Get_CEP())) > 8) then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - CEP informado no cadatro da empresa ultrapassa o limite reservado (8)';
  end;

  if ( Trim (ObjEmpresaGlobal.Get_ENDERECO()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Endere�o n�o informado no cadatro da empresa';
  end;

  if (Length (ObjEmpresaGlobal.Get_ENDERECO()) > 60) then
  begin
      contAtencao := contAtencao + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contAtencao)+' - Endere�o informado no cadatro da empresa ultrapassa o limite reservado (60). O campo ser� abreviado';
  end;

  if ( Trim (ObjEmpresaGlobal.Get_Numero()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Endere�o->N�mero n�o informado no cadatro da empresa';
  end;

  if (Length (ObjEmpresaGlobal.Get_Numero()) > 60) then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Endere�o->N�mero informado no cadatro da empresa ultrapassa o limite reservado (60)';
  end;

  if (Length (ObjEmpresaGlobal.Get_COMPLEMENTO()) > 60) then
  begin
      contAtencao := contAtencao + 1;
      mensagem := mensagem + #13 +'ATEN��O: '+IntToStr(contAtencao)+' - Endere�o->Complemento informado no cadatro da empresa ultrapassa o limite reservado (60). O campo ser� abreviado';
  end;

  if ( Trim (ObjEmpresaGlobal.get_bairro()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Endere�o->Bairro n�o informado no cadatro da empresa';
  end;

  if (Length (ObjEmpresaGlobal.get_bairro()) > 60) then
  begin
      contAtencao := contAtencao + 1;
      mensagem := mensagem + #13 +'ATEN��O: '+intToStr(contAtencao)+' - Endere�o->Bairro informado no cadatro da empresa ultrapassa o limite reservado (60). O campo ser� abreviado';
  end;

  {if ( Trim (ObjEmpresaGlobal.get_cogidoPais()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO: '+contErro' - C�digo do pais n�o informado no cadatro da empresa';
  end;}

  {if (Length (ObjEmpresaGlobal.get_cogidoPais() > 4) then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO: '+contErro' - C�digo do pais informado no cadatro da empresa ultrapassa o limite reservado (4)';
  end; }

  {if ( Trim (ObjEmpresaGlobal.get_pais()) = '') then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO: '+contErro' - Pa�s n�o informado no cadatro da empresa';
  end;}

  {if (Length (ObjEmpresaGlobal.get_pais() > 60) then
  begin
      contErro := contErro + 1;
      mensagem := mensagem + #13 +'ERRO: '+IntToStr(contErro)' - Pa�s informado no cadatro da empresa ultrapassa o limite reservado (60)';
  end;}

  {validando dados do cliente ou do fornecedor}

  if (PObjnotaFiscal.Cliente.Get_CODIGO() <> '' ) then
  begin
  
    if (RetornaSoNUmeros(PObjnotaFiscal.Cliente.Get_Cpf_CGC) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Cpf_CGC n�o informado no cadatro de clientes. Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if (Length (RetornaSoNUmeros (PObjnotaFiscal.Cliente.Get_Cpf_CGC)) > 60) then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Cpf_CGC informado no cadatro de clientes ultrapassa o limite reservado (14). Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if (RetornaSoNUmeros(PObjnotaFiscal.Cliente.Get_CEP) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - CEP-fatura n�o informado no cadatro de clientes. Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if (Length (Trim (RetornaSoNUmeros (PObjnotaFiscal.Cliente.Get_CEP))) > 8) then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - CEP-fatura informado no cadatro de clientes ultrapassa o limite reservado (8). Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if ( Trim (PObjnotaFiscal.Cliente.Get_Endereco()) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Endere�o-fatura n�o informado no cadatro de clientes. Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if ( Length (Trim (PObjnotaFiscal.Cliente.Get_Endereco())) > 60) then
    begin
        contAtencao := contAtencao + 1;
        mensagem := mensagem + #13 +'ATEN��O: '+IntToStr(contAtencao)+' - Endere�o-fatura informado no cadatro de clientes ultrapassa o limite reservado (60). O campo ser� abreviado. Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if ( Trim (PObjnotaFiscal.Cliente.Get_Bairro()) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Bairro-fatura n�o informado no cadatro de clientes. Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if ( Length (PObjnotaFiscal.Cliente.Get_Bairro()) > 60) then
    begin
        contAtencao := contAtencao + 1;
        mensagem := mensagem + #13 +'ATEN��O: '+intToStr(contAtencao)+' - Bairro fatura informado no cadatro de clientes ultrapassa o limite reservado (60). O campo ser� abreviado. Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if ( Trim (PObjnotaFiscal.Cliente.Get_codigocidade()) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - C�digo da cidade-fatura n�o informado no cadatro de clientes. Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if ( Length (PObjnotaFiscal.Cliente.Get_codigocidade()) > 7) then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - C�digo da cidade-fatura informado no cadatro de clientes ultrapassa o limite reservado (7). Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if ( Trim (PObjnotaFiscal.Cliente.Get_Cidade()) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Cidade-fatura n�o informada no cadatro de clientes. Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if ( Length (PObjnotaFiscal.Cliente.Get_Cidade()) > 60) then
    begin
        contAtencao := contAtencao + 1;
        mensagem := mensagem + #13 +'ATEN��O: '+intToStr(contAtencao)+' - Cidade-fatura informada no cadatro de clientes ultrapassa o limite reservado (60). O campo ser� abreviado. Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if ( Trim (PObjnotaFiscal.Cliente.Get_Estado) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Estado-fatura n�o informado no cadatro de clientes. Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if ( Length (PObjnotaFiscal.Cliente.Get_Estado) > 2) then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Estado-fatura informado no cadatro de clientes ultrapassa o limite reservado (2). Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if ( Length (RetornaSoNUmeros (PObjnotaFiscal.Cliente.Get_Fone())) > 14) then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Fone n�o informado no cadatro de clientes ultrapassa o limite reservado (14). Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    {if ( Trim (RetornaSoNUmeros (PObjnotaFiscal.Cliente.Get_Rg_Ie())) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - RG IE n�o informado no cadatro de clientes. Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if ( Length (RetornaSoNUmeros (PObjnotaFiscal.Cliente.Get_Rg_Ie())) > 14) then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - RG IE informado no cadatro de clientes ultrapassa o limite reservado (14). Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;}

    if ( Trim (PObjnotaFiscal.Cliente.Get_Nome()) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Nome n�o informado no cadatro de clientes. Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    if ( Length (PObjnotaFiscal.Cliente.Get_Nome()) > 60) then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Nome informado no cadatro de clientes ultrapassa o limite reservado (60). O campo ser� abreviado. Cliente de c�digo: '+PObjnotaFiscal.Cliente.Get_CODIGO();
    end;

    {if ( Trim (PObjnotaFiscal.Cliente.Get_codigoPais()) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO: '+IntToStr(contErro)+' - C�digo do pa�s n�o informado no cadatro de clientes';
    end;}


    {if ( Trim (PObjnotaFiscal.Cliente.Get_Pais()) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO: '+IntToStr(contErro)+' - Pa�s n�o informado no cadatro de clientes';
    end;}



  end else if (PObjnotaFiscal.Fornecedor.Get_CODIGO() <> '') then
  begin

    if (RetornaSoNUmeros(PObjnotaFiscal.Fornecedor.Get_CGC) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - CGC n�o informado no cadatro de fornecedores. fornecedor de c�digo: '+PObjnotaFiscal.Fornecedor.Get_CODIGO();
    end;

    if (RetornaSoNUmeros(PObjnotaFiscal.Fornecedor.Get_Cep) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - CEP n�o informado no cadatro de fornecedores. Fornecedor de c�digo: '+PObjnotaFiscal.fornecedor.Get_CODIGO();
    end;

    if (Length (Trim (RetornaSoNUmeros (PObjnotaFiscal.Fornecedor.Get_Cep))) > 8) then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - CEP informado no cadatro de Fornecedores ultrapassa o limite reservado (8). Fornecedor de c�digo: '+PObjnotaFiscal.Fornecedor.Get_CODIGO();
    end;

    if ( Trim (PObjnotaFiscal.Fornecedor.Get_Endereco()) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Endere�o n�o informado no cadatro de fornecedores. Fornecedor de c�digo: '+PObjnotaFiscal.Fornecedor.Get_CODIGO();
    end;

    if ( Trim (PObjnotaFiscal.Fornecedor.Get_Bairro()) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Bairro n�o informado no cadatro de fornecedores. Fornecedor de c�digo: '+PObjnotaFiscal.Fornecedor.Get_CODIGO();
    end;

    if ( Trim (PObjnotaFiscal.Fornecedor.get_codigoCidade()) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - C�digo da cidade n�o informado no cadatro de fornecedores. Fornecedor de c�digo: '+PObjnotaFiscal.Fornecedor.Get_CODIGO();
    end;

    if ( Trim (PObjnotaFiscal.Fornecedor.Get_Cidade()) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Cidade n�o informada no cadatro de fornecedores. Fornecedor de c�digo: '+PObjnotaFiscal.Fornecedor.Get_CODIGO();
    end;

    if ( Trim (PObjnotaFiscal.Fornecedor.Get_Estado) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Estado n�o informado no cadatro de fornecedores. Fornecedor de c�digo: '+PObjnotaFiscal.Fornecedor.Get_CODIGO();
    end;


    if ( Trim (PObjnotaFiscal.Fornecedor.Get_RazaoSocial()) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Razao Social n�o informado no cadatro de fornecedores. Fornecedor de c�digo: '+PObjnotaFiscal.Fornecedor.Get_CODIGO();
    end;

    if ( Length (PObjnotaFiscal.Fornecedor.Get_RazaoSocial()) > 60) then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Razao Social informada no cadatro de fornecedores ultrapassa o limite reservado (60). O campo ser� abreviado. Fornecedor de c�digo: '+PObjnotaFiscal.Fornecedor.Get_CODIGO();
    end;

    {if ( Trim (PObjnotaFiscal.fornecedor.get_codigopais()) = '') then
    begin
        contErro := contErro + 1;
        mensagem := mensagem + #13 +'ERRO: '+IntToStr(contErro)+' - C�digo do pa�s n�o informado no cadatro de fornecedores. Fornecedor de c�digo: '+PObjnotaFiscal.Fornecedor.Get_CODIGO();
    end;}


  end
  else
  begin

    contErro := contErro + 1;
    mensagem := mensagem + #13 + 'ERRO: '+IntToStr(contErro)+' - � necessario escolher um cliente ou um funcionario'; 

  end;


  {jonatan}
  if(ObjParametroGlobal.ValidaParametro('VENDER PROJETOS COMO PRODUTOS')=false) then
    begin

      MensagemErro('Paramentro "VENDER PROJETOS COMO PRODUTOS" n�o encontrado');
      GerarPorProjetos:='NAO';

    end
  else GerarPorProjetos:=ObjParametroGlobal.Get_Valor;
  {jonatan}

  try

    with (queryLocal) do
    begin

       queryLocal := TIBQuery.Create (nil);
       queryLocal.Database := fdataModulo.IBDatabase;


      {jonatan}
      if (PnfeDigitada = '') then
      begin

          if(GerarPorProjetos='NAO') then
          begin
                  sql.Add('select * from viewprodutosvenda where pedido='+ppedido) ;
                 // SQL.Add('and codigoproduto is not null') ; //se for um projeto que esta gravado, n�o posso pegar ele
          end
          else
          begin
                   sql.Add('select * from viewprodutosvenda where pedido='+ppedido) ;
                  // SQL.Add('and codigoproduto is null') ; //esta pra gerar por projetos, ent�o pego o projeto e nao os materiais
          end;

      end else
      begin

          SQL.Add('select *');
          SQL.Add('from VIEWPRODUTOSVENDA');
          SQL.Add('where nfedigitada = '+PnfeDigitada);

      end;


      { Close;
       SQL.Clear;
       SQL.Add('select *');
       SQL.Add('from viewprodutosvenda');

       if (pnfeDigitada = '') then
        SQL.Add('where pedido ='+pPedido )
       else
        SQL.Add('where nfedigitada = '+pnfeDigitada);}

       try
          Open;
          First;
       except

          if (pnfeDigitada = '') then
            MensagemErro ('N�o foi possivel executar a viewprodutosvenda('+pPedido+')')
          else
            MensagemErro ('N�o foi possivel executar a viewprodutosvenda('+pnfeDigitada+')');

          Result := False;
          Exit;
       end;

       while not (queryLocal.Eof) do
       begin

            if (Trim (FieldByName('cfop').AsString) = '') then
            begin
                  contErro := contErro + 1;
                  mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - CFOP n�o informado para o produto de c�digo: '+FieldByname('CODIGOPRODUTO').AsString;
            end;

           { // add --> 23/12/2010 00:35
            if (Length(FieldByName('cfop').AsString) >=1 ) then
            begin

                if (FieldByName('cfop').AsString[1] <> '5') and (FieldByName('cfop').AsString[1] <> '6') and (FieldByName('cfop').AsString[1] <> '7') then
                begin
                      contErro := contErro + 1;
                      mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - CFOP inv�lido para o produto de c�digo: '+FieldByname('codigo').AsString+'. CFOP'+#39+'s de saida deve iniciar com 5, 6 ou 7';
                end;

            end;  }

            if ( Trim (FieldByName ('descricao').asString) = '') then
            begin

              contErro := contErro + 1;
              mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Nome do produto n�o informado no cadastro de produtos. Produto de c�digo: '+FieldByname('CODIGOPRODUTO').AsString;

            end;
              
            if ( Length (FieldByName ('descricao').asString) > 100) then
            begin

              contErro := contErro + 1;
              mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - Nome do produto informado no cadastro de produtos ultrapassa o limite reservado. Produto de c�digo: '+FieldByname('codigo').AsString;

            end;

            if ( Trim (FieldByName ('NCM').AsString) = '') then
            begin

                contErro := contErro + 1;
                mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - NCM do produto n�o informado no cadastro de produtos. Produto de c�digo: '+FieldByname('CODIGOPRODUTO').AsString;

            end;

            if ( Length (Trim (RetornaSoNumeros (FieldByName ('NCM').AsString))) > 8 ) then
            begin

             contErro := contErro + 1;
             mensagem := mensagem + #13 +'ERRO:    '+IntToStr(contErro)+' - NCM do produto informado no cadastro de produtos ultrapassa o limite reservado. Produto de c�digo: '+FieldByname('CODIGOPRODUTO').AsString;

            end;

            queryLocal.Next;


       end;

    end;

  finally

    FreeAndNil (queryLocal);

  end;

  if (contErro > 0) or (contAtencao > 0) then
  begin

    try

      ErrosNFE := TFerrosNFE.Create(nil);

      ErrosNFE.submit_contErro    (IntToStr (contErro));
      ErrosNFE.submit_contAtencao (IntToStr (contAtencao));

      ErrosNFE.mensagem:=mensagem;

      ErrosNFE.adicionaMemo (mensagem);

      ErrosNFE.ShowModal;

      if (ErrosNFE.Tag) = 1 then
      begin
          Result := true;
          Exit;
      end
      else
      begin

          Result := False;
          Exit;

      end;

    finally
      FreeAndNil(ErrosNFE);
    end;


  end
  else
    Result := True;

end;

procedure TObjNotafiscalObjetos.RelacaodeNotasFiscais(Nota:string);
var
  Query:TIBQuery;
  Datainicio:string;
  datafim:string;
  cliente:string;
  ValorTotal:Currency;
  Situacao:string;
begin

  query:=TIBQuery.Create(nil);
  Query.Database:=FDataModulo.IBDatabase;

  try

    with FfiltroImp do
    begin

      DesativaGrupos;
      Grupo01.Enabled:=True;
      Grupo02.Enabled:=True;
      Grupo03.Enabled:=True;
      Grupo04.Enabled:=True;
      Grupo06.Enabled:=True;

      LbGrupo01.Caption:='Nota Fiscal';
      LbGrupo02.caption:='Data Inicial';
      lbgrupo03.Caption:='Data Final';
      LbGrupo04.Caption:='Cliente';
      LbGrupo06.Caption:='Situa��o';

      edtgrupo02.EditMask:='99/99/9999' ;
      edtgrupo03.EditMask:='99/99/9999' ;
      edtgrupo01.OnKeyDown:=edtnotafiscalKeyDown;
      edtgrupo04.OnKeyDown:=edtclienteKeyDown;
      ComboGrupo06.Items.Clear;
      ComboGrupo06.Items.Add('RASURADA');
      ComboGrupo06.Items.Add('IMPRESSA');
      ComboGrupo06.Items.Add('N�O IMPRESSA');
      ComboGrupo06.Items.Add('CANCELADA');

      ShowModal;


      if(Tag=0) then
        Exit;

      Nota:=edtgrupo01.Text;

      Datainicio:=edtgrupo02.Text;
      datafim:=edtgrupo03.Text;
      cliente:=edtgrupo04.Text;

      if(ComboGrupo06.Text='RASURADA') then
        situacao:='R';

      if(ComboGrupo06.Text='IMPRESSA') then
        situacao:='I';

      if(ComboGrupo06.Text='N�O IMPRESSA') then
        situacao:='N';

      if(ComboGrupo06.Text='CANCELADA') then
        situacao:='C';

      ValorTotal:=0;
      with Query do
      begin

           Close;
           sql.Clear;

           {sql.Add('select tabnotafiscal.numero, tabnotafiscal.valorfinal, tabcliente.nome,tabnotafiscal.dataemissao,tabnotafiscal.situacao');
           sql.Add('from tabnotafiscal');
           sql.Add('join tabcliente on tabcliente.codigo=tabnotafiscal.cliente');}

           sql.Add('select nf.numero, nf.valorfinal, case when (nf.cliente is not null) then c.nome else f.fantasia end as nome,');
           sql.Add('nf.dataemissao,nf.situacao');
           sql.Add('from tabnotafiscal nf');
           sql.Add('left join tabcliente c on c.codigo=nf.cliente');
           sql.Add('left join tabfornecedor f on f.codigo = nf.fornecedor');


           //sql.Add('left join tabpedido on tabpedido.codigo=tabnotafiscal.numpedido');

           sql.Add('where nf.codigo<>-50');

           if(nota<>'') then
            sql.Add('and nf.codigo='+Nota);

           if(cliente<>'') then
            sql.Add('and nf.cliente='+cliente);

           if(Datainicio<>'  /  /    ') then
            sql.Add('and nf.dataemissao>='+#39+formatdatetime('mm/dd/yyyy',StrToDate(datainicio))+#39);

           if(datafim<>'  /  /    ') then
            SQL.Add('and nf.dataemissao<='+#39+formatdatetime('mm/dd/yyyy',StrToDate(datafim))+#39);

           if(Situacao<>'') then
            sql.Add('and nf.situacao='+#39+Situacao+#39);

            //InputBox('','',sql.Text);

           Open;
           FreltxtRDPRINT.ConfiguraImpressao;
           FreltxtRDPRINT.LinhaLocal:=3;
           FreltxtRDPRINT.RDprint.Abrir;

           if (FreltxtRDPRINT.RDprint.Setup=False) then
           begin
            FreltxtRDPRINT.rdprint.Fechar;
            exit;
           end;

           FreltxtRDPRINT.VerificaLinha;
           FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,1,'RELA��O DE NOTA FISCAIS POR PER�ODO',[negrito]);
           FreltxtRDPRINT.incrementalinha(2);
           FreltxtRDPRINT.VerificaLinha;
           FreltxtRDPRINT.RDprint.ImpC(FreltxtRDPRINT.linhalocal,1,CompletaPalavra('N�mero',8,' ')+' '+
                                    CompletaPalavra('Cliente/Fornecedor',40,' ')+' '+
                                    CompletaPalavra('Valor',12,' ')+' '+
                                    CompletaPalavra('Data Emiss�o',15,' ')+' '+
                                    CompletaPalavra('Situa��o',12,' '),[negrito]);
           FreltxtRDPRINT.incrementalinha(2);

           while not Eof do
           begin


            try

              if(FieldByName('situacao').AsString = 'I') then
                Situacao:='Impressa';

              if(FieldByName('situacao').AsString = 'C') then
                Situacao:='Cancelada';

              if(FieldByName('situacao').AsString = 'R') then
                Situacao:='Rasurada';

              if(FieldByName('situacao').AsString = 'N') then
                Situacao:='N�o Impressa';


              FreltxtRDPRINT.VerificaLinha;
              FreltxtRDPRINT.RDprint.Imp(FreltxtRDPRINT.linhalocal,1,CompletaPalavra(fieldbyname('numero').AsString,8,' ')+' '+
                                  CompletaPalavra(fieldbyname('nome').AsString,40,' ')+' '+
                                  CompletaPalavra(fieldbyname('valorfinal').AsString,12,' ')+' '+
                                  CompletaPalavra(fieldbyname('dataemissao').AsString,15,' ')+' '+
                                  CompletaPalavra(Situacao,12,' '));
              FreltxtRDPRINT.incrementalinha(1);
              ValorTotal:=ValorTotal+fieldbyname('valorfinal').AsCurrency;
              Next;

            except

              on e:Exception do
              begin

                ShowMessage('Erro ao montar relatorio: '+e.Message);
                {ShowMessage('Numero: '+fieldbyname('numero').AsString+#13#10
                            +'nome: '+fieldbyname('nome').AsString+#13#10
                            +'valorfinal: '+fieldbyname('valorfinal').AsString+#13#10
                            +'dataemissao: '+fieldbyname('dataemissao').AsString+#13#10
                            +'situacao: '+Situacao);}

                FreltxtRDPRINT.RDprint.Fechar;
                Exit;

              end;

            end;

           end;
           
           FreltxtRDPRINT.incrementalinha(1);
           FreltxtRDPRINT.VerificaLinha;
           FreltxtRDPRINT.RDprint.ImpF(FreltxtRDPRINT.linhalocal,1,CompletaPalavra('VALOR TOTAL : ',15,' ')+' '+
                                    CompletaPalavra(formata_valor(CurrToStr(ValorTotal)),12,' '),[negrito]);

           //FreltxtRDPRINT.RDprint.Fechar;

      end;


    end;

  finally
    FreeAndNil(query);
    FreltxtRDPRINT.RDprint.Fechar;
  end;

end;

procedure TObjNotafiscalObjetos.edtclienteKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
    ObjNotaFiscalCfop.NOTAFISCAL.EdtClienteKeyDown(sender,key,shift,nil);
end;

procedure TObjNotafiscalObjetos.edtnotafiscalKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
     ObjNotaFiscalCfop.EdtNOTAFISCALKeyDown(sender,key,shift);
end;

function TObjNotafiscalObjetos.AjustaAcrescimos(AcrescimoTotal: Currency;
  pedido: string): Currency;
var
  Query:TIBQuery;
  Diferenca:Currency;
  Codigo:string;
  NovoAcrescimo:Currency;
begin
  result:=0;
  try
    query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;
  except

  end;
  try
    with Query do
    begin
        //CALCULO A SOMA DOS ACRESCIMOS ATE O MOMENTO
        Close ;
        SQL.Clear;
        SQL.Add('select sum(acrescimo) from tabmateriaisvenda where pedido='+pedido);
        Open;
        //calculo a diferen�a q falta ou sobra para o valor correto
        Diferenca:= FieldByName('sum').AsCurrency-AcrescimoTotal;

        if( Diferenca < 0) then
        begin
          {a diferen�a foi negativa, ou seja, na distribui��o do acrescimo, a soma
          dos mesmos ultrapassou o valor desejado, ent�o preciso subtrair de algum registro
          essa diferen�a.
          Para isso o registro deve possuir valor > que valor absoluto da diferen�a.
          }

          Close;
          sql.Clear;
          sql.Add('select codigo, max(ACRESCIMO) AS ACRESCIMO from tabmateriaisvenda where pedido='+pedido);
          sql.Add('and ACRESCIMO>'+virgulaparaponto(CurrToStr(Abs(diferenca))));
          sql.Add('group by codigo');
        end
        else
          if (Diferenca > 0) then
          begin
            {a diferen�a foi positiva, ou seja, ainda falta atingir o valor desejado
            para o acrescimo. Ent�o precisa pegar o menor valor encontrado e somar
            a diferen�a.
            }
            //pego o menor valor
            Close;
            sql.Clear;
            sql.Add('select codigo, min(ACRESCIMO) AS ACRESCIMO from tabmateriaisvenda where pedido='+pedido);
            sql.Add('group by codigo');
          end
          else
            Exit;

        Open;
        Last;
        codigo:=fieldbyname('codigo').AsString;
        NovoAcrescimo:=fieldbyname('ACRESCIMO').AsCurrency + Diferenca;

        if(Codigo='')
        then exit;
        Close;
        sql.Clear;
        sql.Add('Update tabmateriaisvenda set ACRESCIMO='+virgulaparaponto(CurrToStr(NovoAcrescimo)));
        SQL.Add('where codigo='+Codigo);
        ExecSQL;
        //FDataModulo.IBTransaction.CommitRetaining;

        result:=Diferenca;
    end;

  finally
    FreeAndNil(Query);
  end;
end;

function TObjNotafiscalObjetos.verificaNotasAbertas(pNumero: string):Boolean;
var
  mostraStringList:TFmostraStringList;
begin

  result := false;

  if pNumero = '' then
    Exit;

  Objquery.SQL.Clear;
  Objquery.Close;

  Objquery.SQL.Text :=
  'select nfe.codigo,nfe.statusnota '+
  'from tabnfe nfe '+
  'where upper (nfe.statusnota) = ''A'' and nfe.codigo < '+pNumero+' '+
  'order by nfe.codigo';

  Objquery.Open;
  Objquery.FetchAll;

  if not Objquery.IsEmpty then
  begin

    Result := True;
    mostraStringList := TFmostraStringList.Create(nil);
    mostraStringList.Height := 355;
    mostraStringList.Memo.Lines.Add('Acerte a situa��o das notas antes de gerar a NF-e');
    mostraStringList.Memo.Lines.Add('-------------------------------------------------');
    mostraStringList.Memo.Lines.Add('');
    mostraStringList.Memo.Lines.Add('N�mero    Situa��o');
    mostraStringList.Memo.Lines.Add('');

    try
    
      Objquery.First;
      while not Objquery.Eof do
      begin
        mostraStringList.Memo.Lines.Add(alinhaTexto(Objquery.Fields[0].AsString,' ',8) + alinhaTexto(Objquery.Fields[1].AsString,' ',8));
        Objquery.Next;
      end;

      mostraStringList.Memo.Lines.Add('');
      mostraStringList.Memo.Lines.Add('Nota atual n� : '+pNumero);
      mostraStringList.ShowModal;

    finally
      FreeAndNil(mostraStringList);
    end;

  end;


end;


function TObjNotafiscalObjetos.vinculaNFe_ao_pedido(
  XMLDocument1: TXMLDocument; codigoCliente,
  codigoPedido: string): Boolean;
var
  ExtraiXML:TobjExtraiXML;
  OpenDialog1:TOpenDialog;
  countProdutos, i:Integer;
  codigoNF:string;
  queryPV,queryPNF,queryTmp:TIBQuery;
  //objProdVendaNF:TObjPRODVENDANOTAFISCAL;
  sqlText:TobjSqlTxt;
begin
  {
  19/02/15 - a Id�ia era utilizar o cProd(c�digo do produto) e o xProd(descri��o do prod)
  que vem na NF-e, mas a descri��o que vai para a NF-e � sem acentos e caracteres especiais
  assim, somente pelo c�digo n�o conseguiria localizar o material, pois o mesmo c�digo
  se repete em todas as tabelas de materiais.
  }

  result := False;
  (*
  if codigoCliente = '' then
  begin
    MensagemAviso('Parametro cliente vazio');
    Exit;
  end;


  OpenDialog1 := TOpenDialog.Create(nil);
  ExtraiXML   := TobjExtraiXML.create;
  queryPV     := TIBQuery.Create(nil);
  queryPNF    := TIBQuery.Create(nil);
  queryTmp    := TIBQuery.Create(nil);

  queryPV.Database   := FDataModulo.IBDatabase;
  queryPNF.Database  := FDataModulo.IBDatabase;
  queryTmp.Database  := FDataModulo.IBDatabase;

  OpenDialog1.Filter := 'Arquivo XML | *.XML';
  if not OpenDialog1.Execute then
    Exit;

  try

    Screen.Cursor := crHourGlass;
    ExtraiXML.limpaCampos ();
    ExtraiXML.submit_caminhoArquivo (OpenDialog1.FileName);
    ExtraiXML.submit_XMLDocument (XMLDocument1);
    ExtraiXML.retornaDados ();
    countProdutos := ExtraiXML.get_totalprodutos ();

    {IDE}

    {pegando o codigo da NF atraves do numero da nfe}
    queryTmp.Close;
    queryTmp.SQL.Clear;
    queryTmp.SQL.Text :=
      'select nf.codigo '+
      'from tabnotafiscal nf '+
      'where (nf.modelonf = 26) and (nf.numero = '+QuotedStr(ExtraiXML.IDE.nNF)+')';
    queryTmp.Open;

    codigoNF := queryTmp.Fields[0].AsString;

    //Self.ObjNotaFiscalCfop.

    Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(codigoNF);
    Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Status := dsEdit;
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('I');
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORTOTAL       (format_si (ExtraiXML.ICMSTot.vProd));
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_DESCONTO         (format_si (ExtraiXML.ICMSTot.vDesc));
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_NATUREZAOPERACAO (ExtraiXML.IDE.natOp);

    {ICMS}
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_BASECALCULOICMS  (format_si (ExtraiXML.ICMSTot.vBC));
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORICMS        (format_si (ExtraiXML.ICMSTot.vICMS));

    {ICMSST}
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_BASECALCULOICMS_SUBSTITUICAO (format_si (ExtraiXML.ICMSTot.vBCST));
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORICMS_SUBSTITUICAO       (format_si (ExtraiXML.ICMSTot.vICMSST));

    {VALOR PIS}
    Self.ObjNotaFiscalCfop.NOTAFISCAL.submit_valorpis(format_si (ExtraiXML.ICMSTot.vPIS));

    {VALOR COFINS}
    Self.ObjNotaFiscalCfop.NOTAFISCAL.submit_valorcofins(format_si (ExtraiXML.ICMSTot.vCOFINS));

    Self.ObjNotaFiscalCfop.NOTAFISCAL.submit_serieNF(ExtraiXML.IDE.serie);
    //Self.ObjNotaFiscalCfop.NOTAFISCAL.submit_crt(ExtraiXML.Emit.CRT);

    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORFRETE     (format_si (ExtraiXML.ICMSTot.vFrete));
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORSEGURO    (format_si (ExtraiXML.ICMSTot.vSeg));
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_OUTRASDESPESAS (format_si (ExtraiXML.ICMSTot.vOutro));

    {IPI}
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_VALORTOTALIPI (format_si (ExtraiXML.ICMSTot.vIPI));


    {TRANSPORTADORA}
    {
    if ExtraiXML.transp.modFrete = '0' then
      NOTAFISCAL.Submit_FreteporContaTransportadora ('1')
    else if ExtraiXML.transp.modFrete = '1' then
      NOTAFISCAL.Submit_FreteporContaTransportadora('2');}

    //NOTAFISCAL.Submit_DadosAdicionais (ExtraiXML.);

    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_datasaida   (fDate(ExtraiXML.IDE.dSaiEnt));
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Horasaida   (ExtraiXML.IDE.hSaiEnt);
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Submit_dataemissao (fDate(ExtraiXML.IDE.dEmi));


    Self.ObjNotaFiscalCfop.NOTAFISCAL.Cliente.Submit_CODIGO(codigoCliente);
    Self.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Submit_CODIGO(ExtraiXML.IDE.nNF);
    Self.ObjNotaFiscalCfop.NOTAFISCAL.submit_nfecomplementar('N');
    //Self.ObjNotaFiscalCfop.NOTAFISCAL.submit_NFdevolucao('N');

    if not Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(false) then
      Exit;

      
    //gravar materiais_nf (diverso_nf, ferragem_nf ... vidro_nf)
    {Mas e como vai saber qual � o produto no sab? pode ser um diverso, ferragem, kitbox...

      Vou procurar pelo c�digo do produto e descricao ( cProd e xProd ) e buscar em
      todas as tabelas. Se tiver somente 1 ocorrencia j� saberei qual � o tipo do material
      Se tiver mais de 1 ocorrencia pede para o usuario escolher.
    }

    {PRODUTOS}
    for i := 0  to countProdutos-1  do
    begin

      self.ZerarTabela;

      self.Status := dsInsert;
      Submit_CODIGO                     ('0');
      NOTAFISCAL.Submit_CODIGO          (codigoNF);
      PRODUTO.Submit_CODIGO             (ExtraiXML.produtos[i].cProd);
      Submit_EMB_UNID                   (ExtraiXML.produtos[i].uCom[1]);
      Submit_QUANTIDADE                 (format_si (ExtraiXML.produtos[i].qCom));
      Submit_UNIDADE                    (ExtraiXML.produtos[i].uCom);
      Submit_VALOR                      (format_si (ExtraiXML.produtos[i].vProd));
      Submit_DESCONTO                   (format_si (ExtraiXML.produtos[i].vDesc));
      Submit_QUANTIDADEREAL             (format_si (ExtraiXML.produtos[i].qCom));
      Submit_ALIQUOTA                   (format_si (ExtraiXML.produtos[i].ICMS.pICMS));
      Submit_REDUCAOBASECALCULO         (format_si (ExtraiXML.produtos[i].ICMS.pRedBC));
      Submit_VALORFINAL                 (CurrToStr(  StrToCurr(format_si(ExtraiXML.produtos[i].vProd)) - StrToCurrDef(format_si(ExtraiXML.produtos[i].vDesc),0)) );
      Submit_DescricaoProduto           (ExtraiXML.produtos[i].xProd);
      Submit_INFORMACOESCOMPLEMENTARES  ('executado pelo vinculo no pedido');
      Submit_PercentualIPI              (format_si (ExtraiXML.produtos[i].IPI.IPITrib.pIPI));
      Submit_BaixouEstoque              ('N');
      Submit_CFOP                       (ExtraiXML.produtos[i].CFOP);
      submit_valorFrete                 (format_si (ExtraiXML.produtos[i].vFrete));
      submit_valorOutros                (format_si (ExtraiXML.produtos[i].vOutro));
      submit_valorSeguro                (format_si (ExtraiXML.produtos[i].vSeg));
      Submit_JUROS                      ('');
      Submit_VALORPAUTA                 ('');
      Submit_ISENTO                     (get_campoTabela('icms_isento_consumidorfinal','codigo','TABPRODUTO',ExtraiXML.produtos[i].cProd));
      Submit_SUBSTITUICAOTRIBUTARIA     (get_campoTabela('icms_substituto','codigo','TABPRODUTO',ExtraiXML.produtos[i].cProd));

      {ICMS}
      submit_stTabelaA                  (ExtraiXML.produtos[i].ICMS.orig);
      submit_stTabelaB                  (ExtraiXML.produtos[i].ICMS.CST);
      submit_csosn                      (ExtraiXML.produtos[i].ICMS.CSOSN);
      Submit_BaseCalculoICMS            (format_si (ExtraiXML.produtos[i].ICMS.vBC));
      submit_VALOR_ICMS                 (format_si (ExtraiXML.produtos[i].ICMS.vICMS));
      submit_BC_ICMS_ST                 (format_si (ExtraiXML.produtos[i].ICMS.vBCST));
      submit_VALOR_ICMS_ST              (format_si (ExtraiXML.produtos[i].ICMS.vICMSST));
      submit_aliquotast                 (format_si (ExtraiXML.produtos[i].ICMS.pICMSST));

      {IPI}
      submit_CSTIPI                     (ExtraiXML.produtos[i].IPI.IPITrib.CST);
      submit_bcipi                      (format_si (ExtraiXML.produtos[i].IPI.IPITrib.vBC));
      submit_valoripi                   (format_si (ExtraiXML.produtos[i].IPI.IPITrib.vIPI));

      {PIS}
      submit_CSTPIS                     (ExtraiXML.produtos[i].PIS.CST);
      submit_percentualPis              (format_si (ExtraiXML.produtos[i].PIS.pPIS));
      submit_valorPis                   (format_si (ExtraiXML.produtos[i].PIS.vPIS));
      submit_BCPis                      (format_si (ExtraiXML.produtos[i].PIS.vBC));

      {COFINS}
      submit_CSTCOFINS                  (ExtraiXML.produtos[i].COFINS.CST);
      submit_percentualCofins           (format_si (ExtraiXML.produtos[i].COFINS.pCOFINS));
      submit_valorCofins                (format_si (ExtraiXML.produtos[i].COFINS.vCOFINS));
      submit_BCCofins                   (format_si (ExtraiXML.produtos[i].COFINS.vBC));

      if not self.Salvar(false) then
        Exit;

    end;

  Try
              //Procurando o pedido

              //aqui procura todos os materias contidos no pedido, ou seja dos projetos que tem no pedido em quest�o
              //busca quais s�o os materias que comp�e o mesmo pra poder calcular os impostos
              With Self.ObjqueryTEMP do
              Begin

                   //***************FERRAGEM*******************************

                   //procura na tabferragem pedido projeto junto com a tabpedido_projeto
                   //PEDIDOPROJETO,FERRAGEMCOR,QUANTIDADE,VALOR,VALORFINAL(valor*quantidade)

                   close;
                   sql.clear;
                   sql.add('Select tabferragem_pp.* from tabferragem_pp');
                   sql.add('join tabpedido_proj on tabferragem_pp.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where tabpedido_proj.pedido='+Ppedido);
                   open;

                try
                   while not(eof) do
                   Begin


                                With ObjFerragem_nflocal do
                                Begin
                                     ZerarTabela;

                                     //Procura pela cor da ferragem
                                     if (FerragemCor.LocalizaCodigo(fieldbyname('ferragemcor').asstring)=False)
                                     Then Begin
                                             Messagedlg('N�o foi Encontrado essa Ferragem!',mterror,[mbok],0);
                                             exit;
                                     End;
                                     if (notafiscal.LocalizaCodigo(pnota)=False)
                                     Then Begin
                                               mensagemerro('Nota Fiscal n�o encontrada');
                                               exit;
                                     End;
                                     notafiscal.TabelaparaObjeto;

                                     //carrega para o objetos os dados da tabela
                                     //CODIGO,FERRAGEM,COR,PORCENTAGEMACRESCIMO,ESTOQUE,ACRESCIMOEXTRA,PORCENTAGEMACRESCIMOFINAL,
                                     //VALOREXTRA,VALORFINAL,CLASSIFICACAOFISCAL
                                     FerragemCor.TabelaparaObjeto;

                                     //***ALTERA��O FEITA POR JONATAN MEDINA
                                     //ANTES BUSCAVA OS IMPOSTOS NA HORA DE FOSSE GERAR UMA NOTA FISCAL, O QUE � ERRADO
                                     //O CERTO � GERAR UMA VENDA, E GUARDAR OS IMPOSTOS DOS MATERIAIS, POIS ASSIM N�O TEM O PROBLEMA
                                     //DO USUARIO APOS GERAR UMA VENDA, IR LA NO CADASTRO DO MATERIAL E MUDAR AS INFORMA��ES E A
                                     //NOTA SAIR COM AS NOVAS INFORMA��ES E N�O COM AS INFORMA��ES CORRETAS, Q TEMOS AO GERAR A VENDA
                                     //AGORA AO GERAR UMA VENDA, TODOS OS IMPOSTOS DO MATERIAL/MATERIAIS, S�O GUARDADOS NA TABMATERIAISVENDA
                                     //ASSIM NA HORA DE BUSCAR OS IMPOSTOS, SO BUSCA NESTA TABELA

                                     //Seleciono os dados que est�o na tabmateriaisvenda usando a viewprodutosvenda
                                     //nela eu busco pelo codigo do produto (codigo da ferragem, neste caso)
                                     //e pelo codigo do material (como eu estou procurando por ferragens, aqui o codigo � 1)
                                     Query.Close;
                                     Query.SQL.Clear;
                                     Query.SQL.Add('select * from viewprodutosvenda');
                                     Query.SQL.Add('where codigoprodutoPK ='+Ferragemcor.Ferragem.Get_Codigo);
                                     Query.SQL.Add('and material = 1');
                                     Query.SQL.Add('and pedido='+ppedido);
                                     Query.SQL.Add('and notafiscal='+Pnota);
                                     Query.Open;
                                     if(Query.RecordCount>0)
                                     then begin
                                         status:=dsInactive;
                                         Status:=dsinsert;
                                         Submit_Codigo('0');

                                         //Passando a quantidade de ferragem que tem no pedido
                                         Submit_Quantidade(fieldbyname('quantidade').asstring);
                                         //Passando o valor unitario de cada pedido
                                         Submit_Valor(fieldbyname('valor').asstring);

                                         IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                         IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                         IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                         IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                         CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                         IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                                         IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                         IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                                         Submit_BC_ICMS(Query.fieldbyname('BC_ICMS').AsString);
                                         Submit_BC_ICMS_ST(Query.fieldbyname('BC_ICMS_ST').AsString);
                                         Submit_VALOR_ICMS(Query.fieldbyname('VALOR_ICMS').AsString);
                                         Submit_VALOR_ICMS_ST(Query.fieldbyname('VALOR_ICMS_ST').AsString);

                                         Submit_BC_IPI(Query.fieldbyname('BC_IPI').AsString);
                                         Submit_VALOR_IPI(Query.fieldbyname('VALOR_IPI').AsString);

                                         Submit_BC_PIS(Query.fieldbyname('BC_PIS').AsString);
                                         Submit_VALOR_PIS(Query.fieldbyname('VALOR_IPI').AsString);
                                         Submit_BC_PIS_ST(Query.fieldbyname('BC_PIS_ST').AsString);
                                         Submit_VALOR_PIS_ST(Query.fieldbyname('VALOR_PIS_ST').AsString);

                                         Submit_BC_COFINS(Query.fieldbyname('BC_COFINS').AsString);
                                         Submit_VALOR_COFINS(Query.fieldbyname('VALOR_COFINS').AsString);
                                         Submit_BC_COFINS_ST(Query.fieldbyname('BC_COFINS_ST').AsString);
                                         Submit_VALOR_COFINS_ST(Query.fieldbyname('VALOR_COFINS_ST').AsString);


                                         Submit_valorpauta(Query.fieldbyname('VALORPAUTA').AsString);
                                         Submit_ISENTO(Query.fieldbyname('ISENTO').AsString);
                                         Submit_SUBSTITUICAOTRIBUTARIA(Query.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
                                         Submit_ALIQUOTA(Query.fieldbyname('ALIQUOTA').AsString);
                                         Submit_REDUCAOBASECALCULO(Query.fieldbyname('REDUCAOBASECALCULO').AsString);
                                         Submit_ALIQUOTACUPOM(Query.fieldbyname('ALIQUOTACUPOM').AsString);
                                         Submit_IPI('0');
                                         Submit_percentualagregado(Query.fieldbyname('PERCENTUALAGREGADO').AsString);
                                         Submit_Margemvaloragregadoconsumidor(Query.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
                                         SituacaoTributaria_TabelaA.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
                                         SituacaoTributaria_TabelaB.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
                                         //*********************************


                                         Submit_valorfrete(Query.fieldbyname('valorfrete').AsString);
                                         Submit_valorseguro(Query.fieldbyname('valorseguro').AsString);
                                         Submit_referencia('');
                                         Submit_desconto(virgulaparaponto(Query.fieldbyname('desconto').AsString));
                                         CSOSN.Submit_codigo(Query.fieldbyname('csosn').AsString);
                                         submit_UNIDADE(Query.FieldByName('unidade').AsString);



                                         if (Salvar(False)=False)
                                         Then Begin
                                                   messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                                                   exit;
                                         End;
                                     end;
                                End;
                         //end;

                        next;//pr�xima ferragem do pedido
                   End;//While
                finally
                    ObjFerragem_NFlocal.Free;
                end;




                   //*********************DIVERSO******************

                   close;
                   sql.clear;
                   sql.add('Select tabdiverso_pp.* from tabdiverso_pp');
                   sql.add('join tabpedido_proj on tabdiverso_pp.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where Tabpedido_proj.pedido='+Ppedido);
                   open;
                try
                   while not(eof) do
                   Begin
                        With ObjDiverso_nflocal do
                        Begin
                             ZerarTabela;
                             if (DiversoCor.LocalizaCodigo(fieldbyname('diversocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Diverso nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             DiversoCor.TabelaparaObjeto;
                             if (notafiscal.LocalizaCodigo(pnota)=False)
                             Then Begin
                                  mensagemerro('Nota Fiscal n�o encontrada');
                                  exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+Diversocor.Diverso.Get_Codigo);
                             Query.SQL.Add('and material = 6');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.SQL.Add('and notafiscal='+Pnota);
                             Query.Open;
                             if(Query.RecordCount>0)
                             then begin
                                    status:=dsInactive;
                                    Status:=dsinsert;
                                    Submit_Codigo('0');

                                    //Passando a quantidade de ferragem que tem no pedido
                                    Submit_Quantidade(fieldbyname('quantidade').asstring);
                                    //Passando o valor unitario de cada pedido
                                    Submit_Valor(fieldbyname('valor').asstring);


                                    IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                    IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                    IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                    IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                    CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                    IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                                    IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                    IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                                    Submit_BC_ICMS(Query.fieldbyname('BC_ICMS').AsString);
                                    Submit_BC_ICMS_ST(Query.fieldbyname('BC_ICMS_ST').AsString);
                                    Submit_VALOR_ICMS(Query.fieldbyname('VALOR_ICMS').AsString);
                                    Submit_VALOR_ICMS_ST(Query.fieldbyname('VALOR_ICMS_ST').AsString);

                                    Submit_BC_IPI(Query.fieldbyname('BC_IPI').AsString);
                                    Submit_VALOR_IPI(Query.fieldbyname('VALOR_IPI').AsString);

                                    Submit_BC_PIS(Query.fieldbyname('BC_PIS').AsString);
                                    Submit_VALOR_PIS(Query.fieldbyname('VALOR_IPI').AsString);
                                    Submit_BC_PIS_ST(Query.fieldbyname('BC_PIS_ST').AsString);
                                    Submit_VALOR_PIS_ST(Query.fieldbyname('VALOR_PIS_ST').AsString);

                                    Submit_BC_COFINS(Query.fieldbyname('BC_COFINS').AsString);
                                    Submit_VALOR_COFINS(Query.fieldbyname('VALOR_COFINS').AsString);
                                    Submit_BC_COFINS_ST(Query.fieldbyname('BC_COFINS_ST').AsString);
                                    Submit_VALOR_COFINS_ST(Query.fieldbyname('VALOR_COFINS_ST').AsString);


                                    Submit_valorpauta(Query.fieldbyname('VALORPAUTA').AsString);
                                    Submit_ISENTO(Query.fieldbyname('ISENTO').AsString);
                                    Submit_SUBSTITUICAOTRIBUTARIA(Query.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
                                    Submit_ALIQUOTA(Query.fieldbyname('ALIQUOTA').AsString);
                                    Submit_REDUCAOBASECALCULO(Query.fieldbyname('REDUCAOBASECALCULO').AsString);
                                    Submit_ALIQUOTACUPOM(Query.fieldbyname('ALIQUOTACUPOM').AsString);
                                    Submit_IPI('0');
                                    Submit_percentualagregado(Query.fieldbyname('PERCENTUALAGREGADO').AsString);
                                    Submit_Margemvaloragregadoconsumidor(Query.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
                                    SituacaoTributaria_TabelaA.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
                                    SituacaoTributaria_TabelaB.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
                                     //*********************************


                                    Submit_valorfrete(Query.fieldbyname('valorfrete').AsString);
                                    Submit_valorseguro(Query.fieldbyname('valorseguro').AsString);
                                    Submit_referencia('');
                                    Submit_desconto(virgulaparaponto(Query.fieldbyname('desconto').AsString));
                                    CSOSN.Submit_codigo(Query.fieldbyname('csosn').AsString);
                                    submit_UNIDADE(Query.FieldByName('unidade').AsString);

                                    if (Salvar(False)=False)
                                    Then Begin
                                               messagedlg('N�o foi poss�vel Gravar o Diverso',mterror,[mbok],0);
                                               exit;
                                    End;
                             end;

                        End;

                      next;//proximo divero
                   End;//while
                
                finally
                     ObjDiverso_NFlocal.Free;
                end;



                   //*********************KITBOX******************
                   close;
                   sql.clear;
                   sql.add('Select TABKITBOX_PP.* from TABKITBOX_PP');
                   sql.add('join tabpedido_proj on TABKITBOX_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where Tabpedido_proj.pedido='+Ppedido);
                   open;
                 try

                   while not(eof) do
                   Begin
                        With ObjKitbox_nflocal do
                        Begin
                             ZerarTabela;
                             if (KitboxCor.LocalizaCodigo(fieldbyname('kitboxcor').asstring)=False)
                             Then
                             Begin
                                     Messagedlg('N�o foi Encontrado esse Kitbox nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             KitboxCor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnota)=False)
                             Then Begin
                                               mensagemerro('Nota Fiscal n�o encontrada');
                                               exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+Kitboxcor.Kitbox.Get_Codigo);
                             Query.SQL.Add('and material = 4');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.SQL.Add('and notafiscal='+Pnota);
                             Query.Open;
                             if(Query.RecordCount>0)
                             then begin
                                    status:=dsInactive;
                                    Status:=dsinsert;
                                    Submit_Codigo('0');

                                    //Passando a quantidade de ferragem que tem no pedido
                                    Submit_Quantidade(fieldbyname('quantidade').asstring);
                                    //Passando o valor unitario de cada pedido
                                    Submit_Valor(fieldbyname('valor').asstring);


                                     IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                     IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                     IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                     IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                     CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                     IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                     IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                     IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                     IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                     CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                     IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                                     IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                     IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                                     Submit_BC_ICMS(Query.fieldbyname('BC_ICMS').AsString);
                                     Submit_BC_ICMS_ST(Query.fieldbyname('BC_ICMS_ST').AsString);
                                     Submit_VALOR_ICMS(Query.fieldbyname('VALOR_ICMS').AsString);
                                     Submit_VALOR_ICMS_ST(Query.fieldbyname('VALOR_ICMS_ST').AsString);

                                     Submit_BC_IPI(Query.fieldbyname('BC_IPI').AsString);
                                     Submit_VALOR_IPI(Query.fieldbyname('VALOR_IPI').AsString);

                                     Submit_BC_PIS(Query.fieldbyname('BC_PIS').AsString);
                                     Submit_VALOR_PIS(Query.fieldbyname('VALOR_IPI').AsString);
                                     Submit_BC_PIS_ST(Query.fieldbyname('BC_PIS_ST').AsString);
                                     Submit_VALOR_PIS_ST(Query.fieldbyname('VALOR_PIS_ST').AsString);

                                     Submit_BC_COFINS(Query.fieldbyname('BC_COFINS').AsString);
                                     Submit_VALOR_COFINS(Query.fieldbyname('VALOR_COFINS').AsString);
                                     Submit_BC_COFINS_ST(Query.fieldbyname('BC_COFINS_ST').AsString);
                                     Submit_VALOR_COFINS_ST(Query.fieldbyname('VALOR_COFINS_ST').AsString);


                                     Submit_valorpauta(Query.fieldbyname('VALORPAUTA').AsString);
                                     Submit_ISENTO(Query.fieldbyname('ISENTO').AsString);
                                     Submit_SUBSTITUICAOTRIBUTARIA(Query.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
                                     Submit_ALIQUOTA(Query.fieldbyname('ALIQUOTA').AsString);
                                     Submit_REDUCAOBASECALCULO(Query.fieldbyname('REDUCAOBASECALCULO').AsString);
                                     Submit_ALIQUOTACUPOM(Query.fieldbyname('ALIQUOTACUPOM').AsString);
                                     Submit_IPI('0');
                                     Submit_percentualagregado(Query.fieldbyname('PERCENTUALAGREGADO').AsString);
                                     Submit_Margemvaloragregadoconsumidor(Query.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
                                     SituacaoTributaria_TabelaA.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
                                     SituacaoTributaria_TabelaB.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
                                             //*********************************


                                     Submit_valorfrete(Query.fieldbyname('valorfrete').AsString);
                                     Submit_valorseguro(Query.fieldbyname('valorseguro').AsString);
                                     Submit_referencia('');
                                     Submit_desconto(virgulaparaponto(Query.fieldbyname('desconto').AsString));
                                     CSOSN.Submit_codigo(Query.fieldbyname('csosn').AsString);
                                     submit_UNIDADE(Query.FieldByName('unidade').AsString);


                                     if (Salvar(False)=False)
                                     Then
                                     Begin
                                               messagedlg('N�o foi poss�vel Gravar o kitbox',mterror,[mbok],0);
                                               exit;
                                     End;
                             end;
                        End;
                        next;//pr�ximo kitbox do pedido
                   End;//while

                 finally
                    ObjKitBox_NFlocal.Free;
                 end;


                   //*********************PERFILADO******************
                   close;
                   sql.clear;
                   sql.add('Select TABperfilado_PP.* from TABperfilado_PP');
                   sql.add('join tabpedido_proj on TABperfilado_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where Tabpedido_proj.pedido='+Ppedido);
                   open;
                try

                   while not(eof) do
                   Begin
                        With ObjPerfilado_nflocal do
                        Begin
                             ZerarTabela;
                             if (PerfiladoCor.LocalizaCodigo(fieldbyname('perfiladocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Perfilado nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             PerfiladoCor.TabelaparaObjeto;

                             if (notafiscal.LocalizaCodigo(pnota)=False)
                             Then Begin
                                  mensagemerro('Nota Fiscal n�o encontrada');
                                  exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+Perfiladocor.Perfilado.Get_Codigo);
                             Query.SQL.Add('and material = 2');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.SQL.Add('and notafiscal='+Pnota);
                             Query.Open;
                             if(Query.RecordCount>0)
                             then begin
                                  status:=dsInactive;
                                  Status:=dsinsert;
                                  Submit_Codigo('0');

                                  //Passando a quantidade de ferragem que tem no pedido
                                  Submit_Quantidade(fieldbyname('quantidade').asstring);
                                  //Passando o valor unitario de cada pedido
                                  Submit_Valor(fieldbyname('valor').asstring);

                                 IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                 IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                 IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                 IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                 CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                 IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                                 IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                 IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                                 Submit_BC_ICMS(Query.fieldbyname('BC_ICMS').AsString);
                                 Submit_BC_ICMS_ST(Query.fieldbyname('BC_ICMS_ST').AsString);
                                 Submit_VALOR_ICMS(Query.fieldbyname('VALOR_ICMS').AsString);
                                 Submit_VALOR_ICMS_ST(Query.fieldbyname('VALOR_ICMS_ST').AsString);

                                 Submit_BC_IPI(Query.fieldbyname('BC_IPI').AsString);
                                 Submit_VALOR_IPI(Query.fieldbyname('VALOR_IPI').AsString);

                                 Submit_BC_PIS(Query.fieldbyname('BC_PIS').AsString);
                                 Submit_VALOR_PIS(Query.fieldbyname('VALOR_IPI').AsString);
                                 Submit_BC_PIS_ST(Query.fieldbyname('BC_PIS_ST').AsString);
                                 Submit_VALOR_PIS_ST(Query.fieldbyname('VALOR_PIS_ST').AsString);

                                 Submit_BC_COFINS(Query.fieldbyname('BC_COFINS').AsString);
                                 Submit_VALOR_COFINS(Query.fieldbyname('VALOR_COFINS').AsString);
                                 Submit_BC_COFINS_ST(Query.fieldbyname('BC_COFINS_ST').AsString);
                                 Submit_VALOR_COFINS_ST(Query.fieldbyname('VALOR_COFINS_ST').AsString);


                                 Submit_valorpauta(Query.fieldbyname('VALORPAUTA').AsString);
                                 Submit_ISENTO(Query.fieldbyname('ISENTO').AsString);
                                 Submit_SUBSTITUICAOTRIBUTARIA(Query.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
                                 Submit_ALIQUOTA(Query.fieldbyname('ALIQUOTA').AsString);
                                 Submit_REDUCAOBASECALCULO(Query.fieldbyname('REDUCAOBASECALCULO').AsString);
                                 Submit_ALIQUOTACUPOM(Query.fieldbyname('ALIQUOTACUPOM').AsString);
                                 Submit_IPI('0');
                                 Submit_percentualagregado(Query.fieldbyname('PERCENTUALAGREGADO').AsString);
                                 Submit_Margemvaloragregadoconsumidor(Query.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
                                 SituacaoTributaria_TabelaA.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
                                 SituacaoTributaria_TabelaB.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
                                         //*********************************


                                 Submit_valorfrete(Query.fieldbyname('valorfrete').AsString);
                                 Submit_valorseguro(Query.fieldbyname('valorseguro').AsString);

                                 Submit_referencia('');
                                 Submit_desconto(virgulaparaponto(Query.fieldbyname('desconto').AsString));
                                 CSOSN.Submit_codigo(Query.fieldbyname('csosn').AsString);
                                 submit_UNIDADE(Query.FieldByName('unidade').AsString);
                                 if (Salvar(False)=False)
                                 Then Begin
                                           messagedlg('N�o foi poss�vel Gravar o Perfilado',mterror,[mbok],0);
                                           exit;
                                 End;
                             end;
                        End;

                        next;//pr�ximo perfilado do pedido
                   End;//while

                finally
                      ObjPerfilado_NFlocal.Free;
                end;


                try

                   //***************PERSIANA*******************************
                   close;
                   sql.clear;
                   sql.add('Select TABPERSIANA_PP.* from TABPERSIANA_PP');
                   sql.add('join tabpedido_proj on TABPERSIANA_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where tabpedido_proj.pedido='+Ppedido);
                   open;

                   while not(eof) do
                   Begin
                        With ObjPersiana_nflocal do
                        Begin
                             ZerarTabela;
                             if (persianagrupodiametrocor.LocalizaCodigo(fieldbyname('persianagrupodiametrocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Persiana nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             persianagrupodiametrocor.TabelaparaObjeto;
                             if (notafiscal.LocalizaCodigo(pnota)=False)
                             Then Begin
                                               mensagemerro('Nota Fiscal n�o encontrada');
                                               exit;
                             End;
                             notafiscal.TabelaparaObjeto;

                             Status:=dsinsert;
                             Submit_Codigo('0');
                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+persianagrupodiametrocor.Persiana.Get_Codigo);
                             Query.SQL.Add('and material = 5');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.SQL.Add('and notafiscal='+Pnota);
                             Query.Open;
                             if(Query.RecordCount>0)
                             then begin
                                    status:=dsInactive;
                                    Status:=dsinsert;
                                    Submit_Codigo('0');

                                    //Passando a quantidade de ferragem que tem no pedido
                                    Submit_Quantidade(fieldbyname('quantidade').asstring);
                                    //Passando o valor unitario de cada pedido
                                    Submit_Valor(fieldbyname('valor').asstring);
                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                   IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                   CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                                   Submit_BC_ICMS(Query.fieldbyname('BC_ICMS').AsString);
                                   Submit_BC_ICMS_ST(Query.fieldbyname('BC_ICMS_ST').AsString);
                                   Submit_VALOR_ICMS(Query.fieldbyname('VALOR_ICMS').AsString);
                                   Submit_VALOR_ICMS_ST(Query.fieldbyname('VALOR_ICMS_ST').AsString);

                                   Submit_BC_IPI(Query.fieldbyname('BC_IPI').AsString);
                                   Submit_VALOR_IPI(Query.fieldbyname('VALOR_IPI').AsString);

                                   Submit_BC_PIS(Query.fieldbyname('BC_PIS').AsString);
                                   Submit_VALOR_PIS(Query.fieldbyname('VALOR_IPI').AsString);
                                   Submit_BC_PIS_ST(Query.fieldbyname('BC_PIS_ST').AsString);
                                   Submit_VALOR_PIS_ST(Query.fieldbyname('VALOR_PIS_ST').AsString);

                                   Submit_BC_COFINS(Query.fieldbyname('BC_COFINS').AsString);
                                   Submit_VALOR_COFINS(Query.fieldbyname('VALOR_COFINS').AsString);
                                   Submit_BC_COFINS_ST(Query.fieldbyname('BC_COFINS_ST').AsString);
                                   Submit_VALOR_COFINS_ST(Query.fieldbyname('VALOR_COFINS_ST').AsString);


                                   Submit_valorpauta(Query.fieldbyname('VALORPAUTA').AsString);
                                   Submit_ISENTO(Query.fieldbyname('ISENTO').AsString);
                                   Submit_SUBSTITUICAOTRIBUTARIA(Query.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
                                   Submit_ALIQUOTA(Query.fieldbyname('ALIQUOTA').AsString);
                                   Submit_REDUCAOBASECALCULO(Query.fieldbyname('REDUCAOBASECALCULO').AsString);
                                   Submit_ALIQUOTACUPOM(Query.fieldbyname('ALIQUOTACUPOM').AsString);
                                   Submit_IPI('0');
                                   Submit_percentualagregado(Query.fieldbyname('PERCENTUALAGREGADO').AsString);
                                   Submit_Margemvaloragregadoconsumidor(Query.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
                                   SituacaoTributaria_TabelaA.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
                                   SituacaoTributaria_TabelaB.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
                                           //*********************************


                                   Submit_valorfrete(Query.fieldbyname('valorfrete').AsString);
                                   Submit_valorseguro(Query.fieldbyname('valorseguro').AsString);

                                   Submit_desconto(virgulaparaponto(Query.fieldbyname('desconto').AsString));
                                   CSOSN.Submit_codigo(Query.fieldbyname('csosn').AsString);
                                   Submit_referencia('');
                                   submit_UNIDADE(Query.FieldByName('unidade').AsString);
                                   if (Salvar(False)=False)
                                   Then Begin
                                             messagedlg('N�o foi poss�vel Gravar a Persiana',mterror,[mbok],0);
                                             exit;
                                   End;
                             end;
                        End;

                        next;//pr�xima persiana
                   End;

                finally
                    ObjPersiana_NFlocal.Free;
                end;


                   //***************VIDRO*******************************
                   close;
                   sql.clear;
                   sql.add('Select TABVIDRO_PP.* from TABVIDRO_PP');
                   sql.add('join tabpedido_proj on TABVIDRO_PP.pedidoprojeto=tabpedido_proj.codigo');
                   sql.add('where tabpedido_proj.pedido='+Ppedido);
                   open;
                try

                   while not(eof) do
                   Begin
                        With ObjVidro_nflocal do
                        Begin
                             ZerarTabela;
                             if (VidroCor.LocalizaCodigo(fieldbyname('vidrocor').asstring)=False)
                             Then Begin
                                     Messagedlg('N�o foi Encontrado esse Vidro nessa cor!',mterror,[mbok],0);
                                     exit;
                             End;
                             VidroCor.TabelaparaObjeto;
                             if (notafiscal.LocalizaCodigo(pnota)=False)
                             Then Begin
                                               mensagemerro('Nota Fiscal n�o encontrada');
                                               exit;
                             End;
                             notafiscal.TabelaparaObjeto;        

                             Status:=dsinsert;
                             Submit_Codigo('0');

                             Submit_Quantidade(fieldbyname('quantidade').asstring);
                             Submit_Valor(fieldbyname('valor').asstring);

                             Query.Close;
                             Query.SQL.Clear;
                             Query.SQL.Add('select * from viewprodutosvenda');
                             Query.SQL.Add('where codigoprodutoPK ='+Vidrocor.Vidro.Get_Codigo);
                             Query.SQL.Add('and material = 3');
                             Query.SQL.Add('and pedido='+ppedido);
                             Query.SQL.Add('and notafiscal='+Pnota);
                             Query.Open;
                             if(Query.RecordCount>0)
                             then begin
                                    status:=dsInactive;
                                    Status:=dsinsert;
                                    Submit_Codigo('0');

                                    //Passando a quantidade de ferragem que tem no pedido
                                    Submit_Quantidade(fieldbyname('quantidade').asstring);
                                    //Passando o valor unitario de cada pedido
                                    Submit_Valor(fieldbyname('valor').asstring);


                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_ORIGEM').asstring);
                                   IMPOSTO_IPI.Submit_codigo(Query.fieldbyname('IMPOSTO_IPI').asstring);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);
                                   CFOP.Submit_CODIGO(Query.fieldbyname('CFOP').asstring);

                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Query.fieldbyname('IMPOSTO_ICMS_DESTINO').asstring);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_PIS_ORIGEM').asstring);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(Query.fieldbyname('IMPOSTO_COFINS_ORIGEM').asstring);

                                   Submit_BC_ICMS(Query.fieldbyname('BC_ICMS').AsString);
                                   Submit_BC_ICMS_ST(Query.fieldbyname('BC_ICMS_ST').AsString);
                                   Submit_VALOR_ICMS(Query.fieldbyname('VALOR_ICMS').AsString);
                                   Submit_VALOR_ICMS_ST(Query.fieldbyname('VALOR_ICMS_ST').AsString);

                                   Submit_BC_IPI(Query.fieldbyname('BC_IPI').AsString);
                                   Submit_VALOR_IPI(Query.fieldbyname('VALOR_IPI').AsString);

                                   Submit_BC_PIS(Query.fieldbyname('BC_PIS').AsString);
                                   Submit_VALOR_PIS(Query.fieldbyname('VALOR_IPI').AsString);
                                   Submit_BC_PIS_ST(Query.fieldbyname('BC_PIS_ST').AsString);
                                   Submit_VALOR_PIS_ST(Query.fieldbyname('VALOR_PIS_ST').AsString);

                                   Submit_BC_COFINS(Query.fieldbyname('BC_COFINS').AsString);
                                   Submit_VALOR_COFINS(Query.fieldbyname('VALOR_COFINS').AsString);
                                   Submit_BC_COFINS_ST(Query.fieldbyname('BC_COFINS_ST').AsString);
                                   Submit_VALOR_COFINS_ST(Query.fieldbyname('VALOR_COFINS_ST').AsString);


                                   Submit_valorpauta(Query.fieldbyname('VALORPAUTA').AsString);
                                   Submit_ISENTO(Query.fieldbyname('ISENTO').AsString);
                                   Submit_SUBSTITUICAOTRIBUTARIA(Query.fieldbyname('SUBSTITUICAOTRIBUTARIA').AsString);
                                   Submit_ALIQUOTA(Query.fieldbyname('ALIQUOTA').AsString);
                                   Submit_REDUCAOBASECALCULO(Query.fieldbyname('REDUCAOBASECALCULO').AsString);
                                   Submit_ALIQUOTACUPOM(Query.fieldbyname('ALIQUOTACUPOM').AsString);
                                   Submit_IPI('0');
                                   Submit_percentualagregado(Query.fieldbyname('PERCENTUALAGREGADO').AsString);
                                   Submit_Margemvaloragregadoconsumidor(Query.fieldbyname('MARGEMVALORAGREGADOCONSUMIDOR').Asstring);
                                   SituacaoTributaria_TabelaA.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAA').AsString);
                                   SituacaoTributaria_TabelaB.Submit_CODIGO(Query.fieldbyname('SITUACAOTRIBUTARIA_TABELAB').AsString);
                                           //*********************************


                                   Submit_valorfrete(Query.fieldbyname('valorfrete').AsString);
                                   Submit_valorseguro(Query.fieldbyname('valorseguro').AsString);
                                   Submit_desconto(virgulaparaponto(Query.fieldbyname('desconto').AsString));
                                   CSOSN.Submit_codigo(Query.fieldbyname('csosn').AsString);
                                   submit_UNIDADE(Query.FieldByName('unidade').AsString);
                                   Submit_referencia('');

                                   if (Salvar(false)=False)
                                   Then Begin
                                             messagedlg('N�o foi poss�vel Gravar o Vidro',mterror,[mbok],0);
                                             exit;
                                   End;
                             end;
                        End;


                       next;//pr�ximo vidro
                   End;

                finally
                    ObjVidro_NFlocal.Free;
                end;

                   Self.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(Pnota);
                   Self.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
                   Self.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;

                    if (Self.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(True)=False)
                    then Begin
                              Mensagemerro('Erro na tentativa de Salvar o desconto na Nota Fiscal');
                              Self.LimpaNF(Pnota);
                              exit;
                    End;


              End;//With Self.ObjqueryTemp


  Finally
            FDataModulo.IBTransaction.RollbackRetaining;
            ObjPedidoObjetos.Free;
            FreeAndNil(Query);
  End;



    {CRIANDO RELACIONAMENTO PRODVENDASNOTAFISCAL}

    queryPV.Close;
    queryPV.SQL.Clear;
    queryPV.SQL.Text :=
    'select pv.codigo '+
    'from tabprodvendas '+
    'pv where pv.venda = '+get_campoTabela('codigo','codigoorcamento','TABVENDAS',codigoPedido);
    queryPV.Open;
    queryPV.First;

    queryPNF.Close;
    queryPNF.SQL.Clear;
    queryPNF.SQL.Text :=
    'select pnf.codigo '+
    'from tabprodutosnotafiscal pnf '+
    'where pnf.notafiscal = '+codigoNF;
    queryPNF.Open;
    queryPNF.First;

    if (queryPV.RecordCount <> queryPNF.RecordCount) then
    begin
      MensagemAviso('Quantidade de produtos na "prodVendas" e "produtosNotaFiscal" divergem');
      Exit;
    end;

    objProdVendaNF := TObjPRODVENDANOTAFISCAL.Create(self.Owner);

    queryPNF.First;
    queryPV.First;
    while not (queryPNF.Eof) do
    begin

      objProdVendaNF.ZerarTabela;
      objProdVendaNF.Status := dsInsert;
      objProdVendaNF.Submit_CODIGO('0');
      objProdVendaNF.ProdVendas.Submit_CODIGO(queryPV.Fields[0].AsString);
      objProdVendaNF.ProdutosNotaFiscal.Submit_CODIGO(queryPNF.Fields[0].AsString);

      if not objProdVendaNF.Salvar(false) then
        Exit;

      queryPNF.Next;
      queryPV.Next;

    end;

    {vinculando NFE}

    sqlText := TobjSqlTxt.Create(nil,ExtractFilePath(Application.ExeName)+'\sqlNFE.txt');

    objgeranfe.Nfe.ZerarTabela;
    objgeranfe.Nfe.LocalizaCodigo(ExtraiXML.IDE.nNF);
    objgeranfe.Nfe.TabelaparaObjeto;
    
    objgeranfe.Nfe.Status := dsEdit;
    objgeranfe.Nfe.LocalizaCodigo(ExtraiXML.IDE.nNF);

    objgeranfe.Nfe.Submit_STATUSNOTA    ('G');
    objgeranfe.Nfe.submit_arquivo_xml   (sqlText.readString('NFE CAMINHO DOS ARQUIVOS XML',true)+FComponentesNfe.RetornaValorCampos(OpenDialog1.FileName,'chNFe')+'-NFe.xml');
    objgeranfe.Nfe.Submit_CERTIFICADO   (sqlText.readString('CERTIFICADO',true));
    objgeranfe.Nfe.submit_chave_acesso  (FComponentesNfe.RetornaValorCampos(OpenDialog1.FileName,'chNFe'));
    objgeranfe.Nfe.Submit_ReciboEnvio   ('*OBS: Executado pelo vinculo no pedido');

    if not objgeranfe.Nfe.Salvar(false) then
      Exit;

    result := True;

  finally

    Screen.Cursor := crDefault;
    FreeAndNil(ExtraiXML);
    FreeAndNil(OpenDialog1);
    FreeAndNil(queryPV);
    FreeAndNil(queryPNF);
    FreeAndNil(queryTmp);

    //if Assigned(objProdVendaNF) then
    //  objProdVendaNF.Free;

    if Assigned(sqlText) then
      FreeAndNil(sqlText);

  end;
  *)
end;

end.


(*
VERSAO LAYOUT:2008-B
06.349.631/0001-05SNF   1    25922525922503/01/2009MS327.088.909-72    AGOSTINHO MARCIO CAMARA                 ISENTO              50.037.02000000039,70000000000000000000000,0000000000000000,0000000000000000,0000000000000000,00000000000000000                00         00000000000000000000,000
  259225        V315.405 000000039,70000000039,7025,0000000000009,93000000000,00000000000,00000000000,0000,0000000000000,00000000000,0000,0000000000000,00000000000,00000000000,0000,0000000000000,00000000000,00000000000,00000000000,000                                                                                      0000000000000,000000                                                                                                                                                                                                                                                          00  000000000000000                                                    001
    001001001                CERVEJA PILSEN GFA 0,60L                             CAIXAS000,00025,00                                             CAIXAS                        00000000000,0000000000000000000001,0000000000000039,700000000000000000039,700001000000000000000,00540500                                             0000001000000000000039,70000000000000009,93000000000000000,00000000000000000,000000000000,000000000000,000000000000,000000000000,000000000000,001000000000000000,00000000000000000,000                                                     00000000000000000000,00
06.349.631/0001-05SNF   1    25923525923503/01/2009MS617.277.099-34    VEISE REGINA MORO                       ISENTO              50.037.02000000032,20000000000000000000000,0000000000000000,0000000000000000,0000000000000000,00000000000000000                00         00000000000000000000,000
  259235        V315.405 000000015,80000000015,8017,0000000000002,69000000000,00000000000,00000000000,0000,0000000000000,00000000000,0000,0000000000000,00000000000,00000000000,0000,0000000000000,00000000000,00000000000,00000000000,000                                                                                      0000000000000,000000                                                                                                                                                                                                                                                          00  000000000000000                                                    002
    001095001                AGUA MIN C/GAS SCHIN PET 0,50L                       CAIXAS000,00025,00                                             CAIXAS                        00000000000,0000000000000000000002,0000000000000008,200000000000000000016,400001000000000000000,00540500                                             0000001000000000000016,40000000000000004,10000000000000000,00000000000000000,000000000000,000000000000,000000000000,000000000000,000000000000,001000000000000000,00000000000000000,000                                                     00000000000000000000,00
    002095002                AGUA MIN S/GAS SCHIN PET 0,50L                       CAIXAS000,00017,00                                             CAIXAS                        00000000000,0000000000000000000002,0000000000000007,900000000000000000015,800001000000000000000,00540500                                             0000001000000000000015,80000000000000002,69000000000000000,00000000000000000,000000000000,000000000000,000000000000,000000000000,000000000000,001000000000000000,00000000000000000,000                                                     00000000000000000000,00
*)



