object FreprocessaChequeDevolvidoComissao: TFreprocessaChequeDevolvidoComissao
  Left = 199
  Top = 150
  Width = 870
  Height = 487
  Caption = 
    'Reprocessamento de Cheques Devolvidos para lan'#231'amento de Comiss'#227 +
    'o'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TStringGrid
    Left = 0
    Top = 113
    Width = 862
    Height = 347
    Align = alClient
    TabOrder = 0
    OnDblClick = GridDblClick
    OnKeyPress = GridKeyPress
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 862
    Height = 113
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 104
      Top = 36
      Width = 17
      Height = 13
      Caption = 'a'
    end
    object Label2: TLabel
      Left = 8
      Top = 16
      Width = 108
      Height = 13
      Caption = 'Per'#237'odo da Devolu'#231#227'o'
    end
    object EdtDataInicial: TMaskEdit
      Left = 8
      Top = 32
      Width = 89
      Height = 21
      EditMask = '99/99/9999'
      MaxLength = 10
      TabOrder = 0
      Text = '  /  /    '
    end
    object EdtDataFinal: TMaskEdit
      Left = 128
      Top = 32
      Width = 89
      Height = 21
      EditMask = '99/99/9999'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object btpesquisar: TButton
      Left = 224
      Top = 32
      Width = 75
      Height = 25
      Caption = 'Pesquisar'
      TabOrder = 2
      OnClick = btpesquisarClick
    end
    object BtSelecionaTodos: TButton
      Left = 8
      Top = 80
      Width = 105
      Height = 25
      Caption = 'Seleciona Todos'
      TabOrder = 3
      OnClick = BtSelecionaTodosClick
    end
    object BtRetiraSelecao: TButton
      Left = 120
      Top = 80
      Width = 105
      Height = 25
      Caption = 'Retira Sele'#231#227'o'
      TabOrder = 4
      OnClick = BtRetiraSelecaoClick
    end
    object btprocessar: TButton
      Left = 312
      Top = 32
      Width = 75
      Height = 25
      Caption = 'Processar'
      TabOrder = 5
      OnClick = btprocessarClick
    end
  end
  object IBQuery1: TIBQuery
    Left = 792
    Top = 8
  end
end
