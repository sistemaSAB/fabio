object FimportaPedido: TFimportaPedido
  Left = 264
  Top = 248
  Width = 799
  Height = 342
  Caption = 'Importa'#231#227'o de Pedidos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object MemoDados: TMemo
    Left = 0
    Top = 106
    Width = 783
    Height = 198
    Align = alClient
    Lines.Strings = (
      'MemoDados')
    ScrollBars = ssBoth
    TabOrder = 0
    WordWrap = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 783
    Height = 106
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 5
      Top = 8
      Width = 57
      Height = 16
      Caption = 'Arquivo: '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbcaminho: TLabel
      Left = 69
      Top = 8
      Width = 48
      Height = 13
      Caption = 'lbcaminho'
    end
    object btcaminhoarquivo: TButton
      Left = 5
      Top = 32
      Width = 105
      Height = 21
      Caption = '&Arquivo'
      TabOrder = 0
      OnClick = btcaminhoarquivoClick
    end
    object BtInicia: TBitBtn
      Left = 5
      Top = 54
      Width = 105
      Height = 25
      Caption = '&Iniciar Importa'#231#227'o'
      TabOrder = 1
      OnClick = BtIniciaClick
    end
    object edterro: TEdit
      Left = 5
      Top = 82
      Width = 780
      Height = 21
      TabOrder = 2
      Text = 'edterro'
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 544
    Top = 8
  end
end
