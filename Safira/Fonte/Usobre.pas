unit Usobre;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
     Buttons, ExtCtrls, jpeg;

type
  TFsobre = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Memo1: TMemo;
    Image1: TImage;
    lbVersaoSistema: TLabel;
    pnl1: TPanel;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fsobre: TFsobre;

implementation
uses Dialogs, UobjVersaoSistema;
{$R *.DFM}

procedure TFsobre.BitBtn2Click(Sender: TObject);
begin
Messagedlg('Para Legaliza��o do Software contate o Suporte!',mtinformation,[mbok],0);
end;

procedure TFsobre.BitBtn1Click(Sender: TObject);
begin
FSobre.Close;
end;

procedure TFsobre.FormCreate(Sender: TObject);
var
objVersao:TObjVersaosistema;
begin
     Try
        objVersao:=TObjVersaosistema.create;
        LbVersaoSistema.caption:=objVersao.VersaodoArquivo;
        objVersao.Free;
     Except
           Messagedlg('Erro na tentativa de Criar o Objeto de Vers�o de Sistema',mterror,[mbok],0);
           exit;
     End;
end;

end.

