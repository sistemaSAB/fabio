unit UGeraRomaneioPedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ImgList, ExtCtrls,UessencialGlobal, Buttons,UobjPEDIDOPROJETOROMANEIO,
  Grids,IBQuery,UDataModulo,DB,UFiltraImp;

type
    TFGeraRomaneioPedido = class(TForm)
    panelCabecalho: TPanel;
    panelBorda: TPanel;
    imgrodape: TImage;
    img1: TImage;
    il1: TImageList;
    lbPedido: TLabel;
    lbCliente: TLabel;
    panelGeral: TPanel;
    lbColocador: TLabel;
    btLocalizar: TSpeedButton;
    panel2: TPanel;
    STRGPedidoProjeto: TStringGrid;
    btPesquisaTranspotadora: TSpeedButton;
    lbtransportadora: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    lb5: TLabel;
    ilProdutos: TImageList;
    panel6: TPanel;
    lbDia: TLabel;
    lbnomeformulario: TLabel;
    btGerarRomaneio: TSpeedButton;
    btCancelar: TSpeedButton;
    imgEntregador: TImage;
    lb1: TLabel;
    lbDataEmissao: TLabel;
    lb6: TLabel;
    lbEntrega: TLabel;
    btAtualizar: TSpeedButton;
    bteditaentrega: TSpeedButton;
    lbaddromaneioexistente: TLabel;
    edt1: TEdit;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btLocalizarClick(Sender: TObject);
    procedure STRGPedidoProjetoDrawCell(Sender: TObject; ACol,
      ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure STRGPedidoProjetoDblClick(Sender: TObject);
    procedure btPesquisaTranspotadoraClick(Sender: TObject);
    procedure btAtualizarClick(Sender: TObject);
    procedure bteditaentregaClick(Sender: TObject);
    procedure btGerarRomaneioClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure lbaddromaneioexistenteMouseLeave(Sender: TObject);
    procedure lbaddromaneioexistenteMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbaddromaneioexistenteClick(Sender: TObject);
    procedure edt1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    ObjPedidoProjetoRomaneio:TObjPedidoProjetoRomaneio;

    Pedido:string;
    Cliente:string;
    NomeCliente:string;
    Romaneio:string;
    ExisteProejetosSelecionados:Boolean;

    procedure MontaStringGrid;
    procedure LimpaStringGrid;
  public
    procedure PassaObjetos(Pedido,Cliente:string);
  end;

var
  FGeraRomaneioPedido: TFGeraRomaneioPedido;

implementation

uses UescolheImagemBotao, Upesquisa, UCOLOCADOR, UTRANSPORTADORA, Math;

{$R *.dfm}

procedure TFGeraRomaneioPedido.FormShow(Sender: TObject);
begin
  FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
  
  ObjPedidoProjetoRomaneio:=TObjPedidoProjetoRomaneio.Create(self);
  LimpaStringGrid;
  MontaStringGrid;

  lbEntrega.Caption:=DateToStr(Now);
  lbDataEmissao.Caption:=DateToStr(Now);
  lbColocador.Caption:='';
  lbtransportadora.Caption:='';
  ExisteProejetosSelecionados:=True;
end;

procedure TFGeraRomaneioPedido.PassaObjetos(Pedido,Cliente:string);
begin
   self.Pedido:=Pedido;
   self.Cliente:=Cliente;
   Self.NomeCliente:= get_campoTabela('nome','codigo','tabcliente',Cliente); 

   lbPedido.Caption  := self.Pedido;
   lbCliente.Caption := self.Cliente+' - '+self.NomeCliente;

end;

procedure TFGeraRomaneioPedido.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  ObjPedidoProjetoRomaneio.Free;
end;

procedure TFGeraRomaneioPedido.btLocalizarClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Fcolocador:TFColocador;
   SQL:string;
begin
  Try
    Fpesquisalocal:=Tfpesquisa.create(Nil);
    FColocador:=TFCOLOCADOR.Create(nil);

    SQL:= 'SELECT F.NOME AS COLOCADOR,C.CODIGO,F.CODIGO AS CODIGOFUNCIONARIO '+
    'FROM TABCOLOCADOR C '+
    'JOIN TABFUNCIONARIOS F ON F.CODIGO=C.FUNCIONARIO';

    If (FpesquisaLocal.PreparaPesquisa(SQL,'Pesquisa local de colocadores',Fcolocador)=True)
    Then Begin
      Try
        If (FpesquisaLocal.showmodal=mrok)Then
        Begin
          lbColocador.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('CODIGO').asstring+ ' - '+ FpesquisaLocal.QueryPesq.fieldbyname('COLOCADOR').asstring;
        End;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;

  Finally
    FreeandNil(FPesquisaLocal);
    FreeandNil(Fcolocador);
  End;
end;

Procedure TFGeraRomaneioPedido.MontaStringGrid;
var
  Qry_Pesquisa:TibQuery;
begin
  STRGPedidoProjeto.RowCount:=2;
  STRGPedidoProjeto.ColCount:=4;
  STRGPedidoProjeto.FixedRows:=0;

  STRGPedidoProjeto.ColWidths[0]:=50;
  STRGPedidoProjeto.ColWidths[1]:=400;
  STRGPedidoProjeto.ColWidths[2]:=50;
  STRGPedidoProjeto.ColWidths[3]:=200;


  STRGPedidoProjeto.Cells[0,0]:='            X';
  STRGPedidoProjeto.Cells[1,0]:='';

  if(Pedido<>'') then
  begin
    try
      Qry_Pesquisa:=TIBQuery.Create(nil);
      Qry_Pesquisa.Database:=FDataModulo.IBDatabase;
    except
      Exit;
    end;

    try
      Qry_Pesquisa.Close;
      Qry_Pesquisa.SQL.Clear;
      Qry_Pesquisa.SQL.Text:=
      'Select viewpedidoprojeto.* from viewpedidoprojeto '+
      'left join tabpedidoprojetoromaneio on tabpedidoprojetoromaneio.pedidoprojeto=viewpedidoprojeto.codigo '+
      'where viewpedidoprojeto.pedido= '+Pedido+'  '+
      'and TabPedidoProjetoromaneio.Codigo is null';

      Qry_Pesquisa.Open;

      while not Qry_Pesquisa.Eof do
      begin
        STRGPedidoProjeto.Cells[0,STRGPedidoProjeto.RowCount-1]:='            X';
        STRGPedidoProjeto.Cells[1,STRGPedidoProjeto.RowCount-1]:=Qry_Pesquisa.Fieldbyname('Descricao').AsString;
        STRGPedidoProjeto.Cells[2,STRGPedidoProjeto.RowCount-1]:=Qry_Pesquisa.Fieldbyname('codigo').AsString;
        STRGPedidoProjeto.Cells[3,STRGPedidoProjeto.RowCount-1]:=Qry_Pesquisa.Fieldbyname('local').AsString;
        STRGPedidoProjeto.RowCount:=STRGPedidoProjeto.RowCount+1;
        Qry_Pesquisa.Next;
      end;

      STRGPedidoProjeto.RowCount:=STRGPedidoProjeto.RowCount-1;
    finally
      FreeAndNil(Qry_Pesquisa);
    end;


  end;

end;


procedure TFGeraRomaneioPedido.STRGPedidoProjetoDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}  
begin
    if(ACol = 0)
    then begin
        if(STRGPedidoProjeto.Cells[ACol,ARow] = '            X' ) or (STRGPedidoProjeto.Cells[ACol,ARow] = 'X' )then
        begin
              Ilprodutos.Draw(STRGPedidoProjeto.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 6); //check box marcado
        end
        else
        begin
              Ilprodutos.Draw(STRGPedidoProjeto.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 5); //check box sem marcar    5
        end;
    end;
    {if(ACol = 1)
    then begin
        if(STRGPedidoProjeto.Cells[ACol,ARow] = '            X' ) or (STRGPedidoProjeto.Cells[ACol,ARow] = 'X' )then
        begin
              Ilprodutos.Draw(STRGPedidoProjeto.Canvas, LM*-1 + Rect.Left+1 , TM + Rect.Top+1, 9); //check box marcado
        end
        else
        begin
              Ilprodutos.Draw(STRGPedidoProjeto.Canvas, LM*-1 + Rect.Left+1 , TM + Rect.Top+1, 9); //check box sem marcar    5
        end;
    end; }
end;

procedure TFGeraRomaneioPedido.STRGPedidoProjetoDblClick(Sender: TObject);
var
  i:Integer;
begin
  if(STRGPedidoProjeto.Cells[0,STRGPedidoProjeto.row] = '') then
  begin
    STRGPedidoProjeto.Cells[0,STRGPedidoProjeto.Row]:='            X';
    if(STRGPedidoProjeto.row=0) then
    begin
      for i:=1 to STRGPedidoProjeto.RowCount-1 do
      begin
        STRGPedidoProjeto.Cells[0,i]:='            X'
      end;
    end;
    ExisteProejetosSelecionados:=True;
  end
  else
  begin
    STRGPedidoProjeto.Cells[0,STRGPedidoProjeto.Row]:='';
    if(STRGPedidoProjeto.row=0) then
    begin
      for i:=1 to STRGPedidoProjeto.RowCount-1 do
      begin
        STRGPedidoProjeto.Cells[0,i]:=''
      end;
    end;
    ExisteProejetosSelecionados:=False;
  end
end;

procedure TFGeraRomaneioPedido.btPesquisaTranspotadoraClick(
  Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FTransportador:TFTransportadora;
begin
  Try
    Fpesquisalocal:=Tfpesquisa.create(Nil);
    FTransportador:=TFTransportadora.Create(nil);
    If (FpesquisaLocal.PreparaPesquisa('SELECT * FROM TABTRANSPORTADORA','',FTransportador)=True) Then
    Begin
      Try
        If (FpesquisaLocal.showmodal=mrok)Then
        Begin
          lbtransportadora.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('CODIGO').asstring+ ' - '+ FpesquisaLocal.QueryPesq.fieldbyname('NOME').asstring;
        End;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;
  Finally
    FreeandNil(FPesquisaLocal);
    FreeandNil(FTransportador);
  End;
end;

procedure TFGeraRomaneioPedido.btAtualizarClick(Sender: TObject);
var
  DataEmissao:string;
begin
  DataEmissao:=DateToStr(Now);
  InputQuery('Data Emiss�o','Data Emiss�o',DataEmissao);
  lbDataEmissao.Caption:=DataEmissao;
end;

procedure TFGeraRomaneioPedido.bteditaentregaClick(Sender: TObject);
var
  Dataentrega:string;
begin
  Dataentrega:=DateToStr(Now);
  InputQuery('Data Entrega','Data Entrega',Dataentrega);
  lbEntrega.Caption:=Dataentrega;
end;

procedure TFGeraRomaneioPedido.btGerarRomaneioClick(Sender: TObject);
var
  i:Integer;
  addromaneio:String;
begin
  if(STRGPedidoProjeto.RowCount<=1) then
  begin
    MensagemAviso('N�o h� projetos para entregar neste pedido');
    Exit;
  end;
  if(lbColocador.Caption='') then
  begin
    MensagemAviso('Informe o colocador');
    Exit;
  end;
  if(lbtransportadora.Caption='') then
  begin
    MensagemAviso('Informe a transportadora');
    Exit;
  end;
  if(ExisteProejetosSelecionados=false) then
  begin
    MensagemAviso('Voc� n�o selecionou nenhum projeto para entregar!');
  end;


  addromaneio:=retornaPalavrasDepoisSimbolo(lbaddromaneioexistente.Caption,'-');

  if(addromaneio='')then
  begin
    Self.ObjPedidoProjetoRomaneio.Romaneio.Status:=dsInsert;
    With Self.ObjpedidoProjetoromaneio.ROMANEIO do
    Begin
      Submit_CODIGO('0');
      TRANSPORTADORA.Submit_codigo(Trim(retornaPalavrasAntesSimbolo(lbtransportadora.Caption,'-')));
      Submit_DataEmissao(lbDataEmissao.Caption);
      Submit_DataEntrega(lbEntrega.Caption);
      Submit_Concluido('N');
      Colocador.Submit_CODIGO(Trim(retornaPalavrasAntesSimbolo(lbColocador.Caption,'-')));
      If (Self.ObjpedidoProjetoromaneio.ROMANEIO.salvar(true)=False)
      Then exit;
      Romaneio:=Self.ObjpedidoProjetoromaneio.ROMANEIO.Get_codigo;
    End;
  end
  else
  begin
     Self.ObjPedidoProjetoRomaneio.Romaneio.LocalizaCodigo(trim(addromaneio));
     Self.ObjPedidoProjetoRomaneio.Romaneio.TabelaparaObjeto;
     Romaneio:=Self.ObjPedidoProjetoRomaneio.Romaneio.Get_CODIGO;
  end;

  
  for i:=1 to STRGPedidoProjeto.RowCount-1 do
  begin
    if(STRGPedidoProjeto.Cells[0,i]='            X')then
    begin
      ObjPedidoProjetoRomaneio.ZerarTabela;
      ObjPedidoProjetoRomaneio.Submit_CODIGO('0');
      ObjPedidoProjetoRomaneio.Romaneio.Submit_CODIGO(Romaneio);
      ObjPedidoProjetoRomaneio.PedidoProjeto.Submit_Codigo(STRGPedidoProjeto.Cells[2,i]);
      ObjPedidoProjetoRomaneio.status:=dsinsert;

      if (ObjPedidoProjetoRomaneio.Salvar(True)=False)
      Then exit;
    end;
  end;
  LimpaStringGrid;
  MontaStringGrid;
end;

procedure TFGeraRomaneioPedido.LimpaStringGrid;
var
  i:integer;
begin
  for i:=0 to STRGPedidoProjeto.RowCount-1 do
  begin
    STRGPedidoProjeto.Cells[0,i]:='';
    STRGPedidoProjeto.Cells[1,i]:='';
    STRGPedidoProjeto.Cells[2,i]:='';
    STRGPedidoProjeto.Cells[3,i]:='';
  end;
end;

procedure TFGeraRomaneioPedido.btCancelarClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TFGeraRomaneioPedido.lbaddromaneioexistenteMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFGeraRomaneioPedido.lbaddromaneioexistenteMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
       TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFGeraRomaneioPedido.lbaddromaneioexistenteClick(
  Sender: TObject);
begin
  FfiltroImp.DesativaGrupos;
  FfiltroImp.Grupo01.Enabled:=True;

  FfiltroImp.edtgrupo01.Text:='';
  FfiltroImp.edtgrupo01.Color:=$005CADFE;
  FfiltroImp.LbGrupo01.Caption:='Romaneio';
  FfiltroImp.edtgrupo01.OnKeyDown:=edt1KeyDown;

  FfiltroImp.ShowModal;

  If (FfiltroImp.Tag=0)
  then exit;
  if(FfiltroImp.edtgrupo01.Text<>'')
  then lbaddromaneioexistente.Caption := 'Adicionar itens no romaneio - '+FfiltroImp.edtgrupo01.Text
  else lbaddromaneioexistente.Caption := 'Adicionar � romaneio existente';

  lbaddromaneioexistente.WordWrap:=True;

end;

procedure TFGeraRomaneioPedido.edt1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:TFpesquisa;
begin
  if(Key<>VK_F9)
  then exit;

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabromaneio','Romaneios',Nil)=True)
            Then Begin
                try
                   If (FpesquisaLocal.showmodal=mrok)
                   Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('CODIGO').asstring;

                   End;
                Finally
                   FpesquisaLocal.QueryPesq.close;
                End;
            End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;
end.

