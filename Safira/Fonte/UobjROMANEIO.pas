unit UobjROMANEIO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,uobjTRANSPORTADORA,UobjColocador;

Type
   TObjROMANEIO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                TRANSPORTADORA:TOBJTRANSPORTADORA;
                Colocador:TobjColocador;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Dataemissao(parametro: string);
                Function Get_DataEmissao: string;
                Procedure Submit_Observacoes(parametro: String);
                Function Get_Observacoes: string;
                Procedure Submit_Concluido(parametro: string);
                Function Get_Concluido: string;
                function Get_DataEntrega:string;
                Procedure Submit_DataEntrega(parametro:string);

                procedure EdtTRANSPORTADORAExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtTRANSPORTADORAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtColocadorExit(Sender: TObject; LABELNOME: TLABEL);
                procedure EdtColocadorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);overload;
                procedure EdtColocadorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
//CODIFICA DECLARA GETSESUBMITS


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Dataemissao:string;
               DataEntrega:string;
               Observacoes:String;
               Concluido:string;
//CODIFICA VARIAVEIS PRIVADAS






               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
  UMenuRelatorios, UObjFUNCIONARIOS;


{ TTabTitulo }


Function  TObjROMANEIO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('TRANSPORTADORA').asstring<>'')
        Then Begin
                 If (Self.TRANSPORTADORA.LocalizaCodigo(FieldByName('TRANSPORTADORA').asstring)=False)
                 Then Begin
                          Messagedlg('Transportadora N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.TRANSPORTADORA.TabelaparaObjeto;
        End;

        If(FieldByName('Colocador').asstring<>'')
        Then Begin
                 If (Self.Colocador.LocalizaCodigo(FieldByName('Colocador').asstring)=False)
                 Then Begin
                          Messagedlg('Colocador N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Colocador.TabelaparaObjeto;
        End;

        Self.DataEmissao:=fieldbyname('DataEmissao').asstring;
        Self.Dataentrega:=fieldbyname('DataEntrega').asstring;
        Self.Observacoes:=fieldbyname('Observacoes').asstring;
        Self.Concluido:=fieldbyname('Concluido').asstring;
//CODIFICA TABELAPARAOBJETO





        result:=True;
     End;
end;


Procedure TObjROMANEIO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('TRANSPORTADORA').asstring:=Self.TRANSPORTADORA.GET_CODIGO;
        ParamByName('Colocador').asstring:=Self.Colocador.GET_CODIGO;
        ParamByName('DataEmissao').asstring:=Self.Dataemissao;
        ParamByName('DataEntrega').asstring:=Self.DataEntrega;
        ParamByName('Observacoes').asstring:=Self.Observacoes;
        ParamByName('Concluido').asstring:=Self.Concluido;
//CODIFICA OBJETOPARATABELA





  End;
End;

//***********************************************************************

function TObjROMANEIO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjROMANEIO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        TRANSPORTADORA.ZerarTabela;
        Colocador.ZerarTabela;
        Dataemissao:='';
        DataEntrega:='';
        Observacoes:='';
        Concluido:='';
//CODIFICA ZERARTABELA





     End;
end;

Function TObjROMANEIO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Concluido='')
      Then Mensagem:=mensagem+'/Conclu�do';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjROMANEIO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     If (Self.TRANSPORTADORA.LocalizaCodigo(Self.TRANSPORTADORA.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Transportadora n�o Encontrado!';

     If (Self.Colocador.LocalizaCodigo(Self.Colocador.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Colocador n�o Encontrado!';

//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjROMANEIO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.TRANSPORTADORA.Get_Codigo<>'')
        Then Strtoint(Self.TRANSPORTADORA.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Transportadora';
     End;

     try
        If (Self.Colocador.Get_Codigo<>'')
        Then Strtoint(Self.Colocador.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Colocador';
     End;

//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjROMANEIO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try

        Strtodate(Self.Dataentrega);
     Except
           Mensagem:=mensagem+'/Data de entrega';
     End;

     try

        Strtodate(Self.DataEmissao);
     Except
           Mensagem:=mensagem+'/Data de Emiss�o';
     End;


//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjROMANEIO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
      If not( (CONCLUIDO='S') OR (CONCLUIDO='N'))
      Then Mensagem:=mensagem+'/Valor Inv�lido para o campo Conclu�do';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjROMANEIO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro ROMANEIO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,TRANSPORTADORA,colocador,DataEmissao,Dataentrega,Observacoes,Concluido');
           SQL.ADD(' from  TabRomaneio');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjROMANEIO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjROMANEIO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjROMANEIO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.TRANSPORTADORA:=TOBJTRANSPORTADORA.create;
        self.Colocador:=TobjColocador.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabRomaneio(CODIGO,TRANSPORTADORA,colocador,DataEmissao,Dataentrega,Observacoes');
                InsertSQL.add(' ,Concluido)');
                InsertSQL.add('values (:CODIGO,:TRANSPORTADORA,:colocador,:DataEmissao,:Dataentrega,:Observacoes,:Concluido');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabRomaneio set CODIGO=:CODIGO,TRANSPORTADORA=:TRANSPORTADORA,colocador=:colocador,');
                ModifySQL.add('DataEmissao=:Dataemissao,Dataentrega=:Dataentrega,Observacoes=:Observacoes,Concluido=:Concluido');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabRomaneio where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjROMANEIO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjROMANEIO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabROMANEIO');
     Result:=Self.ParametroPesquisa;
end;

function TObjROMANEIO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de ROMANEIO ';
end;


function TObjROMANEIO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENROMANEIO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENROMANEIO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjROMANEIO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.TRANSPORTADORA.FREE;
    Self.Colocador.Free;
    
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjROMANEIO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjROMANEIO.RetornaCampoNome: string;
begin
      result:='Nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjRomaneio.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjRomaneio.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjRomaneio.Submit_Observacoes(parametro: STRing);
begin
        Self.Observacoes:=Parametro;
end;
function TObjRomaneio.Get_Observacoes: STRing;
begin
        Result:=Self.Observacoes;
end;
procedure TObjRomaneio.Submit_Concluido(parametro: string);
begin
        Self.Concluido:=Parametro;
end;
function TObjRomaneio.Get_Concluido: string;
begin
        Result:=Self.Concluido;
end;
//CODIFICA GETSESUBMITS


procedure TObjROMANEIO.EdtTRANSPORTADORAExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.TRANSPORTADORA.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.TRANSPORTADORA.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.TRANSPORTADORA.GET_NOME;
End;
procedure TObjROMANEIO.EdtTRANSPORTADORAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.TRANSPORTADORA.Get_Pesquisa,Self.TRANSPORTADORA.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TRANSPORTADORA.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.TRANSPORTADORA.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TRANSPORTADORA.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;



//CODIFICA EXITONKEYDOWN
procedure TObjROMANEIO.EdtColocadorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Colocador.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Colocador.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Colocador.Funcionario.Get_Nome;
End;
procedure TObjROMANEIO.EdtColocadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Colocador.Get_Pesquisa,Self.Colocador.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Colocador.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Colocador.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Colocador.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjROMANEIO.EdtColocadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Colocador.Get_Pesquisa,Self.Colocador.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Colocador.RETORNACAMPOCODIGO).asstring;
                                
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjROMANEIO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJROMANEIO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:Begin
            End;
          End;
     end;

end;



function TObjROMANEIO.Get_DataEmissao: string;
begin
     Result:=self.Dataemissao;
end;

function TObjROMANEIO.Get_DataEntrega: string;
begin
     Result:=Self.DataEntrega;
end;

procedure TObjROMANEIO.Submit_Dataemissao(parametro: string);
begin
     Self.Dataemissao:=parametro;
end;

procedure TObjROMANEIO.Submit_DataEntrega(parametro: string);
begin
     if (trim(comebarra(parametro))='')
     Then parametro:='';
     
     Self.DataEntrega:=parametro;
end;

end.



