unit UTIPOCLIENTE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjTIPOCLIENTE,
  jpeg,IBQuery;

type
  TFTIPOCLIENTE = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigoTipoCliente: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btgravar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    edtnome: TEdit;
    lbLbNOME: TLabel;
    pnl1: TPanel;
    imgrodape: TImage;
    lb14: TLabel;
    btAjuda: TSpeedButton;
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btAjudaClick(Sender: TObject);
  private
         ObjTIPOCLIENTE:TObjTIPOCLIENTE;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         procedure mostraquantidadecadastrada;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FTIPOCLIENTE: TFTIPOCLIENTE;


implementation

uses UessencialGlobal, Upesquisa, UDataModulo, UescolheImagemBotao,
  Uprincipal, UAjuda;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFTIPOCLIENTE.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjTIPOCLIENTE do
    Begin
        Submit_CODIGO(lbCodigoTipoCliente.caption);
        Submit_NOME(edtNOME.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFTIPOCLIENTE.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjTIPOCLIENTE do
     Begin
        lbCodigoTipoCliente.caption:=Get_CODIGO;
        EdtNOME.text:=Get_NOME;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFTIPOCLIENTE.TabelaParaControles: Boolean;
begin
     If (Self.ObjTIPOCLIENTE.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;


procedure TFTIPOCLIENTE.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjTIPOCLIENTE=Nil)
     Then exit;

    If (Self.ObjTIPOCLIENTE.status<>dsinactive)
    Then Begin
              Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
              abort;
              exit;
    End;

    Self.ObjTIPOCLIENTE.free;
    self.Tag:=0;
end;

procedure TFTIPOCLIENTE.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFTIPOCLIENTE.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);
     lbCodigoTipoCliente.caption:='0';
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;
     btalterar.visible:=false;
     btrelatorio.visible:=false;
     btsair.Visible:=false;
     btopcoes.visible:=false;
     btexcluir.Visible:=false;
     btnovo.Visible:=false;
     btopcoes.visible:=false;
     Self.ObjTIPOCLIENTE.status:=dsInsert;
     edtNOME.setfocus;
end;


procedure TFTIPOCLIENTE.btalterarClick(Sender: TObject);
begin
    If (Self.ObjTIPOCLIENTE.Status=dsinactive) and (lbCodigoTipoCliente.caption<>'')
    Then Begin
                habilita_campos(Self);

                Self.ObjTIPOCLIENTE.Status:=dsEdit;

                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtNOME.setfocus;
                 btalterar.visible:=false;
               btrelatorio.visible:=false;
               btsair.Visible:=false;
               btopcoes.visible:=false;
               btexcluir.Visible:=false;
               btnovo.Visible:=false;
               btopcoes.visible:=false;

                
          End;


end;

procedure TFTIPOCLIENTE.btgravarClick(Sender: TObject);
begin

     If Self.ObjTIPOCLIENTE.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjTIPOCLIENTE.salvar(true)=False)
     Then exit;

     lbCodigoTipoCliente.caption:=Self.ObjTIPOCLIENTE.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btalterar.visible:=true;
     btrelatorio.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
      btopcoes.visible:=true;
      lbCodigoTipoCliente.Caption:='';
      mostraquantidadecadastrada;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFTIPOCLIENTE.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjTIPOCLIENTE.status<>dsinactive) or (lbCodigoTipoCliente.caption='')
     Then exit;

     If (Self.ObjTIPOCLIENTE.LocalizaCodigo(lbCodigoTipoCliente.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjTIPOCLIENTE.exclui(lbCodigoTipoCliente.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFTIPOCLIENTE.btcancelarClick(Sender: TObject);
begin
     Self.ObjTIPOCLIENTE.cancelar;
      btalterar.visible:=true;
     btrelatorio.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
     lbCodigoTipoCliente.Caption:='';
      btopcoes.visible:=true;
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFTIPOCLIENTE.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFTIPOCLIENTE.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjTIPOCLIENTE.Get_pesquisa,Self.ObjTIPOCLIENTE.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjTIPOCLIENTE.status<>dsinactive
                                  then exit;

                                  If (Self.ObjTIPOCLIENTE.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjTIPOCLIENTE.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFTIPOCLIENTE.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFTIPOCLIENTE.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);


     Try
        Self.ObjTIPOCLIENTE:=TObjTIPOCLIENTE.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSAR O CADASTRO DE TIPO DE CLIENTE')=false)
     then desab_botoes(self)
     Else habilita_botoes(self);

     lbCodigoTipoCliente.caption:='';

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(imgrodape,'RODAPE');

     mostraquantidadecadastrada;

     if(Tag<>0)
     then begin
        if(ObjTIPOCLIENTE.LocalizaCodigo(IntToStr(tag))=True)
        then begin
            ObjTIPOCLIENTE.TabelaparaObjeto;
            self.ObjetoParaControles;
        end;

     end;
     
end;

procedure TFTIPOCLIENTE.mostraquantidadecadastrada;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabtipocliente');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb14.Caption:='Existem '+IntToStr(contador)+' tipos cadastrados'
       else
       begin
            lb14.Caption:='Existe '+IntToStr(contador)+' tipo cadastrado';
       end;

    finally

    end;


end;


//CODIFICA ONKEYDOWN E ONEXIT


procedure TFTIPOCLIENTE.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if(key=vk_f1) then
    begin
        Fprincipal.chamaPesquisaMenu;
    end;
    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('TIPOCLIENTE');
         FAjuda.ShowModal;
    end;
end;

procedure TFTIPOCLIENTE.btAjudaClick(Sender: TObject);
begin
     FAjuda.PassaAjuda('TIPOCLIENTE');
     FAjuda.ShowModal;
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjTIPOCLIENTE.OBJETO.Get_Pesquisa,Self.ObjTIPOCLIENTE.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjTIPOCLIENTE.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjTIPOCLIENTE.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjTIPOCLIENTE.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
