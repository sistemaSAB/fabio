unit UCOMPONENTE_PP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjCOMPONENTE_PP,IBQuery;

type
  TFCOMPONENTE_PP = class(TForm)
    edtPEDIDOPROJETO: TEdit;
    lbLbPEDIDOPROJETO: TLabel;
    lbLbCOMPONENTE: TLabel;
    edtCOMPONENTE: TEdit;
    edtALTURA: TEdit;
    edtLARGURA: TEdit;
    lbLbLARGURA: TLabel;
    lbLbALTURA: TLabel;
    imgrodape: TImage;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbcodigo: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
//DECLARA COMPONENTES
    procedure edtPEDIDOPROJETOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtPEDIDOPROJETOExit(Sender: TObject);
    procedure edtCOMPONENTEKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtCOMPONENTEExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtPEDIDOPROJETODblClick(Sender: TObject);
    procedure edtCOMPONENTEDblClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
         ObjCOMPONENTE_PP:TObjCOMPONENTE_PP;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCOMPONENTE_PP: TFCOMPONENTE_PP;


implementation

uses UessencialGlobal, Upesquisa, UDataModulo, UescolheImagemBotao,
  Uprincipal;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCOMPONENTE_PP.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjCOMPONENTE_PP do
    Begin
        Submit_CODIGO(lbcodigo.Caption);
        PEDIDOPROJETO.Submit_codigo(edtPEDIDOPROJETO.text);
        COMPONENTE.Submit_codigo(edtCOMPONENTE.text);
        Submit_LARGURA(edtLARGURA.text);
        Submit_ALTURA(edtALTURA.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCOMPONENTE_PP.ObjetoParaControles: Boolean;
var
  query:TIBQuery;
Begin
  Try
    { try
       try
         query:=TIBQuery.Create(nil);
         query.Database:=FDataModulo.IBDatabase;
         with query do
         begin
           close;
           sql.Clear;
           sql.Add('select descricao from tabcomponente where codigo='+Self.ObjCOMPONENTE_PP.Get_CODIGO);
           Open;
           lbNomeCOMPONENTE.Caption:=fieldbyname('descricao').AsString;
         end;
       except

       end;

     finally
            FreeAndNil(query);
     end;  }


     With Self.ObjCOMPONENTE_PP do
     Begin
       lbcodigo.Caption:=Get_CODIGO;
        EdtPEDIDOPROJETO.text:=PEDIDOPROJETO.Get_codigo;
        EdtCOMPONENTE.text:=COMPONENTE.Get_codigo;
        EdtLARGURA.text:=Get_LARGURA;
        EdtALTURA.text:=Get_ALTURA;

//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCOMPONENTE_PP.TabelaParaControles: Boolean;
begin
     If (Self.ObjCOMPONENTE_PP.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFCOMPONENTE_PP.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjCOMPONENTE_PP:=TObjCOMPONENTE_PP.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE COMPONENTES DOS PEDIDOS PROJETOS')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);
end;

procedure TFCOMPONENTE_PP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCOMPONENTE_PP=Nil)
     Then exit;

If (Self.ObjCOMPONENTE_PP.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjCOMPONENTE_PP.free;
end;

procedure TFCOMPONENTE_PP.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFCOMPONENTE_PP.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     //desab_botoes(Self);

     lbcodigo.Caption:='0';
     //edtcodigo.text:=Self.ObjCOMPONENTE_PP.Get_novocodigo;


     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjCOMPONENTE_PP.status:=dsInsert;
     edtPEDIDOPROJETO.setfocus;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

end;


procedure TFCOMPONENTE_PP.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCOMPONENTE_PP.Status=dsinactive) and (lbcodigo.Caption<>'')
    Then Begin
                habilita_campos(Self);
                Self.ObjCOMPONENTE_PP.Status:=dsEdit;

               // desab_botoes(Self);

                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtPEDIDOPROJETO.setfocus;
                btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;
                
          End;


end;

procedure TFCOMPONENTE_PP.btgravarClick(Sender: TObject);
begin

     If Self.ObjCOMPONENTE_PP.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjCOMPONENTE_PP.salvar(true)=False)
     Then exit;

     lbcodigo.Caption:=Self.ObjCOMPONENTE_PP.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCOMPONENTE_PP.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjCOMPONENTE_PP.status<>dsinactive) or (lbcodigo.Caption='')
     Then exit;

     If (Self.ObjCOMPONENTE_PP.LocalizaCodigo(lbcodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCOMPONENTE_PP.exclui(lbcodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFCOMPONENTE_PP.btcancelarClick(Sender: TObject);
begin
     Self.ObjCOMPONENTE_PP.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;
     lbcodigo.Caption:='';

end;

procedure TFCOMPONENTE_PP.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFCOMPONENTE_PP.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCOMPONENTE_PP.Get_pesquisa,Self.ObjCOMPONENTE_PP.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCOMPONENTE_PP.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCOMPONENTE_PP.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjCOMPONENTE_PP.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFCOMPONENTE_PP.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFCOMPONENTE_PP.FormShow(Sender: TObject);
begin
       FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     lbcodigo.caption:='';
end;
procedure TFCOMPONENTE_PP.edtPEDIDOPROJETOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjCOMPONENTE_PP.edtPEDIDOPROJETOkeydown(sender,key,shift,nil);
end;
 
procedure TFCOMPONENTE_PP.edtPEDIDOPROJETOExit(Sender: TObject);
begin
    ObjCOMPONENTE_PP.edtPEDIDOPROJETOExit(sender,nil);
end;
procedure TFCOMPONENTE_PP.edtCOMPONENTEKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjCOMPONENTE_PP.edtCOMPONENTEkeydown(sender,key,shift,nil);
end;
 
procedure TFCOMPONENTE_PP.edtCOMPONENTEExit(Sender: TObject);
begin
    ObjCOMPONENTE_PP.edtCOMPONENTEExit(sender,nil);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFCOMPONENTE_PP.edtPEDIDOPROJETODblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;

begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from TabPEDIDO_PROJ','',nil)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 edtPEDIDOPROJETO.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;


                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);


     End;
end;




procedure TFCOMPONENTE_PP.edtCOMPONENTEDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;

begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from TabCOMPONENTE','',nil)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 edtCOMPONENTE.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;


                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);


     End;
end;


procedure TFCOMPONENTE_PP.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      if(key=vk_f1) then
    begin
        Fprincipal.chamaPesquisaMenu;
    end;
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjCOMPONENTE_PP.OBJETO.Get_Pesquisa,Self.ObjCOMPONENTE_PP.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjCOMPONENTE_PP.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjCOMPONENTE_PP.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjCOMPONENTE_PP.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
