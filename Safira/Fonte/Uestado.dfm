object Festado: TFestado
  Left = 520
  Top = 354
  Width = 718
  Height = 269
  Caption = 'Festado'
  Color = 10643006
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lb1: TLabel
    Left = 28
    Top = 75
    Width = 39
    Height = 14
    Caption = 'C'#243'digo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb2: TLabel
    Left = 28
    Top = 95
    Width = 89
    Height = 14
    Caption = 'Nome do Estado'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb3: TLabel
    Left = 28
    Top = 120
    Width = 45
    Height = 14
    Caption = 'Al'#237'quota'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edtCodigo: TEdit
    Left = 132
    Top = 72
    Width = 70
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 2
    TabOrder = 0
  end
  object edtnome: TEdit
    Left = 132
    Top = 95
    Width = 301
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 40
    TabOrder = 1
  end
  object edtaliquota: TEdit
    Left = 132
    Top = 118
    Width = 70
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 9
    TabOrder = 2
    OnKeyPress = edtaliquotaKeyPress
  end
  object pnlrodape: TPanel
    Left = 0
    Top = 181
    Width = 702
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 3
    DesignSize = (
      702
      50)
    object lbquantidade: TLabel
      Left = 380
      Top = 16
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object imgRodape: TImage
      Left = 0
      Top = 0
      Width = 702
      Height = 50
      Align = alClient
      Stretch = True
    end
    object bt1: TBitBtn
      Left = 856
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object bt2: TBitBtn
      Left = 856
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 702
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 4
    object lbnomeformulario: TLabel
      Left = 576
      Top = 0
      Width = 126
      Height = 50
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Estados'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -29
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btNovo: TBitBtn
      Left = 0
      Top = -3
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtNovoClick
    end
    object btalterar: TBitBtn
      Left = 50
      Top = -3
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 100
      Top = -3
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BtgravarClick
    end
    object btCancelar: TBitBtn
      Left = 150
      Top = -3
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = BtCancelarClick
    end
    object btexcluir: TBitBtn
      Left = 200
      Top = -3
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btpesquisar: TBitBtn
      Left = 250
      Top = -3
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 300
      Top = -3
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
    end
    object btOpcoes: TBitBtn
      Left = 350
      Top = -3
      Width = 50
      Height = 52
      Caption = '&O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
    object btsair: TBitBtn
      Left = 400
      Top = -3
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
  end
end
