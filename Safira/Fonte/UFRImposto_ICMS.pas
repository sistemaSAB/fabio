unit UFRImposto_ICMS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  db,Dialogs, Grids, StdCtrls, ExtCtrls,udatamodulo,uobjmaterial_ICMS,
  ComCtrls, Menus, Buttons;

Type
  TGrupoImpostos = (GI_Geral,GI_Icms,GI_IcmsST,GI_IPI,GI_Pis,GI_PisST,GI_Cofins,GI_CofinsST);


type
  TFRImposto_ICMS = class(TFrame)
    Panel_TOP: TPanel;
    STRG_Imposto: TStringGrid;
    Label17: TLabel;
    Label18: TLabel;
    lbnometipocliente: TLabel;
    lbcodigo_material_icms: TLabel;
    Label1: TLabel;
    edtestado_material_icms: TEdit;
    edttipocliente_material_icms: TEdit;
    Aba_Impostos: TPageControl;
    TabSheet1: TTabSheet;
    LbMODALIDADE: TLabel;
    LbPERC_REDUCAO_BC: TLabel;
    LbALIQUOTA: TLabel;
    LbIVA: TLabel;
    LbPAUTA: TLabel;
    LbMODALIDADE_ST: TLabel;
    LbPERC_REDUCAO_BC_ST: TLabel;
    LbALIQUOTA_ST: TLabel;
    LbIVA_ST: TLabel;
    LbPAUTA_ST: TLabel;
    Label2: TLabel;
    lbnomeCFOP_ICMS: TLabel;
    Label3: TLabel;
    LbNomeSTA_ICMS: TLabel;
    LbNomeSTB_ICMS: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdtPERC_REDUCAO_BC_ICMS: TEdit;
    EdtALIQUOTA_ICMS: TEdit;
    EdtIVA_ICMS: TEdit;
    EdtPAUTA_ICMS: TEdit;
    EdtPERC_REDUCAO_BC_ST_ICMS: TEdit;
    EdtALIQUOTA_ST_ICMS: TEdit;
    EdtIVA_ST_ICMS: TEdit;
    EdtPAUTA_ST_ICMS: TEdit;
    EdtCFOP_ICMS: TEdit;
    ComboMODALIDADE_ICMS: TComboBox;
    ComboMODALIDADE_ST_ICMS: TComboBox;
    edtSTA_ICMS: TEdit;
    EdtSTB_ICMS: TEdit;
    edtformula_bc_ICMS: TEdit;
    edtformula_bc_st_ICMS: TEdit;
    Label19: TLabel;
    EDTImpostoModelo_ICMS: TEdit;
    PanelBotoes: TPanel;
    BtGravar_material_icms: TButton;
    btcancelar_material_ICMS: TButton;
    btexcluir_material_ICMS: TButton;
    TabSheet2: TTabSheet;
    Label6: TLabel;
    edtimposto_ipi_modelo: TEdit;
    LbST: TLabel;
    LbNomeST_IPI: TLabel;
    LbClasseEnq: TLabel;
    LbCodigoEnquadramento: TLabel;
    LbCnpjProdutor: TLabel;
    LbCodigoSelo: TLabel;
    LbTipoCalculo: TLabel;
    LbQuantidadeTotalUP: TLabel;
    LbValorUnidade: TLabel;
    LbFORMULABASECALCULO: TLabel;
    EdtST_IPI: TEdit;
    EdtClasseEnq_IPI: TEdit;
    EdtCodigoEnquadramento_IPI: TEdit;
    EdtCnpjProdutor_IPI: TEdit;
    EdtCodigoSelo_IPI: TEdit;
    EdtQuantidadeTotalUP_IPI: TEdit;
    EdtValorUnidade_IPI: TEdit;
    EdtFORMULABASECALCULO_IPI: TEdit;
    ComboTipoCalculo_IPI: TComboBox;
    Label7: TLabel;
    edtaliquota_ipi: TEdit;
    TabSheet3: TTabSheet;
    Label9: TLabel;
    EdtST_PIS: TEdit;
    LbNomeST_PIS: TLabel;
    Label10: TLabel;
    LbALIQUOTAPERCENTUAL: TLabel;
    EdtALIQUOTAPERCENTUAL_PIS: TEdit;
    LbALIQUOTAVALOR: TLabel;
    EdtALIQUOTAVALOR_PIS: TEdit;
    LbTIPOCALCULO_ST: TLabel;
    LbALIQUOTAPERCENTUAL_ST: TLabel;
    EdtALIQUOTAPERCENTUAL_ST_PIS: TEdit;
    LbALIQUOTAVALOR_ST: TLabel;
    EdtALIQUOTAVALOR_ST_PIS: TEdit;
    ComboTipoCalculo_PIS: TComboBox;
    comboTIPOCALCULO_ST_PIS: TComboBox;
    EdtFORMULABASECALCULO_PIS: TEdit;
    Label11: TLabel;
    EdtFORMULABASECALCULO_ST_PIS: TEdit;
    Label12: TLabel;
    edtimposto_pis_modelo: TEdit;
    Label13: TLabel;
    TabSheet4: TTabSheet;
    Label8: TLabel;
    LbNomeST_COFINS: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    EdtST_COFINS: TEdit;
    EdtALIQUOTAPERCENTUAL_COFINS: TEdit;
    EdtALIQUOTAVALOR_COFINS: TEdit;
    EdtALIQUOTAPERCENTUAL_ST_COFINS: TEdit;
    EdtALIQUOTAVALOR_ST_COFINS: TEdit;
    ComboTipoCalculo_COFINS: TComboBox;
    comboTIPOCALCULO_ST_COFINS: TComboBox;
    EdtFORMULABASECALCULO_COFINS: TEdit;
    EdtFORMULABASECALCULO_ST_COFINS: TEdit;
    edtimposto_cofins_modelo: TEdit;
    Label25: TLabel;
    Label26: TLabel;
    edtFORMULA_VALOR_IMPOSTO_ICMS: TEdit;
    Label27: TLabel;
    edtFORMULA_VALOR_IMPOSTO_ST_ICMS: TEdit;
    Label28: TLabel;
    edtFORMULA_VALOR_IMPOSTO_IPI: TEdit;
    Label29: TLabel;
    edtFORMULA_VALOR_IMPOSTO_PIS: TEdit;
    Label30: TLabel;
    edtFORMULA_VALOR_IMPOSTO_ST_PIS: TEdit;
    Label31: TLabel;
    edtFORMULA_VALOR_IMPOSTO_COFINS: TEdit;
    edtFORMULA_VALOR_IMPOSTO_ST_COFINS: TEdit;
    Label32: TLabel;
    RICH_COPIA: TRichEdit;
    POPUPMenuVariaveisICMS: TPopupMenu;
    PModelo: TMenuItem;
    POPUPMenuVariaveisICMS_ST: TPopupMenu;
    PopupMenuVariaveisIPI: TPopupMenu;
    PopupMenuVariaveisPIS: TPopupMenu;
    PopupMenuVariaveisPIS_ST: TPopupMenu;
    PopupMenuVariaveisCOFINS: TPopupMenu;
    PopupMenuVariaveisCOFINS_ST: TPopupMenu;
    edtoperacao: TEdit;
    Label33: TLabel;
    Button1: TButton;
    bvl1: TBevel;
    lb1: TLabel;
    edtCFOPForaEstado: TEdit;
    lbCfopForaEstado: TLabel;
    edtCSOSN: TEdit;
    Label34: TLabel;
    btReplicarImpostos: TSpeedButton;
    edtpercentualtributo: TEdit;
    lb2: TLabel;
    procedure BtGravar_material_icmsClick(Sender: TObject);
    procedure edttipocliente_material_icmsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edttipocliente_material_icmsExit(Sender: TObject);
    procedure EDTImpostoModelo_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EDTImpostoModelo_ICMSExit(Sender: TObject);
    procedure STRG_ImpostoDblClick(Sender: TObject);
    procedure btexcluir_material_ICMSClick(Sender: TObject);
    procedure btcancelar_material_ICMSClick(Sender: TObject);
    procedure edtSTA_ICMSExit(Sender: TObject);
    procedure edtSTA_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtSTB_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtSTB_ICMSExit(Sender: TObject);
    procedure EdtCFOP_ICMSExit(Sender: TObject);
    procedure EdtCFOP_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtimposto_ipi_modeloExit(Sender: TObject);
    procedure edtimposto_ipi_modeloKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtST_IPIKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtST_IPIExit(Sender: TObject);
    procedure edtimposto_pis_modeloExit(Sender: TObject);
    procedure edtimposto_pis_modeloKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtST_PISExit(Sender: TObject);
    procedure EdtST_PISKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtimposto_cofins_modeloExit(Sender: TObject);
    procedure edtimposto_cofins_modeloKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure EdtST_COFINSExit(Sender: TObject);
    procedure EdtST_COFINSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtformula_bc_ICMSEnter(Sender: TObject);
    procedure edtformula_bc_ICMSExit(Sender: TObject);
    procedure PModeloClick(Sender: TObject);
    procedure edtoperacaoExit(Sender: TObject);
    procedure edtoperacaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure edtCFOPForaEstadoExit(Sender: TObject);
    procedure edtCFOPForaEstadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCSOSNKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edttipocliente_material_icmsDblClick(Sender: TObject);
    procedure EDTImpostoModelo_ICMSDblClick(Sender: TObject);
    procedure edtSTA_ICMSDblClick(Sender: TObject);
    procedure EdtSTB_ICMSDblClick(Sender: TObject);
    procedure edtCSOSNDblClick(Sender: TObject);
    procedure edtCFOPForaEstadoDblClick(Sender: TObject);
    procedure EdtCFOP_ICMSDblClick(Sender: TObject);
  private
    { Private declarations }
        EditLocal:TEdit;
        PcodigoMaterial:String;
        Permissao:boolean;
        PRocedure RetornaImpostos;
        Procedure TrataStiuacaoTributariaB;



  public
    { Public declarations }
    ObjMaterial_ICMS:TobjMaterial_ICMS;
    Procedure Acessaframe(Pcodigo:String);
    Function CriaMenu(Pmenu:TpopupMenu;PGrupo:TGrupoImpostos):boolean;
    Procedure RetornaVariaveisGrupo(Pgrupo:TGrupoImpostos);
  end;




implementation

uses UessencialGlobal, UobjIMPOSTO_ICMS, UobjIMPOSTO_IPI,
  UEscolheMateriaisReplicar, UMostraBarraProgresso;

{$R *.dfm}

procedure TFRImposto_ICMS.BtGravar_material_icmsClick(Sender: TObject);
var
PcodigoImposto:String;
PcodigoImposto_IPI:String;
PcodigoImposto_PIS:String;
PcodigoImposto_cofins:String;
CriaNovo,CriaNovoIPI,CriaNovoPIS,CriaNovocofins:boolean;
begin
     if (Self.Permissao=False)
     Then Begin
               MensagemErro('Seu usu�rio n�o tem permiss�o para alterar os impostos do produto');
               exit;
     End;

Try
    ObjMaterial_ICMS.ZerarTabela;
    CriaNovo:=False;
    CriaNovoIPI:=False;
    CriaNovoPIS:=False;
    CriaNovocofins:=False;
    
    PcodigoImposto:='';
    PcodigoImposto_IPI:='';
    PcodigoImposto_PIS:='';
    PcodigoImposto_cofins:='';

     IF (lbcodigo_material_icms.caption='')
     Then Begin
               ObjMaterial_ICMS.Submit_CODIGO('0');
               objmaterial_ICms.Status:=dsInsert;
               CriaNovo:=True;//indica um novo imposto
               CriaNovoIPI:=True;
               CriaNovoPis:=true;
               CriaNovocofins:=true;
     End
     Else Begin
               //Altera��o, se alguma informa��o do Imposto foi alterada
               //preciso criar um novo, pois ele pode ter sido usado
               //em uma NF e n�o posso perder o historico dele
               //Para reimpressao futuras ou geracao de arquivos para o governo

               //*********************ICMS***********************************
               if (ObjMaterial_ICMS.LocalizaCodigo(lbcodigo_material_icms.caption)=False)
               Then Begin
                         mensagemErro('Erro na tentativa de localizar o C�digo '+lbcodigo_material_icms.caption);
                         exit;
               End;
               ObjMaterial_ICMS.TabelaparaObjeto;

               PcodigoImposto:=ObjMaterial_ICMS.IMPOSTO.Get_CODIGO;
               PcodigoImposto_IPI:=ObjMaterial_ICMS.imposto_ipi.Get_CODIGO;
               PcodigoImposto_pis:=ObjMaterial_ICMS.imposto_pis.Get_CODIGO;
               PcodigoImposto_cofins:=ObjMaterial_ICMS.imposto_cofins.Get_CODIGO;
               
               //Conferindo se o imposto icms mudou algo
               With ObjMaterial_ICMS.IMPOSTO do
               Begin
                    if (EdtSTA_ICMS.text<>STA.Get_codigo)
                    Then CriaNovo:=True;
   
                    if (EdtSTB_ICMS.text<>STB.Get_codigo)
                    Then CriaNovo:=True;

                    if (EdtCFOP_ICMS.text<>CFOP.Get_codigo)
                    Then CriaNovo:=True;

                    if (edtCSOSN.Text <>CSOSN.Get_codigo)
                    then CriaNovo:=True;


                    if not((Get_MODALIDADE='') and (ComboMODALIDADE_ICMS.ItemIndex=-1))
                    Then BEgin
                            if (ComboMODALIDADE_ICMS.ItemIndex<>Strtoint(Get_MODALIDADE))
                            Then CriaNovo:=True;
                    End;

                    if not((Get_MODALIDADE_ST='') and (ComboMODALIDADE_ST_ICMS.ItemIndex=-1))
                    Then BEgin
                               if (ComboMODALIDADE_ST_ICMS.ItemIndex<>Strtoint(Get_MODALIDADE_ST))
                               Then CriaNovo:=True;
                    End;

                    if (EdtPERC_REDUCAO_BC_ICMS.text<>Get_PERC_REDUCAO_BC)
                    Then CriaNovo:=True;

                    if (EdtALIQUOTA_ICMS.text<>Get_ALIQUOTA)
                    Then CriaNovo:=True;

                    if (EdtIVA_ICMS.text<>Get_IVA)
                    Then CriaNovo:=True;

                    if (EdtPAUTA_ICMS.text<>Get_PAUTA)
                    Then CriaNovo:=True;


                    if (EdtPERC_REDUCAO_BC_ST_ICMS.text<>Get_PERC_REDUCAO_BC_ST)
                    Then CriaNovo:=True;

                    if (EdtALIQUOTA_ST_ICMS.text<>Get_ALIQUOTA_ST)
                    Then CriaNovo:=True;

                    if (EdtIVA_ST_ICMS.text<>Get_IVA_ST)
                    Then CriaNovo:=True;

                    if (EdtPAUTA_ST_ICMS.text<>Get_PAUTA_ST)
                    Then CriaNovo:=True;

                    if (edtformula_bc_ICMS.text<>Get_FORMULA_BC)
                    Then CriaNovo:=True;

                    if (edtformula_bc_ST_ICMS.text<>Get_FORMULA_BC_ST)
                    Then CriaNovo:=True;


                    if (edtformula_valor_imposto_ICMS.text<>Get_formula_valor_imposto)
                    Then CriaNovo:=True;

                    if (edtformula_valor_imposto_ST_ICMS.text<>Get_formula_valor_imposto_ST)
                    Then CriaNovo:=True;

                    if(edtCFOPForaEstado.Text<> Get_CFOP_FORAESTADO)
                    then CriaNovo:=True;

                    if(edtpercentualtributo.Text<>ObjMaterial_ICMS.Get_PERCENTUALTRIBUTO)
                    then CriaNovo:=True;

               End;

               With ObjMaterial_ICMS.imposto_ipi do
               Begin
                  CriaNovoIpi:=False;
                   //Conferindo se o imposto IPI mudou algo
                  if EdtST_ipi.text<>ST.Get_codigo
                  Then CriaNovoIpi:=True;

                  if EdtClasseEnq_ipi.text<>Get_ClasseEnq
                  Then CriaNovoIpi:=True;
                  if EdtCodigoEnquadramento_ipi.text<>Get_CodigoEnquadramento
                  Then CriaNovoIpi:=True;
                  if EdtCnpjProdutor_ipi.text<>Get_CnpjProdutor
                  Then CriaNovoIpi:=True;
                  if EdtCodigoSelo_ipi.text<>Get_CodigoSelo
                  Then CriaNovoIpi:=True;
                  if comboTipoCalculo_ipi.text<>Get_TipoCalculo
                  Then CriaNovoIpi:=True;
                  if EdtAliquota_ipi.text<>Get_Aliquota
                  Then CriaNovoIpi:=True;
                  if EdtQuantidadeTotalUP_ipi.text<>Get_QuantidadeTotalUP
                  Then CriaNovoIpi:=True;
                  if EdtValorUnidade_ipi.text<>Get_ValorUnidade
                  Then CriaNovoIpi:=True;
                  if EdtFORMULABASECALCULO_ipi.text<>Get_FORMULABASECALCULO
                  Then CriaNovoIpi:=True;
                  if Edtformula_valor_imposto_ipi.text<>Get_formula_valor_imposto
                  Then CriaNovoIpi:=True;
               End;

               With ObjMaterial_ICMS.imposto_PIS do
               Begin
                  CriaNovoPIS:=False;

                  if EdtST_PIS.text<>ST.Get_codigo
                  Then CriaNovoPis:=true;
                  if ComboTIPOCALCULO_PIS.text<>Get_TIPOCALCULO
                  Then CriaNovoPis:=true;
                  if EdtALIQUOTAPERCENTUAL_PIS.text<>Get_ALIQUOTAPERCENTUAL
                  Then CriaNovoPis:=true;
                  if EdtALIQUOTAVALOR_PIS.text<>Get_ALIQUOTAVALOR
                  Then CriaNovoPis:=true;
                  if ComboTIPOCALCULO_ST_PIS.text<>Get_TIPOCALCULO_ST
                  Then CriaNovoPis:=true;
                  if EdtALIQUOTAPERCENTUAL_ST_PIS.text<>Get_ALIQUOTAPERCENTUAL_ST
                  Then CriaNovoPis:=true;
                  if EdtALIQUOTAVALOR_ST_PIS.text<>Get_ALIQUOTAVALOR_ST
                  Then CriaNovoPis:=true;
                  if edtFORMULABASECALCULO_PIS.text<>Get_FORMULABASECALCULO
                  Then CriaNovoPis:=true;
                  if edtFORMULABASECALCULO_ST_PIS.text<>Get_FORMULABASECALCULO_ST
                  Then CriaNovoPis:=true;


                  if edtformula_valor_imposto_PIS.text<>Get_formula_valor_imposto
                  Then CriaNovoPis:=true;
                  if edtformula_valor_imposto_ST_PIS.text<>Get_formula_valor_imposto_ST
                  Then CriaNovoPis:=true;
               End;

               With ObjMaterial_ICMS.imposto_cofins do
               Begin
                  CriaNovocofins:=False;

                  if EdtST_cofins.text<>ST.Get_codigo
                  Then CriaNovocofins:=true;
                  if ComboTIPOCALCULO_cofins.text<>Get_TIPOCALCULO
                  Then CriaNovocofins:=true;
                  if EdtALIQUOTAPERCENTUAL_cofins.text<>Get_ALIQUOTAPERCENTUAL
                  Then CriaNovocofins:=true;
                  if EdtALIQUOTAVALOR_cofins.text<>Get_ALIQUOTAVALOR
                  Then CriaNovocofins:=true;
                  if ComboTIPOCALCULO_ST_cofins.text<>Get_TIPOCALCULO_ST
                  Then CriaNovocofins:=true;
                  if EdtALIQUOTAPERCENTUAL_ST_cofins.text<>Get_ALIQUOTAPERCENTUAL_ST
                  Then CriaNovocofins:=true;
                  if EdtALIQUOTAVALOR_ST_cofins.text<>Get_ALIQUOTAVALOR_ST
                  Then CriaNovocofins:=true;

                  if edtFORMULABASECALCULO_cofins.text<>Get_FORMULABASECALCULO
                  Then CriaNovocofins:=true;
                  if edtFORMULABASECALCULO_ST_cofins.text<>Get_FORMULABASECALCULO_ST
                  Then CriaNovocofins:=true;


                  if edtformula_valor_imposto_cofins.text<>Get_formula_valor_imposto
                  Then CriaNovocofins:=true;
                  if edtformula_valor_imposto_ST_cofins.text<>Get_formula_valor_imposto_ST
                  Then CriaNovocofins:=true;
               End;

               objmaterial_ICms.Submit_CODIGO(lbcodigo_material_icms.caption);
               objmaterial_ICms.Status:=dsedit;
     End;

     if (CriaNovo)
     Then Begin
               //Se for insercao, ou alteraram alguma informacao na edicao, tenho que criar um novo Imposto_ICMS para ligar a ele
               With Self.ObjMaterial_ICMS.IMPOSTO do
               BEgin
                    ZerarTabela;
                    Submit_codigo('0');
                    STA.Submit_codigo(edtSTA_ICMS.text);
                    STB.Submit_codigo(edtSTB_ICMS.text);
                    CFOP.Submit_codigo(edtCFOP_ICMS.text);
                    CSOSN.Submit_codigo(edtCSOSN.Text);

                    Submit_MODALIDADE(inttostr(ComboMODALIDADE_ICMS.itemindex));

                    Submit_PERC_REDUCAO_BC(edtPERC_REDUCAO_BC_ICMS.text);
                    Submit_ALIQUOTA(edtALIQUOTA_ICMS.text);
                    Submit_IVA(edtIVA_ICMS.text);
                    Submit_PAUTA(edtPAUTA_ICMS.text);

                    Submit_MODALIDADE_ST(inttostr(ComboMODALIDADE_ST_ICMS.itemindex));

                    Submit_PERC_REDUCAO_BC_ST(edtPERC_REDUCAO_BC_ST_ICMS.text);
                    Submit_ALIQUOTA_ST(edtALIQUOTA_ST_ICMS.text);
                    Submit_IVA_ST(edtIVA_ST_ICMS.text);
                    Submit_PAUTA_ST(edtPAUTA_ST_ICMS.text);
                    Submit_FORMULA_BC(edtformula_bc_ICMS.text);
                    Submit_FORMULA_BC_ST(edtformula_bc_ST_ICMS.text);
                    Submit_formula_valor_imposto(edtformula_valor_imposto_ICMS.text);
                    Submit_formula_valor_imposto_ST(edtformula_valor_imposto_ST_ICMS.text);
                    Submit_CFOP_FORAESTADO(edtCFOPForaEstado.Text);
                    //25/03 - celio - ser� enviado direto no ObjMaterial_ICMS
                    //Submit_PERCENTUALTRIBUTO(edtpercentualtributo.Text);
                    status:=dsinsert;
                    if (Salvar(False)=False)
                    Then Begin
                              MensagemErro('N�o foi poss�vel salvar um Novo Imposto ICMS');
                              exit;

                    End;
                    PcodigoImposto:=Get_Codigo;
               End;
     End;

     if (CriaNovoIPI)
     Then Begin
                With Self.ObjMaterial_ICMS.imposto_ipi do
                Begin
                     ZerarTabela;
                     Status:= dsInsert;
                     Submit_CODIGO('0');
                     ST.Submit_codigo(edtST_ipi.text);
                     Submit_ClasseEnq(edtClasseEnq_ipi.text);
                     Submit_CodigoEnquadramento(edtCodigoEnquadramento_ipi.text);
                     Submit_CnpjProdutor(edtCnpjProdutor_ipi.text);
                     Submit_CodigoSelo(edtCodigoSelo_ipi.text);
                     Submit_TipoCalculo(ComboTipoCalculo_ipi.text);
                     Submit_Aliquota(edtAliquota_ipi.text);
                     Submit_QuantidadeTotalUP(edtQuantidadeTotalUP_ipi.text);
                     Submit_ValorUnidade(edtValorUnidade_ipi.text);
                     Submit_FORMULABASECALCULO(edtFORMULABASECALCULO_ipi.text);
                     Submit_formula_valor_imposto(edtformula_valor_imposto_ipi.text);

                     if (Salvar(False)=False)
                     Then Begin
                               MensagemErro('N�o foi poss�vel salvar um novo imposto IPI');
                               exit;

                     End;
                     PcodigoImposto_IPI:=Get_Codigo;
                End;
     End;


     if (CriaNovoPIS)
     Then Begin
                With Self.ObjMaterial_ICMS.imposto_PIS do
                Begin
                     ZerarTabela;
                     Status:= dsInsert;
                     Submit_CODIGO('0');
                     ST.Submit_codigo(edtST_PIS.text);
                     Submit_TIPOCALCULO(comboTIPOCALCULO_PIS.text);
                     Submit_ALIQUOTAPERCENTUAL(edtALIQUOTAPERCENTUAL_PIS.text);
                     Submit_ALIQUOTAVALOR(edtALIQUOTAVALOR_PIS.text);
                     Submit_TIPOCALCULO_ST(comboTIPOCALCULO_ST_PIS.text);
                     Submit_ALIQUOTAPERCENTUAL_ST(edtALIQUOTAPERCENTUAL_ST_PIS.text);
                     Submit_ALIQUOTAVALOR_ST(edtALIQUOTAVALOR_ST_PIS.text);
                     Submit_FORMULABASECALCULO(EdtFORMULABASECALCULO_PIS.text);
                     Submit_FORMULABASECALCULO_ST(EdtFORMULABASECALCULO_ST_PIS.text);

                     Submit_formula_valor_imposto(Edtformula_valor_imposto_PIS.text);
                     Submit_formula_valor_imposto_ST(Edtformula_valor_imposto_ST_PIS.text);
                     if (Salvar(False)=False)
                     Then Begin
                               MensagemErro('N�o foi poss�vel salvar um novo imposto PIS');
                               exit;

                     End;
                     PcodigoImposto_PIS:=Get_Codigo;
                End;
     End;

     if (CriaNovocofins)
     Then Begin
                With Self.ObjMaterial_ICMS.imposto_cofins do
                Begin
                     ZerarTabela;
                     Status:= dsInsert;
                     Submit_CODIGO('0');
                     ST.Submit_codigo(edtST_cofins.text);
                     Submit_TIPOCALCULO(comboTIPOCALCULO_cofins.text);
                     Submit_ALIQUOTAPERCENTUAL(edtALIQUOTAPERCENTUAL_cofins.text);
                     Submit_ALIQUOTAVALOR(edtALIQUOTAVALOR_cofins.text);
                     Submit_TIPOCALCULO_ST(comboTIPOCALCULO_ST_cofins.text);
                     Submit_ALIQUOTAPERCENTUAL_ST(edtALIQUOTAPERCENTUAL_ST_cofins.text);
                     Submit_ALIQUOTAVALOR_ST(edtALIQUOTAVALOR_ST_cofins.text);
                     Submit_FORMULABASECALCULO(EdtFORMULABASECALCULO_cofins.text);
                     Submit_FORMULABASECALCULO_ST(EdtFORMULABASECALCULO_ST_cofins.text);

                     Submit_formula_valor_imposto(Edtformula_valor_imposto_cofins.text);
                     Submit_formula_valor_imposto_ST(Edtformula_valor_imposto_ST_cofins.text);
                     if (Salvar(False)=False)
                     Then Begin
                               MensagemErro('N�o foi poss�vel salvar um novo imposto cofins');
                               exit;

                     End;
                     PcodigoImposto_cofins:=Get_Codigo;
                End;
     End;



     objmaterial_ICms.imposto_ipi.Submit_CODIGO(PcodigoImposto_IPI);
     objmaterial_ICms.imposto_pis.Submit_CODIGO(PcodigoImposto_pis);
     objmaterial_ICms.imposto_cofins.Submit_CODIGO(PcodigoImposto_cofins);
     objmaterial_ICms.IMPOSTO.Submit_CODIGO(PcodigoImposto);
     objmaterial_ICms.Submit_ESTADO(edtestado_material_icms.Text);
     objmaterial_ICms.tipocliente.Submit_CODIGO(edttipocliente_material_icms.Text);
     objmaterial_ICms.operacao.submit_codigo(edtoperacao.text);
     objmaterial_ICms.Submit_CodigoMaterial(PCodigoMaterial);
     //25/03 celio - antes mandava o percentual no Self.ObjMaterial_ICMS.IMPOSTO
     ObjMaterial_ICMS.Submit_PERCENTUALTRIBUTO(edtpercentualtributo.Text);

     if (objmaterial_ICms.Salvar(true)=False)
     then exit;

     Self.RetornaImpostos;
     btcancelar_material_ICMSClick(sender);

     edtestado_material_icms.SetFocus;

Finally
       FDataModulo.IBTransaction.CommitRetaining;
End;


end;

procedure TFRImposto_ICMS.RetornaImpostos;
begin
    ObjMaterial_ICMS.RetornaImposto(PcodigoMaterial,STRG_Imposto);
end;

procedure TFRImposto_ICMS.edttipocliente_material_icmsKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjMaterial_ICms.EdttipoclienteKeyDown(sender,key,shift,lbnometipocliente);
end;

procedure TFRImposto_ICMS.edttipocliente_material_icmsExit(Sender: TObject);
begin
    ObjMaterial_ICMS.EdttipoclienteExit(sender,lbnometipocliente);
end;

procedure TFRImposto_ICMS.EDTImpostoModelo_ICMSKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjMaterial_ICms.EdtIMPOSTOKeyDown(sender,key,shift,nil);
end;

procedure TFRImposto_ICMS.EDTImpostoModelo_ICMSExit(Sender: TObject);
begin
     ObjMaterial_ICms.EdtIMPOSTOExit(sender,nil);

     if (EDTImpostoModelo_ICMS.Text<>'')
     then Begin
               EDTImpostoModelo_ICMS.Text:='';
               
               With Self.ObjMaterial_ICMS.IMPOSTO do
               Begin
                    EdtSTA_ICMS.text:=STA.Get_codigo;
                    EdtSTB_ICMS.text:=STB.Get_codigo;
                    EdtCFOP_ICMS.text:=CFOP.Get_codigo;
                    LbNomeSTA_ICMS.caption:=STA.Get_DESCRICAO;
                    LbNomeSTB_ICMS.caption:=STB.Get_DESCRICAO;
                    lbnomeCFOP_ICMS.caption:=CFOP.Get_NOME;
                    edtCSOSN.Text:=CSOSN.Get_codigo;

                    ComboMODALIDADE_ICMS.ItemIndex:=-1;

                    if (Get_MOdalidade<>'')
                    Then ComboMODALIDADE_ICMS.ItemIndex:=Strtoint(Get_MODALIDADE);

                    ComboMODALIDADE_ST_ICMS.ItemIndex:=-1;

                    if (Get_MODALIDADE_ST<>'')
                    Then ComboMODALIDADE_ST_ICMS.ItemIndex:=Strtoint(Get_MODALIDADE_ST);


                    EdtPERC_REDUCAO_BC_ICMS.text:=Get_PERC_REDUCAO_BC;
                    EdtALIQUOTA_ICMS.text:=Get_ALIQUOTA;
                    EdtIVA_ICMS.text:=Get_IVA;
                    EdtPAUTA_ICMS.text:=Get_PAUTA;

                    EdtPERC_REDUCAO_BC_ST_ICMS.text:=Get_PERC_REDUCAO_BC_ST;
                    EdtALIQUOTA_ST_ICMS.text:=Get_ALIQUOTA_ST;
                    EdtIVA_ST_ICMS.text:=Get_IVA_ST;
                    EdtPAUTA_ST_ICMS.text:=Get_PAUTA_ST;

                    edtformula_bc_ICMS.text:=Get_FORMULA_BC;
                    edtformula_bc_ST_ICMS.text:=Get_FORMULA_BC_ST;
                    edtformula_valor_imposto_ICMS.text:=Get_formula_valor_imposto;
                    edtformula_valor_imposto_ST_ICMS.text:=Get_formula_valor_imposto_ST;
                    edtCFOPForaEstado.text:=Get_CFOP_FORAESTADO;
                    lbCfopForaEstado.Caption:=RetornaNomeCFOPFORA;
                    //celio 25/03
                    //edtpercentualtributo.Text:=Get_PERCENTUALTRIBUTO;
                    
               End;
     End;
end;

procedure TFRImposto_ICMS.STRG_ImpostoDblClick(Sender: TObject);

var
pcoluna:integer;
begin
     Pcoluna:=STRG_Imposto.Rows[0].indexof('CODIGO');

     if (Pcoluna<0)
     then BEgin
               MensagemErro('Coluna C�DIGO n�o encontrada');
               exit;
     End;
     
     Objmaterial_ICms.ZerarTabela;
     
     if(Objmaterial_ICms.LocalizaCodigo(STRG_Imposto.Cells[PColuna,STRG_Imposto.row])=False)
     Then exit;


     With Objmaterial_ICms do
     Begin
          TabelaparaObjeto;
          lbcodigo_material_icms.caption:=Get_CODIGO;
          edtestado_material_icms.Text:=Get_ESTADO;
          edttipocliente_material_icms.Text:=tipocliente.Get_CODIGO;
          edtoperacao.text:=operacao.get_codigo;

          edtoperacao.enabled:=False;


          EDTImpostoModelo_ICMS.Text:=IMPOSTO.Get_CODIGO;
          EDTImpostoModelo_ICMSExit(sender);//para preencher os campos dos impostos embaixo
          EDTImpostoModelo_ICMS.Text:='';


          edtimposto_ipi_modelo.Text:=imposto_ipi.Get_CODIGO;
          edtimposto_ipi_modeloExit(sender);//para preencher os campos dos impostos DE IPi embaixo
          edtimposto_ipi_modelo.Text:='';

          edtimposto_pis_modelo.Text:=imposto_pis.Get_CODIGO;
          edtimposto_pis_modeloExit(sender);//para preencher os campos dos impostos DE pis embaixo
          edtimposto_pis_modelo.Text:='';

          edtimposto_cofins_modelo.Text:=imposto_cofins.Get_CODIGO;
          edtimposto_cofins_modeloExit(sender);//para preencher os campos dos impostos DE cofins embaixo
          edtimposto_cofins_modelo.Text:='';

          edtpercentualtributo.Text := Get_PERCENTUALTRIBUTO;

          edtestado_material_icms.SetFocus;
     End;
end;

procedure TFRImposto_ICMS.btexcluir_material_ICMSClick(Sender: TObject);
var
pcoluna:integer;
begin
     if (Self.Permissao=False)
     Then Begin
               MensagemErro('Seu usu�rio n�o tem permiss�o para alterar os impostos do produto');
               exit;
     End;
     
     Pcoluna:=STRG_Imposto.Rows[0].indexof('CODIGO');

     if (Pcoluna<0)
     then BEgin
               MensagemErro('Coluna C�DIGO n�o encontrada');
               exit;
     End;

     if (MensagemPergunta('Certeza deseja excluir?')=mrno)
     Then exit;

     ObjMaterial_ICms.Exclui(STRG_Imposto.Cells[pcoluna,STRG_Imposto.Row],true);
     Self.RetornaImpostos;
End;


procedure TFRImposto_ICMS.Acessaframe(Pcodigo: String);
var
   menuItem : TMenuItem;
begin
     //Esta Funcao � chamada quando entra na Aba de Imposto do Cadastro do Material
     Self.PcodigoMaterial:=pcodigo;
     Self.RetornaImpostos;
     habilita_campos(Panel_TOP);
     btcancelar_material_ICMSClick(nil);
     Self.permissao:=False;
     Self.EditLocal:=Nil;

     Self.permissao:=ObjMaterial_ICMS.ValidaPermissaoImpostos;

     BtGravar_material_icms.Enabled:=Self.Permissao;
     btexcluir_material_ICMS.Enabled:=Self.Permissao;

     //No IPI S� posso usar as gerais (Valorfinal, frete e seguro)
     //E as pr�prias vari�veis do IPI
     Self.PopupMenuVariaveisIPI.Items.Clear;
     Self.CriaMenu(Self.PopupMenuVariaveisIPI,GI_Geral);
     Self.CriaMenu(Self.PopupMenuVariaveisIPI,GI_IPI);

     //PIS
     Self.PopupMenuVariaveisPIS.Items.clear;
     Self.CriaMenu(Self.PopupMenuVariaveisPIS,GI_Geral);
     Self.CriaMenu(Self.PopupMenuVariaveisPIS,GI_IPI);
     Self.CriaMenu(Self.PopupMenuVariaveisPIS,GI_Pis);

     //PIS_ST
     Self.PopupMenuVariaveisPIS_ST.Items.clear;
     Self.CriaMenu(Self.PopupMenuVariaveisPIS_ST,GI_Geral);
     Self.CriaMenu(Self.PopupMenuVariaveisPIS_ST,GI_IPI);
     Self.CriaMenu(Self.PopupMenuVariaveisPIS_ST,GI_Pis);
     Self.CriaMenu(Self.PopupMenuVariaveisPIS_ST,GI_PisST);

     //COFINS
     Self.PopupMenuVariaveisCOFINS.Items.clear;
     Self.CriaMenu(Self.PopupMenuVariaveisCOFINS,GI_Geral);
     Self.CriaMenu(Self.PopupMenuVariaveisCOFINS,GI_IPI);
     Self.CriaMenu(Self.PopupMenuVariaveisCOFINS,GI_PIS);
     Self.CriaMenu(Self.PopupMenuVariaveisCOFINS,GI_PisST);
     Self.CriaMenu(Self.PopupMenuVariaveisCOFINS,GI_COFINS);

     //COFINS_ST
     Self.PopupMenuVariaveisCOFINS_ST.Items.clear;
     Self.CriaMenu(Self.PopupMenuVariaveisCOFINS_ST,GI_Geral);
     Self.CriaMenu(Self.PopupMenuVariaveisCOFINS_ST,GI_IPI);
     Self.CriaMenu(Self.PopupMenuVariaveisCOFINS_ST,GI_PIS);
     Self.CriaMenu(Self.PopupMenuVariaveisCOFINS_ST,GI_PisST);
     Self.CriaMenu(Self.PopupMenuVariaveisCOFINS_ST,GI_COFINS);
     Self.CriaMenu(Self.PopupMenuVariaveisCOFINS_ST,GI_COFINSST);


     //ICMS
     Self.PopupMenuVariaveisICMS.Items.clear;
     Self.CriaMenu(Self.PopupMenuVariaveisICMS,GI_Geral);
     Self.CriaMenu(Self.PopupMenuVariaveisICMS,GI_IPI);
     Self.CriaMenu(Self.PopupMenuVariaveisICMS,GI_PIS);
     Self.CriaMenu(Self.PopupMenuVariaveisICMS,GI_PisST);
     Self.CriaMenu(Self.PopupMenuVariaveisICMS,GI_COFINS);
     Self.CriaMenu(Self.PopupMenuVariaveisICMS,GI_COFINSST);
     Self.CriaMenu(Self.PopupMenuVariaveisICMS,GI_Icms);

     //ICMS ST
     Self.PopupMenuVariaveisICMS_ST.Items.clear;
     Self.CriaMenu(Self.PopupMenuVariaveisICMS_ST,GI_Geral);
     Self.CriaMenu(Self.PopupMenuVariaveisICMS_ST,GI_IPI);
     Self.CriaMenu(Self.PopupMenuVariaveisICMS_ST,GI_PIS);
     Self.CriaMenu(Self.PopupMenuVariaveisICMS_ST,GI_PisST);
     Self.CriaMenu(Self.PopupMenuVariaveisICMS_ST,GI_COFINS);
     Self.CriaMenu(Self.PopupMenuVariaveisICMS_ST,GI_COFINSST);
     Self.CriaMenu(Self.PopupMenuVariaveisICMS_ST,GI_Icms);
     Self.CriaMenu(Self.PopupMenuVariaveisICMS_ST,GI_IcmsST);

     edtestado_material_icms.SetFocus;
end;

procedure TFRImposto_ICMS.btcancelar_material_ICMSClick(Sender: TObject);
begin
     LIMPAEDIT(Self);
     lbcodigo_material_icms.Caption:='';
     lbnometipocliente.caption:='';
     lbnomeCFOP_ICMS.Caption:='';
     LbNomeSTA_ICMS.Caption:='';
     LbNomeSTB_ICMS.Caption:='';

     LbNomeST_IPI.caption:='';
     LbNomeST_PIS.caption:='';
     LbNomeST_COFINS.caption:='';
     edtoperacao.enabled:=True;//liberando para  novos
     lbCfopForaEstado.Caption:='';
     


end;

procedure TFRImposto_ICMS.edtSTA_ICMSExit(Sender: TObject);
begin
     Self.ObjMaterial_ICMS.IMPOSTO.EdtSTAExit(sender,LbNomeSTA_ICMS);
end;

procedure TFRImposto_ICMS.edtSTA_ICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    Self.ObjMaterial_ICMS.IMPOSTO.EdtSTAKeyDown(sender,key,shift,LbNomeSTA_ICMS);
end;

procedure TFRImposto_ICMS.EdtSTB_ICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    Self.ObjMaterial_ICMS.IMPOSTO.EdtSTBKeyDown(sender,key,shift,LbNomeSTB_ICMS);
end;

procedure TFRImposto_ICMS.EdtSTB_ICMSExit(Sender: TObject);
begin
     Self.ObjMaterial_ICMS.IMPOSTO.EdtSTBExit(sender,LbNomeSTB_ICMS);

     Self.TrataStiuacaoTributariaB;

end;

procedure TFRImposto_ICMS.EdtCFOP_ICMSExit(Sender: TObject);
begin
     Self.ObjMaterial_ICMS.IMPOSTO.EdtCFOPExit(sender,lbnomeCFOP_ICMS);
end;

procedure TFRImposto_ICMS.EdtCFOP_ICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    Self.ObjMaterial_ICMS.IMPOSTO.EdtCFOPKeyDown(sender,key,shift,lbnomeCFOP_ICMS);

end;
procedure TFRImposto_ICMS.TrataStiuacaoTributariaB;
begin
     Try
        Strtoint(EdtSTB_ICMS.Text);
     except
           MensagemErro('Situa��o tribut�ria Inv�lida');
           exit;
     End;
     
end;

procedure TFRImposto_ICMS.edtimposto_ipi_modeloExit(Sender: TObject);
begin
     ObjMaterial_ICms.Edtimposto_ipiExit(sender,nil);

     if (edtimposto_ipi_modelo.Text<>'')
     then Begin
               edtimposto_ipi_modelo.Text:='';
               
               With Self.ObjMaterial_ICMS.imposto_ipi do
               Begin
                   EdtST_ipi.text:=ST.Get_codigo;
                   EdtClasseEnq_ipi.text:=Get_ClasseEnq;
                   EdtCodigoEnquadramento_ipi.text:=Get_CodigoEnquadramento;
                   EdtCnpjProdutor_ipi.text:=Get_CnpjProdutor;
                   EdtCodigoSelo_ipi.text:=Get_CodigoSelo;
                   comboTipoCalculo_ipi.text:=Get_TipoCalculo;
                   EdtAliquota_ipi.text:=Get_Aliquota;
                   EdtQuantidadeTotalUP_ipi.text:=Get_QuantidadeTotalUP;
                   EdtValorUnidade_ipi.text:=Get_ValorUnidade;
                   EdtFORMULABASECALCULO_ipi.text:=Get_FORMULABASECALCULO;
                   Edtformula_valor_imposto_ipi.text:=Get_formula_valor_imposto;
               End;
     End;
end;

procedure TFRImposto_ICMS.edtimposto_ipi_modeloKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjMaterial_ICMS.Edtimposto_ipiKeyDown(sender,key,shift,nil);
end;

procedure TFRImposto_ICMS.EdtST_IPIKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    Self.ObjMaterial_ICMS.imposto_ipi.EdtSTKeyDown(sender,key,shift,LbNomeST_IPI);
end;

procedure TFRImposto_ICMS.EdtST_IPIExit(Sender: TObject);
begin
     Self.ObjMaterial_ICMS.imposto_ipi.EdtSTExit(sender,LbNomeST_IPI);
end;

procedure TFRImposto_ICMS.edtimposto_pis_modeloExit(Sender: TObject);
begin
     ObjMaterial_ICms.Edtimposto_pisExit(sender,nil);

     if (edtimposto_pis_modelo.Text<>'')
     then Begin
               edtimposto_pis_modelo.Text:='';
               
               With Self.ObjMaterial_ICMS.imposto_pis do
               Begin
                   EdtST_PIS.text:=ST.Get_codigo;
                   ComboTIPOCALCULO_PIS.text:=Get_TIPOCALCULO;
                   EdtALIQUOTAPERCENTUAL_PIS.text:=Get_ALIQUOTAPERCENTUAL;
                   EdtALIQUOTAVALOR_PIS.text:=Get_ALIQUOTAVALOR;
                   ComboTIPOCALCULO_ST_PIS.text:=Get_TIPOCALCULO_ST;
                   EdtALIQUOTAPERCENTUAL_ST_PIS.text:=Get_ALIQUOTAPERCENTUAL_ST;
                   EdtALIQUOTAVALOR_ST_PIS.text:=Get_ALIQUOTAVALOR_ST;
                   edtFORMULABASECALCULO_PIS.text:=Get_FORMULABASECALCULO;
                   edtFORMULABASECALCULO_ST_PIS.text:=Get_FORMULABASECALCULO_ST;

                   edtformula_valor_imposto_PIS.text:=Get_formula_valor_imposto;
                   edtformula_valor_imposto_ST_PIS.text:=Get_formula_valor_imposto_ST;
               End;
     End;

end;

procedure TFRImposto_ICMS.edtimposto_pis_modeloKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjMaterial_ICms.Edtimposto_pisKeyDown(sender,key,shift,nil);
end;

procedure TFRImposto_ICMS.EdtST_PISExit(Sender: TObject);
begin                                                                                                          
     ObjMaterial_ICms.imposto_pis.EdtSTExit(sender,LbNomeST_PIS);
end;

procedure TFRImposto_ICMS.EdtST_PISKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    ObjMaterial_ICms.imposto_pis.EdtSTKeyDown(sender,key,shift,LbNomeST_PIS);
end;

procedure TFRImposto_ICMS.edtimposto_cofins_modeloExit(Sender: TObject);
begin
     ObjMaterial_ICms.Edtimposto_cofinsExit(sender,nil);

     if (edtimposto_cofins_modelo.Text<>'')
     then Begin
               edtimposto_cofins_modelo.Text:='';
               
               With Self.ObjMaterial_ICMS.imposto_cofins do
               Begin
                   EdtST_cofins.text:=ST.Get_codigo;
                   ComboTIPOCALCULO_cofins.text:=Get_TIPOCALCULO;
                   EdtALIQUOTAPERCENTUAL_cofins.text:=Get_ALIQUOTAPERCENTUAL;
                   EdtALIQUOTAVALOR_cofins.text:=Get_ALIQUOTAVALOR;
                   ComboTIPOCALCULO_ST_cofins.text:=Get_TIPOCALCULO_ST;
                   EdtALIQUOTAPERCENTUAL_ST_cofins.text:=Get_ALIQUOTAPERCENTUAL_ST;
                   EdtALIQUOTAVALOR_ST_cofins.text:=Get_ALIQUOTAVALOR_ST;
                   edtFORMULABASECALCULO_cofins.text:=Get_FORMULABASECALCULO;
                   edtFORMULABASECALCULO_ST_cofins.text:=Get_FORMULABASECALCULO_ST;

                   edtformula_valor_imposto_cofins.text:=Get_formula_valor_imposto;
                   edtformula_valor_imposto_ST_cofins.text:=Get_formula_valor_imposto_ST;
               End;
     End;


end;

procedure TFRImposto_ICMS.edtimposto_cofins_modeloKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjMaterial_ICms.Edtimposto_cofinsKeyDown(sender,key,shift,nil);
end;

procedure TFRImposto_ICMS.EdtST_COFINSExit(Sender: TObject);
begin
     Self.ObjMaterial_ICMS.imposto_pis.EdtSTExit(sender,LbNomeST_PIS);
end;

procedure TFRImposto_ICMS.EdtST_COFINSKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjMaterial_ICMS.imposto_pis.EdtSTKeyDown(sender,key,shift,LbNomeST_PIS);
end;

procedure TFRImposto_ICMS.edtformula_bc_ICMSEnter(Sender: TObject);
begin
     Self.EditLocal:=Tedit(Sender);
end;


procedure TFRImposto_ICMS.edtformula_bc_ICMSExit(Sender: TObject);
begin
     Self.EditLocal:=nil;
end;

procedure TFRImposto_ICMS.PModeloClick(Sender: TObject);
var
Pvar,Ptexto:String;
posicao:integer;
begin
//********
     Ptexto:=Tmenuitem(Sender).caption;

     if (ptexto='') or (ptexto='-')
     then exit;

     if (ptexto[1]='*')//T�tulos (*****grupos*****)
     then exit;

     //O & � o atalho  do Menu
     ptexto:=' '+StringReplace(ptexto,'&','',[rfReplaceAll,rfIgnoreCase])+' ';



     if (Self.EditLocal<>nil)
     then Begin
               Pvar:=Self.EditLocal.Hint;
               posicao:=Pos(':',Pvar);
               //Se a variavel escolhida for o pr�prio EDIT nao posso deixar
               //Para isso vou usar a propriedade HINT, pois ela possui F�rmula: NOMEVARIAVEL
               if (posicao>0)
               Then Begin
                         pvar:=trim(copy(pvar,posicao+1,length(pvar)-posicao));
                         if trim(pvar)=trim(ptexto)
                         Then Begin
                                   Mensagemerro('N�o � poss�vel a pr�pria vari�vel na sua f�rmula');
                                   exit;
                         End;

               End
               Else MensagemErro('Erro no Hint');//n�o tem o : no HINST


              RICH_COPIA.Lines.Clear;
              RICH_COPIA.Lines.add(ptexto);
              RICH_COPIA.SelectAll;
              RICH_COPIA.CutToClipboard;

              Self.EditLocal.PasteFromClipboard;
     End;

end;

function TFRImposto_ICMS.CriaMenu(Pmenu:TpopupMenu;PGrupo:TGrupoImpostos):boolean;
var
Menuitem:tmenuitem;
cont:integer;
begin
     Self.RetornaVariaveisGrupo(Pgrupo);

     for cont:=0 to Self.ObjMaterial_ICMS.Strl_Variaveis.Count-1 do
     Begin
          menuItem := TMenuItem.Create(Pmenu) ;
          menuItem.Caption := Self.ObjMaterial_ICMS.Strl_Variaveis[cont];
          menuItem.OnClick := Self.PModeloClick;
          Pmenu.Items.add(menuItem);
     End;
end;

procedure TFRImposto_ICMS.RetornaVariaveisGrupo(Pgrupo: TGrupoImpostos);
begin
     With Self.ObjMaterial_ICMS.Strl_Variaveis do
     Begin
            Clear;

           if (Pgrupo=GI_Geral)
           Then Begin
                    ADD('-');
                    ADD('******GERAL*******');
                    ADD('VALORFINAL_PROD');
                    ADD('VALORFRETE_PROD');
                    ADD('VALORSEGURO_PROD');
           End;
           if (PGrupo=GI_Icms)
           Then Begin
                    ADD('-');
                    ADD('******ICMS********');
                    ADD('-');
                    ADD('PERC_REDUCAO_BC_ICMS');
                    ADD('ALIQUOTA_ICMS');
                    ADD('IVA_ICMS');
                    ADD('PAUTA_ICMS');
                    ADD('BC_ICMS');
                    ADD('VALOR_ICMS');
           End;

           if (Pgrupo=GI_IcmsST)
           Then Begin
                    ADD('-');
                    ADD('******ICMS ST*****');
                    ADD('-');
                    ADD('PERC_REDUCAO_BC_ST_ICMS');
                    ADD('ALIQUOTA_ST_ICMS');
                    ADD('IVA_ST_ICMS');
                    ADD('PAUTA_ST_ICMS');
                    ADD('BC_ST_ICMS');
                    ADD('VALOR_ST_ICMS');
           End;

           if (Pgrupo=GI_IPI)
           Then Begin
                    ADD('-');
                    ADD('******IPI********');
                    ADD('-');
                    ADD('ALIQUOTA_IPI');
                    ADD('QTDE_UP_IPI');
                    ADD('VL_UP_IPI');
                    ADD('BC_IPI');
                    ADD('VALORIPI_PROD');
           End;

           If (pGRUPO=GI_Pis)
           Then Begin
                    ADD('-');
                    ADD('******PIS********');
                    ADD('-');
                    ADD('ALIQUOTAPERCENTUAL_PIS');
                    ADD('ALIQUOTAVALOR_PIS');
                    ADD('BC_PIS');
                    ADD('VALOR_PIS_PROD');
           End;

           If (pGRUPO=GI_PisST)
           Then Begin

                    ADD('-');
                    ADD('*******PIS ST*****');
                    ADD('-');
                    
                    ADD('ALIQUOTAPERCENTUAL_ST_PIS');
                    ADD('ALIQUOTAVALOR_ST_PIS');
                    ADD('BC_ST_PIS');
                    ADD('VALOR_PIS_ST_PROD');
           End;

           If (pGRUPO=GI_COFINS)
           Then Begin
                    ADD('-');
                    ADD('******COFINS******');
                    ADD('-');
                    ADD('ALIQUOTAPERCENTUAL_COFINS');
                    ADD('ALIQUOTAVALOR_COFINS');
                    ADD('BC_COFINS');
                    ADD('VALOR_COFINS_PROD');
           End;

           If (pGRUPO=GI_COFINSST)
           Then Begin
                    ADD('-');
                    ADD('*****COFINS ST*****');
                    ADD('-');
                    ADD('ALIQUOTAPERCENTUAL_ST_COF');
                    ADD('INS');
                    ADD('ALIQUOTAVALOR_ST_COFINS');
                    ADD('BC_ST_COFINS');
                    ADD('VALOR_COFINS_ST_PROD');
           End;
     End;
end;

procedure TFRImposto_ICMS.edtoperacaoExit(Sender: TObject);
begin
    ObjMaterial_ICMS.EdtoperacaoExit(sender,nil);
end;

procedure TFRImposto_ICMS.edtoperacaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjMaterial_ICMS.EdtoperacaoKeyDown(sender,key,shift,nil);
end;

procedure TFRImposto_ICMS.Button1Click(Sender: TObject);
var
cont:integer;
begin
     if (ObjPermissoesUsoGlobal.ValidaPermissao('REPLICAR IMPOSTOS')=False)
     then exit;


     if (lbcodigo_material_icms.caption='')
     then Begin
               MensagemAviso('Primeira escolha na Grade o imposto que deseja replicar');
               exit;
     End;


     if (ObjMaterial_ICMS.LocalizaCodigo(lbcodigo_material_icms.caption)=False)
     Then Begin
               mensagemErro('Erro na tentativa de localizar o C�digo '+lbcodigo_material_icms.caption);
               exit;
     End;
     ObjMaterial_ICMS.TabelaparaObjeto;

     With Self.ObjMaterial_ICMS.Objquery do
     Begin
           With FEscolheMateriaisReplicar do
           Begin
                GRID.ColCount:=6;
                GRID.rowcount:=1;
                GRID.FixedCols:=1;

                GRID.Cells[0,0]:='X';
                GRID.Cells[1,0]:='CODIGO';
                GRID.Cells[2,0]:='DESCRICAO';
                GRID.Cells[3,0]:='REFERENCIA';
                GRID.Cells[4,0]:='GRUPO';
                GRID.Cells[5,0]:='NOMEGRUPO';

                GRID.Rows[1].Clear;


                Self.ObjMaterial_ICMS.RetornaMateriais(ObjMaterial_ICMS.Get_CodigoMaterial);

                While not(Self.ObjMaterial_ICMS.Objquery.eof) do
                Begin
                    GRID.RowCount:=GRID.RowCount+1;
                    GRID.Cells[0,GRID.RowCount-1]:='';
                    GRID.Cells[1,GRID.RowCount-1]:=Self.ObjMaterial_ICMS.Objquery.FIELDBYNAME('CODIGO').ASSTRING;
                    GRID.Cells[2,GRID.RowCount-1]:=Self.ObjMaterial_ICMS.Objquery.FIELDBYNAME('DESCRICAO').ASSTRING;
                    GRID.Cells[3,GRID.RowCount-1]:=Self.ObjMaterial_ICMS.Objquery.FIELDBYNAME('REFERENCIA').ASSTRING;
                    GRID.Cells[4,GRID.RowCount-1]:=Self.ObjMaterial_ICMS.Objquery.FIELDBYNAME('GRUPO').ASSTRING;
                    GRID.Cells[5,GRID.RowCount-1]:=Self.ObjMaterial_ICMS.Objquery.FIELDBYNAME('NOMEGRUPO').ASSTRING;


                    Self.ObjMaterial_ICMS.Objquery.next;
                End;

                if (GRID.RowCount>1)
                Then GRID.FixedRows:=1;


                AjustaLArguraColunaGrid(GRID);

                showmodal;

                if (GRID.RowCount>1)
                Then Begin
                          FMostraBarraProgresso.ConfiguracoesIniciais(Grid.RowCount-1,0);
                          FMostraBarraProgresso.Lbmensagem.caption:='Cadastrando';
                          FMostraBarraProgresso.Lbmensagem2.caption:='Cadastrando';
                End;


                for cont:=1 to Grid.RowCount-1 do
                begin
                     FMostraBarraProgresso.IncrementaBarra1(1);
                     FMostraBarraProgresso.show;

                     if (Grid.cells[0,cont]='X')
                     Then Begin
                              Self.ObjMaterial_ICMS.Status:=dsInsert;

                              //os demais dados ja foram preenchidos no localiza... tabela...
                              Self.ObjMaterial_ICMS.Submit_codigo('0');
                              //troco apenas o c�digo do material
                              Self.ObjMaterial_ICMS.Submit_CodigoMaterial(Grid.cells[1,cont]);
                              //verifico se ja existe um imposto para o estado,tipocliente,operacao no Material Atual
                              if (self.ObjMaterial_ICMS.Localiza(Self.ObjMaterial_ICMS.ESTADO,Self.ObjMaterial_ICMS.tipocliente.Get_CODIGO,Grid.cells[1,cont],Self.ObjMaterial_ICMS.operacao.get_codigo)=True)
                              Then Begin
                                        //se tiver apenas edito
                                        self.ObjMaterial_ICMS.Status:=dsedit;
                                        //troco s� o c�digo para dar certo  edi��o
                                        self.ObjMaterial_ICMS.Submit_Codigo(Self.ObjMaterial_ICMS.Objquery.fieldbyname('codigo').asstring);
                              End;
                              
                              if (Self.ObjMaterial_ICMS.Salvar(True)=False)
                              then Begin
                                        if (MensagemPergunta('N�o foi poss�vel salvar os impostos para o produto '+Grid.Cells[2,cont]+#13+'Deseja continuar?')=mrno)
                                        then exit;
                              End;

                     End;
                End;
                FMostraBarraProgresso.close;
           End;
     End;
     







     
     
end;

procedure TFRImposto_ICMS.edtCFOPForaEstadoExit(Sender: TObject);
begin
      Self.ObjMaterial_ICMS.IMPOSTO.EdtCFOPExit(sender,lbCfopForaEstado);
end;

procedure TFRImposto_ICMS.edtCFOPForaEstadoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
       Self.ObjMaterial_ICMS.IMPOSTO.EdtCFOPKeyDown(sender,key,shift,lbCfopForaEstado);
end;

procedure TFRImposto_ICMS.edtCSOSNKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  Self.ObjMaterial_ICMS.edtCSOSNkeydown(Sender,Key,Shift,nil);
  self.edtCSOSN.Hint:=get_campoTabela('descricao','codigo','TABCSOSN',self.edtCSOSN.Text);
end;                           
procedure TFRImposto_ICMS.edttipocliente_material_icmsDblClick(
  Sender: TObject);
var
  key:Word;
  shift:TShiftState;
begin
  key:=VK_F9;
  ObjMaterial_ICms.EdttipoclienteKeyDown(sender,key,shift,lbnometipocliente);
end;

procedure TFRImposto_ICMS.EDTImpostoModelo_ICMSDblClick(Sender: TObject);
var
  key:Word;
  shift:TShiftState;
begin
  key:=VK_F9;
  ObjMaterial_ICms.EdtIMPOSTOKeyDown(sender,key,shift,nil);
end;

procedure TFRImposto_ICMS.edtSTA_ICMSDblClick(Sender: TObject);
var
  key:Word;
  shift:TShiftState;
begin
  key:=VK_F9;
  Self.ObjMaterial_ICMS.IMPOSTO.EdtSTAKeyDown(sender,key,shift,LbNomeSTA_ICMS);
end;

procedure TFRImposto_ICMS.EdtSTB_ICMSDblClick(Sender: TObject);
var
  key:Word;
  shift:TShiftState;
begin
  key:=VK_F9;
  Self.ObjMaterial_ICMS.IMPOSTO.EdtSTBKeyDown(sender,key,shift,LbNomeSTB_ICMS);
end;

procedure TFRImposto_ICMS.edtCSOSNDblClick(Sender: TObject);
var
  key:Word;
  shift:TShiftState;
begin
  key:=VK_F9;
  Self.ObjMaterial_ICMS.edtCSOSNkeydown(Sender,Key,Shift,nil);
  self.edtCSOSN.Hint:=get_campoTabela('descricao','codigo','TABCSOSN',self.edtCSOSN.Text);
end;

procedure TFRImposto_ICMS.edtCFOPForaEstadoDblClick(Sender: TObject);
var
  key:Word;
  shift:TShiftState;
begin
  key:=VK_F9;
  Self.ObjMaterial_ICMS.IMPOSTO.EdtCFOPKeyDown(sender,key,shift,lbCfopForaEstado);  
end;

procedure TFRImposto_ICMS.EdtCFOP_ICMSDblClick(Sender: TObject);
var
  key:Word;
  shift:TShiftState;
begin
  key:=VK_F9;
  Self.ObjMaterial_ICMS.IMPOSTO.EdtCFOPKeyDown(sender,key,shift,lbnomeCFOP_ICMS);
end;

End.




























