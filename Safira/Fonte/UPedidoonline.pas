unit UPedidoonline;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, StdCtrls, ImgList,IBQuery,UDataModulo, Menus,
  Buttons,UessencialGlobal;

type
  TFPEDIDOONLINE = class(TForm)
    pnlbotes: TPanel;
    pnl1: TPanel;
    lbltitulo: TLabel;
    pnl6: TPanel;
    lbllb8: TLabel;
    STRGMateriais: TStringGrid;
    ilProdutos: TImageList;
    pmPopUpCadastros: TPopupMenu;
    Visualizardadospedido: TMenuItem;
    btnFinalizar: TSpeedButton;
    Valor: TMenuItem;
    btnaprovarmarcados: TSpeedButton;
    procedure STRGMateriaisDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure STRGMateriaisDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure VisualizardadospedidoClick(Sender: TObject);
    procedure ValorClick(Sender: TObject);
    procedure btnFinalizarClick(Sender: TObject);
    procedure btnaprovarmarcadosClick(Sender: TObject);
  private
      Procedure CarregaPedidosonline();
  public
    { Public declarations }
  end;

var
  FPEDIDOONLINE: TFPEDIDOONLINE;

implementation

uses DB;

{$R *.dfm}

procedure TFPEDIDOONLINE.STRGMateriaisDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
    if((ARow <> 0) and (ACol = 0))
    then begin
        if(STRGMateriais.Cells[ACol,ARow] = '            X' ) or (STRGMateriais.Cells[ACol,ARow] = 'X' )then
        begin
              Ilprodutos.Draw(STRGMateriais.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 6); //check box marcado
        end
        else
        begin
              Ilprodutos.Draw(STRGMateriais.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 5); //check box sem marcar    5
        end;
    end;
end;

procedure TFPEDIDOONLINE.STRGMateriaisDblClick(Sender: TObject);
begin
    if (TStringGrid(sender).cells[0,TStringgrid(sender).row]='')
    Then TStringGrid(sender).cells[0,TStringgrid(sender).row]:='            X'
    Else TStringGrid(sender).cells[0,TStringgrid(sender).row]:='';
end;

procedure TFPEDIDOONLINE.CarregaPedidosonline();
var
  qry:TIBQuery;
  cont:Integer;
begin
  qry:=TIBQuery.Create(nil);
  qry.Database:=FDataModulo.IBDatabase;

  try
    STRGMateriais.RowCount:= 2;
    STRGMateriais.ColCount:= 9;

    STRGMateriais.ColWidths[0] := 50;
    STRGMateriais.ColWidths[1] := 100;
    STRGMateriais.ColWidths[2] := 400;
    STRGMateriais.ColWidths[3] := 300;
    STRGMateriais.ColWidths[4] := 100;
    STRGMateriais.ColWidths[5] := 100;
    STRGMateriais.ColWidths[6] := 0;
    STRGMateriais.ColWidths[7] := 0;
    STRGMateriais.ColWidths[8] := 0;

    STRGMateriais.Cells[0,0] := '';
    STRGMateriais.Cells[1,0] := '';
    STRGMateriais.Cells[2,0] := '';
    STRGMateriais.Cells[3,0] := '';
    STRGMateriais.Cells[4,0] := 'VALOR';
    STRGMateriais.Cells[5,0] := 'VALORFINAL';

    qry.Close;
    qry.SQL.Clear;
    qry.SQL.Add('select po.*,c.nome as nomecliente,v.nome as nomevendedor from tabpedidosonline po');
    qry.SQL.Add('join tabcliente c on c.codigo=po.cliente');
    qry.SQL.Add('join tabvendedor v on v.codigo=po.vendedor');
    qry.SQL.Add('where aprovado is null or aprovado= '''' ');
    qry.Open;
    cont:=1;
    while not qry.eof do
    begin
      STRGMateriais.Cells[1,cont] := qry.fieldbyname('DATAORCAMENTO').AsString;
      STRGMateriais.Cells[2,cont] := qry.fieldbyname('nomecliente').AsString;
      STRGMateriais.Cells[3,cont] := qry.fieldbyname('nomevendedor').AsString;
      STRGMateriais.Cells[4,cont] := qry.fieldbyname('valorpedido').AsString;
      STRGMateriais.Cells[5,cont] := qry.fieldbyname('valorfinalpedido').AsString;
      STRGMateriais.Cells[6,cont] := qry.fieldbyname('cliente').AsString;
      STRGMateriais.Cells[7,cont] := qry.fieldbyname('vendedor').AsString;
      STRGMateriais.Cells[8,cont] := qry.fieldbyname('codigo').AsString;
      STRGMateriais.Cells[0,cont] := '';

      Inc(cont,1);
      STRGMateriais.RowCount:=STRGMateriais.RowCount+1;
      qry.Next;
    end;
    STRGMateriais.RowCount:=STRGMateriais.RowCount-1;
  finally
    FreeAndNil(qry);
  end;


end;

procedure TFPEDIDOONLINE.FormShow(Sender: TObject);
begin
  CarregaPedidosonline;
end;

procedure TFPEDIDOONLINE.VisualizardadospedidoClick(Sender: TObject);
begin
    //Visualizar dados do pedido
end;

procedure TFPEDIDOONLINE.ValorClick(Sender: TObject);
begin
    //Atualiza valor dos itens do pedido
end;

procedure TFPEDIDOONLINE.btnFinalizarClick(Sender: TObject);
var
  qry,qry2,qry3,qry4:TIBQuery;
  codigopedido,codigopedidoprojeto,materialcor,proxcodigo,proxcodigo2,codigopedidoonline,cont:Integer;
begin
  //finalizar pedido
  qry:=TIBQuery.Create(nil);
  qry.Database:=FDataModulo.IBDatabase;
  qry2:=TIBQuery.Create(nil);
  qry2.Database:=FDataModulo.IBDatabase;
  qry3:=TIBQuery.Create(nil);
  qry3.Database:=FDataModulo.IBDatabase;
  qry4:=TIBQuery.Create(nil);
  qry4.Database:=FDataModulo.IBDatabase;

  try
    qry.Close;
    qry.SQL.Clear;
    //codigo,codigopedidobaseonline,descricao,valorpedido,valorfinalpedido,desconto,dataorcamento,cliente,vendedor,aprovado
    qry.SQL.Add('select codigo,codigopedidobaseonline,descricao,valorpedido,valorfinalpedido,desconto,');
    qry.SQL.Add('dataorcamento,cliente,vendedor,aprovado from tabpedidosonline where aprovado is null or aprovado='''' ');
    qry.Open;
    if(qry.fieldbyname('codigo').AsString='')
    then Exit;
    

    while not qry.Eof do
    begin
       //primeiro gravo o pedido
       //depois gravo os projetos do pedido
       //gravo os materiais de cada projeto
       //gravo os componentes de cada projeto
       //gravo as medidas do projeto

       //GRAVANDO O PEDIDO
       codigopedidoonline:=qry.fieldbyname('codigo').Value;
       qry2.Close;
       qry2.SQL.Clear;
       qry2.SQL.Add('SELECT GEN_ID(GENPEDIDO,1) CODIGO FROM RDB$DATABASE');
       qry2.Open;
       codigopedido := qry2.Fields[0].Value;


       qry2.Close;
       qry2.SQL.Clear;
       qry2.SQL.Add('insert into tabpedido(descricao,cliente,vendedor,data,concluido,valortotal,');
       qry2.SQL.Add('valordesconto,valortitulo,valorfinal,email,dataorcamento,ALTERADOPORTROCA,codigo,');
       qry2.sql.add('valorcomissaoarquiteto,valorbonificacaocliente,bonificacaogerada,valoracrescimo,PORCENTAGEMCOMISSAOARQUITETO)');
       qry2.SQL.Add('values (:descricao,:cliente,:vendedor,:data,:concluido,:valortotal,');
       qry2.SQL.Add(':valordesconto,:valortitulo,:valorfinal,:email,:dataorcamento,:ALTERADOPORTROCA,:codigo,');
       qry2.sql.add(':valorcomissaoarquiteto,:valorbonificacaocliente,:bonificacaogerada,:valoracrescimo,:PORCENTAGEMCOMISSAOARQUITETO)');

       qry2.Params[0].Value  :=  qry.Fields[2].Value;
       qry2.Params[1].Value  :=  qry.Fields[7].Value;
       qry2.Params[2].Value  :=  qry.Fields[8].Value;
       qry2.Params[3].Value  :=  qry.Fields[6].Value;
       qry2.Params[4].Value  :=  'N';
       qry2.Params[5].Value  :=  qry.Fields[3].Value;
       qry2.Params[6].Value  :=  qry.Fields[5].Value;
       qry2.Params[7].Value  :=  qry.Fields[4].Value;
       qry2.Params[8].Value  :=  qry.Fields[4].Value;
       qry2.Params[9].Value  :=  get_campoTabela('email','codigo','tabcliente',qry.Fields[7].Value);
       qry2.Params[10].Value :=  qry.Fields[6].Value;
       qry2.Params[11].Value :=  'N';
       qry2.Params[12].Value :=  codigopedido;
       qry2.Params[13].Value :=  0;
       qry2.Params[14].Value :=  0;
       qry2.Params[15].Value :=  0;
       qry2.Params[16].Value :=  0;

       if(qry.Fields[5].Value>0)
       then qry2.Params[17].Value :=  (qry.Fields[5].Value*100)/qry.Fields[3].Value
       else
       qry2.Params[17].Value :=  0;

       qry2.ExecSQL;

      { qry2.Close;
       qry2.SQL.Clear;
       qry2.SQL.Add('select max(codigo) from tabpedido');
       qry2.Open; }


       //GRAVANDO OS PROJETOS DESTE PEDIDO
       //codigo,codigopponline,altura,largura,codigopedidoonline,codigoprojetoonline,codigoprojetolocal,corferragemlocal,
       //corvidrolocal,cordiversolocal,cordiversolocal,corkitboxlocal,corperfiladolocal,quantidade,valor,valorfinal,codigotabelapedidoonline
       qry3.Close;
       qry3.SQL.Clear;
       qry3.SQL.Add('select codigo,codigopponline,altura,largura,codigopedidoonline,codigoprojetoonline,codigoprojetolocal,corferragemlocal,');
       qry3.SQL.Add('corvidrolocal,cordiversolocal,corkitboxlocal,corperfiladolocal,quantidade,valor,valorfinal,CODIGOTABELAPEDIDOONLINELOCAL');
       qry3.SQL.Add('from tabpedidoprojetoonline where CODIGOTABELAPEDIDOONLINELOCAL='+qry.fieldbyname('codigo').AsString);
       qry3.Open;
       while not qry3.Eof do
       begin
          for Cont:=1 to qry3.Fields[12].Value do
          begin
            qry2.Close;
            qry2.SQL.Clear;
            qry2.SQL.Add('SELECT GEN_ID(GENPEDIDO_PROJ,1) CODIGO FROM RDB$DATABASE');
            qry2.Open;
            codigopedidoprojeto := qry2.Fields[0].Value;

            qry2.Close;
            qry2.SQL.Clear;
            qry2.SQL.Add('insert into tabpedido_proj(pedido, projeto,corperfilado,corferragem,corvidro,corkitbox,cordiverso,');
            qry2.SQL.Add('quantidade,valortotal,valordesconto,valoracrescimo,codigo)');
            qry2.SQL.Add('values (:pedido,:projeto,:corperfilado,:corferragem,:corvidro,:corkitbox,:cordiverso,');
            qry2.SQL.Add(':quantidade,:valortotal,:valordesconto,:valoracrescimo,:codigo)');

            qry2.Params[0].Value  :=  codigopedido;
            qry2.Params[1].Value  :=  qry3.Fields[6].Value;
            qry2.Params[2].Value  :=  qry3.Fields[11].Value;
            qry2.Params[3].Value  :=  qry3.Fields[7].Value;
            qry2.Params[4].Value  :=  qry3.Fields[8].Value;
            qry2.Params[5].Value  :=  qry3.Fields[10].Value;
            qry2.Params[6].Value  :=  qry3.Fields[9].Value;
            qry2.Params[7].Value  :=  qry3.Fields[12].Value;
            qry2.Params[8].Value  :=  qry3.Fields[13].Value;
            qry2.Params[9].Value  :=  0;
            qry2.Params[10].Value :=  0;
            qry2.Params[11].Value := codigopedidoprojeto;
            qry2.ExecSQL;

            //qry2.Close;
            //qry2.SQL.Clear;
            //qry2.SQL.Add('select last(codigo) from tabpedido_proj');
            //qry2.Open;


            //GRAVANDO OS COMPONENTES DO PROJETO
            qry4.Close;
            qry4.SQL.Clear;
            qry4.SQL.Add('select componentelocal,altura,largura from tabcomponentepp_online where pedidoprojeto='+qry3.fieldbyname('codigo').AsString);
            qry4.Open;
            while not qry4.Eof do
            begin
              qry2.Close;
              qry2.SQL.Clear;
              qry2.SQL.Add('SELECT GEN_ID(GENCOMPONENTE_PP,1) CODIGO FROM RDB$DATABASE');
              qry2.Open;
              proxcodigo:=qry2.Fields[0].Value;

              qry2.Close;
              qry2.SQL.Clear;
              qry2.SQL.Add('insert into tabcomponente_pp (pedidoprojeto,componente,largura,altura,codigo)');
              qry2.SQL.Add('values (:pedidoprojeto,:componente,:largura,:altura,:codigo)');

              qry2.Params[0].Value  :=  codigopedidoprojeto;
              qry2.Params[1].Value  :=  qry4.Fields[0].Value;
              qry2.Params[2].Value  :=  qry4.Fields[1].Value;
              qry2.Params[3].Value  :=  qry4.Fields[2].Value;
              qry2.Params[4].Value  :=  proxcodigo;
              qry2.ExecSQL;

              qry4.Next;
            end;

            //GRAVANDO OS MATERIAIS DO PROJETO
            qry4.Close;
            qry4.SQL.Clear;
            qry4.SQL.Add('select codigo,codigobaseonline,codigomateriallocal,codigocorlocal,');
            qry4.SQL.Add('tabeladeorigem,quantidade,valor,valorfinal,altura,largura,servico');
            qry4.SQL.Add('from tabmateriaispedidoonline where pedidoprojeto='+qry3.fieldbyname('codigo').AsString);
            qry4.Open;

            while not qry4.Eof do
            begin
              if (qry4.Fields[4].AsString='tabferragem')then
              begin
                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('SELECT GEN_ID(GENFERRAGEM_PP,1) CODIGO FROM RDB$DATABASE');
                  qry2.Open;
                  proxcodigo:=qry2.Fields[0].Value;

                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('select codigo from tabferragemcor where ferragem='+qry4.Fields[2].AsString);
                  qry2.SQL.Add('and cor='+qry4.Fields[3].AsString);
                  qry2.Open;
                  materialcor := qry2.Fields[0].Value;

                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('insert into tabferragem_pp (pedidoprojeto,ferragemcor,quantidade,valor,codigo)');
                  qry2.SQL.Add('values (:pedidoprojeto,:ferragemcor,:quantidade,:valor,:codigo)');
                  qry2.Params[0].Value  :=  codigopedidoprojeto;
                  qry2.Params[1].Value  :=  materialcor;
                  qry2.Params[2].Value  :=  qry4.Fields[5].Value;
                  qry2.Params[3].Value  :=  qry4.Fields[6].Value;
                  qry2.Params[4].Value  :=  proxcodigo;
                  qry2.ExecSQL;


              end;

              if (qry4.Fields[4].AsString='tabperfilado')then
              begin
                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('SELECT GEN_ID(GENPERFILADO_PP,1) CODIGO FROM RDB$DATABASE');
                  qry2.Open;
                  proxcodigo:=qry2.Fields[0].Value;


                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('select codigo from tabperfiladocor where perfilado='+qry4.Fields[2].AsString);
                  qry2.SQL.Add('and cor='+qry4.Fields[3].AsString);
                  qry2.Open;
                  materialcor := qry2.Fields[0].Value;

                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('insert into tabperfilado_pp (pedidoprojeto,perfiladocor,quantidade,valor,codigo)');
                  qry2.SQL.Add('values (:pedidoprojeto,:perfiladocor,:quantidade,:valor,:codigo)');
                  qry2.Params[0].Value  :=  codigopedidoprojeto;
                  qry2.Params[1].Value  :=  materialcor;
                  qry2.Params[2].Value  :=  qry4.Fields[5].Value;
                  qry2.Params[3].Value  :=  qry4.Fields[6].Value;
                  qry2.Params[4].Value  :=  proxcodigo ;
                  qry2.ExecSQL;
              end;

              if (qry4.Fields[4].AsString='tabdiverso')then
              begin
                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('SELECT GEN_ID(GENDIVERSO_PP,1) CODIGO FROM RDB$DATABASE');
                  qry2.Open;
                  proxcodigo:=qry2.Fields[0].Value;


                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('select codigo from tabdiversocor where diverso='+qry4.Fields[2].AsString);
                  qry2.SQL.Add('and cor='+qry4.Fields[3].AsString);
                  qry2.Open;
                  materialcor := qry2.Fields[0].Value;

                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('insert into tabdiverso_pp (pedidoprojeto,diversocor,quantidade,valor,codigo)');
                  qry2.SQL.Add('values (:pedidoprojeto,:diversocor,:quantidade,:valor,:codigo)');
                  qry2.Params[0].Value  :=  codigopedidoprojeto;
                  qry2.Params[1].Value  :=  materialcor;
                  qry2.Params[2].Value  :=  qry4.Fields[5].Value;
                  qry2.Params[3].Value  :=  qry4.Fields[6].Value;
                  qry2.Params[4].Value  :=  proxcodigo ;
                  qry2.ExecSQL;
              end;

              if (qry4.Fields[4].AsString='tabkitbox')then
              begin
                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('SELECT GEN_ID(GENKITBOX_PP,1) CODIGO FROM RDB$DATABASE');
                  qry2.Open;
                  proxcodigo:=qry2.Fields[0].Value;


                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('select codigo from tabkitboxcor where kitbox='+qry4.Fields[2].AsString);
                  qry2.SQL.Add('and cor='+qry4.Fields[3].AsString);
                  qry2.Open;
                  materialcor := qry2.Fields[0].Value;

                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('insert into tabkitbox_pp (pedidoprojeto,kitboxcor,quantidade,valor,:codigo)');
                  qry2.SQL.Add('values (:pedidoprojeto,:kitboxcor,:quantidade,:valor,:codigo)');
                  qry2.Params[0].Value  :=  codigopedidoprojeto;
                  qry2.Params[1].Value  :=  materialcor;
                  qry2.Params[2].Value  :=  qry4.Fields[5].Value;
                  qry2.Params[3].Value  :=  qry4.Fields[6].Value;
                  qry2.Params[4].Value  :=  proxcodigo;
                  qry2.ExecSQL;
              end;

              if (qry4.Fields[4].AsString='tabvidro')then
              begin
                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('SELECT GEN_ID(GENVIDRO_PP,1) CODIGO FROM RDB$DATABASE');
                  qry2.Open;
                  proxcodigo:=qry2.Fields[0].Value;


                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('select codigo from tabvidrocor where vidro='+qry4.Fields[2].AsString);
                  qry2.SQL.Add('and cor='+qry4.Fields[3].AsString);
                  qry2.Open;
                  materialcor := qry2.Fields[0].Value;

                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('insert into tabvidro_pp (pedidoprojeto,vidrocor,quantidade,valor,altura,largura,codigo)');
                  qry2.SQL.Add('values (:pedidoprojeto,:vidrocor,:quantidade,:valor,:altura,:largura,:codigo)');
                  qry2.Params[0].Value  :=  codigopedidoprojeto;
                  qry2.Params[1].Value  :=  materialcor;
                  qry2.Params[2].Value  :=  qry4.Fields[5].Value;
                  qry2.Params[3].Value  :=  qry4.Fields[6].Value;
                  qry2.Params[4].Value  :=  qry3.Fields[2].Value;
                  qry2.Params[5].Value  :=  qry3.Fields[3].Value;
                  qry2.Params[6].Value  :=  proxcodigo;
                  qry2.ExecSQL;
              end;

              if (qry4.Fields[4].AsString='tabservico')then
              begin
                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('SELECT GEN_ID(GENSERVICO_PP,1) CODIGO FROM RDB$DATABASE');
                  qry2.Open;
                  proxcodigo:=qry2.Fields[0].Value;

                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('insert into tabservico_pp (pedidoprojeto,quantidade,valor,altura,largura,codigo,servico)');
                  qry2.SQL.Add('values (:pedidoprojeto,:quantidade,:valor,:altura,:largura,:codigo,:servico)');
                  qry2.Params[0].Value  :=  codigopedidoprojeto;
                  qry2.Params[1].Value  :=  qry4.Fields[5].Value;
                  qry2.Params[2].Value  :=  qry4.Fields[6].Value;
                  qry2.Params[3].Value  :=  qry4.Fields[8].Value;
                  qry2.Params[4].Value  :=  qry4.Fields[9].Value;
                  qry2.Params[5].Value  :=  proxcodigo;
                  qry2.Params[6].Value  :=  qry4.Fields[10].Value;
                  qry2.ExecSQL;
              end;

              qry4.Next;
            end;

            //gravar medidas projeto
            qry2.Close;
            qry2.SQL.Clear;
            qry2.SQL.Add('SELECT GEN_ID(GENMEDIDAS_PROJ,1) CODIGO FROM RDB$DATABASE');
            qry2.open;
            proxcodigo2:= qry2.Fields[0].AsInteger;

            qry2.Close;
            qry2.SQL.Clear;
            qry2.SQL.Add('insert into tabmedidas_proj (codigo,pedidoprojeto,componente,altura,largura,ALTURAARREDONDAMENTO,LARGURAARREDONDAMENTO)');
            qry2.SQL.Add('values (:codigo,:pedidoprojeto,:componente,:altura,:largura,:ALTURAARREDONDAMENTO,:LARGURAARREDONDAMENTO)');
            qry2.Params[0].Value  :=  proxcodigo2;
            qry2.Params[1].Value  :=  codigopedidoprojeto;
            qry2.Params[2].Value  :=  'FRONTAL';
            qry2.Params[3].Value  :=  qry3.Fields[2].Value;
            qry2.Params[4].Value  :=  qry3.Fields[3].Value;
            qry2.Params[5].Value  :=  0;
            qry2.Params[6].Value  :=  0;
            qry2.ExecSQL;
          end;

          qry3.Next;
       end;

       qry2.Close;
       qry2.SQL.Clear;
       qry2.SQL.Add('update tabpedidosonline set aprovado=''S'' ');
       qry2.SQL.Add('where codigo='+IntToStr(codigopedidoonline));
       qry2.ExecSQL;




      qry.Next;
    end;


    FDataModulo.IBTransaction.CommitRetaining;

  finally
    FreeAndNil(qry);
    FreeAndNil(qry2);
    FreeAndNil(qry3);
    FreeAndNil(qry4);
    CarregaPedidosonline();
  end;

end;

procedure TFPEDIDOONLINE.btnaprovarmarcadosClick(Sender: TObject);
var
  qry,qry2,qry3,qry4:TIBQuery;
  codigopedido,codigopedidoprojeto,materialcor,proxcodigo,codigopedidoonline,cont,cont2,proxcodigo2:Integer;
begin
  //aprovar somente os marcados
  for Cont:=1 to STRGMateriais.RowCount do
  Begin
     if(STRGMateriais.Cells[0,cont] = '            X') or(STRGMateriais.Cells[0,cont] = 'X')then
     begin
          qry:=TIBQuery.Create(nil);
          qry.Database:=FDataModulo.IBDatabase;
          qry2:=TIBQuery.Create(nil);
          qry2.Database:=FDataModulo.IBDatabase;
          qry3:=TIBQuery.Create(nil);
          qry3.Database:=FDataModulo.IBDatabase;
          qry4:=TIBQuery.Create(nil);
          qry4.Database:=FDataModulo.IBDatabase;

          try
            qry.Close;
            qry.SQL.Clear;
            qry.SQL.Add('select codigo,codigopedidobaseonline,descricao,valorpedido,valorfinalpedido,desconto,');
            qry.SQL.Add('dataorcamento,cliente,vendedor,aprovado from tabpedidosonline where codigo='+STRGMateriais.Cells[8,cont]);
            qry.Open;

            codigopedidoonline:=StrToInt(STRGMateriais.Cells[8,cont]);
            //primeiro gravo o pedido
            //depois gravo os projetos do pedido
            //gravo os materiais de cada projeto
            //gravo os componentes de cada projeto
            //gravo as medidas do projeto

            //GRAVANDO O PEDIDO

            qry2.Close;
            qry2.SQL.Clear;
            qry2.SQL.Add('SELECT GEN_ID(GENPEDIDO,1) CODIGO FROM RDB$DATABASE');
            qry2.Open;
            codigopedido := qry2.Fields[0].Value;


            qry2.Close;
            qry2.SQL.Clear;
            qry2.SQL.Add('insert into tabpedido(descricao,cliente,vendedor,data,concluido,valortotal,');
            qry2.SQL.Add('valordesconto,valortitulo,valorfinal,email,dataorcamento,ALTERADOPORTROCA,codigo,');
            qry2.sql.add('valorcomissaoarquiteto,valorbonificacaocliente,bonificacaogerada,valoracrescimo,PORCENTAGEMCOMISSAOARQUITETO)');
            qry2.SQL.Add('values (:descricao,:cliente,:vendedor,:data,:concluido,:valortotal,');
            qry2.SQL.Add(':valordesconto,:valortitulo,:valorfinal,:email,:dataorcamento,:ALTERADOPORTROCA,:codigo,');
            qry2.sql.add(':valorcomissaoarquiteto,:valorbonificacaocliente,:bonificacaogerada,:valoracrescimo,:PORCENTAGEMCOMISSAOARQUITETO)');

            qry2.Params[0].Value  :=  qry.Fields[2].Value;
            qry2.Params[1].Value  :=  qry.Fields[7].Value;
            qry2.Params[2].Value  :=  qry.Fields[8].Value;
            qry2.Params[3].Value  :=  qry.Fields[6].Value;
            qry2.Params[4].Value  :=  'N';
            qry2.Params[5].Value  :=  qry.Fields[3].Value;
            qry2.Params[6].Value  :=  qry.Fields[5].Value;
            qry2.Params[7].Value  :=  qry.Fields[4].Value;
            qry2.Params[8].Value  :=  qry.Fields[4].Value;
            qry2.Params[9].Value  :=  get_campoTabela('email','codigo','tabcliente',qry.Fields[7].Value);
            qry2.Params[10].Value :=  qry.Fields[6].Value;
            qry2.Params[11].Value :=  'N';
            qry2.Params[12].Value :=  codigopedido;
            qry2.Params[13].Value :=  0;
            qry2.Params[14].Value :=  0;
            qry2.Params[15].Value :=  0;
            qry2.Params[16].Value :=  0;

            if(qry.Fields[5].Value>0)
            then qry2.Params[17].Value :=  (qry.Fields[5].Value*100)/qry.Fields[3].Value
            else
            qry2.Params[17].Value :=  0;

            qry2.ExecSQL;


            //GRAVANDO OS PROJETOS DESTE PEDIDO
            //codigo,codigopponline,altura,largura,codigopedidoonline,codigoprojetoonline,codigoprojetolocal,corferragemlocal,
            //corvidrolocal,cordiversolocal,cordiversolocal,corkitboxlocal,corperfiladolocal,quantidade,valor,valorfinal,codigotabelapedidoonline
            qry3.Close;
            qry3.SQL.Clear;
            qry3.SQL.Add('select codigo,codigopponline,altura,largura,codigopedidoonline,codigoprojetoonline,codigoprojetolocal,corferragemlocal,');
            qry3.SQL.Add('corvidrolocal,cordiversolocal,corkitboxlocal,corperfiladolocal,quantidade,valor,valorfinal,CODIGOTABELAPEDIDOONLINELOCAL');
            qry3.SQL.Add('from tabpedidoprojetoonline where CODIGOTABELAPEDIDOONLINELOCAL='+qry.fieldbyname('codigo').AsString);
            qry3.Open;
            while not qry3.Eof do
            begin
              for cont2:=1 to qry3.Fields[12].Value do
              begin
                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('SELECT GEN_ID(GENPEDIDO_PROJ,1) CODIGO FROM RDB$DATABASE');
                  qry2.Open;
                  codigopedidoprojeto := qry2.Fields[0].Value;

                  qry2.Close;
                  qry2.SQL.Clear;
                  qry2.SQL.Add('insert into tabpedido_proj(pedido, projeto,corperfilado,corferragem,corvidro,corkitbox,cordiverso,');
                  qry2.SQL.Add('quantidade,valortotal,valordesconto,valoracrescimo,codigo)');
                  qry2.SQL.Add('values (:pedido,:projeto,:corperfilado,:corferragem,:corvidro,:corkitbox,:cordiverso,');
                  qry2.SQL.Add(':quantidade,:valortotal,:valordesconto,:valoracrescimo,:codigo)');

                  qry2.Params[0].Value  :=  codigopedido;
                  qry2.Params[1].Value  :=  qry3.Fields[6].Value;
                  qry2.Params[2].Value  :=  qry3.Fields[11].Value;
                  qry2.Params[3].Value  :=  qry3.Fields[7].Value;
                  qry2.Params[4].Value  :=  qry3.Fields[8].Value;
                  qry2.Params[5].Value  :=  qry3.Fields[10].Value;
                  qry2.Params[6].Value  :=  qry3.Fields[9].Value;
                  qry2.Params[7].Value  :=  qry3.Fields[12].Value;
                  qry2.Params[8].Value  :=  qry3.Fields[13].Value;
                  qry2.Params[9].Value  :=  0;
                  qry2.Params[10].Value :=  0;
                  qry2.Params[11].Value := codigopedidoprojeto;
                  qry2.ExecSQL;

                  //GRAVANDO OS COMPONENTES DO PROJETO
                  qry4.Close;
                  qry4.SQL.Clear;
                  qry4.SQL.Add('select componentelocal,altura,largura from tabcomponentepp_online where pedidoprojeto='+qry3.fieldbyname('codigo').AsString);
                  qry4.Open;
                  while not qry4.Eof do
                  begin
                    qry2.Close;
                    qry2.SQL.Clear;
                    qry2.SQL.Add('SELECT GEN_ID(GENCOMPONENTE_PP,1) CODIGO FROM RDB$DATABASE');
                    qry2.Open;
                    proxcodigo:=qry2.Fields[0].Value;

                    qry2.Close;
                    qry2.SQL.Clear;
                    qry2.SQL.Add('insert into tabcomponente_pp (pedidoprojeto,componente,largura,altura,codigo)');
                    qry2.SQL.Add('values (:pedidoprojeto,:componente,:largura,:altura,:codigo)');

                    qry2.Params[0].Value  :=  codigopedidoprojeto;
                    qry2.Params[1].Value  :=  qry4.Fields[0].Value;
                    qry2.Params[2].Value  :=  qry4.Fields[1].Value;
                    qry2.Params[3].Value  :=  qry4.Fields[2].Value;
                    qry2.Params[4].Value  :=  proxcodigo;
                    qry2.ExecSQL;

                    qry4.Next;
                  end;

                  //GRAVANDO OS MATERIAIS DO PROJETO
                  qry4.Close;
                  qry4.SQL.Clear;
                  qry4.SQL.Add('select codigo,codigobaseonline,codigomateriallocal,codigocorlocal,');
                  qry4.SQL.Add('tabeladeorigem,quantidade,valor,valorfinal,altura,largura,servico');
                  qry4.SQL.Add('from tabmateriaispedidoonline where pedidoprojeto='+qry3.fieldbyname('codigo').AsString);
                  qry4.Open;

                  while not qry4.Eof do
                  begin
                    if (qry4.Fields[4].AsString='tabferragem')then
                    begin
                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('SELECT GEN_ID(GENFERRAGEM_PP,1) CODIGO FROM RDB$DATABASE');
                        qry2.Open;
                        proxcodigo:=qry2.Fields[0].Value;

                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('select codigo from tabferragemcor where ferragem='+qry4.Fields[2].AsString);
                        qry2.SQL.Add('and cor='+qry4.Fields[3].AsString);
                        qry2.Open;
                        materialcor := qry2.Fields[0].Value;

                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('insert into tabferragem_pp (pedidoprojeto,ferragemcor,quantidade,valor,codigo)');
                        qry2.SQL.Add('values (:pedidoprojeto,:ferragemcor,:quantidade,:valor,:codigo)');
                        qry2.Params[0].Value  :=  codigopedidoprojeto;
                        qry2.Params[1].Value  :=  materialcor;
                        qry2.Params[2].Value  :=  qry4.Fields[5].Value;
                        qry2.Params[3].Value  :=  qry4.Fields[6].Value;
                        qry2.Params[4].Value  :=  proxcodigo;
                        qry2.ExecSQL;


                    end;

                    if (qry4.Fields[4].AsString='tabperfilado')then
                    begin
                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('SELECT GEN_ID(GENPERFILADO_PP,1) CODIGO FROM RDB$DATABASE');
                        qry2.Open;
                        proxcodigo:=qry2.Fields[0].Value;


                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('select codigo from tabperfiladocor where perfilado='+qry4.Fields[2].AsString);
                        qry2.SQL.Add('and cor='+qry4.Fields[3].AsString);
                        qry2.Open;
                        materialcor := qry2.Fields[0].Value;

                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('insert into tabperfilado_pp (pedidoprojeto,perfiladocor,quantidade,valor,codigo)');
                        qry2.SQL.Add('values (:pedidoprojeto,:perfiladocor,:quantidade,:valor,:codigo)');
                        qry2.Params[0].Value  :=  codigopedidoprojeto;
                        qry2.Params[1].Value  :=  materialcor;
                        qry2.Params[2].Value  :=  qry4.Fields[5].Value;
                        qry2.Params[3].Value  :=  qry4.Fields[6].Value;
                        qry2.Params[4].Value  :=  proxcodigo ;
                        qry2.ExecSQL;
                    end;

                    if (qry4.Fields[4].AsString='tabdiverso')then
                    begin
                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('SELECT GEN_ID(GENDIVERSO_PP,1) CODIGO FROM RDB$DATABASE');
                        qry2.Open;
                        proxcodigo:=qry2.Fields[0].Value;


                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('select codigo from tabdiversocor where diverso='+qry4.Fields[2].AsString);
                        qry2.SQL.Add('and cor='+qry4.Fields[3].AsString);
                        qry2.Open;
                        materialcor := qry2.Fields[0].Value;

                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('insert into tabdiverso_pp (pedidoprojeto,diversocor,quantidade,valor,codigo)');
                        qry2.SQL.Add('values (:pedidoprojeto,:diversocor,:quantidade,:valor,:codigo)');
                        qry2.Params[0].Value  :=  codigopedidoprojeto;
                        qry2.Params[1].Value  :=  materialcor;
                        qry2.Params[2].Value  :=  qry4.Fields[5].Value;
                        qry2.Params[3].Value  :=  qry4.Fields[6].Value;
                        qry2.Params[4].Value  :=  proxcodigo ;
                        qry2.ExecSQL;
                    end;

                    if (qry4.Fields[4].AsString='tabkitbox')then
                    begin
                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('SELECT GEN_ID(GENKITBOX_PP,1) CODIGO FROM RDB$DATABASE');
                        qry2.Open;
                        proxcodigo:=qry2.Fields[0].Value;


                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('select codigo from tabkitboxcor where kitbox='+qry4.Fields[2].AsString);
                        qry2.SQL.Add('and cor='+qry4.Fields[3].AsString);
                        qry2.Open;
                        materialcor := qry2.Fields[0].Value;

                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('insert into tabkitbox_pp (pedidoprojeto,kitboxcor,quantidade,valor,:codigo)');
                        qry2.SQL.Add('values (:pedidoprojeto,:kitboxcor,:quantidade,:valor,:codigo)');
                        qry2.Params[0].Value  :=  codigopedidoprojeto;
                        qry2.Params[1].Value  :=  materialcor;
                        qry2.Params[2].Value  :=  qry4.Fields[5].Value;
                        qry2.Params[3].Value  :=  qry4.Fields[6].Value;
                        qry2.Params[4].Value  :=  proxcodigo;
                        qry2.ExecSQL;
                    end;

                    if (qry4.Fields[4].AsString='tabvidro')then
                    begin
                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('SELECT GEN_ID(GENVIDRO_PP,1) CODIGO FROM RDB$DATABASE');
                        qry2.Open;
                        proxcodigo:=qry2.Fields[0].Value;


                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('select codigo from tabvidrocor where vidro='+qry4.Fields[2].AsString);
                        qry2.SQL.Add('and cor='+qry4.Fields[3].AsString);
                        qry2.Open;
                        materialcor := qry2.Fields[0].Value;

                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('insert into tabvidro_pp (pedidoprojeto,vidrocor,quantidade,valor,altura,largura,codigo)');
                        qry2.SQL.Add('values (:pedidoprojeto,:vidrocor,:quantidade,:valor,:altura,:largura,:codigo)');
                        qry2.Params[0].Value  :=  codigopedidoprojeto;
                        qry2.Params[1].Value  :=  materialcor;
                        qry2.Params[2].Value  :=  qry4.Fields[5].Value;
                        qry2.Params[3].Value  :=  qry4.Fields[6].Value;
                        qry2.Params[4].Value  :=  qry4.Fields[8].Value;
                        qry2.Params[5].Value  :=  qry4.Fields[9].Value;
                        qry2.Params[6].Value  :=  proxcodigo;
                        qry2.ExecSQL;
                    end;

                    if (qry4.Fields[4].AsString='tabservico')then
                    begin
                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('SELECT GEN_ID(GENSERVICO_PP,1) CODIGO FROM RDB$DATABASE');
                        qry2.Open;
                        proxcodigo:=qry2.Fields[0].Value;


                        qry2.Close;
                        qry2.SQL.Clear;
                        qry2.SQL.Add('insert into tabservico_pp (pedidoprojeto,quantidade,valor,altura,largura,codigo,servico)');
                        qry2.SQL.Add('values (:pedidoprojeto,:quantidade,:valor,:altura,:largura,:codigo,:servico)');
                        qry2.Params[0].Value  :=  codigopedidoprojeto;
                        qry2.Params[1].Value  :=  qry4.Fields[5].Value;
                        qry2.Params[2].Value  :=  qry4.Fields[6].Value;
                        qry2.Params[3].Value  :=  qry4.Fields[8].Value;
                        qry2.Params[4].Value  :=  qry4.Fields[9].Value;
                        qry2.Params[5].Value  :=  proxcodigo;
                        qry2.Params[6].Value  :=  qry4.Fields[10].Value;
                        qry2.ExecSQL;
                    end;


                    qry4.Next;
                  end;

              end;
              //gravar medidas projeto
              qry2.Close;
              qry2.SQL.Clear;
              qry2.SQL.Add('SELECT GEN_ID(GENMEDIDAS_PROJ,1) CODIGO FROM RDB$DATABASE');
              qry2.open;
              proxcodigo2:= qry2.Fields[0].AsInteger;

              qry2.Close;
              qry2.SQL.Clear;
              qry2.SQL.Add('insert into tabmedidas_proj (codigo,pedidoprojeto,componente,altura,largura,ALTURAARREDONDAMENTO,LARGURAARREDONDAMENTO)');
              qry2.SQL.Add('values (:codigo,:pedidoprojeto,:componente,:altura,:largura,:ALTURAARREDONDAMENTO,:LARGURAARREDONDAMENTO)');
              qry2.Params[0].Value  :=  proxcodigo2;
              qry2.Params[1].Value  :=  codigopedidoprojeto;
              qry2.Params[2].Value  :=  'FRONTAL';
              qry2.Params[3].Value  :=  qry3.Fields[2].Value;
              qry2.Params[4].Value  :=  qry3.Fields[3].Value;
              qry2.Params[5].Value  :=  0;
              qry2.Params[6].Value  :=  0;
              qry2.ExecSQL;
              
              qry3.Next;
            end;


            qry2.Close;
            qry2.SQL.Clear;
            qry2.SQL.Add('update tabpedidosonline set aprovado=''S'' ');
            qry2.SQL.Add('where codigo='+IntToStr(codigopedidoonline));
            qry2.ExecSQL;


            FDataModulo.IBTransaction.CommitRetaining;

          finally
            FreeAndNil(qry);
            FreeAndNil(qry2);
            FreeAndNil(qry3);
            FreeAndNil(qry4);

          end;
     end;
  end;
  CarregaPedidosonline();
end;

end.
