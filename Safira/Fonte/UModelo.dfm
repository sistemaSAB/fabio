object Fmodelo: TFmodelo
  Left = 109
  Top = 10
  Width = 544
  Height = 439
  Caption = 'Cadastro Modelo - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 322
    Width = 536
    Height = 90
    Align = alBottom
    Color = 3355443
    TabOrder = 1
    object Btnovo: TBitBtn
      Left = 3
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Novo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 115
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Alterar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 227
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Gravar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 339
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 3
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Pesquisar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 115
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Relat'#243'rios'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object btexcluir: TBitBtn
      Left = 227
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Excluir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 339
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Sair'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btsairClick
    end
  end
  object Guia: TTabbedNotebook
    Left = 1
    Top = 2
    Width = 535
    Height = 320
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = 'Principal'
      object Label1: TLabel
        Left = 264
        Top = 0
        Width = 37
        Height = 13
        Caption = 'Label1'
      end
      object Edit1: TEdit
        Left = 64
        Top = 64
        Width = 121
        Height = 19
        TabOrder = 0
        Text = 'Edit1'
      end
    end
  end
end
