unit UordemInstalacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, StdCtrls, Buttons, Mask,UobjORDEMINSTALACAO,UessencialGlobal,DB
  ,Upesquisa,UobjROMANEIOSORDEMINSTALACAO, DBGrids,UDataModulo,UAgendaInstalacao;

type
  TFordemInstalacao = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    btAjuda: TSpeedButton;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    ImgRodape: TImage;
    lbconcluido: TLabel;
    pnl2: TPanel;
    lbNomeColocador: TLabel;
    edtColocador: TMaskEdit;
    lb1: TLabel;
    lb2: TLabel;
    edtdata: TMaskEdit;
    pnl3: TPanel;
    lb4: TLabel;
    lbCodigoMaterial: TLabel;
    btGrava: TBitBtn;
    btExclui: TBitBtn;
    btCancela: TBitBtn;
    edtromaneio: TEdit;
    pnl4: TPanel;
    dbgridRomaneio: TDBGrid;
    lb3: TLabel;
    edthorario: TMaskEdit;
    edtPedidoProjeto: TEdit;
    lb5: TLabel;
    lb6: TLabel;
    edtTempoMedioInstalacao: TMaskEdit;
    lb7: TLabel;
    lbHorarioTermino: TLabel;
    lb8: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsalvarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edtColocadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtromaneioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtColocadorDblClick(Sender: TObject);
    procedure edtromaneioDblClick(Sender: TObject);
    procedure btGravaClick(Sender: TObject);
    procedure edtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtPedidoProjetoDblClick(Sender: TObject);
    procedure dbgridRomaneioDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure edtColocadorExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btCancelaClick(Sender: TObject);
    procedure lbNomeColocadorMouseLeave(Sender: TObject);
    procedure lbNomeColocadorMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure lbNomeColocadorClick(Sender: TObject);
    procedure btExcluiClick(Sender: TObject);
    procedure edtPedidoProjetoExit(Sender: TObject);
    procedure edtTempoMedioInstalacaoExit(Sender: TObject);
    procedure edtColocadorKeyPress(Sender: TObject; var Key: Char);
    procedure edtromaneioKeyPress(Sender: TObject; var Key: Char);
    procedure edtPedidoProjetoKeyPress(Sender: TObject; var Key: Char);
    procedure lb8Click(Sender: TObject);
  private
    ObjORDEMINSTALACAO:TObjORDEMINSTALACAO;
    ObjROMANEIOSORDEMINSTALACAO:TObjROMANEIOSORDEMINSTALACAO;

    procedure AjustaHoraFimInstalacao;

    function ControlesParaObjeto:Boolean;
    function TabelaParaControles: Boolean;
    function ObjetoParaControles:Boolean;
    procedure LimpaLabels;
    procedure MontaStrinGrids;
  public
    { Public declarations }
  end;

var
  FordemInstalacao: TFordemInstalacao;

implementation

uses UescolheImagemBotao;

{$R *.dfm}

procedure TFordemInstalacao.FormShow(Sender: TObject);
begin
    ObjORDEMINSTALACAO:=TObjORDEMINSTALACAO.Create;
    ObjROMANEIOSORDEMINSTALACAO:=TObjROMANEIOSORDEMINSTALACAO.Create;

    dbgridRomaneio.DataSource:=self.ObjROMANEIOSORDEMINSTALACAO.ObjDatasource;

    FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
    FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
    FescolheImagemBotao.PegaFiguraBotaopequeno(btGrava,'BOTAOINSERIR.BMP');
    FescolheImagemBotao.PegaFiguraBotaopequeno(BtCancela,'BOTAOCANCELARPRODUTO.BMP');
    FescolheImagemBotao.PegaFiguraBotaopequeno(BtExclui,'BOTAORETIRAR.BMP');
    desabilita_campos(Self);
    lbCodigo.Caption:='';
    ObjROMANEIOSORDEMINSTALACAO.CarregaDados(lbCodigo.Caption);

end;

procedure TFordemInstalacao.btsairClick(Sender: TObject);
begin
     Self.Close;
end;

procedure TFordemInstalacao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
      ObjORDEMINSTALACAO.Free;
      ObjROMANEIOSORDEMINSTALACAO.Free;
end;

procedure TFordemInstalacao.btnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     limpalabels;
     habilita_campos(Self);
     desabilita_campos(pnl3);
     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;


     ObjORDEMINSTALACAO.Status:=dsinsert;
     lbCodigo.Caption:='0';
end;

procedure TFordemInstalacao.btalterarClick(Sender: TObject);
begin
     if(lbCodigo.Caption='')
     THEN Exit;

     habilita_campos(Self);
     desabilita_campos(pnl3);
     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;
     ObjORDEMINSTALACAO.Status:=dsEdit;
end;

procedure TFordemInstalacao.btcancelarClick(Sender: TObject);
begin
     limpaedit(Self);
     limpalabels;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true ;
     btopcoes.Visible :=true;
     desabilita_campos(Self);
     lbCodigo.Caption:='';
     ObjORDEMINSTALACAO.Status:=dsInactive;
end;

procedure TFordemInstalacao.btsalvarClick(Sender: TObject);
var
  CodigoLocal:string;
begin
       If ObjORDEMINSTALACAO.Status=dsInactive
       Then exit;

       if(Objordeminstalacao.LocalizPorData(edtdata.Text,CodigoLocal,edtColocador.Text)=True) then
       begin
             ObjORDEMINSTALACAO.Status:=dsInactive;
             ObjORDEMINSTALACAO.LocalizaCodigo(CodigoLocal);
             ObjORDEMINSTALACAO.TabelaparaObjeto;
             Self.TabelaParaControles;
             btnovo.Visible :=true;
             btalterar.Visible:=true;
             btpesquisar.Visible:=true;
             btrelatorio.Visible:=true;
             btexcluir.Visible:=true;
             btsair.Visible:=true;
             btopcoes.Visible :=true;
             desabilita_campos(self);
             habilita_campos(pnl3);

             Exit;
       end;

       If ControlesParaObjeto=False
       Then Begin
                 Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
                 exit;
            End;

       If (ObjORDEMINSTALACAO.salvar(true)=False)
       Then exit;
       lbCodigo.Caption:=ObjORDEMINSTALACAO.Get_CODIGO;

       ObjORDEMINSTALACAO.LocalizaCodigo(lbCodigo.Caption) ;
       ObjORDEMINSTALACAO.TabelaparaObjeto;
       Self.TabelaParaControles;

       btnovo.Visible :=true;
       btalterar.Visible:=true;
       btpesquisar.Visible:=true;
       btrelatorio.Visible:=true;
       btexcluir.Visible:=true;
       btsair.Visible:=true;
       btopcoes.Visible :=true;
       desabilita_campos(self);
       habilita_campos(pnl3);
end;

function TFordemInstalacao.ControlesParaObjeto:Boolean;
begin
    try
         with ObjORDEMINSTALACAO do
         begin
                Submit_CODIGO(lbCodigo.Caption);
                COLOCADOR.Submit_CODIGO(edtColocador.Text);
                Submit_DATA(edtdata.Text);
                result:=True;
         end;

    except
         Result:=False;
    end;

end;

function TFordemInstalacao.TabelaParaControles: Boolean;
begin
     If (ObjORDEMINSTALACAO.TabelaparaObjeto=False) Then
     Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False) Then
     Begin
                result:=False;
                exit;
     End;
     Result:=True;

end;

function TFordemInstalacao.ObjetoParaControles:Boolean;
begin
      try
           with ObjORDEMINSTALACAO do
           begin
                edtColocador.Text:=COLOCADOR.Get_CODIGO;
                lbCodigo.Caption:=Get_CODIGO;
                edtdata.Text:=Get_DATA;
                ObjROMANEIOSORDEMINSTALACAO.CarregaDados(lbCodigo.Caption);
                lbNomeColocador.Caption:=COLOCADOR.Funcionario.Get_Nome;
           end;
           result:=True;
      except
          result:=False;
      end;

end;

procedure TFordemInstalacao.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin
        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjORDEMINSTALACAO.Get_pesquisa,ObjORDEMINSTALACAO.Get_TituloPesquisa,Nil)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                  If ObjORDEMINSTALACAO.status<>dsinactive
                                  then exit;

                                  If (ObjORDEMINSTALACAO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then
                                  Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;

                                  End;
                                  ObjORDEMINSTALACAO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then
                                  Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            //Self.limpaLabels;
                                            exit;
                                  End;
                                  desabilita_campos(self);
                                  habilita_campos(pnl3);

                        End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
            End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFordemInstalacao.btexcluirClick(Sender: TObject);
begin
     If (ObjORDEMINSTALACAO.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (ObjORDEMINSTALACAO.LocalizaCodigo(lbCodigo.Caption)=False)Then
     Begin
           Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
           exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjORDEMINSTALACAO.exclui(lbCodigo.Caption,True)=False)Then
     Begin
           Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
           exit;
     End;
     limpaedit(Self);
     lbCodigo.Caption:='';
     limpalabels;
end;

procedure TFordemInstalacao.LimpaLabels;
begin
     lbNomeColocador.Caption:='';
end;

procedure TFordemInstalacao.edtColocadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjORDEMINSTALACAO.EdtColocadorKeyDown(sender,Key,Shift,lbNomeColocador);
end;

procedure TFordemInstalacao.edtromaneioKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjROMANEIOSORDEMINSTALACAO.EdtRomaneioKeyDown(Sender,Key,Shift);
end;

procedure TFordemInstalacao.edtColocadorDblClick(Sender: TObject);
var
  key:Word;
  Shift:TShiftState;
begin
    key:=VK_F9;
    ObjORDEMINSTALACAO.EdtColocadorKeyDown(sender,Key,Shift,lbNomeColocador);
end;

procedure TFordemInstalacao.edtromaneioDblClick(Sender: TObject);
var
    Key:Word;
    Shift:TShiftState;
begin
    key:=VK_F9;
    ObjROMANEIOSORDEMINSTALACAO.EdtRomaneioKeyDown(Sender,Key,Shift);
end;


procedure TFordemInstalacao.MontaStrinGrids;
begin
     //
end;

procedure TFordemInstalacao.btGravaClick(Sender: TObject);
var
  NomeProjeto:string;
begin
      if(ObjROMANEIOSORDEMINSTALACAO.VerificaHorarioDisponivel(edthorario.Text,edtTempoMedioInstalacao.Text,lbHorarioTermino.Caption,lbCodigo.Caption,NomeProjeto)= False)then
      begin
           MensagemErro('Horario j� esta sendo usado para a instala��o do(a) '+#13+NomeProjeto);
           Exit;
      end;
      with ObjROMANEIOSORDEMINSTALACAO do
      begin
            if(edtromaneio.Text='')
            then Exit;
                        
            Status:=dsInsert;
            Submit_CODIGO('0');
            ROMANEIO.Submit_CODIGO(edtromaneio.Text);
            PEDIDOPROJETO.Submit_Codigo(edtPedidoProjeto.Text);
            ORDEMINSTALACAO.Submit_CODIGO(lbCodigo.Caption);
            Submit_HORARIO(edthorario.Text);
            Submit_TEMPOMEDIOINSTALACAO(edtTempoMedioInstalacao.Text);
            Submit_CONCLUIDO('N');
            Submit_TempoFinal(lbHorarioTermino.Caption);
            if(Salvar(true)=False) then
            begin
                 MensagemErro('Erro ao tentar salvar');
                 Exit;
            end;
            ObjROMANEIOSORDEMINSTALACAO.CarregaDados(lbCodigo.Caption);
            limpaedit(pnl3);
      end;
end;

procedure TFordemInstalacao.edtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Sql:string;
begin

     If (key <>vk_f9)
     Then exit;

     if(edtromaneio.Text='')
     then Exit;

     sql:='Select ppr.pedidoprojeto,ppr.romaneio,tabprojeto.descricao';
     sql:=sql+' from Tabpedidoprojetoromaneio ppr join tabpedido_proj on tabpedido_proj.codigo=ppr.pedidoprojeto';
     Sql:=sql+' join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto where romaneio='+edtromaneio.Text;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(sql,'',nil)=True) Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)Then
                        Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('pedidoprojeto').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFordemInstalacao.edtPedidoProjetoDblClick(Sender: TObject);
var
  key:Word;
  Shift:TShiftState;
begin
    key:=VK_F9;
    edtPedidoProjetoKeyDown(Sender,key,Shift);
end;

procedure TFordemInstalacao.dbgridRomaneioDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With dbgridRomaneio.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(dbgridRomaneio.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                dbgridRomaneio.DefaultDrawDataCell(Rect, dbgridRomaneio.Columns[DataCol].Field, State);
          End;
end;

procedure TFordemInstalacao.edtColocadorExit(Sender: TObject);
begin
       if(edtColocador.Text='')
       then Exit;

       ObjORDEMINSTALACAO.COLOCADOR.LocalizaCodigo(edtColocador.Text);
       ObjORDEMINSTALACAO.COLOCADOR.TabelaparaObjeto;
       lbNomeColocador.Caption:=ObjORDEMINSTALACAO.COLOCADOR.Funcionario.Get_Nome;
end;

procedure TFordemInstalacao.FormKeyPress(Sender: TObject; var Key: Char);
begin
       if key=#13
       Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFordemInstalacao.btCancelaClick(Sender: TObject);
begin
     limpaedit(pnl3);
end;

procedure TFordemInstalacao.lbNomeColocadorMouseLeave(Sender: TObject);
begin
    TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFordemInstalacao.lbNomeColocadorMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFordemInstalacao.lbNomeColocadorClick(Sender: TObject);
begin
      //colocar para abrir o cadastro do funcionario

end;

procedure TFordemInstalacao.btExcluiClick(Sender: TObject);
begin
     if(lbCodigo.Caption='')
     then Exit;

     if(dbgridRomaneio.DataSource.DataSet.fieldbyname('codigo').asstring = '')
     then Exit;

     if(ObjROMANEIOSORDEMINSTALACAO.Exclui(dbgridRomaneio.DataSource.DataSet.fieldbyname('codigo').asstring,True)=False) then
     begin
          MensagemErro('Erro ao tentar retirar o projeto');
          Exit;
     end;

     ObjROMANEIOSORDEMINSTALACAO.CarregaDados(lbCodigo.Caption);

end;

procedure TFordemInstalacao.edtPedidoProjetoExit(Sender: TObject);
begin
      //Verifica se o pedido/projeto j� foi inserido para instala��o

      if(edtPedidoProjeto.Text='')
      then Exit;

      if(ObjROMANEIOSORDEMINSTALACAO.VerificaProjetoInserido(edtPedidoProjeto.Text)=True) then
      begin
          edtPedidoProjeto.Text:='';
          edtPedidoProjeto.SetFocus;
      end;


end;

procedure TFordemInstalacao.AjustaHoraFimInstalacao;
var
   TempodeInstalacao:Currency;
   TempoHora:string;
   TempoMinuto:string;
   TempoHoraMarcada:string;
   TempoMinutoMarcada:string;
   SomaMinuto:Currency;
   FIni,FFim : TDateTime;
begin
     if(edthorario.Text='')
     then Exit;

     Fini:=StrToTime(edthorario.Text);
     FFim:=StrToTime(edtTempoMedioInstalacao.Text);
     lbHorarioTermino.Caption:= TimeToStr(Fini+FFim)   ;

end;

procedure TFordemInstalacao.edtTempoMedioInstalacaoExit(Sender: TObject);
begin
     if(edtTempoMedioInstalacao.Text='')
     then Exit;
     AjustaHoraFimInstalacao;

end;

procedure TFordemInstalacao.edtColocadorKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8),',']) then
       begin
                Key:= #0;
       end;
end;

procedure TFordemInstalacao.edtromaneioKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8),',']) then
       begin
                Key:= #0;
       end;
end;

procedure TFordemInstalacao.edtPedidoProjetoKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8),',']) then
       begin
                Key:= #0;
       end;
end;

procedure TFordemInstalacao.lb8Click(Sender: TObject);
var
  FagendaInstalacao:TFAgendaInstalacao;
begin
      FagendaInstalacao:=TFAgendaInstalacao.Create(nil);

      try
        FagendaInstalacao.PassaDados(lbCodigo.Caption,edtdata.Text,edtColocador.Text);
        FagendaInstalacao.ShowModal;
      finally
          FreeAndNil(FagendaInstalacao);
      end;

end;

end.
