Unit UobjPedidoObjetos;

Interface
Uses UobjExportaContabilidade,UrelPedidoCompacto, Grids, Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,Uobjferragem_pp_pedido,
UObjPerfilado_PP_Pedido,UObjVidro_PP_Pedido,UObjKitBox_PP_Pedido,UObjServico_PP_PEDIDO,UObjPersiana_PP_PEDIDO,UObjDiverso_PP_PEDIDO,
QuickRpt, QRCtrls,     SysUtils, Messages,  Graphics, Controls,
ExtCtrls, Forms,uobjmedidas_proj_PEDIDo,UobjComissaoColocador,UobjComponente_pp_pedido,UobjVendas,UobjNotaFiscalObjetos
,UobjORDEMSERVICO,UobjSERVICOSOS,UobjMATERIAISOS,UrelControleEntrega,UMenuRelatorios,UAlteraCoresPProjeto,UobjTransmiteNFE;


Type
   TObjPedidoObjetos=class

          Public
                ObjMedidas_Proj     :TObjMedidas_proj_PEDIDO;

                ObjFerragem_PP      :TObjFERRAGEM_PP_Pedido;
                ObjPerfilado_PP     :TObjPerfilado_PP_Pedido;
                ObjVidro_PP         :TObjVidro_PP_Pedido;
                ObjKitBox_PP        :TObjKITBOX_PP_Pedido;
                ObjDiverso_PP       :TObjDIVERSO_PP_PEDIDO;
                ObjPersiana_PP      :TObjPERSIANA_PP_PEDIDO;
                ObjServico_PP       :TObjSERVICO_PP_PEDIDo;
                ObjComponente_PP    :TobjComponente_pp_pedido;
                ObjNotaFiscalObjetos:TObjNotafiscalObjetos;
                ObjOrdemServico     :TObjORDEMSERVICO;
                ObjServicoOS        :TObjSERVICOSOS;
                ObjMateriaisOS      :TObjMATERIAISOS;
                objtransmitenfe:TObjTransmiteNFE;

                Parametro_ImprimeAutorizacaoNF:Boolean;

                Constructor Create;
                Destructor  Free;
                Function    DiminuiEstoque(PpedidoProjetoRomaneio,PpedidoProjeto,Pdata:String):Boolean;
                Function    DiminuiEstoquePedido(Nota,Pedido,Pdata:String):Boolean;
                Function    AumentaEstoque(Nota,Pedido,Pdata:String):Boolean;

                Function    GravaRelacionamentos(PpedidoProjeto:string;Largura:string;Altura:string):Boolean;
                Function    AtualizavalorProjeto(PpedidoProjeto:string):boolean;overload;
                Function    AtualizavalorProjeto(PpedidoProjeto:string;Pcomcommit:boolean):boolean;overload;
                Function    ExcluiPedidoProjeto(PpedidoProjeto:string):boolean;
                function    ExcluiPedidoProjeto_sem_atualizar_valor_total(PpedidoProjeto: string): boolean;
                Function    RetornaValorFerragem(PpedidoProjeto,PcorFerragem:string):string;
                Function    RetornaValorPerfilado(PpedidoProjeto,PcorPerfilado:string):string;
                Function    RetornaValorVidro    (PpedidoProjeto,PcorVidro:string):string;
                Function    RetornaValorKitBox   (PpedidoProjeto,PcorKitBox:string):string;
                Function    RetornaValorServico  (PpedidoProjeto,PServico:string):string;
                Function    RetornaValorPersiana (PpedidoProjeto,PPersianaGrupoDiametroCor:string):string;
                Function    RetornaValorDiverso(PpedidoProjeto,PCorDiverso:string):string;
                Procedure   Imprime(PPedido : string);
                Function    AtualizaValorPedido(Ppedido:string):boolean;
                Function    AtualizaVidros(Var PStrGrid : TStringGrid; PPedido,PPedidoProjeto :string):Boolean;
                Procedure   RetornaMedidas(Var PMemo : TMemo; PPedidoProjeto:string;var edtlargura:string;var edtaltura:string);
                Procedure   opcoes(Var PTagFormOpcoes:Integer; PcodigoPedidoProjeto:string;LarguraProjeto:string;AlturaProjeto:string);
                Procedure   ReCalculaProjeto(PPedidoPRojeto:string;Largura:string;Altura:string);overload;
                function    ReCalculaProjeto(PPedidoPRojeto: string;ComCommit:boolean;Largura:string;Altura:string):Boolean;overload;
                Procedure   ImprimePedidoControleEntrega(PPEdido:string);
                Procedure   ImprimePedidoCompacto(Ppedido:string);
                Procedure   ImprimeAutorizacaoEmissaoNF(Ppedido:string);


                function    RetornaProjetoDoPedido(PPEdidoProjeto: string): string;
                Function    ApagaPedidoProjetoRepliocadosERecria(PPedidoProjetoPrincipal:string):Boolean;
                function    RetornaComissaoColocadorPedidoProjeto(PpedidoProjeto: string): Boolean;
                procedure   BotaoOpcoes(pcodigo: string);
                Function    GerarVenda(Pcodigo:string):boolean;
                Procedure   RetornarVenda(Pcodigo:string);

                //****CONTABILIDADE*******
                function    ExportaContabilidade_financeiro_venda(Pcodigo: string): Boolean;
                function    ExportaContabilidade_Estoque_Romaneio(PpedidoProjeto,PRomaneio,PdataEntregaRomaneio:string):Boolean;
                function    ExportaContabilidade_financeiro_Romaneio(PpedidoProjeto,Promaneio: string;PdataEntregaRomaneio:string): Boolean;
                function    ExportaContabilidade_TROCA(PPedido: string;PvalorDevolvido: Currency): Boolean;
                procedure   RefazContabilidade(PdataInicial, PdataFinal: string);
                procedure   ApagaContabilidadeNaoConcluidos(PdataInicial,PdataFinal: string);
                //*********************************


                
                Procedure   Relatorio_Movimentacao_Diaria;
                Procedure   Relatorio_Pedidos_nao_entraram_romaneio_e_nao_concluidos;
                procedure   Relatorio_Pedidos_nao_entregues;
                Procedure   Relatorio_pedidos_recebidos_entregues;

                Procedure   ImprimePedidoVidroLote(Ppedido:string);
                procedure   refaz_base_calculo_comissao_colocador;

                Procedure   ImprimeCustos(ppedido:string);

                Function    RetornaAreaMininaVidro (PpedidoProjeto, PcorVidro:string): string;

                function    GeraOrdemProducao(pedido:string):Boolean;
                function   ___GeraCodigoBarrasComponentes(PP_ORDEMPRODUCAO,COMPONENTES,PP,SERVICO:string;var ListaCodigoBarras:TStringList):Boolean;
                function    __Verifica_CodigoBarras(codigobarras,PP:string;ListaCodigoBarras:TStringList):Boolean;
                procedure   AlterarCorVidroProjetos(pedido:string);

                procedure   ImprimeListaPedidos(parametro:string);
                procedure   ImprimeVendaPorPeriodo(parametro:string);
                function    AtualizaComissaoTrocaMateriaisPedConcluido(Ppedido:string;ValorBonificacao:string):Boolean;
                function    VerficaEstoque(Pedido:string):Boolean;
                procedure   ReplicarProjetoPublico(PedidoProjeto:string;Quantidade:Integer);

                function TotalAcrescimoPedido(pPedido: String): Currency;


         Private
               Objquery:Tibquery;
               ObjqueryTEMP:Tibquery;
               TMPQRpedidoCompacto:TQRpedidoCompacto;
               ObjexportaContabilidade:TObjExportaContabilidade;

               procedure   ReplicaProjeto(PcodigoPedidoProjeto: string);overload;
               function    ReplicaProjeto(PcodigoPedidoProjeto :string; PQuantidade:Integer):Boolean;overload;

               procedure   ImprimeControleDeEntrega(Pedido:string);

               Procedure   ImprimePedido(Ppedido:string);
               Procedure   ImprimePedidoPersiana(PPedido:string);
               Procedure   ImprimePedidoProjetoBranco(PPedido:string);

               function  CalculaVidro(var PValorTotal: Currency): boolean;
               Function  PreencheVidrosDisponiveis(PProjetoemBranco:Boolean):boolean;
               procedure PreencheStrGridDemaisValores(var PStrGrid: TStringGrid;PColuna: Integer; PValor: Currency);
               procedure SomaLinhaStrGrid(PStrGrid: TStringGrid;PcolunaValorVidro:integer;Pvalorfinal:currency;PcolunaValorFinal:integer);

               procedure BandaDetailBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
               procedure BandaDetailBeforePrintPRojetoBranco(Sender: TQRCustomBand; var PrintBand: Boolean);
               procedure BandaDetailBeforePrintPedidoCompacto(Sender: TQRCustomBand; var PrintBand: Boolean);
               procedure BandaDetailBeforePrintPedidoControleEntrega(Sender: TQRCustomBand; var PrintBand: Boolean);
               procedure BandaDetailBeforePrintControleEntrega(Sender:TQRCustomBand; var PrinBand:Boolean);

               Function  ResgataDadosClienteEVendedor(PPedido:string):Boolean;
               Function  ResgataDadosClienteEVendedorPedidoControleEntrega(PPedido:string):Boolean;
               Function  ResgataDadosClienteEVendedorPedidoCompacto(PPedido:string):Boolean;
               Function  ResgataDadosClienteEVendedorPedidoPersiana(PPedido:string):Boolean;
               Function  ResgataDadosClienteEVendedorPedidoProjetoBranco(PPedido:string):Boolean;
               function  ResgataDadosClienteEVendedorControleEntrega(Ppedido:string):Boolean;

               Procedure PreencheCabecalhoPedido;
               Procedure PreencheCabecalhoPedidoCompacto;
               Procedure PreencheCabecalhoPedidoControleEntrega;
               Procedure PreencheCabecalhoPedidoPersiana;
               Procedure PreencheCabecalhoPedidoProjetoBranco;
               procedure PreencheCabecalhoControleEntrega;

               Function GravaMedidas(PpedidoProjeto:string;PProjetoemBranco:Boolean;Largura:string;Altura:string):Boolean;
               Function VerificaGrupoPerfilado(PPedidoProjeto:string; Var PNomeGrupoPerfilado:string):Boolean;

               //**************************************************************
               Function VerificaSeTemFerragem(PPedidoProjeto:string):Boolean;
               Function VerificaSeTemPerfilado(PPedidoProjeto:string):Boolean;
               Function VerificaSeTemServico(PPedidoProjeto:string):Boolean;
               Function VerificaSeTemKitBox(PPedidoProjeto:string):Boolean;
               Function VerificaSeTemPersiana(PPedidoProjeto:string):Boolean;
               Function VerificaSeTemDiversos(PPedidoProjeto:string):Boolean;
               Function VerificaSeTemVidro(PPedidoProjeto:string):Boolean;
               //***************************************************************
               Function VerificaSeTemProjetosNoPedido(PPedido:string):Boolean;

               function RetornaCoreskitBox(PPedidoProjeto: string): String;
               Function RetornaInformacoesVidroPedidoCompacto(Var PLinhaComplemento:string;PPedidoProjeto:string;Pquantidade:integer):Boolean;
               Function RetornaInformacoesVidroPedidoControleEntrega(Var PLinhaComplemento:string;PPedidoProjeto:string):Boolean;
               Function RetornaLarguraPedidoProjeto(Var PLinhaComplemento:string ;PPedidoProjeto:string):Boolean;

               Function PReencheInformacoesRelPersiana(PPedidoProjeto:string):Boolean;
               procedure ImprimePendencias_Pedido(Ptitulo: string;PMemo:TQRMemo);

               Procedure PreencheObservacaoPedido(PObservacao : String);
               Function ProjetoEmBrancoNoPedidoCompacto(PPedidoProjeto:string):Boolean;

               Function  ApagaProjetosReplicados(PPedidoProjetoPrincipal:string):Boolean;
               Function  MarcaLinhaVidroAoRecalcular(PPEdidoProjeto:string):Boolean;
               function  GeraComissaoPedido(Ppedido: string;var PComissaoVendedor: string): Boolean;
               function  RetornaComissaoVendedorPedido(Ppedido: string): Boolean;
               function  VerificaPPRomaneioporPedido(ppedido: string): boolean;
               function  RetornaComissaoColocadorPedido(Ppedido: string): Boolean;
               Procedure ImprimeVendaPeriodo_vendedor;
               function  RetornaValorComissaoPedido (Ppedido: string): Currency;
               Procedure ImprimeComissaoArquiteto;
               Function  ValidaDescontomaximo:boolean;
               procedure Imprime_quantidade_e_media_valor_produto;
               procedure EdtCodigoMaterialKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
               Procedure RecalculaPedido(pcodigo:string);
               Procedure ImprimeComponentesProjetos_do_pedido(ppedido:string);
               procedure Imprime_quantidade_entregue_e_media_valor_produto;

               procedure MostraComissoesColocador(ppedido:string);
               Procedure Imprime_Relatorio_materiais_por_pedido_e_cliente;
               procedure Imprime_Relatorio_materiais_por_pedido_e_cliente_sintetico;//Jonatan (PedidoVidrex)
               procedure StringGridKeyPress(Sender: TObject; var Key: Char);
               procedure StringGridDblClick(Sender: TObject);
               procedure StringGridKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
               Procedure CarregaParametros;
               procedure ApagaVendaDaTabvendas(pedido:string);
               procedure ApagaMateriasDaVenda(pedido:string);
               procedure CancelaNotaFiscal(pedido:string);
               procedure Replica_Pedido(Pedido:string);
               function EscolheKitPerfilado(codigovidro,pedidoprojeto,espessuravidro:string):Boolean;

   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,Dialogs,UDatamodulo, 
  UessencialLocal, UescolheVidro, UCalculaformula_Vidro, UobjFERRAGEM,
  UrelPedido, UobjCLIENTE, UobjPROJETO, UMedidas_proj,
  IBCustomDataSet, UrelPedidoPersiana,
  UrelPedidoControleEntrega, UrelPedidoProjetoBranco, UObjGeraTitulo,
  UObjLancamento, UObjPendencia, UobjFAIXADESCONTO_COMISSAO, UObjTitulo,
  UReltxtRDPRINT,rdprint, UobjPEDIDO_PEDIDO, DateUtils, UmostraStringGrid,
  UobjVIDRO, UobjPERSIANAGRUPODIAMETROCOR,
  UobjDIVERSOCOR, UMostraBarraProgresso, UobjPEDIDO_PROJ_PEDIDO,
  UobjVENDEDOR, 
  UobjCIDADE, URelPedidoRdPrint, UobjARQUITETO, UmostraStringList,
  UobjParametros, UobjRELPERSREPORTBUILDER, UmenuNfe,
  UobjLANCAMENTOCREDITO, Math;

constructor TObjPedidoObjetos.Create;
begin
  With Self do
  Begin
      //ObjPedidoProjeto    :=TobjPedido_Proj.Create;
      ObjMedidas_Proj     :=TObjMedidas_proj_PEDIDO.create;
      ObjFerragem_PP:=TObjFERRAGEM_PP_PEDIDO.Create;
      ObjPerfilado_PP:=TObjPerfilado_PP_Pedido.Create;
      ObjVidro_PP:=TObjVidro_PP_Pedido.Create;
      ObjKitBox_PP:=TObjKITBOX_PP_Pedido.Create;
      ObjServico_PP:=TObjSERVICO_PP_PEDIDO.Create;
      ObjPersiana_PP:=TObjPERSIANA_PP_PEDIDO.Create;
      ObjDiverso_PP:=TObjDIVERSO_PP_PEDIDO.Create;
      ObjComponente_PP    :=TobjComponente_pp_pedido.create;
      Objquery:=Tibquery.create(nil);
      Objquery.database:=FdataMOdulo.Ibdatabase;
      ObjqueryTEMP:=Tibquery.create(nil);
      ObjqueryTEMP.database:=FdataMOdulo.Ibdatabase;
      ObjexportaContabilidade:=TObjExportaContabilidade.create;
      ObjNotaFiscalObjetos:=TObjNotafiscalObjetos.Create;
      ObjOrdemServico:=TObjORDEMSERVICO.Create;
      ObjServicoOS:=TObjSERVICOSOS.Create;
      ObjMateriaisOS:=TObjMATERIAISOS.Create;

      objtransmitenfe := TObjTransmiteNFE.Create(nil,FDataModulo.IBDatabase);

      Self.carregaparametros;

      
  End;
end;

destructor TObjPedidoObjetos.Free;
begin
     With Self do
     Begin
        ObjMedidas_Proj .Free;
        ObjFerragem_PP  .Free;
        ObjPerfilado_PP .Free;
        ObjVidro_PP     .Free;
        ObjKitBox_PP    .Free;
        ObjServico_PP   .Free;
        ObjPersiana_PP  .Free;
        ObjDiverso_PP   .Free;
        ObjComponente_PP.Free;
        ObjMateriaisOS.Free;
        ObjServicoOS.Free;
        ObjexportaContabilidade.free;
        ObjNotaFiscalObjetos.Free;
        ObjOrdemServico.Free;
        Freeandnil(Self.Objquery);
        Freeandnil(Self.ObjqueryTEMP);
        FreeAndNil(self.objtransmitenfe);
     End;
end;


function TObjPedidoObjetos.GravaRelacionamentos(PpedidoProjeto:string;Largura:string;Altura:string): Boolean;
Var
  PqtdePerfilado,Pvalor,PvalorFinal:currency;
  cont:integer;
  palturalateral,plarguralateral,paltura,plargura:string;
  palturaarredondamento,plarguraarredondamento:string;
  pcodigobranco:string;
  PprojetoBRanco:Boolean;
  codigovidroPP:string;
  EspessuraVidro:string;
begin
     result:=False;
     
     pcodigobranco:=FDataModulo.CodigoProjetoBrancoGlobal;

Try
     PvalorFinal:=0;
                    
     Self.ObjMedidas_Proj.PedidoProjeto.ZerarTabela;
     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
     then begin
               Messagedlg('Escolha um Projeto para este pedido antes de calcular!',mterror,[mbok],0);
               exit;
     End;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Codigo=pcodigobranco)//� um projeto em branco
     Then PprojetoBRanco:=True
     Else PprojetoBRanco:=False;


     //De acordo com o eixo do Projeto Escolhido, tem q pedir as medidas
     if (Self.GravaMedidas(PpedidoProjeto,PprojetoBRanco,Largura,Altura)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Gravar as Medidas do Projeto escolhido!',mterror,[mbok],0);
               exit;

     End;

     if (ObjComponente_PP.Deleta_componente_pp(ppedidoprojeto)=False)
     then exit;

     //Venho Inserindo em Cada Tabela as Informacoes que estao ligadas ao projeto escolhido
     //No Final Somo Todas Essas Informa��es e Acrescento no PedidoProjeto
     //****************************************************

     FEscolheVidro.STRGGRID.ColCount:=14;

     for cont:=0 to FEscolheVidro.STRGGRID.ColCount-1 do
     Begin
           FEscolheVidro.STRGGRID.cols[cont].clear;
     End;
     FEscolheVidro.STRGGRID.rowcount:=2;

     FEscolheVidro.strggrid.Cells[0,0]:='CVC';
     FEscolheVidro.strggrid.Cells[1,0]:='REFER�NCIA';
     FEscolheVidro.strggrid.Cells[2,0]:='DESCRI��O';
     FEscolheVidro.strggrid.Cells[3,0]:='QTDE  VIDRO M�';
     FEscolheVidro.strggrid.Cells[4,0]:='VALOR VIDRO M�';
     FEscolheVidro.strggrid.Cells[5,0]:='VALOR TOTAL VIDRO';
     FEscolheVidro.strggrid.Cells[6,0]:='VALOR FERRAGENS';
     FEscolheVidro.strggrid.Cells[7,0]:='QTDE  PERFILADOS';
     FEscolheVidro.strggrid.Cells[8,0]:='VALOR PERFILADOS';
     FEscolheVidro.strggrid.Cells[9,0]:='VALOR SERVI�OS';
     FEscolheVidro.strggrid.Cells[10,0]:='VALOR KITBOX';
     FEscolheVidro.strggrid.Cells[11,0]:='VALOR PERSIANA';
     FEscolheVidro.strggrid.Cells[12,0]:='VALOR DIVERSOS';
     FEscolheVidro.strggrid.Cells[13,0]:='VALOR TOTAL';


     if (Self.PreencheVidrosDisponiveis(PprojetoBRanco)=False)
     Then exit;

     Pvalor:=0;

     if (Self.ObjFerragem_PP.PreencheFerragemPP(PpedidoProjeto,PValor,pprojetobranco)=false)
     then Begin
              MensagemErro('Erro ao tentar Preencher as Ferragens');
              exit;
     End;
     
     //volta na PValor o Valor que deu as ferragens
     PvalorFinal:=PValorFinal+Pvalor;
     Self.PreencheStrGridDemaisValores(FescolheVidro.STRGGRID,6,PValor);

       
     //****************************************************
     if (PprojetoBRanco=False)
     Then Begin
               //aqui
               Self.ObjMedidas_Proj.BuscaComponente(Self.ObjMedidas_Proj.PedidoProjeto.get_codigo,'FRONTAL',paltura,plargura,palturaarredondamento,plarguraarredondamento);
               Self.ObjMedidas_Proj.BuscaComponente(Self.ObjMedidas_Proj.PedidoProjeto.get_codigo,'LATERAL',palturalateral,plarguralateral,palturaarredondamento,plarguraarredondamento);
     End;

     Pvalor:=0;
     if (Self.ObjPerfilado_PP.PreenchePerfiladoPP(PpedidoProjeto,Pvalor,PqtdePerfilado,paltura,plargura,palturalateral,plarguralateral,PprojetoBRanco)=false)
     then Begin
             MensagemErro('Erro ao tentar Preencher os Perfilados');
             exit;
     end;
     PvalorFinal:=PValorFinal+Pvalor;
     Self.PreencheStrGridDemaisValores(FescolheVidro.STRGGRID,7,PQtdePerfilado);
     Self.PreencheStrGridDemaisValores(FescolheVidro.STRGGRID,8,PValor);


     //*************************************************
     pvalor:=0;
     if (Self.ObjServico_PP.PreencheServicoPP(PpedidoProjeto,PValor,PprojetoBRanco,paltura,plargura))=false
     then Begin
             MensagemErro('Erro ao tentar Preencher os Servi�os');
             exit;
     end;
     PvalorFinal:=PValorFinal+Pvalor;
     Self.PreencheStrGridDemaisValores(FescolheVidro.STRGGRID,9,PValor);
            
     //****************************************************
     PValor:=0;
     if (Self.ObjKitBox_PP.PreencheKitBoxPP(PpedidoProjeto,Pvalor,PprojetoBRanco))=false
     then Begin
             MensagemErro('Erro ao tentar Preencher os KitBox');
             exit;
     end;
     PvalorFinal:=PValorFinal+Pvalor;
     Self.PreencheStrGridDemaisValores(FescolheVidro.STRGGRID,10,PValor);

     // Persiana n�o est� cadastrados no projeto
     // portanto eu naum posso preencher e sim resgatar caso
     // tenho excolhido algum desses direto no pedido

     PValor:=0;
     if (Self.ObjPersiana_PP.ResgataValorFinal (PpedidoProjeto,Pvalor))=false
     then Begin
             MensagemErro('Erro ao tentar resgatar o valor final das Persianas');
             exit;
     end;
     PvalorFinal:=PValorFinal+Pvalor;
     Self.PreencheStrGridDemaisValores(FescolheVidro.STRGGRID,11,PValor);


     //****************************************************
     PValor:=0;
     if (Self.ObjDiverso_PP.PreencheDiversoPP(PpedidoProjeto,Pvalor,PprojetoBRanco))=false
     then Begin
             MensagemErro('Erro ao tentar Preencher os Diverso');
             exit;
     end;
     PvalorFinal:=PValorFinal+Pvalor;
     Self.PreencheStrGridDemaisValores(FescolheVidro.STRGGRID,12,PValor);

     //*********************************************************]

     //somando os valores para o StringGrid
     Self.SomaLinhaStrGrid(FescolheVidro.STRGGRID,5,Pvalorfinal,13);

     AjustaLArguraColunaGrid(FescolheVidro.STRGGRID);

     //chamo a tela com os vidros disponiveis, e no retorno gravo na tabvidro_pp
     //se for recalcular um projeto eu devo marcar a linha em que foi escolhido o vidro
     //na ultima vez que foi calculado
     Self.MarcaLinhaVidroAoRecalcular(PpedidoProjeto);

     if (PprojetoBRanco=False)//soh abre se nao for projeto em branco
     Then Begin
             FEscolheVidro.Showmodal;

             if (FEscolheVidro.Tag=0)
             Then exit;
     End;

     //gravando na Vidro_pp

     if (Self.ObjVidro_PP.DeletaVidro_PP(Self.ObjMedidas_Proj.PedidoProjeto.Get_Codigo)=False)
     Then exit;

     if (PprojetoBRanco=False)
     Then Begin
               Self.ObjVidro_PP.ZerarTabela;
               Self.ObjVidro_PP.Status:=dsInsert;
               Self.ObjVidro_PP.Submit_Codigo(Self.ObjVidro_PP.Get_NovoCodigo);
               Self.ObjVidro_PP.VidroCor.Submit_Codigo(FescolheVidro.STRGGRID.Cells[0,fescolhevidro.strggrid.row]);
               Self.ObjVidro_PP.Submit_PedidoProjeto(Self.ObjMedidas_Proj.PedidoProjeto.Get_Codigo);
               Self.ObjVidro_PP.Submit_Quantidade(tira_ponto(FescolheVidro.STRGGRID.Cells[3,fescolhevidro.strggrid.row]));
               Self.ObjVidro_PP.Submit_Valor(tira_ponto(FescolheVidro.STRGGRID.Cells[4,fescolhevidro.strggrid.row]));

               if (Self.ObjVidro_PP.Salvar(False)=False)
               Then Begin
                         Messagedlg('Erro na Tentativa de Salvar o Vidro no Projeto Atual',mterror,[mbok],0);
                         exit;
               End;
               
               //pegando o valor final do vidro
               codigovidroPP:=Self.ObjVidro_PP.Get_Codigo ;
               Self.ObjVidro_PP.LocalizaCodigo(Self.ObjVidro_PP.Get_Codigo);
               Self.ObjVidro_PP.TabelaparaObjeto;

               Try
                  strtofloat(Self.ObjVidro_PP.Get_ValorFinal);
                  Pvalorfinal:=PvalorFinal+strtofloat(Self.ObjVidro_PP.Get_ValorFinal);
               Except
               
               End;
     End;

     //se o projeto usar kitperfilado
     if(ObjMedidas_Proj.PedidoProjeto.Projeto.Get_UtilizaKitPerfilado='S') then
     begin
           EspessuraVidro:=Self.ObjVidro_PP.VidroCor.Vidro.Get_Espessura;
           if(EscolheKitPerfilado(codigovidroPP,PpedidoProjeto,EspessuraVidro)=False)
           then Exit;
     end;

     //atualiza o valor do projeto
     if (Self.AtualizavalorProjeto(PpedidoProjeto)=False)
     Then Begin
               Messagedlg('Erro na Tentativa de Gravar o Valor Total nesse Projeto',mterror,[mbok],0);
               exit;
     End;

     Result:=True;

Finally
       FDataModulo.IBTransaction.RollbackRetaining;
End;
end;

function TObjPedidoObjetos.EscolheKitPerfilado(codigovidro,pedidoprojeto,espessuravidro:string):Boolean;
var
  QueryAux:TIBQuery;
  Altura:currency;
  Largura:currency;
  codigoperfiladopp:string;
begin
     Result:=False;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModulo.IBDatabase;

     try
          with QueryAux do
          begin
                 Close;
                 SQL.Clear;
                 SQL.Add('select altura, largura from tabmedidas_proj where pedidoprojeto='+pedidoprojeto);
                 Open;
                 altura:=fieldbyname('altura').ascurrency;
                 largura:=fieldbyname('largura').ascurrency;

                 Close;
                 sql.Clear;
                 sql.Add('select tabperfilado_pp.*,tabperfilado.descricao, tabperfilado.altura,tabperfilado.espessuravidro');
                 sql.Add(',tabperfilado.largura from tabperfilado_pp');
                 sql.Add('join tabperfiladocor on tabperfiladocor.codigo=tabperfilado_pp.perfiladocor');
                 sql.Add('join tabperfilado on tabperfilado.codigo=tabperfiladocor.perfilado');
                 sql.Add('where pedidoprojeto='+pedidoprojeto);
                 sql.Add('and tabperfilado.kitperfilado=''S'' ');
                 sql.Add('order by tabperfilado.altura,tabperfilado.largura');
                 Open;
                 codigoperfiladopp:='';
                 while not eof do
                 begin
                      if(FieldByName('espessuravidro').AsString=espessuravidro) then
                      begin
                          if(Altura<=FieldByName('altura').ascurrency) and (Largura<=FieldByName('largura').ascurrency)then
                          begin
                               codigoperfiladopp:=fieldbyname('codigo').AsString;
                               Last;
                          end;
                      end;

                      Next;
                 end;
                 if(codigoperfiladopp='') then
                 begin
                       MensagemErro('N�o existe nenhum "KIT PERFILADO" que atende as medidas escolhidas no projeto!!! ');
                       Exit;
                       
                 end
                 else
                 begin
                      Close;
                      sql.Clear;
                      SQL.Add('delete from tabperfilado_pp where codigo<>'+codigoperfiladopp);
                      SQL.Add('and pedidoprojeto='+pedidoprojeto);
                      ExecSQL;
                 end;

          end;
          Result:=True;
     finally
          FreeAndNil(QueryAux);
     end;

end;


Function TObjPedidoObjetos.PreencheVidrosDisponiveis(PProjetoemBranco:Boolean):Boolean;
var
PQuantidadeMetroVidro:Currency;
begin
    Result:=False;

    if (PProjetoemBranco=True)
    Then Begin
              result:=True;
              exit;
    End;

    With Self.Objquery do
    Begin
         //Calculando quantos metros de vidros sera gasto no projeto atual
         PQuantidadeMetroVidro:=0;
                       
         if (Self.CalculaVidro(PQuantidadeMetroVidro)=False)
         Then exit;

         //passando para metro� e nao mm�
         //divido por 1 milhao porque vem em mm�
         //Exemplo
         //1.000mm x 1.000mm = 1.000.000 mm�
         //isso seria 1 m�

         Try
            PQuantidadeMetroVidro:=(PQuantidadeMetroVidro/1000000);
         Except
               PQuantidadeMetroVidro:=0;
         End;

         Close;
         SQL.Clear;
         SQL.Add('Select  TabVidro.descricao,TabVidroCor.Codigo as VidroCor,TabVidro.Referencia,');

        if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
        Then SQL.Add('(tabvidro.PrecoVendaInstalado+(Tabvidro.PrecoVendaInstalado*TabvidroCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
        Then SQL.Add('(tabvidro.PrecoVendaretirado+(Tabvidro.PrecoVendaretirado*TabvidroCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
        Then SQL.Add('(tabvidro.PrecoVendafornecido+(Tabvidro.PrecoVendafornecido*TabvidroCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        SQL.Add('from  Tabvidro');
        SQL.Add('Join  TabvidroCor on TabvidroCor.vidro = Tabvidro.codigo');
        SQL.Add('where TabvidroCor.Cor = '+Self.ObjMedidas_Proj.PedidoProjeto.Corvidro.Get_Codigo);
        SQL.Add('and   TabVidro.GrupoVidro='+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.GrupoVidro.Get_Codigo);
        SQL.Add('and tabvidro.ativo=''S'' ');
        Open;
        last;

        if (recordcount=0)
        Then Begin
                  MensagemErro('N�o existe vidros com a cor escolhida');
                  exit;
        End;
        first;

        While not (Eof) do
        Begin
             //montando na stringgrid os devido valores para cada vidro
             FescolheVidro.STRGGRID.Row:=FescolheVidro.STRGGRID.RowCount-1;
             FescolheVidro.STRGGRID.Cells[0,FescolheVidro.STRGGRID.Row]:=fieldbyname('vidrocor').asstring;
             FescolheVidro.STRGGRID.Cells[1,FescolheVidro.STRGGRID.Row]:=fieldbyname('Referencia').AsString;
             FescolheVidro.STRGGRID.Cells[2,FescolheVidro.STRGGRID.Row]:=fieldbyname('Descricao').AsString;
             FescolheVidro.STRGGRID.Cells[3,FescolheVidro.STRGGRID.Row]:=formata_valor(PQuantidadeMetroVidro);
             FescolheVidro.STRGGRID.Cells[4,FescolheVidro.STRGGRID.Row]:=formata_valor(fieldbyname('preco').AsString);
             FescolheVidro.STRGGRID.Cells[5,FescolheVidro.STRGGRID.Row]:=formata_valor(PQuantidadeMetroVidro*fieldbyname('preco').asfloat);

             FescolheVidro.STRGGRID.RowCount:=FescolheVidro.STRGGRID.RowCount+1;
             Next;
         end;

         FescolheVidro.STRGGRID.RowCount:=FescolheVidro.STRGGRID.RowCount-1;
    end;
    Result:=True;

end;

function TObjPedidoObjetos.CalculaVidro(var PValorTotal:Currency):boolean;
var
paltura,plargura:string;
palturaarredondamento,plarguraarredondamento:string;
palturaarredondamento2,plarguraarredondamento2:Currency;
somaalturaarredondamento,somalarguraarredondamento:Currency;
cont:integer;
Query:TIBQuery;
codigomedida:string;
begin
  result:=False;
  PvalorTotal:=0;

  // Zero a funcao recursiva
  Fcalculaformula_Vidro.QuantidadeExecucaoRecursiva:=0;
  Query:=TIBQuery.Create(nil);
  Query.Database:=FDataModulo.IBDatabase;

  try
      With Self.Objquery do
    Begin
        Fcalculaformula_vidro.BTLIMPARTUDOClick(nil);

        if (uppercase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo)='AXL')
        Then Begin
                  (*Apenas FRONTAL*)

                  if (Self.ObjMedidas_Proj.BuscaComponente(Self.ObjMedidas_Proj.PedidoProjeto.get_codigo,'FRONTAL',paltura,plargura,palturaarredondamento,plarguraarredondamento)=False)
                  Then begin
                            Messagedlg('N�o foi poss�vel resgatar as Medidas Frontais do Projeto',mterror,[mbok],0);
                            exit;
                  End;

                  Try
                     strtocurr(palturaarredondamento);
                  Except
                        palturaarredondamento:='0';
                  End;

                  Try
                     strtocurr(plarguraarredondamento);
                  Except
                        plarguraarredondamento:='0';
                  End;

                  Fcalculaformula_vidro.edtaltura.text:=currtostr(strtocurr(paltura)+strtocurr(palturaarredondamento));
                  Fcalculaformula_vidro.edtlargura.text:=currtostr(strtocurr(plargura)+strtocurr(plarguraarredondamento));
        End;

        if (uppercase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo)='AXL AXL')
        Then Begin
                  if (Self.ObjMedidas_Proj.BuscaComponente(Self.ObjMedidas_Proj.PedidoProjeto.get_codigo,'FRONTAL',paltura,plargura,palturaarredondamento,plarguraarredondamento)=False)
                  Then begin
                            Messagedlg('N�o foi poss�vel resgatar as Medidas Frontais do Projeto',mterror,[mbok],0);
                            exit;
                  End;

                  Try
                     strtocurr(palturaarredondamento);
                  Except
                        palturaarredondamento:='0';
                  End;

                  Try
                     strtocurr(plarguraarredondamento);
                  Except
                        plarguraarredondamento:='0';
                  End;

                  Fcalculaformula_vidro.edtaltura.text:=currtostr(strtocurr(paltura)+strtocurr(palturaarredondamento));
                  Fcalculaformula_vidro.edtlargura.text:=currtostr(strtocurr(plargura)+strtocurr(plarguraarredondamento));

                  if (Self.ObjMedidas_Proj.BuscaComponente(Self.ObjMedidas_Proj.PedidoProjeto.get_codigo,'LATERAL',paltura,plargura,palturaarredondamento,plarguraarredondamento)=False)
                  Then begin
                            Messagedlg('N�o foi poss�vel resgatar as Medidas Laterais do Projeto',mterror,[mbok],0);
                            exit;
                  End;

                  Try
                     strtocurr(palturaarredondamento);
                  Except
                        palturaarredondamento:='0';
                  End;

                  Try
                     strtocurr(plarguraarredondamento);
                  Except
                        plarguraarredondamento:='0';
                  End;

                  Fcalculaformula_vidro.edtalturalateral.text:=currtostr(strtocurr(paltura)+strtocurr(palturaarredondamento));
                  Fcalculaformula_vidro.edtlarguralateral.text:=currtostr(strtocurr(plargura)+strtocurr(plarguraarredondamento));

        End;

        //eixo VARIOS
        if (uppercase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo)='VARIOS')
        Then Begin
                  //nesse caso, as formulas nao sao calculadas,
                  //cada componente � digitado manuialmente a largura e altura
                  //por isso apenas coloco no FORM e ja coloco o resultado
                  close;
                  sql.clear;
                  sql.Add('Select * from TabMedidas_proj where pedidoProjeto='+Self.ObjMedidas_Proj.PedidoProjeto.Get_Codigo);
                  open;
                  first;
                  While not (eof) do
                  Begin
                      Fcalculaformula_vidro.edtvariavel.Text:=fieldbyname('componente').asstring;
                      Fcalculaformula_vidro.edtformulaaltura.TEXT:=floattostr(fieldbyname('altura').asfloat+fieldbyname('alturaarredondamento').asfloat);
                      Fcalculaformula_vidro.edtformulalargura.text:=floattostr(fieldbyname('largura').asfloat+fieldbyname('larguraarredondamento').asfloat);
                      Try
                          Fcalculaformula_vidro.btinsereformulaClick(nil);
                      Except
                            Messagedlg('Erro na tentativa de Inserir f�rmulas aos calculos!',mterror,[mbok],0);
                            exit;
                      End;
                      Next;
                  end;
        end
        Else Begin
                  //em caso de eixos "AXL" e "AXL AXL"
                  //precisa percorrer os componentes colocado no projeto
                  //e suas formulas, e ir calculando

                  //Para cada projeto que tem no pedido eu calculo e lagura e a Altura
                  close;
                  SQl.Clear;
                  Sql.Add('Select TabComponente.codigo as codigocomponente,TabComponente.Referencia, FormulaAltura, FormulaLargura from TabComponente_Proj') ;
                  SQL.Add('join Tabcomponente on TabComponente.Codigo = TabComponente_PRoj.Componente');
                  SQl.Add('Where TabComponente_Proj.Projeto  ='+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Codigo);
                  Open;

                  //Para cada Componente econtrado e preencho o FCalclulaFormula
                  While not (eof) do
                  Begin
                      Fcalculaformula_vidro.edtvariavel.Text:=fieldbyname('Referencia').asstring;
                      Fcalculaformula_vidro.edtformulaaltura.TEXT:=fieldbyname('formulaaltura').asstring;
                      Fcalculaformula_vidro.edtformulalargura.text:=fieldbyname('formulalargura').asstring;
                      Try
                          Fcalculaformula_vidro.btinsereformulaClick(nil);
                      Except
                            Messagedlg('Erro na tentativa de Inserir f�rmulas aos calculos!',mterror,[mbok],0);
                            exit;
                      End;
                      Next;
                  end;
                  //agora calculo linha a linha
                  if (Fcalculaformula_vidro.Calcula_Area=False)
                  Then Begin
                            Messagedlg('Erro no C�lculo da F�rmula do Vidro!',mterror,[mbok],0);
                            exit;
                  End;

        End;
        //Fcalculaformula_Vidro.ShowModal;

        if (ObjComponente_PP.Deleta_Componente_PP(Self.ObjMedidas_Proj.PedidoProjeto.get_codigo)=False)
        Then exit;



        for cont:=0 to Fcalculaformula_vidro.lbvariavel.Items.count-1 do
        Begin
             ObjComponente_PP.ZerarTabela;
             ObjComponente_PP.Status:=dsinsert;
             if (ObjComponente_PP.COMPONENTE.LocalizaReferencia(Fcalculaformula_vidro.lbvariavel.Items[cont])=false)
             Then Begin
                       mensagemerro('Componente n�o encontrado '+Fcalculaformula_vidro.lbvariavel.Items[cont]);
                       exit;
             End;
             ObjComponente_PP.COMPONENTE.TabelaparaObjeto;
             ObjComponente_PP.Submit_CODIGO('0');
             ObjComponente_PP.Submit_LARGURA(Fcalculaformula_Vidro.lblargura.items[cont]);
             ObjComponente_PP.Submit_altura(Fcalculaformula_Vidro.lbaltura.items[cont]);
             ObjComponente_PP.Submit_pedidoprojeto(Self.ObjMedidas_Proj.PedidoProjeto.get_codigo);



             if (ObjComponente_PP.Salvar(False)=False)
             then Begin
                       Mensagemerro('Erro na tentativa de Salvar o Componente '+ObjComponente_PP.COMPONENTE.get_referencia+' na tabela de componente_pp');
                       exit;
             End;
        End;

        //A pedido da Vidra�aria Central Vidros
        //Respons�vel Gilson
        //Jonatan Medina
        //A ideia � a de arredondar 5 CM em cada componente, pq na tempera existe a cobran�a das
        //sobras de 5 cm a 5 cm, ou seja, se um componente a ser cortado deu 322 mm ou seja 32,2 cm
        //a cobran�a ser� em cima de 35 cm, ent�o � feito arredondamento pra calcular a quantidade do
        //vidro apenas, as medidas dos componentes devem continuar as mesma
        {
            Exemplo:
            Projeto Axl Axl
            Altura 1500 ; Largura 1700
            Componentes
            P1 A por L/4
            P2 A por L/4
            P3 A por L/4
            P4 A por L/4

            Valores sem arredondar
            P1 A=1500; Largura=425
            P2 A=1500; Largura=425
            P3 A=1500; Largura=425
            P4 A=1500; Largura=425

            Quantidade de Vidro =2,7 m�

            Valores arredondados
            P1 A=1500; Largura=425   (calcula a largura arredondada para 450)
            P2 A=1500; Largura=425   (calcula a largura arredondada para 450)
            P3 A=1500; Largura=425   (calcula a largura arredondada para 450)
            P4 A=1500; Largura=425   (calcula a largura arredondada para 450)

            Quantidade de Vidro =2,85 m�

        }



        if (ObjParametroGlobal.ValidaParametro('ARREDONDAMENTO 5 CM POR COMPONENTES')=false)then
        Begin
             MensagemErro('O par�metro "ARREDONDAMENTO 5 CM POR COMPONENTES" n�o foi encontrado.');
             exit;
        end;

        if(ObjParametroGlobal.Get_Valor='SIM') then
        begin
            Query.Close;
            Query.SQL.Clear;
            Query.SQL.Add('select ped.codigo,cpp.altura,cpp.largura,cpp.componente,mpp.codigo as codmedida');
            Query.SQL.Add('from tabpedido ped');
            Query.SQL.Add('join tabpedido_proj pp on pp.pedido=ped.codigo');
            Query.SQL.Add('join tabcomponente_pp cpp on cpp.pedidoprojeto=pp.codigo');
            Query.SQL.Add('join tabmedidas_proj mpp on mpp.pedidoprojeto=pp.codigo');
            query.SQL.Add('where pp.codigo='+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo);
            Query.Open;

            somaalturaarredondamento:=0;
            somalarguraarredondamento :=0;

            while not Query.Eof do
            begin
               codigomedida:=Query.fieldbyname('codmedida').AsString;
               palturaarredondamento:='0';
               plarguraarredondamento:='0';
               FDataModulo.Arredonda5Cm(Query.fieldbyname('altura').AsCurrency,palturaarredondamento2);
               FDataModulo.Arredonda5Cm(Query.fieldbyname('largura').AsCurrency,plarguraarredondamento2);
               somaalturaarredondamento:=somaalturaarredondamento+palturaarredondamento2;
               somalarguraarredondamento :=somalarguraarredondamento+plarguraarredondamento2;

               Query.Next;
            end;

            palturaarredondamento2:=0;
            plarguraarredondamento2:=0;

            //Caso o valor j� tenha sido arredondado, pego o valor anterior pra somar com a quantidade que
            //devo arredondar por componente.
            Query.Close;
            Query.SQL.Clear;
            Query.SQL.Add('select alturaarredondamento from tabmedidas_proj where codigo='+codigomedida);
            Query.Open;
            palturaarredondamento2:=Query.fieldbyname('alturaarredondamento').AsCurrency;

            Query.Close;
            Query.SQL.Clear;
            Query.SQL.Add('select larguraarredondamento from tabmedidas_proj where codigo='+codigomedida);
            Query.Open;
            plarguraarredondamento2:=Query.fieldbyname('larguraarredondamento').AsCurrency;

            somaalturaarredondamento:=somaalturaarredondamento+palturaarredondamento2;
            somalarguraarredondamento:=somalarguraarredondamento+plarguraarredondamento2;

            Query.Close;
            Query.SQL.Clear;
            Query.SQL.Add('update tabmedidas_proj set alturaarredondamento='+virgulaparaponto(CurrToStr(somaalturaarredondamento))+' where codigo='+codigomedida);
            Query.ExecSQL;

            Query.Close;
            Query.SQL.Clear;
            Query.SQL.Add('update tabmedidas_proj set larguraarredondamento='+virgulaparaponto(CurrToStr(somalarguraarredondamento))+' where codigo='+codigomedida);
            Query.ExecSQL;

            //recalculando os valores
             Fcalculaformula_vidro.BTLIMPARTUDOClick(nil);

            if (uppercase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo)='AXL')
            Then Begin
                      (*Apenas FRONTAL*)

                      if (Self.ObjMedidas_Proj.BuscaComponente(Self.ObjMedidas_Proj.PedidoProjeto.get_codigo,'FRONTAL',paltura,plargura,palturaarredondamento,plarguraarredondamento)=False)
                      Then begin
                                Messagedlg('N�o foi poss�vel resgatar as Medidas Frontais do Projeto',mterror,[mbok],0);
                                exit;
                      End;

                      Try
                         strtocurr(palturaarredondamento);
                      Except
                            palturaarredondamento:='0';
                      End;

                      Try
                         strtocurr(plarguraarredondamento);
                      Except
                            plarguraarredondamento:='0';
                      End;

                      Fcalculaformula_vidro.edtaltura.text:=currtostr(strtocurr(paltura)+strtocurr(palturaarredondamento));
                      Fcalculaformula_vidro.edtlargura.text:=currtostr(strtocurr(plargura)+strtocurr(plarguraarredondamento));
            End;

            if (uppercase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo)='AXL AXL')
            Then Begin
                      if (Self.ObjMedidas_Proj.BuscaComponente(Self.ObjMedidas_Proj.PedidoProjeto.get_codigo,'FRONTAL',paltura,plargura,palturaarredondamento,plarguraarredondamento)=False)
                      Then begin
                                Messagedlg('N�o foi poss�vel resgatar as Medidas Frontais do Projeto',mterror,[mbok],0);
                                exit;
                      End;

                      Try
                         strtocurr(palturaarredondamento);
                      Except
                            palturaarredondamento:='0';
                      End;

                      Try
                         strtocurr(plarguraarredondamento);
                      Except
                            plarguraarredondamento:='0';
                      End;

                      Fcalculaformula_vidro.edtaltura.text:=currtostr(strtocurr(paltura)+strtocurr(palturaarredondamento));
                      Fcalculaformula_vidro.edtlargura.text:=currtostr(strtocurr(plargura)+strtocurr(plarguraarredondamento));

                      if (Self.ObjMedidas_Proj.BuscaComponente(Self.ObjMedidas_Proj.PedidoProjeto.get_codigo,'LATERAL',paltura,plargura,palturaarredondamento,plarguraarredondamento)=False)
                      Then begin
                                Messagedlg('N�o foi poss�vel resgatar as Medidas Laterais do Projeto',mterror,[mbok],0);
                                exit;
                      End;

                      Try
                         strtocurr(palturaarredondamento);
                      Except
                            palturaarredondamento:='0';
                      End;

                      Try
                         strtocurr(plarguraarredondamento);
                      Except
                            plarguraarredondamento:='0';
                      End;

                      Fcalculaformula_vidro.edtalturalateral.text:=currtostr(strtocurr(paltura)+strtocurr(palturaarredondamento));
                      Fcalculaformula_vidro.edtlarguralateral.text:=currtostr(strtocurr(plargura)+strtocurr(plarguraarredondamento));

            End;

            //eixo VARIOS
            if (uppercase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo)='VARIOS')
            Then Begin
                      //nesse caso, as formulas nao sao calculadas,
                      //cada componente � digitado manuialmente a largura e altura
                      //por isso apenas coloco no FORM e ja coloco o resultado
                      close;
                      sql.clear;
                      sql.Add('Select * from TabMedidas_proj where pedidoProjeto='+Self.ObjMedidas_Proj.PedidoProjeto.Get_Codigo);
                      open;
                      first;
                      While not (eof) do
                      Begin
                          Fcalculaformula_vidro.edtvariavel.Text:=fieldbyname('componente').asstring;
                          Fcalculaformula_vidro.edtformulaaltura.TEXT:=floattostr(fieldbyname('altura').asfloat+fieldbyname('alturaarredondamento').asfloat);
                          Fcalculaformula_vidro.edtformulalargura.text:=floattostr(fieldbyname('largura').asfloat+fieldbyname('larguraarredondamento').asfloat);
                          Try
                              Fcalculaformula_vidro.btinsereformulaClick(nil);
                          Except
                                Messagedlg('Erro na tentativa de Inserir f�rmulas aos calculos!',mterror,[mbok],0);
                                exit;
                          End;
                          Next;
                      end;
            end
            Else Begin
                      //em caso de eixos "AXL" e "AXL AXL"
                      //precisa percorrer os componentes colocado no projeto
                      //e suas formulas, e ir calculando

                      //Para cada projeto que tem no pedido eu calculo e lagura e a Altura
                      close;
                      SQl.Clear;
                      Sql.Add('Select TabComponente.codigo as codigocomponente,TabComponente.Referencia, FormulaAltura, FormulaLargura from TabComponente_Proj') ;
                      SQL.Add('join Tabcomponente on TabComponente.Codigo = TabComponente_PRoj.Componente');
                      SQl.Add('Where TabComponente_Proj.Projeto  ='+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Codigo);
                      Open;

                      //Para cada Componente econtrado e preencho o FCalclulaFormula
                      While not (eof) do
                      Begin
                          Fcalculaformula_vidro.edtvariavel.Text:=fieldbyname('Referencia').asstring;
                          Fcalculaformula_vidro.edtformulaaltura.TEXT:=fieldbyname('formulaaltura').asstring;
                          Fcalculaformula_vidro.edtformulalargura.text:=fieldbyname('formulalargura').asstring;
                          Try
                              Fcalculaformula_vidro.btinsereformulaClick(nil);
                          Except
                                Messagedlg('Erro na tentativa de Inserir f�rmulas aos calculos!',mterror,[mbok],0);
                                exit;
                          End;
                          Next;
                      end;
                      //agora calculo linha a linha
                      if (Fcalculaformula_vidro.Calcula_Area=False)
                      Then Begin
                                Messagedlg('Erro no C�lculo da F�rmula do Vidro!',mterror,[mbok],0);
                                exit;
                      End;

            End;
            //Fcalculaformula_Vidro.ShowModal;

           { if (ObjComponente_PP.Deleta_Componente_PP(Self.ObjMedidas_Proj.PedidoProjeto.get_codigo)=False)
            Then exit;



            for cont:=0 to Fcalculaformula_vidro.lbvariavel.Items.count-1 do
            Begin
                 ObjComponente_PP.ZerarTabela;
                 ObjComponente_PP.Status:=dsinsert;
                 if (ObjComponente_PP.COMPONENTE.LocalizaReferencia(Fcalculaformula_vidro.lbvariavel.Items[cont])=false)
                 Then Begin
                           mensagemerro('Componente n�o encontrado '+Fcalculaformula_vidro.lbvariavel.Items[cont]);
                           exit;
                 End;
                 ObjComponente_PP.COMPONENTE.TabelaparaObjeto;
                 ObjComponente_PP.Submit_CODIGO('0');
                 ObjComponente_PP.Submit_LARGURA(Fcalculaformula_Vidro.lblargura.items[cont]);
                 ObjComponente_PP.Submit_altura(Fcalculaformula_Vidro.lbaltura.items[cont]);
                 ObjComponente_PP.Submit_pedidoprojeto(Self.ObjMedidas_Proj.PedidoProjeto.get_codigo);



                 if (ObjComponente_PP.Salvar(False)=False)
                 then Begin
                           Mensagemerro('Erro na tentativa de Salvar o Componente '+ObjComponente_PP.COMPONENTE.get_referencia+' na tabela de componente_pp');
                           exit;
                 End;
            End;}

        end;


        //pegando a soma total (multiplica nas linhas largura por altura e no final somo tudo)

        if (Fcalculaformula_vidro.ResgataValorMetroQuadrado(PValorTotal)=False)
        then exit;

        //Fcalculaformula_vidro.showmodal;

    end; // With

    Result:=True;
  finally
       FreeAndNil(Query);
  end;

end;




procedure TObjPEDIDOObjetos.PreencheStrGridDemaisValores(var PStrGrid: TStringGrid;PColuna: Integer; PValor: Currency);
Var Cont : Integer;
begin
    for Cont:=1 to PStrGrid.RowCount-1 do
    Begin
        PStrGrid.Cells[PColuna, Cont]:=formata_valor(PValor);
    end;                            
end;

procedure TObjPedidoObjetos.SomaLinhaStrGrid(PStrGrid: TStringGrid;PcolunaValorVidro:integer;Pvalorfinal:currency;PcolunaValorFinal:integer);
Var
Linha, Coluna : Integer;
PValorTotal:Currency;
begin
     for  Linha:=1  to PStrGrid.RowCount-1 do
     Begin
         Try
            //pegando o valor total do vidro no grid
            pvalortotal:=0;
            PValorTotal:=Strtofloat(tira_ponto(PStrGrid.Cells[pcolunavalorVidro,linha]));
         Except
         End;
         //somando o vidro e o total dos outros para por numa coluna
         PStrGrid.Cells[PcolunaValorFinal,linha]:=formata_valor(PValorTotal+Pvalorfinal);
     end;
end;


function TObjPedidoObjetos.AtualizavalorProjeto(PpedidoProjeto: string): boolean;
Begin
     result:=self.AtualizavalorProjeto(PpedidoProjeto,true);
End;

function TObjPedidoObjetos.AtualizavalorProjeto(PpedidoProjeto: string;Pcomcommit:boolean): boolean;
var
Pvalor:Currency;
Ppedido:string;
begin          
     result:=False;

     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
     Then begin
               MensagemErro('Projeto n�o localizado para ser exclu�do');
               exit;
     End;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
     Ppedido:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Codigo;

     Try
       pvalor:=0;                     
       Pvalor:=pvalor+Self.ObjFerragem_PP .Soma_por_PP(PpedidoProjeto);
       Pvalor:=pvalor+Self.ObjPerfilado_PP.Soma_por_PP(PpedidoProjeto);
       Pvalor:=pvalor+Self.ObjVidro_PP    .Soma_por_PP(PpedidoProjeto);
       Pvalor:=pvalor+Self.ObjKitBox_PP   .Soma_por_PP(PpedidoProjeto);
       Pvalor:=pvalor+Self.ObjServico_PP  .Soma_por_PP(PpedidoProjeto);
       Pvalor:=pvalor+Self.ObjPersiana_PP .Soma_por_PP(PpedidoProjeto);
       Pvalor:=pvalor+Self.ObjDiverso_PP  .Soma_por_PP(PpedidoProjeto);

        //apos gravar em todas as tabelas, preciso acertar o valor total
       //na tabela de pedidoprojeto, que seria a soma de todas as tabelas
       Self.ObjMedidas_Proj.PedidoProjeto.ZerarTabela;
       Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(ppedidoprojeto);
       Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
       Self.ObjMedidas_Proj.PedidoProjeto.Submit_ValorTotal(floattostr(Pvalor));
       Self.ObjMedidas_Proj.PedidoProjeto.status:=dsedit;
       //passo com True pois � a ultima coisa
       if (Self.ObjMedidas_Proj.PedidoProjeto.Salvar(False)=False)
       Then Begin
                 Messagedlg('Erro na Tentativa de Gravar o Valor Total nesse Projeto',mterror,[mbok],0);
                 exit;
       End;

       if (Self.atualizavalorpedido(ppedido)=False)
       Then exit;

       if (PComCommit=true)
       then FDataModulo.IBTransaction.CommitRetaining;

     Finally
       if (PComCommit=true)
       then FDataModulo.IBTransaction.RollbackRetaining;
     End;

     result:=True;
end;


function TObjPedidoObjetos.ExcluiPedidoProjeto(PpedidoProjeto: string): boolean;
var
Ppedido:string;
begin
     result:=False;
     
     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
     Then begin
               MensagemErro('Projeto n�o localizado para ser exclu�do');
               exit;
     End;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
     Ppedido:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Codigo;

     Try
        // Antes de excluir o projeto eu preciso excluir todos os seus relacionamentos
        if (Self.ObjFerragem_PP.DeletaFerragem_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar as ferragens desse Pedido.');
           exit;
        end;

        if (Self.ObjPerfilado_PP.DeletaPerfilado_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar os perfilados desse Pedido.');
           exit;
        end;

        if (Self.ObjVidro_PP.DeletaVidro_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar os Vidros desse Pedido.');
           exit;
        end;

        if (Self.ObjKitBox_PP.DeletaKitBox_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar os KitBox desse Pedido.');
           exit;
        end;

        if (Self.ObjServico_PP.DeletaServico_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar os Servicos desse Pedido.');
           exit;
        end;

        if (Self.ObjPersiana_PP.DeletaPersiana_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar os Persiana desse Pedido.');
           exit;
        end;

        if (Self.ObjDiverso_PP.DeletaDiverso_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar os Diversos desse Pedido.');
           exit;
        end;

        if (Self.ObjMedidas_proj.exclui_PP(PpedidoProjeto)=False)
        Then begin
                  MensagemErro('Erro na tentativa de Excluir as medidas do Projeto do Pedido');
                  exit;
        End;

        if (Self.ObjComponente_PP.Deleta_Componente_PP(PpedidoProjeto)=False)
        Then begin
                  MensagemErro('Erro na tentativa de Excluir os componentes do Projeto do Pedido');
                  exit;
        End;

        if (Self.ObjMedidas_Proj.PedidoProjeto.Exclui(PpedidoProjeto,false)=False)
        Then begin
                  MensagemErro('Erro na tentativa de Excluir o Projeto do Pedido');
                  exit;
                  //aqui pedido 3932
        End;


        if (Self.atualizavalorpedido(ppedido)=False)
        Then exit;

        FdataModulo.Ibtransaction.commitretaining;
        result:=true;
     Finally
            FdataModulo.Ibtransaction.rollbackretaining;
     End;
end;

function TObjPedidoObjetos.ExcluiPedidoProjeto_sem_atualizar_valor_total(PpedidoProjeto: string): boolean;
var
   Ppedido:string;
begin
     //Usado em troca, que atualiza s� o valor total
     //na conclusao

     result:=False;
     
     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
     Then begin
               MensagemErro('Projeto n�o localizado para ser exclu�do');
               exit;
     End;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
     Ppedido:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Codigo;

     Try
        // Antes de excluir o projeto eu preciso excluir todos os seus relacionamentos
        if (Self.ObjFerragem_PP.DeletaFerragem_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar as ferragens desse Pedido.');
           exit;
        end;

        if (Self.ObjPerfilado_PP.DeletaPerfilado_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar os perfilados desse Pedido.');
           exit;
        end;

        if (Self.ObjVidro_PP.DeletaVidro_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar os Vidros desse Pedido.');
           exit;
        end;

        if (Self.ObjKitBox_PP.DeletaKitBox_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar os KitBox desse Pedido.');
           exit;
        end;

        if (Self.ObjServico_PP.DeletaServico_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar os Servicos desse Pedido.');
           exit;
        end;

        if (Self.ObjPersiana_PP.DeletaPersiana_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar os Persiana desse Pedido.');
           exit;
        end;

        if (Self.ObjDiverso_PP.DeletaDiverso_PP(PpedidoProjeto)=false)then
        Begin
           MensagemErro('Erro ao tentar deletar os Diversos desse Pedido.');
           exit;
        end;




        if (Self.ObjMedidas_proj.exclui_PP(PpedidoProjeto)=False)
        Then begin
                  MensagemErro('Erro na tentativa de Excluir as medidas do Projeto do Pedido');
                  exit;
        End;

        if (Self.ObjComponente_PP.Deleta_Componente_PP(PpedidoProjeto)=False)
        Then begin
                  MensagemErro('Erro na tentativa de Excluir os componentes do Projeto do Pedido');
                  exit;
        End;


        if (Self.ObjMedidas_Proj.PedidoProjeto.Exclui(PpedidoProjeto,false)=False)
        Then begin
                  MensagemErro('Erro na tentativa de Excluir o Projeto do Pedido');
                  exit;
        End;
        result:=true;
        
     Finally
     End;
end;


function TObjPedidoObjetos.RetornaValorFerragem(PpedidoProjeto, PcorFerragem: string): string;
begin
     Result:='';

     if (Ppedidoprojeto='') or (Pcorferragem='')
     Then exit;

     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
     Then exit;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (self.ObjFerragem_PP.FerragemCor.LocalizaCodigo(PcorFerragem)=False)
     Then exit;
     Self.ObjFerragem_PP.FerragemCor.TabelaparaObjeto;

     Try
         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
         Then Result:=floattostr(strtofloat(Self.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendaInstalado)+((strtofloat(Self.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendaInstalado)*strtofloat(Self.ObjFerragem_PP.FerragemCor.get_PorcentagemAcrescimoFinal))/100));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
         Then Result:=floattostr(strtofloat(Self.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendaretirado)+((strtofloat(Self.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendaretirado)*strtofloat(Self.ObjFerragem_PP.FerragemCor.get_PorcentagemAcrescimoFinal))/100));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
         Then Result:=floattostr(strtofloat(Self.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendafornecido)+((strtofloat(Self.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendafornecido)*strtofloat(Self.ObjFerragem_PP.FerragemCor.get_PorcentagemAcrescimoFinal))/100));
    Except
          Result:='';
    End;
end;

procedure TObjPedidoObjetos.ImprimeControleDeEntrega(Pedido:string);
begin
     if (Pedido='')
     Then Begin
               Messagedlg('Escolha o Pedido que deseja imprimir',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.VerificaSeTemProjetosNoPedido(Pedido)=false)then
     Begin
          MensagemErro('Nenhum projeto cadastrado neste pedido');
          exit;
     end;

     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(Pedido)=false)then
     Begin
          MensagemErro('Pedido n�o encontrado.');
          exit;
     end;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     QrControleEntrega.imgLogotipo.Stretch:=False;
     if (ObjParametroGlobal.ValidaParametro('STRETCH NO LOGOTIPO DO PEDIDO')=True)
     then Begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               Then QrControleEntrega.imgLogotipo.Stretch:=true;
     End;

      With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select * from ProcPedidoProjetoQtde('+Pedido+')');
          open;
          QrControleEntrega.DataSet:=Self.Objquery;


          Self.PreencheCabecalhoControleEntrega;
          Self.ResgataDadosClienteEVendedorControleEntrega(Pedido);

          QrControleEntrega.lBDataEntrega.Caption:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_DataEntrega;
          QrControleEntrega.lBdata.Caption:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data;
          QrControleEntrega.qrmObservacao.Lines.Text:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Observacao;

          //********************************************************************************

          QrControleEntrega.LBProposta.Caption:='PROPOSTA N� '+Pedido;
          QrControleEntrega.lbTitulo.Caption  :='T�TULO   N� '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Titulo;
          QrControleEntrega.BandaDetail.BeforePrint:=BandaDetailBeforePrintControleEntrega;


          QrControleEntrega.Preview;
     End;



end;

procedure TObjPedidoObjetos.ImprimePedido(Ppedido: string);
begin
     if (Ppedido='')
     Then Begin
               Messagedlg('Escolha o Pedido que deseja imprimir',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.VerificaSeTemProjetosNoPedido(Ppedido)=false)then
     Begin
          MensagemErro('Nenhum projeto cadastrado neste pedido');
          exit;
     end;

     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(Ppedido)=false)then
     Begin
          MensagemErro('Pedido n�o encontrado.');
          exit;
     end;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     QRpedido.QRImageLogotipo.Stretch:=False;
     if (ObjParametroGlobal.ValidaParametro('STRETCH NO LOGOTIPO DO PEDIDO')=True)
     then Begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               Then QRpedido.QRImageLogotipo.Stretch:=true;
     End;


     With Self.Objquery do
     Begin
          close;
          sql.clear;
         { sql.add('Select Distinct(tabpedido_proj.codigo), TabMedidas_Proj.Largura, TabMedidas_Proj.Altura');
          sql.add('from tabpedido_proj');
          sql.add('left Join TabMedidas_Proj on TabMedidas_Proj.PedidoProjeto = TabPedido_proj.Codigo');
          sql.add('where TabPedido_proj.pedido = '+Ppedido); }
          sql.add('Select * from ProcPedidoProjetoQtde('+Ppedido+')');
          open;
          QRpedido.DataSet:=Self.Objquery;


          Self.PreencheCabecalhoPedido;
          Self.ResgataDadosClienteEVendedor(Ppedido);
          Self.ImprimePendencias_Pedido(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Titulo,QRpedido.QrMemoPendencias);
          //********************************************************************************

          QRpedido.LBProposta.Caption:='PROPOSTA N� '+Ppedido;
          QRpedido.lbTitulo.Caption  :='T�TULO   N� '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Titulo;
          Qrpedido.BandaDetail.BeforePrint:=Self.BandaDetailBeforePrint;

         // Atualizando os Valores Totais
          Self.ObjMedidas_Proj.PedidoProjeto.ZerarTabela;
          Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(Fieldbyname('Codigo').AsString);
          Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

          QRpedido.LbValorTotal.Caption:=    'Valor Total Pedido '+CompletaPalavra_a_Esquerda(formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorTotal), 15, ' ');
          QRpedido.LBValorAcrescimo.Caption:='Valor Acr�scimo    '+CompletaPalavra_a_Esquerda(formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorAcrescimo),15,' ');
          QRpedido.LBValorDesconto.Caption:= 'Valor Desconto     '+CompletaPalavra_a_Esquerda(formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorDesconto),15,' ');
          QRpedido.LBValorFinal.Caption:=    'Valor Final        '+CompletaPalavra_a_Esquerda(formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorFinal),15,' ');


          Qrpedido.Preview;
     End;

end;

procedure TObjPedidoObjetos.BandaDetailBeforePrintControleEntrega(Sender:TQRCustomBand; var PrinBand:Boolean);
var
PPedidoPRojeto:string;
query:TIBQuery;
begin

     //esse procedimento Imprime Os Dados do Projeto no Relatorio de pedidocompleto
     PPedidoPRojeto:=Self.Objquery.fieldbyname('codigo').asstring;

     //resgatando a imagem atual
     Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PPedidoPRojeto);
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
     Try
       QrControleEntrega.imgmProjeto.Picture.Graphic:=nil;

       if (Fileexists(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.jpg'))
       Then  QrControleEntrega.imgmProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.jpg')

       Else
       begin
          if (Fileexists(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp'))
          Then  QrControleEntrega.imgmProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp')
          else  QrControleEntrega.imgmProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+'branco.bmp');
       end;
     Except
     End;
     {Try
       QrControleEntrega.imgmProjeto.visible:=True;
       if (Fileexists(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp'))
       Then QrControleEntrega.imgmProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp');

      // Else QRpedido.QrImagemProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+'branco.bmp');
     Except
     End;  }

     //*********************************************
     //dados do projeto, na mesma linha da figura
     QrControleEntrega.LbReferenciaProjeto.caption:='PROJETO: '+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia;
     QrControleEntrega.lbDescricao.Caption:=Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Descricao;
     QrControleEntrega.LbComplemento.Caption:=Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Complemento;
     QrControleEntrega.LbLocal.Caption:=Self.ObjMedidas_Proj.PedidoProjeto.Get_Local;
     if (Self.ObjMedidas_Proj.PedidoProjeto.Get_observacao<>'')
     Then QrControleEntrega.lBObservacaoProjeto.Caption:='OBS: '+Self.ObjMedidas_Proj.PedidoProjeto.Get_observacao
     else QrControleEntrega.lBObservacaoProjeto.Caption:='';



     // No vitrus funciona assim:
     // Se o eixo tem mais de uma medida os campos altura e largura
     //aparecem 0;
     if (UpperCase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo) = 'AXL')then
     Begin
         QrControleEntrega.lbAltura.Caption:='Altura: '+Self.Objquery.fieldbyname('Altura').asstring+' mm.';
         QrControleEntrega.lbLargura.Caption:='Largura: '+Self.Objquery.fieldbyname('Largura').asstring+' mm.';
         QrControleEntrega.lBlado.Caption:=Self.Objquery.fieldbyname('componente').asstring;
         QrControleEntrega.lB8.Caption:='';
         QrControleEntrega.lb9.Caption:='';
         QrControleEntrega.lBlado1.Caption:='';
     end else
     begin
         try
           query:=TIBQuery.Create(nil);
           Query.Database:=FDataModulo.IBDatabase;
         except

         end;

         try
            with Query do
            begin
                Close;
                sql.Clear;
                sql.Add('select * from tabmedidas_proj where pedidoprojeto='+Self.Objquery.fieldbyname('codigo').asstring);
                Open;
                QrControleEntrega.lBlado.Caption:=fieldbyname('componente').asstring;
                QrControleEntrega.lbAltura.Caption:='Altura: '+fieldbyname('Altura').asstring+' mm.';
                QrControleEntrega.lbLargura.Caption:='Largura: '+fieldbyname('Largura').asstring+' mm.';
                Next;
                QrControleEntrega.lBlado1.Caption:=fieldbyname('componente').asstring;
                QrControleEntrega.lb8.Caption:='Altura: '+fieldbyname('Altura').asstring+' mm.';
                QrControleEntrega.lb9.Caption:='Largura: '+fieldbyname('Largura').asstring+' mm.';
            end;
         finally

         end;
     end;


     // Cabe�alho ITEM , DESCRICAO, QTDE, VALOR
     QrControleEntrega.lbITEMQTDEValor.Caption:='';
     QrControleEntrega.lbITEMQTDEValor.Caption:=CompletaPalavra('�TEM',8,' ')+' '+
                            CompletaPalavra('COR',14,' ')+' '+
                            CompletaPalavra('DESCRI��O',39,' ')+' '+
                            'QTDE ';

     //resgatando os dados (ferragems,vidro, perfilado etc;...)
     QrControleEntrega.QrStrBandsb.Items.clear;
     Self.ObjFerragem_PP.RetornaDadosRel(PpedidoProjeto,QrControleEntrega.QrStrBandsb.Items,False);
     Self.ObjPerfilado_PP.RetornaDadosRel(PpedidoProjeto,QrControleEntrega.QrStrBandsb.Items,False);
     Self.ObjVidro_PP.RetornaDadosRel(PpedidoProjeto,QrControleEntrega.QrStrBandsb.Items,False);
     Self.ObjKitBox_PP.RetornaDadosRel(PpedidoProjeto,QrControleEntrega.QrStrBandsb.Items,False);
     Self.ObjServico_PP.RetornaDadosRel(PpedidoProjeto,QrControleEntrega.QrStrBandsb.Items,False);
     Self.ObjPersiana_PP.RetornaDadosRel(PpedidoProjeto,QrControleEntrega.QrStrBandsb.Items,False);
     Self.ObjDiverso_PP.RetornaDadosRel(PpedidoProjeto,QrControleEntrega.QrStrBandsb.Items,False);
     //********************************************************************
     QrControleEntrega.QrStrBandsb.Items.Add('?'+CompletaPalavra('QUANTIDADE FINAL :'+formata_valor(Self.Objquery.fieldbyname('quantidade').AsString)+' UNIDADES',102,' '));
     // Soma Projeto


    { Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PPedidoPRojeto);
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;      }

End;

procedure TObjPedidoObjetos.BandaDetailBeforePrint(Sender: TQRCustomBand;var PrintBand: Boolean);
var
PPedidoPRojeto:string;
query:TIBQuery;
valorfinal:Currency;
begin

     //esse procedimento Imprime Os Dados do Projeto no Relatorio de pedidocompleto
     PPedidoPRojeto:=Self.Objquery.fieldbyname('codigo').asstring;

     //resgatando a imagem atual
     Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PPedidoPRojeto);
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
     Try
       QRpedido.QrImagemProjeto.Picture.Graphic:=nil;
       if (Fileexists(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.jpg'))
       Then QRpedido.QrImagemProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.jpg')

       Else
       begin
          if (Fileexists(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp'))
          Then QRpedido.QrImagemProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp')
          else QRpedido.QrImagemProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+'branco.bmp');
       end;
     Except
     End;

     //*********************************************
     //dados do projeto, na mesma linha da figura
     QRpedido.LbReferenciaProjeto.caption:='PROJETO: '+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia;
     QRpedido.lbDescricao.Caption:=Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Descricao;
     QRpedido.LbComplemento.Caption:=Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Complemento;
     QRpedido.LbLocal.Caption:=Self.ObjMedidas_Proj.PedidoProjeto.Get_Local;


     // No vitrus funciona assim:
     // Se o eixo tem mais de uma medida os campos altura e largura
     //aparecem 0;
     if (UpperCase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo) = 'AXL')then
     Begin
         QRpedido.lbAltura.Caption:='Altura: '+Self.Objquery.fieldbyname('Altura').asstring+' mm.';
         QRpedido.lbLargura.Caption:='Largura: '+Self.Objquery.fieldbyname('Largura').asstring+' mm.';
         QRpedido.lBlado.Caption:=Self.Objquery.fieldbyname('componente').asstring;
         QRpedido.lB1.Caption:='';
         QRpedido.lb2.Caption:='';
         QRpedido.lBlado1.Caption:='';
     end else
     begin
         try
           query:=TIBQuery.Create(nil);
           Query.Database:=FDataModulo.IBDatabase;
         except

         end;

         try
            with Query do
            begin
                Close;
                sql.Clear;
                sql.Add('select * from tabmedidas_proj where pedidoprojeto='+Self.Objquery.fieldbyname('codigo').asstring);
                Open;
                QRpedido.lBlado.Caption:=fieldbyname('componente').asstring;
                QRpedido.lbAltura.Caption:='Altura: '+fieldbyname('Altura').asstring+' mm.';
                QRpedido.lbLargura.Caption:='Largura: '+fieldbyname('Largura').asstring+' mm.';
                Next;
                QRpedido.lBlado1.Caption:=fieldbyname('componente').asstring;
                QRpedido.lb1.Caption:='Altura: '+fieldbyname('Altura').asstring+' mm.';
                QRpedido.lb2.Caption:='Largura: '+fieldbyname('Largura').asstring+' mm.';
            end;
         finally

         end;
     end;


     // Cabe�alho ITEM , DESCRICAO, QTDE, VALOR
     QRpedido.lbITEMQTDEValor.Caption:='';
     QRpedido.lbITEMQTDEValor.Caption:=CompletaPalavra('�TEM',8,' ')+' '+
                            CompletaPalavra('COR',14,' ')+' '+
                            CompletaPalavra('DESCRI��O',39,' ')+' '+
                            'QTDE       VAL.UN  VALOR';

     //resgatando os dados (ferragems,vidro, perfilado etc;...)
     QRpedido.SB.Items.clear;
     Self.ObjFerragem_PP.RetornaDadosRel2(PpedidoProjeto,QRpedido.SB.Items,True);
     Self.ObjPerfilado_PP.RetornaDadosRel2(PpedidoProjeto,QRpedido.SB.Items,True);
     Self.ObjVidro_PP.RetornaDadosRel2(PpedidoProjeto,QRpedido.SB.Items,True);
     Self.ObjKitBox_PP.RetornaDadosRel2(PpedidoProjeto,QRpedido.SB.Items,True);
     Self.ObjServico_PP.RetornaDadosRel2(PpedidoProjeto,QRpedido.SB.Items,True);
     Self.ObjPersiana_PP.RetornaDadosRel2(PpedidoProjeto,QRpedido.SB.Items,True);
     Self.ObjDiverso_PP.RetornaDadosRel2(PpedidoProjeto,QRpedido.SB.Items,True);
     //********************************************************************

     // Soma Projeto
     Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PPedidoPRojeto);
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
     QRpedido.SB.Items.Add('?'+CompletaPalavra_a_Esquerda('TOTAL PROJETO: '+formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Get_ValorFinal),102,' '));
     QRpedido.SB.Items.Add('?'+CompletaPalavra_a_Esquerda('QUANTIDADE: '+formata_valor(Self.Objquery.fieldbyname('quantidade').AsString),102,' '));

     valorfinal:= StrToCurr(Self.ObjMedidas_Proj.PedidoProjeto.Get_ValorFinal)*Self.Objquery.fieldbyname('quantidade').AsCurrency;
     
     QRpedido.SB.Items.Add('?'+CompletaPalavra_a_Esquerda('VALOR FINAL: '+formata_valor(valorfinal),102,' '));


End;



function TObjPedidoObjetos.AtualizaValorPedido(Ppedido: string): boolean;
var
   ValorComissaoArquiteto:Currency;
begin
     Result:=False;

     if (ppedido='')
     then exit;

     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(Ppedido)=False)
     Then Begin
               Messagedlg('Pedido n� '+ppedido+' n�o localizado para ser atualizado',mterror,[mbok],0);
               exit;
     End;

     Self.ObjMedidas_Proj.PedidoProjeto.pedido.TabelaparaObjeto;

     

     With Self.Objquery do
     Begin
         Close;
         Sql.Clear;
         Sql.Add('Select SUM(tabpedido_proj.ValorFinal)  as Soma,tabpedido.PORCENTAGEMCOMISSAOARQUITETO,tabpedido.valorfinal from TabPedido_Proj');
         SQL.Add('join tabpedido on tabpedido.codigo=tabpedido_proj.pedido');
         Sql.Add('Where Pedido = '+PPedido);
         SQL.Add('group by PORCENTAGEMCOMISSAOARQUITETO,valorfinal');
         Open;

         Self.ObjMedidas_Proj.PedidoProjeto.pedido.Status:=dsedit;
         Self.ObjMedidas_Proj.PedidoProjeto.pedido.Submit_valortotal(floattostr(fieldbyname('Soma').Asfloat));
         if(fieldbyname('PORCENTAGEMCOMISSAOARQUITETO').AsString<>'') then
         begin
             ValorComissaoArquiteto:= ((StrToCurr(tira_ponto(floattostr(fieldbyname('valorfinal').Asfloat))) * fieldbyname('PORCENTAGEMCOMISSAOARQUITETO').Asfloat)/100);
             Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Submit_ValorComissaoArquiteto(tira_ponto(formata_valor(CurrToStr(ValorComissaoArquiteto))));
         end;
         Result:=Self.ObjMedidas_Proj.PedidoProjeto.pedido.salvar(false);
     end;
End;

function TObjPedidoObjetos.ResgataDadosClienteEVendedor(PPedido: string): Boolean;
var
imprimearquiteto:boolean;
begin
     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(PPedido)=false)then
     Begin
          MensagemErro('Pedido n�o encontrado. N�o foi poss�vel preencher as informa��es do Cliente.');
          exit;
     end;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     With Self.ObjMedidas_Proj.PedidoProjeto.Pedido do
     Begin
          QRpedido.LbNome.Caption:=Cliente.Get_Nome;
          QRpedido.LBEndereco.Caption:=Cliente.Get_Endereco+','+
                                       Cliente.Get_Numero+' - '+
                                       Cliente.Get_Bairro+' - '+
                                       Cliente.Get_Cidade;

          imprimearquiteto:=False;

          if (ObjParametroGlobal.ValidaParametro('IMPRIME ARQUITETO NO LUGAR DO CONTATO NO PEDIDO COMPLETO')=true)
          then Begin
                    if (ObjParametroGlobal.Get_Valor='SIM')
                    Then imprimearquiteto:=True;
          End;

          if (imprimearquiteto)
          Then QRpedido.LbContato.Caption:=Arquiteto.Get_Nome
          Else QRpedido.LbContato.Caption:=Cliente.Get_Contato;

          QRpedido.LbObra.Caption:=Get_Obra;


          if Cliente.Get_Fone <> ''
          then QRpedido.LBTelefone.Caption:=Cliente.Get_Fone
          Else QRpedido.LBTelefone.Caption:='';

          if  Cliente.Get_Celular <>''
          then QRpedido.LBTelefone.Caption:=QRpedido.LBTelefone.Caption+' - '+Cliente.Get_Celular   ;




          if  Cliente.Get_Fax <>'' then
          QRpedido.LBTelefone.Caption:=QRpedido.LBTelefone.Caption+' - '+Cliente.Get_Fax;

          QRpedido.lbCNPJCPF.Caption:=Cliente.Get_CPF_CGC;
          QRpedido.lbRGIE.Caption:=Cliente.Get_RG_IE;
          QRpedido.LbVendedor.Caption:=Vendedor.Get_Nome;
     end;
end;

function TObjPedidoObjetos.ResgataDadosClienteEVendedorControleEntrega(Ppedido:string):Boolean;
var
imprimearquiteto:boolean;
begin
     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(PPedido)=false)then
     Begin
          MensagemErro('Pedido n�o encontrado. N�o foi poss�vel preencher as informa��es do Cliente.');
          exit;
     end;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     With Self.ObjMedidas_Proj.PedidoProjeto.Pedido do
     Begin
          QrControleEntrega.LbNome.Caption:=Cliente.Get_Nome;
          QrControleEntrega.LBEndereco.Caption:=Cliente.Get_Endereco+','+
                                       Cliente.Get_Numero+' - '+
                                       Cliente.Get_Bairro+' - '+
                                       Cliente.Get_Cidade;

          QrControleEntrega.LbContato.Caption:=Cliente.Get_Contato;

          QrControleEntrega.LbObra.Caption:=Get_Obra;

          if Cliente.Get_Fone <> ''
          then QrControleEntrega.LBTelefone.Caption:=Cliente.Get_Fone
          Else QrControleEntrega.LBTelefone.Caption:='';
          
          if  Cliente.Get_Celular <>''
          then QrControleEntrega.LBTelefone.Caption:=QrControleEntrega.LBTelefone.Caption+' - '+Cliente.Get_Celular;



          if  Cliente.Get_Fax <>'' then
          QrControleEntrega.LBTelefone.Caption:=QrControleEntrega.LBTelefone.Caption+' - '+Cliente.Get_Fax;

          QrControleEntrega.lbCNPJCPF.Caption:=Cliente.Get_CPF_CGC;
          QrControleEntrega.lbRGIE.Caption:=Cliente.Get_RG_IE;
          QrControleEntrega.LbVendedor.Caption:=Vendedor.Get_Nome;

          QrControleEntrega.lB1.Caption:='Nome     :';
          QrControleEntrega.lB2.Caption:='Endere�o :';
          QrControleEntrega.lBcomplemento1.caption:=Cliente.Get_Complemento;

     end;
end;

procedure TObjPedidoObjetos.PreencheCabecalhoControleEntrega;
Var  PLocalLogoTipo: string;
begin
     With ObjParametroGlobal  do
     Begin
          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 1')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 1" n�o encontrado');
          end
          else QrControleEntrega.LBCabecalhoLinha1.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 2')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 2" n�o encontrado');
          end
          else QrControleEntrega.LBCabecalhoLinha2.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 3')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 3" n�o encontrado');
          end
          else QrControleEntrega.LBCabecalhoLinha3.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 4')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 4" n�o encontrado');
          end
          else QrControleEntrega.LBCabecalhoLinha4.Caption:=Get_Valor;

          PLocalLogoTipo:=ExtractFilePath(Application.exename)+'Images\Logo_Vidracaria.jpg';
          Try
              QrControleEntrega.imgLogotipo.Picture.LoadFromFile(PLocalLogoTipo);
          Except
              MensagemErro('Erro ao tentar localizar o Logotipo da Vidra�aria no endere�o:'+#13+PLocalLogoTipo);
          End;
     end;
end;

procedure TObjPedidoObjetos.PreencheCabecalhoPedido;
Var  PLocalLogoTipo: string;
begin
     With ObjParametroGlobal  do
     Begin
          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 1')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 1" n�o encontrado');
          end
          else QRpedido.LBCabecalhoLinha1.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 2')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 2" n�o encontrado');
          end
          else QRpedido.LBCabecalhoLinha2.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 3')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 3" n�o encontrado');
          end
          else QRpedido.LBCabecalhoLinha3.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 4')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 4" n�o encontrado');
          end
          else QRpedido.LBCabecalhoLinha4.Caption:=Get_Valor;

          PLocalLogoTipo:=ExtractFilePath(Application.exename)+'Images\Logo_Vidracaria.jpg';
          Try
              QRpedido.QRImageLogotipo.Picture.LoadFromFile(PLocalLogoTipo);
          Except
              MensagemErro('Erro ao tentar localizar o Logotipo da Vidra�aria no endere�o:'+#13+PLocalLogoTipo);
          End;
     end;
end;
function TObjPedidoObjetos.RetornaValorKitBox(PpedidoProjeto,
  PcorKitBox: string): string;
begin
     Result:='';

     if (Ppedidoprojeto='') or (PcorKitBox='')
     Then exit;

     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
     Then exit;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (self.ObjKitBox_PP.KitBoxCor.LocalizaCodigo(PcorKitBox)=False)
     Then exit;
     Self.ObjKitBox_PP.KitBoxCor.TabelaparaObjeto;

     Try
         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
         Then Result:=floattostr(strtofloat(Self.ObjKitBox_PP.KitBoxCor.KitBox.Get_PrecoVendaInstalado)+((strtofloat(Self.ObjKitBox_PP.KitBoxCor.KitBox.Get_PrecoVendaInstalado)*strtofloat(Self.ObjKitBox_PP.KitBoxCor.get_PorcentagemAcrescimoFinal))/100));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
         Then Result:=floattostr(strtofloat(Self.ObjKitBox_PP.KitBoxCor.KitBox.Get_PrecoVendaretirado)+((strtofloat(Self.ObjKitBox_PP.KitBoxCor.KitBox.Get_PrecoVendaretirado)*strtofloat(Self.ObjKitBox_PP.KitBoxCor.get_PorcentagemAcrescimoFinal))/100));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
         Then Result:=floattostr(strtofloat(Self.ObjKitBox_PP.KitBoxCor.KitBox.Get_PrecoVendafornecido)+((strtofloat(Self.ObjKitBox_PP.KitBoxCor.KitBox.Get_PrecoVendafornecido)*strtofloat(Self.ObjKitBox_PP.KitBoxCor.get_PorcentagemAcrescimoFinal))/100));
    Except
          Result:='';
    End;

end;

function TObjPedidoObjetos.RetornaValorPerfilado(PpedidoProjeto, PcorPerfilado: string): string;
begin
     Result:='';

     if (Ppedidoprojeto='') or (PcorPerfilado='')
     Then exit;

     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
     Then exit;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (self.ObjPerfilado_PP.PerfiladoCor.LocalizaCodigo(PcorPerfilado)=False)
     Then exit;
     Self.ObjPerfilado_PP.PerfiladoCor.TabelaparaObjeto;

     Try
         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
         Then Result:=floattostr(strtofloat(Self.ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_PrecoVendaInstalado)+((strtofloat(Self.ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_PrecoVendaInstalado)*strtofloat(Self.ObjPerfilado_PP.PerfiladoCor.get_PorcentagemAcrescimoFinal))/100));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
         Then Result:=floattostr(strtofloat(Self.ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_PrecoVendaretirado)+((strtofloat(Self.ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_PrecoVendaretirado)*strtofloat(Self.ObjPerfilado_PP.PerfiladoCor.get_PorcentagemAcrescimoFinal))/100));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
         Then Result:=floattostr(strtofloat(Self.ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_PrecoVendafornecido)+((strtofloat(Self.ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_PrecoVendafornecido)*strtofloat(Self.ObjPerfilado_PP.PerfiladoCor.get_PorcentagemAcrescimoFinal))/100));
    Except
          Result:='';
    End;

end;

function TObjPedidoObjetos.RetornaValorPersiana(PpedidoProjeto,PPersianaGrupoDiametroCor: string): string;
begin
     Result:='';

     if (Ppedidoprojeto='') or (PPersianaGrupoDiametroCor ='')
     Then exit;

     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
     Then exit;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (self.ObjPersiana_PP.PersianaGrupoDiametroCor.LocalizaCodigo(PPersianaGrupoDiametroCor)=False)
     Then exit;
     Self.ObjPersiana_PP.PersianaGrupoDiametroCor.TabelaparaObjeto;

     Try
         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
         Then Result:=floattostr(strtofloat(Self.ObjPersiana_PP.PersianaGrupoDiametroCor.Get_PrecoVendaInstalado));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
         Then Result:=floattostr(strtofloat(Self.ObjPersiana_PP.PersianaGrupoDiametroCor.Get_PrecoVendaretirado));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
         Then Result:=floattostr(strtofloat(Self.ObjPersiana_PP.PersianaGrupoDiametroCor.Get_PrecoVendafornecido));
    Except
         Result:='';
    End;

end;

function TObjPedidoObjetos.RetornaValorServico(PpedidoProjeto,
  PServico: string): string;
begin
     Result:='';

     if (Ppedidoprojeto='') or (PServico='')
     Then exit;

     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
     Then exit;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (self.ObjServico_PP.Servico.LocalizaCodigo(PServico)=False)
     Then exit;
     Self.ObjServico_PP.Servico.TabelaparaObjeto;

     Try
         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
         Then Result:=floattostr(strtofloat(Self.ObjServico_PP.Servico.Get_PrecoVendaInstalado));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
         Then Result:=floattostr(strtofloat(Self.ObjServico_PP.Servico.Get_PrecoVendaretirado));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
         Then Result:=floattostr(strtofloat(Self.ObjServico_PP.Servico.Get_PrecoVendafornecido));
    Except
          Result:='';
    End;

end;

function TObjPedidoObjetos.RetornaValorVidro(PpedidoProjeto, PcorVidro:string): string;
begin
     Result:='';

     if (Ppedidoprojeto='') or (PcorVidro='')
     Then exit;

     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
     Then exit;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (self.ObjVidro_PP.VidroCor.LocalizaCodigo(PcorVidro)=False)
     Then exit;
     Self.ObjVidro_PP.VidroCor.TabelaparaObjeto;

     Try
         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
         Then Result:=floattostr(strtofloat(Self.ObjVidro_PP.VidroCor.Vidro.Get_PrecoVendaInstalado)+((strtofloat(Self.ObjVidro_PP.VidroCor.Vidro.Get_PrecoVendaInstalado)*strtofloat(Self.ObjVidro_PP.VidroCor.get_PorcentagemAcrescimoFinal))/100));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
         Then Result:=floattostr(strtofloat(Self.ObjVidro_PP.VidroCor.Vidro.Get_PrecoVendaretirado)+((strtofloat(Self.ObjVidro_PP.VidroCor.Vidro.Get_PrecoVendaretirado)*strtofloat(Self.ObjVidro_PP.VidroCor.get_PorcentagemAcrescimoFinal))/100));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
         Then Result:=floattostr(strtofloat(Self.ObjVidro_PP.VidroCor.Vidro.Get_PrecoVendafornecido)+((strtofloat(Self.ObjVidro_PP.VidroCor.Vidro.Get_PrecoVendafornecido)*strtofloat(Self.ObjVidro_PP.VidroCor.get_PorcentagemAcrescimoFinal))/100));
    Except
          Result:='';
    End;

end;

function TObjPedidoObjetos.AtualizaVidros(var PStrGrid: TStringGrid;
  PPedido, PPedidoProjeto: string): Boolean;
Var QueryLocal : TIBQuery;
begin
{     try
          try
          QueryLocal:=TIBQuery.Create(nil);
          QueryLocal.Database:=FDataModulo.IBDatabase;
          except
              MensagemErro('Erro ao tentar criar a QueryLocal');
              exit;
          end;

          With QueryLocal do
          Begin
               Close;
               Sql.Clear;
               Sql.Add('Select Codigo from TabPedidoProjeto Where Pedido = '+PPedido);

               if (PProjeto <> '')then
               Sql.Add('and codigo = '+PProjeto);

               if (PPedido<>'')then
               exit;

               Open;
               First;

               While not (eof) do
               Begin
                    //Soma Vidros
                    Self.Objquery.Close;
                    Self.Objquery.SQL.Clear;
                    Self.Objquery.SQL.Add('Select SUM(ValorFinal)  as Valor');
                    Self.Objquery.SQL.Add('from TabVidro_PP');
                    Self.Objquery.SQL.Add('Join TabPedido_Proj on TabVidro_PP.PEdidoPRojeto = TabPedido_PRoj.Codigo');
                    Self.Objquery.SQL.Add('Join TabPedido on TabPedido_Proj.Pedido = TabPedido.Codigo');
                    Self.Objquery.SQL.Add('Where TabVidro_PP.PEdidoProjeto ='+QueryLocal.fieldbyname('codigo').asstring);
                    Self.Objquery.Open;

                    //Soma Ferragems

                    //Soma Perfilados

                    //Soma Servicos

                    //Soma KitBox

                    //Soma Persiana

                    //Soma diversos

               end;



          end;








     Finally
        FreeAndNil(QueryLOcal);
     end;
 }
end;

function TObjPedidoObjetos.GravaMedidas(PpedidoProjeto: string;PProjetoemBranco:Boolean;Largura:string;Altura:string): Boolean;
Var
PAlturaAnterior, PLarguraAnterior : string;
Paltura,Plargura,PalturaArredondamento,PlarguraArredondamento:currency;
begin
     //JONATAN MEDINA 12/05/2012
     //A altera��o feita aqui � para pegar as medidas direto da tela de cadastro sem ter a necessidade de mostrar
     //a tela para pegar medidas para os projetos AxL


     Result:=False;
     //F�bio --> Preciso guardar a altura e largura do pedidoprojeto
     //para preencher os edits com os valores anteriores
     //isso serve pra quando for alterado um pedidoprojeto o cara poder ver as
     //medidas que estavam gravadas, mas s� mostrarei quando for um projeto com eixo AxL
     if (Self.ObjMedidas_Proj.RetornaAlturaLarguraPedidiProjeto(PAlturaAnterior,PLarguraAnterior,PPedidoProjeto)=false)
     then exit;

     //primeiro excluo todas as medidas para este pedidoprojeto
     if (Self.ObjMedidas_Proj.exclui_PP(PpedidoProjeto)=False)
     Then exit;

     if (PProjetoemBranco=True)
     Then Begin
               result:=True;
               exit;
     End;

     //verificando qual o eixo do Projetro
     Fmedidas_proj.PanelMedidaFrontal.Visible:=False;
     Fmedidas_proj.PanelMedidalateral.Visible:=False;
     Fmedidas_proj.PanelComponentes.Visible:=False;
     Fmedidas_proj.DbGridMedidas.Visible:=False;
     Fmedidas_proj.pnlMedidasUnicasDuplas.Visible:=False;

     if (uppercase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo)='AXL')//altura e largura simples
     Then Begin
               //Para eixos simples basta pegar a informa��o que esta nos edits na tela de cadastro
               //desprezando a necessidade de abrir a tela de filtro de medidas

               Fmedidas_proj.pnlMedidasUnicasDuplas.Visible:=True;
               Fmedidas_proj.PanelMedidaFrontal.Visible:=True;
               //Fmedidas_proj.EdtAlturaFrontal.Text:=PAlturaAnterior;
               //Fmedidas_proj.EdtLarguraFrontal.Text:=PLarguraAnterior;
               Fmedidas_proj.EdtAlturaFrontal.Text:=Altura;
               Fmedidas_proj.EdtLarguraFrontal.Text:=Largura;
               
               {Fmedidas_proj.showmodal;

               if (Fmedidas_proj.tag=0)
               then Begin
                        Messagedlg('A digita��o das Medidas n�o foi Conclu�da!',mterror,[mbok],0);
                        exit;
               End; }
               try
                   StrToCurr(Altura);
               except
                   Messagedlg('Altura inv�lida!',mterror,[mbok],0);
                   exit;
               end;

               try
                   StrToCurr(Largura);
               except
                   Messagedlg('Largura inv�lida!',mterror,[mbok],0);
                   exit;
               end;


               Try
                  Plargura:=strtocurr(Fmedidas_proj.EdtLarguraFrontal.Text);
                  PlarguraArredondamento:=0;
                  paltura:=strtocurr(Fmedidas_proj.EdtAlturaFrontal.Text);
                  PalturaArredondamento:=0;
               Except
                     Messagedlg('Medidas Frontais Inv�lidas',mterror,[mbok],0);
                     exit;
               End;

               With Self.ObjMedidas_Proj do
               Begin

                    if (FDataModulo.UtilizaArredondamentoGlobal5cm)
                    Then Begin
                              FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
                              FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);
                    End;


                    ZerarTabela;
                    Status:=dsinsert;
                    PedidoProjeto.Submit_codigo(PpedidoProjeto);
                    Submit_CODIGO(Get_NovoCodigo);
                    //nome padrao para medidas frontais
                    Submit_Componente('FRONTAL');

                    Submit_Altura(currtostr(paltura));
                    Submit_Largura(currtostr(Plargura));
                    Submit_AlturaArredondamento(currtostr(palturaarredondamento));
                    Submit_Larguraarredondamento(currtostr(Plarguraarredondamento));

                    if (Salvar(False)=False)
                    Then Begin
                              Messagedlg('Erro na tentativa de Gravar as Medidas Frontais na Tabela de Medidas do Projeto',mterror,[mbok],0);
                              exit;
                    End;
                    Result:=true;
                    exit;
              End;
     End;

     if (uppercase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo)='AXL AXL')//altura e largura 2 vezes
     Then Begin
               //� necessario altura e largura frontal e lateral para este tipo de eixo
               //por isso habilito apenas os 2 panels
               //a gravacao no banco sera feita apos retornar do FORM
               //e nao dentro do FORM
               Fmedidas_proj.pnlMedidasUnicasDuplas.Visible:=True;
               Fmedidas_proj.PanelMedidaFrontal.Visible:=True;
               Fmedidas_proj.PanelMedidaLateral.Visible:=True;
               Fmedidas_proj.edtLarguraFrontal.Text:=Largura;
               Fmedidas_proj.edtAlturaFrontal.Text:=Altura;
               Fmedidas_proj.showmodal;

               if (Fmedidas_proj.tag=0)
               then Begin
                        Messagedlg('A digita��o das Medidas n�o foi Conclu�da!',mterror,[mbok],0);
                        exit;
               End;

               Try
                  strtofloat(Fmedidas_proj.EdtLarguraFrontal.Text);
                  strtofloat(Fmedidas_proj.EdtAlturaFrontal.Text);
               Except
                     Messagedlg('Medidas Frontais Inv�lidas',mterror,[mbok],0);
                     exit;
               End;

               Try
                  strtofloat(Fmedidas_proj.edtlarguralateral.Text);
                  strtofloat(Fmedidas_proj.edtalturalateral.Text);
               Except
                     Messagedlg('Medidas Laterais Inv�lidas',mterror,[mbok],0);
                     exit;
               End;



               With Self.ObjMedidas_Proj do
               Begin
                     Try
                        Plargura:=strtocurr(Fmedidas_proj.EdtLarguraFrontal.Text);
                        PlarguraArredondamento:=0;
                        paltura:=strtocurr(Fmedidas_proj.EdtAlturaFrontal.Text);
                        PalturaArredondamento:=0;
                     Except
                           Messagedlg('Medidas Frontais Inv�lidas',mterror,[mbok],0);
                           exit;
                     End;

                     if (FDataModulo.UtilizaArredondamentoGlobal5cm)
                     Then Begin
                               FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
                               FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);
                     End;


                    ZerarTabela;
                    Status:=dsinsert;
                    PedidoProjeto.Submit_codigo(PpedidoProjeto);
                    Submit_CODIGO(Get_NovoCodigo);
                    //nome padrao para medidas frontais
                    Submit_Componente('FRONTAL');
                    Submit_Altura(Fmedidas_proj.EdtAlturaFrontal.Text);
                    Submit_Largura(Fmedidas_proj.EdtLarguraFrontal.Text);

                    Submit_AlturaArredondamento(currtostr(PalturaArredondamento));
                    Submit_larguraArredondamento(currtostr(PlarguraArredondamento));

                    if (Salvar(False)=False)
                    Then Begin
                              Messagedlg('Erro na tentativa de Gravar as Medidas Frontais na Tabela de Medidas do Projeto',mterror,[mbok],0);
                              exit;
                    End;



                     Try
                        Plargura:=strtocurr(Fmedidas_proj.EdtLarguralateral.Text);
                        PlarguraArredondamento:=0;
                        paltura:=strtocurr(Fmedidas_proj.EdtAlturalateral.Text);
                        PalturaArredondamento:=0;
                     Except
                           Messagedlg('Medidas Frontais Inv�lidas',mterror,[mbok],0);
                           exit;
                     End;

                     if (FDataModulo.UtilizaArredondamentoGlobal5cm)
                     Then Begin
                               FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
                               FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);
                     End;

                    ZerarTabela;
                    Status:=dsinsert;
                    PedidoProjeto.Submit_codigo(PpedidoProjeto);
                    Submit_CODIGO(Get_NovoCodigo);
                    //nome padrao para medidas laterais
                    Submit_Componente('LATERAL');
                    Submit_Altura(Fmedidas_proj.edtalturalateral.Text);
                    Submit_Largura(Fmedidas_proj.edtlarguralateral.Text);
                    Submit_AlturaArredondamento(currtostr(PalturaArredondamento));
                    Submit_larguraArredondamento(currtostr(PlarguraArredondamento));

                    if (Salvar(False)=False)
                    Then Begin
                              Messagedlg('Erro na tentativa de Gravar as Medidas Frontais na Tabela de Medidas do Projeto',mterror,[mbok],0);
                              exit;
                    End;

                    Result:=true;
                    exit;
              End;
     End;

     if (uppercase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo) = 'VARIOS')
     Then Begin
               //nesse caso nao se utiliza nem frontal nem lateral
               //e sim os componentes que estao no projeto escolhido
               Fmedidas_proj.PanelComponentes.Visible:=True;
               Fmedidas_proj.DbGridMedidas.Visible:=True;

               if (Fmedidas_proj.PassaObjeto(Self.ObjMedidas_Proj)=False)
               then exit;

               Fmedidas_proj.showmodal;

               //verificando se todos os componentes foram inseridos

               if (Fmedidas_proj.ComboComponente.Text<>'')
               Then Begin
                         Messagedlg('Todos os componentes devem ser preenchidos',mterror,[mbok],0);
                         exit;
               End;

               if (Fmedidas_proj.tag=0)
               then Begin
                        Messagedlg('A digita��o das Medidas n�o foi Conclu�da!',mterror,[mbok],0);
                        exit;
               End;
               result:=True;
     End;



end;

function TObjPedidoObjetos.RetornaValorDiverso(PpedidoProjeto,
  PCorDiverso: string): string;
begin
     Result:='';

     if (Ppedidoprojeto='') or (PcorDiverso='')
     Then exit;

     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
     Then exit;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (self.ObjDiverso_PP.DiversoCor.LocalizaCodigo(PcorDiverso)=False)
     Then exit;
     Self.ObjDiverso_PP.DiversoCor.TabelaparaObjeto;

     Try
         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
         Then Result:=floattostr(strtofloat(Self.ObjDiverso_PP.DiversoCor.Diverso.Get_PrecoVendaInstalado)+((strtofloat(Self.ObjDiverso_PP.DiversoCor.Diverso.Get_PrecoVendaInstalado)*strtofloat(Self.ObjDiverso_PP.DiversoCor.get_PorcentagemAcrescimoFinal))/100));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
         Then Result:=floattostr(strtofloat(Self.ObjDiverso_PP.DiversoCor.Diverso.Get_PrecoVendaretirado)+((strtofloat(Self.ObjDiverso_PP.DiversoCor.Diverso.Get_PrecoVendaretirado)*strtofloat(Self.ObjDiverso_PP.DiversoCor.get_PorcentagemAcrescimoFinal))/100));


         if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
         Then Result:=floattostr(strtofloat(Self.ObjDiverso_PP.DiversoCor.Diverso.Get_PrecoVendafornecido)+((strtofloat(Self.ObjDiverso_PP.DiversoCor.Diverso.Get_PrecoVendafornecido)*strtofloat(Self.ObjDiverso_PP.DiversoCor.get_PorcentagemAcrescimoFinal))/100));
    Except
          Result:='';
    End;

end;

procedure TObjPedidoObjetos.Imprime(PPedido: string);
begin
     With FmenuRelatorios do
     Begin
          //era no titulo antes
          NomeObjeto:='UOBJPEDIDOSOBJETOS';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Pedido Compacto ');
                items.add('Pedido Completo ');
                items.Add('Projeto em Branco');
                items.add('Vendas por Per�odo e Vendedor');
                items.add('Comiss�o por Arquiteto');
                items.add('Valor M�dio e quantidade vendida por produto');
                items.add('Componentes do Projetos do Pedido');
                items.add('Valor M�dio e quantidade entregue por produto');
                items.add('Materiais por pedido e Cliente (Analitico)');
                Items.Add('Materiais por pedido e Cliente (Sintetico)');
                items.add('Autoriza��o de Emiss�o de NF');
                items.add('Custo dos Produtos do Pedido');
                Items.Add('Lista de Pedidos');
                Items.Add('Controle de Entrega Compacto');
                Items.Add('Controle de Entrega Completo');
               // Items.Add('Vendas por Per�odo');
          end;


          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0 : Self.ImprimePedidoCompacto(PPedido);
                1 : Self.ImprimePedido(PPedido);
                2 : Self.ImprimePedidoProjetoBranco(PPedido);
                3 : Self.ImprimeVendaPeriodo_vendedor;
                4 : Self.ImprimeComissaoArquiteto;
                5 : Self.Imprime_quantidade_e_media_valor_produto;
                6 : Self.ImprimeComponentesProjetos_do_pedido(ppedido);
                7 : Self.Imprime_quantidade_entregue_e_media_valor_produto;
                8 : Self.Imprime_Relatorio_materiais_por_pedido_e_cliente;
                9 : Imprime_Relatorio_materiais_por_pedido_e_cliente_sintetico;
                10: Self.ImprimeAutorizacaoEmissaoNF(ppedido);
                11: Self.ImprimeCustos(ppedido);
                12: Self.ImprimeListaPedidos(PPedido);
                13: Self.ImprimePedidoControleEntrega(PPedido);
                14: Self.ImprimeControleDeEntrega(PPedido);
               // 12: Self.ImprimeVendaPorPeriodo(PPedido);
          End;
     end;

  end;


  
procedure TObjPedidoObjetos.ImprimePedidoCompacto(Ppedido: string);
var
Hora1,hora2:Tdatetime;
begin
     Hora1:=now;
     Try
         TMPQRpedidoCompacto:=TQRpedidoCompacto.create(nil);
     Except

     End;

Try
     if (Ppedido='')
     Then Begin
               Messagedlg('Escolha o Pedido que deseja imprimir',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.VerificaSeTemProjetosNoPedido(Ppedido)=false)then
     Begin
          MensagemErro('Nenhum projeto cadastrado neste pedido');
          exit;
     end;


     TMPQRpedidoCompacto.QRImageLogotipo.Stretch:=False;
     if (ObjParametroGlobal.ValidaParametro('STRETCH NO LOGOTIPO DO PEDIDO')=True)
     then Begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               Then TMPQRpedidoCompacto.QRImageLogotipo.Stretch:=true;
     End;

     //Tamanho normal da banda
     TMPQRpedidoCompacto.BandaDetail.Height:=85;

     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(Ppedido)=false)then
     Begin
          MensagemErro('Pedido n�o encontrado.');
          exit;
     end;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     TMPQRpedidoCompacto.LbValorTotal.Caption:=formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorTotal);
     TMPQRpedidoCompacto.LBValorAcrescimo.Caption:=formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorAcrescimo);
     TMPQRpedidoCompacto.LBValorDesconto.Caption:=formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorDesconto);
     TMPQRpedidoCompacto.LBValorBonificacao.Caption:=formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorBonificacaoCliente);
     TMPQRpedidoCompacto.LBValorFinal.Caption:=formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorFinal);


     //***************************************************************
     TMPQRpedidoCompacto.lbCondicoesPagamento.Caption:='';
     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Concluido = 'S')then
     begin
          TMPQRpedidoCompacto.QrLbProposta.Caption:='PEDIDO';
          TMPQRpedidoCompacto.lbCondicoesPagamento.Caption:='Condi��es de Pagamento';
          PreencheObservacaoPedido(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.get_Observacao);
     end else
     Begin
          TMPQRpedidoCompacto.QrLbProposta.Caption:='PROPOSTA';
          PreencheObservacaoPedido(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.get_Observacao);
     end;


     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select * from ProcPedidoProjetoQtde('+Ppedido+')');
          open;
          TMPQRpedidoCompacto.DataSet:=Self.Objquery;

          Self.PreencheCabecalhoPedidoCompacto;
          Self.ResgataDadosClienteEVendedorPedidoCompacto(Ppedido);

          Self.ImprimePendencias_Pedido(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Titulo,TMPQRpedidoCompacto.QrMemoPendencias);

          TMPQRpedidoCompacto.LBProposta.Caption:='PROPOSTA N� '+Ppedido;
          TMPQRpedidoCompacto.lbTitulo.Caption  :='T�TULO   N� '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Titulo;
          TMPQRpedidoCompacto.BandaDetail.BeforePrint:=Self.BandaDetailBeforePrintPedidoCompacto;

          //Soma Total do Pedido
          //Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(fieldbyname('Codigo').AsString);
          //Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
          //totalizacoes colocadas la em cima



          TMPQRpedidoCompacto.Preview;

          //Hora2:=now;
          //Showmessage(datetimetostr(hora2-hora1));
          //TMPQRpedidoCompacto.SB.Items.clear;
          close;

     End;
Finally
       Freeandnil(TMPQRpedidoCompacto);
End;

end;

function TObjPedidoObjetos.ResgataDadosClienteEVendedorPedidoCompacto(
  PPedido: string): Boolean;
begin
     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(PPedido)=false)then
     Begin
          MensagemErro('Pedido n�o encontrado. N�o foi poss�vel preencher as informa��es do Cliente.');
          exit;
     end;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     With Self.ObjMedidas_Proj.PedidoProjeto.Pedido do
     Begin
          TMPQRpedidoCompacto.LbNome.Caption:=Cliente.Get_Codigo+' - '+Cliente.Get_Nome;
          TMPQRpedidoCompacto.LbNomeClienteAssinatura.Caption:=Cliente.Get_Nome;

          TMPQRpedidoCompacto.LBEndereco.Caption:=Cliente.Get_Endereco+', '+Cliente.Get_Numero+' - '+
                                       Cliente.Get_Bairro+' - '+
                                       Cliente.Get_Cidade+' - CEP '+Cliente.Get_CEP;;
          TMPQRpedidoCompacto.LbContato.Caption:=Cliente.Get_Contato;
          TMPQRpedidoCompacto.lBEmail2.Caption := Get_email;
          TMPQRpedidoCompacto.LbObra.Caption:=Get_Obra;
          if Cliente.Get_Fone <> '' then
          TMPQRpedidoCompacto.LBTelefone.Caption:=Cliente.Get_Fone;
          if  Cliente.Get_Celular <>'' then
          TMPQRpedidoCompacto.LBTelefone.Caption:=TMPQRpedidoCompacto.LBTelefone.Caption+'-'+Cliente.Get_Celular;
          if  Cliente.Get_Fax <>'' then
          TMPQRpedidoCompacto.LBTelefone.Caption:=TMPQRpedidoCompacto.LBTelefone.Caption+'-'+Cliente.Get_Fax;

          TMPQRpedidoCompacto.lbCNPJCPF.Caption:=Cliente.Get_CPF_CGC;
          TMPQRpedidoCompacto.lbRGIE.Caption:=Cliente.Get_RG_IE;
          TMPQRpedidoCompacto.LbVendedor.Caption:=Vendedor.Get_Nome;
          TMPQRpedidoCompacto.lBFoneVendedor.Caption:=Vendedor.Get_Fone;
          TMPQRpedidoCompacto.lbNomeVendedorAssinatura.Caption:='Vendedor: '+Vendedor.Get_Nome;


          TMPQRpedidoCompacto.LBTelefone.Width:=311;
          if(Get_Concluido='S')
          then TMPQRpedidoCompacto.lBdata.Caption:=Get_Data
          else TMPQRpedidoCompacto.lBdata.Caption:='';
          TMPQRpedidoCompacto.lBDataOrcamento.Caption:=Get_DataOrcameto;
          TMPQRpedidoCompacto.lBDataEntrega.Caption:=Get_DataEntrega;

          if (ImprimeCepSeparadoPedidoCompacto_global=True)
          Then Begin
                  //diminuo o telefone
                  TMPQRpedidoCompacto.LBTelefone.Width:=150;
                  TMPQRpedidoCompacto.QrDbLabelCEP.Caption:=Cliente.Get_CEP;
                  TMPQRpedidoCompacto.QrLabelCep.caption:='CEP   :'
          End
          Else Begin
                    TMPQRpedidoCompacto.QrLabelCep.caption:='';
                    TMPQRpedidoCompacto.QrDbLabelCEP.Caption:='';
          End;
     end;
end;

procedure TObjPedidoObjetos.BandaDetailBeforePrintPedidoCompacto(Sender: TQRCustomBand; var PrintBand: Boolean);
var
PPedidoPRojeto:string;
LinhaComplemento:string;
PNomeGrupoPerfilado:string;
Pquantidade:integer;
Query:TIBQuery;
begin

     //esse procedimento Imprime Os Dados do Projeto no Relatorio de pedidoCompacto
     PPedidoPRojeto:=Self.Objquery.fieldbyname('codigo').asstring;
     Pquantidade:=Self.Objquery.fieldbyname('quantidade').asinteger;

     //resgatando a imagem atual
     Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PPedidoPRojeto);
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;


     TMPQRpedidoCompacto.BandaDetail.Height :=85;


     //Quando o projeto � o projeto em Branco naum aparece a parte do desenho e suas informa��es
     //deve aparecer o detalhe do produto, igual  aparece no pedido completo.
     if (Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Codigo = FdataModulo.CodigoProjetoBrancoGlobal)then
     Begin
          TMPQRpedidoCompacto.BandaDetail.Height :=5;
          if (ProjetoEmBrancoNoPedidoCompacto(PPedidoPRojeto) = false)then
          Begin
               MensagemErro('Erro ao tentar imprimir as informa��es do projeto em branco');
               exit;
          end;
          //saindo da impressao de pedido em Branco
          Exit;
     end;

    //METODO ANTIGO
    { Try
         TMPQRpedidoCompacto.QrImagemProjeto.Visible:=true;
         if FileExists(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp')
         then TMPQRpedidoCompacto.QrImagemProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp')
         Else TMPQRpedidoCompacto.QrImagemProjeto.Visible:=False;
     Except
     End;   }

     //resgatando a imagem atual
     Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PPedidoPRojeto);
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
     Try
       TMPQRpedidoCompacto.QrImagemProjeto.Picture.Graphic:=nil;
       if (Fileexists(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.jpg'))
       Then TMPQRpedidoCompacto.QrImagemProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.jpg')

       Else
       begin
          if (Fileexists(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp'))
          Then TMPQRpedidoCompacto.QrImagemProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp')
          else TMPQRpedidoCompacto.QrImagemProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+'branco.bmp');
       end;
     Except
     End;

     //*********************************************
     //dados do projeto, na mesma linha da figura
     TMPQRpedidoCompacto.LbReferenciaProjeto.caption:='PROJETO: '+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia;
     TMPQRpedidoCompacto.lbDescricao.Caption:=Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Descricao;
     TMPQRpedidoCompacto.LbLocal.Caption:='AMBIENTE: '+Self.ObjMedidas_Proj.PedidoProjeto.Get_Local;


     // No vitrus funciona assim:
     // Se o eixo tem mais de uma medida os campos altura e largura
     //aparecem 0;
     //ShowMessage(Self.Objquery.fieldbyname('Altura').asstring);
     //ShowMessage(Self.Objquery.fieldbyname('Largura').asstring);
     if (UpperCase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo) = 'AXL')then
     Begin
         TMPQRpedidoCompacto.lbAltura.Caption:='Altura: '+Self.Objquery.fieldbyname('Altura').asstring+' mm.';
         TMPQRpedidoCompacto.lbLargura.Caption:='Largura: '+Self.Objquery.fieldbyname('Largura').asstring+' mm.';
         TMPQRpedidoCompacto.lBMedidalocal.Caption:=Self.Objquery.fieldbyname('componente').asstring;
         TMPQRpedidoCompacto.lBmedidalocal1.Caption:='';
         TMPQRpedidoCompacto.lb2.Caption:='';
         TMPQRpedidoCompacto.lb1.Caption:='';
     end
     else
     begin
       try
         query:=TIBQuery.Create(nil);
         Query.Database:=FDataModulo.IBDatabase;
       except

       end;

       try
          with Query do
          begin
              Close;
              sql.Clear;
              sql.Add('select * from tabmedidas_proj where pedidoprojeto='+Self.Objquery.fieldbyname('codigo').asstring);
              Open;
              TMPQRpedidoCompacto.lBMedidalocal.Caption:=fieldbyname('componente').asstring;
              TMPQRpedidoCompacto.lbAltura.Caption:='Altura: '+fieldbyname('Altura').asstring+' mm.';
              TMPQRpedidoCompacto.lbLargura.Caption:='Largura: '+fieldbyname('Largura').asstring+' mm.';
              Next;
              TMPQRpedidoCompacto.lBMedidalocal1.Caption:=fieldbyname('componente').asstring;
              TMPQRpedidoCompacto.lb2.Caption:='Altura: '+fieldbyname('Altura').asstring+' mm.';
              TMPQRpedidoCompacto.lb1.Caption:='Largura: '+fieldbyname('Largura').asstring+' mm.';
          end;
       finally
            FreeAndNil(Query);
       end;


         //TMPQRpedidoCompacto.lbAltura.Caption:='Altura: 0 mm.';
         //TMPQRpedidoCompacto.lbLargura.Caption:='Largura: 0 mm.';
     end;

     // Montando a linha de complemento
     if Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido = 'INSTALADO'
     then LinhaComplemento:='                INSTALADO COM '
     else
        if Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido = 'FORNECIDO'
        then LinhaComplemento:='                FORNECIDO COM '
        else
            if Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido = 'RETIRADO'
            then LinhaComplemento:='                RETIRADO COM';


      //****************************************************************
     if (Self.VerificaSeTemFerragem(PPedidoPRojeto) = true)
     then LinhaComplemento:=LinhaComplemento+'FERRAGENS '+Self.ObjMedidas_Proj.PedidoProjeto.CorFerragem.Get_Descricao+' E ';

     PNomeGrupoPerfilado:='';
     if (Self.VerificaGrupoPerfilado(PPedidoPRojeto, PNomeGrupoPerfilado) = true)
     then LinhaComplemento:=LinhaComplemento+PNomeGrupoPerfilado+' '+Self.ObjMedidas_Proj.PedidoProjeto.CorPerfilado.Get_Descricao+', ';

     if (Self.VerificaSeTemKitBox(PPedidoPRojeto)=True)
     then LinhaComplemento:=LinhaComplemento+'KITBOX NA COR '+Self.RetornaCoreskitBox(PPedidoPRojeto)+', ';


     if (Self.VerificaSeTemPersiana(PPedidoPRojeto)=True)
     then LinhaComplemento:=LinhaComplemento+'PERSIANA, ';

     if (Self.VerificaSeTemDiversos(PPedidoPRojeto)=True)
     then LinhaComplemento:=LinhaComplemento+'DIVERSO, ';
     //**********************************************************************

     // Altura e Largura que agora est� numa tabela separada.


     TMPQRpedidoCompacto.SB.Items.clear;
     TMPQRpedidoCompacto.SB.Items.Add(LinhaComplemento);
     //+'CONFORME COR ESPECIFICADA ABAIXO.');

     // Colocando informacoes do Vidro
     LinhaComplemento:='';
     if (Self.RetornaInformacoesVidroPedidoCompacto(LinhaComplemento, PPedidoPRojeto,Pquantidade)=false)then
     Begin
         MensagemErro('Vidro n�o encontrado.');
         exit;
     end;
     TMPQRpedidoCompacto.SB.Items.Add('?'+LinhaComplemento);
     //teste de informacao individual
     if (Self.ObjMedidas_Proj.PedidoProjeto.Get_observacao<>'')
     Then TMPQRpedidoCompacto.SB.Items.Add('OBS: '+Self.ObjMedidas_Proj.PedidoProjeto.Get_observacao);
end;
function TObjPedidoObjetos.VerificaSeTemFerragem(PPedidoProjeto: string): Boolean;
begin

     Result:=false;

     With Self.ObjqueryTEMP do
     Begin
          Try
            Close;
            SQL.Clear;
            Sql.Add('Select count(Codigo) as CONTA from TabFerragem_PP');
            Sql.Add('Where PedidoProjeto = '+PPedidoProjeto);

            if (PPedidoProjeto<>'')
            then Open
            Else exit;

            if (Self.ObjqueryTEMP.Fieldbyname('conta').asinteger> 0)
            then Result:=true
            else Result:=false;
          Finally
                 close;
          End;
     end;
end;

function TObjPedidoObjetos.VerificaGrupoPerfilado(PPedidoProjeto: string; var PNomeGrupoPerfilado: string): Boolean;
begin


     Result:=false;

     With Self.ObjqueryTEMP do
     Begin
          Try
            Close;
            SQL.Clear;
            Sql.Add('Select TabGrupoPerfilado.Nome from TabPerfilado_PP');
            Sql.Add('Join TabPerfiladoCor on TabPerfiladoCor.Codigo = TabPerfilado_PP.PerfiladoCor');
            Sql.Add('Join TabPerfilado on TabPerfilado.Codigo = TabPerfiladoCor.Perfilado');
            Sql.Add('Join TabGrupoPerfilado on TabGrupoPErfilado.Codigo = TabPerfilado.GrupoPerfilado');
            Sql.Add('Where TabPerfilado_PP.PedidoProjeto = '+PPedidoProjeto);
            if (PPedidoProjeto<>'')
            then Open;

            if (RecordCount > 0)
            then Begin
                     PNomeGrupoPerfilado:=fieldbyname('NOME').AsString;
                     Result:=true
            end
            else Begin
                     PNomeGrupoPerfilado:='';
                   Result:=false;
            end;
          Finally
                 close;
          End;
     end;
end;

function TObjPedidoObjetos.VerificaSeTemServico(
  PPedidoProjeto: string): Boolean;
begin

     Result:=false;
     With Self.ObjqueryTEMP do
     Begin
          Try
            Close;
            SQL.Clear;
            Sql.Add('Select count(Codigo) as CONTA from TabServico_PP');
            Sql.Add('Where PedidoProjeto = '+PPedidoProjeto);

            if PPedidoProjeto<>''
            then Open
            Else exit;

            if (Fieldbyname('conta').asinteger> 0)
            then Result:=true
            Else Result:=false;
          Finally
            close;
          End;
     end;
end;

function TObjPedidoObjetos.VerificaSeTemKitBox( PPedidoProjeto: string): Boolean;
begin
     Result:=false;
     With Self.ObjqueryTEMP do
     Begin
          Try
            Close;
            SQL.Clear;
            Sql.Add('Select count(Codigo) as CONTA from TabKitBox_PP');
            Sql.Add('Where PedidoProjeto = '+PPedidoProjeto);
            if PPedidoProjeto<>''
            then Open
            Else exit;

            if (fieldbyname('conta').asinteger> 0)
            then Result:=true
            else Result:=false;
          Finally
                 close;
          End;
     end;
end;



function TObjPedidoObjetos.RetornaCoreskitBox( PPedidoProjeto: string): String;
var
temp:String;
begin
     Result:='';

     if PPedidoProjeto=''
     then exit;
     
     With Self.ObjqueryTEMP do
     Begin
          Try
            Close;
            SQL.Clear;
            Sql.Add('Select distinct(TabCor.Descricao)');
            Sql.Add('from TabKitBox_PP');
            Sql.Add('join TabKitBoxCor on TabKitBox_PP.KitBoxCor=TabkitBoxCor.codigo');
            Sql.Add('join TabCor on TabKitBoxCor.Cor=TabCor.Codigo');
            Sql.Add('Where PedidoProjeto = '+PPedidoProjeto);
            open;
            first;
            temp:='';
            While not(eof) do
            Begin
                 if (temp='')
                 then temp:=fieldbyname('descricao').asstring
                 Else temp:=temp+' e '+fieldbyname('descricao').asstring;
                 next;
            End;
            result:=temp;
          Finally
                 close;
          End;
     end;
end;


function TObjPedidoObjetos.VerificaSeTemPersiana( PPedidoProjeto: string): Boolean;
begin


     Result:=false;

     With Self.ObjqueryTEMP do
     Begin
          Try
            Close;
            SQL.Clear;
            Sql.Add('Select count(Codigo) as CONTA from TabPersiana_PP');
            Sql.Add('Where PedidoProjeto = '+PPedidoProjeto);

            if PPedidoProjeto<>''
            then Open
            Else exit;

            if (Fieldbyname('conta').asinteger> 0)
            then Result:=true
            Else Result:=false;
          Finally
                 close;
          End;
     end;
end;

function TObjPedidoObjetos.VerificaSeTemDiversos(PPedidoProjeto: string): Boolean;
begin

     Result:=false;
     With Self.ObjqueryTEMP do
     Begin
          Try
            Close;
            SQL.Clear;
            Sql.Add('Select count(codigo)as CONTA from TabDiverso_PP');
            Sql.Add('Where PedidoProjeto = '+PPedidoProjeto);
            if PPedidoProjeto<>''
            then Open;

            if (Fieldbyname('conta').asinteger > 0)
            then Result:=true
            else  Result:=false;
          Finally
                 close;
          End;
     end;
end;

function TObjPedidoObjetos.RetornaInformacoesVidroPedidoCompacto(var PLinhaComplemento: string; PPedidoProjeto:string;Pquantidade:integer): Boolean;
var
PvalorFinal:currency;
begin

       Result:=false;

       With Self.ObjqueryTEMP do
       Begin
            close;
            Sql.Clear;
            Sql.Add('Select TabVidro.Descricao as NomeVidro,');
            Sql.Add('TabCor.Descricao as NomeCor,');
            Sql.Add('TabVidro_PP.Valor as ValorVidro,');
            Sql.Add('TabVidro_PP.Quantidade as QuantidadeVidro');
            Sql.Add('from TabVidro_PP');
            Sql.Add('join TabVidroCor on TabVidroCor.Codigo = TabVidro_PP.VidroCor');
            Sql.Add('join TabVidro on TabVidro.Codigo = TabVidroCor.Vidro');
            Sql.Add('join TabCor on TabCor.Codigo = TabVidroCor.Cor');
            Sql.Add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabVidro_PP.PEdidoProjeto');
            Sql.Add('Where TabVidro_PP.PedidoProjeto = '+PPedidoProjeto);
            if (PPedidoProjeto='')then
            exit;

            Open;

            if (RecordCount > 0)then
            Begin
                 PLinhaComplemento:=CompletaPalavra(fieldbyname('NomeVidro').AsString+' '+
                                                    fieldbyname('NomeCor').AsString,50,' ')+' '+
                                    CompletaPalavra(fieldbyname('QuantidadeVidro').AsString+' m�',10,' ')+' ';
            end;
            Close;
            Sql.Clear;
            Sql.Add('Select TabPedido_Proj.ValorFinal as ValorFinalProjeto');
            Sql.Add('from TabPedido_proj');
            Sql.Add('Where Codigo ='+PPedidoProjeto);
            Open;

            Try
              PvalorFinal:=fieldbyname('ValorFinalProjeto').asfloat*Pquantidade;
            Except
                  Messagedlg('Erro no C�lculo do Valor total do Vidro pela quantidade replicada',mterror,[mbok],0);
                  exit;
            End;

            PLinhaComplemento:=PLinhaComplemento+' '+'QTDE  '+completapalavra(IntToStr(PQUANTiDADE),5,' ')+CompletaPalavra_a_Esquerda('R$ '+formata_valor(fieldbyname('ValorFinalProjeto').asfloat),10,' ')+'  '+CompletaPalavra_a_Esquerda('R$ '+formata_valor(Pvalorfinal),10 ,' ') ;
            close;
            Result:=True;
       end;
end;

function TObjPedidoObjetos.RetornaLarguraPedidoProjeto(Var PLinhaComplemento: string; PPedidoProjeto: string): Boolean;
Var   QueryLocal : TIBQuery;
begin

           try
                QueryLocal:=TIBQuery.Create(nil);
                QueryLocal.Database:=FDataModulo.IBDatabase;
           except
                MensagemErro('Erro ao tentar criar a QueryLocal');
                exit;
           end;
     try
           With QueryLocal do
           Begin
                close;
                Sql.Clear;
                SQl.Add('Select Altura, Largura from TabMedidas_Proj');
                SQl.Add('Where PedidoProjeto = '+PPedidoProjeto);
                if (PPedidoProjeto='')then
                exit;

                Open;
           end;      

     Finally
           FreeAndNil(QueryLocal);
     end;

end;

procedure TObjPedidoObjetos.PreencheCabecalhoPedidoCompacto;
Var  PLocalLogoTipo: string;
begin
     With ObjParametroGlobal  do
     Begin
          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 1')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 1" n�o encontrado');
          end
          else TMPQRpedidoCompacto.LBCabecalhoLinha1.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 2')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 2" n�o encontrado');
          end
          else TMPQRpedidoCompacto.LBCabecalhoLinha2.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 3')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 3" n�o encontrado');
          end
          else TMPQRpedidoCompacto.LBCabecalhoLinha3.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 4')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 4" n�o encontrado');
          end
          else TMPQRpedidoCompacto.LBCabecalhoLinha4.Caption:=Get_Valor;

          PLocalLogoTipo:=ExtractFilePath(Application.exename)+'Images\Logo_Vidracaria.jpg';
          Try
              TMPQRpedidoCompacto.QRImageLogotipo.Picture.LoadFromFile(PLocalLogoTipo);
          Except
              MensagemErro('Erro ao tentar localizar o Logotipo da Vidra�aria no endere�o:'+#13+PLocalLogoTipo);
          End;
     end;
end;

procedure TObjPedidoObjetos.RetornaMedidas(var PMemo: TMemo;
  PPedidoProjeto: string;var edtlargura:string;var edtaltura:string);
begin
      With  Self.Objquery do
      Begin
           Close;
           Sql.Clear;
           Sql.Add('Select Componente, Altura, Largura from TabMedidas_Proj');
           SQL.Add('Where PedidoProjeto = '+PPedidoProjeto);
           if (PPedidoProjeto='')then
           exit;

           Open;
           PMemo.Clear;
           PMemo.Lines.Add(CompletaPalavra('COMPONENTE',10,' ')+' '+
                           CompletaPalavra_a_Esquerda('ALTURA',10,' ')+' '+
                           CompletaPalavra_a_Esquerda('LARGURA',10,' '));
           PMemo.Lines.Add(CompletaPalavra('-',32,'-'));
           While not (eof) do
           Begin
                if(fieldbyname('componente').AsString ='FRONTAL') THEN
                begin
                    edtaltura:=fieldbyname('altura').AsString;
                    edtlargura:=fieldbyname('largura').AsString;
                end;  
                PMemo.Lines.Add(CompletaPalavra(fieldbyname('Componente').AsString,10,' ')+' '+
                                CompletaPalavra_a_Esquerda(fieldbyname('Altura').AsString,10,' ')+' '+
                                CompletaPalavra_a_Esquerda(fieldbyname('Largura').AsString,10,' '));
                next;
           end;
      end;
end;

Procedure TObjPedidoObjetos.ReplicaProjeto(PcodigoPedidoProjeto: string);
var
cont,TmpQuantidade:Integer;
PNovoPedidoProjeto:string;
begin

     if (PcodigoPedidoProjeto='')
     then Begin
               Messagedlg('Escolha o projeto que deseja replicar',mterror,[mbok],0);
               exit;
     End;

     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PcodigoPedidoProjeto)=False)
     Then begin
               Messagedlg('Projeto n�o encontrado!',mterror,[mbok],0);
               exit;
     End;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (self.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal<>'')
     Then Begin
               mensagemerro('N�o � permitido replicar um projeto que j� foi replicado');
               exit; 
     End;

     With FfiltroImp do
     Begin
           DesativaGrupos;
           Grupo01.Enabled:=True;
           LbGrupo01.caption:='Quantidade';
           showmodal;
           if Tag=0
           Then exit;
           Try
              Tmpquantidade:=strtoint(edtgrupo01.Text);
           Except
                 Messagedlg('Quantidade Inv�lida',mterror,[mbok],0);
                 exit;
           End;
     End;
     Try
         FMostraBarraProgresso.ConfiguracoesIniciais(8,0);
         FMostraBarraProgresso.lbmensagem.caption:='Gravando Materiais da Venda';
         FMostraBarraProgresso.Lbmensagem2.caption:='Gravando Materiais da Venda';

         FMostraBarraProgresso.IncrementaBarra1(1);
         FMostraBarraProgresso.Show;
         for cont:=1 to tmpquantidade do
         begin
             //criando um novo pedidoprojeto com os dados do escolhido
             Self.ObjMedidas_Proj.PedidoProjeto.Status:=dsinsert;
             Self.ObjMedidas_Proj.PedidoProjeto.Submit_Codigo('0');
             Self.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal(PcodigoPedidoProjeto);

             if (Self.ObjMedidas_Proj.PedidoProjeto.Salvar(False)=False)
             Then Begin
                       Messagedlg('Erro na tentativa de Criar um Novo Projeto para este pedido',mterror,[mbok],0);
                       exit;
             End;

             //inserir os dados via sql
             With self.Objquery do
             Begin
                  PNovoPedidoProjeto:='';
                  PNovoPedidoProjeto:=Self.ObjMedidas_Proj.PedidoProjeto.get_codigo;
                  //********************* Ferragem **************************
                  close;
                  sql.clear;
                  sql.add('Insert Into TABFerragem_PP(Codigo,FerragemCor');
                  sql.add(',PedidoProjeto,Quantidade,Valor)');
                  sql.add('Select gen_id(genFerragem_pp,1),FerragemCor ,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from TabFerragem_pp');
                  sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                  Try
                     execsql;
                  Except
                        Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                        exit;
                  End;

                  //********************* Perfilado **************************
                  close;
                  sql.clear;
                  sql.add('Insert Into TABPerfilado_PP(Codigo,PerfiladoCor');
                  sql.add(',PedidoProjeto,Quantidade,Valor)');
                  sql.add('Select gen_id(genPerfilado_pp,1),PerfiladoCor ,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from TabPerfilado_pp');
                  sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                  Try
                     execsql;
                  Except
                        Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                        exit;
                  End;

                  //********************* Vidro **************************
                  close;
                  sql.clear;
                  sql.add('Insert Into TABVidro_PP(Codigo,VidroCor');
                  sql.add(',PedidoProjeto,Quantidade,Valor)');
                  sql.add('Select gen_id(genVidro_pp,1),VidroCor ,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from TabVidro_pp');
                  sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                  Try
                     execsql;
                  Except
                        Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                        exit;
                  End;

                  //********************* KitBox **************************
                  close;
                  sql.clear;
                  sql.add('Insert Into TABKitBox_PP(Codigo,KitBoxCor');
                  sql.add(',PedidoProjeto,Quantidade,Valor)');
                  sql.add('Select gen_id(genKitBox_pp,1),KitBoxCor ,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from TabKitBox_pp');
                  sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                  Try
                     execsql;
                  Except
                        Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                        exit;
                  End;

                  //********************* Servico **************************
                  close;
                  sql.clear;
                  sql.add('Insert Into TABServico_PP(Codigo,Servico');
                  sql.add(',PedidoProjeto,Quantidade,Valor)');
                  sql.add('Select gen_id(genServico_pp,1),Servico ,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from TabServico_pp');
                  sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                  Try
                     execsql;
                  Except
                        Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                        exit;
                  End;


                  //********************* Persiana **************************
                  close;
                  sql.clear;
                  sql.add('Insert Into TABPERSIANA_PP(Codigo,PersianaGrupoDiametroCor');
                  sql.add(',PedidoProjeto,Quantidade,Valor)');
                  sql.add('Select gen_id(genpersiana_pp,1),PersianaGrupoDiametroCor ,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from Tabpersiana_pp');
                  sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                  Try
                     execsql;
                  Except
                        Messagedlg('Erro na tentativa de Inserir as Persianas',mterror,[mbok],0);
                        exit;
                  End;


                  //********************* Diverso **************************
                  close;
                  sql.clear;
                  sql.add('Insert Into TABDiverso_PP(Codigo,DiversoCor');
                  sql.add(',PedidoProjeto,Quantidade,Valor)');
                  sql.add('Select gen_id(genDiverso_pp,1),DiversoCor,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from TabDiverso_pp');
                  sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                  Try
                     execsql;
                  Except
                        Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                        exit;
                  End;

                  //********************* Medidas **************************
                  //Se o o eixo do Projeto for Diferente de AxL n�o precisa gravar as medidas, pois
                  //n�o saira no pedido Impresso
                  if (Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo = 'AXL')then
                  Begin
                        if (Self.ObjMedidas_Proj.LocalizaPorCodigoPedidoProjeto(PcodigoPedidoProjeto)=True)
                        then Begin
                                  Self.ObjMedidas_Proj.TabelaparaObjeto;

                                  close;
                                  sql.clear;
                                  sql.add('Insert Into TABMedidas_Proj(Codigo,PedidoProjeto,');
                                  sql.add('Componente,Altura, Largura)');
                                  sql.add('Select gen_id(genMedidas_Proj,1),'+PNovoPedidoProjeto+','+#39+Self.ObjMedidas_Proj.Get_Componente+#39+',');
                                  sql.Add(Self.ObjMedidas_Proj.Get_Altura+','+Self.ObjMedidas_Proj.Get_Largura);
                                  sql.Add('from TabMedidas_proj');
                                  sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                                  Try
                                     execsql;
                                  Except
                                        Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                                        exit;
                                  End;
                        End;
                  end;

            End;

            //atualiza o valor do projeto
            if (Self.AtualizavalorProjeto(PcodigoPedidoProjeto)=False)
            Then Begin
                       Messagedlg('Erro na Tentativa de Gravar o Valor Total nesse Projeto',mterror,[mbok],0);
                       exit;
            End;

            FMostraBarraProgresso.IncrementaBarra1(1);
            FMostraBarraProgresso.Show;

         End;

     Finally
            FDataModulo.IBTransaction.RollbackRetaining;
            FMostraBarraProgresso.Close;
     End;
end;

procedure TObjPedidoObjetos.opcoes(Var PTagFormOpcoes:Integer; PcodigoPedidoProjeto: string;LarguraProjeto:string;AlturaProjeto:string);
begin
     With FOpcaorel do
     begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Recalcular Projeto');
          RgOpcoes.Items.add('Replicar Projeto');
          showmodal;

          PTagFormOpcoes:=Tag;

          if (Tag=0)
          Then exit;

          case RgOpcoes.ItemIndex of
          0:Begin
                 Self.ReCalculaProjeto(PcodigoPedidoProjeto,LarguraProjeto,AlturaProjeto);
          End;
          1:Begin
                 Self.ReplicaProjeto(pcodigopedidoprojeto);
          End;

          End;

     End;


end;

function TObjPedidoObjetos.AumentaEstoque(Nota,Pedido,Pdata:String):Boolean;
var
     PControlaNegativo:Boolean;
     UtilizaSpedFiscal:Boolean;
     PpedidoProjeto:string;
     QueryPP:TIBQuery;
begin
    //No caso de cancelar a nota fiscal
    QueryPP:=TIBQuery.Create(nil);
    QueryPP.Database:=FDataModulo.IBDatabase;
    try
         Result:=False;
         pcontrolanegativo:=False;
         //Se o cliente � obrigado a emitir sped fiscal, quando cancelar uma nota fiscal o estoque tem q voltar
         if(ObjParametroGlobal.ValidaParametro('UTILIZA SPED FISCAL')=false) then
         begin
              MensagemAviso('O Parametro "UTILIZA SPED FISCAL" n�o foi encontrado!!!');
              Exit;
         end;
         if(ObjParametroGlobal.Get_Valor='SIM')
         then UtilizaSpedFiscal:=True
         else UtilizaSpedFiscal:=False;

         if (ObjParametroGlobal.ValidaParametro('TRAVAR PARA ESTOQUE N�O FICAR NEGATIVO?')=False)
         then exit;
         if (ObjParametroGlobal.Get_Valor='SIM')
         Then PControlaNegativo:=True;

         QueryPP.Close;
         QueryPP.SQL.Clear;
         QueryPP.SQL.Add('select ped.codigo as pedido,pp.codigo as pedidoprojeto');
         QueryPP.SQL.Add('from tabpedido ped');
         QueryPP.SQL.Add('join tabpedido_proj pp on pp.pedido=ped.codigo where ped.codigo='+Pedido);
         QueryPP.Open;

         while not QueryPP.Eof do
         begin
               if(UtilizaSpedFiscal=True) then
               begin
                   PpedidoProjeto:=QueryPP.fieldbyname('pedidoprojeto').asstring;

                   if (Self.ObjFerragem_PP.DiminuiAumentaEstoquePedido(Nota,PpedidoProjeto,PControlaNegativo,Pdata,'S')=False)
                   then exit;

                   if (Self.ObjPerfilado_PP.DiminuiAumentaEstoquePedido(Nota,PpedidoProjeto,PControlaNegativo,Pdata,'S')=False)
                   then exit;

                   if (Self.ObjVidro_PP.DiminuiAumentaEstoquePedido(Nota,PpedidoProjeto,PControlaNegativo,Pdata,'S')=False)
                   then exit;

                   if (Self.Objkitbox_PP.DiminuiAumentaEstoquePedido(Nota,PpedidoProjeto,PControlaNegativo,Pdata,'S')=False)
                   then exit;

                   if (Self.ObjDiverso_PP.DiminuiAumentaEstoquePedido(Nota,PpedidoProjeto,PControlaNegativo,Pdata,'S')=False)
                   then exit;

                   if (Self.ObjPersiana_PP.DiminuiAumentaEstoquePedido(Nota,PpedidoProjeto,PControlaNegativo,Pdata,'S')=False)
                   then exit;
               end;
               QueryPP.Next;
         end;
         Result:=True;
    finally
         FreeAndNil(QueryPP);
    end;

end;


function TObjPedidoObjetos.DiminuiEstoquePedido(Nota,Pedido,Pdata:String):Boolean;
var
PControlaNegativo:Boolean;
UtilizaSpedFiscal:Boolean;
PpedidoProjeto:string;
QueryPP:TIBQuery;
begin
    //Baixa de estoque pelo pedido
    QueryPP:=TIBQuery.Create(nil);
    QueryPP.Database:=FDataModulo.IBDatabase;
    try
         Result:=False;
         pcontrolanegativo:=False;
         //Se o cliente usa Sped Fiscal n�o pode baixar estoque pelo romaneio
         //somente baixa estoque com nota fiscal
         if(ObjParametroGlobal.ValidaParametro('UTILIZA SPED FISCAL')=false) then
         begin
              MensagemAviso('O Parametro "UTILIZA SPED FISCAL" n�o foi encontrado!!!');
              Exit;
         end;
         if(ObjParametroGlobal.Get_Valor='SIM')
         then UtilizaSpedFiscal:=True
         else UtilizaSpedFiscal:=False;

         if (ObjParametroGlobal.ValidaParametro('TRAVAR PARA ESTOQUE N�O FICAR NEGATIVO?')=False)
         then exit;
         if (ObjParametroGlobal.Get_Valor='SIM')
         Then PControlaNegativo:=True;

         QueryPP.Close;
         QueryPP.SQL.Clear;
         QueryPP.SQL.Add('select ped.codigo as pedido,pp.codigo as pedidoprojeto');
         QueryPP.SQL.Add('from tabpedido ped');
         QueryPP.SQL.Add('join tabpedido_proj pp on pp.pedido=ped.codigo where ped.codigo='+Pedido);
         QueryPP.Open;

         while not QueryPP.Eof do
         begin
               if(UtilizaSpedFiscal=True) then
               begin
                   PpedidoProjeto:=QueryPP.fieldbyname('pedidoprojeto').asstring;

                   if (Self.ObjFerragem_PP.DiminuiAumentaEstoquePedido(Nota,PpedidoProjeto,PControlaNegativo,Pdata,'N')=False)
                   then exit;

                   if (Self.ObjPerfilado_PP.DiminuiAumentaEstoquePedido(Nota,PpedidoProjeto,PControlaNegativo,Pdata,'N')=False)
                   then exit;

                   if (Self.ObjVidro_PP.DiminuiAumentaEstoquePedido(Nota,PpedidoProjeto,PControlaNegativo,Pdata,'N')=False)
                   then exit;

                   if (Self.Objkitbox_PP.DiminuiAumentaEstoquePedido(Nota,PpedidoProjeto,PControlaNegativo,Pdata,'N')=False)
                   then exit;

                   if (Self.ObjDiverso_PP.DiminuiAumentaEstoquePedido(Nota,PpedidoProjeto,PControlaNegativo,Pdata,'N')=False)
                   then exit;

                   if (Self.ObjPersiana_PP.DiminuiAumentaEstoquePedido(Nota,PpedidoProjeto,PControlaNegativo,Pdata,'N')=False)
                   then exit;
               end;
               QueryPP.Next;
         end;
         Result:=True;
    finally
         FreeAndNil(QueryPP);
    end;


end;

function TObjPedidoObjetos.DiminuiEstoque(PpedidoProjetoRomaneio,PpedidoProjeto,Pdata:String):Boolean;
var
PControlaNegativo:Boolean;
UtilizaSpedFiscal:Boolean;
begin
     Result:=False;
     //este procedimento � chamando pelo Objeto PedidoProjetoRomaneio
     //que � a entrega de pedidos, o estoque s� � baixado
     //no momento da entrega

     pcontrolanegativo:=False;

     //Se o cliente usa Sped Fiscal n�o pode baixar estoque pelo romaneio
     //somente baixa estoque com nota fiscal
     if(ObjParametroGlobal.ValidaParametro('UTILIZA SPED FISCAL')=false) then
     begin
          MensagemAviso('O Parametro "UTILIZA SPED FISCAL" n�o foi encontrado!!!');
          Exit;
     end;
     if(ObjParametroGlobal.Get_Valor='SIM')
     then UtilizaSpedFiscal:=True
     else UtilizaSpedFiscal:=False;

     if (ObjParametroGlobal.ValidaParametro('TRAVAR PARA ESTOQUE N�O FICAR NEGATIVO?')=False)
     then exit;

     if (ObjParametroGlobal.Get_Valor='SIM')
     Then PControlaNegativo:=True;

     //Somente se o cliente n�o usa SPED FISCAL
     //Se usar n�o pode baixar estoque
     if(UtilizaSpedFiscal=False) then
     begin
         if (Self.ObjFerragem_PP.DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto,PControlaNegativo,Pdata)=False)
         then exit;

         if (Self.ObjPerfilado_PP.DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto,PControlaNegativo,Pdata)=False)
         then exit;

         if (Self.ObjVidro_PP.DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto,PControlaNegativo,Pdata)=False)
         then exit;

         if (Self.Objkitbox_PP.DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto,PControlaNegativo,Pdata)=False)
         then exit;

         if (Self.ObjDiverso_PP.DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto,PControlaNegativo,Pdata)=False)
         then exit;

         if (Self.ObjPersiana_PP.DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto,PControlaNegativo,Pdata)=False)
         then exit;
     end;

     Result:=True;

end;

function TObjPedidoObjetos.PReencheInformacoesRelPersiana(PPedidoProjeto: string): Boolean;
Var PStrList : TStringList;
    Cont:Integer;
begin
     Result:=false;

     try
           PStrList:=TStringList.Create;
     except
          MensagemErro('erro ao Tentar Criar a PStringList');
          exit;
     end;
try



     if (VerificaSeTemFerragem(PPedidoProjeto)=true)then
     Begin
          MensagemErro('Esse tipo de Pedido s� � v�lido para Persianas. Foram encontradas algumas Ferragens.'+#13+
                       'Portanto o Pedido n�o ser� impresso. ');
          Result:=false;
          exit;
     end;

     if (VerificaSeTemServico(PPedidoProjeto)=true)then
     Begin
          MensagemErro('Esse tipo de Pedido s� � v�lido para Persianas. Foram encontrados alguns Servi�os.'+#13+
                       'Portanto o Pedido n�o ser� impresso. ');
          Result:=false;
          exit;
     end;

     if (VerificaSeTemKitBox(PPedidoProjeto)=true)then
     Begin
          MensagemErro('Esse tipo de Pedido s� � v�lido para Persianas. Foram encontrados alguns KitBox.'+#13+
                       'Portanto o Pedido n�o ser� impresso. ');
          Result:=false;
          exit;
     end;

     if (VerificaSeTemDiversos(PPedidoProjeto)=true)then
     Begin
          MensagemErro('Esse tipo de Pedido s� � v�lido para Persianas. Foram encontrados alguns Diversos.'+#13+
                       'Portanto o Pedido n�o ser� impresso. ');
          Result:=false;
          exit;
     end;

     if (VerificaSeTemPerfilado(PPedidoProjeto)=true)then
     Begin
          MensagemErro('Esse tipo de Pedido s� � v�lido para Persianas. Foram encontrados alguns Perfilado.'+#13+
                       'Portanto o Pedido n�o ser� impresso. ');
          Result:=false;
          exit;
     end;

     if (VerificaSeTemVidro(PPedidoProjeto)=true)then
     Begin
          MensagemErro('Esse tipo de Pedido s� � v�lido para Persianas. Foram encontrados alguns Vidro.'+#13+
                       'Portanto o Pedido n�o ser� impresso. ');
          Result:=false;
          exit;
     end;


     With Self.Objquery  do
     Begin
          Close;
          SQL.Clear;
          SQl.Add('Select TabPersiana.Nome as Nome,TabPersiana_PP.Complemento, TabGrupoPersiana.Nome as NOmeGrupo,');
          SQl.Add('TabCor.Referencia as ReferenciaCor, TabCor.Descricao as NomeCor,');
          SQl.Add('TabPersianaGrupoDiametroCor.Diametro,TabPersiana_PP.Quantidade,');
          SQL.Add('(TabPersiana_PP.Quantidade*TabPersiana_PP.Valor) as Valor');
          SQl.Add('from TabPErsiana_PP');
          SQl.Add('Join TabPersianaGrupoDiametroCor on TabPersianaGrupoDiametroCor.Codigo = TabPersiana_PP.PersianaGrupoDiametroCor');
          SQl.Add('Join TabPersiana on TabPersiana.Codigo = TabPersianaGrupoDiametroCor.Persiana');
          SQl.Add('Join TabGrupoPersiana on TabGrupoPersiana.Codigo = TabPersianaGrupoDiametroCor.GrupoPersiana');
          SQl.Add('Join TabCor on TabCor.Codigo = TabPersianaGrupoDiametroCor.Cor');
          SQl.Add('Where PedidoProjeto = '+PPedidoProjeto);
          Open;

          QRpedidoPersiana.SB.Items.Clear;

          QRpedidoPersiana.SB.Items.Add(CompletaPalavra('?DESCRI��O',30,' ')+' '+
                                        CompletaPalavra('GRUPO',15,' ')+' '+
                                        CompletaPalavra('COR',20,' ')+' '+
                                        CompletaPalavra('DIAMETRO',8,' ')+' '+
                                        CompletaPalavra('QTDE',10,' ')+' '+
                                        CompletaPalavra_a_Esquerda('VALOR',10,' '));

          While not (eof) do
          Begin
              QRpedidoPersiana.SB.Items.Add(CompletaPalavra(fieldbyname('Nome').AsString,29,' ')+' '+
                                            CompletaPalavra(fieldbyname('NomeGrupo').AsString,15,' ')+' '+
                                            CompletaPalavra(fieldbyname('ReferenciaCor').AsString+'-'+fieldbyname('NomeCor').AsString,20,' ')+' '+
                                            CompletaPalavra(fieldbyname('Diametro').AsString+' mm.',8,' ')+' '+
                                            CompletaPalavra(fieldbyname('Quantidade').AsString,10,' ')+' '+
                                            CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('Valor').AsString),10,' '));

              PreencheStringList(PStrList,fieldbyname('Complemento').asString);
              for Cont :=0 to PStrList.Count-1 do
              Begin
                   QRpedidoPersiana.SB.Items.Add(CompletaPalavra(PStrlist.Strings[cont],29,' '));
              end;


          Next;
          end;
          QRpedidoPersiana.SB.Items.Add('');
          QRpedidoPersiana.SB.Items.Add('');

          // Atualizando os Valores Totais
          Self.ObjMedidas_Proj.PedidoProjeto.ZerarTabela;
          Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PPedidoProjeto);
          Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

          QRpedidoPersiana.LbValorTotal.Caption:=    'Valor Total Pedido '+CompletaPalavra_a_Esquerda(formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorTotal), 15, ' ');
          QRpedidoPersiana.LBValorAcrescimo.Caption:='Valor Acr�scimo    '+CompletaPalavra_a_Esquerda(formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorAcrescimo),15,' ');
          QRpedidoPersiana.LBValorDesconto.Caption:= 'Valor Desconto     '+CompletaPalavra_a_Esquerda(formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorDesconto),15,' ');
          QRpedidoPersiana.LBValorFinal.Caption:=    'Valor Final        '+CompletaPalavra_a_Esquerda(formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorFinal),15,' ');

          Result:=true;
     end;
finally
     FreeAndNil(PStrList);
end;
end;

procedure TObjPedidoObjetos.ImprimePedidoPersiana(PPedido: string);
begin
     if (Ppedido='')
     Then Begin
               Messagedlg('Escolha o Pedido que deseja imprimir',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.VerificaSeTemProjetosNoPedido(Ppedido)=false)then
     Begin
          MensagemErro('Nenhum projeto cadastrado neste pedido');
          exit;
     end;

     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(Ppedido)=false)then
     Begin
          MensagemErro('Pedido n�o encontrado.');
          exit;
     end;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;


     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select codigo from tabpedido_proj where pedido='+Ppedido);
          open;
          //QRpedidoPersiana.DataSet:=Self.Objquery;

          Self.PreencheCabecalhoPedidoPersiana;
          Self.ResgataDadosClienteEVendedorPedidoPersiana(Ppedido);
          Self.ImprimePendencias_Pedido(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Titulo,QRpedidoPersiana.QrMemoPendencias);

          QRpedidoPersiana.LBProposta.Caption:='PROPOSTA N� '+Ppedido;
          QRpedidoPersiana.lbTitulo.Caption  :='T�TULO   N� '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Titulo;
          if (Self.PReencheInformacoesRelPersiana(fieldbyname('Codigo').AsString)=false) then
          Begin
               Exit;
          end;

          QRpedidoPersiana.Preview;
     End;

end;

procedure TObjPedidoObjetos.PreencheCabecalhoPedidoPersiana;
Var  PLocalLogoTipo: string;
begin
     With ObjParametroGlobal  do
     Begin
          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 1')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 1" n�o encontrado');
          end
          else QRpedidoPersiana.LBCabecalhoLinha1.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 2')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 2" n�o encontrado');
          end
          else QRpedidoPersiana.LBCabecalhoLinha2.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 3')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 3" n�o encontrado');
          end
          else QRpedidoPersiana.LBCabecalhoLinha3.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 4')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 4" n�o encontrado');
          end
          else QRpedidoPersiana.LBCabecalhoLinha4.Caption:=Get_Valor;

          PLocalLogoTipo:=ExtractFilePath(Application.exename)+'Images\Logo_Vidracaria.jpg';
          Try
              QRpedidoPersiana.QRImageLogotipo.Picture.LoadFromFile(PLocalLogoTipo);
          Except
              MensagemErro('Erro ao tentar localizar o Logotipo da Vidra�aria no endere�o:'+#13+PLocalLogoTipo);
          End;
     end;
end;

function TObjPedidoObjetos.ResgataDadosClienteEVendedorPedidoPersiana(
  PPedido: string): Boolean;
begin
     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(PPedido)=false)then
     Begin
          MensagemErro('Pedido n�o encontrado. N�o foi poss�vel preencher as informa��es do Cliente.');
          exit;
     end;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     With Self.ObjMedidas_Proj.PedidoProjeto.Pedido do
     Begin
          QRpedidoPersiana.LbNome.Caption:=Cliente.Get_Nome;
          QRpedidoPersiana.LBEndereco.Caption:=Cliente.Get_Endereco+' - '+
                                       Cliente.Get_Bairro+' - '+
                                       Cliente.Get_Cidade;
          QRpedidoPersiana.LbContato.Caption:=Cliente.Get_Contato;
          QRpedidoPersiana.LbObra.Caption:=Get_Obra;
          if Cliente.Get_Fone <> '' then
          QRpedidoPersiana.LBTelefone.Caption:=Cliente.Get_Fone;
          if  Cliente.Get_Celular <>'' then
          QRpedidoPersiana.LBTelefone.Caption:=QRpedidoPersiana.LBTelefone.Caption+' - '+Cliente.Get_Celular;
          if  Cliente.Get_Fax <>'' then
          QRpedidoPersiana.LBTelefone.Caption:=QRpedidoPersiana.LBTelefone.Caption+' - '+Cliente.Get_Fax;

          QRpedidoPersiana.lbCNPJCPF.Caption:=Cliente.Get_CPF_CGC;
          QRpedidoPersiana.lbRGIE.Caption:=Cliente.Get_RG_IE;
          QRpedidoPersiana.LbVendedor.Caption:=Vendedor.Get_Nome;
     end;

end;

procedure TObjPedidoObjetos.ImprimePedidoControleEntrega(PPEdido: string);
begin
     if (Ppedido='')
     Then Begin
               Messagedlg('Escolha o Pedido que deseja imprimir',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.VerificaSeTemProjetosNoPedido(Ppedido)=false)then
     Begin
          MensagemErro('Nenhum projeto cadastrado neste pedido');
          exit;
     end;

     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(PPEdido)=false)then
     Begin
          MensagemErro('Pedido n�o localizado.');
          exit;
     end;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Concluido <> 'S')then
     Begin
          MensagemErro('Pedido Controle de Entrega n�o pode ser impresso antes de gerar a venda.');
          exit;
     end;


     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select Distinct(tabpedido_proj.codigo), TabMedidas_Proj.Largura, TabMedidas_Proj.Altura');
          sql.add('from tabpedido_proj');
          sql.add('left Join TabMedidas_Proj on TabMedidas_Proj.PedidoProjeto = TabPedido_proj.Codigo');
          sql.add('where TabPedido_proj.pedido = '+Ppedido);
          open;
          QRpedidoControleEntrega.DataSet:=Self.Objquery;

          Self.PreencheCabecalhoPedidoControleEntrega;
          Self.ResgataDadosClienteEVendedorPedidoControleEntrega(Ppedido);
          QRpedidoControleEntrega.LBProposta.Caption:='PROPOSTA N� '+Ppedido;
          QRpedidoControleEntrega.lbTitulo.Caption  :='T�TULO   N� '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Titulo;          
          QRpedidoControleEntrega.BandaDetail.BeforePrint:=Self.BandaDetailBeforePrintPedidoControleEntrega;
          QRpedidoControleEntrega.Preview;
     End;

end;

procedure TObjPedidoObjetos.BandaDetailBeforePrintPedidoControleEntrega(Sender: TQRCustomBand; var PrintBand: Boolean);
var
PPedidoPRojeto:string;
LinhaComplemento:string;
PNomeGrupoPerfilado:string;
begin

     //esse procedimento Imprime Os Dados do Projeto no Relatorio de pedidoCompacto
     PPedidoPRojeto:=Self.Objquery.fieldbyname('codigo').asstring;

     //resgatando a imagem atual
     Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PPedidoPRojeto);
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
     Try
       QRpedidoControleEntrega.QrImagemProjeto.Picture.Graphic:=nil;

       if (Fileexists(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.jpg'))
       Then  QRpedidoControleEntrega.QrImagemProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.jpg')

       Else
       begin
          if (Fileexists(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp'))
          Then  QRpedidoControleEntrega.QrImagemProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp')
          else  QRpedidoControleEntrega.QrImagemProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+'branco.bmp');
       end;
     Except
     End;
     {Try
       QRpedidoControleEntrega.QrImagemProjeto.Visible:=true;
       if (Fileexists(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp'))
       Then QRpedidoControleEntrega.QrImagemProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp')
       Else QRpedidoControleEntrega.QrImagemProjeto.Visible:=False;

     Except
     End; }

     //*********************************************
     //dados do projeto, na mesma linha da figura
     QRpedidoControleEntrega.LbPedidoProjeto.caption:=    'PEDIDO PROJETO: '+Self.ObjMedidas_Proj.PedidoProjeto.Get_Codigo;
     QRpedidoControleEntrega.LbReferenciaProjeto.caption:='PROJETO       : '+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia;
     QRpedidoControleEntrega.lbDescricao.Caption:=Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Descricao;
//     QRpedidoControleEntrega.LbComplemento.Caption:=Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Complemento;
//     QRpedidoControleEntrega.LbLocal.Caption:=Self.ObjMedidas_Proj.PedidoProjeto.Get_Local;


     // No vitrus funciona assim:
     // Se o eixo tem mais de uma medida os campos altura e largura
     //aparecem 0;
     if (UpperCase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo) = 'AXL')then
     Begin
         QRpedidoControleEntrega.lbAltura.Caption:='Altura: '+Self.Objquery.fieldbyname('Altura').asstring+' mm.';
         QRpedidoControleEntrega.lbLargura.Caption:='Largura: '+Self.Objquery.fieldbyname('Largura').asstring+' mm.';
     end else
     begin
         QRpedidoControleEntrega.lbAltura.Caption:='Altura: 0 mm.';
         QRpedidoControleEntrega.lbLargura.Caption:='Largura: 0 mm.';
     end;

     // Montando a linha de complemento
     if Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido = 'INSTALADO' then
     LinhaComplemento:='INSTALADO COM '
     else if Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido = 'FORNECIDO' then
     LinhaComplemento:='FORNECIDO COM '
     else if Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido = 'RETIRADO' then
     LinhaComplemento:='RETIRADO COM';

     if (Self.VerificaSeTemFerragem(PPedidoPRojeto) = true)then
     LinhaComplemento:=LinhaComplemento+'FERRAGENS, ';

     PNomeGrupoPerfilado:='';
     if (Self.VerificaGrupoPerfilado(PPedidoPRojeto, PNomeGrupoPerfilado) = true)then
     LinhaComplemento:=LinhaComplemento+PNomeGrupoPerfilado+', ';

     if (Self.VerificaSeTemServico(PPedidoPRojeto)=True)
     then LinhaComplemento:=LinhaComplemento+'SERVI�OS, ';

     if (Self.VerificaSeTemKitBox(PPedidoPRojeto)=True)
     then LinhaComplemento:=LinhaComplemento+'KITBOX, ';

     if (Self.VerificaSeTemPersiana(PPedidoPRojeto)=True)
     then LinhaComplemento:=LinhaComplemento+'PERSIANA, ';

     if (Self.VerificaSeTemDiversos(PPedidoPRojeto)=True)
     then LinhaComplemento:=LinhaComplemento+'DIVERSO, ';

     // Altura e Largura que agora est� numa tabela separada.


     QRpedidoControleEntrega.SB.Items.clear;
     QRpedidoControleEntrega.SB.Items.Add(LinhaComplemento);
     //+'CONFORME COR ESPECIFICADA ABAIXO.');

     // Colocando informacoes do Vidro
     LinhaComplemento:='';
     if (Self.RetornaInformacoesVidroPedidoControleEntrega(LinhaComplemento, PPedidoPRojeto)=false)then
     Begin
         MensagemErro('Vidro n�o encontrado.');
         exit;
     end;
     QRpedidoControleEntrega.SB.Items.Add(LinhaComplemento);
     QRpedidoControleEntrega.SB.Items.Add(' ');

end;

function TObjPedidoObjetos.RetornaInformacoesVidroPedidoControleEntrega(var PLinhaComplemento: string; PPedidoProjeto: string): Boolean;
Var QueryLocal : TIBQuery;
begin

       Result:=false;
       try
           QueryLocal:=TIBQuery.Create(nil);
           QueryLocal.Database:=FDataModulo.IBDatabase;
       except
            MensagemErro('Erro ao tentar criar a QueryLocal');
            exit;
       end;
try
       With QueryLocal do
       Begin
            close;
            Sql.Clear;
            Sql.Add('Select TabVidro.Descricao as NomeVidro,');
            Sql.Add('TabCor.Descricao as NomeCor,');
            Sql.Add('TabVidro_PP.Valor as ValorVidro,');
            Sql.Add('TabVidro_PP.Quantidade as QuantidadeVidro,');
            Sql.Add('TabPedido_Proj.ValorFinal as ValorFinalProjeto');
            Sql.Add('from TabVidro_PP');
            Sql.Add('join TabVidroCor on TabVidroCor.Codigo = TabVidro_PP.VidroCor');
            Sql.Add('join TabVidro on TabVidro.Codigo = TabVidroCor.Vidro');
            Sql.Add('join TabCor on TabCor.Codigo = TabVidroCor.Cor');
            Sql.Add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabVidro_PP.PEdidoProjeto');
            Sql.Add('Where TabVidro_PP.PedidoProjeto = '+PPedidoProjeto);
            if (PPedidoProjeto='')then
            exit;

            Open;


            if (RecordCount > 0)then
            Begin
                 PLinhaComplemento:=CompletaPalavra(fieldbyname('NomeVidro').AsString+' '+
                                                    fieldbyname('NomeCor').AsString,50,' ');//+' '+
                                    //CompletaPalavra(fieldbyname('QuantidadeVidro').AsString+' m�',15,' ')+' '+
                                    //CompletaPalavra('VALOR',6,' ')+' '+
                                    //CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('ValorVidro').AsString),10,' ')+' '+
                                    //CompletaPalavra('TOTAL',6,' ')+' '+
                                    //CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('ValorFinalProjeto').AsString),10,' ');
                 Result := true;
            end
            Else Result:=True;
       end;
finally
    FreeAndNil(QueryLocal);
end;
end;

function TObjPedidoObjetos.ResgataDadosClienteEVendedorPedidoControleEntrega(PPedido: string): Boolean;
begin
     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(PPedido)=false)then
     Begin
          MensagemErro('Pedido n�o encontrado. N�o foi poss�vel preencher as informa��es do Cliente.');
          exit;
     end;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     With Self.ObjMedidas_Proj.PedidoProjeto.Pedido do
     Begin
          QRpedidoControleEntrega.LbNome.Caption:=Cliente.Get_Nome;
          QRpedidoControleEntrega.LBEndereco.Caption:=Cliente.Get_Endereco+','+Cliente.Get_numero+' - '+
                                       Cliente.Get_Bairro+' - '+
                                       Cliente.Get_Cidade;
          QRpedidoControleEntrega.LbContato.Caption:=Cliente.Get_Contato;
          QRpedidoControleEntrega.LbObra.Caption:=Get_Obra;
          if Cliente.Get_Fone <> '' then
          QRpedidoControleEntrega.LBTelefone.Caption:=Cliente.Get_Fone;
          if  Cliente.Get_Celular <>'' then
          QRpedidoControleEntrega.LBTelefone.Caption:=QRpedidoControleEntrega.LBTelefone.Caption+' - '+Cliente.Get_Celular;
          if  Cliente.Get_Fax <>'' then
          QRpedidoControleEntrega.LBTelefone.Caption:=QRpedidoControleEntrega.LBTelefone.Caption+' - '+Cliente.Get_Fax;

          QRpedidoControleEntrega.lbCNPJCPF.Caption:=Cliente.Get_CPF_CGC;
          QRpedidoControleEntrega.lbRGIE.Caption:=Cliente.Get_RG_IE;
          QRpedidoControleEntrega.LbVendedor.Caption:=Vendedor.Get_Nome;
     end;

end;

procedure TObjPedidoObjetos.PreencheCabecalhoPedidoControleEntrega;
Var  PLocalLogoTipo: string;
begin
     With ObjParametroGlobal  do
     Begin
          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 1')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 1" n�o encontrado');
          end
          else QRpedidoControleEntrega.LBCabecalhoLinha1.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 2')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 2" n�o encontrado');
          end
          else QRpedidoControleEntrega.LBCabecalhoLinha2.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 3')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 3" n�o encontrado');
          end
          else QRpedidoControleEntrega.LBCabecalhoLinha3.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 4')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 4" n�o encontrado');
          end
          else QRpedidoControleEntrega.LBCabecalhoLinha4.Caption:=Get_Valor;

          PLocalLogoTipo:=ExtractFilePath(Application.exename)+'Images\Logo_Vidracaria.jpg';
          Try
              QRpedidoControleEntrega.QRImageLogotipo.Picture.LoadFromFile(PLocalLogoTipo);
          Except
              MensagemErro('Erro ao tentar localizar o Logotipo da Vidra�aria no endere�o:'+#13+PLocalLogoTipo);
          End;
     end;
end;

procedure TObjPedidoObjetos.ImprimePedidoProjetoBranco(PPedido: string);
begin
     if (Ppedido='')
     Then Begin
               Messagedlg('Escolha o Pedido que deseja imprimir',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.VerificaSeTemProjetosNoPedido(Ppedido)=false)then
     Begin
          MensagemErro('Nenhum projeto cadastrado neste pedido');
          exit;
     end;     

     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(Ppedido)=false)then
     Begin
          MensagemErro('Pedido n�o encontrado.');
          exit;
     end;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     QRpedidoProjetoBranco.QRImageLogotipo.Stretch:=False;
     if (ObjParametroGlobal.ValidaParametro('STRETCH NO LOGOTIPO DO PEDIDO')=True)
     then Begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               Then QRpedidoProjetoBranco.QRImageLogotipo.Stretch:=true;
     End;


     With Self.Objquery do
     Begin
          close;
          sql.clear;
          //sql.add('Select Distinct(tabpedido_proj.codigo), TabMedidas_Proj.Largura, TabMedidas_Proj.Altura');
         // sql.add('from tabpedido_proj');
          //sql.add('left Join TabMedidas_Proj on TabMedidas_Proj.PedidoProjeto = TabPedido_proj.Codigo');
          //sql.add('where TabPedido_proj.pedido = '+Ppedido);
          sql.add('Select * from ProcPedidoProjetoQtde('+Ppedido+')');
          open;

          QRpedidoProjetoBranco.DataSet:=Self.Objquery;


          Self.PreencheCabecalhoPedidoProjetoBranco;
          Self.ResgataDadosClienteEVendedorPedidoProjetoBranco(Ppedido);

          Self.ImprimePendencias_Pedido(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Titulo,QrPedidoProjetoBranco.QrMemoPendencias);

          QRpedidoProjetoBranco.LBProposta.Caption:='PROPOSTA N� '+Ppedido;
          QRpedidoProjetoBranco.lbTitulo.Caption  :='T�TULO   N� '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Titulo;
          QRpedidoProjetoBranco.BandaDetail.BeforePrint:=Self.BandaDetailBeforePrintProjetoBranco;

         // Atualizando os Valores Totais
          Self.ObjMedidas_Proj.PedidoProjeto.ZerarTabela;
          Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(Fieldbyname('Codigo').AsString);
          Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

          QRpedidoProjetoBranco.LbValorTotal.Caption:=    'Valor Total Pedido '+CompletaPalavra_a_Esquerda(formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorTotal), 15, ' ');
          QRpedidoProjetoBranco.LBValorAcrescimo.Caption:='Valor Acr�scimo    '+CompletaPalavra_a_Esquerda(formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorAcrescimo),15,' ');
          QRpedidoProjetoBranco.LBValorDesconto.Caption:= 'Valor Desconto     '+CompletaPalavra_a_Esquerda(formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorDesconto),15,' ');
          QRpedidoProjetoBranco.LBValorFinal.Caption:=    'Valor Final        '+CompletaPalavra_a_Esquerda(formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorFinal),15,' ');

          QRpedidoProjetoBranco.Preview;
     End;

end;

procedure TObjPedidoObjetos.BandaDetailBeforePrintPRojetoBranco(Sender: TQRCustomBand; var PrintBand: Boolean);
var
PPedidoPRojeto:string;
valorfinal, ptotal:Currency;

begin
     // Cabe�alho ITEM , DESCRICAO, QTDE, VALOR
     PPedidoPRojeto:=Self.Objquery.fieldbyname('codigo').asstring;

     QRpedidoProjetoBranco.lbITEMQTDEValor.Caption:='';
     QRpedidoProjetoBranco.lbITEMQTDEValor.Caption:=CompletaPalavra('�TEM',8,' ')+' '+
                            CompletaPalavra('COR',14,' ')+' '+
                            CompletaPalavra('DESCRI��O',39,' ')+' '+
                            'QTDE      VAL.UN.  VALOR';

     //resgatando os dados (ferragems,vidro, perfilado etc;...)
     QRpedidoProjetoBranco.SB.Items.clear;
     Self.ObjFerragem_PP.RetornaDadosRel2 (PpedidoProjeto,QRpedidoProjetoBranco.SB.Items,True);
     Self.ObjPerfilado_PP.RetornaDadosRel2(PpedidoProjeto,QRpedidoProjetoBranco.SB.Items,True);
     Self.ObjVidro_PP.RetornaDadosRel2    (PpedidoProjeto,QRpedidoProjetoBranco.SB.Items,True);
     Self.ObjKitBox_PP.RetornaDadosRel2   (PpedidoProjeto,QRpedidoProjetoBranco.SB.Items,True);
     Self.ObjServico_PP.RetornaDadosRel2  (PpedidoProjeto,QRpedidoProjetoBranco.SB.Items,True);
     Self.ObjPersiana_PP.RetornaDadosRel2 (PpedidoProjeto,QRpedidoProjetoBranco.SB.Items,True);
     Self.ObjDiverso_PP.RetornaDadosRel2  (PpedidoProjeto,QRpedidoProjetoBranco.SB.Items,True);
     //********************************************************************

     // Soma Projeto
     Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PPedidoPRojeto);
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
     QRpedidoPRojetoBranco.SB.Items.Add('?'+CompletaPalavra_a_Esquerda('TOTAL PROJETO: '+formata_valor(Self.ObjMedidas_Proj.PedidoProjeto.Get_ValorFinal),102,' '));
     QRpedidoProjetoBranco.SB.Items.Add('?'+CompletaPalavra_a_Esquerda('QUANTIDADE: '+formata_valor(Self.Objquery.fieldbyname('quantidade').AsString),102,' '));

     valorfinal:= StrToCurr(Self.ObjMedidas_Proj.PedidoProjeto.Get_ValorFinal)*Self.Objquery.fieldbyname('quantidade').AsCurrency;
     
     QRpedidoProjetoBranco.SB.Items.Add('?'+CompletaPalavra_a_Esquerda('VALOR FINAL: '+formata_valor(valorfinal),102,' '));


end;
procedure TObjPedidoObjetos.PreencheCabecalhoPedidoProjetoBranco;
Var  PLocalLogoTipo: string;
begin
     With ObjParametroGlobal  do
     Begin
          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 1')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 1" n�o encontrado');
          end
          else QRpedidoProjetoBranco.LBCabecalhoLinha1.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 2')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 2" n�o encontrado');
          end
          else QRpedidoProjetoBranco.LBCabecalhoLinha2.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 3')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 3" n�o encontrado');
          end
          else QRpedidoProjetoBranco.LBCabecalhoLinha3.Caption:=Get_Valor;

          If (ValidaParametro('CABE�ALHO DO PEDIDO LINHA 4')=false) then
          Begin
              MensagemErro('Parametro "CABE�ALHO DO PEDIDO LINHA 4" n�o encontrado');
          end
          else QRpedidoProjetoBranco.LBCabecalhoLinha4.Caption:=Get_Valor;

          PLocalLogoTipo:=ExtractFilePath(Application.exename)+'Images\Logo_Vidracaria.jpg';
          Try
              QRpedidoProjetoBranco .QRImageLogotipo.Picture.LoadFromFile(PLocalLogoTipo);
          Except
              MensagemErro('Erro ao tentar localizar o Logotipo da Vidra�aria no endere�o:'+#13+PLocalLogoTipo);
          End;
     end;
end;

function TObjPedidoObjetos.ResgataDadosClienteEVendedorPedidoProjetoBranco(
  PPedido: string): Boolean;
begin
     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(PPedido)=false)then
     Begin
          MensagemErro('Pedido n�o encontrado. N�o foi poss�vel preencher as informa��es do Cliente.');
          exit;
     end;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     With Self.ObjMedidas_Proj.PedidoProjeto.Pedido do
     Begin
          QRpedidoProjetoBranco.LbNome.Caption:=Cliente.Get_Nome;
          QRpedidoProjetoBranco.LBEndereco.Caption:=Cliente.Get_Endereco+' - '+
                                       Cliente.Get_Bairro+' - '+
                                       Cliente.Get_Cidade;
          QRpedidoProjetoBranco.LbContato.Caption:=Cliente.Get_Contato;
          QRpedidoProjetoBranco.LbObra.Caption:=Get_Obra;
          if Cliente.Get_Fone <> '' then
          QRpedidoProjetoBranco.LBTelefone.Caption:=Cliente.Get_Fone;
          if  Cliente.Get_Celular <>'' then
          QRpedidoProjetoBranco.LBTelefone.Caption:=QRpedidoProjetoBranco.LBTelefone.Caption+' - '+Cliente.Get_Celular;
          if  Cliente.Get_Fax <>'' then
          QRpedidoProjetoBranco.LBTelefone.Caption:=QRpedidoProjetoBranco.LBTelefone.Caption+' - '+Cliente.Get_Fax;

          QRpedidoProjetoBranco.lbCNPJCPF.Caption:=Cliente.Get_CPF_CGC;
          QRpedidoProjetoBranco.lbRGIE.Caption:=Cliente.Get_RG_IE;
          QRpedidoProjetoBranco.LbVendedor.Caption:=Vendedor.Get_Nome;
     end;

end;

function TObjPedidoObjetos.VerificaSeTemPerfilado(
  PPedidoProjeto: string): Boolean;

begin

     Result:=false;
     With Self.ObjqueryTEMP do
     Begin
          Try
            Close;
            SQL.Clear;
            Sql.Add('Select count(Codigo) as CONTA from TabPerfilado_PP');
            Sql.Add('Where PedidoProjeto = '+PPedidoProjeto);

            if (PPedidoProjeto<>'')
            then Open
            Else exit;

            if (Self.ObjqueryTEMP.fieldbyname('conta').asinteger > 0)
            then Result:=true
            else Result:=false;
            
          Finally
                 close;
          End;
     end;
end;

function TObjPedidoObjetos.VerificaSeTemVidro(PPedidoProjeto: string): Boolean;
begin

     Result:=false;
     With Self.ObjqueryTEMP do
     Begin
          Try
            Close;
            SQL.Clear;
            Sql.Add('Select count(Codigo) as CONTA from TabVidro_PP');
            Sql.Add('Where PedidoProjeto = '+PPedidoProjeto);
            if PPedidoProjeto<>''
            then Open
            Else exit;

            if (Fieldbyname('conta').asinteger> 0)
            then Result:=true
            else Result:=false;
          Finally
                 close;
          End;
     end;
end;

procedure TObjPedidoObjetos.ImprimePendencias_Pedido(Ptitulo: string;PMemo:TQRMemo);
var
Pcodigo,Phistorico,Pvencimento,Pvalor,Pobservacao:TstringList;
cont,contlinha:integer;
temp:string;
ObjPendencia:TobjPendencia;
begin
     //Esse Procedimento imprime as parcelas e suas observacoes no pedido
     //foi pedido para trabalhar com 3 pendencias por linha
     Pmemo.Lines.clear;
     Pmemo.Font.Name:='courier new';
     Pmemo.Font.Size:=8;

     if (Ptitulo='')
     Then exit;

     Try
        Pcodigo:=TStringList.create;
        Phistorico:=TStringList.create;
        Pvencimento:=TStringlist.create;
        Pvalor:=TStringList.create;
        Pobservacao:=TStringList.create;
        ObjPendencia:=TobjPendencia.create;
     except
           Messagedlg('Erro na tentativa de Criar as TStrings que ser�o usadas para resgatar os dados das pend�ncias para o relat�rio',mterror,[mbok],0);
           exit;
     End;

     Try
        //* * * * * * * * * * * * *BUSCANDO INFORMACOES DAS PENDENCIAS* * * * * * * * * * * * *
        ObjPendencia.Get_ListaCodigo(Pcodigo,ptitulo,'T');
        ObjPendencia.Get_ListaHistorico(Phistorico,Ptitulo,'T');
        ObjPendencia.Get_ListaVencimento(pVencimento,Ptitulo,'T');
        ObjPendencia.Get_ListaValor(pValor,Ptitulo,'T');
        ObjPendencia.Get_ListaObservacao(pobservacao,Ptitulo,'T');
        //*************************************************************************************

        if (Pcodigo.count>1)
        Then Begin
                  //titulo das colunas
                  temp:=CompletaPalavra('PEND',5,' ')+' '+CompletaPalavra('VENCTO',8,' ')+' '+CompletaPalavra_a_Esquerda('VALOR',11,' ')+' '+CompletaPalavra('OBSERVA��O',20,' ');
                  if Pcodigo.count>2
                  Then temp:=temp+' | '+temp;
                  Pmemo.Lines.add(temp);

        End;


        temp:='';
        contlinha:=0;
        For cont:=1 to Pcodigo.count-1 do
        Begin
             if (contlinha=2)
             Then Begin
                       Pmemo.Lines.add(temp);
                       contlinha:=0;
                       temp:='';
             end;
             if (temp<>'')
             Then temp:=temp+' | '+CompletaPalavra(pcodigo[cont],5,' ')+' '+CompletaPalavra(formatdatetime('dd/mm/yy',strtodate(Pvencimento[cont])),8,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(pvalor[cont]),11,' ')+' '+CompletaPalavra(pobservacao[cont],20,' ')
             Else temp:=           CompletaPalavra(pcodigo[cont],5,' ')+' '+CompletaPalavra(formatdatetime('dd/mm/yy',strtodate(Pvencimento[cont])),8,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(pvalor[cont]),11,' ')+' '+CompletaPalavra(pobservacao[cont],20,' ');
             inc(contlinha,1);
        end;

        Pmemo.Lines.add(temp);
        contlinha:=0;
        temp:='';

     finally
            Freeandnil(Pcodigo);
            Freeandnil(Phistorico);
            Freeandnil(Pvencimento);
            Freeandnil(Pvalor);
            Freeandnil(Pobservacao);
            ObjPendencia.free;
     end;


end;

function TObjPedidoObjetos.RetornaProjetoDoPedido( PPEdidoProjeto: string): string;
begin
     Result:='';
     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PPEdidoProjeto)=false)then
     Begin
          MensagemErro('Pedido Projeto n�o encontrado');
          exit;
     end;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
     Result:=Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Codigo;

end;


procedure TObjPedidoObjetos.PreencheObservacaoPedido(PObservacao: String);

begin
     TMPQRpedidoCompacto.QrMemoObservacao.Lines.Clear;
     TMPQRpedidoCompacto.QRMemoObservacao.Lines.Text:=PObservacao;
     TMPQRpedidoCompacto.QRMemoObservacao.Enabled:=true;

     TMPQRpedidoCompacto.BandSumario.Height:=TMPQRpedidoCompacto.BandSumario.Height+
                                                   TMPQRpedidoCompacto.QrMemoObservacao.Lines.Count*16;

    // TMPQRpedidoCompacto.ShapeTracoVendedor.Top:=TMPQRpedidoCompacto.ShapeTracoVendedor.Top+TMPQRpedidoCompacto.QrMemoObservacao.Lines.Count*16;
     //TMPQRpedidoCompacto.lbNomeVendedorAssinatura.Top:=TMPQRpedidoCompacto.lbNomeVendedorAssinatura.Top+TMPQRpedidoCompacto.QrMemoObservacao.Lines.Count*16;
      
end;

function TObjPedidoObjetos.ProjetoEmBrancoNoPedidoCompacto(PPedidoProjeto: string): Boolean;
Var
PTotal:Currency;
hora1,hora2:tdatetime;
pquantidade:integer;
begin
     //Esse procedimento imprime os detalhes de um projeto em branco dentro de um pedido compacto

     //04/09/2008-> Foi verificado que quando se replica um projeto em branco nao imprime dobrado
     //aqui nesse procedimento, funciona apenas para projetos que nao sao em branco

     pquantidade:=Self.Objquery.fieldbyname('quantidade').asinteger;

     PTotal:=0;
     Result:=false;

     TMPQRpedidoCompacto.BandaDetail.Height:=5;// Diminuo o tamanho da banda pra naum aparecer as informa��es

     //resgatando a imagem atual
     Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PPedidoPRojeto);
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     Try
       TMPQRpedidoCompacto.QrImagemProjeto.Visible:=True;
       if (FileExists(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp'))
       Then TMPQRpedidoCompacto.QrImagemProjeto.Picture.LoadFromFile(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataLocalDesenhos+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia+'.bmp')
       Else TMPQRpedidoCompacto.QrImagemProjeto.Visible:=False;
     Except
     End;

     //*********************************************
     //dados do projeto, na mesma linha da figura
     TMPQRpedidoCompacto.LbReferenciaProjeto.caption:='';//  'PROJETO: '+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Referencia;
     TMPQRpedidoCompacto.lbDescricao.Caption:='';//Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Descricao;
     TMPQRpedidoCompacto.LbLocal.Caption:='';//Self.ObjMedidas_Proj.PedidoProjeto.Get_Local;


     (*
     // No vitrus funciona assim:
     // Se o eixo tem mais de uma medida os campos altura e largura
     //aparecem 0;
     if (UpperCase(Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo) = 'AXL')then
     Begin*)
         TMPQRpedidoCompacto.lbAltura.Caption:='';
         TMPQRpedidoCompacto.lbLargura.Caption:='';
     (*end else
     begin
         TMPQRpedidoCompacto.lbAltura.Caption:='';//'Altura: 0 mm.';
         TMPQRpedidoCompacto.lbLargura.Caption:='';//'Largura: 0 mm.';
     end;*)

     TMPQRpedidoCompacto.SB.Items.clear;
             
     Self.ObjFerragem_PP.RetornaDadosRelProjetoBranco(PpedidoProjeto,TMPQRpedidoCompacto.SB.Items,pquantidade, PTotal);

     Self.ObjPerfilado_PP.RetornaDadosRelProjetoBranco(PpedidoProjeto,TMPQRpedidoCompacto.SB.Items, pquantidade,PTotal);

     Self.ObjVidro_PP.RetornaDadosRelProjetoBranco(PpedidoProjeto,TMPQRpedidoCompacto.SB.Items, pquantidade,PTotal);

     Self.ObjKitBox_PP.RetornaDadosRelProjetoBranco(PpedidoProjeto,TMPQRpedidoCompacto.SB.Items, pquantidade,PTotal);

     Self.ObjServico_PP.RetornaDadosRelProjetoBranco(PpedidoProjeto,TMPQRpedidoCompacto.SB.Items, pquantidade,PTotal);

     Self.ObjPersiana_PP.RetornaDadosRelProjetoBranco(PpedidoProjeto,TMPQRpedidoCompacto.SB.Items, pquantidade,PTotal);

     Self.ObjDiverso_PP.RetornaDadosRelProjetoBranco(PpedidoProjeto,TMPQRpedidoCompacto.SB.Items, pquantidade,PTotal);

     TMPQRpedidoCompacto.SB.Items.Add('?'+CompletaPalavra_a_Esquerda('TOTAL  '+formata_valor(PTotal),100,' '));
     //********************************************************************

     Result:=true;
End;

function TObjPedidoObjetos.VerificaSeTemProjetosNoPedido(PPedido:string): Boolean;
begin
      Result:=false;
      With  Self.Objquery do
      Begin
          close;
          Sql.Clear;
          Sql.Add('Select Count(Codigo) as Qtde from TabPedido_proj');
          Sql.Add('Where Pedido = '+PPedido);
          Open;

          if (fieldbyname('Qtde').AsInteger <> 0)then
          Result:=true
          else
          Result:=false;
      end;
end;

procedure TObjPedidoObjetos.BotaoOpcoes(pcodigo: string);
begin

     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Retornar Venda');
          RgOpcoes.Items.Add('Recalcular Proposta');
          Rgopcoes.items.add('Pesquisar Comiss�es do Colocador do Pedido Atual');
          RgOpcoes.Items.add('Cancelar Nota Fiscal deste Pedido');
          RgOpcoes.Items.Add('Gerar Ordem de Produ��o');
          RgOpcoes.Items.Add('Alterar cor do vidro nos projetos');

          showmodal;
          if (tag=0)
          Then exit;
          Case RgOpcoes.ItemIndex of
               0:Self.RetornarVenda(pcodigo);
               1:Self.RecalculaPedido(pcodigo);
               2:Self.MostraComissoesColocador(pcodigo);
               3:Self.CancelaNotaFiscal(pcodigo);
               4:self.GeraOrdemProducao(pcodigo);
               5:Self.AlterarCorVidroProjetos(pcodigo);
          End;
     End;
end;

Function TObjPedidoObjetos.GerarVenda(Pcodigo: string):boolean;
var
Objgeratitulo:Tobjgeratitulo;
ObjPendencia:TobjPendencia;
CodigoTitulo:string;
PComissaoVendedor:string;
ObjComissaoColocador:TobjComissaoColocador;
DataAtual:TDate;
PValorBonificacao1,PCreditoCliente1:Currency;
ObjlancamentoCredito:TObjLANCAMENTOCREDITO;
QueryLocal:TIBQuery;
begin
  result:=False;
  DataAtual:=Now;

  if (Pcodigo='')
  Then Begin
               Messagedlg('Escolha o pedido que deseja gerar a venda',mtinformation,[mbok],0);
               exit;
  End;

  if (ObjMedidas_Proj.PedidoProjeto.PEDIDO.LocalizaCodigo(pcodigo)=False)
  Then Begin
               Messagedlg('Pedido n�o localizado',mterror,[mbok],0);
               exit;
  End;

  QueryLocal:=TIBQuery.Create(nil);
  QueryLocal.Database:=FDataModulo.IBDatabase;
  try
         with QueryLocal do
         begin
              Close;
              SQL.Clear;
              SQL.Add('select codigo,concluido from tabordemservico where pedido='+Pcodigo);
              Open;
              if(FieldByName('concluido').AsString='N') then
              begin
                   MensagemAviso('N�o houve libera��o da Produ��o ainda / Ordem de Produ��o N� '+fieldbyname('codigo').AsString);
                   Exit;
              end;

         end;
  finally
         FreeAndNil(QueryLocal);
  end ;

  ObjMedidas_Proj.PedidoProjeto.PEDIDO.TabelaparaObjeto;

  if (ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_Concluido='S')
  or (ObjMedidas_Proj.PedidoProjeto.PEDIDO.get_Titulo<>'')
  Then Begin
              exit;
  End;

  if (Fdatamodulo.ValidaCPFCNPJ_venda_global=True)
  Then Begin
               if ((RetornaSoNUmeros(ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_CPF_CGC)='')
               or (come(RetornaSoNUmeros(ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_CPF_CGC),'0')=''))
               then Begin
                         mensagemerro('N�o � poss�vel fazer uma venda para um cliente sem CPF ou CNPJ');
                         exit;
               End;
  End;

  Try
        Objgeratitulo:=TObjGeraTitulo.create;
        ObjPendencia:=TobjPendencia.Create;
        ObjlancamentoCredito:=TObjLANCAMENTOCREDITO.Create;
  Except
           messagedlg('Erro na tentativa de Cria��o do Objeto de T�tulo e Gera��o de T�tulo!',mterror,[mbok],0);
           exit;
  End;

  Try

        if (Self.ValidaDescontomaximo=False)
        then exit;


        If (Objgeratitulo.LocalizaHistorico('TITULO GERADO PELO PEDIDO')=False)
        Then Begin
                  Messagedlg('O GERATITULO de hist�rico="TITULO GERADO PELO PEDIDO" n�o foi encontrado!',mterror,[mbok],0);
                  exit;
        End;
        Objgeratitulo.TabelaparaObjeto;

        ObjPendencia.Titulo.ZerarTabela;
        ObjPendencia.Titulo.Status:=dsinsert;
        codigotitulo:=ObjPendencia.Titulo.get_novocodigo;
        ObjPendencia.Titulo.Submit_CODIGO(codigotitulo);
        ObjPendencia.Titulo.Submit_HISTORICO('VENDA PARA '+ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome+' - N� '+ObjMedidas_Proj.PedidoProjeto.Pedido.Get_codigo);
        ObjPendencia.Titulo.Submit_GERADOR(Objgeratitulo.Get_Gerador);
        ObjPendencia.Titulo.Submit_CODIGOGERADOR(Objgeratitulo.Get_CodigoGerador);
        ObjPendencia.Titulo.Submit_CREDORDEVEDOR(Objgeratitulo.Get_CredorDevedor);
        ObjPendencia.Titulo.Submit_CODIGOCREDORDEVEDOR(ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.get_codigo);
        ObjPendencia.Titulo.Submit_EMISSAO(Datetostr(dataatual));
        ObjPendencia.Titulo.Submit_PRAZO(Objgeratitulo.Get_Prazo);
        ObjPendencia.Titulo.Submit_PORTADOR(Objgeratitulo.get_portador);
        ObjPendencia.Titulo.Submit_VALOR(ObjMedidas_Proj.PedidoProjeto.Pedido.get_ValorFinal);
        ObjPendencia.Titulo.Submit_CONTAGERENCIAL(Objgeratitulo.Get_ContaGerencial);
        ObjPendencia.Titulo.Submit_NUMDCTO(Pcodigo);//vai o numero do pedido
        ObjPendencia.Titulo.Submit_GeradoPeloSistema('N');
        ObjPendencia.Titulo.Submit_ParcelasIguais(True);
        if (ObjPendencia.Titulo.salvar(False,False)=False)//nao exporta a contabilidade pois vamos exportar com estoque por esse modulo
        Then Begin
                  Messagedlg('Erro na tentativa de Gerar o T�tulo',mterror,[mbok],0);
                  exit;
        End;

        //Colocando Concluido no Pedido
        ObjMedidas_Proj.PedidoProjeto.Pedido.Submit_Titulo(codigotitulo);
        ObjMedidas_Proj.PedidoProjeto.PEDIDO.Status:=dsEdit;
        ObjMedidas_Proj.PedidoProjeto.PEDIDO.Submit_Concluido('S');
        //Deixar data do pedido e n�o data atual ao alterar a venda
        //25/04/2011
        ObjMedidas_Proj.PedidoProjeto.PEDIDO.Submit_Data(datetostr(dataatual));
        ObjMedidas_Proj.PedidoProjeto.PEDIDO.Submit_ValorTitulo(ObjMedidas_Proj.PedidoProjeto.PEDIDO.get_valorfinal);

        if (ObjMedidas_Proj.PedidoProjeto.PEDIDO.Salvar(False)=False)
        Then Begin
                  MEssagedlg('Erro na tentativa de Gravar N�mero do T�tulo no Pedido',mterror,[mbok],0);
                  exit;
        End;

        //Comissao do Vendedor
        if (Self.GeraComissaoPedido(Pcodigo,PComissaoVendedor)=false)
        Then Begin
                  FDataModulo.IBTransaction.Rollback;
                  Messagedlg('Erro no Lan�amento da comiss�o dos vendedores',mterror,[mbok],0);
                  exit;
        End;

        try
              ObjComissaoColocador:=TobjComissaoColocador.Create;
        Except
              FDataModulo.IBTransaction.Rollback;
              Messagedlg('Erro na tentativa de Criar o Objeto de Comiss�o do Colocador',mterror,[mbok],0);
              exit;
        End;

        Try

            //Comissao do Colocador
            //revisar
            if (ObjComissaoColocador.GeraComissaoPedido(Pcodigo,PcomissaoVendedor)=false)
            Then Begin
                      FDataModulo.IBTransaction.Rollback;
                      Messagedlg('Erro no Lan�amento da comiss�o dos Colocadores',mterror,[mbok],0);
                      exit;
            End;
        Finally
               ObjComissaoColocador.Free;
        End;



        ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.LocalizaCodigo(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.get_codigo);
        ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.TabelaparaObjeto;

        PValorBonificacao1:=StrTofloat(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_ValorBonificacaoCliente);
        if(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Get_ValorCredito<>'')
        then PCreditoCliente1:=Strtofloat(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Get_ValorCredito);

        if (PValorBonificacao1>0)//se tiver bonificacao, diminuo a bonificaco do cliente
        Then begin
                  if (PCreditoCliente1<PValorBonificacao1)

                  then Begin
                            MensagemErro('A bonifica��o est� superior ao cr�dito do Cliente'+#13+'Cr�dito Atual '+formata_valor(PcreditoCliente1));
                            exit;
                  End;

                  {ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Status:=dsedit;
                  ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Submit_ValorCredito(floattostr(PCreditoCliente1-PValorBonificacao1));
                  if (ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Salvar(False)=False)
                  Then begin
                            MensagemErro('Erro na tentativa de Diminuir o Valor do Cr�dito do Cliente');
                            exit;
                  End; }

                  ObjlancamentoCredito.ZerarTabela;
                  ObjlancamentoCredito.Status:=dsInsert;
                  ObjlancamentoCredito.Submit_Codigo('0');
                  ObjlancamentoCredito.Submit_Data(DateToStr(Now));
                  ObjlancamentoCredito.Submit_Cliente(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Get_Codigo);
                  ObjlancamentoCredito.Submit_Valor(Floattostr(PValorBonificacao1 * (-1)));
                  ObjlancamentoCredito.Submit_Pedido(Pcodigo);
                  ObjlancamentoCredito.Submit_Historico('Bonifica��o gerada no retorno de vendas');
                  if(ObjlancamentoCredito.Salvar(False)=False)
                  then Exit;

        End;

        if (Self.ExportaContabilidade_Financeiro_Venda(Pcodigo)=False)
        then begin
                  MensagemErro('Erro na tentativa de gerar a contabilidade Financeira do Pedido');
                  exit;
        End;


        //Primeiro Commit, pois o lancamento da observacao
        //pode demorar e assim travaria tabela de titulo e pendencia
        FDataModulo.IBTransaction.CommitRetaining;
        result:=True;

        //Lancando a Observacao nas pendencias
        if (ObjPENDENCIA.Lanca_Observacao_titulo(CodigoTitulo)=False)
        Then begin
                 Messagedlg('Erro no Lan�amento da Observa��o das Parcelas',mterror,[mbok],0);
                 exit;
        end;


        FDataModulo.IBTransaction.CommitRetaining;

  Finally
           FDataModulo.IBTransaction.RollbackRetaining;
           ObjlancamentoCredito.Free;
           Objgeratitulo.Free;
           ObjPendencia.Free;
  end;


End;

procedure TObjPedidoObjetos.RetornarVenda(Pcodigo: string);
var
ObjLancamento:TobjLancamento;
PcodigoTitulo:string;
pBonificacaoGerada1,PValorBonificacao1,PCreditoCliente1:Currency;
Query:TIBQuery;
Numpedido:string;
ObjlancamentoCredito:TObjLANCAMENTOCREDITO;
Begin
     try
         query:=TIBQuery.Create(nil);
         Query.Database:=FDataModulo.IBDatabase;
         ObjlancamentoCredito:=TObjLANCAMENTOCREDITO.Create;
     except

     end;


     Try
         with query do
         begin

              Close;
              sql.Clear;
              SQL.Add('select numpedido from tabnotafiscal where numpedido ='+#39+Pcodigo+#39);
              SQL.Add('and situacao=''I''') ;
              Open;
              Numpedido:=fieldbyname('numpedido').AsString;

         end;

         if(Pcodigo= Numpedido) then
         begin
                Messagedlg('J� existem notas geradas para este pedido',mtinformation,[mbok],0);
                Messagedlg('N�o foi poss�vel retornar esta venda',mtinformation,[mbok],0);
                Exit;
         end;
            //Houve altera��es, pensar numa forma de reotornar este pedido, ja
            //que se deletar pode haver erros na tablancamentocredito
            {with Query do
            begin
                   Close;
                   SQL.clear;
                   sql.Add('delete from tabmateriaistroca where pedido='+Pcodigo);
                   ExecSQL;

                   Close;
                   sql.Clear;
                   sql.Add('delete from tabmateriaisdevolucao where pedido='+Pcodigo);
                   ExecSQL;

                   Close;
                   sql.Clear;
                   sql.Add('delete from tablancamentocredito where pedido='+Pcodigo);
                   ExecSQL;
            end;   }



        if (ObjPermissoesUsoGlobal.ValidaPermissao('RETORNAR PEDIDO')=False)
        then exit;

        if (Pcodigo='')
        Then Begin
                  Messagedlg('Escolha o pedido que deseja retornar a venda',mtinformation,[mbok],0);
                  exit;
        End;


        if (ObjMedidas_Proj.PedidoProjeto.PEDIDO.LocalizaCodigo(pcodigo)=False)
        Then Begin
                  Messagedlg('Pedido n�o localizado',mterror,[mbok],0);
                  exit;
        End;
        ObjMedidas_Proj.PedidoProjeto.PEDIDO.TabelaparaObjeto;




        if (ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_Concluido<>'S')
        or (ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_Titulo='')
        Then Begin
                  Messagedlg('Este pedido n�o foi conclu�do, n�o � poss�vel retornar a venda',mtinformation,[mbok],0);
                  exit;
        End;

        //verificando se tem alfgum pedido/projeto desse pedido que esteja ligado a algum romaneio
        if (self.VerificaPPRomaneioporPedido(pcodigo)=False)
        Then Begin
                  FDataModulo.IBTransaction.RollbackRetaining;
                  Messagedlg('N�o � poss�vel retornar este pedido pois existem pedidos/projetos ligados a romaneios de entrega',mterror,[mbok],0);
                  exit;
        End;

        //passando para concluido ='N'
        PCodigoTitulo:=ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_Titulo;
        ObjMedidas_Proj.PedidoProjeto.PEDIDO.Status:=dsedit;
        ObjMedidas_Proj.PedidoProjeto.PEDIDO.Submit_Titulo('');
        ObjMedidas_Proj.PedidoProjeto.PEDIDO.Submit_Concluido('N');
        ObjMedidas_Proj.PedidoProjeto.PEDIDO.Submit_AlteradoporTroca('N');
        ObjMedidas_Proj.PedidoProjeto.PEDIDO.Submit_Valortitulo('0');

        //nao verifico o desconto, pois esse pedido pode ter sofrido troca
        if (ObjMedidas_Proj.PedidoProjeto.PEDIDO.Salvar(False,false)=False)
        Then Begin
                  FDataModulo.IBTransaction.RollbackRetaining;
                  Messagedlg('Erro na Tentativa de Salvar o Pedido!',mterror,[mbok],0);
                  exit;
        End;

        //retornando as comissoes de vendedor
       if (Self.RetornaComissaoVendedorPedido(Pcodigo)=False)
       then Begin
                 FDataModulo.IBTransaction.RollbackRetaining;
                 Messagedlg('Erro na tentativa de Excluir a comiss�o do Vendedor!',mterror,[mbok],0);
                 exit;
       End;



       //retornando as comissoes de colocador
       if (Self.RetornaComissaoColocadorPedido(Pcodigo)=False)
       then Begin
                 FDataModulo.IBTransaction.RollbackRetaining;
                 Messagedlg('Erro na tentativa de Excluir a comiss�o do Colocador!',mterror,[mbok],0);
                 exit;
       End;

       Try
          ObjLancamento:=TObjLancamento.Create;
       Except
             Messagedlg('Erro na tentativa de Criar o Objeto de Lan�amento',mterror,[mbok],0);
             exit;
       End;

       Try
           if (ObjLancamento.ExcluiTitulo(PcodigoTitulo,False)=False)
           Then Begin
                     FDataModulo.IBTransaction.RollbackRetaining;
                     Messagedlg('Erro na tentativa de Excluir o T�tulo!',mterror,[mbok],0);
                     exit;
           End;
       Finally
               ObjLancamento.Free;
       End;

       ObjMedidas_Proj.PedidoProjeto.PEDIDO.LocalizaCodigo(Pcodigo);
       ObjMedidas_Proj.PedidoProjeto.PEDIDO.TabelaparaObjeto;


       PValorBonificacao1:=StrTofloat(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_ValorBonificacaoCliente);
       //PCreditoCliente1:=Strtofloat(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Get_ValorCredito);
       pBonificacaoGerada1:=Strtofloat(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_BonificacaoGerada);


       if (PValorBonificacao1>0)//se tiver bonificacao, aumento a bonificaco do cliente
       Then begin//porque quando eu uso diminui
                 //Antes o credito do cleinte era gravado direto na tabcliente
                 //agora � feito um lanc�amento de credito pro cliente na tablancamentocredito

                 {ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Status:=dsedit;
                 ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Submit_ValorCredito(floattostr(PCreditoCliente1+PValorBonificacao1));
                 if (ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Salvar(False)=False)
                 Then begin
                           MensagemErro('Erro na tentativa de Aumentar o Valor do Cr�dito do Cliente referente ao Desconto de Bonifica��o usado nesse pedido');
                           exit;
                 End;  }

                 ObjlancamentoCredito.ZerarTabela;
                 ObjlancamentoCredito.Status:=dsInsert;
                 ObjlancamentoCredito.Submit_Codigo('0');
                 ObjlancamentoCredito.Submit_Data(DateToStr(Now));
                 ObjlancamentoCredito.Submit_Cliente(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Get_Codigo);
                 ObjlancamentoCredito.Submit_Valor(Floattostr(PValorBonificacao1));
                 ObjlancamentoCredito.Submit_Pedido(Pcodigo);
                 ObjlancamentoCredito.Submit_Historico('Bonifica��o gerada no retorno de vendas');
                 if(ObjlancamentoCredito.Salvar(False)=False)
                 then Exit;
       End;

       if (PBonificacaogerada1>0)//teve bonificaco no pedido ou seja (aumento o credito do cliente em alguma troca)
       Then Begin
                 //Exemplo
                 //Fiz um pedido de 10.000 e paguei
                 //fiz uma troca e o pedido passou para 9.000
                 //entao gerou uma bonificacao de 1.000 pro cliente
                 //Se eu retornar esse pedido, preciso tirar esse 1.000 do cliente
                 //e zerar o campo do pedido que contem essa bonificacoa, porque
                 //como retornei o financeiro tmbm, o pedido passa a valer 9.000
                 //e nao mais 10.000 porque soh te 9.000 de produtos.

                 //Antes o credito do cleinte era gravado direto na tabcliente
                 //agora � feito um lanc�amento de credito pro cliente na tablancamentocredito

                 
                 {ObjMedidas_Proj.PedidoProjeto.PEDIDO.LocalizaCodigo(Pcodigo);
                 ObjMedidas_Proj.PedidoProjeto.PEDIDO.TabelaparaObjeto;
                 ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Status:=dsedit;
                 //pegando o credito de novo porque ele pode ter sido mexido na rotina acima
                 PCreditoCliente1:=Strtofloat(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Get_ValorCredito);
                 ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Submit_ValorCredito(floattostr(PCreditoCliente1-pBonificacaoGerada1));

                 if ((PCreditoCliente1-pBonificacaoGerada1)<0)
                 then begin
                           if (messagedlg('O cr�dito do Cliente se tornar� negativo em R$ '+formata_valor(PCreditoCliente1-pBonificacaoGerada1)+#13+'Certeza que deseja retornar esse pedido?',mtconfirmation,[mbyes,mbno],0)=mrno)
                           then exit;
                 End;

                 if (ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Salvar(False)=False)
                 Then begin
                           MensagemErro('Erro na tentativa de Diminuir o Valor do Cr�dito do Cliente referente a Bonifica��o contida no produto');
                           exit;
                 End;   }
                ObjlancamentoCredito.ZerarTabela;
                ObjlancamentoCredito.Status:=dsInsert;
                ObjlancamentoCredito.Submit_Codigo('0');
                ObjlancamentoCredito.Submit_Data(DateToStr(Now));
                ObjlancamentoCredito.Submit_Cliente(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Cliente.Get_Codigo);
                ObjlancamentoCredito.Submit_Valor(Floattostr(PValorBonificacao1*(-1)));
                ObjlancamentoCredito.Submit_Pedido(Pcodigo);
                ObjlancamentoCredito.Submit_Historico('Bonifica��o gerada no retorno de vendas');
                if(ObjlancamentoCredito.Salvar(False)=False)
                then Exit;


                 //Retirando a Bonificacao do Pedido
                 ObjMedidas_Proj.PedidoProjeto.PEDIDO.LocalizaCodigo(Pcodigo);
                 ObjMedidas_Proj.PedidoProjeto.PEDIDO.TabelaparaObjeto;
                 ObjMedidas_Proj.PedidoProjeto.PEDIDO.Status:=dsedit;
                 ObjMedidas_Proj.PedidoProjeto.PEDIDO.Submit_BonificacaoGerada('0');
                 //salvo sem verificar o desconto, pois o pedido pode ter sido alterado por troca
                 if (ObjMedidas_Proj.PedidoProjeto.PEDIDO.Salvar(False,false)=False) Then
                 Begin
                      MensagemErro('Erro na tentativa de retirar o valor da Bonifica��o do Cliente no Pedido');
                      exit;
                 End;
       End;

       With Self.ObjqueryTEMP do
       begin
            //verificando se a contabilidade foi exportada
            close;
            sql.clear;
            sql.add('Select count(codigo) as CONTA from tabExportaContabilidade');
            sql.add('where Objgerador='+#39+'OBJPEDIDO'+#39);
            sql.add('and Codgerador='+Pcodigo);
            sql.add('and Exportado=''S'' ');

            Try
               open;
               if (fieldbyname('conta').asinteger>0)
               then begin
                         if (MessageDlg('Alguns registros da contabilidade foram exportados, certeza que deseja retornar o pedido?',mtConfirmation,[mbyes,mbno],0)=mrno)
                         then exit;

                         if (ObjexportaContabilidade.LancaInverso('OBJPEDIDO',Pcodigo,datetostr(now))=False)
                         then begin
                                   mensagemerro('N�o foi poss�vel lan�ar o inverso na contabilidade');
                                   exit;
                         End;
               End
               Else Begin
                         //apagando a contabilidade do estoque e do financeiro
                         close;
                         sql.clear;
                         sql.add('Delete from tabExportaContabilidade');
                         sql.add('where Objgerador='+#39+'OBJPEDIDO'+#39);
                         sql.add('and Codgerador='+Pcodigo);
                         execsql;
               End;
            except
               MensagemErro('N�o foi poss�vel excluir a contabilidade do Pedido');
               exit;
            End;
       End;

       self.ApagaVendaDaTabvendas(Pcodigo);
       self.ApagaMateriasDaVenda(Pcodigo);
       FDataModulo.IBTransaction.CommitRetaining;


      // Messagedlg('Pedido retornado com Sucesso!',mtInformation,[mbok],0);
     finally
            FDataModulo.IBTransaction.RollbackRetaining;
            FreeAndNil(Query);
            ObjlancamentoCredito.Free;
     End;
End;

function TObjPedidoObjetos.MarcaLinhaVidroAoRecalcular(PPEdidoProjeto: string): Boolean;
Var PVidroCor : string;
    Cont:Integer;
begin
     // Funcao para marcar a linha do form Escolhe vidro caso
     // seja um recalculo. Senao nao deve marcar nenhuma.

     Result:=false;
     try
         PVidroCor:=Self.ObjVidro_PP.RetornaVidroCor(PPEdidoProjeto);

         //Se Naum existir vidro_Cor entaum n�o marco nenhuma.
         if (PVidroCor = '') then
         Begin
             FescolheVidro.STRGGRID.Row:=1;
             FescolheVidro.STRGGRID.Col:=1;
             Result:=true;
             exit;
         end;


         // Para marcar eu localizo a Celula onde tem
         // o VidroCor , sempre ser� a Primeira coluna
         for Cont:=0 to FescolheVidro.STRGGRID.RowCount-1 do
         Begin
              if (FescolheVidro.STRGGRID.Cells[0,Cont] = PVidroCor)then
              Begin
                    FescolheVidro.STRGGRID.Row:=Cont;
                    FescolheVidro.STRGGRID.Col:=2;// Sempre marcarei a coluna referencia
                    Result:=true;
                    exit;
              end;
         end;

     except
         Result:=false;
     end;
end;


function TObjPedidoObjetos.ApagaProjetosReplicados(PPedidoProjetoPrincipal: string): Boolean;
Var  Querylocal:TIBQuery;
begin

try
    try
          Querylocal := TIBQuery.Create(nil);
          Querylocal.Database:=FDataModulo.IBDatabase;
    except
          MensagemErro('Erro ao tentar criar a Query local');
          result:=false;
          exit;
    end;


    Result:=false;
    With  Querylocal do
    Begin
         Close;
         SQL.Clear;
         Sql.Add('Select Codigo from TabPedido_Proj');
         Sql.Add('Where PedidoProjetoPrincipal = '+PPedidoProjetoPrincipal);
         Open;

         if (RecordCount = 0)then
         Begin
              Result:=false;
              exit;
         end;


         While not (Eof) do
         Begin
              if (Self.ExcluiPedidoProjeto(fieldbyname('Codigo').AsString)=false) then
              Begin
                   MensagemErro('Erro ao tentar excluir o pedido projeto n� '+fieldbyname('Codigo').AsString);
                   Result:=false;
                   exit;
              end;
              Next;
         end;

         Result:=true;
    end;

finally
    FreeAndNil(Querylocal);
end;

end;

function TObjPedidoObjetos.ApagaPedidoProjetoRepliocadosERecria(PPedidoProjetoPrincipal: string):Boolean;
Var PQuantidade : Integer;
begin
// Ao alter aum pedido projeto principal o sistema dever� apagar todos os pedidos projetos derivados
//e recri�-los. Preciso descobrir quantos pedidos projeto foram replicados;
Result:=false;

try
    With  Self.Objquery  do
    Begin
         Close;
         SQl.Clear;
         SQL.Add('Select Count(Codigo)  as Quantidade from TabPedido_Proj');
         SQL.Add('Where PedidoProjetoPrincipal = '+PPedidoProjetoPrincipal);
         Open;

         PQuantidade:=fieldbyname('Quantidade').AsInteger;


         // Se o cara relmente quizer alterar entaum eu preciso:
         //1� Gravo a Altera��o feita no projeto principal (esta j� foi feira antes de chamar este procedimento)
         //2� Apago seus derivados
         //3� Rescrio os projetos


         //*****Apagando os projetos replicados
         if (Self.ApagaProjetosReplicados(PPedidoProjetoPrincipal)=false) then
         Begin
              MensagemErro('Erro ao tentar agapar os projetos replicados');
              Result:=false;
         end;


         //***** Criando os projtos novamente
         if (Self.ReplicaProjeto(PPedidoProjetoPrincipal, PQuantidade)=false) then
         exit;

         Result:=true;
    end;
except
    Result:=false;
end;

end;

procedure TObjPedidoObjetos.ReplicarProjetoPublico(pedidoprojeto:string;quantidade:integer);
begin
    if(ReplicaProjeto(PedidoProjeto,Quantidade)=False)then
    begin
          MensagemErro('Erro ao tentar replicar o projeto');
          Exit;
    end;
end;

function TObjPedidoObjetos.ReplicaProjeto(PcodigoPedidoProjeto :string; PQuantidade:Integer):Boolean;
var
cont:Integer;
PNovoPedidoProjeto:string;
begin

Result:= false;

    if (PcodigoPedidoProjeto='')
    then Begin
              Messagedlg('Escolha o projeto que deseja replicar',mterror,[mbok],0);
              exit;
    End;

    if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PcodigoPedidoProjeto)=False)
    Then begin
              Messagedlg('Projeto n�o encontrado!',mterror,[mbok],0);
              exit;
    End;
    Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

Try

    try
        FMostraBarraProgresso.ConfiguracoesIniciais(8,0);
        FMostraBarraProgresso.lbmensagem.caption:='Gravando!';
        FMostraBarraProgresso.Lbmensagem2.caption:='Gravando!';

        FMostraBarraProgresso.IncrementaBarra1(1);
        FMostraBarraProgresso.Show;
        for cont:=1 to PQuantidade do
        begin
            //criando um novo pedidoprojeto com os dados do escolhido
            Self.ObjMedidas_Proj.PedidoProjeto.Status:=dsinsert;
            Self.ObjMedidas_Proj.PedidoProjeto.Submit_Codigo('0');
            Self.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal(PcodigoPedidoProjeto);

            if (Self.ObjMedidas_Proj.PedidoProjeto.Salvar(False)=False)
            Then Begin
                      Messagedlg('Erro na tentativa de Criar um Novo Projeto para este pedido',mterror,[mbok],0);
                      exit;
            End;

            //inserir os dados via sql
            With self.Objquery do
            Begin
                 PNovoPedidoProjeto:='';
                 PNovoPedidoProjeto:=Self.ObjMedidas_Proj.PedidoProjeto.get_codigo;
                 //********************* Ferragem **************************
                 close;
                 sql.clear;
                 sql.add('Insert Into TABFerragem_PP(Codigo,FerragemCor');
                 sql.add(',PedidoProjeto,Quantidade,Valor)');
                 sql.add('Select gen_id(genFerragem_pp,1),FerragemCor ,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from TabFerragem_pp');
                 sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                 Try
                    execsql;
                 Except
                       Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                       exit;
                 End;

                 //********************* Perfilado **************************
                 close;
                 sql.clear;
                 sql.add('Insert Into TABPerfilado_PP(Codigo,PerfiladoCor');
                 sql.add(',PedidoProjeto,Quantidade,Valor)');
                 sql.add('Select gen_id(genPerfilado_pp,1),PerfiladoCor ,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from TabPerfilado_pp');
                 sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                 Try
                    execsql;
                 Except
                       Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                       exit;
                 End;

                 //********************* Vidro **************************
                 close;
                 sql.clear;
                 sql.add('Insert Into TABVidro_PP(Codigo,VidroCor');
                 sql.add(',PedidoProjeto,Quantidade,Valor)');
                 sql.add('Select gen_id(genVidro_pp,1),VidroCor ,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from TabVidro_pp');
                 sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                 Try
                    execsql;
                 Except
                       Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                       exit;
                 End;

                 //********************* KitBox **************************
                 close;
                 sql.clear;
                 sql.add('Insert Into TABKitBox_PP(Codigo,KitBoxCor');
                 sql.add(',PedidoProjeto,Quantidade,Valor)');
                 sql.add('Select gen_id(genKitBox_pp,1),KitBoxCor ,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from TabKitBox_pp');
                 sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                 Try
                    execsql;
                 Except
                       Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                       exit;
                 End;

                 //********************* Servico **************************
                 close;
                 sql.clear;
                 sql.add('Insert Into TABServico_PP(Codigo,Servico');
                 sql.add(',PedidoProjeto,Quantidade,Valor)');
                 sql.add('Select gen_id(genServico_pp,1),Servico ,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from TabServico_pp');
                 sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                 Try
                    execsql;
                 Except
                       Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                       exit;
                 End;


                 //********************* Persiana **************************
                 close;
                 sql.clear;
                 sql.add('Insert Into TABPERSIANA_PP(Codigo,PersianaGrupoDiametroCor');
                 sql.add(',PedidoProjeto,Quantidade,Valor)');
                 sql.add('Select gen_id(genpersiana_pp,1),PersianaGrupoDiametroCor ,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from Tabpersiana_pp');
                 sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                 Try
                    execsql;
                 Except
                       Messagedlg('Erro na tentativa de Inserir as Persianas',mterror,[mbok],0);
                       exit;
                 End;


                 //********************* Diverso **************************
                 close;
                 sql.clear;
                 sql.add('Insert Into TABDiverso_PP(Codigo,DiversoCor');
                 sql.add(',PedidoProjeto,Quantidade,Valor)');
                 sql.add('Select gen_id(genDiverso_pp,1),DiversoCor,'+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo+',Quantidade,Valor from TabDiverso_pp');
                 sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                 Try
                    execsql;
                 Except
                       Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                       exit;
                 End;

                 //********************* Medidas **************************
                 //Se o o eixo do Projeto for Diferente de AxL n�o precisa gravas as medida, pois
                 //n�o saira no pedido Impresso
                 if (Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Eixo = 'AXL')then
                 Begin
                       if (Self.ObjMedidas_Proj.LocalizaPorCodigoPedidoProjeto(PcodigoPedidoProjeto)=false)then
                       Begin
                            MensagemErro('Pedido Projeto n�o encontrado na Tabela de Medidas');
                            exit;
                       end;
                       Self.ObjMedidas_Proj.TabelaparaObjeto;

                       close;
                       sql.clear;
                       sql.add('Insert Into TABMedidas_Proj(Codigo,PedidoProjeto,');
                       sql.add('Componente,Altura, Largura)');
                       sql.add('Select gen_id(genMedidas_Proj,1),'+PNovoPedidoProjeto+','+#39+Self.ObjMedidas_Proj.Get_Componente+#39+',');
                       sql.Add(Self.ObjMedidas_Proj.Get_Altura+','+Self.ObjMedidas_Proj.Get_Largura);
                       sql.Add('from TabMedidas_proj');
                       sql.add('where pedidoprojeto='+Pcodigopedidoprojeto);
                       Try
                          execsql;
                       Except
                             Messagedlg('Erro na tentativa de Inserir as Ferragens',mterror,[mbok],0);
                             exit;
                       End;
                 end;

           End;

           //atualiza o valor do projeto
           if (Self.AtualizavalorProjeto(PcodigoPedidoProjeto)=False)
           Then Begin
                      Messagedlg('Erro na Tentativa de Gravar o Valor Total nesse Projeto',mterror,[mbok],0);
                      exit;
           End;
           
            FMostraBarraProgresso.IncrementaBarra1(1);
            FMostraBarraProgresso.Show;

        End;
        Result:=True;
    except
        Result:=false;
    end;

Finally
       FDataModulo.IBTransaction.RollbackRetaining;
       FMostraBarraProgresso.Close;
End;


end;

procedure TObjPedidoObjetos.ReCalculaProjeto(PPedidoPRojeto: string;Largura:string;Altura:string);
begin
     Self.recalculaprojeto(ppedidoprojeto,true,Largura,Altura);
End;

function TObjPedidoObjetos.ReCalculaProjeto(PPedidoPRojeto: string;ComCommit:boolean;Largura:string;Altura:string):Boolean;
begin
    result:=False;

    // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
    Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PPedidoPRojeto);
    Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
    if (Self.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')
    then Begin
             if (MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                           'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                           'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo)
            then exit
            else Begin
                     Self.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                     Self.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
                     Self.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
            end;
    end;

    if (Messagedlg('Este procedimento excluir� todos os relacionamentos do projeto atual e gravar� novamente baseado nas informa��es do cadastro de projeto, todas as altera��es efetuadas no mesmo ser�o perdidas!'+#13+'Tem certeza que deseja Recalcular o Projeto atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
    Then exit;

     // Se este � o projeto principal naum poderia ser alterado em hip�tese alguma,
     // mas ouve mudan�a nos planos agora Sr. Carlos quer que quando altera um podido projeto
     //principal autmaticamente o sistema altera todos os seus deriVados
     if (Self.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(PPedidoPRojeto)=true)
     then Begin
             if (MensagemPergunta('Existem projetos derivados desse projeto.'+#13+
                                  'Este procedimento ir� APAGAR todos os projetos replicados e RECRI�-LOS conforme altera��o feita'+#13+
                                  'no Projeto Principal.'+#13#13+
                                  'Tem CERTEZA  que deseja seguir com o procedimento?' )=mrno)
             then Begin
                    exit;
             end;

             if (Self.GravaRelacionamentos(PPedidoProjeto,Largura,Altura)=False)
             Then Begin
                    FDataModulo.IBTransaction.RollbackRetaining;
                    Messagedlg('N�o foi poss�vel gravar os relacionamentos do projeto escolhido para este pedido',mterror,[mbok],0);
                    exit;
             end;
             // Executo operacoes para excluir e reciar os projetos novamente
             Self.ApagaPedidoProjetoRepliocadosERecria(PPedidoPRojeto);

     End
     Else  Begin
             if (Self.GravaRelacionamentos(PPedidoProjeto,Largura,Altura)=False)
             Then Begin
                     FDataModulo.IBTransaction.RollbackRetaining;
                     Messagedlg('N�o foi poss�vel gravar os relacionamentos do projeto escolhido para este pedido',mterror,[mbok],0);
                     exit;
             End;
     end;

     Messagedlg('C�lculos Efetuados com Sucesso!',mtinformation,[mbok],0);
     result:=True;
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPedidoObjetos.AtualizaComissaoTrocaMateriaisPedConcluido(Ppedido: string;ValorBonificacao:string):Boolean;
var
Pacrescimo1,PComissaoArquiteto,PvalorFinal1,PDesconto1,Pcomissao1,Pporcentagem1:string;
ObjfaixaDescontoComissao:TObjFAIXADESCONTO_COMISSAO;
PvalorTotal1:string;
Ptitulo:string;
PvalorBonificacao1,Porcentagemcomissaoarquiteto:currency;
PComissaoVendedor:string;
begin
   {
      Quando um cliente efetua a troca de materiais ou devolve algum, aconte que a comiss�o do arquiteto e a
      comiss�o do vendedor precisa ser atualizada, pra mais ou pra menos, ent�o acontece o seguinte...
      preciso saber quantos % � a comiss�o de ambos...
      calculo a comiss�o...
      se for um valor a pagar, lan�o pro vendedor uma comiss�o negativa, ou seja, a descontar do proximo pagamento
      se for um valor a recever, lan�o pro vendedor uma comiss�o positiva, ou seja, a pagar pro vendedor
      no caso do arquiteto, eu gravo um valor na tabdescacrescomissaoarquiteto pra descontar do arquiteto na proxima gera��o
      de comiss�o
   }

    if (ObjMedidas_Proj.PedidoProjeto.PEDIDO.LocalizaCodigo(ppedido)=False)
     Then Begin
               Messagedlg('Pedido '+ppedido+' n�o localizado',mterror,[mbok],0);
               exit;
     end;
     ObjMedidas_Proj.PedidoProjeto.PEDIDO.TabelaparaObjeto;
      Ptitulo:=ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_Titulo;

     Try
        strtofloat(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_ValorDesconto);
        Pdesconto1:=ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_ValorDesconto;
     Except
           Messagedlg('Erro na tentativa de resgatar o Valor do Desconto do Pedido',mterror,[mbok],0);
           exit;
     End;

     Try
        if (strtofloat(ValorBonificacao)>0)
        Then Pporcentagem1:=floattostr((strtofloat(PDesconto1)*100)/(strtofloat(ValorBonificacao)))
        Else Pporcentagem1:='100';
     Except
           Messagedlg('Erro No C�lculo da porcentagem',mterror,[mbok],0);
           exit;
     End;

     //ja tem o percentual que foi descontado
     //agora encontrando a faixadecomissao que se encaixa esse desconto
     Try
        ObjfaixaDescontoComissao:=TObjFAIXADESCONTO_COMISSAO.create;
     Except
           Messagedlg('Erro na tentativa de Criar o Objeto de Faixa de Descontos da Comiss�o',mterror,[mbok],0);
           exit;
     End;

     try
        if (Strtofloat(Pporcentagem1)<0)
        Then Pporcentagem1:='0';

        if (ObjfaixaDescontoComissao.Localiza_desconto(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Vendedor.FaixaDesconto.Get_CODIGO,Pporcentagem1)=False)
        Then Begin
                  Messagedlg('O Desconto aplicado n�o se enquadra em nenhuma faixa de desconto desse vendedor',mterror,[mbok],0);
                  exit;
        end;
        ObjfaixaDescontoComissao.TabelaparaObjeto;

        try
            strtofloat(ObjfaixaDescontoComissao.Get_Comissao);
            PComissao1:=ObjfaixaDescontoComissao.Get_Comissao;
        Except
            Messagedlg('A comiss�o da Faixa '+ObjMedidas_Proj.PedidoProjeto.PEDIDO.Vendedor.FaixaDesconto.Get_CODIGO+' para o desconto '+pdesconto1+' � inv�lida',mterror,[mbok],0);
            exit;
        End;


        Try
           PComissaoVendedor:=floattostr(((strtofloat(Pcomissao1)*(strtofloat(ValorBonificacao)))/100)*(-1));

        Except
              PcomissaoVendedor:='0';
        End;

        if(StrTofloat(PComissaoVendedor)<0)
        then Pcomissao1:= FloatToStr(StrToFloat(Pcomissao1)*(-1)) ;

         with Self.ObjqueryTEMP do
         Begin
              //gravo as comiss�es negativas ou positivas pro vendedor
             // ShowMessage(ValorBonificacao);
              close;
              sql.clear;
              sql.clear;
              sql.add('insert into TabComissaoVendedores(Codigo,Vendedor,Pedido,Pendencia,Valor,Comissao)');
              sql.add('select -1,'+ObjMedidas_Proj.PedidoProjeto.pedido.Vendedor.get_codigo+','+ObjMedidas_Proj.PedidoProjeto.pedido.get_codigo+',tabpendencia.codigo,'+virgulaparaponto(ValorBonificacao)+','+Pcomissao1);
              sql.add('from tabpendencia where titulo='+Ptitulo);
              
              Try
                  ExecSQL;
              Except
                    on e:exception do
                    Begin
                         Messagedlg(e.message,mterror,[mbok],0);
                         exit;
                    end;
              end;
              result:=true;

         End;



     Finally
            ObjfaixaDescontoComissao.Free;
     end;


end;


function TObjPedidoObjetos.GeraComissaoPedido(Ppedido: string;var PComissaoVendedor:string): Boolean;
var
Pacrescimo1,PComissaoArquiteto,PvalorFinal1,PDesconto1,Pcomissao1,Pporcentagem1:string;
ObjfaixaDescontoComissao:TObjFAIXADESCONTO_COMISSAO;
PvalorTotal1:string;
Ptitulo:string;
PvalorBonificacao1,Porcentagemcomissaoarquiteto:currency;
begin
     Result:=false;


     if (ObjMedidas_Proj.PedidoProjeto.PEDIDO.LocalizaCodigo(ppedido)=False)
     Then Begin
               Messagedlg('Pedido '+ppedido+' n�o localizado',mterror,[mbok],0);
               exit;
     end;
     ObjMedidas_Proj.PedidoProjeto.PEDIDO.TabelaparaObjeto;
     Ptitulo:=ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_Titulo;

     if (ObjMedidas_Proj.PedidoProjeto.PEDIDO.Vendedor.FaixaDesconto.Get_CODIGO='')
     Then Begin
               Messagedlg('O vendedor '+ObjMedidas_Proj.PedidoProjeto.PEDIDO.Vendedor.get_codigo+'-'+ObjMedidas_Proj.PedidoProjeto.PEDIDO.Vendedor.Get_Nome+' n�o possui faixa de Desconto para gerar a comiss�o',mterror,[mbok],0);
               exit;
     End;


     Try
        strtofloat(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_ValorTotal);
        PvalorTotal1:=ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_ValorTotal;
     Except
           Messagedlg('Erro na tentativa de resgatar o Valor Total do Pedido',mterror,[mbok],0);
           exit;
     End;

     Try
        PvalorBonificacao1:=strtofloat(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_ValorBonificacaoCliente);
     Except
        PvalorBonificacao1:=0;
     End;

     //calculando a % de desconto do pedido
     //A % de desconto para encontrar a faixa de comissao � calculada
     //sobre o valor final, entao Valortotal-bonificacao   - desconto = valor final
     //Assim a % nao � sobre o valor dos produtos e sim Produtos - bonificacao usada
     PvalorTotal1:=Floattostr(Strtofloat(PvalorTotal1)-PvalorBonificacao1);

     Try
        strtofloat(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_ValorFinal);
        PvalorFinal1:=ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_ValorFinal;
     Except
           Messagedlg('Erro na tentativa de resgatar o Valor final do Pedido',mterror,[mbok],0);
           exit;
     End;

     Try
        strtofloat(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_ValorDesconto);
        Pdesconto1:=ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_ValorDesconto;
     Except
           Messagedlg('Erro na tentativa de resgatar o Valor do Desconto do Pedido',mterror,[mbok],0);
           exit;
     End;

     Try
        Pacrescimo1:='0';
        strtofloat(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_ValorAcrescimo);
        Pacrescimo1:=ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_ValorAcrescimo;
     Except
           Messagedlg('Erro na tentativa de resgatar o Valor do Acrescimo do Pedido',mterror,[mbok],0);
           exit;
     End;

     Try
        PComissaoArquiteto:='0';

        strtofloat(ObjMedidas_Proj.PedidoProjeto.PEDIDO.get_ValorComissaoArquiteto);
        PComissaoArquiteto:=ObjMedidas_Proj.PedidoProjeto.PEDIDO.get_ValorComissaoArquiteto;
     Except
           Messagedlg('Erro na tentativa de resgatar o Valor da Comiss�o Arquiteto',mterror,[mbok],0);
           exit;
     End;

     //Caso tenha Comiss�o de Arquiteto, entra como se fosse um desconto, assim
     //o valor da comiss�o do vendedor muda

     if (DescontaComissaoArquitetoVendedor=True)
     Then Pdesconto1:=floattostr(strtofloat(PDesconto1)+strtofloat(PComissaoArquiteto))
     Else Pdesconto1:=floattostr(strtofloat(PDesconto1));

     //PvalorTotal+acrescimo   100%
     //Pdesconto                X%

     //X=(pdesconto*100)/pvalortotal

     Try
        if ((strtofloat(PvalorTotal1)+strtofloat(Pacrescimo1))>0)
        Then Pporcentagem1:=floattostr((strtofloat(PDesconto1)*100)/(strtofloat(PvalorTotal1)+strtofloat(Pacrescimo1)))
        Else Pporcentagem1:='100';
     Except
           Messagedlg('Erro No C�lculo da porcentagem',mterror,[mbok],0);
           exit;
     End;

     //ja tem o percentual que foi descontado
     //agora encontrando a faixadecomissao que se encaixa esse desconto
     Try
        ObjfaixaDescontoComissao:=TObjFAIXADESCONTO_COMISSAO.create;
     Except
           Messagedlg('Erro na tentativa de Criar o Objeto de Faixa de Descontos da Comiss�o',mterror,[mbok],0);
           exit;
     End;

     try
        if (Strtofloat(Pporcentagem1)<0)
        Then Pporcentagem1:='0';

        if (ObjfaixaDescontoComissao.Localiza_desconto(ObjMedidas_Proj.PedidoProjeto.PEDIDO.Vendedor.FaixaDesconto.Get_CODIGO,Pporcentagem1)=False)
        Then Begin
                  Messagedlg('O Desconto aplicado n�o se enquadra em nenhuma faixa de desconto desse vendedor',mterror,[mbok],0);
                  exit;
        end;
        ObjfaixaDescontoComissao.TabelaparaObjeto;

        try
            strtofloat(ObjfaixaDescontoComissao.Get_Comissao);
            PComissao1:=ObjfaixaDescontoComissao.Get_Comissao;
        Except
            Messagedlg('A comiss�o da Faixa '+ObjMedidas_Proj.PedidoProjeto.PEDIDO.Vendedor.FaixaDesconto.Get_CODIGO+' para o desconto '+pdesconto1+' � inv�lida',mterror,[mbok],0);
            exit;
        End;


        Try
           //calculando o valor que vai dar a comissao
           //sera usado depois no calculo da comissao do colocador
           //por isso � um parametro OUT

           //Exemplo
           //Valor 1100  desconto 100 VAlor final 1000 porem comissao arqt. 100,00
           //Sera considerado entao 1000-100=900 para gerar o valor da comissao
           If (DescontaComissaoArquitetoVendedor=True)
           Then PComissaoVendedor:=floattostr((strtofloat(Pcomissao1)*(strtofloat(PvalorFinal1)-strtofloat(PComissaoArquiteto)))/100)
           else PComissaoVendedor:=floattostr((strtofloat(Pcomissao1)*(strtofloat(PvalorFinal1)))/100);
        Except
              PcomissaoVendedor:='0';
        End;
     Finally
            ObjfaixaDescontoComissao.Free;
     end;

     //Calculando quantos % a comissao do arquiteto representa no valor final, assim desconto esse valor
     //das pendencias para calculo da comissao

     //Exemplo
     //Valor Final 1000,00, Arquiteto 100,00
     //Foram geradas 10 parcelas de R$ 100,00 assim
     //Como 100,00 � 10% de 1000,00
     //Desconto 10% de cada parcela no calculo da comissao
     //A pendencia continua a mesma, mas para calculo
     //da comissao sera por exemplo 10 parcelas de 90,00

     //ValorFinal   100%
     //ComArq        X
     //X=(ComArq*100)/ValorFinal

     Try
        if (strtofloat(PVALORFINAL1)=0)//pode acontecer no caso de desconto total com uso de bonificacao
        Then Porcentagemcomissaoarquiteto:=0
        Else Porcentagemcomissaoarquiteto:=(strtofloat(pcomissaoarquiteto)*100)/strtofloat(PVALORFINAL1);
     Except
           Porcentagemcomissaoarquiteto:=0;
     End;

     //ja tenho a comissao que devera ser aplicada
     //basta fazer um relacionamento com as pend�ncia do pedido
     with Self.ObjqueryTEMP do
     Begin
          close;
          sql.Clear;
          sql.add('Select count(codigo) as CONTA from TabComissaoVendedores where Pedido='+ppedido);
          open;
          if (fieldbyname('conta').asinteger>0)
          Then Begin
                    Messagedlg('J� existe comiss�o gerada para este pedido',mterror,[mbok],0);
                    exit;
          end;

          close;
          sql.clear;
          sql.clear;
          sql.add('Insert Into TabComissaoVendedores(Codigo,Vendedor,Pedido,Pendencia,Valor,Comissao)');
          sql.add('select -1,'+ObjMedidas_Proj.PedidoProjeto.pedido.Vendedor.get_codigo+','+ObjMedidas_Proj.PedidoProjeto.pedido.get_codigo+',');

          if (DescontaComissaoArquitetoVendedor=True)
          Then sql.add('tabpendencia.codigo,(tabpendencia.valor-((tabpendencia.valor*'+virgulaparaponto(formata_valor(Porcentagemcomissaoarquiteto))+')/100)),'+virgulaparaponto(pcomissao1))
          else sql.add('tabpendencia.codigo,tabpendencia.valor,'+virgulaparaponto(pcomissao1));

          sql.add('from tabpendencia where titulo='+Ptitulo);

          Try
              ExecSQL;
          Except
                on e:exception do
                Begin
                     Messagedlg(e.message,mterror,[mbok],0);
                     exit;
                end;
          end;
          result:=true;

     End;

end;


Function TObjPedidoObjetos.RetornaComissaoVendedorPedido(Ppedido: string): Boolean;
begin
     result:=False;
     //primeiro localizo todas as comissoes desse pedido que ja tenham lote, isto siginifca que ja foram pagas
     With Self.Objquerytemp do
     Begin

        close;
        sql.clear;
        sql.add('Select count(TCAF.codigo) as CONTA from TabComissao_Adiant_FuncFolha TCAF');
        sql.add('join TabComissaoVendedores on TCAF.comissaovendedor=tabcomissaovendedores.codigo');
        sql.add('where TabComissaoVendedores.Pedido='+Ppedido);
        open;
        if (fieldbyname('conta').asinteger>0)
        Then Begin
                  Messagedlg('Algumas comiss�es de vendedor ligadas a este romaneio ja foram pagas',mterror,[mbok],0);
                  exit;
        End;

        close;
        sql.clear;
        sql.add('Select count(TCV.codigo) as CONTA from TabComissaoVendedores TCV');
        sql.add('where TCV.Lotepagamento is not null and pedido='+ppedido);
        open;
        if (fieldbyname('conta').asinteger>0)
        Then Begin
                  if (Messagedlg('Algumas comiss�es desse vendedor ligadas a este romaneio ja foram pagas fechadas. Certeza que deseja continuar?',mtConfirmation,[mbyes,mbno],0)=mrno)
                  then exit;
        End;



        close;
        sql.clear;
        sql.add('Delete from TabComissaoVendedores where Pedido='+ppedido);
        Try
           execsql;
           result:=true;
        Except
              on e:exception do
              Begin
                  Messagedlg('Erro na tentativa de Excluir as Comiss�es de Vendedores do Pedido '+ppedido+#13+E.message,mterror,[mbok],0);
                  exit;
              End;
        end;
     End;
end;

function TObjPEDIDOObjetos.VerificaPPRomaneioporPedido(ppedido: string): boolean;
begin
     Result:=False;
     //verificando se tem algum pedidoprojeto pertence ao pedido em parametro
     //ligado a algum romaneio
     //este procedimento � chamando antes de retornar a venda de um pedido
     //senao ele volta pra concluido='N' e o pedidoprojeto fica ligado ao romaneio,
     //mas isso nao pode porque nao pode entregar pedido/projeto de pedidos que nao
     //foram concluidos
     With Self.Objquery do
     Begin
          close;
          SQL.clear;
          sql.add('Select count(TPPR.codigo) as conta from TabPedidoProjetoRomaneio TPPR');
          sql.add('join TabPedido_proj on TPPR.PedidoProjeto=TabPedido_proj.codigo');
          sql.add('join TabPedido on TabPedido_proj.pedido=tabpedido.codigo');
          sql.add('where TabPedido.codigo='+ppedido);
          open;
          if (fieldbyname('conta').asinteger=0)
          Then result:=True;
     End;
end;

function TObjPedidoObjetos.RetornaComissaoColocadorPedido(Ppedido: string): Boolean;
begin
     result:=False;
     With Self.ObjqueryTemp do
     Begin
          Close;
          SQL.clear;
          sql.add('Select count(TCC.codigo) as CONTA from TabComissaoColocador TCC');
          sql.add('join TabPedido_Proj on TCC.PedidoProjeto=TabPedido_proj.codigo');
          sql.add('join tabpedido on tabPedido_Proj.Pedido=Tabpedido.codigo');
          sql.add('where not (TCC.pedidoprojetoromaneio is null)');
          sql.add('and TabPedido.codigo='+PPEDIDO);
          open;
          if (Fieldbyname('conta').asinteger>0)
          Then Begin
                    Messagedlg('Existem registro de Comiss�o de Colocador que j� foram utilizados em Romaneio',mterror,[mbok],0);
                    exit;
          End;

          Close;
          SQL.clear;
          sql.add('Delete from TabComissaoColocador where codigo in');
          sql.add('(Select tabComissaoColocador.codigo from TabComissaoColocador');
          sql.add('join TabPedido_Proj on TabComissaoColocador.PedidoProjeto=TabPedido_proj.codigo');
          sql.add('join tabpedido on tabPedido_Proj.Pedido=Tabpedido.codigo');
          sql.add('where TabPedido.codigo='+PPEDIDO+')');
          try
              execsql;
              result:=True;
          Except
                Messagedlg('Erro na tentativa de Excluir as Comiss�es dos Colocadores, verifique se n�o existe alguma pend�ncia ligada a elas',mterror,[mbok],0);
                exit;
          End;
     End;
end;

function TObjPedidoObjetos.RetornaComissaoColocadorPedidoProjeto(PpedidoProjeto: string): Boolean;
begin
     //sera usado na troca de pedido concluido pois para apagar um pedido projeto inteiro � necessario apagar
     //a comissao primeiro
     result:=False;
     With Self.ObjqueryTemp do
     Begin
          Close;
          SQL.clear;
          sql.add('Select count(TCC.codigo) as CONTA from TabComissaoColocador TCC');
          sql.add('where not (TCC.pedidoprojetoromaneio is null)');
          sql.add('and Tcc.PedidoProjeto='+PpedidoProjeto);
          open;
          if (Fieldbyname('conta').asinteger>0)
          Then Begin
                    Messagedlg('Existem registro de Comiss�o de Colocador que j� foram utilizados em Romaneio',mterror,[mbok],0);
                    exit;
          End;

          Close;
          SQL.clear;
          sql.add('Delete from TabComissaoColocador where pedidoprojeto='+PpedidoProjeto);
          try
              execsql;
              result:=True;
          Except
                Messagedlg('Erro na tentativa de Excluir as Comiss�es dos Colocadores, verifique se n�o existe alguma pend�ncia ligada a elas',mterror,[mbok],0);
                exit;
          End;
     End;
end;



procedure TObjPedidoObjetos.ImprimeVendaPeriodo_vendedor;
var
Pdata1,Pdata2:string;
Pcliente,Pvendedor:string;
Pcomissao,PsomaBrutoVendedor,PSomaBrutoGeral:Currency;
PSomaComissaoVendedor,PsomaComissaoGeral,prelacaoporcento,PsomaLiquidovendedor,PsomaLiquidogeral:Currency;
PsomaDescontoVendedor,PsomaDescontoGeral:Currency;
PsomabonificacaoVendedor,PSomabonificacaoGeral:currency;
begin

     pvendedor:='';

     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('RELAT�RIO DE VENDAS POR PER�ODO E VENDEDOR')=False)
     then Begin
               //nao tem permissao, entao pe�o senha para escolher o vendedor
               //com senha

               if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.vendedor.PegaVendedor(Pvendedor)=False)
               Then exit;
     End;

     {
     Vendedor X

     Pedido   Cliente   Valor Total   Desconto  Acrescimo Valor Final
     }

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.caption:='Data Inicial';
          edtgrupo01.EditMask:=MascaraData;

          Grupo02.Enabled:=true;
          LbGrupo02.caption:='Data Final';
          edtgrupo02.EditMask:=MascaraData;

          Grupo03.Enabled:=True;
          LbGrupo03.caption:='Cliente';

          if (Pvendedor='')//senao tiver vazio � porque um vendedor digitou a senha
          Then Begin
                  Grupo04.Enabled:=True;
                  LbGrupo04.caption:='Vendedor';
                  edtgrupo04.OnKeyDown:=Self.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtVendedorKeyDown;
                  edtgrupo03.OnKeyDown:=Self.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtClienteKeyDown;
                  edtgrupo03.color:=$005CADFE;
          End
          Else edtgrupo04.text:=pvendedor;


          Showmodal;

          if (tag=0)
          then exit;

          Pdata1:='';
          Pdata2:='';
          Pcliente:='';
          Pvendedor:='';

          Try
             if (trim(comebarra(edtgrupo01.text))<>'')
             Then Begin
                       strtodate(edtgrupo01.text);
                       pdata1:=edtgrupo01.text;
             End;
          Except
                Messagedlg('Data Inicial inv�lida',mterror,[mbok],0);
                exit;
          End;

          Try
             if (trim(comebarra(edtgrupo02.text))<>'')
             Then Begin
                       strtodate(edtgrupo02.text);
                       pdata2:=edtgrupo02.text;
             End;
          Except
                Messagedlg('Data Final inv�lida',mterror,[mbok],0);
                exit;
          End;

          Try
             if (trim(edtgrupo03.text)<>'')
             Then Begin
                       strtoint(edtgrupo03.text);
                       Pcliente:=edtgrupo03.text;

                       if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.LocalizaCodigo(pcliente)=False)
                       then Begin
                                 Messagedlg('Cliente n�o encontrado',mterror,[mbok],0);
                                 exit;
                       End;
                       Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.TabelaparaObjeto;


             End;
          Except
                Messagedlg('Cliente inv�lido',mterror,[mbok],0);
                exit;
          End;

          Try
             if (trim(edtgrupo04.text)<>'')
             Then Begin
                       strtoint(edtgrupo04.text);
                       Pvendedor:=edtgrupo04.text;

                       if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Vendedor.LocalizaCodigo(Pvendedor)=False)
                       then Begin
                                 Messagedlg('Vendedor n�o encontrado',mterror,[mbok],0);
                                 exit;
                       End;
                       Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Vendedor.TabelaparaObjeto;
             End;
          Except
                Messagedlg('Vendedor inv�lido',mterror,[mbok],0);
                exit;
          End;





     End;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select tabpedido.Codigo,tabpedido.data,tabpedido.valortotal,tabpedido.ValorBonificacaoCliente,');
          sql.add('tabpedido.valordesconto,tabpedido.valorfinal,tabpedido.vendedor,Tabvendedor.Nome AS nomevendedor,');
          sql.add('tabpedido.cliente,Tabcliente.Nome AS NOMECLIENTE,tabcliente.cidade from tabpedido');
          sql.add('join tabvendedor on Tabpedido.Vendedor=Tabvendedor.codigo');
          sql.add('join tabCliente on Tabpedido.Cliente=Tabcliente.codigo');
          Sql.add('Where TabPedido.codigo<>-500');
          Sql.add('and Tabpedido.Concluido=''S'' ');


          if(Pdata1<>'')
          Then sql.add('and TabPedido.Data>='+#39+Formatdatetime('mm/dd/yyyy',Strtodate(pdata1))+#39);

          if(Pdata2<>'')
          Then sql.add('and TabPedido.Data<='+#39+Formatdatetime('mm/dd/yyyy',Strtodate(pdata2))+#39);

          if(Pvendedor<>'')
          Then sql.add('and TabPedido.Vendedor='+pvendedor);

          if(PCliente<>'')
          Then sql.add('and TabPedido.Cliente='+pcliente);

          sql.add('order by Tabpedido.vendedor,tabpedido.data');
          open;

          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma informa��o foi selecionada',mterror,[mbok],0);
                    exit;
          End;


          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               RDprint.TamanhoQteColunas:=130;
               RDprint.FonteTamanhoPadrao:=S17cpp;

               LinhaLocal:=3;
               RDprint.Abrir;
               if (RDprint.Setup=False)
               Then Begin
                         RDprint.fechar;
                         exit;
               End;


               RDprint.ImpC(linhalocal,65,'VENDAS POR PER�ODO',[negrito]);
               IncrementaLinha(2);

               if (pcliente<>'')
               then Begin
                         RDprint.ImpF(LinhaLocal,1,'Cliente  '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome,[negrito]);
                         IncrementaLinha(1);
               End;

               if (Pvendedor<>'')
               then Begin
                         RDprint.ImpF(LinhaLocal,1,'Vendedor '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Vendedor.Get_Nome,[negrito]);
                         IncrementaLinha(1);
               End;

               if (Pdata1<>'')
               or (pdata2<>'')
               then Begin
                         RDprint.ImpF(LinhaLocal,1,'Per�odo: '+Pdata1+' a '+Pdata2,[negrito]);
                         IncrementaLinha(1);
               End;
               IncrementaLinha(1);




               RDprint.ImpF(LinhaLocal,1,CompletaPalavra('PEDIDO',9,' ')+' '+
                                         CompletaPalavra('DATA',8,' ')+' '+
                                         CompletaPalavra('CLIENTE',30,' ')+' '+
                                         CompletaPalavra('CIDADE',20,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR TOTAL',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('DESC.BON.',9,' ')+' '+
                                         CompletaPalavra_a_Esquerda('DESCONTO',9,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR FINAL',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('COMISS�O',12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               IncrementaLinha(1);

               Pvendedor:='';

               PsomaBrutoVendedor:=0;
               PSomabrutoGeral:=0;

               PsomaLiquidoVendedor:=0;
               PSomaLiquidoGeral:=0;

               PsomaComissaoVendedor:=0;
               PsomaComissaoGeral:=0;

               PsomaDescontoVendedor:=0;
               PsomaDescontoGeral:=0;

               PsomabonificacaoVendedor:=0;
               PSomabonificacaoGeral:=0;

               While not(Self.Objquery.eof) do
               Begin

                    if (Pvendedor<>Fieldbyname('vendedor').asstring)
                    Then Begin
                              //mudou o vendedor
                              if (Pvendedor<>'')
                              Then Begin
                                        //Psomabruto       100%
                                        //psomadesconto     X
                                        //->X=(PSOMADESCONTO*100)/PSOMABRUTO
                                        prelacaoporcento:=(PsomaDescontoVendedor*100)/PsomaBrutoVendedor;
                                        //totalizando o vendedor anterior
                                        VerificaLinha;
                                        RDprint.Impf(LinhaLocal,1,CompletaPalavra('SOMA DO VENDEDOR ',45,' ')+completapalavra(' ',26,' ')+CompletaPalavra_a_Esquerda(formata_valor(PsomaBrutoVendedor),12,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PRelacaoPorcento)+'%',19,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PsomaLiquidovendedor),12,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PsomaComissaoVendedor),12,' '),[negrito]);
                                        IncrementaLinha(2);

                              End;

                              VerificaLinha;
                              RDprint.Impf(LinhaLocal,1,CompletaPalavra(fieldbyname('vendedor').asstring+'-'+fieldbyname('nomevendedor').asstring,90,' '),[negrito]);
                              IncrementaLinha(1);
                              Pvendedor:=Fieldbyname('vendedor').asstring;
                              PsomabrutoVendedor:=0;
                              PsomaLiquidoVendedor:=0;
                              PSomaComissaoVendedor:=0;
                              PsomaDescontoVendedor:=0;
                              PsomabonificacaoVendedor:=0;

                    End;
                    PComissao:=Self.RetornaValorComissaoPedido(fieldbyname('codigo').asstring);

                    VerificaLinha;
                    RDprint.Imp(LinhaLocal,1,CompletaPalavra(fieldbyname('codigo').asstring,9,' ')+' '+
                                         CompletaPalavra(formatdatetime('dd/mm/yy',fieldbyname('DATA').asdatetime),8,' ')+' '+
                                         CompletaPalavra(fieldbyname('CLIENTE').asstring+'-'+fieldbyname('nomeCLIENTE').asstring,30,' ')+' '+
                                         CompletaPalavra(fieldbyname('cidade').asstring,20,' ')+' '+
                                         CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORTOTAL').asstring),12,' ')+' '+
                                         CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('ValorBonificacaoCliente').asstring),9,' ')+' '+
                                         CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorDESCONTO').asstring),9,' ')+' '+
                                         CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORFINAL').asstring),12,' ')+' '+
                                         CompletaPalavra_a_Esquerda(formata_valor(PComissao),12,' '));
                    IncrementaLinha(1);

                    PsomaLiquidovendedor:=PsomaLiquidoVendedor+fieldbyname('valorfinal').asfloat;
                    PsomaLiquidogeral:=PSomaLiquidoGeral+fieldbyname('valorfinal').asfloat;

                    PsomaDescontoVendedor:=PsomaDescontoVendedor+fieldbyname('valordesconto').asfloat;
                    PsomaDescontoGeral:=PsomaDescontoGeral+fieldbyname('valordesconto').asfloat;

                    PsomaBrutoVendedor:=PsomaBrutoVendedor+fieldbyname('valortotal').asfloat;
                    PSomaBrutoGeral   :=PSomaBrutoGeral+fieldbyname('valortotal').asfloat;

                    PsomabonificacaoVendedor:=PsomabonificacaoVendedor+fieldbyname('valorbonificacaocliente').asfloat;
                    PSomabonificacaoGeral:=PSomabonificacaoGeral+fieldbyname('valorbonificacaocliente').asfloat;


                    PsomaComissaoVendedor:=PsomaComissaoVendedor+Pcomissao;
                    PsomaComissaoGeral:=PsomaComissaoGeral+Pcomissao;

                    Self.Objquery.Next;
               End;//while

               prelacaoporcento:=(PsomaDescontoVendedor*100)/PsomaBrutoVendedor;

               VerificaLinha;
               RDprint.Impf(LinhaLocal,1,CompletaPalavra('SOMA DO VENDEDOR ',45,' ')+completapalavra(' ',26,' ')+CompletaPalavra_a_Esquerda(formata_valor(PsomaBrutoVendedor),12,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PRelacaoPorcento)+'%',19,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PsomaLiquidovendedor),12,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PsomaComissaoVendedor),12,' '),[negrito]);
               IncrementaLinha(2);
               DesenhaLinha;

               prelacaoporcento:=(PsomaDescontoGeral*100)/PSomaBrutoGeral;

               VerificaLinha;
               RDprint.Impf(LinhaLocal,1,CompletaPalavra('SOMA GERAL ',45,' ')+completapalavra(' ',26,' ')+CompletaPalavra_a_Esquerda(formata_valor(PsomaBrutogeral),12,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PRelacaoPorcento)+'%',19,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PsomaLiquidoGeral),12,' ')+' '+ CompletaPalavra_a_Esquerda(formata_valor(PsomaComissaoGeral),12,' '),[negrito]);
               IncrementaLinha(2);


               RDprint.Fechar;
          End;
     End;
end;

function TObjPedidoObjetos.RetornaValorComissaoPedido(Ppedido: string): Currency;
begin
     result:=0;
     if (Ppedido='')
     Then exit;

     With Self.Objquerytemp do
     Begin
          close;
          sql.clear;
          sql.add('Select sum(valorcomissao) as SOMA from tabComissaovendedores where Pedido='+Ppedido);
          open;
          result:=Fieldbyname('soma').asfloat;
          close;
     End;
end;



Function TObjPedidoObjetos.ExportaContabilidade_Estoque_Romaneio(PpedidoProjeto,PRomaneio,PdataEntregaRomaneio:string):Boolean;
var
PcontaCmv:string;
PqueryEntrada:TibQuery;
Pcusto:Currency;
Pdata:Tdate;
PPlanodeContasFinal,PvalorFinal:TStringList;
cont:integer;
Plote:string;
PMediaCompras:boolean;
Begin
{
Estoque

Para baixar o estoque de cada cadastro (grupo) n�o posso baixar no valor de
venda e sim no valor de custo
ou seja = quantidade vendida * valor de custo

Para encontrar o valor de Custo
O valor do custo � o valor m�dio unit�rio  da compra  dentro do per�odo da venda (m�s)

Exemplo venda do dia 15/11

Encontro a media de compra 1/11 a 15/11
se  n�o houve compra no m�s 11 vou para o m�s 10 e assim ate encontrar uma compra,
ao encontrar utilizo a media daquele m�s. Senao tiver compra antes do periodo da Venda
Utilizo o valor do campo Custo.

Credita					                  Debito
Estoque (de acordo com o produto)	cmv (parametrizar)


MODIFICADO 27/01/07

ESSE CMV JA ERA

FAZER NA CONCLUSAO DO ROMANEIO,

A DATA UTILIZADA PARA CALCULO DO CUSTO NAO SERA DA VENDA E SIM DATA DE ENTREGA DO ROMANEIO
E OS PRODUTOS SELECIONADOS NAO SERAO DO PEDIDO E SIM APENAS DO PEDIDO PROJETO QUE
SERA ENTREGUE NO ROMANEIO.

MODIFICADO DIA 28 DEZEMBRO
PARA NAO USAR A MEDIA DO VALOR UNITARIO E SIM A MEDIA DA SOMA DO VALOR FINAL / PELA SOMA DA QUANTIDADE



A CAMILA PEDIU DIA 15/04/09
PARA QUE O PERIODO SEJA PARAMETRIZADO, POR EXEMPLO A GLASSBOX USA DENTRO DO MES
MAS A DOURAGLASS A MEDIA DAS ULTIMAS COMPRAS


A CAMILA PEDIU DIA 15/05/2009 PARA

O valor do produto para ser calculada a m�dia para custo ser� :

V.produto unit *aliquota Icms = X          -->    100*7% =    7,00
V.produto unit *aliquota IPI = Y             -->    100*5% =    5,00
V.produto unit *aliquota PIS = Z            -->    100*1,65%= 1,65
V.produto unit *aliquota COFINS = W      -->    100*7,6% = 7,60

O valor a ser considerado ser� = (V.NF)-(X+Y+Z+W) --> (105,00)-(21,25)= 83,75



}



    result:=False;

    if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
    then Begin
              MensagemErro('Pedido Projeto n�o encontrado');
              exit;
    End;
    Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;


Try
    PqueryEntrada:=TibQuery.create(nil);
    PqueryEntrada.Database:=FDataModulo.IBDatabase;

    PPlanodeContasFinal:=TStringList.Create;
    PvalorFinal:=TStringList.create;

Except
    MensagemErro('Erro na tentativa de Criar a Query PqueryEntrada');
    exit;
End;

Try//finally
    PMediaCompras:=False;

    if (ObjParametroGlobal.ValidaParametro('REALIZA CMV PELA M�DIA DE TODAS COMPRAS')=true)
    Then Begin
              if (ObjParametroGlobal.Get_Valor='SIM')
              Then PMediaCompras:=true;
    End;


    PcontaCmv:='';
    if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL PARA CMV NA VENDA')=true)
    then PContaCmv:=Objparametroglobal.get_valor;

    FmostraStringGrid.Configuracoesiniciais;
    FmostraStringGrid.StringGrid.ColCount:=4;
    FmostraStringGrid.StringGrid.RowCount:=2;
    FmostraStringGrid.StringGrid.Cols[0].clear;
    FmostraStringGrid.StringGrid.Cols[1].clear;
    FmostraStringGrid.StringGrid.Cols[2].clear;
    FmostraStringGrid.StringGrid.Cols[3].clear;
    FmostraStringGrid.StringGrid.Cells[0,0]:='CODIGO CADASTRO';
    FmostraStringGrid.StringGrid.Cells[1,0]:='PLANO DE CONTAS';
    FmostraStringGrid.StringGrid.Cells[2,0]:='VALOR DO CUSTO';
    FmostraStringGrid.StringGrid.Cells[3,0]:='CADASTRO';


    Try//except
     with Self.ObjqueryTEMP do
     begin
         //**********************DIVERSO******************************
         close;
         sql.clear;
         sql.add('Select TabDiverso_PP.DiversoCor,sum(TabDiverso_PP.Quantidade) as SOMA');
         sql.add('From tabDiverso_pp');
         sql.add('join TabPedido_Proj on TabDiverso_PP.PedidoProjeto=tabpedido_proj.codigo');
         sql.add('where TabPedido_proj.Codigo='+PpedidoProjeto);
         sql.add('Group by tabDiverso_PP.DiversoCor');

         open;
         if (recordcount>0)
         Then begin
                   //teve diverso nesse pedido
                   //pegando um a um e procurando o Custo
                   While not(eof) do
                   Begin
                         ObjDiverso_PP.DiversoCor.LocalizaCodigo(Fieldbyname('diversocor').asstring);
                         ObjDiverso_PP.DiversoCor.TabelaparaObjeto;

                         //Primeiro verificando se existe alguma compra antes da data de entrega do  romaneio
                         PqueryEntrada.Close;
                         PqueryEntrada.sql.clear;
                         PqueryEntrada.sql.add('Select TabEntradaProdutos.Data,TabDiverso_EP.* From tabDiverso_EP');
                         PqueryEntrada.sql.add('join TabEntradaProdutos on tabDiverso_ep.Entrada=TabentradaProdutos.codigo');
                         PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataEntregaRomaneio))+#39);
                         PqueryEntrada.sql.add('and TabDiverso_ep.DiversoCor='+Fieldbyname('diversocor').asstring);
                         PqueryEntrada.sql.add('order by TabEntradaProdutos.Data desc');//do maior pro menor
                         PqueryEntrada.Open;
                         PqueryEntrada.first;
                         if (PqueryEntrada.Recordcount=0)//nao tem entrada entao uso o preco de custo
                         Then Begin
                                   try
                                      Pcusto:=Strtofloat(ObjDiverso_PP.DiversoCor.Diverso.Get_PRECOCUSTO)*Fieldbyname('soma').asfloat;
                                   Except
                                      Pcusto:=0;
                                   End;

                                   FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=ObjDiverso_PP.DiversoCor.Diverso.GrupoDiverso.PlanoDeContas.Get_CODIGO;
                                   FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);

                                   FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=ObjDiverso_PP.DiversoCor.Diverso.Get_Codigo;
                                   FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='DIVERSO';

                                   FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                         End
                         Else Begin//teve compra
                                           if (FormatDateTime('mm/yyyy',pqueryentrada.fieldbyname('data').asdatetime)=FormatDateTime('mm/yyyy',strtodate(PdataEntregaRomaneio)))
                                           Then begin//se for o mesmo mes, tiro a media dentro do mes, do dia 1 ate o dia da venda
                                                     Pdata:=strtodate(PdataEntregaRomaneio);

                                                     PqueryEntrada.Close;
                                                     PqueryEntrada.sql.clear;
                                                     PqueryEntrada.sql.add('Select sum(TabDiverso_EP.quantidade) as somaquantidade,');
                                                     PqueryEntrada.sql.add('sum(');
                                                     PqueryEntrada.sql.add('(TabDiverso_EP.valorfinal+(coalesce(tabdiverso_ep.ipipago,0)*TabDiverso_EP.valorfinal)/100)');
                                                     PqueryEntrada.sql.add('-(coalesce(tabdiverso_ep.creditoicms,0)*TabDiverso_EP.valorfinal)/100');
                                                     PqueryEntrada.sql.add('-(coalesce(tabdiverso_ep.creditoipi,0)*TabDiverso_EP.valorfinal)/100');
                                                     PqueryEntrada.sql.add('-(coalesce(tabdiverso_ep.creditopis,0)*TabDiverso_EP.valorfinal)/100');
                                                     PqueryEntrada.sql.add('-(coalesce(tabdiverso_ep.creditocofins,0)*TabDiverso_EP.valorfinal)/100');
                                                     PqueryEntrada.sql.add(')');
                                                     PqueryEntrada.sql.add(' as somavalorfinal From tabDiverso_EP');
                                                     PqueryEntrada.sql.add('join TabEntradaProdutos on tabDiverso_ep.Entrada=TabentradaProdutos.codigo');
                                                     PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);

                                                     if (PMediaCompras=False)//se for pela media nao tem data inicial
                                                     Then PqueryEntrada.sql.add('and TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);

                                                     PqueryEntrada.sql.add('and   TabDiverso_ep.DiversoCor='+Fieldbyname('diversocor').asstring);
                                                     PqueryEntrada.Open;



                                                     Pcusto:=(PqueryEntrada.fieldbyname('somavalorfinal').asfloat/PqueryEntrada.fieldbyname('somaquantidade').asfloat)*Fieldbyname('soma').asfloat;

                                                     FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=ObjDiverso_PP.DiversoCor.Diverso.GrupoDiverso.PlanoDeContas.Get_CODIGO;
                                                     FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                                     FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=ObjDiverso_PP.DiversoCor.Diverso.Get_Codigo;
                                                     FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='DIVERSO';

                                                     FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                                           End
                                           Else Begin
                                                     //nao foi dentro do mesmo mes mas teve compra, entao tiro a media do mes da ultima compra
                                                     Pdata:=PqueryEntrada.Fieldbyname('data').asdatetime;
                                                     PqueryEntrada.Close;
                                                     PqueryEntrada.sql.clear;
                                                     PqueryEntrada.sql.add('Select sum(TabDiverso_EP.quantidade) as somaquantidade,');
                                                     PqueryEntrada.sql.add('sum(');
                                                     PqueryEntrada.sql.add('(TabDiverso_EP.valorfinal+(coalesce(tabdiverso_ep.ipipago,0)*TabDiverso_EP.valorfinal)/100)');
                                                     PqueryEntrada.sql.add('-(coalesce(tabdiverso_ep.creditoicms,0)*TabDiverso_EP.valorfinal)/100');
                                                     PqueryEntrada.sql.add('-(coalesce(tabdiverso_ep.creditoipi,0)*TabDiverso_EP.valorfinal)/100');
                                                     PqueryEntrada.sql.add('-(coalesce(tabdiverso_ep.creditopis,0)*TabDiverso_EP.valorfinal)/100');
                                                     PqueryEntrada.sql.add('-(coalesce(tabdiverso_ep.creditocofins,0)*TabDiverso_EP.valorfinal)/100');
                                                     PqueryEntrada.sql.add(')');
                                                     PqueryEntrada.sql.add('as somavalorfinal From tabDiverso_EP');
                                                     PqueryEntrada.sql.add('join TabEntradaProdutos on tabDiverso_ep.Entrada=TabentradaProdutos.codigo');

                                                     if (PMediaCompras=False)
                                                     Then Begin
                                                               PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                                               PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',EndOfTheMonth(pdata))+#39);
                                                     End
                                                     Else Begin
                                                                //pela m�dia, tomando em considerando a data<= a data do romaneio
                                                                PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataEntregaRomaneio))+#39);
                                                     End;



                                                     PqueryEntrada.sql.add('and TabDiverso_ep.DiversoCor='+Fieldbyname('diversocor').asstring);
                                                     PqueryEntrada.Open;

                                                     Pcusto:=(PqueryEntrada.fieldbyname('somavalorfinal').asfloat/PqueryEntrada.fieldbyname('somaquantidade').asfloat)*Fieldbyname('soma').asfloat;

                                                     FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=ObjDiverso_PP.DiversoCor.Diverso.GrupoDiverso.PlanoDeContas.Get_CODIGO;
                                                     FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                                     FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=ObjDiverso_PP.DiversoCor.Diverso.Get_Codigo;
                                                     FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='DIVERSO';

                                                     FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                                           end;
                         End;

                         next;
                   End;//while dos diversos
         End;
         //*********************************************************************

         //**********************FERRAGEM******************************
         close;
         sql.clear;
         sql.add('Select Tabferragem_PP.ferragemCor,sum(Tabferragem_PP.Quantidade) as SOMA');
         sql.add('From tabferragem_pp');
         sql.add('join TabPedido_Proj on Tabferragem_PP.PedidoProjeto=tabpedido_proj.codigo');
         sql.add('where TabPedido_proj.Codigo='+PpedidoProjeto);
         sql.add('Group by tabferragem_PP.ferragemCor');

         open;
         if (recordcount>0)
         Then begin
                   //teve ferragem nesse pedido
                   //pegando um a um e procurando o Custo
                   While not(eof) do
                   Begin
                         Objferragem_PP.ferragemCor.LocalizaCodigo(Fieldbyname('ferragemcor').asstring);
                         Objferragem_PP.ferragemCor.TabelaparaObjeto;

                         //Primeiro verificando se existe alguma compra antes da data do pedido
                         PqueryEntrada.Close;
                         PqueryEntrada.sql.clear;
                         PqueryEntrada.sql.add('Select TabEntradaProdutos.Data,Tabferragem_EP.* From tabferragem_EP');
                         PqueryEntrada.sql.add('join TabEntradaProdutos on tabferragem_ep.Entrada=TabentradaProdutos.codigo');
                         PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataEntregaRomaneio))+#39);
                         PqueryEntrada.sql.add('and Tabferragem_ep.ferragemCor='+Fieldbyname('ferragemcor').asstring);
                         PqueryEntrada.sql.add('order by TabEntradaProdutos.Data desc');//do maior pro menor
                         PqueryEntrada.Open;
                         PqueryEntrada.first;
                         if (PqueryEntrada.Recordcount=0)//nao tem entrada entao uso o preco de custo
                         Then Begin
                                   try
                                      Pcusto:=Strtofloat(Objferragem_PP.ferragemCor.ferragem.Get_PRECOCUSTO)*Fieldbyname('soma').asfloat;
                                   Except
                                      Pcusto:=0;
                                   End;


                                   FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objferragem_PP.ferragemCor.ferragem.Grupoferragem.PlanoDeContas.Get_CODIGO;
                                   FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                   FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objferragem_PP.ferragemCor.ferragem.Get_Codigo;
                                   FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='FERRAGEM';

                                   FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                         End
                         Else Begin//teve compra
                                   if (FormatDateTime('mm/yyyy',pqueryentrada.fieldbyname('data').asdatetime)=FormatDateTime('mm/yyyy',strtodate(PdataEntregaRomaneio)))
                                   Then begin//se for o mesmo mes, tiro a media dentro do mes, do dia 1 ate o dia da entrega
                                             Pdata:=strtodate(PdataEntregaRomaneio);

                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select sum(Tabferragem_EP.quantidade) as somaquantidade,');
                                             PqueryEntrada.sql.add('sum(');
                                             PqueryEntrada.sql.add('(Tabferragem_EP.valorfinal+(coalesce(tabferragem_ep.ipipago,0)*Tabferragem_EP.valorfinal)/100)');
                                             PqueryEntrada.sql.add('-(coalesce(tabferragem_ep.creditoicms,0)*Tabferragem_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabferragem_ep.creditoipi,0)*Tabferragem_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabferragem_ep.creditopis,0)*Tabferragem_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabferragem_ep.creditocofins,0)*Tabferragem_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add(')');
                                             PqueryEntrada.sql.add('as somavalorfinal From tabferragem_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabferragem_ep.Entrada=TabentradaProdutos.codigo');

                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);

                                             if (PMediaCompras=False)//tem data inicial, se for pela media nao tem data inicial
                                             Then PqueryEntrada.sql.add('and TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);

                                             PqueryEntrada.sql.add('and Tabferragem_ep.ferragemCor='+Fieldbyname('ferragemcor').asstring);
                                             PqueryEntrada.Open;

                                             Pcusto:=(PqueryEntrada.fieldbyname('somavalorfinal').asfloat/PqueryEntrada.fieldbyname('somaquantidade').asfloat)*Fieldbyname('soma').asfloat;

                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objferragem_PP.ferragemCor.ferragem.Grupoferragem.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objferragem_PP.ferragemCor.ferragem.Get_Codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='FERRAGEM';
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                                   End
                                   Else Begin
                                             //nao foi dentro do mesmo mes mas teve compra, entao tiro a media do mes da ultima compra
                                             Pdata:=PqueryEntrada.Fieldbyname('data').asdatetime;
                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select sum(Tabferragem_EP.quantidade) as somaquantidade, ');
                                             PqueryEntrada.sql.add('sum(');
                                             PqueryEntrada.sql.add('(Tabferragem_EP.valorfinal+(coalesce(tabferragem_ep.ipipago,0)*Tabferragem_EP.valorfinal)/100)');
                                             PqueryEntrada.sql.add('-(coalesce(tabferragem_ep.creditoicms,0)*Tabferragem_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabferragem_ep.creditoipi,0)*Tabferragem_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabferragem_ep.creditopis,0)*Tabferragem_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabferragem_ep.creditocofins,0)*Tabferragem_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add(')');
                                             PqueryEntrada.sql.add('as somavalorfinal From tabferragem_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabferragem_ep.Entrada=TabentradaProdutos.codigo');

                                             if (PMediaCompras=False)
                                             Then Begin
                                                       PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                                       PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',EndOfTheMonth(pdata))+#39);
                                             End
                                             Else Begin
                                                       //pela m�dia uso a m�dia com a data do romaneio<=
                                                       PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataEntregaRomaneio))+#39);
                                             End;


                                             PqueryEntrada.sql.add('and Tabferragem_ep.ferragemCor='+Fieldbyname('ferragemcor').asstring);
                                             PqueryEntrada.Open;

                                             Pcusto:=(PqueryEntrada.fieldbyname('somavalorfinal').asfloat/PqueryEntrada.fieldbyname('somaquantidade').asfloat)*Fieldbyname('soma').asfloat;

                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objferragem_PP.ferragemCor.ferragem.Grupoferragem.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objferragem_PP.ferragemCor.ferragem.Get_Codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='FERRAGEM';
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                                   end;
                         End;

                         next;
                   End;//while dos ferragems
         End;
         //*********************************************************************

         //**********************perfilado******************************
         close;
         sql.clear;
         sql.add('Select Tabperfilado_PP.perfiladoCor,sum(Tabperfilado_PP.Quantidade) as SOMA');
         sql.add('From tabperfilado_pp');
         sql.add('join TabPedido_Proj on Tabperfilado_PP.PedidoProjeto=tabpedido_proj.codigo');
         sql.add('where TabPedido_proj.Codigo='+PpedidoProjeto);
         sql.add('Group by tabperfilado_PP.perfiladoCor');

         open;
         if (recordcount>0)
         Then begin
                   //teve perfilado nesse pedido
                   //pegando um a um e procurando o Custo
                   While not(eof) do
                   Begin
                         Objperfilado_PP.perfiladoCor.LocalizaCodigo(Fieldbyname('perfiladocor').asstring);
                         Objperfilado_PP.perfiladoCor.TabelaparaObjeto;



                         //Primeiro verificando se existe alguma compra antes da data do pedido
                         PqueryEntrada.Close;
                         PqueryEntrada.sql.clear;
                         PqueryEntrada.sql.add('Select TabEntradaProdutos.Data,Tabperfilado_EP.* From tabperfilado_EP');
                         PqueryEntrada.sql.add('join TabEntradaProdutos on tabperfilado_ep.Entrada=TabentradaProdutos.codigo');
                         PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataEntregaRomaneio))+#39);
                         PqueryEntrada.sql.add('and Tabperfilado_ep.perfiladoCor='+Fieldbyname('perfiladocor').asstring);
                         PqueryEntrada.sql.add('order by TabEntradaProdutos.Data desc');//do maior pro menor
                         PqueryEntrada.Open;
                         PqueryEntrada.first;
                         if (PqueryEntrada.Recordcount=0)//nao tem entrada entao uso o preco de custo
                         Then Begin
                                   try
                                       Pcusto:=Strtofloat(Objperfilado_PP.perfiladoCor.perfilado.Get_PRECOCUSTO)*Fieldbyname('soma').asfloat;
                                   Except
                                      Pcusto:=0;
                                   End;
                                   FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objperfilado_PP.perfiladoCor.perfilado.get_codigo;
                                   FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='PERFILADO';

                                   FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objperfilado_PP.perfiladoCor.perfilado.Grupoperfilado.PlanoDeContas.Get_CODIGO;
                                   FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                   FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                         End
                         Else Begin//teve compra
                                   //Showmessage('ultima compra '+pqueryentrada.fieldbyname('data').asstring);
                                   if (FormatDateTime('mm/yyyy',pqueryentrada.fieldbyname('data').asdatetime)=FormatDateTime('mm/yyyy',strtodate(PdataEntregaRomaneio)))
                                   Then begin//se for o mesmo mes, tiro a media dentro do mes, do dia 1 ate o dia da venda
                                             Pdata:=strtodate(PdataEntregaRomaneio);

                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;

                                             PqueryEntrada.sql.add('Select sum(Tabperfilado_EP.quantidade) as somaquantidade, ');
                                             PqueryEntrada.sql.add('sum(');
                                             PqueryEntrada.sql.add('(Tabperfilado_EP.valorfinal+(coalesce(tabperfilado_ep.ipipago,0)*Tabperfilado_EP.valorfinal)/100)');
                                             PqueryEntrada.sql.add('-(coalesce(tabperfilado_ep.creditoicms,0)*Tabperfilado_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabperfilado_ep.creditoipi,0)*Tabperfilado_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabperfilado_ep.creditopis,0)*Tabperfilado_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabperfilado_ep.creditocofins,0)*Tabperfilado_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add(')');
                                             PqueryEntrada.sql.add('as somavalorfinal From tabperfilado_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabperfilado_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where    TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);

                                             if (PMediaCompras=False)//se for pela media nao tem data inicial
                                             Then PqueryEntrada.sql.add('and TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);

                                             PqueryEntrada.sql.add('and Tabperfilado_ep.perfiladoCor='+Fieldbyname('perfiladocor').asstring);
                                             PqueryEntrada.Open;

                                             Pcusto:=(PqueryEntrada.fieldbyname('somavalorfinal').asfloat/PqueryEntrada.fieldbyname('somaquantidade').asfloat)*Fieldbyname('soma').asfloat;

                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objperfilado_PP.perfiladoCor.perfilado.get_codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='PERFILADO';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objperfilado_PP.perfiladoCor.perfilado.Grupoperfilado.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                                   End
                                   Else Begin
                                             //nao foi dentro do mesmo mes mas teve compra, entao tiro a media do mes da ultima compra
                                             Pdata:=PqueryEntrada.Fieldbyname('data').asdatetime;
                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select sum(Tabperfilado_EP.quantidade) as somaquantidade, ');
                                             PqueryEntrada.sql.add('sum(');
                                             PqueryEntrada.sql.add('(Tabperfilado_EP.valorfinal+(coalesce(tabperfilado_ep.ipipago,0)*Tabperfilado_EP.valorfinal)/100)');
                                             PqueryEntrada.sql.add('-(coalesce(tabperfilado_ep.creditoicms,0)*Tabperfilado_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabperfilado_ep.creditoipi,0)*Tabperfilado_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabperfilado_ep.creditopis,0)*Tabperfilado_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabperfilado_ep.creditocofins,0)*Tabperfilado_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add(')');
                                             PqueryEntrada.sql.add('as somavalorfinal From tabperfilado_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabperfilado_ep.Entrada=TabentradaProdutos.codigo');

                                             if (PMediaCompras=False)
                                             Then Begin
                                                      //aqui usei o mes da ultima compra
                                                      PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                                      PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',EndOfTheMonth(pdata))+#39);
                                             End
                                             Else Begin
                                                       //pela m�dia uso a data do romaneio<=
                                                       PqueryEntrada.sql.add('where    TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataEntregaRomaneio))+#39);

                                             End;


                                             PqueryEntrada.sql.add('and Tabperfilado_ep.perfiladoCor='+Fieldbyname('perfiladocor').asstring);
                                             PqueryEntrada.Open;

                                             Pcusto:=(PqueryEntrada.fieldbyname('somavalorfinal').asfloat/PqueryEntrada.fieldbyname('somaquantidade').asfloat)*Fieldbyname('soma').asfloat;

                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objperfilado_PP.perfiladoCor.perfilado.get_codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='PERFILADO';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objperfilado_PP.perfiladoCor.perfilado.Grupoperfilado.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                                   end;
                         End;

                         next;
                   End;//while dos perfilados
         End;
         //*********************************************************************

         //**********************kitbox******************************
         close;
         sql.clear;
         sql.add('Select Tabkitbox_PP.kitboxCor,sum(Tabkitbox_PP.Quantidade) as SOMA');
         sql.add('From tabkitbox_pp');
         sql.add('join TabPedido_Proj on Tabkitbox_PP.PedidoProjeto=tabpedido_proj.codigo');
         sql.add('where TabPedido_proj.Codigo='+PpedidoProjeto);
         sql.add('Group by tabkitbox_PP.kitboxCor');

         open;
         if (recordcount>0)
         Then begin
                   //teve kitbox nesse pedido
                   //pegando um a um e procurando o Custo
                   While not(eof) do
                   Begin
                         Objkitbox_PP.kitboxCor.LocalizaCodigo(Fieldbyname('kitboxcor').asstring);
                         Objkitbox_PP.kitboxCor.TabelaparaObjeto;



                         //Primeiro verificando se existe alguma compra antes da data do pedido
                         PqueryEntrada.Close;
                         PqueryEntrada.sql.clear;
                         PqueryEntrada.sql.add('Select TabEntradaProdutos.Data,Tabkitbox_EP.* From tabkitbox_EP');
                         PqueryEntrada.sql.add('join TabEntradaProdutos on tabkitbox_ep.Entrada=TabentradaProdutos.codigo');
                         PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataEntregaRomaneio))+#39);
                         PqueryEntrada.sql.add('and Tabkitbox_ep.kitboxCor='+Fieldbyname('kitboxcor').asstring);
                         PqueryEntrada.sql.add('order by TabEntradaProdutos.Data desc');//do maior pro menor
                         //showmessage(PqueryEntrada.sql.text);
                         //PqueryEntrada.sql.savetofile('c:\teste.sql');
                         PqueryEntrada.Open;
                         PqueryEntrada.first;
                         if (PqueryEntrada.Recordcount=0)//nao tem entrada entao uso o preco de custo
                         Then Begin
                                   try
                                      Pcusto:=Strtofloat(Objkitbox_PP.kitboxCor.kitbox.Get_PRECOCUSTO)*Fieldbyname('soma').asfloat;
                                   Except
                                      Pcusto:=0;
                                   End;
                                   FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objkitbox_PP.kitboxCor.kitbox.get_codigo;
                                   FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='KITBOX';

                                   FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objkitbox_PP.kitboxCor.kitbox.PlanoDeContas.Get_CODIGO;
                                   FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                   FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                         End
                         Else Begin//teve compra
                                   //Showmessage('ultima compra '+pqueryentrada.fieldbyname('data').asstring);
                                   if (FormatDateTime('mm/yyyy',pqueryentrada.fieldbyname('data').asdatetime)=FormatDateTime('mm/yyyy',strtodate(PdataEntregaRomaneio)))
                                   Then begin//se for o mesmo mes, tiro a media dentro do mes, do dia 1 ate o dia da venda
                                             Pdata:=strtodate(PdataEntregaRomaneio);



                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select sum(Tabkitbox_EP.quantidade) as somaquantidade, ');
                                             PqueryEntrada.sql.add('sum(');
                                             PqueryEntrada.sql.add('(Tabkitbox_EP.valorfinal+(coalesce(tabkitbox_ep.ipipago,0)*Tabkitbox_EP.valorfinal)/100)');
                                             PqueryEntrada.sql.add('-(coalesce(tabkitbox_ep.creditoicms,0)*Tabkitbox_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabkitbox_ep.creditoipi,0)*Tabkitbox_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabkitbox_ep.creditopis,0)*Tabkitbox_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabkitbox_ep.creditocofins,0)*Tabkitbox_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add(')');
                                             PqueryEntrada.sql.add('as somavalorfinal From tabkitbox_EP');

                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabkitbox_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);

                                             if (PMediaCompras=False)//se fosse pela media nao teria data inicial
                                             Then PqueryEntrada.sql.add('and TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);

                                             PqueryEntrada.sql.add('and Tabkitbox_ep.kitboxCor='+Fieldbyname('kitboxcor').asstring);
                                             PqueryEntrada.Open;

                                             Pcusto:=(PqueryEntrada.fieldbyname('somavalorfinal').asfloat/PqueryEntrada.fieldbyname('somaquantidade').asfloat)*Fieldbyname('soma').asfloat;

                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objkitbox_PP.kitboxCor.kitbox.get_codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='KITBOX';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objkitbox_PP.kitboxCor.kitbox.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                                   End
                                   Else Begin
                                             //nao foi dentro do mesmo mes mas teve compra, entao tiro a media do mes da ultima compra
                                             Pdata:=PqueryEntrada.Fieldbyname('data').asdatetime;
                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select sum(Tabkitbox_EP.quantidade) as somaquantidade, ');
                                             PqueryEntrada.sql.add('sum(');
                                             PqueryEntrada.sql.add('(Tabkitbox_EP.valorfinal+(coalesce(tabkitbox_ep.ipipago,0)*Tabkitbox_EP.valorfinal)/100)');
                                             PqueryEntrada.sql.add('-(coalesce(tabkitbox_ep.creditoicms,0)*Tabkitbox_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabkitbox_ep.creditoipi,0)*Tabkitbox_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabkitbox_ep.creditopis,0)*Tabkitbox_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabkitbox_ep.creditocofins,0)*Tabkitbox_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add(')');
                                             PqueryEntrada.sql.add('as somavalorfinal From tabkitbox_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabkitbox_ep.Entrada=TabentradaProdutos.codigo');

                                             if (PMediaCompras=False)
                                             then Begin
                                                       //uso o mes da ultima compra
                                                       PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                                       PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',EndOfTheMonth(pdata))+#39);
                                             End
                                             Else Begin
                                                       //pela m�dia tiro tudo comprado onde <=dataromaneio
                                                       PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataEntregaRomaneio))+#39);
                                             End;


                                             PqueryEntrada.sql.add('and Tabkitbox_ep.kitboxCor='+Fieldbyname('kitboxcor').asstring);
                                             PqueryEntrada.Open;

                                             Pcusto:=(PqueryEntrada.fieldbyname('somavalorfinal').asfloat/PqueryEntrada.fieldbyname('somaquantidade').asfloat)*Fieldbyname('soma').asfloat;

                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objkitbox_PP.kitboxCor.kitbox.get_codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='KITBOX';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objkitbox_PP.kitboxCor.kitbox.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                                   end;
                         End;

                         next;
                   End;//while dos kitboxs
         End;
         //*********************************************************************

         //**********************vidro******************************
         close;
         sql.clear;
         sql.add('Select Tabvidro_PP.vidroCor,sum(Tabvidro_PP.Quantidade) as SOMA');
         sql.add('From tabvidro_pp');
         sql.add('join TabPedido_Proj on Tabvidro_PP.PedidoProjeto=tabpedido_proj.codigo');
         sql.add('where TabPedido_proj.codigo='+PpedidoProjeto);
         sql.add('Group by tabvidro_PP.vidroCor');

         open;
         if (recordcount>0)
         Then begin
                   //teve vidro nesse pedido
                   //pegando um a um e procurando o Custo
                   While not(eof) do
                   Begin
                         Objvidro_PP.vidroCor.LocalizaCodigo(Fieldbyname('vidrocor').asstring);
                         Objvidro_PP.vidroCor.TabelaparaObjeto;



                         //Primeiro verificando se existe alguma compra antes da data do pedido
                         PqueryEntrada.Close;
                         PqueryEntrada.sql.clear;
                         PqueryEntrada.sql.add('Select TabEntradaProdutos.Data,Tabvidro_EP.* From tabvidro_EP');
                         PqueryEntrada.sql.add('join TabEntradaProdutos on tabvidro_ep.Entrada=TabentradaProdutos.codigo');
                         PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataEntregaRomaneio))+#39);
                         PqueryEntrada.sql.add('and Tabvidro_ep.vidroCor='+Fieldbyname('vidrocor').asstring);
                         PqueryEntrada.sql.add('order by TabEntradaProdutos.Data desc');//do maior pro menor
                         //showmessage(PqueryEntrada.sql.text);
                         //PqueryEntrada.sql.savetofile('c:\teste.sql');
                         //showmessage(Fieldbyname('vidrocor').asstring);
                         PqueryEntrada.Open;
                         PqueryEntrada.first;
                         if (PqueryEntrada.Recordcount=0)//nao tem entrada entao uso o preco de custo
                         Then Begin
                                   try
                                      Pcusto:=Strtofloat(Objvidro_PP.vidroCor.vidro.Get_PRECOCUSTO)*Fieldbyname('soma').asfloat;
                                   Except
                                      Pcusto:=0;
                                   End;
                                   FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objvidro_PP.vidroCor.vidro.get_codigo;
                                   FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='VIDRO';

                                   FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objvidro_PP.vidroCor.vidro.GrupoVidro.PlanoDeContas.Get_CODIGO;
                                   FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                   FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                         End
                         Else Begin//teve compra
                                   //Showmessage('ultima compra '+pqueryentrada.fieldbyname('data').asstring);
                                   if (FormatDateTime('mm/yyyy',pqueryentrada.fieldbyname('data').asdatetime)=FormatDateTime('mm/yyyy',strtodate(PdataEntregaRomaneio)))
                                   Then begin//se for o mesmo mes, tiro a media dentro do mes, do dia 1 ate o dia da venda
                                             Pdata:=strtodate(PdataEntregaRomaneio);

                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select sum(Tabvidro_EP.quantidade) as somaquantidade, ');
                                             PqueryEntrada.sql.add('sum(');
                                             PqueryEntrada.sql.add('(Tabvidro_EP.valorfinal+(coalesce(tabvidro_ep.ipipago,0)*Tabvidro_EP.valorfinal)/100)');
                                             PqueryEntrada.sql.add('-(coalesce(tabvidro_ep.creditoicms,0)*Tabvidro_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabvidro_ep.creditoipi,0)*Tabvidro_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabvidro_ep.creditopis,0)*Tabvidro_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabvidro_ep.creditocofins,0)*Tabvidro_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add(')');
                                             PqueryEntrada.sql.add('as somavalorfinal From tabvidro_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabvidro_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);

                                             if (PMediaCompras=False)//se fosse pela media nao teria data inicial
                                             Then PqueryEntrada.sql.add('and TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);

                                             PqueryEntrada.sql.add('and Tabvidro_ep.vidroCor='+Fieldbyname('vidrocor').asstring);
                                             PqueryEntrada.Open;

                                             Pcusto:=(PqueryEntrada.fieldbyname('somavalorfinal').asfloat/PqueryEntrada.fieldbyname('somaquantidade').asfloat)*Fieldbyname('soma').asfloat;

                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objvidro_PP.vidroCor.vidro.get_codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='VIDRO';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objvidro_PP.vidroCor.vidro.GrupoVidro.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                                   End
                                   Else Begin
                                             //nao foi dentro do mesmo mes mas teve compra, entao tiro a media do mes da ultima compra
                                             Pdata:=PqueryEntrada.Fieldbyname('data').asdatetime;
                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select sum(Tabvidro_EP.quantidade) as somaquantidade, ');
                                             PqueryEntrada.sql.add('sum(');
                                             PqueryEntrada.sql.add('(Tabvidro_EP.valorfinal+(coalesce(tabvidro_ep.ipipago,0)*Tabvidro_EP.valorfinal)/100)');
                                             PqueryEntrada.sql.add('-(coalesce(tabvidro_ep.creditoicms,0)*Tabvidro_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabvidro_ep.creditoipi,0)*Tabvidro_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabvidro_ep.creditopis,0)*Tabvidro_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabvidro_ep.creditocofins,0)*Tabvidro_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add(')');
                                             PqueryEntrada.sql.add('as somavalorfinal From tabvidro_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabvidro_ep.Entrada=TabentradaProdutos.codigo');
                                             if (PMediaCompras=False)
                                             then Begin
                                                       //uso o mes da ultima compra
                                                       PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                                       PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',EndOfTheMonth(pdata))+#39);
                                             End
                                             Else Begin
                                                       //pela m�dia tiro tudo comprado onde <=dataromaneio
                                                       PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataEntregaRomaneio))+#39);
                                             End;
                                             PqueryEntrada.sql.add('and Tabvidro_ep.vidroCor='+Fieldbyname('vidrocor').asstring);
                                             PqueryEntrada.Open;

                                             Pcusto:=(PqueryEntrada.fieldbyname('somavalorfinal').asfloat/PqueryEntrada.fieldbyname('somaquantidade').asfloat)*Fieldbyname('soma').asfloat;

                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objvidro_PP.vidroCor.vidro.get_codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='VIDRO';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objvidro_PP.vidroCor.vidro.GrupoVidro.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                                   end;
                         End;

                         next;
                   End;//while dos vidros
         End;
         //*********************************************************************

         //**********************Persiana******************************
         close;
         sql.clear;
         sql.add('Select TabPersiana_PP.persianagrupodiametrocor,sum(TabPersiana_PP.Quantidade) as SOMA');
         sql.add('From tabPersiana_pp');
         sql.add('join TabPedido_Proj on TabPersiana_PP.PedidoProjeto=tabpedido_proj.codigo');
         sql.add('where TabPedido_proj.codigo='+PpedidoProjeto);
         sql.add('Group by tabPersiana_PP.persianagrupodiametrocor');

         open;
         if (recordcount>0)
         Then begin
                   //teve Persiana nesse pedido
                   //pegando um a um e procurando o Custo
                   While not(eof) do
                   Begin
                         ObjPersiana_PP.persianagrupodiametrocor.LocalizaCodigo(Fieldbyname('persianagrupodiametrocor').asstring);
                         ObjPersiana_PP.persianagrupodiametrocor.TabelaparaObjeto;

                         //Primeiro verificando se existe alguma compra antes da data do pedido
                         PqueryEntrada.Close;
                         PqueryEntrada.sql.clear;
                         PqueryEntrada.sql.add('Select TabEntradaProdutos.Data,TabPersiana_EP.* From tabPersiana_EP');
                         PqueryEntrada.sql.add('join TabEntradaProdutos on tabPersiana_ep.Entrada=TabentradaProdutos.codigo');
                         PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataEntregaRomaneio))+#39);
                         PqueryEntrada.sql.add('and TabPersiana_ep.persianagrupodiametrocor='+Fieldbyname('persianagrupodiametrocor').asstring);
                         PqueryEntrada.sql.add('order by TabEntradaProdutos.Data desc');//do maior pro menor
                         //showmessage(PqueryEntrada.sql.text);
                         //PqueryEntrada.sql.savetofile('c:\teste.sql');
                         PqueryEntrada.Open;
                         PqueryEntrada.first;
                         if (PqueryEntrada.Recordcount=0)//nao tem entrada entao uso o preco de custo
                         Then Begin
                                   try
                                      Pcusto:=Strtofloat(ObjPersiana_PP.persianagrupodiametrocor.Get_PRECOCUSTO)*Fieldbyname('soma').asfloat;
                                   Except
                                      Pcusto:=0;
                                   End;
                                   FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=ObjPersiana_PP.persianagrupodiametrocor.Persiana.Get_Codigo;
                                   FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='PERSIANA';

                                   FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=ObjPersiana_PP.persianagrupodiametrocor.GrupoPersiana.PlanoDeContas.Get_CODIGO;
                                   FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                   FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                         End
                         Else Begin//teve compra
                                   //Showmessage('ultima compra '+pqueryentrada.fieldbyname('data').asstring);
                                   if (FormatDateTime('mm/yyyy',pqueryentrada.fieldbyname('data').asdatetime)=FormatDateTime('mm/yyyy',strtodate(PdataEntregaRomaneio)))
                                   Then begin//se for o mesmo mes, tiro a media dentro do mes, do dia 1 ate o dia da venda
                                             Pdata:=strtodate(PdataEntregaRomaneio);

                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select sum(Tabpersiana_EP.quantidade) as somaquantidade, ');
                                             PqueryEntrada.sql.add('sum(');
                                             PqueryEntrada.sql.add('(Tabpersiana_EP.valorfinal+(coalesce(tabpersiana_ep.ipipago,0)*Tabpersiana_EP.valorfinal)/100)');
                                             PqueryEntrada.sql.add('-(coalesce(tabpersiana_ep.creditoicms,0)*Tabpersiana_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabpersiana_ep.creditoipi,0)*Tabpersiana_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabpersiana_ep.creditopis,0)*Tabpersiana_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabpersiana_ep.creditocofins,0)*Tabpersiana_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add(')');
                                             PqueryEntrada.sql.add('as somavalorfinal From tabpersiana_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabPersiana_ep.Entrada=TabentradaProdutos.codigo');

                                             PqueryEntrada.sql.add('where  TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
                                             PqueryEntrada.sql.add('and TabPersiana_ep.persianagrupodiametrocor='+Fieldbyname('persianagrupodiametrocor').asstring);

                                             if (PMediaCompras=False)//pela m�dia nao tem data inicial
                                             Then PqueryEntrada.sql.add('and TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);

                                             PqueryEntrada.Open;

                                             Pcusto:=(PqueryEntrada.fieldbyname('somavalorfinal').asfloat/PqueryEntrada.fieldbyname('somaquantidade').asfloat)*Fieldbyname('soma').asfloat;

                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=ObjPersiana_PP.persianagrupodiametrocor.Persiana.Get_Codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='PERSIANA';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=ObjPersiana_PP.persianagrupodiametrocor.GrupoPersiana.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                                   End
                                   Else Begin
                                             //nao foi dentro do mesmo mes mas teve compra, entao tiro a media do mes da ultima compra
                                             Pdata:=PqueryEntrada.Fieldbyname('data').asdatetime;
                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select sum(Tabpersiana_EP.quantidade) as somaquantidade, ');
                                             PqueryEntrada.sql.add('sum(');
                                             PqueryEntrada.sql.add('(Tabpersiana_EP.valorfinal+(coalesce(tabpersiana_ep.ipipago,0)*Tabpersiana_EP.valorfinal)/100)');
                                             PqueryEntrada.sql.add('-(coalesce(tabpersiana_ep.creditoicms,0)*Tabpersiana_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabpersiana_ep.creditoipi,0)*Tabpersiana_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabpersiana_ep.creditopis,0)*Tabpersiana_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add('-(coalesce(tabpersiana_ep.creditocofins,0)*Tabpersiana_EP.valorfinal)/100');
                                             PqueryEntrada.sql.add(')');
                                             PqueryEntrada.sql.add('as somavalorfinal From tabpersiana_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabPersiana_ep.Entrada=TabentradaProdutos.codigo');
                                             if (PMediaCompras=False)
                                             then Begin
                                                       //uso o mes da ultima compra
                                                       PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                                       PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',EndOfTheMonth(pdata))+#39);
                                             End
                                             Else Begin
                                                       //pela m�dia tiro tudo comprado onde <=dataromaneio
                                                       PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataEntregaRomaneio))+#39);
                                             End;
                                             PqueryEntrada.sql.add('and TabPersiana_ep.persianagrupodiametrocor='+Fieldbyname('persianagrupodiametrocor').asstring);
                                             PqueryEntrada.Open;

                                             Pcusto:=(PqueryEntrada.fieldbyname('somavalorfinal').asfloat/PqueryEntrada.fieldbyname('somaquantidade').asfloat)*Fieldbyname('soma').asfloat;

                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=ObjPersiana_PP.persianagrupodiametrocor.Persiana.Get_Codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='PERSIANA';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=ObjPersiana_PP.persianagrupodiametrocor.GrupoPersiana.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                                   end;
                         End;

                         next;
                   End;//while dos Persianas


         End;
         //*********************************************************************

         FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount-1;
         close;
         sql.clear;
         sql.add('SELECT GEN_ID(GenLoteExporta,1) CODIGO FROM RDB$DATABASE');
         open;
         Plote:=fieldbyname('codigo').asstring;

         //Conferindo se todos os produtos tem codigo no plano de contas
         for cont:=1 to FmostraStringGrid.StringGrid.Rowcount-1 do
         begin
              if (FmostraStringGrid.StringGrid.Cells[1,cont]='')
              Then begin
                        MensagemErro('O(a) '+FmostraStringGrid.StringGrid.cells[3,cont]+' c�digo '+FmostraStringGrid.StringGrid.cells[0,cont]+' n�o possui c�digo de plano de contas');
                        exit;
              End;
              close;
              sql.clear;
              sql.add('Insert into TabTempContabil (codigo,CodigoPlanodecontas,Soma,loteexporta)');
              sql.add('values (0,'+FmostraStringGrid.StringGrid.Cells[1,cont]);
              sql.add(','+virgulaparaponto(FmostraStringGrid.StringGrid.Cells[2,cont]));
              sql.add(','+Plote+')');
              execsql;
         End;

         //Agrupando por codigo do plano de contas
         close;
         sql.clear;
         sql.add('Select codigoplanodecontas,sum(soma) as soma');
         sql.add('from TabTempContabil');
         sql.add('where LoteExporta='+Plote);
         sql.add('group by Codigoplanodecontas');
         open;

         while not(eof) do
         Begin
              ObjExportaContabilidade.ZerarTabela;
              ObjExportaContabilidade.status:=dsinsert;
              ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
              ObjExportaContabilidade.Submit_Data(PdataEntregaRomaneio);
              ObjExportaContabilidade.Submit_ContaDebite(PcontaCmv);
              ObjExportaContabilidade.Submit_ContaCredite(Fieldbyname('codigoplanodecontas').asstring);
              ObjExportaContabilidade.Submit_Valor(fieldbyname('soma').asstring);
              ObjExportaContabilidade.Submit_Historico('CMV-Venda '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.get_Codigo+' Ped.Proj. '+PpedidoProjeto+'-'+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
              ObjExportaContabilidade.Submit_ObjGerador('OBJROMANEIO');
              ObjExportaContabilidade.Submit_CodGerador(PRomaneio);
              ObjExportaContabilidade.Submit_Exportado('N');
              ObjExportaContabilidade.Submit_Exporta('S');
              ObjExportaContabilidade.Submit_NomedoArquivo('');
              if (ObjExportaContabilidade.Salvar(False,False)=False)
              Then Begin
                        MensagemErro('Erro na tentativa de gravar a contabilidade do estoque');
                        exit;
              End;

              next;
         End;

         close;
         sql.clear;
         sql.add('Delete from TabTempContabil');
         sql.add('where LoteExporta='+Plote);
         execsql;
         result:=True;
     End;//With
    Except
          on e:exception do
          Begin
               result:=False;
               MensagemErro('Erro na gera��o da contabilidade '+#13+E.message);
               exit;
          End;

    End;

Finally
      Freeandnil(PqueryEntrada);
      Freeandnil(PPlanodeContasFinal);
      Freeandnil(PvalorFinal);
end;

End;

{Function TObjPedidoObjetos.ExportaContabilidade_Estoque(pcodigo:string):Boolean;
var
PcontaCmv:string;
PqueryEntrada:TibQuery;
Pcusto:Currency;
Pdata:Tdate;
PPlanodeContasFinal,PvalorFinal:TStringList;
cont:integer;
Plote:string;
Begin
{
Estoque

Para baixar o estoque de cada cadastro (grupo) n�o posso baixar no valor de
venda e sim no valor de custo
ou seja = quantidade vendida * valor de custo

Para encontrar o valor de Custo
O valor do custo � o valor m�dio unit�rio  da compra  dentro do per�odo da venda (m�s)

Exemplo venda do dia 15/11

Encontro a media de compra 1/11 a 15/11
se  n�o houve compra no m�s 11 vou para o m�s 10 e assim ate encontrar uma compra,
ao encontrar utilizo a media daquele m�s. Senao tiver compra antes do periodo da Venda
Utilizo o valor do campo Custo.

Credita					                  Debito
Estoque (de acordo com o produto)	cmv (parametrizar)


MODIFICADO 27/01/07

ESSE CMV JA ERA

FAZER NA CONCLUSAO DO ROMANEIO,

A DATA UTILIZADA PARA CALCULO DO CUSTO NAO SERA DA VENDA E SIM DATA DE ENTREGA DO ROMANEIO
E OS PRODUTOS SELECIONADOS NAO SERAO DO PEDIDO E SIM APENAS DO PEDIDO PROJETO QUE
SERA ENTREGUE NO ROMANEIO.

}
{    result:=False;

    if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(pcodigo)=False)
    then Begin
              MensagemErro('Pedido n�o encontrado');
              exit;
    End;
    Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;


Try
    PqueryEntrada:=TibQuery.create(nil);
    PqueryEntrada.Database:=FDataModulo.IBDatabase;

    PPlanodeContasFinal:=TStringList.Create;
    PvalorFinal:=TStringList.create;

Except
    MensagemErro('Erro na tentativa de Criar a Query PqueryEntrada');
    exit;
End;

Try

    PcontaCmv:='';
    if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL PARA CMV NA VENDA')=true)
    then PContaCmv:=Objparametroglobal.get_valor;

    FmostraStringGrid.Configuracoesiniciais;
    FmostraStringGrid.StringGrid.ColCount:=4;
    FmostraStringGrid.StringGrid.RowCount:=2;
    FmostraStringGrid.StringGrid.Cols[0].clear;
    FmostraStringGrid.StringGrid.Cols[1].clear;
    FmostraStringGrid.StringGrid.Cols[2].clear;
    FmostraStringGrid.StringGrid.Cols[3].clear;
    FmostraStringGrid.StringGrid.Cells[0,0]:='CODIGO CADASTRO';
    FmostraStringGrid.StringGrid.Cells[1,0]:='PLANO DE CONTAS';
    FmostraStringGrid.StringGrid.Cells[2,0]:='VALOR DO CUSTO';
    FmostraStringGrid.StringGrid.Cells[3,0]:='CADASTRO';


    with Self.ObjqueryTEMP do
    begin
         //**********************DIVERSO******************************
         close;
         sql.clear;
         sql.add('Select TabDiverso_PP.DiversoCor,sum(TabDiverso_PP.Quantidade) as SOMA');
         sql.add('From tabDiverso_pp');
         sql.add('join TabPedido_Proj on TabDiverso_PP.PedidoProjeto=tabpedido_proj.codigo');
         sql.add('where TabPedido_proj.Pedido='+pcodigo);
         sql.add('Group by tabDiverso_PP.DiversoCor');

         open;
         if (recordcount>0)
         Then begin
                   //teve diverso nesse pedido
                   //pegando um a um e procurando o Custo
                   While not(eof) do
                   Begin
                         ObjDiverso_PP.DiversoCor.LocalizaCodigo(Fieldbyname('diversocor').asstring);
                         ObjDiverso_PP.DiversoCor.TabelaparaObjeto;



                         //Primeiro verificando se existe alguma compra antes da data do pedido
                         PqueryEntrada.Close;
                         PqueryEntrada.sql.clear;
                         PqueryEntrada.sql.add('Select TabEntradaProdutos.Data,TabDiverso_EP.* From tabDiverso_EP');
                         PqueryEntrada.sql.add('join TabEntradaProdutos on tabDiverso_ep.Entrada=TabentradaProdutos.codigo');
                         PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data))+#39);
                         PqueryEntrada.sql.add('and TabDiverso_ep.DiversoCor='+Fieldbyname('diversocor').asstring);
                         PqueryEntrada.sql.add('order by TabEntradaProdutos.Data desc');//do maior pro menor
                         PqueryEntrada.Open;
                         PqueryEntrada.first;
                         if (PqueryEntrada.Recordcount=0)//nao tem entrada entao uso o preco de custo
                         Then Begin
                                   try
                                      Pcusto:=Strtofloat(ObjDiverso_PP.DiversoCor.Diverso.Get_PRECOCUSTO)*Fieldbyname('soma').asinteger;
                                   Except
                                      Pcusto:=0;
                                   End;

                                   FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=ObjDiverso_PP.DiversoCor.Diverso.GrupoDiverso.PlanoDeContas.Get_CODIGO;
                                   FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);

                                   FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=ObjDiverso_PP.DiversoCor.Diverso.Get_Codigo;
                                   FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='DIVERSO';

                                   FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                         End
                         Else Begin//teve compra
                                   //Showmessage('ultima compra '+pqueryentrada.fieldbyname('data').asstring);
                                   if (FormatDateTime('mm/yyyy',pqueryentrada.fieldbyname('data').asdatetime)=FormatDateTime('mm/yyyy',strtodate(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data)))
                                   Then begin//se for o mesmo mes, tiro a media dentro do mes, do dia 1 ate o dia da venda
                                             Pdata:=strtodate(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);

                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select avg(TabDiverso_EP.valor) as MEDIA From tabDiverso_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabDiverso_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                             PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
                                             PqueryEntrada.sql.add('and TabDiverso_ep.DiversoCor='+Fieldbyname('diversocor').asstring);
                                             //showmessage(PqueryEntrada.sql.Text);
                                             PqueryEntrada.Open;
                                             Pcusto:=PqueryEntrada.fieldbyname('media').asfloat*Fieldbyname('soma').asinteger;

                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=ObjDiverso_PP.DiversoCor.Diverso.GrupoDiverso.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);

                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=ObjDiverso_PP.DiversoCor.Diverso.Get_Codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='DIVERSO';

                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                                   End
                                   Else Begin
                                             //nao foi dentro do mesmo mes mas teve compra, entao tiro a media do mes da ultima compra
                                             Pdata:=PqueryEntrada.Fieldbyname('data').asdatetime;
                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select avg(TabDiverso_EP.valor) as MEDIA From tabDiverso_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabDiverso_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                             PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',EndOfTheMonth(pdata))+#39);
                                             PqueryEntrada.sql.add('and TabDiverso_ep.DiversoCor='+Fieldbyname('diversocor').asstring);
                                             //showmessage(PqueryEntrada.sql.Text);
                                             PqueryEntrada.Open;
                                             Pcusto:=PqueryEntrada.fieldbyname('media').asfloat*Fieldbyname('soma').asinteger;

                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=ObjDiverso_PP.DiversoCor.Diverso.GrupoDiverso.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=ObjDiverso_PP.DiversoCor.Diverso.Get_Codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='DIVERSO';

                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                                   end;
                         End;

                         next;
                   End;//while dos diversos
         End;
         //*********************************************************************

         //**********************FERRAGEM******************************
         close;
         sql.clear;
         sql.add('Select Tabferragem_PP.ferragemCor,sum(Tabferragem_PP.Quantidade) as SOMA');
         sql.add('From tabferragem_pp');
         sql.add('join TabPedido_Proj on Tabferragem_PP.PedidoProjeto=tabpedido_proj.codigo');
         sql.add('where TabPedido_proj.Pedido='+pcodigo);
         sql.add('Group by tabferragem_PP.ferragemCor');

         open;
         if (recordcount>0)
         Then begin
                   //teve ferragem nesse pedido
                   //pegando um a um e procurando o Custo
                   While not(eof) do
                   Begin
                         Objferragem_PP.ferragemCor.LocalizaCodigo(Fieldbyname('ferragemcor').asstring);
                         Objferragem_PP.ferragemCor.TabelaparaObjeto;



                         //Primeiro verificando se existe alguma compra antes da data do pedido
                         PqueryEntrada.Close;
                         PqueryEntrada.sql.clear;
                         PqueryEntrada.sql.add('Select TabEntradaProdutos.Data,Tabferragem_EP.* From tabferragem_EP');
                         PqueryEntrada.sql.add('join TabEntradaProdutos on tabferragem_ep.Entrada=TabentradaProdutos.codigo');
                         PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data))+#39);
                         PqueryEntrada.sql.add('and Tabferragem_ep.ferragemCor='+Fieldbyname('ferragemcor').asstring);
                         PqueryEntrada.sql.add('order by TabEntradaProdutos.Data desc');//do maior pro menor
                         PqueryEntrada.Open;
                         PqueryEntrada.first;
                         if (PqueryEntrada.Recordcount=0)//nao tem entrada entao uso o preco de custo
                         Then Begin
                                   try
                                      Pcusto:=Strtofloat(Objferragem_PP.ferragemCor.ferragem.Get_PRECOCUSTO)*Fieldbyname('soma').asinteger;
                                   Except
                                      Pcusto:=0;
                                   End;


                                   FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objferragem_PP.ferragemCor.ferragem.Grupoferragem.PlanoDeContas.Get_CODIGO;
                                   FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                   FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objferragem_PP.ferragemCor.ferragem.Get_Codigo;
                                   FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='FERRAGEM';

                                   FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                         End
                         Else Begin//teve compra
                                   //Showmessage('ultima compra '+pqueryentrada.fieldbyname('data').asstring);
                                   if (FormatDateTime('mm/yyyy',pqueryentrada.fieldbyname('data').asdatetime)=FormatDateTime('mm/yyyy',strtodate(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data)))
                                   Then begin//se for o mesmo mes, tiro a media dentro do mes, do dia 1 ate o dia da venda
                                             Pdata:=strtodate(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);

                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select avg(Tabferragem_EP.valor) as MEDIA From tabferragem_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabferragem_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                             PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
                                             PqueryEntrada.sql.add('and Tabferragem_ep.ferragemCor='+Fieldbyname('ferragemcor').asstring);
                                             //showmessage(PqueryEntrada.sql.Text);
                                             PqueryEntrada.Open;
                                             Pcusto:=PqueryEntrada.fieldbyname('media').asfloat*Fieldbyname('soma').asinteger;
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objferragem_PP.ferragemCor.ferragem.Grupoferragem.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objferragem_PP.ferragemCor.ferragem.Get_Codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='FERRAGEM';
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                                   End
                                   Else Begin
                                             //nao foi dentro do mesmo mes mas teve compra, entao tiro a media do mes da ultima compra
                                             Pdata:=PqueryEntrada.Fieldbyname('data').asdatetime;
                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select avg(Tabferragem_EP.valor) as MEDIA From tabferragem_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabferragem_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                             PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',EndOfTheMonth(pdata))+#39);
                                             PqueryEntrada.sql.add('and Tabferragem_ep.ferragemCor='+Fieldbyname('ferragemcor').asstring);
                                             //showmessage(PqueryEntrada.sql.Text);
                                             PqueryEntrada.Open;
                                             Pcusto:=PqueryEntrada.fieldbyname('media').asfloat*Fieldbyname('soma').asinteger;

                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objferragem_PP.ferragemCor.ferragem.Grupoferragem.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objferragem_PP.ferragemCor.ferragem.Get_Codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='FERRAGEM';
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                                   end;
                         End;

                         next;
                   End;//while dos ferragems
         End;
         //*********************************************************************

         //**********************perfilado******************************
         close;
         sql.clear;
         sql.add('Select Tabperfilado_PP.perfiladoCor,sum(Tabperfilado_PP.Quantidade) as SOMA');
         sql.add('From tabperfilado_pp');
         sql.add('join TabPedido_Proj on Tabperfilado_PP.PedidoProjeto=tabpedido_proj.codigo');
         sql.add('where TabPedido_proj.Pedido='+pcodigo);
         sql.add('Group by tabperfilado_PP.perfiladoCor');

         open;
         if (recordcount>0)
         Then begin
                   //teve perfilado nesse pedido
                   //pegando um a um e procurando o Custo
                   While not(eof) do
                   Begin
                         Objperfilado_PP.perfiladoCor.LocalizaCodigo(Fieldbyname('perfiladocor').asstring);
                         Objperfilado_PP.perfiladoCor.TabelaparaObjeto;



                         //Primeiro verificando se existe alguma compra antes da data do pedido
                         PqueryEntrada.Close;
                         PqueryEntrada.sql.clear;
                         PqueryEntrada.sql.add('Select TabEntradaProdutos.Data,Tabperfilado_EP.* From tabperfilado_EP');
                         PqueryEntrada.sql.add('join TabEntradaProdutos on tabperfilado_ep.Entrada=TabentradaProdutos.codigo');
                         PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data))+#39);
                         PqueryEntrada.sql.add('and Tabperfilado_ep.perfiladoCor='+Fieldbyname('perfiladocor').asstring);
                         PqueryEntrada.sql.add('order by TabEntradaProdutos.Data desc');//do maior pro menor
                         PqueryEntrada.Open;
                         PqueryEntrada.first;
                         if (PqueryEntrada.Recordcount=0)//nao tem entrada entao uso o preco de custo
                         Then Begin
                                   try
                                      Pcusto:=Strtofloat(Objperfilado_PP.perfiladoCor.perfilado.Get_PRECOCUSTO)*Fieldbyname('soma').asinteger;
                                   Except
                                      Pcusto:=0;
                                   End;
                                   FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objperfilado_PP.perfiladoCor.perfilado.get_codigo;
                                   FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='PERFILADO';

                                   FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objperfilado_PP.perfiladoCor.perfilado.Grupoperfilado.PlanoDeContas.Get_CODIGO;
                                   FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                   FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                         End
                         Else Begin//teve compra
                                   //Showmessage('ultima compra '+pqueryentrada.fieldbyname('data').asstring);
                                   if (FormatDateTime('mm/yyyy',pqueryentrada.fieldbyname('data').asdatetime)=FormatDateTime('mm/yyyy',strtodate(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data)))
                                   Then begin//se for o mesmo mes, tiro a media dentro do mes, do dia 1 ate o dia da venda
                                             Pdata:=strtodate(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);

                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select avg(Tabperfilado_EP.valor) as MEDIA From tabperfilado_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabperfilado_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                             PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
                                             PqueryEntrada.sql.add('and Tabperfilado_ep.perfiladoCor='+Fieldbyname('perfiladocor').asstring);
                                             //showmessage(PqueryEntrada.sql.Text);
                                             PqueryEntrada.Open;
                                             Pcusto:=PqueryEntrada.fieldbyname('media').asfloat*Fieldbyname('soma').asinteger;
                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objperfilado_PP.perfiladoCor.perfilado.get_codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='PERFILADO';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objperfilado_PP.perfiladoCor.perfilado.Grupoperfilado.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                                   End
                                   Else Begin
                                             //nao foi dentro do mesmo mes mas teve compra, entao tiro a media do mes da ultima compra
                                             Pdata:=PqueryEntrada.Fieldbyname('data').asdatetime;
                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select avg(Tabperfilado_EP.valor) as MEDIA From tabperfilado_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabperfilado_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                             PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',EndOfTheMonth(pdata))+#39);
                                             PqueryEntrada.sql.add('and Tabperfilado_ep.perfiladoCor='+Fieldbyname('perfiladocor').asstring);
                                             //showmessage(PqueryEntrada.sql.Text);
                                             PqueryEntrada.Open;
                                             Pcusto:=PqueryEntrada.fieldbyname('media').asfloat*Fieldbyname('soma').asinteger;
                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objperfilado_PP.perfiladoCor.perfilado.get_codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='PERFILADO';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objperfilado_PP.perfiladoCor.perfilado.Grupoperfilado.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                                   end;
                         End;

                         next;
                   End;//while dos perfilados
         End;
         //*********************************************************************

         //**********************kitbox******************************
         close;
         sql.clear;
         sql.add('Select Tabkitbox_PP.kitboxCor,sum(Tabkitbox_PP.Quantidade) as SOMA');
         sql.add('From tabkitbox_pp');
         sql.add('join TabPedido_Proj on Tabkitbox_PP.PedidoProjeto=tabpedido_proj.codigo');
         sql.add('where TabPedido_proj.Pedido='+pcodigo);
         sql.add('Group by tabkitbox_PP.kitboxCor');

         open;
         if (recordcount>0)
         Then begin
                   //teve kitbox nesse pedido
                   //pegando um a um e procurando o Custo
                   While not(eof) do
                   Begin
                         Objkitbox_PP.kitboxCor.LocalizaCodigo(Fieldbyname('kitboxcor').asstring);
                         Objkitbox_PP.kitboxCor.TabelaparaObjeto;



                         //Primeiro verificando se existe alguma compra antes da data do pedido
                         PqueryEntrada.Close;
                         PqueryEntrada.sql.clear;
                         PqueryEntrada.sql.add('Select TabEntradaProdutos.Data,Tabkitbox_EP.* From tabkitbox_EP');
                         PqueryEntrada.sql.add('join TabEntradaProdutos on tabkitbox_ep.Entrada=TabentradaProdutos.codigo');
                         PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data))+#39);
                         PqueryEntrada.sql.add('and Tabkitbox_ep.kitboxCor='+Fieldbyname('kitboxcor').asstring);
                         PqueryEntrada.sql.add('order by TabEntradaProdutos.Data desc');//do maior pro menor
                         //showmessage(PqueryEntrada.sql.text);
                         //PqueryEntrada.sql.savetofile('c:\teste.sql');
                         PqueryEntrada.Open;
                         PqueryEntrada.first;
                         if (PqueryEntrada.Recordcount=0)//nao tem entrada entao uso o preco de custo
                         Then Begin
                                   try
                                      Pcusto:=Strtofloat(Objkitbox_PP.kitboxCor.kitbox.Get_PRECOCUSTO)*Fieldbyname('soma').asinteger;
                                   Except
                                      Pcusto:=0;
                                   End;
                                   FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objkitbox_PP.kitboxCor.kitbox.get_codigo;
                                   FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='KITBOX';

                                   FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objkitbox_PP.kitboxCor.kitbox.PlanoDeContas.Get_CODIGO;
                                   FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                   FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                         End
                         Else Begin//teve compra
                                   //Showmessage('ultima compra '+pqueryentrada.fieldbyname('data').asstring);
                                   if (FormatDateTime('mm/yyyy',pqueryentrada.fieldbyname('data').asdatetime)=FormatDateTime('mm/yyyy',strtodate(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data)))
                                   Then begin//se for o mesmo mes, tiro a media dentro do mes, do dia 1 ate o dia da venda
                                             Pdata:=strtodate(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);

                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select avg(Tabkitbox_EP.valor) as MEDIA From tabkitbox_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabkitbox_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                             PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
                                             PqueryEntrada.sql.add('and Tabkitbox_ep.kitboxCor='+Fieldbyname('kitboxcor').asstring);
                                             //showmessage(PqueryEntrada.sql.Text);
                                             PqueryEntrada.Open;
                                             Pcusto:=PqueryEntrada.fieldbyname('media').asfloat*Fieldbyname('soma').asinteger;
                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objkitbox_PP.kitboxCor.kitbox.get_codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='KITBOX';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objkitbox_PP.kitboxCor.kitbox.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                                   End
                                   Else Begin
                                             //nao foi dentro do mesmo mes mas teve compra, entao tiro a media do mes da ultima compra
                                             Pdata:=PqueryEntrada.Fieldbyname('data').asdatetime;
                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select avg(Tabkitbox_EP.valor) as MEDIA From tabkitbox_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabkitbox_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                             PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',EndOfTheMonth(pdata))+#39);
                                             PqueryEntrada.sql.add('and Tabkitbox_ep.kitboxCor='+Fieldbyname('kitboxcor').asstring);
                                             //showmessage(PqueryEntrada.sql.Text);
                                             PqueryEntrada.Open;
                                             Pcusto:=PqueryEntrada.fieldbyname('media').asfloat*Fieldbyname('soma').asinteger;
                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objkitbox_PP.kitboxCor.kitbox.get_codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='KITBOX';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objkitbox_PP.kitboxCor.kitbox.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                                   end;
                         End;

                         next;
                   End;//while dos kitboxs
         End;
         //*********************************************************************

         //**********************vidro******************************
         close;
         sql.clear;
         sql.add('Select Tabvidro_PP.vidroCor,sum(Tabvidro_PP.Quantidade) as SOMA');
         sql.add('From tabvidro_pp');
         sql.add('join TabPedido_Proj on Tabvidro_PP.PedidoProjeto=tabpedido_proj.codigo');
         sql.add('where TabPedido_proj.Pedido='+pcodigo);
         sql.add('Group by tabvidro_PP.vidroCor');

         open;
         if (recordcount>0)
         Then begin
                   //teve vidro nesse pedido
                   //pegando um a um e procurando o Custo
                   While not(eof) do
                   Begin
                         Objvidro_PP.vidroCor.LocalizaCodigo(Fieldbyname('vidrocor').asstring);
                         Objvidro_PP.vidroCor.TabelaparaObjeto;



                         //Primeiro verificando se existe alguma compra antes da data do pedido
                         PqueryEntrada.Close;
                         PqueryEntrada.sql.clear;
                         PqueryEntrada.sql.add('Select TabEntradaProdutos.Data,Tabvidro_EP.* From tabvidro_EP');
                         PqueryEntrada.sql.add('join TabEntradaProdutos on tabvidro_ep.Entrada=TabentradaProdutos.codigo');
                         PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data))+#39);
                         PqueryEntrada.sql.add('and Tabvidro_ep.vidroCor='+Fieldbyname('vidrocor').asstring);
                         PqueryEntrada.sql.add('order by TabEntradaProdutos.Data desc');//do maior pro menor
                         //showmessage(PqueryEntrada.sql.text);
                         //PqueryEntrada.sql.savetofile('c:\teste.sql');
                         PqueryEntrada.Open;
                         PqueryEntrada.first;
                         if (PqueryEntrada.Recordcount=0)//nao tem entrada entao uso o preco de custo
                         Then Begin
                                   try
                                      Pcusto:=Strtofloat(Objvidro_PP.vidroCor.vidro.Get_PRECOCUSTO)*Fieldbyname('soma').asinteger;
                                   Except
                                      Pcusto:=0;
                                   End;
                                   FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objvidro_PP.vidroCor.vidro.get_codigo;
                                   FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='VIDRO';

                                   FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objvidro_PP.vidroCor.vidro.GrupoVidro.PlanoDeContas.Get_CODIGO;
                                   FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                   FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                         End
                         Else Begin//teve compra
                                   //Showmessage('ultima compra '+pqueryentrada.fieldbyname('data').asstring);
                                   if (FormatDateTime('mm/yyyy',pqueryentrada.fieldbyname('data').asdatetime)=FormatDateTime('mm/yyyy',strtodate(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data)))
                                   Then begin//se for o mesmo mes, tiro a media dentro do mes, do dia 1 ate o dia da venda
                                             Pdata:=strtodate(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);

                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select avg(Tabvidro_EP.valor) as MEDIA From tabvidro_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabvidro_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                             PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
                                             PqueryEntrada.sql.add('and Tabvidro_ep.vidroCor='+Fieldbyname('vidrocor').asstring);
                                             //showmessage(PqueryEntrada.sql.Text);
                                             PqueryEntrada.Open;
                                             Pcusto:=PqueryEntrada.fieldbyname('media').asfloat*Fieldbyname('soma').asinteger;
                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objvidro_PP.vidroCor.vidro.get_codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='VIDRO';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objvidro_PP.vidroCor.vidro.GrupoVidro.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                                   End
                                   Else Begin
                                             //nao foi dentro do mesmo mes mas teve compra, entao tiro a media do mes da ultima compra
                                             Pdata:=PqueryEntrada.Fieldbyname('data').asdatetime;
                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select avg(Tabvidro_EP.valor) as MEDIA From tabvidro_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabvidro_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                             PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',EndOfTheMonth(pdata))+#39);
                                             PqueryEntrada.sql.add('and Tabvidro_ep.vidroCor='+Fieldbyname('vidrocor').asstring);
                                             //showmessage(PqueryEntrada.sql.Text);
                                             PqueryEntrada.sql.savetofile('c:\teste.sql');
                                             PqueryEntrada.Open;
                                             Pcusto:=PqueryEntrada.fieldbyname('media').asfloat*Fieldbyname('soma').asinteger;
                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=Objvidro_PP.vidroCor.vidro.get_codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='VIDRO';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=Objvidro_PP.vidroCor.vidro.GrupoVidro.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                                   end;
                         End;

                         next;
                   End;//while dos vidros
         End;
         //*********************************************************************


         //**********************Persiana******************************
         close;
         sql.clear;
         sql.add('Select TabPersiana_PP.persianagrupodiametrocor,sum(TabPersiana_PP.Quantidade) as SOMA');
         sql.add('From tabPersiana_pp');
         sql.add('join TabPedido_Proj on TabPersiana_PP.PedidoProjeto=tabpedido_proj.codigo');
         sql.add('where TabPedido_proj.Pedido='+pcodigo);
         sql.add('Group by tabPersiana_PP.persianagrupodiametrocor');

         open;
         if (recordcount>0)
         Then begin
                   //teve Persiana nesse pedido
                   //pegando um a um e procurando o Custo
                   While not(eof) do
                   Begin
                         ObjPersiana_PP.persianagrupodiametrocor.LocalizaCodigo(Fieldbyname('persianagrupodiametrocor').asstring);
                         ObjPersiana_PP.persianagrupodiametrocor.TabelaparaObjeto;



                         //Primeiro verificando se existe alguma compra antes da data do pedido
                         PqueryEntrada.Close;
                         PqueryEntrada.sql.clear;
                         PqueryEntrada.sql.add('Select TabEntradaProdutos.Data,TabPersiana_EP.* From tabPersiana_EP');
                         PqueryEntrada.sql.add('join TabEntradaProdutos on tabPersiana_ep.Entrada=TabentradaProdutos.codigo');
                         PqueryEntrada.sql.add('where TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data))+#39);
                         PqueryEntrada.sql.add('and TabPersiana_ep.persianagrupodiametrocor='+Fieldbyname('persianagrupodiametrocor').asstring);
                         PqueryEntrada.sql.add('order by TabEntradaProdutos.Data desc');//do maior pro menor
                         //showmessage(PqueryEntrada.sql.text);
                         //PqueryEntrada.sql.savetofile('c:\teste.sql');
                         PqueryEntrada.Open;
                         PqueryEntrada.first;
                         if (PqueryEntrada.Recordcount=0)//nao tem entrada entao uso o preco de custo
                         Then Begin
                                   try
                                      Pcusto:=Strtofloat(ObjPersiana_PP.persianagrupodiametrocor.Get_PRECOCUSTO)*Fieldbyname('soma').asinteger;
                                   Except
                                      Pcusto:=0;
                                   End;
                                   FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=ObjPersiana_PP.persianagrupodiametrocor.Persiana.Get_Codigo;
                                   FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='PERSIANA';

                                   FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=ObjPersiana_PP.persianagrupodiametrocor.GrupoPersiana.PlanoDeContas.Get_CODIGO;
                                   FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                   FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                         End
                         Else Begin//teve compra
                                   //Showmessage('ultima compra '+pqueryentrada.fieldbyname('data').asstring);
                                   if (FormatDateTime('mm/yyyy',pqueryentrada.fieldbyname('data').asdatetime)=FormatDateTime('mm/yyyy',strtodate(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data)))
                                   Then begin//se for o mesmo mes, tiro a media dentro do mes, do dia 1 ate o dia da venda
                                             Pdata:=strtodate(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);

                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select avg(TabPersiana_EP.valor) as MEDIA From tabPersiana_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabPersiana_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                             PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
                                             PqueryEntrada.sql.add('and TabPersiana_ep.persianagrupodiametrocor='+Fieldbyname('persianagrupodiametrocor').asstring);
                                             //showmessage(PqueryEntrada.sql.Text);
                                             PqueryEntrada.Open;
                                             Pcusto:=PqueryEntrada.fieldbyname('media').asfloat*Fieldbyname('soma').asinteger;
                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=ObjPersiana_PP.persianagrupodiametrocor.Persiana.Get_Codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='PERSIANA';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=ObjPersiana_PP.persianagrupodiametrocor.GrupoPersiana.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;

                                   End
                                   Else Begin
                                             //nao foi dentro do mesmo mes mas teve compra, entao tiro a media do mes da ultima compra
                                             Pdata:=PqueryEntrada.Fieldbyname('data').asdatetime;
                                             PqueryEntrada.Close;
                                             PqueryEntrada.sql.clear;
                                             PqueryEntrada.sql.add('Select avg(TabPersiana_EP.valor) as MEDIA From tabPersiana_EP');
                                             PqueryEntrada.sql.add('join TabEntradaProdutos on tabPersiana_ep.Entrada=TabentradaProdutos.codigo');
                                             PqueryEntrada.sql.add('where TabEnTradaProdutos.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate('01/'+FormatDateTime('mm/yyyy',pdata)))+#39);
                                             PqueryEntrada.sql.add('and   TabEnTradaProdutos.Data<='+#39+formatdatetime('mm/dd/yyyy',EndOfTheMonth(pdata))+#39);
                                             PqueryEntrada.sql.add('and TabPersiana_ep.persianagrupodiametrocor='+Fieldbyname('persianagrupodiametrocor').asstring);
                                             //showmessage(PqueryEntrada.sql.Text);
                                             PqueryEntrada.sql.savetofile('c:\teste.sql');
                                             PqueryEntrada.Open;
                                             Pcusto:=PqueryEntrada.fieldbyname('media').asfloat*Fieldbyname('soma').asinteger;
                                             FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=ObjPersiana_PP.persianagrupodiametrocor.Persiana.Get_Codigo;
                                             FmostraStringGrid.StringGrid.Cells[3,FmostraStringGrid.StringGrid.RowCount-1]:='PERSIANA';
                                             FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=ObjPersiana_PP.persianagrupodiametrocor.GrupoPersiana.PlanoDeContas.Get_CODIGO;
                                             FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.RowCount-1]:=Floattostr(pcusto);
                                             FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                                   end;
                         End;

                         next;
                   End;//while dos Persianas
         End;
         //*********************************************************************

         FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount-1;
         close;
         sql.clear;
         sql.add('SELECT GEN_ID(GenLoteExporta,1) CODIGO FROM RDB$DATABASE');
         open;
         Plote:=fieldbyname('codigo').asstring;

         //Conferindo se todos os produtos tem codigo no plano de contas
         for cont:=1 to FmostraStringGrid.StringGrid.Rowcount-1 do
         begin
              if (FmostraStringGrid.StringGrid.Cells[1,cont]='')
              Then begin
                        MensagemErro('O(a) '+FmostraStringGrid.StringGrid.cells[3,cont]+' c�digo '+FmostraStringGrid.StringGrid.cells[0,cont]+' n�o possui c�digo de plano de contas');
                        exit;
              End;
              close;
              sql.clear;
              sql.add('Insert into TabTempContabil (codigo,CodigoPlanodecontas,Soma,loteexporta)');
              sql.add('values (0,'+FmostraStringGrid.StringGrid.Cells[1,cont]);
              sql.add(','+virgulaparaponto(FmostraStringGrid.StringGrid.Cells[2,cont]));
              sql.add(','+Plote+')');
              execsql;
         End;

         //Agrupando por codigo do plano de contas
         close;
         sql.clear;
         sql.add('Select codigoplanodecontas,sum(soma) as soma');
         sql.add('from TabTempContabil');
         sql.add('where LoteExporta='+Plote);
         sql.add('group by Codigoplanodecontas');
         open;

         while not(eof) do
         Begin
              ObjExportaContabilidade.ZerarTabela;
              ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
              ObjExportaContabilidade.Submit_Data(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);
              ObjExportaContabilidade.Submit_ContaDebite(PcontaCmv);
              ObjExportaContabilidade.Submit_ContaCredite(Fieldbyname('codigoplanodecontas').asstring);
              ObjExportaContabilidade.Submit_Valor(fieldbyname('soma').asstring);
              ObjExportaContabilidade.Submit_Historico('Venda '+pcodigo+' '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
              ObjExportaContabilidade.Submit_ObjGerador('OBJPEDIDO');
              ObjExportaContabilidade.Submit_CodGerador(pcodigo);
              ObjExportaContabilidade.Submit_Exportado('N');
              ObjExportaContabilidade.Submit_Exporta('S');
              ObjExportaContabilidade.Submit_NomedoArquivo('');
              if (ObjExportaContabilidade.Salvar(False,False)=False)
              Then Begin
                        MensagemErro('Erro na tentativa de gravar a contabilidade do estoque');
                        exit;
              End;

              next;
         End;

         close;
         sql.clear;
         sql.add('Delete from TabTempContabil');
         sql.add('where LoteExporta='+Plote);
         execsql;
         result:=True;
    End;//With

Finally
      Freeandnil(PqueryEntrada);
      Freeandnil(PPlanodeContasFinal);
      Freeandnil(PvalorFinal);
end;

End;
}

Function TObjPedidoObjetos.ExportaContabilidade_financeiro_Venda(Pcodigo: string): Boolean;
var
  PvalorFinal,PvalorbonificacaoUsada,PValortotal,Pdesconto,PAcrescimo:Currency;

  PcontaSimplesFaturamento,PcontaCliente,PcontaDesconto,PContaAcrescimo:string;
  PContaAdiantamentoCliente:string;
begin
    Result:=False;

{
--------------------------------------------------------------------------------
Contabilidade da Conclus�o venda


Antes a contabilidade da venda era gerada no t�tulo financeiro,
por�m se houve desconto ou acr�scimo a contabilidade n�o � real
pois ela est� apenas considerando duas contas o
Cliente e a Conta gerencial de Venda (que � acumulativa).

O correto � trabalhar com o valor total e lan�ar descontos e acr�scimos

Exemplo

Pedido

Valor Total = 1.000,00
Desconto    =    60,00
Acr�scimo   =    10,00
-------------------------
Valor Final =   950,00


Antes o t�tulo gerava

Debito		Credito				(valor final)
Cliente		CG de venda (acumulativa)

No momento do pagamento				(valor final)

Debito		Credito
Caixa		Cliente

O Correto

Pedido

Valor Total = 1.000,00
Desconto    =    60,00
Acr�scimo   =    10,00
-------------------------
Valor Final =   950,00

Debito			    Credito
Cliente (950)		Venda (1000,00)

Debito			      Credito
Desconto (60,00)	Juros (10,00)

Usando Partida Simples

Debito 		        Credito
			            Venda (1000,00)
Cliente (950)
Desconto (60,00)
            			Juros (10,00)

**MODIFICACAO SOLICITADA 30/03*****

Conforme solicitado dia 30/03 pela Sra. Camila,
os descontos e juros n�o ser�o mais lan�ados na contabilidade
devido ao fato da entrega do produto ser em mes diferente da venda
sendo assim, no balanco mensalmente existe um "prejuizo"
pois como o produto n�o foi entregue as contas
de desconto s�o alimentadas mas as de venda n�o s�o.

Sera mudado a conta de venda para  - Simples Faturamento

E na entrega sera

Debitar                 Creditar

Simples Faturamento     Venda (DRE)

**OBSERVACAO**

Em caso de venda onde o cliente usou um desconto de bonificacao os lancamentos
passam a ser partida simples

D Venda  (Valor final do titulo)
D Adiantamento do Cliente (valor da bonificacao usada)
C Simples Faturamento (produtos-desconto)

exemplo com valores

Vt 60,00 D 10,00 B 20,00 Vf  30,00

D Duplicatas a Receber       30,00
D Adiantamentos de Clientes  20,00
        C Simples Faturamento 50,00

Quita��o do T�tulo

D Caixa                  30,00
  C Duplicatas a Receber 30,00


**************Na entrega******************

4 Projetos

100
200
300
400

Desconto 200,00=  VALOR FINAL=800,00

Na Entrega (CONCLUIR UM ROMANEIO)

Ao concluir selecionar todos os pedidos projetos
calcular abaixo e lancar.  (CONTA VENDA E RECEITA P. SEGUINTES...)


100 EQUIVALE 10% DO VALOR TOTAL
ENTAO
10% 800 = 80,00  fazer o lancamento com 80,00

200 equivele 20% do VALOR TOTAL
entao
20% 800=160,00 fazer o lancamento com 160
...
--------------------------------------------------------------------------------
}
    if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(pcodigo)=False)
    then Begin
              MensagemErro('Pedido n�o encontrado');
              exit;
    End;
    Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

    Pcontadesconto:='';
    PcontaAcrescimo:='';
    PcontaCliente:='';
    PcontaSimplesFaturamento:='';
    PContaAdiantamentoCliente:='';


    PcontaCliente:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.CodigoPlanoContas.Get_Codigo;

    (*if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL DESCONTO NA VENDA DE PRODUTOS')=True)
    Then PcontaDesconto:=ObjParametroGlobal.Get_Valor;

    if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL ACR�SCIMO NA VENDA DE PRODUTOS')=True)
    Then PContaAcrescimo:=ObjParametroGlobal.Get_Valor;
    *)


    if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL SIMPLES FATURAMENTO USADA NA VENDA DE PRODUTOS')=True)
    Then PcontaSimplesFaturamento:=ObjParametroGlobal.Get_Valor;

    if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL ADIANTAMENTO DE CLIENTES USADA NA TROCA DE PRODUTOS')=True)
    Then PContaAdiantamentoCliente:=ObjParametroGlobal.Get_Valor;



Try

    Try
       PValortotal:=Strtofloat(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorTotal);
    Except
       Pvalortotal:=0;
    End;

    try
       Pdesconto:=Strtofloat(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorDesconto);
    Except
       Pdesconto:=0;
    End;

    try
       PAcrescimo:=Strtofloat(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorAcrescimo);
    Except
       PAcrescimo:=0;
    End;

    try
       PvalorFinal:=Strtofloat(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorFinal);
    Except
       PValorFinal:=0;
    End;

    Try
        PvalorbonificacaoUsada:=StrTofloat(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorBonificacaoCliente);
    Except
        PvalorbonificacaoUsada:=0;
    End;

    //Conforme solicitado dia 30/03 pela Sra. Camila,
    //os descontos e juros n�o ser�o mais lan�ados na contabilidade
    //devido ao fato da entrega do produto ser em mes diferente da venda
    //sendo assim, no balanco mensalmente existe um "prejuizo"
    //pois como o produto n�o foi entregue as contas
    //de desconto s�o alimentadas mas as de venda n�o s�o.

    {if ((Pdesconto=0) and (PAcrescimo=0) and (PValorTotal=Pvalorfinal))
    Then begin//como nao tem desconto nem acrescimo lanco partido dobrada
              ObjExportaContabilidade.ZerarTabela;
              ObjExportaContabilidade.status:=dsinsert;
              ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
              ObjExportaContabilidade.Submit_Data(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);
              ObjExportaContabilidade.Submit_ContaDebite(PcontaCliente);
              ObjExportaContabilidade.Submit_ContaCredite(PcontaSimplesFaturamento);
              ObjExportaContabilidade.Submit_Valor(FloatToStr(PvalorFinal));
              ObjExportaContabilidade.Submit_Historico('Venda '+pcodigo+' '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
              ObjExportaContabilidade.Submit_ObjGerador('OBJPEDIDO');
              ObjExportaContabilidade.Submit_CodGerador(pcodigo);
              ObjExportaContabilidade.Submit_Exportado('N');
              ObjExportaContabilidade.Submit_Exporta('S');
              ObjExportaContabilidade.Submit_NomedoArquivo('');
              if (ObjExportaContabilidade.Salvar(False,False)=False)
              Then Begin
                        MensagemErro('Erro na tentativa de gravar a contabilidade financeiro do pedido');
                        exit;
              End;
    End
    Else Begin//tendo desconto ou juro preciso lancar partida simples
              ObjExportaContabilidade.ZerarTabela;
              ObjExportaContabilidade.status:=dsinsert;

              ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
              ObjExportaContabilidade.Submit_Data(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);
              ObjExportaContabilidade.Submit_ContaDebite('');
              ObjExportaContabilidade.Submit_ContaCredite(PcontaSimplesFaturamento);
              ObjExportaContabilidade.Submit_Valor(FloatToStr(PValortotal));
              ObjExportaContabilidade.Submit_Historico('Venda '+pcodigo+' '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
              ObjExportaContabilidade.Submit_ObjGerador('OBJPEDIDO');
              ObjExportaContabilidade.Submit_CodGerador(pcodigo);
              ObjExportaContabilidade.Submit_Exportado('N');
              ObjExportaContabilidade.Submit_Exporta('S');
              ObjExportaContabilidade.Submit_NomedoArquivo('');
              if (ObjExportaContabilidade.Salvar(False,True)=False)
              Then Begin
                        MensagemErro('Erro na tentativa de gravar a contabilidade financeiro do pedido');
                        exit;
              End;

              ObjExportaContabilidade.ZerarTabela;
              ObjExportaContabilidade.status:=dsinsert;

              ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
              ObjExportaContabilidade.Submit_Data(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);
              ObjExportaContabilidade.Submit_ContaDebite(PcontaCliente);
              ObjExportaContabilidade.Submit_ContaCredite('');
              ObjExportaContabilidade.Submit_Valor(FloatToStr(PValorFinal));
              ObjExportaContabilidade.Submit_Historico('Venda '+pcodigo+' '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
              ObjExportaContabilidade.Submit_ObjGerador('OBJPEDIDO');
              ObjExportaContabilidade.Submit_CodGerador(pcodigo);
              ObjExportaContabilidade.Submit_Exportado('N');
              ObjExportaContabilidade.Submit_Exporta('S');
              ObjExportaContabilidade.Submit_NomedoArquivo('');
              if (ObjExportaContabilidade.Salvar(False,True)=False)
              Then Begin
                        MensagemErro('Erro na tentativa de gravar a contabilidade financeiro do pedido');
                        exit;
              End;

              if (Pdesconto>0)
              Then begin
                        ObjExportaContabilidade.ZerarTabela;
                        ObjExportaContabilidade.status:=dsinsert;
                        //*******************************************************
                        ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                        ObjExportaContabilidade.Submit_Data(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);
                        ObjExportaContabilidade.Submit_ContaDebite(PcontaDesconto);
                        ObjExportaContabilidade.Submit_ContaCredite('');
                        ObjExportaContabilidade.Submit_Valor(FloatToStr(Pdesconto));
                        ObjExportaContabilidade.Submit_Historico('Desconto - Venda '+pcodigo+' '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
                        ObjExportaContabilidade.Submit_ObjGerador('OBJPEDIDO');
                        ObjExportaContabilidade.Submit_CodGerador(pcodigo);
                        ObjExportaContabilidade.Submit_Exportado('N');
                        ObjExportaContabilidade.Submit_Exporta('S');
                        ObjExportaContabilidade.Submit_NomedoArquivo('');
                        if (ObjExportaContabilidade.Salvar(False,True)=False)
                        Then Begin
                                  MensagemErro('Erro na tentativa de gravar a contabilidade financeiro do pedido');
                                  exit;
                        End;

              End;

              if (PAcrescimo>0)
              Then begin
                        ObjExportaContabilidade.ZerarTabela;
                        ObjExportaContabilidade.status:=dsinsert;
                        //*****************************************************
                        ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                        ObjExportaContabilidade.Submit_Data(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);
                        ObjExportaContabilidade.Submit_ContaDebite('');
                        ObjExportaContabilidade.Submit_ContaCredite(PContaAcrescimo);
                        ObjExportaContabilidade.Submit_Valor(FloatToStr(PAcrescimo));
                        ObjExportaContabilidade.Submit_Historico('Acr�scimo - Venda '+pcodigo+' '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
                        ObjExportaContabilidade.Submit_ObjGerador('OBJPEDIDO');
                        ObjExportaContabilidade.Submit_CodGerador(pcodigo);
                        ObjExportaContabilidade.Submit_Exportado('N');
                        ObjExportaContabilidade.Submit_Exporta('S');
                        ObjExportaContabilidade.Submit_NomedoArquivo('');
                        if (ObjExportaContabilidade.Salvar(False,True)=False)
                        Then Begin
                                  MensagemErro('Erro na tentativa de gravar a contabilidade financeiro do pedido');
                                  exit;
                        End;

              End;
    End;}

    if (PvalorbonificacaoUsada=0)//nao teve bonificacao usada
    then Begin
              //Nesse caso uso partida dobrada Duplicatas a receber e Simples Faturamento
              ObjExportaContabilidade.ZerarTabela;
              ObjExportaContabilidade.status:=dsinsert;
              ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
              ObjExportaContabilidade.Submit_Data(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);
              ObjExportaContabilidade.Submit_ContaDebite(PcontaCliente);
              ObjExportaContabilidade.Submit_ContaCredite(PcontaSimplesFaturamento);
              ObjExportaContabilidade.Submit_Valor(FloatToStr(PvalorFinal));
              ObjExportaContabilidade.Submit_Historico('Venda '+pcodigo+' '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
              ObjExportaContabilidade.Submit_ObjGerador('OBJPEDIDO');
              ObjExportaContabilidade.Submit_CodGerador(pcodigo);
              ObjExportaContabilidade.Submit_Exportado('N');
              ObjExportaContabilidade.Submit_Exporta('S');
              ObjExportaContabilidade.Submit_NomedoArquivo('');
              if (ObjExportaContabilidade.Salvar(False,False)=False)
              Then Begin
                        MensagemErro('Erro na tentativa de gravar a contabilidade financeiro do pedido');
                        exit;
              End;
    End
    Else Begin//com bonificacao entao partida simples
              //duplicatas a receber
              //adiantamento a clientes
              //simples faturamento
{
Em caso de venda onde o cliente usou um desconto de bonificacao os lancamentos
passam a ser partida simples

D Venda  (Valor final do titulo)
D Adiantamento do Cliente (valor da bonificacao usada)
C Simples Faturamento (produtos-desconto)

exemplo com valores

Vt 60,00 D 10,00 B 20,00 Vf  30,00

D Duplicatas a Receber       30,00
D Adiantamentos de Clientes  20,00
        C Simples Faturamento 50,00

Quita��o do T�tulo

D Caixa                  30,00
  C Duplicatas a Receber 30,00}

              //Debitando Duplicatas a receber no valor do titulo
              ObjExportaContabilidade.ZerarTabela;
              ObjExportaContabilidade.status:=dsinsert;
              ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
              ObjExportaContabilidade.Submit_Data(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);
              ObjExportaContabilidade.Submit_ContaDebite(PcontaCliente);
              ObjExportaContabilidade.Submit_ContaCredite('');
              ObjExportaContabilidade.Submit_Valor(FloatToStr(PvalorFinal));
              ObjExportaContabilidade.Submit_Historico('Venda '+pcodigo+' '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
              ObjExportaContabilidade.Submit_ObjGerador('OBJPEDIDO');
              ObjExportaContabilidade.Submit_CodGerador(pcodigo);
              ObjExportaContabilidade.Submit_Exportado('N');
              ObjExportaContabilidade.Submit_Exporta('S');
              ObjExportaContabilidade.Submit_NomedoArquivo('');
              if (ObjExportaContabilidade.Salvar(False,True)=False)
              Then Begin
                        MensagemErro('Erro na tentativa de gravar a contabilidade financeiro do pedido');
                        exit;
              End;

              //Debitando adiantamento a clientes (bonificacao usada)
              ObjExportaContabilidade.ZerarTabela;
              ObjExportaContabilidade.status:=dsinsert;
              ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
              ObjExportaContabilidade.Submit_Data(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);
              ObjExportaContabilidade.Submit_ContaDebite(PContaAdiantamentoCliente);
              ObjExportaContabilidade.Submit_ContaCredite('');
              ObjExportaContabilidade.Submit_Valor(FloatToStr(PvalorbonificacaoUsada));
              ObjExportaContabilidade.Submit_Historico('Venda '+pcodigo+' '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
              ObjExportaContabilidade.Submit_ObjGerador('OBJPEDIDO');
              ObjExportaContabilidade.Submit_CodGerador(pcodigo);
              ObjExportaContabilidade.Submit_Exportado('N');
              ObjExportaContabilidade.Submit_Exporta('S');
              ObjExportaContabilidade.Submit_NomedoArquivo('');
              if (ObjExportaContabilidade.Salvar(False,True)=False)
              Then Begin
                        MensagemErro('Erro na tentativa de gravar a contabilidade financeiro do pedido');
                        exit;
              End;

              //Creditando Simples faturamento no valor dos produtos - desconto
              ObjExportaContabilidade.ZerarTabela;
              ObjExportaContabilidade.status:=dsinsert;
              ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
              ObjExportaContabilidade.Submit_Data(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);
              ObjExportaContabilidade.Submit_ContaDebite('');
              ObjExportaContabilidade.Submit_ContaCredite(PcontaSimplesFaturamento);
              ObjExportaContabilidade.Submit_Valor(FloatToStr(PValortotal-Pdesconto));
              ObjExportaContabilidade.Submit_Historico('Venda '+pcodigo+' '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
              ObjExportaContabilidade.Submit_ObjGerador('OBJPEDIDO');
              ObjExportaContabilidade.Submit_CodGerador(pcodigo);
              ObjExportaContabilidade.Submit_Exportado('N');
              ObjExportaContabilidade.Submit_Exporta('S');
              ObjExportaContabilidade.Submit_NomedoArquivo('');
              if (ObjExportaContabilidade.Salvar(False,True)=False)
              Then Begin
                        MensagemErro('Erro na tentativa de gravar a contabilidade financeiro do pedido');
                        exit;
              End;
    End;

    result:=true;
Finally
End;

end;




Function TObjPedidoObjetos.ExportaContabilidade_TROCA(PPedido: string;PvalorDevolvido:Currency): Boolean;
var
  PcontaSimplesFaturamento,PContaAdiantamentoCliente:string;
begin
    Result:=False;

    {Na venda foi feito os seguintes lancamentos

    D Duplicatas a receber   (cliente)
    C Simples Faturamento

    se foi devolvido entao tenho que lancar

    D Simples Faturamento
    C Adiantamento de Clientes (BONIFICACAO GERADA)

    Quando fizer outra venda que usar essa bonificacao como desconto
    faco uma partida simples usando duplicata,simples faturamento e adiantamento a clientes
    exemplo:

    Vt 60,00 D 10,00 B 20,00 Vf  30,00
    D Duplicatas a Receber       30,00
    D Adiantamentos de Clientes  20,00
    C Simples Faturamento        50,00}

    if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(ppedido)=False)
    then Begin
              MensagemErro('Pedido n�o encontrado');
              exit;
    End;
    Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

    PcontaSimplesFaturamento:='';
    PContaAdiantamentoCliente:='';

    if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL SIMPLES FATURAMENTO USADA NA VENDA DE PRODUTOS')=True)
    Then PcontaSimplesFaturamento:=ObjParametroGlobal.Get_Valor;

    if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL ADIANTAMENTO DE CLIENTES USADA NA TROCA DE PRODUTOS')=True)
    Then PContaAdiantamentoCliente:=ObjParametroGlobal.Get_Valor;


Try

    ObjExportaContabilidade.ZerarTabela;
    ObjExportaContabilidade.status:=dsinsert;
    ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);


    //ObjExportaContabilidade.Submit_Data(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);
    //Camila no dia 17/09 solicitou que a data nao fosse a do pedido e sim a data do dia da troca
    ObjExportaContabilidade.Submit_Data(datetostr(retornadataservidor));


    ObjExportaContabilidade.Submit_ContaDebite(PcontaSimplesFaturamento);
    ObjExportaContabilidade.Submit_ContaCredite(PContaAdiantamentoCliente);
    ObjExportaContabilidade.Submit_Valor(FloatToStr(PvalorDevolvido));
    ObjExportaContabilidade.Submit_Historico('Venda '+Ppedido+' '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
    ObjExportaContabilidade.Submit_ObjGerador('OBJPEDIDO');
    ObjExportaContabilidade.Submit_CodGerador(ppedido);
    ObjExportaContabilidade.Submit_Exportado('N');
    ObjExportaContabilidade.Submit_Exporta('S');
    ObjExportaContabilidade.Submit_NomedoArquivo('');
    if (ObjExportaContabilidade.Salvar(False,False)=False)
    Then Begin
              MensagemErro('Erro na tentativa de gravar a contabilidade financeiro do pedido');
              exit;
    End;
    result:=true;
Finally
End;

end;



function TObjPedidoObjetos.ExportaContabilidade_financeiro_Romaneio(PpedidoProjeto,Promaneio: string;PdataEntregaRomaneio:string): Boolean;
var
  PvalorSimplesremessa,PporcentagemDescontoAplicadoPedido,PvalorPedidoProjeto,Pvalortitulo,PDesconto,PDescontoBonificacao:Currency;
  PcontaSimplesRemessa,Pcontavenda:string;
begin
    Result:=False;

{
27/01/07

E na entrega sera

Debito                Creditar

Simples Faturamento   Venda


Na entrega

4 Projetos

100
200
300
400

Desconto 200,00=  VALOR FINAL=800,00

Na Entrega (CONCLUIR UM ROMANEIO)

Ao concluir selecionar todos os pedidos projetos
calcular abaixo e lancar.  (CONTA VENDA E RECEITA P. SEGUINTES...)

Calcular % foi dado de desconto no pedido
usar esse pedido nos pedidos projetos a serem entregues

--------------------------------------------------------------------------------
}

    if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
    then Begin
              MensagemErro('Pedido Projeto '+ppedidoprojeto+' n�o encontrado');
              exit;
    End;
    Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

    Pcontavenda:='';
    if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL VENDA DE PRODUTOS')=True)
    Then PContaVenda:=ObjParametroGlobal.Get_Valor;

    PcontaSimplesRemessa:='';
    if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL SIMPLES FATURAMENTO USADA NA VENDA DE PRODUTOS')=True)
    Then PcontaSimplesRemessa:=ObjParametroGlobal.Get_Valor;


Try

    Try
       PValorTitulo:=Strtofloat(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_valortitulo);
    Except
       Pvalortitulo:=0;
    End;

    Try
       PDesconto:=Strtofloat(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorDesconto);
    Except
       PDesconto:=0;
    End;

    Try
       PValorPedidoProjeto:=Strtofloat(Self.ObjMedidas_Proj.PedidoProjeto.Get_ValorFinal);
    Except
       PvalorPedidoProjeto:=0;
    End;

    Try
        PDescontoBonificacao:=Strtofloat(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorBonificacaoCliente);
    Except
        PDescontoBonificacao:=0;
    End;


    Try
       //ValorProdutos    100%
       //Desconto          X
       //X=(Desconto*100)/valorprodutos;

       //Valor dos Prodtos = Valor Final+Desconto+bonificacao pois pode ter sido usado troca por isso nao trabalho com valortotal

       PporcentagemDescontoAplicadoPedido:=0;

       if (PDesconto>0)
       Then PporcentagemDescontoAplicadoPedido:=(  (pdesconto*100)/(Pvalortitulo+PDesconto+PDescontoBonificacao)  );
     Except
           PporcentagemDescontoAplicadoPedido:=0;
     End;

     PvalorSimplesremessa:=PvalorPedidoProjeto-((PvalorPedidoProjeto*PporcentagemDescontoAplicadoPedido)/100);
     PvalorSimplesremessa:=strtofloat(tira_ponto(formata_valor(pvalorsimplesremessa)));



    ObjExportaContabilidade.ZerarTabela;
    ObjExportaContabilidade.Status:=dsinsert;
    ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
    ObjExportaContabilidade.Submit_Data(PdataEntregaRomaneio);
    ObjExportaContabilidade.Submit_ContaDebite(PcontaSimplesRemessa);
    ObjExportaContabilidade.Submit_ContaCredite(Pcontavenda);
    ObjExportaContabilidade.Submit_Valor(floattostr(PvalorSimplesremessa));
    ObjExportaContabilidade.Submit_Historico('Entrega da Venda '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Codigo+' Ped.Proj. '+PpedidoProjeto+' - '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
    ObjExportaContabilidade.Submit_ObjGerador('OBJROMANEIO');
    ObjExportaContabilidade.Submit_CodGerador(promaneio);
    ObjExportaContabilidade.Submit_Exportado('N');
    ObjExportaContabilidade.Submit_Exporta('S');
    ObjExportaContabilidade.Submit_NomedoArquivo('');
    if (ObjExportaContabilidade.Salvar(False,False)=False)
    Then Begin
              MensagemErro('Erro na tentativa de gravar a contabilidade financeiro do pedido');
              exit;
    End;

    result:=true;
Finally

End;

end;


procedure TObjPedidoObjetos.RefazContabilidade(PdataInicial,PdataFinal: string);
var
Objqlocal:Tibquery;
begin
     //primeiro seleciono os pedidos desse periodo
     //depois excluo a contabilidade do titulo a receber
     //logo em seguida contabilidade do proprio pedido

     Try
        StrToDate(PdataInicial);
        StrToDate(PdataFinal);

     Except
           MensagemErro('Data Inv�lida');
           exit;
     End;

     Try
        Objqlocal:=Tibquery.create(nil);
        Objqlocal.Database:=FDataModulo.IBDatabase;
     Except
           MensagemErro('Erro na tentativa de Criar a ObjQlocal');
           exit;
     End;
Try
     With ObjQlocal do
     Begin
          close;
          SQL.clear;
          SQL.add('Select * from TabPedido where Data>='+#39+formatdatetime('mm/dd/yyyy', strtodate(pdatainicial))+#39);
          sql.add('and Data<='+#39+formatdatetime('mm/dd/yyyy', strtodate(pdatafinal))+#39);
          sql.add('and titulo is not null');
          sql.add('order by data');
          open;
          last;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.Lbmensagem.Caption:='Apagando Contabilidade';
          first;
          While not(eof) do
          Begin
               FMostraBarraProgresso.IncrementaBarra1(1);
               FMostraBarraProgresso.Show;

               if (fieldbyname('Titulo').asstring<>'')
               Then Begin
                       If (Self.ObjExportaContabilidade.excluiporgerador('OBJTITULO',fieldbyname('Titulo').asstring)=False)
                       Then Begin
                                 mensagemerro('Erro na tentativa de apagar a contabilidade do t�tulo '+fieldbyname('Titulo').asstring);
                                 exit;
                       End;
               End;

               If (Self.ObjExportaContabilidade.excluiporgerador('OBJPEDIDO',fieldbyname('codigo').asstring)=False)
               Then Begin
                         mensagemerro('Erro na tentativa de apagar a contabilidade da entrada '+fieldbyname('codigo').asstring);
                         exit;
               End;

               if (Self.ExportaContabilidade_Financeiro_Venda(fieldbyname('codigo').asstring)=False)
               then begin
                         MensagemErro('Erro na tentativa de gerar a contabilidade do Financeiro do Pedido');
                         exit;
               End;

               next;
          End;
          MensagemAviso('Conclu�do');
          FDataModulo.IBTransaction.CommitRetaining;


     End;

Finally
       FDataModulo.IBTransaction.RollbackRetaining;
       FMostraBarraProgresso.Close;
       freeandnil(Objqlocal);
End;
end;



procedure TObjPedidoObjetos.ApagaContabilidadeNaoConcluidos(PdataInicial,PdataFinal: string);
var
Objqlocal:Tibquery;
begin
     //primeiro seleciono os pedidos desse periodo
     //depois excluo a contabilidade do titulo a receber
     //logo em seguida contabilidade do proprio pedido

     Try
        StrToDate(PdataInicial);
        StrToDate(PdataFinal);

     Except
           MensagemErro('Data Inv�lida');
           exit;
     End;

     Try
        Objqlocal:=Tibquery.create(nil);
        Objqlocal.Database:=FDataModulo.IBDatabase;
     Except
           MensagemErro('Erro na tentativa de Criar a ObjQlocal');
           exit;
     End;
Try
     With ObjQlocal do
     Begin
          close;
          SQL.clear;
          SQL.add('Select * from TabPedido where Data>='+#39+formatdatetime('mm/dd/yyyy', strtodate(pdatainicial))+#39);
          sql.add('and Data<='+#39+formatdatetime('mm/dd/yyyy', strtodate(pdatafinal))+#39);
          sql.add('and titulo is null');
          sql.add('order by data');
          open;
          last;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.Lbmensagem.Caption:='Apagando Contabilidade';
          first;
          While not(eof) do
          Begin
               FMostraBarraProgresso.IncrementaBarra1(1);
               FMostraBarraProgresso.Show;

               if (fieldbyname('Titulo').asstring<>'')
               Then Begin
                       If (Self.ObjExportaContabilidade.excluiporgerador('OBJTITULO',fieldbyname('Titulo').asstring)=False)
                       Then Begin
                                 mensagemerro('Erro na tentativa de apagar a contabilidade do t�tulo '+fieldbyname('Titulo').asstring);
                                 exit;
                       End;
               End;

               If (Self.ObjExportaContabilidade.excluiporgerador('OBJPEDIDO',fieldbyname('codigo').asstring)=False)
               Then Begin
                         mensagemerro('Erro na tentativa de apagar a contabilidade da entrada '+fieldbyname('codigo').asstring);
                         exit;
               End;
               next;
          End;
          MensagemAviso('Conclu�do');
          FDataModulo.IBTransaction.CommitRetaining;


     End;

Finally
       FDataModulo.IBTransaction.RollbackRetaining;
       FMostraBarraProgresso.Close;
       freeandnil(Objqlocal);
End;
end;


procedure TObjPedidoObjetos.ImprimeComissaoArquiteto;
var
Pdatainicial,pdatafinal:Tdate;
Parquiteto:string;
Psomaarquiteto,PsomaTotal:Currency;
begin
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;

          LbGrupo01.Caption:='Data Inicial';
          LbGrupo02.Caption:='Data Final';
          LbGrupo03.Caption:='Arquiteto';

          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;

          edtgrupo03.Color:=$005CADFE;
          edtgrupo03.OnKeyDown:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.EdtarquitetoKeyDown;

          Showmodal;

          if (tag=0)
          then exit;


          if (Validadata(1,pdatainicial,false)=False)
          then exit;

          if (Validadata(2,pdatafinal,false)=False)
          then exit;

          parquiteto:='';
          if (edtgrupo03.text<>'')
          then Begin
                    Try
                       strtoint(edtgrupo03.text);
                       parquiteto:=edtgrupo03.text;

                       if (self.ObjMedidas_Proj.PedidoProjeto.Pedido.Arquiteto.LocalizaCodigo(parquiteto)=False)
                       then Begin
                                 MensagemAviso('Arquiteto n�o localizado');
                                 exit;
                       End;
                    except
                          mensagemerro('Arquiteto Inv�lido');
                          exit;
                    End;
          End;
     end;

     With Self.ObjqueryTEMP do
     Begin
          close;
          sql.clear;
          sql.add('Select tabpedido.arquiteto,');
          sql.add('tabarquiteto.nome as NOMEARQUITETO,');
          sql.add('tabpedido.codigo as PEDIDO,tabpedido.data,tabpedido.valorfinal,tabpedido.valorcomissaoarquiteto,');
          sql.add('tabpedido.cliente,tabcliente.nome as NOMECLIENTE');
          sql.add('from tabpedido');
          sql.add('join Tabarquiteto on Tabpedido.arquiteto=tabarquiteto.codigo');
          sql.add('join tabcliente on tabpedido.cliente=tabcliente.codigo');
          sql.add('where tabpedido.data>='+#39+formatdatetime('mm/dd/yyyy',Pdatainicial)+#39);
          sql.add('and tabpedido.data<='+#39+formatdatetime('mm/dd/yyyy',PdataFinal)+#39);
          sql.add('and tabpedido.Concluido=''S''  ');

          if (Parquiteto<>'')
          then sql.add('and tabpedido.arquiteto='+Parquiteto);

          sql.add('order by');
          sql.add('tabpedido.arquiteto,tabpedido.data');

          open;
          last;

          if (recordcount=0)
          then Begin
                    mensagemaviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;
          first;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               LinhaLocal:=3;
               RDprint.Abrir;
               if (RDprint.Setup=False)
               then begin
                         rdprint.Fechar;
                         exit;
               end;

               rdprint.ImpC(linhalocal,45,'COMISS�O POR ARQUITETO',[negrito]);
               incrementalinha(2);

               if (Parquiteto<>'')
               then Begin
                          RDprint.ImpF(linhalocal,1,'Arquiteto: '+parquiteto+'-'+Fieldbyname('nomearquiteto').asstring,[negrito]);
                          IncrementaLinha(1);
               End;

               RDprint.ImpF(linhalocal,1,'Per�odo : '+datetostr(Pdatainicial)+' a '+datetostr(pdatafinal),[negrito]);
               IncrementaLinha(2);

               //titulo das colunas
               RDprint.ImpF(linhalocal,1,CompletaPalavra('DATA',10,' ')+' '+
                                         CompletaPalavra('PEDIDO',6,' ')+' '+
                                         CompletaPalavra('CLIENTE',40,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR PEDIDO',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VLR COMISSAO',12,' '),[negrito]);
               IncrementaLinha(2);


               PsomaTotal:=0;

               Psomaarquiteto:=0;
               Parquiteto:=Fieldbyname('arquiteto').asstring;
               RDprint.ImpF(linhalocal,1,CompletaPalavra(fieldbyname('arquiteto').asstring+'-'+fieldbyname('nomearquiteto').asstring,90,' '),[negrito]);
               IncrementaLinha(2);


               while not(Self.ObjqueryTEMP.eof) do
               Begin

                    if (Parquiteto<>fieldbyname('arquiteto').asstring)
                    Then Begin
                              IncrementaLinha(1);
                              RDprint.ImpF(linhalocal,1,'SOMA DO ARQUITETO: '+Formata_valor(Psomaarquiteto),[negrito]);
                              IncrementaLinha(2);

                              Psomaarquiteto:=0;
                              Parquiteto:=Fieldbyname('arquiteto').asstring;
                              VerificaLinha;
                              RDprint.ImpF(linhalocal,1,CompletaPalavra(fieldbyname('arquiteto').asstring+'-'+fieldbyname('nomearquiteto').asstring,90,' '),[negrito]);
                              IncrementaLinha(2);
                    End;
                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('data').asstring,10,' ')+' '+
                                              CompletaPalavra(fieldbyname('pedido').asstring,6,' ')+' '+
                                              CompletaPalavra(fieldbyname('cliente').asstring+'-'+fieldbyname('nomecliente').asstring,40,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorfinal').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorcomissaoarquiteto').asstring),12,' '));
                    IncrementaLinha(1);
                    Psomaarquiteto:=Psomaarquiteto+fieldbyname('valorcomissaoarquiteto').asfloat;
                    PsomaTotal:=PsomaTotal+fieldbyname('valorcomissaoarquiteto').asfloat;
                    Self.ObjqueryTEMP.next;
               end;

               IncrementaLinha(1);
               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'SOMA DO ARQUITETO: '+Formata_valor(Psomaarquiteto),[negrito]);
               IncrementaLinha(2);

               DesenhaLinha;

               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'SOMA FINAL       : '+Formata_valor(PsomaTOTAL),[negrito]);
               IncrementaLinha(2);



               rdprint.Fechar;
          End;
     End;


end;

procedure TObjPedidoObjetos.Relatorio_Movimentacao_Diaria;
var
  pData:Tdate;
  //StrPortadores:TStringList;
  cont:integer;
  psomavalortotal,psomavalorfinal,psomadesconto,pporcentodesconto:currency;
  psomaRecebimento,PsomaPagamento,Psomaentrada_portador,Psomasaida_Portador,PsomaEntrada_total,PsomaSaida_total:Currency;
  //Pportador:string;
  PDC:string;
  ppedido,pnomevendedor,ppendencia,PnomeCredorDevedor:String;
  Pimprime:boolean;
begin
{
Relat�rio Movimenta��o Di�ria


Vendas do Dia 00/00/0000
Filtrar todas a vendas fechadas no dia (Totalizando no Final de cada coluna)

N� Proposta    Nome Cliente  C�d. Vendedor   V.Total Bruto    V.Total L�quido   % Desconto




Recebimentos do Dia 00/00/0000
Filtrar todos os recebimentos do dia

N� Proposta    Nome Cliente  C�d. Vendedor   V.Total     V.Pago



Pagamentos do dia 00/00/0000
Filtrar todos os pagamentos do dia

N� do T�tulo   Nome Fornecedor  Valor Pago     Conta Gerencial
}

    {try
        StrPortadores:=TStringList.Create;
    except
        mensagemerro('Erro na tentativa de cria��o da StringList StrPortadores');
        exit;
    End;}

Try
    With FfiltroImp do
    Begin
         DesativaGrupos;
         Grupo01.Enabled:=True;
         LbGrupo01.caption:='Data';
         edtgrupo01.EditMask:=MascaraData;
         edtgrupo01.text:=Datetostr(now);


         {Grupo02.Enabled:=True;
         LbGrupo02.caption:='Portador(es)';
         edtgrupo02.EditMask:='';
         edtgrupo02.OnKeyDown:=ObjLanctoPortadorGlobal.Lancamento.Pendencia.Titulo.PORTADOR.edtportadorKeyDown_PV;}


         showmodal;

         if (tag=0)
         Then exit;

         if (ValidaData(1,pdata,false)=False)
         Then exit;

         {if (edtgrupo02.text='')
         Then Begin
                   MensagemAviso('Escolha o(s) portador(es) que deseja imprimir o demonstrativo');
                   exit;
         End;

         StrPortadores.Clear;
         if (ExplodeStr(edtgrupo02.text,StrPortadores,';','integer')=False)
         then Begin
                   MensagemErro('Erro no(s) portador(es) digitado(s)');
                   exit;
         End;

         if(StrPortadores.Count=0)
         Then Begin
                  MensagemAviso('Escolha pelo menos um portador');
                  exit;
         End;

         for cont:=0 to StrPortadores.Count-1 do
         Begin
              if (ObjLanctoPortadorGlobal.Lancamento.Pendencia.Titulo.PORTADOR.LocalizaCodigo(StrPortadores[cont])=False)
              Then Begin
                        MensagemErro('Portador '+StrPortadores[cont]+' n�o localizado');
                        exit;
              End;
         End;
         }

    End;

    With Self.Objquery do
    Begin
         With FreltxtRDPRINT do
         Begin
              linhalocal:=3;
              ConfiguraImpressao;
              RDprint.TamanhoQteColunas:=130;
              RDprint.FonteTamanhoPadrao:=S17cpp;
              RDprint.Abrir;
              if (RDprint.Setup=False)
              Then Begin
                        RDprint.Fechar;
                        exit;
              End;

              RDprint.ImpC(linhalocal,65,'RELAT�RIO DE MOVIMENTA��O DI�RIA',[negrito]);
              IncrementaLinha(2);

              RDprint.Impf(linhalocal,1,'Vendas do Dia '+datetostr(pdata),[negrito]);
              IncrementaLinha(2);

              RDprint.Impf(linhalocal,1,CompletaPalavra('PEDIDO',6,' ')+' '+
                                        CompletaPalavra('CLIENTE',40,' ')+' '+
                                        CompletaPalavra('VENDEDOR',20,' ')+' '+
                                        CompletaPalavra_a_esquerda('VALOR BRUTO',12,' ')+' '+
                                        CompletaPalavra_a_esquerda('VALOR DESC.',12,' ')+' '+
                                        CompletaPalavra_a_esquerda('VALOR L�Q.',12,' ')+' '+
                                        CompletaPalavra_a_esquerda('% DESCONTO',12,' '),[negrito]);
              IncrementaLinha(1);

              close;
              sql.clear;
              sql.add('Select tabpedido.Codigo,tabpedido.valortotal,');
              sql.add('tabpedido.valordesconto,tabpedido.valorfinal,tabpedido.vendedor,Tabvendedor.Nome AS nomevendedor,');
              sql.add('tabpedido.cliente,Tabcliente.Nome AS NOMECLIENTE from tabpedido');
              sql.add('join tabvendedor on Tabpedido.Vendedor=Tabvendedor.codigo');
              sql.add('join tabCliente on Tabpedido.Cliente=Tabcliente.codigo');
              Sql.add('Where Tabpedido.Concluido=''S'' ');
              sql.add('and TabPedido.Data='+#39+Formatdatetime('mm/dd/yyyy',pdata)+#39);
              sql.add('order by Tabpedido.vendedor');
              open;
              last;
              FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
              FMostraBarraProgresso.Lbmensagem.caption:='Gerando Relat�rio - Etapa de Vendas';
              first;

              While not(Self.Objquery.Eof) do
              Begin
                   FMostraBarraProgresso.IncrementaBarra1(1);
                   FMostraBarraProgresso.Show;
                   Application.ProcessMessages;

                   if (fieldbyname('valortotal').asfloat>0)
                   Then pporcentodesconto:=(fieldbyname('valordesconto').asfloat*100)/fieldbyname('valortotal').asfloat
                   Else pporcentodesconto:=0;

                   if (pporcentodesconto<0)
                   Then pporcentodesconto:=0;

                   VerificaLinha;
                   RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('codigo').asstring,6,' ')+' '+
                                            CompletaPalavra(fieldbyname('cliente').asstring,6,' ')+' '+CompletaPalavra(fieldbyname('nomecliente').asstring,33,' ')+' '+
                                            CompletaPalavra(fieldbyname('vendedor').asstring,6,' ')+' '+CompletaPalavra(fieldbyname('nomevendedor').asstring,13,' ')+' '+
                                            CompletaPalavra_a_esquerda(formata_valor(fieldbyname('valortotal').asstring),12,' ')+' '+
                                            CompletaPalavra_a_esquerda(formata_valor(fieldbyname('valordesconto').asstring),12,' ')+' '+
                                            CompletaPalavra_a_esquerda(formata_valor(fieldbyname('valorfinal').asstring),12,' ')+' '+
                                            CompletaPalavra_a_esquerda(formata_valor(pporcentodesconto),12,' '));
                   IncrementaLinha(1);

                   psomavalortotal:=psomavalortotal+Fieldbyname('valortotal').asfloat;
                   psomavalorfinal:=psomavalorfinal+Fieldbyname('valorfinal').asfloat;
                   psomadesconto:=psomadesconto+Fieldbyname('valordesconto').asfloat;

                   Self.Objquery.next;
              End;

              if (Psomavalortotal>0)
              Then pporcentodesconto:=(psomadesconto*100)/psomavalortotal
              else pporcentodesconto:=0;

              if (pporcentodesconto<0)
              then pporcentodesconto:=0;

              IncrementaLinha(1);
              VerificaLinha;
              RDprint.Impf(linhalocal,1,CompletaPalavra('TOTAL',68,' ')+' '+
                                       CompletaPalavra_a_esquerda(formata_valor(psomavalortotal),12,' ')+' '+
                                       CompletaPalavra_a_esquerda(formata_valor(psomadesconto),12,' ')+' '+
                                       CompletaPalavra_a_esquerda(formata_valor(psomavalorfinal),12,' ')+' '+
                                       CompletaPalavra_a_esquerda(formata_valor(pporcentodesconto),12,' '),[negrito]);
              IncrementaLinha(1);
              desenhalinha;
              IncrementaLinha(1);


              Close;
              Sql.clear;
              Sql.add('Select Tabtitulo.codigo as titulo,tabtipolancto.classificacao,');
              Sql.add('tabpendencia.codigo as pendencia,');
              Sql.add('tabtitulo.credordevedor,tabtitulo.codigocredordevedor,');
              Sql.add('tabpendencia.valor as VALORPENDENCIA,');
              Sql.add('tablancamento.valor,tabcontager.tipo,tabtitulo.contagerencial,tabcontager.nome as NOMECONTAGERENCIAL,');
              Sql.add('tablancamento.data');
              Sql.add('from tablancamento');
              Sql.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
              Sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
              Sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
              Sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
              //Sql.add('Where Tabtipolancto.classificacao=''Q'' ');
              Sql.add('where Tablancamento.Data='+#39+Formatdatetime('mm/dd/yyyy',pdata)+#39);
              Sql.add('order by tabcontager.tipo,tablancamento.codigo');
              open;
              //sql.SaveToFile('c:\reldemon.sql');
              last;
              FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
              FMostraBarraProgresso.Lbmensagem.caption:='Gerando Relat�rio - Etapa Financeira';
              first;

              PDC:='';
              psomaRecebimento:=0;
              PsomaPagamento:=0;

              While not(eof) do
              Begin
                   if (PDC<>Fieldbyname('tipo').asstring)//trocou ou � o primeiro
                   Then Begin
                            If not(Self.Objquery.bof)//senao for o primeiro, � porque trocou de C para D
                            then Begin
                                      if (PDC='C')
                                      then Begin
                                                IncrementaLinha(1);
                                                VerificaLinha;
                                                RDprint.ImpF(linhalocal,1,completapalavra('TOTAL DE RECEBIMENTOS DE T�TULOS DO DIA',107,' ')+' '+
                                                                          completapalavra_a_esquerda(formata_valor(PsomaRecebimento),12,' '),[negrito]);
                                                IncrementaLinha(1);
                                                desenhalinha;
                                      End
                                      Else Begin
                                                IncrementaLinha(1);
                                                VerificaLinha;
                                                RDprint.ImpF(linhalocal,1,completapalavra('TOTAL DE PAGAMENTOS DE T�TULOS DO DIA',107,' ')+' '+
                                                                          completapalavra_a_esquerda(formata_valor(PsomaPagamento),12,' '),[negrito]);
                                                IncrementaLinha(1);
                                                desenhalinha;
                                      End;
                            End;

                            //Cabecalho de Colunas
                            if (Fieldbyname('tipo').asstring='C')
                            Then Begin
                                      IncrementaLinha(1);
                                      VerificaLinha;
                                      RDprint.ImpF(linhalocal,1,'RECEBIMENTOS DE T�TULOS DO DIA '+datetostr(pdata),[negrito]);
                                      IncrementaLinha(2);

                                      VerificaLinha;
                                      RDprint.ImpF(linhalocal,1,completapalavra('PEDIDO',6,' ')+' '+
                                                                completapalavra('PEND.',6,' ')+' '+
                                                                completapalavra('CLIENTE',50,' ')+' '+
                                                                completapalavra('VENDEDOR',42,' ')+' '+
                                                                completapalavra_a_esquerda('VALOR REC.',12,' '),[negrito]);
                                       IncrementaLinha(1);
                            End
                            Else Begin
                                      IncrementaLinha(1);
                                      VerificaLinha;
                                      RDprint.ImpF(linhalocal,1,'PAGAMENTOS DE T�TULOS DO DIA '+datetostr(pdata),[negrito]);
                                      IncrementaLinha(2);

                                      VerificaLinha;
                                      RDprint.ImpF(linhalocal,1,completapalavra('T�TULO',6,' ')+' '+
                                                                completapalavra('PEND.',6,' ')+' '+
                                                                completapalavra('FORNECEDOR',50,' ')+' '+
                                                                completapalavra('CONTA GERENCIAL',42,' ')+' '+
                                                                completapalavra_a_esquerda('VALOR PAGO',12,' '),[negrito]);
                                       IncrementaLinha(1);
                            End;
                            Pdc:=Fieldbyname('tipo').asstring;
                   End;

                   Ppedido:='';
                   Pnomevendedor:='';
                   pnomecredordevedor:='';
                   PnomeCredorDevedor:=CompletaPalavra(Fieldbyname('codigocredordevedor').asstring,6,' ')+' - '+ObjLanctoPortadorGlobal.Lancamento.Pendencia.Titulo.credordevedor.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring);

                   if (Pdc='C')
                   Then Begin
                             if (fieldbyname('classificacao').asstring='Q')
                             Then psomaRecebimento:=psomaRecebimento+fieldbyname('valor').asfloat;

                             if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaTitulo(fieldbyname('titulo').asstring)=True)
                             Then Begin
                                       Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
                                       Ppedido:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.get_codigo;
                                       pnomevendedor:=completapalavra(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Vendedor.Get_Codigo,6,' ')+' - '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Vendedor.Get_Nome;
                             End;
                   End
                   Else Begin
                             if (fieldbyname('classificacao').asstring='Q')
                             Then psomaPagamento:=psomaPagamento+fieldbyname('valor').asfloat;

                             Ppedido:=Fieldbyname('titulo').asstring;
                             Pnomevendedor:=Completapalavra(Fieldbyname('contagerencial').asstring,6,' ')+' - '+Fieldbyname('nomecontagerencial').asstring;

                   End;

                   VerificaLinha;
                   RDprint.Imp(linhalocal,1,completapalavra(ppedido,6,' ')+' '+
                                            completapalavra(Fieldbyname('pendencia').asstring,6,' ')+' '+
                                            completapalavra(pnomecredordevedor,50,' ')+' '+
                                            completapalavra(Pnomevendedor,42,' ')+' '+
                                            completapalavra_a_esquerda(formata_valor(Fieldbyname('valor').asstring),12,' ')+' '+
                                            completapalavra(fieldbyname('classificacao').asstring,1,' '));
                   IncrementaLinha(1);

                   Self.Objquery.next;
              End;//While dos lancamentos

              if (recordcount>0)//as vezes nao teve rec nem pag
              Then Begin
                        //****************************************************************
                        if (PDC='C')
                        then Begin
                                  IncrementaLinha(1);
                                  VerificaLinha;
                                  RDprint.ImpF(linhalocal,1,completapalavra('TOTAL DE RECEBIMENTOS DE T�TULOS DO DIA',107,' ')+' '+
                                                            completapalavra_a_esquerda(formata_valor(PsomaRecebimento),12,' '),[negrito]);
                                  IncrementaLinha(1);
                                  desenhalinha;
                        End
                        Else Begin
                                  IncrementaLinha(1);
                                  VerificaLinha;
                                  RDprint.ImpF(linhalocal,1,completapalavra('TOTAL DE PAGAMENTOS DE T�TULOS DO DIA',107,' ')+' '+
                                                            completapalavra_a_esquerda(formata_valor(PsomaPagamento),12,' '),[negrito]);
                                  IncrementaLinha(1);
                                  desenhalinha;
                        End;
                        //****************************************************************
              End;
              rdprint.fechar;
         End;//with Freltxtx
    End;//Self.objquery

Finally
       FMostraBarraProgresso.Close;
end;

end;

function TObjPedidoObjetos.ValidaDescontomaximo: boolean;
var
Pporcento,PDescontoPedido,PvalorTotal,PvalorBonificacao:currency;
begin
     Result:=False;

     Try
        PDescontoPedido:=Strtofloat(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorDesconto);
     Except
        PDescontoPedido:=0;
     End;

     Try
        PvalorTotal:=Strtofloat(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorTotal);
     Except
        Pvalortotal:=0;
     End;


     Try
        PvalorBonificacao:=Strtofloat(ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorBonificacaoCliente);
     Except
        PvalorBonificacao:=0;
    End;

     //Pvalortotal    100%
     //PdescontoPedido  X
     //X=(PdescontoPedido*100)/PvalorTotal



     {Na glassbox o desconto permitido � 35% ou seja
     sobre uma venda de R$ 1.000 posso dar at� 350,00 de desconto
     por�m os vendedores estavam utilizando bonificacao para que o desconto
     pudesse ser maior, por exemplo

     R$ 1.000,00
     desconto R$ 350,00

     bonificacao R$ 50,00

     Nesse caso o valor final seria R$ 600,00

     Para evitar isso a Camila solicitou no dia 08/06/08 que fosse calculado
     o percentual sobre o valordosprodutos-bonificacao

     ou seja


     R$ 1000,00 - R$ 50,00 = R$ 950,00 * 35% = R$ 332,50 seria o desconto m�ximo}

     try
        Pporcento:=(PDescontoPedido*100)/(PvalorTotal-PvalorBonificacao);
     Except
        Pporcento:=0;
     End;

     if (strtofloat(floattostr(Pporcento))>FdataModulo.DescontoMaximo_global)
     Then Begin
               if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('DESCONTO MAIOR QUE O PERMITIDO')=False)
               Then begin
                         if (ObjUsuarioGlobal.PedePermissao(strtoint(ObjPermissoesUsoGlobal.Get_nivel),'"DESCONTO MAIOR QUE O PERMITIDO"')=False)
                         then Begin
                                   MensagemErro('Usu�rio sem autoriza��o para desconto maior que '+formata_valor(FdataModulo.DescontoMaximo_global)+' % '+#13+'Desconto aplicado no pedido '+Formata_valor(pporcento));
                                   exit;
                         end;
               End;
     End;
     result:=True;
end;

procedure TObjPedidoObjetos.Relatorio_Pedidos_nao_entregues;
var
   PdataInicial,PdataFinal,DataEntrega:string;
   Pcliente               :string;
   Pquant:Integer;
begin
     limpaedit(Ffiltroimp);

     With Ffiltroimp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;

          LbGrupo01.caption:='Data Pedido Inicial';
          LbGrupo02.caption:='Data Pedido Final';
          LbGrupo03.caption:='Cliente';

          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;

          edtgrupo03.OnKeyDown:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.EdtClienteKeyDown;

          edtgrupo03.Color:=$005CADFE;

          Showmodal;

          if (Tag=0)
          then exit;

          PdataInicial:='';
          PDataFinal:='';
          Pcliente:='';


          if (comebarra(trim(edtgrupo01.text))<>'')
          Then Begin
                    Try
                       strtodate(edtgrupo01.text);
                       Pdatainicial:=edtgrupo01.text;
                    Except
                          MensagemErro('Data Inicial Inv�lida');
                    End;
          End;

          if (comebarra(trim(edtgrupo02.text))<>'')
          Then Begin
                    Try
                       strtodate(edtgrupo02.text);
                       PdataFinal:=edtgrupo02.text;
                    Except
                          MensagemErro('Data Final Inv�lida');
                    End;
          End;


          if (edtgrupo03.Text<>'')
          Then Begin
                    Try
                       Strtoint(edtgrupo03.Text);
                       Pcliente:=edtgrupo03.Text;
                       if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.LocalizaCodigo(pcliente)=False)
                       Then Begin
                                 MensagemErro('Cliente n�o encontrado');
                                 exit;
                       End;
                       Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.TabelaparaObjeto;
                    Except
                          MensagemErro('C�digo inv�lido para o Cliente');
                          exit;
                    End;
          End;


     End;
     With Self.ObjqueryTEMP do
     Begin
         //pedidos que entraram em romaneio e ainda n�o foram entregues

         close;
         sql.clear;
         sql.add('Select tabpedido_proj.pedido,Tabpedido_proj.codigo as PEDIDOPROJETO,tabpedido.data,');
         sql.add('tabpedido.cliente,tabcliente.nome as NOMECLIENTE,tppr.romaneio');
         sql.add('from tabpedidoprojetoromaneio TPPR');
         sql.add('join tabromaneio on TPPR.romaneio=tabromaneio.codigo');
         sql.add('join Tabpedido_proj on TPPR.pedidoprojeto=Tabpedido_proj.codigo');
         sql.add('join tabpedido on tabpedido_proj.pedido=tabpedido.codigo');
         sql.add('join tabcliente on tabpedido.cliente=tabcliente.codigo');
         sql.add('where Tabromaneio.concluido=''N'' ');

         if (PdataInicial<>'')
         Then sql.add('and tabPedido.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatainicial))+#39);

         if (PdataFinal<>'')
         Then sql.add('and tabPedido.data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdataFinal))+#39);

         if (Pcliente<>'')
         Then sql.add('and tabpedido.cliente='+pcliente);

         //if(DataEntrega<>'')
         //then SQL.Add('and tabpedido.dataentrega='+#39+formatdatetime('mm/dd/yyyy',strtodate(DataEntrega))+#39);

         sql.add('order by tabpedido.data');
         open;

         last;
         FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
         FMostraBarraProgresso.Lbmensagem.Caption:='Pedidos que n�o foram entregues ';
         first;
          With Freltxtrdprint do
          Begin
               ConfiguraImpressao;
               RDprint.abrir;

               if (RDprint.setup=False)
               Then begin
                         RDprint.fechar;
                         exit;
               End;
               linhalocal:=3;//devido ao cabecalho

               RDprint.ImpC(linhalocal,45,'PEDIDOS E PROJETOS QUE N�O FORAM ENTREGUES',[negrito]);
               IncrementaLinha(2);

               if (PdataInicial<>'') or (Pdatafinal<>'')
               then Begin
                         RDprint.Impf(linhalocal,1,'Per�odo: '+Pdatainicial+' a '+Pdatafinal,[negrito]);
                         IncrementaLinha(1);
               End;

               if (Pcliente<>'')
               then begin
                         RDprint.Impf(linhalocal,1,'Cliente: '+Pcliente+'-'+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome,[negrito]);
                         IncrementaLinha(1);
               End;
               IncrementaLinha(1);

               RDprint.Impf(linhalocal,1,CompletaPalavra('DATA',10,' ')+' '+
                                         CompletaPalavra('PEDIDO',6,' ')+' '+
                                         CompletaPalavra('PEDPRJ',6,' ')+' '+
                                         CompletaPalavra('CLIENTE',56,' ')+' '+
                                         CompletaPalavra('ROMANEIO',8,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'******PEDIDOS QUE N�O FORAM ENTREGUES********',[negrito]);
               IncrementaLinha(2);

               While not(Self.ObjqueryTEMP.eof) do
               begin
                    FMostraBarraProgresso.IncrementaBarra1(1);
                    FMostraBarraProgresso.Show;
                    Application.ProcessMessages;

                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('data').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('pedido').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('pedidoprojeto').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('cliente').asstring+'-'+fieldbyname('nomecliente').asstring,56,' ')+' '+
                                             CompletaPalavra(fieldbyname('romaneio').asstring,6,' '));
                    IncrementaLinha(1);

                    Self.ObjqueryTEMP.Next;
               end;
               Pquant:=Pquant+Self.ObjqueryTEMP.RecordCount;
               VerificaLinha;
               RDprint.Impf(linhalocal,1,'Quantidade de Pedidos em romaneio que n�o foram entregues : '+inttostr(Self.ObjqueryTEMP.RecordCount),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               VerificaLinha;
               RDprint.Impf(linhalocal,1,'Quantidade Total: '+inttostr(Pquant),[negrito]);
               IncrementaLinha(1);

               FMostraBarraProgresso.close;
               RDprint.Fechar;
          End;//Wth Freltxt..
     End;//with Self.Objquery
end;

procedure TObjPedidoObjetos.Relatorio_Pedidos_nao_entraram_romaneio_e_nao_concluidos;
var
   PdataInicial,PdataFinal,DataEntrega:string;
   Pcliente               :string;
   Pquant:Integer;

begin
     limpaedit(Ffiltroimp);

     With Ffiltroimp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;
          Grupo04.Enabled:=True;

          LbGrupo01.caption:='Data Pedido Inicial';
          LbGrupo02.caption:='Data Pedido Final';
          LbGrupo03.caption:='Cliente';
          LbGrupo04.Caption:='Data entrega no PED';

          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;
          edtgrupo04.EditMask:=MascaraData;

          edtgrupo03.OnKeyDown:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.EdtClienteKeyDown;
          edtgrupo03.Color:=$005CADFE;

          Showmodal;

          if (Tag=0)
          then exit;

          PdataInicial:='';
          PDataFinal:='';
          Pcliente:='';


          if (comebarra(trim(edtgrupo01.text))<>'')
          Then Begin
                    Try
                       strtodate(edtgrupo01.text);
                       Pdatainicial:=edtgrupo01.text;
                    Except
                          MensagemErro('Data Inicial Inv�lida');
                    End;
          End;

          if (comebarra(trim(edtgrupo02.text))<>'')
          Then Begin
                    Try
                       strtodate(edtgrupo02.text);
                       PdataFinal:=edtgrupo02.text;
                    Except
                          MensagemErro('Data Final Inv�lida');
                    End;
          End;

          if (comebarra(trim(edtgrupo04.text))<>'')
          Then Begin
                    Try
                       strtodate(edtgrupo04.text);
                       DataEntrega:=edtgrupo04.text;
                    Except
                          MensagemErro('Data Final Inv�lida');
                    End;
          End;

          if (edtgrupo03.Text<>'')
          Then Begin
                    Try
                       Strtoint(edtgrupo03.Text);
                       Pcliente:=edtgrupo03.Text;
                       if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.LocalizaCodigo(pcliente)=False)
                       Then Begin
                                 MensagemErro('Cliente n�o encontrado');
                                 exit;
                       End;
                       Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.TabelaparaObjeto;
                    Except
                          MensagemErro('C�digo inv�lido para o Cliente');
                          exit;
                    End;
          End;


     End;

     With Self.ObjqueryTEMP do
     Begin
          //PEDIDOS QUE NAO ENTRARAM EM ROMANEIO
          close;
          sql.clear;
          sql.add('Select tabpedido_proj.pedido,Tabpedido_proj.codigo as PEDIDOPROJETO,tabpedido.data,tabpedido.cliente,tabcliente.nome as NOMECLIENTE,tabpedido.dataentrega');
          sql.add('from Tabpedido_proj');
          sql.add('join tabpedido on tabpedido_proj.pedido=tabpedido.codigo');
          sql.add('join tabcliente on tabpedido.cliente=tabcliente.codigo');
          sql.add('where tabpedido.Concluido=''S'' ');

          if (PdataInicial<>'')
          Then sql.add('and tabPedido.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatainicial))+#39);

          if (PdataFinal<>'')
          Then sql.add('and tabPedido.data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdataFinal))+#39);

          if (Pcliente<>'')
          Then sql.add('and tabpedido.cliente='+pcliente);

          if(DataEntrega<>'')
          then SQL.Add('and tabpedido.dataentrega='+#39+formatdatetime('mm/dd/yyyy',strtodate(DataEntrega))+#39);

          sql.add('and not tabpedido_proj.codigo in');
          sql.add('(');
          sql.add('Select distinct(TPPR.pedidoprojeto) from tabpedidoprojetoromaneio TPPR');
          sql.add(')');
          //sql.savetofile('c:\teste.sql');
          sql.add('order by tabpedido.data');
          open;
          last;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.Lbmensagem.caption:='Pedidos que n�o entraram em romaneio';
          first;

          With Freltxtrdprint do
          Begin
               ConfiguraImpressao;
               RDprint.abrir;

               if (RDprint.setup=False)
               Then begin
                         RDprint.fechar;
                         exit;
               End;
               linhalocal:=3;//devido ao cabecalho

               RDprint.ImpC(linhalocal,45,'PEDIDOS E PROJETOS QUE N�O ENTRARAM EM ROMANEIO',[negrito]);
               IncrementaLinha(2);

               if (PdataInicial<>'') or (Pdatafinal<>'')
               then Begin
                         RDprint.Impf(linhalocal,1,'Per�odo: '+Pdatainicial+' a '+Pdatafinal,[negrito]);
                         IncrementaLinha(1);
               End;

               if (Pcliente<>'')
               then begin
                         RDprint.Impf(linhalocal,1,'Cliente: '+Pcliente+'-'+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome,[negrito]);
                         IncrementaLinha(1);
               End;

               if(DataEntrega<>'') then
               begin
                    RDprint.Impf(linhalocal,1,'Data de entregue no pedido: '+DataEntrega,[negrito]);
                    IncrementaLinha(1);
               end;

               IncrementaLinha(1);

               RDprint.Impf(linhalocal,1,CompletaPalavra('DATA',10,' ')+' '+
                                         CompletaPalavra('PEDIDO',6,' ')+' '+
                                         CompletaPalavra('PEDPRJ',6,' ')+' '+
                                         CompletaPalavra('CLIENTE',56,' ')+' '+
                                         CompletaPalavra('DATA ENTREGA',12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;

               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'******PEDIDOS QUE N�O ENTRARAM EM ROMANEIO********',[negrito]);
               IncrementaLinha(2);

               while not(Self.ObjqueryTEMP.eof) do
               begin
                    FMostraBarraProgresso.IncrementaBarra1(1);
                    FMostraBarraProgresso.show;
                    Application.ProcessMessages;


                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('data').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('pedido').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('pedidoprojeto').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('cliente').asstring+'-'+fieldbyname('nomecliente').asstring,56,' ')+' '+
                                             CompletaPalavra(fieldbyname('dataentrega').asstring,12,' '));
                    IncrementaLinha(1);
                    Self.ObjqueryTEMP.next;
               end;
               Pquant:=Self.ObjqueryTEMP.RecordCount;
               VerificaLinha;
               RDprint.Impf(linhalocal,1,'Quantidade de Pedidos que n�o entraram em romaneio: '+inttostr(Self.ObjqueryTEMP.RecordCount),[negrito]);
               IncrementaLinha(1);


               VerificaLinha;
               DesenhaLinha;
              {***************************************************************************************
              *  Eram dois relatorios em um, a partir de agora foi dividido                         *
              *  Assim temos um para mostrar os pedidos q n�o entraram e romaneios                  *
              *  e outro quem mostra os pedidos que entraram em romaneio porem ainda nao            *
              *  foram entregues.                                                                   *
              ***************************************************************************************
              //pedidos que entraram em romaneio e ainda n�o foram entregues

               close;
               sql.clear;
               sql.add('Select tabpedido_proj.pedido,Tabpedido_proj.codigo as PEDIDOPROJETO,tabpedido.data,');
               sql.add('tabpedido.cliente,tabcliente.nome as NOMECLIENTE,tppr.romaneio');
               sql.add('from tabpedidoprojetoromaneio TPPR');
               sql.add('join tabromaneio on TPPR.romaneio=tabromaneio.codigo');
               sql.add('join Tabpedido_proj on TPPR.pedidoprojeto=Tabpedido_proj.codigo');
               sql.add('join tabpedido on tabpedido_proj.pedido=tabpedido.codigo');
               sql.add('join tabcliente on tabpedido.cliente=tabcliente.codigo');
               sql.add('where Tabromaneio.concluido=''N'' ');

               if (PdataInicial<>'')
               Then sql.add('and tabPedido.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatainicial))+#39);

               if (PdataFinal<>'')
               Then sql.add('and tabPedido.data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdataFinal))+#39);

               if (Pcliente<>'')
               Then sql.add('and tabpedido.cliente='+pcliente);

               if(DataEntrega<>'')
               then SQL.Add('and tabpedido.dataentrega='+#39+formatdatetime('mm/dd/yyyy',strtodate(DataEntrega))+#39);

               sql.add('order by tabpedido.data');
               open;

               last;
               FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
               FMostraBarraProgresso.Lbmensagem.Caption:='Pedidos que n�o foram entregues - Etapa 2/2';
               first;

               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'******PEDIDOS QUE N�O FORAM ENTREGUES********',[negrito]);
               IncrementaLinha(2);

               While not(Self.ObjqueryTEMP.eof) do
               begin
                    FMostraBarraProgresso.IncrementaBarra1(1);
                    FMostraBarraProgresso.Show;
                    Application.ProcessMessages;

                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('data').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('pedido').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('pedidoprojeto').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('cliente').asstring+'-'+fieldbyname('nomecliente').asstring,56,' ')+' '+
                                             CompletaPalavra(fieldbyname('romaneio').asstring,6,' '));
                    IncrementaLinha(1);

                    Self.ObjqueryTEMP.Next;
               end;
               Pquant:=Pquant+Self.ObjqueryTEMP.RecordCount;
               VerificaLinha;
               RDprint.Impf(linhalocal,1,'Quantidade de Pedidos em romaneio que n�o foram entregues : '+inttostr(Self.ObjqueryTEMP.RecordCount),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               VerificaLinha;
               RDprint.Impf(linhalocal,1,'Quantidade Total: '+inttostr(Pquant),[negrito]);
               IncrementaLinha(1);    }

               FMostraBarraProgresso.close;
               RDprint.Fechar;
          End;//Wth Freltxt..
     End;//with Self.Objquery
end;

procedure TObjPedidoObjetos.EdtCodigoMaterialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
FpesquisaXX:Tfpesquisa;
comandotemp:string;
begin
     if (key<>vk_f9)
     then exit;


     if ((FfiltroImp.ComboGrupo07.Text='FERRAGEM')
       or(FfiltroImp.ComboGrupo07.Text='PERFILADO')
      or (FfiltroImp.ComboGrupo07.Text='VIDRO')
      or (FfiltroImp.ComboGrupo07.Text='KITBOX')
      or (FfiltroImp.ComboGrupo07.Text='PERSIANA')
      or (FfiltroImp.ComboGrupo07.Text='DIVERSO'))
     then comandotemp:='Select * from tab'+FfiltroImp.ComboGrupo07.Text
     Else Begin
               MensagemAviso('Escolha o cadastro');
               exit;
     End;

     try
        FpesquisaXX:=Tfpesquisa.create(nil);
     except
           Mensagemerro('Erro na tentativa de criar o formul�rio de pesquisa');
           exit;
     End;

     Try
        if (FpesquisaXX.PreparaPesquisa(comandotemp,'Pesquisa de '+FfiltroImp.ComboGrupo07.Text,nil)=true)
        then Begin
                  Try
                     If (FpesquisaXX.showmodal=mrok)
                     Then Begin
                              TEdit(Sender).text:=FpesquisaXX.DBGrid.DataSource.DataSet.fieldbyname('codigo').asstring;
                     End;
                  Finally
                        FpesquisaXX.QueryPesq.close;
                  End;
        End;
     finally
            freeandnil(FpesquisaXX);
     End;


End;

procedure TObjPedidoObjetos.Imprime_quantidade_e_media_valor_produto;
var
Pdatainicial,pdatafinal:String;
pnomecliente,pcliente,Pmaterial,pnomematerialescolhido:String;
Pcodigomaterial:string;
psomaquantidade,psomavalor:Currency;
begin
     Pmaterial:='';
     pcodigomaterial:='';
     pnomematerialescolhido:='';
     pnomecliente:='';
     pcliente:='';
     pdatainicial:='';
     pdatafinal:='';

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;
          grupo07.Enabled:=true;

          LbGrupo01.Caption:='Data Inicial Venda';
          LbGrupo02.Caption:='Data Final Venda';
          LbGrupo03.Caption:='Cliente';

          LbGrupo07.caption:='Cadastro e produto';

          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;
          edtgrupo03.OnKeyDown:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.EdtClienteKeyDown;
          edtgrupo03.Color:=$005CADFE;

          ComboGrupo07.items.clear;
          ComboGrupo07.items.add('FERRAGEM');
          ComboGrupo07.items.add('PERFILADO');
          ComboGrupo07.items.add('VIDRO');
          ComboGrupo07.items.add('KITBOX');
          ComboGrupo07.items.add('PERSIANA');
          ComboGrupo07.items.add('DIVERSO');

          edtgrupo07.color:=$005CADFE;
          edtgrupo07.OnKeyDown:=Self.edtCodigomaterialKeyDown;

          Showmodal;

          if (tag=0)
          then exit;


          if (trim(comebarra(edtgrupo01.text))<>'')
          Then Begin
                    try
                       strtodate(edtgrupo01.Text);
                       Pdatainicial:=edtgrupo01.Text;
                    except
                          mensagemerro('Data inicial inv�lida');
                          exit;
                    End;
          End;

          if (trim(comebarra(edtgrupo02.text))<>'')
          Then Begin
                    try
                       strtodate(edtgrupo02.Text);
                       PdataFinal:=edtgrupo02.Text;
                    except
                          mensagemerro('Data final inv�lida');
                          exit;
                    End;
          End;

         if ((FfiltroImp.ComboGrupo07.Text='FERRAGEM')
           or(FfiltroImp.ComboGrupo07.Text='PERFILADO')
          or (FfiltroImp.ComboGrupo07.Text='VIDRO')
          or (FfiltroImp.ComboGrupo07.Text='KITBOX')
          or (FfiltroImp.ComboGrupo07.Text='PERSIANA')
          or (FfiltroImp.ComboGrupo07.Text='DIVERSO'))
         then Pmaterial:=FfiltroImp.ComboGrupo07.Text;

         Pcodigomaterial:=edtgrupo07.Text;

         if (edtgrupo03.text<>'')
         then Begin
                   try
                      strtoint(edtgrupo03.text);
                      pcliente:=edtgrupo03.text;


                      if (self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.LocalizaCodigo(pcliente)=False)
                      Then Begin
                                MensagemErro('Cliente n�o encontrado');
                                exit;
                      End;
                      self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.TabelaparaObjeto;
                      pnomecliente:=self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome;
                   Except
                         MensagemErro('Cliente Inv�lido');
                         exit;
                   End;
         End;
     end;

     With Self.ObjqueryTEMP do
     Begin


          close;
          sql.clear;
          sql.add('Select PMPP.Material,');
          sql.add('PMPP.codigomaterial,PMPP.referenciamaterial,PMPP.nomematerial,');
          sql.add('PMPP.referenciacor,PMPP.nomecor,');
          sql.add('sum(PMPP.quantidade) as QUANTIDADE,avg(PMPP.valor) as MEDIAVALOR');
          sql.add('from PROC_MATERIAIS_PP PMPP');
          sql.add('join tabpedido_proj on PMPP.pedidoprojeto=tabpedido_proj.codigo');
          sql.add('join tabpedido on tabpedido_proj.pedido=tabpedido.codigo');
          sql.add('where tabpedido.concluido=''S'' ');

          if (Pdatainicial<>'')
          then sql.add('and tabpedido.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(Pdatainicial))+#39);

          if (pdatafinal<>'')
          Then sql.add('and tabpedido.data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(Pdatafinal))+#39);

          if (pmaterial<>'')
          Then sql.add('and material='+#39+uppercase(pmaterial)+#39);

          if (pcliente<>'')
          Then sql.add('and Tabpedido.cliente='+pcliente);

          if (Pcodigomaterial<>'')
          then sql.add('and codigomaterial='+pcodigomaterial);

          sql.add('group by PMPP.Material,');
          sql.add('PMPP.codigomaterial,PMPP.referenciamaterial,PMPP.nomematerial,');
          sql.add('PMPP.referenciacor,PMPP.nomecor');
          sql.add('order by PMPP.material,PMPP.nomematerial');
          //sql.savetofile('c:\rel6vendas.sql');

          open;
          last;

          if (recordcount=0)
          then Begin
                    mensagemaviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;
          pnomematerialescolhido:=fieldbyname('nomematerial').asstring;
          first;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               RDprint.TamanhoQteColunas:=130;
               RDprint.FonteTamanhoPadrao:=S17cpp;

               LinhaLocal:=3;
               RDprint.Abrir;

               if (RDprint.Setup=False)
               then begin
                         rdprint.Fechar;
                         exit;
               end;

               rdprint.ImpC(linhalocal,65,'M�DIA DE VALOR DE VENDA POR PRODUTO',[negrito]);
               incrementalinha(2);

               if (Pmaterial<>'')
               then Begin
                          RDprint.ImpF(linhalocal,1,'CADASTRO: '+pmaterial,[negrito]);
                          IncrementaLinha(1);
               End;

               if (Pcodigomaterial<>'')
               then Begin

                          RDprint.ImpF(linhalocal,1,'PRODUTO: '+pnomematerialescolhido,[negrito]);
                          IncrementaLinha(1);
               End;

               if (Pcliente<>'')
               Then BEgin
                         RDprint.ImpF(linhalocal,1,'CLIENTE: '+pnomecliente,[negrito]);
                         IncrementaLinha(1);
               End;


               if (Pdatainicial<>'') or (Pdatafinal<>'')
               Then begin
                         RDprint.ImpF(linhalocal,1,'Per�odo : '+Pdatainicial+' a '+pdatafinal,[negrito]);
                         IncrementaLinha(1);
               End;
               IncrementaLinha(1);
               //titulo das colunas
               RDprint.ImpF(linhalocal,1,CompletaPalavra('MATERIAL',12,' ')+' '+
                                         CompletaPalavra('REFERENCIA',10,' ')+' '+
                                         CompletaPalavra('PRODUTO',40,' ')+' '+
                                         CompletaPalavra('REF.COR',10,' ')+' '+
                                         CompletaPalavra('NOME COR',15,' ')+' '+
                                         CompletaPalavra_a_Esquerda('QUANTIDADE',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR M�DIO',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('TOTAL',12,' '),[negrito]);
               IncrementaLinha(1);
               Desenhalinha;

               psomaquantidade:=0;
               psomavalor:=0;

               while not(Self.ObjqueryTEMP.eof) do
               Begin

                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('material').asstring,12,' ')+' '+
                                              CompletaPalavra(fieldbyname('referenciamaterial').asstring,10,' ')+' '+
                                              CompletaPalavra(fieldbyname('codigomaterial').asstring,6,' ')+'-'+CompletaPalavra(fieldbyname('nomematerial').asstring,33,' ')+' '+
                                              CompletaPalavra(fieldbyname('referenciacor').asstring,10,' ')+' '+
                                              CompletaPalavra(fieldbyname('nomecor').asstring,15,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('quantidade').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('mediavalor').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('quantidade').asfloat*fieldbyname('mediavalor').asfloat),12,' '));
                    IncrementaLinha(1);

                    psomaquantidade:=psomaquantidade+Fieldbyname('quantidade').asfloat;
                    psomavalor:=psomavalor+(Fieldbyname('quantidade').asfloat*Fieldbyname('mediavalor').asfloat);

                    Self.ObjqueryTEMP.next;
               end;
               DesenhaLinha;
               VerificaLinha;
               RDprint.ImpF(linhalocal,1,completapalavra('TOTALIZA��O',92,' ')+
                                         CompletaPalavra_a_Esquerda(formata_valor(psomaquantidade),12,' ')+' '+
                                         CompletaPalavra_a_Esquerda(formata_valor(psomavalor),25,' '),[negrito]);

               rdprint.Fechar;
          End;
     End;


end;


procedure TObjPedidoObjetos.Imprime_quantidade_entregue_e_media_valor_produto;
var
Pdatainicial,pdatafinal:String;
pnomecliente,pcliente,Pmaterial,pnomematerialescolhido:String;
Pcodigomaterial:string;
psomaquantidade,psomavalor:Currency;
begin
     Pmaterial:='';
     pcodigomaterial:='';
     pnomematerialescolhido:='';
     pnomecliente:='';
     pcliente:='';
     pdatainicial:='';
     pdatafinal:='';

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;
          grupo07.Enabled:=true;

          LbGrupo01.Caption:='Data de Entrega Inicial';
          LbGrupo02.Caption:='Data de Entrega Final';
          LbGrupo03.Caption:='Cliente';

          LbGrupo07.caption:='Cadastro e produto';

          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;
          edtgrupo03.OnKeyDown:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.EdtClienteKeyDown;
          edtgrupo03.Color:=$005CADFE;

          ComboGrupo07.items.clear;
          ComboGrupo07.items.add('FERRAGEM');
          ComboGrupo07.items.add('PERFILADO');
          ComboGrupo07.items.add('VIDRO');
          ComboGrupo07.items.add('KITBOX');
          ComboGrupo07.items.add('PERSIANA');
          ComboGrupo07.items.add('DIVERSO');

          edtgrupo07.OnKeyDown:=Self.edtCodigomaterialKeyDown;
          edtgrupo07.Color:=$005CADFE;

          Showmodal;

          if (tag=0)
          then exit;


          if (trim(comebarra(edtgrupo01.text))<>'')
          Then Begin
                    try
                       strtodate(edtgrupo01.Text);
                       Pdatainicial:=edtgrupo01.Text;
                    except
                          mensagemerro('Data inicial inv�lida');
                          exit;
                    End;
          End;

          if (trim(comebarra(edtgrupo02.text))<>'')
          Then Begin
                    try
                       strtodate(edtgrupo02.Text);
                       PdataFinal:=edtgrupo02.Text;
                    except
                          mensagemerro('Data final inv�lida');
                          exit;
                    End;
          End;

         if ((FfiltroImp.ComboGrupo07.Text='FERRAGEM')
           or(FfiltroImp.ComboGrupo07.Text='PERFILADO')
          or (FfiltroImp.ComboGrupo07.Text='VIDRO')
          or (FfiltroImp.ComboGrupo07.Text='KITBOX')
          or (FfiltroImp.ComboGrupo07.Text='PERSIANA')
          or (FfiltroImp.ComboGrupo07.Text='DIVERSO'))
         then Pmaterial:=FfiltroImp.ComboGrupo07.Text;

         Pcodigomaterial:=edtgrupo07.Text;

         if (edtgrupo03.text<>'')
         then Begin
                   try
                      strtoint(edtgrupo03.text);
                      pcliente:=edtgrupo03.text;


                      if (self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.LocalizaCodigo(pcliente)=False)
                      Then Begin
                                MensagemErro('Cliente n�o encontrado');
                                exit;
                      End;
                      self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.TabelaparaObjeto;
                      pnomecliente:=self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome;
                   Except
                         MensagemErro('Cliente Inv�lido');
                         exit;
                   End;
         End;
     end;

     With Self.ObjqueryTEMP do
     Begin


          close;
          sql.clear;
          sql.add('Select PMPP.Material,');
          sql.add('PMPP.codigomaterial,PMPP.referenciamaterial,PMPP.nomematerial,');
          sql.add('PMPP.referenciacor,PMPP.nomecor,');
          sql.add('sum(PMPP.quantidade) as QUANTIDADE,avg(PMPP.valor) as MEDIAVALOR');
          sql.add('from PROC_MATERIAIS_PP PMPP');
          sql.add('join tabpedidoprojetoromaneio on PMPP.pedidoprojeto=tabpedidoprojetoromaneio.pedidoprojeto');
          sql.add('join tabromaneio on tabpedidoprojetoromaneio.romaneio=tabromaneio.codigo');
          sql.add('join tabpedido_proj on tabpedidoprojetoromaneio.pedidoprojeto=tabpedido_proj.codigo');
          sql.add('join tabpedido on tabpedido_proj.pedido=tabpedido.codigo');
          sql.add('where tabpedido.concluido=''S'' ');

          if (Pdatainicial<>'')
          then sql.add('and tabromaneio.dataentrega>='+#39+formatdatetime('mm/dd/yyyy',strtodate(Pdatainicial))+#39);

          if (pdatafinal<>'')
          Then sql.add('and tabromaneio.dataentrega<='+#39+formatdatetime('mm/dd/yyyy',strtodate(Pdatafinal))+#39);

          if (pmaterial<>'')
          Then sql.add('and material='+#39+uppercase(pmaterial)+#39);

          if (pcliente<>'')
          Then sql.add('and Tabpedido.cliente='+pcliente);

          if (Pcodigomaterial<>'')
          then sql.add('and codigomaterial='+pcodigomaterial);

          sql.add('group by PMPP.Material,');
          sql.add('PMPP.codigomaterial,PMPP.referenciamaterial,PMPP.nomematerial,');
          sql.add('PMPP.referenciacor,PMPP.nomecor');
          sql.add('order by PMPP.material,PMPP.nomematerial');

          open;
          last;

          if (recordcount=0)
          then Begin
                    mensagemaviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;
          pnomematerialescolhido:=fieldbyname('nomematerial').asstring;
          first;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               RDprint.TamanhoQteColunas:=130;
               RDprint.FonteTamanhoPadrao:=S17cpp;

               LinhaLocal:=3;
               RDprint.Abrir;

               if (RDprint.Setup=False)
               then begin
                         rdprint.Fechar;
                         exit;
               end;

               rdprint.ImpC(linhalocal,65,'M�DIA DE VALOR DE VENDA E QUANTIDADE ENTREGUE POR PRODUTO',[negrito]);
               incrementalinha(2);

               if (Pmaterial<>'')
               then Begin
                          RDprint.ImpF(linhalocal,1,'CADASTRO: '+pmaterial,[negrito]);
                          IncrementaLinha(1);
               End;

               if (Pcodigomaterial<>'')
               then Begin

                          RDprint.ImpF(linhalocal,1,'PRODUTO: '+pnomematerialescolhido,[negrito]);
                          IncrementaLinha(1);
               End;

               if (Pcliente<>'')
               Then BEgin
                         RDprint.ImpF(linhalocal,1,'CLIENTE: '+pnomecliente,[negrito]);
                         IncrementaLinha(1);
               End;


               if (Pdatainicial<>'') or (Pdatafinal<>'')
               Then begin
                         RDprint.ImpF(linhalocal,1,'Per�odo de Entrega: '+Pdatainicial+' a '+pdatafinal,[negrito]);
                         IncrementaLinha(1);
               End;
               IncrementaLinha(1);
               //titulo das colunas
               RDprint.ImpF(linhalocal,1,CompletaPalavra('MATERIAL',12,' ')+' '+
                                         CompletaPalavra('REFERENCIA',10,' ')+' '+
                                         CompletaPalavra('PRODUTO',40,' ')+' '+
                                         CompletaPalavra('REF.COR',10,' ')+' '+
                                         CompletaPalavra('NOME COR',15,' ')+' '+
                                         CompletaPalavra_a_Esquerda('QUANTIDADE',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR M�DIO',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('TOTAL',12,' '),[negrito]);
               IncrementaLinha(1);
               Desenhalinha;

               psomaquantidade:=0;
               psomavalor:=0;

               while not(Self.ObjqueryTEMP.eof) do
               Begin

                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('material').asstring,12,' ')+' '+
                                              CompletaPalavra(fieldbyname('referenciamaterial').asstring,10,' ')+' '+
                                              CompletaPalavra(fieldbyname('codigomaterial').asstring,6,' ')+'-'+CompletaPalavra(fieldbyname('nomematerial').asstring,33,' ')+' '+
                                              CompletaPalavra(fieldbyname('referenciacor').asstring,10,' ')+' '+
                                              CompletaPalavra(fieldbyname('nomecor').asstring,15,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('quantidade').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('mediavalor').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('quantidade').asfloat*fieldbyname('mediavalor').asfloat),12,' '));
                    IncrementaLinha(1);

                    psomaquantidade:=psomaquantidade+Fieldbyname('quantidade').asfloat;
                    psomavalor:=psomavalor+(Fieldbyname('quantidade').asfloat*Fieldbyname('mediavalor').asfloat);

                    Self.ObjqueryTEMP.next;
               end;
               DesenhaLinha;
               VerificaLinha;
               RDprint.ImpF(linhalocal,1,completapalavra('TOTALIZA��O',92,' ')+
                                         CompletaPalavra_a_Esquerda(formata_valor(psomaquantidade),12,' ')+' '+
                                         CompletaPalavra_a_Esquerda(formata_valor(psomavalor),25,' '),[negrito]);

               rdprint.Fechar;
          End;
     End;


end;




procedure TObjPedidoObjetos.RecalculaPedido(pcodigo: string);
var
Querylocal:Tibquery;
QueryMateriais:Tibquery;
pporcentagemfinal,ppreco:currency;
begin

     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(pcodigo)=False)
     Then begin
               MensagemErro('Pedido '+Pcodigo+' n�o localizado');
               exit;
     End;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Concluido='S')
     Then Begin
               MensagemErro('N�o � poss�vel recalcular um pedido conclu�do, retorne-o primeiramente');
               exit;
     End;

     if (mensagempergunta('Certeza que deseja recalcular os valores desse pedido?')=mrno)
     then exit;

     Try
        Querylocal:=Tibquery.create(Nil);
        Querylocal.database:=FdataModulo.IBDatabase;
        QueryMateriais:=Tibquery.create(Nil);
        Querymateriais.database:=FdataModulo.IBDatabase;
     Except
        MensagemErro('Erro na tentativa de criar a Querylocal');
        exit;
     End;

     Try
        With Querylocal do
        begin
             close;
             sql.clear;
             sql.Add('Select codigo from tabpedido_proj where pedido='+pcodigo);
             sql.Add('order by codigo');
             open;

             last;
             FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
             FMostraBarraProgresso.Lbmensagem.caption:='Recalculando pedido';
             FMostraBarraProgresso.Lbmensagem2.caption:='';
             FMostraBarraProgresso.BarradeProgresso2.visible:=true;
             FMostraBarraProgresso.Lbmensagem2.visible:=true;
             first;

             open;

             While not(eof) do
             begin
                  FMostraBarraProgresso.Lbmensagem.caption:='Recalculando pedido projeto '+fieldbyname('codigo').asstring;
                  FMostraBarraProgresso.IncrementaBarra1(1);
                  FMostraBarraProgresso.show;
                  Application.ProcessMessages;



                  Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(fieldbyname('codigo').asstring);
                  Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

                  //****ferragem***
                  QueryMateriais.close;
                  QueryMateriais.SQL.clear;
                  QueryMateriais.SQL.add('Select codigo from tabferragem_PP where pedidoprojeto='+Fieldbyname('codigo').asstring);
                  QueryMateriais.open;
                  QueryMateriais.last;
                  FMostraBarraProgresso.BarradeProgresso2.MaxValue:=QueryMateriais.RecordCount;
                  FMostraBarraProgresso.BarradeProgresso2.MinValue:=0;
                  FMostraBarraProgresso.Lbmensagem2.caption:='Recalculando ferragem';
                  QueryMateriais.first;
                  While not (QueryMateriais.eof) do
                  begin
                        FMostraBarraProgresso.IncrementaBarra2(1);
                        FMostraBarraProgresso.show;
                        Application.ProcessMessages;

                        Self.ObjFerragem_PP.LocalizaCodigo(QueryMateriais.fieldbyname('codigo').asstring);
                        Self.ObjFerragem_PP.TabelaparaObjeto;

                        Try
                          pporcentagemfinal:=strtofloat(Self.ObjFerragem_PP.FerragemCor.Get_PorcentagemAcrescimoFinal);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
                          Then ppreco:=strtofloat(Self.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendaInstalado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
                          then ppreco:=strtofloat(Self.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendaRetirado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
                          then ppreco:=strtofloat(Self.ObjFerragem_PP.FerragemCor.Ferragem.Get_PrecoVendaRetirado);

                          Self.ObjFerragem_PP.Submit_Valor(floattostr(ppreco+((ppreco*pporcentagemfinal)/100)));
                          Self.ObjFerragem_PP.Status:=dsedit;
                          if (Self.ObjFerragem_PP.Salvar(False)=False)
                          then begin
                                    mensagemerro('erro na tentativa de atualizar o valor da ferragem');
                                    exit;
                          End;
                        Except
                              on e:exception do
                              begin
                                  mensagemerro('Erro na tentativa de atualizar o valor da ferragem'+#13+E.message);
                              End;
                        End;

                        QueryMateriais.Next;
                  End;
                  //****************

                  //****perfilado***
                  QueryMateriais.close;
                  QueryMateriais.SQL.clear;
                  QueryMateriais.SQL.add('Select codigo from tabperfilado_PP where pedidoprojeto='+Fieldbyname('codigo').asstring);
                  QueryMateriais.open;
                  QueryMateriais.last;
                  FMostraBarraProgresso.BarradeProgresso2.MaxValue:=QueryMateriais.RecordCount;
                  FMostraBarraProgresso.BarradeProgresso2.MinValue:=0;
                  FMostraBarraProgresso.Lbmensagem2.caption:='Recalculando perfilado';
                  QueryMateriais.first;
                  While not (QueryMateriais.eof) do
                  begin
                        FMostraBarraProgresso.IncrementaBarra2(1);
                        FMostraBarraProgresso.show;
                        Application.ProcessMessages;

                        Self.Objperfilado_PP.LocalizaCodigo(QueryMateriais.fieldbyname('codigo').asstring);
                        Self.Objperfilado_PP.TabelaparaObjeto;

                        Try
                          pporcentagemfinal:=strtofloat(Self.Objperfilado_PP.perfiladoCor.Get_PorcentagemAcrescimoFinal);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
                          Then ppreco:=strtofloat(Self.Objperfilado_PP.perfiladoCor.perfilado.Get_PrecoVendaInstalado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
                          then ppreco:=strtofloat(Self.Objperfilado_PP.perfiladoCor.perfilado.Get_PrecoVendaRetirado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
                          then ppreco:=strtofloat(Self.Objperfilado_PP.perfiladoCor.perfilado.Get_PrecoVendaRetirado);

                          Self.Objperfilado_PP.Submit_Valor(floattostr(ppreco+((ppreco*pporcentagemfinal)/100)));
                          Self.Objperfilado_PP.Status:=dsedit;
                          if (Self.Objperfilado_PP.Salvar(False)=False)
                          then begin
                                    mensagemerro('erro na tentativa de atualizar o valor do perfilado');
                                    exit;
                          End;
                        Except
                              on e:exception do
                              begin
                                  mensagemerro('Erro na tentativa de atualizar o valor do perfilado'+#13+E.message);
                              End;
                        End;

                        QueryMateriais.Next;
                  End;
                  //****************

                  //****vidro***
                  QueryMateriais.close;
                  QueryMateriais.SQL.clear;
                  QueryMateriais.SQL.add('Select codigo from tabvidro_PP where pedidoprojeto='+Fieldbyname('codigo').asstring);
                  QueryMateriais.open;

                  QueryMateriais.last;
                  FMostraBarraProgresso.BarradeProgresso2.MaxValue:=QueryMateriais.RecordCount;
                  FMostraBarraProgresso.BarradeProgresso2.MinValue:=0;
                  FMostraBarraProgresso.Lbmensagem2.caption:='Recalculando vidro';
                  QueryMateriais.first;
                  While not (QueryMateriais.eof) do
                  begin
                        FMostraBarraProgresso.IncrementaBarra2(1);
                        FMostraBarraProgresso.show;
                        Application.ProcessMessages;

                        Self.Objvidro_PP.LocalizaCodigo(QueryMateriais.fieldbyname('codigo').asstring);
                        Self.Objvidro_PP.TabelaparaObjeto;

                        Try
                          pporcentagemfinal:=strtofloat(Self.Objvidro_PP.vidroCor.Get_PorcentagemAcrescimoFinal);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
                          Then ppreco:=strtofloat(Self.Objvidro_PP.vidroCor.vidro.Get_PrecoVendaInstalado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
                          then ppreco:=strtofloat(Self.Objvidro_PP.vidroCor.vidro.Get_PrecoVendaRetirado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
                          then ppreco:=strtofloat(Self.Objvidro_PP.vidroCor.vidro.Get_PrecoVendaRetirado);

                          Self.Objvidro_PP.Submit_Valor(floattostr(ppreco+((ppreco*pporcentagemfinal)/100)));
                          Self.Objvidro_PP.Status:=dsedit;
                          if (Self.Objvidro_PP.Salvar(False)=False)
                          then begin
                                    mensagemerro('erro na tentativa de atualizar o valor do vidro');
                                    exit;
                          End;
                        Except
                              on e:exception do
                              begin
                                  mensagemerro('Erro na tentativa de atualizar o valor do vidro'+#13+E.message);
                              End;
                        End;

                        QueryMateriais.Next;
                  End;
                  //****************

                  //****kitbox***
                  QueryMateriais.close;
                  QueryMateriais.SQL.clear;
                  QueryMateriais.SQL.add('Select codigo from tabkitbox_PP where pedidoprojeto='+Fieldbyname('codigo').asstring);
                  QueryMateriais.open;

                  QueryMateriais.last;
                  FMostraBarraProgresso.BarradeProgresso2.MaxValue:=QueryMateriais.RecordCount;
                  FMostraBarraProgresso.BarradeProgresso2.MinValue:=0;
                  FMostraBarraProgresso.Lbmensagem2.caption:='Recalculando kitbox';
                  QueryMateriais.first;
                  While not (QueryMateriais.eof) do
                  begin
                        FMostraBarraProgresso.IncrementaBarra2(1);
                        FMostraBarraProgresso.show;
                        Application.ProcessMessages;

                        Self.Objkitbox_PP.LocalizaCodigo(QueryMateriais.fieldbyname('codigo').asstring);
                        Self.Objkitbox_PP.TabelaparaObjeto;

                        Try
                          pporcentagemfinal:=strtofloat(Self.Objkitbox_PP.kitboxCor.Get_PorcentagemAcrescimoFinal);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
                          Then ppreco:=strtofloat(Self.Objkitbox_PP.kitboxCor.kitbox.Get_PrecoVendaInstalado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
                          then ppreco:=strtofloat(Self.Objkitbox_PP.kitboxCor.kitbox.Get_PrecoVendaRetirado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
                          then ppreco:=strtofloat(Self.Objkitbox_PP.kitboxCor.kitbox.Get_PrecoVendaRetirado);

                          Self.Objkitbox_PP.Submit_Valor(floattostr(ppreco+((ppreco*pporcentagemfinal)/100)));
                          Self.Objkitbox_PP.Status:=dsedit;
                          if (Self.Objkitbox_PP.Salvar(False)=False)
                          then begin
                                    mensagemerro('erro na tentativa de atualizar o valor do kitbox');
                                    exit;
                          End;
                        Except
                              on e:exception do
                              begin
                                  mensagemerro('Erro na tentativa de atualizar o valor do kitbox'+#13+E.message);
                              End;
                        End;

                        QueryMateriais.Next;
                  End;
                  //****************

                  //****diversos***
                  QueryMateriais.close;
                  QueryMateriais.SQL.clear;
                  QueryMateriais.SQL.add('Select codigo from tabdiverso_PP where pedidoprojeto='+Fieldbyname('codigo').asstring);
                  QueryMateriais.open;

                  QueryMateriais.last;
                  FMostraBarraProgresso.BarradeProgresso2.MaxValue:=QueryMateriais.RecordCount;
                  FMostraBarraProgresso.BarradeProgresso2.MinValue:=0;
                  FMostraBarraProgresso.Lbmensagem2.caption:='Recalculando diversos';
                  QueryMateriais.first;
                  While not (QueryMateriais.eof) do
                  begin
                        FMostraBarraProgresso.IncrementaBarra2(1);
                        FMostraBarraProgresso.show;
                        Application.ProcessMessages;

                        Self.ObjDiverso_PP.LocalizaCodigo(QueryMateriais.fieldbyname('codigo').asstring);
                        Self.Objdiverso_PP.TabelaparaObjeto;

                        Try
                          pporcentagemfinal:=strtofloat(Self.Objdiverso_PP.diversoCor.Get_PorcentagemAcrescimoFinal);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
                          Then ppreco:=strtofloat(Self.Objdiverso_PP.diversoCor.diverso.Get_PrecoVendaInstalado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
                          then ppreco:=strtofloat(Self.Objdiverso_PP.diversoCor.diverso.Get_PrecoVendaRetirado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
                          then ppreco:=strtofloat(Self.Objdiverso_PP.diversoCor.diverso.Get_PrecoVendaRetirado);

                          Self.Objdiverso_PP.Submit_Valor(floattostr(ppreco+((ppreco*pporcentagemfinal)/100)));
                          Self.Objdiverso_PP.Status:=dsedit;
                          if (Self.Objdiverso_PP.Salvar(False)=False)
                          then begin
                                    mensagemerro('erro na tentativa de atualizar o valor do diverso');
                                    exit;
                          End;
                        Except
                              on e:exception do
                              begin
                                  mensagemerro('Erro na tentativa de atualizar o valor do diverso'+#13+E.message);
                              End;
                        End;

                        QueryMateriais.Next;
                  End;
                  //****************

                  //****persianas***
                  QueryMateriais.close;
                  QueryMateriais.SQL.clear;
                  QueryMateriais.SQL.add('Select codigo from tabpersiana_PP where pedidoprojeto='+Fieldbyname('codigo').asstring);
                  QueryMateriais.open;

                  QueryMateriais.last;
                  FMostraBarraProgresso.BarradeProgresso2.MaxValue:=QueryMateriais.RecordCount;
                  FMostraBarraProgresso.BarradeProgresso2.MinValue:=0;
                  FMostraBarraProgresso.Lbmensagem2.caption:='Recalculando persianas';
                  QueryMateriais.first;
                  While not (QueryMateriais.eof) do
                  begin
                        FMostraBarraProgresso.IncrementaBarra2(1);
                        FMostraBarraProgresso.show;
                        Application.ProcessMessages;

                        Self.Objpersiana_PP.LocalizaCodigo(QueryMateriais.fieldbyname('codigo').asstring);
                        Self.Objpersiana_PP.TabelaparaObjeto;

                        Try
                          pporcentagemfinal:=0;

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
                          Then ppreco:=strtofloat(Self.Objpersiana_PP.PersianaGrupoDiametroCor.Get_PrecoVendaInstalado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
                          then ppreco:=strtofloat(Self.Objpersiana_PP.PersianaGrupoDiametroCor.Get_PrecoVendaRetirado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
                          then ppreco:=strtofloat(Self.Objpersiana_PP.PersianaGrupoDiametroCor.Get_PrecoVendaRetirado);

                          Self.Objpersiana_PP.Submit_Valor(floattostr(ppreco+((ppreco*pporcentagemfinal)/100)));
                          Self.Objpersiana_PP.Status:=dsedit;
                          if (Self.Objpersiana_PP.Salvar(False)=False)
                          then begin
                                    mensagemerro('erro na tentativa de atualizar o valor da persiana');
                                    exit;
                          End;
                        Except
                              on e:exception do
                              begin
                                  mensagemerro('Erro na tentativa de atualizar o valor da persiana'+#13+E.message);
                              End;
                        End;

                        QueryMateriais.Next;
                  End;
                  //****************

                  //****servico***
                  QueryMateriais.close;
                  QueryMateriais.SQL.clear;
                  QueryMateriais.SQL.add('Select codigo from tabservico_PP where pedidoprojeto='+Fieldbyname('codigo').asstring);
                  QueryMateriais.open;

                  QueryMateriais.last;
                  FMostraBarraProgresso.BarradeProgresso2.MaxValue:=QueryMateriais.RecordCount;
                  FMostraBarraProgresso.BarradeProgresso2.MinValue:=0;
                  FMostraBarraProgresso.Lbmensagem2.caption:='Recalculando servi�os';
                  QueryMateriais.first;
                  While not (QueryMateriais.eof) do
                  begin
                        FMostraBarraProgresso.IncrementaBarra2(1);
                        FMostraBarraProgresso.show;
                        Application.ProcessMessages;

                        Self.Objservico_PP.LocalizaCodigo(QueryMateriais.fieldbyname('codigo').asstring);
                        Self.Objservico_PP.TabelaparaObjeto;

                        Try
                          pporcentagemfinal:=0;

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='INSTALADO')
                          Then ppreco:=strtofloat(Self.Objservico_PP.servico.Get_PrecoVendaInstalado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='RETIRADO')
                          then ppreco:=strtofloat(Self.Objservico_PP.servico.Get_PrecoVendaRetirado);

                          if (Self.ObjMedidas_Proj.PedidoProjeto.Get_TipoPedido='FORNECIDO')
                          then ppreco:=strtofloat(Self.Objservico_PP.servico.Get_PrecoVendaRetirado);

                          Self.Objservico_PP.Submit_Valor(floattostr(ppreco+((ppreco*pporcentagemfinal)/100)));
                          Self.Objservico_PP.Status:=dsedit;
                          if (Self.Objservico_PP.Salvar(False)=False)
                          then begin
                                    mensagemerro('erro na tentativa de atualizar o valor da servico');
                                    exit;
                          End;
                        Except
                              on e:exception do
                              begin
                                  mensagemerro('Erro na tentativa de atualizar o valor da servico'+#13+E.message);
                              End;
                        End;

                        QueryMateriais.Next;
                  End;
                  //****************

                  if (Self.AtualizavalorProjeto(fieldbyname('codigo').asstring,false)=False)
                  then begin
                            Mensagemerro('Erro na tentativa de atualizar o valor do pedido projeto '+fieldbyname('codigo').asstring);
                            exit;
                  End;


                  next;
             End;
             FDataModulo.IBTransaction.CommitRetaining;
             MensagemAviso('Conclu�do');

        End;

     Finally
            FMostraBarraProgresso.close;
            FDataModulo.IBTransaction.RollbackRetaining;
            freeandnil(querylocal);
            freeandnil(QueryMateriais);
     end;




end;

procedure TObjPedidoObjetos.Relatorio_pedidos_recebidos_entregues;
var
ObjCidade:tobjcidade;
pdatainicial,pdatafinal:tdate;
pvendedor:string;
pcidade:string;
begin
//sera usado para fazer o p�s-venda

    Try
       ObjCidade:=TobjCidade.create;
    Except
          mensagemerro('Erro na tentativa de criar o objeto de cidade');
          exit;
    End;

    Try

        With FfiltroImp do
        Begin
              DesativaGrupos;
              Grupo01.Enabled:=true;
              Grupo02.Enabled:=true;
              Grupo03.Enabled:=true;
              Grupo07.Enabled:=true;

              LbGrupo01.Caption:='Data inicial da Venda';
              LbGrupo02.Caption:='Data final da Venda';
              LbGrupo03.caption:='Vendedor';
              lbgrupo07.caption:='Cidade';

              edtgrupo01.EditMask:=MascaraData;
              edtgrupo02.EditMask:=MascaraData;
              edtgrupo03.OnKeyDown:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.EdtVendedorKeyDown;
              edtgrupo03.Color:=$005CADFE;

              Objcidade.ResgataCidade(ComboGrupo07);
              ComboGrupo07.itemindex:=0;
              edtgrupo07.visible:=False;


              showmodal;
              
              if(tag=0)
              then Exit;

              if (Validadata(1,pdatainicial,false)=False)
              then exit;

              if (Validadata(2,pdatafinal,false)=False)
              then exit;

              pvendedor:='';

              Try
                 if (edtgrupo03.text<>'')
                 then Begin
                           strtoint(edtgrupo03.text);
                           pvendedor:=edtgrupo03.text;
                           if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Vendedor.LocalizaCodigo(pvendedor)=False)
                           then Begin
                                     mensagemerro('Vendedor n�o localizado');
                                     exit;
                           End;
                 End;
              Except
                     mensagemerro('C�digo Inv�lido para o vendedor');
                     exit;
              End;

              pcidade:='';
              if (trim(ComboGrupo07.text)<>'')
              Then pcidade:=ComboGrupo07.text;
        End;
        
    Finally
           ObjCidade.free;
    End;

    With Self.Objquery do
    Begin
         close;
         sql.clear;
         sql.add('select tabpedido.codigo,tabpedido.data,tabvendedor.nome as nomevendedor,tabpedido.cliente,tabcliente.nome,tabcliente.fone,tabcliente.celular,tabcliente.cidade');
         sql.add('from tabpedido');
         sql.add('join proctitulosaldo on proctitulosaldo.titulo=tabpedido.titulo');
         sql.add('join tabcliente on tabpedido.cliente=tabcliente.codigo');
         sql.add('join tabvendedor on tabpedido.vendedor=tabvendedor.codigo');
         sql.add('where data>='+#39+formatdatetime('mm/dd/yyyy',pdatainicial)+#39);
         sql.add('and data<='+#39+formatdatetime('mm/dd/yyyy',pdatafinal)+#39);
         sql.add('and proctitulosaldo.saldo=0');

         if (pvendedor<>'')
         then sql.add('and tabpedido.vendedor='+pvendedor);

         if (pcidade<>'')
         then sql.add('and tabcliente.cidade like '+#39+pcidade+'%'+#39);
         
         sql.add('order by tabcliente.cidade,tabpedido.vendedor,tabpedido.data');

         open;

         if (recordcount=0)
         then Begin
                   MensagemAviso('Nenhuma informa��o foi selecionada');
                   exit;
         End;

         With FreltxtRDPRINT do
         Begin
              ConfiguraImpressao;
              RDprint.FonteTamanhoPadrao:=S17cpp;
              RDprint.TamanhoQteColunas:=130;

              linhalocal:=3;
              rdprint.Abrir;
              if (RDprint.Setup=False)
              Then Begin
                        RDprint.Fechar;
                        exit;
              End;

              RDprint.ImpC(linhalocal,65,'RELAT�RIO DE P�S-VENDA - '+datetostr(pdatainicial)+' a '+datetostr(Pdatafinal),[negrito]);
              IncrementaLinha(2);

              if (Pvendedor<>'')
              then Begin
                        rdprint.impf(linhalocal,1,'VENDEDOR: '+fieldbyname('nomevendedor').asstring,[negrito]);
                        incrementalinha(1);
              End;

              if (Pcidade<>'')
              then Begin
                        rdprint.impf(linhalocal,1,'CIDADE  :  '+pcidade,[negrito]);
                        incrementalinha(1);
              End;

              incrementalinha(1);
              rdprint.ImpF(linhalocal,1,CompletaPalavra('PEDIDO',9,' ')+' '+
                                        CompletaPalavra('DATA',10,' ')+' '+
                                        CompletaPalavra('VENDEDOR',20,' ')+' '+
                                        CompletaPalavra('CLIENTE',40,' ')+' '+
                                        CompletaPalavra('TELEFONE',25,' ')+' '+
                                        CompletaPalavra('CIDADE',15,' '),[negrito]);
              IncrementaLinha(1);
              DesenhaLinha;

              Self.Objquery.last;
              FMostraBarraProgresso.ConfiguracoesIniciais(Self.objquery.recordcount,0);
              FMostraBarraProgresso.Lbmensagem.caption:='Gerando relat�rio';
              Self.Objquery.first;


              While not (Self.Objquery.eof) do
              begin
                    FMostraBarraProgresso.IncrementaBarra1(1);
                    FMostraBarraProgresso.Show;
                    Application.ProcessMessages;

                    //antes de imprimir verifico se todas pe�as foram entregues
                    Self.ObjqueryTEMP.close;
                    Self.ObjqueryTEMP.sql.clear;
                    Self.ObjqueryTEMP.sql.add('Select tabpedido_proj.codigo,tabpedidoprojetoromaneio.codigo from tabpedido_proj');
                    Self.ObjqueryTEMP.sql.add('left join tabpedidoprojetoromaneio on tabpedido_proj.codigo=tabpedidoprojetoromaneio.pedidoprojeto');
                    Self.ObjqueryTEMP.sql.add('where tabpedidoprojetoromaneio.codigo is null');
                    Self.ObjqueryTEMP.sql.add('and tabpedido_proj.pedido='+Fieldbyname('codigo').asstring);
                    Self.ObjqueryTEMP.open;
                    Self.ObjqueryTEMP.last;
                    if (Self.ObjqueryTEMP.recordcount=0)
                    Then Begin
                              Self.ObjqueryTEMP.close;
                              VerificaLinha;
                              RDprint.imp(linhalocal,1,CompletaPalavra(fieldbyname('codigo').asstring,9,' ')+' '+
                                        CompletaPalavra(fieldbyname('data').asstring,10,' ')+' '+
                                        CompletaPalavra(fieldbyname('nomevendedor').asstring,20,' ')+' '+
                                        CompletaPalavra(fieldbyname('cliente').asstring,5,' ')+'-'+completapalavra(fieldbyname('nome').asstring,34,' ')+' '+
                                        CompletaPalavra(fieldbyname('fone').asstring+' / '+fieldbyname('celular').asstring,25,' ')+' '+
                                        CompletaPalavra(fieldbyname('cidade').asstring,15,' '));
                              IncrementaLinha(1);
                    End;
                    Self.Objquery.Next;
              End;
              desenhalinha;
              FMostraBarraProgresso.close;
              rdprint.Fechar;
         End;
    End;
end;


procedure TObjPedidoObjetos.ImprimePedidoVidroLote(Ppedido: string);
var
preferenciacor,preferenciainicial:String;
pcontareferencia,cont:integer;
psubtotal:currency;
linhadata1,linhadata2:string;
psomaquantidade:integer;
begin

     if (ppedido='')
     Then begin
               mensagemaviso('Escolha o pedido que deseja imprimir');
               exit;
     End;

     if (self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(ppedido)=False)
     Then Begin
               Mensagemerro('Pedido n�o encontrado');
               exit;
     End;
     self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     With Self.ObjqueryTEMP do
     Begin
          close;
          sql.clear;
          SQL.Clear;
          Sql.add('Select PMPP.material,PMPP.referenciamaterial,PMPP.nomematerial,');
          Sql.add('PMPP.referenciacor,PMPP.nomecor,PMPP.altura,PMPP.largura,PMPP.quantidade,PMPP.complemento,count(PMPP.material) as CONTAQUANT,sum(PMPP.valorfinal) as VALORFINAL');
          Sql.add('from PROC_MAT_SERV_PP_PEDIDO('+ppedido+') PMPP');
          Sql.add('group by PMPP.material,PMPP.referenciamaterial,PMPP.nomematerial,');
          Sql.add('PMPP.referenciacor,PMPP.nomecor,PMPP.altura,PMPP.largura,PMPP.quantidade,PMPP.complemento');
          Sql.add('order by PMPP.material desc ,PMPP.referenciamaterial,PMPP.referenciacor');
          //Sql.SaveToFile('d:\relsimples.sql');
         // inputbox('','',sql.Text);
          open;
          if (recordcount=0)
          then Begin
                    mensagemaviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;
          With FRelPedidoRdPrint do
          Begin
               ConfiguraImpressao;
               RDprint1.FonteTamanhoPadrao:=s17cpp;
               RDprint1.TamanhoQteColunas:=130;
               imprimecabecalho:=true;
               linhalocal:=3;
               RDprint1.abrir;
               if (RDprint1.Setup=False)
               Then Begin
                         RDprint1.fechar;
                         exit;
               End;

               VerificaLinha;
               rdprint1.impc(linhalocal,65,'PEDIDO N� '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.get_codigo,[negrito]);
               IncrementaLinha(1);


               VerificaLinha;
               rdprint1.impc(linhalocal,1,completapalavra('Data: '+formatdatetime('dd mmmm yyyy',strtodate(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data)),98,' ')+' PRODUZIDO: ______/______/_____',[negrito]);
               IncrementaLinha(1);

               VerificaLinha;
               rdprint1.impf(linhalocal,1,'Cliente: '+completapalavra(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome,50,' ')+' TEL: '+CompletaPalavra(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Fone,25,' '),[negrito]);
               IncrementaLinha(1);

               VerificaLinha;
               rdprint1.impf(linhalocal,1,completapalavra('Endere�o: '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Endereco,59,' ')+' N�'+completapalavra(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Numero,4,' ')+' '+CompletaPalavra(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Bairro,38,' ')+' INSTALADO ____/____/____',[negrito]);
               IncrementaLinha(1);

               VerificaLinha;
               rdprint1.impf(linhalocal,1,completapalavra('Arquiteto: '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Arquiteto.Get_nome,69,' '),[negrito]);
               IncrementaLinha(1);

               VerificaLinha;
               rdprint1.impf(linhalocal,1,completapalavra('Local da Obra: '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Obra,65,' '),[negrito]);
               IncrementaLinha(2);


               VerificaLinha;
               rdprint1.impf(linhalocal,1,CompletaPalavra('DESCRICAO',40,' ')+'|'+
                                          CompletaPalavra_a_Esquerda('QTDE',10,' ')+'|'+
                                          CompletaPalavra('ALTURA',16,' ')+'X'+
                                          CompletaPalavra_a_Esquerda('LARGURA',16,' ')+'|'+
                                          CompletaPalavra_a_Esquerda('M2/QTDE',16,' ')+'|'+
                                         { CompletaPalavra_a_Esquerda('$TOTAL',12,' ')+'|'+  }
                                          CompletaPalavra('OBSERVACAO',14,' '),[negrito,sublinhado]);

               IncrementaLinha(1);

               preferenciainicial:=fieldbyname('referenciamaterial').asstring;
               preferenciacor:=fieldbyname('referenciacor').asstring;
               pcontareferencia:=0;
               psubtotal:=0;
               psomaquantidade:=0;

               While not(Self.ObjqueryTEMP.eof) do
               Begin
                    if (preferenciainicial<>fieldbyname('referenciamaterial').asstring)
                    or (preferenciacor<>fieldbyname('referenciacor').asstring)
                    Then Begin
                              if (Pcontareferencia>1)
                              then Begin
                                        //subtotaliza
                                        VerificaLinha;
                                        rdprint1.impf(linhalocal,1,CompletaPalavra_a_Esquerda('SUB-TOTAL',89,' ')+' '+
                                                                   CompletaPalavra_a_Esquerda(formata_valor(PsubTotal),12,' '),[negrito]);
                                        linhalocal:=linhalocal+1;
                              End;

                              IncrementaLinha(1);
                              preferenciainicial:=fieldbyname('referenciamaterial').asstring;
                              preferenciacor:=fieldbyname('referenciacor').asstring;
                              pcontareferencia:=0;
                              psubtotal:=0;
                    End;

                    linhadata1:='';



                    VerificaLinha;
                    rdprint1.impf(linhalocal,1,CompletaPalavra(fieldbyname('nomematerial').asstring+' '+fieldbyname('nomecor').asstring,40,' ')+'|'+
                                          CompletaPalavra_a_Esquerda(fieldbyname('CONTAQUANT').asstring,10,' ')+'|'+
                                          CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('altura').asinteger),16,' ')+'| '+
                                          CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('largura').asinteger),15,' ')+'|'+
                                          CompletaPalavra_a_Esquerda(formata_Valor((fieldbyname('quantidade').ascurrency*fieldbyname('CONTAQUANT').asinteger)),16,' ')+'|'+
                                          {CompletaPalavra_a_Esquerda(formata_Valor(fieldbyname('valorfinal').asstring),12,' ')+'|'+  }
                                          CompletaPalavra(fieldbyname('complemento').asstring,14,' '),[sublinhado]);
                    linhalocal:=linhalocal+1;

                    psubtotal:=psubtotal+fieldbyname('quantidade').ascurrency;
                    //12/02 Alterado
                    psubtotal:=psubtotal+(fieldbyname('quantidade').asfloat*fieldbyname('CONTAQUANT').asinteger);

                    pcontareferencia:=pcontareferencia+1;
                    psomaquantidade:=psomaquantidade+fieldbyname('CONTAQUANT').asinteger;
                    Self.ObjqueryTEMP.next;
               End;

               //subtotaliza
               if (Pcontareferencia>1)
               then Begin
                          VerificaLinha;
                          rdprint1.impf(linhalocal,1,CompletaPalavra_a_Esquerda('SUB-TOTAL',89,' ')+' '+
                                                     CompletaPalavra_a_Esquerda(formata_valor(PsubTotal),12,' '),[negrito]);
                          linhalocal:=linhalocal+1;
               End;



               linhalocal:=linhalocal+1;
               verificalinha;
               rdprint1.impf(linhalocal,1,CompletaPalavra('TOTAL ',41,' ')+CompletaPalavra_a_Esquerda(inttostr(psomaquantidade),10,' ')+completapalavra('',51,' ')+' '
                                           {+CompletaPalavra_a_Esquerda(formata_Valor(Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorFinal),12,' ')},[negrito]);
               incrementalinha(1);
               desenhalinha;
               //**************************************************************
               //impressao da observacao do pedido
               linhadata1:=Self.ObjMedidas_Proj.PedidoProjeto.pedido.get_observacao;

               if (linhadata1<>'')
               Then Begin
                       linhadata2:='Observa��es: ';
                       for cont:=1 to length(linhadata1) do
                       Begin

                            if (linhadata1[cont]<>#10)
                            Then
                                if (linhadata1[cont]=#13)
                                Then begin
                                          verificalinha;
                                          rdprint1.Imp(linhalocal,1,linhadata2);
                                          INC(linhalocal,1);
                                          linhadata2:='';
                                End
                                Else Begin
                                          linhadata2:=linhadata2+linhadata1[cont];
                                          if ((cont mod RDprint1.TamanhoQteColunas)=0)
                                          Then Begin
                                                    verificalinha;
                                                    rdprint1.Imp(linhalocal,1,linhadata2);
                                                    INC(linhalocal,1);
                                                    linhadata2:='';
                                          End;
                                End;
                       End;

                       if(linhadata2<>'')
                       Then Begin
                                 verificalinha;
                                 rdprint1.Imp(linhalocal,1,linhadata2);
                                 INC(linhalocal,1);
                                 linhadata2:='';
                       End;
               End;
               //**************************************************************
               RDprint1.fechar;
          End;
     End;


end;


procedure TObjPedidoObjetos.ImprimeComponentesProjetos_do_pedido(
  ppedido: string);
var
  Altura,Largura:Currency;
  FolgaAltura,FolgaLargura:Currency;

begin
     if (ppedido='')
     Then Begin
               mensagemaviso('Escolha o pedido que deseja imprimir os componentes');
               exit;
     End;

     if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(ppedido)=False)
     Then Begin
               mensagemerro('Pedido n�o localizado');
               exit;
     End;
     Self.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     With Self.Objquery do
     begin
          close;
          SQL.clear;
          sql.add('Select codigo from tabpedido_proj where pedido='+ppedido+' order by codigo');
          open;
          last;
          if (recordcount=0)
          Then Begin
                    mensagemerro('N�o foi encontrado nenhum projeto nesse pedido');
                    exit;
          End;
          first;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               LinhaLocal:=3;
               rdprint.abrir;
               if (rdprint.setup=False)
               Then Begin
                         rdprint.fechar;
                         exit;
               End;
               rdprint.impc(linhalocal,45,'COMPONENTES DOS PROJETOS DO PEDIDO '+ppedido,[negrito]);
               IncrementaLinha(2);
               VerificaLinha;
               RDprint.impf(linhalocal,1,CompletaPalavra('COMPONENTE',50,' ')+' '+
                                        CompletaPalavra_a_Esquerda('ALTURA',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('LARGURA',12,' '),[negrito]);
               IncrementaLinha(1);

               While not(Self.objquery.eof) do
               Begin
                    Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(Self.Objquery.fieldbyname('codigo').asstring);
                    Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

                    desenhalinha;
                    IncrementaLinha(1);
                    rdprint.impf(linhalocal,1,'PROJETO: '+Self.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Descricao,[negrito]);
                    IncrementaLinha(2);

                    Self.ObjqueryTEMP.close;
                    Self.ObjqueryTEMP.sql.clear;
                    Self.ObjqueryTEMP.sql.add('Select distinct(TCPP.componente),TCPP.largura,TCPP.altura,Tabcomponente.DESCRICAO,tp.folgaaltura,tp.folgalargura ');
                    self.ObjqueryTEMP.SQL.Add('from tabcomponente_pp TCPP');
                    Self.ObjqueryTEMP.sql.add('join tabcomponente on TCPP.componente=tabcomponente.codigo');
                    self.ObjqueryTEMP.sql.Add('join tabcomponente_proj tp on tp.componente=TCPP.componente');
                    Self.ObjqueryTEMP.sql.Add('join tabpedido_proj on tabpedido_proj.codigo=TCPP.pedidoprojeto');
                    Self.ObjqueryTEMP.sql.add('where TCPP.pedidoprojeto='+Self.ObjMedidas_Proj.PedidoProjeto.get_codigo);
                    self.ObjqueryTEMP.sql.Add('and tabpedido_proj.projeto=tp.projeto');
                    Self.ObjqueryTEMP.open;


                    //Se houver folgas, calcula as folgas nas pe�as de vidros...




                    while not(Self.ObjqueryTEMP.eof) do
                    Begin
                         FolgaAltura:= Self.ObjqueryTEMP.fieldbyname('folgaaltura').AsCurrency ;
                         FolgaLargura:= Self.ObjqueryTEMP.fieldbyname('folgalargura').AsCurrency;

                         if(ObjParametroGlobal.ValidaParametro('CALCULAR A FOLGA NOS COMPONENTES')=False) then
                         begin
                               MensagemErro('O parametro "CALCULAR A FOLGA NOS COMPONENTES" n�o foi encontrado');

                         end;

                         if(ObjParametroGlobal.Get_Valor='SIM') THEN
                         begin
                                Altura:=Self.ObjqueryTEMP.fieldbyname('altura').AsCurrency - FolgaAltura ;
                                Largura:=Self.ObjqueryTEMP.fieldbyname('largura').AsCurrency - FolgaLargura;
                         end
                         else
                         begin
                                Altura:=Self.ObjqueryTEMP.fieldbyname('altura').AsCurrency;
                                Largura:=Self.ObjqueryTEMP.fieldbyname('largura').AsCurrency;
                         end;
                         
                         VerificaLinha;
                         RDprint.imp(linhalocal,1,CompletaPalavra(Self.ObjqueryTEMP.fieldbyname('descricao').asstring,50,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(CurrToStr(Altura),12,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(CurrToStr(Largura),12,' '));
                         IncrementaLinha(1);

                         Self.ObjqueryTEMP.next;
                    End;
                    Self.objquery.next;
               End;
               desenhalinha;
               rdprint.fechar;
          End;
     End;

end;


procedure TObjPedidoObjetos.MostraComissoesColocador(ppedido: string);
var
   FpesquisaLocal:TFpesquisa;
   Ptemp:TStringList;
begin
     if (Ppedido='')
     Then Begin
               mensagemAviso('Escolha um pedido');
               exit;
     End;


     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSAR COMISSOES DOS COLOCADORES NO FORMULARIO DE PEDIDO')=False)
     then exit;

     try
        Ptemp:=TStringList.create;
        Fpesquisalocal:=Tfpesquisa.create(nil);
     Except
           on e:exception do
           begin
                MensagemErro(e.message);
                exit;
           End;
     End;


     Try
        ptemp.clear;
        ptemp.add('Select TABCOMISSAOCOLOCADOR.* from');
        ptemp.add('TABCOMISSAOCOLOCADOR');
        ptemp.add('join tabpedido_proj on tabcomissaocolocador.pedidoprojeto=tabpedido_proj.codigo');
        ptemp.add('where tabpedido_proj.pedido='+Ppedido);

        If (FpesquisaLocal.PreparaPesquisa(ptemp,'Pesquisa de Comiss�o do Colocador. Pedido '+ppedido,Nil)=True)
        Then Begin
                  FpesquisaLocal.showmodal;
        End;
     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(ptemp);
     End;
end;



Procedure Tobjpedidoobjetos.refaz_base_calculo_comissao_colocador;
var
ObjComissaoColocador:Tobjcomissaocolocador;
begin
     (*A pedido da Camila 04/11/08
     Fazer um m�dulo para reprocessar as comiss�es de colocadores
     que ainda n�o tenham sido pagas, recalculando a nova
     base de c�lculo com base na forma de c�lculo Nova

04/11/2008  16:12:10  Ronnei  Camila  Camila
04/11/2008  16:12:18  Ronnei  Camila  No M�dulo de reprocessamento das comiss�es
04/11/2008  16:12:39  Ronnei  Camila  � para reprocessar apenas os pedidos projetos que ainda n�o foram pago a comiss�o correto
04/11/2008  16:12:40  Ronnei  Camila  ?
04/11/2008  16:14:06  Camila  Ronnei  sim
04/11/2008  16:14:24  Ronnei  Camila  mesmo que em um pedido 1 PP tenha sido pago e outro esteja em aberto, reprocesso s� esse em aberto?
04/11/2008  16:14:39  Camila  Ronnei  sim
04/11/2008  16:14:49  Ronnei  Camila  Ok ent�o obrigado
     *)

     With Self.ObjqueryTEMP do
     Begin
          close;
          sql.clear;
          sql.add('Select tabcomissaocolocador.*,');
          sql.add('tabpedido_proj.pedido,');
          sql.add('TABCOMISSAO_ADIANT_FUNCFOLHA.codigo as PAGTO');
          sql.add('from tabcomissaocolocador');
          sql.add('left join');
          sql.add('TABCOMISSAO_ADIANT_FUNCFOLHA');
          sql.add('on TABCOMISSAO_ADIANT_FUNCFOLHA.comissaocolocador =tabcomissaocolocador.codigo');
          sql.add('join tabpedido_proj on tabcomissaocolocador.pedidoprojeto=tabpedido_proj.codigo');
          sql.add('where TABCOMISSAO_ADIANT_FUNCFOLHA.codigo is null');
          sql.add('order by Tabpedido_proj.pedido');
          open;
          last;
          if (recordcount=0)
          then Begin
                    mensagemAviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.Lbmensagem.caption:='Reprocessando Comiss�es';
          first;

          try
           ObjComissaoColocador:=TobjComissaoColocador.Create;
        Except
              FDataModulo.IBTransaction.Rollback;
              Messagedlg('Erro na tentativa de Criar o Objeto de Comiss�o do Colocador',mterror,[mbok],0);
              exit;
        End;

        Try
           ObjComissaoColocador:=Tobjcomissaocolocador.create;
        Except
              on e:exception do
              begin
                  mensagemerro(e.message);
                  exit;
              end;
        End;

        Try

            While not(eof) do
            Begin
                 FMostraBarraProgresso.IncrementaBarra1(1);
                 FMostraBarraProgresso.show;
                 Application.ProcessMessages;

                 if (ObjComissaoColocador.RecalculaComissao(fieldbyname('codigo').asstring)=false)
                 Then Begin
                          Messagedlg('Erro no rec�lculo da comiss�o. C�digo da Comiss�o : '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                          exit;
                 End;

                 next;
            End;

            FDataModulo.IBTransaction.CommitRetaining;
            mensagemaviso('Conclu�do');

          Finally
                 FDataModulo.IBTransaction.RollbackRetaining;
                 ObjComissaoColocador.Free;
                 FMostraBarraProgresso.close;
          End;
     End;
End;


procedure TObjPedidoObjetos.Imprime_Relatorio_materiais_por_pedido_e_cliente_sintetico;
var
Pdata1,Pdata2:string;
Pcliente:string;
cont:integer;
psomafinal,psomamaterial:currency;
QuantidadeRel,ValorRel,ValorFinalRel:Currency;
CodigoMaterial:Integer;
Material,nomematerial,nomecor:string;
begin
     {
           Este relatorio foi pedido pela vidrex, com o intuito de de agrupar os materiais iguais
           o Relatorio 9, mostra todos os materiais por pedido e cliente, porem n�o agrupa materiais
           iguais, logo, este agrupa os materiais e totaliza o valor, valorfinal e a sua quantidade.

           Jonatan Medina
           16/02/2012

     }
     if (ObjPermissoesUsoGlobal.ValidaPermissao('RELAT�RIO DE VENDAS POR MATERIAL PEDIDO E CLIENTE')=False)
     Then exit;


     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.caption:='Data Inicial';
          edtgrupo01.EditMask:=MascaraData;

          Grupo02.Enabled:=true;
          LbGrupo02.caption:='Data Final';
          edtgrupo02.EditMask:=MascaraData;

          Grupo03.Enabled:=True;
          LbGrupo03.caption:='Cliente';
          edtgrupo03.OnKeyDown:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.EdtClienteKeyDown;
          edtgrupo03.OnDblClick:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.EdtClienteDblClick;  {Rodolfo}
          edtgrupo03.Color:=$005CADFE;

          Showmodal;

          if (tag=0)
          then exit;

          Pdata1:='';
          Pdata2:='';
          Pcliente:='';

          Try
             if (trim(comebarra(edtgrupo01.text))<>'')
             Then Begin
                       strtodate(edtgrupo01.text);
                       pdata1:=edtgrupo01.text;
             End;
          Except
                Messagedlg('Data Inicial inv�lida',mterror,[mbok],0);
                exit;
          End;

          Try
             if (trim(comebarra(edtgrupo02.text))<>'')
             Then Begin
                       strtodate(edtgrupo02.text);
                       pdata2:=edtgrupo02.text;
             End;
          Except
                Messagedlg('Data Final inv�lida',mterror,[mbok],0);
                exit;
          End;

          Try
             if (trim(edtgrupo03.text)<>'')
             Then Begin
                       strtoint(edtgrupo03.text);
                       Pcliente:=edtgrupo03.text;

                       if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.LocalizaCodigo(pcliente)=False)
                       then Begin
                                 Messagedlg('Cliente n�o encontrado',mterror,[mbok],0);
                                 exit;
                       End;
                       Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.TabelaparaObjeto;
             End;
          Except
                Messagedlg('Cliente inv�lido',mterror,[mbok],0);
                exit;
          End;
     End;

     With Self.Objquery do
     Begin

          {seleciono os pedidos do periodo e uso o}


          close;
          sql.clear;
          sql.add('Select tabpedido.Codigo,tabpedido.obra,tabpedido.data,tabpedido.valortotal,tabpedido.ValorBonificacaoCliente,');
          sql.add('tabpedido.valordesconto,tabpedido.valorfinal,tabpedido.vendedor,Tabvendedor.Nome AS nomevendedor,');
          sql.add('tabpedido.cliente,Tabcliente.Nome AS NOMECLIENTE,tabcliente.cidade from tabpedido');
          sql.add('join tabvendedor on Tabpedido.Vendedor=Tabvendedor.codigo');
          sql.add('join tabCliente on Tabpedido.Cliente=Tabcliente.codigo');
          Sql.add('Where TabPedido.codigo<>-500');
          Sql.add('and Tabpedido.Concluido=''S'' ');


          if(Pdata1<>'')
          Then sql.add('and TabPedido.Data>='+#39+Formatdatetime('mm/dd/yyyy',Strtodate(pdata1))+#39);

          if(Pdata2<>'')
          Then sql.add('and TabPedido.Data<='+#39+Formatdatetime('mm/dd/yyyy',Strtodate(pdata2))+#39);

          if(PCliente<>'')
          Then sql.add('and TabPedido.Cliente='+pcliente);

          sql.add('order by Tabpedido.data');
          open;
          last;
          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma informa��o foi selecionada',mterror,[mbok],0);
                    exit;
          End;
          first;
          //Selecionando os pedidos que irao para o relat�rio

          FmostraStringGrid.LbMensagem.caption:='F5 seleciona todos | F6 retira sele��o | ENTER ou Duplo clique seleciona linha atual';
          FmostraStringGrid.lbNomeTela.Caption:='Escolhas os pedidos para visualizar os materiais';
          FmostraStringGrid.lbrodape.caption:='';
          FmostraStringGrid.StringGrid.OnDblClick:=Self.StringGridDblClick;
          FmostraStringGrid.StringGrid.OnKeyPress:=Self.StringGridKeyPress;
          FmostraStringGrid.StringGrid.OnKeyDown:=Self.StringGridKeyDown;
          FmostraStringGrid.StringGrid.RowCount:=recordcount+1;
          FmostraStringGrid.StringGrid.ColCount:=8;
          FmostraStringGrid.StringGrid.FixedRows:=1;
          FmostraStringGrid.StringGrid.FixedCols:=0;
          FmostraStringGrid.StringGrid.ColWidths[0] := 50;

          cont:=0;

          FmostraStringGrid.StringGrid.cells[0,cont]:='';
          FmostraStringGrid.StringGrid.cells[1,cont]:='PEDIDO';
          FmostraStringGrid.StringGrid.cells[2,cont]:='DATA';
          FmostraStringGrid.StringGrid.cells[3,cont]:='VALOR TOTAL';
          FmostraStringGrid.StringGrid.cells[4,cont]:='DESCONTO';
          FmostraStringGrid.StringGrid.cells[5,cont]:='BONIFICA��O';
          FmostraStringGrid.StringGrid.cells[6,cont]:='VALOR FINAL';
          FmostraStringGrid.StringGrid.cells[7,cont]:='OBRA';

          cont:=1;
          While not(eof)
          do Begin
                  FmostraStringGrid.StringGrid.cells[0,cont]:='';
                  FmostraStringGrid.StringGrid.cells[1,cont]:=fieldbyname('codigo').asstring;
                  FmostraStringGrid.StringGrid.cells[2,cont]:=fieldbyname('data').asstring;
                  FmostraStringGrid.StringGrid.cells[3,cont]:=fieldbyname('valortotal').asstring;
                  FmostraStringGrid.StringGrid.cells[4,cont]:=fieldbyname('valordesconto').asstring;
                  FmostraStringGrid.StringGrid.cells[5,cont]:=fieldbyname('ValorBonificacaoCliente').asstring;
                  FmostraStringGrid.StringGrid.cells[6,cont]:=fieldbyname('valorfinal').asstring;
                  FmostraStringGrid.StringGrid.cells[7,cont]:=fieldbyname('obra').asstring;
                  inc(cont,1);
                  next;
          End;
          AjustaLArguraColunaGrid(FmostraStringGrid.StringGrid);

          FmostraStringGrid.PanelRodape.Visible:=True;
          //FmostraStringGrid.lbmensagem.caption:='';
          FmostraStringGrid.Showmodal;

          if (FmostraStringGrid.Tag=0)
          then exit;


          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               RDprint.TamanhoQteColunas:=130;
               RDprint.FonteTamanhoPadrao:=S17cpp;

               LinhaLocal:=3;
               RDprint.Abrir;
               if (RDprint.Setup=False)
               Then Begin
                         RDprint.fechar;
                         exit;
               End;

            Try
               RDprint.ImpC(linhalocal,65,'VENDAS POR MATERIAL E PEDIDO',[negrito]);
               IncrementaLinha(2);

               if (pcliente<>'')
               then Begin
                         RDprint.ImpF(LinhaLocal,1,'Cliente  '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome,[negrito]);
                         IncrementaLinha(1);
               End;

               if (Pdata1<>'')
               or (pdata2<>'')
               then Begin
                         RDprint.ImpF(LinhaLocal,1,'Per�odo: '+Pdata1+' a '+Pdata2,[negrito]);
                         IncrementaLinha(1);
               End;
               IncrementaLinha(1);



               FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
               FMostraBarraProgresso.lbmensagem.caption:='Gerando Relat�rio';
               FMostraBarraProgresso.btcancelar.Visible:=True;
               first;
               cont:=1;
               psomafinal:=0;

               for cont:=1 to FmostraStringGrid.StringGrid.RowCount-1 do
               Begin
                     FMostraBarraProgresso.IncrementaBarra1(1);
                     FMostraBarraProgresso.Show;
                     Application.ProcessMessages;

                     if (FMostraBarraProgresso.Cancelar)
                     Then Begin
                          FMostraBarraProgresso.Cancelar:=False;
                          if (Messagedlg('Tem certeza que deseja cancelar a gera��o do relat�rio?',mtConfirmation,[mbyes,mbno],0)=mryes)
                          Then Begin
                               RDprint.Abortar;
                               exit;  
                          End;
                     End;

                     if (FmostraStringGrid.StringGrid.cells[0,cont]='       X')
                     Then begin

                              VerificaLinha;
                              RDprint.Impf(LinhaLocal,1,'Pedido: '+FmostraStringGrid.StringGrid.Cells[1,cont]+' '+
                                                        'Data: '+FmostraStringGrid.StringGrid.Cells[2,cont]+' '+
                                                        'Valor Total: '+formata_valor(FmostraStringGrid.StringGrid.Cells[3,cont])+' '+
                                                        'Desconto: '+formata_valor(FmostraStringGrid.StringGrid.Cells[4,cont])+' '+
                                                        'Bonifica��o: '+formata_valor(FmostraStringGrid.StringGrid.Cells[5,cont])+' '+
                                                        'Valor Final: '+formata_valor(FmostraStringGrid.StringGrid.Cells[6,cont]),[negrito]);
                              IncrementaLinha(2);

                              //selecionando os materiais
                              Self.ObjqueryTEMP.close;
                              Self.ObjqueryTEMP.sql.clear;
                              Self.ObjqueryTEMP.sql.add('select * from PROC_MAT_SERV_PP_PEDIDO('+FmostraStringGrid.StringGrid.Cells[1,cont]+')');
                              Self.ObjqueryTEMP.sql.add('order by material');
                              Self.ObjqueryTEMP.open;
                              Self.ObjqueryTEMP.first;

                              VerificaLinha;
                              RDprint.Impf(LinhaLocal,1,CompletaPalavra('MATERIAL',20,' ')+' '+
                                                            CompletaPalavra('NOME',45,' ')+' '+
                                                            CompletaPalavra('COR',20,' ')+' '+
                                                            CompletaPalavra_a_Esquerda('QUANTIDADE',12,' ')+' '+
                                                            CompletaPalavra_a_Esquerda('VALOR',12,' ')+' '+
                                                            CompletaPalavra_a_Esquerda('VALOR FINAL',12,' '),[negrito]);

                              IncrementaLinha(1);
                              psomamaterial:=0;
                              ValorRel:=0;
                              ValorFinalRel:=0;
                              QuantidadeRel:=0;
                              CodigoMaterial:=self.ObjqueryTEMP.FieldByName('codigomaterial').AsInteger;
                              While not(Self.ObjqueryTEMP.eof) do
                              Begin

                                   if(CodigoMaterial<>self.ObjqueryTEMP.FieldByName('codigomaterial').AsInteger) then
                                   begin

                                          VerificaLinha;
                                          ValorRel:=ValorFinalRel/QuantidadeRel;
                                          RDprint.Imp(LinhaLocal,1,CompletaPalavra(Material,20,' ')+' '+
                                                                  CompletaPalavra(nomematerial,45,' ')+' '+
                                                                  CompletaPalavra(nomecor,20,' ')+' '+
                                                                  CompletaPalavra_a_Esquerda(formata_valor_3_casas(CurrToStr(QuantidadeRel)),12,' ')+' '+
                                                                  CompletaPalavra_a_Esquerda(CurrToStr(ValorRel),12,' ')+' '+
                                                                  CompletaPalavra_a_Esquerda(formata_valor(ValorFinalRel),12,' '));
                                          IncrementaLinha(1);

                                          ValorFinalRel:=0;

                                          QuantidadeRel:=0;
                                          CodigoMaterial:=self.ObjqueryTEMP.FieldByName('codigomaterial').AsInteger;
                                   end ;

                                   nomematerial:= Self.ObjqueryTEMP.fieldbyname('nomematerial').asstring;
                                   Material:= Self.ObjqueryTEMP.fieldbyname('material').asstring;
                                   nomecor:= Self.ObjqueryTEMP.fieldbyname('nomecor').asstring;
                                   QuantidadeRel:=QuantidadeRel+Self.ObjqueryTEMP.fieldbyname('quantidade').AsCurrency;
                                   ValorRel:=ValorRel+StrToCurr(tira_ponto(formata_valor(Self.ObjqueryTEMP.fieldbyname('valor').asstring))) ;
                                   ValorFinalRel:=ValorFinalRel+StrToCurr(tira_ponto(formata_valor(Self.ObjqueryTEMP.fieldbyname('valorfinal').asstring)));

                                   psomamaterial:=psomamaterial+Self.ObjqueryTEMP.fieldbyname('valorfinal').ascurrency;
                                   Self.ObjqueryTEMP.next;
                              End;
                              VerificaLinha;
                              ValorRel:=ValorFinalRel/QuantidadeRel;
                              RDprint.Imp(LinhaLocal,1,CompletaPalavra(Material,20,' ')+' '+
                                                       CompletaPalavra(nomematerial,45,' ')+' '+
                                                       CompletaPalavra(nomecor,20,' ')+' '+
                                                       CompletaPalavra_a_Esquerda(formata_valor_3_casas(CurrToStr(QuantidadeRel)),12,' ')+' '+
                                                       CompletaPalavra_a_Esquerda(CurrToStr(ValorRel),12,' ')+' '+
                                                       CompletaPalavra_a_Esquerda(formata_valor(ValorFinalRel),12,' '));
                              IncrementaLinha(1);

                              ValorFinalRel:=0;
                              ValorRel:=0;
                              QuantidadeRel:=0;
                              CodigoMaterial:=self.ObjqueryTEMP.FieldByName('codigomaterial').AsInteger;
                              VerificaLinha;
                              RDprint.Impf(LinhaLocal,1,CompletaPalavra('TOTAL EM PRODUTOS E SERVI�OS ',116,' ')+
                                                        CompletaPalavra_a_Esquerda(formata_valor(psomamaterial),12,' '),[negrito]);

                              IncrementaLinha(2);
                              psomafinal:=psomafinal+strtocurr(FmostraStringGrid.StringGrid.Cells[6,cont]);


                     End;//X

               End;//for
               DesenhaLinha;
               VerificaLinha;
               RDprint.Impf(LinhaLocal,1,CompletaPalavra('TOTAL FINAL ',116,' ')+
                                         CompletaPalavra_a_Esquerda(formata_valor(psomafinal),12,' '),[negrito]);

            Finally
                   FMostraBarraProgresso.Close;
                   RDprint.Fechar;
            End;
          End;
     end;

End;


{Rodolfo}
procedure TObjPedidoObjetos.Imprime_Relatorio_materiais_por_pedido_e_cliente;
var
Pdata1,Pdata2:string;
Pcliente:string;
cont:integer;
psomafinal,psomamaterial:currency;
begin

     if (ObjPermissoesUsoGlobal.ValidaPermissao('RELAT�RIO DE VENDAS POR MATERIAL PEDIDO E CLIENTE')=False)
     Then exit;

     {

     Filtrar por Cliente e per�odo, somente pedidos concluidos(vendas)
     Pedido   Material    Valor

     Total por Pedido

     }

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.caption:='Data Inicial';
          edtgrupo01.EditMask:=MascaraData;

          Grupo02.Enabled:=true;
          LbGrupo02.caption:='Data Final';
          edtgrupo02.EditMask:=MascaraData;

          Grupo03.Enabled:=True;
          LbGrupo03.caption:='Cliente';
          edtgrupo03.OnKeyDown:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.EdtClienteKeyDown;
          edtgrupo03.OnDblClick:=Self.ObjMedidas_Proj.PedidoProjeto.Pedido.EdtClienteDblClick;  {Rodolfo}
          edtgrupo03.Color:=$005CADFE;

          Showmodal;

          if (tag=0)
          then exit;

          Pdata1:='';
          Pdata2:='';
          Pcliente:='';

          Try
             if (trim(comebarra(edtgrupo01.text))<>'')
             Then Begin
                       strtodate(edtgrupo01.text);
                       pdata1:=edtgrupo01.text;
             End;
          Except
                Messagedlg('Data Inicial inv�lida',mterror,[mbok],0);
                exit;
          End;

          Try
             if (trim(comebarra(edtgrupo02.text))<>'')
             Then Begin
                       strtodate(edtgrupo02.text);
                       pdata2:=edtgrupo02.text;
             End;
          Except
                Messagedlg('Data Final inv�lida',mterror,[mbok],0);
                exit;
          End;

          Try
             if (trim(edtgrupo03.text)<>'')
             Then Begin
                       strtoint(edtgrupo03.text);
                       Pcliente:=edtgrupo03.text;

                       if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.LocalizaCodigo(pcliente)=False)
                       then Begin
                                 Messagedlg('Cliente n�o encontrado',mterror,[mbok],0);
                                 exit;
                       End;
                       Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.TabelaparaObjeto;
             End;
          Except
                Messagedlg('Cliente inv�lido',mterror,[mbok],0);
                exit;
          End;
     End;

     With Self.Objquery do
     Begin

          {seleciono os pedidos do periodo e uso o

          PROC_MAT_SERV_PP_PEDIDO}

          close;
          sql.clear;
          sql.add('Select tabpedido.Codigo,tabpedido.obra,tabpedido.data,tabpedido.valortotal,tabpedido.ValorBonificacaoCliente,');
          sql.add('tabpedido.valordesconto,tabpedido.valorfinal,tabpedido.vendedor,Tabvendedor.Nome AS nomevendedor,');
          sql.add('tabpedido.cliente,Tabcliente.Nome AS NOMECLIENTE,tabcliente.cidade from tabpedido');
          sql.add('join tabvendedor on Tabpedido.Vendedor=Tabvendedor.codigo');
          sql.add('join tabCliente on Tabpedido.Cliente=Tabcliente.codigo');
          Sql.add('Where TabPedido.codigo<>-500');
          Sql.add('and Tabpedido.Concluido=''S'' ');


          if(Pdata1<>'')
          Then sql.add('and TabPedido.Data>='+#39+Formatdatetime('mm/dd/yyyy',Strtodate(pdata1))+#39);

          if(Pdata2<>'')
          Then sql.add('and TabPedido.Data<='+#39+Formatdatetime('mm/dd/yyyy',Strtodate(pdata2))+#39);

          if(PCliente<>'')
          Then sql.add('and TabPedido.Cliente='+pcliente);

          sql.add('order by Tabpedido.data');
          open;
          last;
          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma informa��o foi selecionada',mterror,[mbok],0);
                    exit;
          End;
          first;
          //Selecionando os pedidos que irao para o relat�rio

          FmostraStringGrid.LbMensagem.caption:='F5 seleciona todos | F6 retira sele��o | ENTER ou Duplo clique seleciona linha atual';
          FmostraStringGrid.lbNomeTela.Caption:='Escolhas os pedidos para visualizar os materiais';
          FmostraStringGrid.lbrodape.caption:='';
          FmostraStringGrid.StringGrid.OnDblClick:=Self.StringGridDblClick;
          FmostraStringGrid.StringGrid.OnKeyPress:=Self.StringGridKeyPress;
          FmostraStringGrid.StringGrid.OnKeyDown:=Self.StringGridKeyDown;
          FmostraStringGrid.StringGrid.RowCount:=recordcount+1;
          FmostraStringGrid.StringGrid.ColCount:=8;
          FmostraStringGrid.StringGrid.FixedRows:=1;
          FmostraStringGrid.StringGrid.FixedCols:=0;
          FmostraStringGrid.StringGrid.ColWidths[0] := 50;

          cont:=0;

          FmostraStringGrid.StringGrid.cells[0,cont]:='';
          FmostraStringGrid.StringGrid.cells[1,cont]:='PEDIDO';
          FmostraStringGrid.StringGrid.cells[2,cont]:='DATA';
          FmostraStringGrid.StringGrid.cells[3,cont]:='VALOR TOTAL';
          FmostraStringGrid.StringGrid.cells[4,cont]:='DESCONTO';
          FmostraStringGrid.StringGrid.cells[5,cont]:='BONIFICA��O';
          FmostraStringGrid.StringGrid.cells[6,cont]:='VALOR FINAL';
          FmostraStringGrid.StringGrid.cells[7,cont]:='OBRA';

          cont:=1;
          While not(eof)
          do Begin
                  FmostraStringGrid.StringGrid.cells[0,cont]:='';
                  FmostraStringGrid.StringGrid.cells[1,cont]:=fieldbyname('codigo').asstring;
                  FmostraStringGrid.StringGrid.cells[2,cont]:=fieldbyname('data').asstring;
                  FmostraStringGrid.StringGrid.cells[3,cont]:=fieldbyname('valortotal').asstring;
                  FmostraStringGrid.StringGrid.cells[4,cont]:=fieldbyname('valordesconto').asstring;
                  FmostraStringGrid.StringGrid.cells[5,cont]:=fieldbyname('ValorBonificacaoCliente').asstring;
                  FmostraStringGrid.StringGrid.cells[6,cont]:=fieldbyname('valorfinal').asstring;
                  FmostraStringGrid.StringGrid.cells[7,cont]:=fieldbyname('obra').asstring;
                  inc(cont,1);
                  next;
          End;
          AjustaLArguraColunaGrid(FmostraStringGrid.StringGrid);

          FmostraStringGrid.PanelRodape.Visible:=True;
          //FmostraStringGrid.lbmensagem.caption:='';
          FmostraStringGrid.Showmodal;



          if (FmostraStringGrid.Tag=0)
          then exit;


          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               RDprint.TamanhoQteColunas:=130;
               RDprint.FonteTamanhoPadrao:=S17cpp;

               LinhaLocal:=3;
               RDprint.Abrir;
               if (RDprint.Setup=False)
               Then Begin
                         RDprint.fechar;
                         exit;
               End;

            Try
               RDprint.ImpC(linhalocal,65,'VENDAS POR MATERIAL E PEDIDO',[negrito]);
               IncrementaLinha(2);

               if (pcliente<>'')
               then Begin
                         RDprint.ImpF(LinhaLocal,1,'Cliente  '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome,[negrito]);
                         IncrementaLinha(1);
               End;

               if (Pdata1<>'')
               or (pdata2<>'')
               then Begin
                         RDprint.ImpF(LinhaLocal,1,'Per�odo: '+Pdata1+' a '+Pdata2,[negrito]);
                         IncrementaLinha(1);
               End;
               IncrementaLinha(1);



               FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
               FMostraBarraProgresso.lbmensagem.caption:='Gerando Relat�rio';
               FMostraBarraProgresso.btcancelar.Visible:=True;
               first;
               cont:=1;
               psomafinal:=0;

               for cont:=1 to FmostraStringGrid.StringGrid.RowCount-1 do
               Begin

                    FMostraBarraProgresso.IncrementaBarra1(1);
                    FMostraBarraProgresso.Show;
                    Application.ProcessMessages;

                    if (FMostraBarraProgresso.Cancelar)
                    Then Begin
                              FMostraBarraProgresso.Cancelar:=False;
                              if (Messagedlg('Tem certeza que deseja cancelar a gera��o do relat�rio?',mtConfirmation,[mbyes,mbno],0)=mryes)
                              Then Begin
                                        RDprint.Abortar;
                                        exit;

                              End;
                    End;

                   if (FmostraStringGrid.StringGrid.cells[0,cont]='       X')
                   Then begin

                            VerificaLinha;
                            RDprint.Impf(LinhaLocal,1,'Pedido: '+FmostraStringGrid.StringGrid.Cells[1,cont]+' '+
                                                      'Data: '+FmostraStringGrid.StringGrid.Cells[2,cont]+' '+
                                                      'Valor Total: '+formata_valor(FmostraStringGrid.StringGrid.Cells[3,cont])+' '+
                                                      'Desconto: '+formata_valor(FmostraStringGrid.StringGrid.Cells[4,cont])+' '+
                                                      'Bonifica��o: '+formata_valor(FmostraStringGrid.StringGrid.Cells[5,cont])+' '+
                                                      'Valor Final: '+formata_valor(FmostraStringGrid.StringGrid.Cells[6,cont]),[negrito]);
                            IncrementaLinha(2);

                            //selecionando os materiais
                            Self.ObjqueryTEMP.close;
                            Self.ObjqueryTEMP.sql.clear;
                            Self.ObjqueryTEMP.sql.add('select * from PROC_MAT_SERV_PP_PEDIDO('+FmostraStringGrid.StringGrid.Cells[1,cont]+')');
                            Self.ObjqueryTEMP.sql.add('order by material');
                            Self.ObjqueryTEMP.open;
                            Self.ObjqueryTEMP.first;

                            VerificaLinha;
                            RDprint.Impf(LinhaLocal,1,CompletaPalavra('MATERIAL',20,' ')+' '+
                                                          CompletaPalavra('NOME',40,' ')+' '+
                                                          CompletaPalavra('COR',20,' ')+' '+
                                                          CompletaPalavra_a_Esquerda('QUANTIDADE',12,' ')+' '+
                                                          CompletaPalavra_a_Esquerda('VALOR',12,' ')+' '+
                                                          CompletaPalavra_a_Esquerda('VALOR FINAL',12,' '),[negrito]);


                            IncrementaLinha(1);
                            psomamaterial:=0;
                            While not(Self.ObjqueryTEMP.eof) do
                            Begin

                                 VerificaLinha;
                                 RDprint.Imp(LinhaLocal,1,CompletaPalavra(Self.ObjqueryTEMP.fieldbyname('material').asstring,20,' ')+' '+
                                                          CompletaPalavra(Self.ObjqueryTEMP.fieldbyname('nomematerial').asstring,40,' ')+' '+
                                                          CompletaPalavra(Self.ObjqueryTEMP.fieldbyname('nomecor').asstring,20,' ')+' '+
                                                          CompletaPalavra_a_Esquerda(formata_valor_3_casas(Self.ObjqueryTEMP.fieldbyname('quantidade').asstring),12,' ')+' '+
                                                          CompletaPalavra_a_Esquerda(formata_valor(Self.ObjqueryTEMP.fieldbyname('valor').asstring),12,' ')+' '+
                                                          CompletaPalavra_a_Esquerda(formata_valor(Self.ObjqueryTEMP.fieldbyname('valorfinal').asstring),12,' '));


                                 IncrementaLinha(1);
                                 psomamaterial:=psomamaterial+Self.ObjqueryTEMP.fieldbyname('valorfinal').ascurrency;

                                 Self.ObjqueryTEMP.next;
                            End;
                            VerificaLinha;
                            RDprint.Impf(LinhaLocal,1,CompletaPalavra('TOTAL EM PRODUTOS E SERVI�OS ',109,' ')+
                                                      CompletaPalavra_a_Esquerda(formata_valor(psomamaterial),12,' '),[negrito]);

                            IncrementaLinha(2);
                            psomafinal:=psomafinal+strtocurr(FmostraStringGrid.StringGrid.Cells[6,cont]);


                   End;//X

               End;//for
               DesenhaLinha;
               VerificaLinha;
               RDprint.Impf(LinhaLocal,1,CompletaPalavra('TOTAL FINAL ',109,' ')+
                                         CompletaPalavra_a_Esquerda(formata_valor(psomafinal),12,' '),[negrito]);

            Finally
                   FMostraBarraProgresso.Close;
                   RDprint.Fechar;
            End;
          End;
     end;

End;


procedure TObjPedidoObjetos.StringGridKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then Begin
               Self.StringGridDblClick(sender);
     End;
end;

procedure TObjPedidoObjetos.StringGridDblClick(Sender: TObject);
begin
    if (TStringGrid(sender).cells[0,TStringgrid(sender).row]='')
    Then TStringGrid(sender).cells[0,TStringgrid(sender).row]:='       X'
    Else TStringGrid(sender).cells[0,TStringgrid(sender).row]:='';
end;

procedure TObjPedidoObjetos.StringGridKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
cont:integer;
begin
     if (key=vk_f5)
     then Begin
               for cont:=1 to TStringGrid(Sender).RowCount-1 do
               Begin
                    TStringGrid(sender).cells[0,cont]:='       X';
               End;
     End;

     if (key=vk_f6)
     then Begin
               for cont:=1 to TStringGrid(Sender).RowCount-1 do
               Begin
                    TStringGrid(sender).cells[0,cont]:='';
               End;
     End;

End;


procedure TObjPedidoObjetos.ImprimeAutorizacaoEmissaoNF(Ppedido: string);
var
ObjPersonalizadoRP:TObjRELPERSREPORTBUILDER;
begin
     Try
        ObjPersonalizadoRP:=TObjRELPERSREPORTBUILDER.Create;
     Except
           on e:exception do
           Begin
                mensagemerro('Erro na tentativa de criar o objeto de relat�rio personalizado no report builder'+#13+e.message);
                exit;
           End;
     End;

     try
        if (ObjPersonalizadoRP.LocalizaNOme('AUTORIZACAO_EMISSAO_NF')=False)
        Then Begin
                  MensagemErro('Relat�rio Personalizado (RPB) Nome: "AUTORIZACAO_EMISSAO_NF" n�o encontrado');
                  exit;
        End;
        ObjPersonalizadoRP.TabelaparaObjeto;

        ObjPersonalizadoRP.SQLCabecalhoPreenchido:=ObjPersonalizadoRP.Get_SQLCabecalho;
        ObjPersonalizadoRP.SQLRepeticaoPreenchido:=ObjPersonalizadoRP.Get_SQLRepeticao;
        ObjPersonalizadoRP.SQLRepeticao_2Preenchido:='';

        ObjPersonalizadoRP.SQLCabecalhoPreenchido:=StringReplace(ObjPersonalizadoRP.SQLCabecalhoPreenchido,':ppedido',ppedido,[rfReplaceAll]);
        ObjPersonalizadoRP.SQLRepeticaoPreenchido:=StringReplace(ObjPersonalizadoRP.SQLRepeticaoPreenchido,':ppedido',ppedido,[rfReplaceAll]);
        ObjPersonalizadoRP.ChamaRelatorio(False);

     Finally
            ObjPersonalizadoRP.Free;

     End;


end;

procedure TObjPedidoObjetos.CarregaParametros;
begin
     Parametro_ImprimeAutorizacaoNF:=False;

     if (ObjParametroGlobal.ValidaParametro('IMPRIME AUTORIZACAO EMISSAO DE NF APOS GERAR O PEDIDO')=True)
     Then Begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               Then Parametro_ImprimeAutorizacaoNF:=True;
     End;

end;


{Rodolfo}
procedure TObjPedidoObjetos.ImprimeCustos(ppedido: string);
var
psomavalor: currency;
begin
     if (ppedido='')
     then Begin
               MensagemAviso('Escolha um pedido');
               exit;
     End;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('EMITIR RELATORIO DE CUSTO DO PEDIDO')=False)
     then exit;


     With Self.Objquery do
     Begin
         close;
         sql.clear;
         sql.add('select ');
         sql.add('PM.material,');
         sql.add('PM.codigomaterial,');
         sql.add('PM.nomematerial,');
         sql.add('PM.nomecor,');
         sql.add('PM.quantidade,');
         SQL.Add('(PM.PrecoCusto + ((PM.precocusto * PM.PorcentagemAcrescimofinal)/100)) as PRECOCUSTO,');
         SQL.Add('(PM.quantidade * (PM.PrecoCusto + ((PM.precocusto * PM.PorcentagemAcrescimofinal)/100)) ) as VALORFINALCUSTO');
         sql.add('from PROC_MAT_SERV_PP_PEDIDO (:pedido) PM');
         sql.add('order by material');
         ParamByName('pedido').asstring:=ppedido;

         open;    //Foi feito um join com a a tabela de cor para pegar o acrescimo de porcentagem e calcular de forma correta o preco de custo e o custo final

         last;

          if (recordcount=0)
          then Begin
                    mensagemaviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;

          first;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               RDprint.TamanhoQteColunas:=130;
               RDprint.FonteTamanhoPadrao:=S17cpp;

               LinhaLocal:=3;
               RDprint.Abrir;

               if (RDprint.Setup=False)
               then begin
                         rdprint.Fechar;
                         exit;
               end;

               rdprint.ImpC(linhalocal,65,'CUSTO INDIVIDUAL POR PRODUTO - PEDIDO N�'+PPEDIDO,[negrito]);
               incrementalinha(2);


               //titulo das colunas
               RDprint.ImpF(linhalocal,1,CompletaPalavra('MATERIAL',12,' ')+' '+
                                         CompletaPalavra('PRODUTO',50,' ')+' '+
                                         CompletaPalavra('NOME COR',20,' ')+' '+
                                         CompletaPalavra_a_Esquerda('QUANTIDADE',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('CUSTO',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('QTDExCUSTO',12,' '),[negrito]);
               IncrementaLinha(1);
               Desenhalinha;

               psomavalor:=0;

               while not(Self.Objquery.eof) do
               Begin

                    VerificaLinha;

                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('material').asstring,12,' ')+' '+
                                              CompletaPalavra(fieldbyname('codigomaterial').asstring,6,' ')+'-'+CompletaPalavra(fieldbyname('nomematerial').asstring,43,' ')+' '+
                                              CompletaPalavra(fieldbyname('nomecor').asstring,20,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('quantidade').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('PRECOCUSTO').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORFINALCUSTO').ASSTRING),12,' '));
                    IncrementaLinha(1);

                    psomavalor:=psomavalor+Fieldbyname('VALORFINALCUSTO').asfloat;

                    Self.Objquery.next;
               end;

               DesenhaLinha;
               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'TOTAL DE CUSTO R$ '+formata_valor(psomavalor),[negrito]);

               rdprint.Fechar;
          End;




     end;


end;

function TObjPedidoObjetos.RetornaAreaMininaVidro(PpedidoProjeto, PcorVidro:string): string;
begin
     Result:='';

     if (Ppedidoprojeto='') or (PcorVidro='')
     Then exit;

     if (Self.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
     Then exit;
     Self.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (self.ObjVidro_PP.VidroCor.LocalizaCodigo(PcorVidro)=False)
     Then exit;
     Self.ObjVidro_PP.VidroCor.TabelaparaObjeto;
     try
         Result:= FloatToStr(strtofloat(Self.ObjVidro_PP.VidroCor.Vidro.Get_AreaMinima));
     except
         Result:='';
     end;

end;

procedure TObjPedidoObjetos.ApagaVendaDaTabvendas(pedido:string);
var
  query:TIBQuery;
  codigo:string;
 // Objvendas:Tobjvendas;
begin
    try
        try
          query:=TIBQuery.Create(nil);
          query.Database:=FDataModulo.IBDatabase;
        except

        end;
        with query do
        begin
            Close;
            sql.Clear;
            sql.Add('delete from tabvendas where pedido='+#39+pedido+#39);
            Open;
            FDataModulo.IBTransaction.CommitRetaining;

        end;


    finally
        FreeAndNil(query);
    end;

end;

procedure TObjPedidoObjetos.ApagaMateriasDaVenda(pedido:string);
var
  query:TIBQuery;
begin
    try
        try
          query:=TIBQuery.Create(nil);
          query.Database:=FDataModulo.IBDatabase;
        except

        end;
        with query do
        begin
            Close;
            sql.Clear;
            sql.Add('delete from tabmateriaisvenda where pedido='+#39+pedido+#39);
            Open;
            FDataModulo.IBTransaction.CommitRetaining;

        end;


    finally
        FreeAndNil(query);
    end;

end;

procedure TObjPedidoObjetos.CancelaNotaFiscal(pedido:string);
var
    query,queryaux:TIBQuery;
    menuNFE:TFmenuNfe;
    nfe:string;

begin

   if (pedido = '') then
    Exit;


    try
      query:=TIBQuery.Create(nil);
      query.Database:=FDataModulo.IBDatabase;
      queryaux:=TIBQuery.Create(nil);
      queryaux.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
          with query do
          begin

             if (UtilizaNfe) then
             begin


              Active:=false;
              SQL.Clear;
              sql.Add('select nf.codigo,nf.numero');
              sql.Add('from tabnotafiscal nf');
              sql.Add('where nf.numpedido = '+#39+pedido+#39+' and nf.modelo_nf = 2');
              SQL.Add('and situacao=''I''');

              Active:=True;

              nfe:=fieldbyname('numero').AsString;

              if (nfe = '') then
              begin

                MensagemAviso('Este pedido n�o possui NF-e');
                Exit;

              end else
              begin

                 try
                 
                    menuNFE := TFmenuNfe.Create(nil);
                    
                    try

                      if not (ObjNotaFiscalObjetos.objgeranfe.Nfe.LocalizaCodigo (nfe)) then
                      begin

                        MensagemAviso ('NF-e de c�digo: '+nfe+' n�o encontrada');
                        Exit;

                      end;

                      if not (ObjNotaFiscalObjetos.objgeranfe.Nfe.TabelaparaObjeto()) then
                      begin

                        MensagemAviso ('N�o foi possivel transferir os dados da NF-e para objeto. NF-e de c�digo: '+nfe);
                        Exit;

                      end;

                      if (ObjNotaFiscalObjetos.objgeranfe.Nfe.get_arquivo_xml() = '') then
                      begin

                        MensagemAviso ('Arquivo XML vazio. NF-e de c�digo: '+nfe);
                        Exit;

                      end;

                      menuNFE := TFmenuNfe.Create(nil);
                      menuNFE.objTransmitenfe := self.objtransmitenfe;
                      menuNFE.PassaObjeto (ObjNotaFiscalObjetos);
                      menuNFE.CancelaNfe (ObjNotaFiscalObjetos.objgeranfe.Nfe.get_arquivo_xml());
                      
                      Close;
                      sql.Clear;
                      sql.Add('select codigo from tabnotafiscal where numpedido='+#39+pedido+#39+' and situacao=''I'' ');
                      Open;
                      AumentaEstoque(fieldbyname('codigo').AsString,pedido,DateToStr(Now)) ;

                     try
                        Close;
                        sql.Clear;
                        sql.Add('update tabmateriaisvenda set notafiscal=null');
                        SQL.Add('where pedido='+pedido);
                        ExecSQL;

                     except
                        MensagemErro('Erro ao tentar cancelar a nota');
                     end;

                      if(menuNFE.Tag=-1) then
                      begin
                        FDataModulo.IBTransaction.RollbackRetaining;
                        exit;
                      end
                      else
                        FDataModulo.IBTransaction.CommitRetaining;

                    finally

                      FreeAndNil (menuNFE);

                    end;

                    MensagemAviso('NF-e cancelada');
                    FDataModulo.IBTransaction.CommitRetaining;
                    Exit;
                 except
                    FDataModulo.IBTransaction.RollbackRetaining;
                    Exit;
                 end;

              end;

             end else
             begin

               try
                  Close;
                  sql.Clear;
                  sql.Add('select codigo from tabnotafiscal where numpedido='+#39+pedido+#39+' and situacao=''I'' ');
                  Open;
                  while not Eof do
                  begin
                       AumentaEstoque(fieldbyname('codigo').AsString,pedido,DateToStr(Now)) ;
                       Next;
                  end;

                  Close;
                  sql.Clear;
                  sql.Add('update tabnotafiscal set situacao=''C''');
                  SQL.Add('where numpedido='+#39+pedido+#39+' and modelo_nf = 1');
                  ExecSQL;

               except
                  MensagemErro('Erro ao tentar cancelar a nota');
                  FDataModulo.IBTransaction.RollbackRetaining;
                  Exit;
               end;

             end;

             try
                  Close;
                  sql.Clear;
                  sql.Add('update tabmateriaisvenda set notafiscal=null');
                  SQL.Add('where pedido='+pedido);
                  ExecSQL;

             except
                  MensagemErro('Erro ao tentar cancelar a nota');
                  FDataModulo.IBTransaction.RollbackRetaining;
                  Exit;
             end;

             FDataModulo.IBTransaction.CommitRetaining;

             MensagemSucesso('NOTA FISCAL DO PEDIDO '+PEDIDO+' FOI CANCELADA COM SUCESSO!!!');
          end;
    finally
          FreeAndNil(query);
    end;
end;

procedure TObjPedidoObjetos.Replica_Pedido(pedido:string);
begin
     if(ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(Pedido)=true)
     then
     begin
           ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
     end;
end;

function TObjPedidoObjetos.__Verifica_CodigoBarras(codigobarras,PP:string;ListaCodigoBarras:TStringList):Boolean;
var
  i:Integer;
  //Qry_LocalizaCodigobarras:TIBQuery;
begin
  for i:=0 to ListaCodigoBarras.Count-1 do
  begin
    if(retornaPalavrasDepoisSimbolo2(ListaCodigoBarras[i],';')=codigobarras) then
    begin
      Result:=True;
      Exit;
    end;
  end;
  Result:=False;
end;

Function TObjPedidoObjetos.___GeraCodigoBarrasComponentes(PP_ORDEMPRODUCAO,COMPONENTES,PP,SERVICO:string;var ListaCodigoBarras:TStringList):Boolean;
var
 sentinela,i,numrand:Integer;
 codigobarras:string;
 Palavra, PalavraRestante:string;
 Qry_GravaCodigoBarrasComp_QUERY:TIBQuery;
 Existe:Boolean;
begin
  Result:=False;
  try
    Qry_GravaCodigoBarrasComp_QUERY:=TIBQuery.Create(nil);
    Qry_GravaCodigoBarrasComp_QUERY.Database:=FDataModulo.IBDatabase;
  except
    Exit;
  end;
  try
    PalavraRestante:=COMPONENTES;
    while (PalavraRestante<>'') do
    begin
      Palavra:=retornaPalavrasAntesSimbolo(PalavraRestante,';');
      PalavraRestante:=retornaPalavrasDepoisSimbolo2(PalavraRestante,';');
      Existe:=False;
      if(ListaCodigoBarras.count>0)  then
      begin
        for i:=0 to ListaCodigoBarras.count-1 do
        begin
          if(Palavra=retornaPalavrasAntesSimbolo(ListaCodigoBarras[i],';')) then
          begin
            Existe:=True;
            codigobarras:=retornaPalavrasDepoisSimbolo2(ListaCodigoBarras[i],';');
          end;
        end;
      end;

      if(Existe=False)then
      begin
        sentinela := 0;
        codigobarras:='';
        while(sentinela = 0)
        do begin
          Randomize;
          for I := 1 to 10 do
          begin
            numrand:=Random(10);
            codigobarras:= codigobarras+IntToStr(numrand);
          end;
          codigobarras:=codigobarras+'0';
          if(ListaCodigoBarras.count>0)  then
          begin
            if (__Verifica_CodigoBarras(codigobarras,PP,ListaCodigoBarras) =False ) then
            begin
              sentinela:=1;
            end
            else
            begin
              sentinela:=0;
            end;
          end
          else sentinela:=1;
        end;
        ListaCodigoBarras.Add(Palavra+';'+codigobarras);
      end;


      Qry_GravaCodigoBarrasComp_QUERY.Close;
      Qry_GravaCodigoBarrasComp_QUERY.SQL.Clear;
      Qry_GravaCodigoBarrasComp_QUERY.SQL.Text:='INSERT INTO TABCOMPONENTES_ORDEMPRODUCAO(PP_ORDEMPRODUCAO,REFCOMPONENTE,CODIGOBARRAS,estadoproducao,PEDIDOPROJETO,SERVICO) '+
      'VALUES (:PP_ORDEMPRODUCAO,:REFCOMPONENTE,:CODIGOBARRAS,:estadoproducao,:PEDIDOPROJETO,:SERVICO)';

      Qry_GravaCodigoBarrasComp_QUERY.Params[0].AsString:= PP_ORDEMPRODUCAO;
      Qry_GravaCodigoBarrasComp_QUERY.Params[2].AsString:= codigobarras;
      Qry_GravaCodigoBarrasComp_QUERY.Params[1].AsString:= Palavra;
      Qry_GravaCodigoBarrasComp_QUERY.Params[3].AsString:= 'A';
      Qry_GravaCodigoBarrasComp_QUERY.Params[4].AsString:= PP;
      Qry_GravaCodigoBarrasComp_QUERY.Params[5].AsString:= SERVICO;
      try
        Qry_GravaCodigoBarrasComp_QUERY.ExecSQL;
      except
        Exit;
      end; 
    end;
    Result:=True;
  finally
    FreeAndNil(Qry_GravaCodigoBarrasComp_QUERY);
  end;
end;



//USADA PARA GERAR UMA ORDEM DE SERVI�O A PARTIR DE UM PEDIDO
//N�O SE FAZ MAIS NECESSARIA POIS A SUA UTILIZA��O DAVA SE PELO FATO DE VENDA DE VIDROS AVLSAS
//VENDA DE VIDRO EM LOTE, NESSE CASO A VENDA DE VIDRO EM LOTE VAI SER FEITA A PARTIR DO MODULO
//ORDEM DE SERVI�O, NESSE CASO PODE-SE VENDER UM VIDRO AVULSO PASSANDO SEU TAMANHHO E QUAIS SERVI�OS
//O MESMO VAI SOFRER, SE NECESSARIO VOLTAR A GERAR OS PELO PEDIDO, BASTA DA UMA ESTUDADA NESTA FUN��O
//Jonatan Medina 31/05/2011

//Reutilizada a partir de 01/06/2011
function TObjPedidoObjetos.GeraOrdemProducao(pedido:string):Boolean;
var
    Qry_QUERY:TIBQuery;
    Qry_QUERYGeracao:TIBQuery;
    Qry_QUERYGeracaoCODIGOBARRAS:TIBQuery;
    OrdemServico:string;
    CodFuncionario,PP:string;
    ListaPP:TStringList;
    i:integer;
    ListaComponentesCodigoBarras:TStringList;
begin
   if (messagedlg('Gerar uma OS para esse pedido?',mtconfirmation,[mbyes,mbno],0)=mrno)
   then exit ;

   if(pedido='')
   then Exit;

   try
      Qry_QUERY:=TIBQuery.Create(nil);
      Qry_QUERY.Database:=FDataModulo.IBDatabase;

      Qry_QUERYGeracaoCODIGOBARRAS:=TIBQuery.Create(nil);
      Qry_QUERYGeracaoCODIGOBARRAS.Database:=FDataModulo.IBDatabase;

      Qry_QUERYGeracao :=TIBQuery.Create(nil);
      Qry_QUERYGeracao.Database:=FDataModulo.IBDatabase;

      ListaPP:=TStringList.Create;
      ListaComponentesCodigoBarras:=TStringList.Create;
   except
      Exit;
   end;

   try
      Qry_QUERY.Close;
      Qry_QUERY.SQL.Clear;
      Qry_QUERY.SQL.Text:=
      'SELECT PP.CODIGO, '+
      'V.descricao, '+
      'C.descricao, '+
      'VPP.quantidade, '+
      'SERVICO.descricao, '+
      'SERVICO.CODIGO, '+
      'TSP.ORDEM, '+
      'TSP.listadecomponentes '+
      'FROM TABPEDIDO_PROJ PP '+
      'JOIN TABVIDRO_PP VPP ON VPP.PEDIDOPROJETO=PP.CODIGO '+
      'JOIN TABVIDROCOR VC  ON VC.CODIGO=VPP.vidrocor '+
      'JOIN tabvidro V ON V.CODIGO=VC.vidro '+
      'JOIN TABCOR C ON C.CODIGO=VC.cor '+
      'JOIN TABSERVICOPRODUCAO_PROJ TSP ON  TSP.PROJETO=PP.PROJETO '+
      'JOIN tabservico SERVICO ON SERVICO.CODIGO=TSP.SERVICO  '+
      'WHERE PP.PEDIDO='+pedido+' '+
        'and '+
        'NOT EXISTS ( '+
            'SELECT CODIGO '+
            'FROM '+
            'tabpedidoproj_ordemproducao '+
             'WHERE '+
             'tabpedidoproj_ordemproducao.pedidoprojeto = PP.CODIGO '+
      ') '+
      'ORDER BY PP.CODIGO,TSP.ORDEM  ';

      Qry_QUERY.Open;
      pp:='';

      while not Qry_QUERY.Eof do
      begin
         Qry_QUERYGeracao.Close;
         Qry_QUERYGeracao.SQL.Clear;
         Qry_QUERYGeracao.SQL.Text := FDataModulo.IBDataPEDIDOPROJ_ORDEMPRODUCAO.InsertSQL.Text;
         Qry_QUERYGeracao.Params[0].AsString:= '0';
         Qry_QUERYGeracao.Params[1].AsString:= Qry_QUERY.Fields[0].AsString;
         Qry_QUERYGeracao.Params[2].AsString:= Qry_QUERY.Fields[5].AsString;
         // A = Aguardando
         // P = Produ��o
         // C = Concluido
         Qry_QUERYGeracao.Params[3].AsString:= 'A';

         //Define horario de entrada/horario de saida na linha de produ��o
         Qry_QUERYGeracao.Params[4].AsString:= '';
         Qry_QUERYGeracao.Params[5].AsString:= '';

         //Define data de entrada/data de saida na linha de produ��o
         Qry_QUERYGeracao.Params[6].AsString:= '';
         Qry_QUERYGeracao.Params[7].AsString:= '';
         Qry_QUERYGeracao.Params[8].AsString:= Qry_QUERY.Fields[7].AsString;

         if(ListaPP.Count>0) then
         begin
           if(PP<>Qry_QUERY.Fields[0].AsString)
           then ListaPP.Add(Qry_QUERY.Fields[0].AsString)
         end
         else ListaPP.Add(Qry_QUERY.Fields[0].AsString);  
         pp:=Qry_QUERY.Fields[0].AsString;


         try
          Qry_QUERYGeracao.ExecSQL;
         except
          FDataModulo.IBTransaction.RollbackRetaining;
          Exit;
         end;

         Qry_QUERY.Next;
      end;
      for i:=0 to ListaPP.count-1 do
      begin
        Qry_QUERYGeracaoCODIGOBARRAS.Close;
        Qry_QUERYGeracaoCODIGOBARRAS.SQL.Clear;
        Qry_QUERYGeracaoCODIGOBARRAS.SQL.Text:=
        'SELECT CODIGO,LISTADECOMPONENTES,SERVICO FROM TABPEDIDOPROJ_ORDEMPRODUCAO '+
        'WHERE PEDIDOPROJETO='+ListaPP[i];

        Qry_QUERYGeracaoCODIGOBARRAS.Open;
                                             
        while not Qry_QUERYGeracaoCODIGOBARRAS.Eof do
        begin

          if(___GeraCodigoBarrasComponentes(Qry_QUERYGeracaoCODIGOBARRAS.Fields[0].AsString,Qry_QUERYGeracaoCODIGOBARRAS.Fields[1].AsString,ListaPP[i],Qry_QUERYGeracaoCODIGOBARRAS.Fields[2].AsString,ListaComponentesCodigoBarras)=False)then
          begin
            MensagemAviso('Erro ao gerar codigo de barras para os componentes/ CONTATE O SUPORTE');
            Exit;
          end;

          Qry_QUERYGeracaoCODIGOBARRAS.Next;
        end;
        ListaComponentesCodigoBarras.Clear;
      end;

      FDataModulo.IBTransaction.CommitRetaining;

   finally
      FreeAndNil(Qry_QUERY);
      FreeAndNil(Qry_QUERYGeracaoCODIGOBARRAS);
      FreeAndNil(Qry_QUERYGeracao);
      ListaPP.Free;
      ListaComponentesCodigoBarras.Free;
   end;
end;

procedure TObjPedidoObjetos.ImprimeListaPedidos(parametro:string);
var
    ObjReportBuilder:TObjRELPERSREPORTBUILDER;
    DataFim, DataInicio, pVendedor:string;
begin
      try
          ObjReportBuilder:=TObjRELPERSREPORTBUILDER.Create;
      except

      end;

      ObjReportBuilder.ZerarTabela;
      try
            with FfiltroImp do
            begin
                  DesativaGrupos;
                  Grupo06.Enabled:=True;
                  Grupo02.Enabled:=True;
                  Grupo03.Enabled:=True;
                  Grupo04.Enabled:=True;
                //  Grupo01.Enabled:=True;

                  LbGrupo06.Caption:='Conclu�do?';
                  LbGrupo02.Caption:='Data Inicial';
                  lbgrupo03.Caption:='Data Final';
                  LbGrupo04.Caption:='Vendedor';


               //   LbGrupo01.Caption:='Cliente' ;

                  edtgrupo02.EditMask:='99/99/9999';
                  edtgrupo03.EditMask:='99/99/9999';
                  edtgrupo04.OnKeyDown := Self.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtVendedorKeyDown;
                  ComboGrupo06.Items.Clear;
                  ComboGrupo06.Items.Add('SIM');
                  ComboGrupo06.Items.Add('NAO');
                  ShowModal;
                  if (tag=0)
                  then Exit;
                  datafim:='  /  /    ';
                  DataInicio:='  /  /    ';
                  if(edtgrupo02.Text<>'  /  /    ')
                  then DataInicio:=FormatDateTime('mm/dd/yyyy',StrToDate(edtgrupo02.Text)) ;
                  if(edtgrupo03.Text<>'  /  /    ')
                  then DataFim:=FormatDateTime('mm/dd/yyyy',StrToDate(edtgrupo03.Text)) ;

                  if Trim(edtgrupo04.Text) <> '' then
                    pVendedor := edtgrupo04.Text
                  else
                    pVendedor := '';

                  if (ObjReportBuilder.LocalizaNOme('STATUS DO PEDIDO') = false)
                  then Begin
                        MensagemErro('O Relatorio de ReporteBuilder "STATUS DO PEDIDO" n�o foi econtrado');
                        exit;
                  end;
                  ObjReportBuilder.TabelaparaObjeto;
                  ObjReportBuilder.SQLCabecalhoPreenchido:=StrReplace(ObjReportBuilder.Get_SQLCabecalho,':PCODIGO','');
                  ObjReportBuilder.SQLRepeticaoPreenchido:=StrReplace(ObjReportBuilder.get_SQLRepeticao,':PCODIGO','');
                  ObjReportBuilder.SQLRepeticao_2Preenchido:=StrReplace(ObjReportBuilder.Get_SQLRepeticao_2,':PCODIGO','');
                  if(ComboGrupo06.Text<>'')
                  then
                  begin

                    if(ComboGrupo06.Text = 'SIM') then
                      ObjReportBuilder.SQLRepeticaoPreenchido := ObjReportBuilder.SQLRepeticaoPreenchido+' and concluido=''S'' ';

                    if(ComboGrupo06.Text = 'NAO') then
                      ObjReportBuilder.SQLRepeticaoPreenchido := ObjReportBuilder.SQLRepeticaoPreenchido+' and concluido=''N'' ';

                  end;

                  if (pVendedor <> '') then
                    ObjReportBuilder.SQLRepeticaoPreenchido := ObjReportBuilder.SQLRepeticaoPreenchido+' and vendedor = '+pVendedor;

                  if ( Trim(comebarra(DataFim)) <> '') then
                    ObjReportBuilder.SQLRepeticaoPreenchido:= ObjReportBuilder.SQLRepeticaoPreenchido+' and data>='+#39+DataInicio+#39;

                  if (Trim(comebarra(DataInicio)) <> '') then
                    ObjReportBuilder.SQLRepeticaoPreenchido:=ObjReportBuilder.SQLRepeticaoPreenchido+' and data<='+#39+DataFim+#39;

                  ObjReportBuilder.ChamaRelatorio(false);
            end;
      finally
            ObjReportBuilder.Free;
      end;


end;

procedure TObjPedidoObjetos.ImprimeVendaPorPeriodo(parametro:string);
var
Pdata1,Pdata2:string;
Pcliente,Pvendedor:string;
Pcomissao,PsomaBrutoVendedor,PSomaBrutoGeral:Currency;
PSomaComissaoVendedor,PsomaComissaoGeral,prelacaoporcento,PsomaLiquidovendedor,PsomaLiquidogeral:Currency;
PsomaDescontoVendedor,PsomaDescontoGeral:Currency;
PsomabonificacaoVendedor,PSomabonificacaoGeral:currency;
begin

     pvendedor:='';

     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('RELAT�RIO DE VENDAS POR PER�ODO E VENDEDOR')=False)
     then Begin
               //nao tem permissao, entao pe�o senha para escolher o vendedor
               //com senha

               if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.vendedor.PegaVendedor(Pvendedor)=False)
               Then exit;
     End;

     {
     Vendedor X

     Pedido   Cliente   Valor Total   Desconto  Acrescimo Valor Final
     }

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.caption:='Data Inicial';
          edtgrupo01.EditMask:=MascaraData;

          Grupo02.Enabled:=true;
          LbGrupo02.caption:='Data Final';
          edtgrupo02.EditMask:=MascaraData;

          Grupo03.Enabled:=True;
          LbGrupo03.caption:='Cliente';

          if (Pvendedor='')//senao tiver vazio � porque um vendedor digitou a senha
          Then Begin
                  Grupo04.Enabled:=True;
                  LbGrupo04.caption:='Vendedor';
                  edtgrupo04.OnKeyDown:=Self.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtVendedorKeyDown;
                  edtgrupo03.OnKeyDown:=Self.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtClienteKeyDown;
          End
          Else edtgrupo04.text:=pvendedor;


          Showmodal;

          if (tag=0)
          then exit;

          Pdata1:='';
          Pdata2:='';
          Pcliente:='';
          Pvendedor:='';

          Try
             if (trim(comebarra(edtgrupo01.text))<>'')
             Then Begin
                       strtodate(edtgrupo01.text);
                       pdata1:=edtgrupo01.text;
             End;
          Except
                Messagedlg('Data Inicial inv�lida',mterror,[mbok],0);
                exit;
          End;

          Try
             if (trim(comebarra(edtgrupo02.text))<>'')
             Then Begin
                       strtodate(edtgrupo02.text);
                       pdata2:=edtgrupo02.text;
             End;
          Except
                Messagedlg('Data Final inv�lida',mterror,[mbok],0);
                exit;
          End;

          Try
             if (trim(edtgrupo03.text)<>'')
             Then Begin
                       strtoint(edtgrupo03.text);
                       Pcliente:=edtgrupo03.text;

                       if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.LocalizaCodigo(pcliente)=False)
                       then Begin
                                 Messagedlg('Cliente n�o encontrado',mterror,[mbok],0);
                                 exit;
                       End;
                       Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.TabelaparaObjeto;


             End;
          Except
                Messagedlg('Cliente inv�lido',mterror,[mbok],0);
                exit;
          End;

          Try
             if (trim(edtgrupo04.text)<>'')
             Then Begin
                       strtoint(edtgrupo04.text);
                       Pvendedor:=edtgrupo04.text;

                       if (Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Vendedor.LocalizaCodigo(Pvendedor)=False)
                       then Begin
                                 Messagedlg('Vendedor n�o encontrado',mterror,[mbok],0);
                                 exit;
                       End;
                       Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Vendedor.TabelaparaObjeto;
             End;
          Except
                Messagedlg('Vendedor inv�lido',mterror,[mbok],0);
                exit;
          End;





     End;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select tabpedido.Codigo,tabpedido.data,tabpedido.valortotal,tabpedido.ValorBonificacaoCliente,');
          sql.add('tabpedido.valordesconto,tabpedido.valorfinal,tabpedido.vendedor,Tabvendedor.Nome AS nomevendedor,');
          sql.add('tabpedido.cliente,Tabcliente.Nome AS NOMECLIENTE,tabcliente.cidade from tabpedido');
          sql.add('join tabvendedor on Tabpedido.Vendedor=Tabvendedor.codigo');
          sql.add('join tabCliente on Tabpedido.Cliente=Tabcliente.codigo');
          Sql.add('Where TabPedido.codigo<>-500');
          Sql.add('and Tabpedido.Concluido=''S'' ');


          if(Pdata1<>'')
          Then sql.add('and TabPedido.Data>='+#39+Formatdatetime('mm/dd/yyyy',Strtodate(pdata1))+#39);

          if(Pdata2<>'')
          Then sql.add('and TabPedido.Data<='+#39+Formatdatetime('mm/dd/yyyy',Strtodate(pdata2))+#39);

          if(Pvendedor<>'')
          Then sql.add('and TabPedido.Vendedor='+pvendedor);

          if(PCliente<>'')
          Then sql.add('and TabPedido.Cliente='+pcliente);

          sql.add('order by Tabpedido.vendedor,tabpedido.data');
          open;

          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma informa��o foi selecionada',mterror,[mbok],0);
                    exit;
          End;


          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               RDprint.TamanhoQteColunas:=130;
               RDprint.FonteTamanhoPadrao:=S17cpp;

               LinhaLocal:=3;
               RDprint.Abrir;
               if (RDprint.Setup=False)
               Then Begin
                         RDprint.fechar;
                         exit;
               End;


               RDprint.ImpC(linhalocal,65,'VENDAS POR PER�ODO',[negrito]);
               IncrementaLinha(2);

               if (pcliente<>'')
               then Begin
                         RDprint.ImpF(LinhaLocal,1,'Cliente  '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome,[negrito]);
                         IncrementaLinha(1);
               End;

               if (Pvendedor<>'')
               then Begin
                         RDprint.ImpF(LinhaLocal,1,'Vendedor '+Self.ObjMedidas_Proj.PedidoProjeto.Pedido.Vendedor.Get_Nome,[negrito]);
                         IncrementaLinha(1);
               End;

               if (Pdata1<>'')
               or (pdata2<>'')
               then Begin
                         RDprint.ImpF(LinhaLocal,1,'Per�odo: '+Pdata1+' a '+Pdata2,[negrito]);
                         IncrementaLinha(1);
               End;
               IncrementaLinha(1);




               RDprint.ImpF(LinhaLocal,1,CompletaPalavra('PEDIDO',9,' ')+' '+
                                         CompletaPalavra('DATA',8,' ')+' '+
                                         CompletaPalavra('CLIENTE',30,' ')+' '+
                                         CompletaPalavra('CIDADE',20,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR TOTAL',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('DESC.BON.',9,' ')+' '+
                                         CompletaPalavra_a_Esquerda('DESCONTO',9,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR FINAL',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('COMISS�O',12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               IncrementaLinha(1);

               Pvendedor:='';

               PsomaBrutoVendedor:=0;
               PSomabrutoGeral:=0;

               PsomaLiquidoVendedor:=0;
               PSomaLiquidoGeral:=0;

               PsomaComissaoVendedor:=0;
               PsomaComissaoGeral:=0;

               PsomaDescontoVendedor:=0;
               PsomaDescontoGeral:=0;

               PsomabonificacaoVendedor:=0;
               PSomabonificacaoGeral:=0;

               While not(Self.Objquery.eof) do
               Begin

                    if (Pvendedor<>Fieldbyname('vendedor').asstring)
                    Then Begin
                              //mudou o vendedor
                              if (Pvendedor<>'')
                              Then Begin
                                        //Psomabruto       100%
                                        //psomadesconto     X
                                        //->X=(PSOMADESCONTO*100)/PSOMABRUTO
                                        prelacaoporcento:=(PsomaDescontoVendedor*100)/PsomaBrutoVendedor;
                                        //totalizando o vendedor anterior
                                        VerificaLinha;
                                        RDprint.Impf(LinhaLocal,1,CompletaPalavra('SOMA DO VENDEDOR ',45,' ')+completapalavra(' ',26,' ')+CompletaPalavra_a_Esquerda(formata_valor(PsomaBrutoVendedor),12,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PRelacaoPorcento)+'%',19,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PsomaLiquidovendedor),12,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PsomaComissaoVendedor),12,' '),[negrito]);
                                        IncrementaLinha(2);

                              End;

                              VerificaLinha;
                              RDprint.Impf(LinhaLocal,1,CompletaPalavra(fieldbyname('vendedor').asstring+'-'+fieldbyname('nomevendedor').asstring,90,' '),[negrito]);
                              IncrementaLinha(1);
                              Pvendedor:=Fieldbyname('vendedor').asstring;
                              PsomabrutoVendedor:=0;
                              PsomaLiquidoVendedor:=0;
                              PSomaComissaoVendedor:=0;
                              PsomaDescontoVendedor:=0;
                              PsomabonificacaoVendedor:=0;

                    End;
                    PComissao:=Self.RetornaValorComissaoPedido(fieldbyname('codigo').asstring);

                    VerificaLinha;
                    RDprint.Imp(LinhaLocal,1,CompletaPalavra(fieldbyname('codigo').asstring,9,' ')+' '+
                                         CompletaPalavra(formatdatetime('dd/mm/yy',fieldbyname('DATA').asdatetime),8,' ')+' '+
                                         CompletaPalavra(fieldbyname('CLIENTE').asstring+'-'+fieldbyname('nomeCLIENTE').asstring,30,' ')+' '+
                                         CompletaPalavra(fieldbyname('cidade').asstring,20,' ')+' '+
                                         CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORTOTAL').asstring),12,' ')+' '+
                                         CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('ValorBonificacaoCliente').asstring),9,' ')+' '+
                                         CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorDESCONTO').asstring),9,' ')+' '+
                                         CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORFINAL').asstring),12,' ')+' '+
                                         CompletaPalavra_a_Esquerda(formata_valor(PComissao),12,' '));
                    IncrementaLinha(1);

                    PsomaLiquidovendedor:=PsomaLiquidoVendedor+fieldbyname('valorfinal').asfloat;
                    PsomaLiquidogeral:=PSomaLiquidoGeral+fieldbyname('valorfinal').asfloat;

                    PsomaDescontoVendedor:=PsomaDescontoVendedor+fieldbyname('valordesconto').asfloat;
                    PsomaDescontoGeral:=PsomaDescontoGeral+fieldbyname('valordesconto').asfloat;

                    PsomaBrutoVendedor:=PsomaBrutoVendedor+fieldbyname('valortotal').asfloat;
                    PSomaBrutoGeral   :=PSomaBrutoGeral+fieldbyname('valortotal').asfloat;

                    PsomabonificacaoVendedor:=PsomabonificacaoVendedor+fieldbyname('valorbonificacaocliente').asfloat;
                    PSomabonificacaoGeral:=PSomabonificacaoGeral+fieldbyname('valorbonificacaocliente').asfloat;


                    PsomaComissaoVendedor:=PsomaComissaoVendedor+Pcomissao;
                    PsomaComissaoGeral:=PsomaComissaoGeral+Pcomissao;

                    Self.Objquery.Next;
               End;//while

               prelacaoporcento:=(PsomaDescontoVendedor*100)/PsomaBrutoVendedor;

               VerificaLinha;
               RDprint.Impf(LinhaLocal,1,CompletaPalavra('SOMA DO VENDEDOR ',45,' ')+completapalavra(' ',26,' ')+CompletaPalavra_a_Esquerda(formata_valor(PsomaBrutoVendedor),12,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PRelacaoPorcento)+'%',19,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PsomaLiquidovendedor),12,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PsomaComissaoVendedor),12,' '),[negrito]);
               IncrementaLinha(2);
               DesenhaLinha;

               prelacaoporcento:=(PsomaDescontoGeral*100)/PSomaBrutoGeral;

               VerificaLinha;
               RDprint.Impf(LinhaLocal,1,CompletaPalavra('SOMA GERAL ',45,' ')+completapalavra(' ',26,' ')+CompletaPalavra_a_Esquerda(formata_valor(PsomaBrutogeral),12,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PRelacaoPorcento)+'%',19,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PsomaLiquidoGeral),12,' ')+' '+ CompletaPalavra_a_Esquerda(formata_valor(PsomaComissaoGeral),12,' '),[negrito]);
               IncrementaLinha(2);


               RDprint.Fechar;
          End;
     End;
end;

procedure TObjPedidoObjetos.AlterarCorVidroProjetos(pedido:string);
var
   FAlterarCoresPProjetos:TFAlterarCoresPProjetos;
   QueryPesq:TIBQuery;
begin
    if(pedido='') then
    begin
          MensagemAviso('Escolha o pedido');
          Exit;
    end;
    QueryPesq:=TIBQuery.Create(nil);
    QueryPesq.Database:=FDataModulo.IBDatabase;

    QueryPesq.Close;
    QueryPesq.SQL.Clear;
    QueryPesq.SQL.Add('select codigo from tabnotafiscal where numpedido='+#39+pedido+#39);
    QueryPesq.SQL.Add('and situacao=''I''') ;
    QueryPesq.open;

    if(QueryPesq.RecordCount>0) then
    begin
            MensagemAviso('J� foram geradas notas para este pedido');
            Exit;
    end;

    FAlterarCoresPProjetos:=TFAlterarCoresPProjetos.Create(nil);

    try
        FAlterarCoresPProjetos.Tag:=StrToInt(pedido);
        FAlterarCoresPProjetos.ShowModal;
    finally
        FreeAndNil(FAlterarCoresPProjetos);
        FreeAndNil(QueryPesq);
    end;


end;

function TObjPedidoObjetos.VerficaEstoque(pedido:string):Boolean;
var
   QueryLocal:TIBQuery;
   QueryLocal2:TIBQuery;
   CodigoMaterial:string;
   PestoqueAtual,PnovoEstoque:Currency;
begin
       Result:=False;

       QueryLocal:= TIBQuery.Create(nil);
       QueryLocal.Database:=FDataModulo.IBDatabase;

       QueryLocal2:= TIBQuery.Create(nil);
       QueryLocal2.Database:=FDataModulo.IBDatabase;

       try
            with QueryLocal do
            begin
                 Close;
                 SQL.Clear;
                 SQL.Add('select ped.codigo as pedido,pp.codigo as pedidoprojeto');
                 SQL.Add('from tabpedido ped');
                 SQL.Add('join tabpedido_proj pp on pp.pedido=ped.codigo where ped.codigo='+Pedido);
                 Open;

                 while not Eof do
                 begin
                      //Verifica estoque do pedido...
                      QueryLocal2.Close;
                      QueryLocal2.SQL.Clear;
                      QueryLocal2.Sql.add('Select codigo,ferragemcor,(quantidade*-1) as quantidade from TabFerragem_PP where PedidoProjeto='+fieldbyname('pedidoprojeto').AsString);
                      QueryLocal2.Open;
                      while not QueryLocal2.Eof do
                      begin
                            ObjFerragem_PP.FerragemCor.LocalizaCodigo(QueryLocal2.fieldbyname('ferragemcor').AsString);
                            ObjFerragem_PP.FerragemCor.TabelaparaObjeto;
                            //Preciso ver se essa ferragem tem um estoque valido
                            try
                                  PestoqueAtual:=strtofloat(ObjFerragem_PP.ferragemCor.RetornaEstoque);
                            Except
                                  Messagedlg('Erro no Estoque Atual da ferragem '+ObjFerragem_PP.FerragemCor.Ferragem.Get_Descricao+' da cor '+ObjFerragem_PP.FerragemCor.Cor.Get_Descricao,mterror,[mbok],0);
                                  exit;
                            End;

                            Try
                                PnovoEstoque:=PestoqueAtual+QueryLocal2.fieldbyname('quantidade').asfloat;
                            Except
                                Messagedlg('Erro na gera��o do novo Estoque da ferragem '+ObjFerragem_PP.FerragemCor.Ferragem.Get_Descricao+' da cor '+ObjFerragem_PP.FerragemCor.Cor.Get_Descricao,mterror,[mbok],0);
                                exit;
                            End;

                            //Caso o estoque fique negativo e se n�o pode vender com estoque negativo,
                            //ent�o avisa e n�o deixa gerar nota fiscal
                            if (Pnovoestoque<0)
                            then Begin
                                 Messagedlg('A ferragem '+ObjFerragem_PP.FerragemCor.Ferragem.Get_Descricao+' da cor '+ObjFerragem_PP.FerragemCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                 exit;
                            End;
                            QueryLocal2.Next;
                      end;
                      
                      QueryLocal2.Close;
                      QueryLocal2.SQL.Clear;
                      QueryLocal2.Sql.add('Select codigo,perfiladocor,(quantidade*-1) as quantidade from Tabperfilado_PP where PedidoProjeto='+fieldbyname('pedidoprojeto').AsString);
                      QueryLocal2.Open;
                      while not QueryLocal2.Eof do
                      begin
                           ObjPerfilado_PP.PerfiladoCor.LocalizaCodigo(QueryLocal2.fieldbyname('perfiladocor').AsString);
                           ObjPerfilado_PP.PerfiladoCor.TabelaparaObjeto;

                            try
                                  PestoqueAtual:=strtofloat(ObjPerfilado_PP.PerfiladoCor.RetornaEstoque);
                            Except
                                  Messagedlg('Erro no Estoque Atual do perfilado '+ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_Descricao+' da cor '+ObjPerfilado_PP.PerfiladoCor.Cor.Get_Descricao,mterror,[mbok],0);
                                  exit;
                            End;

                            Try
                                PnovoEstoque:=PestoqueAtual+QueryLocal2.fieldbyname('quantidade').asfloat;
                            Except
                                Messagedlg('Erro na gera��o do novo Estoque do perfilado '+ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_Descricao+' da cor '+ObjPerfilado_PP.PerfiladoCor.Cor.Get_Descricao,mterror,[mbok],0);
                                exit;
                            End;

                            //Caso o estoque fique negativo e se n�o pode vender com estoque negativo,
                            //ent�o avisa e n�o deixa gerar nota fiscal
                            if (Pnovoestoque<0)
                            then Begin
                                 Messagedlg('O perfilado '+ObjPerfilado_PP.PerfiladoCor.Perfilado.Get_Descricao+' da cor '+ObjPerfilado_PP.PerfiladoCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                 exit;
                            End;
                            QueryLocal2.Next;
                      end;

                      QueryLocal2.Close;
                      QueryLocal2.SQL.Clear;
                      QueryLocal2.Sql.add('Select codigo,vidrocor,(quantidade*-1) as quantidade from Tabvidro_PP where PedidoProjeto='+fieldbyname('pedidoprojeto').AsString);
                      QueryLocal2.Open;
                      while not QueryLocal2.Eof do
                      begin
                           ObjVidro_PP.VidroCor.LocalizaCodigo(QueryLocal2.fieldbyname('vidrocor').AsString);
                           ObjVidro_PP.VidroCor.TabelaparaObjeto;

                            try
                                  PestoqueAtual:=strtofloat(ObjVidro_PP.VidroCor.RetornaEstoque);
                            Except
                                  Messagedlg('Erro no Estoque Atual do vidro '+ObjVidro_PP.VidroCor.Vidro.Get_Descricao+' da cor '+ObjVidro_PP.VidroCor.Cor.Get_Descricao,mterror,[mbok],0);
                                  exit;
                            End;

                            Try
                                PnovoEstoque:=PestoqueAtual+QueryLocal2.fieldbyname('quantidade').asfloat;
                            Except
                                Messagedlg('Erro na gera��o do novo Estoque do Vidro '+ObjVidro_PP.VidroCor.Vidro.Get_Descricao+' da cor '+ObjVidro_PP.VidroCor.Cor.Get_Descricao,mterror,[mbok],0);
                                exit;
                            End;

                            //Caso o estoque fique negativo e se n�o pode vender com estoque negativo,
                            //ent�o avisa e n�o deixa gerar nota fiscal
                            if (Pnovoestoque<0)
                            then Begin
                                 Messagedlg('O Vidro '+ObjVidro_PP.VidroCor.Vidro.Get_Descricao+' da cor '+ObjVidro_PP.VidroCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                 exit;
                            End;
                            QueryLocal2.Next;
                      end;

                      QueryLocal2.Close;
                      QueryLocal2.SQL.Clear;
                      QueryLocal2.Sql.add('Select codigo,kitboxcor,(quantidade*-1) as quantidade from Tabkitbox_PP where PedidoProjeto='+fieldbyname('pedidoprojeto').AsString);
                      QueryLocal2.Open;
                      while not QueryLocal2.Eof do
                      begin
                           ObjKitBox_PP.KitBoxCor.LocalizaCodigo(QueryLocal2.fieldbyname('kitboxcor').AsString);
                           ObjKitBox_PP.KitBoxCor.TabelaparaObjeto;

                            try
                                  PestoqueAtual:=strtofloat(ObjKitBox_PP.KitBoxCor.RetornaEstoque);
                            Except
                                  Messagedlg('Erro no Estoque Atual do KitBox '+ObjKitBox_PP.KitBoxCor.KitBox.Get_Descricao+' da cor '+ObjKitBox_PP.KitBoxCor.Cor.Get_Descricao,mterror,[mbok],0);
                                  exit;
                            End;

                            Try
                                PnovoEstoque:=PestoqueAtual+QueryLocal2.fieldbyname('quantidade').asfloat;
                            Except
                                Messagedlg('Erro na gera��o do novo Estoque do KitBox '+ObjKitBox_PP.KitBoxCor.KitBox.Get_Descricao+' da cor '+ObjKitBox_PP.KitBoxCor.Cor.Get_Descricao,mterror,[mbok],0);
                                exit;
                            End;

                            //Caso o estoque fique negativo e se n�o pode vender com estoque negativo,
                            //ent�o avisa e n�o deixa gerar nota fiscal
                            if (Pnovoestoque<0)
                            then Begin
                                 Messagedlg('O KitBox '+ObjKitBox_PP.KitBoxCor.KitBox.Get_Descricao+' da cor '+ObjKitBox_PP.KitBoxCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                 exit;
                            End;
                            QueryLocal2.Next;
                      end;

                      QueryLocal2.Close;
                      QueryLocal2.SQL.Clear;
                      QueryLocal2.Sql.add('Select codigo,diversocor,(quantidade*-1) as quantidade from Tabdiverso_PP where PedidoProjeto='+fieldbyname('pedidoprojeto').AsString);
                      QueryLocal2.Open;
                      while not QueryLocal2.Eof do
                      begin
                           ObjDiverso_PP.DiversoCor.LocalizaCodigo(QueryLocal2.fieldbyname('diversocor').AsString);
                           ObjDiverso_PP.DiversoCor.TabelaparaObjeto;

                            try
                                  PestoqueAtual:=strtofloat(ObjDiverso_PP.DiversoCor.RetornaEstoque);
                            Except
                                  Messagedlg('Erro no Estoque Atual do Diverso '+ObjDiverso_PP.DiversoCor.Diverso.Get_Descricao+' da cor '+ObjDiverso_PP.DiversoCor.Cor.Get_Descricao,mterror,[mbok],0);
                                  exit;
                            End;

                            Try
                                PnovoEstoque:=PestoqueAtual+QueryLocal2.fieldbyname('quantidade').asfloat;
                            Except
                                Messagedlg('Erro na gera��o do novo Estoque do Diverso '+ObjDiverso_PP.DiversoCor.Diverso.Get_Descricao+' da cor '+ObjDiverso_PP.DiversoCor.Cor.Get_Descricao,mterror,[mbok],0);
                                exit;
                            End;

                            //Caso o estoque fique negativo e se n�o pode vender com estoque negativo,
                            //ent�o avisa e n�o deixa gerar nota fiscal
                            if (Pnovoestoque<0)
                            then Begin
                                 Messagedlg('O Diverso '+ObjDiverso_PP.DiversoCor.Diverso.Get_Descricao+' da cor '+ObjDiverso_PP.DiversoCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                 exit;
                            End;
                            QueryLocal2.Next;
                      end;

                      QueryLocal2.Close;
                      QueryLocal2.SQL.Clear;
                      QueryLocal2.Sql.add('Select codigo,persianagrupodiametrocor,(quantidade*-1) as quantidade from tabpersiana_PP where PedidoProjeto='+fieldbyname('pedidoprojeto').AsString);
                      QueryLocal2.Open;
                      while not QueryLocal2.Eof do
                      begin
                           ObjPersiana_PP.PersianaGrupoDiametroCor.LocalizaCodigo(QueryLocal2.fieldbyname('persianagrupodiametrocor').AsString);
                           ObjPersiana_PP.PersianaGrupoDiametroCor.TabelaparaObjeto;

                            try
                                  PestoqueAtual:=strtofloat(ObjPersiana_PP.PersianaGrupoDiametroCor.RetornaEstoque);
                            Except
                                  Messagedlg('Erro no Estoque Atual da Persiana '+ObjPersiana_PP.PersianaGrupoDiametroCor.Persiana.Get_nome+' da cor '+ObjPersiana_PP.PersianaGrupoDiametroCor.Cor.Get_Descricao,mterror,[mbok],0);
                                  exit;
                            End;

                            Try
                                PnovoEstoque:=PestoqueAtual+QueryLocal2.fieldbyname('quantidade').asfloat;
                            Except
                                Messagedlg('Erro na gera��o do novo Estoque da Persiana '+ObjPersiana_PP.PersianaGrupoDiametroCor.Persiana.Get_Nome+' da cor '+ObjPersiana_PP.PersianaGrupoDiametroCor.Cor.Get_Descricao,mterror,[mbok],0);
                                exit;
                            End;

                            //Caso o estoque fique negativo e se n�o pode vender com estoque negativo,
                            //ent�o avisa e n�o deixa gerar nota fiscal
                            if (Pnovoestoque<0)
                            then Begin
                                 Messagedlg('A Persiana '+ObjPersiana_PP.PersianaGrupoDiametroCor.Persiana.Get_Nome+' da cor '+ObjPersiana_PP.PersianaGrupoDiametroCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                 exit;
                            End;
                            QueryLocal2.Next;
                      end;

                      Next;
                 end;

            end;
            result:=True;
       finally
            FreeAndNil(QueryLocal);
            FreeAndNil(QueryLocal2);
       end;

end;

function TObjPedidoObjetos.TotalAcrescimoPedido(pPedido: String): Currency;
var
  qry: TIBQuery;
begin
  Result := 0;
  qry := TIBQuery.Create(nil);
  qry.Database := FDataModulo.IBDatabase;

  try
    try
      qry.Close;
      qry.SQL.Clear;
      qry.SQL.Add('select SUM(VALORACRESCIMO) as VALORACRESCIMO from tabpedido_proj where pedido='+pPedido);
      qry.Open;

      Result := qry.fieldbyname('VALORACRESCIMO').AsCurrency;
    except
      Exit;
    end;
  finally
    FreeAndNil(qry);
  end;
end;

end.




