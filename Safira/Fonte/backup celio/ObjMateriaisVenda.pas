unit ObjMateriaisVenda;

interface

uses Windows, Classes,UdataModulo,IBQuery, IBCustomDataSet,DB,Dialogs,UObjNfeDigitada,UessencialGlobal;

type
    TObjMateriaisVenda = class

          public
                state  :TDataSetState;
                Nfedigitada:TObjNFEDIGITADA;
                constructor  create;
                destructor   free;
                Function Salvar(ComCommit :Boolean):Boolean;
                Function LocalizaCodigo(Parametro :String):Boolean;
                Function Exclui(Pcodigo:string;ComCommit:Boolean):Boolean;
                Function Get_Pesquisa   :string;

                Function TabelaParaObjetos :Boolean;
                procedure ZerarTabela;
                procedure Cancelar;
                Procedure Commit;

                Function Get_NovoCodigo:string;
                Function RetornaCampoCodigo:string;

                procedure Submit_Codigo(parametro:string);
                Function Get_Codigo:string;
                procedure Submit_Pedido(parametro:string);
                Function Get_Pedido:string;
                procedure Submit_Ferragem(parametro:string);
                function Get_Ferragem:String;
                procedure Submit_Vidro(parametro:string);
                Function Get_Vidro:string;
                procedure Submit_Perfilado(parametro:String);
                Function Get_Perfilado:string;
                procedure Submit_Diverso(parametro:string);
                function Get_Diverso:string;
                procedure Submit_Componente(parametro:string);
                function Get_Componente:string;
                procedure Submit_KitBox(parametro:string);
                function Get_KitBox:string;
                procedure Submit_Persiana(parametro:string);
                function Get_Persiana:string;
                function Get_ALIQUOTA:string;
                function Get_REDUCAOBASECALCULO:string;
                function Get_ALIQUOTACUPOM:string;
                function Get_ISENTO:string;
                function Get_SUBSTITUICAOTRIBUTARIA:string;
                function Get_PERCENTUALAGREGADO:string;
                function Get_VALORPAUTA:String;
                function Get_MARGEMVALORAGREGADOCONSUMIDOR:string;
                function Get_CFOP:string;
                function Get_IMPOSTO_ICMS_ORIGEM:string;
                function Get_IMPOSTO_ICMS_DESTINO:string;
                function Get_VALORFRETE:string;
                function Get_VALORSEGURO:string;
                function Get_BC_ICMS:string;
                function Get_VALOR_ICMS:string;
                function Get_BC_ICMS_ST:string;
                function Get_VALOR_ICMS_ST:string;
                function Get_BC_IPI:string;
                function Get_VALOR_IPI:string;
                function Get_BC_PIS:string;
                function Get_VALOR_PIS:string;
                function Get_BC_PIS_ST:String;
                function Get_VALOR_PIS_ST:string;
                function Get_BC_COFINS:string;
                function Get_VALOR_COFINS:String;
                function Get_BC_COFINS_ST:String;
                function Get_VALOR_COFINS_ST:string;
                function Get_IMPOSTO_PIS_ORIGEM:string;
                function Get_IMPOSTO_COFINS_ORIGEM:string;
                function Get_IMPOSTO_PIS_DESTINO:string;
                function Get_IMPOSTO_COFINS_DESTINO:string;

                procedure Submit_ALIQUOTA(parametro:string);
                procedure Submit_REDUCAOBASECALCULO(parametro:string);
                procedure Submit_ALIQUOTACUPOM(parametro:string);
                procedure Submit_ISENTO(parametro:string);
                procedure Submit_SUBSTITUICAOTRIBUTARIA(parametro:string);
                procedure Submit_PERCENTUALAGREGADO(parametro:string);
                procedure Submit_VALORPAUTA(parametro:string);
                procedure Submit_MARGEMVALORAGREGADOCONSUMIDOR(parametro:string);
                procedure Submit_CFOP(parametro:string);
                procedure Submit_MPOSTO_ICMS_ORIGEM(parametro:string);
                procedure Submit_IMPOSTO_ICMS_DESTINO(parametro:string);
                procedure Submit_VALORFRETE(parametro:string);
                procedure Submit_VALORSEGURO(parametro:string);
                procedure Submit_BC_ICMS(parametro:string);
                procedure Submit_VALOR_ICMS(parametro:string);
                procedure Submit_BC_ICMS_ST(parametro:string);
                procedure Submit_VALOR_ICMS_ST(parametro:string);
                procedure Submit_BC_IPI(parametro:string);
                procedure Submit_VALOR_IPI(parametro:string);
                procedure Submit_BC_PIS(parametro:string);
                procedure Submit_VALOR_PIS(parametro:string);
                procedure Submit_BC_PIS_ST(parametro:string);
                procedure Submit_VALOR_PIS_ST(parametro:string);
                procedure Submit_BC_COFINS(parametro:string);
                procedure Submit_VALOR_COFINS(parametro:string);
                procedure Submit_BC_COFINS_ST(parametro:string);
                procedure Submit_VALOR_COFINS_ST(parametro:string);
                procedure Submit_IMPOSTO_PIS_ORIGEM(parametro:string);
                procedure Submit_IMPOSTO_COFINS_ORIGEM(parametro:string);
                procedure Submit_MPOSTO_PIS_DESTINO(parametro:string);
                procedure Submit_IMPOSTO_COFINS_DESTINO(parametro:string);
                procedure Submit_SITUACAOTRIBUTARIA_TABELAA(parametro:string);
                function Get_SITUACAOTRIBUTARIA_TABELAA:string;
                procedure Submit_SITUACAOTRIBUTARIA_TABELAB(parametro:String);
                function Get_SITUACAOTRIBUTARIA_TABELAB:string;
                procedure Submit_IMPOSTO_IPI(parametro:string);
                function Get_IMPOSTO_IPI:string;
                procedure Submit_QUANTIDADE(parametro:string);
                function Get_QUANTIDADE:string;
                procedure Submit_VALORFINAL(parametro:string);
                function Get_VALORFINAL:string;
                procedure Submit_Descricao(parametro:string);
                function Get_Descricao:string;
                function Get_ValorUnitario:string;
                procedure Submit_ValorUnitario(parametro:string);
                function Get_Unidade:string;
                procedure Submit_Unidade(parametro:String);
                function Get_referencia:string;
                procedure Submit_Referencia(parametro:string);
                function Get_Material:string;
                procedure Submit_Material(parametro:string);
                procedure Submit_NotaFiscal(parametro:String);
                function Get_NotaFiscal:string;
                procedure Submit_classificacaofiscal(parametro:string);
                function Get_classificacaofiscal:string;
                function Get_COR:string;
                procedure Submit_Cor(parametro:string);
                procedure Submit_PesoUnitario(parametro:String);
                function Get_PesoUnitario:string;
                procedure Submit_NCM(parametro:string);
                function Get_NCM:string;
                procedure Submit_Desconto(parametro:string);
                function Get_Desconto:string;

                procedure submit_OBSERVACAO(parametro:string);
                function get_OBSERVACAO():string;

                procedure submit_CSOSN(parametro:string);
                function get_CSOSN():string;

                procedure Submit_Projeto(parametro:string);
                function Get_Projeto:string;
                procedure Submit_CodigoCor(parametro:string);
                Function Get_CodigoCor:string;

                procedure Submit_Acrescimo(parametro:string);
                function Get_Acrescimo:string;
          private
                ObjData:TIBDataSet;
                Query:TIBQuery;
                CODIGO                         :String;
                PEDIDO                         :String;
                FERRAGEM                       :String;
                VIDRO                          :String;
                PERFILADO                      :String;
                DIVERSO                        :String;
                COMPONENTE                     :String;
                KITBOX                         :String;
                PERSIANA                       :String;
                QUANTIDADE                     :String;
                VALORFINAL                     :String;
                VALORUNITARIO                  :String;
                DESCRICAO                      :String;
                UNIDADE                        :String;
                MATERIAL                       :String;
                ALIQUOTA                       :String;
                REDUCAOBASECALCULO             :String;
                ALIQUOTACUPOM                  :String;
                ISENTO                         :String;
                SUBSTITUICAOTRIBUTARIA         :String;
                SITUACAOTRIBUTARIA_TABELAA     :String;
                SITUACAOTRIBUTARIA_TABELAB     :String;
                PERCENTUALAGREGADO             :String;
                VALORPAUTA                     :String;
                MARGEMVALORAGREGADOCONSUMIDOR  :String;
                CFOP                           :String;
                IMPOSTO_ICMS_ORIGEM            :String;
                IMPOSTO_ICMS_DESTINO           :String;
                VALORFRETE                     :String;
                VALORSEGURO                    :String;
                BC_ICMS                        :String;
                VALOR_ICMS                     :String;
                BC_ICMS_ST                     :String;
                VALOR_ICMS_ST                  :String;
                BC_IPI                         :String;
                VALOR_IPI                      :String;
                BC_PIS                         :String;
                VALOR_PIS                      :String;
                BC_PIS_ST                      :String;
                VALOR_PIS_ST                   :String;
                BC_COFINS                      :String;
                VALOR_COFINS                   :String;
                BC_COFINS_ST                   :String;
                VALOR_COFINS_ST                :String;
                IMPOSTO_PIS_ORIGEM             :String;
                IMPOSTO_COFINS_ORIGEM          :String;
                IMPOSTO_PIS_DESTINO            :String;
                IMPOSTO_COFINS_DESTINO         :String;
                REFERENCIA                     :String;
                IMPOSTO_IPI                    :String;
                NOTAFISCAL                     :string;
                classificacaofiscal            :string;
                COR                            :string;
                PesoUnitario                   :string;
                NCM                            :string;
                DESCONTO                       :string;
                OBSERVACAO                     :string;
                CSOSN                          :string;
                PROJETO                        :string;
                CODIGOCOR                      :string;
                // 11/02/2013 Celio
                ACRESCIMO                      :string;
                //fim celio
                
                Function VerificaBrancos:Boolean;
                Function VerificaRelacionamentos:Boolean;
                Function VerificaNumericos:Boolean;
                procedure ObjetoParaTabela;

    end;



implementation

uses TypInfo, SysUtils;


constructor TObjMateriaisVenda.create;
begin
    ObjData:=TIBDataSet.Create(nil);
    objdata.Database:=FDataModulo.IBDatabase;
    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;
    self.Nfedigitada:=TObjNFEDIGITADA.Create;

    with ObjData do
    begin
          SelectSQL.Clear;
          SelectSQL.Add('select codigo, pedido,ferragem,vidro, perfilado,diverso, componente, kitbox,persiana,quantidade,valorfinal,valorunitario,descricao,unidade,');
          SelectSQL.Add('material,aliquota,reducaobasecalculo,aliquotacupom,isento,SUBSTITUICAOTRIBUTARIA,SITUACAOTRIBUTARIA_TABELAA,SITUACAOTRIBUTARIA_TABELAB,PERCENTUALAGREGADO,');
          SelectSQL.Add('VALORPAUTA,MARGEMVALORAGREGADOCONSUMIDOR,CFOP,IMPOSTO_ICMS_ORIGEM,IMPOSTO_ICMS_DESTINO,VALORFRETE,VALORSEGURO,BC_ICMS,VALOR_ICMS,BC_ICMS_ST,VALOR_ICMS_ST,');
          SelectSQL.Add('BC_IPI,VALOR_IPI,BC_PIS,VALOR_PIS,BC_PIS_ST,VALOR_PIS_ST,BC_COFINS,VALOR_COFINS,BC_COFINS_ST,VALOR_COFINS_ST,IMPOSTO_COFINS_ORIGEM,IMPOSTO_PIS_ORIGEM,');
          SelectSQL.Add('IMPOSTO_PIS_DESTINO,IMPOSTO_COFINS_DESTINO,REFERENCIA,IMPOSTO_IPI,classificacaofiscal,notafiscal,cor,pesounitario,NCM,DESCONTO,OBSERVACAO,Nfedigitada,CSOSN,');
          SelectSQL.Add('PROJETO,cor_codigo,ACRESCIMO from tabmateriaisvenda where codigo=0');

          InsertSQL.Clear;
          InsertSQL.Add('insert into tabmateriaisvenda(codigo, pedido,ferragem,vidro, perfilado,diverso, componente, kitbox,persiana,quantidade,valorfinal,valorunitario,descricao,unidade,');
          InsertSQL.Add('material,aliquota,reducaobasecalculo,aliquotacupom,isento,SUBSTITUICAOTRIBUTARIA,SITUACAOTRIBUTARIA_TABELAA,SITUACAOTRIBUTARIA_TABELAB,PERCENTUALAGREGADO,');
          InsertSQL.Add('VALORPAUTA,MARGEMVALORAGREGADOCONSUMIDOR,CFOP,IMPOSTO_ICMS_ORIGEM,IMPOSTO_ICMS_DESTINO,VALORFRETE,VALORSEGURO,BC_ICMS,VALOR_ICMS,BC_ICMS_ST,VALOR_ICMS_ST,');
          InsertSQL.Add('BC_IPI,VALOR_IPI,BC_PIS,VALOR_PIS,BC_PIS_ST,VALOR_PIS_ST,BC_COFINS,VALOR_COFINS,BC_COFINS_ST,VALOR_COFINS_ST,IMPOSTO_COFINS_ORIGEM,IMPOSTO_PIS_ORIGEM,');
          InsertSQL.Add('IMPOSTO_PIS_DESTINO,IMPOSTO_COFINS_DESTINO,REFERENCIA,IMPOSTO_IPI,classificacaofiscal,notafiscal,cor,pesounitario,NCM,DESCONTO,OBSERVACAO,Nfedigitada,CSOSN,PROJETO,cor_codigo,');
          InsertSQL.Add('ACRESCIMO)');
          InsertSQL.Add('values(:codigo,:pedido,:ferragem,:vidro, :perfilado,:diverso, :componente, :kitbox,:persiana,:quantidade,:valorfinal,:valorunitario,:descricao,:unidade,');
          InsertSQL.Add(':material,:aliquota,:reducaobasecalculo,:aliquotacupom,:isento,:SUBSTITUICAOTRIBUTARIA,:SITUACAOTRIBUTARIA_TABELAA,:SITUACAOTRIBUTARIA_TABELAB,:PERCENTUALAGREGADO,');
          InsertSQL.Add(':VALORPAUTA,:MARGEMVALORAGREGADOCONSUMIDOR,:CFOP,:IMPOSTO_ICMS_ORIGEM,:IMPOSTO_ICMS_DESTINO,:VALORFRETE,:VALORSEGURO,:BC_ICMS,:VALOR_ICMS,:BC_ICMS_ST,:VALOR_ICMS_ST,');
          InsertSQL.Add(':BC_IPI,:VALOR_IPI,:BC_PIS,:VALOR_PIS,:BC_PIS_ST,:VALOR_PIS_ST,:BC_COFINS,:VALOR_COFINS,:BC_COFINS_ST,:VALOR_COFINS_ST,:IMPOSTO_COFINS_ORIGEM,:IMPOSTO_PIS_ORIGEM,');
          InsertSQL.Add(':IMPOSTO_PIS_DESTINO,:IMPOSTO_COFINS_DESTINO,:REFERENCIA,:IMPOSTO_IPI,:classificacaofiscal,:notafiscal,:cor,:pesounitario,:NCM,:DESCONTO,:OBSERVACAO,:Nfedigitada,:CSOSN,:PROJETO,:cor_codigo,');
          InsertSQL.Add(':ACRESCIMO)');

          ModifySQL.Clear;
          ModifySQL.Add('update tabmateriaisvenda set codigo=:codigo, pedido=:pedido,ferragem=:ferragem,vidro=:vidro, perfilado=:perfilado,diverso=:diverso, componente=:componente,');
          ModifySQL.Add('kitbox=:kitbox,persiana=:persiana,quantidade=:quantidade,valorfinal=:valorfinal,valorunitario=:valorunitario,descricao=:descricao,unidade=:unidade,');
          ModifySQL.Add('material=:material,aliquota=:aliquoto,reducaobasecalculo=:reducaobasecalculo,aliquotacupom=:aliquotacupom,isento=:isento,SUBSTITUICAOTRIBUTARIA=:SUBSTITUICAOTRIBUTARIA,');
          ModifySQL.Add('SITUACAOTRIBUTARIA_TABELAA=:SITUACAOTRIBUTARIA_TABELAA,SITUACAOTRIBUTARIA_TABELAB=:SITUACAOTRIBUTARIA_TABELAB,PERCENTUALAGREGADO=:PERCENTUALAGREGADO,');
          ModifySQL.Add('VALORPAUTA=:VALORPAUTA,MARGEMVALORAGREGADOCONSUMIDOR=:MARGEMVALORAGREGADOCONSUMIDOR,CFOP=:CFOP,IMPOSTO_ICMS_ORIGEM=:IMPOSTO_ICMS_ORIGEM,');
          ModifySQL.Add('IMPOSTO_ICMS_DESTINO=:IMPOSTO_ICMS_DESTINO,VALORFRETE=:VALORFRETE,VALORSEGURO=:VALORSEGURO,BC_ICMS=:BC_ICMS,VALOR_ICMS=:VALOR_ICMS,BC_ICMS_ST=:BC_ICMS_ST,VALOR_ICMS_ST=:VALOR_ICMS_ST,');
          ModifySQL.Add('BC_IPI=:BC_IPI,VALOR_IPI=:VALOR_IPI,BC_PIS=:BC_PIS,VALOR_PIS=:VALOR_PIS,BC_PIS_ST=:BC_PIS_ST,VALOR_PIS_ST=:VALOR_PIS_ST,BC_COFINS=:BC_COFINS,VALOR_COFINS=:VALOR_COFINS,');
          ModifySQL.Add('BC_COFINS_ST=:BC_COFINS_ST,VALOR_COFINS_ST=:VALOR_COFINS_ST,IMPOSTO_COFINS_ORIGEM=:IMPOSTO_COFINS_ORIGEM,IMPOSTO_PIS_ORIGEM=:IMPOSTO_PIS_ORIGEM,');
          ModifySQL.Add('IMPOSTO_PIS_DESTINO=:IMPOSTO_PIS_DESTINO,IMPOSTO_COFINS_DESTINO=:IMPOSTO_COFINS_DESTINO,REFERENCIA=:REFERENCIA,IMPOSTO_IPI=:IMPOSTO_IPI,classificacaofiscal=:classificacaofiscal,notafiscal=:notafiscal');
          ModifySQL.Add(',cor=:cor,pesounitario=:pesounitario,NCM=:NCM,DESCONTO=:DESCONTO,OBSERVACAO=:OBSERVACAO,Nfedigitada=:Nfedigitada,CSOSN=:CSOSN,PROJETO=:PROJETO,cor_codigo=:cor_codigo,');
          ModifySQL.Add('ACRESCIMO=:ACRESCIMO where codigo=:codigo ');


          DeleteSQL.Clear;
          DeleteSQL.Add('delete from tabmateriaisvenda where codigo=:codigo');

          RefreshSQL.Clear;
          RefreshSQL.Add('select codigo, pedido,ferragem,vidro, perfilado,diverso, componente, kitbox,persiana,quantidade,valorfinal,valorunitario,descricao,unidade,');
          RefreshSQL.Add('material,aliquota,reducaobasecalculo,aliquotacupom,isento,SUBSTITUICAOTRIBUTARIA,SITUACAOTRIBUTARIA_TABELAA,SITUACAOTRIBUTARIA_TABELAB,PERCENTUALAGREGADO,');
          RefreshSQL.Add('VALORPAUTA,MARGEMVALORAGREGADOCONSUMIDOR,CFOP,IMPOSTO_ICMS_ORIGEM,IMPOSTO_ICMS_DESTINO,VALORFRETE,VALORSEGURO,BC_ICMS,VALOR_ICMS,BC_ICMS_ST,VALOR_ICMS_ST,');
          RefreshSQL.Add('BC_IPI,VALOR_IPI,BC_PIS,VALOR_PIS,BC_PIS_ST,VALOR_PIS_ST,BC_COFINS,VALOR_COFINS,BC_COFINS_ST,VALOR_COFINS_ST,IMPOSTO_COFINS_ORIGEM,IMPOSTO_PIS_ORIGEM,');
          RefreshSQL.Add('IMPOSTO_PIS_DESTINO,IMPOSTO_COFINS_DESTINO,REFERENCIA,IMPOSTO_IPI,classificacaofiscal,notafiscal,cor,pesounitario,NCM,DESCONTO,OBSERVACAO,Nfedigitada,CSOSN,');
          RefreshSQL.Add('PROJETO,cor_codigo,ACRESCIMO from tabmateriaisvenda where codigo is null');

          Open;
          Self.state:=dsInactive;

    end;

end;

destructor TObjMateriaisVenda.free;
begin
    FreeAndNil(query);
    FreeAndNil(Self.objdata);
    FreeAndNil(self.nfedigitada);

end;

procedure TObjMateriaisVenda.ZerarTabela;
begin
     CODIGO                                    :='';
                PEDIDO                         :='';
                FERRAGEM                       :='';
                VIDRO                          :='';
                PERFILADO                      :='';
                DIVERSO                        :='';
                COMPONENTE                     :='';
                KITBOX                         :='';
                PERSIANA                       :='';
                QUANTIDADE                     :='';
                VALORFINAL                     :='';
                VALORUNITARIO                  :='';
                DESCRICAO                      :='';
                UNIDADE                        :='';
                MATERIAL                       :='';
                ALIQUOTA                       :='';
                REDUCAOBASECALCULO             :='';
                ALIQUOTACUPOM                  :='';
                ISENTO                         :='';
                SUBSTITUICAOTRIBUTARIA         :='';
                IMPOSTO_IPI                    :='';
                SITUACAOTRIBUTARIA_TABELAA     :='';
                SITUACAOTRIBUTARIA_TABELAB     :='';
                PERCENTUALAGREGADO             :='';
                VALORPAUTA                     :='';
                MARGEMVALORAGREGADOCONSUMIDOR  :='';
                CFOP                           :='';
                IMPOSTO_ICMS_ORIGEM            :='';
                IMPOSTO_ICMS_DESTINO           :='';
                VALORFRETE                     :='';
                VALORSEGURO                    :='';
                BC_ICMS                        :='';
                VALOR_ICMS                     :='';
                BC_ICMS_ST                     :='';
                VALOR_ICMS_ST                  :='';
                BC_IPI                         :='';
                VALOR_IPI                      :='';
                BC_PIS                         :='';
                VALOR_PIS                      :='';
                BC_PIS_ST                      :='';
                VALOR_PIS_ST                   :='';
                BC_COFINS                      :='';
                VALOR_COFINS                   :='';
                BC_COFINS_ST                   :='';
                VALOR_COFINS_ST                :='';
                IMPOSTO_PIS_ORIGEM             :='';
                IMPOSTO_COFINS_ORIGEM          :='';
                IMPOSTO_PIS_DESTINO            :='';
                IMPOSTO_COFINS_DESTINO         :='';
                REFERENCIA                     :='';
                IMPOSTO_IPI                    :='';
                notafiscal                     :='';
                classificacaofiscal            :='';
                COR                            :='';
                PesoUnitario                   :='';
                NCM                            :='';
                DESCONTO                       :='';
                OBSERVACAO                     :='';
                CSOSN                          :='';
                PROJETO                        :='';
                codigocor:='';
                self.Nfedigitada.ZerarTabela;
                ACRESCIMO := '';

end;

procedure TObjMateriaisVenda.Cancelar;
begin
    FDataModulo.IBTransaction.RollbackRetaining;
end;

procedure TObjMateriaisVenda.Commit;
begin
    FDataModulo.IBTransaction.CommitRetaining;
end;

procedure TObjMateriaisVenda.Submit_Codigo(parametro:string);
begin
    self.Codigo:=parametro;
end;

procedure TObjMateriaisVenda.Submit_Pedido(parametro:string);
begin
    Self.PEDIDO:=parametro;
end;

procedure TObjMateriaisVenda.Submit_Ferragem(parametro:string);
begin
    Self.FERRAGEM:=parametro;
end;

procedure TObjMateriaisVenda.Submit_Vidro(parametro:String);
begin
    self.VIDRO:=parametro;
end;

procedure TObjMateriaisVenda.Submit_Perfilado(parametro:string);
begin
    self.PERFILADO:=parametro;
end;

procedure TObjMateriaisVenda.Submit_Diverso(parametro:string);
begin
    self.DIVERSO:=parametro;
end;

procedure TObjMateriaisVenda.Submit_Componente(parametro:String);
begin
    Self.COMPONENTE:=parametro;
end;

procedure TObjMateriaisVenda.Submit_KitBox(parametro:string);
begin
    Self.KITBOX:=parametro;
end;

procedure TObjMateriaisVenda.Submit_Persiana(parametro:string);
begin
    Self.PERSIANA:=parametro;
end;
procedure TObjMateriaisVenda.Submit_SITUACAOTRIBUTARIA_TABELAA(parametro:string);
begin
    self.SITUACAOTRIBUTARIA_TABELAA:=parametro;
end;

procedure TObjMateriaisVenda.Submit_SITUACAOTRIBUTARIA_TABELAB(parametro:string);
begin
     self.SITUACAOTRIBUTARIA_TABELAB:=parametro;
end;

procedure TObjMateriaisVenda.Submit_QUANTIDADE(parametro:string);
begin
    self.QUANTIDADE:=parametro;
end;

procedure TObjMateriaisVenda.Submit_VALORFINAL(parametro:string);
begin
    self.VALORFINAL:=parametro;
end;

procedure TObjMateriaisVenda.Submit_CFOP(parametro:string);
begin
    self.CFOP:=parametro;
end;

procedure TObjMateriaisVenda.ObjetoParaTabela;
begin
     with ObjData do
     begin
             FieldByName('Codigo').AsString                           :=Codigo;
             FieldByName('PEDIDO').AsString                           :=PEDIDO;
             FieldByName('FERRAGEM').AsString                         :=FERRAGEM;
             FieldByName('VIDRO').AsString                            :=VIDRO;
             FieldByName('PERFILADO').AsString                        :=PERFILADO;
             FieldByName('DIVERSO').AsString                          :=DIVERSO;
             FieldByName('COMPONENTE').AsString                       :=COMPONENTE;
             FieldByName('KITBOX').AsString                           :=KITBOX;
             FieldByName('PERSIANA').AsString                         :=PERSIANA;
             FieldByName('ALIQUOTA').AsString                         :=ALIQUOTA;
             FieldByName('REDUCAOBASECALCULO').AsString               :=REDUCAOBASECALCULO;
             FieldByName('ALIQUOTACUPOM').AsString                    :=ALIQUOTACUPOM;
             FieldByName('ISENTO').AsString                           :=ISENTO;
             FieldByName('SUBSTITUICAOTRIBUTARIA').AsString           :=SUBSTITUICAOTRIBUTARIA;
             FieldByName('PERCENTUALAGREGADO').AsString               :=PERCENTUALAGREGADO;
             FieldByName('VALORPAUTA').AsString                       :=VALORPAUTA;
             FieldByName('MARGEMVALORAGREGADOCONSUMIDOR').AsString    :=MARGEMVALORAGREGADOCONSUMIDOR;
             FieldByName('CFOP').AsString                             :=CFOP;
             FieldByName('IMPOSTO_ICMS_ORIGEM').AsString              :=IMPOSTO_ICMS_ORIGEM;
             FieldByName('IMPOSTO_ICMS_DESTINO').AsString             :=IMPOSTO_ICMS_DESTINO;
             FieldByName('VALORFRETE').AsString                       :=VALORFRETE;
             FieldByName('VALORSEGURO').AsString                      :=VALORSEGURO;
             FieldByName('BC_ICMS').AsString                          :=BC_ICMS;
             FieldByName('VALOR_ICMS').AsString                       :=VALOR_ICMS;
             FieldByName('BC_ICMS_ST').AsString                       :=BC_ICMS_ST;
             FieldByName('VALOR_ICMS_ST').AsString                    :=VALOR_ICMS_ST;
             FieldByName('BC_IPI').AsString                           :=BC_IPI;
             FieldByName('VALOR_IPI').AsString                        :=VALOR_IPI;
             FieldByName('BC_PIS').AsString                           :=BC_PIS;
             FieldByName('VALOR_PIS').AsString                        :=VALOR_PIS;
             FieldByName('BC_PIS_ST').AsString                        :=BC_PIS_ST;
             FieldByName('VALOR_PIS_ST').AsString                     :=VALOR_PIS_ST;
             FieldByName('BC_COFINS').AsString                        :=BC_COFINS;
             FieldByName('VALOR_COFINS').AsString                     :=VALOR_COFINS;
             FieldByName('BC_COFINS_ST').AsString                     :=BC_COFINS_ST;
             FieldByName('VALOR_COFINS_ST').AsString                  :=VALOR_COFINS_ST;
             FieldByName('IMPOSTO_PIS_ORIGEM').AsString               :=IMPOSTO_PIS_ORIGEM;
             FieldByName('IMPOSTO_COFINS_ORIGEM').AsString            :=IMPOSTO_COFINS_ORIGEM;
             FieldByName('IMPOSTO_PIS_DESTINO').AsString              :=IMPOSTO_PIS_DESTINO;
             FieldByName('IMPOSTO_COFINS_DESTINO').AsString           :=IMPOSTO_COFINS_DESTINO;
             FieldByName('SITUACAOTRIBUTARIA_TABELAA').AsString       :=SITUACAOTRIBUTARIA_TABELAA;
             FieldByName('SITUACAOTRIBUTARIA_TABELAB').AsString       :=SITUACAOTRIBUTARIA_TABELAB;
             FieldByName('IMPOSTO_IPI').AsString                      :=IMPOSTO_IPI;
             FieldByName('QUANTIDADE').AsString                       :=QUANTIDADE;
             FieldByName('VALORFINAL').AsString                       :=VALORFINAL;
             FieldByName('CFOP').AsString                             :=CFOP;
             FieldByName('VALORUNITARIO').AsString                    :=VALORUNITARIO;
             FieldByName('descricao').AsString                        :=DESCRICAO;
             FieldByName('unidade').AsString                          :=UNIDADE;
             FieldByName('referencia').AsString                       :=REFERENCIA;
             FieldByName('material').asstring                         :=MATERIAL;
             FieldByName('notafiscal').AsString                       :=notafiscal;
             FieldByName('classificacaofiscal').AsString              :=classificacaofiscal;
             FieldByName('cor').AsString                              :=COR;
             FieldByName('pesounitario').AsString                     :=PesoUnitario;
             FieldByName('NCM').AsString                              :=NCM;
             FieldByName('DESCONTO').AsString                         :=tira_ponto(DESCONTO);
             FieldByName('OBSERVACAO').AsString                       :=OBSERVACAO;
             FieldByName('CSOSN').AsString                            :=CSOSN;
             FieldByName('Nfedigitada').AsString                      :=self.Nfedigitada.Get_CODIGO;
             FieldByName('PROJETO').AsString:=self.PROJETO;
             FieldByName('cor_codigo').asstring:=Self.CODIGOCOR;
             FieldByName('ACRESCIMO').AsString                        :=tira_ponto(ACRESCIMO);


     end;
end;

function TObjMateriaisVenda.Salvar(ComCommit:boolean):Boolean;
begin
    with self do
    begin
       { if(VerificaBrancos=False)
        then begin
            result:=false;
            exit;
        end;

        if(VerificaNumericos=False)
        then begin
            result:=false;
            exit;
        end;

        if(VerificaData=False)
        then begin
            result:=false;
            exit;
        end;

        if(VerificaFaixa=False)
        then begin
            result:=false;
            exit;
        end;}

        if(LocalizaCodigo(codigo)=false)
        then begin
              if(State=dsEdit)
              then begin
                   //avisa que o registro n�o foi encotrado pra edi��o
                   result:=false;
                   exit;
              end;
        end
        else
        begin
              if(State=dsInsert)
              then begin
                   //avisa q registro esta duplicado
                   result:=false;
                   exit;

              end;
        end;

       if(State=dsInsert)
       then begin
              ObjData.Insert;
       end
       else
       begin
            if(State=dsedit)
            then begin
                ObjData.Edit;
            end
            else
            begin
                //n�o esta setado pra edi��o e nein pra inser��o
            end;
       end;
       ObjetoParaTabela;
       ObjData.Post;
       if(ComCommit =True)
       Then FDataModulo.IBTransaction.CommitRetaining;

       State:=dsInactive;
       result:=true;

    end;
end;

function TObjMateriaisVenda.LocalizaCodigo(parametro:string):Boolean;
begin
    with Self.ObjData do
    Begin
        close;

        SelectSQL.Clear;
        SelectSQL.Add('select codigo, pedido,ferragem,vidro, perfilado,diverso, componente, kitbox,persiana,quantidade,valorfinal,valorunitario,descricao,unidade,');
        SelectSQL.Add('material,aliquota,reducaobasecalculo,aliquotacupom,isento,SUBSTITUICAOTRIBUTARIA,SITUACAOTRIBUTARIA_TABELAA,SITUACAOTRIBUTARIA_TABELAB,PERCENTUALAGREGADO,');
        SelectSQL.Add('VALORPAUTA,MARGEMVALORAGREGADOCONSUMIDOR,CFOP,IMPOSTO_ICMS_ORIGEM,IMPOSTO_ICMS_DESTINO,VALORFRETE,VALORSEGURO,BC_ICMS,VALOR_ICMS,BC_ICMS_ST,VALOR_ICMS_ST,');
        SelectSQL.Add('BC_IPI,VALOR_IPI,BC_PIS,VALOR_PIS,BC_PIS_ST,VALOR_PIS_ST,BC_COFINS,VALOR_COFINS,BC_COFINS_ST,VALOR_COFINS_ST,IMPOSTO_COFINS_ORIGEM,IMPOSTO_PIS_ORIGEM,');
        SelectSQL.Add('IMPOSTO_PIS_DESTINO,IMPOSTO_COFINS_DESTINO,REFERENCIA,IMPOSTO_IPI,classificacaofiscal,notafiscal,cor,pesounitario,NCM ');
        SelectSQL.Add(',DESCONTO,OBSERVACAO,Nfedigitada,CSOSN,PROJETO,cor_codigo,ACRESCIMO from tabmateriaisvenda');
        SelectSQL.Add('where codigo ='+Parametro);

        Open;
         If (recordcount>0)
         Then Result:=True
         Else Result:=False;
    end;
end;

function TObjMateriaisVenda.Exclui(Pcodigo:string;ComCommit:boolean):Boolean;
begin
      with self do
      begin
            try
                    result:=true;
                    if(LocalizaCodigo(Pcodigo)=true)
                    then begin
                          ObjData.Delete;
                         if(ComCommit=True)
                            then FDataModulo.IBTransaction.CommitRetaining;
                    end
                    else result:=false;
            except
                    result:=false;
            end;
      end;
end;

function TObjMateriaisVenda.Get_Pesquisa:string;
begin
    Result:='select *  from tabmateriaisvenda';
end;

function TObjMateriaisVenda.TabelaParaObjetos:Boolean;
begin
      with ObjData do
      begin
             Codigo               :=FieldByName('Codigo').AsString;
             PEDIDO               :=FieldByName('PEDIDO').AsString;
             FERRAGEM             :=FieldByName('FERRAGEM').AsString;
             VIDRO                :=FieldByName('VIDRO').AsString;
             PERFILADO            :=FieldByName('PERFILADO').AsString;
             DIVERSO              :=FieldByName('DIVERSO').AsString;
             COMPONENTE           :=FieldByName('COMPONENTE').AsString;
             KITBOX               :=FieldByName('KITBOX').AsString;
             PERSIANA             :=FieldByName('PERSIANA').AsString;
             ALIQUOTA             :=FieldByName('ALIQUOTA').AsString;
             REDUCAOBASECALCULO   :=FieldByName('REDUCAOBASECALCULO').AsString;
             ALIQUOTACUPOM   :=FieldByName('ALIQUOTACUPOM').AsString;
             ISENTO               :=FieldByName('ISENTO').AsString;
             SUBSTITUICAOTRIBUTARIA:=FieldByName('SUBSTITUICAOTRIBUTARIA').AsString;
             PERCENTUALAGREGADO   :=FieldByName('PERCENTUALAGREGADO').AsString;
             VALORPAUTA           :=FieldByName('VALORPAUTA').AsString;
             MARGEMVALORAGREGADOCONSUMIDOR:=FieldByName('MARGEMVALORAGREGADOCONSUMIDOR').AsString;
             CFOP                         :=FieldByName('CFOP').AsString;
             IMPOSTO_ICMS_ORIGEM          :=FieldByName('IMPOSTO_ICMS_ORIGEM').AsString;
             IMPOSTO_ICMS_DESTINO         :=FieldByName('IMPOSTO_ICMS_DESTINO').AsString;
             VALORFRETE                   :=FieldByName('VALORFRETE').AsString;
             VALORSEGURO                  :=FieldByName('VALORSEGURO').AsString;
             BC_ICMS                      :=FieldByName('BC_ICMS').AsString ;
             VALOR_ICMS                   :=FieldByName('VALOR_ICMS').AsString;
             BC_ICMS_ST                   :=FieldByName('BC_ICMS_ST').AsString;
             VALOR_ICMS_ST                :=FieldByName('VALOR_ICMS_ST').AsString;
             BC_IPI                       :=FieldByName('BC_IPI').AsString;
             VALOR_IPI                    :=FieldByName('VALOR_IPI').AsString;
             BC_PIS                       :=FieldByName('BC_PIS').AsString;
             VALOR_PIS                    :=FieldByName('VALOR_PIS').AsString;
             BC_PIS_ST                    :=FieldByName('BC_PIS_ST').AsString;
             VALOR_PIS_ST                 :=FieldByName('VALOR_PIS_ST').AsString;
             BC_COFINS                    :=FieldByName('BC_COFINS').AsString;
             VALOR_COFINS                 :=FieldByName('VALOR_COFINS').AsString;
             BC_COFINS_ST                 :=FieldByName('BC_COFINS_ST').AsString;
             VALOR_COFINS_ST              :=FieldByName('VALOR_COFINS_ST').AsString;
             IMPOSTO_PIS_ORIGEM           :=FieldByName('IMPOSTO_PIS_ORIGEM').AsString;
             IMPOSTO_COFINS_ORIGEM        :=FieldByName('IMPOSTO_COFINS_ORIGEM').AsString;
             IMPOSTO_PIS_DESTINO          :=FieldByName('IMPOSTO_PIS_DESTINO').AsString;
             IMPOSTO_COFINS_DESTINO       :=FieldByName('IMPOSTO_COFINS_DESTINO').AsString;
             SITUACAOTRIBUTARIA_TABELAA   :=FieldByName('SITUACAOTRIBUTARIA_TABELAA').AsString;
             SITUACAOTRIBUTARIA_TABELAB   :=FieldByName('SITUACAOTRIBUTARIA_TABELAB').AsString;
             IMPOSTO_IPI                  :=FieldByName('IMPOSTO_IPI').AsString;
             QUANTIDADE                   :=FieldByName('QUANTIDADE').AsString;
             VALORFINAL                   :=FieldByName('VALORFINAL').AsString;
             CFOP                         :=FieldByName('CFOP').AsString;
             VALORUNITARIO                :=FieldByName('VALORUNITARIO').AsString;
             DESCRICAO                    :=FieldByName('descricao').AsString;
             UNIDADE                      :=FieldByName('unidade').AsString;
             REFERENCIA                   :=FieldByName('referencia').AsString;
             MATERIAL                     :=FieldByName('material').asstring;
             notafiscal                   :=fieldbyname('notafiscal').AsString;
             classificacaofiscal          :=fieldbyname('classificacaofiscal').AsString;
             COR                          :=fieldbyname('cor').AsString;
             PesoUnitario                 :=fieldbyname('pesounitario').AsString;
             NCM                          :=fieldbyname('NCM').AsString;
             DESCONTO                     :=fieldbyname('DESCONTO').AsString;
             OBSERVACAO                   :=fieldbyname('OBSERVACAO').AsString;
             CSOSN                        :=fieldbyname('CSOSN').AsString;
             PROJETO:=fieldbyname('PROJETO').AsString;
             CODIGOCOR:=fieldbyname('cor_codigo').AsString;

             If(FieldByName('NFEDIGITADA').asstring<>'') then
             begin

                If (Self.NFEDIGITADA.LocalizaCodigo(FieldByName('NFEDIGITADA').asstring)=False) then
                Begin
                      Messagedlg('NFEDIGITADA N�o encontrado(a)!',mterror,[mbok],0);
                      Self.ZerarTabela;
                      result:=False;
                      exit;
                End
                Else Self.NFEDIGITADA.TabelaparaObjeto;

             End;

             ACRESCIMO := fieldbyname('ACRESCIMO').AsString;


      end;
end;

function TObjMateriaisVenda.Get_NovoCodigo:string;
var
  ObjQuery:TIBQuery;
  codigo:Integer;
begin
    try
      ObjQuery:=TIBQuery.Create(nil);
      ObjQuery.Database:=FDataModulo.IBDatabase;
    Except

    end;

    try
           with ObjQuery do
           begin
                    Close;
                    sql.Clear;
                    sql.Add('select codigo from tabmateriaisvenda order by codigo');
                    Open;
                    Last;
                    if(recordcount =0)
                    then begin
                           codigo:=1;
                    end
                    else
                    begin
                           codigo:=fieldbyname('codigo').AsInteger;
                           Inc(codigo,1);
                    end;
                    result:=IntToStr(codigo);
           end;
    finally
           FreeAndNil(ObjQuery);
    end;



end;

function TObjMateriaisVenda.RetornaCampoCodigo:string;
begin
    result:=self.Codigo;
end;

function TObjMateriaisVenda.Get_Codigo:string;
begin
    result:=self.Codigo;
end;

function TObjMateriaisVenda.Get_Pedido:string;
begin
    result:=self.PEDIDO;
end;

function TObjMateriaisVenda.Get_Ferragem:string;
begin
    result:=self.FERRAGEM;
end;

function TObjMateriaisVenda.Get_Vidro:string;
begin
    result:=Self.VIDRO;
end;

function TObjMateriaisVenda.Get_Perfilado:string;
begin
    result:=Self.PERFILADO;
end;

function TObjMateriaisVenda.Get_Diverso:string;
begin
    result:=Self.DIVERSO;
end;

function TObjMateriaisVenda.Get_Componente:string;
begin
    result:=Self.COMPONENTE;
end;

function TObjMateriaisVenda.Get_KitBox:string;
begin
    result:=Self.KITBOX;
end;

function TObjMateriaisVenda.Get_SITUACAOTRIBUTARIA_TABELAA:string;
begin
    Result:=self.SITUACAOTRIBUTARIA_TABELAA;
end;

function TObjMateriaisVenda.Get_SITUACAOTRIBUTARIA_TABELAB:string;
begin
    Result:=Self.SITUACAOTRIBUTARIA_TABELAB;
end;

function TObjMateriaisVenda.Get_QUANTIDADE:string;
begin
    result:=self.QUANTIDADE;
end;

function TObjMateriaisVenda.Get_VALORFINAL:string;
begin
    result:=Self.VALORFINAL;
end;

function TObjMateriaisVenda.Get_CFOP:string;
begin
    Result:=self.CFOP;
end;

function TObjMateriaisVenda.VerificaBrancos:Boolean;
begin
    //
  if(Self.ACRESCIMO = '') then
    Self.ACRESCIMO := '0';
end;

function TObjMateriaisVenda.VerificaRelacionamentos:Boolean;
begin
    //
end;

function TObjMateriaisVenda.VerificaNumericos:Boolean;
begin
    //
end;

function TObjMateriaisVenda.Get_ValorUnitario:string;
begin
    result:=self.VALORUNITARIO;
end;

procedure TObjMateriaisVenda.Submit_ValorUnitario(parametro:string);
begin
    VALORUNITARIO:=parametro;
end;

procedure TObjMateriaisVenda.Submit_Descricao(parametro:string);
begin
    DESCRICAO:=parametro;
end;

function TObjMateriaisVenda.Get_Descricao:string;
begin
    Result:=DESCRICAO;
end;

procedure TObjMateriaisVenda.Submit_Unidade(parametro:string);
begin
    UNIDADE:=parametro;
end;

function TObjMateriaisVenda.Get_Unidade:string;
begin
    result:=Self.UNIDADE;
end;

procedure TObjMateriaisVenda.Submit_Referencia(parametro:String);
begin
    REFERENCIA:=parametro;
end;

function TObjMateriaisVenda.Get_referencia:string;
begin
    Result:=self.REFERENCIA;
end;


function TObjMateriaisVenda.Get_Material:string;
begin
    result:=self.MATERIAL;
end;

procedure TObjMateriaisVenda.Submit_Material(parametro:string);
begin
    MATERIAL:=parametro;
end;

function TObjMateriaisVenda.Get_Persiana:string;
begin
    result:=self.PERSIANA;
end;

function TObjMateriaisVenda.Get_ALIQUOTA:string;
begin
    result:=self.ALIQUOTA;
end;

function TObjMateriaisVenda.Get_REDUCAOBASECALCULO:string;
begin
    result:=Self.REDUCAOBASECALCULO;
end;

function TObjMateriaisVenda.Get_ALIQUOTACUPOM:string;
begin
    result:=Self.ALIQUOTACUPOM;
end;

function TObjMateriaisVenda.Get_ISENTO:string;
begin
    result:=Self.ISENTO;
end;

function TObjMateriaisVenda.Get_SUBSTITUICAOTRIBUTARIA:string;
begin
    result:=self.SUBSTITUICAOTRIBUTARIA;
end;

function TObjMateriaisVenda.Get_PERCENTUALAGREGADO:string;
begin
    result:=self.PERCENTUALAGREGADO;
end;

function TObjMateriaisVenda.Get_VALORPAUTA:string;
begin
    result:=Self.VALORPAUTA;
end;

function TObjMateriaisVenda.Get_MARGEMVALORAGREGADOCONSUMIDOR:string;
begin
    result:=self.MARGEMVALORAGREGADOCONSUMIDOR;
end;

function TObjMateriaisVenda.Get_IMPOSTO_ICMS_ORIGEM:string;
begin
    Result:=Self.IMPOSTO_ICMS_ORIGEM;
end;

function TObjMateriaisVenda.Get_IMPOSTO_ICMS_DESTINO:string;
begin
  result:=self.IMPOSTO_ICMS_DESTINO;
end;

function TObjMateriaisVenda.Get_VALORFRETE:string;
begin
    result:=Self.VALORFRETE;
end;

function TObjMateriaisVenda.Get_VALORSEGURO:string;
begin
    result:=self.VALORSEGURO;
end;

function TObjMateriaisVenda.Get_BC_ICMS:string;
begin
    result:=self.BC_ICMS;
end;

function TObjMateriaisVenda.Get_VALOR_ICMS:string;
begin
    result:=self.VALOR_ICMS;
end;

function TObjMateriaisVenda.Get_BC_ICMS_ST:string;
begin
    result:=self.BC_ICMS_ST;
end;

function TObjMateriaisVenda.Get_VALOR_ICMS_ST:string;
begin
    Result:=Self.VALOR_ICMS_ST;
end;

function TObjMateriaisVenda.Get_BC_IPI:string;
begin
    result:=Self.BC_IPI;
end;

function TObjMateriaisVenda.Get_VALOR_IPI:string;
begin
  result:=Self.VALOR_IPI;
end;

function TObjMateriaisVenda.Get_BC_PIS:string;
begin
    Result:=Self.BC_PIS;
end;

function TObjMateriaisVenda.Get_VALOR_PIS:string;
begin
    result:=Self.VALOR_PIS;
end;

function TObjMateriaisVenda.Get_BC_PIS_ST:string;
begin
   result:=self.BC_PIS_ST;
end;

function TObjMateriaisVenda.Get_VALOR_PIS_ST:string;
begin
    result:=self.VALOR_PIS_ST;
end;

function TObjMateriaisVenda.Get_BC_COFINS:string;
begin
    result:=Self.BC_COFINS;
end;

function TObjMateriaisVenda.Get_VALOR_COFINS:string;
begin
    result:=self.VALOR_COFINS;
end;

function TObjMateriaisVenda.Get_BC_COFINS_ST:string;
begin
    result:=Self.BC_COFINS_ST;
end;

function TObjMateriaisVenda.Get_VALOR_COFINS_ST:string;
begin
    result:=Self.VALOR_COFINS_ST;
end;

function TObjMateriaisVenda.Get_IMPOSTO_PIS_ORIGEM:string;
begin
    result:=self.IMPOSTO_PIS_ORIGEM;
end;

function TObjMateriaisVenda.Get_IMPOSTO_COFINS_ORIGEM:string;
begin
    result:=self.IMPOSTO_COFINS_ORIGEM;
end;

function TObjMateriaisVenda.Get_IMPOSTO_PIS_DESTINO:string;
begin
    result:=Self.IMPOSTO_PIS_DESTINO;
end;

function TObjMateriaisVenda.Get_IMPOSTO_COFINS_DESTINO:string;
begin
    result:=self.IMPOSTO_COFINS_DESTINO;
end;

function TObjMateriaisVenda.Get_IMPOSTO_IPI:string;
begin
    result:=self.IMPOSTO_IPI;
end;

procedure TObjMateriaisVenda.Submit_ALIQUOTA(parametro:string);
begin
    self.ALIQUOTA:=parametro;
end;

procedure TObjMateriaisVenda.Submit_REDUCAOBASECALCULO(parametro:string);
begin
    self.REDUCAOBASECALCULO:=parametro;
end;

procedure TObjMateriaisVenda.Submit_ALIQUOTACUPOM(parametro:string);
begin
    self.ALIQUOTACUPOM:=parametro;
end;

procedure TObjMateriaisVenda.Submit_ISENTO(parametro:string);
begin
    self.ISENTO:=parametro;
end;

procedure TObjMateriaisVenda.Submit_SUBSTITUICAOTRIBUTARIA(parametro:string);
begin
    self.SUBSTITUICAOTRIBUTARIA:=parametro;
end;

procedure TObjMateriaisVenda.Submit_PERCENTUALAGREGADO(parametro:string);
begin
    self.PERCENTUALAGREGADO:=parametro;
end;

procedure TObjMateriaisVenda.Submit_VALORPAUTA(parametro:string);
begin
    self.VALORPAUTA:=parametro;
end;

procedure TObjMateriaisVenda.Submit_MARGEMVALORAGREGADOCONSUMIDOR(parametro:string);
begin
    self.MARGEMVALORAGREGADOCONSUMIDOR:=parametro;
end;

procedure TObjMateriaisVenda.Submit_MPOSTO_ICMS_ORIGEM(parametro:string);
begin
    self.IMPOSTO_ICMS_ORIGEM:=parametro;
end;

procedure TObjMateriaisVenda.Submit_IMPOSTO_ICMS_DESTINO(parametro:string);
begin
    Self.IMPOSTO_ICMS_DESTINO:=parametro;
end;

procedure TObjMateriaisVenda.Submit_VALORFRETE(parametro:string);
begin
    self.VALORFRETE:=parametro;
end;

procedure TObjMateriaisVenda.Submit_VALORSEGURO(parametro:string);
begin
    self.VALORSEGURO:=parametro;
end;

procedure TObjMateriaisVenda.Submit_BC_ICMS(parametro:string);
begin
    Self.BC_ICMS:=parametro;
end;

procedure TObjMateriaisVenda.Submit_VALOR_ICMS(parametro:string);
begin
    Self.VALOR_ICMS:=parametro;
end;

procedure TObjMateriaisVenda.Submit_BC_ICMS_ST(parametro:string);
begin
   self.BC_ICMS_ST:=parametro;
end;

procedure TObjMateriaisVenda.Submit_VALOR_ICMS_ST(parametro:string);
begin
    self.VALOR_ICMS_ST:=parametro;
end;

procedure TObjMateriaisVenda.Submit_BC_IPI(parametro:string);
begin
    self.BC_IPI:=parametro;
end;

procedure TObjMateriaisVenda.Submit_VALOR_IPI(parametro:string);
begin
    Self.VALOR_IPI:=parametro;
end;

procedure TObjMateriaisVenda.Submit_BC_PIS(parametro:string);
begin
    self.BC_PIS:=parametro;
end;

procedure TObjMateriaisVenda.Submit_VALOR_PIS(parametro:string);
begin
    self.VALOR_PIS:=parametro;
end;

procedure TObjMateriaisVenda.Submit_BC_PIS_ST(parametro:string);
begin
    self.BC_PIS_ST:=parametro;
end;

procedure TObjMateriaisVenda.Submit_VALOR_PIS_ST(parametro:string);
begin
    self.VALOR_PIS_ST:=parametro;
end;

procedure TObjMateriaisVenda.Submit_BC_COFINS(parametro:string);
begin
    self.BC_COFINS:=parametro;
end;

procedure TObjMateriaisVenda.Submit_VALOR_COFINS(parametro:string);
begin
    self.VALOR_COFINS:=parametro;
end;

procedure TObjMateriaisVenda.Submit_BC_COFINS_ST(parametro:string);
begin
    self.BC_COFINS_ST:=parametro;
end;

procedure TObjMateriaisVenda.Submit_VALOR_COFINS_ST(parametro:string);
begin
    self.VALOR_COFINS_ST:=parametro;
end;

procedure TObjMateriaisVenda.Submit_IMPOSTO_PIS_ORIGEM(parametro:string);
begin
    Self.IMPOSTO_PIS_ORIGEM:=parametro;
end;

procedure TObjMateriaisVenda.Submit_IMPOSTO_COFINS_ORIGEM(parametro:string);
begin
    self.IMPOSTO_COFINS_ORIGEM:=parametro;
end;

procedure TObjMateriaisVenda.Submit_MPOSTO_PIS_DESTINO(parametro:string);
begin
     self.IMPOSTO_PIS_DESTINO:=parametro;
end;

procedure TObjMateriaisVenda.Submit_IMPOSTO_COFINS_DESTINO(parametro:string);
begin
    Self.IMPOSTO_COFINS_DESTINO:=parametro;
end;

procedure TObjMateriaisVenda.Submit_IMPOSTO_IPI(parametro:string);
begin
    self.IMPOSTO_IPI:=parametro;
end;

procedure TObjMateriaisVenda.Submit_NotaFiscal(parametro:string);
begin
    self.NOTAFISCAL:=parametro;
end;

function TObjMateriaisVenda.Get_NotaFiscal:string;
begin
    Result:=self.NOTAFISCAL;
end;

function TObjMateriaisVenda.Get_classificacaofiscal:string;
begin
    result:=Self.classificacaofiscal;
end;

procedure TObjMateriaisVenda.Submit_classificacaofiscal(parametro:string);
begin
    Self.classificacaofiscal:=parametro;
end;

procedure TObjMateriaisVenda.Submit_Cor(parametro:string);
begin
    Self.COR:=parametro;
end;

function TObjMateriaisVenda.Get_COR:string;
begin
    Result:=COR;
end;

function TObjMateriaisVenda.Get_PesoUnitario:string;
begin
    Result:=Self.PesoUnitario;
end;

procedure TObjMateriaisVenda.Submit_PesoUnitario(parametro:string);
begin
    PesoUnitario:=parametro;
end;

procedure TObjMateriaisVenda.Submit_NCM(parametro:string);
begin
  ncm:=parametro;
end;

function TObjMateriaisVenda.Get_NCM:string;
begin
    result:=NCM;
end;

procedure TObjMateriaisVenda.Submit_Desconto(parametro:String);
begin
    Self.DESCONTO:=parametro;
end;

function TObjMateriaisVenda.Get_Desconto:string;
begin
    Result:=self.DESCONTO;
end;


function TObjMateriaisVenda.get_OBSERVACAO: string;
begin

  result:=self.OBSERVACAO;

end;

procedure TObjMateriaisVenda.submit_OBSERVACAO(parametro: string);
begin

  self.OBSERVACAO:=parametro;

end;

function TObjMateriaisVenda.get_CSOSN: string;
begin

  result:=self.CSOSN;

end;

procedure TObjMateriaisVenda.submit_CSOSN(parametro: string);
begin

  self.CSOSN:=parametro;

end;

function TObjMateriaisVenda.Get_Projeto:string;
begin
    Result:=Self.PROJETO;
end;

procedure TObjMateriaisVenda.Submit_Projeto(parametro:string);
begin
    Self.PROJETO:=parametro;
end;

procedure TObjMateriaisVenda.Submit_CodigoCor(parametro:string);
begin
    CODIGOCOR:=parametro;
end;

function TObjMateriaisVenda.Get_CodigoCor:string;
begin
  Result:=CODIGOCOR;
end;


function TObjMateriaisVenda.Get_Acrescimo: string;
begin
  Result := self.ACRESCIMO;
end;

procedure TObjMateriaisVenda.Submit_Acrescimo(parametro: string);
begin
  self.ACRESCIMO := parametro;
end;

end.
