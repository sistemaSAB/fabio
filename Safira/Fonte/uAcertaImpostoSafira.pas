unit uAcertaImpostoSafira;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UacertaImposto, ImgList, DB, IBCustomDataSet, IBQuery, StdCtrls,
  Buttons, ExtCtrls, Grids, DBGrids, ComCtrls, Menus;

type
  TfAcertaImpostoSafira = class(TfAcertaImposto)
    procedure FormShow(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure btAntClick(Sender: TObject);
    procedure btproxClick(Sender: TObject);
    procedure btOk_ICMSClick(Sender: TObject);
    procedure pageControlTotaisChange(Sender: TObject);
    procedure btOK_ICMS_STClick(Sender: TObject);
    procedure btOk_PISClick(Sender: TObject);
    procedure btOK_COFINSClick(Sender: TObject);
    procedure btOK_IPIClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    sqlTot:string;
    procedure gridParaControles();
    procedure getTotalICMS();
    procedure getTotais();
    procedure getTotalICMSST();
    procedure getTotalPIS();
    procedure getTotalCOFINS();
    procedure getTotalIPI();
    procedure getCRT();
    procedure atualizaTotICMS();
    procedure atualizaTotICMS_ST();
    procedure atualizaTotIPI();
  public
    nfedigitada:string;
  end;

var
  fAcertaImpostoSafira: TfAcertaImpostoSafira;

implementation

uses UessencialGlobal, UDataModulo;

{$R *.dfm}

procedure TfAcertaImpostoSafira.FormShow(Sender: TObject);
begin
  inherited;

  query1.SQL.Text :=
  'Select pnfe.codigo_produto as produto,pnfe.nome_produto,pnfe.quantidade,pnfe.valor, '+
  'pnfe.desconto,pnfe.codigo,pnfe.valorfinal '+
  'from viewprodnfedigitada pnfe '+
  'where pnfe.nfedigitada = '+nfedigitada;

  query1.Active := True;
  query1.FetchAll;
  DBGrid1.SetFocus;

  getCRT;

  //apenas para valida��o do crt
  getTotalICMS();

end;

procedure TfAcertaImpostoSafira.gridParaControles;
begin

  queryAux.SQL.Text :=

  'Select pnfe.valorbasecalculo,pnfe.percentualicms,pnfe.valoricms, '+
  'pnfe.percentualreducaobc, '+
  'pnfe.valorbasecalculo_st,pnfe.percentualicms_st,pnfe.valoricms_st, '+
  'pnfe.percentualreducaobc_st, '+
  'pnfe.cstpis,pnfe.percentualpis,pnfe.valorpis,pnfe.bcpis, '+
  'pnfe.cstcofins,pnfe.percentualcofins,pnfe.valorcofins,pnfe.bccofins, '+
  'pnfe.cstipi,pnfe.percentualipi,pnfe.valoripi,pnfe.VALORBASECALCULO_IPI,pnfe.valorfrete, '+
  'pnfe.cst,pnfe.csosn, pnfe.valoroutros,pnfe.percentualtributo,pnfe.vtottrib '+
  'from TABPRODNFEDIGITADA pnfe '+
  'where pnfe.codigo = '+ds1.DataSet.fieldbyname('codigo').AsString;

  queryAux.Active := True;

  edt_ICMS_BC.Text         := queryAux.Fields[0].Text;
  edt_ICMS_ALIQUOTA.Text   := queryAux.Fields[1].Text;
  edt_ICMS_VALOR.Text      := queryAux.Fields[2].Text;
  edt_ICMS_REDUCAO.Text    := queryAux.Fields[3].Text;

  edt_ICMSST_BC.Text       := queryAux.Fields[4].Text;
  edt_ICMSST_ALIQUOTA.Text := queryAux.Fields[5].Text;
  edt_ICMSST_VALOR.Text    := queryAux.Fields[6].Text;
  edt_ICMSST_REDUCAO.Text  := queryAux.Fields[7].Text;

  edt_PIS_CST.Text      := queryAux.Fields[8].Text;
  edt_PIS_ALIQUOTA.Text := queryAux.Fields[9].Text;
  edt_PIS_VALOR.Text    := queryAux.Fields[10].Text;
  edt_PIS_BC.Text       := queryAux.Fields[11].Text;

  edt_COFINS_CST.Text      := queryAux.Fields[12].Text;
  edt_COFINS_ALIQUOTA.Text := queryAux.Fields[13].Text;
  edt_COFINS_VALOR.Text    := queryAux.Fields[14].Text;
  edt_COFINS_BC.Text       := queryAux.Fields[15].Text;

  edt_IPI_CST.Text      := queryAux.Fields[16].Text;
  edt_IPI_ALIQUOTA.Text := queryAux.Fields[17].Text;
  edt_IPI_VALOR.Text    := queryAux.Fields[18].Text;
  edt_IPI_BC.Text       := queryAux.Fields[19].Text;

  edt_ICMS_ValorFrete.Text  := queryAux.Fields[20].Text;
  edt_ICMS_CST.Text        := queryAux.Fields[21].Text;
  edt_ICMS_CSOSN.Text      := queryAux.Fields[22].Text;

  edt_ICMS_ValorOutros.Text := queryAux.fields[23].Text;
  edt_outros_percentualTributo.Text := queryAux.Fields[24].text;
  edt_outros_valorTributos.Text := queryAux.Fields[25].Text;

  self.chamaOnExit;

end;

procedure TfAcertaImpostoSafira.DBGrid1DblClick(Sender: TObject);
begin
  inherited;
  gridParaControles;
end;

procedure TfAcertaImpostoSafira.PageControl1Change(Sender: TObject);
begin
  inherited;

  if PageControl1.TabIndex = 1 then
  begin
    pageControlTotais.TabIndex := 0;
    getTotalICMS();
    getTotais();
  end;

end;

procedure TfAcertaImpostoSafira.btOkClick(Sender: TObject);
begin
  inherited;

    if ( (tot_ICMS_VALOR.Text <> '') and (tot_ICMS_VALOR.Text <> '0,00') or ((edt_ICMSST_VALOR.Text <> '') and (edt_ICMSST_VALOR.Text <> '0,00'))) and ((outros_CRT.Text = '') or (outros_CRT.Text = 'NULL'))  then
      if (MensagemPergunta('Vai alterar o CRT na aba TOTAIS >> OUTROS ??') = mryes)  then
      begin

        PageControl1.TabIndex := 1;
        pageControlTotais.TabIndex := 5;

        if (outros_CRT.Enabled) then
          outros_CRT.SetFocus;

        Exit;

      end;
        
  FDataModulo.IBTransaction.CommitRetaining;
  self.Close;
end;

procedure TfAcertaImpostoSafira.btCancelClick(Sender: TObject);
begin
  inherited;
  FDataModulo.IBTransaction.RollbackRetaining;
  self.Close;
end;

procedure TfAcertaImpostoSafira.btAntClick(Sender: TObject);
begin
  inherited;
  gridParaControles;
end;

procedure TfAcertaImpostoSafira.btproxClick(Sender: TObject);
begin
  inherited;
  gridParaControles;
end;

procedure TfAcertaImpostoSafira.btOk_ICMSClick(Sender: TObject);
var
  sql:string;
begin
  inherited;

  sql := 'update TABPRODNFEDIGITADA set '+
         'valorbasecalculo = '+format_db(edt_ICMS_BC.Text) +
         ',percentualicms = '+format_db(edt_ICMS_ALIQUOTA.Text) +
         ',valoricms = '+format_db(edt_ICMS_VALOR.Text)+
         ',percentualreducaobc = '+format_db(edt_ICMS_REDUCAO.Text) +
         ',valorfrete = '+format_db(edt_ICMS_ValorFrete.Text) +
         ',cst = '+edt_ICMS_CST.Text +
         ',csosn = '+QuotedStr(edt_ICMS_CSOSN.Text) +
         ',valoroutros = '+format_db(edt_ICMS_ValorOutros.Text) +
         ' where codigo = '+ds1.DataSet.fieldbyname('codigo').AsString;


  if not exec_sql(sql) then
    MensagemErro('Erro ao alterar imposto do produto: '+ds1.DataSet.fieldbyname('produto').AsString)
  else
    atualizaTotICMS();

end;

procedure TfAcertaImpostoSafira.pageControlTotaisChange(Sender: TObject);
begin
  inherited;
  case pageControlTotais.TabIndex of
    0:getTotalICMS();
    1:getTotalICMSST();
    2:getTotalPIS();
    3:getTotalCOFINS();
    4:getTotalIPI();
    5:getCRT();
  else ;
  end;

  getTotalICMSST();
end;

procedure TfAcertaImpostoSafira.getTotalICMSST;
begin
  tot_ICMSST_BC.Text    := get_campoTabela('SUM(valorbasecalculo_st)  tot'  ,'nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');
  tot_ICMSST_VALOR.Text := get_campoTabela('SUM(valoricms_st)         tot'  ,'nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');
  self.chamaOnExit;
end;

procedure TfAcertaImpostoSafira.getTotalPIS;
begin
  tot_PIS_BC.Text    := get_campoTabela('SUM(bcpis)    tot'  ,'nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');
  tot_PIS_VALOR.Text := get_campoTabela('SUM(valorpis) tot'  ,'nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');
  self.chamaOnExit;
end;

procedure TfAcertaImpostoSafira.getTotalCOFINS();
begin
  tot_COFINS_BC.Text       := get_campoTabela('SUM(bccofins)    tot'  ,'nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');
  tot_COFINS_VALOR.Text := get_campoTabela('SUM(valorcofins) tot'  ,'nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');
  self.chamaOnExit;
end;

procedure TfAcertaImpostoSafira.getTotalIPI();
begin
  tot_IPI_BC.Text       := get_campoTabela('SUM(VALORBASECALCULO_IPI) tot'  ,'nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');
  tot_IPI_VALOR.Text    := get_campoTabela('SUM(valoripi)             tot'  ,'nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');
  self.chamaOnExit;
end;

procedure TfAcertaImpostoSafira.getTotalICMS;
begin
  tot_ICMS_BC.Text          := get_campoTabela('SUM(valorbasecalculo) tot'  ,'nfedigitada' ,'TABPRODNFEDIGITADA',nfedigitada,'tot');
  tot_ICMS_VALOR.Text       := get_campoTabela('SUM(valoricms)        tot'  ,'nfedigitada' ,'TABPRODNFEDIGITADA',nfedigitada,'tot');
  tot_ICMS_ValorFrete.Text  := get_campoTabela('SUM(valorfrete)       tot'  ,'nfedigitada' ,'TABPRODNFEDIGITADA',nfedigitada,'tot');
  tot_ICMS_ValorOutros.Text := get_campoTabela('SUM(valoroutros)      tot'  ,'nfedigitada'  ,'TABPRODNFEDIGITADA',nfedigitada,'tot');
  self.chamaOnExit;
end;

procedure TfAcertaImpostoSafira.getTotais;
begin

  tot_produto.Text   := get_campoTabela('SUM(valortotal) tot'  ,'nfedigitada' ,'TABPRODNFEDIGITADA',nfedigitada,'tot');
  tot_desconto.Text  := get_campoTabela('SUM(desconto)   tot'  ,'nfedigitada' ,'TABPRODNFEDIGITADA',nfedigitada,'tot');

  tot_nf.Text := get_campoTabela('valornf','codigo','TABNFEDIGITADA',nfedigitada);

  self.chamaOnExit;
  
end;

procedure TfAcertaImpostoSafira.getCRT();
begin
  outros_CRT.Text := get_campoTabela('crt','codigo','TABNFEDIGITADA',nfedigitada);
end;

procedure TfAcertaImpostoSafira.btOK_ICMS_STClick(Sender: TObject);
var
  sql:string;
begin
  inherited;

  sql := 'update TABPRODNFEDIGITADA set '+
         'valorbasecalculo_st     = '+format_db(edt_ICMSST_BC.Text) +
         ',percentualicms_st      = '+format_db(edt_ICMSST_ALIQUOTA.Text) +
         ',valoricms_st           = '+format_db(edt_ICMSST_VALOR.Text)+
         ',percentualreducaobc_st = '+format_db(edt_ICMSST_REDUCAO.Text) +
         ' where codigo = '+ds1.DataSet.fieldbyname('codigo').AsString;

  if not exec_sql(sql) then
    MensagemErro('Erro ao alterar imposto do produto: '+ds1.DataSet.fieldbyname('produto').AsString)
  else
    atualizaTotICMS_ST;

end;


procedure TfAcertaImpostoSafira.btOk_PISClick(Sender: TObject);
var
  sql:string;
begin
  inherited;

  sql := 'update TABPRODNFEDIGITADA set '+
         'bcpis = '+format_db(edt_PIS_BC.Text) +
         ',percentualpis = '+format_db(edt_PIS_ALIQUOTA.Text) +
         ',valorpis = '+format_db(edt_PIS_VALOR.Text)+
         ',cstpis = '+edt_PIS_CST.Text+
         ' where codigo = '+ds1.DataSet.fieldbyname('codigo').AsString;


  if not exec_sql(sql) then
    MensagemErro('Erro ao alterar imposto do produto: '+ds1.DataSet.fieldbyname('produto').AsString);

end;

procedure TfAcertaImpostoSafira.btOK_COFINSClick(Sender: TObject);
var
  sql:string;
begin
  inherited;

  sql := 'update TABPRODNFEDIGITADA set '+
         'bccofins = '+format_db(edt_COFINS_BC.Text) +
         ',percentualcofins = '+format_db(edt_COFINS_ALIQUOTA.Text) +
         ',valorcofins = '+format_db(edt_COFINS_VALOR.Text)+
         ',cstcofins = '+edt_COFINS_CST.Text+
         ' where codigo = '+ds1.DataSet.fieldbyname('codigo').AsString;

  if not exec_sql(sql) then
    MensagemErro('Erro ao alterar imposto do produto: '+ds1.DataSet.fieldbyname('produto').AsString);

end;

procedure TfAcertaImpostoSafira.btOK_IPIClick(Sender: TObject);
var
  sql:string;
begin
  inherited;

  sql := 'update TABPRODNFEDIGITADA set '+
         'valorbasecalculo_ipi = '+format_db(edt_IPI_BC.Text) +
         ',percentualipi = '+format_db(edt_IPI_ALIQUOTA.Text) +
         ',valoripi = '+format_db(edt_IPI_VALOR.Text)+
         ',cstipi = '+edt_IPI_CST.Text+
         ' where codigo = '+ds1.DataSet.fieldbyname('codigo').AsString;

  if not exec_sql(sql) then
    MensagemErro('Erro ao alterar imposto do produto: '+ds1.DataSet.fieldbyname('produto').AsString)
  else
    atualizaTotIPI;
    
end;

procedure TfAcertaImpostoSafira.SpeedButton2Click(Sender: TObject);
var
  sql:string;
begin
  inherited;

  sql := 'update TABPRODNFEDIGITADA set '+
         'percentualtributo = '+format_db(edt_outros_percentualTributo.Text)+
         ',vtottrib = '+format_db(edt_outros_valorTributos.Text)+
         ' where codigo = '+ds1.DataSet.fieldbyname('codigo').AsString;


  if not exec_sql(sql) then
    MensagemErro('Erro ao alterar, produto: '+ds1.DataSet.fieldbyname('produto').AsString);
    
end;

procedure TfAcertaImpostoSafira.SpeedButton1Click(Sender: TObject);
var
  sqlOutr:string;
begin

  sqlOutr :=
  'update TABNFEDIGITADA set '+
  'crt = '+outros_CRT.Text+
  ' where codigo = '+self.nfedigitada;

  if not exec_sql(sqlOutr) then
    MensagemErro('N�o foi possivel acertar "outros"');

end;

procedure TfAcertaImpostoSafira.atualizaTotICMS;
begin

  tot_produto.Text          := get_campoTabela('SUM(valortotal)       as tot','nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');
  tot_desconto.Text         := get_campoTabela('SUM(desconto)         as tot','nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');
  tot_ICMS_BC.Text          := get_campoTabela('SUM(valorbasecalculo) as tot','nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');
  tot_ICMS_VALOR.Text       := get_campoTabela('SUM(valoricms)        as tot','nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');
  tot_ICMS_ValorFrete.Text  := get_campoTabela('SUM(valorfrete)       as tot','nfedigitada','TABPRODNFEDIGITADA',self.nfedigitada,'tot');
  tot_ICMS_ValorOutros.Text := get_campoTabela('SUM(valoroutros)      as tot','nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');

  sqlTot :=
  'update TABNFEDIGITADA set '+
  'valorproduto        = '+format_db(tot_produto.Text)+
  ',valordesconto       = '+format_db(tot_desconto.Text)+
  ',valorbasecalculo    = '+format_db(tot_ICMS_BC.Text)+
  ',valoricms           = '+format_db(tot_ICMS_VALOR.Text)+
  ',valorfrete          = '+format_db(tot_ICMS_ValorFrete.Text)+
  ',valoroutros         = '+format_db(tot_ICMS_ValorOutros.Text)+
  ' where codigo        = '+nfedigitada;

  if not exec_sql(sqlTot) then
    MensagemErro('N�o foi possivel acertar totais ICMS');

end;

procedure TfAcertaImpostoSafira.atualizaTotICMS_ST;
begin

  tot_ICMSST_VALOR.Text := get_campoTabela('SUM(VALORICMS_ST)        as tot','nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');

  sqlTot :=
  'update TABNFEDIGITADA set '+
  'VALORICMS_ST       = '+format_db(tot_ICMSST_VALOR.Text)+
  ' where codigo       = '+nfedigitada;

  if not exec_sql(sqlTot) then
    MensagemErro('N�o foi possivel acertar totais ICMS ST');
    
end;

procedure TfAcertaImpostoSafira.atualizaTotIPI();
begin

  tot_IPI_VALOR.Text := get_campoTabela('SUM(VALORIPI) as tot','nfedigitada','TABPRODNFEDIGITADA',nfedigitada,'tot');

  sqlTot :=
  'update TABNFEDIGITADA set '+
  'VALORIPI      = '+format_db(tot_IPI_VALOR.Text)+
  ' where codigo = '+nfedigitada;

  if not exec_sql(sqlTot) then
    MensagemErro('N�o foi possivel acertar totais IPI');
end;



end.
