unit UTRANSPORTADORA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjTRANSPORTADORA,
  UessencialGlobal, Tabs,IBQuery;

type
  TFTRANSPORTADORA = class(TForm)
    edtNOME: TEdit;
    lbnome: TLabel;
    edtPLACA: TEdit;
    lbplaca: TLabel;
    lbbservacoes: TLabel;
    mmoObservacoes: TMemo;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    imgrodape: TImage;
    lb14: TLabel;
    Label1: TLabel;
    edtUFplaca: TEdit;
    Label2: TLabel;
    edtCPFCNPJ: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    edtEndereco: TEdit;
    edtIE: TEdit;
    edtUF: TEdit;
    edtPeso: TEdit;
    edtPesoMaximo: TEdit;
    edtMunicipio: TEdit;
    btAjuda: TSpeedButton;
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure memoObservacoesKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure edtPLACAKeyPress(Sender: TObject; var Key: Char);
    procedure edtPesoKeyPress(Sender: TObject; var Key: Char);
    procedure edtPesoMaximoKeyPress(Sender: TObject; var Key: Char);
    procedure edtCPFCNPJKeyPress(Sender: TObject; var Key: Char);
    procedure btAjudaClick(Sender: TObject);
  private
    ObjTRANSPORTADORA:TObjTRANSPORTADORA;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    procedure MostraQuantidadeCadastrada;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FTRANSPORTADORA: TFTRANSPORTADORA;


implementation

uses Upesquisa, UessencialLocal, UDataModulo, UescolheImagemBotao,
  Uprincipal, UAjuda;

{$R *.dfm}


procedure TFTRANSPORTADORA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjTRANSPORTADORA=Nil)
     Then exit;

     If (Self.ObjTRANSPORTADORA.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjTRANSPORTADORA.free;
    Action := caFree;
    Self := nil;
end;

procedure TFTRANSPORTADORA.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbCodigo.caption:='0';
     //edtcodigo.text:=ObjTRANSPORTADORA.Get_novocodigo;

     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjTRANSPORTADORA.status:=dsInsert;

     Edtnome.setfocus;

end;

procedure TFTRANSPORTADORA.btSalvarClick(Sender: TObject);
begin

     If ObjTRANSPORTADORA.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjTRANSPORTADORA.salvar(true)=False)
     Then exit;

     lbCodigo.caption:=ObjTRANSPORTADORA.Get_codigo;
     habilita_botoes(Self);
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     MostraQuantidadeCadastrada;

     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFTRANSPORTADORA.btAlterarClick(Sender: TObject);
begin
    If (ObjTRANSPORTADORA.Status=dsinactive) and (lbCodigo.caption<>'')
    Then Begin
                habilita_campos(Self);

                ObjTRANSPORTADORA.Status:=dsEdit;

                edtnome.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btnovo.Visible :=false;
               btalterar.Visible:=false;
               btpesquisar.Visible:=false;
               btrelatorio.Visible:=false;
               btexcluir.Visible:=false;
               btsair.Visible:=false;
               btopcoes.Visible :=false;
          End;

end;

procedure TFTRANSPORTADORA.btCancelarClick(Sender: TObject);
begin
     ObjTRANSPORTADORA.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;

     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFTRANSPORTADORA.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjTRANSPORTADORA.Get_pesquisa,ObjTRANSPORTADORA.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjTRANSPORTADORA.status<>dsinactive
                                  then exit;

                                  If (ObjTRANSPORTADORA.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjTRANSPORTADORA.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFTRANSPORTADORA.btExcluirClick(Sender: TObject);
begin
     If (ObjTRANSPORTADORA.status<>dsinactive) or (lbCodigo.caption='')
     Then exit;

     If (ObjTRANSPORTADORA.LocalizaCodigo(lbCodigo.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjTRANSPORTADORA.exclui(lbCodigo.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFTRANSPORTADORA.btRelatorioClick(Sender: TObject);
begin
    ObjTRANSPORTADORA.Imprime(lbCodigo.caption);
end;

procedure TFTRANSPORTADORA.btSairClick(Sender: TObject);
begin
    Self.close;
end;


procedure TFTRANSPORTADORA.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFTRANSPORTADORA.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFTRANSPORTADORA.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
    if(key=vk_f1) then
    begin
        Fprincipal.chamaPesquisaMenu;
    end;
    
    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('ARQUITETO');
         FAjuda.ShowModal;
    end;

end;

procedure TFTRANSPORTADORA.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFTRANSPORTADORA.ControlesParaObjeto: Boolean;
begin
  Try

    With ObjTRANSPORTADORA do
    Begin

      Submit_CODIGO(lbCodigo.caption);
      Submit_NOME(edtNOME.text);
      Submit_PLACA(edtPLACA.text);
      Submit_Observacoes(mmoObservacoes.text);

      submit_Endereco(self.edtEndereco.Text);
      submit_UF(self.edtUF.Text);
      submit_CPFCNPJ(self.edtCPFCNPJ.Text);
      submit_IE(self.edtIE.Text);
      submit_Municipio(self.edtMunicipio.Text);
      submit_UFPlaca(self.edtUFplaca.Text);
      submit_PesoMaximo(self.edtPesoMaximo.Text);
      submit_peso(self.edtPeso.Text);


      result:=true;

    End;

  Except
    result:=False;
  End;

end;

procedure TFTRANSPORTADORA.LimpaLabels;
begin
    lbCodigo.caption:='';
end;

function TFTRANSPORTADORA.ObjetoParaControles: Boolean;
begin
  Try
     With ObjTRANSPORTADORA do
     Begin

        lbCodigo.caption:=Get_CODIGO;
        EdtNOME.text:=Get_NOME;
        EdtPLACA.text:=Get_PLACA;
        mmoObservacoes.text:=Get_Observacoes;

        self.edtUF.Text:=get_UF;
        self.edtCPFCNPJ.Text:=get_CPFCNPJ;
        self.edtIE.Text:=get_IE;
        self.edtMunicipio.Text:=get_Municipio;
        self.edtEndereco.Text:=get_Endereco;
        self.edtUFplaca.Text:=get_UFPlaca;
        self.edtPesoMaximo.Text:=get_PesoMaximo;
        self.edtPeso.Text:=get_peso;

        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFTRANSPORTADORA.TabelaParaControles: Boolean;
begin
     If (ObjTRANSPORTADORA.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFTRANSPORTADORA.memoObservacoesKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then mmoobservacoes.setfocus;
end;

procedure TFTRANSPORTADORA.MostraQuantidadeCadastrada;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabtransportadora');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb14.Caption:='Existem '+IntToStr(contador)+' transportadoras cadastrados'
       else
       begin
            lb14.Caption:='Existe '+IntToStr(contador)+' transportadora cadastrado';
       end;

    finally

    end;


end;


procedure TFTRANSPORTADORA.FormShow(Sender: TObject);
begin
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjTRANSPORTADORA:=TObjTRANSPORTADORA.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE TRANSPORTADORA')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);
     MostraQuantidadeCadastrada;


     if (Self.Tag <> 0) then
     begin

      self.ObjTRANSPORTADORA.LocalizaCodigo (IntToStr (self.tag));
      Self.TabelaParaControles;

     end;

end;

procedure TFTRANSPORTADORA.edtPLACAKeyPress(Sender: TObject;var Key: Char);
begin

  if not (Key in['0'..'9','a'..'z','A'..'Z',Chr(8)]) then
    Key:= #0

end;

procedure TFTRANSPORTADORA.edtPesoKeyPress(Sender: TObject; var Key: Char);
begin

  ValidaNumeros(self.edtpeso,Key,'FLOAT');

end;

procedure TFTRANSPORTADORA.edtPesoMaximoKeyPress(Sender: TObject;
  var Key: Char);
begin

  ValidaNumeros(self.edtPesoMaximo,Key,'FLOAT');

end;

procedure TFTRANSPORTADORA.edtCPFCNPJKeyPress(Sender: TObject;
  var Key: Char);
begin

  ValidaNumeros(self.edtCPFCNPJ,Key,'INT');

end;

procedure TFTRANSPORTADORA.btAjudaClick(Sender: TObject);
begin
         FAjuda.PassaAjuda('ARQUITETO');
         FAjuda.ShowModal;
end;

end.
