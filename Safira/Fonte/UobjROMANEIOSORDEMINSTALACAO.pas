unit UobjROMANEIOSORDEMINSTALACAO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,UobjROMANEIO,UobjORDEMINSTALACAO, UobjPEDIDO_PROJ
;
//USES_INTERFACE


Type
   TObjROMANEIOSORDEMINSTALACAO=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                ROMANEIO                                    :TObjROMANEIO;
                ORDEMINSTALACAO                             :TObjORDEMINSTALACAO;
                PEDIDOPROJETO                               :TObjPEDIDO_PROJ;
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_CONCLUIDO(parametro: string);
                Function Get_CONCLUIDO: string;
                Procedure Submit_HORARIO(parametro: string);
                Function Get_HORARIO: string;
                procedure Submit_TEMPOMEDIOINSTALACAO(parametro:string);
                function Get_TEMPOMEDIOINSTALACAO:string;
                procedure Submit_TempoFinal(parametro:String);
                Function Get_TempoFinal:string;
                procedure Submit_Observacao(parametro:string);
                Function Get_Observacao:string;

                procedure EdtRomaneioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);

                procedure CarregaDados(parametro:string);
                function VerificaProjetoInserido(parametro:string):Boolean;
                function VerificaHorarioDisponivel(tempoinicial,duracao,tempofinal,ordeminstalacao:string;var projeto:string):Boolean;
                
         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               CODIGO:string;
               CONCLUIDO:string;
               HORARIO:string;
               TEMPOMEDIOINSTALACAO:string;
               TEMPOFINAL:string;
               OBSERVACAO:string;



               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;





Function  TObjROMANEIOSORDEMINSTALACAO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;

        If(fieldbyname('ROMANEIO').asstring<>'')
        Then Begin
                 If (Self.ROMANEIO.LocalizaCodigo(fieldbyname('ROMANEIO').asstring)=False)
                 Then Begin
                          Messagedlg('PEDIDO N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.ROMANEIO.TabelaparaObjeto;
        End;
        Self.CONCLUIDO:=fieldbyname('CONCLUIDO').asstring;
        If(fieldbyname('ORDEMINSTALACAO').asstring<>'')
        Then Begin
                 If (Self.ORDEMINSTALACAO.LocalizaCodigo(fieldbyname('ORDEMINSTALACAO').asstring)=False)
                 Then Begin
                          Messagedlg('PEDIDO N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.ORDEMINSTALACAO.TabelaparaObjeto;
        End;

        If(fieldbyname('PEDIDOPROJETO').asstring<>'')
        Then Begin
                 If (Self.PEDIDOPROJETO.LocalizaCodigo(fieldbyname('PEDIDOPROJETO').asstring)=False)
                 Then Begin
                          Messagedlg('PEDIDO N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PEDIDOPROJETO.TabelaparaObjeto;
        End;
        Self.HORARIO:=fieldbyname('HORARIO').asstring;
        TEMPOMEDIOINSTALACAO:=fieldbyname('TEMPOMEDIOINSTALACAO').AsString;
        TEMPOFINAL:=fieldbyname('tempofinal').AsString;
        OBSERVACAO:=fieldbyname('observacao').AsString;


        result:=True;
     End;
end;


Procedure TObjROMANEIOSORDEMINSTALACAO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('ROMANEIO').asstring:=Self.ROMANEIO.Get_CODIGO;
        ParamByName('CONCLUIDO').asstring:=Self.CONCLUIDO;
        ParamByName('ORDEMINSTALACAO').asstring:=Self.ORDEMINSTALACAO.Get_CODIGO;
        ParamByName('HORARIO').asstring:=Self.HORARIO;
        ParamByName('PEDIDOPROJETO').AsString:=PEDIDOPROJETO.Get_Codigo;
        ParamByName('TEMPOMEDIOINSTALACAO').AsString:=TEMPOMEDIOINSTALACAO;
        ParamByName('tempofinal').AsString:=TEMPOFINAL;
        ParamByName('observacao').AsString:=OBSERVACAO;

  End;
End;

//***********************************************************************

function TObjROMANEIOSORDEMINSTALACAO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjROMANEIOSORDEMINSTALACAO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        ROMANEIO.ZerarTabela;
        CONCLUIDO:='';
        ORDEMINSTALACAO.ZerarTabela;
        HORARIO:='';
        PEDIDOPROJETO.ZerarTabela;
        TEMPOMEDIOINSTALACAO:='';
        TEMPOFINAL:='';
        OBSERVACAO:='';


     End;
end;

Function TObjROMANEIOSORDEMINSTALACAO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjROMANEIOSORDEMINSTALACAO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjROMANEIOSORDEMINSTALACAO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;

//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjROMANEIOSORDEMINSTALACAO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtotime(Self.HORARIO);
     Except
           Mensagem:=mensagem+'/HORARIO';
     End;

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjROMANEIOSORDEMINSTALACAO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjROMANEIOSORDEMINSTALACAO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro ROMANEIOSORDEMINSTALACAO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,ROMANEIO,CONCLUIDO,ORDEMINSTALACAO,HORARIO,USERM');
           SQL.ADD(' ,USERC,DATAC,DATAM,PEDIDOPROJETO,TEMPOMEDIOINSTALACAO,tempofinal,observacao');
           SQL.ADD(' from  TABROMANEIOSORDEMINSTALACAO');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjROMANEIOSORDEMINSTALACAO.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjROMANEIOSORDEMINSTALACAO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjROMANEIOSORDEMINSTALACAO.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjROMANEIOSORDEMINSTALACAO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        ObjqueryPesquisa :=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDataSource.DataSet:=Self.ObjqueryPesquisa;

        ROMANEIO:=TObjROMANEIO.Create;
        ORDEMINSTALACAO:=TObjORDEMINSTALACAO.Create;
        PEDIDOPROJETO:=TObjPEDIDO_PROJ.Create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABROMANEIOSORDEMINSTALACAO(CODIGO,ROMANEIO');
                InsertSQL.add(' ,CONCLUIDO,ORDEMINSTALACAO,HORARIO,USERM,USERC,DATAC');
                InsertSQL.add(' ,DATAM,PEDIDOPROJETO,TEMPOMEDIOINSTALACAO,tempofinal,observacao)');
                InsertSQL.add('values (:CODIGO,:ROMANEIO,:CONCLUIDO,:ORDEMINSTALACAO');
                InsertSQL.add(' ,:HORARIO,:USERM,:USERC,:DATAC,:DATAM,:PEDIDOPROJETO,:TEMPOMEDIOINSTALACAO,:tempofinal,:observacao)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABROMANEIOSORDEMINSTALACAO set CODIGO=:CODIGO');
                ModifySQL.add(',ROMANEIO=:ROMANEIO,CONCLUIDO=:CONCLUIDO,ORDEMINSTALACAO=:ORDEMINSTALACAO');
                ModifySQL.add(',HORARIO=:HORARIO,USERM=:USERM,USERC=:USERC,DATAC=:DATAC');
                ModifySQL.add(',DATAM=:DATAM,PEDIDOPROJETO=:PEDIDOPROJETO,TEMPOMEDIOINSTALACAO=:TEMPOMEDIOINSTALACAO,tempofinal=:tempofinal,observacao=:observacao');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABROMANEIOSORDEMINSTALACAO where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjROMANEIOSORDEMINSTALACAO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjROMANEIOSORDEMINSTALACAO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabROMANEIOSORDEMINSTALACAO');
     Result:=Self.ParametroPesquisa;
end;

function TObjROMANEIOSORDEMINSTALACAO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de ROMANEIOSORDEMINSTALACAO ';
end;


function TObjROMANEIOSORDEMINSTALACAO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENROMANEIOSORDEMINSTALACAO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENROMANEIOSORDEMINSTALACAO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjROMANEIOSORDEMINSTALACAO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    ROMANEIO.Free;
    ORDEMINSTALACAO.Free;
    PEDIDOPROJETO.Free;
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjROMANEIOSORDEMINSTALACAO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjROMANEIOSORDEMINSTALACAO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjROMANEIOSORDEMINSTALACAO.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjROMANEIOSORDEMINSTALACAO.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;

procedure TObjROMANEIOSORDEMINSTALACAO.Submit_CONCLUIDO(parametro: string);
begin
        Self.CONCLUIDO:=Parametro;
end;
function TObjROMANEIOSORDEMINSTALACAO.Get_CONCLUIDO: string;
begin
        Result:=Self.CONCLUIDO;
end;

procedure TObjROMANEIOSORDEMINSTALACAO.Submit_HORARIO(parametro: string);
begin
        Self.HORARIO:=Parametro;
end;
function TObjROMANEIOSORDEMINSTALACAO.Get_HORARIO: string;
begin
        Result:=Self.HORARIO;
end;
//CODIFICA EXITONKEYDOWN
procedure TObjROMANEIOSORDEMINSTALACAO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJROMANEIOSORDEMINSTALACAO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;

procedure TObjROMANEIOSORDEMINSTALACAO.EdtRomaneioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa('Select * from TabROMANEIO where concluido=''S'' ', ROMANEIO.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self. ROMANEIO.RETORNACAMPOCODIGO).asstring;

                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjROMANEIOSORDEMINSTALACAO.CarregaDados(parametro:string);
begin
    //Carregar dados

    with ObjqueryPesquisa do
    begin
          if(parametro='')
          then Exit;

          Close;
          SQL.Clear;
          SQL.Add('select tabromaneiosordeminstalacao.codigo,tabprojeto.descricao,tabromaneiosordeminstalacao.concluido,');
          SQL.Add('tabromaneiosordeminstalacao.horario,tabromaneiosordeminstalacao.tempomedioinstalacao');
          SQL.Add(',tabromaneiosordeminstalacao.tempofinal as HorarioTermino,tabromaneiosordeminstalacao.pedidoprojeto');
          SQL.Add('from tabromaneiosordeminstalacao');
          SQL.Add('join tabpedido_proj on tabpedido_proj.codigo=tabromaneiosordeminstalacao.pedidoprojeto');
          SQL.Add('join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto') ;
          SQL.Add('where ORDEMINSTALACAO='+parametro);

          Open;
    end;
end;

procedure TObjROMANEIOSORDEMINSTALACAO.Submit_TEMPOMEDIOINSTALACAO(parametro:string);
begin
    TEMPOMEDIOINSTALACAO:=parametro;
end;

function TObjROMANEIOSORDEMINSTALACAO.Get_TEMPOMEDIOINSTALACAO:string;
begin
    Result:=TEMPOMEDIOINSTALACAO;
end;

function TObjROMANEIOSORDEMINSTALACAO.VerificaProjetoInserido(parametro:string):Boolean;
var
  QueryVerifica:TIBQuery;
begin
       QueryVerifica :=TIBQuery.Create(nil);
       QueryVerifica.Database:=FDataModulo.IBDatabase;
       Result:=False;
       try
              with QueryVerifica do
              begin
                   Close;
                   SQL.Clear;
                   SQL.Add('select * from tabromaneiosordeminstalacao');
                   SQL.Add('join tabordeminstalacao on tabordeminstalacao.codigo=tabromaneiosordeminstalacao.ordeminstalacao');
                   SQL.Add('join tabcolocador on tabcolocador.codigo=tabordeminstalacao.colocador');
                   SQL.Add('join tabfuncionarios on tabfuncionarios.codigo=tabcolocador.funcionario');
                   SQL.Add('where pedidoprojeto='+parametro);

                   Open;

                   if(recordcount>0) then
                   begin
                        MensagemAviso('Este projeto j� est� com a instala��o agendada'+#13+'Para a data de '+fieldbyname('data').AsString+#13+'Colocador: '+Fieldbyname('nome').AsString);
                        Result:=True;
                   end;

              end;
       finally
              FreeAndNil(QueryVerifica);
       end;

end;

procedure TObjROMANEIOSORDEMINSTALACAO.Submit_TempoFinal(parametro:string);
begin
    TEMPOFINAL:=parametro;
end;

function TObjROMANEIOSORDEMINSTALACAO.Get_TempoFinal:string;
begin
     Result:=TEMPOFINAL;
end;

function TObjROMANEIOSORDEMINSTALACAO.VerificaHorarioDisponivel(tempoinicial,duracao,tempofinal,ordeminstalacao:string;var projeto:string):Boolean;
var
  Query:TIBQuery;
  TempoInicialLocal,TempoFinalLocal:string;
begin
      Query:= TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;

      result:=False;
      with Query do
      begin
          Close;
          SQL.Clear;
          SQL.Add('select tabromaneiosordeminstalacao.codigo,tabprojeto.descricao,tabromaneiosordeminstalacao.concluido,');
          SQL.Add('tabromaneiosordeminstalacao.horario,tabromaneiosordeminstalacao.tempomedioinstalacao');
          SQL.Add(',tabromaneiosordeminstalacao.tempofinal as HorarioTermino,tabromaneiosordeminstalacao.pedidoprojeto');
          SQL.Add('from tabromaneiosordeminstalacao');
          SQL.Add('join tabpedido_proj on tabpedido_proj.codigo=tabromaneiosordeminstalacao.pedidoprojeto');
          SQL.Add('join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto') ;
          SQL.Add('where ORDEMINSTALACAO='+ordeminstalacao);
          SQL.Add('order by horario');

          Open;

          while not eof do
          begin
                TempoInicialLocal:=fieldbyname('horario').AsString;
                TempoFinalLocal:=fieldbyname('horarioTermino').AsString;

                if(StrToTime (tempoinicial) < StrToTime(TempoInicialLocal)) then
                begin
                     if(StrToTime(tempofinal)> StrToTime(TempoInicialLocal)) then
                     begin
                           result:=False;
                           projeto:= fieldbyname('descricao').AsString;
                           Exit;
                     end
                     else
                     begin
                            Result:=True;
                            Exit;
                     end;

                end
                else
                begin
                     if(StrToTime(tempoinicial)>= StrToTime(TempoInicialLocal)) and (StrToTime(tempoinicial)<= StrToTime(TempoFinalLocal))then
                     begin
                            Result:=False;
                            projeto:= fieldbyname('descricao').AsString;
                            Exit;
                     end;

                end;


                Next;
          end;
          Result:=True;
      end;

end;

procedure TObjROMANEIOSORDEMINSTALACAO.Submit_Observacao(parametro:string);
begin
    OBSERVACAO:=parametro;
end;

function TObjROMANEIOSORDEMINSTALACAO.Get_Observacao:string;
begin
    Result:=OBSERVACAO;
end;

end.



