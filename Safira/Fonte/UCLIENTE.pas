unit UCLIENTE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls,db, UObjCLIENTE,
  UessencialGlobal, Tabs, UObjBairro, UobjRua, UObjCidade,IBQuery,URUA,UBAIRRO,URAMOATIVIDADE,UTIPOCLIENTE,UPLanodeContas,UpesquisaMenu,
  ComCtrls, ImgList;

type
  TFCLIENTE = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigoCliente: TLabel;
    lb1: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    imgrodape: TImage;
    lb9: TLabel;
    btAjuda: TSpeedButton;
    Guia: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    panel2: TPanel;
    panel3: TPanel;
    grpGroupPessoa: TGroupBox;
    rbRadioPessoaFisica: TRadioButton;
    rbRadioPessoaJuridica: TRadioButton;
    lbNome: TLabel;
    lbLbFantasia: TLabel;
    lbLbCPF_CGC: TLabel;
    lbLbemail: TLabel;
    lbLbFone: TLabel;
    lbLbRamoAtividade: TLabel;
    lb3: TLabel;
    lb2: TLabel;
    lbObservacao: TLabel;
    memoObservacao1: TMemo;
    edttipocliente: TEdit;
    edtContato: TEdit;
    edtRamoAtividade: TEdit;
    edtFone: TEdit;
    edtemail: TEdit;
    edtCPF_CGC: TMaskEdit;
    edtFantasia: TEdit;
    edtNome: TEdit;
    lbLbRG_IE: TLabel;
    edtRG_I: TEdit;
    lbLbSexo: TLabel;
    ComboSexo: TComboBox;
    edtCelular: TEdit;
    lbcelular: TLabel;
    edtFax: TEdit;
    lbLbFax: TLabel;
    lbNomeRamoAtividade: TLabel;
    lbnometipocliente: TLabel;
    lb4: TLabel;
    lbCep: TLabel;
    lblokura: TLabel;
    lb6: TLabel;
    lbLbEstadoCobranca: TLabel;
    lbLbCidadeCobranca: TLabel;
    lbLbBairroCobranca: TLabel;
    lb5: TLabel;
    lbNumero: TLabel;
    lbLbEnderecoCobranca: TLabel;
    edtendereco: TEdit;
    edtNumero: TEdit;
    edtComplemento: TEdit;
    edtbairro: TEdit;
    edtestado: TEdit;
    Combocbbcidade: TComboBox;
    edtcodigocidade: TEdit;
    ComboCodigoPais: TComboBox;
    edtCEP: TEdit;
    edtcodigo2: TEdit;
    chkAtivo: TCheckBox;
    lbAtivoInativo: TLabel;
    edtSituacaoSerasa: TEdit;
    edtLimiteCredito: TEdit;
    lbLbLimiteCredito: TLabel;
    lbLbSituacaoSerasa: TLabel;
    lbCodigoPlanoContas: TLabel;
    edtCodigoPlanoContas: TEdit;
    edtBanco: TEdit;
    lbLbBanco: TLabel;
    lbLbAgencia: TLabel;
    edtAgencia: TEdit;
    edtContaCorrente: TEdit;
    edtDataNascimento: TMaskEdit;
    lbLbDataNascimento: TLabel;
    lbLbContaCorrente: TLabel;
    lbLbPermiteVendaAPrazo: TLabel;
    ComboPermiteVendaAPrazo: TComboBox;
    edtDataCadastro: TMaskEdit;
    lb7: TLabel;
    panelDataValorCompra1: TPanel;
    lbLbDataMaiorFatura: TLabel;
    lbLbValorMaiorFatura: TLabel;
    lbLbValorUltimaCompra: TLabel;
    lbLbDataUltimaCompra: TLabel;
    lbDataMaiorFatura: TLabel;
    lbValorMaiorFatura: TLabel;
    lbDataUltimaCompra: TLabel;
    lbValorUltimaCompra: TLabel;
    lb8: TLabel;
    lbValorCredito: TLabel;
    lbNomePlanodeContas: TLabel;
    il2: TImageList;
    procedure edtRamoAtividadeKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtRamoAtividadeExit(Sender: TObject);
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure cbbComboBairroEnter(Sender: TObject);
    procedure cbbComboEnderecoEnter(Sender: TObject);
    procedure cbbComboCidadeEnter(Sender: TObject);
    procedure edtCodigoPlanoContasExit(Sender: TObject);
    procedure edtCodigoPlanoContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure memoObservacao1Exit(Sender: TObject);
    procedure rbRadioPessoaJuridicaClick(Sender: TObject);
    procedure rbRadioPessoaFisicaClick(Sender: TObject);
    procedure edtEdtCEPExit(Sender: TObject);
    procedure edttipoclienteExit(Sender: TObject);
    procedure edttipoclienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnPrimeiroClick(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnProximoClick(Sender: TObject);
    procedure btnUltimoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtCodigoPlanoContasKeyPress(Sender: TObject; var Key: Char);
    procedure edttipoclienteKeyPress(Sender: TObject; var Key: Char);
    procedure edtRamoAtividadeKeyPress(Sender: TObject; var Key: Char);
    procedure ComboSexoKeyPress(Sender: TObject; var Key: Char);
    procedure cbbComboEstadoKeyPress(Sender: TObject; var Key: Char);
    procedure ComboPermiteVendaAPrazoKeyPress(Sender: TObject;
      var Key: Char);
    procedure ComboAtivoKeyPress(Sender: TObject; var Key: Char);
    procedure cbbComboEnderecoKeyPress(Sender: TObject; var Key: Char);
    procedure edtenderecoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtbairroExit(Sender: TObject);
    procedure edtenderecoExit(Sender: TObject);
    procedure edtbairroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbnometipoclienteClick(Sender: TObject);
    procedure lbNomeRamoAtividadeClick(Sender: TObject);
    procedure lbNomeRamoAtividadeMouseLeave(Sender: TObject);
    procedure lbNomeRamoAtividadeMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure NotebookMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbNomePlanodeContasClick(Sender: TObject);
    procedure edtCodigoPlanoContasDblClick(Sender: TObject);
    procedure edttipoclienteDblClick(Sender: TObject);
    procedure edtRamoAtividadeDblClick(Sender: TObject);
    procedure edtenderecoDblClick(Sender: TObject);
    procedure edtbairroDblClick(Sender: TObject);
    procedure edtcidadeDblClick(Sender: TObject);
    procedure CombocbbcidadeExit(Sender: TObject);
    procedure edtestadoExit(Sender: TObject);
    procedure chkAtivoClick(Sender: TObject);
    procedure btAjudaClick(Sender: TObject);
    procedure edtCPF_CGCExit(Sender: TObject);
    procedure GuiaChange(Sender: TObject);
    procedure edtcodigo2Exit(Sender: TObject);
  private

        ObjBairro : TOBJBairro;
        ObjRua : TOBjRua;
        
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure PreencherOCampoPlanoDeContas;
    procedure MostraQuantidadeClientes;
    Procedure LimpaLabels;
   // procedure CarregaEdits(parametro:string);
    { Private declarations }
  public
     // procedure AbreComDados(parametro:string);
     ObjCLIENTE:TObjCLIENTE;

  end;

var
  FCLIENTE: TFCLIENTE;



implementation

uses Upesquisa, UessencialLocal, UDataModulo, UescolheImagemBotao, UCIDADE,
  UobjRAMOATIVIDADE, Uprincipal, UAjuda;

{$R *.dfm}


procedure TFCLIENTE.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCLIENTE=Nil)
     Then exit;

     If (Self.ObjCLIENTE.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;
    Self.tag:=0;
    Self.ObjCLIENTE.free;
    Self.ObjBairro.Free;
    Self.OBjRua.Free;
    
end;


procedure TFCLIENTE.btnovoClick(Sender: TObject);
begin
      Self.limpaLabels;


     lbCodigoCliente.Caption:='0';
     if (SistemaemModoDemoGLOBAL=True)
     Then Begin
               if (FDataModulo.ContaRegistros('TABCLIENTE')>50)
               Then Begin
                         MostraMensagemSistemaDemo('S� � poss�vel cadastrar acima de 50 Clientes');
                         exit;
               End;
     End;


     limpaedit(Self);
     edtendereco.Text:='';
     edtbairro.Text:='';
     edtestado.Text:=ESTADOSISTEMAGLOBAL;
     lbValorUltimaCompra.caption:='0,00';
     lbValorMaiorFatura.caption:='0';
     ComboPermiteVendaAPrazo.itemindex:=0;
     EdtLimiteCredito.text:='0';
     EdtRamoAtividade.text:='1';

     Edttipocliente.text:='1';

     habilita_campos(Self);
     PanelDataValorCompra1.Enabled:=false;
     desab_botoes(Self);
     EdtDataCadastro.Text:=DateToStr(Date);

     //edtValorCredito.text:='0';
     lbValorCredito.Caption:='0.00';
     btalterar.visible:=false;
     btrelatorios.visible:=false;
     btsair.Visible:=false;
     btopcoes.visible:=false;
     btexcluir.Visible:=false;
     btnovo.Visible:=false;
     btopcoes.visible:=false;

     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjCLIENTE.status:=dsInsert;
     Guia.TabIndex:=0;

     rbRadioPessoaFisica.Checked:=true;

     PreencherOCampoPlanoDeContas;

     EdtCPF_CGC.EditMask:=MascaraCPF;
     chkAtivo.Checked:=True;
     lbAtivoInativo.Caption:='Ativo';
     EdtNome.SetFocus;

end;

procedure TFCLIENTE.btSalvarClick(Sender: TObject);
begin

     If Self.ObjCLIENTE.Status=dsInactive
     Then exit;

    //Verfica se ja existe algum cliente com o mesmo CPF
    if(VerificaDuplicidadeCPFCGCCadastro('TABCLIENTE','CPF_CGC',edtCPF_CGC.Text,lbCodigoCliente.Caption)=True) then
    begin
       If (Messagedlg('Existe um outro cliente cadastrado com este mesmo CPF_CGC. Deseja gravar este cliente com o mesmo CPF_CGC?',mtconfirmation,[mbyes,mbno],0)=Mrno) Then
       begin
         edtCPF_CGC.Text:='';
         edtCPF_CGC.SetFocus;
         exit;
       end;
    end;

     If ControlesParaObjeto=False
     Then
     Begin
        Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
        exit;
     End;

     If (Self.ObjCLIENTE.salvar(true)=False)
     Then exit;

     lbCodigoCliente.caption:= Self.ObjCLIENTE.Get_codigo;
     habilita_botoes(Self);
     btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=True;

     PanelDataValorCompra1.Enabled:=true;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
     MostraQuantidadeClientes;
     Guia.TabIndex:=0;

end;

procedure TFCLIENTE.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCLIENTE.Status=dsinactive) and (lbCodigoCliente.caption<>'')
    Then Begin
               habilita_campos(Self);
               PanelDataValorCompra1.Enabled:=false;
               Self.ObjCLIENTE.Status:=dsEdit;
               guia.TabIndex:=0;
               desab_botoes(Self);
               btSalvar.enabled:=True;
               BtCancelar.enabled:=True;
               btpesquisar.enabled:=True;
               btalterar.visible:=false;
               btrelatorios.visible:=false;
               btsair.Visible:=false;
               btopcoes.visible:=false;
               btexcluir.Visible:=false;
               btnovo.Visible:=false;
               btopcoes.visible:=false;

               //Antes era feito da seguinte forma:
               //O credito do cliente era gravado na tabcliente, agora � feito no padr�o do financeiro lanca�ando lan�amentos de creditos
              {if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('ALTERAR VALOR CR�DITO DE BONIFICA��O NO CADASTRO DE CLIENTE')=True)
               then edtValorCredito.Enabled:=True;   }
                
               edtNome.setfocus;
    End;

end;

procedure TFCLIENTE.btCancelarClick(Sender: TObject);
begin
     Self.ObjCLIENTE.cancelar;
     limpaedit(Self);
     edtendereco.Text:='';
     edtestado.Text:='';
     edtbairro.Text:='';
     Combocbbcidade.Text:='';
     lbNomePlanodeContas.Caption:='';

     edtendereco.Text:='';
     edtbairro.Text:='';
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
      btopcoes.visible:=true;

      Guia.TabIndex:=0;
    // PanelDataValorCompra.Enabled:=false;

end;

procedure TFCLIENTE.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        EdtCPF_CGC.EditMask:='';
        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCLIENTE.Get_pesquisa,Self.ObjCLIENTE.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCLIENTE.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCLIENTE.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjCLIENTE.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            edtendereco.Text:='';
                                            edtestado.Text:='';
                                            edtbairro.Text:='';
                                            Combocbbcidade.Text:='';

                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFCLIENTE.btExcluirClick(Sender: TObject);
begin
     If (Self.ObjCLIENTE.status<>dsinactive) or (lbCodigoCliente.caption='')
     Then exit;

     If (Self.ObjCLIENTE.LocalizaCodigo(lbCodigoCliente.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCLIENTE.exclui(lbCodigoCliente.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     edtendereco.Text:='';
     edtestado.Text:='';
     edtbairro.Text:='';
     Combocbbcidade.Text:='';

     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCLIENTE.btRelatorioClick(Sender: TObject);
begin
    Self.ObjCLIENTE.Imprime(lbCodigoCliente.caption);
    Guia.TabIndex:=0; {Rodolfo}
end;

procedure TFCLIENTE.btSairClick(Sender: TObject);
begin
    Self.close;
end;


procedure TFCLIENTE.btFecharClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFCLIENTE.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFCLIENTE.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    If Key=VK_Escape
    Then Self.Close;

     if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;


    end;

    if (Key = VK_F2) then
    begin

           FAjuda.PassaAjuda('CADASTRO DE CLIENTES');
           FAjuda.ShowModal;
    end;
end;

procedure TFCLIENTE.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFCLIENTE.ControlesParaObjeto: Boolean;
begin
  Try
    With Self.ObjCLIENTE do
    Begin
        Submit_Codigo(lbCodigoCliente.caption);
        Submit_Codigo2(edtCodigo2.text);

        Submit_Nome(edtNome.text);
        Submit_Sexo(ComboSexo.text);
        Submit_Fantasia(edtFantasia.text);

        if (rbRadioPessoaFisica.Checked = true)then
        Submit_Fisica_Juridica('F')
        else if (rbRadioPessoaJuridica.Checked = true)then
        Submit_Fisica_Juridica('J');

        Submit_CPF_CGC(edtCPF_CGC.text);
        Submit_RG_IE(edtRG_I.text);
        RamoAtividade.Submit_codigo(edtRamoAtividade.text);
        tipocliente.Submit_codigo(edttipocliente.text);

        Submit_email(edtemail.text);
        Submit_Fone(edtFone.text);
        Submit_Celular(edtCelular.text);
        Submit_Fax(edtFax.text);
        Submit_Endereco(edtendereco.text);
        Submit_Bairro(edtbairro.text);
        Submit_Cidade(combocbbcidade.text);
        Submit_CEP(edtCEP.text);
        Submit_Estado(edtestado.text);
        Submit_DataCadastro(edtDataCadastro.text);

        if (lbDataUltimaCompra.caption='  /  /    ')then
        Submit_DataUltimaCompra('01/01/1500')
        else Submit_DataUltimaCompra(lbDataUltimaCompra.caption);

        Submit_ValorUltimaCompra(lbValorUltimaCompra.caption);

        if (lbDataMaiorFatura.caption='  /  /    ')then
        Submit_DataMaiorFatura('01/01/1500')
        else Submit_DataMaiorFatura(lbDataMaiorFatura.caption);

        Submit_ValorMaiorFatura(lbValorMaiorFatura.caption);
        Submit_SituacaoSerasa(edtSituacaoSerasa.text);
        Submit_LimiteCredito(edtLimiteCredito.text);
        CodigoPlanoContas.Submit_CODIGO(edtCodigoPlanoContas.text);
        Submit_Banco(edtBanco.text);
        Submit_Agencia(edtAgencia.text);
        Submit_ContaCorrente(edtContaCorrente.text);

        if (edtDataNascimento.text='  /  /    ')then
        Submit_DataNascimento('01/01/1500')
        else Submit_DataNascimento(edtDataNascimento.text);

        Submit_PermiteVendaAPrazo(ComboPermiteVendaAPrazo.text);
        Submit_Observacao(MemoObservacao1.text);
        Submit_Contato(edtContato.Text);
        Submit_Numero(edtNumero.Text);
        Submit_Complemento(EdtComplemento.Text);
        Submit_codigocidade(Edtcodigocidade.Text);
        Submit_ValorCredito(lbValorCredito.Caption);

        submit_codigopais         (retornaCodigoPais (comboCodigoPais.Text));

        if(chkAtivo.Checked=True)
        then Submit_Ativo('S')
        else Submit_Ativo('N');

        result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFCLIENTE.LimpaLabels;
begin
    LbNomeRamoAtividade.Caption:='';
    LbNometipocliente.Caption:='';
    lbCodigoCliente.Caption:='';
end;

function TFCLIENTE.ObjetoParaControles: Boolean;
begin
  Try
     With Self.ObjCLIENTE do
     Begin
        EdtCodigo2.text:=Get_Codigo2;

        lbCodigoCliente.caption:= Get_Codigo;

        EdtNome.text:=Get_Nome;
        ComboSexo.text:=Get_Sexo;
        EdtFantasia.text:=Get_Fantasia;

        EdtCPF_CGC.EditMask:='';
        EdtCPF_CGC.text:=comebarra(Get_CPF_CGC);
        EdtCPF_CGC.text:=tira_ponto(EdtCPF_CGC.text);
        EdtCPF_CGC.text:=come(EdtCPF_CGC.text,'-');

        if (Get_Fisica_Juridica = 'F')
        then Begin
                  rbRadioPessoaFisica.Checked:=true;
                  EdtCPF_CGC.EditMask:=MascaraCPF;
        End
        else
           if (Get_Fisica_Juridica = 'J')
           then Begin
                  rbRadioPessoaJuridica.Checked:=true;
                  EdtCPF_CGC.EditMask:=MascaraCNPJ;
           End;


        EdtRG_I.text:=Get_RG_IE;
        EdtRamoAtividade.text:=RamoAtividade.Get_codigo;
        lbNomeRamoAtividade.Caption:=RamoAtividade.Get_Nome;
        Edttipocliente.text:=tipocliente.Get_codigo;
        LbNomeTipoCliente.caption:=TipoCliente.get_nome;
        Edtemail.text:=Get_email;
        EdtFone.text:=Get_Fone;
        EdtCelular.text:=Get_Celular;
        EdtFax.text:=Get_Fax;
        edtendereco.text:=Get_Endereco;
        edtbairro.text:=Get_Bairro;
        combocbbcidade.text:=Get_Cidade;
        EdtCEP.text:=Get_CEP;
        edtestado.text:=Get_Estado;
        EdtDataCadastro.text:=Get_DataCadastro;

        if (Get_DataUltimaCompra = '01/01/1500')then
        lbDataUltimaCompra.caption:=''
        else lbDataUltimaCompra.caption:=Get_DataUltimaCompra;

        lbValorUltimaCompra.caption:=Get_ValorUltimaCompra;

        if (Get_DataMaiorFatura='01/01/1500')then
        lbDataMaiorFatura.caption:=''
        else lbDataMaiorFatura.caption:=Get_DataMaiorFatura;

        lbValorMaiorFatura.caption:=Get_ValorMaiorFatura;
        EdtSituacaoSerasa.text:=Get_SituacaoSerasa;
        EdtLimiteCredito.text:=Get_LimiteCredito;
        EdtCodigoPlanoContas.text:=CodigoPlanoContas.Get_CODIGO;
        lbNomePlanodeContas.Caption:=CodigoPlanoContas.Get_Nome;
        EdtBanco.text:=Get_Banco;
        EdtAgencia.text:=Get_Agencia;
        EdtContaCorrente.text:=Get_ContaCorrente;

        if (Get_DataNascimento='01/01/1500')then
        EdtDataNascimento.text:=''
        else EdtDataNascimento.text:=Get_DataNascimento;

        ComboPermiteVendaAPrazo.text:=Get_PermiteVendaAPrazo;
        if(Get_Ativo='S') then
        begin
            chkAtivo.Checked:=True;
            lbAtivoInativo.Caption:='Ativo';
        end
        else
        begin
            chkAtivo.Checked:=False;
            lbAtivoInativo.Caption:='Inativo';
        end;
        MemoObservacao1.text:=Get_Observacao;
        edtContato.Text:=Get_Contato;
        edtNumero.Text:=Get_Numero;
        EdtComplemento.Text:=Get_Complemento;
        Edtcodigocidade.Text:=Get_codigocidade;
        //Edtvalorcredito.text:=Get_valorcredito;
        lbValorCredito.Caption:=Get_ValorCredito;
        comboCodigoPais.Text            := retornaPais (get_codigopais);
        
        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFCLIENTE.TabelaParaControles: Boolean;
begin
     If (Self.ObjCLIENTE.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFCLIENTE.edtRamoAtividadeExit(Sender: TObject);
begin
  Self.ObjCLIENTE.EdtRamoAtividadeExit(Sender, LbNomeRamoAtividade);
end;

procedure TFCLIENTE.edtRamoAtividadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  Self.ObjCLIENTE.EdtRamoAtividadeKeyDown(Sender, Key, Shift, LbNomeRamoAtividade);
end;

procedure TFCLIENTE.cbbComboBairroEnter(Sender: TObject);
begin
    edtbairro.Text:=Self.ObjCLIENTE.Get_Bairro;
end;

procedure TFCLIENTE.cbbComboEnderecoEnter(Sender: TObject);
begin
    edtendereco.Text:=Self.ObjCLIENTE.Get_Endereco;

end;

procedure TFCLIENTE.cbbComboCidadeEnter(Sender: TObject);
begin
    //Self.ObjCLIENTE.Objcidade.ResgataCidade(cbbComboCidade,cbbComboEstado.Text );
    Combocbbcidade.Text:=Self.ObjCLIENTE.Get_Cidade;
end;

procedure TFCLIENTE.edtCodigoPlanoContasExit(Sender: TObject);
begin
    ObjCLIENTE.EdtCodigoPlanoContasExit(Sender, LbNomePlanodeContas);
end;

procedure TFCLIENTE.edtCodigoPlanoContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjCLIENTE.EdtCodigoPlanoContasKeyDown(Sender, Key, Shift, LbNomePlanodeContas);
end;

procedure TFCLIENTE.PreencherOCampoPlanoDeContas;
begin
     if (ObjParametroGlobal.ValidaParametro('CODIGO PLANO DE CONTAS CLIENTE')=false)then
     Begin
          MensagemErro('Parametro "CODIGO PLANO DE CONTAS CLIENTE" n�o foi encontrado.');
          exit;
     end;

     EdtCodigoPlanoContas.Text:=ObjParametroGlobal.Get_Valor;
end;

procedure TFCLIENTE.memoObservacao1Exit(Sender: TObject);
begin
    //Guia.TabIndex:=1;
end;

{r4mr}
procedure TFCLIENTE.rbRadioPessoaJuridicaClick(Sender: TObject);
begin
     ComboSexo.itemindex:=2;

    //Jonatan Medina, comentei este trecho pq estava errado a ideia do rodolfo     

     // Caso mude de cnpj para cpf limpa todo o edit   -   rodolfo
     {if(EdtCPF_CGC.GetTextLen < 18) then
        EdtCPF_CGC.text := '';   }

     EdtCPF_CGC.EditMask:=MascaraCNPJ;
end;

{r4mr}
procedure TFCLIENTE.rbRadioPessoaFisicaClick(Sender: TObject);
begin
    ComboSexo.itemindex:=0;

    //Jonatan Medina, comentei este trecho pq estava errado a ideia do rodolfo

    // Caso mude de cnpj para cpf limpa todo o edit - rodolfo
   { if(EdtCPF_CGC.GetTextLen > 14) then
       EdtCPF_CGC.text := '';    }

    EdtCPF_CGC.EditMask:=MascaraCPF;

end;

procedure TFCLIENTE.edtEdtCEPExit(Sender: TObject);
begin
    Guia.TabIndex:=2;
end;



procedure TFCLIENTE.edttipoclienteExit(Sender: TObject);
begin
  Self.ObjCLIENTE.EdttipoclienteExit(Sender, LbNometipocliente);
end;

procedure TFCLIENTE.edttipoclienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  Self.ObjCLIENTE.EdttipoclienteKeyDown(Sender, Key, Shift, LbNometipocliente);
end;

procedure TFCLIENTE.btnPrimeiroClick(Sender: TObject);
begin
    if  (Self.Objcliente.PrimeiroRegistro = false)
    then exit;

    Objcliente.TabelaparaObjeto;
    Self.ObjetoParaControles;


end;

procedure TFCLIENTE.btnAnteriorClick(Sender: TObject);
begin
    if (lbCodigoCliente.caption='')
    then lbCodigoCliente.caption:='0';

    if  (Objcliente.RegistoAnterior(StrToInt(lbCodigoCliente.caption)) = false)then
    exit;

    Objcliente.TabelaparaObjeto;
    Self.ObjetoParaControles;
end;

procedure TFCLIENTE.btnProximoClick(Sender: TObject);
begin
    if (lbCodigoCliente.caption='')
    then lbCodigoCliente.caption:='0';

    if  (Objcliente.ProximoRegisto(StrToInt(lbCodigoCliente.caption)) = false)then
    exit;

    Objcliente.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFCLIENTE.btnUltimoClick(Sender: TObject);
begin
    if  (Objcliente.UltimoRegistro = false)
    then exit;

    Objcliente.TabelaparaObjeto;
    Self.ObjetoParaControles;
end;

procedure TFCLIENTE.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     edtendereco.Text:='';
     edtestado.Text:='';
     edtbairro.Text:='';
     Combocbbcidade.Text:='';
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.TabIndex:=0;
     ColocaUpperCaseEdit(Self);


     Try
        Self.ObjCLIENTE:=TObjCLIENTE.create;
        Self.ObjBairro:=TObjBairro.Create;
        Self.OBjRua := TOBjRua.Create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     lbCodigoCliente.caption:='';
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE CLIENTE')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);
     MostraQuantidadeClientes;

     if not (ObjCLIENTE.carregaCidades(Combocbbcidade)) then
        MensagemAviso('ERRO ao carregar as cidades');

     if not (ObjCliente.carregaCodigoPaises(comboCodigoPais)) then
        MensagemAviso('ERRO ao carregar pa�ses');

     if(Tag<>0)
     then begin
            if(ObjCLIENTE.LocalizaCodigo(inttostr(Self.tag))=True)
            then begin
              ObjCLIENTE.TabelaparaObjeto;
              self.ObjetoParaControles;
            end;
     end;
     self.Color:=clBtnFace;
end;

procedure TFCLIENTE.MostraQuantidadeClientes;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabcliente');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb9.Caption:='Existem '+IntToStr(contador)+' clientes cadastrados'
       else
       begin
            lb9.Caption:='Existe '+IntToStr(contador)+' cliente cadastrado';
       end;

    finally

    end;


end;


procedure TFCLIENTE.edtCodigoPlanoContasKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFCLIENTE.edttipoclienteKeyPress(Sender: TObject; var Key: Char);
begin
        if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFCLIENTE.edtRamoAtividadeKeyPress(Sender: TObject;
  var Key: Char);
begin
         if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFCLIENTE.ComboSexoKeyPress(Sender: TObject; var Key: Char);
begin
        Key:= #0;

end;

procedure TFCLIENTE.cbbComboEstadoKeyPress(Sender: TObject; var Key: Char);
begin
    Key:= #0;

end;

procedure TFCLIENTE.ComboPermiteVendaAPrazoKeyPress(Sender: TObject;
  var Key: Char);
begin
     Key:= #0;
end;

procedure TFCLIENTE.ComboAtivoKeyPress(Sender: TObject; var Key: Char);
begin
   Key:= #0;
end;

procedure TFCLIENTE.cbbComboEnderecoKeyPress(Sender: TObject;
  var Key: Char);
begin
      Key:= #0;
end;

procedure TFCLIENTE.edtenderecoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Frua:TFrua;
begin
   If (key =vk_f9)
   Then
   begin
    Try
      Fpesquisalocal:=Tfpesquisa.create(self);
      Frua:=TFRUA.Create(nil);
      If (FpesquisaLocal.PreparaPesquisa('select * from TABRUA','Pesquisa de endere�os',Frua)=True)
      Then
      Begin
        Try
          If (FpesquisaLocal.showmodal=mrok)
          Then  begin
            edtendereco.Text :=FpesquisaLocal.querypesq.fieldbyname('nome').asstring;
          end;
        Finally
          FpesquisaLocal.querypesq.close;
        End;
      End;
    Finally
      FreeandNil(FPesquisaLocal);
      FreeAndNil(Frua);
    End;
   end;

end;
procedure TFCLIENTE.edtcidadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:TFpesquisa;
   StringPesquisa:string;
   Fcidade:TFCIDADE;
begin
     if (key<>vk_f9)
     Then exit;

     if(edtestado.Text<>'') then
     begin
          StringPesquisa:=('Select * from tabcidade where UF ='+#39+edtestado.Text+#39);
     end
     else
     begin
       StringPesquisa:='Select * from tabcidade';
     end;

     Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
           Fcidade:=TFCIDADE.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa(StringPesquisa,'Pesquisa de Cidades',FCIDADE)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  combocbbcidade.Text:=FpesquisaLocal.querypesq.fieldbyname('nome').asstring;
                                  edtestado.Text:=FpesquisaLocal.querypesq.fieldbyname('UF').asstring;
                                  edtcodigocidade.text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fcidade);
     End;
end;

procedure TFCLIENTE.edtbairroExit(Sender: TObject);
begin
    Self.ObjBairro.CadastraBairro(edtbairro.Text);
end;

procedure TFCLIENTE.edtenderecoExit(Sender: TObject);
begin
    Self.OBjRua.CadastraRua(edtendereco.Text);
end;

procedure TFCLIENTE.edtbairroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fbairro:TFBAIRRO;
begin
   If (key =vk_f9)
   Then begin
        Try

            Fpesquisalocal:=Tfpesquisa.create(self);
            Fbairro:=TFBAIRRO.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('select * from tabbairro','Pesquisa de endere�os',Fbairro)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then  begin
                            edtbairro.Text :=FpesquisaLocal.querypesq.fieldbyname('nome').asstring;

                        end;
                      Finally
                             FpesquisaLocal.querypesq.close;
                      End;
                  End;


        Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fbairro);
        End;
     end;

end;

{procedure TFCLIENTE.AbreComDados(parametro:string);
begin
     LocalizaPorCodigo:=1;
end;

procedure TFCLIENTE.CarregaEdits(parametro:string);
begin
    if(ObjCLIENTE.LocalizaCodigo(parametro)=True)
    then begin
      ObjCLIENTE.TabelaparaObjeto;
      self.ObjetoParaControles;
    end;
end; }


procedure TFCLIENTE.lbnometipoclienteClick(Sender: TObject);
var
  FtipoCliente:TFTIPOCLIENTE;
begin
   if(edttipocliente.Text='')
   then Exit;

   try
      FtipoCliente:=TFTIPOCLIENTE.Create(nil);
   except

   end;

   try
      FtipoCliente.Tag:=StrToInt(edttipocliente.Text);
      FtipoCliente.ShowModal;
   finally
      FreeAndNil(FtipoCliente);
   end;



end;

procedure TFCLIENTE.lbNomeRamoAtividadeClick(Sender: TObject);
Var
  FramoAtividade:TFRAMOATIVIDADE;
begin
     if(EdtRamoAtividade.Text='')
     then Exit;

     try
       FramoAtividade:=TFRAMOATIVIDADE.Create(nil);

     except

     end;

     try
       FramoAtividade.Tag:=StrToInt(EdtRamoAtividade.Text);
       FramoAtividade.ShowModal;
     finally
        FreeAndNil(FramoAtividade);
     end;



end;

procedure TFCLIENTE.lbNomeRamoAtividadeMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFCLIENTE.lbNomeRamoAtividadeMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
      ScreenCursorProc(crHandPoint);
end;

procedure TFCLIENTE.NotebookMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ScreenCursorProc(crDefault);
end;

procedure TFCLIENTE.lbNomePlanodeContasClick(Sender: TObject);
var
  Fplanocontas:TFplanodeContas;
begin
    if(EdtCodigoPlanoContas.Text = '')
    then Exit;

    try
          Fplanocontas:=TFplanodeContas.Create(nil);
    except

    end;

    try
          Fplanocontas.Tag:=StrToInt(EdtCodigoPlanoContas.Text);
          Fplanocontas.ShowModal;
    finally
          FreeAndNil(Fplanocontas);
    end;

end;

procedure TFCLIENTE.edtCodigoPlanoContasDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FplanoContas:TFplanodeContas;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FplanoContas:=TFplanodeContas.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabplanodecontas','',FplanoContas)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtCodigoPlanoContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbNomePlanodeContas.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FplanoContas);

     End;
end;

procedure TFCLIENTE.edttipoclienteDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FtipoCliente:TFTIPOCLIENTE;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FtipoCliente:=TFTIPOCLIENTE.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabtipocliente','',FtipoCliente)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 edttipocliente.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbnometipocliente.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FtipoCliente);

     End;
end;


procedure TFCLIENTE.edtRamoAtividadeDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FramoAtividade:TFRAMOATIVIDADE ;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FramoAtividade:=TFRAMOATIVIDADE.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabramoatividade','',FramoAtividade)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtRamoAtividade.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbNomeRamoAtividade.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FramoAtividade);

     End;
end;


procedure TFCLIENTE.edtenderecoDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Frua:TFrua;
begin
       Try

              Fpesquisalocal:=Tfpesquisa.create(self);
              Frua:=TFRUA.Create(nil);
              If (FpesquisaLocal.PreparaPesquisa('select * from TABRUA','Pesquisa de endere�os',Frua)=True)
              Then
              Begin
                        Try
                                If (FpesquisaLocal.showmodal=mrok)
                                Then  begin
                                    edtendereco.Text :=FpesquisaLocal.querypesq.fieldbyname('nome').asstring;

                                end;
                        Finally
                               FpesquisaLocal.querypesq.close;
                        End;
              End;

        Finally

              FreeandNil(FPesquisaLocal);
              FreeAndNil(Frua);
        End;
   

end;

procedure TFCLIENTE.edtbairroDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Fbairro:TFBAIRRO;
begin
       Try

            Fpesquisalocal:=Tfpesquisa.create(self);
            Fbairro:=TFBAIRRO.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('select * from tabbairro','Pesquisa de endere�os',Fbairro)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then  begin
                            edtbairro.Text :=FpesquisaLocal.querypesq.fieldbyname('nome').asstring;

                        end;
                      Finally
                             FpesquisaLocal.querypesq.close;
                      End;
                  End;


        Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fbairro);
        End;
    

end;


procedure TFCLIENTE.edtcidadeDblClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
   StringPesquisa:string;
   Fcidade:TFCIDADE;
begin
     if(edtestado.Text<>'') then
     begin
          StringPesquisa:=('Select * from tabcidade where UF ='+#39+edtestado.Text+#39);
     end
     else
     begin
       StringPesquisa:='Select * from tabcidade';
     end;

     Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
           Fcidade:=TFCIDADE.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa(StringPesquisa,'Pesquisa de Cidades',FCIDADE)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  combocbbcidade.Text:=FpesquisaLocal.querypesq.fieldbyname('nome').asstring;
                                  edtestado.Text:=FpesquisaLocal.querypesq.fieldbyname('UF').asstring;
                                  edtcodigocidade.text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fcidade);
     End;
end;

procedure TFCLIENTE.CombocbbcidadeExit(Sender: TObject);
begin
      if (Combocbbcidade.Text <> '') then
      begin
        edtestado.Text := ObjCliente.retornaUF(Combocbbcidade.Text);

        if (edtestado.Text <> '') then
        begin
          edtestadoExit(edtestado);
          ComboCodigoPais.SetFocus;
        end

      end
end;

procedure TFCLIENTE.edtestadoExit(Sender: TObject);
var
   cidade:string;
begin
  cidade := ObjCliente.retornaCidade(combocbbcidade.Text);
  edtCodigoCidade.Text := ObjCliente.retornaCodigoCidade (cidade,edtestado.Text);

end;

procedure TFCLIENTE.chkAtivoClick(Sender: TObject);
begin
  if(lbCodigoCLIENTE.Caption='') or (lbCodigoCLIENTE.Caption='0')
  then Exit;
  if(chkAtivo.Checked=True)
  then lbAtivoInativo.Caption:='Ativo';

  if(chkAtivo.Checked=False)
  then  lbAtivoInativo.Caption:='Inativo'
end;

procedure TFCLIENTE.btAjudaClick(Sender: TObject);
begin
  FAjuda.PassaAjuda('CADASTRO DE CLIENTES');
  FAjuda.ShowModal;
end;

procedure TFCLIENTE.edtCPF_CGCExit(Sender: TObject);
begin
  //Verfica se ja existe algum cliente com o mesmo CPF
  if(VerificaDuplicidadeCPFCGCCadastro('TABCLIENTE','CPF_CGC',edtCPF_CGC.Text,lbCodigoCliente.Caption)=True) then
  begin
     If (Messagedlg('Existe um outro cliente cadastrado com este mesmo CPF_CGC. Deseja gravar este cliente com o mesmo CPF_CGC?',mtconfirmation,[mbyes,mbno],0)=Mrno) Then
     begin
       edtCPF_CGC.Text:='';
       exit;
     end;
  end;
end;

procedure TFCLIENTE.GuiaChange(Sender: TObject);
begin
//
end;

procedure TFCLIENTE.edtcodigo2Exit(Sender: TObject);
begin
  Guia.TabIndex:=1;
end;

end.




