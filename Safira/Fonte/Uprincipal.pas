unit Uprincipal;

interface

uses                      
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Menus, ExtCtrls, ComCtrls,
  ToolWin, Buttons,IBQuery,Uobjrelatoriopersonalizadoextendido,
  jpeg,Uobjencerrasistema, StdCtrls, ExtDlgs,UPesquisaProjNovo,Uestado,UModeloDeNF,ShellAPI,UOrdemServico,ULancCredito,UAjuda, UOrdemMedicao, UCadastroAjuda
  ,UordemInstalacao, UAgendaInstalacao,UVendaRapida, OleCtrls, SHDocVw,UCONTADOR,UUNIDADEMEDIDA,UAcertaUnidades, UessencialGlobal,UobjNCM,UobjKITBOX,
  UPedidoonline, uEnderecoEntrega;

type                                                       
  TFprincipal = class(TForm)
  
    MenuSysmaster   : TMainMenu;             
    sOBRE1          : TMenuItem;
    Manuteno1       : TMenuItem;
    Backup1         : TMenuItem;
    Loggof1         : TMenuItem;
    Sair2           : TMenuItem;
    Parametros1     : TMenuItem;
    Usuarios1       : TMenuItem;
    Cadastrodenveis1: TMenuItem;
    Cadastrodeusurios1: TMenuItem;                                                                                       
    Permisses3: TMenuItem;
    Importao1: TMenuItem;
    Ajustes1: TMenuItem;
    AcertoseAjustesnoBD1: TMenuItem;
    ScriptsSQL1: TMenuItem;
    MenuFinanceiro: TMenuItem;
    Janelas1: TMenuItem;
    Organizarcones1: TMenuItem;
    TimerFechaSistema: TTimer;
    Cadastros1: TMenuItem;
    GrupodeFerrragens1: TMenuItem;
    GrupodeServios1: TMenuItem;
    GrupodeFerragens1: TMenuItem;
    GrupodeVidros1: TMenuItem;
    Grupos1: TMenuItem;
    RamoAtividade1: TMenuItem;
    Fornecedor1: TMenuItem;
    Cor1: TMenuItem;
    Ferragens1: TMenuItem;
    Servio1: TMenuItem;
    Perfilados1: TMenuItem;
    Vidros1: TMenuItem;
    KitBox1: TMenuItem;
    Compoentes1: TMenuItem;
    Clientes1: TMenuItem;
    Vendedor1: TMenuItem;
    Projeto1: TMenuItem;
    Pedido1: TMenuItem;
    GrupodeDiversos1: TMenuItem;
    Diversos1: TMenuItem;
    GrupodePersianas1: TMenuItem;
    Persiana1: TMenuItem;
    Diametro1: TMenuItem;
    CalculaFrmula1: TMenuItem;
    Bairro1: TMenuItem;
    Rua1: TMenuItem;
    Cidade1: TMenuItem;
    EntadadeProdutos1: TMenuItem;
    Materiais1: TMenuItem;
    Localidade1: TMenuItem;
    Outros1: TMenuItem;
    AtualizaExecutvel1: TMenuItem;
    ransportadora1: TMenuItem;
    Romaneio1: TMenuItem;
    PedidoProjetonoRomaneio1: TMenuItem;
    DP1: TMenuItem;
    CargodeFuncionrios1: TMenuItem;
    Funcionrios1: TMenuItem;
    Comisso1: TMenuItem;
    FaixadeDesconto1: TMenuItem;
    ComissodeVendedores1: TMenuItem;
    FolhadePagamento1: TMenuItem;
    FuncionriosporFolha1: TMenuItem;
    Romaneios1: TMenuItem;
    Colocador1: TMenuItem;
    Consultas1: TMenuItem;
    PedidoRpido1: TMenuItem;
    Materiais2: TMenuItem;
    AtualizaCNPJeCPF1: TMenuItem;
    RelacFolhaComisses1: TMenuItem;
    ComissodoColocador1: TMenuItem;
    Extras1: TMenuItem;
    FaixadeDescontoIRPF1: TMenuItem;
    HoraExtra1: TMenuItem;
    FaixadeDescontodeINSS1: TMenuItem;
    Adiantamento1: TMenuItem;
    Menurel: TMenuItem;
    Vendedores1: TMenuItem;
    Comisso2: TMenuItem;
    SalrioFamlia1: TMenuItem;
    ransfereBackuppersianas1: TMenuItem;
    Gratificaes1: TMenuItem;
    Arquiteto1: TMenuItem;
    NotaFiscal1: TMenuItem;
    CFOP1: TMenuItem;
    NotaFiscal2: TMenuItem;
    ConfiguraodeNF1: TMenuItem;
    rocadeMateriaisemPedidosConcludos1: TMenuItem;
    RefazContabilidadeEntradaseSaidas1: TMenuItem;
    Estoque1: TMenuItem;
    EstoqueporMaterial1: TMenuItem;
    Financeiro1: TMenuItem;
    ContasaReceberporVendedor1: TMenuItem;
    ReajustaValores1: TMenuItem;
    AcrscimodeDSR1: TMenuItem;
    DescontodeContSindical1: TMenuItem;
    GeraContabilidadeChDevcomFornecedor1: TMenuItem;
    MovimentaoDiria1: TMenuItem;
    Fiscal1: TMenuItem;
    RelatriodeItensemNF1: TMenuItem;
    PsVenda1: TMenuItem;
    Compras1: TMenuItem;
    PedidodeCompra1: TMenuItem;
    MenuFolhaPagamento: TMenuItem;
    LotedeComissodeVendedores1: TMenuItem;
    Pedidos1: TMenuItem;
    ComponentesdoPedidoprojeto1: TMenuItem;
    ImportaodePedidos1: TMenuItem;
    GeraSqlsdeAlteracaodeDatasnaContabilidade1: TMenuItem;
    FluxodeCaixaExcel1: TMenuItem;
    MenuPersonalizado: TMenuItem;
    ImportaodeDados1: TMenuItem;
    ArquivoINI1: TMenuItem;
    FinalizaPedidosProjetos1: TMenuItem;
    Romaneio2: TMenuItem;
    ComissodeColocador1: TMenuItem;
    Ajuda1: TMenuItem;
    Novidades1: TMenuItem;
    HabilitarCriaodecone1: TMenuItem;
    DesabilitarCriaodecone1: TMenuItem;
    TimerIcone: TTimer;
    CadastrodeEmpresa1: TMenuItem;
    RelatrioPersonalizadoReportBuilder1: TMenuItem;
    ConfiguraodeImpresso1: TMenuItem;
    ConfiguraodeDuplicataModelo11: TMenuItem;
    ConfiguraodeDuplicataModelo21: TMenuItem;
    ConfiguraBoletoPrImpresso1: TMenuItem;
    ImpostoICMS1: TMenuItem;
    ipodeCliente1: TMenuItem;
    SituaoTributriaIPI1: TMenuItem;
    ImpostoIPI1: TMenuItem;
    SituaoTributriaPIS1: TMenuItem;
    SituaoTributriaCOFINS1: TMenuItem;
    ImpostoPIS1: TMenuItem;
    ImpostoCofins1: TMenuItem;
    IESubtTributrioporEstado1: TMenuItem;
    OperaodeNF1: TMenuItem;
    ReprocessaComissodeChequesDevolvidos1: TMenuItem;
    tlb1: TToolBar;
    btPortadores: TSpeedButton;
    btContas: TSpeedButton;
    btClientes: TSpeedButton;
    btPedidos: TSpeedButton;
    btProjeto: TSpeedButton;
    btPedidoRapido: TSpeedButton;
    btConsultaRapida: TSpeedButton;
    btSobre: TSpeedButton;
    lb2: TLabel;
    btCompras: TSpeedButton;
    img1: TImage;
    img4: TImage;
    pnl1: TPanel;
    lbUsuario: TLabel;
    lbOptante: TLabel;
    lbData: TLabel;
    lbHora: TLabel;
    tmr1: TTimer;
    imgLogo: TImage;
    img2: TImage;
    dlgOpenPic1: TOpenPictureDialog;
    img3: TImage;
    Estados: TMenuItem;
    ModeloDeNotaFiscal1: TMenuItem;
    lb4: TLabel;
    NFe1: TMenuItem;
    Nfedigitada1: TMenuItem;
    CadastroNfe1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    img5: TImage;
    btOrdemServico: TSpeedButton;
    OrdemServio1: TMenuItem;
    IntegraoFrentedeCaixa1: TMenuItem;
    GeraodosRecebimentos1: TMenuItem;
    N1: TMenuItem;
    RelacionamentoFormasdePagamento1: TMenuItem;
    LanamentodeCrditoParaClientes1: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N4: TMenuItem;
    N8: TMenuItem;
    lb5: TLabel;
    btOrdemMedicao: TSpeedButton;
    OrdemdeMedio1: TMenuItem;
    ModulodeCobrana1: TMenuItem;
    CadastrodeCobradores1: TMenuItem;
    ControledeCobrana1: TMenuItem;
    N9: TMenuItem;
    Pedidosquenoforamentregues1: TMenuItem;
    Pedidosquenoentraramemromaneio2: TMenuItem;
    N7: TMenuItem;
    MdulodeAjuda1: TMenuItem;
    OrdemdeInstalao1: TMenuItem;
    VendaRpida1: TMenuItem;
    pnl2: TPanel;
    wbTempo: TWebBrowser;
    lbCaminhoBD: TLabel;
    Ajuda2: TMenuItem;
    lb1: TLabel;
    edt1: TEdit;
    lbVersaoSistema: TLabel;
    Contador1: TMenuItem;
    N10: TMenuItem;
    CRTCdigodeRegimeTributrio1: TMenuItem;
    CSOSNCdigodeSituaodaOperaonoSimplesNacional1: TMenuItem;
    CSTACdigoTabelaAOrigemMercadoria1: TMenuItem;
    CTSBCdigoTabelaB1: TMenuItem;
    SPEDFiscal1: TMenuItem;
    UnidadedeMedida1: TMenuItem;
    N11: TMenuItem;
    AcertodeUnidades1: TMenuItem;
    imgVideos: TImage;
    AjudaFace: TImage;
    imgAjuda: TImage;
    OrdemdeProduo1: TMenuItem;
    ProjetosemProduo1: TMenuItem;
    N12: TMenuItem;
    Cartacorreoeletrnica1: TMenuItem;
    ConfiguraoWeb1: TMenuItem;
    AtualizaTributospeloNCM1: TMenuItem;
    tmrverificapedidoonline: TTimer;
    pnl3: TPanel;
    pnlPedidosonline: TPanel;
    lbl1: TLabel;
    lbl2: TLabel;
    N13: TMenuItem;
    CEST1: TMenuItem;
    EstoqueMnimo1: TMenuItem;
    EqualizaEstoque1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sOBRE1Click(Sender: TObject);
    procedure Backup1Click(Sender: TObject);
    procedure Loggof1Click(Sender: TObject);
    procedure Sair2Click(Sender: TObject);
    procedure Parametros1Click(Sender: TObject);
    procedure Cadastrodeusurios1Click(Sender: TObject);
    procedure Cadastrodenveis1Click(Sender: TObject);
    procedure Permisses3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ScriptsSQL1Click(Sender: TObject);
    procedure Cascata1Click(Sender: TObject);
    procedure Horizontalmente1Click(Sender: TObject);
    procedure Organizarcones1Click(Sender: TObject);
    procedure Minimizartodas1Click(Sender: TObject);
    procedure FecharAtual1Click(Sender: TObject);
    procedure Fechartodas1Click(Sender: TObject);
    procedure TimerFechaSistemaTimer(Sender: TObject);
    procedure Maximizartodas1Click(Sender: TObject);
    Procedure PegaFiguras;
    procedure GrupodeFerrragens1Click(Sender: TObject);
    procedure GrupodeServios1Click(Sender: TObject);
    procedure GrupodeFerragens1Click(Sender: TObject);
    procedure GrupodeVidros1Click(Sender: TObject);
    procedure RamoAtividade1Click(Sender: TObject);
    procedure Fornecedor1Click(Sender: TObject);
    procedure Cor1Click(Sender: TObject);
    procedure Ferragens1Click(Sender: TObject);
    procedure Servio1Click(Sender: TObject);
    procedure Perfilados1Click(Sender: TObject);
    procedure Vidros1Click(Sender: TObject);
    procedure KitBox1Click(Sender: TObject);
    procedure Compoentes1Click(Sender: TObject);
    procedure Clientes1Click(Sender: TObject);
    procedure Vendedor1Click(Sender: TObject);
    procedure Projeto1Click(Sender: TObject);
    procedure Pedido1Click(Sender: TObject);
    procedure GrupodeDiversos1Click(Sender: TObject);
    procedure Diversos1Click(Sender: TObject);
    procedure GrupodePersianas1Click(Sender: TObject);
    procedure Diametro1Click(Sender: TObject);
    procedure Persiana1Click(Sender: TObject);
    procedure Pedido2Click(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure BtPedidosClick(Sender: TObject);
    procedure BtProjetoClick(Sender: TObject);
    procedure BtClientesClick(Sender: TObject);
    procedure CalculaFrmula1Click(Sender: TObject);
    procedure Bairro1Click(Sender: TObject);
    procedure Rua1Click(Sender: TObject);
    procedure Cidade1Click(Sender: TObject);
    procedure EntadadeProdutos1Click(Sender: TObject);
    procedure AtualizaExecutvel1Click(Sender: TObject);
    procedure ransportadora1Click(Sender: TObject);
    procedure Romaneio1Click(Sender: TObject);
    procedure BtPortadoresClick(Sender: TObject);
    procedure PedidoProjetonoRomaneio1Click(Sender: TObject);
    procedure FaixadeDesconto1Click(Sender: TObject);
    procedure ComissodeVendedores1Click(Sender: TObject);
    procedure BtSobreClick(Sender: TObject);
    procedure FuncionriosporFolha1Click(Sender: TObject);
    procedure Colocador1Click(Sender: TObject);
    procedure btConsultaRapidaClick(Sender: TObject);
    procedure btPedidoRapidoClick(Sender: TObject);
    procedure PedidoRpido1Click(Sender: TObject);
    procedure Materiais2Click(Sender: TObject);
    procedure AtualizaCNPJeCPF1Click(Sender: TObject);
    procedure RelacFolhaComisses1Click(Sender: TObject);
    procedure ComissodoColocador1Click(Sender: TObject);
    procedure HoraExtra1Click(Sender: TObject);
    procedure FaixadeDescontoIRPF1Click(Sender: TObject);
    procedure FaixadeDescontodeINSS1Click(Sender: TObject);
    procedure Adiantamento1Click(Sender: TObject);
    procedure Comisso2Click(Sender: TObject);
    procedure SalrioFamlia1Click(Sender: TObject);
    procedure ransfereBackuppersianas1Click(Sender: TObject);
    procedure Gratificaes1Click(Sender: TObject);
    procedure Arquiteto1Click(Sender: TObject);
    procedure CFOP1Click(Sender: TObject);
    procedure NotaFiscal2Click(Sender: TObject);
    procedure ConfiguraodeNF1Click(Sender: TObject);
    procedure rocadeMateriaisemPedidosConcludos1Click(Sender: TObject);
    procedure RefazContabilidadeEntradaseSaidas1Click(Sender: TObject);
    procedure EstoqueporMaterial1Click(Sender: TObject);
    procedure ContasaReceberporVendedor1Click(Sender: TObject);
    procedure ReajustaValores1Click(Sender: TObject);
    procedure AcrscimodeDSR1Click(Sender: TObject);
    procedure DescontodeContSindical1Click(Sender: TObject);
    procedure GeraContabilidadeChDevcomFornecedor1Click(Sender: TObject);
    procedure MovimentaoDiria1Click(Sender: TObject);
    procedure PedidosquenoentraramemRomaneio1Click(Sender: TObject);
    procedure RelatriodeItensemNF1Click(Sender: TObject);
    procedure PsVenda1Click(Sender: TObject);
    procedure PedidodeCompra1Click(Sender: TObject);
    procedure LotedeComissodeVendedores1Click(Sender: TObject);
    procedure ComponentesdoPedidoprojeto1Click(Sender: TObject);
    procedure ImportaodePedidos1Click(Sender: TObject);
    procedure AcertoseAjustesnoBD1Click(Sender: TObject);
    procedure FluxodeCaixaExcel1Click(Sender: TObject);
    procedure GeraSqlsdeAlteracaodeDatasnaContabilidade1Click(
      Sender: TObject);
    procedure ImportaodeDados1Click(Sender: TObject);
    procedure ArquivoINI1Click(Sender: TObject);
    procedure FinalizaPedidosProjetos1Click(Sender: TObject);
    procedure ComissodeColocador1Click(Sender: TObject);
    procedure Novidades1Click(Sender: TObject);
    procedure HabilitarCriaodecone1Click(Sender: TObject);
    procedure DesabilitarCriaodecone1Click(Sender: TObject);
    procedure TimerIconeTimer(Sender: TObject);
    procedure CadastrodeEmpresa1Click(Sender: TObject);
    procedure RelatrioPersonalizadoReportBuilder1Click(Sender: TObject);
    procedure ConfiguraodeDuplicataModelo11Click(Sender: TObject);
    procedure ConfiguraodeDuplicataModelo21Click(Sender: TObject);
    procedure ConfiguraBoletoPrImpresso1Click(Sender: TObject);
    procedure ImpostoICMS1Click(Sender: TObject);
    procedure ipodeCliente1Click(Sender: TObject);
    procedure SituaoTributriaIPI1Click(Sender: TObject);
    procedure ImpostoIPI1Click(Sender: TObject);
    procedure SituaoTributriaPIS1Click(Sender: TObject);
    procedure SituaoTributriaCOFINS1Click(Sender: TObject);
    procedure ImpostoPIS1Click(Sender: TObject);
    procedure ImpostoCofins1Click(Sender: TObject);
    procedure IESubtTributrioporEstado1Click(Sender: TObject);
    procedure OperaodeNF1Click(Sender: TObject);
    procedure ReprocessaComissodeChequesDevolvidos1Click(Sender: TObject);
    procedure CargodeFuncionrios1Click(Sender: TObject);
    procedure Funcionrios1Click(Sender: TObject);
    procedure btComprasClick(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure but1Click(Sender: TObject);
    procedure EstadosClick(Sender: TObject);
    procedure ModeloDeNotaFiscal1Click(Sender: TObject);
    procedure img2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CadastroNfe1Click(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Nfedigitada1Click(Sender: TObject);
    procedure img3Click(Sender: TObject);
    procedure btOrdemServicoClick(Sender: TObject);
    procedure GeraodosRecebimentos1Click(Sender: TObject);
    procedure RelacionamentoFormasdePagamento1Click(Sender: TObject);
    procedure LanamentodeCrditoParaClientes1Click(Sender: TObject);
    procedure bt1Click(Sender: TObject);
    procedure CadastrodoCRT1Click(Sender: TObject);
    procedure btOrdemMedicaoClick(Sender: TObject);
    procedure OrdemdeMedio1Click(Sender: TObject);
    procedure CadastrodeCobradores1Click(Sender: TObject);
    procedure Pedidosquenoentraramemromaneio2Click(Sender: TObject);
    procedure Pedidosquenoforamentregues1Click(Sender: TObject);
    procedure MdulodeAjuda1Click(Sender: TObject);
    procedure lb5MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lb5MouseLeave(Sender: TObject);
    procedure lb4Click(Sender: TObject);
    procedure lb5Click(Sender: TObject);
    procedure OrdemdeInstalao1Click(Sender: TObject);
    procedure VendaRpida1Click(Sender: TObject);
    procedure Ajuda2Click(Sender: TObject);
    procedure lb1Click(Sender: TObject);
    procedure imgLogoClick(Sender: TObject);
    procedure Contador1Click(Sender: TObject);
    procedure CRTCdigodeRegimeTributrio1Click(Sender: TObject);
    procedure CSOSNCdigodeSituaodaOperaonoSimplesNacional1Click(
      Sender: TObject);
    procedure CSTACdigoTabelaAOrigemMercadoria1Click(Sender: TObject);
    procedure CTSBCdigoTabelaB1Click(Sender: TObject);
    procedure SPEDFiscal1Click(Sender: TObject);
    procedure UnidadedeMedida1Click(Sender: TObject);
    procedure AcertodeUnidades1Click(Sender: TObject);
    procedure imgAjudaClick(Sender: TObject);
    procedure AjudaFaceClick(Sender: TObject);
    procedure imgVideosClick(Sender: TObject);
    procedure OrdemdeProduo1Click(Sender: TObject);
    procedure ProjetosemProduo1Click(Sender: TObject);
    procedure Cartacorreoeletrnica1Click(Sender: TObject);
    procedure ConfiguraoWeb1Click(Sender: TObject);
    procedure AtualizaTributospeloNCM1Click(Sender: TObject);
    procedure DevoluoNFe1Click(Sender: TObject);
    procedure pnlPedidosonlineClick(Sender: TObject);
    procedure lbl1Click(Sender: TObject);
    procedure lbl2Click(Sender: TObject);
    procedure tmrverificapedidoonlineTimer(Sender: TObject);
    procedure CEST1Click(Sender: TObject);
    procedure EstoqueMnimo1Click(Sender: TObject);
    procedure EqualizaEstoque1Click(Sender: TObject);

  private
    logado:Boolean;
    ObjRelatoriopersonalizado:Tobjrelatoriopersonalizadoextendido;
    PRocedure CarregaParametrosGlobais;
    function ValidaVenctoPEnd: boolean;
    procedure MostraNovidade;
    procedure checaEqualizaEstoque;

    
    { Private declarations }
  public

    Objencerrasistema:tobjencerrasistema;

     procedure chamaPesquisaMenu(status:TDataSetState = dsInactive);
  end;

var
  Fprincipal: TFprincipal;


implementation
uses UUsuarios,
  UNiveis, UPermissoesUso,
  Uparametros,
  UBackup, UDataModulo,
  Usobre,ULogin,UObjPermissoesUso,
  UObjUsuarios, UobjParametros, UobjConfRelatorio, UObjLanctoPortador,
  UGRUPOFERRAGEM, UGRUPOSERVICO, UGRUPOPERFILADO, UGRUPOVIDRO,
  URAMOATIVIDADE, UFORNECEDOR, UCOR, UFERRAGEM, USERVICO, UPERFILADO,
  UVIDRO, UKITBOX, UCOMPONENTE, UCLIENTE, UVENDEDOR, UPROJETO, UPEDIDO,
  UGRUPODIVERSO, UDIVERSO, UGRUPOPERSIANA, UPERSIANA, UDIAMETRO,
  Uportador, UTitulo, 
  UCalculaformula_Vidro, UBAIRRO, URUA, UCIDADE, UENTRADAPRODUTOS,
  UTRANSPORTADORA, UROMANEIO, UObjTitulo,
  UPEDIDOPROJETOROMANEIO, UcargoFuncionario, Ufuncionarios,
  UADIANTAMENTOFUNCIONARIO, UFAIXADESCONTO, UCOMISSAOVENDEDORES,UFOLHAPAGAMENTO, UFUNCIONARIOFOLHAPAGAMENTO,
  UCOLOCADOR, UConsultaRapida, UobjCLIENTE, UCOMISSAO_ADIANT_FUNCFOLHA,
  UCOMISSAOCOLOCADOR, UHORAEXTRA, UFAIXADESCONTOIRPF, UFAIXADESCONTOINSS,
  UobjCOMISSAO_ADIANT_FUNCFOLHA, USALARIOFAMILIA, URecuperaBackup,
  UGratificacao, UARQUITETO, UCFOP, UNotaFiscal, UrelNotaFiscalRdPrint,
  UtrocaMaterialpedido, URefazContabilidadeEntradaeSaida,
  URelatorioEstoque, UobjPEDIDO, UreajustaMateriais,
  UDescansoSemanalRemunerado, UDescontoContribuicaoSindical,
  UvisualizaTabela, UrefazContabilidadeChequeDevolvido, UobjPedidoObjetos,
  UrelatoriosItensNF, UobjCREDITOVALORES, UPEDIDOCOMPRA, ULOTECOMISSAO,
  UAcertaDataServidor, UAcertaVenctoServidor, UObjVENCTOPEND,
  UCOMPONENTE_PP, UimportaPedido, Uajustes, UFLUXOCAIXA,
  UCADASTRARELATORIOPERSONALIZADO, UImportaTabela, UarquivoIni,
  UFinalizaComissaoColocador, UobjCOMISSAOCOLOCADOR, UmostraMEnsagem,
  UNOVIDADE, UobjMenuFinanceiro, UobjICONES, UEMPRESA, UobjEMPRESA,
  URELPERSREPORTBUILDER,  UConfDuplicata, UConfDuplicata02,
  uConfiguraBoleto, UIMPOSTO_ICMS, UTIPOCLIENTE, 
  USITUACAOTRIBUTARIA_IPI, UIMPOSTO_IPI, USITUACAOTRIBUTARIA_COFINS,
  USITUACAOTRIBUTARIA_PIS, UIMPOSTO_PIS, UIMPOSTO_COFINS, UIE_ST_ESTADO,
  UOPERACAONF, UobjIntegracao, UreprocessaChequeDevolvidoComissao,
  UobjCONFCAMPOSPESQUISA, UobjESTOQUE, Ucliente2, UTitulo_novo,
  UpesquisaMenu, UnotaFiscalEletronica, UNfeDigitada,
  UescolheTransportadora, UobjLancamentosFC, UFORMASPAGAMENTO_FC,
  UobjLancamentosFCSafira, UCRT, UCSOSN,UCobrador, UTABELAA_ST,
  UTABELAB_ST, UspedFiscal, UProducao, UVisualizaStatusProducao,
  uCartaCorrecao, UConfiguracaoSite, Uobjvencimento, UobjDIVERSO{,UNfeDigitada},
  UDevolucaoNFE, UobjPROJETO, UCEST, UobjRELPERSREPORTBUILDER, UFiltraImp;

{$R *.DFM}


procedure TFprincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //Fechando todas as janelas MDI abertas
    Fechartodas1Click(sender);

    //destruindo os objetos globais

    If (objusuarioglobal<>nil)
    Then ObjUsuarioGlobal.free;

    If (ObjPermissoesUsoGlobal<>nil)
    Then ObjPermissoesUsoGlobal.free;

    If (ObjParametroGlobal<>nil)
    Then ObjParametroGlobal.free;

    If (ObjConfiguraRelGlobal <> nil) Then
      ObjConfiguraRelGlobal.Free;

    if (ObjLanctoPortadorGlobal<>nil)
    Then ObjLanctoPortadorGlobal.free;

    if (ObjCreditoValoresGlobal<>nil)
    Then ObjCreditoValoresGlobal.free;

    if (Self.Objrelatoriopersonalizado<>nil)
    Then Self.Objrelatoriopersonalizado.free;

    if (ObjIconesglobal<>nil)
    Then ObjIconesglobal.free;

    if (ObjEmpresaGlobal<>nil)
    Then ObjEmpresaGlobal.free;

    if (ObjIntegracaoglobal<>nil)
    then ObjIntegracaoglobal.free;

    if (ObjCamposPesquisa_Global<>nil)
    then ObjCamposPesquisa_Global.free;

    if (ObjEstoqueGlobal<>nil)
    then ObjEstoqueGlobal.free;

    if (Objencerrasistema <> nil) then
      Objencerrasistema.free;

    if (ObjMenuFinanceiro <> nil) then
      ObjMenuFinanceiro.Free;


    //Fechando o Banco de Dados
    Try
        FDataModulo.IBDatabase.close;
    Except
    End;

end;

procedure TFprincipal.sOBRE1Click(Sender: TObject);
var
Fsobre:TFsobre;
begin
     if (ObjIconesglobal.VerificaClick('sOBRE1Click'))
       then exit;

     Try
        Fsobre:=TFsobre.create(nil);
        Fsobre.Showmodal;
     Finally
            Freeandnil(Fsobre);
     End;

end;

procedure TFprincipal.Backup1Click(Sender: TObject);
var
Fbackup:TFbackup;
begin
     if (ObjIconesglobal.VerificaClick('Backup1Click'))
     then exit;

     Try
        Fbackup:=TFbackup.create(nil);
        Fbackup.Showmodal;
     Finally
            Freeandnil(Fbackup);
     End;

end;


procedure TFprincipal.Loggof1Click(Sender: TObject);
begin
      Close;
      ShellExecute(Application.Handle, Pchar('OPEN'), PChar(Application.ExeName), '', '',SW_MAXIMIZE)
end;

procedure TFprincipal.Sair2Click(Sender: TObject);
begin
     if (ObjIconesglobal.VerificaClick('Sair2Click'))
     then exit;

     Close;
end;

procedure TFprincipal.Parametros1Click(Sender: TObject);
var
fparametros:Tfparametros;
begin
     if (ObjIconesglobal.VerificaClick('Parametros1Click'))
     then exit;


     Try
        fparametros:=Tfparametros.create(nil);
        fparametros.Showmodal;
        ObjParametroGlobal.RecarregaParametro_STL;
        Self.CarregaParametrosGlobais;
     Finally
            Freeandnil(fparametros);
     End;

end;



procedure TFprincipal.Cadastrodeusurios1Click(Sender: TObject);
var
FUsuariosXX:TFUsuarios;
begin
     if (ObjIconesglobal.VerificaClick('Cadastrodeusurios1Click'))
     then exit;


     Try
        FUsuariosXX:=TFusuarios.create(nil);
        FUsuariosXX.ShowModal;
     Finally
        Freeandnil(FUsuariosXX);
     end;
end;

procedure TFprincipal.Cadastrodenveis1Click(Sender: TObject);
var
FNiveisXX:TFNiveis;
begin
     if (ObjIconesglobal.VerificaClick('Cadastrodenveis1Click'))
     then exit;


        Try
               FNiveisXX:=TFNiveis.create(nil);
               FNiveisXX.ShowModal;
        Finally
               Freeandnil(FNiveisXX);
        End;
end;

procedure TFprincipal.Permisses3Click(Sender: TObject);
var
FPermissoesUsoXX:TFPermissoesUso;
begin
     if (ObjIconesglobal.VerificaClick('Permisses3Click'))
     then exit;

     Try
        FPermissoesUsoXX:=TFPermissoesUso.create(nil);
        FPermissoesUsoXX.showmodal;
     Finally
            Freeandnil(FPermissoesUsoXX);
     End;
end;

procedure TFprincipal.FormShow(Sender: TObject);
var
  Flogin:Tflogin;
  objVencimento:TobjVencimento;
begin
  PfechasistemaGlobal:=False;
  // but1.Visible:=False;
  //Desativando o timer
  TimerFechaSistema.Enabled:=False;
  //Configurando para 1s
  TimerFechaSistema.Interval:=1000;
  btPedidoRapido.Visible:=false;
  //Marcando que o sistema sera MDI
  SistemaMdiGlobal:=True;
  logado:=False;
  //Criando o Form de Login

  try
    Flogin:=Tflogin.Create(nil);
    FacertaDataServidor:=TFacertaDataServidor.create(nil);
    FacertaVenctoServidor:=TFacertaVenctoServidor.create(nil);

    //Chamando o Form de Login
    FLogin.edtnome.text:='';
    FLogin.edtsenha.text:='';
    FLogin.showmodal;

    if (Flogin.Tag=0) then
    begin
      PfechasistemaGlobal:=True;
    end
    else
    begin
      //O form de Login apenas loga base
      //Agora todo processo de cria��o esta no FormPrincipal
      //devido ao MDI

      menuFinanceiro.Clear;
      ObjMenuFinanceiro:=TObjMenuFinanceiro.create(menuFinanceiro,Self,SistemaMdiGlobal);

      //Criando os Objetos que ser�o USADOS NO SISTEMA INTEIRO
      if (ObjUsuarioGlobal=Nil) then
        ObjUsuarioGlobal:=Tobjusuarios.Create(self);

      if (objusuarioglobal.LocalizaNome(Flogin.edtnome.text)=False) then
      begin
        Messagedlg('Usu�rio n�o encontrado no sistema!',mterror,[mbok],0);
        PfechasistemaGlobal:=True;
        exit;
      end;

      objusuarioglobal.TabelaparaObjeto;

      if (ObjPermissoesUsoGlobal=Nil) then
        ObjPermissoesUsoGlobal:=TObjPermissoesUso.create;

      If (ObjConfiguraRelGlobal=Nil) then
        ObjConfiguraRelGlobal:=TObjConfRelatorio.create;

      if (ObjLanctoPortadorGlobal=nil) then
        ObjLanctoPortadorGlobal:=Tobjlanctoportador.create;

      if (ObjCreditoValoresGlobal=nil) then
        ObjCreditoValoresGlobal:=tobjCreditoValores.create;

      if (objEstoqueGlobal=nil) then
        ObjEstoqueGlobal:=TobjEstoque.create;

      Self.ObjRelatoriopersonalizado:=TObjRELATORIOPERSONALIZADOExtendido.Create;
      Self.ObjRelatoriopersonalizado.CriaMenu(MenuPersonalizado);

      if (ObjEmpresaGlobal=nil) then
        ObjEmpresaGlobal:=Tobjempresa.create;

      if (ObjIntegracaoglobal=nil) then
        ObjIntegracaoglobal:=TObjIntegracao.create;


      ObjEmpresaGlobal.ZerarTabela;
      if (ObjEmpresaGlobal.LocalizaCodigo('1')=False) then
      begin
        mensagemerro('Configure a empresa c�digo 1 no cadastro de empresa');
        ObjEmpresaGlobal.status:=dsinsert;
        ObjEmpresaGlobal.submit_codigo('1');
        ObjEmpresaGlobal.Submit_RAZAOSOCIAL('RAZ�O SOCIAL DA EMPRESA');
        ObjEmpresaGlobal.Submit_FANTASIA('FANTASIA DA EMPRESA');
        ObjEmpresaGlobal.Submit_simples('N');
        ObjEmpresaGlobal.salvar(true);
      end
      else ObjEmpresaGlobal.TabelaparaObjeto;

      if (ObjCamposPesquisa_Global = nil) then
        ObjCamposPesquisa_Global:=TobjCONFCAMPOSPESQUISA.create;


      //********PARAMETROS********************************
      Self.CarregaParametrosGlobais;
      //*************************************************

      lbUsuario.Caption:=ObjUsuarioGlobal.Get_nome;
      //lbData.Caption:=formatdatetime ('dd/mm/yyyy',now);
      lbData.Caption:=FormatDateTime('dd "de" mmmm "de" yyyy', Now) ;
      imgLogo.Picture.LoadFromFile('images\logo.jpg');

      if (Self.Objencerrasistema=nil) then
        Self.Objencerrasistema:=Tobjencerrasistema.create(self);

      //***********Atualiza��o automatica de vencimento******************
     { try
          objVencimento := TobjVencimento.Create;
      except
          on e:Exception do
                MensagemErro(e.Message);
      end;
      try

          objVencimento.VerificaVencimento;
      finally
          if(objVencimento<>nil)
          then objVencimento.Free;
      end;
            }
      //*****************************************************************

      if (Self.ValidaVenctoPEnd=False) then
        PfechasistemaGlobal:=true;
      //Else TimerVerificaEncerrar.Enabled:=True;

      if (ObjIconesGlobal=nil) then
      begin
        ObjIconesglobal:=tobjIcones.create(Fprincipal,imgLogo);
        ObjIconesglobal.CriaTodosIcones;
      end;

      if(ObjEmpresaGlobal.get_Simples='S') then
        lbOptante.caption:='Optante pelo Simples'
      else lbOptante.Caption:='N�o Optante pelo Simples';


      lbCaminhoBD.Caption:=lbCaminhoBD.Caption+FDataModulo.IbDatabase.Databasename;
      lbVersaoSistema.Caption:=lbVersaoSistema.Caption+ Flogin.VERSAO;

      //Colocando o selo do clima tempo no sistema
      ObjParametroGlobal.ValidaParametro('HABILITA PREVIS�O DO TEMPO')  ;

      if(ObjParametroGlobal.Get_Valor='SIM') then
      begin
        pnl2.Visible:=True;
        ObjParametroGlobal.ValidaParametro('CODIGO CIDADE CLIMA TEMPO');
        wbTempo.Navigate('http://selos.climatempo.com.br/selos/MostraSelo.php?CODCIDADE='+ObjParametroGlobal.Get_Valor+'&SKIN=azul');
      end;

      VerificaVencimentoCertificadoDigital; //Rodolfo
      logado:=True;
    end;

  finally
    Freeandnil(Flogin);
    FreeAndNil(FacertaDataServidor);
    FreeAndNil(FacertaVenctoServidor);
    //Como nao consigo fechar dentro do evento oncreate
    //ativo um timer que dentro do ontimer dele
    //fecha o form principal
    if (PfechasistemaGlobal=True) then
      TimerFechaSistema.Enabled:=True
    else
    begin
      if SistemaemModoDemoGlobal=True then
        Self.caption:='SAB BLINDEX- Control Glass - Copyright EXCLAIM TECNOLOGIA LTDA - VERS�O DEMO'
      else Self.caption:='SAB BLINDEX- Control Glass - Copyright EXCLAIM TECNOLOGIA LTDA';
    end;
  end;

  CORGRIDZEBRADOGLOBAL1:=rgb(169,186,204);
  CORGRIDZEBRADOGLOBAL2:=rgb(193,205,219);

  tag:=0;
  Self.PegaFiguras;

  //if (TimerFechaSistema.Enabled=False) and (PfechasistemaGlobal=False) then
    //Self.MostraNovidade;    tirado a pedido do marcelo misaki, atual suporte
  if not( PfechasistemaGlobal ) then
    checaEqualizaEstoque;

end;

procedure TFprincipal.ScriptsSQL1Click(Sender: TObject);
begin
     if (ObjIconesglobal.VerificaClick('ScriptsSQL1Click'))
     then exit;


     if (ObjUsuarioGlobal.Get_nome<>'SYSDBA')
     then exit;
     Fvisualizatabela.showmodal;
end;

procedure TFprincipal.Cascata1Click(Sender: TObject);
begin
Cascade;
end;

procedure TFprincipal.Horizontalmente1Click(Sender: TObject);
begin
TileMode := tbVertical;
Tile;

end;

procedure TFprincipal.Organizarcones1Click(Sender: TObject);
begin
     ArrangeIcons;
end;

procedure TFprincipal.Minimizartodas1Click(Sender: TObject);
var
X : Byte;
begin
      for X := 0 to Pred(MDIChildCount)
      do MDIChildren[X].WindowState := wsMinimized;
      ActiveMDIChild.WindowState := wsMinimized;
end;


procedure TFprincipal.FecharAtual1Click(Sender: TObject);
begin
ActiveMDIChild.Close;
end;

procedure TFprincipal.Fechartodas1Click(Sender: TObject);
var
X : Byte;
begin
    if MDIChildCount > 0
    then
        for X := 0 to Pred(MDIChildCount) do
        MDIChildren[X].Close;
end;

procedure TFprincipal.TimerFechaSistemaTimer(Sender: TObject);
begin
     PfechasistemaGlobal:=True;
     TimerFechaSistema.Enabled:=False;
     Self.Close;
end;

procedure TFprincipal.Maximizartodas1Click(Sender: TObject);
var
X : Byte;
begin
      for X := 0 to Pred(MDIChildCount)
      do MDIChildren[X].WindowState := wsNormal;
      ActiveMDIChild.WindowState := wsNormal;
end;

procedure TFprincipal.PegaFiguras;
begin
    PegaFiguraBotao(BtPortadores,'');
    PegaFiguraBotao(BtContas,'inicial_contas.bmp');
    PegaFiguraBotao(BtPedidos,'inicial_pedido.bmp');
    PegaFiguraBotao(BtProjeto,'inicial_projeto.bmp');
    PegaFiguraBotao(BtClientes,'inicial_cliente.bmp');
    PegaFiguraBotao(BtSobre,'inicial_fornecedor.bmp');
    PegaFiguraBotao(BtPortadores,'inicial_portador.bmp');
    PegaFiguraBotao(btconsultarapida,'inicial_material.bmp');
    PegaFiguraBotao(btPedidorapido,'botaoorcamento.bmp');
    PegaFiguraBotao(btCompras,'inicial_compra.bmp');
    PegaFiguraBotao(btOrdemServico,'inicial_servicos.bmp');
    PegaFiguraBotao(btOrdemMedicao,'inicial_OrdemMedicao.bmp');

end;

procedure TFprincipal.GrupodeFerrragens1Click(Sender: TObject);
Var FGrupoFerragem : TFGRUPOFERRAGEM;
begin
     if (ObjIconesglobal.VerificaClick('GrupodeFerrragens1Click'))
     then exit;

     try
         FGrupoFerragem:=TFGRUPOFERRAGEM.Create(nil);

     except
         MensagemErro('Erro ao tentar criar a tela de Grupo de Ferragens');
     end;

     try
       FGrupoFerragem.ShowModal;
     finally
       FreeAndNil(FGrupoFerragem);
     end;

end;

procedure TFprincipal.GrupodeServios1Click(Sender: TObject);
Var FGrupoServico : TFGRUPOServico;
begin
     if (ObjIconesglobal.VerificaClick('GrupodeServios1Click'))
     then exit;

     try
         FGrupoServico:=TFGRUPOServico.Create(nil);

     except
         MensagemErro('Erro ao tentar criar a tela de Grupo de Servi�os');
     end;

     try
       FGrupoServico.ShowModal;
     finally
        FreeAndNil(FGrupoServico);
     end;

end;

procedure TFprincipal.GrupodeFerragens1Click(Sender: TObject);
Var FGrupoPerfilado : TFGRUPOPerfilado;
begin
     if (ObjIconesglobal.VerificaClick('GrupodeFerragens1Click'))
     then exit;

     try
         FGrupoPerfilado:=TFGRUPOPerfilado.Create(nil);

     except
         MensagemErro('Erro ao tentar criar a tela de Grupo de Perfilados');
     end;

     try
        FGrupoPerfilado.ShowModal;
     finally
        FreeAndNil(FGrupoPerfilado);
     end;

end;

procedure TFprincipal.GrupodeVidros1Click(Sender: TObject);
Var FGrupoVidro : TFGRUPOVidro;
begin
     if (ObjIconesglobal.VerificaClick('GrupodeVidros1Click'))
     then exit;

     try
         FGrupoVidro:=TFGRUPOVidro.Create(nil);

     except
         MensagemErro('Erro ao tentar criar a tela de Grupo de Vidros');
     end;

     try
       FGrupoVidro.ShowModal;
     finally
       FreeAndNil(FGrupoVidro);
     end;

end;

procedure TFprincipal.RamoAtividade1Click(Sender: TObject);
Var FRamoAtividade : TFRamoAtividade;
begin
     if (ObjIconesglobal.VerificaClick('RamoAtividade1Click'))
     then exit;

     try
         FRamoAtividade:=TFRamoAtividade.Create(nil);

     except
         MensagemErro('Erro ao tentar criar a tela de RamoAtividades');
     end;

     try
       FRamoAtividade.ShowModal;
     finally
        FreeAndNil(FRamoAtividade);
     end;

end;

procedure TFprincipal.Fornecedor1Click(Sender: TObject);
Var FFornecedor : TFFornecedor;
begin
     if (ObjIconesglobal.VerificaClick('Fornecedor1Click'))
     then exit;

     try
         FFornecedor:=TFFornecedor.Create(nil);

     except
         MensagemErro('Erro ao tentar criar a tela de Fornecedores');
     end;

     try
       FFornecedor.ShowModal;
     finally
        FreeAndNil(FFornecedor);
     end;

end;

procedure TFprincipal.Cor1Click(Sender: TObject);
Var FCor : TFCor;
begin
     if (ObjIconesglobal.VerificaClick('Cor1Click'))
     then exit;

     try
         FCor:=TFCor.Create(nil);

     except
         MensagemErro('Erro ao tentar criar a tela de Cores');
     end;

     try
          FCor.ShowModal;
     finally
          FreeAndNil(FCor);
     end;

end;


procedure TFprincipal.Ferragens1Click(Sender: TObject);
Var FFerragem : TFFerragem;
begin
     if (ObjIconesglobal.VerificaClick('Ferragens1Click'))
     then exit;

     try
         FFerragem:=TFFerragem.Create(nil);

     except
         MensagemErro('Erro ao tentar criar a tela de Ferragens');
     end;

     try
        FFerragem.Showmodal;
     finally
         FreeAndNil(FFerragem);
     end;

end;

procedure TFprincipal.Servio1Click(Sender: TObject);
Var FServico : TFServico;
begin
     if (ObjIconesglobal.VerificaClick('Servio1Click'))
     then exit;

    try
        FSERVICO := TFSERVICO.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Servi�o');
    end;

    try
       FServico.showmodal;
    finally
        FreeAndNil(FServico);
    end;


end;

procedure TFprincipal.Perfilados1Click(Sender: TObject);

Var FPerfilado : TFPerfilado;
begin
    if (ObjIconesglobal.VerificaClick('Perfilados1Click'))
    then exit;

    try
        FPerfilado := TFPerfilado.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Perfilados');
    end;

    try
      FPerfilado.ShowModal;
    finally
      FreeAndNil(FPerfilado);
    end;


end;
procedure TFprincipal.Vidros1Click(Sender: TObject);
Var FVidro : TFVidro;
begin
     if (ObjIconesglobal.VerificaClick('Vidros1Click'))
     then exit;

    try
        FVidro := TFVidro.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Vidros');
    end;

    try
      FVidro.ShowModal;
    finally
      FreeAndNil(FVidro);
    end;

end;

procedure TFprincipal.KitBox1Click(Sender: TObject);
Var FKitBox : TFKitBox;
begin
     if (ObjIconesglobal.VerificaClick('KitBox1Click'))
     then exit;

    try
        FKitBox := TFKitBox.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de KitBox');
    end;

    try
      FKitBox.ShowModal;
    finally
      FreeAndNil(FKitBox);
    end;


end;

procedure TFprincipal.Compoentes1Click(Sender: TObject);
Var FComponente : TFComponente;
begin
     if (ObjIconesglobal.VerificaClick('Compoentes1Click'))
     then exit;

    try
        FComponente := TFComponente.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Componentes');
    end;

    try
      FComponente.ShowModal;
    finally
       FreeAndNil(FComponente);
    end;


end;

procedure TFprincipal.Clientes1Click(Sender: TObject);
Var FCliente : TFCliente;
begin

     if (ObjIconesglobal.VerificaClick('Clientes1Click'))
     then exit;

    try
        FCliente := TFCliente.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Clientes');
    end;

    try
        FCliente.ShowModal;
    finally
       FreeAndNil(FCliente);
    end;

end;

procedure TFprincipal.Vendedor1Click(Sender: TObject);
Var FVendedor : TFVendedor;
begin

    if (ObjIconesglobal.VerificaClick('Vendedor1Click'))
    then exit;

    try
        FVendedor := TFVendedor.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Vendedores');
    end;

    try
        FVendedor.ShowModal;
    finally
        FreeAndNil(FVendedor);
    end;


end;

procedure TFprincipal.Projeto1Click(Sender: TObject);
Var FProjeto : TFProjeto;
begin


     if (ObjIconesglobal.VerificaClick('Projeto1Click'))
     then exit;

    try
        FProjeto := TFProjeto.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Projetos');
    end;

    try
        FProjeto.ShowModal;
    finally
        FreeAndNil(FProjeto);
    end;

end;


procedure TFprincipal.Pedido1Click(Sender: TObject);
Var FPedido : TFPedido;
begin


     if (ObjIconesglobal.VerificaClick('Pedido1Click'))
     then exit;

    try
        FPedido := TFPedido.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Pedido');
    end;

    try
        FPedido.ShowModal;
    finally
        FreeAndNil(FPedido);
    end;

end;

procedure TFprincipal.GrupodeDiversos1Click(Sender: TObject);
Var FGrupoDiverso : TFGrupoDiverso;
begin

     if (ObjIconesglobal.VerificaClick('GrupodeDiversos1Click'))
     then exit;

    try
        FGrupoDiverso := TFGrupoDiverso.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Grupo dos Diversos');
    end;

    try
      FGrupoDiverso.ShowModal;
    finally
       FreeAndNil(FGrupoDiverso);
    end;

end;

procedure TFprincipal.Diversos1Click(Sender: TObject);
Var FDiverso : TFDiverso;
begin

     if (ObjIconesglobal.VerificaClick('Diversos1Click'))
     then exit;

    try
        FDiverso := TFDiverso.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Diversos');
    end;

    try
      FDiverso.ShowModal;
    finally
      FreeAndNil(FDiverso);
    end;


end;

procedure TFprincipal.GrupodePersianas1Click(Sender: TObject);
Var FGrupoPersiana : TFGrupoPersiana;
begin

     if (ObjIconesglobal.VerificaClick('GrupodePersianas1Click'))
     then exit;


    try
        FGrupoPersiana := TFGrupoPersiana.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de GrupoPersianas');
    end;

    try
        FGrupoPersiana.ShowModal;
    finally
        FreeAndNil(FGrupoPersiana);
    end;

end;

procedure TFprincipal.Diametro1Click(Sender: TObject);
Var FDiametro : TFDiametro;
begin
     if (ObjIconesglobal.VerificaClick('Diametro1Click'))
     then exit;


    try
        FDiametro := TFDiametro.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Diametros');
    end;

    try
      FDiametro.ShowModal;
    finally
       FreeAndNil(FDiametro);
    end;



end;

procedure TFprincipal.Persiana1Click(Sender: TObject);
Var FPersiana : TFPersiana;
Begin

     if (ObjIconesglobal.VerificaClick('Persiana1Click'))
     then exit;

     try
           FPersiana:=TFPERSIANA.Create(nil);

     except
          MensagemErro('Erro ao tentar mostrar a tela de Persiana');
     end;

     try
       FPersiana.ShowModal;
     finally
       FreeAndNil(FPersiana);
     end;


end;
procedure TFprincipal.Pedido2Click(Sender: TObject);
Var FPedido : TFPedido;
Begin

       if (ObjIconesglobal.VerificaClick('Pedido2Click'))
     then exit;

     try
           FPedido:=TFPedido.Create(nil);
           FPedido.Show;
     except
          MensagemErro('Erro ao tentar mostrar a tela de Pedido');
     end;

end;


procedure TFprincipal.BtContasClick(Sender: TObject);
var
  Ftitulo:TFtitulo_novo;
begin

  if(ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE TITULOS')=False) then
    Exit;

  if (ObjIconesglobal.VerificaClick('bttituloClick')) Then
    exit;

  Try

    Ftitulo:=TFtitulo_novo.create(nil);
    //*Indico que o titulo n�o sera lancamento externo
    RegLancTituloExterno.lancamentoExterno:=False;
    //Limpo os dados de lancamento externo
    LimpaRegLancTituloExterno;

    Ftitulo.showmodal;

  Finally

    Freeandnil(Ftitulo);

  End;

end;
//Comentario..


procedure TFprincipal.BtPedidosClick(Sender: TObject);
Var
  FPedido : TFPedido;
begin

  FPedido:=TFPEDIDO.Create(nil);

  try

    if (ObjIconesglobal.VerificaClick('BtPedidosClick')) then
      exit;

    if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE PEDIDO')=False) Then
      exit;

    FPedido.ShowModal;

  Finally
    Freeandnil(FPedido);
  end;

end;

procedure TFprincipal.BtProjetoClick(Sender: TObject);
Var FProjeto : TFProjeto;
begin

    if (ObjIconesglobal.VerificaClick('BtProjetoClick'))
    then exit;

    try
        if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE PROJETO')=False)
        Then exit;
        
        FProjeto := TFProjeto.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Projetos');
    end;

    try
         FProjeto.ShowModal;
    finally
          FreeAndNil(FProjeto);
    end;


end;

procedure TFprincipal.BtClientesClick(Sender: TObject);
Var FCliente : TFCliente;
begin
    if (ObjIconesglobal.VerificaClick('BtClientesClick'))
       then exit;


    try
        if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE CLIENTE')=False)
        Then exit;

        FCliente := TFCliente.Create(nil);
    except
        MensagemErro('Erro ao tentar criar a tela de Clientes');
    end;

    try
        FCliente.ShowModal;
    finally
        FreeAndNil(FCliente);
    end;


end;

procedure TFprincipal.CalculaFrmula1Click(Sender: TObject);
begin
     if (ObjIconesglobal.VerificaClick('CalculaFrmula1Click'))
     then exit;

     FcalculaFormula_vidro.Showmodal;
end;

procedure TFprincipal.Bairro1Click(Sender: TObject);
Var FBairro : TFBairro;
begin

    if (ObjIconesglobal.VerificaClick('Bairro1Click'))
    then exit;

    try
        FBairro := TFBairro.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Bairro');
    end;

    try
      FBairro.ShowModal;
    finally
       FreeAndNil(FBairro);
    end;


end;

procedure TFprincipal.Rua1Click(Sender: TObject);
Var FRua : TFRua;
begin

    if (ObjIconesglobal.VerificaClick('Rua1Click'))
    then exit;

    try
        FRua := TFRua.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Rua');
    end;

    try
      FRua.ShowModal;
    finally
       FreeAndNil(FRua);
    end;


end;

procedure TFprincipal.Cidade1Click(Sender: TObject);
Var FCidade : TFCidade;
begin

    if (ObjIconesglobal.VerificaClick('Cidade1Click'))
    then exit;

    try
        FCidade := TFCidade.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Cidade');
    end;

    try
      FCidade.ShowModal;
    finally
       FreeAndNil(FCidade);
    end;

end;

procedure TFprincipal.EntadadeProdutos1Click(Sender: TObject);
Var FEntradaProdutos : TFEntradaProdutos;
begin

    if (ObjIconesglobal.VerificaClick('EntadadeProdutos1Click'))
    then exit;

    try
        FEntradaProdutos := TFEntradaProdutos.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Entrada de Produtos');
    end;

    try
      FEntradaProdutos.ShowModal;
    finally
      FreeAndNil(FEntradaProdutos);
    end;

end;

procedure TFprincipal.AtualizaExecutvel1Click(Sender: TObject);
Begin
     if (CarregaAtualizacao=True)
     Then Begin
              PfechasistemaGlobal:=true;
              Self.close;
     end;
End;

procedure TFprincipal.ransportadora1Click(Sender: TObject);
Var
FTransportadora : TFTransportadora;
begin

    if (ObjIconesglobal.VerificaClick('ransportadora1Click'))
    then exit;


    try
        FTransportadora := TFTransportadora.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Entrada de Transportadora');
    end;

    try
       FTransportadora.ShowModal;
    finally
       FreeAndNil(FTransportadora);
    end;

end;

procedure TFprincipal.Romaneio1Click(Sender: TObject);
Var
Fromaneio : TFromaneio;
begin
    if (ObjIconesglobal.VerificaClick('Romaneio1Click'))
    then exit;

    try
        Fromaneio := TFromaneio.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Romaneio');
    end;

    try
        Fromaneio.ShowModal;
    finally
        FreeAndNil(Fromaneio);
    end;

end;

procedure TFprincipal.BtPortadoresClick(Sender: TObject);
var
Fportador:TFportador;
begin
     if (ObjIconesglobal.VerificaClick('BtPortadoresClick'))
     then exit;

     Try
            Fportador:=TFportador.create(nil);
            Fportador.Showmodal;
     Finally
            Freeandnil(Fportador);
     End;

end;

procedure TFprincipal.PedidoProjetonoRomaneio1Click(Sender: TObject);
 Var
FPedidoProjetoRomaneio : TFPedidoProjetoRomaneio;
begin

    if (ObjIconesglobal.VerificaClick('PedidoProjetonoRomaneio1Click'))
    then exit;
    
    try


        FPedidoProjetoRomaneio:= TFPedidoProjetoRomaneio.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Pedido Projetos do Romaneio');
    end;

    try
      FPedidoProjetoRomaneio.ShowModal;
    finally
      FreeAndNil(FPedidoProjetoRomaneio);
    end;

end;

//FAZEM PARTE DO MENU DP, QUANDO FOR REATIVADO � S� TIRAR O COMENT�RIO
{procedure TFprincipal.CargodeFuncionrios1Click(Sender: TObject);
Var
FCargoFuncionario : TFCargoFuncionario;
begin

    if (ObjIconesglobal.VerificaClick('CargodeFuncionrios1Click'))
    then exit;

    try
        FCargoFuncionario:= TFCargoFuncionario.Create(nil);
        FCargoFuncionario.show;
    except
        MensagemErro('Erro ao tentar criar a tela de Cargo de Funcionario');
    end;
end;   }

{procedure TFprincipal.Funcionrios1Click(Sender: TObject);
Var
FFuncionarios:TFFuncionarios;
begin
    if (ObjIconesglobal.VerificaClick('Funcionrios1Click'))
    then exit;


    try
        FFuncionarios:= TFfuncionarios.Create(nil);
        FFuncionarios.show;
    except
        MensagemErro('Erro ao tentar criar a tela de Funcionarios');
    end;
end;   }

procedure TFprincipal.FaixadeDesconto1Click(Sender: TObject);
var
ffaixadesconto:TFFAIXADESCONTO;
begin
    if (ObjIconesglobal.VerificaClick('FaixadeDesconto1Click'))
    then exit;

     Try
         ffaixadesconto:=TFFAIXADESCONTO.Create(nil);

     Except
         Messagedlg('Erro na tentativa de criar o formul�rio de Faixa de Desconto',mterror,[mbok],0);
         exit;
     End;

     try
       ffaixadesconto.ShowModal;
     finally
        FreeAndNil(ffaixadesconto);
     end;


end;

procedure TFprincipal.ComissodeVendedores1Click(Sender: TObject);
Var
FComissaoVendedores:TFCOMISSAOVENDEDORES;
begin

    if (ObjIconesglobal.VerificaClick('ComissodeVendedores1Click'))
    then exit;

    try
        FComissaoVendedores:=TFCOMISSAOVENDEDORES.Create(nil);
        FComissaoVendedores.show;
    except
        MensagemErro('Erro ao tentar criar a tela Comiss�o de vendedores');
    end;
end;

procedure TFprincipal.BtSobreClick(Sender: TObject);
Var FFornecedor : TFFornecedor;
begin
     if (ObjIconesglobal.VerificaClick('Fornecedor1Click'))
     then exit;

     try
         FFornecedor:=TFFornecedor.Create(nil);

     except
         MensagemErro('Erro ao tentar criar a tela de Fornecedores');
     end;

     try
       FFornecedor.ShowModal;
     finally
        FreeAndNil(FFornecedor);
     end;

end;

//FAZEM PARTE DO MENU DP, QUANDO FOR REATIVADO � S� TIRAR O COMENT�RIO
{procedure TFprincipal.FolhadePagamento1Click(Sender: TObject);
Var
FFolhaPagamento:TFFolhaPagamento;
begin
    if (ObjIconesglobal.VerificaClick('FolhadePagamento1Click'))
    then exit;

    try
        FFolhaPagamento:=TFFolhaPagamento.Create(nil);
        FFolhaPagamento.show;
    except
        MensagemErro('Erro ao tentar criar a tela de Folha de Pagamento');
    end;
end;    }

procedure TFprincipal.FuncionriosporFolha1Click(Sender: TObject);
Var
FFuncionarioFolhaPagamento:TFFuncionarioFolhaPagamento;
begin
    if (ObjIconesglobal.VerificaClick('FuncionriosporFolha1Click'))
    then exit;


    try
        FFuncionarioFolhaPagamento:=TFFuncionarioFolhaPagamento.Create(nil);
        FFuncionarioFolhaPagamento.show;
    except
        MensagemErro('Erro ao tentar criar a tela de Funcion�rios por Folha de Pagamento');
    end;
end;

procedure TFprincipal.Colocador1Click(Sender: TObject);
Var
FColocador:TFColocador;
begin

    if (ObjIconesglobal.VerificaClick('Colocador1Click'))
    then exit;

    try
        FColocador:=TFColocador.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Colocadores');
    end;

    try
      FColocador.ShowModal;
    finally
       FreeAndNil(FColocador);
    end;


end;

procedure TFprincipal.btConsultaRapidaClick(Sender: TObject);
Var  FConsultaRapida:TFConsultaRapida;
begin
    if (ObjIconesglobal.VerificaClick('btConsultaRapidaClick'))
    then exit;


     try
          FConsultaRapida:=TFConsultaRapida.Create(nil);

     except
          MensagemErro('Erro ao tentar criar a tela de pesquisa r�pida.');
     end;

     try
         FConsultaRapida.ShowModal;
     finally
         FreeAndNil(FConsultaRapida);
     end;

end;

procedure TFprincipal.btPedidoRapidoClick(Sender: TObject);
Var FPedido : TFPedido;
begin

    if (ObjIconesglobal.VerificaClick('btPedidoRapidoClick'))
    then exit;


    try
        FPedido := TFPedido.Create(nil);

        if (FPedido.PreenchePedidoParaConsultaRapida=false) then
        Begin
             MensagemErro('Erro ao tentar pr� preencher o pedido');
             FPedido.Close;
             exit;
        end;

        FPedido.show;
    except
        MensagemErro('Erro ao tentar criar a tela de Pedido');
    end;
end;

procedure TFprincipal.PedidoRpido1Click(Sender: TObject);
Var FPedido : TFPedido;
begin
    if (ObjIconesglobal.VerificaClick('PedidoRpido1Click'))
    then exit;



    try
        FPedido := TFPedido.Create(nil);

        if (FPedido.PreenchePedidoParaConsultaRapida=false) then
        Begin
             MensagemErro('Erro ao tentar pr� preencher o pedido');
             FPedido.Close;
             exit;
        end;

        FPedido.show;
    except
        MensagemErro('Erro ao tentar criar a tela de Pedido');
    end;
end;

procedure TFprincipal.Materiais2Click(Sender: TObject);
Var  FConsultaRapida:TFConsultaRapida;
begin
    if (ObjIconesglobal.VerificaClick('Materiais2Click'))
    then exit;


     try
          FConsultaRapida:=TFConsultaRapida.Create(nil);
          FConsultaRapida.Show;
     except
          MensagemErro('Erro ao tentar criar a tela de pesquisa r�pida.');
     end;
end;

procedure TFprincipal.AtualizaCNPJeCPF1Click(Sender: TObject);
Var   ObjCliente : TOBJCLiente;
      Query : TIBQuery;
      PCNPJ:string;
begin
    //VERIFICAR PORQUE N�O ESTA FUNCIONADO, OU MELHOR POR N�O ESTA FUNCIONANDO... TALVEZ SEJA POR UM MOTIVO �BVIO
end;

procedure TFprincipal.RelacFolhaComisses1Click(Sender: TObject);
Var
   Fcomissao_Adiant_FuncFolha:TFcomissao_Adiant_FuncFolha;
begin
    if (ObjIconesglobal.VerificaClick('RelacFolhaComisses1Click'))
    then exit;

     try
          Fcomissao_Adiant_FuncFolha:=TFcomissao_Adiant_FuncFolha.Create(nil);
          Fcomissao_Adiant_FuncFolha.Show;
     except
          MensagemErro('Erro ao tentar criar a tela de Relac. Entre Comiss�es, Adiantamento e a Folha de Pagamento');
     end;
end;

procedure TFprincipal.ComissodoColocador1Click(Sender: TObject);
Var
   FComissaoColocador:TFComissaoColocador;
begin
    if (ObjIconesglobal.VerificaClick('ComissodoColocador1Click'))
    then exit;

     try
          FComissaoColocador:=TFComissaoColocador.Create(nil);
          FComissaoColocador.Show;
     except
          MensagemErro('Erro ao tentar criar a tela de Comiss�o de Colocador');
     end;
end;

procedure TFprincipal.HoraExtra1Click(Sender: TObject);
Var
   FHoraExtra:TFHoraExtra;
begin
    if (ObjIconesglobal.VerificaClick('HoraExtra1Click'))
    then exit;

     try
          FHoraExtra:=TFHoraExtra.Create(nil);
          FHoraExtra.Show;
     except
          MensagemErro('Erro ao tentar criar a tela de Hora Extra');
     end;
end;

procedure TFprincipal.FaixadeDescontoIRPF1Click(Sender: TObject);
Var
   FFaixaDescontoIRPF:TFFaixaDescontoIRPF;
begin
    if (ObjIconesglobal.VerificaClick('FaixadeDescontoIRPF1Click'))
    then exit;

     try
          FFaixaDescontoIRPF:=TFFaixaDescontoIRPF.Create(nil);
          FFaixaDescontoIRPF.Show;
     except
          MensagemErro('Erro ao tentar criar a tela de Faixa de Desconto de Imposto de Renda');
     end;
end;

procedure TFprincipal.FaixadeDescontodeINSS1Click(Sender: TObject);
Var
   FFaixaDescontoINSS:TFFaixaDescontoINSS;
begin
    if (ObjIconesglobal.VerificaClick('FaixadeDescontodeINSS1Click'))
    then exit;

     try
          FFaixaDescontoINSS:=TFFaixaDescontoINSS.Create(nil);
          FFaixaDescontoINSS.Show;
     except
          MensagemErro('Erro ao tentar criar a tela de Faixa de Desconto de INSS');
     end;
end;

procedure TFprincipal.Adiantamento1Click(Sender: TObject);
Var
Fadiantamentofuncionario:TFadiantamentofuncionario;
begin
    if (ObjIconesglobal.VerificaClick('Adiantamento1Click'))
    then exit;

    try
        Fadiantamentofuncionario:= TFadiantamentofuncionario.Create(nil);
        Fadiantamentofuncionario.show;
    except
        MensagemErro('Erro ao tentar criar a tela de Adiantamento de Sal�rio');
    end;
end;

procedure TFprincipal.Comisso2Click(Sender: TObject);
var
TmpObjComissao_Adiant_FuncFolha:TobjComissao_Adiant_FuncFolha;
begin
    if (ObjIconesglobal.VerificaClick('Comisso2Click'))
    then exit;

     try
        TmpObjComissao_Adiant_FuncFolha:=TobjComissao_Adiant_FuncFolha.create(self);
     Except
        Messagedlg('Erro na Cria��o de Criar o Objeto de Comiss�o de Funcion�rio',mterror,[mbok],0);
        exit;
     End;

     Try
        TmpObjComissao_Adiant_FuncFolha.relatorioscomissao;
     Finally
        TmpObjComissao_Adiant_FuncFolha.free;
     End;
end;

procedure TFprincipal.SalrioFamlia1Click(Sender: TObject);
var
FSalarioFamiliaX:TFSalarioFamilia;
begin
    if (ObjIconesglobal.VerificaClick('SalrioFamlia1Click'))
    then exit;

     Try
        FSalarioFamiliaX:=TFSalarioFamilia.Create(nil);
        FSalarioFamiliaX.Show;
     Except
           Messagedlg('Erro na tentativa de Criar o Cadastro de Sal�rio Fam�lia',mterror,[mbok],0);
           exit;
     End;
end;

procedure TFprincipal.ransfereBackuppersianas1Click(Sender: TObject);
begin
    if (ObjIconesglobal.VerificaClick('ransfereBackuppersianas1Click'))
    then exit;

     if (ObjUsuarioGlobal.Get_nome='SYSDBA')
     Then FRecuperaBackup.showmodal;
end;

procedure TFprincipal.Gratificaes1Click(Sender: TObject);
var
FGratificacaoX:TFGratificacao;
begin
    if (ObjIconesglobal.VerificaClick('Gratificaes1Click'))
    then exit;

     Try
        FGratificacaoX:=TFGratificacao.Create(nil);
        FGratificacaoX.Show;
     Except
           Messagedlg('Erro na tentativa de Criar o Cadastro de Gratifica��es',mterror,[mbok],0);
           exit;
     End;
end;

procedure TFprincipal.Arquiteto1Click(Sender: TObject);
var
FArquitetoX:TFArquiteto;
begin
    if (ObjIconesglobal.VerificaClick('Arquiteto1Click'))
    then exit;

     Try
        FArquitetoX:=TFArquiteto.Create(nil);

     Except
           Messagedlg('Erro na tentativa de Criar o Cadastro de Arquiteto',mterror,[mbok],0);
           exit;
     End;

     try
          FArquitetoX.ShowModal;
     finally
          FreeAndNil(FArquitetoX);
     end;
End;

procedure TFprincipal.CFOP1Click(Sender: TObject);
var
FCfop:TFCfop;
begin

    if (ObjIconesglobal.VerificaClick('CFOP1Click'))
    then exit;

     Try
        FCfop:=TFCfop.create(nil);

     Except
           Messagedlg('Erro na tentativa de Criar o Cadastro de Cfop',mterror,[mbok],0);
           exit;
     End;

     try
        FCfop.ShowModal;
     finally
        FreeAndNil(FCfop);
     end;

End;

procedure TFprincipal.NotaFiscal2Click(Sender: TObject);
var
Fnotafiscal:TFNotaFiscal;
begin
    if (ObjIconesglobal.VerificaClick('NotaFiscal2Click'))
    then exit;

     Try
        Fnotafiscal:=TFNotaFiscal.create(nil);

     Except
           Messagedlg('Erro na tentativa de Criar o Cadastro de nota fiscal',mterror,[mbok],0);
           exit;
     End;

     try
         Fnotafiscal.Showmodal;
     finally
           FreeAndNil(Fnotafiscal);
     end;

End;

procedure TFprincipal.ConfiguraodeNF1Click(Sender: TObject);
begin
    if (ObjIconesglobal.VerificaClick('ConfiguraodeNF1Click'))
    then exit;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('CONFIGURACAO DE NOTA FISCAL')=False)
     then exit;

     FRelNotaFiscalRdPrint.Showmodal;
end;

procedure TFprincipal.rocadeMateriaisemPedidosConcludos1Click(
  Sender: TObject);
begin

    if (ObjIconesglobal.VerificaClick('rocadeMateriaisemPedidosConcludos1Click'))
    then exit;

     FtrocaMaterialpedido.showmodal;
end;

procedure TFprincipal.RefazContabilidadeEntradaseSaidas1Click(Sender: TObject);
begin
    if (ObjIconesglobal.VerificaClick('RefazContabilidadeEntradaseSaidas1Click'))
    then exit;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('REFAZER CONTABILIDADE')=False)
     Then exit;

     FRefazContabilidadeEntradaeSaida.Showmodal;
end;

procedure TFprincipal.EstoqueporMaterial1Click(Sender: TObject);
var
   FRelatorioEstoque:TfrelatorioEstoque;
begin
    if (ObjIconesglobal.VerificaClick('EstoqueporMaterial1Click'))
    then exit;

   Try
      FRelatorioEstoque:=TfrelatorioEstoque.Create(nil);
      FRelatorioEstoque.showmodal;
   Finally
      Freeandnil(FRelatorioEstoque);
   end;

end;

procedure TFprincipal.ContasaReceberporVendedor1Click(Sender: TObject);
Var
Objpedido:tobjPedido;
begin
    if (ObjIconesglobal.VerificaClick('ContasaReceberporVendedor1Click'))
    then exit;

     try
        Objpedido:=tobjPedido.create;
     Except
           mensagemerro('Erro na tentativa de Criar o Objeto de Pedido');
           exit;
     End;

     Try
           ObjPedido.Imprime_Contas_a_Receber_Por_Vendedor;
     Finally
            Objpedido.free;
     End;

end;

procedure TFprincipal.ReajustaValores1Click(Sender: TObject);
begin
    if (ObjIconesglobal.VerificaClick('ReajustaValores1Click'))
    then exit;

     FreajustaMateriais.showmodal;
end;


procedure TFprincipal.AcrscimodeDSR1Click(Sender: TObject);
var
FAcrescimoDSRX:TFDescansoSemanalRemunerado;
begin
    if (ObjIconesglobal.VerificaClick('AcrscimodeDSR1Click'))
    then exit;

     Try
        FAcrescimoDSRX:=TFDescansoSemanalRemunerado.Create(nil);
        FAcrescimoDSRX.Show;
     Except
           Messagedlg('Erro na tentativa de Criar o Cadastro de Gratifica��es',mterror,[mbok],0);
           exit;
     End;
end;


procedure TFprincipal.DescontodeContSindical1Click(Sender: TObject);
var
FdescContribuicaosindicalX:TFDescontoContribuicaoSindical;
begin
    if (ObjIconesglobal.VerificaClick('DescontodeContSindical1Click'))
    then exit;

     Try
        FdescContribuicaosindicalX:=TFDescontoContribuicaoSindical.create(nil);
        FdescContribuicaosindicalX.Show;
     Except
           Messagedlg('Erro na tentativa de Criar o Cadastro de Desconto de Contribui��o Sindical',mterror,[mbok],0);
           exit;
     End;
end;

procedure TFprincipal.GeraContabilidadeChDevcomFornecedor1Click(Sender: TObject);
begin
    if (ObjIconesglobal.VerificaClick('GeraContabilidadeChDevcomFornecedor1Click'))
    then exit;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('REFAZER CONTABILIDADE DE CHEQUE DEVOLVIDO')=False)
     then exit;
     
     FrefazContabilidadeChequeDevolvido.showmodal;
end;

procedure TFprincipal.MovimentaoDiria1Click(Sender: TObject);
var
objpedidoobjeto:TobjPedidoObjetos;
begin
    if (ObjIconesglobal.VerificaClick('MovimentaoDiria1Click'))
    then exit;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('IMPRIMIR RELAT�RIO DE MOVIMENTA��O DI�RIA')=False)
     Then exit;

     Try
        objpedidoobjeto:=TobjPedidoObjetos.create(nil);
     Except
           MensagemErro('Erro na tentativa de Criar o Objeto de Pedido de Objeto');
           exit;
     End;

     Try
        objpedidoobjeto.Relatorio_Movimentacao_Diaria;

     Finally
            objpedidoobjeto.free;
     End;

end;

procedure TFprincipal.PedidosquenoentraramemRomaneio1Click(
  Sender: TObject);
var
   objpedidoobjeto:TobjPedidoObjetos;
begin

    if (ObjIconesglobal.VerificaClick('PedidosquenoentraramemRomaneio1Click'))
    then exit;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('IMPRIMIR RELAT�RIO DE PEDIDOS QUE N�O ENTRAM EM ROMANEIO E ROMANEIO N�O CONCLU�DO')=False)
     Then exit;

     Try
        objpedidoobjeto:=TobjPedidoObjetos.create(nil);
     Except
           MensagemErro('Erro na tentativa de Criar o Objeto de Pedido de Objeto');
           exit;
     End;

     Try
        objpedidoobjeto.Relatorio_Pedidos_nao_entraram_romaneio_e_nao_concluidos;
     Finally
            objpedidoobjeto.free;
     End;
end;

procedure TFprincipal.RelatriodeItensemNF1Click(Sender: TObject);
var
   FrelatoriosItensNFx:TFrelatoriosItensNF;
begin
    if (ObjIconesglobal.VerificaClick('RelatriodeItensemNF1Click'))
    then exit;

   try
      FrelatoriosItensNFx:=TFrelatoriosItensNF.Create(nil);
   Except
         Mensagemerro('Erro na tentativa de Criar o Formul�rio Relat�rio de Itens em NF');
         exit;
   End;

   Try
         FrelatoriosItensNFx.showmodal;
   finally
          freeandnil(FrelatoriosItensNFx);
   End;

end;

procedure TFprincipal.PsVenda1Click(Sender: TObject);
var
   objpedidoobjeto:TobjPedidoObjetos;
begin
    if (ObjIconesglobal.VerificaClick('PsVenda1Click'))
    then exit;

     Try
        objpedidoobjeto:=TobjPedidoObjetos.create(self);
     Except
           MensagemErro('Erro na tentativa de Criar o Objeto de Pedido de Objeto');
           exit;
     End;

     Try
        objpedidoobjeto.Relatorio_pedidos_recebidos_entregues;
     Finally
            objpedidoobjeto.free;
     End;
end;


procedure TFprincipal.PedidodeCompra1Click(Sender: TObject);
var
   FpedidoComprax:TFPEDIDOCOMPRA;
begin
    if (ObjIconesglobal.VerificaClick('PedidodeCompra1Click'))
    then exit;

     Try
        FpedidoComprax:=TFPEDIDOCOMPRA.create(nil);

     Except
           Messagedlg('Erro na tentativa de Criar o Cadastro de Pedido de Compra',mterror,[mbok],0);
           exit;
     End;

     try
       FpedidoComprax.ShowModal;
     finally
        FreeAndNil(FpedidoComprax);
     end;

end;


procedure TFprincipal.LotedeComissodeVendedores1Click(Sender: TObject);
var
   FLoteComissaoX:TFLoteComissao;
begin
    if (ObjIconesglobal.VerificaClick('LotedeComissodeVendedores1Click'))
    then exit;

     Try
        FLoteComissaoX:=TFLoteComissao.create(nil);
     Except
           Messagedlg('Erro na tentativa de Criar o Cadastro de Lote de Comiss�o',mterror,[mbok],0);
           exit;
     End;

     try
        FLoteComissaoX.showmodal;
     Finally
            freeandnil(FLoteComissaoX);
     End;
end;


procedure TFprincipal.CarregaParametrosGlobais;
begin
    //*************************************************
    If (ObjParametroGlobal.ValidaParametro('CAMINHO DOS DESENHOS')=FALSE)
    Then Begin
              Messagedlg('Par�metro n�o encontrado "CAMINHO DOS DESENHOS"!', mterror,[mbok],0);
              strtoint('a');
    End;
    FDataModulo.LocalDesenhosGlobal := VerificaBarraFinalDiretorio( ObjParametroGlobal.Get_Valor );

    //*************************************************
    if (ObjParametroGlobal.ValidaParametro('CODIGO DO PROJETO EM BRANCO')=false)
    then begin
             MensagemErro('O Par�metro "CODIGO DO PROJETO EM BRANCO" n�o foi encontrado');
             strtoint('a');
    end;
    FDataModulo.CodigoProjetoBrancoGlobal:=ObjParametroGlobal.Get_Valor;
    //*************************************************
    //PDescontoMaximo_global:Currency;

    if (ObjParametroGlobal.ValidaParametro('DESCONTO M�XIMO NO PEDIDO EM PORCENTAGEM')=False)
    then begin
             strtoint('a');
    end;
    Try
          FDataModulo.descontomaximo_global:=strtofloat(ObjParametroGlobal.Get_Valor);
    Except
          FDataModulo.descontomaximo_global:=30;
          MensagemErro('Valor Inv�lido no par�metro "DESCONTO M�XIMO NO PEDIDO EM PORCENTAGEM" ser� definido em 30% como padr�o');
    End;
    //*************************************************
   { MenuFolhaPagamento.Visible:=False;
    if (ObjParametroGlobal.ValidaParametro('HABILITA MENU FOLHA DE PAGAMENTO')=True)
    then begin
             if (ObjParametroGlobal.get_valor='SIM')
             Then MenuFolhaPagamento.Visible:=True;
    End;  }
    //*************************************************
    DescontaComissaoArquitetoVendedor:=False;
    if (ObjParametroGlobal.ValidaParametro('UTILIZA COMISSAO DO ARQUITETO NO CALCULO DA COMISSAO DO VENDEDOR')=True)
    then begin
             if (ObjParametroGlobal.get_valor='SIM')
             Then DescontaComissaoArquitetoVendedor:=True;
    End;
    //***********************************************
    EstadoSistemaGlobal:='MS';
    if (ObjParametroGlobal.ValidaParametro('UNIDADE DA FEDERA��O(ESTADO) DO SISTEMA')=True)
    then begin
             ESTADOSISTEMAGLOBAL:=ObjParametroGlobal.get_valor;
    End;
    //**********************************************
    Fdatamodulo.ValidaCPFCNPJ_venda_global:=False;

    if (ObjParametroGlobal.ValidaParametro('VALIDA CPF E CNPJ NO MOMENTO DA VENDA?')=True)
    then begin
             if (ObjParametroGlobal.get_valor='SIM')
             Then FDataModulo.ValidaCPFCNPJ_venda_global:=True;
    End;
    //**********************************************


    Fdatamodulo.ValidaCPFCNPJ_cadastramento_Cliente:=False;

    if (ObjParametroGlobal.ValidaParametro('VALIDA CPF E CNPJ NO CADASTRAMENTO DE CLIENTE?')=True)
    then begin
             if (ObjParametroGlobal.get_valor='SIM')
             Then FDataModulo.ValidaCPFCNPJ_cadastramento_Cliente:=True;
    End;
    //**********************************************


   Fdatamodulo.UtilizaArredondamentoGlobal5cm:=False;
   if (ObjParametroGlobal.ValidaParametro('UTILIZA ARREDONDAMENTO DE 5 CM?')=True)
   then begin
             if (ObjParametroGlobal.get_valor='SIM')
             Then Fdatamodulo.UtilizaArredondamentoGlobal5cm:=True;
   End;
   //**********************************************


   ImprimeCepSeparadoPedidoCompacto_global:=False;
   if (ObjParametroGlobal.ValidaParametro('IMPRIME CEP SEPARADO NO PEDIDO COMPACTO')=True)
   then
   begin
             if (ObjParametroGlobal.get_valor='SIM')
             Then ImprimeCepSeparadoPedidoCompacto_global:=True;
   End;


   {jonas}
   UtilizaNfe:=False;
   if (ObjParametroGlobal.ValidaParametro('UTILIZA NFE?')) then
      if (ObjParametroGlobal.Get_Valor = 'SIM') then
        UtilizaNfe := True;

   escolheTransportadora := False;
   if (ObjParametroGlobal.ValidaParametro('ESCOLHE TRANSPORTADORA AO GERAR NF?')) then
   begin

    if (ObjParametroGlobal.get_valor='SIM') then
      escolheTransportadora:=True;

   End;

   ObjParametroGlobal.ValidaParametro('UTILIZA SPED FISCAL');
   if (ObjParametroGlobal.Get_Valor = 'SIM') then
   utilizaSpedFiscal:=True;

   ObjParametroGlobal.ValidaParametro('PADRAO INDICE DE PAGAMENTO NA NFE');
   indicePagmentoNfe := StrToIntDef(ObjParametroGlobal.Get_Valor()[1],0);

   {}

end;

function TFPrincipal.ValidaVenctoPEnd: boolean;
var
objvenctopend:TObjVENCTOPEND;
PdataVencimento:Tdatetime;
DataAtual:Tdate;
DataCompilacao:string;
begin
     result:=False;
     Try
        objvenctopend:=TObjVENCTOPEND.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto da venctopend!',mterror,[mbok],0);
           exit;
     End;
     Try
        result:=objvenctopend.ValidaEntrada(PdataVencimento);

        DataAtual:=objvenctopend.RetornaDataServidor;

        //Showmessage('Cria��o '+DataDeCriacaoExe+' Modifica��o '+DataDeModificacaoExe);
        DataCompilacao:=DataDeModificacaoExe;

        if (dataCompilacao='')//erro, nao conseguiu detectar a data de modificacao do arquivo
        Then Begin//fixo uma data
                  if (DataAtual>strtodate('10/12/2010'))
                  Then Begin
                            Messagedlg('ERROR: The system database needs a new compilation',mterror,[mbok],0);
                            Result:=False;
                  End;
        End
        Else Begin
                  //O Sistema conseguiu resgatar a data da ultima modificacao no arquivo
                  if (dataatual>IncMonth(Strtodate(DataCompilacao),20))
                  Then Begin
                            //Validade de 20 meses
                            Messagedlg('0.6.ERROR: The system database needs a new compilation',mterror,[mbok],0);
                            Result:=False;
                  End;
        End;

        if (strtodate(Datetostr(now))<>DataAtual)
        Then Begin
                  MensagemErro('Data do Computador diferente da data do servidor '+#13+'Data do Servidor '+datetostr(dataatual)+'. Data do computador '+datetostr(now));
                  result:=False;
        End;

     Finally
        objvenctopend.free;
     End;
end;

procedure TFprincipal.ComponentesdoPedidoprojeto1Click(Sender: TObject);
Var
Fcomponente_PPX:TfComponente_PP;
begin

    if (ObjIconesglobal.VerificaClick('ComponentesdoPedidoprojeto1Click'))
    then exit;

    try
        Fcomponente_PPX:=TfComponente_PP.create(nil);
    except
        MensagemErro('Erro ao tentar criar a tela de Componente dos Pedidos/Projetos');
        exit;
    end;

    Try
        Fcomponente_PPX.showmodal;
    Finally
        freeandnil(Fcomponente_PPX);
    End;
end;

procedure TFprincipal.ImportaodePedidos1Click(Sender: TObject);
var
FimportapedidoX:TFimportapedido;
begin
    if (ObjIconesglobal.VerificaClick('ImportaodePedidos1Click'))
    then exit;

     Try
        if (ObjPermissoesUsoGlobal.ValidaPermissao('IMPORTAR PEDIDOS')=False)
        then exit;

        Fimportapedidox:=TFimportapedido.create(nil);

     Except
           Messagedlg('Erro na tentativa de Criar o M�dulo de Importa��o',mterror,[mbok],0);
           exit;
     End;

     try
         Fimportapedidox.ShowModal;
     finally
         FreeAndNil(FimportapedidoX);
     end;


End;


procedure TFprincipal.AcertoseAjustesnoBD1Click(Sender: TObject);
begin
    if (ObjIconesglobal.VerificaClick('AcertoseAjustesnoBD1Click'))
    then exit;

     if (ObjUsuarioGlobal.Get_nome<>'SYSDBA')
     Then BEgin
               MensagemErro('O usu�rio atual n�o tem permiss�o para acessar esse cadastro');
               exit;
     End;

     Fajustes.showmodal;
end;

procedure TFprincipal.FluxodeCaixaExcel1Click(Sender: TObject);
begin
    if (ObjIconesglobal.VerificaClick('FluxodeCaixaExcel1Click'))
    then exit;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO FLUXO DE CAIXA')=False)
     Then exit;

     FfluxoCaixa.showmodal;
end;

procedure TFprincipal.GeraSqlsdeAlteracaodeDatasnaContabilidade1Click(
  Sender: TObject);
begin
    if (ObjIconesglobal.VerificaClick('GeraSqlsdeAlteracaodeDatasnaContabilidade1Click'))
    then exit;

     FCADASTRARELATORIOPERSONALIZADO.Showmodal;
end;

procedure TFprincipal.ImportaodeDados1Click(Sender: TObject);
var
FimportatabelaX:Tfimportatabela;
begin

    if (ObjIconesglobal.VerificaClick('ImportaodeDados1Click'))
    then exit;


     Try
        FimportatabelaX:=Tfimportatabela.Create(nil);
     Except
           MensagemErro('Erro na tentativa de criar o formul�rio');
           exit;
     End;

     Try
          FimportatabelaX.showmodal;
     Finally
            freeandnil(FimportatabelaX);
     End;
end;

procedure TFprincipal.ArquivoINI1Click(Sender: TObject);
var
FarquivoIniX:TFarquivoIni;
begin
    if (ObjIconesglobal.VerificaClick('ArquivoINI1Click'))
    then exit;

     Try
        FarquivoIniX:=TFarquivoIni.Create(nil);
     Except
           MensagemErro('Erro na tentativa de criar o formul�rio de arquivo INI');
           exit;
     End;

     Try
        FarquivoIniX.showmodal;
     Finally
            Freeandnil(FarquivoIniX);
     End;
end;

procedure TFprincipal.FinalizaPedidosProjetos1Click(Sender: TObject);
begin
    if (ObjIconesglobal.VerificaClick('FinalizaPedidosProjetos1Click'))
    then exit;

    FFinalizaComissaoColocador.Showmodal;
end;

procedure TFprincipal.ComissodeColocador1Click(Sender: TObject);
var
objcomissaocolocador:TObjCOMISSAOCOLOCADOR;
begin
    if (ObjIconesglobal.VerificaClick('ComissodeColocador1Click'))
    then exit;

     Try
        objcomissaocolocador:=TObjCOMISSAOCOLOCADOR.Create(self);
     Except
           Mensagemerro('Erro na tentativa de Criar a Comiss�o do Colocador');
           exit;
     End;

     Try
        objcomissaocolocador.ImprimeComissaoColocador;
     Finally
            objcomissaocolocador.Free;
     End;
end;


procedure TFprincipal.MostraNovidade;
Var
  FNovidadeXX:TFNovidade;
Begin

     try
         FNovidadeXX:=TFNovidade.Create(nil);

         if (FNovidadeXX.VerificaNovidade=true)
         then Begin
                  FNovidadeXX.SomenteNovasUsuario:=true;
                  FNovidadeXX.ShowModal;
         End;

     finally
         FreeAndNil(FNovidadeXX);

     end;
end;


procedure TFprincipal.Novidades1Click(Sender: TObject);
Var
   FNovidadeXX:TFNovidade;
Begin
    if (ObjIconesglobal.VerificaClick('Novidades1Click'))
    then exit;

     try
         FNovidadeXX:=TFNovidade.Create(nil);
         FNovidadeXX.SomenteNovasUsuario:=False;
         FNovidadeXX.ShowModal;
     finally
         FreeAndNil(FNovidadeXX);
     end;
end;

procedure TFprincipal.HabilitarCriaodecone1Click(Sender: TObject);
begin
    if (ObjIconesglobal.VerificaClick('HabilitarCriaodecone1Click'))
    then exit;

    ObjIconesglobal.Control:=True;

end;

procedure TFprincipal.DesabilitarCriaodecone1Click(Sender: TObject);
begin
    if (ObjIconesglobal.VerificaClick('DesabilitarCriaodecone1Click'))
    then exit;

    ObjIconesglobal.Control:=False;

end;

procedure TFprincipal.TimerIconeTimer(Sender: TObject);
begin
    if (Objiconesglobal<>nil)
    then Begin
              if (ObjIconesglobal.Control=False)
              Then //lbIconeHabilitado.caption:='�cone Desabilitado';
    End;
end;

procedure TFprincipal.CadastrodeEmpresa1Click(Sender: TObject);
begin
    if (ObjIconesglobal.VerificaClick('CadastrodeEmpresa1Click'))
    then exit;

     Fempresa.showmodal;


     if (ObjEmpresaGlobal.LocalizaCodigo('1')=False)
     Then Begin
               Mensagemerro('A empresa c�digo 1 n�o foi encontrada, o sistema ser� finalizado');
               Self.Close;
               exit;
     End;
     ObjEmpresaGlobal.TabelaparaObjeto;

end;

procedure TFprincipal.RelatrioPersonalizadoReportBuilder1Click(
  Sender: TObject);
begin
     FRELPERSREPORTBUILDER.showmodal;
end;

procedure TFprincipal.ConfiguraodeDuplicataModelo11Click(Sender: TObject);
begin
     FconfDuplicata.showmodal;
end;

procedure TFprincipal.ConfiguraodeDuplicataModelo21Click(Sender: TObject);
begin
     FconfDuplicata02.showmodal;
end;

procedure TFprincipal.ConfiguraBoletoPrImpresso1Click(Sender: TObject);
begin
   FConfiguraboleto.showmodal;
end;

procedure TFprincipal.ImpostoICMS1Click(Sender: TObject);
var
FImposto_ICmsX:TfImposto_ICMS;
begin
     if (ObjUsuarioGlobal.Get_nome<>'SYSDBA')
     Then Begin
               MensagemErro('Somente o usu�rio SYSDBA tem acesso a este m�dulo');
               exit;
     End;

     Try
        FImposto_ICmsX:=TfImposto_ICMS.create(nil);
     Except
           Mensagemerro('Erro na tentativa de criar o formul�rio de Imposto ICMS');
           exit;
     End;

     Try
        FImposto_ICmsX.Showmodal;
     Finally
            Freeandnil(FImposto_ICmsX);
     End;

end;

procedure TFprincipal.ipodeCliente1Click(Sender: TObject);
var
FtipoclienteX:Tftipocliente;
begin
     Try
        FtipoclienteX:=Tftipocliente.create(nil);
     Except
           Mensagemerro('Erro na tentativa de criar o formul�rio de Tipo de Clientes');
           exit;
     End;

     Try
        FtipoclienteX.Showmodal;
     Finally
            Freeandnil(FtipoclienteX);
     End;

end;

procedure TFprincipal.SituaoTributriaIPI1Click(Sender: TObject);
var
   FsituacaoTributariaIPIX:TFSITUACAOTRIBUTARIA_IPI;
begin

    if (ObjIconesglobal.VerificaClick('SituaoTributriaIPI1Click'))
    then exit;

    Try
       FsituacaoTributariaIPIX:=TFSITUACAOTRIBUTARIA_IPI.create(nil);
    Except
          on e:exception do
          Begin
              mensagemerro(e.message);
              exit;
          End;
    End;

    Try
       FsituacaoTributariaIPIX.showmodal;
    Finally
           freeandnil(FsituacaoTributariaIPIX)
    End;
end;

procedure TFprincipal.ImpostoIPI1Click(Sender: TObject);
var
   Fimposto_ipiX:TFImposto_IPI;
begin
    if (ObjUsuarioGlobal.Get_nome<>'SYSDBA')
    Then Begin
               MensagemErro('Somente o usu�rio SYSDBA tem acesso a este m�dulo');
               exit;
    End;

    Try
       Fimposto_ipiX:=TFImposto_IPI.create(nil);
    Except
          on e:exception do
          Begin
              mensagemerro(e.message);
              exit;
          End;
    End;

    Try
       Fimposto_ipiX.showmodal;
    Finally
           freeandnil(Fimposto_ipiX)
    End;
end;


procedure TFprincipal.SituaoTributriaPIS1Click(Sender: TObject);
var
   FsituacaoTributariaPISX:TFSITUACAOTRIBUTARIA_PIS;
begin

    if (ObjIconesglobal.VerificaClick('SituaoTributriaPIS1Click'))
    then exit;

    Try
       FsituacaoTributariaPISX:=TFSITUACAOTRIBUTARIA_PIS.create(nil);
    Except
          on e:exception do
          Begin
              mensagemerro(e.message);
              exit;
          End;
    End;

    Try
       FsituacaoTributariaPISX.showmodal;
    Finally
           freeandnil(FsituacaoTributariaPISX)
    End;
end;

procedure TFprincipal.SituaoTributriaCOFINS1Click(Sender: TObject);
var
   FsituacaoTributariaCOFINSX:TFSITUACAOTRIBUTARIA_COFINS;
begin

    if (ObjIconesglobal.VerificaClick('SituaoTributriaCOFINS1Click'))
    then exit;

    Try
       FsituacaoTributariaCOFINSX:=TFSITUACAOTRIBUTARIA_COFINS.create(nil);
    Except
          on e:exception do
          Begin
              mensagemerro(e.message);
              exit;
          End;
    End;

    Try
       FsituacaoTributariaCOFINSX.showmodal;
    Finally
           freeandnil(FsituacaoTributariaCOFINSX)
    End;
end;

procedure TFprincipal.ImpostoPIS1Click(Sender: TObject);
var
FImposto_PISX:TfImposto_PIS;
begin
     if (ObjUsuarioGlobal.Get_nome<>'SYSDBA')
     Then Begin
               MensagemErro('Somente o usu�rio SYSDBA tem acesso a este m�dulo');
               exit;
     End;

     Try
        FImposto_PISX:=TfImposto_PIS.create(nil);
     Except
           Mensagemerro('Erro na tentativa de criar o formul�rio de Imposto PIS');
           exit;
     End;

     Try
        FImposto_PISX.Showmodal;
     Finally
            Freeandnil(FImposto_PISX);
     End;

end;


procedure TFprincipal.ImpostoCofins1Click(Sender: TObject);
var
FImposto_COFINSX:TfImposto_COFINS;
begin
     if (ObjUsuarioGlobal.Get_nome<>'SYSDBA')
     Then Begin
               MensagemErro('Somente o usu�rio SYSDBA tem acesso a este m�dulo');
               exit;
     End;

     Try
        FImposto_COFINSX:=TfImposto_COFINS.create(nil);
     Except
           Mensagemerro('Erro na tentativa de criar o formul�rio de Imposto COFINS');
           exit;
     End;

     Try
        FImposto_COFINSX.Showmodal;
     Finally
            Freeandnil(FImposto_COFINSX);
     End;

end;

procedure TFprincipal.IESubtTributrioporEstado1Click(Sender: TObject);
var
   FIE_ST_ESTADOX :TFIE_ST_ESTADO;
begin

    if (ObjIconesglobal.VerificaClick('IESubtTributrioporEstado1Click'))
    then exit;

    Try
       FIE_ST_ESTADOX :=TFIE_ST_ESTADO.create(nil);
    Except
          on e:exception do
          Begin
              mensagemerro(e.message);
              exit;
          End;
    End;

    Try
       FIE_ST_ESTADOX .showmodal;
    Finally
           freeandnil(FIE_ST_ESTADOX );
    End;
end;

procedure TFprincipal.OperaodeNF1Click(Sender: TObject);
var
   FoperacaoNFX :TFoperacaoNF;
begin

    if (ObjIconesglobal.VerificaClick('OperaodeNF1Click'))
    then exit;

    Try
       FoperacaoNFX :=TFoperacaoNF.create(nil);
    Except
          on e:exception do
          Begin
              mensagemerro(e.message);
              exit;
          End;
    End;

    Try
       FoperacaoNFX .showmodal;
    Finally
        freeandnil(FoperacaoNFX );
    End;
end;


procedure TFprincipal.ReprocessaComissodeChequesDevolvidos1Click(
  Sender: TObject);
begin
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSAR MODULO DE REPROCESSAMENTO DE COMISSAO DE CHEQUE DEVOLVIDO')=False)
     then exit;
     FreprocessaChequeDevolvidoComissao.showmodal;
end;

procedure TFprincipal.CargodeFuncionrios1Click(Sender: TObject);
begin
    if (ObjIconesglobal.VerificaClick('CargoFuncionario1Click'))
    then exit;
    try
         FcargoFuncionario:= TFcargoFuncionario.create(nil);
         FcargoFuncionario.Show;
    except
         MensagemErro('Erro ao tentar criar a tela de RamoAtividades');
    end;
end;

procedure TFprincipal.Funcionrios1Click(Sender: TObject);
begin
     if (ObjIconesglobal.VerificaClick('Funcionario1Click'))
    then exit;
    try
        Ffuncionarios:= TFfuncionarios.Create(nil);

          Ffuncionarios.Show;
    except
         MensagemErro('Erro ao tentar criar a tela de RamoAtividades');
    end;
end;

procedure TFprincipal.btComprasClick(Sender: TObject);
Var FEntradaProdutos : TFEntradaProdutos;
begin
  try
       try
            FEntradaProdutos := TFEntradaProdutos.Create(nil);

       except
            MensagemErro('Erro ao tentar criar a tela de Entrada de Produtos');
       end;

       FEntradaProdutos.ShowModal;
  finally
        FreeAndNil(FEntradaProdutos);
  end;
end;

procedure TFprincipal.tmr1Timer(Sender: TObject);
begin
    lbHora.Caption:=TimeToStr (time);
end;

procedure TFprincipal.bt1Click(Sender: TObject);
var
  Query:TIBQuery;
  codigo:string;
begin
     { query:=TIBQuery.Create(nil);
      query.Database:=FDataModulo.IBDatabase;

      try
            //fazer aqui o teste de gravar com criptografia
            with Query do
            begin
                  InputQuery('','',codigo);

                  Close;
                  SQL.Clear;
                  sql.Add('select * from teste where codigo='+codigo);
                  Open;
                  if(Criptografa(FieldByName('nome').AsString)<>(FieldByName('CAMPOCRIPTO1').AsString))
                  then edt1.Text:='?????'
                  else edt1.Text:=FieldByName('nome').AsString;
                  if(Criptografa(FieldByName('nome2').AsString)<>(FieldByName('CAMPOCRIPTO2').AsString))
                  then edt2.Text:='?????'
                  else edt2.Text:=FieldByName('nome2').AsString;
                  if(Criptografa(FieldByName('valor').AsString)<>(FieldByName('CAMPOCRIPTO3').AsString))
                  then edt3.Text:='?????'
                  else edt3.Text:=FieldByName('valor').AsString;
                  


            end;



      finally
           FreeAndNil(Query);
      end;  }
end;


procedure TFprincipal.but1Click(Sender: TObject);
var
  Query:TIBQuery;
  codigo:Integer;
begin
      {query:=TIBQuery.Create(nil);
      query.Database:=FDataModulo.IBDatabase;

      try
            //fazer aqui o teste de gravar com criptografia
            with Query do
            begin
                  Close;
                  SQL.Clear;
                  sql.Add('select codigo from teste order by codigo');
                  Open;
                  Last;
                  if(fieldbyname('codigo').AsString='')
                  then codigo:=1
                  else codigo:=fieldbyname('codigo').AsInteger;

                  Inc(codigo,1);

                  Close;
                  sql.Clear;
                  sql.Add('insert into teste(codigo,nome,nome2,valor,CAMPOCRIPTO1,CAMPOCRIPTO2,CAMPOCRIPTO3) values('+#39+IntToStr(codigo)+#39+','+#39+edt1.Text+#39+','+#39+edt2.Text+#39+','+#39+edt3.Text+#39+','+#39+Criptografa(edt1.Text)+#39+','+#39+Criptografa(edt2.Text)+#39+','+#39+Criptografa(edt3.Text)+#39+')');
                  ExecSQL;
                  FDataModulo.IBTransaction.CommitRetaining;


            end;



      finally
           FreeAndNil(Query);
      end;
           }




end;


procedure TFprincipal.EstadosClick(Sender: TObject);
Var Festado:TFestado;
begin

      if (ObjIconesglobal.VerificaClick('Cidade1Click'))
     then exit;


    try
        Festado := TFestado.Create(nil);

    except
        MensagemErro('Erro ao tentar criar a tela de Cidade');
    end;

    try
        Festado.ShowModal
    finally
       FreeAndNil(Festado);
    end;

end;

procedure TFprincipal.ModeloDeNotaFiscal1Click(Sender: TObject);
begin
     FModeloDeNF.ShowModal;
end;

procedure TFprincipal.img2Click(Sender: TObject);
var
  Link:string;
begin

   Link:='http://www.exclaimhost.com.br/livezilla/chat.php';
   ShellExecute(Application.Handle, nil, PChar(Link), nil, nil, sw_hide);
end;

procedure TFprincipal.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
    formPesquisaMenu:TfPesquisaMenu;
    FvendaRapida:TFVendaRapida;
begin
  If (ssCtrl in Shift) then
  ObjIconesglobal.Control:=True;

  if (Key = VK_F1) then
  begin

    try
      formPesquisaMenu:=TfPesquisaMenu.Create(nil);
      formPesquisaMenu.submit_formulario(self);
      formPesquisaMenu.ShowModal;

    finally
      FreeAndNil(formPesquisaMenu);
    end;

  end;

  if (Key = VK_F2) then
  begin

       FAjuda.PassaAjuda('');
       FAjuda.ShowModal;
  end;

  if(key = VK_F3) then
  begin
      FvendaRapida:=TFVendaRapida.Create(nil);
      try
            FvendaRapida.ShowModal;
      finally
            FreeAndNil(FvendaRapida);
      end;
  end;

end;

procedure TFprincipal.CadastroNfe1Click(Sender: TObject);
var
  formNFe:TFnotaFiscalEletronica;
begin


  formNFe:=TFnotaFiscalEletronica.Create(nil);

  try

    formNFe.ShowModal;

  finally

    FreeAndNil(formNFe);

  end;
end;

procedure TFprincipal.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     If not(ssCtrl in Shift)
     Then ObjIconesglobal.Control:=False;
end;

procedure TFprincipal.Nfedigitada1Click(Sender: TObject);
begin

  if (ObjIconesglobal.VerificaClick('NFedigitada1Click')) then
    exit;

  if (FNfeDigitada = nil) then
   FNfeDigitada:=TFNfeDigitada.create (self);

   FNfeDigitada.ShowModal;

   FreeAndNil (FNfeDigitada);

end;

procedure TFprincipal.img3Click(Sender: TObject);
begin
     FpesquisaProjNovo.ShowModal;

end;

procedure TFprincipal.btOrdemServicoClick(Sender: TObject);
var
   FordemProducao:TFProducao;
begin
     FordemProducao:=TFProducao.Create(nil);

     try
          FordemProducao.ShowModal;
     finally
          FreeAndNil(FordemProducao);
     end;

end;

procedure TFprincipal.chamaPesquisaMenu(status:TDataSetState);
var
  formPesquisa:TfPesquisaMenu;
begin

  if (status = dsEdit) or (status = dsInsert) then
  begin
    ShowMessage('Pesquisa de menu n�o pode estar em inser��o ou edi��o');
    Exit;
  end;

  formPesquisa:=TfPesquisaMenu.Create(nil);
  
  try
    formPesquisa.submit_formulario(self);
    formPesquisa.ShowModal;
  finally
    FreeAndNil(formPesquisa);
  end;

end;


procedure TFprincipal.GeraodosRecebimentos1Click(Sender: TObject);
var
      lancamentoFC:TobjLancamentosFC;
begin
      lancamentoFC := TobjLancamentosFCSafira.Create;
      FreeAndNil(lancamentoFC);
end;

procedure TFprincipal.RelacionamentoFormasdePagamento1Click(
  Sender: TObject);
var
FFormasPagamento_FCX:TFFORMASPAGAMENTO_FC;
begin
     if (ObjIconesglobal.VerificaClick('RelacionamentoFormasdePagamento1'))
     Then exit;

     Try
          FFormasPagamento_FCX:=TFFORMASPAGAMENTO_FC.create(nil);
     Except
           Messagedlg('Erro na tentativa de Criar a tela de TeleMarketing',mterror,[mbok],0);
           exit;
     End;

     Try
        FFormasPagamento_FCX.ShowModal;
     Finally
            Freeandnil(FFormasPagamento_FCX);
     End;
end;

procedure TFprincipal.LanamentodeCrditoParaClientes1Click(Sender: TObject);
var
  FlancamentoCredito:TFlancCredito;
begin
      if (ObjIconesglobal.VerificaClick('LanamentodeCrditoParaClientes1Click'))
      then exit;
      
      FlancamentoCredito:=TFlancCredito.Create(nil);
      try
             FlancamentoCredito.ShowModal;
      finally
             FreeAndNil(FlancamentoCredito);
      end;
end;



procedure TFprincipal.CadastrodoCRT1Click(Sender: TObject);
begin

     if (ObjIconesglobal.VerificaClick('CadastrodoCRT1Click'))
     then exit;

     chamaFormulario(TFCRT,self);

end;

procedure TFprincipal.btOrdemMedicaoClick(Sender: TObject);
var
    FOrdemMedicao:TFOrdemMedicao;
begin
     FOrdemMedicao:=TFOrdemMedicao.Create(nil);

     try
          FOrdemMedicao.ShowModal;
     finally
          FreeAndNil(FOrdemMedicao);
     end;

end;

procedure TFprincipal.OrdemdeMedio1Click(Sender: TObject);
var
    FOrdemMedicao:TFOrdemMedicao;
begin
     FOrdemMedicao:=TFOrdemMedicao.Create(nil);
     try
          FOrdemMedicao.ShowModal;
     finally
          FreeAndNil(FOrdemMedicao);
     end;
end;

procedure TFprincipal.CadastrodeCobradores1Click(Sender: TObject);
begin
     FCobrador.ShowModal;
end;

procedure TFprincipal.Pedidosquenoentraramemromaneio2Click(
  Sender: TObject);
var
   objpedidoobjeto:TobjPedidoObjetos;
begin

    if (ObjIconesglobal.VerificaClick('PedidosquenoentraramemRomaneio1Click'))
    then exit;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('IMPRIMIR RELAT�RIO DE PEDIDOS QUE N�O ENTRAM EM ROMANEIO E ROMANEIO N�O CONCLU�DO')=False)
     Then exit;

     Try
        objpedidoobjeto:=TobjPedidoObjetos.create(self);
     Except
           MensagemErro('Erro na tentativa de Criar o Objeto de Pedido de Objeto');
           exit;
     End;

     Try
        objpedidoobjeto.Relatorio_Pedidos_nao_entraram_romaneio_e_nao_concluidos;
     Finally
            objpedidoobjeto.free;
     End;
end;

procedure TFprincipal.Pedidosquenoforamentregues1Click(Sender: TObject);
var
   objpedidoobjeto:TobjPedidoObjetos;
begin

    if (ObjIconesglobal.VerificaClick('Pedidosquenoforamentregues1Click'))
    then exit;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('IMPRIMIR RELAT�RIO DE PEDIDOS QUE N�O ENTRAM EM ROMANEIO E ROMANEIO N�O CONCLU�DO')=False)
     Then exit;

     Try
        objpedidoobjeto:=TobjPedidoObjetos.create(self);
     Except
           MensagemErro('Erro na tentativa de Criar o Objeto de Pedido de Objeto');
           exit;
     End;

     Try
        objpedidoobjeto.Relatorio_Pedidos_nao_entregues;
     Finally
            objpedidoobjeto.free;
     End;
end;

procedure TFprincipal.MdulodeAjuda1Click(Sender: TObject);
var
   FcadastroAjuda:TFCadastroAjuda;
begin
      if (ObjIconesglobal.VerificaClick('MdulodeAjuda1Click'))
      then exit;


       FcadastroAjuda:=TFCadastroAjuda.Create(nil);

       try
            FcadastroAjuda.ShowModal;
       finally
            FreeAndNil(FcadastroAjuda);
       end;

end;

procedure TFprincipal.lb5MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
       TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFprincipal.lb5MouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFprincipal.lb4Click(Sender: TObject);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    try
      formPesquisaMenu:=TfPesquisaMenu.Create(nil);
      formPesquisaMenu.submit_formulario(self);
      formPesquisaMenu.ShowModal;

    finally
      FreeAndNil(formPesquisaMenu);
    end;
end;

procedure TFprincipal.lb5Click(Sender: TObject);
begin

       FAjuda.PassaAjuda('');
       FAjuda.ShowModal;
end;

procedure TFprincipal.OrdemdeInstalao1Click(Sender: TObject);
var
   FordemInstalacao:TFAgendaInstalacao;
begin
      if (ObjIconesglobal.VerificaClick('OrdemdeInstalao1Click'))
      then exit;

      FordemInstalacao:=TFAgendaInstalacao.Create(nil);


      try
            FordemInstalacao.ShowModal;
      finally
            FreeAndNil(FordemInstalacao);
      end;

end;

procedure TFprincipal.VendaRpida1Click(Sender: TObject);
var
   FvendaRapida:TFVendaRapida;
begin
      if (ObjIconesglobal.VerificaClick('VendaRpida1Click'))
      then exit;

      FvendaRapida:=TFVendaRapida.Create(nil);


      try
            FvendaRapida.ShowModal;
      finally
            FreeAndNil(FvendaRapida);
      end;

end;

procedure TFprincipal.Ajuda2Click(Sender: TObject);
begin  
       FAjuda.PassaAjuda('');
       FAjuda.ShowModal;
end;

procedure TFprincipal.lb1Click(Sender: TObject);
var
   FvendaRapida:TFVendaRapida;
begin
      FvendaRapida:=TFVendaRapida.Create(nil);


      try
            FvendaRapida.ShowModal;
      finally
            FreeAndNil(FvendaRapida);
      end;

end;

procedure TFprincipal.imgLogoClick(Sender: TObject);
begin
     // Este edit esta escondido, ele serve para que o key down do form funcione
     // isto se da necessario porque o componente webbrowser anula o evento on keydown do form
     //*-*  Medina
     edt1.SetFocus;
end;

procedure TFprincipal.Contador1Click(Sender: TObject);
var
  FContador:TFCONTADOR;
begin
      FContador:=TFCONTADOR.Create(nil);
      try
            FContador.ShowModal;
      finally
            FreeAndNil(FContador);
      end;

end;

procedure TFprincipal.CRTCdigodeRegimeTributrio1Click(Sender: TObject);
begin  
     if (ObjIconesglobal.VerificaClick('CadastrodoCRT1Click'))
     then exit;

     chamaFormulario(TFCRT,self);
end;

procedure TFprincipal.CSOSNCdigodeSituaodaOperaonoSimplesNacional1Click(
  Sender: TObject);
begin  
   if (ObjIconesglobal.VerificaClick('CadastrodoCRT1Click'))
   then exit;

   chamaFormulario(TFCSOSN,self);
end;

procedure TFprincipal.CSTACdigoTabelaAOrigemMercadoria1Click(
  Sender: TObject);
begin
   if (ObjIconesglobal.VerificaClick('CSTACdigoTabelaAOrigemMercadoria1Click'))
   then exit;

   chamaFormulario(TFTABELAA_ST,self);
end;

procedure TFprincipal.CTSBCdigoTabelaB1Click(Sender: TObject);
begin
   if (ObjIconesglobal.VerificaClick('CSTACdigoTabelaAOrigemMercadoria1Click'))
   then exit;

   chamaFormulario(TFTABELAB_ST,self);
end;

procedure TFprincipal.SPEDFiscal1Click(Sender: TObject);
begin
      if (utilizaSpedFiscal) then
      begin

        if (ObjIconesglobal.VerificaClick('SpedFiscal1Click')) Then
          exit;

        chamaFormulario(TfSpedFiscal,self);

      end
      else
       MensagemAviso ('Par�metro "UTILIZA SPED FISCAL" n�o esta habilitado');
end;

procedure TFprincipal.UnidadedeMedida1Click(Sender: TObject);
begin
   if (ObjIconesglobal.VerificaClick('UnidadedeMedida1Click'))
   then exit;

   chamaFormulario(TFUNIDADEMEDIDA,self);
end;

procedure TFprincipal.AcertodeUnidades1Click(Sender: TObject);
begin
   if (ObjIconesglobal.VerificaClick('AcertodeUnidades1Click'))
   then exit;

    chamaFormulario(TFAcertaUnidades,self);
end;

procedure TFprincipal.imgAjudaClick(Sender: TObject);
begin
    FAjuda.PassaAjuda('');
    FAjuda.ShowModal;
end;

procedure TFprincipal.AjudaFaceClick(Sender: TObject);
var
  Link:string;
begin
   Link:='http://www.facebook.com/sabvidros';
   ShellExecute(Application.Handle, nil, PChar(Link), nil, nil, sw_hide);
end;


procedure TFprincipal.imgVideosClick(Sender: TObject);
var
  Link:string;
begin
   Link:='http://www.youtube.com/user/SABExclaim?feature=mhee';
   ShellExecute(Application.Handle, nil, PChar(Link), nil, nil, sw_hide);
end;

procedure TFprincipal.OrdemdeProduo1Click(Sender: TObject);
var
  FordemProducao:TFProducao;
begin
  if (ObjIconesglobal.VerificaClick('OrdemdeProduo1Click'))
  then exit;

  FordemProducao:=TFProducao.Create(nil);

  try
    FordemProducao.ShowModal;
  finally
    FreeAndNil(FordemProducao);
  end;
end;

procedure TFprincipal.ProjetosemProduo1Click(Sender: TObject);
begin
   if (ObjIconesglobal.VerificaClick('ProjetosemProduo1Click'))
   then exit;
   FvisualizaStatusProducao.showmodal
end;

procedure TFprincipal.Cartacorreoeletrnica1Click(Sender: TObject);
begin

  if (ObjIconesglobal.VerificaClick('Cartacorreoeletrnica1Click')) then
    exit;
  chamaFormulario(TfCartaCorrecao,self);

end;

procedure TFprincipal.ConfiguraoWeb1Click(Sender: TObject);
begin
  if (ObjIconesglobal.VerificaClick('ConfiguraoWeb1Click')) then
    exit;
  chamaFormulario(TfConfiguracaoSite,self);
end;

procedure TFprincipal.AtualizaTributospeloNCM1Click(Sender: TObject);
var
  objNCM: TObjNCM;
  opendialog: TOpenDialog;
  ObjKitBox: TObjKITBOX;
  ObjDiverso: Tobjdiverso;
  ObjProjeto: TObjPROJETO;
begin
  objNCM := TObjNCM.Create(self);
  opendialog := TOpenDialog.Create(nil);

  ObjKitBox:=TObjKITBOX.Create;
  ObjDiverso:=TObjDIVERSO.Create;
  ObjProjeto := TObjPROJETO.Create;

  try
    opendialog.Filter := 'Text files (*.txt)|*.txt|Comma Separated Values (*.csv)|*.csv';
    if not(opendialog.Execute) then
      Exit;

    if not(FileExists(opendialog.FileName)) then
    begin
      MensagemErro('Arquivo escolhido inv�lido');
      exit;
    end;

    //importando o txt com os percentuais atualizados
    if not(objNCM.importaImpostos(opendialog.FileName)) then
      Exit;

    //importou ent�o agora vou atualizar os produtos
    //AtualizaTributospeloNCM;
    ObjFERRAGEM.AtualizaTributospeloNCM;
    ObjPERFILADO.AtualizaTributospeloNCM;
    ObjVIDRO.AtualizaTributospeloNCM;
    ObjKitBox.AtualizaTributospeloNCM;
    ObjDiverso.AtualizaTributospeloNCM;
    ObjProjeto.AtualizaTributospeloNCM;

  finally
    objNCM.Free;
    if(opendialog <> nil) then
      FreeAndNil(opendialog);

    ObjKitBox.Free;
    ObjDiverso.Free;
    ObjProjeto.Free;
  end;


end;


procedure TFprincipal.DevoluoNFe1Click(Sender: TObject);
begin
  //ShowMessage('M�dulo em desenvolvimento');
  //Exit;
  chamaFormulario(TfDevolucaoNFE,nil);
end;

procedure TFprincipal.pnlPedidosonlineClick(Sender: TObject);
begin
    FPEDIDOONLINE.ShowModal;
    pnlPedidosonline.Visible:=False;
end;

procedure TFprincipal.lbl1Click(Sender: TObject);
begin
FPEDIDOONLINE.ShowModal;
pnlPedidosonline.Visible:=False;
end;

procedure TFprincipal.lbl2Click(Sender: TObject);
begin
FPEDIDOONLINE.ShowModal;
pnlPedidosonline.Visible:=False;
end;

procedure TFprincipal.tmrverificapedidoonlineTimer(Sender: TObject);
var
  qry:TIBQuery;
begin
  if(logado=False)
  then Exit;

  qry:=TIBQuery.Create(nil);
  qry.Database:=FDataModulo.IBDatabase;

  try
    qry.Close;
    qry.SQL.Clear;
    qry.sql.Add('select count(codigo) from tabpedidosonline where aprovado is null or aprovado='''' ');
    qry.Open;

    if(qry.Fields[0].AsInteger>0) then
    begin
       pnlPedidosonline.Visible:=True;
       lbl2.Caption:=qry.Fields[0].AsString;
    end
    else
    begin
      pnlPedidosonline.Visible:=False;
    end;

    //Repaint;
  finally
    FreeAndNil(qry);
  end;


end;



procedure TFprincipal.CEST1Click(Sender: TObject);
begin

  FCEST := TFCEST.Create(nil);
  FCEST.ShowModal;
  FreeAndNil(fcest);
  
end;

procedure TFprincipal.EstoqueMnimo1Click(Sender: TObject);
var
  ObjRelPersReportBuilder:TObjRELPERSREPORTBUILDER;
  pMaterial: string;
begin
  while True do
  begin
    limpaedit(FfiltroImp);
    with FfiltroImp do
    begin
      DesativaGrupos;
      Grupo06.Enabled := True;
      Combo2Grupo06.Enabled := false;
      LbGrupo06.Caption := 'Material';
      ComboGrupo06.Clear;
      ComboGrupo06.Items.Add('TODOS');
      ComboGrupo06.Items.Add('FERRAGEM');
      ComboGrupo06.Items.Add('VIDRO');
      ComboGrupo06.Items.Add('PERFILADO');
      ComboGrupo06.Items.Add('KITBOX');
      ComboGrupo06.Items.Add('DIVERSO');
      ComboGrupo06.Items.Add('PERSIANA');
      ShowModal;
      if( FfiltroImp.Tag = 0 ) then
        Break;
      pMaterial := '';
      if( ComboGrupo06.ItemIndex > 0 ) then
        pMaterial := UpperCase( ComboGrupo06.Text );
    end;

    try
      try
        ObjRelPersreportBuilder:=TObjRELPERSREPORTBUILDER.Create;
      except
        MensagemErro('Erro ao tentar criar o Objeto ObjRelPersreportBuilder');
        exit;
      end;

      if (ObjRelPersReportBuilder.LocalizaNOme('ESTOQUE MINIMO') = false) then
      begin
        MensagemErro('O Relatorio de ReporteBuilder "IMPRIME VIDROS NO ROMANEIO" n�o foi econtrado');
        exit;
      end;

      ObjRelPersReportBuilder.TabelaparaObjeto;
      ObjRelPersReportBuilder.SQLCabecalhoPreenchido:=StrReplace(ObjRelPersReportBuilder.Get_SQLCabecalho,':PUSUARIO',ObjUsuarioGlobal.Get_nome);
      ObjRelPersReportBuilder.SQLRepeticaoPreenchido:=StrReplace(ObjRelPersReportBuilder.get_SQLRepeticao,':PMATERIAL',pMaterial);
      ObjRelPersReportBuilder.ChamaRelatorio(false);
    finally
      ObjRelPersreportBuilder.Free;
    end;
  end;
end;

procedure TFprincipal.EqualizaEstoque1Click(Sender: TObject);
begin
  if( MensagemPergunta('Deseja equalizar o estoque de todos os materiais?')=mrno ) then
    exit;
  OBJESTOQUEGLOBAL.AtualizaEstoqueCorMateriais;
end;


procedure tfprincipal.checaEqualizaEstoque;
begin
  if( Trim( get_campoTabela('estoqueequalizado','codigo','tabempresa','1') ) = '' ) then
  begin
    if OBJESTOQUEGLOBAL.AtualizaEstoqueCorMateriais then
    begin
      exec_sql( 'update tabempresa set estoqueequalizado = ' + QuotedStr( FormatDateTime('dd.mm.yyyy hh:mm:ss',now) ) + ' where codigo=1' );
      FDataModulo.IBTransaction.Commit;
    end;
  end;
end;

end.


