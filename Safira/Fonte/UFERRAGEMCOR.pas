unit UFERRAGEMCOR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjFERRAGEMCOR,
  jpeg, UessencialGlobal, Tabs;

type
  TFFERRAGEMCOR = class(TForm)
    btNovo: TSpeedButton;
    btFechar: TSpeedButton;
    btMinimizar: TSpeedButton;
    btSalvar: TSpeedButton;
    btAlterar: TSpeedButton;
    btCancelar: TSpeedButton;
    btSair: TSpeedButton;
    btRelatorio: TSpeedButton;
    btExcluir: TSpeedButton;
    btPesquisar: TSpeedButton;
    lbAjuda: TLabel;
    btAjuda: TSpeedButton;
    PainelExtra: TPanel;
    ImageGuiaPrincipal: TImage;
    ImageGuiaExtra: TImage;
    PainelPrincipal: TPanel;
    ImagePainelPrincipal: TImage;
    ImagePainelExtra: TImage;
    Guia: TTabSet;
    Notebook: TNotebook;
    Bevel1: TBevel;
    LbCodigo: TLabel;
    LbFerragem: TLabel;
    LbCor: TLabel;
    LbPorcentagemAcrescimo: TLabel;
    LbEstoque: TLabel;
    LbAcrescimoExtra: TLabel;
    LbPorcentagemAcrescimoFinal: TLabel;
    EdtPorcentagemAcrescimoFinal: TEdit;
    EdtAcrescimoExtra: TEdit;
    EdtEstoque: TEdit;
    EdtPorcentagemAcrescimo: TEdit;
    EdtCor: TEdit;
    EdtFerragem: TEdit;
    EdtCodigo: TEdit;
    LbNomeFerragem: TLabel;
    LbNomeCor: TLabel;
    procedure edtFerragemKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtFerragemExit(Sender: TObject);
    procedure edtCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtCorExit(Sender: TObject);
//DECLARA COMPONENTES

    procedure FormCreate(Sender: TObject);
    Procedure PosicionaPaineis;
    Procedure PegaFiguras;
    Procedure ColocaAtalhoBotoes;
    Procedure PosicionaForm;
    procedure ImageGuiaPrincipalClick(Sender: TObject);
    procedure ImageGuiaExtraClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GuiaClick(Sender: TObject);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFERRAGEMCOR: TFFERRAGEMCOR;
  ObjFERRAGEMCOR:TObjFERRAGEMCOR;

implementation

uses Upesquisa;

{$R *.dfm}


procedure TFFERRAGEMCOR.FormCreate(Sender: TObject);
var
  Points: array [0..15] of TPoint;
  Regiao1:HRgn;
begin

Points[0].X :=305;      Points[0].Y := 22;
Points[1].X :=286;      Points[1].Y := 3;
Points[2].X :=41;       Points[2].Y := 3;
Points[3].X :=41;       Points[3].Y := 22;
Points[4].X :=28;       Points[4].Y := 35;
Points[5].X :=28;       Points[5].Y := 164;
Points[6].X :=41;       Points[6].Y := 177;
Points[7].X :=41;       Points[7].Y := 360;
Points[8].X :=62;       Points[8].Y := 381;
Points[9].X :=632;      Points[9].Y := 381;
Points[10].X :=647;     Points[10].Y := 396;
Points[11].X :=764;     Points[11].Y := 396;
Points[12].X :=764;     Points[12].Y := 95;
Points[13].X :=780;     Points[13].Y := 79;
Points[14].X :=780;     Points[14].Y := 22;
Points[15].X :=305;     Points[15].Y := 22;

Regiao1:=CreatePolygonRgn(Points, 16, WINDING);
SetWindowRgn(Self.Handle, regiao1, False);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.TabIndex:=0;
     Notebook.PageIndex:=0;

     Try
        ObjFERRAGEMCOR:=TObjFERRAGEMCOR.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     PegaCorForm(Self);
     Self.PosicionaPaineis;
     Self.PosicionaForm;
     Self.PegaFiguras;
     Self.ColocaAtalhoBotoes;
end;

procedure TFFERRAGEMCOR.PosicionaPaineis;
begin
  With Self.PainelPrincipal do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelPrincipal do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  With Self.PainelExtra  do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelExtra  do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  Self.PainelPrincipal.Enabled:=true;
  Self.PainelPrincipal.Visible:=true;
  Self.PainelExtra.Enabled:=false;
  Self.PainelExtra.Visible:=false;
end;

procedure TFFERRAGEMCOR.ImageGuiaPrincipalClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=true;
PainelPrincipal.Visible:=true;
PainelExtra.Enabled:=false;
PainelExtra.Visible:=false;
end;

procedure TFFERRAGEMCOR.ImageGuiaExtraClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=false;
PainelPrincipal.Visible:=false;
PainelExtra.Enabled:=true;
PainelExtra.Visible:=true;

end;

procedure TFFERRAGEMCOR.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjFERRAGEMCOR=Nil)
     Then exit;

     If (ObjFERRAGEMCOR.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjFERRAGEMCOR.free;
    Action := caFree;
    Self := nil;
end;

procedure TFFERRAGEMCOR.PegaFiguras;
begin
    PegaFigura(ImageGuiaPrincipal,'guia_principal.jpg');
    PegaFigura(ImageGuiaExtra,'guia_extra.jpg');
    PegaFigura(ImagePainelPrincipal,'fundo_menu.jpg');
    PegaFigura(ImagePainelExtra,'fundo_menu.jpg');
    PegaFiguraBotao(btNovo,'botao_novo.bmp');
    PegaFiguraBotao(btSalvar,'botao_salvar.bmp');
    PegaFiguraBotao(btAlterar,'botao_alterar.bmp');
    PegaFiguraBotao(btCancelar,'botao_Cancelar.bmp');
    PegaFiguraBotao(btPesquisar,'botao_Pesquisar.bmp');
    PegaFiguraBotao(btExcluir,'botao_excluir.bmp');
    PegaFiguraBotao(btRelatorio,'botao_relatorios.bmp');
    PegaFiguraBotao(btSair,'botao_sair.bmp');
    PegaFiguraBotao(btFechar,'botao_close.bmp');
    PegaFiguraBotao(btMinimizar,'botao_minimizar.bmp');
    PegaFiguraBotao(btAjuda,'lampada.bmp');

end;

procedure TFFERRAGEMCOR.ColocaAtalhoBotoes;
begin
   Coloca_Atalho_Botoes(BtNovo, 'N');
   Coloca_Atalho_Botoes(BtSalvar, 'S');
   Coloca_Atalho_Botoes(BtAlterar, 'A');
   Coloca_Atalho_Botoes(BtCancelar, 'C');
   Coloca_Atalho_Botoes(BtExcluir, 'E');
   Coloca_Atalho_Botoes(BtPesquisar, 'P');
   Coloca_Atalho_Botoes(BtRelatorio, 'R');
   Coloca_Atalho_Botoes(BtSair, 'I');
end;

procedure TFFERRAGEMCOR.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjFERRAGEMCOR.Get_novocodigo;
     edtcodigo.enabled:=False;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjFERRAGEMCOR.status:=dsInsert;
     Guia.TabIndex:=0;
     Notebook.PageIndex:=0;

     EdtFerragem.setfocus;

end;

procedure TFFERRAGEMCOR.btSalvarClick(Sender: TObject);
begin

     If ObjFERRAGEMCOR.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjFERRAGEMCOR.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjFERRAGEMCOR.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFFERRAGEMCOR.btAlterarClick(Sender: TObject);
begin
    If (ObjFERRAGEMCOR.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjFERRAGEMCOR.Status:=dsEdit;
                guia.TabIndex:=0;
                EdtFerragem.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;

end;

procedure TFFERRAGEMCOR.btCancelarClick(Sender: TObject);
begin
     ObjFERRAGEMCOR.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFFERRAGEMCOR.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjFERRAGEMCOR.Get_pesquisa,ObjFERRAGEMCOR.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjFERRAGEMCOR.status<>dsinactive
                                  then exit;

                                  If (ObjFERRAGEMCOR.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjFERRAGEMCOR.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFFERRAGEMCOR.btExcluirClick(Sender: TObject);
begin
     If (ObjFERRAGEMCOR.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjFERRAGEMCOR.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjFERRAGEMCOR.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFFERRAGEMCOR.btRelatorioClick(Sender: TObject);
begin
    ObjFERRAGEMCOR.Imprime(edtCodigo.text);
end;

procedure TFFERRAGEMCOR.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFFERRAGEMCOR.PosicionaForm;
begin
    Self.Caption:='      '+Self.Caption;
    Self.Left:=-18;
    Self.Top:=0;
end;
procedure TFFERRAGEMCOR.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFFERRAGEMCOR.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFFERRAGEMCOR.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
end;

procedure TFFERRAGEMCOR.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFFERRAGEMCOR.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjFERRAGEMCOR do
    Begin
        Submit_Codigo(edtCodigo.text);
        Ferragem.Submit_codigo(edtFerragem.text);
        Cor.Submit_codigo(edtCor.text);
        Submit_Estoque(edtEstoque.text);
        Submit_AcrescimoExtra(edtAcrescimoExtra.text);
        result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFFERRAGEMCOR.LimpaLabels;
begin
//LIMPA LABELS
end;

function TFFERRAGEMCOR.ObjetoParaControles: Boolean;
begin
  Try
     With ObjFERRAGEMCOR do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtFerragem.text:=Ferragem.Get_codigo;
        EdtCor.text:=Cor.Get_codigo;
        EdtPorcentagemAcrescimo.text:=Get_PorcentagemAcrescimo;
        EdtEstoque.text:=Get_Estoque;
        EdtAcrescimoExtra.text:=Get_AcrescimoExtra;
        EdtPorcentagemAcrescimoFinal.text:=Get_PorcentagemAcrescimoFinal;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFFERRAGEMCOR.TabelaParaControles: Boolean;
begin
     If (ObjFERRAGEMCOR.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFFERRAGEMCOR.GuiaClick(Sender: TObject);
begin
Notebook.PageIndex:=Guia.TabIndex;
end;

procedure TFFERRAGEMCOR.edtCorExit(Sender: TObject);
begin
    ObjFERRAGEMCOR.EdtCorExit(Sender, LbNomeCor);
end;

procedure TFFERRAGEMCOR.edtCorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    ObjFERRAGEMCOR.EdtCorKeyDown(Sender, Key, Shift, LbNomeCor);
end;

procedure TFFERRAGEMCOR.edtFerragemExit(Sender: TObject);
begin
    ObjFERRAGEMCOR.EdtFerragemExit(Sender, LbNomeFerragem);
end;

procedure TFFERRAGEMCOR.edtFerragemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    ObjFERRAGEMCOR.EdtCorKeyDown(Sender, key, Shift, LbNomeFerragem);
end;

end.
