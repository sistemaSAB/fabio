object FCONTADOR: TFCONTADOR
  Left = 553
  Top = 286
  Width = 849
  Height = 416
  Caption = 'Cadastro de CONTADOR - EXCLAIM TECNOLOGIA'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LbCodigo: TLabel
    Left = 5
    Top = 65
    Width = 39
    Height = 14
    BiDiMode = bdLeftToRight
    Caption = 'C'#243'digo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    Transparent = True
  end
  object LbNOME: TLabel
    Left = 5
    Top = 92
    Width = 32
    Height = 14
    BiDiMode = bdLeftToRight
    Caption = 'Nome'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    Transparent = True
  end
  object LbCPF: TLabel
    Left = 5
    Top = 116
    Width = 21
    Height = 14
    BiDiMode = bdLeftToRight
    Caption = 'CPF'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    Transparent = True
  end
  object LbCRC: TLabel
    Left = 286
    Top = 116
    Width = 23
    Height = 14
    Caption = 'CRC'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object LbCNPJ: TLabel
    Left = 519
    Top = 116
    Width = 28
    Height = 14
    Caption = 'CNPJ'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object LbCEP: TLabel
    Left = 5
    Top = 138
    Width = 21
    Height = 14
    BiDiMode = bdLeftToRight
    Caption = 'CEP'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    Transparent = True
  end
  object LbENDERECO: TLabel
    Left = 5
    Top = 162
    Width = 52
    Height = 14
    BiDiMode = bdLeftToRight
    Caption = 'Endere'#231'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    Transparent = True
  end
  object LbNUMERO: TLabel
    Left = 519
    Top = 162
    Width = 44
    Height = 14
    Caption = 'N'#250'mero'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object LbCOMPLEMENTO: TLabel
    Left = 5
    Top = 186
    Width = 79
    Height = 14
    BiDiMode = bdLeftToRight
    Caption = 'Complemento'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    Transparent = True
  end
  object LbBAIRRO: TLabel
    Left = 519
    Top = 186
    Width = 33
    Height = 14
    Caption = 'Bairro'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object LbFONE: TLabel
    Left = 5
    Top = 211
    Width = 27
    Height = 14
    BiDiMode = bdLeftToRight
    Caption = 'Fone'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    Transparent = True
  end
  object LbFAX: TLabel
    Left = 5
    Top = 235
    Width = 20
    Height = 14
    BiDiMode = bdLeftToRight
    Caption = 'FAX'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    Transparent = True
  end
  object LbEMAIL: TLabel
    Left = 5
    Top = 259
    Width = 29
    Height = 14
    BiDiMode = bdLeftToRight
    Caption = 'Email'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    Transparent = True
  end
  object LbCIDADE: TLabel
    Left = 5
    Top = 283
    Width = 38
    Height = 14
    BiDiMode = bdLeftToRight
    Caption = 'Cidade'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    Transparent = True
  end
  object LbNomeCIDADE: TLabel
    Left = 189
    Top = 283
    Width = 39
    Height = 14
    Caption = 'CIDADE'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 833
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    DesignSize = (
      833
      50)
    object lbnomeformulario: TLabel
      Left = 488
      Top = 8
      Width = 331
      Height = 32
      Alignment = taRightJustify
      Anchors = []
      Caption = 'Contador respons'#225'vel'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -27
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Btnovo: TBitBtn
      Left = 3
      Top = -3
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btpesquisar: TBitBtn
      Left = 253
      Top = -3
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 303
      Top = -3
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
    end
    object btalterar: TBitBtn
      Left = 53
      Top = -3
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btexcluir: TBitBtn
      Left = 203
      Top = -3
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btgravar: TBitBtn
      Left = 103
      Top = -3
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 153
      Top = -3
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btsair: TBitBtn
      Left = 403
      Top = -3
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
    object btopcoes: TBitBtn
      Left = 353
      Top = -3
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
  end
  object EdtCodigo: TEdit
    Left = 109
    Top = 66
    Width = 70
    Height = 19
    MaxLength = 9
    TabOrder = 1
  end
  object EdtNOME: TEdit
    Left = 109
    Top = 90
    Width = 612
    Height = 19
    Hint = 'Nome do contabilista.'
    MaxLength = 150
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
  end
  object EdtCRC: TEdit
    Left = 326
    Top = 114
    Width = 179
    Height = 19
    Hint = 
      'N'#250'mero de inscri'#231#227'o do contabilista no Conselho Regional de Cont' +
      'abilidade'
    MaxLength = 15
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
  end
  object EdtCEP: TEdit
    Left = 109
    Top = 135
    Width = 156
    Height = 19
    Hint = 'C'#243'digo de Endere'#231'amento Postal'
    MaxLength = 10
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
  end
  object EdtENDERECO: TEdit
    Left = 109
    Top = 159
    Width = 396
    Height = 19
    Hint = 'Logradouro e endere'#231'o do im'#243'vel.'
    MaxLength = 200
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
  end
  object EdtNUMERO: TEdit
    Left = 578
    Top = 159
    Width = 70
    Height = 19
    Hint = 'N'#250'mero do im'#243'vel.'
    MaxLength = 9
    ParentShowHint = False
    ShowHint = True
    TabOrder = 6
  end
  object EdtCOMPLEMENTO: TEdit
    Left = 109
    Top = 183
    Width = 395
    Height = 19
    Hint = 'Dados complementares do endere'#231'o.'
    MaxLength = 60
    ParentShowHint = False
    ShowHint = True
    TabOrder = 7
  end
  object EdtBAIRRO: TEdit
    Left = 578
    Top = 183
    Width = 143
    Height = 19
    Hint = 'Bairro em que o im'#243'vel est'#225' situado.'
    MaxLength = 60
    ParentShowHint = False
    ShowHint = True
    TabOrder = 8
  end
  object EdtEMAIL: TEdit
    Left = 109
    Top = 256
    Width = 395
    Height = 19
    Hint = 'Endere'#231'o do correio eletr'#244'nico.'
    MaxLength = 60
    ParentShowHint = False
    ShowHint = True
    TabOrder = 9
  end
  object EdtCIDADE: TEdit
    Left = 109
    Top = 280
    Width = 70
    Height = 19
    Color = 6073854
    MaxLength = 9
    TabOrder = 10
    OnExit = edtCIDADEExit
    OnKeyDown = edtCIDADEKeyDown
  end
  object panelrodape: TPanel
    Left = 0
    Top = 328
    Width = 833
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 11
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 833
      Height = 50
      Align = alClient
      Stretch = True
    end
  end
  object edtCPF: TMaskEdit
    Left = 109
    Top = 114
    Width = 156
    Height = 19
    Hint = 'N'#250'mero de inscri'#231#227'o do contabilista no CPF.'
    EditMask = '000\.000\.000\-00;0;_'
    MaxLength = 14
    ParentShowHint = False
    ShowHint = True
    TabOrder = 12
  end
  object edtCNPJ: TMaskEdit
    Left = 578
    Top = 114
    Width = 143
    Height = 19
    Hint = 
      'N'#250'mero de inscri'#231#227'o do escrit'#243'rio de contabilidade no CNPJ, se h' +
      'ouver.'
    EditMask = '00\.000\.000\/0000\-00;0;_'
    MaxLength = 18
    ParentShowHint = False
    ShowHint = True
    TabOrder = 13
  end
  object edtFone: TMaskEdit
    Left = 109
    Top = 208
    Width = 140
    Height = 19
    Hint = 'N'#250'mero do telefone (DDD+FONE).'
    EditMask = '(00) 0000-0000;0;_'
    MaxLength = 14
    ParentShowHint = False
    ShowHint = True
    TabOrder = 14
  end
  object EdtFAX: TMaskEdit
    Left = 109
    Top = 232
    Width = 140
    Height = 19
    Hint = 'N'#250'mero do fax.'
    EditMask = '(00) 0000-0000;0;_'
    MaxLength = 14
    ParentShowHint = False
    ShowHint = True
    TabOrder = 15
  end
end
