unit UData;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ExtCtrls, Buttons, jpeg;

type
  TFdata = class(TForm)
    edtdata: TMaskEdit;
    lbfrase: TLabel;
    btok: TBitBtn;
    btcancel: TBitBtn;
    Image1: TImage;
    btn1: TSpeedButton;
    procedure btcancelClick(Sender: TObject);
    procedure btokClick(Sender: TObject);
    procedure edtdataKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fdata: TFdata;

implementation

uses UessencialGlobal;


{$R *.DFM}

procedure TFdata.btcancelClick(Sender: TObject);
begin
     Self.ModalResult:=mrCancel;

end;

procedure TFdata.btokClick(Sender: TObject);
begin
     try
        Strtodate(edtdata.text);
        Self.ModalResult:=Mrok;
        Self.Tag:=1;
        Self.Close;
     Except

     End;
end;

procedure TFdata.edtdataKeyPress(Sender: TObject; var Key: Char);
begin
     If Key=#13
     Then btok.OnClick(sender)
     Else Begin
               If key=#27
               Then btcancel.OnClick(sender);
          End;

end;

procedure TFdata.FormActivate(Sender: TObject);
begin
     Self.Tag:=0;
     edtdata.SetFocus;
end;

procedure TFdata.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;

end.
