unit URUA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjRUA,
  UessencialGlobal, UessencialLocal, Tabs,IBQuery;

type
  TFRUA = class(TForm)
    lbLbNome: TLabel;
    edtNome: TEdit;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbcodigo: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    imgrodape: TImage;
    lb14: TLabel;
//DECLARA COMPONENTES


    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    
  private
    ObjRUA:TObjRUA;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    procedure MostraQuantidadeCadastrada;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRUA: TFRUA;

implementation

uses Upesquisa, UDataModulo, UescolheImagemBotao;

{$R *.dfm}


procedure TFRUA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjRUA=Nil)
     Then exit;

     If (Self.ObjRUA.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjRUA.free;
  
end;


procedure TFRUA.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbcodigo.caption:='0';
     //edtcodigo.text:=ObjRUA.Get_novocodigo;

     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjRUA.status:=dsInsert;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

     EdtNome.setfocus;

end;

procedure TFRUA.btSalvarClick(Sender: TObject);
begin

     If ObjRUA.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjRUA.salvar(true)=False)
     Then exit;

     lbcodigo.caption:=ObjRUA.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;
     MostraQuantidadeCadastrada;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFRUA.btAlterarClick(Sender: TObject);
begin
    If (ObjRUA.Status=dsinactive) and (lbcodigo.caption<>'')
    Then
    Begin
                habilita_campos(Self);

                ObjRUA.Status:=dsEdit;

                edtNOme.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
               btnovo.Visible :=false;
               btalterar.Visible:=false;
               btpesquisar.Visible:=false;
               btrelatorio.Visible:=false;
               btexcluir.Visible:=false;
               btsair.Visible:=false;
               btopcoes.Visible :=false;
    End;

end;

procedure TFRUA.btCancelarClick(Sender: TObject);
begin
     ObjRUA.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;
     lbcodigo.Caption:='';

end;

procedure TFRUA.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjRUA.Get_pesquisa,ObjRUA.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjRUA.status<>dsinactive
                                  then exit;

                                  If (ObjRUA.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjRUA.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFRUA.btExcluirClick(Sender: TObject);
begin
     If (ObjRUA.status<>dsinactive) or (lbcodigo.caption='')
     Then exit;

     If (ObjRUA.LocalizaCodigo(lbcodigo.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjRUA.exclui(lbcodigo.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFRUA.btRelatorioClick(Sender: TObject);
begin
    ObjRUA.Imprime(lbcodigo.caption);
end;

procedure TFRUA.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFRUA.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFRUA.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFRUA.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;


end;

procedure TFRUA.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFRUA.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjRUA do
    Begin
        Submit_Codigo(lbcodigo.caption);
        Submit_Nome(edtNome.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFRUA.LimpaLabels;
begin
//LIMPA LABELS
end;

function TFRUA.ObjetoParaControles: Boolean;
begin
  Try
     With ObjRUA do
     Begin
        lbcodigo.caption:=Get_Codigo;
        EdtNome.text:=Get_Nome;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFRUA.TabelaParaControles: Boolean;
begin
     If (ObjRUA.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;


//CODIFICA ONKEYDOWN E ONEXIT

procedure TFRUA.MostraQuantidadeCadastrada;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabrua');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb14.Caption:='Existem '+IntToStr(contador)+' ruas cadastrados'
       else
       begin
            lb14.Caption:='Existe '+IntToStr(contador)+' rua cadastrado';
       end;

    finally

    end;


end;


procedure TFRUA.FormShow(Sender: TObject);
begin

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjRUA:=TObjRUA.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;


     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     lbcodigo.caption:='';

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE RUA')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);
      MostraQuantidadeCadastrada;
end;

end.
