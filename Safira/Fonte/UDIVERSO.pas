unit UDIVERSO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjDIVERSOCor,
  UessencialGlobal, Tabs, Grids, DBGrids,uobjdiverso_icms, ComCtrls,
  UFRImposto_ICMS,UCOR,UGRUPODIVERSO,UpesquisaMenu,IBQuery, UErrosDesativarMateriais,
  ImgList,ClipBrd;

type
  TFDIVERSO = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    btAjuda: TSpeedButton;
    Guia: TPageControl;
    tsPrincipal: TTabSheet;
    tsImposto: TTabSheet;
    tsCor: TTabSheet;
    panel2: TPanel;
    lbAtivoInativo: TLabel;
    chkAtivo: TCheckBox;
    EdtDescricao: TEdit;
    EdtReferencia: TEdit;
    LbReferencia: TLabel;
    LbDescricao: TLabel;
    EdtGrupoDiverso: TEdit;
    LbNomeGrupoDiverso: TLabel;
    lbFornecedor: TLabel;
    edtFornecedor: TEdit;
    lbLbFornecedor: TLabel;
    LbUnidade: TLabel;
    LbPrecoCusto: TLabel;
    lb2: TLabel;
    edtNCM: TEdit;
    EdtPrecoCusto: TEdit;
    EdtUnidade: TEdit;
    GroupBoxMargem: TGroupBox;
    LbPorcentagemFornecido: TLabel;
    LbPorcentagemRetirado: TLabel;
    LbPorcentagemInstalado: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdtPorcentagemInstalado: TEdit;
    EdtPorcentagemFornecido: TEdit;
    EdtPorcentagemRetirado: TEdit;
    GroupBoxPrecoVenda: TGroupBox;
    LbPrecoVendaInstalado: TLabel;
    LbPrecoVendaFornecido: TLabel;
    lbLbPrecoVendaRetirado: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdtPrecoVendaInstalado: TEdit;
    EdtPrecoVendaFornecido: TEdit;
    EdtPrecoVendaRetirado: TEdit;
    GroupBoxSIMNAO: TGroupBox;
    RadioControlaMetroQuadradoSIM: TRadioButton;
    RadioControlaMetroQuadradoNAo: TRadioButton;
    SpeedButtonUltimo: TSpeedButton;
    SpeedButtonProximo: TSpeedButton;
    SpeedButtonAnterior: TSpeedButton;
    SpeedButtonPrimeiro: TSpeedButton;
    LbGrupoDiverso: TLabel;
    PanelCor: TPanel;
    LbCor: TLabel;
    lbnomecor: TLabel;
    Label8: TLabel;
    Label1: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    LbCusto: TLabel;
    Label15: TLabel;
    lbestoque: TLabel;
    EdtCorReferencia: TEdit;
    edtcodigoCOR: TEdit;
    GroupBoxLucro: TGroupBox;
    LbPorcentagemAcrescimo: TLabel;
    LbAcrescimoExtra: TLabel;
    LbPorcentagemAcrescimoFinal: TLabel;
    EdtPorcentagemAcrescimo: TEdit;
    EdtAcrescimoExtra: TEdit;
    EdtPorcentagemAcrescimoFinal: TEdit;
    EdtCor: TEdit;
    edtinstaladofinal: TEdit;
    edtfornecidofinal: TEdit;
    edtretiradofinal: TEdit;
    Rg_forma_de_calculo_percentual: TRadioGroup;
    edtclassificacaofiscal_cor: TEdit;
    pnl2: TPanel;
    btgravarcor: TBitBtn;
    btexcluircor: TBitBtn;
    btcancelacor: TBitBtn;
    DBGRIDCOR: TDBGrid;
    Panel1: TPanel;
    LbClassificaoFiscal: TLabel;
    EdtClassificacaoFiscal: TEdit;
    PanelICMSForaEstado: TPanel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    EdtAliquota_ICMS_ForaEstado: TEdit;
    EdtReducao_BC_ICMS_ForaEstado: TEdit;
    EdtAliquota_ICMS_Cupom_ForaEstado: TEdit;
    comboisentoicms_foraestado: TComboBox;
    comboSUBSTITUICAOICMS_FORAESTADO: TComboBox;
    edtVALORPAUTA_SUB_TRIB_FORAESTADO: TEdit;
    PanelICMSEstado: TPanel;
    LbAliquota_ICMS_Estado: TLabel;
    LbReducao_BC_ICMS_Estado: TLabel;
    LbAliquota_ICMS_Cupom_Estado: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    EdtAliquota_ICMS_Estado: TEdit;
    EdtReducao_BC_ICMS_Estado: TEdit;
    EdtAliquota_ICMS_Cupom_Estado: TEdit;
    comboisentoicms_estado: TComboBox;
    comboSUBSTITUICAOICMS_ESTADO: TComboBox;
    edtVALORPAUTA_SUB_TRIB_ESTADO: TEdit;
    EdtSituacaoTributaria_TabelaA: TEdit;
    LbSituacaoTributaria_TabelaA: TLabel;
    EdtSituacaoTributaria_TabelaB: TEdit;
    edtpercentualagregado: TEdit;
    Label16: TLabel;
    FRImposto_ICMS1: TFRImposto_ICMS;
    il1: TImageList;
    Label9: TLabel;
    edtCest: TEdit;
    Label11: TLabel;
    edtEstoqueMinimo: TEdit;
    procedure edtGrupoDiversoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtGrupoDiversoExit(Sender: TObject);
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtPorcentagemInstaladoExit(Sender: TObject);
    procedure EdtPorcentagemFornecidoExit(Sender: TObject);
    procedure EdtPorcentagemRetiradoExit(Sender: TObject);
    procedure btGravarCorClick(Sender: TObject);
    procedure BtExcluirCorClick(Sender: TObject);
    procedure BtCancelarCorClick(Sender: TObject);
{    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);    }
    procedure EdtCorReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtCorReferenciaExit(Sender: TObject);
    procedure DBGRIDCORDblClick(Sender: TObject);
    procedure EdtAcrescimoExtraExit(Sender: TObject);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure EdtPrecoCustoExit(Sender: TObject);
    procedure EdtSituacaoTributaria_TabelaAExit(Sender: TObject);
    procedure EdtSituacaoTributaria_TabelaAKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
    procedure EdtSituacaoTributaria_TabelaBExit(Sender: TObject);
    procedure EdtSituacaoTributaria_TabelaBKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
    procedure EdtPrecoVendaInstaladoExit(Sender: TObject);
    procedure EdtPrecoVendaFornecidoExit(Sender: TObject);
    procedure EdtPrecoVendaRetiradoExit(Sender: TObject);
    procedure DBGRIDCORKeyPress(Sender: TObject; var Key: Char);
    procedure DBGRIDCORDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure EdtCorReferenciaDblClick(Sender: TObject);
    procedure EdtGrupoDiversoDblClick(Sender: TObject);
    procedure edtNCMKeyPress(Sender: TObject; var Key: Char);
    procedure lbestoqueMouseLeave(Sender: TObject);
    procedure lbestoqueMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbestoqueClick(Sender: TObject);
    procedure edtquant(Sender: TObject;
  var Key: Char);
    procedure chkAtivoClick(Sender: TObject);
    procedure btAjudaClick(Sender: TObject);
    procedure LbNomeGrupoDiversoClick(Sender: TObject);
    procedure LbNomeGrupoDiversoMouseLeave(Sender: TObject);
    procedure LbNomeGrupoDiversoMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure lbnomecorClick(Sender: TObject);
    procedure EdtPrecoCustoKeyPress(Sender: TObject; var Key: Char);
    procedure EdtPorcentagemInstaladoKeyPress(Sender: TObject;
      var Key: Char);
    procedure EdtPorcentagemFornecidoKeyPress(Sender: TObject;
      var Key: Char);
    procedure EdtPorcentagemRetiradoKeyPress(Sender: TObject;
      var Key: Char);
    procedure EdtGrupoDiversoKeyPress(Sender: TObject; var Key: Char);
    procedure EdtPorcentagemAcrescimoKeyPress(Sender: TObject;
      var Key: Char);
    procedure EdtAcrescimoExtraKeyPress(Sender: TObject; var Key: Char);
    procedure EdtPorcentagemAcrescimoFinalKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtFornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtFornecedorExit(Sender: TObject);
    procedure GuiaChange(Sender: TObject);
    procedure edtNCMKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FRImposto_ICMS1BtGravar_material_icmsClick(Sender: TObject);
    procedure edtCestKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    ObjDIVERSOCor:TObjDIVERSOCor;
    AlteraPrecoPeloCusto:boolean;
    PAlteraCampoEstoque:boolean;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    Procedure PreparaCor;
    Procedure ResgataCores;
  private
    EstadoObjeto:string;

    { Private declarations }
  public
    procedure AbreComCodigo(parametro:string);
  end;

var
  FDIVERSO: TFDIVERSO;


implementation

uses Upesquisa, UessencialLocal, UobjCOR, UDataModulo, UobjDIVERSO,
  UobjGRUPODIVERSO, UescolheImagemBotao, Uprincipal, UFiltraImp, UAjuda,
  UobjCEST;

{$R *.dfm}


procedure TFDIVERSO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjDIVERSOCor.Diverso=Nil)
     Then exit;

     If (Self.ObjDiversoCor.Diverso.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Try
        Self.ObjDiversoCor.free;
    Except
          on e:exception do
          Begin
               mensagemerro(e.message);
          End;
    End;

    try
      FRImposto_ICMS1.ObjMaterial_ICMS.Free;
    Except
          on e:exception do
          Begin
               mensagemerro(e.message);
          End;
    End;
    tag:=0;
end;


procedure TFDIVERSO.btNovoClick(Sender: TObject);
begin
     Self.limpaLabels;
     limpaedit(Self);

     chkAtivo.Checked:=True;
     lbAtivoInativo.Caption:='Ativo';
     habilita_campos(Self);
     habilita_campos(GroupBoxSIMNAO);
     habilita_campos(GroupBoxMargem);
     RadioControlaMetroQuadradoSIM.Checked:=false;
     RadioControlaMetroQuadradoNAo.Checked:=false;
     desab_botoes(Self);

     lbCodigo.Caption:='0';


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjDiversoCor.Diverso.status:=dsInsert;
     Guia.TabIndex:=0;
     Guia.TabIndex:=0;
     EdtReferencia.setfocus;

     //desabilita_campos(GroupBoxPrecoVenda);

      if (AlteraPrecoPeloCusto=True)
      Then Begin
                EdtPrecoVendaInstalado.Enabled:=false;
                EdtPrecoVendaRetirado.Enabled:=false;
                EdtPrecoVendaFornecido.Enabled:=false;
      End;


     Self.EstadoObjeto:='INSER��O';
     RadioControlaMetroQuadradoSIM.Checked:=true;
     EdtSituacaoTributaria_TabelaA.Text:='1';
     EdtSituacaoTributaria_TabelaB.Text:='1';
     EdtClassificacaoFiscal.Text:='1';

     comboisentoicms_estado.Text:='N�O';
     comboSUBSTITUICAOICMS_ESTADO.Text:='SIM';
     edtVALORPAUTA_SUB_TRIB_ESTADO.Text:='0';
     EdtAliquota_ICMS_Estado.Text:='0';
     EdtReducao_BC_ICMS_Estado.Text:='0';
     EdtAliquota_ICMS_Cupom_Estado.Text:='0';

     comboisentoicms_foraestado.Text:='N�O';
     comboSUBSTITUICAOICMS_foraESTADO.Text:='SIM';
     edtVALORPAUTA_SUB_TRIB_foraESTADO.Text:='0';
     EdtAliquota_ICMS_foraEstado.Text:='0';
     EdtReducao_BC_ICMS_foraEstado.Text:='0';
     EdtAliquota_ICMS_Cupom_foraEstado.Text:='0';
          btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

end;

procedure TFDIVERSO.btSalvarClick(Sender: TObject);
var
  strRef : string; {Rodolfo}
begin

     If ObjDiversoCor.Diverso.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjDiversoCor.Diverso.salvar(true)=False)
     Then exit;

     lbCodigo.Caption:=ObjDiversoCor.Diverso.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     desabilita_campos(GroupBoxPrecoVenda);
     Desabilita_campos(GroupBoxSIMNAO);
     Desabilita_campos(GroupBoxMargem);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

     if (Self.EstadoObjeto='INSER��O')then
     if (ObjDiversoCor.CadastraDiversoEmTodasAsCores(lbCodigo.Caption)=false)then
     Begin
         MensagemErro('Erro ao tentar cadastrar esse Diverso nas cores escolhidas.');
         FDataModulo.IBTransaction.RollbackRetaining;
         Self.EstadoObjeto:='';
         exit;
     end
     else MensagemAviso('Cores Cadastradas com Sucesso !');

     FDataModulo.IBTransaction.CommitRetaining;
     Self.EstadoObjeto:='';

     ObjdiversoCor.diverso.LocalizaCodigo(lbCodigo.Caption);
     ObjdiversoCor.diverso.TabelaparaObjeto;
     self.ObjetoParaControles;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
      btopcoes.Visible :=true;



end;

procedure TFDIVERSO.btAlterarClick(Sender: TObject);
begin
    If (ObjDiversoCor.Diverso.Status=dsinactive) and (lbCodigo.Caption<>'')
    Then Begin
                habilita_campos(Self);
                ObjDiversoCor.Diverso.Status:=dsEdit;
                guia.TabIndex:=0;
                EdtReferencia.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                //desabilita_campos(GroupBoxPrecoVenda);
                 if (AlteraPrecoPeloCusto=True)
                 Then Begin
                          EdtPrecoVendaInstalado.Enabled:=false;
                          EdtPrecoVendaRetirado.Enabled:=false;
                          EdtPrecoVendaFornecido.Enabled:=false;
                 End;
                 
                habilita_campos(GroupBoxSIMNAO);
                habilita_campos(GroupBoxMargem);
                Self.EstadoObjeto:='EDI��O';
                     btnovo.Visible :=false;
                 btalterar.Visible:=false;
                 btpesquisar.Visible:=false;
                 btrelatorio.Visible:=false;
                 btexcluir.Visible:=false;
                 btsair.Visible:=false;
                 btopcoes.Visible :=false;
         End;

end;

procedure TFDIVERSO.btCancelarClick(Sender: TObject);
begin
     ObjDiversoCor.Diverso.cancelar;
     Desabilita_campos(GroupBoxSIMNAO);
     Desabilita_campos(GroupBoxMargem);
     lbCodigo.Caption:='';
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     desabilita_campos(GroupBoxPrecoVenda);
     RadioControlaMetroQuadradoSIM.Checked:=false;
     RadioControlaMetroQuadradoNAo.Checked:=false;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;

end;

procedure TFDIVERSO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjDiversoCor.Diverso.Get_pesquisa,ObjDiversoCor.Diverso.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjDiversoCor.Diverso.status<>dsinactive
                                  then exit;

                                  If (ObjDiversoCor.Diverso.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjDiversoCor.Diverso.ZERARTABELA;
                                  Guia.TabIndex:=0;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFDIVERSO.btExcluirClick(Sender: TObject);
begin
     If (ObjDiversoCor.Diverso.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (ObjDiversoCor.Diverso.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjDiversoCor.Diverso.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(GroupBoxPrecoVenda);
     Desabilita_campos(GroupBoxSIMNAO);
     Desabilita_campos(GroupBoxMargem);
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFDIVERSO.btRelatorioClick(Sender: TObject);
begin
///    ObjDiversoCor.Diverso.Imprime(lbCodigo.Caption);
end;

procedure TFDIVERSO.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFDIVERSO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFDIVERSO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFDIVERSO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    If Key=VK_Escape
    Then Self.Close;
    if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('CADASTRO DE DIVERSOS');
         FAjuda.ShowModal;
    end;

end ;

procedure TFDIVERSO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFDIVERSO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjDiversoCor.Diverso do
    Begin
        Submit_Codigo(lbCodigo.Caption);
        Submit_Referencia(edtReferencia.text);
        Submit_Descricao(edtDescricao.text);
        GrupoDiverso.Submit_codigo(edtGrupoDiverso.text);
        Submit_Unidade(edtUnidade.text);
        Submit_PrecoCusto(tira_ponto(edtPrecoCusto.text));

        Submit_PORCENTAGEMINSTALADO(tira_ponto(EdtPorcentagemInstalado.Text));
        Submit_PORCENTAGEMFORNECIDO(tira_ponto(EdtPorcentagemFornecido.Text));
        Submit_PORCENTAGEMRETIRADO(tira_ponto(EdtPorcentagemRetirado.Text));

        Submit_precovendaINSTALADO(tira_ponto(EdtprecovendaInstalado.Text));
        Submit_precovendaFORNECIDO(tira_ponto(EdtprecovendaFornecido.Text));
        Submit_precovendaRETIRADO(tira_ponto(EdtprecovendaRetirado.Text));

        if RadioControlaMetroQuadradoSIM.Checked = true
        then Submit_ContralaPorMetroQuadrado('S')
        else
            if RadioControlaMetroQuadradoNAO.Checked = true
            then Submit_ContralaPorMetroQuadrado('N');

        Submit_ClassificacaoFiscal(edtClassificacaoFiscal.text);

//Campos que nao sao mais usados
        Submit_IsentoICMS_Estado('N');
        Submit_SubstituicaoICMS_Estado('N');
        Submit_ValorPauta_Sub_Trib_Estado('0');
        Submit_Aliquota_ICMS_Estado('0');
        Submit_Reducao_BC_ICMS_Estado('0');
        Submit_Aliquota_ICMS_Cupom_Estado('0');
        Submit_IsentoICMS_ForaEstado('N');
        Submit_SubstituicaoICMS_ForaEstado('N');
        Submit_ValorPauta_Sub_Trib_ForaEstado('0');
        Submit_Aliquota_ICMS_ForaEstado('0');
        Submit_Reducao_BC_ICMS_ForaEstado('0');
        Submit_Aliquota_ICMS_Cupom_ForaEstado('0');
        SituacaoTributaria_TabelaA.Submit_codigo('0');
        SituacaoTributaria_TabelaB.Submit_codigo('0');
        Submit_percentualagregado('0');
        Submit_ipi('0');
        submit_NCM(edtNCM.Text);
        Submit_cest(edtCest.text);
        Submit_Fornecedor(edtFornecedor.Text);

        if(chkAtivo.Checked=True)
        then Submit_Ativo('S')
        else Submit_Ativo('N');



        result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFDIVERSO.LimpaLabels;
begin
    LbNomeGrupoDiverso.Caption:='';
    lbFornecedor.Caption:='';
    lbCodigo.Caption:='';
end;

function TFDIVERSO.ObjetoParaControles: Boolean;
var
  sender:TObject;
begin
  Try
     With ObjDiversoCor.Diverso do
     Begin
        lbCodigo.Caption:=Get_Codigo;
        EdtReferencia.text:=Get_Referencia;
        EdtDescricao.text:=Get_Descricao;
        EdtGrupoDiverso.text:=GrupoDiverso.Get_codigo;
        LbNomeGrupoDiverso.Caption:=GrupoDiverso.Get_Nome;
        EdtUnidade.text:=Get_Unidade;
        EdtPrecoCusto.Text:=formata_valor(Get_PRECOCUSTO);
        EdtPorcentagemInstalado.Text:=Get_PORCENTAGEMINSTALADO;
        EdtPorcentagemFornecido.Text:=Get_PORCENTAGEMFORNECIDO;
        EdtPorcentagemRetirado.Text:=Get_PORCENTAGEMRETIRADO;
        EdtPrecoVendaInstalado.Text:=Get_PRECOVENDAINSTALADO;
        EdtPrecoVendaFornecido.Text:=Get_PRECOVENDAFORNECIDO;
        EdtPrecoVendaRetirado.Text:=Get_PRECOVENDARETIRADO ;

        if (Get_ContralaPorMetroQuadrado = 'S')
        then RadioControlaMetroQuadradoSIM.Checked:=true
        else
          if (Get_ContralaPorMetroQuadrado = 'N')
          then RadioControlaMetroQuadradoNAO.Checked:=true;


        ComboIsentoICMS_Estado.text:=Get_IsentoICMS_Estado;
        ComboSubstituicaoICMS_Estado.text:=Get_SubstituicaoICMS_Estado;
        EdtValorPauta_Sub_Trib_Estado.text:=Get_ValorPauta_Sub_Trib_Estado;
        EdtAliquota_ICMS_Estado.text:=Get_Aliquota_ICMS_Estado;
        EdtReducao_BC_ICMS_Estado.text:=Get_Reducao_BC_ICMS_Estado;
        EdtAliquota_ICMS_Cupom_Estado.text:=Get_Aliquota_ICMS_Cupom_Estado;
        ComboIsentoICMS_ForaEstado.text:=Get_IsentoICMS_ForaEstado;
        ComboSubstituicaoICMS_ForaEstado.text:=Get_SubstituicaoICMS_ForaEstado;
        EdtValorPauta_Sub_Trib_ForaEstado.text:=Get_ValorPauta_Sub_Trib_ForaEstado;
        EdtAliquota_ICMS_ForaEstado.text:=Get_Aliquota_ICMS_ForaEstado;
        EdtReducao_BC_ICMS_ForaEstado.text:=Get_Reducao_BC_ICMS_ForaEstado;
        EdtAliquota_ICMS_Cupom_ForaEstado.text:=Get_Aliquota_ICMS_Cupom_ForaEstado;
        EdtSituacaoTributaria_TabelaA.text:=SituacaoTributaria_TabelaA.Get_codigo;
        EdtSituacaoTributaria_TabelaB.text:=SituacaoTributaria_TabelaB.Get_codigo;
        EdtClassificacaoFiscal.text:=Get_ClassificacaoFiscal;
        edtFornecedor.Text:=Get_Fornecedor;
        edtFornecedorExit(sender);

        edtpercentualagregado.Text:=Get_percentualagregado;
        edtNCM.Text:=Get_NCM;
        edtCest.Text := Get_cest;

        if(Get_Ativo='S') then
        begin
            chkAtivo.Checked:=True;
            lbAtivoInativo.Caption:='Ativo';
        end
        else
        begin
            chkAtivo.Checked:=False;
            lbAtivoInativo.Caption:='Inativo';
        end;


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFDIVERSO.TabelaParaControles: Boolean;
begin
     If (ObjDiversoCor.Diverso.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFDIVERSO.edtGrupoDiversoExit(Sender: TObject);
begin
    ObjDiversoCor.Diverso.EdtGrupoDiversoExit(Sender, LbNomeGrupoDiverso);
end;

procedure TFDIVERSO.edtGrupoDiversoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   ObjDiversoCor.Diverso.EdtGrupoDiversoKeyDown(Sender, Key, Shift, LbNomeGrupoDiverso);
end;

procedure TFDIVERSO.EdtPorcentagemInstaladoExit(Sender: TObject);
Var PrecoCusto:Currency;
begin
PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
if (AlteraPrecoPeloCusto=True)
Then EdtPrecoVendaInstalado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemInstalado.Text))/100)));
end;

procedure TFDIVERSO.EdtPorcentagemFornecidoExit(Sender: TObject);
Var PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaFornecido.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemFornecido.Text))/100)));
end;
procedure TFDIVERSO.EdtPorcentagemRetiradoExit(Sender: TObject);
Var PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaRetirado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemRetirado.Text))/100)));
end;

procedure TFDIVERSO.btGravarCorClick(Sender: TObject);
begin
    With ObjDiversoCor do
    Begin
        ZerarTabela;

        if (edtcodigoCOR.text='')
        or (edtcodigoCOR.text='0')
        Then Begin
                Status:=dsInsert;
                edtcodigoCOR.Text:='0';
        End
        Else Status:=dsEdit;

        Submit_Codigo(edtcodigoCOR.Text);
        Submit_classificacaofiscal(edtclassificacaofiscal_COR.Text);
        Diverso.Submit_Codigo(lbCodigo.Caption);
        Cor.Submit_codigo(edtCor.text);
        Submit_PorcentagemAcrescimo(EdtPorcentagemAcrescimo.Text);
        {if (EdtEstoque.text='')
        Then EdtEstoque.Text:='0';   }

        //Submit_Estoque(EdtEstoque.Text);
        Submit_EstoqueMinimo( edtEstoqueMinimo.Text );

        Submit_AcrescimoExtra(edtAcrescimoExtra.text);
        if (Salvar(True)=False)
        Then Begin
                  EdtCorReferencia.SetFocus;
                  exit;
        End;
        lbnomecor.caption:='';
        limpaedit(panelcor);
        limpaedit(GroupBoxLucro);
        EdtCorReferencia.SetFocus;
        Self.ResgataCores;
    End;

end;

procedure TFDIVERSO.BtExcluirCorClick(Sender: TObject);
begin
     if (edtcodigoCOR.Text='')then
     exit;

     If (DBGRIDCOR.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGRIDCOR.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir essa cor do Diverso Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     ObjDiversoCor.Exclui(DBGRIDCOR.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     limpaedit(PanelCor);
     Self.ResgataCores;

end;

procedure TFDIVERSO.BtCancelarCorClick(Sender: TObject);
begin
     limpaedit(panelcor);
     lbnomecor.caption:='';
     EdtCorReferencia.SetFocus;
end;

procedure TFDIVERSO.PreparaCor;
begin
     //Resgatando as Cores para esse Diverso
     Self.ResgataCores;
     //habilitando os edits desse panel
     habilita_campos(PanelCor);
     habilita_campos(GroupBoxLucro);
     EdtPorcentagemAcrescimo.Enabled:=False;
     EdtPorcentagemAcrescimoFinal.Enabled:=False;


     edtinstaladofinal.enabled:=False;
     edtretiradofinal.enabled:=False;
     edtfornecidofinal.enabled:=False;
     {if(PAlteraCampoEstoque=True)
     then EdtEstoque.Enabled:=True
     else
     EdtEstoque.Enabled:=False;  }

     lbnomecor.caption:='';
     LbCusto.caption:='Custo'+#13+'R$ '+EdtPrecoCusto.text;
end;

procedure TFDIVERSO.ResgataCores;
begin
     ObjDiversoCor.ResgataCorDiverso(lbCodigo.Caption);
     DBGRIDCOR.Ctl3D:=False;
end;
{procedure TFDIVERSO.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     if (newtab=2)//cor
     Then Begin
               lbestoque.Caption:='';
               if (OBJDiversoCOR.Diverso.Status=dsinsert)
               or (lbCodigo.Caption='')
               Then Begin
                         AllowChange:=False;
                         exit;
               End;

               Self.PreparaCor;
               Notebook.PageIndex:=NewTab;
               EdtCorReferencia.SetFocus;

     End
     Else Begin

             if (newtab=1)//Impostos
             Then Begin
                      if (OBJdiversoCOR.diverso.Status=dsinsert)
                      or (lbCodigo.Caption='')
                      Then Begin
                                AllowChange:=False;
                                exit;
                      End;
                      Notebook.PageIndex:=NewTab;
                      FRImposto_ICMS1.Acessaframe(lbCodigo.Caption);


             End
             Else Begin
                     Notebook.PageIndex:=NewTab;
                  
             End;
     end;
end;  }

procedure TFDIVERSO.EdtCorReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjDIVERSOCor.EdtCorKeyDown(Sender,EdtCor,  Key, Shift, LbNomeCor);
end;

procedure TFDIVERSO.EdtCorReferenciaExit(Sender: TObject);
begin
     ObjDIVERSOCor.EdtCor_PorcentagemCor_Exit(Sender, EdtCor, lbnomecor, EdtPorcentagemAcrescimo);
end;

procedure TFDIVERSO.DBGRIDCORDblClick(Sender: TObject);
begin

     If (DBGRIDCOR.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGRIDCOR.DataSource.DataSet.RecordCount=0)
     Then exit;



     limpaedit(PanelCor);
     lbnomecor.caption:='';
     if (ObjDiversoCor.LocalizaCodigo(DBGRIDCOR.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataCores;
               exit;
     End;

     Self.preparacor;
     With ObjDiversoCor do
     Begin
          TabelaparaObjeto;
          edtcodigoCOR.text:=Get_Codigo;
          edtclassificacaofiscal_COR.text:=Get_classificacaofiscal;
          EdtCor.text:=Cor.Get_codigo;
          EdtCorReferencia.Text:=Cor.Get_Referencia;
          lbnomecor.caption:=Cor.Get_Descricao;
          EdtPorcentagemAcrescimo.text:=Get_PorcentagemAcrescimo;
          lbestoque.Caption:=RetornaEstoque+' '+Diverso.Get_Unidade;
          edtEstoqueMinimo.Text := Get_EstoqueMinimo;

          EdtAcrescimoExtra.text:=Get_AcrescimoExtra;
          EdtPorcentagemAcrescimoFinal.text:=Get_PorcentagemAcrescimoFinal;

          edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
          edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
          edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));

          EdtCorReferencia.SetFocus;


     End;

end;

procedure TFDIVERSO.EdtAcrescimoExtraExit(Sender: TObject);
Var
PValorCusto, PAcrescimoExtra, PValorExtra :Currency;
begin
    if ((TEdit(Sender).Text=''))then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoCusto.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaInstalado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaFornecido.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaRetirado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtCor.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;

    PValorCusto:=0;
    PAcrescimoExtra:=0;
    PValorExtra:=0;

    try
          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));

          PAcrescimoExtra:=StrTOCurr(tira_ponto(EdtAcrescimoExtra.Text));
          PValorExtra:=((PValorCusto*PAcrescimoExtra)/100);
    except;
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais.');
    end;

    EdtPorcentagemAcrescimoFinal.Text:=CurrToStr(StrToCurr(tira_ponto(EdtPorcentagemAcrescimo.Text))+StrToCurr(tira_ponto(EdtAcrescimoExtra.Text)));

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(tira_ponto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
end;

procedure TFDIVERSO.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjDiversoCor.Diverso.PrimeiroRegistro = false)then
    exit;

    ObjDiversoCor.Diverso.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFDIVERSO.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigo.Caption='')then
    lbCodigo.Caption:='0';

    if  (ObjDiversoCor.Diverso.RegistoAnterior(StrToInt(lbCodigo.Caption)) = false)then
    exit;

    ObjDiversoCor.Diverso.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFDIVERSO.btProximoClick(Sender: TObject);
begin
    if (lbCodigo.Caption='')then
    lbCodigo.Caption:='0';

    if  (ObjDiversoCor.Diverso.ProximoRegisto(StrToInt(lbCodigo.Caption)) = false)then
    exit;

    ObjDiversoCor.Diverso.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFDIVERSO.btUltimoClick(Sender: TObject);
begin
    if  (ObjDiversoCor.Diverso.UltimoRegistro = false)then
    exit;

    ObjDiversoCor.Diverso.TabelaparaObjeto;
    Self.ObjetoParaControles;
end;


procedure TFDIVERSO.EdtPrecoCustoExit(Sender: TObject);
begin
      TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
      if(EdtPorcentagemInstalado.Text<>'')
      then EdtPorcentagemInstaladoExit(Sender);
      if(EdtPorcentagemFornecido.Text<>'')
      then EdtPorcentagemFornecidoExit(sender);
      if(EdtPorcentagemRetirado.Text<>'')
      then EdtPorcentagemRetiradoExit(Sender);
end;

procedure TFDIVERSO.EdtSituacaoTributaria_TabelaAExit(Sender: TObject);
begin
     Self.ObjDIVERSOCor.Diverso.EdtSituacaoTributaria_TabelaAExit(sender);
end;

procedure TFDIVERSO.EdtSituacaoTributaria_TabelaAKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
Self.ObjDIVERSOCor.Diverso.EdtSituacaoTributaria_TabelaAKeyDown(sender,key,shift);
end;

procedure TFDIVERSO.EdtSituacaoTributaria_TabelaBExit(Sender: TObject);
begin
    Self.ObjDIVERSOCor.Diverso.EdtSituacaoTributaria_TabelaBExit(sender);
end;

procedure TFDIVERSO.EdtSituacaoTributaria_TabelaBKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    Self.ObjDIVERSOCor.Diverso.EdtSituacaoTributaria_TabelaBKeyDown(sender,key,shift);
end;

procedure TFDIVERSO.EdtPrecoVendaInstaladoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemInstalado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaInstalado.Text))/precocusto));
end;

procedure TFDIVERSO.EdtPrecoVendaFornecidoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)

    Then EdtPorcentagemfornecido.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendafornecido.Text))/precocusto));
end;

procedure TFDIVERSO.EdtPrecoVendaRetiradoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemretirado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaretirado.Text))/precocusto));
end;

procedure TFDIVERSO.DBGRIDCORKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     then Self.DBGRIDCORDblClick(sender);
end;

procedure TFDIVERSO.DBGRIDCORDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGRIDCOR.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGRIDCOR.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGRIDCOR.DefaultDrawDataCell(Rect,DBGRIDCOR.Columns[DataCol].Field, State);
          End;
end;


procedure TFDIVERSO.AbreComCodigo(parametro:string);
begin
    if(ObjDIVERSOCor.Diverso.LocalizaReferencia(parametro)=true)
    then begin
        ObjDIVERSOCor.Diverso.TabelaparaObjeto;
        Self.ObjetoParaControles;

    end;
end;

procedure TFDIVERSO.FormShow(Sender: TObject);
begin
       limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.TabIndex:=0;
     ColocaUpperCaseEdit(Self);

     Try
        Self.ObjDIVERSOCOr:=TObjDIVERSOCor.create;
        DBGRIDCOR.DataSource:=ObjDIVERSOCor.ObjDatasource;
        FRImposto_ICMS1.ObjMaterial_ICMS:=TObjdiverso_ICMS.Create(self);
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
        FescolheImagemBotao.PegaFiguraBotaopequeno(Btgravarcor,'BOTAOINSERIR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btcancelacor,'BOTAOCANCELARPRODUTO.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btexcluircor,'BOTAORETIRAR.BMP');
     lbCodigo.caption:='';

     habilita_botoes(Self);
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE DIVERSO')=False)
     Then desab_botoes(Self);

     PAlteraCampoEstoque:=False;
     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('PERMITE ALTERAR O CAMPO ESTOQUE NO CADASTRO DE DIVERSO')=True)
     Then PAlteraCampoEstoque:=true;


     Rg_forma_de_calculo_percentual.itemindex:=0;
     if (ObjParametroGlobal.ValidaParametro('FORMA DE CALCULO DE PERCENTUAL DE ACRESCIMO NAS CORES DO DIVERSO')=True)
     then Begin
               if (ObjParametroGlobal.get_valor='1')
               then Rg_forma_de_calculo_percentual.itemindex:=1;
     End;
     Rg_forma_de_calculo_percentual.enabled:=False;

     AlteraPrecoPeloCusto:=True;
     if (ObjParametroGlobal.ValidaParametro('CALCULA PRECO DE VENDA DO DIVERSO PELO CUSTO E PERCENTUAIS?')=True)
     then Begin
               if (ObjParametroGlobal.get_valor<>'SIM')
               then AlteraPrecoPeloCusto:=false;
     End;

     if(Tag<>0)
     then begin
         if(ObjDIVERSOCor.Diverso.LocalizaCodigo(IntToStr(tag)))
         then begin
           ObjDIVERSOCor.Diverso.TabelaparaObjeto;
           Self.ObjetoParaControles;
         end;

     end;

end;

procedure TFDIVERSO.EdtCorReferenciaDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Fcor:TFCOR ;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fcor:=TFCOR.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabcor','',Fcor)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtCorReferencia.text:=FpesquisaLocal.QueryPesq.fieldbyname('referencia').asstring;
                                lbnomecor.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fcor);

     End;
end;

procedure TFDIVERSO.EdtGrupoDiversoDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FGrupoDiverso:TFGRUPODIVERSO;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            try
                FGrupoDiverso:=TFGRUPODIVERSO.Create(nil);
            except

            end;


            If (FpesquisaLocal.PreparaPesquisa('Select * from tabgrupodiverso','',FGrupoDiverso)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtGrupoDiverso.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 LbNomeGrupoDiverso.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FGrupoDiverso);

     End;
end;


procedure TFDIVERSO.edtNCMKeyPress(Sender: TObject; var Key: Char);
begin
      if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFDIVERSO.lbestoqueMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFDIVERSO.lbestoqueMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
        TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFDIVERSO.lbestoqueClick(Sender: TObject);
begin
     if(ObjParametroGlobal.ValidaParametro('UTILIZA SPED FISCAL')=false) then
     begin
          MensagemAviso('O Parametro "UTILIZA SPED FISCAL" n�o foi encontrado!!!');
          Exit;
     end;
     if(ObjParametroGlobal.Get_Valor='SIM')
     then Exit;

     If(PAlteraCampoEstoque=False)then
     begin
          MensagemAviso('O Usu�rio '+ObjUsuarioGlobal.Get_nome+' n�o tem autoriza��o para alterar o estoque');
          Exit;
     end
     else
     begin
          with FfiltroImp do
          begin
              DesativaGrupos;
              Grupo01.Enabled:=True;
              edtgrupo01.Text:='';
              LbGrupo01.Caption:='QUANTIDADE';
              edtgrupo01.OnKeyPress:=edtquant;

              MensagemAviso('Acerte o estoque: Digite a quantidade a tirar ou acrescentar no estoque');

              ShowModal;

              if(Tag=0)
              then Exit;

              if(edtgrupo01.Text='') or(edtgrupo01.Text='0')
              then Exit;
              
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Status:=dsInsert;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Submit_DATA(FormatDateTime('dd/mm/yyyy',Now));
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('PELO MODO ACERTO DE ESTOQUE (VIDRO)');
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(edtgrupo01.Text);
              OBJESTOQUEGLOBAL.Submit_DIVERSOCOR(ObjDIVERSOCOR.Get_Codigo);

              if (OBJESTOQUEGLOBAL.Salvar(True)=False)
              Then Begin
                     MensagemErro('Erro na tentativa de salvar o registro de estoque do PERFILADO');
                     exit;
              End;

              lbestoque.Caption:=ObjDIVERSOCOR.RetornaEstoque+' '+ObjDIVERSOCor.DIVERSO.Get_Unidade;


          end;
     end;
end;

procedure TFDIVERSO.edtquant(Sender: TObject;
  var Key: Char);
begin
     if not (Key in['0'..'9',Chr(8),'-',',']) then
    begin
        Key:= #0;
    end;

end;

procedure TFDIVERSO.chkAtivoClick(Sender: TObject);
var
  QueryPesq:TIBQuery;
  FERRODESATIVARMATERIAL:TFErrosDesativarMaterial;
begin
      if(lbCodigo.Caption='') or (lbCodigo.Caption='0')
      then Exit;

      QueryPesq:=TIBQuery.Create(nil);
      QueryPesq.Database:=FDataModulo.IBDatabase;
      FERRODESATIVARMATERIAL:=TFErrosDesativarMaterial.Create(nil);

      try
          if(chkAtivo.Checked=True)
          then lbAtivoInativo.Caption:='Ativo';

          if(chkAtivo.Checked=False) then
          begin
              with QueryPesq do
              begin
                  Close;
                  SQL.Clear;
                  SQL.Add('select * from tabdiverso_proj where diverso='+lbCodigo.Caption);
                  Open;

                  if(recordcount=0)
                  then lbAtivoInativo.Caption:='Inativo'
                  else
                  begin
                       chkAtivo.Checked:=True;
                       FERRODESATIVARMATERIAL.PassaMaterial(lbCodigo.Caption,EdtDescricao.Text,'diverso');
                       FERRODESATIVARMATERIAL.ShowModal;
                  end;

              end;


          end;
      finally
           FreeAndNil(QueryPesq);
           FERRODESATIVARMATERIAL.Free;
      end;
end;

procedure TFDIVERSO.btAjudaClick(Sender: TObject);
begin
     FAjuda.PassaAjuda('CADASTRO DE DIVERSOS');
     FAjuda.ShowModal;
end;

procedure TFDIVERSO.LbNomeGrupoDiversoClick(Sender: TObject);
var
  FGrupoDiverso:TFGRUPODIVERSO;
begin
   try
     FGrupoDiverso:=TFGRUPODIVERSO.Create(nil);
   except
      Exit;
   end;

   try
     if(EdtGrupoDiverso.Text='')
     then Exit;

     FGrupoDiverso.Tag:=StrToInt(EdtGrupoDiverso.Text);
     FGrupoDiverso.ShowModal;

   finally
     FreeAndNil(FGrupoDiverso);
   end;


end;

procedure TFDIVERSO.LbNomeGrupoDiversoMouseLeave(Sender: TObject);
begin
   TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFDIVERSO.LbNomeGrupoDiversoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
   TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFDIVERSO.lbnomecorClick(Sender: TObject);
var
  Fcor:TFCOR;
begin
   try
      Fcor:=TFCOR.Create(nil);
   except
      Exit;
   end;

   try
     if(EdtCor.Text='')
     then Exit;

     Fcor.Tag:=StrToInt(EdtCor.Text);
     fcor.ShowModal;
   finally
     FreeAndNil(Fcor);
   end; 

end;
procedure TFDIVERSO.EdtPrecoCustoKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFDIVERSO.EdtPorcentagemInstaladoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFDIVERSO.EdtPorcentagemFornecidoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFDIVERSO.EdtPorcentagemRetiradoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFDIVERSO.EdtGrupoDiversoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFDIVERSO.EdtPorcentagemAcrescimoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFDIVERSO.EdtAcrescimoExtraKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFDIVERSO.EdtPorcentagemAcrescimoFinalKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFDIVERSO.edtFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    ObjDIVERSOCor.Diverso.EdtFornecedorKeyDown(Sender,key,Shift,lbFornecedor);
end;

procedure TFDIVERSO.edtFornecedorExit(Sender: TObject);
var
  query:TIBQuery;
begin
  if(edtFornecedor.Text='')
  then Exit;
  query:=TIBQuery.Create(nil);
  query.database:=FDataModulo.IBDatabase;
  try
    query.Close;
    query.SQL.Clear;
    query.SQL.Add('select razaosocial from tabfornecedor where codigo='+edtFornecedor.text);
    query.Open;
    lbFornecedor.Caption:=query.fieldbyname('razaosocial').asstring;
  finally
    FreeAndNil(query);
  end;

end;

procedure TFDIVERSO.GuiaChange(Sender: TObject);
begin
     if (Guia.TabIndex=2)//cor
     Then Begin
               lbestoque.Caption:='';
               if (OBJDiversoCOR.Diverso.Status=dsinsert)
               or (lbCodigo.Caption='')
               Then Begin
                  exit;
               End;

               Self.PreparaCor;
               EdtCorReferencia.SetFocus;

     End
     Else Begin

             if (Guia.TabIndex=1)//Impostos
             Then Begin
                      if (OBJdiversoCOR.diverso.Status=dsinsert)
                      or (lbCodigo.Caption='')
                      Then Begin
                        exit;
                      End;
                      FRImposto_ICMS1.Acessaframe(lbCodigo.Caption);
             End
             Else Begin
               Guia.TabIndex:=0
                  
             End;
     end;
end;

procedure TFDIVERSO.edtNCMKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  FDataModulo.edtNCMKeyDown(Sender,Key,Shift);

end;

procedure TFDIVERSO.FRImposto_ICMS1BtGravar_material_icmsClick(
  Sender: TObject);
begin
  FRImposto_ICMS1.BtGravar_material_icmsClick(Sender);

end;



procedure TFDIVERSO.edtCestKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if Key=vk_f9 then
  begin
    Clipboard.AsText := EDTNCM.Text;
    With TObjCEST.Create do
    begin
      edtCESTkeydown(sender,Key,shift);
      Free;
    end;
  end;
end;

end.
