unit UobjCOMPONENTE_PROJ;
Interface
Uses forms,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJCOMPONENTE,UOBJPROJETO
,UCOMPONENTE;


Type
   TObjCOMPONENTE_PROJ=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Componente:TOBJCOMPONENTE;
                Projeto:TOBJPROJETO;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_FormulaAltura(parametro: string);
                Function Get_FormulaAltura: string;
                Procedure Submit_FormulaLargura(parametro: string);
                Function Get_FormulaLargura: string;
                Procedure Submit_FolgaAltura(parametro: string);
                Function Get_FolgaAltura: string;
                Procedure Submit_FolgaLargura(parametro: string);
                Function Get_FolgaLargura: string;
                Procedure Submit_Corte(parametro: string);
                Function Get_Corte: string;
                procedure EdtComponenteExit(Sender: TObject;Var PEdtCodigo :TEdit;LABELNOME:TLABEL);
                procedure EdtComponenteKeyDown(Sender: TObject; Var PEdtCodigo :TEdit; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtComponenteKeyDown2(Sender: TObject;var Key: Word;Shift: TShiftState;var LABELNOME:string;var Tedit:string;CodigoProjeto:string;var referencia:string);
                procedure EdtProjetoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Procedure Resgata_Componente_Proj(PProjeto : string);
                function VerificaSeJaExisteEsteComponenteNesteProjeto(PProjeto, PComponente: string): Boolean;

                Procedure TrocaPOntoPOrVirgula;

                Function  fieldbyname(PCampo:String):String;overload;
                Procedure fieldbyname(PCampo:String;PValor:String);overload;
                function Replica(PProjeto, PnovoProjeto: string): boolean;

                Function Get_Esquadria:string;
                procedure Submit_Esquadria(parametro:string);


         Private
               Objquery:Tibquery;
               ObjQueryPesquisa : TIBQuery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               FormulaAltura:string;
               FormulaLargura:string;
               FolgaAltura:string;
               FolgaLargura:string;
               Corte:string;
               Esquadria:string;
//CODIFICA VARIAVEIS PRIVADAS








               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UMostraBarraProgresso;


{ TTabTitulo }


Function  TObjCOMPONENTE_PROJ.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If(FieldByName('Componente').asstring<>'')
        Then Begin
                 If (Self.Componente.LocalizaCodigo(FieldByName('Componente').asstring)=False)
                 Then Begin
                          Messagedlg('Componente N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Componente.TabelaparaObjeto;
        End;
        If(FieldByName('Projeto').asstring<>'')
        Then Begin
                 If (Self.Projeto.LocalizaCodigo(FieldByName('Projeto').asstring)=False)
                 Then Begin
                          Messagedlg('Projeto N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Projeto.TabelaparaObjeto;
        End;
        Self.FormulaAltura:=fieldbyname('FormulaAltura').asstring;
        Self.FormulaLargura:=fieldbyname('FormulaLargura').asstring;
        Self.FolgaAltura:=fieldbyname('FolgaAltura').asstring;
        Self.FolgaLargura:=fieldbyname('FolgaLargura').asstring;
        Self.Corte:=fieldbyname('Corte').asstring;
        Self.Esquadria:=fieldbyname('Esquadria').AsString;
//CODIFICA TABELAPARAOBJETO








        result:=True;
     End;
end;


Procedure TObjCOMPONENTE_PROJ.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Componente').asstring:=Self.Componente.GET_CODIGO;
        ParamByName('Projeto').asstring:=Self.Projeto.GET_CODIGO;
        ParamByName('FormulaAltura').asstring:=Self.FormulaAltura;
        ParamByName('FormulaLargura').asstring:=Self.FormulaLargura;
        ParamByName('FolgaAltura').asstring:=virgulaparaponto(Self.FolgaAltura);
        ParamByName('FolgaLargura').asstring:=virgulaparaponto(Self.FolgaLargura);
        ParamByName('Corte').asstring:=Self.Corte;
        ParamByName('Esquadria').AsString:=self.Esquadria;
//CODIFICA OBJETOPARATABELA

  End;
End;

Procedure TObjCOMPONENTE_PROJ.fieldbyname(PCampo:String;PValor:String);
Begin
     PCampo:=Uppercase(pcampo);                                

     If (Pcampo='CODIGO')
     Then Begin
              Self.Submit_CODIGO(pVALOR);
              exit;
     End;
     If (Pcampo='COMPONENTE')
     Then Begin
              Self.COMPONENTE.Submit_codigo(pVALOR);
              exit;
     End;
     If (Pcampo='PROJETO')
     Then Begin
              Self.PROJETO.Submit_codigo(pVALOR);
              exit;
     End;
     If (Pcampo='FORMULAALTURA')
     Then Begin
              Self.Submit_FORMULAALTURA(pVALOR);
              exit;
     End;
     If (Pcampo='FORMULALARGURA')
     Then Begin
              Self.Submit_FORMULALARGURA(pVALOR);
              exit;
     End;
     If (Pcampo='FOLGAALTURA')
     Then Begin
              Self.Submit_FOLGAALTURA(pVALOR);
              exit;
     End;
     If (Pcampo='FOLGALARGURA')
     Then Begin
              Self.Submit_FOLGALARGURA(pVALOR);
              exit;
     End;
     If (Pcampo='CORTE')
     Then Begin
              Self.Submit_CORTE(pVALOR);
              exit;
     End;
end;

Function TObjCOMPONENTE_PROJ.fieldbyname(PCampo:String):String;
Begin                                                          
     PCampo:=Uppercase(pcampo);                                
     Result:='';
      
     If (Pcampo='CODIGO')
     Then Begin
              Result:=Self.Get_CODIGO;
              exit;
     End;
     If (Pcampo='COMPONENTE')
     Then Begin
              Result:=Self.COMPONENTE.Get_codigo;
              exit;
     End;
     If (Pcampo='PROJETO')
     Then Begin
              Result:=Self.PROJETO.Get_codigo;
              exit;
     End;
     If (Pcampo='FORMULAALTURA')
     Then Begin
              Result:=Self.Get_FORMULAALTURA;
              exit;
     End;
     If (Pcampo='FORMULALARGURA')
     Then Begin
              Result:=Self.Get_FORMULALARGURA;
              exit;
     End;
     If (Pcampo='FOLGAALTURA')
     Then Begin
              Result:=Self.Get_FOLGAALTURA;
              exit;
     End;
     If (Pcampo='FOLGALARGURA')
     Then Begin
              Result:=Self.Get_FOLGALARGURA;
              exit;
     End;
     If (Pcampo='CORTE')
     Then Begin
              Result:=Self.Get_CORTE;
              exit;
     End;
end;


//***********************************************************************

function TObjCOMPONENTE_PROJ.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCOMPONENTE_PROJ.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Componente.ZerarTabela;
        Projeto.ZerarTabela;
        FormulaAltura:='';
        FormulaLargura:='';
        FolgaAltura:='';
        FolgaLargura:='';
        Corte:='';
        Esquadria:='';

//CODIFICA ZERARTABELA








     End;
end;

Function TObjCOMPONENTE_PROJ.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCOMPONENTE_PROJ.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Componente.LocalizaCodigo(Self.Componente.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Componente n�o Encontrado!';
      If (Self.Projeto.LocalizaCodigo(Self.Projeto.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Projeto n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCOMPONENTE_PROJ.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.Componente.Get_Codigo<>'')
        Then Strtoint(Self.Componente.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Componente';
     End;
     try
        If (Self.Projeto.Get_Codigo<>'')
        Then Strtoint(Self.Projeto.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Projeto';
     End;
     try
        Strtofloat(Self.FolgaAltura);
     Except
           Mensagem:=mensagem+'/FolgaAltura';
     End;
     try
        Strtofloat(Self.FolgaLargura);
     Except
           Mensagem:=mensagem+'/FolgaLargura';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCOMPONENTE_PROJ.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCOMPONENTE_PROJ.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCOMPONENTE_PROJ.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro COMPONENTE_PROJ vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Componente,Projeto,FormulaAltura,FormulaLargura,FolgaAltura');
           SQL.ADD(' ,FolgaLargura,Corte,Esquadria');
           SQL.ADD(' from  TabComponente_Proj');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCOMPONENTE_PROJ.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCOMPONENTE_PROJ.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCOMPONENTE_PROJ.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin  
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjQueryPesquisa;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Componente:=TOBJCOMPONENTE.create;
        Self.Projeto:=TOBJPROJETO.create;
        
        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabComponente_Proj(Codigo,Componente,Projeto');
                InsertSQL.add(' ,FormulaAltura,FormulaLargura,FolgaAltura,FolgaLargura');
                InsertSQL.add(' ,Corte,Esquadria)');
                InsertSQL.add('values (:Codigo,:Componente,:Projeto,:FormulaAltura');
                InsertSQL.add(' ,:FormulaLargura,:FolgaAltura,:FolgaLargura,:Corte,:Esquadria');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabComponente_Proj set Codigo=:Codigo,Componente=:Componente');
                ModifySQL.add(',Projeto=:Projeto,FormulaAltura=:FormulaAltura,FormulaLargura=:FormulaLargura');
                ModifySQL.add(',FolgaAltura=:FolgaAltura,FolgaLargura=:FolgaLargura');
                ModifySQL.add(',Corte=:Corte,Esquadria=:Esquadria');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabComponente_Proj where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCOMPONENTE_PROJ.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCOMPONENTE_PROJ.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCOMPONENTE_PROJ');
     Result:=Self.ParametroPesquisa;
end;

function TObjCOMPONENTE_PROJ.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de COMPONENTE_PROJ ';
end;


function TObjCOMPONENTE_PROJ.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCOMPONENTE_PROJ,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCOMPONENTE_PROJ,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCOMPONENTE_PROJ.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    FreeAndNil(Self.ObjQueryPesquisa);
    FreeAndNil(Self.ObjDataSource);
    Self.Componente.FREE;
    Self.Projeto.FREE;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCOMPONENTE_PROJ.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCOMPONENTE_PROJ.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjComponente_Proj.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjComponente_Proj.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjComponente_Proj.Submit_FormulaAltura(parametro: string);
begin
        Self.FormulaAltura:=Parametro;
end;
function TObjComponente_Proj.Get_FormulaAltura: string;
begin
        Result:=Self.FormulaAltura;
end;
procedure TObjComponente_Proj.Submit_FormulaLargura(parametro: string);
begin
        Self.FormulaLargura:=Parametro;
end;
function TObjComponente_Proj.Get_FormulaLargura: string;
begin
        Result:=Self.FormulaLargura;
end;
procedure TObjComponente_Proj.Submit_FolgaAltura(parametro: string);
begin
        Self.FolgaAltura:=Parametro;
end;
function TObjComponente_Proj.Get_FolgaAltura: string;
begin
        Result:=Self.FolgaAltura;
end;
procedure TObjComponente_Proj.Submit_FolgaLargura(parametro: string);
begin
        Self.FolgaLargura:=Parametro;
end;
function TObjComponente_Proj.Get_FolgaLargura: string;
begin
        Result:=Self.FolgaLargura;
end;
procedure TObjComponente_Proj.Submit_Corte(parametro: string);
begin
        Self.Corte:=Parametro;
end;
function TObjComponente_Proj.Get_Corte: string;
begin
        Result:=Self.Corte;
end;
//CODIFICA GETSESUBMITS


procedure TObjCOMPONENTE_PROJ.EdtComponenteExit(Sender: TObject;Var PEdtCodigo :TEdit; LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Componente.LocalizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text;
               exit;
     End;
     Self.Componente.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Componente.Get_Descricao;
     PEdtCodigo.Text:=Self.Componente.Get_Codigo;


End;
procedure TObjCOMPONENTE_PROJ.EdtComponenteKeyDown(Sender: TObject;Var PEdtCodigo:TEdit; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Fcomponente:TFCOMPONENTE;
begin

     If (key <>vk_f9)
     Then exit;
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fcomponente:=TFCOMPONENTE.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Componente.Get_Pesquisa,Self.Componente.Get_TituloPesquisa,Fcomponente)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Componente.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Componente.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fcomponente);
     End;
end;

procedure TObjCOMPONENTE_PROJ.EdtComponenteKeyDown2(Sender: TObject;var Key: Word;
  Shift: TShiftState;var LABELNOME:string;var Tedit:string;CodigoProjeto:string;var referencia:string);
var
   FpesquisaLocal:Tfpesquisa;
   Fcomponente:TFCOMPONENTE;
   SQL:string;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            SQL:= 'select tabcomponente.* from tabcomponente_proj join tabcomponente on tabcomponente.codigo=tabcomponente_proj.componente where tabcomponente_proj.projeto= '+CodigoProjeto;
            If (FpesquisaLocal.PreparaPesquisa(SQL,'',Fcomponente)=True)
            Then
            Begin
                  Try
                        If (FpesquisaLocal.showmodal=mrok) Then
                        Begin
                                 TEdit:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If (TEdit<>'') Then
                                 Begin
                                        If FpesquisaLocal.QueryPesq.fieldbyname('DESCRICAO').asstring<>''
                                        Then LABELNOME:=FpesquisaLocal.QueryPesq.fieldbyname('DESCRICAO').asstring
                                        Else LABELNOME:='';

                                        referencia:=FpesquisaLocal.querypesq.fieldbyname('referencia').AsString;
                                 End;
                        End;
                  Finally
                        FpesquisaLocal.QueryPesq.close;
                  End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fcomponente);
     End;
end;

procedure TObjCOMPONENTE_PROJ.EdtProjetoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Projeto.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Projeto.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Projeto.Get_Descricao;
End;
procedure TObjCOMPONENTE_PROJ.EdtProjetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Projeto.Get_Pesquisa,Self.Projeto.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Projeto.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Projeto.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Projeto.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjCOMPONENTE_PROJ.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCOMPONENTE_PROJ';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjCOMPONENTE_PROJ.Resgata_Componente_Proj(PProjeto: string);
begin
     With Self.ObjQueryPesquisa do
     Begin
         close;
         SQL.Clear;
         Sql.Add('Select  TabComponente.Referencia, TabComponente.Descricao,');
         Sql.Add('TabComponente_Proj.FormulaAltura,TabComponente_Proj.FolgaAltura,');
         Sql.Add('TabComponente_Proj.FormulaLargura,TabComponente_Proj.FolgaLargura,');
         Sql.add('TabComponente_Proj.Corte,TabComponente_Proj.Codigo,TabComponente_Proj.Esquadria');
         Sql.Add('from TabComponente_Proj');
         Sql.Add('Join TabComponente on TabComponente.Codigo = TabComponente_Proj.Componente');
         Sql.Add('Where Projeto = '+PProjeto);
         if (PProjeto = '')
         then exit;
         Open;
     end;

end;

function TObjCOMPONENTE_PROJ.VerificaSeJaExisteEsteComponenteNesteProjeto(
  PProjeto, PComponente: string): Boolean;
Var    QueryLOcal:TIBquery;
begin
try
        Result:=false;
        try
             QueryLOcal:=TIBQuery.Create(nil);
             QueryLOcal.Database:=FDataModulo.IBDatabase;
        except
             MensagemErro('Erro ao tentar criar a QueryLocal');
             exit;
        end;

        With QueryLocal do
        Begin
             Close;
             Sql.Clear;
             Sql.Add('Select Codigo from TabComponente_Proj');
             Sql.Add('Where Componente = '+PComponente);
             Sql.Add('and Projeto = '+PProjeto);
             Open;

             if (RecordCount>0)then
             Result:=true;

        end;
Finally
       FreeAndNil(QueryLOcal);
end;
end;
procedure TObjCOMPONENTE_PROJ.TrocaPOntoPOrVirgula;
begin
     With  Self.ObjQueryPesquisa do
     Begin
          Close;
          SQL.Clear;
          SQL.Add('Select Codigo, FormulaLargura, FormulaAltura from TabComponente_Proj');
          Open;

          While Not(Eof) do
          Begin
               Self.ZerarTabela;
               Self.LocalizaCodigo(Fieldbyname('Codigo').AsString);
               TabelaparaObjeto;

               Status:=dsEdit;
               Submit_FormulaAltura(virgulaparaponto(fieldbyname('FormulaAltura').AsString));
               Submit_FormulaLargura(virgulaparaponto(fieldbyname('FormulaLargura').AsString));
               Salvar(false);

               Next;
          end;
     end;

     MensagemAviso('Virgula para Ponto convertidos com Sucesso!');
     FDataModulo.IBTransaction.CommitRetaining;

end;



Function TobjComponente_proj.Replica(PProjeto, PnovoProjeto: string):boolean;
var
pcodigoantigo:string;
begin
     result:=false;

     With Self.ObjQueryPesquisa do
     Begin
         close;
         SQL.Clear;
         Sql.Add('Select  TabComponente_Proj.Codigo');
         Sql.Add('from    TabComponente_Proj');
         Sql.Add('Where   Projeto = '+PProjeto);

         if (PProjeto = '')
         then exit;

         Open;
         last;
         if (recordcount=0)
         Then Begin
                   result:=true;
                   exit;
         End;
         FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
         FMostraBarraProgresso.Lbmensagem.caption:='Replicando Componentes';
         first;
         Try

           While not(eof) do
           begin
                FMostraBarraProgresso.IncrementaBarra1(1);
                FMostraBarraProgresso.Show;
                Application.processmessages;

                Self.ZerarTabela;
                if (Self.LocalizaCodigo(fieldbyname('codigo').asstring)=False)
                then Begin
                          mensagemErro('Componente_Proj C�digo '+fieldbyname('codigo').asstring+' n�o localizada');
                          exit;
                End;
                Self.TabelaparaObjeto;

                Self.Status:= dsinsert;
                PcodigoAntigo:=Self.codigo;
                Self.Submit_Codigo('0');
                Self.Projeto.Submit_Codigo(PnovoProjeto);
                if (self.Salvar(False)=False)
                Then begin
                          mensagemErro('Erro na tentativa de Replicar o Componente_proj C�digo='+PcodigoAntigo);
                          exit;
                End;

                next;
           End;
           result:=True;
           
         Finally
                FMostraBarraProgresso.Close;
         End;
     end;

end;


procedure TObjCOMPONENTE_PROJ.Submit_Esquadria(parametro:string);
begin
  Self.Esquadria:=parametro;
end;

function TObjCOMPONENTE_PROJ.Get_Esquadria:string;
begin
  Result:=Esquadria;
end;

end.

