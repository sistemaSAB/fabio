object FImporta: TFImporta
  Left = 192
  Top = 107
  Width = 415
  Height = 207
  Caption = 'Importação de Dados'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object BtClientes: TSpeedButton
    Left = 8
    Top = 8
    Width = 137
    Height = 57
    Caption = 'Clientes'
    Flat = True
    OnClick = BtClientesClick
  end
  object SpeedButton1: TSpeedButton
    Left = 8
    Top = 66
    Width = 137
    Height = 57
    Caption = 'Fornecedores'
    Flat = True
    OnClick = SpeedButton1Click
  end
end
