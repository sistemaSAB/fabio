unit UImporta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons;

type
  TFImporta = class(TForm)
    BtClientes: TSpeedButton;
    SpeedButton1: TSpeedButton;
    procedure BtClientesClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FImporta: TFImporta;

implementation

uses UObjCliente, UObjFornecedor;

{$R *.DFM}

procedure TFImporta.BtClientesClick(Sender: TObject);
var
objcliente:Tobjcliente;
begin
     Try
        objcliente:=TObjCliente.create;
     Except
           Messagedlg('O Objeto de Cliente n�o pode ser criado!',mterror,[mbok],0);
           exit;
     End;

     Try
        objcliente.ImportarDados;

     Finally
            objcliente.free;
     End;
end;

procedure TFImporta.SpeedButton1Click(Sender: TObject);
var
objfornecedor:TObjfornecedor;
begin
     Try
        objfornecedor:=TObjfornecedor.create;
     Except
           Messagedlg('O Objeto de Fornecedor n�o pode ser criado!',mterror,[mbok],0);
           exit;
     End;

     Try
        objfornecedor.ImportaDados;
     Finally
            objfornecedor.free;
     End;
end;

end.
