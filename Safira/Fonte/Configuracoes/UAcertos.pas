unit UAcertos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TFacertos = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Facertos: TFacertos;

implementation

uses UobjVendas, UObjTitulo;

{$R *.DFM}

procedure TFacertos.Button1Click(Sender: TObject);
var
objvendas:TObjVendas;
begin
     Try
        objvendas:=TObjVendas.create;
     Except
           Messagedlg('Erro na tentativa de Cria��o do Objeto de Vendas!',mterror,[mbok],0);
           exit;
     End;
     Try
        objvendas.acertanumdctofinanceiro;
        objvendas.Titulo.AcertaHistoricoPendencias;

     Finally
            objvendas.free;
     End;

end;


end.
