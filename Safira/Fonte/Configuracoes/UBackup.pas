unit UBackup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IBServices, Buttons, FileCtrl, StdCtrls, ExtCtrls, jpeg;

type
  TFbackup = class(TForm)
    BackupService: TIBBackupService;
    BtGeraBackup: TBitBtn;
    ListaArquivos: TFileListBox;
    Diretorio: TDirectoryListBox;
    Drive: TDriveComboBox;
    Image1: TImage;
    RestoreService: TIBRestoreService;
    BtRestaurar: TBitBtn;
    imgrodape: TImage;
    procedure BtGeraBackupClick(Sender: TObject);
    procedure DriveChange(Sender: TObject);
    procedure DiretorioChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtRestaurarClick(Sender: TObject);
  private
    { Private declarations }

    Procedure ApagaArquivos;
    Function OrdenarporData(Parametro:TStrings):Boolean;
    Function MantemArquivos(Parametro:TStrings;Quantos:INteger):Boolean;
    Function Formata_DataHora(Parametro:string):String;
    function PegaLocalNomeBackup: String;
    function SeparaPath(PathBase: String): String;
    function SeparaIp(PathBase: String): String;
    function SeparaPathBackup(PathBase: String): String;
    function PegaLocalBackup: String;

  public
    { Public declarations }
  end;

const quantdir=10;


var
  Fbackup: TFbackup;

implementation

uses UDataModulo, UessencialGlobal, UescolheImagemBotao;

{$R *.DFM}

procedure TFbackup.BtGeraBackupClick(Sender: TObject);
begin
     With BackupService do
     Begin

          Active:=false;
          LoginPrompt:=FALSE;
          If (VerificaBaseRemota(FDataModulo.IBDatabase.DatabaseName)=False)
          Then Begin
                    DatabaseName:=FDataModulo.IBDatabase.DatabaseName;
                    Protocol:=Local;
                    ServerName:='';
                    BackupFile.clear;
                    BackupFile.add(PegaLocalNomeBackup);

               End
          Else Begin
                    DatabaseName:=SeparaPath(FDataModulo.IBDatabase.DatabaseName);
                    protocol:=TCP;
                    servername:=SeparaIp(FDataModulo.IBDatabase.DatabaseName);
                    BackupFile.clear;
                    BackupFile.add(SeparaPathBackup(DatabaseName));
               End;
          //****
          Params.Clear;
          Params.add('USER_NAME='+fdatamodulo.IBDatabase.Params.Values['USER_NAME']);
          Params.add('PASSWORD='+fdatamodulo.IBDatabase.Params.Values['PASSWORD']);
          Try
                  BtGeraBackup.enabled:=false;
                  Active:=True;
                  ServiceStart;
                  Active:=false;
                  ListaArquivos.Update;
                  Messagedlg('Processamento Conclu�do!',mtinformation,[mbok],0);

                  If ListaArquivos.items.count >QuantDir
                  Then Begin
                          If (Messagedlg('Deseja Manter apenas os '+Inttostr(quantdir)+' �ltimos BAckups e Excluir os demais?',mtconfirmation,[mbyes,mbno],0)=mryes)
                          Then ApagaArquivos;
                       End;
                  ListaArquivos.Update;
          finally
                 BtGeraBackup.enabled:=true;
          End;
     End;
end;

procedure TFbackup.DriveChange(Sender: TObject);
begin
     Diretorio.Drive:=drive.Drive;
end;

procedure TFbackup.DiretorioChange(Sender: TObject);
begin
     ListaArquivos.Drive:=Drive.drive;
     ListaArquivos.Directory:=Diretorio.Directory;
end;


procedure TFbackup.FormActivate(Sender: TObject);
var
caminho:String;
begin
     caminho:='';
     caminho:=PegaLocalBackup;
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');


     If (caminho='')
     Then BtGeraBackup.enabled:=False
     Else Begin
                BtGeraBackup.enabled:=True;
                Drive.drive:=caminho[1];
                Diretorio.Drive:=drive.drive;
                Diretorio.Directory:=caminho;
          End;



end;

procedure TFbackup.ApagaArquivos;
var
conta:integer;
Arquivos:TStrings;
cont:integer;
begin
     conta:=ListaArquivos.Items.count;

     If (Conta>quantdir)
     Then Begin
               Try
                  Arquivos:=TStringList.create;
               Except
                     Messagedlg('Erro ao tentar Criar a StringList Arquivos!',mterror,[mbok],0);
                     exit;
               End;
               Try

                   Arquivos.Clear;

                   for cont:=0 to listaarquivos.items.count-1 do
                   arquivos.add(Listaarquivos.items[cont]);

                   
                   //Arquivos:=ListaArquivos.items;
                   //ordenar por data
                   OrdenarporData(arquivos);
                   //manter somente os  iniciais e apagar os outros
                   MantemArquivos(Arquivos,quantdir);
               Finally
                      Freeandnil(arquivos);
               End;

          End;
end;

Function TFbackup.OrdenarporData(Parametro: TStrings): Boolean;
var
Cont1,cont2:Integer;
Data1,Data2:Tdatetime;
auxilio:String;
begin

     for cont1:=0 to Parametro.Count-1 do
     Begin
          For cont2:=cont1 to Parametro.Count-1 do
          Begin
               data1:=StrToDateTime(Formata_DataHora(Parametro[cont1]));
               data2:=StrToDateTime(Formata_DataHora(Parametro[cont2]));
             //  showmessage(DateTimeToStr(data1)+' e '+DateTimeToStr(data2));
               If data2>data1
               then BEgin
                         auxilio:='';
                         auxilio:=Parametro[cont1];
                         Parametro[cont1]:=Parametro[cont2];
                         Parametro[cont2]:=auxilio;
                    End;

          End;

     End;



end;

function TFbackup.MantemArquivos(Parametro: TStrings;
  Quantos: INteger): Boolean;
var
cont:Integer;
begin

     If (quantos>Parametro.Count-1)
     then exit;
     
     for cont:=quantos to (Parametro.Count-1) do
     Begin
          If (DeleteFile(pegalocalBAckup+parametro[cont])=False)
          Then Begin
                    Messagedlg('N�o foi poss�vel apagar '+pegalocalBackup+parametro[cont],mterror,[mbok],0);
                    result:=False;
                    exit;
               End;
     End;

end;


function TFbackup.Formata_DataHora(Parametro: string): String;
var
cont:integer;
contabarra,contaponto:integer;
Resulta:String;

Begin
     contabarra:=0;
     contaponto:=0;
     Resulta:='';

     //layout dia-mes-ano-hora-minuto
     for cont:=1 to length(Parametro) do
     Begin
          If parametro[cont]='-'
          Then Begin
                    if contabarra<2
                    Then Begin
                              resulta:=resulta+'/';
                              inc(contabarra);
                         End
                    Else Begin
                              If COntaponto=0
                              Then resulta:=resulta+' '
                              Else resulta:=resulta+':';
                              inc(contaponto);
                         End;
               End
          Else Begin
                    If (parametro[cont]='.') //encontrou a parte da extensao no nome do arquivo
                    Then break
                    Else resulta:=resulta+parametro[cont];
               End;
     End;
     result:=resulta;
End;

procedure TFbackup.BtRestaurarClick(Sender: TObject);
var
PathSemArq:String;
begin
     if (ListaArquivos.ItemIndex=-1)
     Then Begin
               Messagedlg('Escolha um arquivo para ser restaurado!',mtinformation,[mbok],0);
               exit;
          End;

          
     If (Messagedlg('A Restaura��o de Backup deve ser executada na m�quina local. Se voc� tiver certeza disto e que deseja restaurar o Backup pressione "Sim", caso contr�rio pressione "N�o"!',mtConfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     With RestoreService do
     Begin
          Active:=false;
          LoginPrompt:=FALSE;

          If (VerificaBaseRemota(FDataModulo.IBDatabase.DatabaseName)=False)
          Then Begin
                    //local

                     PathSemArq:='';
                     PathSemArq:=ExtractFileDir(FDataModulo.IBDatabase.DatabaseName);
                     If (PathSemArq[length(pathsemarq)]<>'\')
                     Then PathSemArq:=PathSemArq+'\';
                     pathsemarq:=PathSemArq+'RestoreDatabase.gdb';

                     If (Messagedlg('Local e nome da Base a ser restaurada: '+PathSemArq+#13+'Continuar?',mtConfirmation,[mbyes,mbno],0)=mrno)
                     Then exit;

                     DatabaseName.Strings[0]:=PathSemArq;
                     Protocol:=Local;

                     ServerName:='';
                     BackupFile.clear;
                     BackupFile.add(ListaArquivos.FileName);
               end;
 {              End
          Else Begin
                    DatabaseName:=Uessencial.SeparaPath(FDataModulo.IBDatabase.DatabaseName);
                    protocol:=TCP;
                    servername:=Uessencial.SeparaIp(FDataModulo.IBDatabase.DatabaseName);
                    BackupFile.clear;
                    BackupFile.add(Uessencial.SeparaPathBackup(DatabaseName));
               End;}
          //****
          Params.Clear;
          Params.add('USER_NAME='+fdatamodulo.IBDatabase.Params.Values['USER_NAME']);
          Params.add('PASSWORD='+fdatamodulo.IBDatabase.Params.Values['PASSWORD']);
          Try
                  BtRestaurar.enabled:=false;
                  Active:=True;
                  ServiceStart;
                  Active:=false;
                  Messagedlg('Processamento Conclu�do!',mtinformation,[mbok],0);
          finally
                 BtRestaurar.enabled:=true;
          End;
     End;

end;

function TFbackup.PegaLocalNomeBackup: String;
var
dataehora:String;
caminho:String;
Begin
     caminho:='';
     caminho:=PegaLocalBackup;

     dataehora:=FormatDateTime('ddmmyyyyhhmm',now);
     dataehora:=dataehora+'.gbk';
     result:=caminho+Dataehora;
end;

Function TFbackup.SeparaPath(PathBase:String):String;
var
cont:integer;
Begin

     for cont:=1 to length(PathBase) do
     Begin
          If (PathBase[cont]=':')
          Then Break;
     End;
     result:=copy(pathbase,cont+1,length(pathbase));


End;

Function TFBackup.SeparaIp(PathBase:String):String;

var
cont:integer;
Begin

     for cont:=1 to length(PathBase) do
     Begin
          If (PathBase[cont]=':')
          Then Break;
     End;
     result:=copy(pathbase,1,cont-1);
End;

Function TfBAckup.SeparaPathBackup(PathBase:String):String;
var
conta,cont:integer;
AuxilioStr:String;
Begin
     auxiliostr:='';
     conta:=0;
     for cont:=length(PathBase) downto 1 do
     Begin
          If (PathBase[cont]='\')
          Then Begin
                    inc(conta,1);
                    If (conta=2)//exe. c:\sysmaster\dados\data.gdb
                    Then Begin
                              auxiliostr:=copy(pathbase,1,cont);
                              break;
                         End;
               End;

     End;
     auxiliostr:=auxiliostr+'backup\';
     result:=auxiliostr+FormatDateTime('dd-mm-yyyy-hh-mm',now)+'.gbk';

End;

function TFbackup.PegaLocalBackup: String;
var
caminho:String;
begin
     //podemos acrescentar um acesso a um arquivo que guardara
     //o path de backup, fazer teste de base remota e arquivo de backup local

     caminho:='';
     caminho:=ExtractFilePath(Application.exename);
     if (caminho[length(caminho)]<>'\')
     Then caminho:=caminho+'\';
     caminho:=caminho+'backup\';
     if (DirectoryExists(caminho)=False)
     Then Begin
               If (Messagedlg('O Diret�rio de Backup n�o existe! Deseja Cri�-lo?',mtconfirmation,[mbyes,mbno],0)=Mryes)
               Then Begin
                         If (createdir(caminho)=False)
                         Then Begin
                                Messagedlg('Erro na Tentativa de Cria��o de Diret�rio!',mterror,[mbok],0);
                                caminho:='';
                              End;
                   End
               Else caminho:='';
          End;
     result:=caminho;
end;




end.
