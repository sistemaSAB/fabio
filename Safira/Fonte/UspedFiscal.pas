unit UspedFiscal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, Mask, ExtCtrls,UobjSpedFiscal, jpeg, ComCtrls,
  Grids, DBGrids, DB, GIFImage, pngimage, Menus,IBQuery, ImgList,
  ToolWin,IniFiles, StdActns, ActnList, XPStyleActnCtrls, ActnMan,UobjSpedFiscal_Novo;

type
  TfSpedFiscal = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label2: TLabel;
    comboFinalidade: TComboBox;
    comboPerfil: TComboBox;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    edtDataInicial: TMaskEdit;
    edtDataFinal: TMaskEdit;
    edtAno: TEdit;
    Image1: TImage;
    StatusBar1: TStatusBar;
    pageSped: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    Panel4: TPanel;
    Label8: TLabel;                          
    Splitter1: TSplitter;
    panelRodape: TPanel;
    GroupBox3: TGroupBox;
    Label10: TLabel;
    edBufLinhas: TEdit;
    Label11: TLabel;
    edBufNotas: TEdit;
    GroupBox4: TGroupBox;
    comboAtividade: TComboBox;
    Label12: TLabel;
    dsSaida: TDataSource;
    dsEntrada: TDataSource;
    TabSheet3: TTabSheet;
    memoErros: TMemo;
    PanelErros: TPanel;
    Label13: TLabel;
    lbTotalErros: TLabel;
    Label15: TLabel;
    lbTotalAdvertencia: TLabel;
    GroupBox5: TGroupBox;
    checkVisualizaAposGeracao: TCheckBox;
    checkMostraNF: TCheckBox;
    Image3: TImage;
    Image4: TImage;
    lbAtencao: TLabel;
    Image2: TImage;
    Bevel1: TBevel;
    SaveDialog1: TSaveDialog;
    TabSheet4: TTabSheet;
    dbGridEntrada: TDBGrid;
    dbGridSaida: TDBGrid;
    edtPesquisa: TEdit;
    treeViewNF: TTreeView;
    Splitter2: TSplitter;
    Panel5: TPanel;
    GroupBox8: TGroupBox;
    Label31: TLabel;
    Label33: TLabel;
    Label35: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    GroupBox9: TGroupBox;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    pageDetalhesNF: TPageControl;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    TabSheet10: TTabSheet;
    Panel2: TPanel;
    popCorrecao: TPopupMenu;
    Saida1: TMenuItem;
    btCorrecaoErros: TSpeedButton;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    edtNumeroNF: TEdit;
    edtSerie: TEdit;
    edtDataEmissao: TEdit;
    edtValorFinalNota: TEdit;
    edtCertificado: TEdit;
    edtArquivoXML: TEdit;
    edtReciboEnvio: TEdit;
    edtChaveAcesso: TEdit;
    edtSituacao: TEdit;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    edtNomeEmit: TEdit;
    edtFantasiaEmit: TEdit;
    edtCNPJEmit: TEdit;
    edtEnderecoEmit: TEdit;
    edtBairroEmit: TEdit;
    edtCEPEmit: TEdit;
    edtCidadeEmit: TEdit;
    edtTelefoneEmit: TEdit;
    edtUFEmit: TEdit;
    edtPaisEmit: TEdit;
    edtIEEmit: TEdit;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    edtNomeDest: TEdit;
    edtCNPJDest: TEdit;
    edtEnderecoDest: TEdit;
    edtBairroDest: TEdit;
    edtCEPDest: TEdit;
    edtMunicipaioDest: TEdit;
    edtFoneDest: TEdit;
    edtUFDest: TEdit;
    edtPaisDest: TEdit;
    edtIEDest: TEdit;
    Label97: TLabel;
    Bevel2: TBevel;
    GroupBox6: TGroupBox;
    Label99: TLabel;
    lbCodigoNFSaidaSistema: TLabel;
    lbVersaoXML: TLabel;
    Label98: TLabel;
    edtNomeModelo: TEdit;
    SpeedButton1: TSpeedButton;
    Label32: TLabel;
    lbCodigoDestC: TLabel;
    Label20: TLabel;
    lbCodigoDestF: TLabel;
    Label9: TLabel;
    lbCodigoNFEntradaSistema: TLabel;
    Label14: TLabel;
    edtDataide: TEdit;
    Label16: TLabel;
    lbValorTotalBCICMS: TLabel;
    lbValorTotalICMS: TLabel;
    Label19: TLabel;
    lbValorTotalProdutos: TLabel;
    lbValorTotalFrete: TLabel;
    Label23: TLabel;
    lbValorTotalNF: TLabel;
    lbValorTotalDesconto: TLabel;
    Image5: TImage;
    Label17: TLabel;
    TabSheet9: TTabSheet;
    ImageList1: TImageList;
    Panel6: TPanel;
    Panel8: TPanel;
    ToolBar1: TToolBar;
    btLocalizar: TToolButton;
    ToolButton2: TToolButton;
    btVaiParaLinha: TToolButton;
    ToolButton4: TToolButton;
    btZoomMais: TToolButton;
    btZoomMenos: TToolButton;
    ToolButton8: TToolButton;
    btProximoTipo: TToolButton;
    rich1: TRichEdit;
    Panel7: TPanel;
    Label7: TLabel;
    Label18: TLabel;
    lbLinha: TLabel;
    lbColuna: TLabel;
    Bevel3: TBevel;
    Label24: TLabel;
    lbTipoRegistro: TLabel;
    Bevel4: TBevel;
    Label30: TLabel;
    lbPosInicial: TLabel;
    Label36: TLabel;
    lbPosFinal: TLabel;
    Bevel5: TBevel;
    Label77: TLabel;
    lbDescricaoRegistro: TLabel;
    FindDialog1: TFindDialog;
    btTipoAnterior: TToolButton;
    FontDialog1: TFontDialog;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    btImportaArquivo: TToolButton;
    ToolButton7: TToolButton;
    OpenDialog1: TOpenDialog;
    ToolButton5: TToolButton;
    btSelecionaIntervalo: TToolButton;
    ToolButton6: TToolButton;
    btLimpaRegistro: TToolButton;
    GroupBox7: TGroupBox;
    Label29: TLabel;
    Label34: TLabel;
    GroupBox10: TGroupBox;
    Label40: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label80: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    GroupBox11: TGroupBox;
    GroupBox12: TGroupBox;
    Label21: TLabel;
    Label22: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    Label86: TLabel;
    Label87: TLabel;
    edtNomeTransportadora: TEdit;
    edtCNPJCPFTransportadora: TEdit;
    edtIETransportadora: TEdit;
    edtMunicipioTransportadora: TEdit;
    edtEnderecoTransportadora: TEdit;
    edtUFTransportadora: TEdit;
    edtPlacaVeiculoTransportadora: TEdit;
    edtUFVeiculoTransportadora: TEdit;
    checkPorContaDestinatario: TRadioButton;
    checkPorContaEmitente: TRadioButton;
    edtQuantidadeVolumesTransportados: TEdit;
    edtEspecieVolumesTransportados: TEdit;
    edtMarcaVolumesTransportados: TEdit;
    edtNumeracaoVolumesTransportados: TEdit;
    edtPesoBrutoVolumesTransportados: TEdit;
    edtPesoLiquidoVolumesTransportados: TEdit;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    GroupBox13: TGroupBox;
    Label83: TLabel;
    Label88: TLabel;
    edtOBrigacoesICMS: TEdit;
    edtCodigoReceita: TEdit;
    GroupBox14: TGroupBox;
    checkRegistroH: TCheckBox;
    TabSheet11: TTabSheet;
    Image6: TImage;
    Label89: TLabel;
    Image7: TImage;
    Label90: TLabel;
    panelDataInventario: TPanel;
    Label91: TLabel;
    lbDataInventarioINI: TLabel;
    Label92: TLabel;
    lbDataInventarioFIN: TLabel;
    Image8: TImage;
    Label93: TLabel;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    SpeedButton2: TSpeedButton;
    Label94: TLabel;
    comboRegistro: TComboBox;
    Label95: TLabel;
    comboCampos: TComboBox;
    Label96: TLabel;
    edtNovoValor: TEdit;
    memoCondicoes: TMemo;
    Label100: TLabel;
    Label101: TLabel;
    lbPosicao: TLabel;
    ActionManager1: TActionManager;
    SearchFind1: TSearchFind;
    SearchFindNext1: TSearchFindNext;
    SearchReplace1: TSearchReplace;
    SearchFindFirst1: TSearchFindFirst;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    Notafiscal1: TMenuItem;
    AcertaBCICMS1: TMenuItem;
    comboTipoArquivo: TComboBox;
    ToolButton15: TToolButton;
    ListView1: TListView;
    ListViewEstoque: TListView;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure checkMostraNFClick(Sender: TObject);
    procedure edtDataInicialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDataFinalKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Timer1Timer(Sender: TObject);
    procedure pageSpedChange(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure imgGerarArquivoClick(Sender: TObject);
    procedure StatusBar1DrawPanel(StatusBar: TStatusBar;
      Panel: TStatusPanel; const Rect: TRect);
    procedure dbGridSaidaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbGridEntradaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Unidadedemedida1Click(Sender: TObject);
    procedure ImpostoICMS1Click(Sender: TObject);
    procedure Dadosdesaida1Click(Sender: TObject);
    procedure AcertaCFOPdeentrada1Click(Sender: TObject);
    procedure treeViewNFDblClick(Sender: TObject);
    procedure lbCodigoNFSaidaSistemaClick(Sender: TObject);
    procedure lbCodigoNFSaidaSistemaMouseLeave(Sender: TObject);
    procedure lbCodigoNFSaidaSistemaMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure edtSituacaoChange(Sender: TObject);
    procedure btCorrecaoErrosClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure lbCodigoDestCMouseLeave(Sender: TObject);
    procedure lbCodigoDestFMouseLeave(Sender: TObject);
    procedure lbCodigoDestCMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure lbCodigoDestFMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure lbCodigoDestCClick(Sender: TObject);
    procedure lbCodigoDestFClick(Sender: TObject);
    procedure lbCodigoNFEntradaSistemaMouseLeave(Sender: TObject);
    procedure lbCodigoNFEntradaSistemaMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure lbCodigoNFEntradaSistemaClick(Sender: TObject);
    procedure FindDialog1Find(Sender: TObject);
    procedure rich1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure rich1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure rich1SelectionChange(Sender: TObject);
    procedure btVaiParaLinhaClick(Sender: TObject);
    procedure btZoomMaisClick(Sender: TObject);
    procedure btZoomMenosClick(Sender: TObject);
    procedure btProximoTipoClick(Sender: TObject);
    procedure btTipoAnteriorClick(Sender: TObject);
    procedure Vaiparalinha1Click(Sender: TObject);
    procedure Aprximar1Click(Sender: TObject);
    procedure Afastar1Click(Sender: TObject);
    procedure Prximotipo1Click(Sender: TObject);
    procedure ipoanterior1Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure btImportaArquivoClick(Sender: TObject);
    procedure rich1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btSelecionaIntervaloClick(Sender: TObject);
    procedure btLimpaRegistroClick(Sender: TObject);
    procedure ToolButton9Click(Sender: TObject);
    procedure edtOBrigacoesICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCodigoReceitaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDataInicialExit(Sender: TObject);
    procedure lbDescricaoRegistroMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure edtOBrigacoesICMSChange(Sender: TObject);
    procedure Label89MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Label89MouseLeave(Sender: TObject);
    procedure Label90MouseLeave(Sender: TObject);
    procedure Label90MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbDataInventarioINIMouseLeave(Sender: TObject);
    procedure lbDataInventarioFINMouseLeave(Sender: TObject);
    procedure lbDataInventarioINIMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure lbDataInventarioFINMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure lbDataInventarioINIClick(Sender: TObject);
    procedure lbDataInventarioFINClick(Sender: TObject);
    procedure Label93MouseLeave(Sender: TObject);
    procedure Label93MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Label93Click(Sender: TObject);
    procedure ToolButton12Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure comboRegistroExit(Sender: TObject);
    procedure comboCamposEnter(Sender: TObject);
    procedure comboCamposChange(Sender: TObject);
    procedure comboRegistroEnter(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure Notafiscal1Click(Sender: TObject);
    procedure AcertaBCICMS1Click(Sender: TObject);
  private

    confSped:TIniFile;
    dir:Integer;
    NOMETABELA:string;

    procedure setNomeTabela;
    procedure gerarSpedFiscal();
    procedure gerarSpedFiscal_novo();
    procedure pesquisaNoGrid(gridParametro:TDBGrid;var Key: Word);
    procedure carregaConfSped();
    procedure gravaConfSped();
    procedure NFSaidaParaControles(query:TibQuery);
    procedure NFENtradaParaControles(query:TibQuery);
    procedure limpaEdit();
    procedure limpaLabels();
    procedure limpaLabelsVizualizador();
    procedure pegaLinhaColuna();
    procedure selecionaIntervalo();
    procedure atualizaPosicaoInicialFinal();
    procedure posicionaProximoRegistro();
    procedure posicionaRegistroAnterior();
    procedure substituir ();


    function pegaPosicaoFinal(pStr:string;busca:string):Integer;
    function pegaPosicaoInicial(pStr:string;busca:string):Integer;
    function selecaoInicial(i:integer):integer;
    function selecaoFinal(i:Integer):integer;
    function pegaTipoRegistro():string;
    function pegaPosicaoTipo():integer;
    function pegaDescricaoRegistro(pTipoRegistro:string):string;
    function carregaArquivoSpedParaVizualizador(pCaminho:string):Boolean;

    function localiza (posicao:integer;str:string):Integer;
    function validaRegistros: boolean;

  public
    objSpedFiscal:TobjSpedFiscal;
    objSpedFiscal_Novo:TObjSpedFiscal_Novo;
  end;


var
  fSpedFiscal: TfSpedFiscal;

implementation

uses ACBrSpedFiscal,ACBrEFDBlocos,UDataModulo, UobjEMPRESA, ACBrEFDBloco_C,
  DateUtils, ACBrEFDBloco_H_Class, ACBrEFDBloco_H, Uprincipal,
  UessencialGlobal, UEntradaProdutos, UNotaFiscal,
  UCorrecaoCFOPEntradaProdutos, uCorrecaoCFOPSaida, UUNIDADEMEDIDA,ShellAPI,
  UCliente, UFornecedor, Upesquisa, UOBRIGACOESICMS,
  UCODIGORECEITAESTADUAL;

{$R *.dfm}

procedure TfSpedFiscal.FormShow(Sender: TObject);
var
  dataIni,dataFin:string;
begin

  {antigo}
  begin

    objSpedFiscal        := TobjSpedFiscal.create;
    objSpedFiscal.status := StatusBar1;
    objSpedFiscal.submit_dataSourceNFEntrada(Self.dsEntrada);
    objSpedFiscal.submit_dataSourceNFSaida(Self.dsSaida);
    objSpedFiscal.submit_memoErros (self.memoErros);

  end;


  pageSped.TabIndex       := 0;
  pageDetalhesNF.TabIndex := 0;
  comboTipoArquivo.ItemIndex := 0;

  self.edtDataInicial.SetFocus;

  self.edtAno.Text := FormatDateTime('yyyy',now);

  confSped := TIniFile.Create(ExtractFilePath(Application.ExeName)+'\confSped.ini');
  self.carregaConfSped();
  self.limpaLabels;

  primeiroUltimoDia(FormatDateTime('mm',Date),FormatDateTime('yyyy',date),dataIni,dataFin);

  self.edtDataInicial.Text:=dataIni;
  self.edtDataFinal.Text:=dataFin;

  Panel8.Height:=12;

end;

procedure TfSpedFiscal.FormClose(Sender: TObject;var Action: TCloseAction);
begin

  self.gravaConfSped;

  objSpedFiscal.free;
  
  FreeAndNil(confSped);
  
end;

procedure TfSpedFiscal.FormKeyPress(Sender: TObject; var Key: Char);
begin

  if (self.dbGridSaida.Focused) or (self.dbGridEntrada.Focused) then Exit;

  if (key = #13) then
    Perform(Wm_NextDlgCtl,0,0);
                                       

end;

procedure TfSpedFiscal.checkMostraNFClick(Sender: TObject);
begin

  objSpedFiscal.mostraNFnoGrid(self.checkMostraNF.Checked);

end;

procedure TfSpedFiscal.edtDataInicialKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin

  if (Key = vk_space) then
    edtDataInicial.Text:=DateToStr(date);


end;

procedure TfSpedFiscal.edtDataFinalKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
  dataIni,dataFIn:string;
begin
  try

    Screen.Cursor:=crHourGlass;

    if (Key = vk_space) then
      edtDataFinal.Text:=DateToStr(date)

    else if (Key = vk_return) then
    begin

       if (self.checkMostraNF.Checked) then
       begin

        objSpedFiscal.submit_DATAINI(self.edtDataInicial.Text);
        objSpedFiscal.submit_DATAFIN(self.edtDataFinal.Text);

        objSpedFiscal.abreQueryNFentrada;
        objSpedFiscal.abreQueryNFSaida;
        self.pageDetalhesNF.TabIndex:=0;
        Application.ProcessMessages;
        objSpedFiscal.criarTreeView(treeViewNF);


        //primeiroUltimoDia ('12',IntToStr (strtoint(edtAno.Text)-1),dataini,dataFIn);

        panelDataInventario.Visible:=True;
        //lbDataInventarioINI.Caption := '01/01/'+IntToStr (strtoint(edtAno.Text)-1);
        //lbDataInventarioFIN.Caption := dataFIn;

        lbDataInventarioINI.Caption := self.edtDataInicial.Text;
        lbDataInventarioFIN.Caption := self.edtDataFinal.Text;

        objSpedFiscal.submit_DATAINI_ESTOQUE(lbDataInventarioINI.Caption);
        objSpedFiscal.submit_DATAFIN_ESTOQUE(lbDataInventarioFIN.Caption);
        objSpedFiscal.criaListViewEstoque(listViewEstoque);

       end;

    end;

  finally

    Screen.Cursor:=crDefault;

  end;

end;

procedure TfSpedFiscal.gerarSpedFiscal;
begin

  SaveDialog1.FileName:='';
  self.memoErros.Clear;
  self.lbTotalErros.Caption:='0';
  self.lbTotalAdvertencia.Caption:='0';

  SaveDialog1.InitialDir:= ExtractFileDir(FDataModulo.IBDatabase.DatabaseName);

  if (SaveDialog1.Execute) then
  begin

      with (objSpedFiscal) do
      begin

        submit_DATAINI(self.edtDataInicial.Text);
        submit_DATAFIN(self.edtDataFinal.Text);
        submit_pathDestino(SaveDialog1.FileName);
        submit_COD_FIN(comboFinalidade.Items[comboFinalidade.itemindex][1]);
        submit_IND_PERFIL(comboPerfil.Items[comboPerfil.itemindex][1]);
        submit_IND_ATIV(comboAtividade.Items[comboAtividade.itemindex][1]);
        submit_linhasBuffer(self.edBufLinhas.Text);
        submit_buffernotas(self.edBufNotas.Text);
        submit_cod_obrigacoes_icms(self.edtOBrigacoesICMS.Text);
        submit_COD_RECEITAESTADUAL(self.edtCodigoReceita.Text);

        submit_geraRegistroH (self.checkRegistroH.Checked);
        submit_DATAINI_ESTOQUE(lbDataInventarioINI.Caption);
        submit_DATAFIN_ESTOQUE(lbDataInventarioFIN.Caption);

        Application.ProcessMessages;

        try

          Screen.Cursor:=crHourGlass;
          
          validaDados_antesDaGeracao();
          lbTotalErros.Caption:=IntToStr(strERROS.Count);
          lbTotalAdvertencia.Caption:=IntToStr(strAdvertencia.Count);

          if (strERROS.Count <= 0) then
            begin

              Application.ProcessMessages;
              
              if not (gravaBlocos()) then
                begin

                  MensagemErro(msgERRO);

                end
              else
              begin

                if (strERROS.Count <= 0) then
                  begin

                    Self.StatusBar1.Panels[1].Text:='Arquivo sped gerado com sucesso: "'+self.SaveDialog1.FileName+'"';
                    Application.ProcessMessages;

                    if (checkVisualizaAposGeracao.Checked) then
                    begin

                      self.pageSped.TabIndex:=4;
                      if not(self.carregaArquivoSpedParaVizualizador(self.SaveDialog1.FileName)) then
                      begin

                        MensagemErro('N�o foi possivel abrir o carquivo sped. Vizualize manualmente');
                        Exit;

                      end;

                    end;

                  end
                else
                begin

                  lbTotalErros.Caption:=IntToStr(strERROS.Count);
                  carregaErrosMemo();
                  Self.StatusBar1.Panels[1].Text:='A gera��o do arquivo foi interrompida. Erros foram encontrados';
                  self.pageSped.TabIndex:=2;

                end;

              end;
                                       
            end
          else
          begin

            Self.StatusBar1.Panels[1].Text:='Fim da valida��o. Erros foram encontrados';
            self.pageSped.TabIndex:=2;

          end;

        finally

          Screen.Cursor:=crDefault;

        end;
  
      end;                                       

  end;

end;

procedure TfSpedFiscal.gerarSpedFiscal_novo();
begin

  objSpedFiscal_Novo := TObjSpedFiscal_Novo.Create(self,FDataModulo.IBDatabase);
  
  try

    objSpedFiscal_Novo.status := StatusBar1;
    objSpedFiscal_Novo.memoErros := memoErros;


    SaveDialog1.FileName:='';
    self.memoErros.Clear;
    self.lbTotalErros.Caption:='0';
    self.lbTotalAdvertencia.Caption:='0';

    SaveDialog1.InitialDir:= ExtractFileDir(FDataModulo.IBDatabase.DatabaseName);

    if (SaveDialog1.Execute) then
    begin

        with (objSpedFiscal_Novo) do
        begin

          DataIni := StrToDate(edtDataInicial.Text);
          DataFin := StrToDate(edtDataFinal.Text);

          PathArquivo  :=  SaveDialog1.FileName;
          CodFin       :=  comboFinalidade.Items[comboFinalidade.itemindex][1];
          IndPerfil    :=  comboPerfil.Items    [comboPerfil.itemindex][1];
          IndAtiv      :=  comboAtividade.Items [comboAtividade.itemindex][1];
          linhasBuffer :=  StrToInt(edBufLinhas.Text);
          notasBuffer  :=  StrToInt(edBufNotas.Text);

          COD_OBRIGACOES_ICMS :=  edtOBrigacoesICMS.Text;
          COD_RECEITAESTADUAL :=  edtCodigoReceita.Text;

          {PIS/COFINS}
          //objSpedFiscal_Novo.pPIS    := StrToCurr(objEmpresaGlobal.get_pis);
          //objSpedFiscal_Novo.pCOFINS := StrToCurr(ObjEmpresaGlobal.Get_Cofins);

          objSpedFiscal_Novo.software := 'SAB';

          {REGISTRO H}
          geraRegistroH  := checkRegistroH.Checked;
          if geraRegistroH then
          begin
            DataIniEstoque := StrToDate(lbDataInventarioINI.Caption);
            DataFinEstoque := StrToDate(lbDataInventarioFIN.Caption);
          end;

          Application.ProcessMessages;

          try

            Screen.Cursor:=crHourGlass;

            if (strERROS.Count <= 0) then
            begin

                Application.ProcessMessages;

                if not (geraSped) then
                begin

                  lbTotalErros.Caption       := IntToStr(strERROS.Count);
                  lbTotalAdvertencia.Caption := IntToStr(strAdvertencia.Count);
                  carregaErrosMemo;
                  self.pageSped.TabIndex := 2;

                end
                else
                begin

                  if (strERROS.Count <= 0) then
                    begin

                      Self.StatusBar1.Panels[1].Text:='Arquivo sped gerado com sucesso: "'+self.SaveDialog1.FileName+'"';
                      Application.ProcessMessages;

                      if (checkVisualizaAposGeracao.Checked) then
                      begin

                        self.pageSped.TabIndex:=4;
                        if not(self.carregaArquivoSpedParaVizualizador(self.SaveDialog1.FileName)) then
                        begin

                          MensagemErro('N�o foi possivel abrir o carquivo sped. Vizualize manualmente');
                          Exit;

                        end;

                      end;

                    end
                  else
                  begin

                    lbTotalErros.Caption := IntToStr(strERROS.Count);
                    carregaErrosMemo();
                    Self.StatusBar1.Panels[1].Text:='A gera��o do arquivo foi interrompida. Erros foram encontrados';
                    self.pageSped.TabIndex:=2;

                  end;

                end;

            end
            else
            begin

              Self.StatusBar1.Panels[1].Text:='Fim da valida��o. Erros foram encontrados';
              self.pageSped.TabIndex:=2;

            end;

          finally

            Screen.Cursor:=crDefault;

          end;

        end;

    end;

  finally
    FreeAndNil(objSpedFiscal_Novo);
  end;

end;

procedure TfSpedFiscal.Timer1Timer(Sender: TObject);
begin

  Application.ProcessMessages;

end;

procedure TfSpedFiscal.pageSpedChange(Sender: TObject);
begin

  case self.pageSped.TabIndex of

    0:begin


      end;

    1:begin

      end;

    2:begin

        if (self.memoErros.Text = '') then
          pageSped.TabIndex:=0;


        if (self.lbTotalErros.Caption = '0') then
          lbAtencao.Visible:=False
        else
          lbAtencao.Visible:=True;


      end;

    3:begin

         Exit;
        {self.pageSped.TabIndex:=0;
        MensagemAviso('Em desenvolvimento');
        Exit;}
        self.objSpedFiscal.criarTreeView(treeViewNF);

      end;

    4:begin

        self.limpaLabelsVizualizador();

      end;

  end;


end;

procedure TfSpedFiscal.Image2Click(Sender: TObject);
begin

  SaveDialog1.FileName:='';

  try

    if (SaveDialog1.Execute) then
      self.memoErros.Lines.SaveToFile(SaveDialog1.FileName);

    if (MensagemPergunta('Lista salva, deseja abrir?') = mrYes) then
    begin

      if not (FileExists(SaveDialog1.FileName)) then
        MensagemAviso('Ops, ocorreu um erro ao abrir o arquivo, verifique se o mesmo existe')
      else
        ShellExecute(Application.Handle,PChar('open'),PChar('explorer.exe'),PChar(SaveDialog1.FileName),nil,SW_SHOWNORMAL);

    end;

  except

    Application.MessageBox('Erro ao salvar lista','ERRO',MB_ICONERROR);

  end;


end;

procedure TfSpedFiscal.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if (Key = vk_f1) then
    Fprincipal.chamaPesquisaMenu();

end;


procedure TfSpedFiscal.imgGerarArquivoClick(Sender: TObject);
begin

  if (validaRegistros) then
    gerarSpedFiscal_novo
  else
    pageSped.TabIndex := 2;

end;

procedure TfSpedFiscal.StatusBar1DrawPanel(StatusBar: TStatusBar;Panel: TStatusPanel; const Rect: TRect);
begin

  if (panel = StatusBar1.Panels[0]) then
  begin
    //StatusBar1.Canvas.Brush.Color:=$005CADFE;
    StatusBar1.Canvas.FillRect(Rect);
    //StatusBar1.Canvas.Font.Color:=$005CADFE;
    StatusBar1.Canvas.Font.Style:=[fsBold];
    StatusBar1.Canvas.TextOut(Rect.Left,Rect.Top,StatusBar1.Panels[0].Text);
  end;

  if (panel = StatusBar1.Panels[1]) then
  begin
    //StatusBar1.Canvas.Brush.Color:=$005CADFE;
    StatusBar1.Canvas.FillRect(Rect);
    //StatusBar1.Canvas.Font.Color:=$005CADFE;
    StatusBar1.Canvas.Font.Style:=[fsBold];
    StatusBar1.Canvas.TextOut(Rect.Left,Rect.Top,StatusBar1.Panels[1].Text);
  end;


end;


procedure TfSpedFiscal.dbGridSaidaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  if (Key = vk_space) then
    begin

      self.edtPesquisa.Left := self.dbGridSaida.Left + (self.dbGridSaida.Width - self.edtPesquisa.Width) - 2;
      self.edtPesquisa.Top  := self.dbGridSaida.Top  + (self.dbGridSaida.Height - self.edtPesquisa.Height) - 1;
      self.edtPesquisa.Visible:=True;
      self.edtPesquisa.SetFocus;

    end
  else if (Key = vk_return) then
    chamaFormulario(TFNotaFiscal,self,self.dbGridSaida.DataSource.DataSet.fieldbyname('codigo').AsString);


end;

procedure TfSpedFiscal.edtPesquisaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  if (Key = vk_return) then
    begin

      self.pesquisaNoGrid(self.dbGridSaida,key)

    end
  else if (Key = vk_escape) then
  begin

    Self.edtPesquisa.Visible:=False;

  end;

end;

procedure TfSpedFiscal.pesquisaNoGrid(gridParametro: TDBGrid;var Key: Word);
begin

  if (self.edtPesquisa.Text = '') then Exit;

  if (Key = vk_escape) then
    begin

      self.edtPesquisa.Visible:=False;

    end
  else if (Key = vk_return) then
  begin

    if not (gridParametro.DataSource.DataSet.Locate(gridParametro.Columns[gridParametro.SelectedIndex].Title.Caption,SELF.edtPesquisa.Text,[loPartialKey,loCaseInsensitive])) then
      MensagemAviso('Nenhum registro encontrado: '+'"'+self.edtPesquisa.Text+'"')
    else
    begin
      gridParametro.OnCellClick(nil);
      self.edtPesquisa.Visible:=False;
    end;


  end;

  gridParametro.SetFocus;

end;

procedure TfSpedFiscal.carregaConfSped;
begin

  self.comboFinalidade.ItemIndex := confSped.ReadInteger('DETALHES DA GERACAO','tipo',0);
  self.comboPerfil.ItemIndex     := confSped.ReadInteger('DETALHES DA GERACAO','perfil',0);

  self.checkVisualizaAposGeracao.Checked := StrToBool(confSped.ReadString('GERACAO','visualizar arquivo apos geracao','0'));
  self.checkMostraNF.Checked             := StrToBool(confSped.ReadString('GERACAO','mostra nota de entrada e saida','1'));
  self.checkRegistroH.Checked            := StrToBool(confSped.ReadString('GERACAO','gera blocos do registro H','0'));

  self.comboAtividade.ItemIndex := confSped.ReadInteger('INDICADOR DE ATIVIDADE','atividade',1);

  self.edBufLinhas.Text := confSped.ReadString('BUFFER','linhas','1000');
  self.edBufNotas.Text  := confSped.ReadString('BUFFER','notas','1000');

  self.edtOBrigacoesICMS.Text := confSped.ReadString('CODIGO DA OBRIGACAO A RECOLHER','codigo','000');
  self.edtCodigoReceita.Text  := confSped.ReadString('CODIGO DE RECEITA','codigo','310');

end;

procedure TfSpedFiscal.gravaConfSped;
begin

  confSped.WriteString('DETALHES DA GERACAO','tipo',IntToStr(self.comboFinalidade.itemIndex));
  confSped.WriteString('DETALHES DA GERACAO','perfil',IntToStr(self.comboPerfil.ItemIndex));

  confSped.WriteBool('GERACAO','visualizar arquivo apos geracao',self.checkVisualizaAposGeracao.Checked);
  confSped.WriteBool('GERACAO','mostra nota de entrada e saida',self.checkMostraNF.Checked);
  confSped.WriteBool('GERACAO','gera blocos do registro H',self.checkRegistroH.Checked);

  confSped.WriteString('INDICADOR DE ATIVIDADE','atividade',IntToStr(self.comboAtividade.ItemIndex));

  confSped.WriteString('BUFFER','linhas',self.edBufLinhas.Text);
  confSped.WriteString('BUFFER','notas',self.edBufNotas.Text);

  confSped.WriteString('CODIGO DA OBRIGACAO A RECOLHER','codigo',self.edtOBrigacoesICMS.Text);
  confSped.WriteString('CODIGO DE RECEITA','codigo',self.edtCodigoReceita.Text);

end;

procedure TfSpedFiscal.dbGridEntradaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  if (key = vk_return) then
    chamaFormulario(TFentradaProdutos,self,self.dbGridEntrada.DataSource.DataSet.fieldbyname('codigo').AsString);

end;

procedure TfSpedFiscal.Unidadedemedida1Click(Sender: TObject);
begin

  chamaFormulario(TFUNIDADEMEDIDA,self);

end;

procedure TfSpedFiscal.ImpostoICMS1Click(Sender: TObject);
begin

  chamaFormulario(TfCorrecaoCFOPSaida,self);

end;

procedure TfSpedFiscal.Dadosdesaida1Click(Sender: TObject);
begin
chamaFormulario(TfCorrecaoCFOPSaida,self);
end;

procedure TfSpedFiscal.AcertaCFOPdeentrada1Click(Sender: TObject);
begin
  //FCorrecaoCFOPEntradaProdutos.PassaObjeto(nil);
  FCorrecaoCFOPEntradaProdutos.ShowModal;
end;

procedure TfSpedFiscal.treeViewNFDblClick(Sender: TObject);
var
  pModelo:string;
  pTipoNota:string;
  pCodigoNota:string;
begin

  pModelo := '-1';

  if (self.treeViewNF.Selected = nil) then Exit;

  pModelo   := self.objSpedFiscal.pegaModeloTreeView(self.treeViewNF);
  pTipoNota := self.objSpedFiscal.pegaTipoNFTreeView(self.treeViewNF);

  if (pTipoNota = '') then
  begin

    MensagemAviso('N�o foi possivel pegar o tipo da NF (E-S)');
    Exit;
    
  end;

  if (pModelo = '') or (pModelo = '-1') then
  begin

    if pModelo = '' then
      MensagemAviso('N�o foi possivel pegar o modelo da NF)');

    Exit;

  end;

  self.limpaEdit;
  self.limpaLabels;

  if (pTipoNota = 'S') then
    begin

      self.NFSaidaParaControles (self.objSpedFiscal.localizaNFSaida(self.treeViewNF.Selected.Text,pModelo));
      self.objSpedFiscal.criaListViewProdutosSaida(lbCodigoNFSaidaSistema.Caption,self.ListView1);

    end
  else                                            
  if (pTipoNota = 'E') then
    begin

      self.NFENtradaParaControles(self.objSpedFiscal.localizaNFEntrada(self.treeViewNF.Selected.Text,pModelo));
      self.objSpedFiscal.criaListViewProdutosEntrada(lbCodigoNFEntradaSistema.Caption,Self.ListView1);

    end
  else
  begin

    MensagemAviso('Tipo da NF: '+pTipoNota+' desconhecido');
    Exit;
      
  end;

end;

procedure TfSpedFiscal.NFENtradaParaControles(query:TibQuery);
begin

  {ide}
  Self.edtNumeroNF.Text       := query.FieldByName('nf').AsString;
  self.edtDataide.Text        := query.FieldByName('data').AsString;
  self.edtDataEmissao.Text    := query.fieldbyname('emissaonf').AsString;
  self.edtValorFinalNota.Text := query.fieldbyname('valorfinal').AsString;
  self.edtNomeModelo.Text     := query.fieldbyname('modelo').AsString;

  {emitente}
  self.edtNomeEmit.Text       := ObjEmpresaGlobal.Get_RAZAOSOCIAL;
  self.edtFantasiaEmit.Text   := ObjEmpresaGlobal.Get_FANTASIA;
  self.edtCNPJEmit.Text       := ObjEmpresaGlobal.Get_CNPJ;
  self.edtEnderecoEmit.Text   := ObjEmpresaGlobal.Get_ENDERECO;
  self.edtBairroEmit.Text     := ObjEmpresaGlobal.get_bairro;
  self.edtCEPEmit.Text        := ObjEmpresaGlobal.Get_CEP;
  self.edtCidadeEmit.Text     := ObjEmpresaGlobal.Get_CIDADE;
  self.edtTelefoneEmit.Text   := ObjEmpresaGlobal.Get_FONE;
  self.edtUFEmit.Text         := ObjEmpresaGlobal.Get_ESTADO;
  {self.edtPaisEmit.Text       := ObjEmpresaGlobal}
  self.edtIEEmit.Text         := ObjEmpresaGlobal.Get_IE;



 {destinatario}
 self.edtNomeDest.Text       := query.FieldByName('razaosocial').AsString;
 self.edtCNPJDest.Text       := query.FieldByName('cgc').AsString;
 self.edtEnderecoDest.Text   := query.FieldByName('endereco').AsString;
 self.edtBairroDest.Text     := query.FieldByName('bairro').AsString;
 self.edtCEPDest.Text        := query.FieldByName('cep').AsString;
 self.edtMunicipaioDest.Text := query.FieldByName('cidade').AsString;
 self.edtFoneDest.Text       := query.FieldByName('fone').AsString;
 self.edtUFDest.Text         := query.FieldByName('estado').AsString;
 self.lbCodigoDestF.Caption  := query.FieldByName('codigo_fornecedor').AsString;

 if (query.FieldByName('ie').AsString <> '') then
  self.edtIEDest.Text  := query.FieldByName('ie').AsString
 else
  //self.edtIEDest.Text  := query.FieldByName('ieprodutorrural').AsString;
  self.edtIEDest.Text  := query.FieldByName('ie').AsString;


  {VALORES TOTAIS}
  lbValorTotalProdutos.Caption :=  get_valorF(query.FieldByName('valorprodutos').AsString);
  lbValorTotalFrete.Caption    :=  get_valorF(query.FieldByName('valorfrete').AsString);
  lbValorTotalBCICMS.Caption   :=  get_valorF(query.FieldByName('basecalculoicms').AsString);
  lbValorTotalICMS.Caption     :=  get_valorF(query.FieldByName('valoricms').AsString);
  lbValorTotalNF.Caption       :=  get_valorF(query.FieldByName('valorfinal').AsString);
  lbValorTotalDesconto.Caption :=  get_valorF(query.FieldByName('descontos').AsString);


  if (query.FieldByName('codigo_fiscal').AsString = '55') then
  begin
    self.edtSerie.Text := '1';
    self.lbVersaoXML.Caption:='2.00';
  end;


  self.lbCodigoNFEntradaSistema.Caption := query.FieldByName('codigo').AsString;


end;

procedure TfSpedFiscal.NFSaidaParaControles(query:TibQuery);
begin

  {ide}
  Self.edtNumeroNF.Text       := query.FieldByName('numero').AsString;
  self.edtChaveAcesso.Text    := query.FieldByName('chave_acesso').AsString;
  self.edtCertificado.Text    := query.FieldByName('certificado').AsString;
  self.edtArquivoXML.Text     := query.FieldByName('arquivo_xml').AsString;
  self.edtReciboEnvio.Text    := query.FieldByName('reciboenvio').AsString;
  self.edtDataEmissao.Text    := query.fieldbyname('dataemissao').AsString;
  self.edtSituacao.Text       := query.fieldbyname('situacao').AsString;
  self.edtValorFinalNota.Text := query.fieldbyname('valorfinal').AsString;
  self.edtNomeModelo.Text     := query.fieldbyname('modelo').AsString;

  {emitente}
  self.edtNomeEmit.Text       := ObjEmpresaGlobal.Get_RAZAOSOCIAL;
  self.edtFantasiaEmit.Text   := ObjEmpresaGlobal.Get_FANTASIA;
  self.edtCNPJEmit.Text       := ObjEmpresaGlobal.Get_CNPJ;
  self.edtEnderecoEmit.Text   := ObjEmpresaGlobal.Get_ENDERECO;
  self.edtBairroEmit.Text     := ObjEmpresaGlobal.get_bairro;
  self.edtCEPEmit.Text        := ObjEmpresaGlobal.Get_CEP;
  self.edtCidadeEmit.Text     := ObjEmpresaGlobal.Get_CIDADE;
  self.edtTelefoneEmit.Text   := ObjEmpresaGlobal.Get_FONE;
  self.edtUFEmit.Text         := ObjEmpresaGlobal.Get_ESTADO;
  {self.edtPaisEmit.Text       := ObjEmpresaGlobal}
  self.edtIEEmit.Text         := ObjEmpresaGlobal.Get_IE;

  if (query.FieldByName('codigo_cliente').AsString <> '') then
    begin

      {destinatario}
      self.edtNomeDest.Text       := query.FieldByName('nome_cliente').AsString;
      self.edtCNPJDest.Text       := query.FieldByName('cpf_cgc_cliente').AsString;
      self.edtEnderecoDest.Text   := query.FieldByName('endereco').AsString;
      self.edtBairroDest.Text     := query.FieldByName('bairro').AsString;
      self.edtCEPDest.Text        := query.FieldByName('cep').AsString;
      self.edtMunicipaioDest.Text := query.FieldByName('cidade').AsString;
      self.edtFoneDest.Text       := query.FieldByName('fone_cliente').AsString;
      self.edtUFDest.Text         := query.FieldByName('estado').AsString;
      self.edtIEDest.Text         := query.FieldByName('rg_ie').AsString;
      self.lbCodigoDestC.Caption   := query.FieldByName('codigo_cliente').AsString;

    end
  else
  begin

     {destinatario}
      self.edtNomeDest.Text       := query.FieldByName('razaosocial').AsString;
      self.edtCNPJDest.Text       := query.FieldByName('cgc_fornecedor').AsString;
      self.edtEnderecoDest.Text   := query.FieldByName('endereco').AsString;
      self.edtBairroDest.Text     := query.FieldByName('bairro').AsString;
      self.edtCEPDest.Text        := query.FieldByName('cep').AsString;
      self.edtMunicipaioDest.Text := query.FieldByName('cidade').AsString;
      self.edtFoneDest.Text       := query.FieldByName('fone_cliente').AsString;
      self.edtUFDest.Text         := query.FieldByName('estado').AsString;
      self.lbCodigoDestF.Caption  := query.FieldByName('codigo_fornecedor').AsString;

      if (query.FieldByName('rg_ie').AsString <> '') then
        self.edtIEDest.Text  := query.FieldByName('rg_ie').AsString
      else
        //self.edtIEDest.Text  := query.FieldByName('ieprodutorrural').AsString
        self.edtIEDest.Text  := query.FieldByName('rg_ie').AsString

  end;

  {VALORES TOTAIS}
  lbValorTotalProdutos.Caption :=  get_valorF(query.FieldByName('valortotal').AsString);
  lbValorTotalFrete.Caption    :=  get_valorF(query.FieldByName('valorfrete').AsString);
  lbValorTotalBCICMS.Caption   :=  get_valorF(query.FieldByName('basecalculoicms').AsString);
  lbValorTotalICMS.Caption     :=  get_valorF(query.FieldByName('valoricms').AsString);
  lbValorTotalNF.Caption       :=  get_valorF(query.FieldByName('valorfinal').AsString);
  lbValorTotalDesconto.Caption :=  get_valorF(query.FieldByName('desconto').AsString);


  if (query.FieldByName('codigo_fiscal').AsString = '55') then
  begin
    self.edtSerie.Text := '1';
    self.lbVersaoXML.Caption:='2.00';
  end;

  {TRANSPORTADORA}
  self.edtNomeTransportadora.Text             := query.FieldByName('NOMETRANSPORTADORA').AsString;
  self.edtCNPJCPFTransportadora.Text          := query.FieldByName('CNPJTRANSPORTADORA').AsString;
  self.edtIETransportadora.Text               := query.FieldByName('IETRANSPORTADORA').AsString;
  self.edtPlacaVeiculoTransportadora.Text     := query.FieldByName('PLACAVEICULOTRANSPORTADORA').AsString;
  self.edtEnderecoTransportadora.Text         := query.FieldByName('ENDERECOTRANSPORTADORA').AsString;
  self.edtMunicipioTransportadora.Text        := query.FieldByName('MUNICIPIOTRANSPORTADORA').AsString;
  self.edtUFTransportadora.Text               := query.FieldByName('UFTRANSPORTADORA').AsString;
  self.edtUFVeiculoTransportadora.Text        := query.FieldByName('UFVEICULOTRANSPORTADORA').AsString;
  self.edtQuantidadeVolumesTransportados.Text := query.FieldByName('QUANTIDADE').AsString;
  self.edtMarcaVolumesTransportados.Text      := query.FieldByName('MARCA').AsString;
  self.edtEspecieVolumesTransportados.Text    := query.FieldByName('ESPECIE').AsString;
  self.edtNumeracaoVolumesTransportados.Text  := query.FieldByName('NUMEROVOLUMES').AsString;
  self.edtPesoBrutoVolumesTransportados.Text  := query.FieldByName('PESOBRUTO').AsString;
  self.edtPesoLiquidoVolumesTransportados.Text:= query.FieldByName('PESOLIQUIDO').AsString;

  if (query.FieldByName('FRETEPORCONTATRANSPORTADORA').AsString = '1') then
    self.checkPorContaEmitente.Checked:=True

  else if (query.FieldByName('FRETEPORCONTATRANSPORTADORA').AsString = '2') then
    self.checkPorContaDestinatario.Checked:=True;

  self.lbCodigoNFSaidaSistema.Caption:=query.fieldbyname('codigo').AsString;


end;


procedure TfSpedFiscal.limpaEdit;
begin

  {ide}
  self.edtNumeroNF.Text:='';
  self.edtChaveAcesso.Text:='';
  self.edtCertificado.Text:='';
  self.edtArquivoXML.Text:='';
  self.edtReciboEnvio.Text:='';
  self.edtSerie.Text:='';
  self.edtDataEmissao.Text:='';
  self.edtSituacao.Text:='';
  self.edtValorFinalNota.Text:='';
  self.edtNomeModelo.Text:='';
  self.edtDataide.Text:='';

  {emitente}
  self.edtNomeEmit.Text     :='';
  self.edtCNPJEmit.Text     :='';
  Self.edtEnderecoEmit.Text :='';
  self.edtBairroEmit.Text   :='';
  self.edtCEPEmit.Text      :='';
  self.edtCidadeEmit.Text   :='';
  self.edtTelefoneEmit.Text :='';
  self.edtUFEmit.Text       :='';
  self.edtPaisEmit.Text     :='';
  self.edtIEEmit.Text       :='';

  {destinatario}
  self.edtNomeDest.Text       := '';
  self.edtCNPJDest.Text       := '';
  self.edtEnderecoDest.Text   := '';
  self.edtBairroDest.Text     := '';
  self.edtCEPDest.Text        := '';
  self.edtMunicipaioDest.Text := '';
  self.edtFoneDest.Text       := '';
  self.edtUFDest.Text         := '';
  self.edtPaisDest.Text       := '';
  self.edtIEDest.Text         := '';

  {transportadora}
  self.edtNomeTransportadora.Text             := '';
  self.edtCNPJCPFTransportadora.Text          := '';
  self.edtIETransportadora.Text               := '';
  self.edtPlacaVeiculoTransportadora.Text     := '';
  self.edtEnderecoTransportadora.Text         := '';
  self.edtMunicipioTransportadora.Text        := '';
  self.edtUFTransportadora.Text               := '';
  self.edtUFVeiculoTransportadora.Text        := '';
  self.edtQuantidadeVolumesTransportados.Text := '';
  self.edtMarcaVolumesTransportados.Text      := '';
  self.edtEspecieVolumesTransportados.Text    := '';
  self.edtNumeracaoVolumesTransportados.Text  := '';
  self.edtPesoBrutoVolumesTransportados.Text  := '';
  self.edtPesoLiquidoVolumesTransportados.Text:= '';
  self.checkPorContaDestinatario.Checked      :=False;
  self.checkPorContaEmitente.Checked          :=false;


end;

procedure TfSpedFiscal.limpaLabels;
begin

  self.lbCodigoNFSaidaSistema.Caption:='';
  self.lbCodigoNFEntradaSistema.Caption:='';
  self.lbVersaoXML.Caption:='';
  self.lbCodigoDestC.Caption:='';
  self.lbCodigoDestF.Caption:='';

  lbValorTotalProdutos.Caption :='';
  lbValorTotalFrete.Caption    :='';
  lbValorTotalBCICMS.Caption   :='';
  lbValorTotalICMS.Caption     :='';
  lbValorTotalNF.Caption       :='';
  lbValorTotalDesconto.Caption :='';

end;

procedure TfSpedFiscal.lbCodigoNFSaidaSistemaClick(Sender: TObject);
begin

  chamaFormulario(TFNotaFiscal,self,self.lbCodigoNFSaidaSistema.Caption);

end;

procedure TfSpedFiscal.lbCodigoNFSaidaSistemaMouseLeave(Sender: TObject);
begin

  Tlabel(Sender).Font.Style:=[];

end;

procedure TfSpedFiscal.lbCodigoNFSaidaSistemaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin

  Tlabel(Sender).Font.Style:=[fsBold,fsUnderline];

end;

procedure TfSpedFiscal.edtSituacaoChange(Sender: TObject);
begin

  if (edtSituacao.Text = 'I') or (edtSituacao.Text = 'G') then
    edtSituacao.Text:='Gerada'

  else if (edtSituacao.Text = 'T') then
    edtSituacao.Text:='Conting�ncia'

  else if (edtSituacao.Text = 'P') then
    edtSituacao.Text:='Em processamento'

  else if (edtSituacao.Text = 'Z') then
    edtSituacao.Text:='Inutilizada'

  else if (edtSituacao.Text = 'C') then
    edtSituacao.Text:='Cancelada';

end;

procedure TfSpedFiscal.btCorrecaoErrosClick(Sender: TObject);
begin

    self.popCorrecao.Popup(Left + (self.btCorrecaoErros.Left - 100), Top + (self.pageSped.Top + 30));

  {-->> o 100 � o width do popup, e o 30 � o heigth do popup (aproximadamente)}  

end;

procedure TfSpedFiscal.SpeedButton1Click(Sender: TObject);
begin

  if not (FileExists(self.edtArquivoXML.Text)) and Not (DirectoryExists(self.edtArquivoXML.Text)) then
    MensagemAviso('O arquivo ou diret�rio n�o existe')
  else
    ShellExecute(Application.Handle,PChar('open'),PChar('explorer.exe'),PChar(edtArquivoXML.Text),nil,SW_SHOWMAXIMIZED);

end;



procedure TfSpedFiscal.lbCodigoDestCMouseLeave(Sender: TObject);
begin

  Tlabel(Sender).Font.Style:=[];

end;

procedure TfSpedFiscal.lbCodigoDestFMouseLeave(Sender: TObject);
begin

  Tlabel(Sender).Font.Style:=[];

end;

procedure TfSpedFiscal.lbCodigoDestCMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin

  Tlabel(Sender).Font.Style:=[fsBold,fsUnderline];

end;

procedure TfSpedFiscal.lbCodigoDestFMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
Tlabel(Sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TfSpedFiscal.lbCodigoDestCClick(Sender: TObject);
begin

  chamaFormulario(TFcliente,self,lbCodigoDestC.Caption);

end;

procedure TfSpedFiscal.lbCodigoDestFClick(Sender: TObject);
begin

  chamaFormulario(TFFORNECEDOR,self,lbCodigoDestF.Caption);

end;

procedure TfSpedFiscal.lbCodigoNFEntradaSistemaMouseLeave(Sender: TObject);
begin
Tlabel(Sender).Font.Style:=[];
end;

procedure TfSpedFiscal.lbCodigoNFEntradaSistemaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
 Tlabel(Sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TfSpedFiscal.lbCodigoNFEntradaSistemaClick(Sender: TObject);
begin

  chamaFormulario(TFentradaProdutos,self,self.lbCodigoNFEntradaSistema.Caption);

end;

procedure TfSpedFiscal.FindDialog1Find(Sender: TObject);
var
  FoundAt, StartPos, ToEnd:Integer;
begin

  {SelLength devolve o n�mero de caracteres selecionados}
  {se h� caracteres selecionados em RichEdit1}
  {SelStart devolve a posi��o do primeiro caracter selecionado no texto}
  if (rich1.SelLength > 0) then
     StartPos:= rich1.SelStart + rich1.SelLength
  else
    StartPos:= 0;

  ToEnd:= Length (rich1.Text) - StartPos;

  if (frMatchCase in FindDialog1.Options) then
    FoundAt:= rich1.FindText(FindDialog1.FindText,StartPos, ToEnd,[stMatchCase])

  else if (frWholeWord in FindDialog1.Options) then
    FoundAt:= rich1.FindText(FindDialog1.FindText,StartPos, ToEnd,[stWholeWord])

  else
    FoundAt:= rich1.FindText(FindDialog1.FindText,StartPos, ToEnd,[]);

   if (FoundAt <> -1) then {se a vari�vel for difetente de -1}
    rich1.SetFocus();

   {coloca o cursor na primeira ocorr�ncia}
  rich1.SelStart:= FoundAt;

   {Seleciona a string procurada no texto do RichEdit1}
  rich1.SelLength:= Length(FindDialog1.FindText);

end;

procedure TfSpedFiscal.rich1KeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  {if (Key = 37) then
    dir:=-1
  else if (Key = 39) then
    dir:=1;

  if (Key in[37..40]) then
    self.selecionaIntervalo; }

end;

procedure TfSpedFiscal.rich1KeyUp(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  if (Key = 37) then {seta esquerda}
    dir:=-1
  else if (Key = 39) then {seta direita}
    dir:=2
  else if (Key = 40) or (Key = 38) then {seta baixo seta cima}
    dir:=0;

  if (StrToInt(lbLinha.Caption) > rich1.Lines.Count) then
    Exit;

  if (rich1.CaretPos.X+(dir) <= 0) then Exit;
  if (rich1.CaretPos.X+(dir) >= Length(rich1.Lines[rich1.caretpos.y])) then Exit;

  if (Key in[37..40]) then
  begin
    selecionaIntervalo();
    atualizaPosicaoInicialFinal();
  end;

  lbTipoRegistro.Caption:=self.pegaTipoRegistro();
  lbDescricaoRegistro.Caption:=self.pegaDescricaoRegistro(lbTipoRegistro.Caption);

end;

procedure TfSpedFiscal.rich1SelectionChange(Sender: TObject);
begin

    Self.pegaLinhaColuna;

end;

function TfSpedFiscal.pegaPosicaoFinal(pStr:string;busca: string): Integer;
var
  i:integer;
begin

  if (pStr = '') then Exit;

  i:=StrToInt(lbColuna.caption);
  result:=0;

  while (pStr[i] <> busca) and (i <= Length(pStr)) do
    i:=i+1;

  Result:=i-1;

end;

function TfSpedFiscal.pegaPosicaoInicial(pStr, busca: string): Integer;
var
  i:integer;
begin

  if (pStr = '') then Exit;

  i:=StrToInt(lbColuna.caption);
  result:=0;

  while (pStr[i] <> busca) and (i >= 1) do
    i:=i-1;

  result:=i;

end;

procedure TfSpedFiscal.pegaLinhaColuna;
begin

  lbLinha.Caption:=  IntToStr(rich1.CaretPos.Y + 1);
  lbColuna.Caption:= IntToStr(rich1.CaretPos.X);

end;



procedure TfSpedFiscal.selecionaIntervalo;
var
  inicio,i,f:Integer;
begin

  if (Self.btSelecionaIntervalo.Marked) then Exit;
  if (Length(rich1.Text) <= 0) then Exit;

  inicio:=rich1.SelStart;

  inicio:=inicio+(dir);

  i  := selecaoInicial(inicio);
  f  := selecaoFinal  (inicio)-1;

 rich1.SelStart:=i;
 rich1.SelLength:=f-i;

end;

function TfSpedFiscal.selecaoFinal(i:integer): integer;
begin

  result:=0;

  while (rich1.Text[i] <> '|') do
    i:=i+1;

  result:=i;

end;

function TfSpedFiscal.selecaoInicial(i:integer): integer;
begin

  result:=0;

  while (rich1.Text[i] <> '|') do
    i:=i-1;

  result:=i;

end;

procedure TfSpedFiscal.atualizaPosicaoInicialFinal;
begin

  self.lbPosInicial.Caption:= IntToStr (self.pegaPosicaoInicial (rich1.Lines[strtoint(self.lbLinha.Caption)-1],'|'));
  self.lbPosFinal  .Caption:= IntToStr (self.pegaPosicaoFinal   (rich1.Lines[strtoint(self.lbLinha.Caption)-1],'|'));

end;

procedure TfSpedFiscal.limpaLabelsVizualizador;
begin

  {vizualizador}
  lbLinha.Caption             := '';
  lbColuna.Caption            := '';
  lbTipoRegistro.Caption      := '';
  lbPosInicial.Caption        := '';
  lbPosFinal.Caption          := '';
  lbDescricaoRegistro.Caption := '';

end;

function TfSpedFiscal.pegaTipoRegistro:string;
begin

    if (rich1.Lines[rich1.CaretPos.Y] = '') then Exit;

    result:=rich1.Lines[rich1.CaretPos.Y][2]+rich1.Lines[rich1.CaretPos.Y][3]+rich1.Lines[rich1.CaretPos.Y][4]+rich1.Lines[rich1.CaretPos.Y][5];

end;

procedure TfSpedFiscal.btVaiParaLinhaClick(Sender: TObject);
begin

  rich1.SelStart := rich1.Perform(EM_LINEINDEX,StrToIntDef(InputBox('Vai para linha','N�mero da linha (1 a '+IntToStr (rich1.Lines.Count)+')','',),0)-1,0);
  rich1.SetFocus;


end;

procedure TfSpedFiscal.btZoomMaisClick(Sender: TObject);
begin

  rich1.Font.Size:=rich1.Font.Size+2;

end;

procedure TfSpedFiscal.btZoomMenosClick(Sender: TObject);
begin

  rich1.Font.Size:=rich1.Font.Size-2;

end;

procedure TfSpedFiscal.btProximoTipoClick(Sender: TObject);
begin

  self.posicionaProximoRegistro();

  atualizaPosicaoInicialFinal();
  lbTipoRegistro.Caption:=self.pegaTipoRegistro();
  lbDescricaoRegistro.Caption:=self.pegaDescricaoRegistro(lbTipoRegistro.Caption);

end;

procedure TfSpedFiscal.posicionaProximoRegistro;
var
  registroAtual:string;
  i,linhaAtual:Integer;
begin

  linhaAtual    := self.rich1.CaretPos.Y;
  registroAtual:= self.pegaTipoRegistro();

  for i := linhaAtual to rich1.Lines.Count do
  begin

    if (registroAtual <> self.pegaTipoRegistro) then
    begin

      self.rich1.SetFocus;
      self.selecionaIntervalo;
      Exit;

    end
    else
      rich1.SelStart  := rich1.Perform(EM_LINEINDEX,i,0)+2;

  end;

  MensagemAviso('N�o ha mais tipo de registro');

end;

procedure TfSpedFiscal.posicionaRegistroAnterior;
var
  registroAtual:string;
  i,linhaAtual:Integer;
begin

  linhaAtual    := self.rich1.CaretPos.Y;
  registroAtual:= self.pegaTipoRegistro();

  for i := linhaAtual downto 0 do
  begin

    if (registroAtual <> self.pegaTipoRegistro) then
    begin

      self.rich1.SetFocus;
      self.selecionaIntervalo;
      Exit;

    end
    else
      rich1.SelStart  := rich1.Perform(EM_LINEINDEX,i,0)+2;

  end;

  MensagemAviso('N�o ha mais tipo de registro');

end;

procedure TfSpedFiscal.btTipoAnteriorClick(Sender: TObject);
begin

  self.posicionaRegistroAnterior();

  atualizaPosicaoInicialFinal();
  lbTipoRegistro.Caption:=self.pegaTipoRegistro();
  lbDescricaoRegistro.Caption:=self.pegaDescricaoRegistro(lbTipoRegistro.Caption);

end;

procedure TfSpedFiscal.Vaiparalinha1Click(Sender: TObject);
begin
  self.btVaiParaLinhaClick(nil);
end;

procedure TfSpedFiscal.Aprximar1Click(Sender: TObject);
begin
  self.btZoomMaisClick(nil);        
end;

procedure TfSpedFiscal.Afastar1Click(Sender: TObject);
begin
  self.btZoomMenosClick(nil);
end;

procedure TfSpedFiscal.Prximotipo1Click(Sender: TObject);
begin
  self.btProximoTipoClick(nil);
end;

procedure TfSpedFiscal.ipoanterior1Click(Sender: TObject);
begin
  self.btTipoAnteriorClick(nil);
end;

procedure TfSpedFiscal.ToolButton3Click(Sender: TObject);
begin

   if (self.FontDialog1.Execute) then
    Self.rich1.Font.Name:= self.FontDialog1.Font.Name;

end;

procedure TfSpedFiscal.btImportaArquivoClick(Sender: TObject);
begin

  if (OpenDialog1.Execute) then
  begin

    self.carregaArquivoSpedParaVizualizador(OpenDialog1.FileName);

  end

end;

procedure TfSpedFiscal.rich1MouseUp(Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Integer);
begin

  self.selecionaIntervalo();

  atualizaPosicaoInicialFinal();
  lbTipoRegistro.Caption:=self.pegaTipoRegistro();
  lbDescricaoRegistro.Caption:=self.pegaDescricaoRegistro(lbTipoRegistro.Caption);

end;

procedure TfSpedFiscal.btSelecionaIntervaloClick(Sender: TObject);
begin

  self.btSelecionaIntervalo.Marked:= not(self.btSelecionaIntervalo.Marked);

end;

function TfSpedFiscal.pegaDescricaoRegistro(pTipoRegistro:string):string;
begin

  Result:= Self.objSpedFiscal.get_descricaoRegistro(pTipoRegistro,IntToStr(pegaPosicaoTipo),NOMETABELA);

end;

function TfSpedFiscal.pegaPosicaoTipo: integer;
var
  linhaCorrente:string;
  i:integer;
begin

  linhaCorrente:= self.rich1.Lines[self.rich1.CaretPos.y];
  result:=0;

  Try

    for i := 1  to StrToInt(lbColuna.Caption)  do
    begin

      if (linhaCorrente[i] = '|') then
        result:=result+1;

    end;

  except

  end;

end;


function TfSpedFiscal.carregaArquivoSpedParaVizualizador(pCaminho: string):Boolean;
begin

  result:=False;

  setNomeTabela;

  if not (FileExists(pCaminho)) then
  begin

    MensagemAviso('Arquivo ou diret�rio n�o contrado: '+pCaminho);
    Exit;

  end;

  self.rich1.Lines.Clear;
  self.rich1.Lines.LoadFromFile(pCaminho);

  if not (self.objSpedFiscal.arquivoValido_amanda(rich1.Text)) and not (self.objSpedFiscal.arquivoValido_ECF(rich1.text)) then
  begin

    MensagemErro('Arquivo inv�lido para processamento');
    self.rich1.Clear;
    Exit;

  end;

  self.rich1.SetFocus;
  self.rich1.SelStart:=2;
  self.selecionaIntervalo;

  result:=True;

end;

procedure TfSpedFiscal.btLimpaRegistroClick(Sender: TObject);
begin

  rich1.Clear;

end;

procedure TfSpedFiscal.ToolButton9Click(Sender: TObject);
begin

  self.rich1.Print('ArquivoSPED');

end;


procedure TfSpedFiscal.edtOBrigacoesICMSKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   fobrigacoes:TFOBRIGACOESICMS;
begin

  If (key <>vk_f9) then exit;

  Try

    Fpesquisalocal:=Tfpesquisa.create(Nil);
    fobrigacoes:=TFOBRIGACOESICMS.create(nil);

    If (FpesquisaLocal.PreparaPesquisa('select * from TABOBRIGACOESICMS','Pesquisa de obriga��es do ICMS',fobrigacoes)) Then
    Begin

      Try

        If (FpesquisaLocal.showmodal=mrok) Then
          TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

      Finally
        FpesquisaLocal.QueryPesq.close;
      End;

    End;

  Finally

    FreeandNil(FPesquisaLocal);
    Freeandnil(fobrigacoes);

  End;

end;

procedure TfSpedFiscal.edtCodigoReceitaKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   fcodigoreceita:TFCODIGORECEITAESTADUAL;
begin

  If (key <>vk_f9) then exit;

  Try

    Fpesquisalocal:=Tfpesquisa.create(Nil);
    fcodigoreceita:=TFCODIGORECEITAESTADUAL.create(nil);

    If (FpesquisaLocal.PreparaPesquisa('select * from TABCODIGORECEITAESTADUAL','Pesquisa de c�digo de receitas',fcodigoreceita)) Then
    Begin

      Try

        If (FpesquisaLocal.showmodal=mrok) Then
          TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

      Finally
        FpesquisaLocal.QueryPesq.close;
      End;

    End;

  Finally

    FreeandNil(FPesquisaLocal);
    Freeandnil(fcodigoreceita);

  End;

end;

procedure TfSpedFiscal.edtDataInicialExit(Sender: TObject);
var
  dtini,dtFim:string;
begin

  if (Trim(comebarra(self.edtDataInicial.text)) <> '') then
  begin

    primeiroUltimoDia(FormatDateTime('mm',StrToDate(self.edtDataInicial.Text)),FormatDateTime('yyyy',StrToDate(self.edtDataInicial.Text)),dtini,dtFim);
    self.edtDataFinal.Text:=dtFim;

  end;

end;

procedure TfSpedFiscal.lbDescricaoRegistroMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin

  (Sender as TLabel).Hint := (Sender as Tlabel).Caption;

end;

procedure TfSpedFiscal.edtOBrigacoesICMSChange(Sender: TObject);
begin

  self.edtOBrigacoesICMS.Text:= CompletaPalavra_a_Esquerda(self.edtOBrigacoesICMS.Text,3,'0');

end;

procedure TfSpedFiscal.Label89MouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin

  (sender as TLabel).Font.Style:=[fsBold,fsUnderline];

end;

procedure TfSpedFiscal.Label89MouseLeave(Sender: TObject);
begin

  (sender as TLabel).Font.Style:=[];

end;

procedure TfSpedFiscal.Label90MouseLeave(Sender: TObject);
begin

  (sender as TLabel).Font.Style:=[];

end;

procedure TfSpedFiscal.Label90MouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin

  (sender as TLabel).Font.Style:=[fsBold,fsUnderline];

end;

procedure TfSpedFiscal.lbDataInventarioINIMouseLeave(Sender: TObject);
begin
 (sender as TLabel).Font.Style:=[];
end;

procedure TfSpedFiscal.lbDataInventarioFINMouseLeave(Sender: TObject);
begin
 (sender as TLabel).Font.Style:=[];
end;

procedure TfSpedFiscal.lbDataInventarioINIMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
(sender as TLabel).Font.Style:=[fsBold,fsUnderline];
end;

procedure TfSpedFiscal.lbDataInventarioFINMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
(sender as TLabel).Font.Style:=[fsBold,fsUnderline];
end;

procedure TfSpedFiscal.lbDataInventarioINIClick(Sender: TObject);
var
  newValue:string;
begin

  repeat

    if (InputQuery ('Altera data','Informe nova data',newValue)) then
     (Sender as tlabel).Caption:=newValue;

  until (validaData (newValue));

end;

procedure TfSpedFiscal.lbDataInventarioFINClick(Sender: TObject);
var
  newValue:string;
begin

  repeat

    if (InputQuery ('Altera data','Informe nova data',newValue)) then
    (Sender as tlabel).Caption:=newValue;

  until (validaData (newValue));

end;

procedure TfSpedFiscal.Label93MouseLeave(Sender: TObject);
begin
 (sender as TLabel).Font.Style:=[];
end;

procedure TfSpedFiscal.Label93MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
(sender as TLabel).Font.Style:=[fsBold,fsUnderline];
end;

procedure TfSpedFiscal.Label93Click(Sender: TObject);
begin

  if (validaData (lbDataInventarioINI.Caption) and validaData (lbDataInventarioFIN.Caption)) then
    begin

      objSpedFiscal.submit_DATAINI_ESTOQUE(lbDataInventarioINI.Caption);
      objSpedFiscal.submit_DATAFIN_ESTOQUE(lbDataInventarioFIN.Caption);
      objSpedFiscal.criaListViewEstoque(listViewEstoque);

    end
  else
  begin

    MensagemErro ('Informe uma data valida para o inventario');

  end;

end;

procedure TfSpedFiscal.ToolButton12Click(Sender: TObject);
var
  str:TStringList;
begin

  str:=TStringList.Create;

  try

    if (SaveDialog1.Execute) then
    begin

      str.Clear;
      str.Text:=Self.rich1.Text;
      str.SaveToFile(SaveDialog1.FileName);

    end;

  finally

    FreeAndNil(str);

  end;


end;

procedure TfSpedFiscal.SpeedButton2Click(Sender: TObject);
begin

  if (panel8.Height <= 12) then
  begin

    panel8.Height := 80;
    comboRegistro.SetFocus;

  end
  else
    panel8.Height := 12;

end;

procedure TfSpedFiscal.comboRegistroExit(Sender: TObject);
begin

  self.objSpedFiscal.objquery.Active:=False;
  self.objSpedFiscal.objquery.SQL.Clear;

  self.objSpedFiscal.objquery.SQL.Text :=
  'select rgs.posicao, nomecampo '+
  'from '+NOMETABELA+' rgs '+
  'where rgs.tipo = '+quotedstr (tira_caracter (comboRegistro.Text,'|'))+' '+
  'order by rgs.posicao';

  self.objSpedFiscal.objquery.Active:=True;
  self.objSpedFiscal.objquery.First;

  comboCampos.Clear;

  while not (self.objSpedFiscal.objquery.eof) do
  begin

    comboCampos.Items.Add (Trim (self.objSpedFiscal.objquery.fieldbyname ('nomecampo').AsString));
    self.objSpedFiscal.objquery.Next;

  end;

end;

procedure TfSpedFiscal.comboCamposEnter(Sender: TObject);
begin

  comboCampos.ItemIndex:=0;
  comboCamposChange (sender);

end;

procedure TfSpedFiscal.comboCamposChange(Sender: TObject);
begin

  self.objSpedFiscal.objquery.Active:=False;
  self.objSpedFiscal.objquery.SQL.Clear;

  self.objSpedFiscal.objquery.SQL.Text :=
  'select rgs.posicao '+
  'from '+NOMETABELA+' rgs '+
  'where rgs.tipo = '+quotedstr (tira_caracter (comboRegistro.Text,'|'))+' and rgs.nomecampo = '+QuotedStr (comboCampos.Text);

  self.objSpedFiscal.objquery.Active:=True;
  self.objSpedFiscal.objquery.First;

  lbPosicao.Caption := Trim (self.objSpedFiscal.objquery.fieldbyname ('posicao').AsString);

end;

procedure TfSpedFiscal.comboRegistroEnter(Sender: TObject);
begin

  if NOMETABELA <> ''then
  begin

    self.objSpedFiscal.objquery.Active:=False;
    self.objSpedFiscal.objquery.SQL.Clear;

    self.objSpedFiscal.objquery.SQL.Text :=
    'select distinct(rgs.tipo) tipo '+
    'from '+NOMETABELA+' rgs '+
    'order by rgs.tipo';

    self.objSpedFiscal.objquery.Active:=True;
    self.objSpedFiscal.objquery.First;

    comboRegistro.Clear;

    while not (self.objSpedFiscal.objquery.eof) do
    begin

      comboRegistro.Items.Add ('|'+Trim (self.objSpedFiscal.objquery.fieldbyname ('tipo').AsString)+'|');
      self.objSpedFiscal.objquery.Next;

    end;

    comboRegistro.ItemIndex:=0;
    
  end;

end;

procedure TfSpedFiscal.SpeedButton3Click(Sender: TObject);
begin

  //self.substituir();

end;

procedure TfSpedFiscal.substituir;
var
  linha:integer;
  str:string;
begin

  for linha:=0 to rich1.Lines.Count-1 do
  begin

    rich1.SelStart  := rich1.Perform(EM_LINEINDEX,linha,0);
    str:=rich1.Lines[linha];

  end;

  {|0150|3752C|KADEMA SUPERMERCADO LTDA EPP|1058|37190600000250||283537418|5003702||RUA: PONTA POR�  160|||VILA ALVORADA|}


end;

function TfSpedFiscal.localiza(posicao: integer; str: string): Integer;
var
  _i:Integer;
begin
  result:=0;
  for _i := 1  to Length (str)  do
  begin
    if (str[_i] = '|') then
      result:=result+1;

    if (Result = posicao) then
      Exit;
  end;

end;

procedure TfSpedFiscal.Notafiscal1Click(Sender: TObject);
var
  formnf:TFNotaFiscal;
begin

  formnf:=TFNotaFiscal.Create (nil);

  try

    formnf.permiteAlteracao:=True;

    if self.objSpedFiscal.strERROS.Count <= 0 then
    begin

      MensagemAviso ('N�o possui erros para corre��o');
      Exit;

    end;

    formnf.ShowModal;

  except

    FreeAndNil (formnf);

  end;

end;


procedure TfSpedFiscal.AcertaBCICMS1Click(Sender: TObject);
var
  BC_ICMS_PROD,BC_ICMS_NF:Currency;
begin

  {SAIDA}
  objSpedFiscal.queryNFSaida.First;
  while not (objSpedFiscal.queryNFSaida.Eof) do
  begin
    Application.ProcessMessages;

    if (objSpedFiscal.queryNFSaida.FieldByName ('situacao').AsString <> 'C') and (objSpedFiscal.queryNFSaida.FieldByName ('situacao').AsString <> 'Z') and (objSpedFiscal.queryNFSaida.FieldByName ('situacao').AsString <> 'P') then
    begin

      {BC ICMS DO PRODUTO}
      objSpedFiscal.objquery.Close;
      objSpedFiscal.objquery.SQL.Clear;
      objSpedFiscal.objquery.SQL.Add('select SUM(valorfinal - (reducaobasecalculo * valorfinal/100)) as BC_ICMS');
      objSpedFiscal.objquery.SQL.Add('from TABPRODUTOSNOTAFISCAL');
      objSpedFiscal.objquery.SQL.Add('where notafiscal='+objSpedFiscal.queryNFSaida.fieldbyname('codigo').AsString+' and aliquota > 0 and substituicaotributaria = ''N''');
      objSpedFiscal.objquery.Active:=True;

      BC_ICMS_PROD:=StrToCurrDef(objSpedFiscal.objquery.fieldbyname('BC_ICMS').AsString,0.00);
      BC_ICMS_PROD:= StrToCurrDef(tira_ponto(formata_valor(BC_ICMS_PROD)),0.00);

      {BC ICMS DA NOTA}
      BC_ICMS_NF:=StrToCurrDef(tira_ponto(formata_valor(objSpedFiscal.queryNFSaida.FieldByName('basecalculoicms').AsCurrency)),0.00);

      if (BC_ICMS_NF > 0) then
      begin

        if (BC_ICMS_PROD <> BC_ICMS_NF) then
          if not exec_sql('update TABNOTAFISCAL set basecalculoicms = '+CurrToStr(BC_ICMS_PROD) + ' where codigo = '+objSpedFiscal.queryNFSaida.fieldbyname('codigo').AsString) then
          begin
            MensagemErro('Erro ao alterar campo BCICMS da nota c�digo: '+objSpedFiscal.queryNFSaida.fieldbyname('codigo').AsString);
            Exit;
          end;

      end;
    end;
    objSpedFiscal.queryNFSaida.Next;
  end;

  FDataModulo.IBTransaction.CommitRetaining;
  MensagemAviso('Processo concluido');

end;
procedure TfSpedFiscal.setNomeTabela;
begin
  if comboTipoArquivo.ItemIndex = 0 then
    NOMETABELA := 'TABTIPOREGISTROSPED'
  else
    NOMETABELA := 'TABTIPOREGISTROSPED_PISCOFINS';
end;

function TfSpedFiscal.validaRegistros:boolean;
var
  qL:TIBQuery;
begin

  qL := TIBQuery.Create(self);
  qL.Database := FDataModulo.IBDatabase;
  qL.Close;
  qL.SQL.Clear;

  qL.SQL.Text :=
  'select pnf.situacaotributaria_tabelab as cst,pnf.csosn,pnf.notafiscal,pnf.codigoproduto||'+quotedstr(' - ')+'||'+'pnf.descricao as produto '+
  'from viewprodutosvenda pnf '+
  'inner join tabnotafiscal nf on nf.codigo = pnf.notafiscal '+
  'where nf.dataemissao >= '+QuotedStr(troca(edtDataInicial.Text,'/','.'))+' and nf.dataemissao <= '+QuotedStr(troca(edtDataFinal.Text,'/','.'));

  qL.Open;

  memoErros.Clear;
  
  while not qL.Eof do
  begin

    //CST OU CSOSN DEVEM SER PREENCHIDOS
    if (qL.FieldByName('cst').AsString = '') and (qL.FieldByName('csosn').AsString = '') then
      memoErros.Lines.Add('CST e CSOSN esta vazio. Nota fiscal: '+qL.fieldbyname('notafiscal').AsString+'. Produto: '+qL.fieldbyname('produto').AsString);

    if (qL.fieldbyname('cst').asstring <> '') and (qL.fieldbyname('csosn').asstring <> '') then
      memoErros.Lines.Add('CST e CSOSN n�o podem estar preenchidos ao mesmo tempo. Nota fiscal: '+qL.fieldbyname('notafiscal').AsString+'. Produto: '+qL.fieldbyname('produto').AsString);

    qL.Next;

  end;

  Result := (memoErros.Lines.Count = 0);

end;

end.
