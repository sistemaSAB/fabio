unit UreajustaMateriais;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,UobjObjetosEntrada,UobjServico, Upesquisa,
  Grids, ExtCtrls, ImgList, Menus;

type
  TFreajustaMateriais = class(TForm)
    StrGridMateriais: TStringGrid;
    pnl1: TPanel;
    lb1: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lbNomeGrupo: TLabel;
    COMBOMaterial: TComboBox;
    edtreajuste: TEdit;
    edtGrupo: TEdit;
    chkListarProdutos: TCheckBox;
    chk1: TCheckBox;
    btBtreajustar: TSpeedButton;
    pnl3: TPanel;
    btCancelar: TSpeedButton;
    lb4: TLabel;
    pnlRodape: TPanel;
    Img1: TImage;
    ilProdutos: TImageList;
    PopUp1: TPopupMenu;
    LocalizaporReferencia1: TMenuItem;
    Localizarpelocdigo1: TMenuItem;
    Localizarpelonome1: TMenuItem;
    btLocalizar: TSpeedButton;
    procedure btBtSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btBtreajustarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtGrupoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtGrupoKeyPress(Sender: TObject; var Key: Char);
    procedure edtreajusteKeyPress(Sender: TObject; var Key: Char);
    procedure AjustaPrecoVidro();
    procedure AjustaPrecoservico();
    procedure AjustaPrecopersiana();
    procedure AjustaPrecoperfilado();
    procedure AjustaPrecokitbox();   
    procedure AjustaPrecoferragem();
    procedure AjustaPrecodiversos();
    procedure chkListarProdutosClick(Sender: TObject);
    procedure StrGridMateriaisDrawCell(Sender: TObject; ACol,
      ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure COMBOMaterialClick(Sender: TObject);
    procedure StrGridMateriaisDblClick(Sender: TObject);
    procedure StrGridMateriaisKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ReajustaProdutoPorProduto(Material:string);
    procedure chk1Click(Sender: TObject);
    procedure edtGrupoDblClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btLocalizarClick(Sender: TObject);
    procedure Localizarpelocdigo1Click(Sender: TObject);
    procedure Localizarpelonome1Click(Sender: TObject);
    procedure LocalizaporReferencia1Click(Sender: TObject);
  private
    { Private declarations }
    ObjObjetosEntrada:TobjObjetosEntrada;
    ObjServico:TobjServico;
    procedure LimpaDadosStringGrid;
  public
    { Public declarations }
  end;

var
  FreajustaMateriais: TFreajustaMateriais;
  SQLlistaProdutos:string;


implementation

uses UessencialGlobal, UDataModulo,IBQuery, UescolheImagemBotao, DB;

{$R *.dfm}

procedure TFreajustaMateriais.btBtSairClick(Sender: TObject);
begin
     Self.Close;
end;

procedure TFreajustaMateriais.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     edtreajuste.text:='0';
     lbnomegrupo.caption:='';
     desab_botoes(self);

       LimpaDadosStringGrid;
      with StrGridMateriais do
      begin
            RowCount:= 2;
            ColCount:= 5;
            ColWidths[0] := 50;
            ColWidths[1] := 0;
            ColWidths[2] := 400;
            ColWidths[3] := 100;
            ColWidths[4] := 0;
            Cells[0,0] := ' ';
            Cells[1,0] := 'CODIGO';
            Cells[2,0] := 'NOME';
            Cells[3,0] := 'REAJUSTE %';
            Cells[4,0] := 'REFERENCIA';
            FixedCols:=0;
            FixedRows:=1;
      end;

      FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');
     try
        ObjObjetosEntrada:=TobjObjetosEntrada.create(self);
        ObjServico:=TObjSERVICO.create;
        habilita_botoes(self);
        ComboMaterial.setfocus;

        habilita_botoes(Self);
        if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE REAJUSTE DE MATERIAIS')=False)
        Then desab_botoes(Self);

     Except
           mensagemerro('Erro na tentativa de Criar o Objeto de Entrada');
           exit;
     End;
end;

//quando o usuario quiser modificar produto a produto
procedure TFreajustaMateriais.ReajustaProdutoPorProduto(Material:string);
var
  cont:Integer;
  Objquery:TIBQuery;
  reajuste,novocusto,pVendaInstalado,
  pVendaFornecido,pVendaRetirado:Currency;
begin

  cont:=1;
  
  try
    Objquery:=TIBQuery.Create(nil);
    Objquery.Database:=FDataModulo.IBDatabase;
  except
    ShowMessage('erro na cria��o de objetos');
  end;

  try

    while(cont < StrGridMateriais.RowCount)do
    begin

      if(StrGridMateriais.Cells[0,cont]='            X')then
      begin

        With Objquery do
        Begin

          {primeiro busca antigo custo, e faz os calculos, depois grava o novo valor do custo}
          Close;
          sql.Clear;

          sql.Add('select PRECOCUSTO,');
          sql.Add('PORCENTAGEMINSTALADO,PORCENTAGEMFORNECIDO,PORCENTAGEMRETIRADO');
          sql.Add('from '+Material);
          sql.Add('where codigo='+StrGridMateriais.Cells[1,cont]);

          Open;
          reajuste        := 0;
          novocusto       := 0;
          pVendaInstalado := 0;
          pVendaFornecido := 0;
          pVendaRetirado  := 0;

          reajuste  := (fieldbyname('PRECOCUSTO').AsCurrency * StrToCurr(StrGridMateriais.cells[3,cont]))/100 ;
          novocusto := fieldbyname('PRECOCUSTO').AsCurrency + reajuste;

          pVendaInstalado :=  novocusto +  (fieldbyname('PORCENTAGEMINSTALADO').AsCurrency / 100) * novocusto;
          pVendaFornecido :=  novocusto +  (fieldbyname('PORCENTAGEMFORNECIDO').AsCurrency / 100) * novocusto;
          pVendaRetirado  :=  novocusto +  (fieldbyname('PORCENTAGEMRETIRADO').AsCurrency  / 100) * novocusto;

          close;
          SQL.clear;

          SQL.add('Update '+Material+' Set ');
          sql.Add('PRECOCUSTO = '+format_db(CurrToStr(novocusto)));
          SQL.Add(',PRECOVENDAINSTALADO = '+format_db(CurrToStr(pVendaInstalado)));
          SQL.Add(',PRECOVENDAFORNECIDO = '+format_db(CurrToStr(pVendaFornecido)));
          SQL.Add(',PRECOVENDARETIRADO = '+format_db(CurrToStr(pVendaRetirado)));
          SQL.add('Where codigo='+StrGridMateriais.Cells[1,cont]);

          try
            execsql;
            FDataModulo.IBTransaction.CommitRetaining;
          Except
            on e:exception do
            begin
              FDataModulo.IBTransaction.RollbackRetaining;
              mensagemerro('Erro na tentativa de Reajustar o valor do vidro'+#13+E.message);
              exit;
            End;
          End;

        End;

        StrGridMateriais.Cells[0,cont]:='';
        StrGridMateriais.Cells[3,cont]:='';

      end;

      inc(cont,1);

    end;

    mensagemaviso('Conclu�do');

  finally

   FreeAndNil(Objquery);

  end;

end;

procedure TFreajustaMateriais.AjustaPrecoVidro();
begin

      if(chkListarProdutos.Checked=false)then
      begin
            //quando o usu�rio quiser ajustar todos os produtos de um determinado grupo
            ObjObjetosEntrada.Objvidro_ep.VidroCor.vidro.Reajusta(edtreajuste.text,EdtGrupo.text);
      end
      else
      begin
            //quando o usuario quiser ajustar apenas determinados produtos de cada material
           self.ReajustaProdutoPorProduto('tabvidro');
      end;

end;

procedure TFreajustaMateriais.AjustaPrecoservico();
begin
      if(chkListarProdutos.Checked=false)then
      begin
          ObjServico.Reajusta(edtreajuste.text,EdtGrupo.text);
      end
      else
      begin
          Self.ReajustaProdutoPorProduto('tabservico');
      end;

end;

procedure TFreajustaMateriais.AjustaPrecopersiana();
begin

   if(chkListarProdutos.Checked=false)then
   begin
          ObjObjetosEntrada.ObjPersiana_ep.PersianaGrupoDiametroCor.Reajusta(edtreajuste.text,EdtGrupo.text);
   end
   else
   begin
          Self.ReajustaProdutoPorProduto('tabpersiana');
   end;

end;

procedure TFreajustaMateriais.AjustaPrecoperfilado();
begin
   if(chkListarProdutos.Checked=false)then
   begin
          ObjObjetosEntrada.Objperfilado_ep.PerfiladoCor.perfilado.Reajusta(edtreajuste.text,EdtGrupo.text);
   end
   else
   begin
          Self.ReajustaProdutoPorProduto('tabperfilado');
   end;

end;

procedure TFreajustaMateriais.AjustaPrecokitbox();
begin
   if(chkListarProdutos.Checked=false)then
   begin
           ObjObjetosEntrada.Objkitbox_ep.KitBoxCor.kitbox.Reajusta(edtreajuste.text,EdtGrupo.text);
   end
   else
   begin
          Self.ReajustaProdutoPorProduto('tabkitbox');
   end;
end;

procedure TFreajustaMateriais.AjustaPrecoferragem();
begin
   if(chkListarProdutos.Checked=false)then
   begin
          ObjObjetosEntrada.Objferragem_ep.FerragemCor.Ferragem.Reajusta(edtreajuste.text,EdtGrupo.text);
   end
   else
   begin
          Self.ReajustaProdutoPorProduto('tabferragem');
   end;

end;

procedure TFreajustaMateriais.AjustaPrecodiversos();
begin

   if(chkListarProdutos.Checked=false)then
   begin
          ObjObjetosEntrada.ObjDiverso_ep.DiversoCor.Diverso.Reajusta(edtreajuste.text,EdtGrupo.text);
   end
   else
   begin
          Self.ReajustaProdutoPorProduto('tabdiverso');
   end;

end;


procedure TFreajustaMateriais.btBtreajustarClick(Sender: TObject);
begin
     case ComboMaterial.itemindex of
        0:Self.AjustaPrecodiversos();
        1:Self.AjustaPrecoferragem();
        2:self.AjustaPrecokitbox();
        3:self.AjustaPrecoperfilado();
        4:self.AjustaPrecopersiana();
        5:Self.AjustaPrecoVidro();
        6:Self.AjustaPrecoservico();
     Else
      Begin
        MensagemAviso('Escolha um material');
      End;
     End;
end;



procedure TFreajustaMateriais.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     if (ObjObjetosEntrada<>nil)
     Then ObjObjetosEntrada.free;

     if (ObjServico<>nil)
     Then ObjServico.Free;

     chkListarProdutos.Checked:=False;
end;

procedure TFreajustaMateriais.edtGrupoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if key<>Vk_f9
     Then exit;

     case ComboMaterial.ItemIndex of
        0:ObjObjetosEntrada.ObjDiverso_ep.DiversoCor.Diverso.EdtGrupoDiversoKeyDown(Sender,Key,shift,LbNomeGrupo);
        1:ObjObjetosEntrada.Objferragem_ep.FerragemCor.Ferragem.EdtGrupoFerragemKeyDown(Sender,Key,shift,LbNomeGrupo);
        2:ObjObjetosEntrada.Objkitbox_ep.KitBoxCor.EdtKitBoxKeyDown(Sender,Key,shift,LbNomeGrupo);
        3:ObjObjetosEntrada.Objperfilado_ep.PerfiladoCor.perfilado.EdtGrupoPerfiladoKeyDown(Sender,Key,shift,LbNomeGrupo);
        4:ObjObjetosEntrada.ObjPersiana_ep.PersianaGrupoDiametroCor.EdtGrupoPersianaKeyDown(Sender,Key,shift,LbNomeGrupo);
        5:ObjObjetosEntrada.Objvidro_ep.VidroCor.vidro.EdtGrupoVidroKeyDown(Sender,Key,shift,LbNomeGrupo);
        6:ObjServico.EdtgrupoServicoKeyDown(Sender,Key,shift,LbNomeGrupo);
     Else
        Begin
                 MensagemAviso('Escolha um material');
        End;
     End;
       LimpaDadosStringGrid;
      with StrGridMateriais do
      begin
            RowCount:= 2;
            ColCount:= 5;
            ColWidths[0] := 50;
            ColWidths[1] := 0;
            ColWidths[2] := 400;
            ColWidths[3] := 100;
            ColWidths[4] := 0;
            Cells[0,0] := ' ';
            Cells[1,0] := 'CODIGO';
            Cells[2,0] := 'NOME';
            Cells[3,0] := 'REAJUSTE %';
            Cells[4,0] := 'REFERENCIA';
            FixedCols:=0;
            FixedRows:=1;
            Cells[1,1]:='';
            Cells[2,1]:='';
            Cells[3,1]:='';
            Cells[4,1]:='';
      end;
      chkListarProdutos.Checked:=False;

end;

procedure TFreajustaMateriais.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFreajustaMateriais.edtGrupoKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     then key:=#0;
end;

procedure TFreajustaMateriais.edtreajusteKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not (key in ['0'..'9',#8,',','-'])
     then Begin
               if key='.'
               Then Key:=','
               Else key:=#0;
     End;
end;

procedure TFreajustaMateriais.chkListarProdutosClick(Sender: TObject);
var
  Query:TIBQuery;
begin
    case ComboMaterial.itemindex of
        0:
        begin
              if(EdtGrupo.Text<>'')
              then begin
                SQLlistaProdutos:='select * from tabdiverso where grupodiverso='+edtgrupo.text;
              end
              else
              begin
                 SQLlistaProdutos:='select * from tabdiverso';
              end;
        end;
        1:
        begin
              if(EdtGrupo.Text<>'')
              then begin
                SQLlistaProdutos:='select * from tabferragem where grupoferragem='+edtgrupo.text;
              end
              else
              begin
                 SQLlistaProdutos:='select * from tabferragem';
              end;
        end;
        2:
        begin
              if(EdtGrupo.Text<>'')
              then begin
                SQLlistaProdutos:='select * from tabkitbox where descricao='+#39+LbNomeGrupo.caption+#39;
              end
              else
              begin
                 SQLlistaProdutos:='select * from tabkitbox';
              end;
        end;
        3:
        begin
              if(EdtGrupo.Text<>'')
              then begin
                SQLlistaProdutos:='select * from tabperfilado where grupoperfilado='+edtgrupo.text;
              end
              else
              begin
                 SQLlistaProdutos:='select * from tabperfilado';
              end;
        end;
        4:
        begin
              if(EdtGrupo.Text<>'')
              then begin
                SQLlistaProdutos:='select * from tabpersiana where grupopersiana='+edtgrupo.text;
              end
              else
              begin
                 SQLlistaProdutos:='select * from tabpersiana';
              end;
        end;
        5:
        begin
              if(EdtGrupo.Text<>'')
              then begin
                SQLlistaProdutos:='select * from tabvidro where grupovidro='+EdtGrupo.text;
              end
              else
              begin
                 SQLlistaProdutos:='select * from tabvidro';
              end;
        end;
        6:
         begin
              if(EdtGrupo.Text<>'')
              then begin
                SQLlistaProdutos:='select * from tabsevico where gruposervico='+edtgrupo.text;
              end
              else
              begin
                 SQLlistaProdutos:='select * from tabservico';
              end;
        end;
    else
      begin
        ShowMessage('Selecione um material');
        chkListarProdutos.Checked := false;
        Exit;
      end;

    End;
    if(chkListarProdutos.Checked) then
      chk1.Enabled := True
    else
      chk1.Enabled := False;


    try
      Query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;
    try
        if(chkListarProdutos.Checked=true)
        then begin
              with Query do
              begin
                Close;
                sql.Clear;
                sql.Add(SQLlistaProdutos);
                Open;
                while not Eof do
                begin
                   with StrGridMateriais do
                   begin
                        Cells[1,RowCount-1]:= fieldbyname('codigo').AsString;
                        if(ComboMaterial.Text = 'Persiana')
                        then
                        begin
                            Cells[2,RowCount-1]:= fieldbyname('nome').AsString;
                        end
                        else
                        begin
                            Cells[2,RowCount-1]:= fieldbyname('descricao').AsString;
                        end;
                        Cells[4,RowCount-1]:= fieldbyname('REFERENCIA').AsString;
                        RowCount:=RowCount+1;
                        Next;
                   end;

                end;
                StrGridMateriais.RowCount:=StrGridMateriais.RowCount-1;
              end;
        end
        else
        begin
                LimpaDadosStringGrid;
              with StrGridMateriais do
              begin
                    RowCount:= 2;
                    ColCount:= 5;
                    ColWidths[0] := 50;
                    ColWidths[1] := 0;
                    ColWidths[2] := 400;
                    ColWidths[3] := 100;
                    ColWidths[4] := 0;
                    Cells[0,0] := ' ';
                    Cells[1,0] := 'CODIGO';
                    Cells[2,0] := 'NOME';
                    Cells[3,0] := 'REAJUSTE %';
                    Cells[4,0] := 'REFERENCIA';
                    FixedCols:=0;
                    FixedRows:=1;
                    Cells[1,1]:='';
                    Cells[2,1]:='';
                    Cells[3,1]:='';
                    Cells[4,1]:='';
              end;
        end;
    finally
        FreeAndNil(Query);
    end;


end;

procedure TFreajustaMateriais.StrGridMateriaisDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}  
begin
    if((ARow <> 0) and (ACol = 0))
    then begin
        if(StrGridMateriais.Cells[ACol,ARow] = '            X' ) or (StrGridMateriais.Cells[ACol,ARow] = 'X' )then
        begin
              Ilprodutos.Draw(StrGridMateriais.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 6); //check box marcado
        end
        else
        begin
              Ilprodutos.Draw(StrGridMateriais.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 5); //check box sem marcar    5
        end;
    end;
end;

procedure TFreajustaMateriais.COMBOMaterialClick(Sender: TObject);
begin
  LimpaDadosStringGrid;
  with StrGridMateriais do
  begin
    RowCount:= 2;
    ColCount:= 5;
    ColWidths[0] := 50;
    ColWidths[1] := 0;
    ColWidths[2] := 400;
    ColWidths[3] := 100;
    ColWidths[4] := 0;
    Cells[0,0] := ' ';
    Cells[1,0] := 'CODIGO';
    Cells[2,0] := 'NOME';
    Cells[3,0] := 'REAJUSTE %';
    Cells[4,0] := 'REFERENCIA';
    FixedCols:=0;
    FixedRows:=1;
    Cells[1,1]:='';
    Cells[2,1]:='';
    Cells[3,1]:='';
    Cells[4,1]:='';
  end;
  chkListarProdutos.Checked:=False;
  LbNomeGrupo.caption:='';
  EdtGrupo.Text:='';
end;

procedure TFreajustaMateriais.StrGridMateriaisDblClick(Sender: TObject);
begin
  if(StrGridMateriais.Cells[2,StrGridMateriais.row] = '') then
    Exit;
  if(StrGridMateriais.Cells[3,StrGridMateriais.row] = '')
  then
  begin
    StrGridMateriais.Cells[3,StrGridMateriais.row]:= edtreajuste.Text;
    StrGridMateriais.Cells[0,StrGridMateriais.Row]:='            X';
    StrGridMateriais.Refresh;
  end
  else
  begin
     StrGridMateriais.Cells[3,StrGridMateriais.row]:= '';
     StrGridMateriais.Cells[0,StrGridMateriais.Row]:='';
     StrGridMateriais.Refresh;
  end;
end;

procedure TFreajustaMateriais.StrGridMateriaisKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      //Somente a coluna de reajuste da STRINGGRID pode ser editada
      if(Key<>37) and (Key<>38) and (Key<>39) and (Key<>40) then
      begin
               if(StrGridMateriais.Col =0) Or (StrGridMateriais.Col =1) or (strGridMateriais.Col =2) or (StrGridMateriais.Row = 0)
               then begin
                    Key:=0;
                    StrGridMateriais.Options:= StrGridMateriais.Options - [goEditing];
               end
               else
               begin
                   StrGridMateriais.Options:= StrGridMateriais.Options + [goEditing];
               end;
      end;
end;

procedure TFreajustaMateriais.chk1Click(Sender: TObject);
var
  Cont:Integer;
begin

    if(chk1.Checked=True) then
    begin
        cont:=1;
        while Cont<StrGridMateriais.RowCount do
        begin
            StrGridMateriais.Cells[0,cont]:= '            X';
            StrGridMateriais.Cells[3,cont]:= edtreajuste.Text;
            inc(Cont,1);

        end;
        StrGridMateriais.Refresh;
    end
    else
    begin
        cont:=1;
        while Cont<StrGridMateriais.RowCount do
        begin
            StrGridMateriais.Cells[0,cont]:= '';
            StrGridMateriais.Cells[3,cont]:= '';
            inc(Cont,1);

        end;
        StrGridMateriais.Refresh;
    end;


end;

procedure TFreajustaMateriais.edtGrupoDblClick(Sender: TObject);
var
  Key:Word;
  Shif:TShiftState;
begin
   key:=VK_F9;
   edtGrupoKeyDown(sender,Key,Shif);
end;

procedure TFreajustaMateriais.LimpaDadosStringGrid;
var
  i:integer;
begin
  for i:=0 to StrGridMateriais.RowCount-1 do
  begin
    StrGridMateriais.Cells[0,i]:='';
    StrGridMateriais.Cells[1,i]:='';
    StrGridMateriais.Cells[2,i]:='';
    StrGridMateriais.Cells[3,i]:='';
    StrGridMateriais.Cells[4,i]:='';
  end;
end;


procedure TFreajustaMateriais.btPesquisarClick(Sender: TObject);
begin
  PopUp1.Popup(Mouse.CursorPos.X,Mouse.CursorPos.Y);
end;

procedure TFreajustaMateriais.btLocalizarClick(Sender: TObject);
begin
  PopUp1.Popup(Mouse.CursorPos.X,Mouse.CursorPos.Y);
end;

procedure TFreajustaMateriais.Localizarpelocdigo1Click(Sender: TObject);
var
  i:Integer;
  Valor:string;
begin
  InputQuery('Informe o c�digo que deseja localizar','Codigo',Valor);
  if(Valor='') then
  begin
    chkListarProdutosClick(Sender);
    Exit;
  end;
  for i:=1 to  StrGridMateriais.RowCount-1 do
  begin
      if(StrGridMateriais.Cells[1,i]=Valor)then
      begin
        StrGridMateriais.Cells[1,1]:= StrGridMateriais.Cells[1,i];
        StrGridMateriais.Cells[2,1]:= StrGridMateriais.Cells[2,i];
        StrGridMateriais.Cells[3,1]:= StrGridMateriais.Cells[3,i];
        StrGridMateriais.Cells[4,1]:= StrGridMateriais.Cells[4,i];
        StrGridMateriais.RowCount:=2;
      end;
  end;
end;

procedure TFreajustaMateriais.Localizarpelonome1Click(Sender: TObject);
var
  i:Integer;
  Valor:string;
begin
  InputQuery('Informe o nome que deseja localizar','Nome',Valor);
  if(Valor='') then
  begin
    chkListarProdutosClick(Sender);
    Exit;
  end;
  for i:=1 to  StrGridMateriais.RowCount-1 do
  begin
      if(StrGridMateriais.Cells[2,i]=Valor)then
      begin
        StrGridMateriais.Cells[1,1]:= StrGridMateriais.Cells[1,i];
        StrGridMateriais.Cells[2,1]:= StrGridMateriais.Cells[2,i];
        StrGridMateriais.Cells[3,1]:= StrGridMateriais.Cells[3,i];
        StrGridMateriais.RowCount:=2;
      end;
  end;
end;

procedure TFreajustaMateriais.LocalizaporReferencia1Click(Sender: TObject);
var
  i:Integer;
  Valor:string;
begin
  InputQuery('Informe o refer�ncia que deseja localizar','Refer�ncia',Valor);
  if(Valor='') then
  begin
    chkListarProdutosClick(Sender);
    Exit;
  end;
  for i:=1 to  StrGridMateriais.RowCount-1 do
  begin
      if(StrGridMateriais.Cells[4,i]=Valor)then
      begin
        StrGridMateriais.Cells[1,1]:= StrGridMateriais.Cells[1,i];
        StrGridMateriais.Cells[2,1]:= StrGridMateriais.Cells[2,i];
        StrGridMateriais.Cells[3,1]:= StrGridMateriais.Cells[3,i];
        StrGridMateriais.Cells[4,1]:= StrGridMateriais.Cells[4,i];
        StrGridMateriais.RowCount:=2;
      end;
  end;
end;

end.


