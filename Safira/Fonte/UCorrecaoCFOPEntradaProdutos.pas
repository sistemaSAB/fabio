unit UCorrecaoCFOPEntradaProdutos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs{,UObjProdutosEntradaProdutos}, StdCtrls, Mask, ExtCtrls, Grids,
  ComCtrls;

type
  TFCorrecaoCFOPEntradaProdutos = class(TForm)
    PanelTopo: TPanel;
    edtEntrada: TEdit;
    Entrada: TLabel;
    btOK: TButton;
    Panel1: TPanel;
    PanelProdutos: TPanel;
    strgProdutosEntrada: TStringGrid;
    strgNovoCFOP: TStringGrid;
    Splitter1: TSplitter;
    Panel2: TPanel;
    btGravar: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label2: TLabel;
    lbCodigoFornecedor: TLabel;
    Label5: TLabel;
    lbendereco: TLabel;
    Label6: TLabel;
    lbRazaoSocial: TLabel;
    lbCidade: TLabel;
    Label24: TLabel;
    Label23: TLabel;
    lbFantasia: TLabel;
    lbBairro: TLabel;
    Label9: TLabel;
    lbUF: TLabel;
    lbCEP: TLabel;
    Label16: TLabel;
    Label11: TLabel;
    lbCNPJ: TLabel;
    lbIE: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    lbFone: TLabel;
    lbFax: TLabel;
    Label4: TLabel;
    Label12: TLabel;
    lbEnt_DataEmissao: TLabel;
    Label17: TLabel;
    lbEnt_Data: TLabel;
    Label19: TLabel;
    lbEnt_NF: TLabel;
    lbEnt_BaseCalculo: TLabel;
    lbEnt_ValorICMS: TLabel;
    lbEnt_BaseCalculoST: TLabel;
    lbEnt_ValorICMSST: TLabel;
    lbEnt_ModeloNF: TLabel;
    lbEnt_ValorIPI: TLabel;
    Label21: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label29: TLabel;
    Label13: TLabel;
    Label3: TLabel;
    lbProd_Descricao: TLabel;
    lbProd_Codigo: TLabel;
    lbProd_CFOP: TLabel;
    lbProd_PorcentagemReducao: TLabel;
    lbProd_SituacaoEntrada: TLabel;
    lbProd_IPI: TLabel;
    lbProd_ValorPago: TLabel;
    lbProd_Quantidade: TLabel;
    lbProd_Desconto: TLabel;
    Label8: TLabel;
    Label18: TLabel;
    Label10: TLabel;
    Label20: TLabel;
    Label22: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label1: TLabel;
    lbEnt_Codigo: TLabel;
    procedure edtEntradaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbCodigoFornecedorMouseLeave(Sender: TObject);
    procedure lbCodigoFornecedorMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure lbCodigoFornecedorClick(Sender: TObject);
    procedure lbProd_CodigoMouseLeave(Sender: TObject);
    procedure lbProd_CodigoMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure lbProd_CodigoClick(Sender: TObject);
    procedure strgProdutosEntradaDblClick(Sender: TObject);
    procedure strgProdutosEntradaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btGravarClick(Sender: TObject);
  private
    { Private declarations }
    procedure LimpaEdit;
    procedure LimpaLabels;
    procedure LimpaLabelsFornecedor;
    procedure LimpaLabelsProduto;
    procedure LimpaLabelsEntrada;
    procedure LimpaGrids;
    function AtualizaProdutos:boolean;
    function RetornaProdutosEntrada(pEntrada:string):integer;
    function VerificaNovoCFOP:boolean;
    function ValidaCFOP(pCFOP:string):boolean;
    function GravaCFOP:boolean;
    procedure CarregaProduto(pProdutoEP:string);
  public
    { Public declarations }
//    function PassaObjeto(pObj:TObjProdutosEntradaProdutos):boolean;
  end;

var
  FCorrecaoCFOPEntradaProdutos: TFCorrecaoCFOPEntradaProdutos;
 // objProdutosEntradaProdutos:TObjProdutosEntradaProdutos;
  criadoAqui:Boolean;

implementation

uses UessencialGlobal, UObjEntradaProdutos, UObjFornecedor, UFornecedor,
  DB, UDataModulo,UObjCFOP;


{$R *.dfm}

{ TForm1 }

{function TFCorrecaoCFOPEntradaProdutos.PassaObjeto(pObj: TObjProdutosEntradaProdutos): boolean;
begin

      if (pObj = nil) then
        begin

          objProdutosEntradaProdutos:=TObjProdutosEntradaProdutos.Create;
          criadoAqui:=True;

        end
      else
      begin

        criadoAqui:=False;
        objProdutosEntradaProdutos := pObj;
        
      end;
end;   }

procedure TFCorrecaoCFOPEntradaProdutos.LimpaEdit;
begin
      edtEntrada.Text := '';
end;

procedure TFCorrecaoCFOPEntradaProdutos.LimpaLabels;
begin
      self.LimpaLabelsFornecedor;
      self.LimpaLabelsProduto;
      self.LimpaLabelsEntrada;
end;

procedure TFCorrecaoCFOPEntradaProdutos.edtEntradaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      //objProdutosEntradaProdutos.EdtEntradaKeyDown(sender,key,shift);
end;

procedure TFCorrecaoCFOPEntradaProdutos.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFCorrecaoCFOPEntradaProdutos.LimpaLabelsFornecedor;
begin
      lbCodigoFornecedor.Caption:='';
      lbRazaoSocial.Caption:='';
      lbFantasia.Caption:='';
      lbendereco.Caption:='';
      lbCidade.Caption:='';
      lbBairro.Caption:='';
      lbUF.Caption:='';
      lbCEP.Caption:='';
      lbFone.Caption:='';
      lbFax.Caption:='';
      lbCNPJ.Caption:='';
      lbIE.Caption:='';
end;

procedure TFCorrecaoCFOPEntradaProdutos.LimpaLabelsProduto;
begin
      lbProd_Descricao.Caption:='';
      lbProd_Codigo.Caption:='';
      lbProd_CFOP.Caption:='';
      lbProd_PorcentagemReducao.Caption:='';
      lbProd_SituacaoEntrada.Caption:='';
      lbProd_IPI.Caption:='';
      lbProd_ValorPago.Caption:='';
      lbProd_Quantidade.Caption:='';
      lbProd_Desconto.Caption:='';
end;

procedure TFCorrecaoCFOPEntradaProdutos.btOKClick(Sender: TObject);
begin
      if(edtEntrada.Text='')
      then exit;

     { with objProdutosEntradaProdutos.Entrada do
      begin
            ZerarTabela;
            LimpaLabels;
            if not(LocalizaCodigo(edtEntrada.Text))
            then begin
                  MensagemErro('Entrada n�o localizada');
                  exit;
            end;
            TabelaparaObjeto;

            lbEnt_Codigo.caption := Get_CODIGO;
            lbEnt_DataEmissao.caption:=Get_EmissaoNF;
            lbEnt_Data.caption:=Get_Data;
            lbEnt_NF.caption:=Get_NF;
            lbEnt_BaseCalculo.caption:=formata_valor(get_BaseCalculoIcms);
            lbEnt_ValorICMS.caption:=formata_valor(Get_ValorIcms);
            lbEnt_BaseCalculoST.caption:=formata_valor(get_BaseCalculoIcmsSubstituicao);
            lbEnt_ValorICMSST.caption:=formata_valor(get_ValorIcmsSubstituicao);
            lbEnt_ValorIPI.caption:=formata_valor(Get_ValorIPI);

            if(ModeloNF.Get_CODIGO<>'')
            then begin
                  ModeloNF.LocalizaCodigo(ModeloNF.Get_CODIGO);
                  ModeloNF.TabelaparaObjeto;
                  lbEnt_ModeloNF.caption:=ModeloNF.Get_MODELO;
            end;

            Fornecedor.LocalizaCodigo(Fornecedor.Get_CODIGO);
            Fornecedor.TabelaparaObjeto;

            lbCodigoFornecedor.Caption:=Fornecedor.Get_CODIGO;
            lbRazaoSocial.Caption:=Fornecedor.Get_RazaoSocial;
            lbFantasia.Caption:=Fornecedor.Get_Fantasia;
            lbendereco.Caption:=Fornecedor.Get_Endereco;
            lbCidade.Caption:=Fornecedor.Get_Cidade;
            lbBairro.Caption:=Fornecedor.Get_Bairro;
            lbUF.Caption:=Fornecedor.Estado.Get_CODIGO;
            lbCEP.Caption:=Fornecedor.Get_Cep;
            lbFone.Caption:=Fornecedor.Get_Fone;
            lbFax.Caption:=Fornecedor.Get_Fax;
            lbCNPJ.Caption:=Fornecedor.Get_CGC;
            lbIE.Caption:=Fornecedor.Get_IE;
      end;

      AtualizaProdutos;     }

end;

procedure TFCorrecaoCFOPEntradaProdutos.LimpaLabelsEntrada;
begin
      lbEnt_Codigo.caption := '';
      lbEnt_DataEmissao.caption:='';
      lbEnt_Data.caption:='';
      lbEnt_NF.caption:='';
      lbEnt_BaseCalculo.caption:='';
      lbEnt_ValorICMS.caption:='';
      lbEnt_BaseCalculoST.caption:='';
      lbEnt_ValorICMSST.caption:='';
      lbEnt_ModeloNF.caption:='';
      lbEnt_ValorIPI.caption:='';
end;

procedure TFCorrecaoCFOPEntradaProdutos.FormShow(Sender: TObject);
begin
      self.LimpaLabels;
      self.LimpaEdit;
      self.LimpaGrids;
      edtEntrada.SetFocus;
end;

procedure TFCorrecaoCFOPEntradaProdutos.lbCodigoFornecedorMouseLeave(
  Sender: TObject);
begin
      Negrita_Label(sender);
end;

procedure TFCorrecaoCFOPEntradaProdutos.lbCodigoFornecedorMouseMove(
  Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
      Sublinha_Label(sender,shift,x,y);
end;

procedure TFCorrecaoCFOPEntradaProdutos.lbCodigoFornecedorClick(
  Sender: TObject);
begin
      FFORNECEDOR.Tag := strtoint(lbCodigoFornecedor.Caption);
      FFORNECEDOR.ShowModal;
end;

procedure TFCorrecaoCFOPEntradaProdutos.lbProd_CodigoMouseLeave(
  Sender: TObject);
begin
      Negrita_Label(sender);
end;

procedure TFCorrecaoCFOPEntradaProdutos.lbProd_CodigoMouseMove(
  Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
      Sublinha_Label(sender,shift,x,y);
end;

procedure TFCorrecaoCFOPEntradaProdutos.lbProd_CodigoClick(
  Sender: TObject);
begin
      //Fproduto.Tag := strtoint(lbProd_Codigo.Caption);
    //  Fproduto.ShowModal;
end;

function TFCorrecaoCFOPEntradaProdutos.AtualizaProdutos: boolean;
var
    Linhas,cont:integer;
begin
      result:=false;
      //consulta query
      Linhas := RetornaProdutosEntrada(lbEnt_Codigo.Caption);

      if(linhas=1)
      then inc(linhas);

      if ((linhas=-1) or (linhas=0))
      then exit;

      //atualiza grid
      LimpaGrids;
      
      strgProdutosEntrada.ColCount:=14;
      strgProdutosEntrada.RowCount:=2;

      strgNovoCFOP.ColCount:=1;
      strgNovoCFOP.RowCount:=2;

      strgProdutosEntrada.Cells[0,0]:='CODIGO';
      strgProdutosEntrada.Cells[1,0]:='PRODUTO';
      strgProdutosEntrada.Cells[2,0]:='DESCRI��O';
      strgProdutosEntrada.Cells[3,0]:='CFOP';
      strgProdutosEntrada.Cells[4,0]:='VALOR UNIT�RIO';
      strgProdutosEntrada.Cells[5,0]:='DESCONTO';
      strgProdutosEntrada.Cells[6,0]:='EMBALAGEM/UNIDADE';
      strgProdutosEntrada.Cells[7,0]:='QUANTIDADE';
      strgProdutosEntrada.Cells[8,0]:='QUANTIDADE REAL';
      strgProdutosEntrada.Cells[9,0]:='UNIDADE';
      strgProdutosEntrada.Cells[10,0]:='PERCENTUAL IPI';
      strgProdutosEntrada.Cells[11,0]:='VALOR TOTAL';
      strgProdutosEntrada.Cells[12,0]:='CR�DITO ICMS';
      strgProdutosEntrada.Cells[13,0]:='ICMS PAGO FORNEC';

      strgNovoCFOP.Cells[0,0]:='NOVO CFOP';

{
            sql.add('tabproduto.descricao,');
            sql.add('Tabprodutosentradaprodutos.valorunitario,');
            sql.add('Tabprodutosentradaprodutos.Desconto,');
            sql.add('Tabprodutosentradaprodutos.EmbalagemUnidade,');
            sql.add('Tabprodutosentradaprodutos.Quantidade,');
            sql.add('Tabprodutosentradaprodutos.Unidade,');
            sql.add('Tabprodutosentradaprodutos.QuantidadeReal,');
            sql.add('Tabprodutosentradaprodutos.PercentualIPI,');
            sql.add('Tabprodutosentradaprodutos.ValorTotal,');
            sql.add('Tabprodutosentradaprodutos.produto as CODPROD,');
            sql.add('Tabprodutosentradaprodutos.CREDITOICMS,');
            sql.add('Tabprodutosentradaprodutos.codigo,');
            SQL.Add('Tabprodutosentradaprodutos.ICMSPagoFornecedor,');
            SQL.Add('Tabprodutosentradaprodutos.CFOP');

      objProdutosEntradaProdutos.QueryTMP.First;
      cont := 1;
      while not (objProdutosEntradaProdutos.QueryTMP.eof) do
      begin
            strgProdutosEntrada.Cells[0,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('CODIGO').asstring;
            strgProdutosEntrada.Cells[1,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('CODPROD').asstring;
            strgProdutosEntrada.Cells[2,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('DESCRICAO').asstring;
            strgProdutosEntrada.Cells[3,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('CFOP').asstring;
            strgProdutosEntrada.Cells[4,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('VALORUNITARIO').asstring;
            strgProdutosEntrada.Cells[5,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('DESCONTO').asstring;
            strgProdutosEntrada.Cells[6,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('EMBALAGEMUNIDADE').asstring;
            strgProdutosEntrada.Cells[7,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('QUANTIDADE').asstring;
            strgProdutosEntrada.Cells[8,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('QUANTIDADEREAL').asstring;
            strgProdutosEntrada.Cells[9,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('UNIDADE').asstring;
            strgProdutosEntrada.Cells[10,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('PERCENTUALIPI').asstring;
            strgProdutosEntrada.Cells[11,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('VALORTOTAL').asstring;
            strgProdutosEntrada.Cells[12,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('CREDITOICMS').asstring;
            strgProdutosEntrada.Cells[13,cont]:=objProdutosEntradaProdutos.QueryTMP.fieldbyname('ICMSPAGOFORNECEDOR').asstring;
            objProdutosEntradaProdutos.QueryTMP.Next;
            strgProdutosEntrada.RowCount := strgProdutosEntrada.RowCount + 1;
            strgNovoCFOP.RowCount := strgNovoCFOP.RowCount + 1;
            inc(cont);
      end;

      AjustaLArguraColunaGrid(strgProdutosEntrada);
      AjustaLArguraColunaGrid(strgNovoCFOP);
     }
end;

function TFCorrecaoCFOPEntradaProdutos.RetornaProdutosEntrada(
  pEntrada: string): integer;
begin
      result := -1;
     { with objProdutosEntradaProdutos.QueryTMP do
      begin

            Close;
            sql.clear;
            sql.add('Select');
            sql.add('tabproduto.descricao,');
            sql.add('Tabprodutosentradaprodutos.valorunitario,');
            sql.add('Tabprodutosentradaprodutos.Desconto,');
            sql.add('Tabprodutosentradaprodutos.EmbalagemUnidade,');
            sql.add('Tabprodutosentradaprodutos.Quantidade,');
            sql.add('Tabprodutosentradaprodutos.Unidade,');
            sql.add('Tabprodutosentradaprodutos.QuantidadeReal,');
            sql.add('Tabprodutosentradaprodutos.PercentualIPI,');
            sql.add('Tabprodutosentradaprodutos.ValorTotal,');
            sql.add('Tabprodutosentradaprodutos.produto as CODPROD,');
            sql.add('Tabprodutosentradaprodutos.CREDITOICMS,');
            sql.add('Tabprodutosentradaprodutos.codigo,');
            SQL.Add('Tabprodutosentradaprodutos.ICMSPagoFornecedor,');
            SQL.Add('Tabprodutosentradaprodutos.CFOP');
            sql.add('from Tabprodutosentradaprodutos');
            sql.add('join Tabproduto on ');
            sql.add('Tabprodutosentradaprodutos.produto=tabproduto.codigo');
            sql.add('where Tabprodutosentradaprodutos.entrada='+PEntrada);
            try

                  //InputBox('','',sql.Text);

                  objProdutosEntradaProdutos.QueryTMP.Open;

                  last;
            except
                  on e:exception do
                  begin
                        MensagemErro(e.message);
                        exit;
                  end;
            end;
            result := objProdutosEntradaProdutos.QueryTMP.RecordCount;

      end;     }
end;

procedure TFCorrecaoCFOPEntradaProdutos.LimpaGrids;
var
    cont:integer;
begin
     for cont := 0 to strgProdutosEntrada.RowCount do
          strgProdutosEntrada.Rows[cont].Clear;

     for cont := 0 to strgProdutosEntrada.RowCount do
          strgNovoCFOP.Rows[cont].Clear;

     strgProdutosEntrada.Cols[0].clear;
     strgProdutosEntrada.ColCount:=2;
     strgProdutosEntrada.RowCount:=2;

     strgProdutosEntrada.Rows[0].clear;
     strgProdutosEntrada.Rows[1].clear;

     strgProdutosEntrada.fixedrows:=1;

     strgNovoCFOP.Cols[0].clear;
     strgNovoCFOP.ColCount:=1;
     strgNovoCFOP.RowCount:=1;

     strgNovoCFOP.Rows[0].clear;

     //strgNovoCFOP.FixedCols:=1;
     //strgNovoCFOP.fixedrows:=1;

      AjustaLArguraColunaGrid(strgProdutosEntrada);

end;

procedure TFCorrecaoCFOPEntradaProdutos.strgProdutosEntradaDblClick(
  Sender: TObject);
var
      NovoCFOP:string;
begin
    {  NovoCFOP := '';

      if(strgProdutosEntrada.Cells[0,strgProdutosEntrada.row]='')
      then exit;

      Self.CarregaProduto(strgProdutosEntrada.Cells[0,strgProdutosEntrada.row]);

      NovoCFOP := InputBox('Altera��o de CFOP','Novo CFOP:','');

      try
            StrToInt(NovoCFOP);
      except
            NovoCFOP := '';
      end;

      if(NovoCFOP<>'')
      then strgNovoCFOP.Cells[0,strgProdutosEntrada.Row] := NovoCFOP;

      {
      strgNovoCFOP.Row := strgProdutosEntrada.Row;
      strgNovoCFOP.Col := 0;
      strgNovoCFOP.SetFocus;
      //MensagemAviso('coluna:'+inttostr(strgProdutosEntrada.Col)+' linha:'+inttostr(strgProdutosEntrada.Row));
      }
end;

procedure TFCorrecaoCFOPEntradaProdutos.strgProdutosEntradaKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
      if(key=vk_return)
      then begin
            strgProdutosEntradaDblClick(self);      
      end;
end;

procedure TFCorrecaoCFOPEntradaProdutos.FormClose(Sender: TObject;var Action: TCloseAction);
begin

  if (criadoAqui) then
  //  objProdutosEntradaProdutos.Free;

end;

function TFCorrecaoCFOPEntradaProdutos.VerificaNovoCFOP: boolean;
var
    cont:integer;
begin
      result := false;
      for cont:=1 to strgNovoCFOP.RowCount do
      begin
            if(trim(strgNovoCFOP.Cells[0,cont])<>'')
            then begin
                  result := true;
                  break;
            end;
      end;
end;

function TFCorrecaoCFOPEntradaProdutos.ValidaCFOP(
  pCFOP: string): boolean;
begin
      result:= false;
      if(pCFOP='')
      then begin
            MensagemErro('CFOP n�o pode ser vazio');
            exit;
      end;
      {if not(objProdutosEntradaProdutos.CFOP.LocalizaCodigo(pcfop))
      then begin
            MensagemErro('CFOP inv�lido');
            exit;
      end;     }
      result := true;
end;

function TFCorrecaoCFOPEntradaProdutos.GravaCFOP: boolean;
var
    cont:integer;
begin
      result := false;

      if not(VerificaNovoCFOP)
      then begin
            MensagemErro('N�o h� novos CFOPS a serem salvos');
            exit;
      end;

    {  for cont:= 1 to strgNovoCFOP.RowCount do
      begin
            if(trim(strgNovoCFOP.Cells[0,cont])<>'')
            then begin
                  if(ValidaCFOP(strgNovoCFOP.Cells[0,cont]))
                  then begin
                        objProdutosEntradaProdutos.ZerarTabela;
                        if not(objProdutosEntradaProdutos.LocalizaCodigo(strgProdutosEntrada.Cells[0,cont]))
                        then begin
                              MensagemErro('Produto '+strgProdutosEntrada.Cells[0,cont]+'-'+strgProdutosEntrada.Cells[2,cont] +' da entrada n� '+lbEnt_Codigo.Caption+' n�o localizado');
                              exit;
                        end;
                        objProdutosEntradaProdutos.TabelaparaObjeto;
                        objProdutosEntradaProdutos.Status := dsedit;
                        objProdutosEntradaProdutos.CFOP.Submit_CODIGO(strgNovoCFOP.Cells[0,cont]);
                        if not(objProdutosEntradaProdutos.Salvar(false))
                        then begin
                              MensagemErro('N�o foi poss�vel alterar novo CFOP');
                              exit;
                        end;
                  end
                  else exit;
            end;
      end;
                }
      FDataModulo.IBTransaction.CommitRetaining;
      strgNovoCFOP.Cols[0].clear;
      result := true;
end;

procedure TFCorrecaoCFOPEntradaProdutos.btGravarClick(Sender: TObject);
begin
      if not(self.GravaCFOP)
      then begin
            MensagemErro('N�o foi poss�vel Salvar novos CFOPS');
      end
      else begin
            self.AtualizaProdutos;
            MensagemSucesso('Altera��es efetuadas com sucesso!!');
      end;
end;

procedure TFCorrecaoCFOPEntradaProdutos.CarregaProduto(pProdutoEP:string);
begin
     { objProdutosEntradaProdutos.ZerarTabela;
      if not(objProdutosEntradaProdutos.LocalizaCodigo(pProdutoEP))
      then begin
            MensagemErro('Produto da Entrada n�o localizado: '+pProdutoEP);
            exit;
      end;
      objProdutosEntradaProdutos.TabelaparaObjeto;
      objProdutosEntradaProdutos.Produto.LocalizaCodigo(objProdutosEntradaProdutos.Produto.Get_CODIGO);
      objProdutosEntradaProdutos.Produto.ZerarTabela;
      objProdutosEntradaProdutos.Produto.TabelaparaObjeto;

      lbProd_Descricao.Caption:=objProdutosEntradaProdutos.Produto.Get_descricao;
      lbProd_Codigo.Caption:=objProdutosEntradaProdutos.Produto.Get_CODIGO;
      lbProd_CFOP.Caption:=objProdutosEntradaProdutos.CFOP.Get_CODIGO;
      lbProd_PorcentagemReducao.Caption:=formata_valor(objProdutosEntradaProdutos.Get_PorcentagemReducao);
      if(objProdutosEntradaProdutos.Get_Substituicao='S')
      then lbProd_SituacaoEntrada.Caption:='SUBSTITUTO'
      else if(objProdutosEntradaProdutos.Get_isento='S')
            then lbProd_SituacaoEntrada.Caption:='ISENTO'
            else lbProd_SituacaoEntrada.Caption:='Tributado '+formata_valor(objProdutosEntradaProdutos.Get_CREDITOICMS)+'%';
      lbProd_IPI.Caption:=formata_valor(objProdutosEntradaProdutos.Get_PercentualIPI);
      lbProd_ValorPago.Caption:=formata_valor(objProdutosEntradaProdutos.Get_ValorTotal);
      lbProd_Quantidade.Caption:=formata_valor(objProdutosEntradaProdutos.Get_Quantidade);
      lbProd_Desconto.Caption:=formata_valor(objProdutosEntradaProdutos.Get_Desconto);     }







end;

end.
