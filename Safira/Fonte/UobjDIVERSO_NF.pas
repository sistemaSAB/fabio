unit Uobjdiverso_NF;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJdiversoCOR,uobjmaterial_nf;
//USES_INTERFACE



Type
   TObjdiverso_NF=class(TobjMaterial_NF)

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                diversoCOR:TOBJdiversoCOR ;

                Constructor Create(Owner:TComponent);
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                
                procedure EdtdiversoCORExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtdiversoCORKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);



         Private
               InsertSql,DeleteSql,ModifySQl:TStringList;
               Owner:TComponent;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls;





Function  TObjdiverso_NF.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        result:=False;
        
        result:=inherited TabelaparaObjeto;

        if (result=False)
        Then exit;

        If(FieldByName('diversoCOR').asstring<>'')
        Then Begin
                 If (Self.diversoCOR.LocalizaCodigo(FieldByName('diversoCOR').asstring)=False)
                 Then Begin
                          Messagedlg('diverso Cor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.diversoCOR.TabelaparaObjeto;
        End;
      

        result:=True;
     End;
end;


Procedure TObjdiverso_NF.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        inherited ObjetoparaTabela;
        
        ParamByName('diversoCOR').asstring:=Self.diversoCOR.GET_CODIGO;
  End;
End;

//***********************************************************************

function TObjdiverso_NF.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.Get_CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Get_Codigo='0')
              Then Self.Submit_codigo(Self.Get_NovoCodigo);
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       on e:exception do
       Begin
         if (Self.Status=dsInsert)
         Then Messagedlg('Erro na  tentativa de Inserir '+#13+E.message,mterror,[mbok],0)
         Else Messagedlg('Erro na  tentativa de Editar'+#13+E.message,mterror,[mbok],0);
         exit;
       End;

 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjdiverso_NF.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        inherited ZerarTabela;

        diversoCOR.ZerarTabela;
     End;
end;

Function TObjdiverso_NF.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
       Mensagem:=inherited VerificaBrancos;

       If (diversoCOR.get_codigo='')
       Then Mensagem:=mensagem+'/diverso Cor';
       
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjdiverso_NF.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
     mensagem:=inherited VerificaRelacionamentos;

     If (Self.diversoCOR.LocalizaCodigo(Self.diversoCOR.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ diverso Cor n�o Encontrado!';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;
End;

function TObjdiverso_NF.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';

     Mensagem:=inherited VerificaNumericos;

     try
        If (Self.diversoCOR.Get_Codigo<>'')
        Then Strtoint(Self.diversoCOR.Get_Codigo);
     Except
           Mensagem:=mensagem+'/diverso Cor';
     End;

     If (Mensagem<>'')
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjdiverso_NF.VerificaData: Boolean;
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';

     mensagem:=Inherited VerificaData;

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjdiverso_NF.VerificaFaixa: boolean;
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
        Mensagem:=Inherited VerificaFaixa;

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjdiverso_NF.LocalizaCodigo(parametro: string): boolean;//ok
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro c�digo do diverso NF vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select '+Self.RetornaCamposSelectSQL+',diversoCor');
           SQL.ADD(' from  TABdiverso_NF');
           SQL.ADD(' WHERE codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjdiverso_NF.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjdiverso_NF.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjdiverso_NF.create(Owner:TComponent);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin
        Self.Owner := Owner;
        inherited create(Self.Owner);//Crio o Pai primeiro
        
        //Self.Objquery:=TIBQuery.create(nil);
        //Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.diversoCOR:=TOBJdiversoCOR .create;
        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABdiverso_NF('+Self.MaterialInsertSql.text+',diversoCor)');
                InsertSQL.add('values ('+Self.MaterialInsertSqlValues.text+',:diversoCor)');

                ModifySQL.clear;
                ModifySQL.add('Update TABdiverso_NF set '+Self.MaterialModifySQl.Text);
                ModifySQL.add(',diversoCOR=:diversoCOR where codigo=:codigo');
                
                DeleteSQL.clear;
                DeleteSql.add('Delete from TABdiverso_NF where codigo=:codigo ');
                
                Self.status          :=dsInactive;
        End;

end;
procedure TObjdiverso_NF.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjdiverso_NF.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from Tabdiverso_NF');
     Result:=Self.ParametroPesquisa;
end;

function TObjdiverso_NF.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de diverso NF ';
end;


function TObjdiverso_NF.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENdiverso_NF,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjdiverso_NF.Free;
begin
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.diversoCOR.FREE;
    inherited free;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjdiverso_NF.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjdiverso_NF.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;


procedure TObjdiverso_NF.EdtdiversoCORExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.diversoCOR.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.diversoCOR.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.diversoCOR.diverso.Get_Descricao+'-'+Self.diversoCOR.Cor.Get_Descricao;
End;
procedure TObjdiverso_NF.EdtdiversoCORKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.diversoCOR.Get_Pesquisa,Self.diversoCOR.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.diversoCOR.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.diversoCOR.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.diversoCOR.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

end.



