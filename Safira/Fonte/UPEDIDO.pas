unit UPEDIDO;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls,db, 
  Tabs, Grids, DBGrids,UessencialGlobal, UObjPedidoObjetos,IBQuery, UTitulo_novo, UescolheNF,UobjNotaFiscalObjetos, UObjNOTAFISCALCFOP
  ,UARQUITETO,UFERRAGEM,UKITBOX,UPERFILADO,UPERSIANA,USERVICO,UVIDRO,UDIVERSO,UCLIENTE,UVENDEDOR,UobjVendas,ObjMateriaisVenda,UobjFERRAGEMCOR
  ,UObjVidro_ICMS,UobjFerragem_ICMS,UobjPerfilado_ICMS,UobjKitbox_ICMS,UobjPersiana_ICMS,UObjDiverso_ICMS,UMostraBarraProgresso,UmenuNfe,
  jpeg, Menus,UpesquisaMenu,UOrdemServico,UobjPROJETO_ICMS, ComCtrls,UVisulizarProjetos,
  pngimage, UobjTransmiteNFE, ImgList,Useg,UFiltraImp, xmldom, XMLIntf,
  msxmldom, XMLDoc;

type
  TFPEDIDO = class(TForm)
    lbNomeVidroCor_PP: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    MemoComplementoVidro_PP: TMemo;
    Label53: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigoPedido: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    edtPorcentagemAcrescimo: TEdit;
    edtvaloracrescimo: TEdit;
    btAnterior: TSpeedButton;
    btPrimeiro: TSpeedButton;
    btProximo: TSpeedButton;
    btUltimo: TSpeedButton;
    pnl1: TPanel;
    lbTitulo: TLabel;
    lbStatusPedido: TLabel;
    lb17: TLabel;
    lbDataPedido: TLabel;
    lbNumTitulo: TLabel;
    lbGravar1: TLabel;
    lbExcluir1: TLabel;
    lbCancelar: TLabel;
    lb38: TLabel;
    lb39: TLabel;
    lbValorProdutos: TLabel;
    lbValorTotal: TLabel;
    lbValor: TLabel;
    lb4: TLabel;
    lb2: TLabel;
    lbValorFinalPedido: TLabel;
    popMenuNFE: TPopupMenu;
    Consultarstatusdoservio1: TMenuItem;
    ConsultarstatusdaNFe1: TMenuItem;
    N1: TMenuItem;
    AbrirmenuNFe1: TMenuItem;
    bt1: TSpeedButton;
    pgc1: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    pnl2: TPanel;
    lbLbDescricao: TLabel;
    lbLbComplemento: TLabel;
    lbLbObra: TLabel;
    lb8: TLabel;
    lbLbCliente: TLabel;
    lbLbVendedor: TLabel;
    lbLblEmail: TLabel;
    lb60: TLabel;
    lb58: TLabel;
    lb5: TLabel;
    lb6: TLabel;
    lbnomearquiteto: TLabel;
    lbNomeCliente: TLabel;
    lbNomeVendedor: TLabel;
    lb56: TLabel;
    lbNFE: TLabel;
    lb7: TLabel;
    lb11: TLabel;
    lb3: TLabel;
    lb9: TLabel;
    btReplicarPedido: TSpeedButton;
    lbBonGeradaPedido: TLabel;
    lbComissaoGerada: TLabel;
    lb12: TLabel;
    lbBonGerada: TLabel;
    lb10: TLabel;
    lb35: TLabel;
    lbGerarNF: TLabel;
    lb57: TLabel;
    lbData: TLabel;
    memoObervacao: TMemo;
    edtDataEntrega: TMaskEdit;
    edtRetornaData: TEdit;
    edtemail: TEdit;
    edtVendedor: TEdit;
    edtCliente: TEdit;
    edtarquiteto: TEdit;
    edtObra: TEdit;
    edtDescricao: TEdit;
    edtValorComissaoArquiteto: TEdit;
    edtPorcentagemDesconto: TEdit;
    edtvalordesconto: TEdit;
    edtValorBonificacaoCliente: TEdit;
    panelPedidoProjeto: TPanel;
    lb13: TLabel;
    lb14: TLabel;
    lb15: TLabel;
    lb16: TLabel;
    lb18: TLabel;
    lb19: TLabel;
    lb20: TLabel;
    lb21: TLabel;
    lb22: TLabel;
    lb23: TLabel;
    lbNomeProjetoPedido: TLabel;
    lbNomeCorFerragemProjetoPedido: TLabel;
    lbNomeCorPerfiladoProjetoPedido: TLabel;
    lbNomeCorVidroProjetoPedido: TLabel;
    lb24: TLabel;
    lbNomeCorKitBoxProjetoPedido: TLabel;
    lb25: TLabel;
    lb26: TLabel;
    lb27: TLabel;
    lbNomeCorDiversoProjetoPedido: TLabel;
    imgDesenho: TImage;
    lb28: TLabel;
    lb54: TLabel;
    edtProjetoPedidoReferencia: TEdit;
    edtCorFerragemProjetoPedidoReferencia: TEdit;
    edtCorPerfiladoProjetoPedidoReferencia: TEdit;
    edtCorVidroProjetoPedidoReferencia: TEdit;
    edtLocalProjetoPedido: TEdit;
    ComboTipoProjetoPedido: TComboBox;
    edtCodigoProjetoPedido: TEdit;
    edtCorKitBoxProjetoPedidoReferencia: TEdit;
    edtvaloracrescimopedidoprojeto: TMaskEdit;
    edtvalordescontopedidoprojeto: TMaskEdit;
    edtProjetoPedido: TEdit;
    edtCorFerragemProjetoPedido: TEdit;
    edtCorPerfiladoProjetoPedido: TEdit;
    edtCorVidroProjetoPedido: TEdit;
    edtCorKitBoxProjetoPedido: TEdit;
    memoMedidas: TMemo;
    edtCorDiversoProjetoPedido: TEdit;
    edtCorDiversoProjetoPedidoReferencia: TEdit;
    edtobservacao: TEdit;
    ts3: TTabSheet;
    ts4: TTabSheet;
    ts5: TTabSheet;
    ts6: TTabSheet;
    ts7: TTabSheet;
    ts8: TTabSheet;
    ts9: TTabSheet;
    ts10: TTabSheet;
    pnl3: TPanel;
    lb36: TLabel;
    lbvalorfinalproj: TLabel;
    lbvalortotalproj: TLabel;
    lb41: TLabel;
    btGravarProjetoPedido: TBitBtn;
    btAlterarPedidoProjeto: TBitBtn;
    btBtExcluirProjetoPedido: TBitBtn;
    btCancelarProjetoPedido: TBitBtn;
    DBGridProjetoPedido: TDBGrid;
    panelFerragem_PP: TPanel;
    lbNomeFerragem_PP: TLabel;
    lb29: TLabel;
    lb30: TLabel;
    lb31: TLabel;
    lb32: TLabel;
    lb33: TLabel;
    lb34: TLabel;
    edtCodigoFerragem_PP: TEdit;
    edtFerragemCor_PPReferenicia: TEdit;
    edtQtdeFerragem_PP: TEdit;
    edtValorFerragem_PP: TEdit;
    edtFerragemCor_PP: TEdit;
    DBGridFerragem_PP: TDBGrid;
    panelPerfilado_PP: TPanel;
    lbnomePerfiladoCor_PP: TLabel;
    lb37: TLabel;
    lb40: TLabel;
    lb42: TLabel;
    lb43: TLabel;
    lb44: TLabel;
    lb45: TLabel;
    edtCodigoPerfilado_PP: TEdit;
    edtPerfiladoCor_PPReferencia: TEdit;
    edtQuantidadePerfilado_PP: TEdit;
    edtValorPerfilado_PP: TEdit;
    edtPerfiladoCor_PP: TEdit;
    DBGridPerfilado_PP: TDBGrid;
    panelVidro: TPanel;
    lb59: TLabel;
    lb61: TLabel;
    lb62: TLabel;
    lb63: TLabel;
    lb64: TLabel;
    lb65: TLabel;
    lb46: TLabel;
    lb47: TLabel;
    lb48: TLabel;
    lb49: TLabel;
    lb50: TLabel;
    lbNomeVidroCor_PP2: TLabel;
    lb55: TLabel;
    lb51: TLabel;
    lb52: TLabel;
    lb53: TLabel;
    edtCodigoVidro_PP2: TEdit;
    edtVidroCor_PPReferencia2: TEdit;
    edtQuantidadeVidro_PP2: TEdit;
    edtValorVidro_PP2: TEdit;
    edtVidroCor_PP2: TEdit;
    edtAlturaVidro_PP2: TEdit;
    edtLarguraVidro_PP2: TEdit;
    mmo1: TMemo;
    DBGridVidro_PP: TDBGrid;
    panelKitBox_PP: TPanel;
    lbNomeKitBoxCor_PP: TLabel;
    lb66: TLabel;
    lb67: TLabel;
    lb68: TLabel;
    lb69: TLabel;
    lb70: TLabel;
    lb71: TLabel;
    edtCodigoKitBox_PP: TEdit;
    edtKitBoxCor_PPReferencia: TEdit;
    edtQuantidadeKitBox_PP: TEdit;
    edtValorKitBox_PP: TEdit;
    edtKitBoxCor_PP: TEdit;
    DBGridKitBox_PP: TDBGrid;
    panelServico_PP: TPanel;
    lbnomeServico_PP: TLabel;
    lb72: TLabel;
    lb73: TLabel;
    lb74: TLabel;
    lb75: TLabel;
    lb76: TLabel;
    lb77: TLabel;
    lblLargServ: TLabel;
    lblAltServ: TLabel;
    edtCodigoServico_PP: TEdit;
    edtServico_PPReferencia: TEdit;
    edtValorServico_PP: TEdit;
    edtquantidadeservico: TEdit;
    edtServico_PP: TEdit;
    grpControla: TGroupBox;
    rbSim: TRadioButton;
    rbNao: TRadioButton;
    edtLargServ: TEdit;
    edtAltServ: TEdit;
    DBGridServico_PP: TDBGrid;
    panelPersiana_PP: TPanel;
    lbNomePersiana_PP: TLabel;
    lb78: TLabel;
    lb79: TLabel;
    lb80: TLabel;
    lb81: TLabel;
    lbAlturaPersiana_PP: TLabel;
    lbLarguraPersiana_PP: TLabel;
    lbmetrosAltura: TLabel;
    lbmetrosLargura: TLabel;
    lb82: TLabel;
    lb83: TLabel;
    lb84: TLabel;
    lb85: TLabel;
    lb86: TLabel;
    lb87: TLabel;
    edtCodigoPersiana_PP: TEdit;
    edtPersiana_PPReferencia: TEdit;
    edtQuantidadePersiana_PP: TEdit;
    edtValorPersiana_PP: TEdit;
    edtPersiana_PP: TEdit;
    memoComplementoPersiana_PP: TMemo;
    edtAlturaPersiana_PP: TEdit;
    edtLarguraPersiana_PP: TEdit;
    edtLocalPersiana_PP: TEdit;
    edtInstalacaoPersiana_PP: TEdit;
    edtComandoPersiana_PP: TEdit;
    DBGridPersiana_PP: TDBGrid;
    panelDiverso_PP: TPanel;
    lbnomeDiverso_PP: TLabel;
    lb88: TLabel;
    lb89: TLabel;
    lb90: TLabel;
    lbAlturaDiverso_PP: TLabel;
    lbLarguraDiverso_PP: TLabel;
    lbMensagemDiverso: TLabel;
    lbMetroQuadrado: TLabel;
    lbMilimetroAlturaDiversoPP: TLabel;
    lbMilimetroLarguraDiversoPP: TLabel;
    lb91: TLabel;
    lb92: TLabel;
    lb93: TLabel;
    edtCodigoDiverso_PP: TEdit;
    edtDiverso_PPReferencia: TEdit;
    edtQuantidadeDiverso_PP: TEdit;
    edtValorDiverso_PP: TEdit;
    edtAlturaDiverso_PP: TEdit;
    edtLarguraDiverso_PP: TEdit;
    edtDiverso_PP: TEdit;
    DBGridDiverso_PP: TDBGrid;
    panelVendaemLote: TPanel;
    lb94: TLabel;
    lb95: TLabel;
    lb96: TLabel;
    lb97: TLabel;
    lb98: TLabel;
    lbNomeVidro_VL: TLabel;
    lb99: TLabel;
    lb100: TLabel;
    lbGravar: TLabel;
    lbExcluir: TLabel;
    lbImprime: TLabel;
    edtquantidade_VL: TEdit;
    edtaltura_VL: TEdit;
    edtlargura_VL: TEdit;
    edtvalor_VL: TEdit;
    edtcomplementovidro_VL: TEdit;
    edtvidro_VL: TEdit;
    edtQuantidadePecas: TEdit;
    DBGridVidroVL_PP: TDBGrid;
    memocomplemento: TEdit;
    lb101: TLabel;
    edtLargura: TMaskEdit;
    lb102: TLabel;
    edtAltura: TMaskEdit;
    edtQuantidade: TMaskEdit;
    lb103: TLabel;
    btAjudaVideo: TSpeedButton;
    btEmProducao: TSpeedButton;
    btRomaneio: TSpeedButton;
    lbRomaneio: TLabel;
    PopUpImprimeromaneios: TPopupMenu;
    il2: TImageList;
    xmldcmnt1: TXMLDocument;
    grp5: TGroupBox;
    rbsimm2: TRadioButton;
    rbnaom2: TRadioButton;
    Label16: TLabel;
    comboprazonome: TComboBox;
    comboprazocodigo: TComboBox;
    PopupMenuNFe: TPopupMenu;
    VincularNFe1: TMenuItem;
    lbgerarCupom: TLabel;
    lbCupom: TLabel;
    lbTipoDocumento: TLabel;
    Label1: TLabel;
    lbValorServico: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    lbValorProdutos_p: TLabel;
    Label5: TLabel;
    edtOperacao: TEdit;
    lbOperacao: TLabel;
    imgrodape: TImage;
    procedure edtClienteKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtClienteExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtVendedorExit(Sender: TObject);
    procedure get(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btGravarProjetoPedidoClick(Sender: TObject);
    procedure ComboTipoProjetoPedidoKeyPress(Sender: TObject; var Key: Char);
    procedure edtProjetoPedidoReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCorFerragemProjetoPedidoReferenciaExit(Sender: TObject);
    procedure edtCorFerragemProjetoPedidoReferenciaKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edtCorPerfiladoProjetoPedidoReferenciaExit(Sender: TObject);
    procedure edtCorPerfiladoProjetoPedidoReferenciaKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edtCorVidroProjetoPedidoReferenciaExit(Sender: TObject);
    procedure edtCorVidroProjetoPedidoReferenciaKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure DBGridProjetoPedidoDblClick(Sender: TObject);
    procedure edtFerragemCor_PPRefereniciaKeyDown(Sender: TObject;
      var Key: Word;Shift: TShiftState);
    procedure btExcluirFerragem_PPClick(Sender: TObject);
    procedure btCancelarFeragem_PPClick(Sender: TObject);
    procedure edtPerfiladoCor_PPReferenciaExit(Sender: TObject);
    procedure edtPerfiladoCor_PPReferenciaKeyDown(Sender: TObject;
      var Key: Word;Shift: TShiftState);
    procedure EdtVidroCor_PPReferenciaExit(Sender: TObject);
    procedure btBtGravarVidro_PPClick(Sender: TObject);
    procedure EdtVidroCor_PPReferenciaKeyDown(Sender: TObject;
      var Key: Word;Shift: TShiftState);
    procedure btExcluirPerfilado_PPClick(Sender: TObject);
    procedure btGravarPerfilado_PPClick(Sender: TObject);
    procedure DBGridPerfilado_PPDblClick(Sender: TObject);
    procedure btCancelarPerfilado_PPClick(Sender: TObject);
    procedure DBGridFerragem_PPDblClick(Sender: TObject);
    procedure btBtExcluirVidro_PPClick(Sender: TObject);
    procedure btBtCancelarVidro_PPClick(Sender: TObject);
    procedure DBGridVidro_PPDblClick(Sender: TObject);
    procedure edtKitBoxCor_PPReferenciaExit(Sender: TObject);
    procedure edtKitBoxCor_PPReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btGravarKitBox_PPClick(Sender: TObject);
    procedure btExcluirKitBox_PPClick(Sender: TObject);
    procedure btCancelarKitBox_PPClick(Sender: TObject);
    procedure DBGridKitBox_PPDblClick(Sender: TObject);
    procedure btGravarServico_PPClick(Sender: TObject);
    procedure btExcluirServico_PPClick(Sender: TObject);
    procedure btCancelarServico_PPClick(Sender: TObject);
    procedure DBGridServico_PPDblClick(Sender: TObject);
    procedure edtServico_PPReferenciaExit(Sender: TObject);
    procedure edtServico_PPReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btGravarPersiana_PPClick(Sender: TObject);
    procedure btExcluirPersiana_PPClick(Sender: TObject);
    procedure btCancelarPersiana_PPClick(Sender: TObject);
    procedure DBGridPersiana_PPDblClick(Sender: TObject);
    procedure btGravarFerragem_PPClick(Sender: TObject);
    procedure edtPersiana_PPReferenciaExit(Sender: TObject);
    procedure edtPersiana_PPReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btGravarDiverso_PPClick(Sender: TObject);
    procedure btExcluirDiverso_PPClick(Sender: TObject);
    procedure btCancelarDiverso_PPClick(Sender: TObject);
    procedure DBGridDiverso_PPDblClick(Sender: TObject);
    procedure edtDiverso_PPReferenciaExit(Sender: TObject);
    procedure edtDiverso_PPReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btBtExcluirProjetoPedidoClick(Sender: TObject);
    procedure edtCorKitBoxProjetoPedidoReferenciaExit(Sender: TObject);
    procedure edtCorKitBoxProjetoPedidoReferenciaKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure btCancelarProjetoPedidoClick(Sender: TObject);
    procedure DBGridProjetoPedidoKeyPress(Sender: TObject; var Key: Char);
    procedure btCalcularClick(Sender: TObject);
    procedure edtFerragemCor_PPRefereniciaKeyUp(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edtPerfiladoCor_PPReferenciaKeyUp(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure EdtVidroCor_PPReferenciaKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtKitBoxCor_PPReferenciaKeyUp(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edtServico_PPReferenciaKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtPersiana_PPReferenciaKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDiverso_PPReferenciaKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtFerragemCor_PPRefereniciaExit(Sender: TObject);
    procedure DBGridFerragem_PPKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridPerfilado_PPKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridVidro_PPKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridKitBox_PPKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridServico_PPKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridPersiana_PPKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridDiverso_PPKeyPress(Sender: TObject; var Key: Char);
    procedure BtopcoesClick(Sender: TObject);
    procedure edtAlturaDiverso_PPExit(Sender: TObject);
    procedure edtLarguraDiverso_PPExit(Sender: TObject);
    procedure edtCorDiversoProjetoPedidoReferenciaExit(Sender: TObject);
    procedure edtCorDiversoProjetoPedidoReferenciaKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure edtvaloracrescimopedidoprojetoExit(Sender: TObject);
    procedure edtvalordescontopedidoprojetoExit(Sender: TObject);
    procedure edtValorFerragem_PPExit(Sender: TObject);
    procedure edtValorPerfilado_PPExit(Sender: TObject);
    procedure EdtValorVidro_PPExit(Sender: TObject);
    procedure edtValorKitBox_PPExit(Sender: TObject);
    procedure edtValorServico_PPExit(Sender: TObject);
    procedure edtValorPersiana_PPExit(Sender: TObject);
    procedure edtValorDiverso_PPExit(Sender: TObject);
    procedure btGerarVendaClick(Sender: TObject);
    procedure btTituloClick(Sender: TObject);
    procedure btAlterarPedidoProjetoClick(Sender: TObject);
    procedure edtAlturaPersiana_PPExit(Sender: TObject);
    procedure edtLarguraPersiana_PPExit(Sender: TObject);
    procedure memoComplementoPersiana_PPKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtQuantidadePersiana_PPExit(Sender: TObject);
    procedure memoObervacaoKeyPress(Sender: TObject; var Key: Char);
    procedure EdtAlturaVidro_PPExit(Sender: TObject);
    procedure memoObervacaoExit(Sender: TObject);
    procedure edtarquitetoExit(Sender: TObject);
    procedure edtarquitetoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtvalordescontoExit(Sender: TObject);
    procedure edtvaloracrescimoExit(Sender: TObject);
    procedure edtPorcentagemAcrescimoExit(Sender: TObject);
    procedure edtPorcentagemDescontoExit(Sender: TObject);
    procedure edtvidro_VLKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtvidro_VLExit(Sender: TObject);
    procedure Btgravar_VLClick(Sender: TObject);
    procedure btexcluir_VLClick(Sender: TObject);
    procedure edtvalor_VLKeyPress(Sender: TObject; var Key: Char);
    procedure edtquantidade_VLKeyPress(Sender: TObject; var Key: Char);
    procedure Btrelatorios_VLClick(Sender: TObject);
    procedure lbGravarMouseLeave(Sender: TObject);
    procedure lbGravarMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure NotebookMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure panelVendaemLoteMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGridVidroVL_PPMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGridVidroVL_PPDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGridFerragem_PPDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGridKitBox_PPDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGridPerfilado_PPDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGridPersiana_PPDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGridServico_PPDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGridVidro_PPDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGridDiverso_PPDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGridProjetoPedidoDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure edtQtdeFerragem_PPExit(Sender: TObject);
    procedure edtQuantidadeKitBox_PPExit(Sender: TObject);
    procedure edtQuantidadePerfilado_PPExit(Sender: TObject);
    procedure edtquantidadeservicoExit(Sender: TObject);
    procedure edtquantidade_VLExit(Sender: TObject);
    procedure EdtQuantidadeVidro_PPExit(Sender: TObject);
    procedure edtQuantidadeDiverso_PPExit(Sender: TObject);
    procedure lbNumTituloClick(Sender: TObject);
    procedure lbGerarNFClick(Sender: TObject);
    procedure edtarquitetoKeyPress(Sender: TObject; var Key: Char);
    procedure edtClienteKeyPress(Sender: TObject; var Key: Char);
    procedure edtVendedorKeyPress(Sender: TObject; var Key: Char);
    procedure edtvidro_VLKeyPress(Sender: TObject; var Key: Char);
    procedure edtDiverso_PPReferenciaKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPersiana_PPReferenciaKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPerfiladoCor_PPReferenciaKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtProjetoPedidoReferenciaKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtCorFerragemProjetoPedidoReferenciaKeyPress(
      Sender: TObject; var Key: Char);
    procedure edtCorVidroProjetoPedidoReferenciaKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtCorKitBoxProjetoPedidoReferenciaKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtCorDiversoProjetoPedidoReferenciaKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtCorPerfiladoProjetoPedidoReferenciaKeyPress(
      Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure lbNomeProjetoPedidoClick(Sender: TObject);
    procedure lbnomearquitetoClick(Sender: TObject);
    procedure lbNomeFerragem_PPClick(Sender: TObject);
    procedure lbNomeKitBoxCor_PPClick(Sender: TObject);
    procedure lbnomePerfiladoCor_PPClick(Sender: TObject);
    procedure lbNomePersiana_PPClick(Sender: TObject);
    procedure lbnomeServico_PPClick(Sender: TObject);
    procedure lbNomeVidro_VLClick(Sender: TObject);
    procedure lbnomeDiverso_PPClick(Sender: TObject);
    procedure lbNomeVidroCor_PP2Click(Sender: TObject);
    procedure lbNomeClienteClick(Sender: TObject);
    procedure lbNomeVendedorClick(Sender: TObject);
    procedure DBGridVidroVL_PPDblClick(Sender: TObject);
    procedure edtFerragemCor_PPRefereniciaDblClick(Sender: TObject);
    procedure edtKitBoxCor_PPReferenciaDblClick(Sender: TObject);
    procedure edtProjetoPedidoReferenciaDblClick(Sender: TObject);
    procedure edtPerfiladoCor_PPReferenciaDblClick(Sender: TObject);
    procedure edtPersiana_PPReferenciaDblClick(Sender: TObject);
    procedure edtVidroCor_PPReferencia2DblClick(Sender: TObject);
    procedure edtServico_PPReferenciaDblClick(Sender: TObject);
    procedure edtDiverso_PPReferenciaDblClick(Sender: TObject);
    procedure edtvidro_VLDblClick(Sender: TObject);
    procedure edtarquitetoDblClick(Sender: TObject);
    procedure edtClienteDblClick(Sender: TObject);
    procedure edtVendedorDblClick(Sender: TObject);
    procedure edtAltServExit(Sender: TObject); {Rodolfo}
    procedure edtCorFerragemProjetoPedidoReferenciaDblClick(
      Sender: TObject);
    procedure lbNFEMouseLeave(Sender: TObject);
    procedure lbNFEMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbNFEClick(Sender: TObject);
    procedure Consultarstatusdoservio1Click(Sender: TObject);
    procedure ConsultarstatusdaNFe1Click(Sender: TObject);
    procedure AbrirmenuNFe1Click(Sender: TObject);
    procedure lbDataPedidoClick(Sender: TObject);
    procedure edtlargura_VLExit(Sender: TObject);
    procedure lbReplicarPedidoMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure btReplicarPedidoClick(Sender: TObject);
    procedure edtQuantidadeVidro_PP2Exit(Sender: TObject);
    procedure lbNumOrdemServicoClick(Sender: TObject);
    procedure edtCorPerfiladoProjetoPedidoReferenciaDblClick(
      Sender: TObject);
    procedure edtCorVidroProjetoPedidoReferenciaDblClick(Sender: TObject);
    procedure edtCorKitBoxProjetoPedidoReferenciaDblClick(Sender: TObject);
    procedure edtCorDiversoProjetoPedidoReferenciaDblClick(
      Sender: TObject);
    procedure bt1Click(Sender: TObject);
    procedure edtaltura_VLExit(Sender: TObject);
    procedure GeraOrcamentoAutomatico(Descricao,arquiteto,cliente,vendedor:string;Sender: TObject;var CodidoPedido:string;Observacao:string);
    procedure edtRetornaDataExit(Sender: TObject);
    procedure rbSimClick(Sender: TObject);
    procedure rbNaoClick(Sender: TObject);
    procedure edtLargServExit(Sender: TObject);
    procedure edtProjetoPedidoReferenciaExit(Sender: TObject);
    procedure edtAltServKeyPress(Sender: TObject; var Key: Char);
    procedure edtServico_PPReferenciaClick(Sender: TObject);
    procedure edtLargServKeyPress(Sender: TObject; var Key: Char);
    procedure edtServico_PPReferenciaKeyPress(Sender: TObject;
      var Key: Char);
    procedure pgc1Change(Sender: TObject);
    procedure imgDesenhoClick(Sender: TObject);
    procedure btAjudaVideoClick(Sender: TObject);
    procedure btEmProducaoClick(Sender: TObject);
    procedure edtLarguraExit(Sender: TObject);
    procedure edtAlturaExit(Sender: TObject);
    procedure btRomaneioClick(Sender: TObject);
    procedure lbRomaneioClick(Sender: TObject);
    procedure edtVendedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure rbsimm2Click(Sender: TObject);
    procedure rbnaom2Click(Sender: TObject);
    procedure edtValorFerragem_PPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtValorPerfilado_PPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtValorVidro_PP2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtValorKitBox_PPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtValorServico_PPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtComandoPersiana_PPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtValorDiverso_PPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcomplementovidro_VLKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    function AtualizaPrecosNovoPrazoPedido(pPedido:string):boolean;
    function AtualizaPrecosNovoPrazoProjeto(pPedidoProjeto: string): boolean;
    procedure VincularNFe1Click(Sender: TObject);

    procedure edtMaterial_PPExit( Sender: TObject );
    function utilizaDescontoAcrescimopeloPrazo:Boolean;
    procedure DATAENTRAKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbgerarCupomClick(Sender: TObject);
    procedure edtOperacaoExit(Sender: TObject);
    procedure edtOperacaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbOperacaoMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbOperacaoMouseLeave(Sender: TObject);
    procedure lbOperacaoClick(Sender: TObject);
  private
     PimprimePedidoAposGerarVenda,PimprimeControleEntregaAposGerarVenda:boolean;
     ObjPedidoObjetos:TObjPedidoObjetos;
     fmenunfe:TFmenuNfe;
     objTransmiteNFE:TObjTransmiteNFE;
     item:TMenuItem;
     senha:string;
     PedeSenhaVendedor:string;
     PedeSenhaADMdesconto:string;
     DescontoAutorizadoPor:string;
     calculatotaltributos:Boolean;
     observacaoPadrao:string;

    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    Procedure LimpaLabels_Pedido_Proj;
    Procedure LimpaLabels_VidroPP;
    Procedure LimpaLabels_PersianaPP;
    Procedure LimpaLabels_DiversoPP;
    Procedure LimpaLabels_KitBoxPP;
    Procedure LimpaLabels_PerfialdoPP;
    Procedure LimpaLabels_ServicoPP;
    Procedure LimpaLabels_FerragemPP;
    Procedure ResgataPedidoProjeto;
    Procedure PreparaPedidoProjeto;
    Procedure ResgataFerragem_PP;
    Procedure PreparaFerragem_PP;
    Procedure LimpaLabelsPedidoProjeto;
    Procedure PreparaPerfilado_PP;
    Procedure ResgataPerfilado_PP;
    Procedure ResgataVidro_PP;
    Procedure PreparaVidro_PP;
    Procedure ResgataKitBox_PP;
    Procedure PreparaKitBox_PP;
    Procedure ResgataServico_PP;
    Procedure PreparaServico_PP;
    Procedure ResgataPersiana_PP;
    Procedure preparaPersiana_PP;
    Procedure ResgataDiverso_PP;
    Procedure preparaDiverso_PP;
    Procedure DesabilitaHabilitaBotoesGravarExcluir(Var PBtGravar, PBtExcluir :TBitBtn);
    procedure DesabilitaBotoesRegistro;
    procedure HabilitaBotoesRegistro;
    Procedure AtualizaPedidoProjeto(PPedidoProjeto:string);
    Procedure AtualizaValoresPedido(PPedido:string);
    Function  AlterouCoresdoPedidoProjeto:Boolean;
    procedure AtualizavalorEditsPedido;
    Procedure CarregaParametros;
    Function VerificaPedidoConcluido:Boolean;
    procedure GravaVendaNaTabVendas(pedido:String);
    procedure ApagaVendaDaTabvendas(pedido:string);
    function GravaMateriaisVendidos(pedido:string):Boolean;
    Function GravaMateriaisPedidoReplicado(ProjetoPedidoantigo:string;ProjetoPedido:string):Boolean;
    function GravaMateriaisPedidoBranco(pedido:string;TipoCliente:string;UFCliente:string):boolean;
    function GravaMedidasPedidoReplicado(ProjetoPedidoantigo:string;ProjetoPedido:string):Boolean;
    procedure onCliqueNF  (Sender:TObject);
    procedure onCliqueNFE (Sender:TObject);

    function ___VerificaEstoque:Boolean;
    procedure __VerificaProducaoPedido;

    function ___Verifica_LimiteLargura:Boolean;
    function ___Verficca_LimeteAltura:Boolean;
    function ___VerificaRomaneioPedido:Boolean;

    function Integrar_CorteCerto:Boolean;

    procedure popupClick(Sender: TObject);

    procedure posicionaComboPrazo( pCodigoPrazo:String );

    procedure LiberaVendaCupom;
    function AtualizaLabelsCupom:boolean;
    procedure getValorServicos(codigo: String);

  public
    Function PreenchePedidoParaConsultaRapida:Boolean;
  end;

var
  FPEDIDO: TFPEDIDO;
  NFgerada:Integer;
  Objvendas:Tobjvendas;
  auxStr : string;

implementation

uses Upesquisa, UobjCOR, UobjPERSIANA, UDataModulo,
  UessencialLocal, UobjPROJETO,UobjFERRAGEM,
  UobjPERSIANAGRUPODIAMETROCOR,
  UobjDIVERSOCOR, UobjCLIENTE, UTitulo,
  UobjFERRAGEM_PP_PEDIDO, UobjPEDIDO_PEDIDO,
  UobjPEDIDO_PROJ_PEDIDO, UobjVIDRO_PP_PEDIDO, UescolheImagemBotao, UPROJETO,
  UnotaFiscalEletronica, UData, Uprincipal, UAjuda, UvideosAjuda,
  UProducao, DateUtils, UMostraEstoque, UGeraRomaneioPedido, UROMANEIO,
  UobjFAIXADESCONTO_COMISSAO, UobjIMPOSTO_ICMS, UobjMEDIDAS_PROJ_PEDIDO,
  UobjSERVICO_PP_Pedido, UobjMaterial_ICMS, UOPERACAONF;

{$R *.dfm}






procedure TFPEDIDO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (self.ObjPedidoObjetos=Nil)
     Then exit;

     If (self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.status<>dsinactive)
     Then Begin
              Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
              abort;
              exit;
     End;

     self.ObjPedidoObjetos.free;
     FreeAndNil(objtransmitenfe);
     FreeAndNil(self.fmenunfe);
     item.Free;
     Tag:=0;
end;



procedure TFPEDIDO.btNovoClick(Sender: TObject);
begin
     if (SistemaemModoDemoGLOBAL=True)
     Then Begin
               if (FDataModulo.ContaRegistros('TABPEDIDO')>200)
               Then Begin
                         MostraMensagemSistemaDemo('S� � poss�vel cadastrar 200 Pedidos');
                         exit;
               End;
     End;

     btEmProducao.Visible:=False;

     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbCodigoPedido.caption:= '0';
     lbStatusPedido.caption:= 'Gerar Venda';
     lbGerarNF.Visible:=True;
     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.status:=dsInsert;
     pgc1.TabIndex:=0;
     lbData.Caption:=DateToStr(Date);
     lbDataPedido.Caption:=DateToStr(Date);
     EdtDescricao.setfocus;

     Self.AtualizavalorEditsPedido;


     edtValorComissaoArquiteto.text:='0';
     lbGerarNF.Visible:=False;
     lbgerarCupom.Visible := false;

     lbNFE.caption:='NF-e:';
     btRomaneio.Visible:=False;
     lbRomaneio.Visible:=False;
     posicionaComboPrazo(PRAZOPAGAMENTOPADRAO);
     memoObervacao.Lines.Text := observacaoPadrao;

end;

procedure TFPEDIDO.btSalvarClick(Sender: TObject);
var
  ObjfaixaDescontoComissao:TObjFAIXADESCONTO_COMISSAO;
  senhaadm:string;
begin

    if(PedeSenhaVendedor='SIM') THEN
    begin
            FfiltroImp.DesativaGrupos;
            FfiltroImp.Grupo02.Enabled:=True;
            FfiltroImp.edtgrupo02.Text:='';
            FfiltroImp.LbGrupo02.Caption:='Senha';
            FfiltroImp.edtgrupo02.PasswordChar:='#';
            
            while((senha<>useg.DesincriptaSenha(get_campoTabela('SENHARELATORIOS','codigo','tabvendedor',edtVendedor.Text))))do
            begin
               FfiltroImp.ShowModal;
               if(FfiltroImp.Tag = 0) then
               begin
                 //edtVendedor.SetFocus;
                 Exit;
               end;
               senha :=FfiltroImp.edtgrupo02.Text;
            end;

    end;


    if(lbCodigoPedido.Caption = '')
    then exit;

    if (VerificaPedidoConcluido=True)
    Then exit;

    { if not(Self.ObjPedidoObjetos.ValidaDescontomaximo(edtvalordesconto.Text,
      get_campoTabela('VALORTOTAL','CODIGO','TABPEDIDO',lbCodigoPedido.Caption),edtValorBonificacaoCliente.Text))
     then exit;       }

     try
          Try
            ObjfaixaDescontoComissao:=TObjFAIXADESCONTO_COMISSAO.create;
          Except
            Messagedlg('Erro na tentativa de Criar o Objeto de Faixa de Descontos da Comiss�o',mterror,[mbok],0);
            exit;
          End;

          if (ObjfaixaDescontoComissao.Localiza_desconto(ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Vendedor.FaixaDesconto.Get_CODIGO,edtPorcentagemDesconto.Text)=False)
          Then Begin
                    if(PedeSenhaADMdesconto='SIM')then
                     begin
                         DescontoAutorizadoPor := '';
                         FfiltroImp.DesativaGrupos;
                         FfiltroImp.Grupo01.Enabled:=True;
                         FfiltroImp.edtgrupo01.Text:='';
                         FfiltroImp.LbGrupo01.Caption:='Administrador';
                         FfiltroImp.Grupo02.Enabled:=True;
                         FfiltroImp.edtgrupo02.Text:='';
                         FfiltroImp.LbGrupo02.Caption:='Senha';
                         FfiltroImp.edtgrupo01.Color:=$005CADFE;
                         FfiltroImp.edtgrupo02.PasswordChar:='#';


                         FfiltroImp.edtgrupo01.OnKeyDown:=Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.edtVendedorKeyDown;
                         MensagemAviso('O Desconto aplicado n�o se enquadra em nenhuma faixa de desconto desse vendedor');

                         FfiltroImp.ShowModal;

                         If FfiltroImp.tag=0
                         Then exit;
                         if((FfiltroImp.edtgrupo02.Text<>useg.DesincriptaSenha(get_campoTabela('SENHARELATORIOS','codigo','tabvendedor',FfiltroImp.edtgrupo01.Text))) or (FfiltroImp.edtgrupo01.Text=''))then
                         begin
                             MensagemAviso('Senha inval�da');
                             Exit;
                         end
                         else
                         begin

                           if (ObjfaixaDescontoComissao.Localiza_desconto(get_campoTabela('faixadesconto','codigo','tabvendedor',FfiltroImp.edtgrupo01.Text),edtPorcentagemDesconto.Text)=False)
                           Then Begin
                              Messagedlg('O Desconto aplicado n�o se enquadra em nenhuma faixa de desconto desse vendedor',mterror,[mbok],0);
                              exit;
                           end;

                           DescontoAutorizadoPor:= get_campoTabela('codigo','codigo','tabvendedor',FfiltroImp.edtgrupo01.Text);
                         end;
                     end
                    else
                    begin
                      Messagedlg('O Desconto aplicado n�o se enquadra em nenhuma faixa de desconto desse vendedor',mterror,[mbok],0);
                      exit;
                    end;
          end;
     finally
          ObjfaixaDescontoComissao.free;
     end;


     If Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     if not Self.AtualizaPrecosNovoPrazoPedido(Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_codigo) then
       exit;

     If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.salvar(true)=False)
     Then exit;

     lbCodigoPedido.caption:=Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_codigo;
     habilita_botoes(Self);
     desabilita_campos(Self);

     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.LocalizaCodigo(lbCodigoPedido.caption)=True)
     Then begin
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.TabelaparaObjeto;
               Self.TabelaParaControles;
     End;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;


     if(DescontoAutorizadoPor<>'')then
     begin
       update_campoTabela('tabpedido','DESCONTOAUTORIZADOPOR',DescontoAutorizadoPor,'codigo',lbCodigoPedido.Caption);
     end;

    if(Integrar_CorteCerto=False)
    then MensagemErro('N�o foi poss�vel gerar arquivo para sincroniza��o SAB/Corte Certo');

end;

procedure TFPEDIDO.btAlterarClick(Sender: TObject);
begin

    if(lbCodigoPedido.Caption = '')
    then exit;

    If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Status=dsinactive) and (lbCodigoPedido.caption<>'')
    Then Begin
                if (VerificaPedidoConcluido=True)
                Then exit;



                if (lbStatusPedido.Caption = 'Pedido Conclu�do')
                Then Begin
                          Messagedlg('N�o � poss�vel alterar um pedido que foi conclu�do, retorno-o antes de de alterar!',mtinformation,[mbok],0);
                          exit;
                End;

                habilita_campos(Self);


               btnovo.Visible :=false;
               btalterar.Visible:=false;
               btpesquisar.Visible:=false;
               btrelatorio.Visible:=false;
               btexcluir.Visible:=false;
               btsair.Visible:=false;
               btopcoes.Visible :=false;

               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Status:=dsEdit;
               pgc1.TabIndex:=0;
               edtDescricao.setfocus;
               desab_botoes(Self);
               btSalvar.enabled:=True;
               BtCancelar.enabled:=True;
               btpesquisar.enabled:=True;


    End;

end;

procedure TFPEDIDO.btCancelarClick(Sender: TObject);
begin
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.cancelar;
     limpaedit(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true ;
     btopcoes.Visible :=true;
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     lbNFE.caption:='NF-e:';

end;

procedure TFPEDIDO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            FpesquisaLocal.NomeCadastroPersonalizacao:='TFPEDIDO.btPesquisarClick';
            If (FpesquisaLocal.PreparaPesquisa(Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_pesquisa,Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  if (Self.ObjPedidoObjetos.AtualizaValorPedido(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=false) then
                                  Begin
                                       MensagemErro('N�o foi poss�vel Atualizar o Valor do Pedido');
                                       exit;
                                  end;
                                  FDataModulo.IBTransaction.CommitRetaining;


                                  If Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.status<>dsinactive
                                  then exit;

                                  If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                                  __VerificaProducaoPedido;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
        pgc1.TabIndex:=0;
        // ImageDesenho.Visible:=false;
        FescolheImagemBotao.PegaFiguraImagem(ImgDesenho,'FOTO');
        Self.SetFocus;

end;

procedure TFPEDIDO.btExcluirClick(Sender: TObject);
begin

     If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.status<>dsinactive) or (lbCodigoPedido.caption='')
     Then exit;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     if(btEmProducao.Visible=True) then
     begin
          MensagemAviso('Existe uma ordem de produ��o ligada a este pedido, Conclua a Ordem de Produ��o N�');
          Exit;
     end;

     If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.LocalizaCodigo(lbCodigoPedido.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.TabelaparaObjeto;

     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_Concluido='S')then
     Begin
          MensagemErro('Este Pedido j� foi Conclu�do. Se deseja realmente exclu�-lo vec� deve retornar a venda.');
          exit;
     end;

     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.VerificaPedidoEmRomaneio(lbCodigoPedido.caption)=true)then
     Begin
          Exit;
     end;


     // Exclui relacionamentos
     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.ExcluiRelacionamentos(lbCodigoPedido.caption)=false)then
     Begin
          MensagemErro('Erro ao tentar excluir os relacionamentos');
          exit;
     end;

     If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.exclui(lbCodigoPedido.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     lbNFE.caption:='NF-e:';
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFPEDIDO.btRelatorioClick(Sender: TObject);
begin
    Self.ObjPedidoObjetos.Imprime(lbCodigoPedido.caption);
    pgc1.TabIndex:=0;

end;

procedure TFPEDIDO.btSairClick(Sender: TObject);
begin
    Self.close;
end;


procedure TFPEDIDO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFPEDIDO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFPEDIDO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin

     if (Key = VK_F1) then
    begin


      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('CADASTRO DE PEDIDOS');
         FAjuda.ShowModal;
    end;

end;

procedure TFPEDIDO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
   if (Key=#27)
   Then Self.Close;
   

end;

function TFPEDIDO.ControlesParaObjeto: Boolean;
var
    ValorComissaoArquiteto:Currency;

begin
  Try
    Self.AtualizavalorEditsPedido;
    With Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO do
    Begin
        Submit_Codigo(lbCodigoPedido.caption);
        Submit_Descricao(edtDescricao.text);
        Submit_Complemento(MemoComplemento.text);
        Submit_Obra(edtObra.text);
        Submit_Email(edtEmail.text); //email
        Cliente.Submit_codigo(edtCliente.text);
        Arquiteto.Submit_CODIGO(edtarquiteto.text);
        Vendedor.Submit_codigo(edtVendedor.text);
        submit_operacao(edtOperacao.Text);
        Submit_Titulo(lbNumTitulo.Caption);

        Submit_Data(lbData.Caption);
        Submit_DataOrcamento(lbDataPedido.Caption);
        if (lbStatuspedido.Caption='Pedido Conclu�do') then
        begin
              Submit_Concluido('S');
        end
        else if (lbStatusPedido.Caption='Gerar Venda')then
        begin
              Submit_Concluido('N');

        end;

        Submit_Observacao(MemoObervacao.Text);


        Submit_ValorTotal(tira_ponto(lbValorProdutos.Caption));
        //Submit_ValorTitulo(tira_ponto(edtvalortitulo.Text));
        Submit_Valoracrescimo(tira_ponto(edtvaloracrescimo.Text));
        Submit_Valordesconto(tira_ponto(edtvalordesconto.Text));
        //Submit_ValorComissaoArquiteto(tira_ponto(edtValorComissaoArquiteto.text));
        Submit_PorcentagemArquiteto(edtValorComissaoArquiteto.Text);

        ValorComissaoArquiteto:= ((StrToCurr(tira_ponto(lbValorFinalPedido.Caption)) * StrToCurr(edtValorComissaoArquiteto.text))/100);

        Submit_ValorComissaoArquiteto(tira_ponto(formata_valor(CurrToStr(ValorComissaoArquiteto))));

        Submit_ValorBonificacaoCliente(tira_ponto(edtValorBonificacaoCliente.text));
        Submit_BonificacaoGerada(tira_ponto(lbBonGeradaPedido.Caption));
        if(edtDataEntrega.Text<>'  /  /    ')
        then Submit_DataEntrega(edtDataEntrega.Text);

        if (comboprazonome.ItemIndex>-1) then
          Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrazoPagamento.Submit_CODIGO(comboprazocodigo.items[comboprazonome.itemindex])
        else
          Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrazoPagamento.Submit_CODIGO('');


        result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFPEDIDO.LimpaLabels;
begin
   LbNomeCliente.Caption:='';
   LbNomeVendedor.Caption:='';
   lbOperacao.Caption := '';
   lbnomearquiteto.Caption:='';
   //LbAlteradoportroca.caption:='';
   edtEmail.text:='';
   lbNumTitulo.Caption:='';
   lbTitulo.Visible:=false;
   lbData.Caption:='';
   lbDataPedido.Caption:='';
   lbValorProdutos.Caption:='';
   lbValorServico.Caption := '';
   lbValorProdutos_p.Caption := '';
   lbValorFinalPedido.Caption:='';
   lbBonGeradaPedido.Caption:='';
   lbGerarNF.Visible:=False;
   lbgerarCupom.Visible := False;
   lbComissaoGerada.Caption:='';
   lbNomeVidroCor_PP2.Caption:='';
   lbCodigoPedido.Caption:='';


end;

function TFPEDIDO.ObjetoParaControles: Boolean;
var
  Query:TIBQuery;
  nfe,pCodigoNFE,Numpedido:string;
  ValorComissaoArquiteto:Currency;

  //inclui valor dos servi�os
  valorProdutos:Currency;

  valorServicos:Currency;


begin
  try
    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;
  except

  end;
  LimpaLabels;
  try
        with query do
        begin
            Close;
            sql.Clear;
            SQL.Add('select numpedido,numero from tabnotafiscal where numpedido ='+#39+self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Codigo+#39);
            SQL.Add('and situacao=''I''');
            Open;
            Numpedido:=fieldbyname('numpedido').AsString;
            nfe:=fieldbyname('numero').AsString;

         {   close;
            sql.Clear;
            sql.add('select * from tabordemservico where pedido='+self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Codigo);
            Open;
            Last;
            if(recordcount>0)
            then begin
                 lbNumOrdemServico.Caption:=fieldbyname('codigo').AsString;
                 lbNumOrdemServico.Visible:=True;
            end;    }

        end;
  finally
       FreeAndNil(Query);
  end;


  Try
     With Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO do
     Begin
        lbCodigoPedido.caption:=Get_Codigo;
        EdtDescricao.text:=Get_Descricao;
        MEmoComplemento.text:=Get_Complemento;
        EdtObra.text:=Get_Obra;
        EdtCliente.text:=Cliente.Get_codigo;
        Edtarquiteto.text:=Arquiteto.Get_CODIGO;
        lbnomearquiteto.Caption:=Arquiteto.Get_Nome;
        LbNomeCliente.Caption:=Cliente.Get_Nome;
        EdtVendedor.text:=Vendedor.Get_codigo;

        edtOperacao.Text := get_operacao;
        edtOperacaoExit(edtOperacao);

        LbNomeVendedor.Caption:=Vendedor.Get_Nome;
        lbNumTitulo.Caption:=Get_Titulo;
        if(lbNumTitulo.Caption='') then
        begin
            lbTitulo.visible:= False;
        end
        else
        begin
           lbTitulo.Visible:=True;
        end;
        lbData.Caption:=Get_Data;
        lbDataPedido.Caption:=Get_DataOrcameto;
        edtEmail.text := Get_Email;

        if Get_Concluido = 'S' then
        begin
              lbStatusPedido.Caption:='Pedido Conclu�do';
              lbGerarNF.Visible:=True;
              lbgerarCupom.Visible := True;
              lbData.Visible:=True;
              btRomaneio.Visible:=True;
              lbRomaneio.Visible:=True;
              ___VerificaRomaneioPedido;
              {jonas if}
              if not (UtilizaNfe) then
              begin

                lbGerarNF.PopupMenu:=nil;

                if(Numpedido=Get_Codigo)
                then begin
                    lbGerarNF.Caption:='Reimprimir Nota Fiscal';
                    NFgerada:=1;
                end
                else
                begin
                  lbGerarNF.Caption:='Gerar Nota Fiscal';
                  NFgerada:=0;
                end;

              end else
              begin

                lbGerarNF.PopupMenu:=self.popMenuNFE;
                
                if(Numpedido=Get_Codigo)
                then begin
                    lbGerarNF.Caption:='Reimprimir NF-e';
                    NFgerada:=1;

                    lbNFE.Caption:='NF-e: '+nfe;

                end
                else
                begin
                  lbGerarNF.Caption:='Gerar NF-e';
                  NFgerada:=0;
                  lbNFE.Caption:= 'NF-e:';
                end;

              end;
              {-}


        end
        else if Get_Concluido = 'N' then
        begin
              lbStatusPedido.Caption:='Gerar Venda';
              lbGerarNF.Visible:=False;
              lbgerarCupom.Visible := false;
              lbData.Visible:=False;
              btRomaneio.Visible:=False;
              lbRomaneio.Visible:=False;
              lbNFE.Caption:= 'NF-e:';
        end;

        lbValorProdutos.Caption:=Formata_Valor(Get_ValorTotal);
        //valorProdutos := StrToCurrDef( format_si(Get_ValorTotal),0 ); se vier 18689,66 transforma para 1868966
        valorProdutos := StrToCurrDef( Get_ValorTotal,0 );

        getValorServicos(Get_Codigo());
        valorServicos := StrToCurr( tira_ponto(lbValorServico.Caption) );

        if (valorProdutos > 0) then
          lbValorProdutos_p.Caption :=  formata_valor(valorProdutos-valorServicos)
        else
          lbValorProdutos_p.Caption := '0,00';

        edtPorcentagemDesconto.Text := '';
        edtRetornaData.Text := '';

        edtvaloracrescimo.Text:=formata_valor(Get_Valoracrescimo);
        edtvalordesconto.Text:=formata_valor(Get_Valordesconto);
        lbValorFinalPedido.Caption:=formata_Valor(Get_Valorfinal);
        MemoObervacao.Text:=Get_Observacao;
        edtvalorComissaoArquiteto.text:=formata_valor(Get_PorcentagemArquiteto);
        edtValorBonificacaoCliente.text:=Get_ValorBonificacaoCliente;
        lbBonGeradaPedido.Caption:=formata_valor(Get_BonificacaoGerada);

        lbComissaoGerada.Caption:=get_ValorComissaoArquiteto;
        edtDataEntrega.Text:=Get_DataEntrega;

       posicionaComboPrazo(self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.prazopagamento.Get_codigo);
       AtualizaLabelsCupom;
        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFPEDIDO.TabelaParaControles: Boolean;
begin
     If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFPEDIDO.edtClienteExit(Sender: TObject);
var
  Query:TIBQuery;
begin
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtClienteExit(Sender, LbNomeCliente);
    try
         try
             Query:=TIBQuery.Create(nil);
             Query.Database:=FDataModulo.IBDatabase;
             with Query do
             begin
                  if(lbNomeCliente.Caption='')
                  then exit;
                  Close;
                  sql.Clear;
                  sql.Add('select email from tabcliente where codigo='+EdtCliente.Text);
                  Open;
                  edtemail.text:=fieldbyname('email').AsString;
             end;
         except

         end;

    finally
      FreeAndNil(Query);
    end;
end;

procedure TFPEDIDO.edtClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtClienteKeyDown(Sender, Key, Shift, LbNomeCliente);
end;

procedure TFPEDIDO.edtVendedorExit(Sender: TObject);
begin
      Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtVendedorExit(Sender, LbNomeVendedor);
      if(PedeSenhaVendedor='SIM') THEN
     begin
            FfiltroImp.DesativaGrupos;
            FfiltroImp.Grupo02.Enabled:=True;
            FfiltroImp.edtgrupo02.Text:='';
            FfiltroImp.LbGrupo02.Caption:='Senha';
            FfiltroImp.edtgrupo02.PasswordChar:='#';

            FfiltroImp.ShowModal;

            while(FfiltroImp.edtgrupo02.Text<>useg.DesincriptaSenha(get_campoTabela('SENHARELATORIOS','codigo','tabvendedor',edtVendedor.Text)))
                  and(FfiltroImp.edtgrupo02.Text<>'') do
            begin
               FfiltroImp.ShowModal;
               if (FfiltroImp.Tag = 0) then
               begin
                 //edtVendedor.SetFocus;
                 Exit;
               end;
            end;

            senha :=FfiltroImp.edtgrupo02.Text;

     end;
end;

procedure TFPEDIDO.get(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

end;

//Requisi��o
procedure TFPEDIDO.btGravarProjetoPedidoClick(Sender: TObject);
var
Pvalorfinal,pvalor:Currency;
PedidoProjetoLocal:string;
Quantidade:integer;
begin
    Pvalorfinal:=0;
    Pvalor:=0;
  Try

    if (VerificaPedidoConcluido=True)
    Then exit;

    if(edtLargura.Text<>'') and (edtAltura.Text<>'')then
    begin
      if(___Verifica_LimiteLargura=False)
      then Exit;

      if(___Verficca_LimeteAltura=False)
      then Exit;
    end;

    With Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto do
    Begin
      //Grava na Tabela Pedido projeto, os dados iniciais
      ZerarTabela;
      Status:=dsInsert;
      edtcodigoProjetoPedido.Text:='0';
      Submit_Codigo(edtcodigoProjetoPedido.Text);
      Pedido.Submit_Codigo(lbCodigoPedido.caption);
      Projeto.Submit_Codigo(EdtProjetoPedido.Text);
      CorFerragem.Submit_Codigo(EdtCorFerragemProjetoPedido.Text);
      CorPerfilado.Submit_Codigo(EdtCorPerfiladoProjetoPedido.Text);
      CorVidro.Submit_Codigo(EdtCorVidroProjetoPedido.Text);
      CorKitBox.Submit_Codigo(EdtCorKitBoxProjetoPedido.Text);
      CorDiverso.Submit_Codigo(EdtCorDiversoProjetoPedido.Text);
      Submit_Local(EdtLocalProjetoPedido.Text);
      Submit_observacao(Edtobservacao.Text);
      Submit_TipoPedido(ComboTipoProjetoPedido.Text);
      Submit_ValorTotal    (tira_ponto(lbvalortotalproj.Caption));
      Submit_ValorDesconto ('0');
      Submit_ValorAcrescimo(tira_ponto(edtvaloracrescimopedidoprojeto.Text));

      if (Salvar(false)=False)
      Then Begin
        EdtProjetoPedidoReferencia.SetFocus;
        exit;
      End;
      EdtCodigoProjetoPedido.Text:=Get_Codigo;

      if (Self.ObjPedidoObjetos.GravaRelacionamentos(EdtCodigoProjetoPedido.Text,edtLargura.Text,edtAltura.Text)=False)
      Then Begin
        EdtCodigoProjetoPedido.Text:='';
        FDataModulo.IBTransaction.RollbackRetaining;
        Messagedlg('Erro na tentativa de Gravar os Relacionamentos: Ferragem,Perfilado,Vidro,KitBox,Servico,Persiana,Diversos',mterror,[mbok],0);
        exit;
      End;
      if (Self.ObjPedidoObjetos.AtualizaValorPedido(lbCodigoPedido.caption)=false)then
      Begin
        MensagemErro('N�o foi poss�vel Atualizar o Pedido');
        FDataModulo.IBTransaction.RollbackRetaining;
        exit;
      end;

      //altera��o dos valores com base no prazo de pagamento
      if not Self.AtualizaPrecosNovoPrazoProjeto(EdtCodigoProjetoPedido.Text) then
        Exit;

      if(edtQuantidade.Text='')
      then edtQuantidade.Text:='1';
      Quantidade:=StrToInt(edtQuantidade.Text);
      Quantidade:=Quantidade-1;
      if(Quantidade>0) then
      begin
        ObjPedidoObjetos.ReplicarProjetoPublico(edtCodigoProjetoPedido.Text,Quantidade);
      end;

      FDataModulo.IBTransaction.CommitRetaining;

      EdtProjetoPedidoReferencia.SetFocus;
      Self.ResgataPedidoProjeto;
      Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
      Self.AtualizaValoresPedido(lbCodigoPedido.caption);
      limpaedit(PanelPedidoProjeto);
      LimpaLabels_Pedido_Proj;

    End;

    if(Integrar_CorteCerto=False)
    then MensagemErro('N�o foi poss�vel gerar arquivo para sincroniza��o SAB/Corte Certo');

  Finally
       FDataModulo.IBTransaction.RollbackRetaining;
  End;
end;

procedure TFPEDIDO.LimpaLabels_Pedido_Proj;
begin
   lbNomeProjetoPedido.Caption:='';
   lbNomeCorFerragemProjetoPedido.Caption:='';
   lbNomeCorPerfiladoProjetoPedido.Caption:='';
   lbNomeCorVidroProjetoPedido.Caption:='';
   lbNomeCorKitBoxProjetoPedido.Caption:='';
   lbNomeCorDiversoProjetoPedido.Caption:='';

end;

procedure TFPEDIDO.ResgataPedidoProjeto;
begin
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.ResgataPEDIDO_PROJ(lbCodigoPedido.caption);
     formatadbgrid(DBGridProjetoPedido);
end;

procedure TFPEDIDO.PreparaPedidoProjeto;
begin
     Self.ResgataPedidoProjeto;
     habilita_campos(PanelPedidoProjeto);
end;

procedure TFPEDIDO.ComboTipoProjetoPedidoKeyPress(Sender: TObject;
  var Key: Char);
begin
    Abort;
end;

procedure TFPEDIDO.edtProjetoPedidoReferenciaExit(Sender: TObject);
Var CaminhoDesenho:string;
begin
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtProjetoExit(Sender,EdtProjetoPedido, lbNomeProjetoPedido);

    if (EdtProjetoPedidoReferencia.Text='')then
    Begin
         ImgDesenho.Visible:=false;
         btCancelarProjetoPedidoClick(Sender);  {Rodolfo}
         exit;
    end;

    if (ComboTipoProjetoPedido.Text='')then
    ComboTipoProjetoPedido.Text:='INSTALADO';

    if (Edtvaloracrescimopedidoprojeto.Text='')then
    Edtvaloracrescimopedidoprojeto.Text:='0';

    if (edtvalordescontopedidoprojeto.Text='')then
    edtvalordescontopedidoprojeto.Text:='0';

    if (EdtCorFerragemProjetoPedidoReferencia.Text='')then
    EdtCorFerragemProjetoPedidoReferencia.Text:='0';

    if (EdtCorPerfiladoProjetoPedidoReferencia.Text='')then
    EdtCorPerfiladoProjetoPedidoReferencia.Text:='0';

    if (EdtCorVidroProjetoPedidoReferencia.Text='')then
    EdtCorVidroProjetoPedidoReferencia.Text:='0';

    if (EdtCorKitBoxProjetoPedidoReferencia.Text='')then
    EdtCorKitBoxProjetoPedidoReferencia.Text:='0';

    if (EdtCorDiversoProjetoPedidoReferencia.Text='')then
    EdtCorDiversoProjetoPedidoReferencia.Text:='0';

     try
        ImgDesenho.Visible:=true;
        CaminhoDesenho:=Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataDesenho(EdtProjetoPedidoReferencia.Text);

        if (CaminhoDesenho<>'')then
        ImgDesenho.Picture.LoadFromFile(CaminhoDesenho)
        else
        ImgDesenho.Visible:=false;

     except
         MensagemErro('Erro ao tentar transferir o desenho.');
     end;
end;


procedure TFPEDIDO.edtProjetoPedidoReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtProjetoKeyDown(Sender, EdtProjetoPedido, Key, Shift, lbNomeProjetoPedido);
end;


procedure TFPEDIDO.edtCorFerragemProjetoPedidoReferenciaExit(Sender: TObject);
begin
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorFerragemExit(Sender, EdtCorFerragemProjetoPedido, lbNomeCorFerragemProjetoPedido);

end;


procedure TFPEDIDO.edtCorFerragemProjetoPedidoReferenciaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorFerragemKeyDown(Sender,EdtCorFerragemProjetoPedido,  Key, Shift, lbNomeCorFerragemProjetoPedido,EdtProjetoPedido.text);
end;


procedure TFPEDIDO.edtCorPerfiladoProjetoPedidoReferenciaExit(Sender: TObject);
begin
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorPerfiladoExit(Sender, EdtCorPerfiladoProjetoPedido, lbNomeCorPerfiladoProjetoPedido);
end;


procedure TFPEDIDO.edtCorPerfiladoProjetoPedidoReferenciaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorPerfiladoKeyDown(Sender,EdtCorPerfiladoProjetoPedido, Key, shift, lbNomeCorPerfiladoProjetoPedido,EdtProjetoPedido.text);
end;


procedure TFPEDIDO.edtCorVidroProjetoPedidoReferenciaExit(Sender: TObject);
begin
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorVidroExit(Sender, EdtCorVidroProjetoPedido, lbNomeCorVidroProjetoPedido);

end;


procedure TFPEDIDO.edtCorVidroProjetoPedidoReferenciaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorVidroKeyDown(Sender, EdtCorVidroProjetoPedido, Key, Shift, lbNomeCorVidroProjetoPedido,edtprojetopedido.text);
end;  


procedure TFPEDIDO.DBGridProjetoPedidoDblClick(Sender: TObject);
Var CaminhoDesenho:string;
altura,largura:string;
begin
     If (DBGridProjetoPedido.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridProjetoPedido.DataSource.DataSet.RecordCount=0)
     Then exit;

     limpaedit(PanelPedidoProjeto);
    // LimpaLabels;

     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(DBGridProjetoPedido.DataSource.DataSet.fieldbyname('PedidoProjeto').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataPedidoProjeto;
               exit;
     End;

     With Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto do
     Begin
        TabelaparaObjeto;
        edtcodigoProjetoPedido.Text:=Get_Codigo;

        EdtProjetoPedido.Text:=Projeto.Get_Codigo;
        EdtProjetoPedidoReferencia.Text:=Projeto.Get_Referencia;
        lbNomeProjetoPedido.Caption:=Projeto.Get_Descricao;

        EdtCorFerragemProjetoPedido.Text:=CorFerragem.Get_Codigo;
        EdtCorFerragemProjetoPedidoReferencia.Text:=CorFerragem.Get_Referencia;
        lbNomeCorFerragemProjetoPedido.Caption:=CorFerragem.Get_Descricao;

        EdtCorPerfiladoProjetoPedido.Text:=CorPerfilado.Get_Codigo;
        EdtCorPerfiladoProjetoPedidoReferencia.Text:=CorPerfilado.Get_Referencia;
        lbNomeCorPerfiladoProjetoPedido.Caption:=CorPerfilado.Get_Descricao;

        EdtCorVidroProjetoPedido.Text:=CorVidro.Get_Codigo;
        EdtCorVidroProjetoPedidoReferencia.Text:=CorVidro.Get_Referencia;
        lbNomeCorVidroProjetoPedido.Caption:=CorVidro.Get_Descricao;

        EdtCorKitBoxProjetoPedido.Text:=CorKitBox.Get_Codigo;
        EdtCorKitBoxProjetoPedidoReferencia.Text:=CorKitBox.Get_Referencia;
        lbNomeCorKitBoxProjetoPedido.Caption:=CorKitBox.Get_Descricao;

        EdtCorDiversoProjetoPedido.Text:=CorDiverso.Get_Codigo;
        EdtCorDiversoProjetoPedidoReferencia.Text:=CorDiverso.Get_Referencia;
        lbNomeCorDiversoProjetoPedido.Caption:=CorDiverso.Get_Descricao;

        EdtLocalProjetoPedido.Text:=Get_Local;
        edtobservacao.Text:=Get_observacao;
        ComboTipoProjetoPedido.Text:=Get_TipoPedido;
        lbvalortotalproj.Caption:=formata_valor(Get_ValorTotal);
        edtvalordescontopedidoprojeto.text:=formata_valor(Get_ValorDesconto);
        edtvaloracrescimopedidoprojeto.text:=formata_valor(Get_ValorAcrescimo);
        lbvalorfinalproj.Caption:=formata_valor(Get_ValorFinal);

        Self.ObjPedidoObjetos.RetornaMedidas(MemoMedidas,EdtCodigoProjetoPedido.Text,largura,altura);
        edtLargura.Text:=largura;
        edtAltura.Text:=altura;

        //Desenho
        try
            ImgDesenho.Visible:=true;
            CaminhoDesenho:=Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.ResgataDesenho(EdtProjetoPedidoReferencia.Text);

            if (CaminhoDesenho<>'')then
            ImgDesenho.Picture.LoadFromFile(CaminhoDesenho)
            else
            ImgDesenho.Visible:=false;

        except
             MensagemErro('Erro ao tentar transferir o desenho.');
        end;
     End;

end;


procedure TFPEDIDO.ResgataFerragem_PP;
begin
      Self.ObjPedidoObjetos.ObjFerragem_PP.ResgataFERRAGEM_PP(lbCodigoPedido.caption, EdtCodigoProjetoPedido.Text);
end;


procedure TFPEDIDO.preparaFerragem_PP;
begin
     Self.ObjPedidoObjetos.ObjFerragem_PP.ResgataFerragem_PP(lbCodigoPedido.caption, EdtCodigoProjetoPedido.Text);
     habilita_campos(PanelFerragem_PP);
     //EdtValorFerragem_PP.Enabled:=False;

end;


procedure TFPEDIDO.LimpaLabelsPedidoProjeto;
begin
    lbNomeProjetoPedido.Caption:='';
    lbNomeCorFerragemProjetoPedido.Caption:='';
    lbNomeCorPerfiladoProjetoPedido.Caption:='';
    lbNomeCorVidroProjetoPedido.Caption:='';
end;


procedure TFPEDIDO.edtFerragemCor_PPRefereniciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   Begin
         TEdit(Sender).Clear;
         exit;
   end;
   Self.ObjPedidoObjetos.ObjFerragem_PP.EdtFerragemCorViewKeyDown(Sender,edtFerragemCor_PP,Key, Shift, lbNomeFerragem_PP);

   //*****usado para pegar valor*******
   //**********************************



end;


procedure TFPEDIDO.btExcluirFerragem_PPClick(Sender: TObject);
begin
     if (VerificaPedidoConcluido=True)
     Then exit;

     if (edtCodigoFerragem_PP.Text='')
     then exit;

     if (EdtCodigoProjetoPedido.Text='')
     Then Begin
               MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
               exit;
     End;

     // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
     then Begin
          MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
          exit;
     end;



     // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (FDataModulo.CodigoProjetoBrancoGlobal<>Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Codigo)
     Then Begin
               //se nao for em branco nao pode excluir se nao tiver permissao
               if (ObjPermissoesUsoGlobal.ValidaPermissao('EXCLUIR ITENS NO PEDIDO')=False)
               Then exit;
     End;


     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')
     then
     Begin
           if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                         'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                         'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
           then begin
                    exit;
           end
           else Begin
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
           end;
     end;

     If (DBGridFerragem_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridFerragem_PP.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir essa Ferragem Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     Self.ObjPedidoObjetos.ObjFerragem_PP.Exclui(DBGridFerragem_PP.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
     limpaedit(PanelFerragem_PP);
     Self.ResgataFerragem_PP;
     Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
     Self.AtualizaValoresPedido(lbCodigoPedido.caption);
     LimpaLabels_FerragemPP;
end;


procedure TFPEDIDO.btCancelarFeragem_PPClick(Sender: TObject);
begin
   limpaedit(PanelFerragem_PP);
   LimpaLabels_FerragemPP;
end;


procedure TFPEDIDO.preparaPerfilado_PP;
begin
     Self.ObjPedidoObjetos.ObjPerfilado_PP.ResgataPerfilado_PP(lbCodigoPedido.caption, EdtCodigoProjetoPedido.Text);
     habilita_campos(PanelPerfilado_PP);
     //edtValorPerfilado_PP.Enabled:=False;
end;


procedure TFPEDIDO.ResgataPerfilado_PP;
begin
    Self.ObjPedidoObjetos.ObjPerfilado_PP.ResgataPerfilado_PP(lbCodigoPedido.Caption, EdtCodigoProjetoPedido.Text);
end;


procedure TFPEDIDO.edtPerfiladoCor_PPReferenciaExit(Sender: TObject);
begin
  if TEdit(Sender).Text = ''then
  Begin
    EdtPerfiladoCor_PP.Text:='';
    lbnomePerfiladoCor_PP.Caption:='';
  end;
end;


procedure TFPEDIDO.edtPerfiladoCor_PPReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   Begin
         TEdit(Sender).Clear;
         exit;
   end;
   Self.ObjPedidoObjetos.ObjPerfilado_PP.EdtPerfiladoCorKeyDown(Sender, EdtPerfiladoCor_PP, Key, Shift, lbnomePerfiladoCor_PP);

end;


procedure TFPEDIDO.EdtVidroCor_PPReferenciaExit(Sender: TObject);
begin
  if TEdit(Sender).Text = ''then
  Begin
    edtVidroCor_PP2.Text:='';
    lbNomeVidroCor_PP2.Caption:='';
  end;
end;


procedure TFPEDIDO.btBtGravarVidro_PPClick(Sender: TObject);
var
temp:currency;
pquantidade,paltura,plargura,plarguraarredondamento,palturaarredondamento:Currency;
AreaMinima:string;
begin

    if (VerificaPedidoConcluido=True)
    Then exit;

    if (EdtCodigoProjetoPedido.Text='')then
    Begin
        MensagemErro('Escolha um Projeto na aba 2');
        exit;
    end;

    // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
    then Begin
         MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto n�o pode ser alterado.');
         exit;
    end;

    // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')then
    Begin
          if MessageDlg('Este � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                        'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                        'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
          then begin
                    exit;
          end else
          Begin
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
          end;
    end;

    With Self.ObjPedidoObjetos.ObjVidro_PP do
    Begin
        ZerarTabela;

        if (edtCodigoVidro_PP2.text='')
        or (edtCodigoVidro_PP2.text='0')
        Then Begin
                   Status:=dsInsert;
                   edtCodigoVidro_PP2.Text:='0';
        End
        Else Status:=dsEdit;



        Try
           paltura:=strtocurr(tira_ponto(EdtAlturaVidro_PP2.Text));
        Except
              MensagemErro('Altura Inv�lida');
              exit;
        End;

        Try
           plargura:=strtocurr(tira_ponto(EdtlarguraVidro_PP2.Text));
        Except
            MensagemErro('Largura Inv�lida');
            exit;
        End;

        FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
        FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);

        pquantidade:=((plargura+plarguraarredondamento)*(paltura+palturaarredondamento))/1000000;


        Submit_Codigo(edtCodigoVidro_PP2.Text);
        Submit_PedidoProjeto(EdtCodigoProjetoPedido.Text);
        VidroCor.Submit_Codigo(edtVidroCor_PP2.Text);

        AreaMinima:= Self.ObjPedidoObjetos.RetornaAreaMininaVidro(EdtCodigoProjetoPedido.Text,EdtVidroCor_PP2.Text);
        if(pquantidade) < StrToFloat(AreaMinima)then
        begin
             pquantidade:=StrToCurr(AreaMinima);
        end;

        Submit_Quantidade(currtostr(pquantidade));
        Submit_Valor(tira_ponto(EdtValorVidro_PP2.Text));
        Submit_Largura(currtostr(plargura));
        Submit_Altura(currtostr(paltura));
        Submit_LarguraArredondamento(currtostr(plarguraarredondamento));
        Submit_AlturaArredondamento(currtostr(palturaarredondamento));
        Submit_Complemento(mmo1.Text);

        
        try
          Temp:=StrtoCurr(tira_ponto(Formata_valor(Self.ObjPedidoObjetos.RetornaValorvidro(EdtCodigoProjetoPedido.Text,edtVidroCor_PP2.Text))));
        Except
          Temp:=0;
        End;

        try
           if (StrtoCurr(tira_ponto(EdtValorVidro_PP2.Text))<temp)
           Then Begin
             if not utilizaDescontoAcrescimopeloPrazo then
             begin
               mensagemerro('O Valor n�o pode ser menor que o pre�o de tabela. Pre�o de Tabela '+Formata_valor(temp));
               exit;
             end;
           end;
        Except

        End;


        if (Salvar(True)=False)
        Then Begin
                  edtVidroCor_PP2.SetFocus;
                  exit;
        End;

        LimpaLabels_VidroPP;
        Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
        limpaedit(panelVidro);
        EdtVidroCor_PPReferencia2.SetFocus;
        Self.ResgataVidro_PP;
        Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
        Self.AtualizaValoresPedido(lbCodigoPedido.caption);
    End;

end;


procedure TFPEDIDO.ResgataVidro_PP;
begin
   Self.ObjPedidoObjetos.ObjVidro_PP.ResgataVidro_PP(lbCodigoPedido.caption, EdtCodigoProjetoPedido.Text);
end;


procedure TFPEDIDO.EdtVidroCor_PPReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   Begin
         TEdit(Sender).Clear;
         exit;
   end;
   Self.ObjPedidoObjetos.ObjVidro_PP.EdtVidroCorKeyDown(Sender, EdtVidroCor_PP2, Key, Shift, lbNomeVidroCor_PP2);

end;


procedure TFPEDIDO.preparaVidro_PP;
begin
     Self.ObjPedidoObjetos.ObjVidro_PP.ResgataVidro_PP(lbCodigoPedido.caption, EdtCodigoProjetoPedido.Text);
     habilita_campos(panelVidro);
     //EdtValorVidro_PP.Enabled:=False;
end;

procedure TFPEDIDO.btExcluirPerfilado_PPClick(Sender: TObject);
begin

    if (VerificaPedidoConcluido=True)
    Then exit;

     if (edtCodigoPerfilado_PP.Text='')
     then exit;

     if (EdtCodigoProjetoPedido.Text='')
     Then Begin
               MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
               exit;
     End;

     // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
     then Begin
          MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
          exit;
     end;

     // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (FDataModulo.CodigoProjetoBrancoGlobal<>Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Codigo)
     Then Begin
               //se nao for em branco nao pode excluir se nao tiver permissao
               if (ObjPermissoesUsoGlobal.ValidaPermissao('EXCLUIR ITENS NO PEDIDO')=False)
               Then exit;
     End;


     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')then
     Begin
           if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                         'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                         'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
           then begin
                exit;
           end else
           Begin
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
           end;
     end;

     If (DBGridPerfilado_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridPerfilado_PP.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir essa Perfilado Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     Self.ObjPedidoObjetos.ObjPerfilado_PP.Exclui(DBGridPerfilado_PP.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
     limpaedit(PanelPerfilado_PP);
     Self.ResgataPerfilado_PP;
     Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
     Self.AtualizaValoresPedido(lbCodigoPedido.caption);
     LimpaLabels_PerfialdoPP;

end;


procedure TFPEDIDO.btGravarPerfilado_PPClick(Sender: TObject);
var
temp:currency;
begin
    if (VerificaPedidoConcluido=True)
    Then exit;

    if (EdtCodigoProjetoPedido.Text='')then
    Begin
        MensagemErro('Escolha um Projeto na aba 2');
        exit;
    end;

    // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
    then Begin
         MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
         exit;
    end;

    // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')then
    Begin
          if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                        'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                        'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
          then begin
               exit;
          end else
          Begin
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
          end;
    end;

    With Self.ObjPedidoObjetos.ObjPerfilado_PP do
    Begin
        ZerarTabela;

        if (edtCodigoPerfilado_PP.text='')
        or (edtCodigoPerfilado_PP.text='0')
        Then Begin
                Status:=dsInsert;
                edtCodigoPerfilado_PP.Text:='0';
        End
        Else Status:=dsEdit;

        Try
           StrtoCurr(edtQuantidadePerfilado_PP.Text);
           if (StrtoCurr(edtQuantidadePerfilado_PP.Text)<=0)
           Then begin
                    Mensagemerro('N�o � poss�vel usar a quantidade igual a zero');
                    exit;
           End;
        except
           mensagemErro('Quantidade inv�lida');
           exit;
        End;


        Submit_Codigo(edtCodigoPerfilado_PP.Text);
        Submit_PedidoProjeto(EdtCodigoProjetoPedido.Text);
        PerfiladoCor.Submit_Codigo(edtPerfiladoCor_PP.Text);
        Submit_Quantidade(edtQuantidadePerfilado_PP.Text);
        Submit_Valor(tira_ponto(EdtValorPerfilado_PP.Text));

        try
          Temp:=StrtoCurr(tira_ponto(Formata_valor(Self.ObjPedidoObjetos.RetornaValorPerfilado(EdtCodigoProjetoPedido.Text,edtperfiladoCor_PP.Text))));
        Except
          Temp:=0;
        End;

        try
           if (StrtoCurr(tira_ponto(EdtValorperfilado_PP.Text))<temp)
           Then Begin
             if not utilizaDescontoAcrescimopeloPrazo then
             begin
               mensagemerro('O Valor n�o pode ser menor que o pre�o de tabela. Pre�o de Tabela '+Formata_valor(temp));
               exit;
             end;
           end;
        Except

        End;


        if (Salvar(True)=False)
        Then Begin
                  edtPerfiladoCor_PP.SetFocus;
                  exit;
        End;

        LimpaLabels_PerfialdoPP;
        Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
        limpaedit(PanelPerfilado_PP);
        EdtPerfiladoCor_PPReferencia.SetFocus;
        Self.ResgataPerfilado_PP;
        Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
        Self.AtualizaValoresPedido(lbCodigoPedido.caption);
    End;

end;


procedure TFPEDIDO.DBGridPerfilado_PPDblClick(Sender: TObject);
begin

     If (DBGridPerfilado_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridPerfilado_PP.DataSource.DataSet.RecordCount=0)
     Then exit;


     limpaedit(PanelPerfilado_PP);
     if (Self.ObjPedidoObjetos.ObjPerfilado_PP.LocalizaCodigo(DBGridPerfilado_PP.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataPerfilado_PP;
               exit;
     End;

     With Self.ObjPedidoObjetos.ObjPerfilado_PP do
     Begin
          TabelaparaObjeto;
          edtCodigoPerfilado_PP.Text:=Get_Codigo;
          EdtCodigoProjetoPedido.Text:=Get_PedidoProjeto;
          EdtPerfiladoCor_PPReferencia.Text:=PerfiladoCor.Perfilado.Get_Referencia;
          edtPerfiladoCor_PP.Text:=PerfiladoCor.Get_Codigo;
          lbnomePerfiladoCor_PP.Caption:=PerfiladoCor.Perfilado.Get_Descricao+' - '+PerfiladoCor.Cor.Get_Descricao;
          edtQuantidadePerfilado_PP.Text:=Get_Quantidade;
          EdtValorPerfilado_PP .Text:=formata_valor(Get_Valor);
          EdtPerfiladoCor_PPReferencia .SetFocus;
     End;

end;


procedure TFPEDIDO.btCancelarPerfilado_PPClick(Sender: TObject);
begin
   limpaedit(PanelPerfilado_PP);
   LimpaLabels_PerfialdoPP;
end;

procedure TFPEDIDO.DBGridFerragem_PPDblClick(Sender: TObject);
begin

     If (DBGridferragem_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridferragem_PP.DataSource.DataSet.RecordCount=0)
     Then exit;


     limpaedit(Panelferragem_PP);
     if (Self.ObjPedidoObjetos.ObjFerragem_PP.LocalizaCodigo(DBGridferragem_PP.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.Resgataferragem_PP;
               exit;
     End;

     With Self.ObjPedidoObjetos.ObjFerragem_PP do
     Begin
          TabelaparaObjeto;
          edtCodigoferragem_PP.Text:=Get_Codigo;
          EdtCodigoProjetoPedido.Text:=Get_PedidoProjeto;
          edtferragemCor_PP.Text:=ferragemCor.Get_Codigo;
          edtFerragemCor_PPReferenicia.Text:=ferragemCor.Ferragem.Get_Referencia;
          lbnomeferragem_PP.Caption:=ferragemCor.ferragem.Get_Descricao+' - '+ferragemCor.Cor.Get_Descricao;
          EdtQtdeFerragem_PP.Text:=Get_Quantidade;
          EdtValorferragem_PP.Text:=formata_valor(Get_Valor);
          edtFerragemCor_PPReferenicia.SetFocus;
     End;

end;


procedure TFPEDIDO.btBtExcluirVidro_PPClick(Sender: TObject);
begin
     if (VerificaPedidoConcluido=True)
    Then exit;

     if (edtCodigoVidro_PP2.Text='')
     Then exit;

     if (EdtCodigoProjetoPedido.Text='')
     Then Begin
               MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
               exit;
     End;

     // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
     then Begin
         MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
         exit;
     end;

     // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (FDataModulo.CodigoProjetoBrancoGlobal<>Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Codigo)
     Then Begin
               //se nao for em branco nao pode excluir se nao tiver permissao
               if (ObjPermissoesUsoGlobal.ValidaPermissao('EXCLUIR ITENS NO PEDIDO')=False)
               Then exit;
     End;

     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')then
     Begin
           if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                         'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                         'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
           then begin
                exit;
           end else
           Begin
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
           end;
     end;

     If (DBGridVidro_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridVidro_PP.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir esse Vidro Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     Self.ObjPedidoObjetos.ObjVidro_PP.Exclui(DBGridVidro_PP.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
     limpaedit(pnl3);
     Self.ResgataVidro_PP;
     Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
     Self.AtualizaValoresPedido(lbCodigoPedido.caption);
     LimpaLabels_VidroPP;

end;


procedure TFPEDIDO.btBtCancelarVidro_PPClick(Sender: TObject);
begin
   limpaedit(panelVidro);
   LimpaLabels_VidroPP;
end;


procedure TFPEDIDO.DBGridVidro_PPDblClick(Sender: TObject);
begin

     If (DBGridVidro_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridVidro_PP.DataSource.DataSet.RecordCount=0)
     Then exit;


     limpaedit(pnl3);
     if (Self.ObjPedidoObjetos.ObjVidro_PP.LocalizaCodigo(DBGridVidro_PP.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataVidro_PP;
               exit;
     End;

     With Self.ObjPedidoObjetos.ObjVidro_PP do
     Begin
          TabelaparaObjeto;
          edtCodigoVidro_PP2.Text:=Get_Codigo;
          EdtCodigoProjetoPedido.Text:=Get_PedidoProjeto;
          edtVidroCor_PP2.Text:=VidroCor.Get_Codigo;
          EdtVidroCor_PPReferencia2.Text:=VidroCor.Vidro.Get_Referencia;
          lbnomeVidroCor_PP2.Caption:=VidroCor.Vidro.Get_Descricao+' - '+VidroCor.Cor.Get_Descricao;
          edtQuantidadeVidro_PP2.Text:=Get_Quantidade;
          EdtValorVidro_PP2.Text:=formata_valor(Get_Valor);
          EdtLarguraVidro_PP2.Text:=Get_Largura;
          EdtAlturaVidro_PP2.Text:=Get_Altura;
          MemoComplementoVidro_PP.Text:=Get_Complemento;
          EdtVidroCor_PPReferencia2.SetFocus;
     End;

end;


procedure TFPEDIDO.edtKitBoxCor_PPReferenciaExit(Sender: TObject);
begin
  if TEdit(Sender).Text = ''then
  Begin
    EdtKitBoxCor_PP.Text:='';
    lbNomeKitBoxCor_PP.Caption:='';
  end;
end;


procedure TFPEDIDO.edtKitBoxCor_PPReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   Begin
         TEdit(Sender).Clear;
         exit;
   end;
   Self.ObjPedidoObjetos.ObjKitBox_PP.EdtKitBoxCorKeyDown(Sender,EdtKitBoxCor_PP, key, Shift, lbNomeKitBoxCor_PP);

   
end;


procedure TFPEDIDO.btGravarKitBox_PPClick(Sender: TObject);
var
temp:Currency;
begin
    if (VerificaPedidoConcluido=True)
    Then exit;

    if (EdtCodigoProjetoPedido.Text='')
    then Begin
        MensagemErro('Escolha um Projeto na aba 2');
        exit;
    end;

    // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
    then Begin
         MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
         exit;
    end;

    // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')then
    Begin
          if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                        'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                        'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
          then begin
               exit;
          end else
          Begin
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
          end;
    end;

    With Self.ObjPedidoObjetos.ObjKitBox_PP do
    Begin
        ZerarTabela;

        if (edtCodigoKitBox_PP.text='')
        or (edtCodigoKitBox_PP.text='0')
        Then Begin
                Status:=dsInsert;
                edtCodigoKitBox_PP.Text:='0';
        End
        Else Status:=dsEdit;

        Try
           StrtoCurr(EdtQuantidadeKitBox_PP.Text);
           if (StrtoCurr(EdtQuantidadeKitBox_PP.Text)<=0)
           Then begin
                    Mensagemerro('N�o � poss�vel usar a quantidade igual a zero');
                    exit;
           End;
        except
           mensagemErro('Quantidade inv�lida');
           exit;
        End;

        Submit_Codigo(edtCodigoKitBox_PP.Text);
        Submit_PedidoProjeto(EdtCodigoProjetoPedido.Text);
        KitBoxCor.Submit_Codigo(edtKitBoxCor_PP.Text);
        Submit_Quantidade(edtQuantidadeKitBox_PP.Text);
        Submit_Valor(tira_ponto(EdtValorKitBox_PP.Text));

        try
          Temp:=StrtoCurr(tira_ponto(Formata_valor(Self.ObjPedidoObjetos.RetornaValorKitBox(EdtCodigoProjetoPedido.Text,EdtKitBoxCor_PP.Text))));
        Except
          Temp:=0;
        End;

        try
           if (StrtoCurr(tira_ponto(EdtValorKitBox_PP.Text))<temp)
           Then Begin
             if not utilizaDescontoAcrescimopeloPrazo then
             begin
               mensagemerro('O Valor n�o pode ser menor que o pre�o de tabela. Pre�o de Tabela '+Formata_valor(temp));
               exit;
             end;
           end;
        Except

        End;

        if (Salvar(True)=False)
        Then Begin
                  edtKitBoxCor_PP.SetFocus;
                  exit;
        End;

        LimpaLabels_KitBoxPP;
        Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
        limpaedit(PanelKitBox_PP);
        EdtKitBoxCor_PPReferencia.SetFocus;
        Self.ResgataKitBox_PP;
        Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
        Self.AtualizaValoresPedido(lbCodigoPedido.caption);
    End;


end;


procedure TFPEDIDO.btExcluirKitBox_PPClick(Sender: TObject);
begin

     if (VerificaPedidoConcluido=True)
    Then exit;

     if (edtCodigoKitBox_PP.Text='')
     then exit;

     if (EdtCodigoProjetoPedido.Text='')
     Then Begin
               MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
               exit;
     End;

     // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
     then Begin
          MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
          exit;
     end;

     // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (FDataModulo.CodigoProjetoBrancoGlobal<>Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Codigo)
     Then Begin
               //se nao for em branco nao pode excluir se nao tiver permissao
               if (ObjPermissoesUsoGlobal.ValidaPermissao('EXCLUIR ITENS NO PEDIDO')=False)
               Then exit;
     End;

     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')then
     Begin
           if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                         'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                         'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
           then begin
                exit;
           end else
           Begin
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
           end;
     end;


     If (DBGridKitBox_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridKitBox_PP.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir essa KitBox Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     Self.ObjPedidoObjetos.ObjKitBox_PP.Exclui(DBGridKitBox_PP.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
     limpaedit(PanelKitBox_PP);
     Self.ResgataKitBox_PP;
     Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
     Self.AtualizaValoresPedido(lbCodigoPedido.caption);
     LimpaLabels_KitBoxPP;


end;


procedure TFPEDIDO.btCancelarKitBox_PPClick(Sender: TObject);
begin
   limpaedit(PanelKitBox_PP);
   LimpaLabels_KitBoxPP;
end;


procedure TFPEDIDO.preparaKitBox_PP;
begin
     Self.ObjPedidoObjetos.ObjKitBox_PP.ResgataKitBox_PP(lbCodigoPedido.caption, EdtCodigoProjetoPedido.Text);
     habilita_campos(PanelKitBox_PP);
     //EdtValorKitBox_PP.Enabled:=False;

end;


procedure TFPEDIDO.ResgataKitBox_PP;
begin
   Self.ObjPedidoObjetos.ObjKitBox_PP.ResgataKitBox_PP(lbCodigoPedido.caption, EdtCodigoProjetoPedido.Text);
end;


procedure TFPEDIDO.DBGridKitBox_PPDblClick(Sender: TObject);
begin
     If (DBGridKitBox_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridKitBox_PP.DataSource.DataSet.RecordCount=0)
     Then exit;


     limpaedit(PanelKitBox_PP);
     if (Self.ObjPedidoObjetos.ObjKitBox_PP.LocalizaCodigo(DBGridKitBox_PP.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataKitBox_PP;
               exit;
     End;

     With Self.ObjPedidoObjetos.ObjKitBox_PP do
     Begin
          TabelaparaObjeto;
          edtCodigoKitBox_PP.Text:=Get_Codigo;
          EdtCodigoProjetoPedido.Text:=Get_PedidoProjeto;
          EdtKitBoxCor_PPReferencia .Text:=KitBoxCor.KitBox.Get_Referencia;
          edtKitBoxCor_PP.Text:=KitBoxCor.Get_Codigo;
          lbnomeKitBoxCor_PP.Caption:=KitBoxCor.KitBox.Get_Descricao+' - '+KitBoxCor.Cor.Get_Descricao;
          edtQuantidadeKitBox_PP.Text:=Get_Quantidade;
          EdtValorKitBox_PP.Text:=formata_valor(Get_Valor);
          EdtKitBoxCor_PPReferencia.SetFocus;
     End;

end;


procedure TFPEDIDO.btGravarServico_PPClick(Sender: TObject);
var
temp:currency;
begin

    if (VerificaPedidoConcluido=True)
    Then exit;

    if (EdtCodigoProjetoPedido.Text='')then
    Begin
        MensagemErro('Escolha um Projeto na aba 2');
        exit;
    end;

    // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
    then Begin
         MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
         exit;
    end;

    // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Tabelaparaobjeto;

    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')then
    Begin
          if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                        'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                        'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
          then begin
               exit;
          end else
          Begin
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
          end;
    end;

    With Self.ObjPedidoObjetos.ObjServico_PP do
    Begin
        ZerarTabela;

        if (edtCodigoServico_PP.text='')
        or (edtCodigoServico_PP.text='0')
        Then Begin
                Status:=dsInsert;
                edtCodigoServico_PP.Text:='0';
        End
        Else Status:=dsEdit;

        {Rodolfo==}
        if(rbSim.checked = True) then begin
            Submit_ALTURA(edtAltServ.text);
            Submit_LARGURA(edtLargServ.text);
            Submit_ControlaPorMilimetro('SIM');

            if(StrtoCurr(edtquantidadeservico.Text)<=0) then
            begin
                 edtLargServExit(Sender);
            end;

        end else
            submit_ControlaPormilimetro('NAO');
        {Rodolfo==}
        if( rbsimm2.Checked = True ) then
        begin
            Submit_ALTURA(edtAltServ.text);
            Submit_LARGURA(edtLargServ.text);
            Submit_controlaM2('S');
            if(StrtoCurr(edtquantidadeservico.Text)<=0) then
            begin
                 edtLargServExit(Sender);
            end;
        end
        else
            Submit_controlaM2('N');

        Try
           StrtoCurr( tira_ponto (edtquantidadeservico.Text)); {Rodolfo}
           if (StrtoCurr(edtquantidadeservico.Text)<=0)
           Then begin
                    Mensagemerro('N�o � poss�vel usar a quantidade igual a zero');
                    exit;
           End;
        except
           mensagemErro('Quantidade inv�lida');
           exit;
        End;

        Submit_Codigo(edtCodigoServico_PP.Text);
        Submit_PedidoProjeto(EdtCodigoProjetoPedido.Text);
        Servico.Submit_Codigo(EdtServico_PP.Text);
        Submit_Valor(tira_ponto(EdtValorServico_PP.Text));
        Submit_Quantidade(edtquantidadeservico.text);


        try
          Temp:=StrtoCurr(tira_ponto(Formata_valor(Self.ObjPedidoObjetos.RetornaValorServico(EdtCodigoProjetoPedido.Text,edtservico_PP.Text))));
        Except
          Temp:=0;
        End;

        try
           if (StrtoCurr(tira_ponto(EdtValorservico_PP.Text))<temp)
           Then Begin
             if not utilizaDescontoAcrescimopeloPrazo then
             begin
               mensagemerro('O Valor n�o pode ser menor que o pre�o de tabela. Pre�o de Tabela '+Formata_valor(temp));
               exit;
             end;
           end;
        Except

        End;

        if (Salvar(True)=False)
        Then Begin
                  edtServico_PP.SetFocus;
                  exit;
        End;

        LimpaLabels_ServicoPP;
        Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
        limpaedit(PanelServico_PP);
        EdtServico_PPReferencia.SetFocus;
        Self.ResgataServico_PP;
        Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
        Self.AtualizaValoresPedido(lbCodigoPedido.caption);
    End;

end;

procedure TFPEDIDO.btExcluirServico_PPClick(Sender: TObject);
begin

     if (VerificaPedidoConcluido=True)
    Then exit;


     if (edtCodigoServico_PP.Text='')
     then exit;

     if (EdtCodigoProjetoPedido.Text='')
     Then Begin
               MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
               exit;
     End;

     // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
     then Begin
          MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
          exit;
     end;

     // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (FDataModulo.CodigoProjetoBrancoGlobal<>Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Codigo)
     Then Begin
               //se nao for em branco nao pode excluir se nao tiver permissao
               if (ObjPermissoesUsoGlobal.ValidaPermissao('EXCLUIR ITENS NO PEDIDO')=False)
               Then exit;
     End;


     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')then
     Begin
           if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                         'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                         'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
           then begin
                exit;
           end else
           Begin
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
           end;
     end;

     If (DBGridServico_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridServico_PP.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir esse Servi�o Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;

     Self.ObjPedidoObjetos.ObjServico_PP.Exclui(DBGridServico_PP.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
     limpaedit(PanelServico_PP);
     Self.ResgataServico_PP;
     Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
     Self.AtualizaValoresPedido(lbCodigoPedido.caption);
     LimpaLabels_ServicoPP;
end;

procedure TFPEDIDO.btCancelarServico_PPClick(Sender: TObject);
begin
   limpaedit(PanelServico_PP);
   LimpaLabels_ServicoPP;
   rbNao.Checked := True; {Rodolfo}
   rbNaoClick(sender);   {Rodolfo}
end;

procedure TFPEDIDO.DBGridServico_PPDblClick(Sender: TObject);
begin
     If (DBGridServico_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridServico_PP.DataSource.DataSet.RecordCount=0)
     Then exit;

     limpaedit(PanelServico_PP);
     if (Self.ObjPedidoObjetos.ObjServico_PP.LocalizaCodigo(DBGridServico_PP.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
           Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
           Self.ResgataServico_PP;
           exit;
     End;

     With Self.ObjPedidoObjetos.ObjServico_PP do
     Begin
          TabelaparaObjeto;
          edtCodigoServico_PP.Text:=Get_Codigo;
          EdtCodigoProjetoPedido.Text:=Get_PedidoProjeto;
          EdtServico_PPReferencia.Text:=Servico.Get_Referencia;
          edtServico_PP.Text:=Servico.Get_Codigo;
          lbnomeServico_PP.Caption:=Servico.Get_Descricao;
          EdtValorServico_PP.Text:=formata_valor(Get_Valor);
          EdtServico_PPReferencia.SetFocus;

          {Rodolfo==}
          if(Get_ControlaPorMilimetro = 'SIM') then begin
              rbSim.Checked := True;
          end
          else  begin
              rbNao.Checked := True;
          end;

          if(Get_controlaM2 = 'S') then begin
              rbsimm2.Checked := True;
          end
          else  begin
              rbnaom2.Checked := True;
          end;
          {==Rodolfo}

          edtquantidadeservico.Text := Get_quantidade;

     End;

end;


procedure TFPEDIDO.PreparaServico_PP;
begin
     Self.ObjPedidoObjetos.ObjServico_PP.ResgataServico_PP(lbCodigoPedido.caption,EdtCodigoProjetoPedido.Text);
     habilita_campos(PanelServico_PP);
     //EdtValorServico_PP.Enabled:=False;

     rbSim.Enabled := True; {Rodolfo}
     rbNao.Enabled := True;          {}

     rbsimm2.Enabled:=True;
     rbnaom2.Enabled:=True;
end;


procedure TFPEDIDO.ResgataServico_PP;
begin
   Self.ObjPedidoObjetos.ObjServico_PP.ResgataServico_PP(lbCodigoPedido.caption, EdtCodigoProjetoPedido.Text);
end;

{Rodolfo}
procedure TFPEDIDO.edtServico_PPReferenciaExit(Sender: TObject);
begin
  if (EdtCodigoProjetoPedido.Text='') then
  begin
    MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
    exit;
  End;
  Self.ObjPedidoObjetos.ObjSERVICO_PP.EdtServicoExit(Sender, EdtServico_PP,  lbnomeServico_PP);

  //Verifica se o campo de identificacao do edit foi alterado ou se foi apenas clicado
  if(auxStr <> lbNomeServico_pp.Caption)then
      edtValorServico_PP.text:=Formata_Valor(Self.ObjPedidoObjetos.RetornaValorServico(EdtCodigoProjetoPedido.Text,EdtServico_PP.Text));

end;


procedure TFPEDIDO.edtServico_PPReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   //if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   //Begin
   //      TEdit(Sender).Clear;
   //      exit;
   //end;

   Self.ObjPedidoObjetos.ObjServico_PP.EdtServicoKeyDown(Sender, EdtServico_PP, key, Shift, lbnomeServico_PP);

end;


procedure TFPEDIDO.btGravarPersiana_PPClick(Sender: TObject);
var
temp:currency;
begin
    if (VerificaPedidoConcluido=True)
    Then exit;


    if (EdtCodigoProjetoPedido.Text='')then
    Begin
        MensagemErro('Escolha um Projeto na aba 2');
        exit;
    end;

    // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
    then Begin
         MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
         exit;
    end;

    // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')then
    Begin
          if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                        'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                        'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
          then begin
               exit;
          end else
          Begin
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
          end;
    end;

    With Self.ObjPedidoObjetos.ObjPersiana_PP do
    Begin
        ZerarTabela;

        if (edtCodigoPersiana_PP.text='')
        or (edtCodigoPersiana_PP.text='0')
        Then Begin
                Status:=dsInsert;
                edtCodigoPersiana_PP.Text:='0';
        End
        Else Status:=dsEdit;

        Try
           StrtoCurr(EdtQuantidadePersiana_PP.Text);
           if (StrtoCurr(EdtQuantidadePersiana_PP.Text)<=0)
           Then begin
                    Mensagemerro('N�o � poss�vel usar a quantidade igual a zero');
                    exit;
           End;
        except
           mensagemErro('Quantidade inv�lida');
           exit;
        End;

        Submit_Codigo(edtCodigoPersiana_PP.Text);
        Submit_PedidoProjeto(EdtCodigoProjetoPedido.Text);
        PersianaGrupoDiametroCor.Submit_Codigo(edtPersiana_PP.Text);
        Submit_Quantidade(edtQuantidadePersiana_PP.Text);
        Submit_Valor(tira_ponto(EdtValorPersiana_PP.Text));
        Submit_Complemento(MemoComplementoPersiana_PP.Text);
        Submit_Altura(EdtAlturaPersiana_PP.Text);
        Submit_Largura(EdtLarguraPersiana_PP.Text);
        Submit_Local(EdtLocalPErsiana_PP.Text);
        Submit_Instalacao(edtInstalacaopersiana_pp.Text);
        Submit_Comando(EdtComandoPersiana_PP.Text);

        try
          Temp:=StrtoCurr(tira_ponto(Formata_valor(Self.ObjPedidoObjetos.RetornaValorPersiana(EdtCodigoProjetoPedido.Text,edtpersiana_PP.Text))));
        Except
          Temp:=0;
        End;

        try
           if (StrtoCurr(tira_ponto(EdtValorPersiana_PP.Text))<temp)
           Then Begin
             if not utilizaDescontoAcrescimopeloPrazo then
             begin
               mensagemerro('O Valor n�o pode ser menor que o pre�o de tabela. Pre�o de Tabela '+Formata_valor(temp));
               exit;
             end;
           end;
        Except

        End;

        if (Salvar(True)=False)
        Then Begin
                  EdtPersiana_PPReferencia.SetFocus;
                  exit;
        End;

        LimpaLabels_PersianaPP;
        Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
        limpaedit(PanelPersiana_PP);
        EdtPersiana_PPReferencia.SetFocus;
        Self.ResgataPersiana_PP;
        Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
        Self.AtualizaValoresPedido(lbCodigoPedido.caption);
    End;

end;


procedure TFPEDIDO.btExcluirPersiana_PPClick(Sender: TObject);
begin

     if (VerificaPedidoConcluido=True)
    Then exit;

     if (edtCodigoPersiana_PP.Text='')
     then exit;

     if (EdtCodigoProjetoPedido.Text='')
     Then Begin
               MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
               exit;
     End;

     // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
     then Begin
          MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
          exit;
     end;

     // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (FDataModulo.CodigoProjetoBrancoGlobal<>Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Codigo)
     Then Begin
               //se nao for em branco nao pode excluir se nao tiver permissao
               if (ObjPermissoesUsoGlobal.ValidaPermissao('EXCLUIR ITENS NO PEDIDO')=False)
               Then exit;
     End;


     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')then
     Begin
           if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                         'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                         'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
           then begin
                exit;
           end else
           Begin
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
           end;
     end;


     If (DBGridPersiana_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridPersiana_PP.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir essa Persiana Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     Self.ObjPedidoObjetos.ObjPersiana_PP.Exclui(DBGridPersiana_PP.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
     limpaedit(PanelPersiana_PP);
     Self.ResgataPersiana_PP;
     Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
     Self.AtualizaValoresPedido(lbCodigoPedido.caption);
     LimpaLabels_PersianaPP;

end;


procedure TFPEDIDO.btCancelarPersiana_PPClick(Sender: TObject);
begin
   limpaedit(PanelPersiana_PP);
   LimpaLabels_PersianaPP;
   lbAlturaPersiana_PP.Visible:=false;
   lbLarguraPersiana_PP.Visible:=false;
   EdtLarguraPersiana_PP.Visible:=false;
   EdtAlturaPersiana_PP.Visible:=false;
   lbmetrosAltura.Visible :=false;
   lbmetrosLargura.Visible:=false;
end;


procedure TFPEDIDO.DBGridPersiana_PPDblClick(Sender: TObject);
begin
     If (DBGridPersiana_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridPersiana_PP.DataSource.DataSet.RecordCount=0)
     Then exit;


     limpaedit(PanelPersiana_PP);
     if (Self.ObjPedidoObjetos.ObjPersiana_PP.LocalizaCodigo(DBGridPersiana_PP.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataPersiana_PP;
               exit;
     End;

     With Self.ObjPedidoObjetos.ObjPersiana_PP do
     Begin
          TabelaparaObjeto;
          edtCodigoPersiana_PP.Text:=Get_Codigo;
          EdtCodigoProjetoPedido.Text:=Get_PedidoProjeto;
          EdtPersiana_PPReferencia.Text:=PersianaGrupoDiametroCor.Persiana.Get_Referencia;
          edtPersiana_PP.Text:=PersianaGrupoDiametroCor.Get_Codigo;
          lbnomePersiana_PP.Caption:=PersianaGrupoDiametroCor.Persiana.Get_Nome+' - '+PersianaGrupoDiametroCor.Cor.Get_Descricao;
          edtQuantidadePersiana_PP.Text:=Get_Quantidade;
          EdtValorPersiana_PP.Text:=formata_valor(Get_Valor);
          MemoComplementoPersiana_PP.Text:=Get_Complemento;
          EdtLocalPersiana_PP.Text:=Get_Local;
          EdtInstalacaoPersiana_PP.Text:=Get_Instalacao;
          EdtComandoPersiana_PP.Text:=Get_Comando;

          EdtPersiana_PPReferencia.SetFocus;

          if (PersianaGrupoDiametroCor.GrupoPersiana.Get_ControlaPorMetroQuadrado = 'S')then
          Begin
              lbAlturaPersiana_PP.Visible:=true;
              lbLarguraPersiana_PP.Visible:=true;
              EdtLarguraPersiana_PP.Visible:=true;
              EdtAlturaPersiana_PP.Visible:=true;
              EdtLarguraPersiana_PP.Text:=Get_Largura;
              EdtAlturaPersiana_PP.Text:=Get_Altura;
              lbmetrosAltura.Visible :=true;
              lbmetrosLargura.Visible:=true;

          end else
          Begin
              lbAlturaPersiana_PP.Visible:=false;
              lbLarguraPersiana_PP.Visible:=false;
              EdtLarguraPersiana_PP.Visible:=false;
              EdtAlturaPersiana_PP.Visible:=false;
              lbmetrosAltura.Visible :=false;
              lbmetrosLargura.Visible:=false;
          end;                              
     End;

end;


procedure TFPEDIDO.preparaPersiana_PP;
begin
     Self.ObjPedidoObjetos.ObjPersiana_PP.ResgataPersiana_PP(lbCodigoPedido.caption, EdtCodigoProjetoPedido.Text);
     habilita_campos(PanelPersiana_PP);
     //EdtValorPersiana_PP.Enabled:=False;

end;

procedure TFPEDIDO.ResgataPersiana_PP;
begin
   Self.ObjPedidoObjetos.ObjPersiana_PP.ResgataPersiana_PP(lbCodigoPedido.caption, EdtCodigoProjetoPedido.Text);
end;

procedure TFPEDIDO.btGravarFerragem_PPClick(Sender: TObject);
var
temp:currency;
begin
    if (VerificaPedidoConcluido=True)
    Then exit;
    

    if (EdtCodigoProjetoPedido.Text='')then
    Begin
        MensagemErro('Escolha um Projeto na aba 2');
        exit;
    end;

    // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
    then Begin
             MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
             exit;
    end;


    // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')
    then Begin
             if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                            'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                            'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
              Then Begin
                        exit;
              End
              Else Begin
                        Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                        Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
                        Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
              End;
    end;

    With Self.ObjPedidoObjetos.ObjFerragem_PP do
    Begin
        ZerarTabela;

        if (edtCodigoFerragem_PP.text='')
        or (edtCodigoFerragem_PP.text='0')
        Then Begin
                Status:=dsInsert;
                edtCodigoFerragem_PP.Text:='0';
        End                                                                         
        Else Status:=dsEdit;

        Try
           StrtoCurr(EdtQtdeFerragem_PP.Text);
           if (StrtoCurr(EdtQtdeFerragem_PP.Text)<=0)
           Then begin
                    Mensagemerro('N�o � poss�vel usar a quantidade igual a zero');
                    exit;
           End;
        except
           mensagemErro('Quantidade inv�lida');
           exit;
        End;


        Submit_Codigo(edtCodigoFerragem_PP.Text);
        Submit_PedidoProjeto(EdtCodigoProjetoPedido.Text);
        FerragemCor.Submit_Codigo(edtFerragemCor_PP.Text);
        Submit_Quantidade(edtQtdeFerragem_PP.Text);
        Submit_Valor(tira_ponto(EdtValorFerragem_PP.Text));

        try
          Temp:=StrtoCurr(tira_ponto(Formata_valor(Self.ObjPedidoObjetos.RetornaValorFerragem(EdtCodigoProjetoPedido.Text,edtFerragemCor_PP.Text))));
        Except
          Temp:=0;
        End;

        try
           if (Strtocurr(tira_ponto(EdtValorFerragem_PP.Text))<temp)
           Then Begin
             if not utilizaDescontoAcrescimopeloPrazo then
             begin
               mensagemerro('O Valor n�o pode ser menor que o pre�o de tabela. Pre�o de Tabela '+Formata_valor(temp));
               exit;
             end;
           end;
        Except

        End;



        if (Salvar(True)=False)
        Then Begin
                  edtFerragemCor_PP.SetFocus;
                  exit;
        End;

        LimpaLabels_FerragemPP;


        Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
        limpaedit(PanelFerragem_PP);
        edtFerragemCor_PPReferenicia.SetFocus;
        Self.ResgataFerragem_PP;
        Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
        Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(lbCodigoPedido.caption);
        Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

        if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Concluido='N')
        Then Self.AtualizaValoresPedido(lbCodigoPedido.caption);
    End;


end;


procedure TFPEDIDO.edtPersiana_PPReferenciaExit(Sender: TObject);
begin
  if TEdit(Sender).Text = ''then
  Begin
     EdtPersiana_PP.Text:='';
     lbNomePersiana_PP.Caption:='';
     exit;
  end;

  if (Self.ObjPedidoObjetos.ObjPersiana_PP.PersianaGrupoDiametroCor.GrupoPersiana.Get_ControlaPorMetroQuadrado = 'S')then
  begin
    lbAlturaPersiana_PP.Visible:=true;
    lbLarguraPersiana_PP.Visible:=true;
    EdtLarguraPersiana_PP.Visible:=true;
    EdtAlturaPersiana_PP.Visible:=true;
    EdtAlturaPersiana_PP.SetFocus;
    lbmetrosAltura.Visible :=true;
    lbmetrosLargura.Visible:=true;
  end else
  Begin
    lbAlturaPersiana_PP.Visible:=false;
    lbLarguraPersiana_PP.Visible:=false;
    EdtLarguraPersiana_PP.Visible:=false;
    EdtAlturaPersiana_PP.Visible:=false;
    lbmetrosAltura.Visible :=false;
    lbmetrosLargura.Visible:=false;
    EdtQuantidadePersiana_PP .SetFocus;
  end;

end;

procedure TFPEDIDO.edtPersiana_PPReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   Begin
         TEdit(Sender).Clear;
         exit;
   end;
   Self.ObjPedidoObjetos.ObjPersiana_PP.EdtPersianaGrupoDiametroCorKeyDown(Sender,EdtPersiana_PP,  Key, Shift, lbNomePersiana_PP);

   //*****usado para pegar valor*******
   if (EdtPersiana_PP.text='')
   Then exit;

  if (EdtCodigoProjetoPedido.Text='') then
  begin
    MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
    exit;
  End;
  
   edtValorPersiana_PP.text:=Formata_Valor(Self.ObjPedidoObjetos.RetornaValorPersiana(EdtCodigoProjetoPedido.Text,EdtPersiana_PP.Text));
   Self.edtMaterial_PPExit( edtValorPersiana_PP );
   //**********************************

end;


procedure TFPEDIDO.btGravarDiverso_PPClick(Sender: TObject);
var
temp:currency;
begin
    if (VerificaPedidoConcluido=True)
    Then exit;


    if (EdtCodigoProjetoPedido.Text='')then
    Begin
        MensagemErro('Escolha um Projeto na aba 2');
        exit;
    end;

    // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
    then Begin
         MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
         exit;
    end;
    
    // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')then
    Begin
          if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                        'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                        'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
          then begin
               exit;
          end else
          Begin
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
          end;
    end;

    With Self.ObjPedidoObjetos.ObjDiverso_PP do
    Begin
        ZerarTabela;

        if (edtCodigoDiverso_PP.text='')
        or (edtCodigoDiverso_PP.text='0')
        Then Begin
                Status:=dsInsert;
                edtCodigoDiverso_PP.Text:='0';
        End
        Else Status:=dsEdit;

        Try
           StrtoCurr(EdtQuantidadeDiverso_PP.Text);
           if (StrtoCurr(EdtQuantidadeDiverso_PP.Text)<=0)
           Then begin
                    Mensagemerro('N�o � poss�vel usar a quantidade igual a zero');
                    exit;
           End;
        except
           mensagemErro('Quantidade inv�lida');
           exit;
        End;

        Submit_Codigo(edtCodigoDiverso_PP.Text);
        Submit_PedidoProjeto(EdtCodigoProjetoPedido.Text);
        DiversoCor.Submit_Codigo(edtDiverso_PP.Text);
        Submit_Altura(edtAlturaDiverso_PP.Text);
        Submit_Largura(edtLarguraDiverso_PP.Text);
        Submit_Quantidade(edtQuantidadeDiverso_PP.Text);
        Submit_Valor(tira_ponto(EdtValorDiverso_PP.Text));

        try
          Temp:=StrtoCurr(tira_ponto(Formata_valor(Self.ObjPedidoObjetos.RetornaValorDiverso(EdtCodigoProjetoPedido.Text,edtDiverso_PP.Text))));
        Except
          Temp:=0;
        End;

        try
           if (StrtoCurr(tira_ponto(EdtValorDiverso_PP.Text))<temp)
           Then Begin
             if not utilizaDescontoAcrescimopeloPrazo then
             begin
               mensagemerro('O Valor n�o pode ser menor que o pre�o de tabela. Pre�o de Tabela '+Formata_valor(temp));
               exit;
             end;
           end;
        Except

        End;

        if (Salvar(True)=False)
        Then Begin
                  edtDiverso_PP.SetFocus;
                  exit;
        End;

        LimpaLabels_DiversoPP;
        Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
        limpaedit(panelDiverso_PP);
        EdtDiverso_PPReferencia.SetFocus;
        edtAlturaDiverso_PP.Visible:=false;
        lbAlturaDiverso_PP.Visible:=false;
        edtLarguraDiverso_PP.Visible:=false;
        lbLarguraDiverso_PP.Visible:=false;
        Self.ResgataDiverso_PP;
        Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
        Self.AtualizaValoresPedido(lbCodigoPedido.caption);
    End;

end;


procedure TFPEDIDO.btExcluirDiverso_PPClick(Sender: TObject);
begin
     if (VerificaPedidoConcluido=True)
    Then exit;


     if (edtCodigoDiverso_PP.Text='')
     then exit;

     if (EdtCodigoProjetoPedido.Text='')
     Then Begin
               MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
               exit;
     End;

     // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
     then Begin
          MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
          exit;
     end;

     // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     if (FDataModulo.CodigoProjetoBrancoGlobal<>Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Codigo)
     Then Begin
               //se nao for em branco nao pode excluir se nao tiver permissao
               if (ObjPermissoesUsoGlobal.ValidaPermissao('EXCLUIR ITENS NO PEDIDO')=False)
               Then exit;
     End;

     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')then
     Begin
           if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                         'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                         'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
           then begin
                    exit;
           end
           else Begin
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
                Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
           end;
     end;

     If (DBGridDiverso_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridDiverso_PP.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir essa Diverso Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     Self.ObjPedidoObjetos.ObjDiverso_PP.Exclui(DBGridDiverso_PP.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);
     limpaedit(PanelDiverso_PP);
     Self.ResgataDiverso_PP;
     Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
     Self.AtualizaValoresPedido(lbCodigoPedido.caption);
     edtAlturaDiverso_PP.Visible:=false;
     lbAlturaDiverso_PP.Visible:=false;
     edtLarguraDiverso_PP.Visible:=false;
     lbLarguraDiverso_PP.Visible:=false;
     LimpaLabels_DiversoPP;

end;


procedure TFPEDIDO.btCancelarDiverso_PPClick(Sender: TObject);
begin
   limpaedit(PanelDiverso_PP);
   LimpaLabels_DiversoPP;
   edtAlturaDiverso_PP.Visible:=false;
   lbAlturaDiverso_PP.Visible:=false;
   edtLarguraDiverso_PP.Visible:=false;
   lbLarguraDiverso_PP.Visible:=false;
   lbMilimetroAlturaDiversoPP.Visible:=false;
   lbMilimetrolarguraDiversoPP.Visible:=false;


end;


procedure TFPEDIDO.DBGridDiverso_PPDblClick(Sender: TObject);
begin

     If (DBGridDiverso_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridDiverso_PP.DataSource.DataSet.RecordCount=0)
     Then exit;


     limpaedit(PanelDiverso_PP);
     if (Self.ObjPedidoObjetos.ObjDiverso_PP.LocalizaCodigo(DBGridDiverso_PP.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataDiverso_PP;
               exit;
     End;

     With Self.ObjPedidoObjetos.ObjDiverso_PP do
     Begin
          TabelaparaObjeto;
          edtCodigoDiverso_PP.Text:=Get_Codigo;
          EdtCodigoProjetoPedido.Text:=Get_PedidoProjeto;
          EdtDiverso_PPReferencia.Text:=DiversoCor.Diverso.Get_Referencia;
          edtDiverso_PP.Text:=DiversoCor.Get_Codigo;
          lbnomeDiverso_PP.Caption:=DiversoCor.Diverso.Get_Descricao;
          edtQuantidadeDiverso_PP.Text:=Get_Quantidade;
          edtAlturaDiverso_PP.Text:=Get_Altura;
          edtLarguraDiverso_PP.Text:=Get_Largura;
          EdtValorDiverso_PP.Text:=formata_valor(Get_Valor);
          EdtDiverso_PPReferencia.SetFocus;


          if (DiversoCor.Diverso.Get_ContralaPorMetroQuadrado='S')then
          begin
               EdtLarguraDiverso_PP.Visible:=true;
               edtAlturaDiverso_PP.Visible:=true;
               LBAlturaDiverso_PP.Visible:=true;
               LBLarguraDiverso_PP.Visible:=true;
               EdtQuantidadeDiverso_PP.Enabled:=false;
          end else
          Begin
               EdtLarguraDiverso_PP.Visible:=false;
               edtAlturaDiverso_PP.Visible:=false;
               LBAlturaDiverso_PP.Visible:=false;
               LBLarguraDiverso_PP.Visible:=false;
               EdtQuantidadeDiverso_PP.Enabled:=true;
          end;

     End;

end;


procedure TFPEDIDO.preparaDiverso_PP;
begin
     Self.ObjPedidoObjetos.ObjDiverso_PP.ResgataDiverso_PP(lbCodigoPedido.caption, EdtCodigoProjetoPedido.Text);
     habilita_campos(PanelDiverso_PP);
     //EdtValorDiverso_PP.Enabled:=False;

end;


procedure TFPEDIDO.ResgataDiverso_PP;
begin
   Self.ObjPedidoObjetos.ObjDiverso_PP.ResgataDiverso_PP(lbCodigoPedido.caption, EdtCodigoProjetoPedido.Text);
end;


procedure TFPEDIDO.edtDiverso_PPReferenciaExit(Sender: TObject);
begin
  if TEdit(Sender).Text = ''then
  Begin
    edtDiverso_PP.Text:='';
    lbNomeDiverso_PP.Caption:='';
    exit;
  end;

  if (Self.ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.Diverso.LocalizaReferencia(TEdit(Sender).Text)=false)then
  Begin
    MensagemErro('Referencia n�o emcontrada.');
    exit;
  end;
  Self.ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.Diverso.TabelaparaObjeto;

  if (Self.ObjPedidoObjetos.ObjDiverso_PP.DiversoCor.Diverso.Get_ContralaPorMetroQuadrado = 'S')then
  Begin
    lbMensagemDiverso.Caption:='Estoque Controlado por M�';
    lbMetroQuadrado.Visible:=true;
    edtAlturaDiverso_PP.Visible:=true;
    lbAlturaDiverso_PP.Visible:=true;
    edtLarguraDiverso_PP.Visible:=true;
    lbLarguraDiverso_PP.Visible:=true;
    lbMilimetroAlturaDiversoPP.Visible:=true;
    lbMilimetrolarguraDiversoPP.Visible:=true;
    EdtQuantidadeDiverso_PP.Enabled:=false;
    edtAlturaDiverso_PP.SetFocus;
  end else
  Begin
    lbMensagemDiverso.Caption:='';
    lbMetroQuadrado.Visible:=false;
    EdtQuantidadeDiverso_PP.Enabled:=true;
    edtAlturaDiverso_PP.Visible:=false;
    lbAlturaDiverso_PP.Visible:=false;
    edtLarguraDiverso_PP.Visible:=false;
    lbLarguraDiverso_PP.Visible:=false;
    lbMilimetroAlturaDiversoPP.Visible:=false;
    lbMilimetrolarguraDiversoPP.Visible:=false;
    EdtQuantidadeDiverso_PP.SetFocus;

  end;

end;


procedure TFPEDIDO.edtDiverso_PPReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   Begin
         TEdit(Sender).Clear;
         exit;
   end;
   Self.ObjPedidoObjetos.ObjDiverso_PP.EdtDiversoKeyDown(Sender,EdtDiverso_PP,  key, shift, lbnomeDiverso_PP);


end;


procedure TFPEDIDO.btBtExcluirProjetoPedidoClick(Sender: TObject);
begin
     if (VerificaPedidoConcluido=True)
     Then exit;
    
      if (EdtCodigoProjetoPedido.Text='')
      then exit;

     // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
     if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
     then Begin
          MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
          exit;
          {if (Messagedlg('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto = '+DBGridProjetoPedido.DataSource.DataSet.fieldbyname('pedidoprojeto').asstring+',deseja excluir todos ?',mtconfirmation,[mbyes,mbno],0)=mrno)
          Then exit;  }
     end;


      If (DBGridProjetoPedido.DataSource.DataSet.Active=False)
      Then exit;

      If (DBGridProjetoPedido.DataSource.DataSet.RecordCount=0)
      Then exit;

      if (Messagedlg('Tem Certeza que deseja excluir Projeto do Pedido Atual, n�mero '+DBGridProjetoPedido.DataSource.DataSet.fieldbyname('pedidoprojeto').asstring+' ?',mtconfirmation,[mbyes,mbno],0)=mrno)
      Then exit;


      Self.ObjPedidoObjetos.ExcluiPedidoProjeto(DBGridProjetoPedido.DataSource.DataSet.fieldbyname('pedidoprojeto').asstring);
      limpaedit(PanelPedidoProjeto);
      ComboTipoProjetoPedido.Text:='';
      Self.ResgataPedidoProjeto;
      Self.AtualizaValoresPedido(lbCodigoPedido.caption);
      LimpaLabels_Pedido_Proj;

      if(Integrar_CorteCerto=False)
    then MensagemErro('N�o foi poss�vel gerar arquivo para sincroniza��o SAB/Corte Certo');

end;


procedure TFPEDIDO.edtCorKitBoxProjetoPedidoReferenciaExit(Sender: TObject);
begin
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorKitBoxExit(Sender, EdtCorKitBoxProjetoPedido, lbNomeCorKitBoxProjetoPedido);

end;


procedure TFPEDIDO.edtCorKitBoxProjetoPedidoReferenciaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorKitBoxKeyDown(Sender, EdtCorKitBoxProjetoPedido, Key, Shift, lbNomeCorKitBoxProjetoPedido,edtprojetopedido.text);
end;


procedure TFPEDIDO.btCancelarProjetoPedidoClick(Sender: TObject);
begin
  limpaedit(PanelPedidoProjeto);
  ComboTipoProjetoPedido.Text:='';
  EdtCodigoProjetoPedido.Clear;
  LimpaLabels_Pedido_Proj;
end;


procedure TFPEDIDO.LimpaLabels_DiversoPP;
begin
lbnomeDiverso_PP.Caption:='';
lbMensagemDiverso.Caption:='';
end;

procedure TFPEDIDO.LimpaLabels_FerragemPP;
begin
lbNomeFerragem_PP.Caption:='';
end;


procedure TFPEDIDO.LimpaLabels_KitBoxPP;
begin
lbNomeKitBoxCor_PP.Caption:='';
end;


procedure TFPEDIDO.LimpaLabels_PerfialdoPP;
begin
lbnomePerfiladoCor_PP.Caption:='';
end;


procedure TFPEDIDO.LimpaLabels_PersianaPP;
begin
lbNomePersiana_PP.Caption:='';
end;


procedure TFPEDIDO.LimpaLabels_ServicoPP;
begin
lbnomeServico_PP.Caption:='';
end;


procedure TFPEDIDO.LimpaLabels_VidroPP;
begin
lbNomeVidroCor_PP.Caption:='';
end;


procedure TFPEDIDO.DBGridProjetoPedidoKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then DBGridProjetoPedidoDblClick(sender);
end;

procedure TFPEDIDO.btCalcularClick(Sender: TObject);
Var   PTag : Integer;
begin
    if (VerificaPedidoConcluido=True)
    Then exit;

     PTag:=0;
     Self.ObjPedidoObjetos.opcoes(PTag, EdtCodigoProjetoPedido.Text,edtLargura.Text,edtAltura.Text);

     if PTag=0
     then exit;

     BtCancelarProjetoPedido.Click;
     Self.PreparaPedidoProjeto;

     Self.AtualizaValoresPedido(lbCodigoPedido.caption); {Rodolfo}

end;


procedure TFPEDIDO.edtFerragemCor_PPRefereniciaKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   TEdit(Sender).Clear;
end;

procedure TFPEDIDO.edtPerfiladoCor_PPReferenciaKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   TEdit(Sender).Clear;
end;


procedure TFPEDIDO.EdtVidroCor_PPReferenciaKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   TEdit(Sender).Clear;
end;


procedure TFPEDIDO.edtKitBoxCor_PPReferenciaKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   TEdit(Sender).Clear;
end;


procedure TFPEDIDO.edtServico_PPReferenciaKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   //if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   //TEdit(Sender).Clear;
end;


procedure TFPEDIDO.edtPersiana_PPReferenciaKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   TEdit(Sender).Clear;

end;


procedure TFPEDIDO.edtDiverso_PPReferenciaKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   TEdit(Sender).Clear;
end;


procedure TFPEDIDO.edtFerragemCor_PPRefereniciaExit(Sender: TObject);
begin
  if TEdit(Sender).Text = ''then
  Begin
    edtFerragemCor_PP.Text:='';
    lbNomeFerragem_PP.Caption:='';
    exit;
  end;
end;


procedure TFPEDIDO.DBGridFerragem_PPKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then DBGridProjetoPedidoDblClick(sender);
end;


procedure TFPEDIDO.DBGridPerfilado_PPKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then DBGridProjetoPedidoDblClick(sender);

end;


procedure TFPEDIDO.DBGridVidro_PPKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then DBGridProjetoPedidoDblClick(sender);
end;


procedure TFPEDIDO.DBGridKitBox_PPKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then DBGridProjetoPedidoDblClick(sender);

end;


procedure TFPEDIDO.DBGridServico_PPKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then DBGridProjetoPedidoDblClick(sender);

end;


procedure TFPEDIDO.DBGridPersiana_PPKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then DBGridProjetoPedidoDblClick(sender);
end;


procedure TFPEDIDO.DBGridDiverso_PPKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then DBGridProjetoPedidoDblClick(sender);

end;

procedure TFPEDIDO.BtopcoesClick(Sender: TObject);
begin
     Self.ObjPedidoObjetos.BotaoOpcoes(lbCodigoPedido.caption);

     if (lbCodigoPedido.caption<>'')
     and (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Status=dsinactive)
     Then Begin
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(lbCodigoPedido.caption);
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
               Self.TabelaParaControles;
     End;
     __VerificaProducaoPedido;
end;

procedure TFPEDIDO.DesabilitaHabilitaBotoesGravarExcluir(var PBtGravar,PBtExcluir: TBitBtn);
begin

     If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Concluido = 'S')
     Then Begin
              PBtGravar.Enabled:=false;
              PBtExcluir.Enabled:=false;
     End
     Else Begin
              PBtGravar.Enabled:=true;
              PBtExcluir.Enabled:=true;
     End;
end;


Procedure TfPedido.AtualizavalorEditsPedido;
Var
PValorTotal,PDesconto,PPorcentagemDesconto, Pacrescimo,PporcentagemAcrescimo,
Pdescontobonificacao,PbonificacaoGerada,PvalorFinal:Currency;
begin
    //"VALORFINAL"
    //COMPUTED BY (valortotal-valordesconto+valoracrescimo-ValorBonificacaoCliente+BonificacaoGerada),

    PValorTotal:=0;
    PDesconto:=0;
    PPorcentagemDesconto:=0;
    Pacrescimo:=0;
    PporcentagemAcrescimo:=0;
    Pdescontobonificacao:=0;
    PbonificacaoGerada:=0;
    PvalorFinal:=0;

    Try
       if(lbValorProdutos.Caption<>'')
       then PValorTotal:=StrToCurr(tira_ponto(lbValorProdutos.Caption));
    Except
       PValorTotal:=0;
    End;

    Try
       if(edtvalordesconto.Text<>'')
       then PDesconto:=StrToCurr(tira_ponto(edtvalordesconto.Text));
    Except
       edtvalordesconto.text:='0';
       Pdesconto:=0;
    End;

    Try
       if (EdtPorcentagemDesconto.Text<>'') Then
        PPorcentagemDesconto:=StrToCurr(tira_ponto(EdtPorcentagemDesconto.Text))
       Else
       Begin
          EdtPorcentagemDesconto.text:='0';
          PPorcentagemDesconto:=0;
       End;
    Except
       EdtPorcentagemDesconto.text:='0';
       PPorcentagemDesconto:=0;
    End;

    Try
       if(edtvaloracrescimo.Text<>'')
       then Pacrescimo:=StrToCurr(tira_ponto(edtvaloracrescimo.Text));
    Except
       edtvaloracrescimo.text:='0';
       Pacrescimo:=0;
    End;

    Try
       if (EdtPorcentagemacrescimo.Text<>'')
       Then PPorcentagemacrescimo:=StrToCurr(tira_ponto(EdtPorcentagemacrescimo.Text))
       Else Begin
                 EdtPorcentagemacrescimo.text:='0';
                 PPorcentagemacrescimo:=0;
       End;
    Except
       EdtPorcentagemacrescimo.text:='0';
       PPorcentagemacrescimo:=0;
    End;


    Try
       if(EdtValorBonificacaoCliente.Text<>'')
       then Pdescontobonificacao:=StrToCurr(tira_ponto(EdtValorBonificacaoCliente.Text));
    Except
       EdtValorBonificacaoCliente.text:='0';
       Pdescontobonificacao:=0;
    End;


    Try
       if(lbBonGeradaPedido.Caption<>'')
       then PbonificacaoGerada:=StrToCurr(tira_ponto(lbBonGeradaPedido.Caption));
    Except
      // EdtBonificacaoGerada.text:='0';
       PbonificacaoGerada:=0;
    End;

    if (Pacrescimo>0)
    Then Begin
              try
                  Pporcentagemacrescimo:=(pacrescimo*100)/pvalortotal;
              Except
                  Pporcentagemacrescimo:=0;
              End;
    End
    Else Begin
              if (PporcentagemAcrescimo>0)
              Then begin
                        Pacrescimo:=(PValorTotal*PporcentagemAcrescimo)/100;
                        PporcentagemAcrescimo:=0;
              end;
    end;

    if (Pdesconto>0)
    Then Begin
              Try
                  Pporcentagemdesconto:=(pdesconto*100)/pvalortotal
              Except
                  Pporcentagemdesconto:=0;
              End;
    End
    Else Begin
              Pdesconto:=(PValorTotal*Pporcentagemdesconto)/100;

    end;

    PvalorFinal:=Pvalortotal-PDesconto+Pacrescimo-Pdescontobonificacao+PbonificacaoGerada;
    //*************************************************************************
    lbValorProdutos.Caption              :=formata_valor(PValorTotal);
    edtvalordesconto.Text           :=formata_valor(PDesconto);
    EdtPorcentagemDesconto.Text     :=formata_valor(PPorcentagemDesconto);
    edtvaloracrescimo.Text          :=formata_valor(Pacrescimo);
    EdtPorcentagemacrescimo.Text    :=formata_valor(PPorcentagemacrescimo);
    EdtValorBonificacaoCliente.Text :=formata_valor(Pdescontobonificacao);
    lbBonGeradaPedido.Caption       :=formata_valor(PbonificacaoGerada);
    lbValorFinalPedido.Caption            :=formata_valor(Pvalorfinal);
    Pporcentagemdesconto:=0;
end;


procedure TFPEDIDO.edtAlturaDiverso_PPExit(Sender: TObject);
begin
  if (EdtCodigoProjetoPedido.Text='') then
  begin
    MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
    exit;
  End;

  if (edtAlturaDiverso_PP.Text='') or (edtLarguraDiverso_PP.Text='')then
    exit;

  EdtQuantidadeDiverso_PP.Text:=CurrToStr((StrToCurr(tira_ponto(edtAlturaDiverso_PP.Text)) *
                                              StrToCurr(tira_ponto(edtLarguraDiverso_PP.Text)))/1000000);

  edtValorDiverso_PP.text:=Formata_Valor(Self.ObjPedidoObjetos.RetornaValorDiverso(EdtCodigoProjetoPedido.Text,EdtDiverso_PP.Text));
  Self.edtMaterial_PPExit( edtValorDiverso_PP ); 

end;


procedure TFPEDIDO.edtLarguraDiverso_PPExit(Sender: TObject);
begin
  if (EdtCodigoProjetoPedido.Text='') then
  begin
    MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
    exit;
  End;

  if (edtAlturaDiverso_PP.Text='') or (edtLarguraDiverso_PP.Text='')then
  exit;

  EdtQuantidadeDiverso_PP.Text:=CurrToStr((StrToCurr(tira_ponto(edtAlturaDiverso_PP.Text)) *
                                          StrToCurr(tira_ponto(edtLarguraDiverso_PP.Text)))/1000000);
  edtValorDiverso_PP.text:=Formata_Valor(Self.ObjPedidoObjetos.RetornaValorDiverso(EdtCodigoProjetoPedido.Text,EdtDiverso_PP.Text));
  Self.edtMaterial_PPExit( edtValorDiverso_PP );

end;


procedure TFPEDIDO.edtCorDiversoProjetoPedidoReferenciaExit(
  Sender: TObject);
begin
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorDiversoExit(Sender, EdtCorDiversoProjetoPedido, lbNomeCorDiversoProjetoPedido);
end;

procedure TFPEDIDO.edtCorDiversoProjetoPedidoReferenciaKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorDiversoKeyDown(Sender, EdtCorDiversoProjetoPedido, Key, Shift, lbNomeCorDiversoProjetoPedido,EdtProjetoPedido.text);
end;


procedure TFPEDIDO.btPrimeiroClick(Sender: TObject);
begin
    if  (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrimeiroRegistro = false)then
    exit;

    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;


procedure TFPEDIDO.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigoPedido.caption='')then
    lbCodigoPedido.caption:='';

    if  (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.RegistoAnterior(StrToInt(lbCodigoPedido.caption)) = false)then
    exit;

    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;


procedure TFPEDIDO.btProximoClick(Sender: TObject);
begin
    if (lbCodigoPedido.caption='')then
    lbCodigoPedido.caption:= '';

    if  (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.ProximoRegisto(StrToInt(lbCodigoPedido.caption)) = false)then
    exit;

    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;


procedure TFPEDIDO.btUltimoClick(Sender: TObject);
begin
    if  (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.UltimoRegistro = false)then
    exit;

    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
    Self.ObjetoParaControles;
end;


procedure TFPEDIDO.HabilitaBotoesRegistro;
begin
    btPrimeiro.Enabled:=true;
    btAnterior.Enabled:=true;
    btProximo.Enabled:=true;
    btUltimo.Enabled:=true;
end;


procedure TFPEDIDO.DesabilitaBotoesRegistro;
begin
    btPrimeiro.Enabled:=false;
    btAnterior.Enabled:=false;
    btProximo.Enabled:=false;
    btUltimo.Enabled:=false;
end;


procedure TFPEDIDO.edtvaloracrescimopedidoprojetoExit(Sender: TObject);
Var PDesconto, PAcrescimo, PValor : Currency;
begin
    TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
    PDesconto:=0;
    PAcrescimo:=0;
    PValor:=0;

    try
        if(lbvalortotalproj.Caption<>'')then
        PValor:=StrToCurr(tira_ponto(lbvalortotalproj.Caption));

        if (edtvaloracrescimopedidoprojeto.Text<>'')then
        PAcrescimo:=StrToCurr(tira_ponto(edtvaloracrescimopedidoprojeto.Text));

        if (edtvalordescontopedidoprojeto.Text<>'')then
        PDesconto:=StrToCurr(tira_ponto(edtvalordescontopedidoprojeto.Text));

        lbvalorfinalproj.Caption:=formata_valor(CurrToStr(PValor+PAcrescimo-PDesconto));
    except
        MensagemErro('Erro ao tentar calcular os valores de Desconto e Acr�scimo');
    end;
end;


procedure TFPEDIDO.edtvalordescontopedidoprojetoExit(Sender: TObject);
Var PDesconto, PAcrescimo, PValor : Currency;
begin
    TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
    PDesconto:=0;
    PAcrescimo:=0;
    PValor:=0;

    try
        if(lbvalortotalproj.Caption<>'')then
        PValor:=StrToCurr(tira_ponto(lbvalortotalproj.Caption));

        if (edtvaloracrescimopedidoprojeto.Text<>'')then
        PAcrescimo:=StrToCurr(tira_ponto(edtvaloracrescimopedidoprojeto.Text));

        if (edtvalordescontopedidoprojeto.Text<>'')then
        PDesconto:=StrToCurr(tira_ponto(edtvalordescontopedidoprojeto.Text));

        lbvalorfinalproj.Caption:=formata_valor(CurrToStr(PValor+PAcrescimo-PDesconto));
    except
        MensagemErro('Erro ao tentar calcular os valores de Desconto e Acr�scimo');
    end;
end;


procedure TFPEDIDO.edtValorFerragem_PPExit(Sender: TObject);
begin
      TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
end;


procedure TFPEDIDO.edtValorPerfilado_PPExit(Sender: TObject);
begin
      TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
end;


procedure TFPEDIDO.EdtValorVidro_PPExit(Sender: TObject);
begin
      TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
end;


procedure TFPEDIDO.edtValorKitBox_PPExit(Sender: TObject);
begin
      TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
end;


procedure TFPEDIDO.edtValorServico_PPExit(Sender: TObject);
begin
      TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
end;


procedure TFPEDIDO.edtValorPersiana_PPExit(Sender: TObject);
begin
      TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
end;


procedure TFPEDIDO.edtValorDiverso_PPExit(Sender: TObject);
begin
      TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
end;


procedure TFPEDIDO.btGerarVendaClick(Sender: TObject);
begin
  Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.RamoAtividade.Get_Nome;
  if (lbCodigoPedido.caption='')then
  begin
    exit;
  end;
  ObjParametroGlobal.ValidaParametro('VERIFICA ESTOQUE ANTES GERAR VENDA')  ;
  if(ObjParametroGlobal.Get_Valor='SIM') then
  begin
     if(___VerificaEstoque=False)
     then Exit;

  end;


  if (Self.ObjPedidoObjetos.GerarVenda(lbCodigoPedido.caption)=False) then
    exit;


  Self.GravaVendaNaTabVendas(lbCodigoPedido.Caption);

  if(self.GravaMateriaisVendidos(lbCodigoPedido.Caption)=False) then
  begin
    ObjPedidoObjetos.RetornarVenda(lbCodigoPedido.Caption) ;
    Exit;
  end;

  //Atualizando a tela 11/06 celio - inverti estava antes de gravavendana tabvendas
  if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(lbCodigoPedido.caption)=false)
  then
  Begin
    exit;
  end;
  Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
  Self.ObjetoParaControles;

    if (Self.PimprimePedidoAposGerarVenda=True) then
    Self.ObjPedidoObjetos.ImprimePedidoCompacto(lbCodigoPedido.caption);

  if (Self.PimprimeControleEntregaAposGerarVenda=True) then
    Self.ObjPedidoObjetos.ImprimePedidoControleEntrega(lbCodigoPedido.caption);

  if (Self.ObjPedidoObjetos.Parametro_ImprimeAutorizacaoNF) then
    Self.ObjPedidoObjetos.ImprimeAutorizacaoEmissaoNF(lbCodigoPedido.caption);


  //Se der erro descomenta aqui e apaga esses comando la em cima...
  {Self.GravaVendaNaTabVendas(lbCodigoPedido.Caption);

  if(self.GravaMateriaisVendidos(lbCodigoPedido.Caption)=False)
  then ObjPedidoObjetos.RetornarVenda(lbCodigoPedido.Caption) ;  }

  if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(lbCodigoPedido.caption)=false) then
  begin
    exit;
  end;
  Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
  Self.ObjetoParaControles;

end;


procedure TFPEDIDO.btTituloClick(Sender: TObject);
Var
FTituloXX:TFtitulo;
begin
     if lbNumTitulo.Caption = '' then
     exit;

     try
        FTituloXX:=TFtitulo.Create(nil);
        FTituloXX.LocalizaCodigoInicial(lbNumTitulo.Caption);
        FTituloXX.ShowModal;
     Finally
        FreeAndNil(FTituloXX);
     End;
end;


procedure TFPEDIDO.AtualizaPedidoProjeto(PPedidoProjeto: string);
var
  Largura,Altura:string;
begin
     if (self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PPedidoProjeto)=false)then
     Begin
          MensagemErro('Pedido Projeto n�o encontrado.');
          Exit;
     end;
     self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     With Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto do
     Begin

        edtcodigoProjetoPedido.Text:=Get_Codigo;

        EdtProjetoPedido.Text:=Projeto.Get_Codigo;
        EdtProjetoPedidoReferencia.Text:=Projeto.Get_Referencia;
        lbNomeProjetoPedido.Caption:=Projeto.Get_Descricao;

        EdtCorFerragemProjetoPedido.Text:=CorFerragem.Get_Codigo;
        EdtCorFerragemProjetoPedidoReferencia.Text:=CorFerragem.Get_Referencia;
        lbNomeCorFerragemProjetoPedido.Caption:=CorFerragem.Get_Descricao;

        EdtCorPerfiladoProjetoPedido.Text:=CorPerfilado.Get_Codigo;
        EdtCorPerfiladoProjetoPedidoReferencia.Text:=CorPerfilado.Get_Referencia;
        lbNomeCorPerfiladoProjetoPedido.Caption:=CorPerfilado.Get_Descricao;

        EdtCorVidroProjetoPedido.Text:=CorVidro.Get_Codigo;
        EdtCorVidroProjetoPedidoReferencia.Text:=CorVidro.Get_Referencia;
        lbNomeCorVidroProjetoPedido.Caption:=CorVidro.Get_Descricao;

        EdtCorKitBoxProjetoPedido.Text:=CorKitBox.Get_Codigo;
        EdtCorKitBoxProjetoPedidoReferencia.Text:=CorKitBox.Get_Referencia;
        lbNomeCorKitBoxProjetoPedido.Caption:=CorKitBox.Get_Descricao;

        EdtCorDiversoProjetoPedido.Text:=CorDiverso.Get_Codigo;
        EdtCorDiversoProjetoPedidoReferencia.Text:=CorDiverso.Get_Referencia;
        lbNomeCorDiversoProjetoPedido.Caption:=CorDiverso.Get_Descricao;

        EdtLocalProjetoPedido.Text:=Get_Local;
        ComboTipoProjetoPedido.Text:=Get_TipoPedido;
        lbvalortotalproj.Caption:=formata_valor(Get_ValorTotal);
        edtvalordescontopedidoprojeto.text:=formata_valor(Get_ValorDesconto);
        edtvaloracrescimopedidoprojeto.text:=formata_valor(Get_ValorAcrescimo);
        lbvalorfinalproj.Caption:=formata_valor(Get_ValorFinal);

        Self.ObjPedidoObjetos.RetornaMedidas(MemoMedidas,EdtCodigoProjetoPedido.Text,largura,altura);
        edtLargura.Text:=Largura;
        edtAltura.Text:=Altura;
     End;

end;


procedure TFPEDIDO.btAlterarPedidoProjetoClick(Sender: TObject);
var
Pvalorfinal,pvalor:Currency;
PedidoProjetoLocal:string;
PAlterouCoresNoPedidoProjeto:Boolean;
begin
    Pvalorfinal:=0;
    Pvalor:=0;

    if (VerificaPedidoConcluido=True)
    Then exit;

Try
    // Naum posso deixar ninguem alterar o campo projeto de um pedido depois que
    // foi gravado seus relacionamentos
    if (Self.ObjPedidoObjetos.RetornaProjetoDoPedido(EdtCodigoProjetoPedido.Text) <> (EdtProjetoPedido.Text))
    then
    Begin
         MensagemErro('O projeto n�o pode ser alterado. Exclua o projeto desejado e grave novamente.');
         exit;
    end;

    // Usarei isso na hora de salvar, mas tem que ser posto aqui por causa do Get e Submit do objeto
    PAlterouCoresNoPedidoProjeto:=Self.AlterouCoresdoPedidoProjeto;


    With Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto do
    Begin

        //Grava na Tabela Pedido projeto, os dados iniciais
        ZerarTabela;
        Status:=dsEdit;

        Submit_Codigo(edtcodigoProjetoPedido.Text);
        Pedido.Submit_Codigo(lbCodigoPedido.caption);
        Projeto.Submit_Codigo(EdtProjetoPedido.Text);
        CorFerragem.Submit_Codigo(EdtCorFerragemProjetoPedido.Text);
        CorPerfilado.Submit_Codigo(EdtCorPerfiladoProjetoPedido.Text);
        CorVidro.Submit_Codigo(EdtCorVidroProjetoPedido.Text);
        CorKitBox.Submit_Codigo(EdtCorKitBoxProjetoPedido.Text);
        CorDiverso.Submit_Codigo(EdtCorDiversoProjetoPedido.Text);
        Submit_Local(EdtLocalProjetoPedido.Text);
        Submit_Observacao(edtobservacao.Text);
        Submit_TipoPedido(ComboTipoProjetoPedido.Text);
        Submit_ValorTotal    (tira_ponto(lbvalortotalproj.Caption));
        Submit_ValorDesconto (tira_ponto(edtvalordescontopedidoprojeto.text));
        Submit_ValorAcrescimo(tira_ponto(edtvaloracrescimopedidoprojeto.text));

        // Se este � o projeto principal naum poderia ser alterado em hip�tese alguma,
        // mas ouve mudan�a nos planos agora Sr. Carlos quer que quando altera um podido projeto
        //principal autmaticamente o sistema altera todos os seus deriVados
        if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
        then Begin
                 if (MensagemPergunta('Existem projetos derivados desse projeto.'+#13+
                                      'Este procedimento ir� APAGAR todos os projetos replicados e RECRI�-LOS conforme altera��o feita'+#13+
                                      'no Projeto Principal.'+#13#13+
                                      'Tem CERTEZA  que deseja seguir com o procedimento?' )=mrno) then
                 Begin
                     exit;
                 end;

                 if (Salvar(True)=False)
                 Then Begin
                       EdtProjetoPedidoReferencia.SetFocus;
                       exit;
                 End;
                  // Apos salvar  Altera��o presciso saber se o cara mexeu nas cores e
                 // como o pre�o varia de acordo com a cor, preciso recalcular
                 if (PAlterouCoresNoPedidoProjeto=true)then
                 Self.ObjPedidoObjetos.ReCalculaProjeto(EdtCodigoProjetoPedido.Text,edtLargura.Text,edtAltura.Text);
                  // Executo operacoes para excluir e reciar os projetos novamente
                 if (Self.ObjPedidoObjetos.ApagaPedidoProjetoRepliocadosERecria(EdtCodigoProjetoPedido.Text) = false) then
                 exit;

        end
        else // Se nao for pedidoprincipal
        Begin
                  if (Salvar(True)=False)
                  Then Begin
                        EdtProjetoPedidoReferencia.SetFocus;
                        exit;
                  End;

                  if (PAlterouCoresNoPedidoProjeto=true)
                  then Self.ObjPedidoObjetos.ReCalculaProjeto(EdtCodigoProjetoPedido.Text,edtLargura.text,edtAltura.Text);
        end;


        if (Self.ObjPedidoObjetos.AtualizaValorPedido(lbCodigoPedido.caption)=false)then
        Begin
             MensagemErro('N�o foi poss�vel Atualizar o Pedido');
             FDataModulo.IBTransaction.RollbackRetaining;
             exit;
        end;

        FDataModulo.IBTransaction.CommitRetaining;
        EdtProjetoPedidoReferencia.SetFocus;
        Self.ResgataPedidoProjeto;
        Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
        Self.AtualizaValoresPedido(lbCodigoPedido.caption);
        MensagemAviso('Alterado com Sucesso');
        BtCancelarProjetoPedido.Click;
        Self.PreparaPedidoProjeto;
    End;


    if(Integrar_CorteCerto=False)
    then MensagemErro('N�o foi poss�vel gerar arquivo para sincroniza��o SAB/Corte Certo');

Finally
       FDataModulo.IBTransaction.RollbackRetaining;
End;

end;


procedure TFPEDIDO.edtAlturaPersiana_PPExit(Sender: TObject);
begin
   if (EdtAlturaPersiana_PP.Text <> '') and (EdtLarguraPersiana_PP.Text <> '') then
   Begin
        EdtQuantidadePersiana_PP.Text:=CurrToStr((StrToCurr(EdtAlturaPersiana_PP.Text) * StrToCurr(EdtLarguraPersiana_PP.Text))/1000000);// Quantidade � em M� mas os campos Altura e LArgura � mem MM.
   end;
end;


procedure TFPEDIDO.edtLarguraPersiana_PPExit(Sender: TObject);
begin
   if (EdtAlturaPersiana_PP.Text <> '') and (EdtLarguraPersiana_PP.Text <> '') then
   Begin
        EdtQuantidadePersiana_PP.Text:=CurrToStr((StrToCurr(EdtAlturaPersiana_PP.Text) * StrToCurr(EdtLarguraPersiana_PP.Text))/1000000);// Quantidade � em M� mas os campos Altura e LArgura � mem MM.
   end;

end;


procedure TFPEDIDO.memoComplementoPersiana_PPKeyPress(Sender: TObject;
  var Key: Char);
begin
if (Key = #13)then
Abort;
end;


procedure TFPEDIDO.edtQuantidadePersiana_PPExit(Sender: TObject);
var
  valor:Currency;
begin
  if (Length(EdtQuantidadePersiana_PP.Text)>6)then
  Begin
       MensagemErro('Valor acima do permitido. Quantidade � em Metros');
       EdtQuantidadePersiana_PP.Clear;
       EdtQuantidadePersiana_PP.SetFocus;
  end;

  if(EdtQuantidadePersiana_PP.Text='')
  then Exit;

  {valor:= StrToCurr(EdtValorPersiana_PP.Text);
  valor:= valor*StrToCurr(edtQuantidadePersiana_PP.Text);
  edtValorPersiana_PP.text:= CurrToStr(valor); }

end;

//memo de observa��o
procedure TFPEDIDO.memoObervacaoKeyPress(Sender: TObject; var Key: Char);
Var Ptexto : string;
begin
    PTexto:='';
    //MemoObservacao.autosize := false;
    MemoObervacao.MaxLength := 2000;      //tamanho maximo do memo � de 250 caracteres
    PTexto:=MemoObervacao.Lines.Strings[MemoObervacao.Lines.Count-1];
   // PTexto:=MemoObervacao.Text;
   //Quando chegar a 45 caracteres dar enter
   (*
     if (Length(PTexto) > 45) and (key <> #8) then
     Begin

       Key:= #13;//enter

     end;
     if(Key=#13)
     then MemoObervacao.SetFocus;

     if (MemoObervacao.Lines.Count >15) then
     Begin
             Abort;
             If Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Status=dsInactive
             Then exit;

             If ControlesParaObjeto=False
             Then Begin
                       Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
                       exit;
             End;

             If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.salvar(true)=False)
             Then exit;

             lbCodigoPedido.caption:=Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_codigo;
             habilita_botoes(Self);
             desabilita_campos(Self);
             btnovo.Visible :=true;
             btalterar.Visible:=true;
             btpesquisar.Visible:=true;
             btrelatorio.Visible:=true;
             btexcluir.Visible:=true;
             btsair.Visible:=true;
             btopcoes.Visible :=true;
             pgc1.TabIndex:=1;
     end;
     *)
end;


procedure TFPEDIDO.EdtAlturaVidro_PPExit(Sender: TObject);
var
pquantidade,paltura,plargura,plarguraarredondamento,palturaarredondamento:Currency;
begin
   if (EdtAlturaVidro_PP2.Text <> '') and (EdtLarguraVidro_PP2.Text <> '') then
   Begin
        Try
           paltura:=strtocurr(tira_ponto(EdtAlturaVidro_PP2.Text));
        Except
              paltura:=0;
        End;

        Try
           plargura:=strtocurr(tira_ponto(EdtlarguraVidro_PP2.Text));
        Except
            plargura:=0;
        End;

        FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
        FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);

        pquantidade:=((plargura+plarguraarredondamento)*(paltura+palturaarredondamento))/1000000;
        EdtQuantidadeVidro_PP2.Text:=formata_valor(CurrToStr(pquantidade));
   end
   Else EdtQuantidadeVidro_PP2.Text:='0';

end;


Function TFPEDIDO.PreenchePedidoParaConsultaRapida:Boolean;
Var  PNovoCodigo,PVendedor, PCliente : string;

begin
      //Consulta r�pida � quando se � necessario  saber quanto d� um orcamento
      //sem que esse orcamento seja um pedido
      //ex.: Alguem liga pra e quer saber quanto d� uma porta de 3x2
      //mas o usuario naum quer clicar no botao novo, escolher cliente,vendedor
      //entao essa modulo ira fazer isso pro usuario poder quanhar tempo.
      //nao pode esquecer de criar um cliente e um Vendedor como nome PADR�O
      //e guardar o codigo num parametro.
      Result:=false;

try
      //primeiro localizo os parametros
      if (ObjParametroGlobal.ValidaParametro('CODIGO CLIENTE PADR�O')=false)then
      Begin
           MensagemErro('O par�metro "CODIGO CLIENTE PADR�O" n�o foi encontrado.');
           exit;
      end;
      PCliente:=ObjParametroGlobal.Get_Valor;

      if (ObjParametroGlobal.ValidaParametro('CODIGO VENDEDOR PADR�O')=false)then
      Begin
           MensagemErro('O par�metro "CODIGO VENDEDOR PADR�O" n�o foi encontrado.');
           exit;
      end;
      PVENDEDOR:=ObjParametroGlobal.Get_Valor;

      // Agora eu vou criar um novo pedido.
      With self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido do
      Begin
           ZerarTabela;
           Status:=dsinsert;
           PNovoCodigo:=Get_NovoCodigo;
           Submit_Codigo(PNovoCodigo);
           Cliente.Submit_codigo(PCliente);
           Vendedor.Submit_codigo(PVendedor);
           Submit_Data(DateToSTr(Now));
           Submit_Concluido('N');
           Submit_ValorTotal('0');
           Submit_Valoracrescimo('0');
           Submit_Valordesconto('0');

           if (Salvar(false)=false)then
           begin
                MensagemErro('Erro na tentativa de Salvar un novo pedido.');
                FDataModulo.IBTransaction.RollbackRetaining;
                Result:=false;
                exit;
           end;

           FDataModulo.IBTransaction.CommitRetaining;
           self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(PNovoCodigo);
           self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
           ObjetoParaControles;

           Self.pgc1.TabIndex:=1;

           Result:=true;

      end;
except
     Result:=false;
end;
end;


procedure TFPEDIDO.AtualizaValoresPedido(PPedido: string);
Var PPorcentagem, PValor : Currency;
begin
   try
      With self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido do
      begin
           LocalizaCodigo(PPedido);
           TabelaparaObjeto;
           Self.TabelaParaControles;
      end;
   except
      MensagemErro('Erro ao tentar atualizar os Valores do Pedido');
   end;
end;


procedure TFPEDIDO.memoObervacaoExit(Sender: TObject);
begin
     {If Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
     End;

     If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.salvar(true)=False)
     Then exit;

     lbCodigoPedido.caption:=Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     Guia.TabIndex:=1;  }

end;


function TFPEDIDO.AlterouCoresdoPedidoProjeto: Boolean;
begin
      //pra eu saber se foi alterado as cores basta comparar o que tem dentro
      //dos edits com o o Get do objeto
      //basta que um item esteja alterado para ser necess�rio recalcular o projeto
      Result:=false;
      With Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto do
      Begin
           if (EdtCorFerragemProjetoPedido.Text <> CorFerragem.Get_Codigo)then
           Begin
                Result:=true;
                exit;
           end;

           if (EdtCorPerfiladoProjetoPedido.Text <> CorPerfilado.Get_Codigo)then
           Begin
                Result:=true;
                exit;
           end;

           if (EdtCorVidroProjetoPedido.Text <> CorVidro.Get_Codigo)then
           Begin
                Result:=true;
                exit;
           end;

           if (EdtCorKitBoxProjetoPedido.Text <> CorKitBox.Get_Codigo)then
           Begin
                Result:=true;
                exit;
           end;

           if (EdtCorDiversoProjetoPedido.Text <> CorDiverso.Get_Codigo)then
           Begin
                Result:=true;
                exit;
           end;

           if (ComboTipoProjetoPedido.Text <> Get_TipoPedido)then
           Begin
                Result:=true;
                exit;
           end;

      end;
end;


procedure TFPEDIDO.edtarquitetoExit(Sender: TObject);
begin
Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtarquitetoExit(sender,lbnomearquiteto);
end;


procedure TFPEDIDO.edtarquitetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   query:TIBQuery;
begin
  Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtarquitetoKeyDown(sender,key,shift,lbnomearquiteto);
end;

procedure TFPEDIDO.edtvalordescontoExit(Sender: TObject);
var
Pdesconto:Currency;

begin
    Try
       PDesconto:=StrtoCurr(edtvalordesconto.Text);
    Except
       PDesconto:=0;
    End;

    if (PDesconto=0)
    Then EdtPorcentagemDesconto.text:='0';

    
    Self.AtualizavalorEditsPedido;
end;


procedure TFPEDIDO.edtvaloracrescimoExit(Sender: TObject);
var
PAcrescimo:Currency;
begin
     try
        pacrescimo:=StrtoCurr(edtvaloracrescimo.text);
     Except
          PAcrescimo:=0;
     End;

     if (Pacrescimo=0)
     Then EdtPorcentagemAcrescimo.text:='0';

     Self.AtualizavalorEditsPedido;

end;


procedure TFPEDIDO.edtPorcentagemAcrescimoExit(Sender: TObject);
begin
    Self.AtualizavalorEditsPedido;
end;


procedure TFPEDIDO.edtPorcentagemDescontoExit(Sender: TObject);
begin
     Self.AtualizavalorEditsPedido;

end;


procedure TFPEDIDO.edtvidro_VLKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if (Key <> VK_F9) and (Key <> VK_TAB) and (Key <> VK_RETURN) then
   Begin
         TEdit(Sender).Clear;
         exit;
   end;
   Self.ObjPedidoObjetos.ObjVidro_PP.EdtVidroCorKeyDown(Sender, Key, Shift, lbnomevidro_vl);

   

end;


procedure TFPEDIDO.edtvidro_VLExit(Sender: TObject);
begin
      if TEdit(Sender).Text = ''then
      Begin
           lbNomeVidro_VL.Caption:='';
      end;
end;


procedure TFPEDIDO.Btgravar_VLClick(Sender: TObject);
var
paltura,plargura,palturaarredondamento,plarguraarredondamento,pquantidade,temp:currency;
preplica,cont:integer;

pvidro,pvalor,AreaMinima:string;
begin
    if (VerificaPedidoConcluido=True)
    Then exit;


    if (EdtCodigoProjetoPedido.Text='')
    then Begin
            MensagemErro('Escolha um Projeto na aba 2');
            exit;
    end;

    // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(EdtCodigoProjetoPedido.Text)=true)
    then Begin
            MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto este n�o pode ser alterado.');
            exit;
    end;

    // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(EdtCodigoProjetoPedido.Text);
    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
    if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')then
    Begin
          if MessageDlg('Este, � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                        'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                        'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
          then begin
               exit;
          end else
          Begin
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
               Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(true);
          end;
    end;
    
    With Self.ObjPedidoObjetos.ObjVidro_PP do
    Begin


        try
           plargura:=StrtoCurr(edtlargura_vl.text)
        Except
           plargura:=0;
        End;

        try
           paltura:=StrtoCurr(edtaltura_vl.text)
        Except
           paltura:=0;
        End;


        if (FdataModulo.Arredonda5CM(plargura,plarguraarredondamento)=False)
        Then exit;


        if (FdataModulo.Arredonda5CM(paltura,palturaarredondamento)=False)
        Then exit;


        pquantidade:=((plargura+plarguraarredondamento)*(paltura+palturaarredondamento))/1000000;

        //aqui ele arredonda a quarta casa decimal 0 a 4 para baixo de 5.. para cima
        pquantidade:=strtofloat(tira_ponto(FormatFloat('0.000',pquantidade)));

        AreaMinima:= Self.ObjPedidoObjetos.RetornaAreaMininaVidro( EdtCodigoProjetoPedido.Text, edtvidro_VL.text);
        if(pquantidade) < StrToFloat(AreaMinima)then
        begin
             pquantidade:=StrToCurr(AreaMinima);
        end;

        //edtquantidade_VL.text :=pquantidade;
        Try
           preplica:=strtoint(edtQuantidadePecas.Text);      
        Except
           preplica:=1;
        End;

        for cont:=1 to preplica do
        Begin
              ZerarTabela;
              Status:=dsInsert;
              Submit_Codigo('0');
              Submit_PedidoProjeto(EdtCodigoProjetoPedido.Text);
              VidroCor.Submit_Codigo(edtVidro_VL.Text);
              Submit_Quantidade(floattostr(pquantidade));
              Submit_Valor(tira_ponto(edtvalor_VL.Text));
              Submit_Largura(floattostr(plargura));
              Submit_Altura(floattostr(paltura));

              Submit_LarguraArredondamento(floattostr(plarguraarredondamento));
              Submit_AlturaArredondamento(floattostr(palturaarredondamento));


              Submit_Complemento(edtcomplementovidro_VL.text);
      
              try

                Temp:=StrtoCurr(tira_ponto(Formata_valor(Self.ObjPedidoObjetos.RetornaValorvidro(EdtCodigoProjetoPedido.Text,edtVidro_VL.Text))));
              Except
                Temp:=0;
              End;
      
              try
                 if (StrtoCurr(tira_ponto(EdtValor_VL.Text))<temp)
                 Then Begin
                   if not utilizaDescontoAcrescimopeloPrazo then
                   begin
                     mensagemerro('O Valor n�o pode ser menor que o pre�o de tabela. Pre�o de Tabela '+Formata_valor(temp));
                     exit;
                   end;
                 end;
              Except
      
              End;
      
      
              if (Salvar(True)=False)
              Then Begin
                        mensagemerro('Erro na tentativa de gravar');
                        edtVidro_VL.SetFocus;
                        exit;
              End;
        End;

        Self.ObjPedidoObjetos.AtualizavalorProjeto(EdtCodigoProjetoPedido.Text);

        pvidro:=edtvidro_VL.Text;
        pvalor:=edtvalor_VL.Text;
        limpaedit(PanelVendaemLote);
        edtvidro_VL.Text:=pvidro;
        edtvalor_VL.Text:=pvalor;
        edtvidro_vl.SetFocus;
        Self.ResgataVidro_PP;
        Self.AtualizaPedidoProjeto(EdtCodigoProjetoPedido.Text);
        Self.AtualizaValoresPedido(lbCodigoPedido.caption);
        edtVidro_VL.Text:='';
        edtvalor_VL.text:='';
    End;
end;


procedure TFPEDIDO.btexcluir_VLClick(Sender: TObject);
var
Xpedidoprojeto:string;
begin
     Exit;

     if (VerificaPedidoConcluido=True)
     Then exit;

     if (EdtCodigoProjetoPedido.Text='')
     Then Begin
               MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
               exit;
     End;

     if (DbGridVidroVL_PP.DataSource.dataset.recordcount=0)
     then exit;

     if (Mensagempergunta('Certeza que deseja excluir o vidro '+DbGridVidroVL_PP.DataSource.dataset.fieldbyname('vidro').asstring
         +' Altura '+DbGridVidroVL_PP.DataSource.dataset.fieldbyname('altura').asstring+' mm '
         +' Largura '+DbGridVidroVL_PP.DataSource.dataset.fieldbyname('largura').asstring+' mm')=mrno)
     Then exit;

     if (Self.ObjPedidoObjetos.ObjVidro_PP.LocalizaCodigo(DbGridVidroVL_PP.DataSource.dataset.fieldbyname('codigo').asstring)=False)
     then Begin
               mensagemerro('Vidro do pedido n�o localizado');
               exit;
     End;
     Self.ObjPedidoObjetos.ObjVidro_PP.TabelaparaObjeto;
     Xpedidoprojeto:=Self.ObjPedidoObjetos.ObjVidro_PP.get_PedidoProjeto;

     Self.ObjPedidoObjetos.ObjVidro_PP.exclui(Self.ObjPedidoObjetos.ObjVidro_PP.get_codigo,true);
     Self.ResgataVidro_PP;
     Self.AtualizaPedidoProjeto(Xpedidoprojeto);
     Self.AtualizaValoresPedido(lbCodigoPedido.caption);

     LimpaLabels_VidroPP;
end;


procedure TFPEDIDO.edtvalor_VLKeyPress(Sender: TObject; var Key: Char);
begin
     if not (key in ['0'..'9',#8,','])
     Then
         if key='.'
         then key:=','
         else key:=#0;
end;


procedure TFPEDIDO.edtquantidade_VLKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then key:=#0;

end;


procedure TFPEDIDO.Btrelatorios_VLClick(Sender: TObject);
begin
     Self.ObjPedidoObjetos.ImprimePedidoVidroLote(lbCodigoPedido.caption);
     Self.ResgataVidro_PP;
end;


procedure TFPEDIDO.CarregaParametros;
begin
    PimprimePedidoAposGerarVenda:=False;
    PimprimeControleEntregaAposGerarVenda:=False;

    if (ObjParametroGlobal.ValidaParametro('IMPRIMIR PEDIDO COMPACTO APOS GERAR VENDA PELO FORMULARIO'))
    Then Begin
              if (ObjParametroGlobal.get_valor='SIM')
              then Self.PimprimePedidoAposGerarVenda:=True;
    End;

    if (ObjParametroGlobal.ValidaParametro('IMPRIMIR CONTROLE DE ENTREGA APOS GERAR VENDA PELO FORMULARIO'))
    Then Begin
              if (ObjParametroGlobal.get_valor='SIM')
              then Self.PimprimeControleEntregaAposGerarVenda:=True;
    End;

end;


function TFPEDIDO.VerificaPedidoConcluido: Boolean;
begin
     result:=False;

     if (lbCodigoPedido.caption<>'0')
     Then Begin
             if (self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(lbCodigoPedido.caption)=False)
             Then Begin
                       MensagemErro('Pedido n�o localizado');
                       exit;
             End;

             self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

             if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.get_campo('concluido')='S')
             Then Begin
                       result:=True;
                       MensagemAviso('O Pedido encontra-se conclu�do!');
                       exit;
             End;
     End;
end;


procedure TFPEDIDO.lbGravarMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;


procedure TFPEDIDO.lbGravarMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
       TEdit(sender).Font.Style:=[fsBold,fsUnderline];
     
end;


procedure TFPEDIDO.NotebookMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
         ScreenCursorProc(crDefault);
end;


procedure TFPEDIDO.panelVendaemLoteMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
         ScreenCursorProc(crDefault);
end;


procedure TFPEDIDO.DBGridVidroVL_PPMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  ScreenCursorProc(crDefault);
end;


procedure TFPEDIDO.DBGridVidroVL_PPDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
  const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DbGridVidroVL_PP.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DbGridVidroVL_PP.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DbGridVidroVL_PP.DefaultDrawDataCell(Rect, DbGridVidroVL_PP.Columns[DataCol].Field, State);
          End;
end;


procedure TFPEDIDO.DBGridFerragem_PPDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridFerragem_PP.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridFerragem_PP.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridFerragem_PP.DefaultDrawDataCell(Rect, DBGridFerragem_PP.Columns[DataCol].Field, State);
          End;
end;


procedure TFPEDIDO.DBGridKitBox_PPDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DbGridKitBox_PP.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DbGridKitBox_PP.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DbGridKitBox_PP.DefaultDrawDataCell(Rect, DbGridKitBox_PP.Columns[DataCol].Field, State);
          End;
end;


procedure TFPEDIDO.DBGridPerfilado_PPDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridPerfilado_PP.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridPerfilado_PP.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridPerfilado_PP.DefaultDrawDataCell(Rect,DBGridPerfilado_PP.Columns[DataCol].Field, State);
          End;
end;


procedure TFPEDIDO.DBGridPersiana_PPDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridPersiana_PP.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridPersiana_PP.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridPersiana_PP.DefaultDrawDataCell(Rect,DBGridPersiana_PP.Columns[DataCol].Field, State);
          End;
end;


procedure TFPEDIDO.DBGridServico_PPDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridServico_PP.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridServico_PP.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridServico_PP.DefaultDrawDataCell(Rect,DBGridServico_PP.Columns[DataCol].Field, State);
          End;
end;


procedure TFPEDIDO.DBGridVidro_PPDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DbGridVidro_PP.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DbGridVidro_PP.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DbGridVidro_PP.DefaultDrawDataCell(Rect,DbGridVidro_PP.Columns[DataCol].Field, State);
          End;
end;


procedure TFPEDIDO.DBGridDiverso_PPDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridDiverso_PP.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridDiverso_PP.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridDiverso_PP.DefaultDrawDataCell(Rect,DBGridDiverso_PP.Columns[DataCol].Field, State);
          End;
end;


procedure TFPEDIDO.DBGridProjetoPedidoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridProjetoPedido.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridProjetoPedido.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridProjetoPedido.DefaultDrawDataCell(Rect, DBGridProjetoPedido.Columns[DataCol].Field, State);
          End;
end;


procedure TFPEDIDO.edtQtdeFerragem_PPExit(Sender: TObject);
var
  Valor:Currency;
begin

  if (edtFerragemCor_PP.text='')
  Then exit;

  if (EdtCodigoProjetoPedido.Text='') then
  begin
    MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
    exit;
  End;

  EdtValorFerragem_PP.text:=Formata_Valor(Self.ObjPedidoObjetos.RetornaValorFerragem(EdtCodigoProjetoPedido.Text,edtFerragemCor_PP.Text));

  self.edtMaterial_PPExit( edtValorFerragem_PP );

  if (EdtQtdeFerragem_PP.Text='')
  then Exit;

  //Valor:= StrToCurr(EdtValorFerragem_PP.Text);
  //Valor:= Valor * StrToCurr(EdtQtdeFerragem_PP.Text);
  //EdtValorFerragem_PP.Text:= CurrToStr(Valor);
end;


procedure TFPEDIDO.edtQuantidadeKitBox_PPExit(Sender: TObject);
var
  Valor:Currency;
begin
  //*****usado para pegar valor*******
  if (EdtKitBoxCor_PP.text='')
  Then exit;

  if (EdtCodigoProjetoPedido.Text='') then
  begin
    MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
    exit;
  End;

  edtValorKitBox_PP.text:=Formata_Valor(Self.ObjPedidoObjetos.RetornaValorKitBox(EdtCodigoProjetoPedido.Text,EdtKitBoxCor_PP.Text));
  Self.edtMaterial_PPExit( edtValorKitBox_PP );

  //**********************************


  if (EdtQuantidadeKitBox_PP.text='')
  then Exit;

  {Valor:= StrToCurr(EdtValorKitBox_PP.Text);
  Valor:= Valor * StrToCurr(EdtQuantidadeKitBox_PP.Text);
  EdtValorKitBox_PP.Text:= CurrToStr(Valor); }
end;


procedure TFPEDIDO.edtQuantidadePerfilado_PPExit(Sender: TObject);
var
  Valor:Currency;
begin
    //*****usado para pegar valor*******
   if (EdtPerfiladoCor_PP.text='')
   Then exit;

  if (EdtCodigoProjetoPedido.Text='') then
  begin
    MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
    exit;
  End;

   edtValorPerfilado_PP.text:=formata_valor(Self.ObjPedidoObjetos.RetornaValorPerfilado(EdtCodigoProjetoPedido.Text,EdtPerfiladoCor_PP.Text));
   self.edtMaterial_PPExit( edtValorPerfilado_PP );

   //**********************************

    if (edtQuantidadePerfilado_PP.text='')
    then Exit;

      {Valor:= StrToCurr(edtValorPerfilado_PP.Text);
      Valor:= Valor * StrToCurr(edtQuantidadePerfilado_PP.Text);
      edtValorPerfilado_PP.Text:= CurrToStr(Valor);  }
end;


procedure TFPEDIDO.edtquantidadeservicoExit(Sender: TObject);
var
  valor:Currency;
begin
    //*****usado para pegar valor*******
   if (EdtServico_PP.text='')
   Then exit;

   if (EdtCodigoProjetoPedido.Text='')
   Then Begin
               MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
               exit;
   End;

   edtValorServico_PP.text:=Formata_Valor(Self.ObjPedidoObjetos.RetornaValorServico(EdtCodigoProjetoPedido.Text,EdtServico_PP.Text));
  Self.edtMaterial_PPExit( edtValorServico_PP );
   //**********************************

    if(edtquantidadeservico.Text='') then
    exit;

    {valor:=StrToCurr(EdtValorServico_PP.Text);
    valor:=valor* StrToCurr(edtquantidadeservico.text);
    EdtValorServico_PP.Text:=CurrToStr(valor); }
end;


procedure TFPEDIDO.edtquantidade_VLExit(Sender: TObject);
var
  valor:currency;
begin
   //*****usado para pegar valor*******
   if (EdtVidro_VL.text='')
   Then exit;

  if (EdtCodigoProjetoPedido.Text='') then
  begin
    MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
    exit;
  End;

   edtValor_VL.text:=Formata_valor(Self.ObjPedidoObjetos.RetornaValorVidro(EdtCodigoProjetoPedido.Text,edtvidro_VL.Text));

   Self.edtMaterial_PPExit( edtValor_VL );
   //**********************************

    if(edtquantidade_VL.Text ='')
    then Exit;

   {valor:=StrToCurr(edtvalor_VL.Text);
    valor:=valor* StrToCurr(edtquantidade_VL.text);
    edtvalor_VL.Text:= CurrToStr(valor); }
end;


procedure TFPEDIDO.EdtQuantidadeVidro_PPExit(Sender: TObject);
var
  valor:Currency;
  AreaMinima:string;
begin
    //*****usado para pegar valor*******
   if (EdtVidroCor_PP2.text='')
   Then exit;

   if (EdtCodigoProjetoPedido.Text='')
   Then Begin
               MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
               exit;
   End;

   edtValorVidro_PP2.text:=Formata_valor(Self.ObjPedidoObjetos.RetornaValorVidro(EdtCodigoProjetoPedido.Text,EdtVidroCor_PP2.Text));
   //**********************************

    if(EdtQuantidadeVidro_PP2.Text='')
    then Exit;

    AreaMinima:= Self.ObjPedidoObjetos.RetornaAreaMininaVidro(EdtCodigoProjetoPedido.Text,EdtVidroCor_PP2.Text);
    if(StrToFloat(EdtQuantidadeVidro_PP2.Text) < StrToFloat(AreaMinima))
    then begin
          //EdtQuantidadeVidro_PP.Text:=AreaMinima;
          valor:=StrToCurr(EdtValorVidro_PP2.text);
          valor:= valor* StrToCurr(AreaMinima);
          EdtValorVidro_PP2.text:= CurrToStr(valor);
    end
    else
    begin
        valor:=StrToCurr(EdtValorVidro_PP2.text);
        valor:= valor* StrToCurr(EdtQuantidadeVidro_PP2.Text);
        EdtValorVidro_PP2.text:= CurrToStr(valor);
    end;
end;


procedure TFPEDIDO.edtQuantidadeDiverso_PPExit(Sender: TObject);
var
  valor:Currency;
  
begin

   //*****usado para pegar valor*******
   if (EdtDiverso_PP.text='')
   Then exit;

   if (EdtCodigoProjetoPedido.Text='')
   Then Begin
               MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
               exit;
   End;

   edtValorDiverso_PP.text:=Formata_Valor(Self.ObjPedidoObjetos.RetornaValorDiverso(EdtCodigoProjetoPedido.Text,EdtDiverso_PP.Text));
   Self.edtMaterial_PPExit( edtValorDiverso_PP );
   
   
   //**********************************


    if(edtQuantidadeDiverso_PP.text='')
    then Exit;


    {valor:= StrToCurr(edtValorDiverso_PP.Text);
    valor:= valor* StrToCurr(edtQuantidadeDiverso_PP.Text);
    edtValorDiverso_PP.text:=CurrToStr(valor); }
end;


procedure TFPEDIDO.lbNumTituloClick(Sender: TObject);
var
Tmptitulo:TFtitulo_novo;
begin
      If (lbCodigoPedido.Caption='' )
      Then Exit;

      if(ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE TITULOS')=False)
      then Exit;

      if (lbNumTitulo.Caption='' )
      Then begin
                MensagemErro('N�o existe T�tulo para essa Compra!');
                Exit;
      End;

      Try
            Tmptitulo:=TFTitulo_novo.create(nil);
      Except
            Messagedlg('Erro na tentativa de Criar o Formul�rio de T�tulo',mterror,[mbok],0);
            exit;
      End;
      Try
            TmpTitulo.quitaPrimeiraParcela:=False;
            TmpTitulo.LocalizaCodigoInicial( lbNumTitulo.Caption);
            TmpTitulo.showmodal;
      Finally
            Freeandnil(TmpTitulo);
      End;
end;


procedure TFPEDIDO.lbGerarNFClick(Sender: TObject);
var
  ObjNotaFiscal:TObjNotafiscalObjetos;
  query:TIBQuery;
begin

  (*try

    if (NFgerada=0) then
    begin

       try

        ObjNotaFiscal:=TObjNotaFiscalObjetos.create;

       except

       end;

       try

        query:=TIBQuery.Create(nil);
        query.Database:=FDataModulo.IBDatabase;

       except

       end;

       FescolheNF.create;
       FescolheNF.ShowModal;

       if (FescolheNF.edtnotafiscalatual.Text = '') then
        Exit;


       ObjNotaFiscal.GeraNotaFiscal(FescolheNF.edtnotafiscalatual.Text,lbCodigoPedido.Caption,FescolheNF.edtEdtCfop.Text,lbValorFinalPedido.Caption);

       with (query) do
       begin

          close;
          sql.Clear;
          SQL.Add('select * from tabnotafiscal where numpedido='+lbCodigoPedido.Caption);
          SQL.Add('and situacao=''I''');
          Open;

          if(recordcount > 0) then
          begin
             {seleciona a nota fiscal do pedido, detalhe(somente a(s) nota(s) que est�o com situacao(I)
              ObjNotaFiscal.ImprimeNotaFiscal(FescolheNF.edtnotafiscalatual.Text);}

             ObjNotaFiscal.ImprimeNotaFiscal(fieldbyname('codigo').AsString);
             ObjNotaFiscal.GravaProdutosNF(lbCodigoPedido.Caption,fieldbyname('codigo').AsString);
             lbGerarNF.Caption:='Reimprimir Nota Fiscal';
             NFgerada:=1;

          end;
       end;


    end
    else
    begin

      if (NFgerada = 1) then
      begin

        try
          query:=TIBQuery.Create(nil);
          query.Database:=FDataModulo.IBDatabase;
        except

        end;

        try
          ObjNotaFiscal:=TObjNotaFiscalObjetos.create;
        except

        end;

        try

          with (query) do
          begin

            close;
            sql.Clear;
            SQL.Add('select * from tabnotafiscal where numpedido='+lbCodigoPedido.Caption);
            SQL.Add('and situacao=''I''');

            Open;

            ObjNotaFiscal.ImprimeNotaFiscal(fieldbyname('codigo').AsString);

          end;

        finally

          FreeAndNil(query);

        end;

      end;

    end;

  finally

    FreeAndNil(ObjNotaFiscal);

  end;   *)

end;


procedure TFPEDIDO.edtarquitetoKeyPress(Sender: TObject; var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8),',']) then
    begin
        Key:= #0;
    end;
end;


procedure TFPEDIDO.edtClienteKeyPress(Sender: TObject; var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;


procedure TFPEDIDO.edtVendedorKeyPress(Sender: TObject; var Key: Char);
begin
     if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;


procedure TFPEDIDO.edtvidro_VLKeyPress(Sender: TObject; var Key: Char);
begin
      if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;


procedure TFPEDIDO.edtDiverso_PPReferenciaKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;


procedure TFPEDIDO.edtPersiana_PPReferenciaKeyPress(Sender: TObject;
  var Key: Char);
begin
      if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;


procedure TFPEDIDO.edtPerfiladoCor_PPReferenciaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;


procedure TFPEDIDO.edtProjetoPedidoReferenciaKeyPress(Sender: TObject;
  var Key: Char);
begin
      if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;


procedure TFPEDIDO.edtCorFerragemProjetoPedidoReferenciaKeyPress(
  Sender: TObject; var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end; 
end;

procedure TFPEDIDO.edtCorVidroProjetoPedidoReferenciaKeyPress(
  Sender: TObject; var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;


procedure TFPEDIDO.edtCorKitBoxProjetoPedidoReferenciaKeyPress(
  Sender: TObject; var Key: Char);
begin
        if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;


procedure TFPEDIDO.edtCorDiversoProjetoPedidoReferenciaKeyPress(
  Sender: TObject; var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;


procedure TFPEDIDO.edtCorPerfiladoProjetoPedidoReferenciaKeyPress(
  Sender: TObject; var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;


procedure TFPEDIDO.FormShow(Sender: TObject);
var
  PhoraUm,PhoraDois,PhoraTres,PhoraQuatro,PhoraCinco:Ttime;
begin
     Phoraum:=now;
      NFgerada:=0;


     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     pgc1.TabIndex:=0;
     ColocaUpperCaseEdit(Self);
     lbCodigoPedido.Caption:='';

     if(ObjParametroGlobal.ValidaParametro('NFE calcula total tributos')=false)
     then calculatotaltributos:=False
     else
     begin
        if(ObjParametroGlobal.Get_Valor='SIM')
        then calculatotaltributos:=True
        else calculatotaltributos:=False;
     end;


     Try
        self.ObjPedidoObjetos:=TObjPedidoObjetos.Create(self);
        PhoraTres:=now;

        DBGridProjetoPedido.DataSource:=Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.ObjDatasource;
        DBGridFerragem_PP.DataSource:=Self.ObjPedidoObjetos.ObjFerragem_PP.ObjDataSource;
        DBGridPerfilado_PP.DataSource:=Self.ObjPedidoObjetos.ObjPerfilado_PP.ObjDatasource;
        DbGridVidro_PP.DataSource:=Self.ObjPedidoObjetos.ObjVidro_PP.ObjDataSource;
        DbGridKitBox_PP.DataSource:=Self.ObjPedidoObjetos.ObjKitBox_PP.ObjDataSource;
        DBGridServico_PP.DataSource:=Self.ObjPedidoObjetos.ObjServico_PP.ObjDataSource;
        DBGridPersiana_PP.DataSource:=Self.ObjPedidoObjetos.ObjPersiana_PP.ObjDataSource;
        DBGridDiverso_PP.DataSource:=Self.ObjPedidoObjetos.ObjDiverso_PP.ObjDataSource;
        DbGridVidroVL_PP.DataSource:=Self.ObjPedidoObjetos.ObjVidro_PP.ObjDataSource;

     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PhoraQuatro:=now;
     PhoraCinco:=Now;
     Self.CarregaParametros;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btGravarProjetoPedido,'BOTAOINSERIR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(BtCancelarProjetoPedido,'BOTAOCANCELARPRODUTO.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btBtExcluirProjetoPedido,'BOTAORETIRAR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btAlterarPedidoProjeto,'BOTAOALTERARPRODUTO.BMP');
     FescolheImagemBotao.PegaFiguraBotaoSpeedButton(btReplicarPedido,'BOTAOREPLICARPEDIDO.BMP');
     FescolheImagemBotao.PegaFiguraImagem(ImgDesenho,'FOTO');


     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE PEDIDO')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);

     if(ObjParametroGlobal.ValidaParametro('PEDIR SENHA AO ESCOLHER O VENDEDOR')=false)
     then PedeSenhaVendedor:='NAO'
     else PedeSenhaVendedor:=ObjParametroGlobal.Get_Valor;

     if(ObjParametroGlobal.ValidaParametro('PEDIR SENHA ADM PARA DESCONTOS MAIORES')=false)
     then PedeSenhaADMdesconto:='NAO'
     else PedeSenhaADMdesconto:=ObjParametroGlobal.Get_Valor;

     PRAZOPAGAMENTOPADRAO := '25';
     if(ObjParametroGlobal.ValidaParametro('PRAZO DE PAGAMENTO PADR�O')) then
     begin
       try
         StrToInt(ObjParametroGlobal.Get_Valor);
         if ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrazoPagamento.LocalizaCodigo(ObjParametroGlobal.Get_Valor) then
          PRAZOPAGAMENTOPADRAO := ObjParametroGlobal.Get_Valor;
       except

       end;
     end;

     EdtCodigoProjetoPedido.Visible:=False;
     edtCodigoFerragem_PP.visible:=false;
     EdtCodigoPerfilado_PP.Visible:=False;
     EdtCodigoVidro_PP2.Visible:=false;
     EdtCodigoKitBox_PP.Visible:=False;
     EdtCodigoServico_PP.Visible:=False;
     EdtCodigoPersiana_PP.Visible:=False;

     {jonas}
     if (UtilizaNfe) then
     begin

      lbGerarNF.Caption:='Gerar NF-e';
      lbNFE.Caption:= 'NF-e:';
      lbGerarNF.OnClick:=self.onCliqueNFE;
      objTransmiteNFE:=TObjTransmiteNFE.Create(nil,FDataModulo.IBDatabase);

     end else
     begin
      lbNFE.Caption:='';
      lbGerarNF.Caption:='Gerar Nota Fiscal';
      lbGerarNF.OnClick:=self.onCliqueNF;

     end;
     {-}

    self.fmenunfe:=TFmenuNfe.Create(nil);
    self.fmenunfe.PassaObjeto(self.ObjPedidoObjetos.ObjNotaFiscalObjetos);
    FmenuNfe.objTransmitenfe := self.objTransmiteNFE;

   if(Tag<>0)
   then begin
         if(ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(IntToStr(Tag))=True)then
         begin
              ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
              self.ObjetoParaControles;
         end;

   end ;

   __VerificaProducaoPedido;

   btRomaneio.Visible:=False;
   Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrazoPagamento.Get_listaCodigo(comboprazocodigo.items);
   Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrazoPagamento.Get_listanome(comboprazonome.items);

  if(ObjParametroGlobal.ValidaParametro('OBSERVACAO PADRAO PEDIDO')=false)
    then observacaoPadrao := ''
    else observacaoPadrao := ObjParametroGlobal.Get_Valor;
end;


procedure TFPEDIDO.lbNomeProjetoPedidoClick(Sender: TObject);
Var
  FProjeto : TFProjeto;
  query:TIBQuery;
  parametro: string; {rodolfo}
begin
    if(EdtProjetoPedidoReferencia.text='')
    then Exit;

    parametro := StrReplaceRef(EdtProjetoPedidoReferencia.text);  {rodolfo}

    try
        FProjeto := TFProjeto.Create(nil);
        query:=TIBQuery.Create(nil);
       query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
          if(EdtProjetoPedidoReferencia.text='')
          then Exit;
          with query do
          begin
              Close;
              sql.Clear;
              //sql.Add('select codigo from tabProjeto where referencia='+#39+EdtProjetoPedidoReferencia.Text+#39);
              sql.Add('select codigo from tabProjeto where referencia='+#39+parametro+#39); {rodolfo}
              Open;
              FProjeto.Tag:=StrToInt(fieldbyname('codigo').AsString);
              FProjeto.ShowModal;
          end;

    finally
         FreeAndNil(FProjeto);
    end;


end;


procedure TFPEDIDO.lbnomearquitetoClick(Sender: TObject);
var
FArquiteto:TFArquiteto;
begin

     Try
        FArquiteto:=TFArquiteto.Create(nil);

     Except
           Messagedlg('Erro na tentativa de Criar o Cadastro de Arquiteto',mterror,[mbok],0);
           exit;
     End;

     try
          if(edtarquiteto.Text='')
          then Exit;
          FArquiteto.Tag:=StrToInt(edtarquiteto.Text);
          FArquiteto.ShowModal;
     finally
          FreeAndNil(FArquiteto);
     end;
End;


procedure TFPEDIDO.lbNomeFerragem_PPClick(Sender: TObject);
var
  Fferragem:TFFERRAGEM;
  Query:TIBQuery;
begin
  try
    Fferragem:=TFFERRAGEM.Create(nil);
    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;
  except

  end;

  try
    if(edtFerragemCor_PPReferenicia.text='')
    then Exit;
    with query do
    begin
        Close;
        sql.Clear;
        sql.Add('select * from tabferragem where referencia='+#39+edtFerragemCor_PPReferenicia.text+#39) ;
        Open;
        Fferragem.Tag:=StrToInt(fieldbyname('codigo').AsString);
        Fferragem.ShowModal;
    end
  finally
     FreeAndNil(Fferragem);
     FreeAndNil(Query);
  end;


end;


procedure TFPEDIDO.lbNomeKitBoxCor_PPClick(Sender: TObject);
var
  FKitbox:TFKITBOX;
  query:TIBQuery;
begin
  try
    FKitbox:=TFKITBOX.Create(nil);
    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;
  except

  end;

  try

    if(EdtKitBoxCor_PPReferencia.Text='')
    then Exit;

     with query do
     begin
        Close;
        sql.Clear;
        sql.Add('select * from tabkitbox where referencia='+#39+EdtKitBoxCor_PPReferencia.Text+#39) ;
        Open;
        FKitbox.Tag:=StrToInt(fieldbyname('codigo').AsString);
        FKitbox.ShowModal;
     end;

  finally
    FreeAndNil(FKitbox);
    FreeAndNil(Query);
  end;



end;


procedure TFPEDIDO.lbnomePerfiladoCor_PPClick(Sender: TObject);
var
  Fperfilado:TFPERFILADO;
  query:TIBQuery;
begin
    if(EdtPerfiladoCor_PPReferencia.Text='')
    then Exit;

    try
      Fperfilado:=TFPERFILADO.Create(nil);
      Query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
       with query do
       begin
            Close;
            sql.Clear;
            sql.Add('select * from tabperfilado where referencia='+#39+EdtPerfiladoCor_PPReferencia.Text+#39) ;
            Open;
            Fperfilado.Tag:=StrToInt(fieldbyname('codigo').AsString);
            Fperfilado.ShowModal;
       end;
    finally
       FreeAndNil(Fperfilado);
       FreeAndNil(Query);
    end;




end;


procedure TFPEDIDO.lbNomePersiana_PPClick(Sender: TObject);
var
  fpersiana:TFPERSIANA;
  query:TIBQuery;
begin
    if(EdtPersiana_PPReferencia.Text='')
    then Exit;

    try
      fpersiana:=TFPERSIANA.Create(nil);
      Query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
       with query do
       begin
            Close;
            sql.Clear;
            sql.Add('select * from tabpersiana where referencia='+#39+EdtPersiana_PPReferencia.Text+#39) ;
            Open;
            fpersiana.Tag:=StrToInt(fieldbyname('codigo').AsString);
            fpersiana.ShowModal
       end;
    finally
       FreeAndNil(fpersiana);
       FreeAndNil(Query);
    end;



end;


procedure TFPEDIDO.lbnomeServico_PPClick(Sender: TObject);
var
  Fservico:TFSERVICO;
  query:TIBQuery;
begin
     if(EdtServico_PPReferencia.Text='')
     then Exit;

     try
        Fservico:=TFservico.Create(nil);
        Query:=TIBQuery.Create(nil);
        Query.Database:=FDataModulo.IBDatabase;
     except

     end;

     try
         with query do
         begin
              Close;
              sql.Clear;
              sql.Add('select * from tabservico where referencia='+#39+EdtServico_PPReferencia.Text+#39) ;
              Open;
              Fservico.Tag:=StrToInt(fieldbyname('codigo').AsString);
              Fservico.ShowModal;
         end;
     finally
        FreeAndNil(Fservico);
        FreeAndNil(Query);
     end;


end;


procedure TFPEDIDO.lbNomeVidro_VLClick(Sender: TObject);
var
  Fvidro:TFVIDRO;
  query:TIBQuery;
begin

   if(edtvidro_VL.Text='') then exit;

   try
     Fvidro:=TFVIDRO.Create(nil);
     Query:=TIBQuery.Create(nil);
     Query.Database:=FDataModulo.IBDatabase;
   except

   end;

   try
      with query do
      begin
              Close;
              sql.Clear;
              sql.Add('select * from tabvidro where referencia='+#39+edtvidro_VL.Text+#39) ;
              Open;
              fvidro.Tag:=StrToInt(fieldbyname('codigo').AsString);
              Fvidro.ShowModal;
      end;

   finally
     FreeAndNil(Fvidro);
     FreeAndNil(Query);
   end;


end;


procedure TFPEDIDO.lbnomeDiverso_PPClick(Sender: TObject);
var
  Fdiverso:TFDIVERSO;
  query:TIBQuery;
begin
    if(edtDiverso_PPReferencia.Text='')
    then Exit;

    try
      Fdiverso:=TFDIVERSO.Create(nil);
      Query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
       with query do
       begin
              Close;
              sql.Clear;
              sql.Add('select * from tabdiverso where referencia='+#39+edtDiverso_PPReferencia.Text+#39) ;
              Open;
              fdiverso.tag:=StrToInt(fieldbyname('codigo').AsString);
              Fdiverso.ShowModal;
       end;
    finally
      FreeAndNil(Fdiverso);
      FreeAndNil(Query);
    end;


end;


procedure TFPEDIDO.lbNomeVidroCor_PP2Click(Sender: TObject);
var
  Fvidro:TFVIDRO;
  query:TIBQuery;
begin
   if(edtVidroCor_PPReferencia2.Text='') then exit;

   try
          Fvidro:=TFVIDRO.Create(nil);
          Query:=TIBQuery.Create(nil);
          Query.Database:=FDataModulo.IBDatabase;
   except

   end;

   try
        with query do
       begin
              Close;
              sql.Clear;
              sql.Add('select * from tabvidro where referencia='+#39+edtVidroCor_PPReferencia2.Text+#39) ;
              Open;
              fvidro.Tag:=StrToInt(fieldbyname('codigo').AsString);
              Fvidro.ShowModal;
       end;
   finally
          FreeAndNil(Fvidro);
          FreeAndNil(Query);
   end;


end;


procedure TFPEDIDO.lbNomeClienteClick(Sender: TObject);
var
  Fcliente:TFCLIENTE;
begin
   if(EdtCliente.Text='')
   then Exit;

   try
     fcliente:=TFCLIENTE.Create(nil);

   except

   end;

   try
      Fcliente.Tag:=StrToInt(EdtCliente.Text);
      Fcliente.ShowModal;

   finally
      FreeAndNil(Fcliente);
   end;


end;


procedure TFPEDIDO.lbNomeVendedorClick(Sender: TObject);
var
  Fvendedor:TFVENDEDOR;
begin

    if(EdtVendedor.Text='')
    then Exit;

    try
      Fvendedor:=TFVENDEDOR.Create(nil);
    except

    end;

    try
      Fvendedor.Tag:=StrToInt(EdtVendedor.Text) ;
      Fvendedor.ShowModal;
    finally
       FreeAndNil(Fvendedor);
    end;

end;


procedure TFPEDIDO.DBGridVidroVL_PPDblClick(Sender: TObject);
begin
     Exit;

     If (DbGridVidroVL_PP.DataSource.DataSet.Active=False)
     Then exit;

     If (DbGridVidroVL_PP.DataSource.DataSet.RecordCount=0)
     Then exit;


     limpaedit(pnl3);
     if (Self.ObjPedidoObjetos.ObjVidro_PP.LocalizaCodigo(DbGridVidroVL_PP.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataVidro_PP;
               exit;
     End;

     With Self.ObjPedidoObjetos.ObjVidro_PP do
     Begin
          TabelaparaObjeto;
          edtvidro_VL.Text:=Get_Codigo;

          edtvidro_VL.Text:=VidroCor.Vidro.Get_Referencia;
          lbNomeVidro_VL.Caption:=VidroCor.Vidro.Get_Descricao+' - '+VidroCor.Cor.Get_Descricao;
          edtquantidade_VL.Text:=Get_Quantidade;
          edtvalor_VL.Text:=(Get_Valor);
          edtlargura_VL.Text:=Get_Largura;
          edtaltura_VL.Text:=Get_Altura;
          edtcomplementovidro_VL.Text:=Get_Complemento;

     End;
end;


procedure TFPEDIDO.edtFerragemCor_PPRefereniciaDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
  FpesquisaLocal:Tfpesquisa;
begin
      Key:=VK_F9;
      Self.ObjPedidoObjetos.ObjFerragem_PP.EdtFerragemCorViewKeyDown(Sender,edtFerragemCor_PP,Key, Shift, lbNomeFerragem_PP);
end;


procedure TFPEDIDO.edtKitBoxCor_PPReferenciaDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
    key:=vk_f9;
    Self.ObjPedidoObjetos.ObjKitBox_PP.EdtKitBoxCorKeyDown(Sender,EdtKitBoxCor_PP, key, Shift, lbNomeKitBoxCor_PP);

end;


procedure TFPEDIDO.edtProjetoPedidoReferenciaDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
     key:=VK_F9;
     self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtProjetoKeyDown(Sender, EdtProjetoPedido, Key, Shift, lbNomeProjetoPedido);
end;


procedure TFPEDIDO.edtPerfiladoCor_PPReferenciaDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
     Key:=VK_F9;
     Self.ObjPedidoObjetos.ObjPerfilado_PP.EdtPerfiladoCorKeyDown(Sender, EdtPerfiladoCor_PP, Key, Shift, lbnomePerfiladoCor_PP);

end;


procedure TFPEDIDO.edtPersiana_PPReferenciaDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
     Key:=VK_F9;
     Self.ObjPedidoObjetos.ObjPersiana_PP.EdtPersianaGrupoDiametroCorKeyDown(Sender,EdtPersiana_PP,  Key, Shift, lbNomePersiana_PP);
end;


procedure TFPEDIDO.edtVidroCor_PPReferencia2DblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
  FpesquisaLocal:Tfpesquisa;
begin
    Key:=VK_f9;
    Self.ObjPedidoObjetos.ObjVidro_PP.EdtVidroCorKeyDown(Sender, EdtVidroCor_PP2, Key, Shift, lbNomeVidroCor_PP2);


end;


procedure TFPEDIDO.edtServico_PPReferenciaDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
     Key:=VK_F9;
     Self.ObjPedidoObjetos.ObjServico_PP.EdtServicoKeyDown(Sender, EdtServico_PP, key, Shift, lbnomeServico_PP);
end;


procedure TFPEDIDO.edtDiverso_PPReferenciaDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
     Key:=VK_F9;
     Self.ObjPedidoObjetos.ObjDiverso_PP.EdtDiversoKeyDown(Sender,EdtDiverso_PP,  key, shift, lbnomeDiverso_PP);
end;


procedure TFPEDIDO.edtvidro_VLDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
    key:=VK_F9;
    Self.ObjPedidoObjetos.ObjVidro_PP.EdtVidroCorKeyDown(Sender, Key, Shift, lbnomevidro_vl);
end;


procedure TFPEDIDO.edtarquitetoDblClick(Sender: TObject);
var
  Key:Word;
  FpesquisaLocal:Tfpesquisa;
  Farquiteto:TFARQUITETO;
begin

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Farquiteto:=TFARQUITETO.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabarquiteto','',Farquiteto)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                edtarquiteto.Text:=FpesquisaLocal.querypesq.fieldbyname('codigo').AsString;
                                lbnomearquiteto.Caption:=FpesquisaLocal.querypesq.fieldbyname('nome').AsString;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Farquiteto);
     End;
end;


procedure TFPEDIDO.edtClienteDblClick(Sender: TObject);
var
  Key:Word;
  FpesquisaLocal:Tfpesquisa;
  Fcliente:TFCLIENTE;
begin

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fcliente:=TFCLIENTE.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabcliente where ativo=''S'' ','',Fcliente)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                EdtCliente.Text:=FpesquisaLocal.querypesq.fieldbyname('codigo').AsString;
                                lbNomeCliente.Caption:=FpesquisaLocal.querypesq.fieldbyname('nome').AsString;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fcliente);
     End;
end;


procedure TFPEDIDO.edtVendedorDblClick(Sender: TObject);
var
  Key:Word;
  FpesquisaLocal:Tfpesquisa;
  Fvendedor:TFVENDEDOR;
begin

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fvendedor:=TFVENDEDOR.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabvendedor where ativo=''S'' ','',Fvendedor)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                EdtVendedor.Text:=FpesquisaLocal.querypesq.fieldbyname('codigo').AsString;
                                lbNomeVendedor.Caption:=FpesquisaLocal.querypesq.fieldbyname('nome').AsString;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fvendedor);
     End;
end;


procedure TFPEDIDO.edtCorFerragemProjetoPedidoReferenciaDblClick(
  Sender: TObject);
  var
  Key: Word;
  Shift: TShiftState ;
begin
        key:=VK_F9;
        Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorFerragemKeyDown(Sender,EdtCorFerragemProjetoPedido,  Key, Shift, lbNomeCorFerragemProjetoPedido,EdtProjetoPedido.text);
end;

procedure TFPEDIDO.GravaVendaNaTabVendas(pedido:string);
begin

  //GRAVAR AQUI A VENDA, OU SEJA, CONCLUIU UM PEDIDO, VENDEU, GRAVA NA TABVENDAS
  //CODIGO DO PEDIDO
  try

    Objvendas:=Tobjvendas.create;

    with Objvendas do
    begin

      state:=dsInsert;
      ZerarTabela;
      Submit_Codigo(Get_NovoCodigo);
      Submit_Pedido(pedido);

      if(Salvar(True)=False) then
      begin
        ShowMessage('erro durante a grava��o de um novo registro');
        sysUtils.Abort;
      end;

    end;

  finally
    Objvendas.free;
  end;


end;

procedure TFPEDIDO.ApagaVendaDaTabvendas(pedido:string);
var
  query:TIBQuery;
  codigo:string;
begin
    try
        try
          Objvendas:=Tobjvendas.create;
          query:=TIBQuery.Create(nil);

        except

        end;
        with query do
        begin
            Close;
            sql.Clear;
            sql.Add('select codigo from tabvendas where pedido='+#39+pedido+#39);
            Open;
            codigo:=fieldbyname('codigo').AsString;
        end;


        if(Objvendas.Exclui(codigo,True)= false)
        then begin
            ShowMessage('Erro ao estornar venda tabvendas');
            Exit;
        end;

    finally
        Objvendas.free;
    end;

end;

{ TODO : grava materiais venda (pedido) }
function TFPEDIDO.GravaMateriaisVendidos(pedido:string):Boolean;
Var
  Objquery:TIBQuery;
  ObjFerragem_ICMS:TobjFerragem_ICMS;
  ObjVidro_ICMS:TobjVidro_ICMS;
  ObjPerfilado_ICMS:TobjPerfilado_ICMS;
  ObjKitbox_ICMS:TobjKitbox_ICMS;
  ObjPersiana_ICMS:TobjPersiana_ICMS;
  ObjDiverso_ICMS:TobjDiverso_ICMS;
  ObjProjeto_ICMS:TObjPROJETO_ICMS;
  TipoCliente:string;
  UFCliente:string;
  ObjNotaFiscal:TObjNotafiscalObjetos;
  GravaProjetosNaMateriaisVenda:string;
  ProjetoEmBraco:string;
  CodigoProjetoBranco,percentualtributo:string;
  vtottrib:Currency;
  operacao:string;
  MateriaisVenda:TObjMateriaisVenda;

  pPERCENTUAL_ICMS_UF_DEST, pVALOR_ICMS_UF_DEST,
  pVALOR_ICMS_UF_REMET, pPERCENTUAL_ICMS_INTER,
  pPERCENTUAL_ICMS_INTER_PART:Currency;
begin
      //O OBJETIVO DESSA FUN��O � FAZER O MESMO Q TABPRODVENDAS DO AMANDA
      //OU SEJA, GRAVAR OS MATERIAS VENDIDOS EM CADA VENDA/PEDIDO, AO INVES DE TER APENAS O PROJETO REFERENTE
      //O INTERESSANTE � QUE NESSA FUN��O � FEITO TODOS OS CALCULOS DOS IMPOSTOS, ASSIM � GERANDO E GUARDADO
      //NESTA TABELA TODOS OS CALCULOS E VALORES DE IMPOSTOS REFERENTES A CADA MATERIAL DO PEDIDO

      //***************************************************************************************//
      //IMPORTANTE -> Jonatan Medina 27/08/2011
      //Altera��o feita a pedido da Metaljon... No caso deles, como eles s�o industria, quando vendem um projeto
      //na nota fiscal tem que sair a descri��o deste produto, e n�o como nas demais vidra�arias onde sai na nota
      //item a item de cada projeto (x de ferrages, x de vidros, etc), logo foi preciso fazer uma altera��o onde
      //efetue a venda e grava na materias alem dos materias o projeto vendido, ou seja, assim tenho o controle dos
      //materias que foram vendido e qual o projeto, tendo o projeto, na hora de gerar a nota fiscal/nfe, so mando a
      //descri��o do projeto e descosidero os materiais que comp�e esse projeto
      //� preciso frizar em um ponto... quando o parametro 'VENDER PROJETOS COMO PRODUTOS?' � preciso tomar cuidado na hora
      //de somar os valores vendidos e o que foi vendido, � preciso verificar se o parametro esta por venda de projetos ou n�o
      //se estiver, so verificar onde o campo "projeto"  na tabmateriaisvenda ou na viewprodutosvenda n�o � nulo
      //se n�o estiver � so verificar onde o campo "projeto" � nulo



      {  CODIFICA��O DOS MATERIAS

          Projeto=''
          Ferragem=1
          Perfilado=2
          vidro=3
          kitbox=4
          persiana=5
          diverso=6

      }


  result:=false;
  ProjetoEmBraco:='N';

  //Jonatan Medina 14/02/2012
  //Antes criavam todos os objetos de uma vez s�, mas isso esta deixando muito pesado
  //Ent�o � melhor criar um a um

   operacao := get_campoTabela('operacao','codigo','tabpedido',pedido);
   if operacao = ''then
    operacao := '1';


    Objquery:=TIBQuery.Create(nil);
    Objquery.Database:=FDataModulo.IBDatabase;
    MateriaisVenda:=TObjMateriaisVenda.create;
    ObjVidro_ICMS:=TobjVidro_ICMS.create(self);
    ObjFerragem_ICMS:=TobjFerragem_ICMS.create(self);
    ObjPerfilado_ICMS:=TobjPerfilado_ICMS.create(self);
    ObjKitbox_ICMS:=TobjKitbox_ICMS.create(self);
    ObjPersiana_ICMS:=TobjPersiana_ICMS.create(self);
    ObjDiverso_ICMS:=TobjDiverso_ICMS.create(self);
    ObjNotaFiscal:=TObjNotafiscalObjetos.Create(self);
    ObjProjeto_ICMS:=TObjPROJETO_ICMS.Create(self);

  try
    try
      //localizando dados do cliente
      ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(lbCodigoPedido.Caption);
      ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
      TipoCliente:=ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.tipocliente.Get_CODIGO;
      UFCliente:=ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Estado;
      FMostraBarraProgresso.ConfiguracoesIniciais(8,0);
      FMostraBarraProgresso.lbmensagem.caption:='Gravando Materiais da Venda';
      FMostraBarraProgresso.Lbmensagem2.caption:='Gravando Materiais da Venda';

      FMostraBarraProgresso.IncrementaBarra1(1);
      FMostraBarraProgresso.Show;
      FMostraBarraProgresso.IncrementaBarra1(1);
      FMostraBarraProgresso.Show;

      with Objquery do
      begin
        //Se a vidra�aria emite notas fiscais dos produtos acabados
        //os projetos em si, ent�o preciso gravar tambem na tabmateriasvenda os projetos e n�o
        //somentes os materiais avulsos...

        if(ObjParametroGlobal.ValidaParametro('VENDER PROJETOS COMO PRODUTOS')=false)then
        begin
          MensagemErro('Paramentro "VENDER PROJETOS COMO PRODUTOS" n�o encontrado');
          GravaProjetosNaMateriaisVenda:='N';
        end
        else GravaProjetosNaMateriaisVenda:=ObjParametroGlobal.Get_Valor;

        if(ObjParametroGlobal.ValidaParametro('CODIGO DO PROJETO EM BRANCO')=false)then
        begin
          MensagemErro('Paramentro "CODIGO DO PROJETO EM BRANCO" n�o encontrado');
          CodigoProjetoBranco:='1';
        end
        else CodigoProjetoBranco:=ObjParametroGlobal.Get_Valor;


        //S� vou gravar na Tabmateriaisvenda se o parametro estiver como sim
        //sen�o nein gravo, gravo somente os materias, como sempre foi
        if(GravaProjetosNaMateriaisVenda='SIM') or (GravaProjetosNaMateriaisVenda='S') then
        begin
          try
            Close;
            SQL.Clear;
            sql.Add('SELECT tabpedido_proj.*,tabprojeto.descricao,tabprojeto.referencia,tabprojeto.ncm,tabprojeto.cest FROM TABPEDIDO_PROJ' ) ;
            sql.Add('join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto');
            sql.Add('WHERE tabpedido_proj.pedido='+Pedido);

            Open;
            while not (Eof) do
            begin
              if(fieldbyname('PROJETO').asstring=CodigoProjetoBranco) then
                ProjetoEmBraco:='S'
              else
              begin
                with MateriaisVenda  do
                begin
                  state:=dsInsert;
                  ZerarTabela;
                  Submit_Codigo(Get_NovoCodigo);
                  Submit_Pedido(pedido);
                  Submit_Descricao(fieldbyname('descricao').asstring);

                  //PROJETO � VENDIDO COMO UNIDADE UN
                  Submit_Unidade('UN');
                  Submit_PROJETO(fieldbyname('PROJETO').AsString);
                  Submit_Vidro('');
                  Submit_Perfilado('');
                  Submit_Diverso('');
                  Submit_Componente('');
                  Submit_KitBox('');
                  Submit_Persiana('');
                  Submit_NCM(fieldbyname('NCM').AsString);
                  submit_cest(fieldbyname('cest').AsString);
                  //Submit_indpag('0');

                  if (ObjProjeto_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('PROJETO').AsString,operacao)=False) then
                  begin
                    //Se para alguma ferragem ainda n�o foram gravados os impostos, avisa qual ferragem �
                    MensagemErro('N�o foi encontrado o imposto de Origem para este Projeto, tipo de cliente e opera��o'+#13+
                                  'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('projeto').AsString);
                    exit;
                  end;
                  ObjProjeto_ICMS.TabelaparaObjeto;
                  Submit_MPOSTO_ICMS_ORIGEM(ObjProjeto_ICMS.IMPOSTO.Get_CODIGO);
                  Submit_IMPOSTO_IPI(ObjProjeto_ICMS.imposto_ipi.Get_CODIGO);
                  Submit_IMPOSTO_PIS_ORIGEM(ObjProjeto_ICMS.imposto_pis.Get_CODIGO);
                  Submit_IMPOSTO_COFINS_ORIGEM(ObjProjeto_ICMS.imposto_cofins.Get_CODIGO);


                  if(UFCliente=ESTADOSISTEMAGLOBAL) then
                    Submit_CFOP(ObjProjeto_ICMS.IMPOSTO.CFOP.Get_CODIGO)
                  else
                    Submit_CFOP(ObjProjeto_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

                  if (ObjProjeto_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('PROJETO').AsString,operacao)=False) then
                  begin
                    MensagemErro('N�o foi encontrado o imposto de Destino para este PROJETO, tipo de cliente e opera��o'+#13+
                      'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('PROJETO').AsString);
                    exit;
                  end;
                  ObjProjeto_ICMS.TabelaparaObjeto;
                  Submit_IMPOSTO_ICMS_DESTINO(ObjProjeto_ICMS.IMPOSTO.Get_CODIGO);
                  Submit_MPOSTO_PIS_DESTINO(ObjProjeto_ICMS.imposto_pis.Get_CODIGO);
                  Submit_IMPOSTO_COFINS_DESTINO(ObjProjeto_ICMS.imposto_cofins.Get_CODIGO);

                  Submit_ALIQUOTA(ObjProjeto_ICMS.IMPOSTO.Get_ALIQUOTA);
                  submit_percentualicms(ObjProjeto_ICMS.IMPOSTO.Get_ALIQUOTA);
                  Submit_REDUCAOBASECALCULO(ObjProjeto_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC);
                  submit_reducaobasecalculo_st(ObjProjeto_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC_ST);

                  Submit_ALIQUOTACUPOM('0');
                  Submit_PERCENTUALAGREGADO('0');
                  Submit_VALORPAUTA(ObjProjeto_ICMS.IMPOSTO.Get_PAUTA);
                  Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');


                  //S�o gravados com esses valores por default
                  //As bases de calculos e os valores de cada imposto
                  //S�o atualizados na fun��o "CalculaBasedeCalculoNovaForma"

                  Submit_VALORFRETE('0');
                  Submit_VALORSEGURO('0');

                  Submit_BC_ICMS('0');
                  Submit_VALOR_ICMS('0');
                  Submit_BC_ICMS_ST('0');
                  Submit_VALOR_ICMS_ST('0');
                  Submit_BC_IPI('0');
                  Submit_VALOR_IPI('0');
                  Submit_BC_PIS('0');
                  Submit_VALOR_PIS('0');
                  Submit_BC_PIS_ST('0');
                  Submit_VALOR_PIS_ST('0');
                  Submit_BC_COFINS('0');
                  Submit_VALOR_COFINS('0');
                  Submit_BC_COFINS_ST('0');
                  Submit_VALOR_COFINS_ST('0');

                  Submit_SITUACAOTRIBUTARIA_TABELAA(ObjProjeto_ICMS.IMPOSTO.STA.Get_CODIGO);

                  if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
                    submit_CSOSN(ObjProjeto_ICMS.IMPOSTO.CSOSN.Get_codigo)
                  else
                    Submit_SITUACAOTRIBUTARIA_TABELAB(ObjProjeto_ICMS.IMPOSTO.STB.Get_CODIGO);

                  if(ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjProjeto_ICMS.IMPOSTO.STB.Get_CODIGO='40') or (ObjProjeto_ICMS.IMPOSTO.STB.Get_CODIGO='41') then
                  begin
                    Submit_ISENTO('S');
                  end
                  else begin
                    Submit_ISENTO('N');
                  end;

                  if(ObjProjeto_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjProjeto_ICMS.IMPOSTO.STB.Get_CODIGO='10') or (ObjProjeto_ICMS.IMPOSTO.STB.Get_CODIGO='60') then
                  begin
                    Submit_SUBSTITUICAOTRIBUTARIA('S');
                  end
                  else begin
                    Submit_SUBSTITUICAOTRIBUTARIA('N');
                  end;

                  Submit_NotaFiscal('');
                  // Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);

                  //Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
                  //Aqui � projeto a projeto, mesmo que o mesmo seja dois
                  //N�o agrupo por projeto
                  //ver uma forma de agrupa-los
                  Submit_QUANTIDADE('1');
                  Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
                  {alterado por celio - ao gravar na materiaisvenda, devido
                  ao acrescimo no projeto, foi necess�rio enviar o valorfinal,
                  tendo sido testado nos descontos e vendas sem desconto e acrescimo
                  como somente a nfe utiliza esse campo n�o interferiu outros m�dulos
                  }
                  //Submit_ValorUnitario(fieldbyname('VALORTOTAL').AsString);
                  Submit_ValorUnitario(fieldbyname('valorfinal').AsString);
                  Submit_Referencia(fieldbyname('referencia').AsString);
                  //Submit_Cor(fieldbyname('cor').AsString);
                  // Submit_PesoUnitario(fieldbyname('peso').AsString);

                  //MATERIAL VAZIO QUANDO � UM PROJETO
                  Submit_Material('');


                  if (calculatotaltributos) then
                  begin
                       try
                          //25/03/2015 celio vai pegar do objprojeto_icms
                          //percentualtributo :=  ObjProjeto_ICMS.IMPOSTO.Get_PERCENTUALTRIBUTO;
                          percentualtributo :=  ObjProjeto_ICMS.Get_PERCENTUALTRIBUTO;
                          vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(fieldbyname('valorfinal').AsString);
                          Submit_percentualtributo(percentualtributo);
                          Submit_VTOTTRIB(CurrToStr(vtottrib));

                       except
                          on e:exception do
                          begin
                            ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                            Exit;
                          end
                       end
                  end;


                  if (Salvar(True)=False) then
                  begin
                    messagedlg('N�o foi poss�vel Gravar a PROJETO referente a PROJETO/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                    exit;
                  end;
                end;
              end;
              next;//pr�xima ferragem do pedido
            end;//While
            if(ProjetoEmBraco='S') then
              if not GravaMateriaisPedidoBranco(pedido,TipoCliente,UFCliente) then
                exit;


            FMostraBarraProgresso.IncrementaBarra1(1);
            FMostraBarraProgresso.Show;
          finally
            //ObjProjeto_ICMS.Free;
          end;
        end
        else
        begin
          try
            Close;
            SQL.Clear;
            SQL.Text := ' Select  f.descricao, f.unidade, f.codigo, f.ncm,f.cest, ' +
                        ' f.classificacaofiscal, c.Descricao as Cor, c.codigo as codigocor, f.referencia,  f.peso, '+
                        ' fp.valor,sum(fp.quantidade) as quantidade, sum(fp.valorfinal) as valorfinal '+
                        ' from TabFerragem_PP fp '+
                        ' inner join TabPedido_Proj pp on (pp.Codigo = fp.PedidoProjeto) '+
                        ' inner join TabPedido pd on (pd.Codigo = pp.Pedido) '+
                        ' inner join TabProjeto pj on (pj.Codigo = pp.Projeto) '+
                        ' inner join TabFerragemCor fc on (fc.Codigo = fp.FerragemCor) '+
                        ' inner join TabFerragem f on (f.Codigo = fc.Ferragem) '+
                        ' inner Join TabCor c on (c.Codigo = fc.Cor) '+
                        ' where pp.Pedido = :pedido '+
                        ' group by 1,2,3,4,5,6,7,8,9,10,11 ';
            Params[0].AsString := pedido;
            Open;
            while not(eof) do
            begin
              with MateriaisVenda  do
              begin
                state:=dsInsert;
                ZerarTabela;
                Submit_Codigo(Get_NovoCodigo);
                Submit_Pedido(pedido);
                Submit_Descricao(fieldbyname('descricao').asstring);
                Submit_Unidade(fieldbyname('unidade').AsString);
                Submit_Ferragem(fieldbyname('Codigo').AsString);
                Submit_Vidro('');
                Submit_Perfilado('');
                Submit_Diverso('');
                Submit_Componente('');
                Submit_KitBox('');
                Submit_Persiana('');
                Submit_NCM(fieldbyname('NCM').AsString);
                submit_cest(fieldbyname('cest').AsString);

                if (ObjFerragem_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) then
                begin

                  //Se para alguma ferragem ainda n�o foram gravados os impostos, avisa qual ferragem �
                  MensagemErro('N�o foi encontrado o imposto de Origem para esta Ferragem, tipo de cliente e opera��o'+#13+
                               'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                  exit;
                end;
                ObjFerragem_ICMS.TabelaparaObjeto;
                Submit_MPOSTO_ICMS_ORIGEM(ObjFerragem_ICMS.IMPOSTO.Get_CODIGO);
                Submit_IMPOSTO_IPI(ObjFerragem_ICMS.imposto_ipi.Get_CODIGO);
                Submit_IMPOSTO_PIS_ORIGEM(ObjFerragem_ICMS.imposto_pis.Get_CODIGO);
                Submit_IMPOSTO_COFINS_ORIGEM(ObjFerragem_ICMS.imposto_cofins.Get_CODIGO);

                if (UFCliente=ESTADOSISTEMAGLOBAL) then
                  Submit_CFOP(ObjFerragem_ICMS.IMPOSTO.CFOP.Get_CODIGO)
                else
                  Submit_CFOP(ObjFerragem_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

                if (ObjFerragem_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) then
                begin
                  MensagemErro('N�o foi encontrado o imposto de Destino para este Ferragem, tipo de cliente e opera��o'+#13+
                               'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                  exit;
                end;
                ObjFerragem_ICMS.TabelaparaObjeto;
                Submit_IMPOSTO_ICMS_DESTINO(ObjFerragem_ICMS.IMPOSTO.Get_CODIGO);
                Submit_MPOSTO_PIS_DESTINO(ObjFerragem_ICMS.imposto_pis.Get_CODIGO);
                Submit_IMPOSTO_COFINS_DESTINO(ObjFerragem_ICMS.imposto_cofins.Get_CODIGO);

                Submit_ALIQUOTA(ObjFerragem_ICMS.IMPOSTO.Get_ALIQUOTA);
                submit_percentualicms(ObjFerragem_ICMS.IMPOSTO.Get_ALIQUOTA);
                Submit_REDUCAOBASECALCULO(ObjFerragem_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC);
                submit_REDUCAOBASECALCULO_ST(ObjFerragem_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC_ST);
                Submit_ALIQUOTACUPOM('0');
                Submit_PERCENTUALAGREGADO('0');
                Submit_VALORPAUTA(ObjFerragem_ICMS.IMPOSTO.Get_PAUTA);
                Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');


                //S�o gravados com esses valores por default
                //As bases de calculos e os valores de cada imposto
                //S�o atualizados na fun��o "CalculaBasedeCalculoNovaForma"

                Submit_VALORFRETE('0');
                Submit_VALORSEGURO('0');

                Submit_BC_ICMS('0');
                Submit_VALOR_ICMS('0');
                Submit_BC_ICMS_ST('0');
                Submit_VALOR_ICMS_ST('0');
                Submit_BC_IPI('0');
                Submit_VALOR_IPI('0');
                Submit_BC_PIS('0');
                Submit_VALOR_PIS('0');
                Submit_BC_PIS_ST('0');
                Submit_VALOR_PIS_ST('0');
                Submit_BC_COFINS('0');
                Submit_VALOR_COFINS('0');
                Submit_BC_COFINS_ST('0');
                Submit_VALOR_COFINS_ST('0');

                Submit_SITUACAOTRIBUTARIA_TABELAA(ObjFerragem_ICMS.IMPOSTO.STA.Get_CODIGO);

                if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
                  submit_CSOSN(ObjFerragem_ICMS.IMPOSTO.CSOSN.Get_codigo)
                else
                  Submit_SITUACAOTRIBUTARIA_TABELAB(ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO);

                if(ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO='40') or (ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO='41') then
                begin
                  Submit_ISENTO('S');
                end
                else begin
                  Submit_ISENTO('N');
                end;

                if(ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO='10') or (ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO='60') then
                begin
                  Submit_SUBSTITUICAOTRIBUTARIA('S');
                end
                else begin
                  Submit_SUBSTITUICAOTRIBUTARIA('N');
                end;

                Submit_NotaFiscal('');
                Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);

                Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
                Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
                Submit_ValorUnitario(fieldbyname('valor').AsString);
                Submit_Referencia(fieldbyname('referencia').AsString);
                Submit_Cor(fieldbyname('cor').AsString);
                Submit_CodigoCor(fieldbyname('codigocor').AsString);
                Submit_PesoUnitario(fieldbyname('peso').AsString);
                Submit_Material('1');
                if (calculatotaltributos) then
                begin
                       try
                          //percentualtributo :=  ObjFerragem_ICMS.IMPOSTO.Get_PERCENTUALTRIBUTO;
                          percentualtributo :=  ObjFerragem_ICMS.Get_PERCENTUALTRIBUTO;
                          vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(fieldbyname('valorfinal').AsString);
                          Submit_percentualtributo(percentualtributo);
                          Submit_VTOTTRIB(CurrToStr(vtottrib));

                       except
                          on e:exception do
                          begin
                            ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                            Exit;
                          end
                       end
                end;

                if (Salvar(True)=False) then
                begin
                  messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                  exit;
                end;
              end;
              next;//pr�xima ferragem do pedido
            end;//While
            FMostraBarraProgresso.IncrementaBarra1(1);
            FMostraBarraProgresso.Show;
          finally
            //ObjFerragem_ICMS.Free;
          end;

          try
            //buscando todos os perfilados do projeto
            Close;
            SQL.Clear;
            SQL.Text := ' Select  f.descricao, f.unidade, f.codigo, f.ncm,f.cest, '+
                        ' f.classificacaofiscal, c.Descricao as Cor, c.codigo as codigocor, f.referencia, f.peso, '+
                        ' fp.valor,sum(fp.quantidade) as quantidade, sum(fp.valorfinal) as valorfinal '+
                        ' from tabperfilado_pp fp '+
                        ' inner join TabPedido_Proj pp on (pp.Codigo = fp.PedidoProjeto) '+
                        ' inner join TabPedido pd on (pd.Codigo = pp.Pedido) '+
                        ' inner join TabProjeto pj on (pj.Codigo = pp.Projeto) '+
                        ' inner join tabperfiladocor fc on (fc.Codigo = fp.perfiladocor) '+
                        ' inner join tabperfilado f on (f.Codigo = fc.perfilado) '+
                        ' inner Join TabCor c on (c.Codigo = fc.Cor) '+
                        ' Where pp.Pedido = :pedido '+
                        ' group by 1,2,3,4,5,6,7,8,9,10,11 ';

            Params[0].AsString := pedido;
            Open;

            while not(eof) do
            begin
              with MateriaisVenda  do
              begin
                state:=dsInsert;
                ZerarTabela;
                Submit_Codigo(Get_NovoCodigo);
                Submit_Pedido(pedido);
                Submit_Descricao(fieldbyname('descricao').asstring);
                Submit_Unidade(fieldbyname('unidade').AsString);
                Submit_Ferragem('');
                Submit_Vidro('');
                Submit_Perfilado(fieldbyname('Codigo').AsString);
                Submit_Diverso('');
                Submit_Componente('');
                Submit_KitBox('');
                Submit_Persiana('');
                Submit_NCM(fieldbyname('NCM').AsString);
                submit_cest(fieldbyname('CEST').AsString);
                {FAZER AQUI UM VERIFICA��O PARA VER SE ESTA CADASTRADO CORRETAMENTE OS IMPOSTOS PARA O MATERIAL}

                if (ObjPerfilado_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) then
                begin
                  MensagemErro('N�o foi encontrado o imposto de Origem para este Perfilado, tipo de cliente e opera��o'+#13+
                               'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                  exit;
                end;
                ObjPerfilado_ICMS.TabelaparaObjeto;
                Submit_MPOSTO_ICMS_ORIGEM(ObjPerfilado_ICMS.IMPOSTO.Get_CODIGO);
                Submit_IMPOSTO_IPI(ObjPerfilado_ICMS.imposto_ipi.Get_CODIGO);
                Submit_IMPOSTO_PIS_ORIGEM(ObjPerfilado_ICMS.imposto_pis.Get_CODIGO);
                Submit_IMPOSTO_COFINS_ORIGEM(ObjPerfilado_ICMS.imposto_cofins.Get_CODIGO);

                if(UFCliente=ESTADOSISTEMAGLOBAL) then
                  Submit_CFOP(ObjPerfilado_ICMS.IMPOSTO.CFOP.Get_CODIGO)
                else
                  Submit_CFOP(ObjPerfilado_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

                if (ObjPerfilado_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) then
                begin
                  MensagemErro('N�o foi encontrado o imposto de Origem para este Perfilado, tipo de cliente e opera��o'+#13+
                               'Estado: '+UFCliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                  exit;
                end;
                ObjPerfilado_ICMS.TabelaparaObjeto;
                Submit_IMPOSTO_ICMS_DESTINO(ObjPerfilado_ICMS.IMPOSTO.Get_CODIGO);
                Submit_MPOSTO_PIS_DESTINO(ObjPerfilado_ICMS.imposto_pis.Get_CODIGO);
                Submit_IMPOSTO_COFINS_DESTINO(ObjPerfilado_ICMS.imposto_cofins.Get_CODIGO);

                Submit_ALIQUOTA(ObjPerfilado_ICMS.IMPOSTO.Get_ALIQUOTA);
                submit_percentualicms(ObjPerfilado_ICMS.IMPOSTO.Get_ALIQUOTA);
                Submit_REDUCAOBASECALCULO(ObjPerfilado_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC);
                submit_reducaobasecalculo_st(ObjPerfilado_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC_ST);
                Submit_ALIQUOTACUPOM('0');
                Submit_PERCENTUALAGREGADO('0');
                Submit_VALORPAUTA(ObjPerfilado_ICMS.IMPOSTO.Get_PAUTA);
                Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');


                Submit_VALORFRETE('0');
                Submit_VALORSEGURO('0');
                Submit_BC_ICMS('0');
                Submit_VALOR_ICMS('0');
                Submit_BC_ICMS_ST('0');
                Submit_VALOR_ICMS_ST('0');
                Submit_BC_IPI('0');
                Submit_VALOR_IPI('0');
                Submit_BC_PIS('0');
                Submit_VALOR_PIS('0');
                Submit_BC_PIS_ST('0');
                Submit_VALOR_PIS_ST('0');
                Submit_BC_COFINS('0');
                Submit_VALOR_COFINS('0');
                Submit_BC_COFINS_ST('0');
                Submit_VALOR_COFINS_ST('0');

                Submit_SITUACAOTRIBUTARIA_TABELAA(ObjPerfilado_ICMS.IMPOSTO.STA.Get_CODIGO);

                if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
                  submit_CSOSN(ObjPerfilado_ICMS.IMPOSTO.CSOSN.Get_codigo)
                else
                  Submit_SITUACAOTRIBUTARIA_TABELAB(ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO);

                if(ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO='40') or (ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO='41') then
                begin
                  Submit_ISENTO('S');
                end
                else begin
                  Submit_ISENTO('N');
                end;

                if(ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO='10') or (ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO='60') then
                begin
                  Submit_SUBSTITUICAOTRIBUTARIA('S');
                end
                else begin
                  Submit_SUBSTITUICAOTRIBUTARIA('N');
                end;

                Submit_NotaFiscal('');
                Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);

                Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
                Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
                Submit_ValorUnitario(fieldbyname('valor').AsString);
                Submit_Referencia(fieldbyname('referencia').AsString);
                Submit_Material('2');
                Submit_CodigoCor(fieldbyname('codigocor').AsString);
                Submit_Cor(fieldbyname('cor').AsString);
                Submit_PesoUnitario(fieldbyname('peso').AsString);
                if (calculatotaltributos) then
                begin
                       try
                          //percentualtributo :=  ObjPerfilado_ICMS.IMPOSTO.Get_PERCENTUALTRIBUTO;
                          percentualtributo :=  ObjPerfilado_ICMS.Get_PERCENTUALTRIBUTO;
                          vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(fieldbyname('valorfinal').AsString);
                          Submit_percentualtributo(percentualtributo);
                          Submit_VTOTTRIB(CurrToStr(vtottrib));

                       except
                          on e:exception do
                          begin
                            ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                            Exit;
                          end
                       end
                end;

                if (Salvar(true)=False) then
                begin
                  messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                  exit;
                end;
              end;
              next;
            end;//While
            FMostraBarraProgresso.IncrementaBarra1(1);
            FMostraBarraProgresso.Show;
          finally
            //ObjPerfilado_ICMS.Free;
          end;

          try
            //buscando todos os vidros do projeto
            Close;
            SQL.Clear;
            SQL.Text := ' select  v.descricao, v.unidade, v.codigo, v.ncm,v.cest, '+
                        ' v.classificacaofiscal, c.Descricao as Cor, c.codigo as codigocor, v.referencia, '+
                        ' vp.valor,sum(vp.quantidade) as quantidade, sum(vp.valorfinal) as valorfinal, '+
                        ' sum(v.peso) as peso '+
                        ' from tabvidro_pp vp '+
                        ' inner join TabPedido_Proj pp on (pp.Codigo = vp.PedidoProjeto) '+
                        ' inner join TabPedido pd on (pd.Codigo = pp.Pedido) '+
                        ' inner join TabProjeto pj on (pj.Codigo = pp.Projeto) '+
                        ' inner join tabvidrocor vc on (vc.Codigo = vp.vidrocor) '+
                        ' inner join tabvidro v on (v.Codigo = vc.vidro) '+
                        ' inner Join TabCor c on (c.Codigo = vc.Cor) '+
                        ' where pp.Pedido = :pedido '+
                        ' group by 1,2,3,4,5,6,7,8,9,10';

            Params[0].AsString := pedido;
            Open;

            while not(eof) do
            begin
              with MateriaisVenda  do
              begin
                state:=dsInsert;
                ZerarTabela;
                Submit_Codigo(Get_NovoCodigo);
                Submit_Pedido(pedido);
                Submit_Descricao(fieldbyname('descricao').asstring);
                Submit_Unidade(fieldbyname('unidade').AsString);
                Submit_Ferragem('');
                Submit_Vidro(fieldbyname('Codigo').AsString);
                Submit_Perfilado('');
                Submit_Diverso('');
                Submit_Componente('');
                Submit_KitBox('');
                Submit_Persiana('');
                Submit_NCM(fieldbyname('NCM').AsString);
                Submit_CEST(fieldbyname('CEST').AsString);

                if (ObjVidro_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) then
                begin
                  //Se para alguma ferragem ainda n�o foram gravados os impostos, avisa qual ferragem �
                  MensagemErro('N�o foi encontrado o imposto de Origem para este Vidro, tipo de cliente e opera��o'+#13+
                               'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                  exit;
                end;
                ObjVidro_ICMS.TabelaparaObjeto;
                Submit_MPOSTO_ICMS_ORIGEM(ObjVidro_ICMS.IMPOSTO.Get_CODIGO);
                Submit_IMPOSTO_IPI(ObjVidro_ICMS.imposto_ipi.Get_CODIGO);
                Submit_IMPOSTO_PIS_ORIGEM(ObjVidro_ICMS.imposto_pis.Get_CODIGO);
                Submit_IMPOSTO_COFINS_ORIGEM(ObjVidro_ICMS.imposto_cofins.Get_CODIGO);

                if (UFCliente=ESTADOSISTEMAGLOBAL) then
                  Submit_CFOP(ObjVidro_ICMS.IMPOSTO.CFOP.Get_CODIGO)
                else
                  Submit_CFOP(ObjVidro_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

                if (ObjVidro_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) then
                begin
                  MensagemErro('N�o foi encontrado o imposto de Origem para este vidro, tipo de cliente e opera��o'+#13+
                               'Estado: '+UFCliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                  exit;
                end;
                ObjVidro_ICMS.TabelaparaObjeto;
                Submit_IMPOSTO_ICMS_DESTINO(ObjVidro_ICMS.IMPOSTO.Get_CODIGO);
                Submit_MPOSTO_PIS_DESTINO(ObjVidro_ICMS.imposto_pis.Get_CODIGO);
                Submit_IMPOSTO_COFINS_DESTINO(ObjVidro_ICMS.imposto_cofins.Get_CODIGO);

                Submit_ALIQUOTA(ObjVidro_ICMS.IMPOSTO.Get_ALIQUOTA);
                submit_percentualicms(ObjVidro_ICMS.IMPOSTO.Get_ALIQUOTA);
                Submit_REDUCAOBASECALCULO(ObjVidro_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC);
                submit_reducaobasecalculo_st(ObjVidro_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC_ST);
                Submit_ALIQUOTACUPOM('0');
                Submit_PERCENTUALAGREGADO('0');
                Submit_VALORPAUTA(ObjVidro_ICMS.IMPOSTO.Get_PAUTA);
                Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');

                Submit_VALORFRETE('0');
                Submit_VALORSEGURO('0');
                Submit_BC_ICMS('0');
                Submit_VALOR_ICMS('0');
                Submit_BC_ICMS_ST('0');
                Submit_VALOR_ICMS_ST('0');
                Submit_BC_IPI('0');
                Submit_VALOR_IPI('0');
                Submit_BC_PIS('0');
                Submit_VALOR_PIS('0');
                Submit_BC_PIS_ST('0');
                Submit_VALOR_PIS_ST('0');
                Submit_BC_COFINS('0');
                Submit_VALOR_COFINS('0');
                Submit_BC_COFINS_ST('0');
                Submit_VALOR_COFINS_ST('0');
                Submit_ALIQUOTACUPOM('0');

                Submit_SITUACAOTRIBUTARIA_TABELAA(ObjVidro_ICMS.IMPOSTO.STA.Get_CODIGO);

                if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
                  submit_CSOSN(ObjVidro_ICMS.IMPOSTO.CSOSN.Get_codigo)
                else
                Submit_SITUACAOTRIBUTARIA_TABELAB(ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO);

                if(ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO='40') or (ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO='41') then
                begin
                  Submit_ISENTO('S');
                end
                else begin
                  Submit_ISENTO('N');
                end;

                if (ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO='10') or (ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO='60') then
                begin
                  Submit_SUBSTITUICAOTRIBUTARIA('S');
                end
                else begin
                  Submit_SUBSTITUICAOTRIBUTARIA('N');
                end;

                Submit_NotaFiscal('');
                Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);


                Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
                Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
                Submit_ValorUnitario(fieldbyname('valor').AsString);
                Submit_Referencia(fieldbyname('referencia').AsString);
                Submit_Material('3');
                Submit_CodigoCor(fieldbyname('codigocor').AsString);
                Submit_Cor(fieldbyname('cor').AsString);
                Submit_PesoUnitario(fieldbyname('peso').AsString);
                if (calculatotaltributos) then
                begin
                       try
                          //percentualtributo :=  ObjVidro_ICMS.IMPOSTO.Get_PERCENTUALTRIBUTO;
                          percentualtributo :=  ObjVidro_ICMS.Get_PERCENTUALTRIBUTO;
                          vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(fieldbyname('valorfinal').AsString);
                          Submit_percentualtributo(percentualtributo);
                          Submit_VTOTTRIB(CurrToStr(vtottrib));

                       except
                          on e:exception do
                          begin
                            ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                            Exit;
                          end
                       end
                end;

                if (Salvar(true)=False) then
                begin
                  messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                  exit;
                end;
              end;
              next;
            end;//While
            FMostraBarraProgresso.IncrementaBarra1(1);
            FMostraBarraProgresso.Show;
          finally
            //ObjVidro_ICMS.Free;
          end;

          try
            //resgatando os kitbox desta pedido
            Close;
            SQL.Clear;
            SQL.Text := ' select  k.descricao,k.unidade,k.codigo,k.ncm,K.cest, '+
                        ' k.classificacaofiscal, c.Descricao as Cor, c.codigo as codigocor, k.referencia, k.peso, '+
                        ' kp.valor,sum(kp.quantidade) as quantidade, sum(kp.valorfinal) as valorfinal '+
                        ' from tabkitbox_pp kp '+
                        ' inner join TabPedido_Proj pp on (pp.Codigo = kp.PedidoProjeto) '+
                        ' inner join TabPedido pd on (pd.Codigo = pp.Pedido) '+
                        ' inner join TabProjeto pj on (pj.Codigo = pp.Projeto) '+
                        ' inner join tabkitboxcor kc on (kc.Codigo = kp.kitboxcor) '+
                        ' inner join tabkitbox k on (k.Codigo = kc.kitbox) '+
                        ' inner Join TabCor c on (c.Codigo = kc.Cor) '+
                        ' where pp.Pedido = :pedido '+
                        ' group by 1,2,3,4,5,6,7,8,9,10,11 ';

            Params[0].AsString := pedido;
            Open;
            while not(eof) do
            begin
              with MateriaisVenda  do
              begin
                state:=dsInsert;
                ZerarTabela;
                Submit_Codigo(Get_NovoCodigo);
                Submit_Pedido(pedido);
                Submit_Descricao(fieldbyname('descricao').asstring);
                Submit_Unidade(fieldbyname('unidade').AsString);
                Submit_Ferragem('');
                Submit_Vidro('');
                Submit_Perfilado('');
                Submit_Diverso('');
                Submit_Componente('');
                Submit_KitBox(fieldbyname('Codigo').AsString);
                Submit_Persiana('');
                Submit_NCM(fieldbyname('NCM').AsString);
                Submit_CEST(fieldbyname('CEST').AsString);

                if (ObjKitbox_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) then
                begin
                  //Se para alguma ferragem ainda n�o foram gravados os impostos, avisa qual ferragem �
                  MensagemErro('N�o foi encontrado o imposto de Origem para este KitBox, tipo de cliente e opera��o'+#13+
                               'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                  exit;
                end;
                ObjKitbox_ICMS.TabelaparaObjeto;
                Submit_MPOSTO_ICMS_ORIGEM(ObjKitbox_ICMS.IMPOSTO.Get_CODIGO);
                Submit_IMPOSTO_IPI(ObjKitbox_ICMS.imposto_ipi.Get_CODIGO);
                Submit_IMPOSTO_PIS_ORIGEM(ObjKitbox_ICMS.imposto_pis.Get_CODIGO);
                Submit_IMPOSTO_COFINS_ORIGEM(ObjKitbox_ICMS.imposto_cofins.Get_CODIGO);

                if (UFCliente=ESTADOSISTEMAGLOBAL) then
                  Submit_CFOP(ObjKitbox_ICMS.IMPOSTO.CFOP.Get_CODIGO)
                else
                  Submit_CFOP(ObjKitbox_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

                if (ObjKitbox_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) then
                begin
                  MensagemErro('N�o foi encontrado o imposto de Origem para este KitBox, tipo de cliente e opera��o'+#13+
                               'Estado: '+UFCliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                  exit;
                end;
                ObjKitbox_ICMS.TabelaparaObjeto;
                Submit_IMPOSTO_ICMS_DESTINO(ObjKitbox_ICMS.IMPOSTO.Get_CODIGO);
                Submit_MPOSTO_PIS_DESTINO(ObjKitbox_ICMS.imposto_pis.Get_CODIGO);
                Submit_IMPOSTO_COFINS_DESTINO(ObjKitbox_ICMS.imposto_cofins.Get_CODIGO);

                Submit_ALIQUOTA(ObjKitbox_ICMS.IMPOSTO.Get_ALIQUOTA);
                submit_percentualicms(ObjKitbox_ICMS.IMPOSTO.Get_ALIQUOTA);
                Submit_REDUCAOBASECALCULO(ObjKitbox_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC);
                submit_reducaobasecalculo_st(ObjKitbox_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC_ST);
                Submit_ALIQUOTACUPOM('0');
                Submit_PERCENTUALAGREGADO('0');
                Submit_VALORPAUTA(ObjKitbox_ICMS.IMPOSTO.Get_PAUTA);
                Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');

                Submit_VALORFRETE('0');
                Submit_VALORSEGURO('0');
                Submit_BC_ICMS('0');
                Submit_VALOR_ICMS('0');
                Submit_BC_ICMS_ST('0');
                Submit_VALOR_ICMS_ST('0');
                Submit_BC_IPI('0');
                Submit_VALOR_IPI('0');
                Submit_BC_PIS('0');
                Submit_VALOR_PIS('0');
                Submit_BC_PIS_ST('0');
                Submit_VALOR_PIS_ST('0');
                Submit_BC_COFINS('0');
                Submit_VALOR_COFINS('0');
                Submit_BC_COFINS_ST('0');
                Submit_VALOR_COFINS_ST('0');

                Submit_SITUACAOTRIBUTARIA_TABELAA(ObjKitbox_ICMS.IMPOSTO.STA.Get_CODIGO);

                if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
                  submit_CSOSN(ObjKitbox_ICMS.IMPOSTO.CSOSN.Get_codigo)
                else
                  Submit_SITUACAOTRIBUTARIA_TABELAB(ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO);

                if(ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO='40') or (ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO='41') then
                begin
                  Submit_ISENTO('S');
                end
                else begin
                  Submit_ISENTO('N');
                end;

                if (ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO='10') or (ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO='60') then
                begin
                  Submit_SUBSTITUICAOTRIBUTARIA('S');
                end
                else begin
                  Submit_SUBSTITUICAOTRIBUTARIA('N');
                end;

                Submit_NotaFiscal('');
                Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);

                Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
                Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
                Submit_ValorUnitario(fieldbyname('valor').AsString);
                Submit_Referencia(fieldbyname('referencia').AsString);
                Submit_Material('4');
                Submit_CodigoCor(fieldbyname('codigocor').AsString);
                Submit_Cor(fieldbyname('cor').AsString);
                Submit_PesoUnitario(fieldbyname('peso').AsString);
                if (calculatotaltributos) then
                begin
                       try
                          //percentualtributo :=  ObjKitbox_ICMS.IMPOSTO.Get_PERCENTUALTRIBUTO;
                          percentualtributo :=  ObjKitbox_ICMS.Get_PERCENTUALTRIBUTO;
                          vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(fieldbyname('valorfinal').AsString);
                          Submit_percentualtributo(percentualtributo);
                          Submit_VTOTTRIB(CurrToStr(vtottrib));

                       except
                          on e:exception do
                          begin
                            ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                            Exit;
                          end
                       end
                end;

                if (Salvar(true)=False) then
                begin
                  messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                  exit;
                end;
              end;
              next;
            end;//While
            FMostraBarraProgresso.IncrementaBarra1(1);
            FMostraBarraProgresso.Show;
          finally
            //ObjKitbox_ICMS.Free;
          end;
          try
            //resgatando as persianas
            Close;
            SQL.Clear;
            SQL.Text := ' Select  s.nome,s.codigo,s.ncm,s.cest, '+
                        ' s.classificacaofiscal, c.Descricao as Cor, c.codigo as codigocor, s.referencia, '+
                        ' sp.valor,sum(sp.quantidade) as quantidade, sum(sp.valorfinal) as valorfinal '+
                        ' from tabpersiana_pp sp '+
                        ' inner join TabPedido_Proj pp on (pp.Codigo = sp.PedidoProjeto) '+
                        ' inner join TabPedido pd on (pd.Codigo = pp.Pedido) '+
                        ' inner join tabpersianagrupodiametrocor sg on (sg.Codigo = sp.persianagrupodiametrocor) '+
                        ' inner join tabpersiana s on (s.Codigo = sg.persiana) '+
                        ' inner Join TabCor c on (c.Codigo = sg.Cor) '+
                        ' Where pp.Pedido = :pedido '+
                        ' group by 1,2,3,4,5,6,7,8,9 ';

            Params[0].AsString := pedido;
            Open;

            while not(eof) do
            begin
              with MateriaisVenda  do
              begin
                state:=dsInsert;
                ZerarTabela;
                Submit_Codigo(Get_NovoCodigo);
                Submit_Pedido(pedido);
                Submit_Descricao(fieldbyname('nome').asstring);
                Submit_Ferragem('');
                Submit_Vidro('');
                Submit_Perfilado('');
                Submit_Diverso('');
                Submit_Componente('');
                Submit_KitBox('');
                Submit_Persiana(fieldbyname('Codigo').AsString);
                Submit_NCM(fieldbyname('NCM').AsString);
                Submit_CEST(fieldbyname('CEST').AsString);

                if (ObjPersiana_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) then
                begin
                  MensagemErro('N�o foi encontrado o imposto de Origem para este Persiana, tipo de cliente e opera��o'+#13+
                               'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                  exit;
                end;
                ObjPersiana_ICMS.TabelaparaObjeto;
                Submit_MPOSTO_ICMS_ORIGEM(ObjPersiana_ICMS.IMPOSTO.Get_CODIGO);
                Submit_IMPOSTO_IPI(ObjPersiana_ICMS.imposto_ipi.Get_CODIGO);
                Submit_IMPOSTO_PIS_ORIGEM(ObjPersiana_ICMS.imposto_pis.Get_CODIGO);
                Submit_IMPOSTO_COFINS_ORIGEM(ObjPersiana_ICMS.imposto_cofins.Get_CODIGO);

                if (UFCliente=ESTADOSISTEMAGLOBAL) then
                  Submit_CFOP(ObjPersiana_ICMS.IMPOSTO.CFOP.Get_CODIGO)
                else
                  Submit_CFOP(ObjPersiana_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

                if (ObjPersiana_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) then
                begin
                  MensagemErro('N�o foi encontrado o imposto de Origem para este Persiana, tipo de cliente e opera��o'+#13+
                               'Estado: '+UFCliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                  exit;
                end;
                ObjPersiana_ICMS.TabelaparaObjeto;
                Submit_IMPOSTO_ICMS_DESTINO(ObjPersiana_ICMS.IMPOSTO.Get_CODIGO);
                Submit_MPOSTO_PIS_DESTINO(ObjPersiana_ICMS.imposto_pis.Get_CODIGO);
                Submit_IMPOSTO_COFINS_DESTINO(ObjPersiana_ICMS.imposto_cofins.Get_CODIGO);

                Submit_ALIQUOTA(ObjPersiana_ICMS.IMPOSTO.Get_ALIQUOTA);
                submit_percentualicms(ObjPersiana_ICMS.IMPOSTO.Get_ALIQUOTA);
                Submit_REDUCAOBASECALCULO(ObjPersiana_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC);
                submit_reducaobasecalculo_st(ObjPersiana_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC_ST);
                Submit_ALIQUOTACUPOM('0');
                Submit_PERCENTUALAGREGADO('0');
                Submit_VALORPAUTA(ObjPersiana_ICMS.IMPOSTO.Get_PAUTA);
                Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');


                Submit_VALORFRETE('0');
                Submit_VALORSEGURO('0');
                Submit_BC_ICMS('0');
                Submit_VALOR_ICMS('0');
                Submit_BC_ICMS_ST('0');
                Submit_VALOR_ICMS_ST('0');
                Submit_BC_IPI('0');
                Submit_VALOR_IPI('0');
                Submit_BC_PIS('0');
                Submit_VALOR_PIS('0');
                Submit_BC_PIS_ST('0');
                Submit_VALOR_PIS_ST('0');
                Submit_BC_COFINS('0');
                Submit_VALOR_COFINS('0');
                Submit_BC_COFINS_ST('0');
                Submit_VALOR_COFINS_ST('0');

                Submit_SITUACAOTRIBUTARIA_TABELAA(ObjPersiana_ICMS.IMPOSTO.STA.Get_CODIGO);

                if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
                  submit_CSOSN(ObjPersiana_ICMS.IMPOSTO.CSOSN.Get_codigo)
                else
                  Submit_SITUACAOTRIBUTARIA_TABELAB(ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO);

                if(ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO='40') or (ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO='41') then
                begin
                  Submit_ISENTO('S');
                end
                else begin
                  Submit_ISENTO('N');
                end;

                if(ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO='10') or (ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO='60') then
                begin
                  Submit_SUBSTITUICAOTRIBUTARIA('S');
                end
                else begin
                  Submit_SUBSTITUICAOTRIBUTARIA('N');
                end;

                Submit_NotaFiscal('');
                Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);

                Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
                Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
                Submit_ValorUnitario(fieldbyname('valor').AsString);
                Submit_Referencia(fieldbyname('referencia').AsString);
                Submit_Material('5');
                Submit_Cor(fieldbyname('cor').AsString);
                Submit_CodigoCor(fieldbyname('codigocor').AsString);
                Submit_PesoUnitario('0');
                Submit_Unidade('M�');
                if (calculatotaltributos) then
                begin
                       try
                          //percentualtributo :=  ObjPersiana_ICMS.IMPOSTO.Get_PERCENTUALTRIBUTO;
                          percentualtributo :=  ObjPersiana_ICMS.Get_PERCENTUALTRIBUTO;
                          vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(fieldbyname('valorfinal').AsString);
                          Submit_percentualtributo(percentualtributo);
                          Submit_VTOTTRIB(CurrToStr(vtottrib));

                       except
                          on e:exception do
                          begin
                            ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                            Exit;
                          end
                       end
                end;

                if (Salvar(true)=False) then
                begin
                  messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                  exit;
                end;
              end;
              next;
            end;//While
            FMostraBarraProgresso.IncrementaBarra1(1);
            FMostraBarraProgresso.Show;
          finally
            //ObjPersiana_ICMS.Free;
          end;
          try
            //Resgatando os Diversos
            Close;       
            SQL.Clear;
            SQL.Text := ' select d.descricao, d.unidade, d.codigo, d.ncm,d.cest, '+
                        ' d.classificacaofiscal, c.Descricao as Cor, c.codigo as codigocor, d.referencia, '+
                        ' dp.altura, dp.largura,dp.valor, '+
                        ' sum(dp.quantidade) as quantidade, sum(dp.valorfinal) as valorfinal '+
                        ' from tabdiverso_pp dp '+
                        ' inner join tabpedido_proj pp on (pp.codigo = dp.pedidoprojeto) '+
                        ' inner join tabpedido p on (p.codigo = pp.pedido) '+
                        ' inner join tabdiversocor dc on (dc.codigo = dp.diversocor) '+
                        ' inner join tabdiverso d on (d.codigo = dc.diverso) '+
                        ' inner join tabcor c on (c.codigo = dc.cor) '+
                        ' where pp.pedido = :pedido '+
                        ' group by 1,2,3,4,5,6,7,8,9,10,11,12 ';

            Params[0].AsString := pedido;
            Open;

            while not(eof) do
            begin
              with MateriaisVenda  do
              begin
                state:=dsInsert;
                ZerarTabela;
                Submit_Codigo(Get_NovoCodigo);
                Submit_Pedido(pedido);
                Submit_Descricao(fieldbyname('descricao').asstring);
                Submit_Unidade(fieldbyname('unidade').AsString);
                Submit_Ferragem('');
                Submit_Vidro('');
                Submit_Perfilado('');
                Submit_Diverso(fieldbyname('Codigo').AsString);
                Submit_Componente('');
                Submit_KitBox('');
                Submit_Persiana('');
                Submit_NCM(fieldbyname('NCM').AsString);
                Submit_CEST(fieldbyname('CEST').AsString);

                if (ObjDiverso_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) then
                begin
                  MensagemErro('N�o foi encontrado o imposto de Origem para este Diverso, tipo de cliente e opera��o'+#13+
                               'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                  exit;
                end;
                ObjDiverso_ICMS.TabelaparaObjeto;
                Submit_MPOSTO_ICMS_ORIGEM(ObjDiverso_ICMS.IMPOSTO.Get_CODIGO);
                Submit_IMPOSTO_IPI(ObjDiverso_ICMS.imposto_ipi.Get_CODIGO);
                Submit_IMPOSTO_PIS_ORIGEM(ObjDiverso_ICMS.imposto_pis.Get_CODIGO);
                Submit_IMPOSTO_COFINS_ORIGEM(ObjDiverso_ICMS.imposto_cofins.Get_CODIGO);

                if (UFCliente=ESTADOSISTEMAGLOBAL) then
                  Submit_CFOP(ObjDiverso_ICMS.IMPOSTO.CFOP.Get_CODIGO)
                else
                  Submit_CFOP(ObjDiverso_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

                if (ObjDiverso_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) then
                begin
                  MensagemErro('N�o foi encontrado o imposto de Origem para este Diverso, tipo de cliente e opera��o'+#13+
                               'Estado: '+UFCliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                  exit;
                end;
                ObjDiverso_ICMS.TabelaparaObjeto;
                Submit_IMPOSTO_ICMS_DESTINO(ObjDiverso_ICMS.IMPOSTO.Get_CODIGO);
                Submit_MPOSTO_PIS_DESTINO(ObjDiverso_ICMS.imposto_pis.Get_CODIGO);
                Submit_IMPOSTO_COFINS_DESTINO(ObjDiverso_ICMS.imposto_cofins.Get_CODIGO);

                Submit_ALIQUOTA(ObjDiverso_ICMS.IMPOSTO.Get_ALIQUOTA);
                submit_percentualicms(ObjDiverso_ICMS.IMPOSTO.Get_ALIQUOTA);
                Submit_REDUCAOBASECALCULO(ObjDiverso_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC);
                submit_reducaobasecalculo_st(ObjDiverso_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC_ST);
                Submit_ALIQUOTACUPOM('0');
                Submit_PERCENTUALAGREGADO('0');
                Submit_VALORPAUTA(ObjDiverso_ICMS.IMPOSTO.Get_PAUTA);
                Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');

                Submit_VALORFRETE('0');
                Submit_VALORSEGURO('0');
                Submit_BC_ICMS('0');
                Submit_VALOR_ICMS('0');
                Submit_BC_ICMS_ST('0');
                Submit_VALOR_ICMS_ST('0');
                Submit_BC_IPI('0');
                Submit_VALOR_IPI('0');
                Submit_BC_PIS('0');
                Submit_VALOR_PIS('0');
                Submit_BC_PIS_ST('0');
                Submit_VALOR_PIS_ST('0');
                Submit_BC_COFINS('0');
                Submit_VALOR_COFINS('0');
                Submit_BC_COFINS_ST('0');
                Submit_VALOR_COFINS_ST('0');

                Submit_SITUACAOTRIBUTARIA_TABELAA(ObjDiverso_ICMS.IMPOSTO.STA.Get_CODIGO);

                if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
                  submit_csosn(objdiverso_icms.IMPOSTO.CSOSN.Get_codigo)
                else
                  Submit_SITUACAOTRIBUTARIA_TABELAB(ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO);


                if(ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO='40') or (ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO='41') then
                begin
                  Submit_ISENTO('S');
                end
                else begin
                  Submit_ISENTO('N');
                end;

                if (ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO='10')or (ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO='60') then
                begin
                  Submit_SUBSTITUICAOTRIBUTARIA('S');
                end
                else begin
                  Submit_SUBSTITUICAOTRIBUTARIA('N');
                end;
                Submit_NotaFiscal('');
                Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);

                Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
                Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
                Submit_ValorUnitario(fieldbyname('valor').AsString);
                Submit_Referencia(fieldbyname('referencia').AsString);
                Submit_Material('6');
                Submit_CodigoCor(fieldbyname('codigocor').AsString);
                Submit_Cor(fieldbyname('cor').AsString);
                Submit_PesoUnitario('0');
                if (calculatotaltributos) then
                begin
                       try
                          //percentualtributo :=  ObjDiverso_ICMS.IMPOSTO.Get_PERCENTUALTRIBUTO;
                          percentualtributo :=  ObjDiverso_ICMS.Get_PERCENTUALTRIBUTO;
                          vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(fieldbyname('valorfinal').AsString);
                          Submit_percentualtributo(percentualtributo);
                          Submit_VTOTTRIB(CurrToStr(vtottrib));

                       except
                          on e:exception do
                          begin
                            ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                            Exit;
                          end
                       end
                end;

                if (Salvar(true)=False) then
                begin
                  messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                  exit;
                end;
              end;
              next;
            end;//While
            FMostraBarraProgresso.IncrementaBarra1(1);
            FMostraBarraProgresso.Show;
          finally
            //ObjDiverso_ICMS.Free;
          end;
        end;
      end;

      //acertando o acr�scimo efetuado nos projetos caso existam
      if((GravaProjetosNaMateriaisVenda='NAO') or (GravaProjetosNaMateriaisVenda='N')) then
      begin
          if not(ObjPedidoObjetos.AcertaAcrescimosMateriaisVenda(lbCodigoPedido.Caption)) then
            MensagemErro('N�o atualizou acr�scimos');
      end
      else
      begin
          if not(ObjPedidoObjetos.AcertaAcrescimosMateriaisVenda_projetospor_produtos(lbCodigoPedido.Caption,CodigoProjetoBranco)) then
            MensagemErro('N�o atualizou acr�scimos');
      end;


      // requisicao aqui
     ObjNotaFiscal.CalculaBaseDeCalculoNovaForma(lbCodigoPedido.Caption,edtvalordesconto.text);
      result:=true;
      FDataModulo.IBTransaction.CommitRetaining;
      FMostraBarraProgresso.BarradeProgresso.Progress:=8;
      FMostraBarraProgresso.Show;
    finally

      FMostraBarraProgresso.close;
      FreeAndNil(Objquery);

      if Assigned(MateriaisVenda) then
        MateriaisVenda.free;

      if Assigned(ObjNotaFiscal) then
        ObjNotaFiscal.Free;

      if Assigned(ObjFerragem_ICMS) then
        ObjFerragem_ICMS.Free;

      if Assigned(ObjVidro_ICMS) then
        ObjVidro_ICMS.Free;

      if Assigned(ObjPerfilado_ICMS) then
        ObjPerfilado_ICMS.Free;

      if Assigned(ObjKitbox_ICMS) then
        ObjKitbox_ICMS.Free;

      if Assigned(ObjPersiana_ICMS) then
        ObjPersiana_ICMS.Free;

      if Assigned(ObjDiverso_ICMS) then
        ObjDiverso_ICMS.Free;

      if Assigned(ObjProjeto_ICMS) then
        ObjProjeto_ICMS.Free;
    end;

  except
    on e:exception do
    begin
      MensagemErro('Erro GravaMateriaisVendidos. Msg: '+e.Message);
      raise;
    end;
  end;

  result:=true;
end;


procedure TFPEDIDO.onCliqueNF(Sender: TObject);
var
  ObjNotaFiscal:TObjNotafiscalObjetos;
  query:TIBQuery;
  Notas:TStringList;
  i:Integer;
  Corte:Integer;
begin


  try

    if (NFgerada=0) then
    begin
        if (ObjParametroGlobal.ValidaParametro('TRAVAR PARA ESTOQUE N�O FICAR NEGATIVO?')=False)
        then MensagemAviso('Parametro "TRAVAR PARA ESTOQUE N�O FICAR NEGATIVO?"');

        if (ObjParametroGlobal.Get_Valor='SIM') then
        begin
              if(ObjPedidoObjetos.VerficaEstoque(lbCodigoPedido.Caption)=False)
              then Exit;
        end;
       try

        ObjNotaFiscal:=TObjNotaFiscalObjetos.create(self);
        Notas:=TStringList.Create;
        
       except

       end;

       try

        query:=TIBQuery.Create(nil);
        query.Database:=FDataModulo.IBDatabase;
        
       except

       end;
       //Verifica se os produtos ja est�o gravados na tabmateriaisvenda...
       //temporario, pra solucionar o problema de vers�es anteriores a ves�o 218
       //sera retirado ou n�o... ver a possibilidade de manter
       with query do
       begin
              Close;
              sql.Clear;
              sql.Add('select codigo from tabmateriaisvenda');
              SQL.Add('where pedido ='+lbCodigoPedido.Caption);
              Open;
              Last;
              if(recordcount= 0)
              then GravaMateriaisVendidos(lbCodigoPedido.Caption);


       end;

       FescolheNF.create;
       FescolheNF.PegaNumPedido(lbCodigoPedido.caption);
       FescolheNF.edtEdtCfop.Text:='1';
       FescolheNF.edtDadosAdicionais.Text:='';
       FescolheNF.ShowModal;

       for i:=0 to FescolheNF.StrGridGrid.RowCount  do
       begin
            if FescolheNF.StrGridGrid.Cells[1,i+1]<>''
            then
            begin
                 Notas.Add(FescolheNF.StrGridGrid.Cells[1,i+1]) ;

            end;

       end;

       if (FescolheNF.edtnotafiscalatual.Text = '') then
       Exit;

       query.Close;
       query.sql.Clear;
       query.SQL.Add('select QUANTIDADEPRODUTOS from tabmodelonf where codigo_fiscal=''1''');
       query.Open;
       corte:= query.fieldbyname('quantidadeprodutos').asinteger;
       //Primeiro eu gero a quantidade de notas fiscais necessarias neste pedido
       if(ObjNotaFiscal.GeraNotaFiscalNova(Notas,lbCodigoPedido.Caption,FescolheNF.edtEdtCfop.Text,corte)) then
       begin
              with (query) do
              begin
                    //Preciso guardar os materias vendidos que foram gerados NF pra eles
                    //Procuro todas as notas deste pedido
                    close;
                    sql.Clear;
                    SQL.Add('select * from tabnotafiscal where numpedido='+lbCodigoPedido.Caption);
                    SQL.Add('and situacao=''I''');
                    Open;
                    Last;
                    FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
                    FMostraBarraProgresso.Lbmensagem.Caption:='Gerando Nota Fiscal';
                    FMostraBarraProgresso.Show;
                    First;
                    //Gravo os materiais que foram gerados NF pra eles
                    while not Eof do
                    begin
                        FMostraBarraProgresso.IncrementaBarra1(1);
                        FMostraBarraProgresso.Show;
                       { if(FescolheNF.edtDadosAdicionais.Text<>'') then
                        begin
                            ObjNotaFiscal.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(fieldbyname('codigo').AsString);
                            ObjNotaFiscal.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsEdit;
                            ObjNotaFiscal.ObjNotaFiscalCfop.NOTAFISCAL.Submit_DadosAdicionais(FescolheNF.edtDadosAdicionais.Text);
                            ObjNotaFiscal.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(True);

                        end;  }
                        
                        //Diminui estoque
                       if( ObjPedidoObjetos.DiminuiEstoquePedido(fieldbyname('codigo').AsString,lbCodigoPedido.Caption,DateToStr(Now))= False)then
                       begin
                            FDataModulo.IBTransaction.RollbackRetaining;
                            FMostraBarraProgresso.close;
                            Exit;
                       end;
                        //Grava produtos que foram gerados notas e comita
                        ObjNotaFiscal.GravaProdutosNF(lbCodigoPedido.Caption,fieldbyname('codigo').AsString);
                        //Imprime nota fiscal
                        ObjNotaFiscal.ImprimeNotaFiscal(fieldbyname('codigo').AsString);

                        lbGerarNF.Caption:='Reimprimir Nota Fiscal';
                        NFgerada:=1;
                        Next;
                    end;

              end;
              
              FDataModulo.IBTransaction.CommitRetaining;
              FMostraBarraProgresso.close;

       end
       else FDataModulo.IBTransaction.RollbackRetaining;

    end
    else
    begin

      if (NFgerada = 1) then
      begin

        try
          query:=TIBQuery.Create(nil);
          query.Database:=FDataModulo.IBDatabase;
        except

        end;

        try
          ObjNotaFiscal:=TObjNotaFiscalObjetos.create(self);
        except

        end;

        try

          with (query) do
          begin

                close;
                sql.Clear;
                SQL.Add('select * from tabnotafiscal where numpedido='+#39+lbCodigoPedido.Caption+#39);
                SQL.Add('and situacao=''I''');
                Open;
                Last;
                FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
                FMostraBarraProgresso.Lbmensagem.Caption:='Gerando Nota Fiscal';
                FMostraBarraProgresso.Show;
                First;
                if(FieldByName('modelo_nf').AsString='2')then
                begin
                    MensagemAviso('Esta NF foi gerada para uma NF-e');
                    Exit;
                end;
                while not Eof do
                begin
                    FMostraBarraProgresso.IncrementaBarra1(1);
                    FMostraBarraProgresso.Show;
                    ObjNotaFiscal.ImprimeNotaFiscal(fieldbyname('codigo').AsString);
                    lbGerarNF.Caption:='Reimprimir Nota Fiscal';
                    NFgerada:=1;
                    Next;
                end;
                FMostraBarraProgresso.close;

          end;

        finally

          FreeAndNil(query);

        end;

      end;

    end;

  finally
    ObjNotaFiscal.Free;
    Notas.Free;

  end;


end;

//ver as casas
procedure TFPEDIDO.onCliqueNFE(Sender: TObject);
var
  ObjNotaFiscal:TObjNotafiscalObjetos;
  findNFE,pnota,pCaminhoDanfe:string;
  strNota:TStringList;
  queryop:TIBQuery;
  operacao:string;
begin

  ObjNotaFiscal:=TObjNotafiscalObjetos.Create(self);
  queryop:=tibquery.Create(nil);
  queryop.Database:=FDataModulo.IBDatabase ;
  
  try

      if (NFgerada = 1) then
      begin

        queryop.close;
        queryop.sql.Clear;
        queryop.SQL.Add('select * from tabnotafiscal where numpedido='+#39+lbCodigoPedido.Caption+#39);
        queryop.SQL.Add('and situacao=''I''');
        queryop.Open;

        findNFE := queryop.fieldbyname('numero').asstring;

        if (findNFE <> '') then
        begin
          pCaminhoDanfe := get_campoTabela('ARQUIVO_XML','CODIGO','TABNFE',findNFE);
          objTransmiteNFE.imprimeDanfe(pCaminhoDanfe);
        end;

      end
      else
      begin

        if not (ObjParametroGlobal.ValidaParametro('TRAVAR PARA ESTOQUE N�O FICAR NEGATIVO?')) then
          MensagemAviso('Verifique o Parametro "TRAVAR PARA ESTOQUE N�O FICAR NEGATIVO?"');

        if (ObjParametroGlobal.Get_Valor='SIM') then
        begin
              if(ObjPedidoObjetos.VerficaEstoque(lbCodigoPedido.Caption)=False)
              then Exit;
        end; 
        try

          strNota:=TStringList.Create;
          pnota:=ObjNotaFiscal.get_proximaNFE();
          strNota.Add(pnota);

          if (pnota = '') then
          begin

            MensagemAviso('Verifique se h� modelo de nota fiscal eletr�nica cadastrado');
            Exit;

          end;

         if edtOperacao.text='' then
          operacao := '1'
         else
          operacao := edtOperacao.Text;

         if not (ObjNotaFiscal.GeraNotaFiscalParaNFE(strNota,lbCodigoPedido.Caption,operacao,999)) then
         begin

            MensagemErro('N�o foi possivel gerar NF-e');
            FDataModulo.IBTransaction.RollbackRetaining;

            //para teste
            //ObjNotaFiscal.GravaProdutosNF(lbCodigoPedido.Caption,strNota[0]);
            //FDataModulo.IBTransaction.CommitRetaining;

         end
         else
         begin

          ObjNotaFiscal.GravaProdutosNF(lbCodigoPedido.Caption,strNota[0]);

          ObjPedidoObjetos.DiminuiEstoquePedido(strNota[0],lbCodigoPedido.Caption,DateToStr(Now));
          MensagemSucesso('NF-e gerada com sucesso');
          FDataModulo.IBTransaction.CommitRetaining;

          with self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido do
          begin

            LocalizaCodigo(self.lbCodigoPedido.Caption);
            TabelaparaObjeto;
            ObjetoParaControles;

          end;
         end;

        finally

          FreeAndNil(strNota);

        end;

      end;

  finally

    ObjNotaFiscal.Free;
    freeandnil(queryop);
  end;

end;

procedure TFPEDIDO.lbNFEMouseLeave(Sender: TObject);
begin
    TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFPEDIDO.lbNFEMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFPEDIDO.lbNFEClick(Sender: TObject);
var
  formNFe:TFnotaFiscalEletronica;
begin


  if (lbNFE.Caption <> 'NF-e:') then
  begin

      formNFe:=TFnotaFiscalEletronica.Create(nil);

      try

        formNFe.Tag := StrToInt(RetornaSoNumeros(lbNFE.Caption));
        formNFe.ShowModal;

      finally

        FreeAndNil (formNFe);

      end;

  end;


end;

procedure TFPEDIDO.Consultarstatusdoservio1Click(Sender: TObject);
begin

  {consulta tervi�o}
  FmenuNfe.SpeedButton1Click(Sender);


end;

procedure TFPEDIDO.ConsultarstatusdaNFe1Click(Sender: TObject);
begin

  {consultar do arquivo}
  FmenuNfe.BitBtn3Click(Sender);

end;

procedure TFPEDIDO.AbrirmenuNFe1Click(Sender: TObject);
begin
  //self.fmenunfe.ShowModal;
  FmenuNfe.ShowModal;
end;

procedure TFPEDIDO.lbDataPedidoClick(Sender: TObject);
begin
      If(lbCodigoPedido.caption = '') or (ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Concluido='S')
      Then Exit;
      if (Fdata=nil)
      then Application.CreateForm(TFdata,Fdata);
      //resgatando nova data
      Fdata.edtdata.text:=lbDataPedido.caption;
      Fdata.Showmodal;
      if (Fdata.tag=1)
      Then lbDataPedido.caption:=Fdata.edtdata.text;
      FreeAndNil(fdata);


end;

procedure TFPEDIDO.edtlargura_VLExit(Sender: TObject);
var
  Plargura,paltura,pquantidade,plarguraarredondamento,palturaarredondamento:Currency;
begin
       if (EdtAltura_VL.Text <> '') and (EdtLargura_VL.Text <> '') then
       Begin
            Try
               paltura:=strtocurr(tira_ponto(edtaltura_VL.Text));
            Except
                  paltura:=0;
            End;

            Try
               plargura:=strtocurr(tira_ponto(edtlargura_VL.Text));
            Except
                plargura:=0;
            End;

            FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
            FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);

            pquantidade:=((plargura+plarguraarredondamento)*(paltura+palturaarredondamento))/1000000;
            edtquantidade_VL.Text:=formata_valor(CurrToStr(pquantidade));
       end
       Else edtquantidade_VL.Text:='0';
end;

procedure TFPEDIDO.lbReplicarPedidoMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
     TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;


procedure TFPEDIDO.GeraOrcamentoAutomatico(Descricao,arquiteto,cliente,vendedor:string;Sender: TObject;var CodidoPedido:string;Observacao:string);
begin
    self.ObjPedidoObjetos:=TObjPedidoObjetos.Create(self);
    try
       Self.limpaLabels;
       Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.status:=dsInsert;
       lbStatusPedido.Caption:='Gerar Venda';
       lbNumTitulo.Caption:='';
       lbCodigoPedido.Caption:='0' ;
       EdtDescricao.Text:=Descricao;
       edtarquiteto.Text:=arquiteto;
       EdtCliente.Text:=cliente;
       edtClienteExit(EdtCliente);
       EdtVendedor.Text:=vendedor;
       lbData.Caption:=DateToStr(Date);
       lbDataPedido.Caption:=DateToStr(Date);
       pgc1.TabIndex:=0;
       edtValorComissaoArquiteto.Text:='0';
       MemoObervacao.Text:=Observacao;

       AtualizavalorEditsPedido;
       btSalvarClick(Sender);
       CodidoPedido:=lbCodigoPedido.Caption;
    finally
       FreeAndNil(ObjPedidoObjetos);
    end;



end;

//JONATAN
//Op��o de replicar pedidos
procedure TFPEDIDO.btReplicarPedidoClick(Sender: TObject);
var
  Query:TIBQuery;
  PedidoAntigo:string;
  CodigoProjetoPedido:string;
  CodigoProjetoPedidoAntigo:string;
begin


        if(lbCodigoPedido.Caption='')
        then Exit;

        {if(VerificaPedidoConcluido = True)
        then Exit; }

        if (messagedlg('Certeza que deseja replicar esse pedido?',mtconfirmation,[mbyes,mbno],0)=mrno)
        then exit ;

        PedidoAntigo:='';
        CodigoProjetoPedido:='';
        CodigoProjetoPedidoAntigo:='';

        try
            Screen.Cursor := crHourGlass;
             try
                   Query:=TIBQuery.Create(nil);
                   Query.Database:=FDataModulo.IBDatabase;

                   Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.status:=dsInsert;
                   lbStatusPedido.Caption:='Gerar Venda';
                   lbNumTitulo.Caption:='';
                   //Adiciona ao fim da descri��o do pedido um idenficador de replida
                   EdtDescricao.text:=EdtDescricao.Text+' -Pedido '+lbCodigoPedido.Caption+' R�PLICA';
                   //guardando o codigo do pedido a ser replicado
                   PedidoAntigo:=lbCodigoPedido.Caption;
                   lbCodigoPedido.Caption:='0';
                   lbDataPedido.Caption:=DateToStr(now);
                   btSalvarClick(Sender);
                   with Query do
                   begin
                        //selecionando os projetos que o pedido a ser replicado tem...
                        Close;
                        sql.Clear;
                        sql.Add('select * from tabpedido_proj');
                        sql.Add('where pedido='+PedidoAntigo);
                        sql.Add('order by codigo');
                        Open;
                        while not Eof do
                        begin
                             //Gravando os projetos do pedido a ser replicado no pedido replica
                             with Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto do
                             begin
                                    LocalizaCodigo(fieldbyname('codigo').AsString);
                                    //TabelaparaObjeto;
                                    Status:=dsInsert;

                                    Submit_Codigo('0');
                                    Pedido.Submit_Codigo(lbCodigoPedido.caption);
                                    Projeto.Submit_Codigo(fieldbyname('projeto').AsString);
                                    CorFerragem.Submit_Codigo(fieldbyname('corferragem').AsString);
                                    CorPerfilado.Submit_Codigo(fieldbyname('corperfilado').AsString);
                                    CorVidro.Submit_Codigo(fieldbyname('corvidro').AsString);
                                    CorKitBox.Submit_Codigo(fieldbyname('corkitbox').AsString);
                                    CorDiverso.Submit_Codigo(fieldbyname('cordiverso').AsString);
                                    Submit_Local(fieldbyname('local').AsString);
                                    Submit_observacao(fieldbyname('observacao').AsString);
                                    Submit_TipoPedido(fieldbyname('tipopedido').AsString);
                                    Submit_ValorTotal    (fieldbyname('valortotal').AsString);
                                    Submit_ValorDesconto ('0');
                                    Submit_ValorAcrescimo(fieldbyname('valoracrescimo').AsString);

                                    if(Salvar(False)=false)then
                                    begin
                                          MensagemErro('Erro ao tentar replicar este pedido');
                                          btExcluirClick(Sender);
                                          Exit;
                                    end;
                                    CodigoProjetoPedido:=Get_Codigo;
                                    CodigoProjetoPedidoAntigo:=fieldbyname('codigo').AsString;

                                    if(GravaMateriaisPedidoReplicado(CodigoProjetoPedidoAntigo,CodigoProjetoPedido)=False)
                                    then Exit;

                                    FDataModulo.IBTransaction.CommitRetaining;
                                    Next;
                             end;
                        end;

                        if (Self.ObjPedidoObjetos.AtualizaValorPedido(lbCodigoPedido.caption)=false) then
                        Begin
                                       MensagemErro('N�o foi poss�vel Atualizar o Valor do Pedido');
                                       exit;
                        end;
                        FDataModulo.IBTransaction.CommitRetaining;
                        If Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.status<>dsinactive
                        then exit;

                        If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.LocalizaCodigo(lbCodigoPedido.caption)=False)
                        Then Begin
                          Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                          exit;
                        End;
                        Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.ZERARTABELA;
                        If (TabelaParaControles=False)
                        Then Begin
                          Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                          limpaedit(Self);
                          Self.limpaLabels;
                          exit;
                        End;

                        __VerificaProducaoPedido;



                   end;
             except
                   Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.status:=dsInactive;

             end;
        finally
            screen.Cursor:=crDefault;
              FreeAndNil(Query);
        end;



end;

function TFPEDIDO.GravaMateriaisPedidoReplicado(ProjetoPedidoantigo:string;ProjetoPedido:string):Boolean;
var
    query:TIBQuery;
begin
    try
        Result:=false;
        try
              query:=TIBQuery.Create(nil);
              query.Database:=FDataModulo.IBDatabase
        except

        end;
        with query do
        begin
              close;
              sql.Clear;
              sql.Add('select * from tabvidro_pp');
              sql.Add('where pedidoprojeto='+ProjetoPedidoantigo);
              Open;
              while not Eof do
              begin
                    with ObjPedidoObjetos.ObjVidro_PP do
                    begin
                         LocalizaCodigo(fieldbyname('codigo').asstring);
                         TabelaparaObjeto;
                         Status:=dsInsert;
                         Submit_Codigo('0');
                         Submit_PedidoProjeto(ProjetoPedido);
                         Salvar(True);
                    end;
                    Next;
              end;
              close;
              sql.Clear;
              sql.Add('select * from tabFERRAGEM_pp');
              sql.Add('where pedidoprojeto='+ProjetoPedidoantigo);
              Open;
              while not Eof do
              begin
                    with ObjPedidoObjetos.ObjFerragem_PP do
                    begin
                         LocalizaCodigo(fieldbyname('codigo').asstring);
                         TabelaparaObjeto;
                         Status:=dsInsert;
                         Submit_Codigo('0');
                         Submit_PedidoProjeto(ProjetoPedido);
                         Salvar(True);
                    end;
                    Next;
              end;
              close;
              sql.Clear;
              sql.Add('select * from tabpersiana_pp');
              sql.Add('where pedidoprojeto='+ProjetoPedidoantigo);
              Open;
              while not Eof do
              begin
                    with ObjPedidoObjetos.ObjPersiana_PP do
                    begin
                         LocalizaCodigo(fieldbyname('codigo').asstring);
                         TabelaparaObjeto;
                         Status:=dsInsert;
                         Submit_Codigo('0');
                         Submit_PedidoProjeto(ProjetoPedido);
                         Salvar(True);
                    end;
                    Next;
              end;
              close;
              sql.Clear;
              sql.Add('select * from tabperfilado_pp');
              sql.Add('where pedidoprojeto='+ProjetoPedidoantigo);
              Open;
              while not Eof do
              begin
                    with ObjPedidoObjetos.ObjPerfilado_PP do
                    begin
                         LocalizaCodigo(fieldbyname('codigo').asstring);
                         TabelaparaObjeto;
                         Status:=dsInsert;
                         Submit_Codigo('0');
                         Submit_PedidoProjeto(ProjetoPedido);
                         Salvar(True);
                    end;
                    Next;
              end;
              close;
              sql.Clear;
              sql.Add('select * from tabservico_pp');
              sql.Add('where pedidoprojeto='+ProjetoPedidoantigo);
              Open;
              while not Eof do
              begin
                    with ObjPedidoObjetos.ObjServico_PP do
                    begin
                         LocalizaCodigo(fieldbyname('codigo').asstring);
                         TabelaparaObjeto;
                         Status:=dsInsert;
                         Submit_Codigo('0');
                         Submit_PedidoProjeto(ProjetoPedido);
                         Salvar(True);
                    end;
                    Next;
              end;
              close;
              sql.Clear;
              sql.Add('select * from tabdiverso_pp');
              sql.Add('where pedidoprojeto='+ProjetoPedidoantigo);
              Open;
              while not Eof do
              begin
                    with ObjPedidoObjetos.ObjDiverso_PP do
                    begin
                         LocalizaCodigo(fieldbyname('codigo').asstring);
                         TabelaparaObjeto;
                         Status:=dsInsert;
                         Submit_Codigo('0');
                         Submit_PedidoProjeto(ProjetoPedido);
                         Salvar(True);
                    end;
                    Next;
              end;
              close;
              sql.Clear;
              sql.Add('select * from tabkitbox_pp');
              sql.Add('where pedidoprojeto='+ProjetoPedidoantigo);
              Open;
              while not Eof do
              begin
                    with ObjPedidoObjetos.ObjKitBox_PP do
                    begin
                         LocalizaCodigo(fieldbyname('codigo').asstring);
                         TabelaparaObjeto;
                         Status:=dsInsert;
                         Submit_Codigo('0');
                         Submit_PedidoProjeto(ProjetoPedido);
                         Salvar(True);
                    end;
                    Next;
              end;
              close;
              sql.Clear;
              sql.Add('select * from tabmedidas_proj');
              sql.Add('where pedidoprojeto='+ProjetoPedidoantigo);
              Open;
              while not Eof do
              begin
                    with ObjPedidoObjetos.ObjMedidas_Proj do
                    begin
                         LocalizaCodigo(fieldbyname('codigo').asstring);
                         TabelaparaObjeto;
                         Status:=dsInsert;
                         Submit_Codigo('0');
                         PedidoProjeto.Submit_Codigo(ProjetoPedido);
                         Salvar(True);
                    end;
                    Next;
              end;
              close;
              sql.Clear;
              sql.Add('select * from tabcomponente_pp');
              sql.Add('where pedidoprojeto='+ProjetoPedidoantigo);
              Open;
              while not Eof do
              begin
                    with ObjPedidoObjetos.ObjComponente_PP do
                    begin
                         LocalizaCodigo(fieldbyname('codigo').asstring);
                         TabelaparaObjeto;
                         Status:=dsInsert;
                         Submit_Codigo('0');
                         Submit_PedidoProjeto(ProjetoPedido);
                         Salvar(True);
                    end;
                    Next;
              end;

        end;

        result:=True;
    finally
        FreeAndNil(query);
    end;


end;

procedure TFPEDIDO.edtQuantidadeVidro_PP2Exit(Sender: TObject);
begin
  if (EdtCodigoProjetoPedido.Text='') then
  begin
    MensagemErro('Escolha primeiramente um pedido/projeto na Aba 02');
    exit;
  End;
  edtValorVidro_PP2.Text:=formata_valor(Self.ObjPedidoObjetos.RetornaValorVidro(EdtCodigoProjetoPedido.Text,edtVidroCor_PP2.Text));
  edtMaterial_PPExit( edtValorVidro_PP2 );
end;

procedure TFPEDIDO.lbNumOrdemServicoClick(Sender: TObject);
var
    FordemServico:TFOrdemServico;
begin

    { Try
        FordemServico:=TFOrdemServico.Create(nil);

     Except
           Messagedlg('Erro na tentativa de Criar o Cadastro de Arquiteto',mterror,[mbok],0);
           exit;
     End;

     try
          if(lbNumOrdemServico.Caption='')
          then Exit;
          FordemServico.Tag:=StrToInt(lbNumOrdemServico.Caption);
          FordemServico.ShowModal;
     finally
          FreeAndNil(FordemServico);
     end; }
End;

procedure TFPEDIDO.edtCorPerfiladoProjetoPedidoReferenciaDblClick(
  Sender: TObject);
var
  key:Word;
  Shift:TShiftState;
begin
     key:=VK_F9;
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorPerfiladoKeyDown(Sender,EdtCorPerfiladoProjetoPedido, Key, shift, lbNomeCorPerfiladoProjetoPedido,EdtProjetoPedido.text);
end;

procedure TFPEDIDO.edtCorVidroProjetoPedidoReferenciaDblClick(
  Sender: TObject);
var
  Key:Word;
  shift:TShiftState;
begin
      key:=VK_F9;
      Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorVidroKeyDown(Sender, EdtCorVidroProjetoPedido, Key, Shift, lbNomeCorVidroProjetoPedido,edtprojetopedido.text);
end;

procedure TFPEDIDO.edtCorKitBoxProjetoPedidoReferenciaDblClick(
  Sender: TObject);
var
  Key:Word;
  shift:TShiftState;
begin
      Key:=VK_F9;
      Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorKitBoxKeyDown(Sender, EdtCorKitBoxProjetoPedido, Key, Shift, lbNomeCorKitBoxProjetoPedido,edtprojetopedido.text);
end;

procedure TFPEDIDO.edtCorDiversoProjetoPedidoReferenciaDblClick(
  Sender: TObject);
var
  key:Word;
  shift:TShiftState;
begin
       key:=VK_F9;
       Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorDiversoKeyDown(Sender, EdtCorDiversoProjetoPedido, Key, Shift, lbNomeCorDiversoProjetoPedido,EdtProjetoPedido.text);
end;

function TFPEDIDO.GravaMateriaisPedidoBranco(pedido:string;TipoCliente:string;UFCliente:string):boolean;
Var
  Objquery:TIBQuery;
  ObjFerragem_ICMS:TobjFerragem_ICMS;
  ObjVidro_ICMS:TobjVidro_ICMS;
  ObjPerfilado_ICMS:TobjPerfilado_ICMS;
  ObjKitbox_ICMS:TobjKitbox_ICMS;
  ObjPersiana_ICMS:TobjPersiana_ICMS;
  ObjDiverso_ICMS:TobjDiverso_ICMS;
  ObjProjeto_ICMS:TObjPROJETO_ICMS;
  ObjNotaFiscal:TObjNotafiscalObjetos;
  ProjetoEmBraco:string;
  CodigoProjetoBranco,percentualtributo:string;
  vtottrib:Currency;
  operacao:string;
  MateriaisVenda:TObjMateriaisVenda;
begin
  result := false;

  Objquery:=TIBQuery.Create(nil);
  Objquery.Database:=FDataModulo.IBDatabase;
  MateriaisVenda:=TObjMateriaisVenda.create;
  ObjFerragem_ICMS:=TObjferragem_ICMS.Create(self);
  ObjVidro_ICMS:=TobjVidro_ICMS.create(self);
  ObjFerragem_ICMS:=TobjFerragem_ICMS.create(self);
  ObjPerfilado_ICMS:=TobjPerfilado_ICMS.create(self);
  ObjKitbox_ICMS:=TobjKitbox_ICMS.create(self);
  ObjPersiana_ICMS:=TobjPersiana_ICMS.create(self);
  ObjDiverso_ICMS:=TobjDiverso_ICMS.create(self);
  ObjNotaFiscal:=TObjNotafiscalObjetos.Create(self);
  ObjProjeto_ICMS:=TObjPROJETO_ICMS.Create(self);

   operacao := get_campoTabela('operacao','codigo','tabpedido',pedido);
   if operacao = ''then
    operacao := '1';

  try
    if(ObjParametroGlobal.ValidaParametro('CODIGO DO PROJETO EM BRANCO')=false) then
    begin
      MensagemErro('Paramentro "CODIGO DO PROJETO EM BRANCO" n�o encontrado');
      CodigoProjetoBranco:='1';
    end
    else
      CodigoProjetoBranco:=ObjParametroGlobal.Get_Valor;

    try
      with Objquery do
      begin
        try
          //GRAVANDO FERRAGENS::SE EXISTIREM
          Close;
          SQL.Clear;
          Sql.add('Select  Tabferragem.*,');
          Sql.add('TabCor.Descricao as Cor,');
          Sql.add('TabFerragem_PP.Quantidade,');
          Sql.add('TabFerragem_PP.Valor,TAbFerragem_PP.ValorFinal,');
          Sql.add('TabPedido_proj.Codigo as PedidoProjeto,');
          Sql.Add('TabFerragem_PP.Codigo as codigoPP,tabcor.codigo as codigocor');
          Sql.add('from TabFerragem_PP');
          Sql.add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabFerragem_PP.PedidoProjeto');
          Sql.add('Join TabPedido on TabPedido.Codigo = TabPedido_Proj.Pedido');
          Sql.add('Join TabProjeto on TabProjeto.Codigo = TabPedido_Proj.Projeto');
          Sql.add('Join TabFerragemCor on TabFerragemCor.Codigo = TabFerragem_PP.FerragemCor');
          Sql.add('join TabFerragem  on TabFerragem.Codigo = TabFerragemCor.Ferragem');
          Sql.add('Join TabCor on TabCor.Codigo = TabFerragemCor.Cor');
          Sql.add('Where TabPedido_Proj.Pedido = '+Pedido);
          sql.add('and tabpedido_proj.projeto='+CodigoProjetoBranco);
          open;

          while not(eof) do
          Begin
            With MateriaisVenda  do
            Begin
              state:=dsInsert;
              ZerarTabela;
              Submit_Codigo(Get_NovoCodigo);
              Submit_Pedido(pedido);
              Submit_Descricao(fieldbyname('descricao').asstring);
              Submit_Unidade(fieldbyname('unidade').AsString);
              Submit_Ferragem(fieldbyname('Codigo').AsString);
              Submit_Vidro('');
              Submit_Perfilado('');
              Submit_Diverso('');
              Submit_Componente('');
              Submit_KitBox('');
              Submit_Persiana('');
              Submit_NCM(fieldbyname('NCM').AsString);
              submit_cest(fieldbyname('cest').AsString);

              If (ObjFerragem_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) Then
              Begin

                //Se para alguma ferragem ainda n�o foram gravados os impostos, avisa qual ferragem �
                MensagemErro('N�o foi encontrado o imposto de Origem para esta Ferragem, tipo de cliente e opera��o'+#13+
                            'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                exit;
              End;
              ObjFerragem_ICMS.TabelaparaObjeto;
              Submit_MPOSTO_ICMS_ORIGEM(ObjFerragem_ICMS.IMPOSTO.Get_CODIGO);
              Submit_IMPOSTO_IPI(ObjFerragem_ICMS.imposto_ipi.Get_CODIGO);
              Submit_IMPOSTO_PIS_ORIGEM(ObjFerragem_ICMS.imposto_pis.Get_CODIGO);
              Submit_IMPOSTO_COFINS_ORIGEM(ObjFerragem_ICMS.imposto_cofins.Get_CODIGO);
              submit_CSOSN(ObjFerragem_ICMS.IMPOSTO.CSOSN.Get_codigo);

              if(UFCliente=ESTADOSISTEMAGLOBAL) then
                Submit_CFOP(ObjFerragem_ICMS.IMPOSTO.CFOP.Get_CODIGO)
              else
                Submit_CFOP(ObjFerragem_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

              if (ObjFerragem_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) Then
              Begin
                MensagemErro('N�o foi encontrado o imposto de Destino para este Ferragem, tipo de cliente e opera��o'+#13+
                  'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                exit;
              End;
              ObjFerragem_ICMS.TabelaparaObjeto;
              Submit_IMPOSTO_ICMS_DESTINO(ObjFerragem_ICMS.IMPOSTO.Get_CODIGO);
              Submit_MPOSTO_PIS_DESTINO(ObjFerragem_ICMS.imposto_pis.Get_CODIGO);
              Submit_IMPOSTO_COFINS_DESTINO(ObjFerragem_ICMS.imposto_cofins.Get_CODIGO);

              Submit_ALIQUOTA(ObjFerragem_ICMS.IMPOSTO.Get_ALIQUOTA);
              Submit_REDUCAOBASECALCULO(ObjFerragem_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC);
              submit_reducaobasecalculo_st(ObjFerragem_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC_ST);
              Submit_ALIQUOTACUPOM('0');
              Submit_PERCENTUALAGREGADO('0');
              Submit_VALORPAUTA(ObjFerragem_ICMS.IMPOSTO.Get_PAUTA);
              Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');


                             //S�o gravados com esses valores por default
                             //As bases de calculos e os valores de cada imposto
                             //S�o atualizados na fun��o "CalculaBasedeCalculoNovaForma"

              Submit_VALORFRETE('0');
              Submit_VALORSEGURO('0');

              Submit_BC_ICMS('0');
              Submit_VALOR_ICMS('0');
              Submit_BC_ICMS_ST('0');
              Submit_VALOR_ICMS_ST('0');
              Submit_BC_IPI('0');
              Submit_VALOR_IPI('0');
              Submit_BC_PIS('0');
              Submit_VALOR_PIS('0');
              Submit_BC_PIS_ST('0');
              Submit_VALOR_PIS_ST('0');
              Submit_BC_COFINS('0');
              Submit_VALOR_COFINS('0');
              Submit_BC_COFINS_ST('0');
              Submit_VALOR_COFINS_ST('0');

              Submit_SITUACAOTRIBUTARIA_TABELAA(ObjFerragem_ICMS.IMPOSTO.STA.Get_CODIGO);
              //Submit_SITUACAOTRIBUTARIA_TABELAB(ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO);
              if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
                submit_CSOSN(ObjFerragem_ICMS.IMPOSTO.CSOSN.Get_codigo)
              else
                Submit_SITUACAOTRIBUTARIA_TABELAB(ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO);

              if(ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO='40') or (ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO='41') then
                Submit_ISENTO('S')
              else
                Submit_ISENTO('N');

              if(ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO='10') or (ObjFerragem_ICMS.IMPOSTO.STB.Get_CODIGO='60') then
                Submit_SUBSTITUICAOTRIBUTARIA('S')
              else
                Submit_SUBSTITUICAOTRIBUTARIA('N');

              Submit_NotaFiscal('');
              Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);

              Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
              Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
              Submit_ValorUnitario(fieldbyname('valor').AsString);
              Submit_Referencia(fieldbyname('referencia').AsString);
              Submit_Cor(fieldbyname('cor').AsString);
              Submit_PesoUnitario(fieldbyname('peso').AsString);
              Submit_Material('1');
              Submit_CodigoCor(fieldbyname('codigocor').AsString);

              if (calculatotaltributos) then
              begin
                try
                  //percentualtributo :=  ObjFerragem_ICMS.IMPOSTO.Get_PERCENTUALTRIBUTO;
                  percentualtributo :=  ObjFerragem_ICMS.Get_PERCENTUALTRIBUTO;
                  vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(fieldbyname('valorfinal').AsString);
                  Submit_percentualtributo(percentualtributo);
                  Submit_VTOTTRIB(CurrToStr(vtottrib));

                except
                  on e:exception do
                  begin
                    ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                    Exit;
                  end
                end
              end;

              if (Salvar(True)=False)
              Then Begin
                  messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                  exit;
              End;
            End;  
            next;//pr�xima ferragem do pedido
          End;//While
          FMostraBarraProgresso.IncrementaBarra1(1);
          FMostraBarraProgresso.Show;
        finally
                ObjFerragem_ICMS.Free;
        end;

        try

          //buscando todos os perfilados do projeto
          Close;
          SQL.Clear;
          Sql.add('Select TabPerfilado.*,');
          Sql.add('TabCor.Descricao as Cor,');
          Sql.add('TabPerfilado_PP.Quantidade,');
          Sql.add('TabPerfilado_PP.Valor,');
          Sql.add('TabPerfilado_PP.ValorFinal,TabPedido_Proj.Codigo as PedidoProjeto,');
          Sql.Add('TabPerfilado_PP.Codigo as codigoPP, tabcor.codigo as codigocor');
          Sql.add('from TabPerfilado_PP');
          Sql.add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabPerfilado_PP.PedidoProjeto');
          Sql.add('Join TabPedido on TabPedido.Codigo = TabPedido_Proj.Pedido');
          Sql.add('Join TabProjeto on TabProjeto.Codigo = TabPedido_Proj.Projeto');
          Sql.add('Join TabPerfiladoCor on TabPerfiladoCor.Codigo = TabPerfilado_PP.PerfiladoCor');
          Sql.add('join TabPerfilado  on TabPerfilado.Codigo = TabPerfiladoCor.Perfilado');
          Sql.add('Join TabCor on TabCor.Codigo = TabPerfiladoCor.Cor');
          Sql.add('Where TabPedido_Proj.Pedido = '+Pedido);
          sql.add('and tabpedido_proj.projeto='+CodigoProjetoBranco);
          Open;
          while not(eof) do
          Begin
            With MateriaisVenda  do
            Begin
              state:=dsInsert;
              ZerarTabela;
              Submit_Codigo(Get_NovoCodigo);
              Submit_Pedido(pedido);
              Submit_Descricao(fieldbyname('descricao').asstring);
              Submit_Unidade(fieldbyname('unidade').AsString);
              Submit_Ferragem('');
              Submit_Vidro('');
              Submit_Perfilado(fieldbyname('Codigo').AsString);
              Submit_Diverso('');
              Submit_Componente('');
              Submit_KitBox('');
              Submit_Persiana('');
              Submit_NCM(fieldbyname('NCM').AsString);
              submit_cest(fieldbyname('cest').AsString);
              {FAZER AQUI UM VERIFICA��O PARA VER SE ESTA CADASTRADO CORRETAMENTE OS IMPOSTOS PARA O MATERIAL}

              If (ObjPerfilado_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) Then
              Begin
                MensagemErro('N�o foi encontrado o imposto de Origem para este Perfilado, tipo de cliente e opera��o'+#13+
                  'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                exit;
              End;
              ObjPerfilado_ICMS.TabelaparaObjeto;
              Submit_MPOSTO_ICMS_ORIGEM(ObjPerfilado_ICMS.IMPOSTO.Get_CODIGO);
              Submit_IMPOSTO_IPI(ObjPerfilado_ICMS.imposto_ipi.Get_CODIGO);
              Submit_IMPOSTO_PIS_ORIGEM(ObjPerfilado_ICMS.imposto_pis.Get_CODIGO);
              Submit_IMPOSTO_COFINS_ORIGEM(ObjPerfilado_ICMS.imposto_cofins.Get_CODIGO);
              submit_CSOSN(ObjPerfilado_ICMS.IMPOSTO.CSOSN.Get_codigo);

              if(UFCliente=ESTADOSISTEMAGLOBAL) then
                Submit_CFOP(ObjPerfilado_ICMS.IMPOSTO.CFOP.Get_CODIGO)
              else
                Submit_CFOP(ObjPerfilado_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

              if (ObjPerfilado_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) Then
              Begin
                MensagemErro('N�o foi encontrado o imposto de Origem para este Perfilado, tipo de cliente e opera��o'+#13+
                                    'Estado: '+UFCliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                exit;
              End;
              ObjPerfilado_ICMS.TabelaparaObjeto;
              Submit_IMPOSTO_ICMS_DESTINO(ObjPerfilado_ICMS.IMPOSTO.Get_CODIGO);
              Submit_MPOSTO_PIS_DESTINO(ObjPerfilado_ICMS.imposto_pis.Get_CODIGO);
              Submit_IMPOSTO_COFINS_DESTINO(ObjPerfilado_ICMS.imposto_cofins.Get_CODIGO);

              Submit_ALIQUOTA(ObjPerfilado_ICMS.IMPOSTO.Get_ALIQUOTA);
              Submit_REDUCAOBASECALCULO(ObjPerfilado_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC);
              submit_reducaobasecalculo_st(ObjPerfilado_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC_ST);
              Submit_ALIQUOTACUPOM('0');
              Submit_PERCENTUALAGREGADO('0');
              Submit_VALORPAUTA(ObjPerfilado_ICMS.IMPOSTO.Get_PAUTA);
              Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');


              Submit_VALORFRETE('0');
              Submit_VALORSEGURO('0');
              Submit_BC_ICMS('0');
              Submit_VALOR_ICMS('0');
              Submit_BC_ICMS_ST('0');
              Submit_VALOR_ICMS_ST('0');
              Submit_BC_IPI('0');
              Submit_VALOR_IPI('0');
              Submit_BC_PIS('0');
              Submit_VALOR_PIS('0');
              Submit_BC_PIS_ST('0');
              Submit_VALOR_PIS_ST('0');
              Submit_BC_COFINS('0');
              Submit_VALOR_COFINS('0');
              Submit_BC_COFINS_ST('0');
              Submit_VALOR_COFINS_ST('0');

              Submit_SITUACAOTRIBUTARIA_TABELAA(ObjPerfilado_ICMS.IMPOSTO.STA.Get_CODIGO);
              //Submit_SITUACAOTRIBUTARIA_TABELAB(ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO);
              if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
                submit_CSOSN(ObjPerfilado_ICMS.IMPOSTO.CSOSN.Get_codigo)
              else
                Submit_SITUACAOTRIBUTARIA_TABELAB(ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO);

              if(ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO='40') or (ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO='41') then
                Submit_ISENTO('S')
              else
                Submit_ISENTO('N');

              if(ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO='10') or (ObjPerfilado_ICMS.IMPOSTO.STB.Get_CODIGO='60') then
                 Submit_SUBSTITUICAOTRIBUTARIA('S')
              else
                 Submit_SUBSTITUICAOTRIBUTARIA('N');

              Submit_NotaFiscal('');
              Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);

              Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
              Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
              Submit_ValorUnitario(fieldbyname('valor').AsString);
              Submit_Referencia(fieldbyname('referencia').AsString);
              Submit_Material('2');
              Submit_Cor(fieldbyname('cor').AsString);
              Submit_PesoUnitario(fieldbyname('peso').AsString);
              Submit_CodigoCor(fieldbyname('codigocor').AsString);

              if (calculatotaltributos) then
              begin
                try
                  //percentualtributo :=  ObjPerfilado_ICMS.IMPOSTO.Get_PERCENTUALTRIBUTO;
                  percentualtributo :=  ObjPerfilado_ICMS.Get_PERCENTUALTRIBUTO;
                  vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(fieldbyname('valorfinal').AsString);
                  Submit_percentualtributo(percentualtributo);
                  Submit_VTOTTRIB(CurrToStr(vtottrib));

                except
                  on e:exception do
                  begin
                    ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                    Exit;
                  end
                end
              end;
                    
              if (Salvar(true)=False) Then
              Begin
                messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                exit;
              End;
            End;

            next;
          End;//While
          FMostraBarraProgresso.IncrementaBarra1(1);
          FMostraBarraProgresso.Show;
        finally
                ObjPerfilado_ICMS.Free;
        end;

        try
          //buscando todos os vidros do projeto
          Close;
          SQL.Clear;
          Sql.add('Select TabVidro.*,');
          Sql.add('TabCor.Descricao as Cor,');
          Sql.add('TabVidro_PP.Quantidade,');
          Sql.add('TabVidro_PP.Valor,');
          Sql.add('TabVidro_PP.ValorFinal,TabPedido_Proj.Codigo as PedidoProjeto,');
          Sql.Add('TabVidro_PP.Altura, TabVidro_PP.Largura,TabVidro_PP.AlturaArredondamento, TabVidro_PP.LarguraArredondamento, TabVidro_PP.Complemento,');
          Sql.Add('TabVidro_PP.Codigo as codigoPP,tabcor.codigo as codigocor');
          Sql.add('from TabVidro_PP');
          Sql.add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabVidro_PP.PedidoProjeto');
          Sql.add('Join TabPedido on TabPedido.Codigo = TabPedido_Proj.Pedido');
          Sql.add('Join TabProjeto on TabProjeto.Codigo = TabPedido_Proj.Projeto');
          Sql.add('Join TabVidroCor on TabVidroCor.Codigo = TabVidro_PP.VidroCor');
          Sql.add('join TabVidro  on TabVidro.Codigo = TabVidroCor.Vidro');
          Sql.add('Join TabCor on TabCor.Codigo = TabVidroCor.Cor');
          Sql.add('Where TabPedido_Proj.Pedido = '+Pedido);
          sql.add('and tabpedido_proj.projeto='+CodigoProjetoBranco);
          Open;
          while not(eof) do
          Begin
            With MateriaisVenda  do
            Begin
              state:=dsInsert;
              ZerarTabela;
              Submit_Codigo(Get_NovoCodigo);
              Submit_Pedido(pedido);
              Submit_Descricao(fieldbyname('descricao').asstring);
              Submit_Unidade(fieldbyname('unidade').AsString);
              Submit_Ferragem('');
              Submit_Vidro(fieldbyname('Codigo').AsString);
              Submit_Perfilado('');
              Submit_Diverso('');
              Submit_Componente('');
              Submit_KitBox('');
              Submit_Persiana('');
              Submit_NCM(fieldbyname('NCM').AsString);
              submit_cest(fieldbyname('cest').AsString);

              If (ObjVidro_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) Then
              Begin
               //Se para alguma ferragem ainda n�o foram gravados os impostos, avisa qual ferragem �
                 MensagemErro('N�o foi encontrado o imposto de Origem para este Vidro, tipo de cliente e opera��o'+#13+
                            'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                exit;
              End;
              ObjVidro_ICMS.TabelaparaObjeto;
              Submit_MPOSTO_ICMS_ORIGEM(ObjVidro_ICMS.IMPOSTO.Get_CODIGO);
              Submit_IMPOSTO_IPI(ObjVidro_ICMS.imposto_ipi.Get_CODIGO);
              Submit_IMPOSTO_PIS_ORIGEM(ObjVidro_ICMS.imposto_pis.Get_CODIGO);
              Submit_IMPOSTO_COFINS_ORIGEM(ObjVidro_ICMS.imposto_cofins.Get_CODIGO);
              submit_CSOSN(ObjVidro_ICMS.IMPOSTO.CSOSN.Get_codigo);

              if(UFCliente=ESTADOSISTEMAGLOBAL) then
                Submit_CFOP(ObjVidro_ICMS.IMPOSTO.CFOP.Get_CODIGO)
              else
                Submit_CFOP(ObjVidro_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

              if (ObjVidro_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) Then
              Begin
                MensagemErro('N�o foi encontrado o imposto de Origem para este vidro, tipo de cliente e opera��o'+#13+
                  'Estado: '+UFCliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                exit;
              End;
              ObjVidro_ICMS.TabelaparaObjeto;
              Submit_IMPOSTO_ICMS_DESTINO(ObjVidro_ICMS.IMPOSTO.Get_CODIGO);
              Submit_MPOSTO_PIS_DESTINO(ObjVidro_ICMS.imposto_pis.Get_CODIGO);
              Submit_IMPOSTO_COFINS_DESTINO(ObjVidro_ICMS.imposto_cofins.Get_CODIGO);

              Submit_ALIQUOTA(ObjVidro_ICMS.IMPOSTO.Get_ALIQUOTA);
              Submit_REDUCAOBASECALCULO(ObjVidro_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC);
              submit_reducaobasecalculo_st(ObjVidro_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC_ST);
              Submit_ALIQUOTACUPOM('0');
              Submit_PERCENTUALAGREGADO('0');
              Submit_VALORPAUTA(ObjVidro_ICMS.IMPOSTO.Get_PAUTA);
              Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');


              Submit_VALORFRETE('0');
              Submit_VALORSEGURO('0');
              Submit_BC_ICMS('0');
              Submit_VALOR_ICMS('0');
              Submit_BC_ICMS_ST('0');
              Submit_VALOR_ICMS_ST('0');
              Submit_BC_IPI('0');
              Submit_VALOR_IPI('0');
              Submit_BC_PIS('0');
              Submit_VALOR_PIS('0');
              Submit_BC_PIS_ST('0');
              Submit_VALOR_PIS_ST('0');
              Submit_BC_COFINS('0');
              Submit_VALOR_COFINS('0');
              Submit_BC_COFINS_ST('0');
              Submit_VALOR_COFINS_ST('0');
              Submit_ALIQUOTACUPOM('0');


              Submit_SITUACAOTRIBUTARIA_TABELAA(ObjVidro_ICMS.IMPOSTO.STA.Get_CODIGO);
              //Submit_SITUACAOTRIBUTARIA_TABELAB(ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO);
              if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
                submit_CSOSN(ObjVidro_ICMS.IMPOSTO.CSOSN.Get_codigo)
              else
                Submit_SITUACAOTRIBUTARIA_TABELAB(ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO);
              if(ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO='40') or (ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO='41') then
                Submit_ISENTO('S')
              else
                Submit_ISENTO('N');

              if(ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO='10') or (ObjVidro_ICMS.IMPOSTO.STB.Get_CODIGO='60') then
                 Submit_SUBSTITUICAOTRIBUTARIA('S')
              else
                 Submit_SUBSTITUICAOTRIBUTARIA('N');

              Submit_NotaFiscal('');
              Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);

              Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
              Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
              Submit_ValorUnitario(fieldbyname('valor').AsString);
              Submit_Referencia(fieldbyname('referencia').AsString);
              Submit_Material('3');
              Submit_Cor(fieldbyname('cor').AsString);
              Submit_PesoUnitario(fieldbyname('peso').AsString);
              Submit_CodigoCor(fieldbyname('codigocor').AsString);

              if (calculatotaltributos) then
              begin
                try
                  //percentualtributo :=  ObjVidro_ICMS.IMPOSTO.Get_PERCENTUALTRIBUTO;
                  percentualtributo :=  ObjVidro_ICMS.Get_PERCENTUALTRIBUTO;
                  vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(fieldbyname('valorfinal').AsString);
                  Submit_percentualtributo(percentualtributo);
                  Submit_VTOTTRIB(CurrToStr(vtottrib));

                except
                  on e:exception do
                  begin
                    ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                    Exit;
                  end
                end
              end;
                    
              if (Salvar(True)=False) Then
              Begin
                messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                exit;
              End;
            End;
            next;
          End;//While
          FMostraBarraProgresso.IncrementaBarra1(1);
          FMostraBarraProgresso.Show;
        finally
          ObjVidro_ICMS.Free;
        end;

        try
          //resgatando os kitbox desta pedido
          Close;
          SQL.Clear;
          Sql.add('Select TabKitBox.*,');
          Sql.add('TabCor.Descricao as Cor,');
          Sql.add('TabKitBox_PP.Quantidade,');
          Sql.add('TabKitBox_PP.Valor,');
          Sql.add('TabKitBox_PP.ValorFinal,TabPedido_Proj.Codigo as PedidoProjeto,');
          Sql.Add('TabKitBox_PP.Codigo as codigoPP,tabcor.codigo as codigocor');
          Sql.add('from TabKitBox_PP');
          Sql.add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabKitBox_PP.PedidoProjeto');
          Sql.add('Join TabPedido on TabPedido.Codigo = TabPedido_Proj.Pedido');
          Sql.add('Join TabProjeto on TabProjeto.Codigo = TabPedido_Proj.Projeto');
          Sql.add('Join TabKitBoxCor on TabKitBoxCor.Codigo = TabKitBox_PP.KitBoxCor');
          Sql.add('join TabKitBox  on TabKitBox.Codigo = TabKitBoxCor.KitBox');
          Sql.add('Join TabCor on TabCor.Codigo = TabKitBoxCor.Cor');
          Sql.add('Where TabPedido_Proj.Pedido = '+Pedido);
          sql.add('and tabpedido_proj.projeto='+CodigoProjetoBranco);
          Open;
          while not(eof) do
          Begin
            With MateriaisVenda  do
            Begin
              state:=dsInsert;
              ZerarTabela;
              Submit_Codigo(Get_NovoCodigo);
              Submit_Pedido(pedido);
              Submit_Descricao(fieldbyname('descricao').asstring);
              Submit_Unidade(fieldbyname('unidade').AsString);
              Submit_Ferragem('');
              Submit_Vidro('');
              Submit_Perfilado('');
              Submit_Diverso('');
              Submit_Componente('');
              Submit_KitBox(fieldbyname('Codigo').AsString);
              Submit_Persiana('');
              Submit_NCM(fieldbyname('NCM').AsString);
              submit_cest(fieldbyname('cest').AsString);

              If (ObjKitbox_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) Then
              Begin

                //Se para alguma ferragem ainda n�o foram gravados os impostos, avisa qual ferragem �
                MensagemErro('N�o foi encontrado o imposto de Origem para este KitBox, tipo de cliente e opera��o'+#13+
                            'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                exit;
              End;
              ObjKitbox_ICMS.TabelaparaObjeto;
              Submit_MPOSTO_ICMS_ORIGEM(ObjKitbox_ICMS.IMPOSTO.Get_CODIGO);
              Submit_IMPOSTO_IPI(ObjKitbox_ICMS.imposto_ipi.Get_CODIGO);
              Submit_IMPOSTO_PIS_ORIGEM(ObjKitbox_ICMS.imposto_pis.Get_CODIGO);
              Submit_IMPOSTO_COFINS_ORIGEM(ObjKitbox_ICMS.imposto_cofins.Get_CODIGO);
              submit_CSOSN(ObjKitbox_ICMS.IMPOSTO.CSOSN.Get_codigo);

              if(UFCliente=ESTADOSISTEMAGLOBAL) then
                Submit_CFOP(ObjKitbox_ICMS.IMPOSTO.CFOP.Get_CODIGO)
              else
                Submit_CFOP(ObjKitbox_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

              if (ObjKitbox_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) Then
              Begin
                MensagemErro('N�o foi encontrado o imposto de Origem para este KitBox, tipo de cliente e opera��o'+#13+
                                    'Estado: '+UFCliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                exit;
              End;
              ObjKitbox_ICMS.TabelaparaObjeto;
              Submit_IMPOSTO_ICMS_DESTINO(ObjKitbox_ICMS.IMPOSTO.Get_CODIGO);
              Submit_MPOSTO_PIS_DESTINO(ObjKitbox_ICMS.imposto_pis.Get_CODIGO);
              Submit_IMPOSTO_COFINS_DESTINO(ObjKitbox_ICMS.imposto_cofins.Get_CODIGO);

              Submit_ALIQUOTA(ObjKitbox_ICMS.IMPOSTO.Get_ALIQUOTA);
              Submit_REDUCAOBASECALCULO(ObjKitbox_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC);
              submit_reducaobasecalculo_st(ObjKitbox_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC_ST);
              Submit_ALIQUOTACUPOM('0');
              Submit_PERCENTUALAGREGADO('0');
              Submit_VALORPAUTA(ObjKitbox_ICMS.IMPOSTO.Get_PAUTA);
              Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');


              Submit_VALORFRETE('0');
              Submit_VALORSEGURO('0');
              Submit_BC_ICMS('0');
              Submit_VALOR_ICMS('0');
              Submit_BC_ICMS_ST('0');
              Submit_VALOR_ICMS_ST('0');
              Submit_BC_IPI('0');
              Submit_VALOR_IPI('0');
              Submit_BC_PIS('0');
              Submit_VALOR_PIS('0');
              Submit_BC_PIS_ST('0');
              Submit_VALOR_PIS_ST('0');
              Submit_BC_COFINS('0');
              Submit_VALOR_COFINS('0');
              Submit_BC_COFINS_ST('0');
              Submit_VALOR_COFINS_ST('0');

              Submit_SITUACAOTRIBUTARIA_TABELAA(ObjKitbox_ICMS.IMPOSTO.STA.Get_CODIGO);
              //Submit_SITUACAOTRIBUTARIA_TABELAB(ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO);
              if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
                submit_CSOSN(ObjKitbox_ICMS.IMPOSTO.CSOSN.Get_codigo)
              else
                Submit_SITUACAOTRIBUTARIA_TABELAB(ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO);

              if(ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO='40') or (ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO='41') then
                Submit_ISENTO('S')
              else
                Submit_ISENTO('N');

              if(ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO='10') or (ObjKitbox_ICMS.IMPOSTO.STB.Get_CODIGO='60') then
                 Submit_SUBSTITUICAOTRIBUTARIA('S')
              else
                 Submit_SUBSTITUICAOTRIBUTARIA('N');

              Submit_NotaFiscal('');
              Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);

              Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
              Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
              Submit_ValorUnitario(fieldbyname('valor').AsString);
              Submit_Referencia(fieldbyname('referencia').AsString);
              Submit_Material('4');
              Submit_Cor(fieldbyname('cor').AsString);
              Submit_PesoUnitario(fieldbyname('peso').AsString);
              Submit_CodigoCor(fieldbyname('codigocor').AsString);

              if (calculatotaltributos) then
              begin
                try
                  //percentualtributo :=  ObjKitbox_ICMS.IMPOSTO.Get_PERCENTUALTRIBUTO;
                  percentualtributo :=  ObjKitbox_ICMS.Get_PERCENTUALTRIBUTO;
                  vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(fieldbyname('valorfinal').AsString);
                  Submit_percentualtributo(percentualtributo);
                  Submit_VTOTTRIB(CurrToStr(vtottrib));

                except
                  on e:exception do
                  begin
                    ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                    Exit;
                  end
                end
              end;
          
              if (Salvar(True)=False)
              Then Begin
                       messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                       exit;
              End;
            End;
            next;
          End;//While
          FMostraBarraProgresso.IncrementaBarra1(1);
          FMostraBarraProgresso.Show;
        finally
          ObjKitbox_ICMS.Free;
        end;

                    
        try
          //resgatando as persianas
          Close;
          SQL.Clear;
          Sql.add('Select  TabPersiana.*,');
          Sql.add('TabPersiana_PP.Quantidade,');
          Sql.add('TabPersiana_PP.Valor,');
          Sql.add('TabPersiana_PP.ValorFinal,');
          Sql.Add('TabPersiana_PP.Complemento,TabPedido_Proj.Codigo as PedidoProjeto,');
          Sql.add('TabPersiana_PP.Codigo as CodigoPP,TabPersianaGrupoDiametroCor.cor as codigocor, tabcor.descricao as cor');
          Sql.add('from TabPersiana_PP');
          Sql.add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabPersiana_PP.PedidoProjeto');
          Sql.add('Join TabPedido on TabPedido.Codigo = TabPedido_Proj.Pedido');
          Sql.add('Join TabPersianaGrupoDiametroCor on TabPersianaGrupoDiametroCor.Codigo = TabPersiana_PP.PersianaGrupoDiametroCor');
          Sql.add('Join TabPersiana on TabPersiana.Codigo = TabPersianaGrupoDiametroCor.Persiana');
          SQL.Add('Join tabcor on tabpersianagrupodiametrocor.cor=tabcor.codigo');
          Sql.add('Where TabPedido_Proj.Pedido = '+Pedido);
          sql.add('and tabpedido_proj.projeto='+CodigoProjetoBranco);
          Open;
          while not(eof) do
          Begin
            With MateriaisVenda  do
            Begin
              state:=dsInsert;
              ZerarTabela;
              Submit_Codigo(Get_NovoCodigo);
              Submit_Pedido(pedido);
              Submit_Descricao(fieldbyname('nome').asstring);
              Submit_Ferragem('');
              Submit_Vidro('');
              Submit_Perfilado('');
              Submit_Diverso('');
              Submit_Componente('');
              Submit_KitBox('');
              Submit_Persiana(fieldbyname('Codigo').AsString);
              Submit_NCM(fieldbyname('NCM').AsString);
              submit_cest(fieldbyname('cest').AsString);

              If (ObjPersiana_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) Then
              Begin

                MensagemErro('N�o foi encontrado o imposto de Origem para este Persiana, tipo de cliente e opera��o'+#13+
                            'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                exit;
              End;
              ObjPersiana_ICMS.TabelaparaObjeto;
              Submit_MPOSTO_ICMS_ORIGEM(ObjPersiana_ICMS.IMPOSTO.Get_CODIGO);
              Submit_IMPOSTO_IPI(ObjPersiana_ICMS.imposto_ipi.Get_CODIGO);
              Submit_IMPOSTO_PIS_ORIGEM(ObjPersiana_ICMS.imposto_pis.Get_CODIGO);
              Submit_IMPOSTO_COFINS_ORIGEM(ObjPersiana_ICMS.imposto_cofins.Get_CODIGO);
              submit_CSOSN(ObjPersiana_ICMS.IMPOSTO.CSOSN.Get_codigo);

              if(UFCliente=ESTADOSISTEMAGLOBAL) then
                Submit_CFOP(ObjPersiana_ICMS.IMPOSTO.CFOP.Get_CODIGO)
              else
                Submit_CFOP(ObjPersiana_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

              if (ObjPersiana_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) Then
              Begin
                MensagemErro('N�o foi encontrado o imposto de Origem para este Persiana, tipo de cliente e opera��o'+#13+
                            'Estado: '+UFCliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                exit;
              End;
              ObjPersiana_ICMS.TabelaparaObjeto;
              Submit_IMPOSTO_ICMS_DESTINO(ObjPersiana_ICMS.IMPOSTO.Get_CODIGO);
              Submit_MPOSTO_PIS_DESTINO(ObjPersiana_ICMS.imposto_pis.Get_CODIGO);
              Submit_IMPOSTO_COFINS_DESTINO(ObjPersiana_ICMS.imposto_cofins.Get_CODIGO);

              Submit_ALIQUOTA(ObjPersiana_ICMS.IMPOSTO.Get_ALIQUOTA);
              Submit_REDUCAOBASECALCULO(ObjPersiana_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC);
              submit_reducaobasecalculo_st(ObjPersiana_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC_ST);
              Submit_ALIQUOTACUPOM('0');
              Submit_PERCENTUALAGREGADO('0');
              Submit_VALORPAUTA(ObjPersiana_ICMS.IMPOSTO.Get_PAUTA);
              Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');


              Submit_VALORFRETE('0');
              Submit_VALORSEGURO('0');
              Submit_BC_ICMS('0');
              Submit_VALOR_ICMS('0');
              Submit_BC_ICMS_ST('0');
              Submit_VALOR_ICMS_ST('0');
              Submit_BC_IPI('0');
              Submit_VALOR_IPI('0');
              Submit_BC_PIS('0');
              Submit_VALOR_PIS('0');
              Submit_BC_PIS_ST('0');
              Submit_VALOR_PIS_ST('0');
              Submit_BC_COFINS('0');
              Submit_VALOR_COFINS('0');
              Submit_BC_COFINS_ST('0');
              Submit_VALOR_COFINS_ST('0');

              Submit_SITUACAOTRIBUTARIA_TABELAA(ObjPersiana_ICMS.IMPOSTO.STA.Get_CODIGO);
              //Submit_SITUACAOTRIBUTARIA_TABELAB(ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO);
              if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
                submit_CSOSN(ObjPersiana_ICMS.IMPOSTO.CSOSN.Get_codigo)
              else
                Submit_SITUACAOTRIBUTARIA_TABELAB(ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO);

              if(ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO='40') or (ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO='41') then
                Submit_ISENTO('S')
              else
                Submit_ISENTO('N');

              if(ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO='10') or (ObjPersiana_ICMS.IMPOSTO.STB.Get_CODIGO='60') then
                 Submit_SUBSTITUICAOTRIBUTARIA('S')
              else
                 Submit_SUBSTITUICAOTRIBUTARIA('N');

              Submit_NotaFiscal('');
              Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);

              Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
              Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
              Submit_ValorUnitario(fieldbyname('valor').AsString);
              Submit_Referencia(fieldbyname('referencia').AsString);
              Submit_Material('5');
              Submit_Cor(fieldbyname('cor').AsString);
              Submit_PesoUnitario('0');
              Submit_Unidade('M�');
              Submit_CodigoCor(fieldbyname('codigocor').AsString);

              if (calculatotaltributos) then
              begin
                try
                  //percentualtributo :=  ObjPersiana_ICMS.IMPOSTO.Get_PERCENTUALTRIBUTO;
                  percentualtributo :=  ObjPersiana_ICMS.Get_PERCENTUALTRIBUTO;
                  vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(fieldbyname('valorfinal').AsString);
                  Submit_percentualtributo(percentualtributo);
                  Submit_VTOTTRIB(CurrToStr(vtottrib));

                except
                  on e:exception do
                  begin
                    ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                    Exit;
                  end
                end
              end;
            
              if (Salvar(True)=False) Then
              Begin
                messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                exit;
              End;
            End;
            next;
          End;//While
          FMostraBarraProgresso.IncrementaBarra1(1);
          FMostraBarraProgresso.Show;
        finally
                  ObjPersiana_ICMS.Free;
        end;

        try
          //Resgatando os Diversos
          Close;
          SQL.Clear;
          Sql.add('Select TabDiverso.*,');
          Sql.add('TabCor.Descricao as Cor,');
          Sql.add('TabDiverso_PP.Quantidade,');
          Sql.add('TabDiverso_PP.Altura,');
          Sql.add('TabDiverso_PP.Largura,');
          Sql.add('TabDiverso_PP.Valor,');
          Sql.add('TabDiverso_PP.ValorFinal,TabPedido_Proj.Codigo as PedidoProjeto,');
          Sql.add('TabDiverso_PP.Codigo as CodigoPP,tabcor.codigo as codigocor');
          Sql.add('from TabDiverso_PP');
          Sql.add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabDiverso_PP.PedidoProjeto');
          Sql.add('Join TabPedido on TabPedido.Codigo = TabPedido_Proj.Pedido');
          Sql.add('Join TabDiversoCor on TabDiversoCor.Codigo = TabDiverso_PP.DiversoCor');
          Sql.add('join TabDiverso  on TabDiverso.Codigo = TabDiversoCor.Diverso');
          Sql.add('Join TabCor on TabCor.Codigo = TabDiversoCor.Cor');
          Sql.add('Where TabPedido_Proj.Pedido = '+Pedido);
          sql.add('and tabpedido_proj.projeto='+CodigoProjetoBranco);
          Open;
          while not(eof) do
          Begin
            With MateriaisVenda  do
            Begin
              state:=dsInsert;
              ZerarTabela;
              Submit_Codigo(Get_NovoCodigo);
              Submit_Pedido(pedido);
              Submit_Descricao(fieldbyname('descricao').asstring);
              Submit_Unidade(fieldbyname('unidade').AsString);
              Submit_Ferragem('');
              Submit_Vidro('');
              Submit_Perfilado('');
              Submit_Diverso(fieldbyname('Codigo').AsString);
              Submit_Componente('');
              Submit_KitBox('');
              Submit_Persiana('');
              Submit_NCM(fieldbyname('NCM').AsString);
              submit_cest(fieldbyname('cest').AsString);

              If (ObjDiverso_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) Then
              Begin

                MensagemErro('N�o foi encontrado o imposto de Origem para este Diverso, tipo de cliente e opera��o'+#13+
                            'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                exit;
              End;
              ObjDiverso_ICMS.TabelaparaObjeto;
              Submit_MPOSTO_ICMS_ORIGEM(ObjDiverso_ICMS.IMPOSTO.Get_CODIGO);
              Submit_IMPOSTO_IPI(ObjDiverso_ICMS.imposto_ipi.Get_CODIGO);
              Submit_IMPOSTO_PIS_ORIGEM(ObjDiverso_ICMS.imposto_pis.Get_CODIGO);
              Submit_IMPOSTO_COFINS_ORIGEM(ObjDiverso_ICMS.imposto_cofins.Get_CODIGO);
              submit_csosn(objdiverso_icms.IMPOSTO.CSOSN.Get_codigo);

              if(UFCliente=ESTADOSISTEMAGLOBAL) then
                Submit_CFOP(ObjDiverso_ICMS.IMPOSTO.CFOP.Get_CODIGO)
              else
                Submit_CFOP(ObjDiverso_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

              if (ObjDiverso_ICMS.Localiza(ESTADOSISTEMAGLOBAL,TipoCliente,fieldbyname('Codigo').AsString,operacao)=False) Then
              Begin
                MensagemErro('N�o foi encontrado o imposto de Origem para este Diverso, tipo de cliente e opera��o'+#13+
                            'Estado: '+UFCliente+' Tipo de Cliente '+TipoCliente+' '+'C�digo do material '+fieldbyname('Codigo').AsString);
                exit;
              End;
              ObjDiverso_ICMS.TabelaparaObjeto;
              Submit_IMPOSTO_ICMS_DESTINO(ObjDiverso_ICMS.IMPOSTO.Get_CODIGO);
              Submit_MPOSTO_PIS_DESTINO(ObjDiverso_ICMS.imposto_pis.Get_CODIGO);
              Submit_IMPOSTO_COFINS_DESTINO(ObjDiverso_ICMS.imposto_cofins.Get_CODIGO);

              Submit_ALIQUOTA(ObjDiverso_ICMS.IMPOSTO.Get_ALIQUOTA);
              Submit_REDUCAOBASECALCULO(ObjDiverso_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC);
              submit_reducaobasecalculo_st(ObjDiverso_ICMS.IMPOSTO.Get_PERC_REDUCAO_BC_ST);
              Submit_ALIQUOTACUPOM('0');
              Submit_PERCENTUALAGREGADO('0');
              Submit_VALORPAUTA(ObjDiverso_ICMS.IMPOSTO.Get_PAUTA);
              Submit_MARGEMVALORAGREGADOCONSUMIDOR('0');


              Submit_VALORFRETE('0');
              Submit_VALORSEGURO('0');
              Submit_BC_ICMS('0');
              Submit_VALOR_ICMS('0');
              Submit_BC_ICMS_ST('0');
              Submit_VALOR_ICMS_ST('0');
              Submit_BC_IPI('0');
              Submit_VALOR_IPI('0');
              Submit_BC_PIS('0');
              Submit_VALOR_PIS('0');
              Submit_BC_PIS_ST('0');
              Submit_VALOR_PIS_ST('0');
              Submit_BC_COFINS('0');
              Submit_VALOR_COFINS('0');
              Submit_BC_COFINS_ST('0');
              Submit_VALOR_COFINS_ST('0');


              Submit_SITUACAOTRIBUTARIA_TABELAA(ObjDiverso_ICMS.IMPOSTO.STA.Get_CODIGO);
              //Submit_SITUACAOTRIBUTARIA_TABELAB(ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO);

              if (ObjEmpresaGlobal.CRT.Get_Codigo = '1') then
                submit_CSOSN(ObjDiverso_ICMS.IMPOSTO.CSOSN.Get_codigo)
              else
                Submit_SITUACAOTRIBUTARIA_TABELAB(ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO);

              if(ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO='40') or (ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO='41') then
                Submit_ISENTO('S')
              else
                Submit_ISENTO('N');

              if(ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO = '30') or (ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO='10')or (ObjDiverso_ICMS.IMPOSTO.STB.Get_CODIGO='60') then
                 Submit_SUBSTITUICAOTRIBUTARIA('S')
              else
                 Submit_SUBSTITUICAOTRIBUTARIA('N');

              Submit_NotaFiscal('');
              Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);

              Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
              Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
              Submit_ValorUnitario(fieldbyname('valor').AsString);
              Submit_Referencia(fieldbyname('referencia').AsString);
              Submit_Material('6');
              Submit_Cor(fieldbyname('cor').AsString);
              Submit_PesoUnitario('0');
              Submit_CodigoCor(fieldbyname('codigocor').AsString);

              if (calculatotaltributos) then
              begin
                try
                  //percentualtributo :=  ObjDiverso_ICMS.IMPOSTO.Get_PERCENTUALTRIBUTO;
                  percentualtributo :=  ObjDiverso_ICMS.Get_PERCENTUALTRIBUTO;
                  vtottrib := (StrToCurrDef(percentualtributo,0)/100) * StrToCurr(fieldbyname('valorfinal').AsString);
                  Submit_percentualtributo(percentualtributo);
                  Submit_VTOTTRIB(CurrToStr(vtottrib));

                except
                  on e:exception do
                  begin
                    ShowMessage('Erro ao calcular valor total tributos: '+e.Message);
                    Exit;
                  end
                end
              end;
                    
              if (Salvar(True)=False) Then
              Begin
                messagedlg('N�o foi poss�vel Gravar a Ferragem referente a Ferragem/PP '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                exit;
              End;
            End;
            next;
          End;//While
          FMostraBarraProgresso.IncrementaBarra1(1);
          FMostraBarraProgresso.Show;
        finally
          ObjDiverso_ICMS.Free;
        end;
      end;
      result := true;
    except
      on e:Exception do
      begin
        MensagemErro('Erro ao gravar materiais do pedido em branco: '+e.Message);
        exit;
      end;
    end;
  finally
    if Assigned(MateriaisVenda) then
      MateriaisVenda.free;
  end;
end;

procedure TFPEDIDO.bt1Click(Sender: TObject);
begin
      FAjuda.PassaAjuda('CADASTRO DE PEDIDOS');
      FAjuda.ShowModal;
end;

procedure TFPEDIDO.edtaltura_VLExit(Sender: TObject);
var
  Plargura,paltura,pquantidade,plarguraarredondamento,palturaarredondamento:Currency;
begin
       if (EdtAltura_VL.Text <> '') and (EdtLargura_VL.Text <> '') then
       Begin
            Try
               paltura:=strtocurr(tira_ponto(edtaltura_VL.Text));
            Except
                  paltura:=0;
            End;

            Try
               plargura:=strtocurr(tira_ponto(edtlargura_VL.Text));
            Except
                plargura:=0;
            End;

            FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
            FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);

            pquantidade:=((plargura+plarguraarredondamento)*(paltura+palturaarredondamento))/1000000;
            edtquantidade_VL.Text:=formata_valor(CurrToStr(pquantidade));
       end
       Else edtquantidade_VL.Text:='0';
end;

procedure TFPEDIDO.edtRetornaDataExit(Sender: TObject);
var
  Data:TDateTime;
  DiasUteis:Integer;
begin
    if(edtRetornaData.Text='')
    then Exit;

    //DiasUteis:=RetornaDiasUteisEntreDuasDatas(DateToStr(now),edt)
    data:=Now;
    Data:=Data+StrToInt(edtRetornaData.Text);
    edtDataEntrega.Text:=DateToStr(Data)

end;

{Rodolfo}
procedure TFPEDIDO.rbSimClick(Sender: TObject);
begin
   rbsimm2.Checked:=False;
   rbnaom2.Checked:=True;
   edtquantidadeservico.Text := '';
    //edtLargServ.Enabled:=True;
    //edtAltServ.Enabled:=True;
    edtQuantidadeServico.enabled:=False;
    //edtAltServ.Color:=clWhite;
    //edtLargServ.Color:=clWhite;
    edtQuantidadeServico.Color:=cl3DLight;

    edtAltServ.Visible:=True;;
    edtLargServ.Visible:=True;
    lblAltServ.Visible:=True;
    lblLargServ.Visible:=True;

    {edtAltServ.Left := 4;
    edtLargServ.Left :=96;
    edtquantidadeservico.Left := 195;
    edtValorServico_PP.Left := 303;
    lb74.Left := 195;
    lb73.Left := 303;  }

end;

{Rodolfo}
procedure TFPEDIDO.rbNaoClick(Sender: TObject);
begin
   edtAltServ.text := '';
    edtLargServ.text := '';
    edtquantidadeservico.Text := '';
    //edtLargServ.Enabled:=False;
    //edtAltServ.Enabled:=False;
    edtAltServ.Visible:=False;
    edtLargServ.Visible:=False;
    lblAltServ.Visible:=False;
    lblLargServ.Visible:=False;
    edtQuantidadeServico.Enabled:=True;
    //edtAltServ.Color:=cl3DLight;
    //edtLargServ.Color:=cl3DLight;
    edtQuantidadeServico.Color:=clWhite;

   { edtquantidadeservico.Left := 4;
    edtValorServico_PP.Left := 96;
    lb74.Left := 4;
    lb73.Left := 96;   }
end;

{Rodolfo}
procedure TFPEDIDO.edtLargServExit(Sender: TObject);
var
pquantidade,paltura,plargura,plarguraarredondamento,palturaarredondamento:Currency;
begin
   if (edtAltServ.Text <> '') and (edtLargServ.Text <> '') then
   Begin
        Try
           paltura:=strtocurr(tira_ponto(edtAltServ.Text));
        Except
              paltura:=0;
        End;

        Try
           plargura:=strtocurr(tira_ponto(edtLargServ.Text));
        Except
            plargura:=0;
        End;

        FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
        FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);

        if(rbSim.Checked=True) then
          pquantidade:=((plargura*2)+(paltura*2))/1000
        else
           pquantidade:=((plargura*paltura)/1000)/1000;

        edtquantidadeservico.Text:=formata_valor(CurrToStr(pquantidade));
   end
   Else edtquantidadeservico.Text:='0';

end;

{Rodolfo}
procedure TFPEDIDO.edtAltServExit(Sender: TObject);
begin
    edtLargServExit(Sender);
end;

{Rodolfo}
procedure TFPEDIDO.edtAltServKeyPress(Sender: TObject; var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFPEDIDO.edtServico_PPReferenciaClick(Sender: TObject);
begin
    auxStr := lbNomeServico_pp.Caption;
end;

procedure TFPEDIDO.edtLargServKeyPress(Sender: TObject; var Key: Char);
begin
    edtAltServKeyPress(Sender, Key);
end;

procedure TFPEDIDO.edtServico_PPReferenciaKeyPress(Sender: TObject;
  var Key: Char);
begin
   if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFPEDIDO.pgc1Change(Sender: TObject);
begin
     if (pgc1.TabIndex=0)//Pedido
     Then Begin
               if (lbCodigoPedido.caption<>'') and (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Status=dsinactive)
               Then Begin
                         Self.AtualizaValoresPedido(lbCodigoPedido.caption);

                         if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(lbCodigoPedido.caption)=False)
                         Then exit;

                         Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
                         Self.ObjetoParaControles;
               End;
               HabilitaBotoesRegistro;
               exit;
     End;


     if (pgc1.TabIndex=1)//Projeto
     Then Begin

               if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Status=dsinsert)
               or (lbCodigoPedido.caption='')
               Then Begin

                         exit;
               End;
               //Self.btBtCancelarProjetoPedidoClick(sender);   {Desabilitado para nao apagar campos do painel projeto - Rodolfo}

               //Self.DesabilitaHabilitaBotoesGravarExcluir(btGravarProjetoPedido,btBtExcluirProjetoPedido);
               //Self.DesabilitaHabilitaBotoesGravarExcluir(btCalcular, btGravarProjetoPedido);
               //Self.DesabilitaHabilitaBotoesGravarExcluir(btAlterarPedidoProjeto,btAlterarPedidoProjeto);
               Self.PreparaPedidoProjeto;
               EdtProjetoPedidoReferencia.SetFocus;
               DesabilitaBotoesRegistro;
               ImgDesenho.Visible:=True;
               FescolheImagemBotao.PegaFiguraImagem(ImgDesenho,'FOTO');
               exit;
     End;


     if (pgc1.TabIndex=2)//Ferragem_PP
     Then Begin
               if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Status=dsinsert)
               or (lbCodigoPedido.caption='') //or (lbNomeProjetoPedido.Caption='') {Rodolfo - bloqueia as outras abas se nao for escolhido um projeto}
               Then Begin
                     //if (lbNomeProjetoPedido.Caption='') and (lbCodigoPedido.caption<>'') then
                        //ShowMessage('Escolha um Projeto');

                      exit;
               End;
               Self.btCancelarFeragem_PPClick(sender);

               //Self.DesabilitaHabilitaBotoesGravarExcluir(btGravarFerragem_PP,btExcluirFerragem_PP);
               Self.preparaFerragem_PP;
               edtFerragemCor_PPReferenicia.SetFocus;
               DesabilitaBotoesRegistro;
               exit;
     End;


     if (pgc1.TabIndex=3)//Perfilado_PP
     Then Begin

               if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Status=dsinsert)
               or (lbCodigoPedido.caption='')  //or (lbNomeProjetoPedido.Caption='') {Rodolfo - bloqueia as outras abas se nao for escolhido um projeto}
               Then Begin
                    // if (lbNomeProjetoPedido.Caption='') and (lbCodigoPedido.caption<>'')then
                        //ShowMessage('Escolha um Projeto');

                     exit;
               End;
               Self.btCancelarPerfilado_PPClick(sender);
               //Self.DesabilitaHabilitaBotoesGravarExcluir(btGravarPerfilado_PP,btExcluirPerfilado_PP);
               Self.preparaPerfilado_PP;
               EdtPerfiladoCor_PPReferencia.SetFocus;
               DesabilitaBotoesRegistro;
               exit;
     End;


     if (pgc1.TabIndex=4)//Vidro_PP
     Then Begin
               panelVidro.Visible:=true;
               if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Status=dsinsert)
               or (lbCodigoPedido.caption='') //or (lbNomeProjetoPedido.Caption='') {Rodolfo - bloqueia as outras abas se nao for escolhido um projeto}
               Then Begin
                    //if (lbNomeProjetoPedido.Caption='') and (lbCodigoPedido.caption<>'')then
                        //ShowMessage('Escolha um Projeto');

                    exit;
               End;
               Self.btBtCancelarVidro_PPClick(sender);
               //Self.DesabilitaHabilitaBotoesGravarExcluir(btGravarVidro_PP,btExcluirVidro_PP);
               Self.preparaVidro_PP;
               EdtVidroCor_PPReferencia2.SetFocus;
               DesabilitaBotoesRegistro;
               exit;
     End;

     if (pgc1.TabIndex=5)//KitBox_PP
     Then Begin
               if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Status=dsinsert)
               or (lbCodigoPedido.caption='') //or (lbNomeProjetoPedido.Caption='')   {Rodolfo - bloqueia as outras abas se nao for escolhido um projeto}
               Then Begin
                     //if (lbNomeProjetoPedido.Caption='') and (lbCodigoPedido.caption<>'') then
                        //ShowMessage('Escolha um Projeto');

                    exit;
               End;
               Self.btCancelarKitBox_PPClick(sender);
              // Self.DesabilitaHabilitaBotoesGravarExcluir(btGravarKitBox_PP,btExcluirKitBox_PP);
               Self.preparaKitBox_PP;
               EdtKitBoxCor_PPReferencia.SetFocus;
               DesabilitaBotoesRegistro;
               exit;
     End;

     if (pgc1.TabIndex=6)//Servico_PP
     Then Begin
               if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Status=dsinsert)
               or (lbCodigoPedido.caption='') //or (lbNomeProjetoPedido.Caption='')  {Rodolfo - bloqueia as outras abas se nao for escolhido um projeto}
               Then Begin
                     //if (lbNomeProjetoPedido.Caption='') and (lbCodigoPedido.caption<>'') then
                        //ShowMessage('Escolha um Projeto');
                        
                     exit;
               End;

               Self.btCancelarServico_PPClick(Sender);
               //Self.DesabilitaHabilitaBotoesGravarExcluir(btGravarServico_PP,btExcluirServico_PP);
               Self.PreparaServico_PP;
               rbNao.Checked:=True;
               rbnaom2.Checked:=True;
               rbNaoClick(sender);   {Rodolfo}

               EdtServico_PPReferencia.SetFocus;
               DesabilitaBotoesRegistro;
               exit;
     End;

     if (pgc1.TabIndex=7)//Persiana_PP
     Then Begin
               if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Status=dsinsert)
               or (lbCodigoPedido.caption='') //or (lbNomeProjetoPedido.Caption='')  {Rodolfo - bloqueia as outras abas se nao for escolhido um projeto}
               Then Begin
                     //if (lbNomeProjetoPedido.Caption='') and (lbCodigoPedido.caption<>'')then
                        //ShowMessage('Escolha um Projeto');

                     exit;
               End;
               Self.btCancelarPersiana_PPClick(Sender);
               //Self.DesabilitaHabilitaBotoesGravarExcluir(btGravarPersiana_PP,btExcluirPersiana_PP);
               Self.preparaPersiana_PP;
               EdtPersiana_PPReferencia.SetFocus;
               DesabilitaBotoesRegistro;
               exit;
     End;


     if (pgc1.TabIndex=8)//Diverso_PP
     Then Begin
               if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Status=dsinsert)
               or (lbCodigoPedido.caption='') //or (lbNomeProjetoPedido.Caption='')    {Rodolfo - bloqueia as outras abas se nao for escolhido um projeto}
               Then Begin
                     //if (lbNomeProjetoPedido.Caption='') and (lbCodigoPedido.caption<>'') then
                        //ShowMessage('Escolha um Projeto');
                         
                     exit;
               End;
               Self.btCancelarDiverso_PPClick(Sender);
               //Self.DesabilitaHabilitaBotoesGravarExcluir(btGravarDiverso_PP,btExcluirDiverso_PP);
               Self.preparaDiverso_PP;
               EdtDiverso_PPReferencia.SetFocus;
               DesabilitaBotoesRegistro;
               exit;
     End;

     if (pgc1.TabIndex=9)//Venda em Lote
     Then Begin
               if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Status=dsinsert)
               or (lbCodigoPedido.caption='') //or (lbNomeProjetoPedido.Caption='')      {Rodolfo - bloqueia as outras abas se nao for escolhido um projeto}
               Then Begin
                     //if (lbNomeProjetoPedido.Caption='') and (lbCodigoPedido.caption<>'') then
                        //ShowMessage('Escolha um Projeto');

                     exit;
               End;

               //Self.DesabilitaHabilitaBotoesGravarExcluir(btGravarDiverso_PP,btExcluirDiverso_PP);
               //Self.preparaDiverso_PP;
               //EdtDiverso_PPReferencia.SetFocus;
               limpaedit(PanelVendaemLote);
               lbNomeVidro_VL.caption:='';
               DesabilitaBotoesRegistro;
               habilita_campos(panelvendaemlote);
               Self.ResgataVidro_PP;
               exit;
     End;

end;

procedure TFPEDIDO.imgDesenhoClick(Sender: TObject);
var
   FVisualizarProjetos:TFVisulizarProjetos;
begin

    FVisualizarProjetos:=TFVisulizarProjetos.Create(nil);

    try
      if(edtProjetoPedido.Text='') or (lbCodigoPedido.Caption='') then
      begin
         Exit;
      end;

      FVisualizarProjetos.PassaObjetos(edtProjetoPedido.Text,lbCodigoPedido.Caption,edtCodigoProjetoPedido.Text);
      FVisualizarProjetos.ShowModal;
    finally
      FreeAndNil(FVisualizarProjetos);
    end;

end;

procedure TFPEDIDO.btAjudaVideoClick(Sender: TObject);
var
   FVideosAjuda:TFvideos ;
begin
    FvideosAjuda:= TFvideos.Create(nil);
    try
      FVideosAjuda.PegaLink('','CADASTRO DE PEDIDOS');
      FVideosAjuda.ShowModal;
    finally
      FreeAndNil(FVideosAjuda);
    end;

end;

procedure TFPEDIDO.__VerificaProducaoPedido;
var
  QryVerifica_Producao:TIBQuery;
begin
  if(lbCodigoPedido.Caption='') or (lbCodigoPedido.Caption='0') then
  begin
    btEmProducao.Visible:=False;
    Exit;
  end;


  try
    QryVerifica_Producao:=TIBQuery.Create(nil);
    QryVerifica_Producao.Database:=FDataModulo.IBDatabase;
  except
    FreeAndNil(QryVerifica_Producao);
  end;

  try
    QryVerifica_Producao.Close;
    QryVerifica_Producao.SQL.Clear;
    QryVerifica_Producao.SQL.Text:=
    'SELECT distinct( pp.codigo) as pedidoprojeto '+
    'FROM TABPEDIDOPROJ_ORDEMPRODUCAO PPO '+
    'JOIN TABPEDIDO_PROJ PP ON PP.CODIGO=PPO.PEDIDOPROJETO '+
    'JOIN TABPEDIDO P ON P.CODIGO=PP.PEDIDO '+
    'JOIN TABCLIENTE C ON C.CODIGO=P.CLIENTE '+
    'JOIN TABPROJETO Proj ON Proj.CODIGO=PP.PROJETO '+
    'WHERE P.CODIGO='+lbCodigoPedido.Caption;

    QryVerifica_Producao.Open;
    QryVerifica_Producao.Last;
    if(QryVerifica_Producao.RecordCount=0)
    then btEmProducao.Visible:=False
    else btEmProducao.Visible:=True;

  finally
    FreeAndNil(QryVerifica_Producao);
  end;
end;

procedure TFPEDIDO.btEmProducaoClick(Sender: TObject);
var
  FProducao:TFproducao;
begin
  try
    FProducao:=TFProducao.Create(nil);
  except
    Exit;
  end;

  try
    FProducao.__LocalizaProjetosPorPedido(lbCodigoPedido.Caption);
    FProducao.ShowModal;
  finally
    FreeAndNil(FProducao);
  end;
end;

Function TFPEDIDO.___VerificaEstoque:Boolean;
var
  Qry_VerificaEstoque_QUERY:TIBQuery;
  Qry_VerificaEstoque_QUERY2:TIBQuery;
  FmostraEstoque:TFMostraEstoque;
begin
  Result:=True;
  try
    Qry_VerificaEstoque_QUERY:=TIBQuery.Create(nil);
    Qry_VerificaEstoque_QUERY.Database:=FDataModulo.IBDatabase;
    Qry_VerificaEstoque_QUERY2:=TIBQuery.Create(nil);
    Qry_VerificaEstoque_QUERY2.Database:=FDataModulo.IBDatabase;
    FmostraEstoque:=TFMostraEstoque.Create(nil);
  except
    Exit;
  end;
  FmostraEstoque.ConfiguraStringgrid;
  FmostraEstoque.lbCodPedido.Caption:=lbCodigoPedido.caption;
  try
    //VIDRO
    Qry_VerificaEstoque_QUERY.Close;
    Qry_VerificaEstoque_QUERY.SQL.Clear;
    Qry_VerificaEstoque_QUERY.SQL.Text:=
    'Select vcor.codigo, v.descricao,cor.descricao as nomecor,sum(vpp.quantidade),v.unidade,vcor.vidro '+
    'from tabvidro_pp vpp '+
    'join tabpedido_proj pp on pp.codigo = vpp.pedidoprojeto '+
    'join tabvidrocor vcor on vcor.codigo=vpp.vidrocor '+
    'join tabvidro v on v.codigo= vcor.vidro '+
    'join tabcor cor on cor.codigo=vcor.cor '+
    'where pp.pedido=:pedido '+
    'group by vcor.codigo, v.descricao,cor.descricao,v.unidade,vcor.vidro';
    Qry_VerificaEstoque_QUERY.Params[0].AsString:=lbCodigoPedido.Caption;
    Qry_VerificaEstoque_QUERY.Open;

    while not Qry_VerificaEstoque_QUERY.Eof do
    begin
      Qry_VerificaEstoque_QUERY2.Close;
      Qry_VerificaEstoque_QUERY2.SQL.Clear;
      Qry_VerificaEstoque_QUERY2.SQL.Text:=
      'Select coalesce(sum(quantidade),0) as estoque from tabestoque where vidrocor='+Qry_VerificaEstoque_QUERY.Fields[0].AsString;
      Qry_VerificaEstoque_QUERY2.Open;

      if((Qry_VerificaEstoque_QUERY2.Fields[0].AsCurrency-Qry_VerificaEstoque_QUERY.Fields[3].AsCurrency)<=0) then
      begin
        Result:=False;
        FmostraEstoque.STRG1.Cells[1,FmostraEstoque.STRG1.RowCount-1]:='VIDRO';
        FmostraEstoque.STRG1.Cells[2,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[1].AsString+' - '+Qry_VerificaEstoque_QUERY.Fields[2].AsString;
        FmostraEstoque.STRG1.Cells[3,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[3].AsString+' '+Qry_VerificaEstoque_QUERY.Fields[4].AsString;
        FmostraEstoque.STRG1.Cells[4,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY2.Fields[0].AsString+' '+Qry_VerificaEstoque_QUERY.Fields[4].AsString;
        FmostraEstoque.STRG1.Cells[5,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[5].AsString;
        FmostraEstoque.STRG1.RowCount:=FmostraEstoque.STRG1.RowCount+1;
      end;

      Qry_VerificaEstoque_QUERY.Next;
    end;

    //FERRAGEM
    Qry_VerificaEstoque_QUERY.Close;
    Qry_VerificaEstoque_QUERY.SQL.Clear;
    Qry_VerificaEstoque_QUERY.SQL.Text:=
    'Select Fcor.codigo, f.descricao,cor.descricao as nomecor,sum(Fpp.quantidade),f.unidade,fcor.ferragem '+
    'from tabferragem_pp fpp '+
    'join tabpedido_proj pp on pp.codigo = fpp.pedidoprojeto '+
    'join tabferragemcor fcor on fcor.codigo=fpp.ferragemcor '+
    'join tabferragem f on f.codigo= fcor.ferragem '+
    'join tabcor cor on cor.codigo=fcor.cor '+
    'where pp.pedido=:pedido '+
    'group by fcor.codigo, f.descricao,cor.descricao,f.unidade,fcor.ferragem';
    Qry_VerificaEstoque_QUERY.Params[0].AsString:=lbCodigoPedido.Caption;
    Qry_VerificaEstoque_QUERY.Open;

    while not Qry_VerificaEstoque_QUERY.Eof do
    begin
      Qry_VerificaEstoque_QUERY2.Close;
      Qry_VerificaEstoque_QUERY2.SQL.Clear;
      Qry_VerificaEstoque_QUERY2.SQL.Text:=
      'Select coalesce(sum(quantidade),0) as estoque from tabestoque where ferragemcor='+Qry_VerificaEstoque_QUERY.Fields[0].AsString;
      Qry_VerificaEstoque_QUERY2.Open;

      if((Qry_VerificaEstoque_QUERY2.Fields[0].AsCurrency-Qry_VerificaEstoque_QUERY.Fields[3].AsCurrency)<=0) then
      begin
        Result:=False;
        FmostraEstoque.STRG1.Cells[1,FmostraEstoque.STRG1.RowCount-1]:='FERRAGEM';
        FmostraEstoque.STRG1.Cells[2,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[1].AsString+' - '+Qry_VerificaEstoque_QUERY.Fields[2].AsString;
        FmostraEstoque.STRG1.Cells[3,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[3].AsString+' '+Qry_VerificaEstoque_QUERY.Fields[4].AsString;
        FmostraEstoque.STRG1.Cells[4,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY2.Fields[0].AsString+' '+Qry_VerificaEstoque_QUERY.Fields[4].AsString;
        FmostraEstoque.STRG1.Cells[5,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[5].AsString;
        FmostraEstoque.STRG1.RowCount:=FmostraEstoque.STRG1.RowCount+1;
      end;

      Qry_VerificaEstoque_QUERY.Next;
    end;

    //PERFILADO
    Qry_VerificaEstoque_QUERY.Close;
    Qry_VerificaEstoque_QUERY.SQL.Clear;
    Qry_VerificaEstoque_QUERY.SQL.Text:=
    'Select pcor.codigo, p.descricao,cor.descricao as nomecor,sum(ppp.quantidade),p.unidade,pcor.perfilado '+
    'from tabperfilado_pp ppp '+
    'join tabpedido_proj pp on pp.codigo = ppp.pedidoprojeto '+
    'join tabperfiladocor pcor on pcor.codigo=ppp.perfiladocor '+
    'join tabperfilado p on p.codigo= pcor.perfilado '+
    'join tabcor cor on cor.codigo=pcor.cor '+
    'where pp.pedido=:pedido '+
    'group by pcor.codigo, p.descricao,cor.descricao,p.unidade,pcor.perfilado';
    Qry_VerificaEstoque_QUERY.Params[0].AsString:=lbCodigoPedido.Caption;
    Qry_VerificaEstoque_QUERY.Open;

    while not Qry_VerificaEstoque_QUERY.Eof do
    begin
      Qry_VerificaEstoque_QUERY2.Close;
      Qry_VerificaEstoque_QUERY2.SQL.Clear;
      Qry_VerificaEstoque_QUERY2.SQL.Text:=
      'Select coalesce(sum(quantidade),0) as estoque from tabestoque where perfiladocor='+Qry_VerificaEstoque_QUERY.Fields[0].AsString;
      Qry_VerificaEstoque_QUERY2.Open;

      if((Qry_VerificaEstoque_QUERY2.Fields[0].AsCurrency-Qry_VerificaEstoque_QUERY.Fields[3].AsCurrency)<=0) then
      begin
        Result:=False;
        FmostraEstoque.STRG1.Cells[1,FmostraEstoque.STRG1.RowCount-1]:='PERFILADO';
        FmostraEstoque.STRG1.Cells[2,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[1].AsString+' - '+Qry_VerificaEstoque_QUERY.Fields[2].AsString;
        FmostraEstoque.STRG1.Cells[3,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[3].AsString+' '+Qry_VerificaEstoque_QUERY.Fields[4].AsString;
        FmostraEstoque.STRG1.Cells[4,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY2.Fields[0].AsString+' '+Qry_VerificaEstoque_QUERY.Fields[4].AsString;
        FmostraEstoque.STRG1.Cells[5,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[5].AsString;
        FmostraEstoque.STRG1.RowCount:=FmostraEstoque.STRG1.RowCount+1;
      end;

      Qry_VerificaEstoque_QUERY.Next;
    end;

    //DIVERSO
    Qry_VerificaEstoque_QUERY.Close;
    Qry_VerificaEstoque_QUERY.SQL.Clear;
    Qry_VerificaEstoque_QUERY.SQL.Text:=
    'Select dcor.codigo, d.descricao,cor.descricao as nomecor,sum(dpp.quantidade),d.unidade,dcor.diverso '+
    'from tabdiverso_pp dpp '+
    'join tabpedido_proj pp on pp.codigo = dpp.pedidoprojeto '+
    'join tabdiversocor dcor on dcor.codigo=dpp.diversocor '+
    'join tabdiverso d on d.codigo= dcor.diverso '+
    'join tabcor cor on cor.codigo=dcor.cor '+
    'where pp.pedido=:pedido '+
    'group by dcor.codigo, d.descricao,cor.descricao,d.unidade,dcor.diverso';
    Qry_VerificaEstoque_QUERY.Params[0].AsString:=lbCodigoPedido.Caption;
    Qry_VerificaEstoque_QUERY.Open;

    while not Qry_VerificaEstoque_QUERY.Eof do
    begin
      Qry_VerificaEstoque_QUERY2.Close;
      Qry_VerificaEstoque_QUERY2.SQL.Clear;
      Qry_VerificaEstoque_QUERY2.SQL.Text:=
      'Select coalesce(sum(quantidade),0) as estoque from tabestoque where diversocor='+Qry_VerificaEstoque_QUERY.Fields[0].AsString;
      Qry_VerificaEstoque_QUERY2.Open;

      if((Qry_VerificaEstoque_QUERY2.Fields[0].AsCurrency-Qry_VerificaEstoque_QUERY.Fields[3].AsCurrency)<=0) then
      begin
        Result:=False;
        FmostraEstoque.STRG1.Cells[1,FmostraEstoque.STRG1.RowCount-1]:='DIVERSO';
        FmostraEstoque.STRG1.Cells[2,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[1].AsString+' - '+Qry_VerificaEstoque_QUERY.Fields[2].AsString;
        FmostraEstoque.STRG1.Cells[3,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[3].AsString+' '+Qry_VerificaEstoque_QUERY.Fields[4].AsString;
        FmostraEstoque.STRG1.Cells[4,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY2.Fields[0].AsString+' '+Qry_VerificaEstoque_QUERY.Fields[4].AsString;
        FmostraEstoque.STRG1.Cells[5,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[5].AsString;
        FmostraEstoque.STRG1.RowCount:=FmostraEstoque.STRG1.RowCount+1;
      end;

      Qry_VerificaEstoque_QUERY.Next;
    end;

     //KITBOX
    Qry_VerificaEstoque_QUERY.Close;
    Qry_VerificaEstoque_QUERY.SQL.Clear;
    Qry_VerificaEstoque_QUERY.SQL.Text:=
    'Select kcor.codigo, k.descricao,cor.descricao as nomecor,sum(kpp.quantidade),k.unidade,kcor.kitbox '+
    'from tabkitbox_pp kpp '+
    'join tabpedido_proj pp on pp.codigo = kpp.pedidoprojeto '+
    'join tabkitboxcor kcor on kcor.codigo=kpp.kitboxcor '+
    'join tabkitbox k on k.codigo= kcor.kitbox '+
    'join tabcor cor on cor.codigo=kcor.cor '+
    'where pp.pedido=:pedido '+
    'group by kcor.codigo, k.descricao,cor.descricao,k.unidade,kcor.kitbox';
    Qry_VerificaEstoque_QUERY.Params[0].AsString:=lbCodigoPedido.Caption;
    Qry_VerificaEstoque_QUERY.Open;

    while not Qry_VerificaEstoque_QUERY.Eof do
    begin
      Qry_VerificaEstoque_QUERY2.Close;
      Qry_VerificaEstoque_QUERY2.SQL.Clear;
      Qry_VerificaEstoque_QUERY2.SQL.Text:=
      'Select coalesce(sum(quantidade),0) as estoque from tabestoque where kitboxcor='+Qry_VerificaEstoque_QUERY.Fields[0].AsString;
      Qry_VerificaEstoque_QUERY2.Open;

      if((Qry_VerificaEstoque_QUERY2.Fields[0].AsCurrency-Qry_VerificaEstoque_QUERY.Fields[3].AsCurrency)<=0) then
      begin
        Result:=False;
        FmostraEstoque.STRG1.Cells[1,FmostraEstoque.STRG1.RowCount-1]:='KIT BOX';
        FmostraEstoque.STRG1.Cells[2,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[1].AsString+' - '+Qry_VerificaEstoque_QUERY.Fields[2].AsString;
        FmostraEstoque.STRG1.Cells[3,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[3].AsString+' '+Qry_VerificaEstoque_QUERY.Fields[4].AsString;
        FmostraEstoque.STRG1.Cells[4,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY2.Fields[0].AsString+' '+Qry_VerificaEstoque_QUERY.Fields[4].AsString;
        FmostraEstoque.STRG1.Cells[5,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[5].AsString;
        FmostraEstoque.STRG1.RowCount:=FmostraEstoque.STRG1.RowCount+1;
      end;

      Qry_VerificaEstoque_QUERY.Next;
    end;

    //PERSIANA
    Qry_VerificaEstoque_QUERY.Close;
    Qry_VerificaEstoque_QUERY.SQL.Clear;
    Qry_VerificaEstoque_QUERY.SQL.Text:=
    'Select Pcor.codigo, P.nome,cor.descricao as nomecor,sum(Ppp.quantidade),pcor.persiana '+
    'from tabpersiana_pp ppp '+
    'join tabpedido_proj pp on pp.codigo = ppp.pedidoprojeto '+
    'join tabpersianagrupodiametrocor pcor on pcor.codigo=ppp.persianagrupodiametrocor '+
    'join tabpersiana p on p.codigo= pcor.persiana '+
    'join tabcor cor on cor.codigo=pcor.cor '+
    'where pp.pedido=:pedido '+
    'group by pcor.codigo, P.nome,cor.descricao,pcor.persiana';
    Qry_VerificaEstoque_QUERY.Params[0].AsString:=lbCodigoPedido.Caption;
    Qry_VerificaEstoque_QUERY.Open;

    while not Qry_VerificaEstoque_QUERY.Eof do
    begin
      Qry_VerificaEstoque_QUERY2.Close;
      Qry_VerificaEstoque_QUERY2.SQL.Clear;
      Qry_VerificaEstoque_QUERY2.SQL.Text:=
      'Select coalesce(sum(quantidade),0) as estoque from tabestoque where persianagrupodiametrocor='+Qry_VerificaEstoque_QUERY.Fields[0].AsString;
      Qry_VerificaEstoque_QUERY2.Open;

      if((Qry_VerificaEstoque_QUERY2.Fields[0].AsCurrency-Qry_VerificaEstoque_QUERY.Fields[3].AsCurrency)<=0) then
      begin
        Result:=False;
        FmostraEstoque.STRG1.Cells[1,FmostraEstoque.STRG1.RowCount-1]:='PERSIANA';
        FmostraEstoque.STRG1.Cells[2,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[1].AsString+' - '+Qry_VerificaEstoque_QUERY.Fields[2].AsString;
        FmostraEstoque.STRG1.Cells[3,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[3].AsString+' M�';
        FmostraEstoque.STRG1.Cells[4,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY2.Fields[0].AsString+' M�';
        FmostraEstoque.STRG1.Cells[5,FmostraEstoque.STRG1.RowCount-1]:=Qry_VerificaEstoque_QUERY.Fields[5].AsString;
        FmostraEstoque.STRG1.RowCount:=FmostraEstoque.STRG1.RowCount+1;
      end;

      Qry_VerificaEstoque_QUERY.Next;
    end;

    FmostraEstoque.STRG1.RowCount:=FmostraEstoque.STRG1.RowCount-1;
    FmostraEstoque.ShowModal;
  finally
    FreeAndNil(Qry_VerificaEstoque_QUERY);
    FreeAndNil(Qry_VerificaEstoque_QUERY2);
    FreeAndNil(FmostraEstoque);
  end;
end;

procedure TFPEDIDO.edtLarguraExit(Sender: TObject);
begin
  ___Verifica_LimiteLargura;
end;

procedure TFPEDIDO.edtAlturaExit(Sender: TObject);
begin
 ___Verficca_LimeteAltura;
end;

function TFPEDIDO.___Verifica_LimiteLargura:Boolean;
begin
 Result:=False;
 if(edtLargura.Text='')
 then Exit;

 if(StrToInt(Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_LarguraMinima)>StrToInt(edtLargura.Text))then
 begin
    ShowMessage('Largura menor que a largura m�nima sugerida para o PROJETO :'+Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Descricao);
    Exit;
 end;
 if(StrToInt(Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_LarguraMaxima)<StrToInt(edtLargura.Text)) and (StrToInt(Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_LarguraMaxima)>0) then
 begin
    ShowMessage('Largura maior que a largura m�xima sugerida para o PROJETO :'+Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Descricao);
    Exit;
 end;
 if(edtAltura.Text<>'') and (edtLargura.Text<>'') then
 begin
   if(StrToInt(Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_AreaMinima)>((StrToInt(edtAltura.Text)*StrToInt(edtLargura.Text))/1000))then
   begin
     ShowMessage('Area informada � menor que a area m�nima sugerida para o PROJETO :'+Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Descricao);
     Exit;
   end;
 end;
 Result:=True;
end;

function TFPEDIDO.___Verficca_LimeteAltura:Boolean;
begin
 Result:=False;
 if(edtAltura.Text='')
 then Exit;

 if(StrToInt(Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_alturaMinima)>StrToInt(edtaltura.Text))then
 begin
    ShowMessage('Altura menor que a altura m�nima sugerida para o PROJETO :'+Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Descricao);
    Exit;
 end;
 if(StrToInt(Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_alturaMaxima)<StrToInt(edtaltura.Text)) and (StrToInt(Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_alturaMaxima)>0) then
 begin
    ShowMessage('Altura maior que a altura m�xima sugerida para o PROJETO :'+Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Descricao);
    Exit;
 end;
 if(edtLargura.Text<>'') and (edtAltura.Text<>'') then
 begin
   if(StrToInt(Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_AreaMinima)>((StrToInt(edtAltura.Text)*StrToInt(edtLargura.Text))/1000))then
   begin
     ShowMessage('Area informada � menor que a area m�nima sugerida para o PROJETO :'+Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Projeto.Get_Descricao);
     Exit;
   end;
 end;
 Result:=True;
end;

procedure TFPEDIDO.btRomaneioClick(Sender: TObject);
var
  FGeraRomaneio:TFGeraRomaneioPedido;
begin
  if(lbCodigoPedido.Caption='0') or (lbCodigoPedido.Caption='')
  then Exit;

  try
    FGeraRomaneio:=TFGeraRomaneioPedido.Create(nil);
  except
    Exit;
  end;

  try
    FGeraRomaneio.PassaObjetos(lbCodigoPedido.Caption,edtCliente.Text);
    FGeraRomaneio.ShowModal;

    ___VerificaRomaneioPedido;

  finally
    FreeAndNil(FGeraRomaneio);
  end;
end;

function TFPEDIDO.___VerificaRomaneioPedido:Boolean;
var
  qry_VerificaRomaneio:TIBQuery;
begin
  Result:=True;
  try
    qry_VerificaRomaneio:=TIBQuery.Create(nil);
    qry_VerificaRomaneio.Database:=FDataModulo.IBDatabase;

    qry_VerificaRomaneio.SQL.Text:=
    'select distinct(r.codigo) '+
    'from tabromaneio r '+
    'join tabpedidoprojetoromaneio ppr on ppr.romaneio=r.codigo '+
    'join tabpedido_proj pp on pp.codigo=ppr.pedidoprojeto '+
    'where  pp.pedido='+lbCodigoPedido.Caption;

    qry_VerificaRomaneio.Open;
    lbRomaneio.Caption:='Romaneios: ';
    qry_VerificaRomaneio.Last;
    if(qry_VerificaRomaneio.RecordCount>0) then
    begin
      qry_VerificaRomaneio.First;
      while not qry_VerificaRomaneio.Eof do
      begin
        lbRomaneio.Caption:=lbRomaneio.Caption + qry_VerificaRomaneio.Fields[0].AsString+' ';
        qry_VerificaRomaneio.Next;
      end;

    end
    else Result:=False;

  finally
    FreeAndNil(qry_VerificaRomaneio);
  end;
end;

procedure TFPEDIDO.lbRomaneioClick(Sender: TObject);
VAR
  tag,palavra:string;
  i:Integer;
begin
  tag:=retornaPalavrasDepoisSimbolo2(lbRomaneio.Caption,' ');
  i:=0;
  palavra:='';
  if(retornaPalavrasDepoisSimbolo2(tag,' ')<>'')then
  begin
     PopUpImprimeromaneios.Items.Clear;
     palavra:=retornaPalavrasAntesSimbolo(tag,' ');
     while (tag<>'') do
     begin
       item:=TMenuItem.Create(PopUpImprimeromaneios);
       PopUpImprimeromaneios.Items.Insert(i,item);
       PopUpImprimeromaneios.Items[i].Caption:=palavra;
       PopUpImprimeromaneios.Items[i].ImageIndex:= 23;
       PopUpImprimeromaneios.Items[i].OnClick:=popupClick;
       Inc(i,1);
       tag:=retornaPalavrasDepoisSimbolo2(tag,' ');
       palavra:=retornaPalavrasAntesSimbolo(tag,' ');
     end;
     PopUpImprimeromaneios.Popup(Mouse.CursorPos.X,Mouse.CursorPos.Y);
  end
  else chamaFormulario(TFROMANEIO,self,Tag);
end;

procedure TFPEDIDO.popupClick(sender:TObject);
begin
  chamaFormulario(TFROMANEIO,self,tira_caracter((Sender as TMenuItem).Caption,'&'));
end;

procedure TFPEDIDO.edtVendedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin 
     Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.EdtVendedorKeyDown(Sender, key, Shift, LbNomeVendedor);
end;

function TFPEDIDO.Integrar_CorteCerto:Boolean;
var
  Arquivo:TextFile;
  Dir:string;
  query_arquivo:TIBQuery;
  query_arquivo2:TIBQuery;
  altura,largura:Currency;
begin
     Result:=True;
     
     if(ObjParametroGlobal.ValidaParametro('INTEGRA��O CORTE CERTO')=false)
     then Exit
     else
     begin
        if(ObjParametroGlobal.Get_Valor='NAO')
        then Exit;
     end;

     Result:=False;

     try
       query_arquivo:=TIBQuery.Create(nil);
       query_arquivo.Database:= FDataModulo.IBDatabase;
       query_arquivo2:=TIBQuery.Create(nil);
       query_arquivo2.Database:= FDataModulo.IBDatabase;
     except
       Exit;
     end;


     //Criando diret�rio Corte Certo junto ao exe do sistema
     Dir:=PegaPathSistema+'CorteCerto\';
     if not DirectoryExists(Dir) then
     if not ForceDirectories(Dir) then
     begin
          MensagemErro('Erro ao criar diret�rio '+Dir);
          exit;
     end;

     //Criando diret�rio do dia de hj
     Dir:=Dir+(troca(DateToStr(Now),'/','.'));
     if not DirectoryExists(Dir) then
     if not ForceDirectories(Dir) then
     begin
          MensagemErro('Erro ao criar diret�rio '+Dir);
          exit;
     end;

     //Criando Arquivo para o pedido
     Dir:=Dir+'\'+lbCodigoPedido.Caption+'-'+lbNomeCliente.Caption+'.txt';
     AssignFile ( Arquivo, Dir );
     Rewrite ( Arquivo );

     try
       query_arquivo.Close;
       query_arquivo.SQL.Clear;
       query_arquivo.sql.text  :=
       'select c.codigo,''1'' as quantidade, cpp.largura,cpp.altura,pp.observacao as materialpeca, '+
       'c.referencia||''-''||c.descricao as nomepeca,pp.pedido||''-''||v.descricao as observacao, '+
       ' ''obs1'' as obs1,''obs2'' as obs2,''obs3'' as obs3,''obs4'' as obs4,''obs5'' as obs5,''obs6'' as obs6, '+
       ' ''obs7'' as obs7,''obs8'' as obs8,''obs9'' as obs9,''obs10'' as obs10,pp.projeto '+
       'from tabpedido_proj pp '+
       'join tabcomponente_pp cpp on cpp.pedidoprojeto=pp.codigo '+
       'join tabvidro_pp vpp on vpp.pedidoprojeto=pp.codigo '+
       'join tabvidrocor vc on vc.codigo=vpp.vidrocor '+
       'join tabvidro v on v.codigo=vc.vidro '+
       'join tabcomponente c on c.codigo=cpp.componente '+
       'where pp.pedido='+lbCodigoPedido.Caption;

        query_arquivo.Open;

        while not query_arquivo.Eof do
        begin
            query_arquivo2.Close;
            query_arquivo2.SQL.Clear;
            query_arquivo2.sql.text  :=
            'select cp.folgalargura from tabcomponente_proj cp where projeto='+query_arquivo.Fields[17].AsString+' '+
            'and componente='+query_arquivo.Fields[0].AsString;
            query_arquivo2.open;
            largura:= query_arquivo.Fields[2].AsCurrency-query_arquivo2.Fields[0].AsCurrency;

            query_arquivo2.Close;
            query_arquivo2.SQL.Clear;
            query_arquivo2.sql.text  :=
            'select cp.folgaaltura from tabcomponente_proj cp where projeto='+query_arquivo.Fields[17].AsString+' '+
            'and componente='+query_arquivo.Fields[0].AsString;
            query_arquivo2.open;
            altura:= query_arquivo.Fields[3].AsCurrency-query_arquivo2.Fields[0].AsCurrency;

            Write( Arquivo,query_arquivo.Fields[0].AsString+';');
            Write( Arquivo,query_arquivo.Fields[1].AsString+';');
            Write( Arquivo,CurrToStr(largura)+';');
            Write( Arquivo,CurrToStr(altura)+';');
            Write( Arquivo,query_arquivo.Fields[4].AsString+';');
            Write( Arquivo,query_arquivo.Fields[5].AsString+';');
            Write( Arquivo,query_arquivo.Fields[6].AsString+';');
            Write( Arquivo,query_arquivo.Fields[7].AsString+';');
            Write( Arquivo,query_arquivo.Fields[8].AsString+';');
            Write( Arquivo,query_arquivo.Fields[9].AsString+';');
            Write( Arquivo,query_arquivo.Fields[10].AsString+';');
            Write( Arquivo,query_arquivo.Fields[11].AsString+';');
            Write( Arquivo,query_arquivo.Fields[12].AsString+';');
            Write( Arquivo,query_arquivo.Fields[13].AsString+';');
            Write( Arquivo,query_arquivo.Fields[14].AsString+';');
            Write( Arquivo,query_arquivo.Fields[15].AsString+';');
            Write( Arquivo,lbnomecliente.caption+';');

            WriteLn ( Arquivo);
            query_arquivo.Next;
        end;

     finally
        FreeAndNil(query_arquivo);
     end;

     CloseFile ( Arquivo );
     Result:=True;
end;

function TFPEDIDO.GravaMedidasPedidoReplicado(ProjetoPedidoantigo:string;ProjetoPedido:string):Boolean;
begin

end;

procedure TFPEDIDO.rbsimm2Click(Sender: TObject);
begin
    rbSim.Checked:=False;
    rbNao.Checked:=True;

   edtquantidadeservico.Text := '';
    //edtLargServ.Enabled:=True;
    //edtAltServ.Enabled:=True;
    edtQuantidadeServico.enabled:=False;
    //edtAltServ.Color:=clWhite;
    //edtLargServ.Color:=clWhite;
    edtQuantidadeServico.Color:=cl3DLight;

    edtAltServ.Visible:=True;;
    edtLargServ.Visible:=True;
    lblAltServ.Visible:=True;
    lblLargServ.Visible:=True;
end;

procedure TFPEDIDO.rbnaom2Click(Sender: TObject);
begin
   edtAltServ.text := '';
    edtLargServ.text := '';
    edtquantidadeservico.Text := '';
    //edtLargServ.Enabled:=False;
    //edtAltServ.Enabled:=False;
    edtAltServ.Visible:=False;
    edtLargServ.Visible:=False;
    lblAltServ.Visible:=False;
    lblLargServ.Visible:=False;
    edtQuantidadeServico.Enabled:=True;
    //edtAltServ.Color:=cl3DLight;
    //edtLargServ.Color:=cl3DLight;
    edtQuantidadeServico.Color:=clWhite;

   { edtquantidadeservico.Left := 4;
    edtValorServico_PP.Left := 96;
    lb74.Left := 4;
    lb73.Left := 96;   }
end;

procedure TFPEDIDO.edtValorFerragem_PPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      if(Key= 13) then
      begin
         btGravarFerragem_PPClick(Sender);
      end;
end;

procedure TFPEDIDO.edtValorPerfilado_PPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      if(Key= 13) then
      begin
        btGravarPerfilado_PPClick(Sender);
      end;
end;

procedure TFPEDIDO.edtValorVidro_PP2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      if(Key= 13) then
      begin
        btBtGravarVidro_PPClick(Sender);
      end;
end;

procedure TFPEDIDO.edtValorKitBox_PPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      if(Key= 13) then
      begin
        btGravarKitBox_PPClick(Sender);
      end;
end;

procedure TFPEDIDO.edtValorServico_PPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
       if(Key= 13) then
      begin
        btGravarServico_PPClick(Sender);
      end;
end;

procedure TFPEDIDO.edtComandoPersiana_PPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
       if(Key= 13) then
      begin
        btGravarPersiana_PPClick(Sender);
      end;
end;

procedure TFPEDIDO.edtValorDiverso_PPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      if(Key= 13) then
      begin
        btGravarDiverso_PPClick(Sender);
      end;
end;

procedure TFPEDIDO.edtcomplementovidro_VLKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      if(Key= 13) then
      begin
        Btgravar_VLClick(Sender);
      end;
end;

function TFPEDIDO.AtualizaPrecosNovoPrazoPedido(pPedido:string):boolean;
var
  percentual:Currency;
begin
  {OBSERVA��O IMPORTANTE
  Como o objeto j� foi preenchido, n�o posso alterar nada, ent�o preciso
  fazer qualquer altera��o diretamente nos registros do banco de dados}

  //ao gravar o pedido precisa verificar se o prazo de pgto mudou, caso positivo
  //� preciso recalcular os valores dos materiais( tabcomponente_pp ... tabvidro_pp etc) e tabpedido_proj

  Result := true;
  //a forma do retorno muda um pouco nesse metodo. comente ser� false caso entre no calculo e de algum tipo
  //de erro. Qualquer outra situa��o ser� considero true

  if (ObjParametroGlobal.ValidaParametro('MODO DE ACRESCIMO/DESCONTO UTILIZANDO A FORMA DE PAGAMENTO')) then
   if (ObjParametroGlobal.Get_Valor = '2') and ( StrToCurrDef( Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorFinal, 0 ) > 0 ) then
     if ((get_campoTabela('prazopagamento','codigo','tabpedido',pPedido) <> Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrazoPagamento.Get_CODIGO) and
        (trim(pPedido) <> ''))then
     begin
        if (MensagemPergunta('Os valores ser�o reajustados para os valores praticados atualmente. Deseja continuar?') = mrno) then
          exit;
          
        //recalcular o valor dos produtos de acordo com o novo prazo de pagamento
        Result := false;
        try
          percentual := StrToCurr( get_campoTabela('CREDITO_DEBITO', 'CODIGO', 'TABPRAZOPAGAMENTO', Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrazoPagamento.Get_CODIGO ) );
        except
          percentual := 0;
        end;

        if not( Self.ObjPedidoObjetos.AtualizaValorPedidoProjetosTodos( pPedido, CurrToStr(percentual) ) ) then
        begin
          MensagemErro('N�o foi poss�vel alterar valores dos itens de acordo com o novo prazo');
          Exit;
        end;
        Result := True;
     end;
end;

function TFPEDIDO.AtualizaPrecosNovoPrazoProjeto(pPedidoProjeto:string):boolean;
var
  percentual:Currency;
begin
  {OBSERVA��O IMPORTANTE
  Como o objeto j� foi preenchido, n�o posso alterar nada, ent�o preciso
  fazer qualquer altera��o diretamente nos registros do banco de dados}

  Result := true;
  //a forma do retorno muda um pouco nesse metodo. comente ser� false caso entre no calculo e de algum tipo
  //de erro. Qualquer outra situa��o ser� considero true

  if (ObjParametroGlobal.ValidaParametro('MODO DE ACRESCIMO/DESCONTO UTILIZANDO A FORMA DE PAGAMENTO')) then
   if (ObjParametroGlobal.Get_Valor = '2') and ( StrToCurrDef( Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorFinal, 0 ) > 0 ) then
   begin
      //recalcular o valor dos produtos de acordo com o novo prazo de pagamento
      Result := false;
      try
        percentual := StrToCurr( get_campoTabela('CREDITO_DEBITO', 'CODIGO', 'TABPRAZOPAGAMENTO', Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrazoPagamento.Get_CODIGO ) );
      except
        percentual := 0;
      end;
     
      if not( Self.ObjPedidoObjetos.AtualizaValorPedidoProjeto( pPedidoProjeto, CurrToStr(percentual) ) ) then
      begin
        MensagemErro('N�o foi poss�vel alterar valores dos itens de acordo com o novo prazo');
        Exit;
      end;
      Result := True;
   end;
end;

procedure TFPEDIDO.VincularNFe1Click(Sender: TObject);
var
  ObjNotaFiscal:TObjNotafiscalObjetos;
begin
  if ( UpperCase(lbStatusPedido.Caption) <> 'PEDIDO CONCLU�DO') then
  begin
    MensagemErro('O pedido deve estar conclu�do');
    exit;
  end;
  ObjNotaFiscal := TObjNotafiscalObjetos.Create(Self);
  try
    if not ObjNotaFiscal.vinculaNFe_ao_pedido(xmldcmnt1, edtCliente.text, lbCodigoPedido.caption ) then
      MensagemErro('N�o foi poss�vel vincular a NF-e ao pedido.');
  finally
    ObjNotaFiscal.Free;
  end;
end;

procedure TFPEDIDO.edtMaterial_PPExit(Sender: TObject);
var
  PporcentagemAcrescimo,pvalorvenda : Currency;
begin
  if (ObjParametroGlobal.ValidaParametro('MODO DE ACRESCIMO/DESCONTO UTILIZANDO A FORMA DE PAGAMENTO')) then
  begin
    {Aten��o, ao gravar � realizado uma verifica��o pois caso alterar o prazo, dever� alterar
    os valores dos itens j� inseridos. verificar no Gravar
    }

    if (ObjParametroGlobal.Get_Valor = '2') then //1=no titulo
    begin

      try
        Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrazoPagamento.LocalizaCodigo(Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrazoPagamento.Get_CODIGO);
        Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrazoPagamento.TabelaparaObjeto;

        if(strtocurr(Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrazoPagamento.get_CREDITO_DEBITO) <> 0) then
        begin
          try
            PporcentagemAcrescimo := strtocurr(Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.PrazoPagamento.get_CREDITO_DEBITO);
          except
            PporcentagemAcrescimo := 0;
          end;

          try
            pvalorvenda := strtofloat(TEdit(Sender).Text);
            Pvalorvenda := pvalorvenda + ((pvalorvenda * pporcentagemacrescimo) / 100);
            TEdit(Sender).Text := floattostr(pvalorvenda);
          except

          end;
        end;
      except

      end;
    end;

  end;
end;

function TFPEDIDO.utilizaDescontoAcrescimopeloPrazo: Boolean;
begin
  Result := false;
  if (ObjParametroGlobal.ValidaParametro('MODO DE ACRESCIMO/DESCONTO UTILIZANDO A FORMA DE PAGAMENTO')) then
    if (ObjParametroGlobal.Get_Valor = '2') then //1=no titulo
      Result := True;
end;

procedure TFPEDIDO.posicionaComboPrazo(pCodigoPrazo: String);
var
  cont : Integer;
begin
  for cont:=0 to comboprazocodigo.items.Count-1 do
  begin
    if comboprazocodigo.Items[cont]=pCodigoPrazo then
    begin
      comboprazocodigo.itemindex:=cont;
      comboprazonome.itemindex:=cont;
      break;
    end
    else
    begin
      comboprazocodigo.itemindex:=-1;
      comboprazonome.itemindex:=-1;
    end;
  end;
end;

procedure TFPEDIDO.DATAENTRAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = vk_space) then
    self.edtDataEntrega.Text := formatdatetime('dd/mm/yyyy',now);
end;

procedure TFPEDIDO.LiberaVendaCupom;
var
  Cupom,auxiliar : string;
begin
  //chamada ao m�todo de liberar venda para emiss�o de cupom fiscal
  if(lbCodigoPedido.Caption = '') then
    exit;

  if(lbNumTitulo.Caption = '') then
    Exit;

  if((UpperCase(lbCupom.Caption) <> 'SEM CUPOM') and (UpperCase(lbCupom.Caption) <> 'AG. CUPOM')) then
    Exit;
  {
  if(get_campoTabela('GERACUPOM','codigo','TABPEDIDO',self.lbCodigoPedido.Caption,'') = 'S') then
  begin
    lbtipodocumento.Caption := 'Cupom:';
    auxiliar := get_campoTabela('NUMERONFCe','codigo','TABPEDIDO',lbCodigoPedido.Caption,'');
    Cupom := get_campoTabela('NUMEROCUPOM','codigo','TABPEDIDO',lbCodigoPedido.Caption,'');
    if(Trim(Cupom) <> '') then
      lbCupom.Caption := Cupom
    else
      if(Trim(auxiliar) <> '') then
      begin
        lbCupom.Caption := auxiliar;
        lbtipodocumento.Caption := 'NFC-e:';
      end
      else
      begin
        lbCupom.Caption := 'Ag. Cupom';
        MensagemAviso('Aguardando emiss�o de Cupom Fiscal/ NFC-e');
      end;

    Exit;
  end;
  }
  if(UpperCase(lbCupom.Caption) = 'AG. CUPOM') then
  begin
    MensagemAviso('Aguardando emiss�o de Cupom Fiscal/ NFC-e');
    exit;
  end;

  if AtualizaLabelsCupom then
    exit;

  if not(ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.AtualizaGeraCupom(lbCodigoPedido.caption)) then
  begin
    MensagemErro('N�o foi poss�vel disponibilizar venda para emiss�o de Cupom Fiscal / NFC-e');
    Exit;
  end;

  if(get_campoTabela('GERACUPOM','codigo','TABPEDIDO',self.lbCodigoPedido.Caption,'') = 'S') then
    lbCupom.Caption := 'Ag. Cupom';
end;

procedure TFPEDIDO.lbgerarCupomClick(Sender: TObject);
begin
  LiberaVendaCupom;
end;

function TFPEDIDO.AtualizaLabelsCupom:boolean;
var
  cupom,auxiliar,geracupom:string;
begin
  Result := False;
  lbtipodocumento.Caption := 'Cupom:';
  lbCupom.Caption := 'Sem Cupom';
  geracupom := get_campoTabela('GERACUPOM','codigo','TABPEDIDO',self.lbCodigoPedido.Caption,'');
  if( ( geracupom = 'A') or ( geracupom = 'S') ) then
  begin
    lbCupom.Caption := 'Ag. Cupom';
    auxiliar := get_campoTabela('NUMERONFCe','codigo','TABPEDIDO',lbCodigoPedido.Caption,'');
    Cupom := get_campoTabela('NUMEROCUPOM','codigo','TABPEDIDO',lbCodigoPedido.Caption,'');
    if(Trim(Cupom) <> '') then
      lbCupom.Caption := Cupom
    else
      if(Trim(auxiliar) <> '') then
      begin
        lbCupom.Caption := auxiliar;
        lbtipodocumento.Caption := 'NFC-e:';
      end;

    Result := true;
  end;

end;

procedure TFPEDIDO.getValorServicos(codigo:String);
var
  qery:TIBQuery;
begin

  if (codigo='') then
  begin
    lbValorServico.Caption := '0,00';
    Exit;
  end;

  qery := TIBQuery.Create(nil);

  try

    qery.Database := FDataModulo.IBDatabase;
    qery.SQL.Text :=
    'select SUM(spp.valorfinal) as total_servicos '+
    'from tabservico_pp spp '+
    'where spp.pedidoprojeto in( '+
    'select pp.codigo '+
    'from tabpedido_proj pp '+
    'where pp.pedido = :codigo)';

    qery.Params[0].AsString := Codigo;
    qery.Open;
    if qery.RecordCount > 0 then
      lbValorServico.Caption := formata_valor(qery.Fields[0].AsCurrency)
    else
      lbValorServico.Caption := '0,00';


  finally
    FreeAndNil(qery);
  end;

end;



procedure TFPEDIDO.edtOperacaoExit(Sender: TObject);
begin

  if (edtOperacao.Text <> '') then
    lbOperacao.caption := get_campoTabela('nome','codigo','TABOPERACAONF',edtOperacao.Text);

end;

procedure TFPEDIDO.edtOperacaoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

   if (key <> vk_f9) then
     exit;

   Try

      Fpesquisalocal:=Tfpesquisa.create(Nil);
      if (FpesquisaLocal.PreparaPesquisa('select * from taboperacaonf' ,'Pesquisa de Opera��o',Nil)=True) Then
      Begin

        If (FpesquisaLocal.showmodal=mrok) Then
        Begin

           TEdit(Sender).text := FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;

           If TEdit(Sender).text <> ''  Then
            lbOperacao.caption := FpesquisaLocal.QueryPesq.fieldbyname('nome').AsString;

        End;

      End;

   Finally
      FreeandNil(FPesquisaLocal);
   End;

end;

procedure TFPEDIDO.lbOperacaoMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFPEDIDO.lbOperacaoMouseLeave(Sender: TObject);
begin
 TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFPEDIDO.lbOperacaoClick(Sender: TObject);
begin

  if not Assigned(FOPERACAONF) then
    Application.CreateForm(TFOPERACAONF,FOPERACAONF);

  FOPERACAONF.Tag := StrToIntDef( edtOperacao.text,0 );
  FOPERACAONF.ShowModal;
  FreeAndNil(FOPERACAONF);

end;

end.
