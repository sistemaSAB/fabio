unit UobjPEDIDOCOMPRA;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,uobjfornecedor,UopcaoRel;

Type
   TObjPEDIDOCOMPRA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               Fornecedor:TOBJFORNECEDOR;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);
                
                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;

                Function Get_CodigoManual: string;

                Procedure Submit_CodigoManual_2(parametro: string);
                Function Get_CodigoManual_2: string;

                Procedure Submit_CodigoManual_1(parametro: string);
                Function Get_CodigoManual_1: string;

                Procedure Submit_Data(parametro: string);
                Function Get_Data: string;
                Procedure Submit_Concluido(parametro: string);
                Function Get_Concluido: string;
                Procedure Submit_Observacao(parametro: String);
                Function Get_Observacao: String;

                procedure EdtFornecedorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFornecedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtFornecedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
                
//CODIFICA DECLARA GETSESUBMITS


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               CodigoManual_2:string;
               CodigoManual_1:string;
               CodigoManual:string;
               Data:string;
               Concluido:string;
               Observacao:String;
//CODIFICA VARIAVEIS PRIVADAS






               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls
,uFORNECEDOR
//USES IMPLEMENTATION

, UMenuRelatorios;


{ TTabTitulo }


Function  TObjPEDIDOCOMPRA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.codigomanual:=fieldbyname('codigomanual').asstring;
        Self.codigomanual_1:=fieldbyname('codigomanual_1').asstring;
        Self.codigomanual_2:=fieldbyname('codigomanual_2').asstring;
        Self.Data:=fieldbyname('Data').asstring;
        Self.Observacao:=fieldbyname('Observacao').asstring;

        If(FieldByName('Fornecedor').asstring<>'')
        Then Begin
                 If (Self.Fornecedor.LocalizaCodigo(FieldByName('Fornecedor').asstring)=False)
                 Then Begin
                          Messagedlg('Fornecedor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Fornecedor.TabelaparaObjeto;
        End;
        Self.Concluido:=fieldbyname('Concluido').asstring;
//CODIFICA TABELAPARAOBJETO





        result:=True;
     End;
end;


Procedure TObjPEDIDOCOMPRA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('codigomanual_1').asstring:=Self.codigomanual_1;
        ParamByName('codigomanual_2').asstring:=Self.codigomanual_2;
        ParamByName('Data').asstring:=Self.Data;
        ParamByName('Fornecedor').asstring:=Self.Fornecedor.GET_CODIGO;
        ParamByName('Concluido').asstring:=Self.Concluido;
        ParamByName('Observacao').asstring:=Self.Observacao;
//CODIFICA OBJETOPARATABELA





  End;
End;

//***********************************************************************

function TObjPEDIDOCOMPRA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPEDIDOCOMPRA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        codigomanual_1:='';
        codigomanual:='';
        codigomanual_2:='';
        Data:='';
        Fornecedor.ZerarTabela;
        Concluido:='';
        Observacao:='';
//CODIFICA ZERARTABELA





     End;
end;

Function TObjPEDIDOCOMPRA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      If (Data='')
      Then Mensagem:=mensagem+'/Data';
      If (Fornecedor.get_Codigo='')
      Then Mensagem:=mensagem+'/Fornecedor';
      If (Concluido='')
      Then Mensagem:=mensagem+'/Conclu�do';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPEDIDOCOMPRA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Fornecedor.LocalizaCodigo(Self.Fornecedor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Fornecedor n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjPEDIDOCOMPRA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.Fornecedor.Get_Codigo<>'')
        Then Strtoint(Self.Fornecedor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Fornecedor';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPEDIDOCOMPRA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.Data);
     Except
           Mensagem:=mensagem+'/Data';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPEDIDOCOMPRA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
      If not( (CONCLUIDO='S') OR (CONCLUIDO='N'))
      Then Mensagem:=mensagem+'/Valor Inv�lido para o campo Conclu�do';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPEDIDOCOMPRA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PEDIDOCOMPRA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,codigomanual_1,codigomanual_2,codigomanual,Data,Fornecedor,Concluido,observacao');
           SQL.ADD(' from  TabPedidoCompra');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPEDIDOCOMPRA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPEDIDOCOMPRA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPEDIDOCOMPRA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Fornecedor:=TOBJFORNECEDOR.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabPedidoCompra(CODIGO,codigomanual_1,codigomanual_2,Data');
                InsertSQL.add(' ,Fornecedor,Concluido,observacao)');
                InsertSQL.add('values (:CODIGO,:codigomanual_1,:codigomanual_2,:Data,:Fornecedor,:Concluido,:observacao');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabPedidoCompra set CODIGO=:CODIGO,codigomanual_1=:codigomanual_1,codigomanual_2=:codigomanual_2');
                ModifySQL.add(',Data=:Data,Fornecedor=:Fornecedor,Concluido=:Concluido,observacao=:observacao');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabPedidoCompra where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPEDIDOCOMPRA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPEDIDOCOMPRA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPEDIDOCOMPRA');
     Result:=Self.ParametroPesquisa;
end;

function TObjPEDIDOCOMPRA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Pedido de Compra ';
end;


function TObjPEDIDOCOMPRA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPEDIDOCOMPRA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENPEDIDOCOMPRA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPEDIDOCOMPRA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Fornecedor.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPEDIDOCOMPRA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPEDIDOCOMPRA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPedidoCompra.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjPedidoCompra.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;

procedure TObjPedidoCompra.Submit_codigomanual_2(parametro: string);
begin
        Self.codigomanual_2:=Parametro;
end;

function TObjPedidoCompra.Get_codigomanual_2: string;
begin
        Result:=Self.codigomanual_2;
end;


procedure TObjPedidoCompra.Submit_codigomanual_1(parametro: string);
begin
        Self.codigomanual_1:=Parametro;
end;

function TObjPedidoCompra.Get_codigomanual_1: string;
begin
        Result:=Self.codigomanual_1;
end;


procedure TObjPedidoCompra.Submit_Data(parametro: string);
begin
        Self.Data:=Parametro;
end;
function TObjPedidoCompra.Get_Data: string;
begin
        Result:=Self.Data;
end;
procedure TObjPedidoCompra.Submit_Concluido(parametro: string);
begin
        Self.Concluido:=Parametro;
end;
function TObjPedidoCompra.Get_Concluido: string;
begin
        Result:=Self.Concluido;
end;
//CODIFICA GETSESUBMITS


procedure TObjPEDIDOCOMPRA.EdtFornecedorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Fornecedor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Fornecedor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Fornecedor.Get_RazaoSocial;
End;
procedure TObjPEDIDOCOMPRA.EdtFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FFORNECEDOR:TFFORNECEDOR;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FFORNECEDOR:=TFFORNECEDOR.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Fornecedor.Get_Pesquisa,Self.Fornecedor.Get_TituloPesquisa,FFornecedor)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Fornecedor.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Fornecedor.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Fornecedor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FFORNECEDOR);
     End;
end;

procedure TObjPEDIDOCOMPRA.EdtFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FFORNECEDOR:TFFORNECEDOR;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FFORNECEDOR:=TFFORNECEDOR.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Fornecedor.Get_Pesquisa,Self.Fornecedor.Get_TituloPesquisa,FFornecedor)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Fornecedor.RETORNACAMPOCODIGO).asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FFORNECEDOR);
     End;
end;


//CODIFICA EXITONKEYDOWN
procedure TObjPEDIDOCOMPRA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPEDIDOCOMPRA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
          0:Begin
          End;
          End;
     end;

end;



function TObjPEDIDOCOMPRA.Get_Observacao: String;
begin
     Result:=Self.Observacao;
end;

procedure TObjPEDIDOCOMPRA.Submit_Observacao(parametro: String);
begin
     Self.Observacao:=parametro;
end;

function TObjPEDIDOCOMPRA.Get_CodigoManual: string;
begin
     Result:=Self.CodigoManual;
end;



end.



