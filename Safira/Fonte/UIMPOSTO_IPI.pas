unit UIMPOSTO_IPI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjIMPOSTO_IPI,
  jpeg;

type
  TFIMPOSTO_IPI = class(TForm)
    edtST_IPI: TEdit;
    lbLbST: TLabel;
    lbNomeST: TLabel;
    edtCodigoEnquadramento_IPI: TEdit;
    lbLbCodigoEnquadramento: TLabel;
    lbLbClasseEnq: TLabel;
    edtClasseEnq_IPI: TEdit;
    lbLbCnpjProdutor: TLabel;
    edtCnpjProdutor_IPI: TEdit;
    edtCodigoSelo_IPI: TEdit;
    lbLbCodigoSelo: TLabel;
    cbbComboTipoCalculo_IPI: TComboBox;
    lbLbTipoCalculo: TLabel;
    edtAliquota_IPI: TEdit;
    lbLbAliquota: TLabel;
    edtQuantidadeTotalUP_IPI: TEdit;
    lbLbQuantidadeTotalUP: TLabel;
    edtFORMULABASECALCULO_IPI: TEdit;
    lbLbFORMULABASECALCULO: TLabel;
    edtFORMULA_VALOR_IMPOSTO_IPI: TEdit;
    lb1: TLabel;
    edtValorUnidade_IPI: TEdit;
    lbLbValorUnidade: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb5: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
//DECLARA COMPONENTES
    procedure EdtST_IPIKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure EdtST_IPIExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtST_IPIDblClick(Sender: TObject);
  private
         ObjIMPOSTO_IPI:TObjIMPOSTO_IPI;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FIMPOSTO_IPI: TFIMPOSTO_IPI;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFIMPOSTO_IPI.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjIMPOSTO_IPI do
    Begin
        Submit_CODIGO(lbCodigo.Caption);
        ST.Submit_codigo(edtST_IPI.text);
        Submit_ClasseEnq(edtClasseEnq_IPI.text);
        Submit_CodigoEnquadramento(edtCodigoEnquadramento_IPI.text);
        Submit_CnpjProdutor(edtCnpjProdutor_IPI.text);
        Submit_CodigoSelo(edtCodigoSelo_IPI.text);
        Submit_TipoCalculo(cbbComboTipoCalculo_IPI.text);

        Submit_Aliquota(edtAliquota_IPI.text);
        Submit_QuantidadeTotalUP(edtQuantidadeTotalUP_IPI.text);
        Submit_ValorUnidade(edtValorUnidade_IPI.text);
        Submit_FORMULABASECALCULO(edtFORMULABASECALCULO_IPI.text);
        Submit_formula_valor_imposto(edtformula_valor_imposto_IPI.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFIMPOSTO_IPI.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjIMPOSTO_IPI do
     Begin
        lbCodigo.Caption:=Get_CODIGO;
        EdtST_IPI.text:=ST.Get_codigo;
        EdtClasseEnq_IPI.text:=Get_ClasseEnq;
        EdtCodigoEnquadramento_IPI.text:=Get_CodigoEnquadramento;
        EdtCnpjProdutor_IPI.text:=Get_CnpjProdutor;
        EdtCodigoSelo_IPI.text:=Get_CodigoSelo;
        cbbcomboTipoCalculo_IPI.text:=Get_TipoCalculo;

        EdtAliquota_IPI.text:=Get_Aliquota;
        EdtQuantidadeTotalUP_IPI.text:=Get_QuantidadeTotalUP;
        EdtValorUnidade_IPI.text:=Get_ValorUnidade;

        EdtFORMULABASECALCULO_IPI.text:=Get_FORMULABASECALCULO;
        Edtformula_valor_imposto_IPI.text:=Get_formula_valor_imposto;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFIMPOSTO_IPI.TabelaParaControles: Boolean;
begin
     If (Self.ObjIMPOSTO_IPI.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFIMPOSTO_IPI.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);


     Try
        Self.ObjIMPOSTO_IPI:=TObjIMPOSTO_IPI.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(imgrodape,'RODAPE');

end;

procedure TFIMPOSTO_IPI.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjIMPOSTO_IPI=Nil)
     Then exit;

If (Self.ObjIMPOSTO_IPI.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjIMPOSTO_IPI.free;
end;

procedure TFIMPOSTO_IPI.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFIMPOSTO_IPI.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     //desab_botoes(Self);

     lbCodigo.Caption:='0';
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjIMPOSTO_IPI.status:=dsInsert;
     edtST_IPI.setfocus;
     btnovo.Visible:=false;
     btalterar.Visible:=False;
     btexcluir.Visible:=false;
     btrelatorios.Visible:=false;
     btopcoes.Visible:=false;
     btsair.Visible:=False;
     btpesquisar.visible:=False;


end;


procedure TFIMPOSTO_IPI.btalterarClick(Sender: TObject);
begin
    If (Self.ObjIMPOSTO_IPI.Status=dsinactive) and (lbCodigo.Caption<>'')
    Then Begin
                MensagemAviso('N�o � permitido a altera��o de Impostos');
                (*habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjIMPOSTO_IPI.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtST_IPI.setfocus;*)
                btnovo.Visible:=false;
                 btalterar.Visible:=False;
                 btexcluir.Visible:=false;
                 btrelatorios.Visible:=false;
                 btopcoes.Visible:=false;
                 btsair.Visible:=False;
                 btpesquisar.visible:=False;

    End;


end;

procedure TFIMPOSTO_IPI.btgravarClick(Sender: TObject);
begin

     If Self.ObjIMPOSTO_IPI.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjIMPOSTO_IPI.salvar(true)=False)
     Then exit;

     lbCodigo.Caption:=Self.ObjIMPOSTO_IPI.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
      btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;

     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFIMPOSTO_IPI.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjIMPOSTO_IPI.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (Self.ObjIMPOSTO_IPI.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjIMPOSTO_IPI.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFIMPOSTO_IPI.btcancelarClick(Sender: TObject);
begin
     Self.ObjIMPOSTO_IPI.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
      btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;


end;

procedure TFIMPOSTO_IPI.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFIMPOSTO_IPI.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjIMPOSTO_IPI.Get_pesquisa,Self.ObjIMPOSTO_IPI.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjIMPOSTO_IPI.status<>dsinactive
                                  then exit;

                                  If (Self.ObjIMPOSTO_IPI.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjIMPOSTO_IPI.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFIMPOSTO_IPI.LimpaLabels;
begin
//LIMPA LABELS
     LbNomeST.Caption:='';
     lbCodigo.Caption:='';
end;

procedure TFIMPOSTO_IPI.FormShow(Sender: TObject);
begin
    // PegaCorForm(Self);
end;

//CODIFICA ONKEYDOWN E ONEXIT
procedure TFIMPOSTO_IPI.EdtST_IPIKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjIMPOSTO_IPI.edtSTkeydown(sender,key,shift,lbnomeST);
end;


procedure TFIMPOSTO_IPI.EdtST_IPIExit(Sender: TObject);
begin
    ObjIMPOSTO_IPI.edtSTExit(sender,lbnomeST);
end;

{r4mr}
procedure TFIMPOSTO_IPI.edtST_IPIDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  EdtST_IPIKeyDown(Sender, Key, Shift);
end;

end.

{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjIMPOSTO_IPI.OBJETO.Get_Pesquisa,Self.ObjIMPOSTO_IPI.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjIMPOSTO_IPI.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjIMPOSTO_IPI.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjIMPOSTO_IPI.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
