unit UFinalizaComissaoColocador;

interface

uses
  db,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls,UobjCOMISSAOCOLOCADOR, Grids,uObjpedidoProjetoPedidoCompra;

type
  TFFinalizaComissaoColocador = class(TForm)
    Panel1: TPanel;
    edtcolocador: TEdit;
    edtpedido: TEdit;
    edtpedidoprojeto: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    btfiltrar: TButton;
    StringGrid: TStringGrid;
    Label4: TLabel;
    edtpedidoprojeto_pedidocompra: TEdit;
    btselecionatodas: TButton;
    btprocessar: TButton;
    RadioFinalizado: TRadioButton;
    RadioAberto: TRadioButton;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtpedidoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtpedidoprojetoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btfiltrarClick(Sender: TObject);
    procedure edtpedidoprojeto_pedidocompraKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edtcolocadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure StringGridDblClick(Sender: TObject);
    procedure StringGridKeyPress(Sender: TObject; var Key: Char);
    procedure btselecionatodasClick(Sender: TObject);
    procedure btprocessarClick(Sender: TObject);
    procedure edtcolocadorDblClick(Sender: TObject);
    procedure edtpedidoDblClick(Sender: TObject);
    procedure edtpedidoprojetoDblClick(Sender: TObject);
    procedure edtpedidoprojeto_pedidocompraDblClick(Sender: TObject);
  private
    { Private declarations }
    ObjComissaoColocador:TObjCOMISSAOCOLOCADOR;
    ObjpedidoProjetoPedidoCompra:TObjpedidoprojetopedidocompra;
  public
    { Public declarations }
  end;

var
  FFinalizaComissaoColocador: TFFinalizaComissaoColocador;

implementation

uses UessencialGlobal, UDataModulo, UFiltraImp, UMostraBarraProgresso;

{$R *.dfm}

procedure TFFinalizaComissaoColocador.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
     FormKeyPress13(sender,key);
end;

procedure TFFinalizaComissaoColocador.FormShow(Sender: TObject);
begin
    desab_botoes(self);
    limpaedit(Self);

    Try
        ObjComissaoColocador:=TObjCOMISSAOCOLOCADOR.create(self);
        ObjpedidoProjetoPedidoCompra:=TObjpedidoprojetopedidocompra.create;

        if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSAR M�DULO DE FINALIZA��O PEDIDO PROJETO NA COMISS�O DO COLOCADOR')=True)
        then habilita_botoes(Self);
        
    Except
          MensagemErro('Erro na tentativa de criar a comiss�o do colocador');
          exit;
    End;

end;

procedure TFFinalizaComissaoColocador.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     if (Self.ObjComissaoColocador<>nil)
     Then Self.ObjComissaoColocador.Free;

     if (Self.ObjpedidoProjetoPedidoCompra<>nil)
     Then Self.ObjpedidoProjetoPedidoCompra.free; 
end;

procedure TFFinalizaComissaoColocador.edtpedidoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjComissaoColocador.PedidoProjeto.EdtPedidoKeyDown(sender,key,shift,nil);
end;

procedure TFFinalizaComissaoColocador.edtpedidoprojetoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     if (edtpedido.text='')
     Then ObjComissaoColocador.EdtPedidoProjetoKeyDown(sender,key,shift,nil)
     Else ObjComissaoColocador.edtpedidoprojetoKeyDown(sender,key,shift,nil,edtpedido.text);
end;

procedure TFFinalizaComissaoColocador.btfiltrarClick(Sender: TObject);
var
somenteaberto:boolean;
begin
     if (RadioFinalizado.Checked)
     Then SomenteAberto:=False
     Else SomenteAberto:=True;
     
     ObjComissaoColocador.RetornaPedidosProjetosNaoFinalizados(StringGrid,edtcolocador.Text,edtpedido.Text,edtpedidoprojeto.Text,edtpedidoprojeto_pedidocompra.TEXT,somenteaberto);
end;

procedure TFFinalizaComissaoColocador.edtpedidoprojeto_pedidocompraKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     ObjpedidoProjetoPedidoCompra.EdtPedidoProjeto_cadastrados_KeyDown(sender,key,shift);
end;

procedure TFFinalizaComissaoColocador.edtcolocadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjComissaoColocador.PEDIDOPROJETOROMANEIO.Romaneio.EdtColocadorKeyDown(sender,key,shift,nil);
end;

procedure TFFinalizaComissaoColocador.StringGridDblClick(Sender: TObject);
begin
     if (StringGrid.cells[0,StringGrid.row]='')
     Then StringGrid.cells[0,StringGrid.row]:='X'
     Else StringGrid.cells[0,StringGrid.row]:='';

end;

procedure TFFinalizaComissaoColocador.StringGridKeyPress(Sender: TObject;
  var Key: Char);
begin

     if key=#13
     Then Begin
               Self.StringGrid.SetFocus;
               Self.StringGridDblClick(sender);
     End;
     
end;

procedure TFFinalizaComissaoColocador.btselecionatodasClick(Sender: TObject);
var
cont:integer;
begin

     For cont:=1 to StringGrid.RowCount-1 do
     Begin
          StringGrid.Row:=cont;
          StringGrid.Cells[0,cont]:='';
          StringGridDblClick(sender);
     End;
     MensagemAviso('Conclu�do');

end;

procedure TFFinalizaComissaoColocador.btprocessarClick(Sender: TObject);
var
pdata:string;
cont:integer;
begin

     if (mensagempergunta('Certeza que deseja processar os pedidos projetos selecionados?')=mrno)
     then exit;


     With Ffiltroimp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          edtgrupo01.EditMask:=MascaraData;
          LbGrupo01.caption:='Data de Finaliza��o';

          showmodal;

          if (tag=0)
          Then exit;

          if (Validadata(1,pdata,false)=false)
          then exit;

     End;


Try

     FMostraBarraProgresso.ConfiguracoesIniciais(StringGrid.RowCount-1,0);
     FMostraBarraProgresso.Lbmensagem.caption:='Finalizando pedidos projetos...';
     FMostraBarraProgresso.btcancelar.Visible:=True;

     For cont:=1 to StringGrid.RowCount-1 do
     Begin
          //StringGrid.row:=cont;
          
          FMostraBarraProgresso.IncrementaBarra1(1);
          FMostraBarraProgresso.show;

          if ((cont mod 10)=0)
          Then Application.ProcessMessages;



          if (FMostraBarraProgresso.Cancelar)
          Then Begin
                    if (MensagemPergunta('Certeza que deseja cancelar o processo')=mryes)
                    then exit;

                    FMostraBarraProgresso.Cancelar:=False;
          End;

          Try
            if (StringGrid.Cells[0,cont]='X')
            then Begin

                      if (ObjComissaoColocador.LocalizaCodigo(StringGrid.cells[9,cont])=False)
                      then Begin
                                MensagemErro('Comiss�o C�digo '+StringGrid.cells[9,cont]+' n�o localizada');
                                exit;
                      End;

                      ObjComissaoColocador.TabelaparaObjeto;
                      ObjComissaoColocador.Status:=dsedit;


                      if (StringGrid.cells[7,StringGrid.row]<>'S')
                      then Begin
                                ObjComissaoColocador.Submit_Finalizado('S');
                                ObjComissaoColocador.Submit_DataFinalizacao(pdata);
                      End
                      Else Begin
                                ObjComissaoColocador.Submit_Finalizado('N');
                                ObjComissaoColocador.Submit_DataFinalizacao('');
                      End;


                      if (ObjComissaoColocador.salvar(false)=False)
                      Then Begin
                                MensagemErro('Erro na tentativa de finalizar o pedido projeto '+ObjComissaoColocador.PedidoProjeto.Get_Codigo);
                                exit;

                      End;
            End;
          Except
                on e:exception do
                Begin
                     mensagemErro('Erro no pedido projeto '+StringGrid.cells[9,cont]+#13'Erro: '+e.message);
                     exit;
                End;
          End;
          
     End;
     
     FDataModulo.IBTransaction.CommitRetaining;
     FMostraBarraProgresso.close;
     MensagemAviso('Conclu�do');
     btfiltrarClick(sender);

Finally
       FMostraBarraProgresso.btcancelar.Visible:=False;
       FMostraBarraProgresso.close;
       FDataModulo.IBTransaction.RollbackRetaining;

End;


end;

{r4mr}
procedure TFFinalizaComissaoColocador.edtcolocadorDblClick(
  Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtcolocadorKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFFinalizaComissaoColocador.edtpedidoDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtpedidoKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFFinalizaComissaoColocador.edtpedidoprojetoDblClick(
  Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtpedidoprojetoKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFFinalizaComissaoColocador.edtpedidoprojeto_pedidocompraDblClick(
  Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtpedidoprojeto_pedidocompraKeyDown(Sender, Key, Shift);
end;

end.




