object FAlteraPrecoVidro: TFAlteraPrecoVidro
  Left = 508
  Top = 317
  Width = 759
  Height = 241
  AutoSize = True
  Caption = 'Altera'#231#227'o de Pre'#231'o  - VIDRO'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LbPrecoCusto: TLabel
    Left = 4
    Top = 9
    Width = 87
    Height = 13
    Caption = 'Pre'#231'o de Custo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 545
    Top = 0
    Width = 82
    Height = 16
    Caption = 'Pre'#231'o Pago: '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbprecopago: TLabel
    Left = 645
    Top = 0
    Width = 82
    Height = 16
    Alignment = taRightJustify
    Caption = 'Pre'#231'o Pago: '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GroupBoxMargem: TGroupBox
    Left = 4
    Top = 28
    Width = 206
    Height = 81
    Caption = 'Margem'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object LbPorcentagemFornecido: TLabel
      Left = 11
      Top = 36
      Width = 55
      Height = 13
      Caption = 'Fornecido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object LbPorcentagemRetirado: TLabel
      Left = 11
      Top = 56
      Width = 48
      Height = 13
      Caption = 'Retirado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object LbPorcentagemInstalado: TLabel
      Left = 11
      Top = 15
      Width = 53
      Height = 13
      Caption = 'Instalado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 158
      Top = 15
      Width = 12
      Height = 13
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 158
      Top = 36
      Width = 12
      Height = 13
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 158
      Top = 56
      Width = 12
      Height = 13
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object EdtPorcentagemInstalado: TEdit
      Left = 85
      Top = 11
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnExit = EdtPorcentagemInstaladoExit
    end
    object EdtPorcentagemFornecido: TEdit
      Left = 85
      Top = 32
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      OnExit = EdtPorcentagemFornecidoExit
    end
    object EdtPorcentagemRetirado: TEdit
      Left = 85
      Top = 52
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      OnExit = EdtPorcentagemRetiradoExit
    end
  end
  object EdtPrecoCusto: TEdit
    Left = 104
    Top = 7
    Width = 80
    Height = 19
    Ctl3D = False
    MaxLength = 9
    ParentCtl3D = False
    TabOrder = 0
  end
  object GroupBoxPrecoVenda: TGroupBox
    Left = 220
    Top = 29
    Width = 206
    Height = 81
    Caption = 'Pre'#231'o Venda'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object LbPrecoVendaInstalado: TLabel
      Left = 11
      Top = 15
      Width = 53
      Height = 13
      Caption = 'Instalado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object LbPrecoVendaFornecido: TLabel
      Left = 11
      Top = 38
      Width = 55
      Height = 13
      Caption = 'Fornecido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object LbPrecoVendaRetirado: TLabel
      Left = 11
      Top = 61
      Width = 48
      Height = 13
      Caption = 'Retirado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 79
      Top = 15
      Width = 15
      Height = 13
      Caption = 'R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 79
      Top = 38
      Width = 15
      Height = 13
      Caption = 'R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 79
      Top = 61
      Width = 15
      Height = 13
      Caption = 'R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object EdtPrecoVendaInstalado: TEdit
      Left = 115
      Top = 11
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnExit = EdtPrecoVendaInstaladoExit
    end
    object EdtPrecoVendaFornecido: TEdit
      Left = 115
      Top = 34
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      OnExit = EdtPrecoVendaFornecidoExit
    end
    object EdtPrecoVendaRetirado: TEdit
      Left = 115
      Top = 57
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      OnExit = EdtPrecoVendaRetiradoExit
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 125
    Width = 743
    Height = 78
    Align = alBottom
    TabOrder = 3
    object Label8: TLabel
      Left = 396
      Top = 11
      Width = 53
      Height = 13
      Caption = 'Instalado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 396
      Top = 31
      Width = 55
      Height = 13
      Caption = 'Fornecido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label13: TLabel
      Left = 396
      Top = 52
      Width = 48
      Height = 13
      Caption = 'Retirado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Rg_forma_de_calculo_percentual: TRadioGroup
      Left = 553
      Top = 4
      Width = 180
      Height = 33
      Caption = 'Calcular % de Acr'#233'scimo'
      Columns = 2
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        'Custo'
        'Valor Final')
      ParentFont = False
      TabOrder = 6
    end
    object edtinstaladofinal: TEdit
      Left = 458
      Top = 8
      Width = 74
      Height = 19
      Color = 15663069
      Ctl3D = False
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
    end
    object edtfornecidofinal: TEdit
      Left = 458
      Top = 28
      Width = 74
      Height = 19
      Color = 15663069
      Ctl3D = False
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
    end
    object edtretiradofinal: TEdit
      Left = 458
      Top = 49
      Width = 74
      Height = 19
      Color = 15663069
      Ctl3D = False
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
    end
    object btgravar: TButton
      Left = 556
      Top = 46
      Width = 75
      Height = 25
      Caption = '&Gravar'
      TabOrder = 4
      OnClick = btgravarClick
    end
    object btcancelar: TButton
      Left = 636
      Top = 46
      Width = 75
      Height = 25
      Caption = '&Cancelar'
      TabOrder = 5
      OnClick = btcancelarClick
    end
    object GroupBoxVidros: TGroupBox
      Left = 6
      Top = 6
      Width = 364
      Height = 58
      Caption = 'Acr'#233'scimos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object LbPorcentagemAcrescimo: TLabel
        Left = 8
        Top = 15
        Width = 37
        Height = 13
        Caption = '% Cor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbAcrescimoExtra: TLabel
        Left = 127
        Top = 15
        Width = 46
        Height = 13
        Caption = '% Extra'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbPorcentagemAcrescimoFinal: TLabel
        Left = 294
        Top = 15
        Width = 42
        Height = 13
        Caption = '% Final'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 68
        Top = 15
        Width = 49
        Height = 13
        Caption = 'R$ Extra'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 183
        Top = 15
        Width = 50
        Height = 13
        Caption = 'R$ ICMS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 239
        Top = 15
        Width = 47
        Height = 13
        Caption = '% ICMS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object EdtPorcentagemAcrescimo: TEdit
        Left = 9
        Top = 29
        Width = 54
        Height = 19
        Color = 15663069
        Ctl3D = False
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
      end
      object EdtAcrescimoExtra: TEdit
        Left = 127
        Top = 29
        Width = 54
        Height = 19
        Color = 15663069
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        OnExit = EdtAcrescimoExtraExit
      end
      object EdtPorcentagemAcrescimoFinal: TEdit
        Left = 294
        Top = 29
        Width = 54
        Height = 19
        Color = 15663069
        Ctl3D = False
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 5
      end
      object EdtValorExtra: TEdit
        Left = 68
        Top = 29
        Width = 54
        Height = 19
        Color = clMedGray
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        OnExit = EdtValorExtraExit
      end
      object edtacrescimoicms: TEdit
        Left = 183
        Top = 29
        Width = 54
        Height = 19
        Color = clMedGray
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
        OnExit = edtacrescimoicmsExit
      end
      object edtporcentagemacrescimoicms: TEdit
        Left = 239
        Top = 29
        Width = 54
        Height = 19
        Color = 15663069
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 4
        OnExit = edtporcentagemacrescimoicmsExit
      end
    end
  end
end
