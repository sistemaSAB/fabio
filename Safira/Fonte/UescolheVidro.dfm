object FescolheVidro: TFescolheVidro
  Left = 472
  Top = 294
  Width = 874
  Height = 395
  BorderIcons = [biSystemMenu]
  Caption = 'Escolha o Vidro que ser'#225' usado no Projeto Atual'
  Color = clBtnHighlight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object STRGGRID: TStringGrid
    Left = 0
    Top = 67
    Width = 858
    Height = 218
    Align = alClient
    BorderStyle = bsNone
    Ctl3D = False
    FixedCols = 0
    Options = [goVertLine, goHorzLine, goDrawFocusSelected, goRowSizing]
    ParentCtl3D = False
    TabOrder = 0
    OnClick = STRGGRIDClick
    OnDblClick = STRGGRIDDblClick
    OnKeyPress = STRGGRIDKeyPress
    OnKeyUp = STRGGRIDKeyUp
    ColWidths = (
      64
      64
      64
      64
      64)
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 858
    Height = 42
    Align = alTop
    BevelOuter = bvNone
    Color = clWhite
    TabOrder = 1
    object lbnomeformulario: TLabel
      Left = 229
      Top = 9
      Width = 379
      Height = 27
      Align = alCustom
      Alignment = taCenter
      Anchors = [akTop]
      Caption = 'Vidros Dispon'#237'veis para este projeto'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 285
    Width = 858
    Height = 72
    Align = alBottom
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 856
      Height = 70
      Align = alClient
      Stretch = True
    end
    object lbValorTotal: TLabel
      Left = 17
      Top = 20
      Width = 162
      Height = 29
      Caption = 'lbValorTotal'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb1: TLabel
      Left = 713
      Top = 3
      Width = 139
      Height = 34
      Cursor = crHandPoint
      Align = alCustom
      Caption = 'Confirmar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -29
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnClick = btokClick
      OnMouseMove = lb1MouseMove
      OnMouseLeave = lb1MouseLeave
    end
    object lb2: TLabel
      Left = 730
      Top = 37
      Width = 122
      Height = 34
      Cursor = crHandPoint
      Align = alCustom
      Caption = 'Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -29
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnClick = btcancelarClick
      OnMouseMove = lb1MouseMove
      OnMouseLeave = lb1MouseLeave
    end
  end
  object pnl6: TPanel
    Left = 0
    Top = 42
    Width = 858
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    Color = 10643006
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 3
    object lb3: TLabel
      Left = 6
      Top = 3
      Width = 256
      Height = 15
      Align = alCustom
      Alignment = taCenter
      Anchors = [akTop, akRight]
      Caption = 'Escolha um vidro para adicionar ao projeto'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6073854
      Font.Height = -11
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
end
