unit UobjPERFILADO_PROJ;
Interface
Uses forms,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJPERFILADO,UOBJPROJETO
,UPERFILADO;

Type
   TObjPERFILADO_PROJ=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Perfilado:TOBJPERFILADO;
                Projeto:TOBJPROJETO;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;

                Procedure Submit_FormulaAltura(parametro: string);
                Function Get_FormulaAltura: string;
                Procedure Submit_FormulaLargura(parametro: string);
                Function Get_FormulaLargura: string;

                procedure EdtPerfiladoExit(Sender: TObject;Var PEdtCodigo : TEdit;LABELNOME:TLABEL);overload;
                procedure EdtPerfiladoExit(Sender: TObject;Var PEdtCodigo : TEdit;LABELNOME:TLABEL;var kitperfilado:Boolean);overload;
                procedure EdtPerfiladoKeyDown(Sender: TObject; Var PEdtCodigo : TEdit;  var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtProjetoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtPerfiladoKeyDown2(Sender: TObject;var Key: Word;Shift: TShiftState;var LABELNOME:string;var Tedit:string;CodigoProjeto:string);

                Procedure Resgata_Perfilado_Proj(PProjeto : string);
                function VerificaSeJaExisteEstaPerfiladoNesteProjeto(PProjeto,PPerfilado: string): Boolean;

                Function  fieldbyname(PCampo:String):String;overload;
                Procedure fieldbyname(PCampo:String;PValor:String);overload;
                function Replica(PProjeto, PnovoProjeto: string): boolean;

                procedure Submit_AlturaCorte(parametro:String);
                procedure Submit_PosicaoCorte(parametro:string);
                procedure Submit_TamanhoCorte(parametro:String);

                Function Get_AlturaCorte:string;
                Function Get_PosicaoCorte:string;
                Function Get_TamanhoCorte:string;
                procedure Submit_quantidade(parametro:string);
                Function Get_Quantidade:string;
                procedure Submit_Formulaquantidade(parametro:string);
                Function Get_Formulaquantidade:string;
                procedure Submit_Esquadria(parametro:string);
                Function Get_Esquadria:string;


          Private
               Objquery:Tibquery;
               ObjQueryPesquisa:TIBQuery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               Codigo:string;
               FormulaAltura:string;
               FormulaLargura:string;
               ParametroPesquisa:TStringList;
               AlturaCorte:string;
               PosicaoCorte:string;
               TamanhoCorte:string;
               Quantidade:string;
               Formulaquantidade:string;
               Esquadria:string;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;


   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UMostraBarraProgresso;


{ TTabTitulo }


Function  TObjPERFILADO_PROJ.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If(FieldByName('Perfilado').asstring<>'')
        Then Begin
                 If (Self.Perfilado.LocalizaCodigo(FieldByName('Perfilado').asstring)=False)
                 Then Begin
                          Messagedlg('Perfilado N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Perfilado.TabelaparaObjeto;
        End;
        If(FieldByName('Projeto').asstring<>'')
        Then Begin
                 If (Self.Projeto.LocalizaCodigo(FieldByName('Projeto').asstring)=False)
                 Then Begin
                          Messagedlg('Projeto N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Projeto.TabelaparaObjeto;
        End;
        Self.FormulaAltura:=fieldbyname('FormulaAltura').asstring;
        Self.FormulaLargura:=fieldbyname('FormulaLargura').asstring;
//CODIFICA TABELAPARAOBJETO

         self.AlturaCorte:=fieldbyname('alturacorte').AsString;
         self.PosicaoCorte:=fieldbyname('posicaocorte').asstring;
         self.TamanhoCorte:=fieldbyname('tamanhocorte').AsString;
         Self.Quantidade:=fieldbyname('quantidade').AsString;
         self.Formulaquantidade:=fieldbyname('FORMULAQUANTIDADE').AsString;
         Self.Esquadria:=fieldbyname('Esquadria').AsString;

        result:=True;
     End;
end;


Procedure TObjPERFILADO_PROJ.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Perfilado').asstring:=Self.Perfilado.GET_CODIGO;
        ParamByName('Projeto').asstring:=Self.Projeto.GET_CODIGO;
        ParamByName('FormulaAltura').asstring:=Self.FormulaAltura;
        ParamByName('FormulaLargura').asstring:=Self.FormulaLargura;

        ParamByName('alturacorte').AsString:=self.AlturaCorte;
        ParamByName('posicaocorte').AsString:=Self.PosicaoCorte;
        ParamByName('tamanhocorte').AsString:=Self.TamanhoCorte;
        ParamByName('quantidade').AsString:=Self.Quantidade;
        ParamByName('FORMULAQUANTIDADE').AsString:=Self.Formulaquantidade;
        ParamByName('Esquadria').AsString:=Self.Esquadria;
//CODIFICA OBJETOPARATABELA

  End;
End;


Procedure TObjPERFILADO_PROJ.fieldbyname(PCampo:String;PValor:String);
Begin
     PCampo:=Uppercase(pcampo);

     If (Pcampo='CODIGO')
     Then Begin
              Self.Submit_CODIGO(pVALOR);
              exit;
     End;
     If (Pcampo='PERFILADO')
     Then Begin
              Self.PERFILADO.Submit_codigo(pVALOR);
              exit;
     End;
     If (Pcampo='PROJETO')
     Then Begin
              Self.PROJETO.Submit_codigo(pVALOR);
              exit;
     End;
     If (Pcampo='FORMULAALTURA')
     Then Begin
              Self.Submit_FORMULAALTURA(pVALOR);
              exit;
     End;
     If (Pcampo='FORMULALARGURA')
     Then Begin
              Self.Submit_FORMULALARGURA(pVALOR);
              exit;
     End;
end;

Function TObjPERFILADO_PROJ.fieldbyname(PCampo:String):String;
Begin
     PCampo:=Uppercase(pcampo);
     Result:=''; 
     If (Pcampo='CODIGO')
     Then Begin
              Result:=Self.Get_CODIGO;
              exit;
     End;
     If (Pcampo='PERFILADO')
     Then Begin
              Result:=Self.PERFILADO.Get_codigo;
              exit;
     End;
     If (Pcampo='PROJETO')
     Then Begin
              Result:=Self.PROJETO.Get_codigo;
              exit;
     End;
     If (Pcampo='FORMULAALTURA')
     Then Begin
              Result:=Self.Get_FORMULAALTURA;
              exit;
     End;
     If (Pcampo='FORMULALARGURA')
     Then Begin
              Result:=Self.Get_FORMULALARGURA;
              exit;
     End;
end;


//***********************************************************************

function TObjPERFILADO_PROJ.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPERFILADO_PROJ.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Perfilado.ZerarTabela;
        Projeto.ZerarTabela;
        FormulaAltura:='';
        FormulaLargura:='';
        AlturaCorte:='';
        PosicaoCorte:='';
        TamanhoCorte:='';
        quantidade:='';
        FORMULAQUANTIDADE:='';
        Esquadria:='';
//CODIFICA ZERARTABELA





     End;
end;

Function TObjPERFILADO_PROJ.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPERFILADO_PROJ.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Perfilado.LocalizaCodigo(Self.Perfilado.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Perfilado n�o Encontrado!';
      If (Self.Projeto.LocalizaCodigo(Self.Projeto.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Projeto n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjPERFILADO_PROJ.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.Perfilado.Get_Codigo<>'')
        Then Strtoint(Self.Perfilado.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Perfilado';
     End;
     try
        If (Self.Projeto.Get_Codigo<>'')
        Then Strtoint(Self.Projeto.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Projeto';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPERFILADO_PROJ.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPERFILADO_PROJ.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPERFILADO_PROJ.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PERFILADO_PROJ vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Perfilado,Projeto,FormulaAltura,FormulaLargura,alturacorte,tamanhocorte,posicaocorte,quantidade,FORMULAQUANTIDADE');
           SQL.ADD(' ,Esquadria from  TabPerfilado_Proj');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPERFILADO_PROJ.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPERFILADO_PROJ.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPERFILADO_PROJ.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjQueryPesquisa;
        
        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Perfilado:=TOBJPERFILADO.create;
        Self.Projeto:=TOBJPROJETO.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabPerfilado_Proj(Codigo,Perfilado,Projeto');
                InsertSQL.add(' ,FormulaAltura,FormulaLargura,alturacorte,tamanhocorte,posicaocorte,quantidade,FORMULAQUANTIDADE,Esquadria)');
                InsertSQL.add('values (:Codigo,:Perfilado,:Projeto,:FormulaAltura,:FormulaLargura,:alturacorte,:tamanhocorte,:posicaocorte,:quantidade,:FORMULAQUANTIDADE,:Esquadria');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabPerfilado_Proj set Codigo=:Codigo,Perfilado=:Perfilado');
                ModifySQL.add(',Projeto=:Projeto,FormulaAltura=:FormulaAltura,FormulaLargura=:FormulaLargura');
                ModifySQL.add(',alturacorte=:alturacorte,tamanhocorte=:tamanhocorte,posicaocorte=:posicaocorte,quantidade=:quantidade,FORMULAQUANTIDADE=:FORMULAQUANTIDADE');
                ModifySQL.add(',Esquadria=:Esquadria where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabPerfilado_Proj where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPERFILADO_PROJ.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPERFILADO_PROJ.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPERFILADO_PROJ');
     Result:=Self.ParametroPesquisa;
end;

function TObjPERFILADO_PROJ.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de PERFILADO_PROJ ';
end;


function TObjPERFILADO_PROJ.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPERFILADO_PROJ,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENPERFILADO_PROJ,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPERFILADO_PROJ.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    FreeAndNil(Self.ObjQueryPesquisa);
    FreeAndNil(Self.ObjDataSource);
    Self.Perfilado.FREE;
    Self.Projeto.FREE;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPERFILADO_PROJ.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPERFILADO_PROJ.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPerfilado_Proj.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjPerfilado_Proj.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjPerfilado_Proj.Submit_FormulaAltura(parametro: string);
begin
        Self.FormulaAltura:=Parametro;
end;
function TObjPerfilado_Proj.Get_FormulaAltura: string;
begin
        Result:=Self.FormulaAltura;
end;
procedure TObjPerfilado_Proj.Submit_FormulaLargura(parametro: string);
begin
        Self.FormulaLargura:=Parametro;
end;
function TObjPerfilado_Proj.Get_FormulaLargura: string;
begin
        Result:=Self.FormulaLargura;
end;

procedure TObjPERFILADO_PROJ.EdtPerfiladoExit(Sender: TObject;Var PEdtCodigo : TEdit;  LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Perfilado.LocalizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     Self.Perfilado.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Perfilado.Get_Descricao;;
     PEdtCodigo.Text:=Self.Perfilado.Get_Codigo;

End;

procedure TObjPERFILADO_PROJ.EdtPerfiladoExit(Sender: TObject;Var PEdtCodigo : TEdit;LABELNOME:TLABEL;var kitperfilado:Boolean);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Perfilado.LocalizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     Self.Perfilado.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Perfilado.Get_Descricao;;
     PEdtCodigo.Text:=Self.Perfilado.Get_Codigo;
     if(Self.Perfilado.Get_KitPerfilado='S')
     then kitperfilado:=True
     else kitperfilado:=False;

End;

procedure TObjPERFILADO_PROJ.EdtPerfiladoKeyDown(Sender: TObject;Var PEdtCodigo:TEdit; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPerfilado:TFPERFILADO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPerfilado:=TFPERFILADO.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Perfilado.Get_Pesquisa2,Self.Perfilado.Get_TituloPesquisa,FPerfilado)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Perfilado.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Perfilado.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FPerfilado);
     End;
end;

procedure TObjPERFILADO_PROJ.EdtPerfiladoKeyDown2(Sender: TObject;var Key: Word;
  Shift: TShiftState;var LABELNOME:string; var Tedit:string;CodigoProjeto:string);
var
   FpesquisaLocal:Tfpesquisa;
   FPerfilado:TFPERFILADO;
   SQL:STRING;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPerfilado:=TFPERFILADO.Create(nil);
            SQL:='select * from tabperfilado p join tabperfilado_proj pp on pp.Perfilado=p.codigo where pp.projeto='+CodigoProjeto;
            If (FpesquisaLocal.PreparaPesquisa(SQL,'',FPerfilado)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If TEdit<>''
                                 Then Begin
                                        If FpesquisaLocal.QueryPesq.fieldbyname('Descricao').asstring<>''
                                        Then LABELNOME:=FpesquisaLocal.QueryPesq.fieldbyname('Descricao').asstring
                                        Else LABELNOME:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FPerfilado);
     End;
end;

procedure TObjPERFILADO_PROJ.EdtProjetoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Projeto.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Projeto.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Projeto.Get_Descricao ;
End;
procedure TObjPERFILADO_PROJ.EdtProjetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Projeto.Get_Pesquisa,Self.Projeto.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Projeto.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Projeto.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Projeto.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjPERFILADO_PROJ.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPERFILADO_PROJ';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjPERFILADO_PROJ.Resgata_Perfilado_Proj(PProjeto: string);
begin
     With Self.ObjQueryPesquisa do
     Begin
         close;
         SQL.Clear;
         Sql.Add('Select  TabPerfilado.Referencia, TabPerfilado.Descricao,');
         Sql.Add('TabPerfilado_Proj.FormulaAltura,TabPerfilado_Proj.FormulaLargura, TabPerfilado_Proj.Codigo,tabperfilado_proj.quantidade,tabperfilado_proj.FORMULAQUANTIDADE');
         Sql.Add(' ,Esquadria from TabPerfilado_Proj');
         Sql.Add('Join TabPerfilado on TabPerfilado.Codigo = TabPerfilado_Proj.Perfilado');
         Sql.Add('Where Projeto = '+PProjeto);
         if (PProjeto = '')
         then exit;
         Open;
     end;
end;
function TObjPerfilado_PROJ.VerificaSeJaExisteEstaPerfiladoNesteProjeto(PProjeto, PPerfilado: string): Boolean;
Var    QueryLOcal:TIBquery;
begin
try
        Result:=false;
        try
             QueryLOcal:=TIBQuery.Create(nil);
             QueryLOcal.Database:=FDataModulo.IBDatabase;
        except
             MensagemErro('Erro ao tentar criar a QueryLocal');
             exit;
        end;

        With QueryLocal do
        Begin
             Close;
             Sql.Clear;
             Sql.Add('Select Codigo from TabPerfilado_Proj');
             Sql.Add('Where Perfilado = '+PPerfilado);
             Sql.Add('and Projeto = '+PProjeto);
             Open;

             if (RecordCount>0)then
             Result:=true;                          
        end;

Finally
       FreeAndNil(QueryLOcal);
end;

end;


Function TobjPerfilado_proj.Replica(PProjeto, PnovoProjeto: string):boolean;
var
pcodigoantigo:string;
begin
     result:=false;

     With Self.ObjQueryPesquisa do
     Begin
         close;
         SQL.Clear;
         Sql.Add('Select  Tabperfilado_Proj.Codigo');
         Sql.Add('from    Tabperfilado_Proj');
         Sql.Add('Where   Projeto = '+PProjeto);

         if (PProjeto = '')
         then exit;

         Open;
         last;
         if (recordcount=0)
         Then Begin
                   result:=true;
                   exit;
         End;
         FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
         FMostraBarraProgresso.Lbmensagem.caption:='Replicando Perfilados';
         first;
         Try

           While not(eof) do
           begin
                FMostraBarraProgresso.IncrementaBarra1(1);
                FMostraBarraProgresso.Show;
                Application.processmessages;

                Self.ZerarTabela;
                if (Self.LocalizaCodigo(fieldbyname('codigo').asstring)=False)
                then Begin
                          mensagemErro('perfilado_Proj C�digo '+fieldbyname('codigo').asstring+' n�o localizada');
                          exit;
                End;
                Self.TabelaparaObjeto;

                Self.Status:= dsinsert;
                PcodigoAntigo:=Self.codigo;
                Self.Submit_Codigo('0');
                Self.Projeto.Submit_Codigo(PnovoProjeto);
                if (self.Salvar(False)=False)
                Then begin
                          mensagemErro('Erro na tentativa de Replicar o perfilado_proj C�digo='+PcodigoAntigo);
                          exit;
                End;

                next;
           End;
           result:=True;
           
         Finally
                FMostraBarraProgresso.Close;
         End;
     end;

end;

procedure TObjPERFILADO_PROJ.Submit_AlturaCorte(parametro:string);
begin
    AlturaCorte:=parametro;
end;

procedure TObjPERFILADO_PROJ.Submit_PosicaoCorte(parametro:string);
begin
    PosicaoCorte:=Parametro;
end;

procedure TObjPERFILADO_PROJ.Submit_TamanhoCorte(parametro:string);
begin
    TamanhoCorte:=parametro;
end;

function TObjPERFILADO_PROJ.Get_AlturaCorte:string;
begin
    Result:=AlturaCorte;
end;

function TObjPERFILADO_PROJ.Get_PosicaoCorte:string;
begin
    Result:=PosicaoCorte;
end;

function TObjPERFILADO_PROJ.Get_TamanhoCorte:string;
begin
    Result:=TamanhoCorte;
end;

procedure TObjPERFILADO_PROJ.Submit_quantidade(parametro:string);
begin
    self.Quantidade:=parametro;
end;

function TObjPERFILADO_PROJ.Get_Quantidade:string;
begin
    Result:=Self.Quantidade;
end;

function TObjPERFILADO_PROJ.Get_Formulaquantidade:string;
begin
  Result:=Self.Formulaquantidade;
end;

procedure TObjPERFILADO_PROJ.Submit_Formulaquantidade(parametro:string);
begin
  FORMULAQUANTIDADE:=parametro;
end;

procedure TObjPERFILADO_PROJ.Submit_Esquadria(parametro:string);
begin
  Esquadria:=parametro;
end;

function TObjPERFILADO_PROJ.Get_Esquadria:string;
begin
  Result:=Esquadria;
end;

end.



