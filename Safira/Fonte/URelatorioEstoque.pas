unit URelatorioEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,UObjObjetosEntrada, ExtCtrls;

type
  TFrelatorioEstoque = class(TForm)
    Label3: TLabel;
    BtImprimir: TBitBtn;
    ComboMaterial: TComboBox;
    pnl3: TPanel;
    btCancelar: TSpeedButton;
    lb1: TLabel;
    pnlRodape: TPanel;
    Img1: TImage;
    btRelatorios: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtImprimirClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    ObjObjetosEntrada:TObjObjetosEntrada;
  public
    { Public declarations }
  end;

var
  FrelatorioEstoque: TFrelatorioEstoque;

implementation

uses UessencialGlobal, UescolheImagemBotao;

{$R *.dfm}

procedure TFrelatorioEstoque.FormShow(Sender: TObject);
begin
     combomaterial.text:='';
     limpaedit(self);
     BtImprimir.Enabled:=False;
     desabilita_campos(self);
     //PegaFiguraBotao(BtImprimir,'Botaorelatorios.Bmp');
     FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');      
     Try
        ObjObjetosEntrada:=TObjObjetosEntrada.create(self);
        BtImprimir.Enabled:=true;
        habilita_campos(self);
        ComboMaterial.setfocus;
     except
           mensagemerro('Erro na tentativa de Criar os Objetos');
           exit;
     end;

end;

procedure TFrelatorioEstoque.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     if (ObjObjetosEntrada<>nil)
     Then ObjObjetosEntrada.free;
end;

procedure TFrelatorioEstoque.BtImprimirClick(Sender: TObject);
begin
     case ComboMaterial.itemindex of
       0:ObjObjetosEntrada.ObjDiverso_ep.DiversoCor.RelatorioEstoque;
       1:ObjObjetosEntrada.Objferragem_ep.FerragemCor.RelatorioEstoque;
       2:ObjObjetosEntrada.Objkitbox_ep.KitBoxCor.RelatorioEstoque;
       3:ObjObjetosEntrada.Objperfilado_ep.PerfiladoCor.RelatorioEstoque;
       4:ObjObjetosEntrada.ObjPersiana_ep.PersianaGrupoDiametroCor.RelatorioEstoque;
       5:ObjObjetosEntrada.Objvidro_ep.VidroCor.RelatorioEstoque;
       Else Begin
                 MensagemAviso('Escolha um material');
       End;
     End;
end;

procedure TFrelatorioEstoque.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Perform(Wm_NextDlgCtl,0,0);
end;

end.
