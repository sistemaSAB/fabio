unit uobjgeranfe;
interface

uses dialogs,forms,classes,ibquery,controls,
SHDocVw,ACBrNFe,ACBrNFeConfiguracoes,UobjNFe;

Type
       Tobjgeranfe=class
          Public
                LocalArquivos:String;
                LocalArquivoCancelamento:string;
                LocalArquivoInutilizacao:string;
                LocalArquivoXML:string;
                Nfe:TObjNFE;
                constructor create;
                destructor free;
                Function  ConfiguraComponente:boolean;
                
                function CadastrarNotas: boolean;
                function EscolherNota(var Pnota: String): boolean;
                function PesquisarNFes: boolean;
                function consulta_por_chaveAcesso (pChave:string = '';visualizarMSG:Boolean = true): Integer;
                procedure imprimeDanfe(pArquivo:string);
                
                
          Private
                ObjqueryGera:tibquery;

      End;


implementation

uses db,sysutils, UcomponentesNfe,UdataMOdulo,pcnconversao, ACBrNFeNotasFiscais,
  pcnNFe, UUtils, UFiltraImp, UMostraBarraProgresso, UmostraStringList,
  Upesquisa,pcnLeitor;



{ TObjNFEObjetos }

function Tobjgeranfe.ConfiguraComponente: boolean;
begin
      result:=False;

      Try
      
          With FComponentesNfe.ACBrNFe do
          Begin
                if (ObjParametroGlobal.ValidaParametro('NFE ORIENTACAO DANFE')=False)
                Then exit;

                if (uppercase(ObjParametroGlobal.Get_Valor)='RETRATO')
                Then DANFE.TipoDANFE  := tiRetrato
                Else DANFE.TipoDANFE  := tiPaisagem;
            




                ObjParametroGlobal.Get_Valor;//Retrato,Paisagem

                if (ObjParametroGlobal.ValidaParametro('NFE FORMA DE EMISSAO DO DANFE')=False)
                Then exit;


                if (UPPERCASE(ObjParametroGlobal.Get_Valor)='NORMAL ON-LINE')
                Then Configuracoes.Geral.FormaEmissao := teNormal;

                if (UPPERCASE(ObjParametroGlobal.Get_Valor)='CONTINGENCIA OFF-LINE')
                Then Configuracoes.Geral.FormaEmissao := teContingencia;

                if (UPPERCASE(ObjParametroGlobal.Get_Valor)='SCAN')
                Then Configuracoes.Geral.FormaEmissao :=  teSCAN;

                if (UPPERCASE(ObjParametroGlobal.Get_Valor)='DPEC')
                Then Configuracoes.Geral.FormaEmissao :=  teDPEC;

                if (UPPERCASE(ObjParametroGlobal.Get_Valor)='FSDA')
                Then Configuracoes.Geral.FormaEmissao :=  teFSDA;


                if (ObjParametroGlobal.ValidaParametro('NFE CAMINHO LOGO MARCA DANFE')=False)
                Then exit;

                DANFE.Logo       :=ObjParametroGlobal.Get_Valor;//caminho

                //if (ObjParametroGlobal.ValidaParametro('NFE CAMINHO DOS ARQUIVOS DE ENVIO E RESPOSTA')=False)
                //Then exit;

                //salva todos os arquivos, envio resposta, etc.
                Configuracoes.Geral.Salvar       := false;//true ou false salvar aqruivos de envio e resposta

                //Self.LocalArquivos:=ObjParametroGlobal.Get_Valor;
                //Configuracoes.Geral.PathSalvar   := ObjParametroGlobal.Get_Valor;//caminho para salvar


                if (ObjParametroGlobal.ValidaParametro('NFE CAMINHO DOS ARQUIVOS DE ENVIO E RESPOSTA') = false) then
                begin

                  Mensagemerro ('Par�metro: "NFE CAMINHO DOS ARQUIVOS DE ENVIO E RESPOSTA" n�o encontrado');
                  Exit;

                end;
                LocalArquivos := ObjParametroGlobal.Get_Valor();
                //Configuracoes.Arquivos.PathDPEC:=LocalArquivos; celio0712

                if (ObjParametroGlobal.ValidaParametro('NFE CAMINHO DOS ARQUIVOS XML') = false) then
                begin

                  Mensagemerro ('Par�metro: "NFE CAMINHO DOS ARQUIVOS XML" n�o encontrado');
                  Exit;

                end;
                LocalArquivoXML:=ObjParametroGlobal.Get_Valor;
                Configuracoes.arquivos.PathNFe:=LocalArquivoXML;

                if (ObjParametroGlobal.ValidaParametro('NFE CAMINHO DOS ARQUIVOS DE CANCELAMENTO') = false) then
                begin

                  Mensagemerro ('Par�metro: "NFE CAMINHO DOS ARQUIVOS DE CANCELAMENTO" n�o encontrado');
                  Exit;

                end;
                LocalArquivoCancelamento := ObjParametroGlobal.Get_Valor;
                //Configuracoes.Arquivos.PathCan:=LocalArquivoCancelamento; celio0712
                Configuracoes.Arquivos.PathEvento:=LocalArquivoCancelamento;


                if (ObjParametroGlobal.ValidaParametro('NFE CAMINHO DOS ARQUIVOS DE INUTILIZACAO') = false) then
                begin

                  Mensagemerro ('Par�metro: "NFE CAMINHO DOS ARQUIVOS DE INUTILIZACAO" n�o encontrado');
                  Exit;

                end;
                LocalArquivoInutilizacao:=ObjParametroGlobal.Get_Valor;
                Configuracoes.Arquivos.PathInu:=LocalArquivoInutilizacao;



                // as pastas repensar
                //Configuracoes.Arquivos.PathNFe:=ObjParametroGlobal.Get_Valor;
                //Configuracoes.Arquivos.PathCan:=ObjParametroGlobal.Get_Valor;
                //Configuracoes.Arquivosd.PathInu:=ObjParametroGlobal.Get_Valor;
                //Configuracoes.Arquivos.PathDPEC:=ObjParametroGlobal.Get_Valor;


                if (ObjParametroGlobal.ValidaParametro('NFE UF WEBSERVICE')=False)
                Then exit;

                Configuracoes.WebServices.UF         := ObjParametroGlobal.Get_Valor;//UF

                if (ObjParametroGlobal.ValidaParametro('NFE AMBIENTE')=False)
                Then exit;


                if (uppercase(ObjParametroGlobal.Get_Valor)='PRODUCAO')
                Then Configuracoes.WebServices.Ambiente   :=taproducao;

                if (uppercase(ObjParametroGlobal.Get_Valor)='HOMOLOGACAO')
                Then Configuracoes.WebServices.Ambiente   :=taHomologacao;

                Configuracoes.WebServices.Visualizar := True;//Visualizar mensagem

                //FComponentesNfe.ACBrNFeDANFERave1.RavFile:=RetornaPathSistema+'Report\NotaFiscalEletronica.rav'; celio0712

                if (ObjParametroGlobal.ValidaParametro('NFE ARQUIVO RAVE')=False)
                Then exit;

                //FComponentesNfe.ACBrNFeDANFERave1.RavFile:=ObjParametroGlobal.get_valor; celio0712

                FComponentesNfe.ACBrNFeDANFERave1.Sistema:='Exclaim Tecnologia';

                result:=True;

                
          End;

      Finally

      End;
end;

constructor Tobjgeranfe.create;
begin
     Self.ObjqueryGera:=tibquery.create(nil);
     Self.ObjqueryGera.Database:=FDataModulo.IBDatabase;
     Self.Nfe:=TobjNfe.create;
end;



destructor Tobjgeranfe.free;
begin
     Freeandnil(Self.ObjqueryGera);
     Self.nfe.free;
end;

function TobjGeraNfe.CadastrarNotas: boolean;
var
PnotaInicial,PnotaFinal:String;
cont:integer;
begin
     //Solicito a primeira e ultima NFe
     //verifico se existem Nfs neste intervalo

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          LbGrupo01.caption:='Nota Inicial';
          LbGrupo02.caption:='Nota Final';
          showmodal;

          if (Tag=0)
          Then exit;

          Try
            Strtoint(edtgrupo01.Text);
            PnotaInicial:=edtgrupo01.Text;
            Strtoint(edtgrupo02.Text);
            PnotaFinal:=edtgrupo02.Text;
          Except
                Messagedlg('Valores Inv�lidos',mterror,[mbok],0);
                exit;
          End;

          if (strtoint(PnotaInicial)>strtoint(PnotaFinal))
          then Begin
                    Messagedlg('A nota inicial tem que ser menor ou igual a Nota Final',mterror,[mbok],0);
                    exit;
          End;
     End;

     FMostraBarraProgresso.ConfiguracoesIniciais(StrToInt(PnotaFinal)-Strtoint(PnotaInicial),0);
     FMostraBarraProgresso.Lbmensagem.caption:='Cadastrando Novas NFe';

     FmostraStringList.Memo.Lines.clear;


     try
      for cont:=Strtoint(PnotaInicial) to StrToInt(PnotaFinal) do
       Begin
            FMostraBarraProgresso.IncrementaBarra1(1);
            FMostraBarraProgresso.show;

            if (Self.NFE.LocalizaCodigo(inttostr(cont))=True)
            Then Begin
                      FmostraStringList.Memo.Lines.add('NFe C�digo '+inttostr(cont)+' j� se encontra cadastrada');
            End
            Else Begin
                      Self.Nfe.ZerarTabela;
                      Self.NFE.Status:=dsInsert;
                      Self.NFE.Submit_Codigo(inttostr(cont));
                      Self.NFE.Submit_StatusNota('A');

                      if (Self.NFE.Salvar(True)=False)
                      Then FmostraStringList.Memo.Lines.add('Erro na tentativa de Salvar a NFe C�digo '+inttostr(cont));

            End;
       End;

       if (FmostraStringList.Memo.Lines.text<>'')
       Then FmostraStringList.showmodal;
     Finally
            FMostraBarraProgresso.Close;
     End;

end;



function TobjGeraNfe.EscolherNota(var Pnota:String): boolean;
var
Fpesquisax:TFpesquisa;
begin
     Pnota:='';
     result:=False;

      Try
         Fpesquisax:=TFpesquisa.Create(nil);
      except
            Messagedlg('Erro na tentativa de criar o Formul�rio de Pesquisa da NFE',mterror,[mbok],0);
            exit;
      End;

      Try
         if (Fpesquisax.PreparaPesquisa('Select * from TabNfe where StatusNota=''A''','Pesquisa de NFe',nil)=False)
         then exit;

         if (Fpesquisax.ShowModal=mrok)
         Then Begin
                   Pnota:=Fpesquisax.DataSource1.DataSet.Fieldbyname('codigo').asstring;

                   if (Pnota<>'')
                   Then result:=True;
                   
                   exit;
         End;

         
      Finally
             freeandnil(FpesquisaX);
      End;




end;


function TobjGeraNfe.PesquisarNFes: boolean;
var
Fpesquisax:Tfpesquisa;
begin
     result:=False;

      Try
         Fpesquisax:=TFpesquisa.Create(nil);
      except
            Messagedlg('Erro na tentativa de criar o Formul�rio de Pesquisa da NFE',mterror,[mbok],0);
            exit;
      End;

      Try
         if (Fpesquisax.PreparaPesquisa('Select * from TabNfe','Pesquisa de NFe',nil)=False)
         then exit;

         if (Fpesquisax.ShowModal=mrok)
         Then Begin

         End;

         
      Finally
             freeandnil(FpesquisaX);
      End;

end;

function TobjGeraNfe.consulta_por_chaveAcesso(pChave:string;visualizarMSG:Boolean): Integer;
begin

  Result := -1;

  FComponentesNfe.ACBrNFe.NotasFiscais.Clear;

  Self.ConfiguraComponente;

  if (Trim (pChave) = '') then
  begin

      if not(InputQuery('WebServices Consultar', 'Chave da NF-e:', pChave)) then
        exit;

       if (Trim (pChave) = '') then
       begin

         MensagemAviso ('Informe a chave de acesso');
         Exit;

       end;
  end;

  FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie := ObjEmpresaGlobal.get_certificado ();

  FComponentesNfe.ACBrNFe.WebServices.Consulta.NFeChave := pChave;

  try

    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar:=visualizarMSG;
    Screen.Cursor:=crHourGlass;
    FComponentesNfe.ACBrNFe.WebServices.Consulta.Executar ();
    Result := FComponentesNfe.ACBrNFe.WebServices.Consulta.cStat;

  finally

    Screen.Cursor:=crDefault;

  end;

end;

procedure Tobjgeranfe.imprimeDanfe(pArquivo: string);
var
   wnProt: TLeitor;
Begin

  try

    if not (FileExists(pArquivo)) then
    begin
      Mensagemerro('Erro ao imprimir danfe. O arquivo: '+pArquivo+' n�o foi encontrado');
      Exit;
    end;

    FComponentesNfe.ACBrNFe.NotasFiscais.Clear;

    if not (Self.ConfiguraComponente) then
    begin
      Mensagemerro('Erro a configura��o dos componentes');
      Exit;
    end;

    FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie:=ObjEmpresaGlobal.Get_certificado;
    FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(Parquivo);

    wnProt:=TLeitor.Create;
    wnProt.CarregarArquivo(Parquivo);
    wnProt.Grupo:=wnProt.Arquivo;
    FcomponentesNfe.ACBrNFe.DANFE.ProtocoloNFe:=wnProt.rCampo(tcStr,'nProt');
    FcomponentesNfe.ACBrNFe.NotasFiscais.Imprimir;

  finally

    wnProt.Free;

  end;

end;

end.
