unit UKITBOX;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjKitBoxCor,
  UessencialGlobal, Tabs, Grids, DBGrids, ComCtrls, UFRImposto_ICMS,Uobjkitbox_icms
  ,UFORNECEDOR,UPLanodeContas,UCOR,UpesquisaMenu,IBQuery,UErrosDesativarMateriais,
  ImgList,ClipBrd;

type
  TFKITBOX = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb4: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    bt1: TSpeedButton;
    pgcGuia: TPageControl;
    tsPrincipal: TTabSheet;
    tsImposto: TTabSheet;
    tsCor: TTabSheet;
    panelPrincipal: TPanel;
    SpeedButtonUltimo: TSpeedButton;
    SpeedButtonProximo: TSpeedButton;
    SpeedButtonAnterior: TSpeedButton;
    SpeedButtonPrimeiro: TSpeedButton;
    LbReferencia: TLabel;
    EdtReferencia: TEdit;
    lb2: TLabel;
    edtNCM: TEdit;
    chkAtivo: TCheckBox;
    lbAtivoInativo: TLabel;
    EdtDescricao: TEdit;
    LbDescricao: TLabel;
    LbFornecedor: TLabel;
    LbUnidade: TLabel;
    LbAlturaIdeal: TLabel;
    LbPrecoCusto: TLabel;
    EdtPrecoCusto: TEdit;
    EdtAlturaIdeal: TEdit;
    EdtUnidade: TEdit;
    EdtFornecedor: TEdit;
    LbPeso: TLabel;
    LbLarguraIdeal: TLabel;
    Label10: TLabel;
    EdtPlanodeContas: TEdit;
    EdtLarguraIdeal: TEdit;
    EdtPeso: TEdit;
    GroupBox3: TGroupBox;
    LbAreaMinima: TLabel;
    LbAlturaMinima: TLabel;
    LbLarguraMinima: TLabel;
    LbAlturaMaxima: TLabel;
    LbLarguraMaxima: TLabel;
    EdtAreaMinima: TEdit;
    EdtAlturaMinima: TEdit;
    EdtLarguraMinima: TEdit;
    EdtAlturaMaxima: TEdit;
    EdtLarguraMaxima: TEdit;
    rgmargem: TGroupBox;
    LbPorcentagemFornecido: TLabel;
    LbPorcentagemRetirado: TLabel;
    LbPorcentagemInstalado: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdtPorcentagemInstalado: TEdit;
    EdtPorcentagemFornecido: TEdit;
    EdtPorcentagemRetirado: TEdit;
    rgpreco: TGroupBox;
    LbPrecoVendaInstalado: TLabel;
    LbPrecoVendaFornecido: TLabel;
    LbPrecoVendaRetirado: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdtPrecoVendaInstalado: TEdit;
    EdtPrecoVendaFornecido: TEdit;
    EdtPrecoVendaRetirado: TEdit;
    Panelimposto: TPanel;
    Panel1: TPanel;
    LbClassificaoFiscal: TLabel;
    EdtClassificacaoFiscal: TEdit;
    PanelICMSForaEstado: TPanel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    EdtAliquota_ICMS_ForaEstado: TEdit;
    EdtReducao_BC_ICMS_ForaEstado: TEdit;
    EdtAliquota_ICMS_Cupom_ForaEstado: TEdit;
    comboisentoicms_foraestado: TComboBox;
    comboSUBSTITUICAOICMS_FORAESTADO: TComboBox;
    edtVALORPAUTA_SUB_TRIB_FORAESTADO: TEdit;
    edtpercentualagregado: TEdit;
    Label16: TLabel;
    LbSituacaoTributaria_TabelaA: TLabel;
    EdtSituacaoTributaria_TabelaA: TEdit;
    EdtSituacaoTributaria_TabelaB: TEdit;
    FRImposto_ICMS1: TFRImposto_ICMS;
    panelCorFundo: TPanel;
    pnl2: TPanel;
    btgravarcor: TBitBtn;
    btexcluircor: TBitBtn;
    btcancelacor: TBitBtn;
    DBGRIDCOR: TDBGrid;
    PanelICMSEstado: TPanel;
    LbAliquota_ICMS_Estado: TLabel;
    LbReducao_BC_ICMS_Estado: TLabel;
    LbAliquota_ICMS_Cupom_Estado: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    EdtAliquota_ICMS_Estado: TEdit;
    EdtReducao_BC_ICMS_Estado: TEdit;
    EdtAliquota_ICMS_Cupom_Estado: TEdit;
    comboisentoicms_estado: TComboBox;
    comboSUBSTITUICAOICMS_ESTADO: TComboBox;
    edtVALORPAUTA_SUB_TRIB_ESTADO: TEdit;
    LbNomeFornecedor: TLabel;
    panelCor: TPanel;
    LbCor: TLabel;
    lbnomecor: TLabel;
    LbEstoque: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    LbCusto: TLabel;
    Label15: TLabel;
    lbestoque1: TLabel;
    EdtCorReferencia: TEdit;
    edtcodigoCOR: TEdit;
    EdtCor: TEdit;
    GroupBoxVidros: TGroupBox;
    LbPorcentagemAcrescimo: TLabel;
    LbAcrescimoExtra: TLabel;
    LbPorcentagemAcrescimoFinal: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    EdtPorcentagemAcrescimo: TEdit;
    EdtAcrescimoExtra: TEdit;
    EdtPorcentagemAcrescimoFinal: TEdit;
    EdtValorExtra: TEdit;
    EdtValorCusto: TEdit;
    EdtValorFinal: TEdit;
    edtinstaladofinal: TEdit;
    edtfornecidofinal: TEdit;
    edtretiradofinal: TEdit;
    Rg_forma_de_calculo_percentual: TRadioGroup;
    edtclassificacaofiscal_cor: TEdit;
    il1: TImageList;
    label4545: TLabel;
    edtCest: TEdit;
    Label12: TLabel;
    edtEstoqueMinimo: TEdit;
    procedure EdtFornecedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure EdtFornecedorExit(Sender: TObject);
    procedure edtSituacaoTributaria_TabelaAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtSituacaoTributaria_TabelaAExit(Sender: TObject);
    procedure edtSituacaoTributaria_TabelaBExit(Sender: TObject);
    
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtSituacaoTributaria_TabelaBKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure EdtPorcentagemFornecidoExit(Sender: TObject);
    procedure EdtPorcentagemRetiradoExit(Sender: TObject);
    procedure tsGuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure btGravarCorClick(Sender: TObject);
    procedure BtExcluirCorClick(Sender: TObject);
    procedure BtCancelarCorClick(Sender: TObject);
    procedure EdtCorReferenciaExit(Sender: TObject);
    procedure EdtCorReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtAcrescimoExtraExit(Sender: TObject);
    procedure DBGRIDCORDblClick(Sender: TObject);
    procedure EdtValorExtraExit(Sender: TObject);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure btOpcoesClick(Sender: TObject);
    procedure EdtPlanodeContasExit(Sender: TObject);
    procedure EdtPlanodeContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtPrecoVendaInstaladoExit(Sender: TObject);
    procedure EdtPrecoVendaFornecidoExit(Sender: TObject);
    procedure EdtPrecoVendaRetiradoExit(Sender: TObject);
    procedure DBGRIDCORKeyPress(Sender: TObject; var Key: Char);
    procedure DBGRIDCORDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure EdtFornecedorDblClick(Sender: TObject);
    procedure EdtPlanodeContasDblClick(Sender: TObject);
    procedure LbEstoqueMouseLeave(Sender: TObject);
    procedure LbEstoqueMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure LbEstoqueClick(Sender: TObject);
    procedure edtquant(Sender: TObject;
  var Key: Char);
    procedure chkAtivoClick(Sender: TObject);
    procedure bt1Click(Sender: TObject);
    procedure LbNomeFornecedorClick(Sender: TObject);
    procedure LbNomeFornecedorMouseLeave(Sender: TObject);
    procedure LbNomeFornecedorMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure EdtFornecedorKeyPress(Sender: TObject; var Key: Char);
    procedure EdtPlanodeContasKeyPress(Sender: TObject; var Key: Char);
    procedure EdtPrecoCustoKeyPress(Sender: TObject; var Key: Char);
    procedure EdtAlturaIdealKeyPress(Sender: TObject; var Key: Char);
    procedure EdtLarguraIdealKeyPress(Sender: TObject; var Key: Char);
    procedure EdtAreaMinimaKeyPress(Sender: TObject; var Key: Char);
    procedure EdtAlturaMinimaKeyPress(Sender: TObject; var Key: Char);
    procedure EdtLarguraMinimaKeyPress(Sender: TObject; var Key: Char);
    procedure EdtAlturaMaximaKeyPress(Sender: TObject; var Key: Char);
    procedure EdtLarguraMaximaKeyPress(Sender: TObject; var Key: Char);
    procedure lbnomecorClick(Sender: TObject);
    procedure pgcGuiaChange(Sender: TObject);
    procedure EdtPorcentagemInstaladoExit(Sender: TObject);
    procedure EdtPrecoCustoExit(Sender: TObject);
    procedure edtNCMKeyPress(Sender: TObject; var Key: Char);
    procedure EdtCorReferenciaDblClick(Sender: TObject);
    procedure edtNCMKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCestKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    AlteraPrecoPeloCusto:Boolean;
    PAlteraCampoEstoque:boolean;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    Procedure ResgataCores;
    Procedure PreparaCor;

  private
    EstadoObjeto:string;

    { Private declarations }
  public
      procedure AbreComCodigo(parametro:string);
  end;

var
  FKITBOX: TFKITBOX;
  ObjKitBoxCor:TObjKitBoxCor;

implementation

uses Upesquisa, UessencialLocal, UDataModulo, UobjKITBOX, UMenuRelatorios,
  UescolheImagemBotao, Uprincipal, UFiltraImp, UAjuda, UobjCEST;

{$R *.dfm}


procedure TFKITBOX.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjKitBoxCor.KitBox=Nil)
     Then exit;

     If (ObjKitBoxCor.KitBox.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Try
      ObjKitBoxCor.KitBox.free;
    Except
          on e:exception do
          Begin
               mensagemerro(e.message);
          End;
    End;
    
    try
      FRImposto_ICMS1.ObjMaterial_ICMS.Free;
    Except
          on e:exception do
          Begin
               mensagemerro(e.message);
          End;
    End;
    Self.Tag:=0;
end;


procedure TFKITBOX.btNovoClick(Sender: TObject);
begin
     Self.limpaLabels;
     limpaedit(Self);

     habilita_campos(Self);
     desab_botoes(Self);
     chkAtivo.Checked:=True;
     lbAtivoInativo.Caption:='Ativo';

     lbCodigo.caption:='0';
     EdtAreaMinima.text:='0';
     EdtAlturaMinima.text:='0';
     EdtLarguraMinima.text:='0';

     EdtAlturamaxima.text:='0';
     EdtLarguraMaxima.text:='0';

     EdtAlturaIdeal.text:='0';
     EdtLarguraIdeal.text:='0';

     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjKitBoxCor.KitBox.status:=dsInsert;
     pgcGuia.TabIndex:=0;

     if (AlteraPrecoPeloCusto=True)
     Then Begin
              EdtPrecoVendaInstalado.Enabled:=false;
              EdtPrecoVendaRetirado.Enabled:=false;
              EdtPrecoVendaFornecido.Enabled:=false;
     End;

     EdtReferencia.setfocus;
     Self.EstadoObjeto:='INSER��O';

     EdtSituacaoTributaria_TabelaA.Text:='1';
     EdtSituacaoTributaria_TabelaB.Text:='1';
     EdtClassificacaoFiscal.Text:='1';

     comboisentoicms_estado.Text:='N�O';
     comboSUBSTITUICAOICMS_ESTADO.Text:='SIM';
     edtVALORPAUTA_SUB_TRIB_ESTADO.Text:='0';
     EdtAliquota_ICMS_Estado.Text:='0';
     EdtReducao_BC_ICMS_Estado.Text:='0';
     EdtAliquota_ICMS_Cupom_Estado.Text:='0';

     comboisentoicms_foraestado.Text:='N�O';
     comboSUBSTITUICAOICMS_foraESTADO.Text:='SIM';
     edtVALORPAUTA_SUB_TRIB_foraESTADO.Text:='0';
     EdtAliquota_ICMS_foraEstado.Text:='0';
     EdtReducao_BC_ICMS_foraEstado.Text:='0';
     EdtAliquota_ICMS_Cupom_foraEstado.Text:='0';

     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;



end;

procedure TFKITBOX.btSalvarClick(Sender: TObject);
begin

     If ObjKitBoxCor.KitBox.Status=dsInactive
     Then exit;

     If (EdtReferencia.Text<>'')then  begin
         If (ObjKitBoxCor.KitBox.VerificaReferencia(ObjKitBoxCor.KitBox.Status,lbCodigo.caption, EdtReferencia.Text)=true) then
         Begin
             MensagemErro('Refer�ncia j� existe.');
             exit;
         end;
     end;


     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjKitBoxCor.KitBox.salvar(true)=False)
     Then exit;

     lbCodigo.caption:=ObjKitBoxCor.KitBox.Get_codigo;
     habilita_botoes(Self);


     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

     if (Self.EstadoObjeto='INSER��O')then
     if (ObjKitBoxCor.CadastraKitBoxEmTodasAsCores(lbCodigo.caption)=false)then
     Begin
         MensagemErro('Erro ao tentar cadastrar esse KitBox nas cores escolhidas.');
         FDataModulo.IBTransaction.RollbackRetaining;
         Self.EstadoObjeto:='';
         exit;
     end
     else MensagemAviso('Cores Cadastradas com Sucesso !');

     FDataModulo.IBTransaction.CommitRetaining;

     Self.EstadoObjeto:='';
//     limpaedit(Self);
//     Self.limpaLabels;
     desabilita_campos(Self);

     
     ObjkitboxCor.kitbox.LocalizaCodigo(lbCodigo.caption);
     ObjkitboxCor.kitbox.TabelaparaObjeto;
     self.ObjetoParaControles;

     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
      btopcoes.Visible :=true;




end;

procedure TFKITBOX.btAlterarClick(Sender: TObject);
begin


    If (ObjKitBoxCor.KitBox.Status=dsinactive) and (lbCodigo.caption<>'')
    Then Begin
                habilita_campos(Self);
                ObjKitBoxCor.KitBox.Status:=dsEdit;
                pgcGuia.TabIndex:=0;
                EdtReferencia.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;

                if (AlteraPrecoPeloCusto=True)
                Then Begin
                         EdtPrecoVendaInstalado.Enabled:=false;
                         EdtPrecoVendaRetirado.Enabled:=false;
                         EdtPrecoVendaFornecido.Enabled:=false;
                End;

                ObjKitBoxCor.KitBox.ReferenciaAnterior:=EdtReferencia.Text;
                Self.EstadoObjeto:='EDI��O';
                btnovo.Visible :=false;
                 btalterar.Visible:=false;
                 btpesquisar.Visible:=false;
                 btrelatorio.Visible:=false;
                 btexcluir.Visible:=false;
                 btsair.Visible:=false;
                 btopcoes.Visible :=false;

          End;

end;

procedure TFKITBOX.btCancelarClick(Sender: TObject);
begin
     ObjKitBoxCor.KitBox.cancelar;
     lbCodigo.caption:='';
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     EdtPrecoVendaInstalado.Enabled:=false;
     EdtPrecoVendaRetirado.Enabled:=false;
     EdtPrecoVendaFornecido.Enabled:=false;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
      btopcoes.Visible :=true;


end;

procedure TFKITBOX.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjKitBoxCor.KitBox.Get_pesquisa,ObjKitBoxCor.KitBox.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjKitBoxCor.KitBox.status<>dsinactive
                                  then exit;

                                  If (ObjKitBoxCor.KitBox.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjKitBoxCor.KitBox.ZERARTABELA;
                                  pgcGuia.TabIndex:=0;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
 end;

procedure TFKITBOX.btExcluirClick(Sender: TObject);
begin
     If (ObjKitBoxCor.KitBox.status<>dsinactive) or (lbCodigo.caption='')
     Then exit;

     If (ObjKitBoxCor.KitBox.LocalizaCodigo(lbCodigo.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjKitBoxCor.KitBox.exclui(lbCodigo.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(self);

     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFKITBOX.btRelatorioClick(Sender: TObject);
begin
 //   ObjKitBoxCor.KitBox.Imprime(lbCodigo.caption);
end;

procedure TFKITBOX.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFKITBOX.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFKITBOX.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFKITBOX.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    If Key=VK_Escape
    Then Self.Close;

      if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('CADASTRO DE KITBOX');
         FAjuda.ShowModal;
    end;


end;

procedure TFKITBOX.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFKITBOX.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjKitBoxCor.KitBox do
    Begin
        Submit_Codigo(lbCodigo.caption);
        Submit_Referencia(edtReferencia.text);
        Submit_Descricao(edtDescricao.text);
        Submit_Unidade(edtUnidade.text);
        Fornecedor.Submit_codigo(edtFornecedor.text);
        Submit_Peso(edtPeso.text);
        Submit_PrecoCusto(tira_ponto(edtPrecoCusto.text));

        Submit_PorcentagemInstalado(tira_ponto(edtPorcentagemInstalado.text));
        Submit_PorcentagemFornecido(tira_ponto(edtPorcentagemFornecido.text));
        Submit_PorcentagemRetirado(tira_ponto(edtPorcentagemRetirado.text));

        Submit_precovendaInstalado(tira_ponto(edtprecovendaInstalado.text));
        Submit_precovendaFornecido(tira_ponto(edtprecovendaFornecido.text));
        Submit_precovendaRetirado(tira_ponto(edtprecovendaRetirado.text));


        Submit_AlturaIdeal(edtAlturaIdeal.text);
        Submit_LarguraIdeal(edtLarguraIdeal.text);
        Submit_AlturaMinima(edtAlturaMinima.text);
        Submit_LarguraMinima(edtLarguraMinima.text);
        Submit_AlturaMaxima(edtAlturaMaxima.text);
        Submit_LarguraMaxima(edtLarguraMaxima.text);
        Submit_AreaMinima(edtAreaMinima.text);

        Submit_ClassificacaoFiscal(edtClassificacaoFiscal.text);
        PlanoDeContas.Submit_CODIGO(EdtPlanodeContas.text);

//Campos que nao sao mais usados
        Submit_IsentoICMS_Estado('N');
        Submit_SubstituicaoICMS_Estado('N');
        Submit_ValorPauta_Sub_Trib_Estado('0');
        Submit_Aliquota_ICMS_Estado('0');
        Submit_Reducao_BC_ICMS_Estado('0');
        Submit_Aliquota_ICMS_Cupom_Estado('0');
        Submit_IsentoICMS_ForaEstado('N');
        Submit_SubstituicaoICMS_ForaEstado('N');
        Submit_ValorPauta_Sub_Trib_ForaEstado('0');
        Submit_Aliquota_ICMS_ForaEstado('0');
        Submit_Reducao_BC_ICMS_ForaEstado('0');
        Submit_Aliquota_ICMS_Cupom_ForaEstado('0');
        SituacaoTributaria_TabelaA.Submit_codigo('0');
        SituacaoTributaria_TabelaB.Submit_codigo('0');
        Submit_percentualagregado('0');
        Submit_ipi('0');
        Submit_NCM(edtNCM.Text);
        Submit_cest(edtCest.Text);

        if(chkAtivo.Checked=True)
        then Submit_Ativo('S')
        else Submit_Ativo('N');

//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFKITBOX.LimpaLabels;
begin
//LIMPA LABELS
  lbCodigo.Caption:='';
  LbNomeFornecedor.Caption:='';
end;

function TFKITBOX.ObjetoParaControles: Boolean;
begin
  Try
     With ObjKitBoxCor.KitBox do
     Begin
        lbCodigo.caption:=Get_Codigo;
        EdtReferencia.text:=Get_Referencia;
        EdtDescricao.text:=Get_Descricao;
        EdtUnidade.text:=Get_Unidade;
        EdtFornecedor.text:=Fornecedor.Get_codigo;
        LbNomeFornecedor.Caption:=Fornecedor.Get_RazaoSocial;
        EdtPeso.text:=Get_Peso;
        EdtPrecoCusto.text:=formata_valor(Get_PrecoCusto);
        EdtPorcentagemInstalado.text:=Get_PorcentagemInstalado;
        EdtPorcentagemFornecido.text:=Get_PorcentagemFornecido;
        EdtPorcentagemRetirado.text:=Get_PorcentagemRetirado;
        EdtPrecoVendaInstalado.text:=Get_PrecoVendaInstalado;
        EdtPrecoVendaFornecido.text:=Get_PrecoVendaFornecido;
        EdtPrecoVendaRetirado.text:=Get_PrecoVendaRetirado;
        EdtAlturaIdeal.text:=Get_AlturaIdeal;
        EdtLarguraIdeal.text:=Get_LarguraIdeal;
        EdtAlturaMinima.text:=Get_AlturaMinima;
        EdtLarguraMinima.text:=Get_LarguraMinima;
        EdtAlturaMaxima.text:=Get_AlturaMaxima;
        EdtLarguraMaxima.text:=Get_LarguraMaxima;
        EdtAreaMinima.text:=Get_AreaMinima;
        ComboIsentoICMS_Estado.text:=Get_IsentoICMS_Estado;
        ComboSubstituicaoICMS_Estado.text:=Get_SubstituicaoICMS_Estado;
        EdtValorPauta_Sub_Trib_Estado.text:=Get_ValorPauta_Sub_Trib_Estado;
        EdtAliquota_ICMS_Estado.text:=Get_Aliquota_ICMS_Estado;
        EdtReducao_BC_ICMS_Estado.text:=Get_Reducao_BC_ICMS_Estado;
        EdtAliquota_ICMS_Cupom_Estado.text:=Get_Aliquota_ICMS_Cupom_Estado;
        ComboIsentoICMS_ForaEstado.text:=Get_IsentoICMS_ForaEstado;
        ComboSubstituicaoICMS_ForaEstado.text:=Get_SubstituicaoICMS_ForaEstado;
        EdtValorPauta_Sub_Trib_ForaEstado.text:=Get_ValorPauta_Sub_Trib_ForaEstado;
        EdtAliquota_ICMS_ForaEstado.text:=Get_Aliquota_ICMS_ForaEstado;
        EdtReducao_BC_ICMS_ForaEstado.text:=Get_Reducao_BC_ICMS_ForaEstado;
        EdtAliquota_ICMS_Cupom_ForaEstado.text:=Get_Aliquota_ICMS_Cupom_ForaEstado;
        EdtSituacaoTributaria_TabelaA.text:=SituacaoTributaria_TabelaA.Get_codigo;
        EdtSituacaoTributaria_TabelaB.text:=SituacaoTributaria_TabelaB.Get_codigo;
        EdtClassificacaoFiscal.text:=Get_ClassificacaoFiscal;
        EdtPlanodeContas.Text:=PlanoDeContas.Get_CODIGO;
        edtNCM.Text:=Get_NCM;
        edtpercentualagregado.Text:=Get_percentualagregado;
        edtCest.Text := Get_cest;
        if(Get_Ativo='S') then
        begin
            chkAtivo.Checked:=True;
            lbAtivoInativo.Caption:='Ativo';
        end
        else
        begin
            chkAtivo.Checked:=False;
            lbAtivoInativo.Caption:='Inativo';
        end;
        
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFKITBOX.TabelaParaControles: Boolean;
begin
     If (ObjKitBoxCor.KitBox.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFKITBOX.EdtFornecedorExit(Sender: TObject);
begin
    ObjKitBoxCor.KitBox.EdtFornecedorExit(Sender, LbNomeFornecedor);
end;

procedure TFKITBOX.EdtFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    ObjKitBoxCor.KitBox.EdtFornecedorKeyDown(Sender, Key, Shift, LbNomeFornecedor);
end;

procedure TFKITBOX.edtSituacaoTributaria_TabelaAExit(Sender: TObject);
begin
    ObjKitBoxCor.KitBox.EdtSituacaoTributaria_TabelaAExit(Sender,Nil);
end;

procedure TFKITBOX.edtSituacaoTributaria_TabelaAKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjKitBoxCor.KitBox.EdtSituacaoTributaria_TabelaAKeyDown(Sender, key, Shift,Nil );
end;

procedure TFKITBOX.edtSituacaoTributaria_TabelaBExit(Sender: TObject);
begin
    ObjKitBoxCor.KitBox.EdtSituacaoTributaria_TabelaBExit(Sender, Nil);
end;

procedure TFKITBOX.EdtSituacaoTributaria_TabelaBKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  ObjKitBoxCor.KitBox.EdtSituacaoTributaria_TabelaBKeyDown(Sender, key, Shift, Nil);
end;

procedure TFKITBOX.EdtPorcentagemInstaladoExit(Sender: TObject);
Var
  PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.Text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaInstalado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(Tira_POnto(EdtPorcentagemInstalado.Text)))/100)));
end;

procedure TFKITBOX.EdtPorcentagemFornecidoExit(Sender: TObject);
Var
  PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(Tira_Ponto(EdtPrecoCusto.Text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaFornecido.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(tira_ponto(EdtPorcentagemFornecido.Text)))/100)));
end;

procedure TFKITBOX.EdtPorcentagemRetiradoExit(Sender: TObject);
Var
    PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(Tira_Ponto(EdtPrecoCusto.Text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaRetirado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(tira_ponto(EdtPorcentagemRetirado.Text)))/100)));
end;

procedure TFKITBOX.tsGuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     {if (newtab=2)//cor
     Then Begin
               LbEstoque.Caption:='';
               if (ObjKitBoxCor.Status=dsinsert)
               or (lbCodigo.caption='')
               Then Begin
                         AllowChange:=False;
                         exit;
               End;

               Self.PreparaCor;
               EdtCorReferencia.SetFocus;

     End
     Else Begin
             if (newtab=1)//Impostos
             Then Begin
                      if (OBJkiTBOXCOR.kiTBOX.Status=dsinsert)
                      or (lbCodigo.caption='')
                      Then Begin
                                AllowChange:=False;
                                exit;
                      End;
                      Notebook.PageIndex:=NewTab;
                      FRImposto_ICMS1.Acessaframe(lbCodigo.caption);


             End
             Else Begin
                       Notebook.PageIndex:=NewTab;

             End;
     end;  }

end;

procedure TFKITBOX.PreparaCor;
begin
     //Resgatando as Cores para essa ferragem
     Self.ResgataCores;
     //habilitando os edits desse panel
     habilita_campos(PanelCor);
     EdtPorcentagemAcrescimo.Enabled:=False;
     EdtPorcentagemAcrescimoFinal.Enabled:=False;
     EdtValorCusto.Enabled:=false;


     edtinstaladofinal.Enabled:=False;
     edtfornecidofinal.Enabled:=False;
     edtretiradofinal.Enabled:=False;
     {if(PAlteraCampoEstoque=True)
     then EdtEstoque.Enabled:=True
     else
     EdtEstoque.Enabled:=False;}

     lbnomecor.caption:='';
     LbCusto.caption:='Custo'+#13+'R$ '+EdtPrecoCusto.text;
end;

procedure TFKITBOX.ResgataCores;
begin
     ObjKitBoxCor.ResgataCorKitBox(lbCodigo.caption);
     formatadbgrid(DBGRIDCOR);
     DBGRIDCOR.Ctl3D:=False;
end;

procedure TFKITBOX.btGravarCorClick(Sender: TObject);
begin
    With ObjKitBoxCOR do
    Begin
        ZerarTabela;

        if (edtcodigoCOR.text='')
        or (edtcodigoCOR.text='0')
        Then Begin
                Status:=dsInsert;
                edtcodigoCOR.Text:='0';
        End
        Else Status:=dsEdit;

        Submit_Codigo(edtcodigoCOR.Text);
        Submit_classificacaofiscal(edtclassificacaofiscal_COR.Text);
        KitBox.Submit_codigo(lbCodigo.caption);
        Cor.Submit_codigo(edtCor.text);
      {  if (EdtEstoque.text='')
        Then EdtEstoque.Text:='0';
        //Submit_Estoque(edtEstoque.text);  }
        Submit_PorcentagemAcrescimo(EdtPorcentagemAcrescimo.Text);
        Submit_AcrescimoExtra(edtAcrescimoExtra.text);
        Submit_ValorExtra(EdtValorExtra.Text);
        Submit_ValorFinal(EdtValorFinal.Text);
      //  Submit_Estoque(EdtEstoque.Text);
        Submit_EstoqueMinimo( edtEstoqueMinimo.Text );

        if (Salvar(True)=False)
        Then Begin
                  EdtCorReferencia.SetFocus;
                  exit;
        End;
        lbnomecor.caption:='';
        limpaedit(panelcor);
        EdtCorReferencia.SetFocus;
        Self.ResgataCores;
    End;

end;

procedure TFKITBOX.BtExcluirCorClick(Sender: TObject);
begin
     if (edtcodigoCOR.Text='')then
     exit;

     If (DBGRIDCOR.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGRIDCOR.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir essa cor da Ferragem Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     ObjKitBoxCor.Exclui(DBGRIDCOR.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     limpaedit(panelcor);
     Self.ResgataCores;

end;

procedure TFKITBOX.BtCancelarCorClick(Sender: TObject);
begin
     limpaedit(panelcor);
     EdtValorCusto.Clear;
     EdtPorcentagemAcrescimo.Clear;
     EdtValorExtra.Clear;
     EdtAcrescimoExtra.Clear;
     EdtPorcentagemAcrescimoFinal.Clear;
     EdtValorFinal.Clear;     
     lbnomecor.caption:='';
     EdtCorReferencia.SetFocus;
end;

procedure TFKITBOX.EdtCorReferenciaExit(Sender: TObject);
begin
   ObjKitBoxCor.EdtCor_PorcentagemCor_Exit(Sender, EdtCor, lbnomecor, EdtPorcentagemAcrescimo);
   EdtValorCusto.Enabled:=false;
   EdtValorCusto.Text:= formata_valor(ObjKitBoxCor.KitBox.Get_PrecoCusto);

   GroupBoxVidros.Enabled:=true;
   edtacrescimoExtra.enabled:=true;
   EdtValorExtra.Enabled:=true;
  
end;

procedure TFKITBOX.EdtCorReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ObjKitBoxCor.EdtCorKeyDown(Sender, EdtCor, key, Shift, lbnomecor);
end;

procedure TFKITBOX.EdtAcrescimoExtraExit(Sender: TObject);
Var PValorCusto, PAcrescimoExtra, PValorExtra :Currency;
begin
    if ((TEdit(Sender).Text=''))then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoCusto.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaInstalado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaFornecido.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaRetirado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtCor.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;

    PValorCusto:=0;
    PAcrescimoExtra:=0;
    PValorExtra:=0;

    try
          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));
          
          PAcrescimoExtra:=StrTOCurr(tira_ponto(EdtAcrescimoExtra.Text));
          PValorExtra:=((PValorCusto*PAcrescimoExtra)/100);
          EdtValorExtra.Text:=Formata_Valor(PValorExtra);
    except;
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais.');
    end;

    EdtPorcentagemAcrescimoFinal.Text:=CurrToStr(StrToCurr(tira_ponto(EdtPorcentagemAcrescimo.Text))+StrToCurr(tira_ponto(EdtAcrescimoExtra.Text)));

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(tira_ponto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
end;

procedure TFKITBOX.DBGRIDCORDblClick(Sender: TObject);
begin
     If (DBGRIDCOR.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGRIDCOR.DataSource.DataSet.RecordCount=0)
     Then exit;

     limpaedit(PanelCor);
     lbnomecor.caption:='';
     if (ObjKitBoxCor.LocalizaCodigo(DBGRIDCOR.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataCores;
               exit;
     End;

     With ObjKitBoxCor do
     Begin
          TabelaparaObjeto;
          edtcodigoCOR.text:=Get_Codigo;
          edtclassificacaofiscal_COR.text:=Get_classificacaofiscal;

          EdtCor.text:=Cor.Get_codigo;
          EdtCorReferencia.Text:=Cor.Get_Referencia;
          lbnomecor.caption:=Cor.Get_Descricao;
          EdtPorcentagemAcrescimo.text:=Get_PorcentagemAcrescimo;
          LbEstoque.Caption:=RetornaEstoque+' '+KitBox.Get_Unidade;
          edtEstoqueMinimo.Text := Get_EstoqueMinimo;

          EdtAcrescimoExtra.text:=Get_AcrescimoExtra;
          EdtPorcentagemAcrescimoFinal.text:=Get_PorcentagemAcrescimoFinal;

          EdtValorCusto.Enabled:=false;
          EdtValorExtra.Enabled:=true;
          EdtAcrescimoExtra.Enabled:=true;
          EdtValorExtra.Text:=formata_valor(Get_ValorExtra);
          EdtValorFinal.Text:=formata_valor(Get_ValorFinal);
          EdtValorCusto.Text:=formata_valor(KitBox.Get_PrecoCusto);

          edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
          edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
          edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));

          EdtCorReferencia.SetFocus;

     End;

end;

procedure TFKITBOX.EdtValorExtraExit(Sender: TObject);
Var
PValorCusto, PPorcentagemExtra, PValorExtra : Currency;
begin
    if ((TEdit(Sender).Text=''))then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoCusto.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaInstalado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaFornecido.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaRetirado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtCor.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;

    TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);

    PValorCusto:=0;
    PValorExtra:=0;
    PPorcentagemExtra:=0;

    try
          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));
          
          PValorExtra:=StrToCurr(Tira_Ponto(EdtValorExtra.Text));

          PPorcentagemExtra:=(PValorExtra*100)/PValorCusto;
          EdtAcrescimoExtra.Text:=FormatFloat('#,##0.00',PPorcentagemExtra);
    except
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais');
    end;
    //********** Atualiza Valor Final, s� pra mostra porque este campo � Computed By
    EdtPorcentagemAcrescimoFinal.Text :=formata_valor(StrToCurr(Tira_Ponto(EdtPorcentagemAcrescimo.Text))+
                                      StrToCurr(tira_ponto(EdtAcrescimoExtra.Text))
                                      );

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(Tira_POnto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
end;

procedure TFKITBOX.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjKitBoxCor.KitBox.PrimeiroRegistro = false)then
    exit;

    ObjKitBoxCor.KitBox.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFKITBOX.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigo.caption='')then
    lbCodigo.caption:='0';

    if  (ObjKitBoxCor.KitBox.RegistoAnterior(StrToInt(lbCodigo.caption)) = false)then
    exit;

    ObjKitBoxCor.KitBox.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFKITBOX.btProximoClick(Sender: TObject);
begin
    if (lbCodigo.caption='')then
    lbCodigo.caption:='0';

    if  (ObjKitBoxCor.KitBox.ProximoRegisto(StrToInt(lbCodigo.caption)) = false)then
    exit;

    ObjKitBoxCor.KitBox.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFKITBOX.btUltimoClick(Sender: TObject);
begin
    if  (ObjKitBoxCor.KitBox.UltimoRegistro = false)then
    exit;

    ObjKitBoxCor.KitBox.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;




procedure TFKITBOX.EdtPrecoCustoExit(Sender: TObject);
begin
      TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
      if(EdtPorcentagemInstalado.Text<>'')
      then EdtPorcentagemInstaladoExit(Sender);
      if(EdtPorcentagemFornecido.Text<>'')
      then EdtPorcentagemFornecidoExit(sender);
      if(EdtPorcentagemRetirado.Text<>'')
      then EdtPorcentagemRetiradoExit(Sender);
end;

procedure TFKITBOX.btOpcoesClick(Sender: TObject);
begin
     With FmenuRelatorios do
     Begin
          //era no titulo antes
          NomeObjeto:='UOBJKITBOX';

          With RgOpcoes do
          Begin
                items.clear;
                Items.Add('Atualiza Pre�o de Custo');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0 :  ObjKitBoxCor.KitBox.AtualizaPrecos;
          End;
     end;

end;

procedure TFKITBOX.EdtPlanodeContasExit(Sender: TObject);
begin
     ObjKitBoxCor.KitBox.EdtPlanoDeContasExit(sender,nil);
end;

procedure TFKITBOX.EdtPlanodeContasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjKitBoxCor.KitBox.EdtPlanoDeContasKeyDown(sender,key,shift,nil);
end;

procedure TFKITBOX.EdtPrecoVendaInstaladoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(come(EdtPrecoCusto.Text,'.'));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemInstalado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaInstalado.Text))/precocusto));
end;

procedure TFKITBOX.EdtPrecoVendaFornecidoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(come(EdtPrecoCusto.Text,'.'));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemfornecido.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendafornecido.Text))/precocusto));
end;

procedure TFKITBOX.EdtPrecoVendaRetiradoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(come(EdtPrecoCusto.Text,'.'));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemretirado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaretirado.Text))/precocusto));
end;

procedure TFKITBOX.DBGRIDCORKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Self.DBGRIDCORDblClick(sender);
end;

procedure TFKITBOX.DBGRIDCORDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGRIDCOR.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGRIDCOR.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGRIDCOR.DefaultDrawDataCell(Rect,DBGRIDCOR.Columns[DataCol].Field, State);
          End;
end;

procedure TFKITBOX.AbreComCodigo(parametro:string);
begin
    if(ObjKitBoxCor.KitBox.LocalizarReferencia(parametro))then
    begin
        ObjKitBoxCor.KitBox.TabelaparaObjeto;
        Self.ObjetoParaControles;
    end;
end;

procedure TFKITBOX.FormShow(Sender: TObject);
begin
      limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     pgcGuia.TabIndex:=0;
     ColocaUpperCaseEdit(Self);

     Try
        ObjKitBoxCor:=TObjKitBoxCor.create;
        Self.DBGRIDCOR.DataSource:=ObjKitBoxCor.ObjDatasource;
        FRImposto_ICMS1.ObjMaterial_ICMS:=tobjkitbox_icms.create(self);
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

       FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
    FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
      FescolheImagemBotao.PegaFiguraBotaopequeno(Btgravarcor,'BOTAOINSERIR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btcancelacor,'BOTAOCANCELARPRODUTO.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btexcluircor,'BOTAORETIRAR.BMP');

     habilita_botoes(Self);
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE KITBOX')=False)
     Then desab_botoes(Self);

     PAlteraCampoEstoque:=False;
     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('PERMITE ALTERAR O CAMPO ESTOQUE NO CADASTRO DE KITBOX')=True)
     Then PAlteraCampoEstoque:=true;

     Rg_forma_de_calculo_percentual.itemindex:=0;
     if (ObjParametroGlobal.ValidaParametro('FORMA DE CALCULO DE PERCENTUAL DE ACRESCIMO NAS CORES DO KITBOX')=True)
     then Begin
               if (ObjParametroGlobal.get_valor='1')
               then Rg_forma_de_calculo_percentual.itemindex:=1; 
     End;
     Rg_forma_de_calculo_percentual.enabled:=False;

     AlteraPrecoPeloCusto:=True;
     if (ObjParametroGlobal.ValidaParametro('CALCULA PRECO DE VENDA DO KITBOX PELO CUSTO E PERCENTUAIS?')=True)
     then Begin
               if (ObjParametroGlobal.get_valor<>'SIM')
               then AlteraPrecoPeloCusto:=false;
     End;

     lbCodigo.Caption:='';


     if(Tag<>0)
     then begin
        if(ObjKitBoxCor.KitBox.LocalizaCodigo(IntToStr(tag))=True)
        then
        begin
             ObjKitBoxCor.KitBox.TabelaparaObjeto;
             Self.ObjetoParaControles;
        end;


     end;

end;

procedure TFKITBOX.EdtFornecedorDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Ffornecedor:TFFORNECEDOR;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Ffornecedor:=TFFORNECEDOR.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabfornecedor','',Ffornecedor)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtFornecedor.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 LbNomeFornecedor.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('razaosocial').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Ffornecedor);

     End;
end;
procedure TFKITBOX.EdtPlanodeContasDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FplanoContas:TFplanodeContas;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FplanoContas:=TFplanodeContas.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabplanodecontas','',FplanoContas)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtPlanodeContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 //lbNomePlanodeContas.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FplanoContas);

     End;
end;

procedure TFKITBOX.edtNCMKeyPress(Sender: TObject; var Key: Char);
begin
      if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFKITBOX.LbEstoqueMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFKITBOX.LbEstoqueMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFKITBOX.LbEstoqueClick(Sender: TObject);
begin
     if(ObjParametroGlobal.ValidaParametro('UTILIZA SPED FISCAL')=false) then
     begin
          MensagemAviso('O Parametro "UTILIZA SPED FISCAL" n�o foi encontrado!!!');
          Exit;
     end;
     if(ObjParametroGlobal.Get_Valor='SIM')
     then Exit;

     If(PAlteraCampoEstoque=False)then
     begin
          MensagemAviso('O Usu�rio '+ObjUsuarioGlobal.Get_nome+' n�o tem autoriza��o para alterar o estoque');
          Exit;
     end
     else
     begin
          with FfiltroImp do
          begin
              DesativaGrupos;
              Grupo01.Enabled:=True;
              edtgrupo01.Text:='';
              LbGrupo01.Caption:='QUANTIDADE';
              edtgrupo01.OnKeyPress:=edtquant;

              MensagemAviso('Acerte o estoque: Digite a quantidade a tirar ou acrescentar no estoque');

              ShowModal;

              if(Tag=0)
              then Exit;

              if(edtgrupo01.Text='') or(edtgrupo01.Text='0')
              then Exit;

              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Status:=dsInsert;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Submit_DATA(FormatDateTime('dd/mm/yyyy',Now));
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('PELO MODO ACERTO DE ESTOQUE (VIDRO)');
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(edtgrupo01.Text);
              OBJESTOQUEGLOBAL.Submit_KITBOXCOR(ObjKITBOXCOR.Get_Codigo);

              if (OBJESTOQUEGLOBAL.Salvar(True)=False)
              Then Begin
                     MensagemErro('Erro na tentativa de salvar o registro de estoque do PERFILADO');
                     exit;
              End;

              lbestoque.Caption:=ObjKITBOXCOR.RetornaEstoque+' '+ObjKITBOXCor.KITBOX.Get_Unidade;


          end;
     end;
end;

procedure TFKITBOX.edtquant(Sender: TObject;
  var Key: Char);
begin
     if not (Key in['0'..'9',Chr(8),'-',',']) then
    begin
        Key:= #0;
    end;

end;

procedure TFKITBOX.chkAtivoClick(Sender: TObject);
var
  QueryPesq:TIBQuery;
  FERRODESATIVARMATERIAL:TFErrosDesativarMaterial;
begin
      if(lbCodigo.Caption='') or (lbCodigo.Caption='0')
      then Exit;

      QueryPesq:=TIBQuery.Create(nil);
      QueryPesq.Database:=FDataModulo.IBDatabase;
      FERRODESATIVARMATERIAL:=TFErrosDesativarMaterial.Create(nil);

      try
          if(chkAtivo.Checked=True)
          then lbAtivoInativo.Caption:='Ativo';

          if(chkAtivo.Checked=False) then
          begin
              with QueryPesq do
              begin
                  Close;
                  SQL.Clear;
                  SQL.Add('select * from tabkitbox_proj where kitbox='+lbCodigo.Caption);
                  Open;

                  if(recordcount=0)
                  then lbAtivoInativo.Caption:='Inativo'
                  else
                  begin
                       chkAtivo.Checked:=True;
                       FERRODESATIVARMATERIAL.PassaMaterial(lbCodigo.Caption,EdtDescricao.Text,'kitbox');
                       FERRODESATIVARMATERIAL.ShowModal;
                  end;

              end;


          end;
      finally
           FreeAndNil(QueryPesq);
           FERRODESATIVARMATERIAL.Free;
      end;



end;

procedure TFKITBOX.bt1Click(Sender: TObject);
begin
     FAjuda.PassaAjuda('CADASTRO DE KITBOX');
     FAjuda.ShowModal;
end;

procedure TFKITBOX.LbNomeFornecedorClick(Sender: TObject);
var
  FFornecedor:TFFORNECEDOR;
begin
  try
    FFornecedor:=TFFORNECEDOR.Create(nil);
  except
    Exit;
  end;

  try
    if(edtFornecedor.Text='')
    then Exit;

    FFornecedor.tag:=StrToInt(edtFornecedor.Text);
    FFornecedor.ShowModal;

  finally
    FreeAndNil(FFornecedor);
  end;
end;

procedure TFKITBOX.LbNomeFornecedorMouseLeave(Sender: TObject);
begin
TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFKITBOX.LbNomeFornecedorMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFKITBOX.EdtFornecedorKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFKITBOX.EdtPlanodeContasKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFKITBOX.EdtPrecoCustoKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFKITBOX.EdtAlturaIdealKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFKITBOX.EdtLarguraIdealKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFKITBOX.EdtAreaMinimaKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFKITBOX.EdtAlturaMinimaKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFKITBOX.EdtLarguraMinimaKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFKITBOX.EdtAlturaMaximaKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFKITBOX.EdtLarguraMaximaKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFKITBOX.lbnomecorClick(Sender: TObject);
var
  Fcor:TFCOR;
begin
   try
      Fcor:=TFCOR.Create(nil);
   except
      Exit;
   end;

   try
     if(EdtCor.Text='')
     then Exit;

     Fcor.Tag:=StrToInt(EdtCor.Text);
     fcor.ShowModal;
   finally
     FreeAndNil(Fcor);
   end; 

end;

procedure TFKITBOX.pgcGuiaChange(Sender: TObject);
begin
  if (pgcGuia.TabIndex=2)//cor
  Then Begin
    LbEstoque.Caption:='';
    if (ObjKitBoxCor.Status=dsinsert) or (lbCodigo.caption='')Then
    Begin
      exit;
    End;
    Self.PreparaCor;
    EdtCorReferencia.SetFocus;
  End
  Else
  Begin
    if (pgcGuia.TabIndex=1)//Impostos
    Then Begin
      if (OBJkiTBOXCOR.kiTBOX.Status=dsinsert) or (lbCodigo.caption='')Then
      Begin
        exit;
      End;
      FRImposto_ICMS1.Acessaframe(lbCodigo.caption);
    End
    Else
    Begin
      pgcGuia.TabIndex:=0
    End;
  end;
end;

procedure TFKITBOX.EdtCorReferenciaDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Fcor:TFCOR ;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fcor:=TFCOR.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabcor','',Fcor)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtCorReferencia.text:=FpesquisaLocal.QueryPesq.fieldbyname('referencia').asstring;
                                 lbnomecor.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fcor);

     End;
end;

procedure TFKITBOX.edtNCMKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  FDataModulo.edtNCMKeyDown(Sender,Key,Shift);

end;

procedure TFKITBOX.edtCestKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if Key=vk_f9 then
  begin
    Clipboard.AsText := EDTNCM.Text;
    With TObjCEST.Create do
    begin
      edtCESTkeydown(sender,Key,shift);
      Free;
    end;
  end;
end;

end.
