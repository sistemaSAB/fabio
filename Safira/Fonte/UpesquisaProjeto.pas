unit UpesquisaProjeto;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DB, DBGrids, ComCtrls, StdCtrls,IBCustomDataSet, IBQuery, IBDatabase,
  Mask, Buttons, UessencialGlobal, ExtCtrls;

type
  TFpesquisaProjeto = class(TForm)
    Panel1: TPanel;
    pnlCabecalho: TPanel;
    lb2: TLabel;
    pnl1: TPanel;
    DBGrid: TDBGrid;
    grp1: TGroupBox;
    ImageDesenho: TImage;
    lbNomeDesenho: TLabel;
    ds1: TDataSource;
    querypesq: TIBQuery;
    Image1: TImage;
    edtbusca: TMaskEdit;
    lbCadastrar: TLabel;
    lbSair: TLabel;
    lbrodape: TLabel;
    pnl2: TPanel;
    lbl1: TLabel;
    lblNomeCampo: TLabel;
    procedure DBGridKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure edtbuscaKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtcadastraClick(Sender: TObject);
    procedure edtbuscaExit(Sender: TObject);
    procedure DBGridDblClick(Sender: TObject);
    procedure BTSAIRClick(Sender: TObject);
    procedure DBGridKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure lbSairMouseLeave(Sender: TObject);
    procedure lbSairMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure DBGridCellClick(Column: TColumn);

  private
      FormCadastro:Tform;
      str_pegacampo:string;
      comandosql:string;
      ComplementoPorcentagem:string;
    function ResgataDesenho(PReferencia: string): string;
    function ResgataLocalDesenhos: string;
    { Private declarations }
  public
        Invisiveis:TStringList;
        Function PreparaPesquisa(comando:TStringList;TitulodoForm:String;FormCad:Tform):Boolean;overload;
        Function PreparaPesquisa(comando:TStringList;TitulodoForm:String;FormCad:Tform;BasedeDados:TIBDatabase):Boolean;overload;
        
        Function PreparaPesquisaULTIMAPESQUISA(comando:TStringList;TitulodoForm:String;FormCad:Tform):Boolean;overload;
        Function PreparaPesquisa(comando:string;TitulodoForm:String;FormCad:Tform):Boolean;overload;
        Function PreparaPesquisa(comando:string;TitulodoForm:String;FormCad:Tform;BasedeDados:TIBDatabase):Boolean;overload;
        
        Function PreparaPesquisaN(comando:string;TitulodoForm:String;NomeForm:string):Boolean;

  end;

var
     FpesquisaProjeto: TFpesquisaProjeto;



implementation

uses UDataModulo, UescolheImagemBotao;



{$R *.DFM}

procedure TFpesquisaProjeto.DBGridKeyPress(Sender: TObject; var Key: Char);
begin

  Case Key of

  #32: Begin
               str_pegacampo:=dbgrid.SelectedField.FieldName;

               Case dbgrid.SelectedField.datatype of

                 ftdatetime :  Begin
                                 edtBusca.EditMask:='00/00/0000 00:00:00';
                                 EdtBusca.width:=70;
                              End;
                 ftdate    :  Begin
                                 edtBusca.EditMask:='00/00/0000;1;_';
                                 EdtBusca.width:=70;
                              End;
                 ftTime    :  Begin
                                 edtBusca.EditMask:='00:00';
                                 EdtBusca.width:=70;
                              End;
                 ftInteger  :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=250;
                                EdtBusca.maxlength:=8
                             End;

                 ftbcd      :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=250;
                                EdtBusca.maxlength:=8
                             End;
                 ftfloat    :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=250;
                                EdtBusca.maxlength:=8
                             End;
                 ftString   :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=400;
                                EdtBusca.maxlength:=255;
                             End;
                 Else Begin
                           Messagedlg('Est� tipo de dados n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
                           DBGrid.setfocus;
                           exit;
                      End;

               End;
               EdtBusca.Text :='';
               EdtBusca.Visible :=true;
               EdtBusca.SetFocus;
       End;
  #13: Begin
            if  Self.QueryPesq.recordcount<>0
            Then Self.modalresult:=mrok
            Else Self.modalresult:=mrCancel;
       End;
  #27:Begin
           Self.modalresult:=mrcancel;
      End;
  End;

end;

procedure TFpesquisaProjeto.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
cont:integer;
begin
    //O Tag � usado para indicar a formatacao do Dbgrid, zero ele
    //para nao influenciar em outras pesquisas
    Self.tag:=0;
    
    if edtbusca.Visible = true
    then  edtbusca.Visible:= false;


    if (Invisiveis<>nil)
    Then Begin
              Invisiveis.clear;
              for cont:=0 to dbgrid.Columns.Count-1 do
              Begin
                  if (dbgrid.Columns.Items[cont].Visible=False)
                  Then Invisiveis.add(inttostr(cont));
              End;
    End;

end;

procedure TFpesquisaProjeto.FormActivate(Sender: TObject);
var
cont:integer;
begin
     Self.Tag:=0;
     //PegaFiguraBotoes(nil,nil,nil,btcadastra,nil,nil,nil,btsair);
    FescolheImagemBotao.PegaFiguraImagem(Image1,'RODAPE');
     Case Self.QueryPesq.state of

        dsInactive: Begin
                        Messagedlg('A tabela n�o foi preparada antes de chamar o form!', mterror,[mbok],0);
                        abort;
                    End;
        Else Begin
                ComplementoPorcentagem:='';
                comandosql:='';
                comandosql:= Self.QueryPesq.SQL.Text;
                if (Self.Tag=3)
                Then Formatadbgrid_3_casas(dbgrid)
                Else formatadbgrid(dbgrid);
                
                dbgrid.setfocus;
                If (ObjParametroGlobal.ValidaParametro('% PADRAO NA PESQUISA EM CAMPOS NOME')=True)
                Then Begin
                          If (ObjParametroGlobal.Get_Valor='SIM')
                          Then ComplementoPorcentagem:='%'
                          Else ComplementoPorcentagem:='';
                End;

                if (Invisiveis<>nil)
                Then Begin
                          for cont:=0 to Invisiveis.Count-1 do
                          Begin
                               try
                                  strtoint(Invisiveis[cont]);
                                  if (DBGrid.Columns.Count>Cont)
                                  Then dbgrid.Columns.Items[strtoint(Invisiveis[cont])].Visible :=False;
                               except
                               end;

                          End;
                End;


             End;
     End;
end;

procedure TFpesquisaProjeto.edtbuscaKeyPress(Sender: TObject; var Key: Char);
var
 int_busca:integer;
 str_busca:string;
 flt_busca:Currency;
 data_busca:Tdate;
 hora_busca:Ttime;
 datahora_busca:tdatetime;
 indice_grid:integer;
begin

   if key =#27//ESC
   then Begin
           EdtBusca.Visible := false;
           dbgrid.SetFocus;
           exit;
        End;

   If Key=#13// Enter - procura
   Then Begin

          {Rodolfo}
          if (edtbusca.Text='')
          then exit;

          str_busca := StrReplaceRef(edtbusca.Text); {Rodolfo}

          //ordenando pela coluna antes de procurar
          If ( Self.QueryPesq.recordcount>0)
          Then Begin
            try
             indice_grid:=self.DBGrid.SelectedIndex;
             Self.QueryPesq.close;
             Self.QueryPesq.sql.clear;
             Self.QueryPesq.sql.add(comandosql+' order by '+str_pegacampo);
             Self.QueryPesq.open;
             formatadbgrid(dbgrid,querypesq);
             self.DBGrid.SelectedIndex:=indice_grid;
           except
           end;
          end;

          Case dbgrid.SelectedField.DataType of


             ftstring    :Begin
                               //utilizar a pesquisa com like "testar"S
                               try
                                        indice_grid:=self.DBGrid.SelectedIndex;
                                        Self.QueryPesq.close;
                                        Self.QueryPesq.sql.clear;
                                        If(Pos('WHERE',UpperCase(comandosql))<>0)
                                        Then Self.QueryPesq.sql.add(comandosql+' and UPPER('+str_pegacampo+') like '+#39
+ComplementoPorcentagem+str_busca+ComplementoPorcentagem+#39)
                                        Else Self.QueryPesq.sql.add(comandosql+' where UPPER('+str_pegacampo+') like '+#39
+ComplementoPorcentagem+str_busca+ComplementoPorcentagem+#39);

                                        Self.QueryPesq.sql.add(' order by '+str_pegacampo);
                                        Self.QueryPesq.open;
                                        //self.querypesq.SQL.SaveToFile('c:\ronnei.txt');
                                        formatadbgrid(dbgrid,querypesq);
                                        self.DBGrid.SelectedIndex:=indice_grid;
                                except
                                end;
                                //Self.QueryPesq.Locate(str_pegacampo,edtbusca.text,[Lopartialkey]);
                          End;
                          
             ftinteger   :Begin
                              try int_busca:=Strtoint(str_busca); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,int_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftfloat     :Begin
                              try flt_busca:=Strtofloat(str_busca); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,flt_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftBcd      :Begin
                              try flt_busca:=Strtofloat(str_busca); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,flt_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftDate     :Begin
                              try data_busca:=Strtodate(str_busca); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,data_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             fttime     :Begin
                              try hora_busca:=Strtodate(str_busca); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,hora_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;

             ftDateTime :Begin
                              try datahora_busca:=Strtodatetime(str_busca); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,datahora_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             Else Begin
                       Messagedlg('Este dado n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
                  End;
             End;


             Dbgrid.SetFocus;
             exit;
        End;

//Aqui defino as regras que podem ser digitadas
//como estou utilizando maskEdit Para Datas e Horas
//N�o preciso pois defini uma mascara, mas para
//float e Integer preciso

        Case dbgrid.SelectedField.DataType OF

                ftInteger:  Begin
                                 if not (Key in ['0'..'9',#8])
                                 Then key:=#0;
                            End;
                ftFloat  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftCurrency  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftBCD  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;


        End;
End;


procedure TFpesquisaProjeto.DBGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
indice_grid:integer;
cont,marca_registro:integer ;
comandoordena:String;
ColunaAtual:integer;
begin
     if (key=VK_Delete)
     then begin
               dbgrid.Columns.Items[dbgrid.SelectedIndex].Visible :=false;
               dbgrid.SelectedIndex :=dbgrid.SelectedIndex +1;
     end;



     if (key=VK_f12)
     Then Begin


                     try
                         ColunaAtual:=DbGrid.SelectedIndex;
                         str_pegacampo:=DbGrid.SelectedField.FieldName;
                         if (Length(str_pegacampo)>4)
                         Then Begin
                                   if (uppercase(copy(str_pegacampo,1,2))='XX')
                                   and (uppercase(copy(str_pegacampo,length(str_pegacampo)-1,2))='XX')
                                   Then exit;
                         End;



                         indice_grid:=self.DBGrid.SelectedIndex;
                         marca_registro:= self.DBGrid.DataSource.DataSet.fieldbyname('codigo').AsInteger;
                         ComandoOrdena:=comandosql+' order by ';

                         If (ssCtrl in Shift)
                         Then Begin  //Control pressionado, sinal de ordem por mais de uma coluna
                                   //procuro a palavra order se encontrar significa que posso ordenar pelo proximo campo
                                   If(Pos('order ',Self.QueryPesq.sql.text)<>0)
                                   Then ComandoOrdena:=Self.QueryPesq.sql.text+','
                                   Else ComandoOrdena:=comandosql+' order by ';
                              End;

                         Self.QueryPesq.close;
                         Self.QueryPesq.sql.clear;
                         Self.QueryPesq.sql.add(Comandoordena+str_pegacampo);
                         Self.QueryPesq.open;
                         DbGrid.SelectedIndex:=ColunaAtual;
                         formatadbgrid(dbgrid,querypesq);
                        
                        //self.DBGrid.DataSource.DataSet.Locate( 'codigo',marca_registro,[loCaseInsensitive]);
                        //self.DBGrid.SelectedIndex:=indice_grid;
                        //self.DBGrid.DataSource.DataSet.GotoBookmark(marca_registro);

                     except

                     end;
                //End
                //Else Begin
                 //       If (Self.QueryPesq.recordcount=0)
                 //       Then messagedlg('N�o Existem Registros para Serem Ordenados!',mterror,[mbok],0)
                  //      Else messagedlg('Selecione um Campo para Ordena��o e Pressione Ordenar!',mtwarning,[mbok],0);
                   //  End;
                DbGrid.setfocus;
          End
     Else Begin
               if key=VK_F11
               Then Begin
                         for cont:=0 to dbgrid.Columns.Count-1 do
                         dbgrid.Columns.Items[cont].Visible :=true;
               End;
     End;


end;

function TFpesquisaProjeto.PreparaPesquisa(comando: string;TitulodoForm:String;FormCad:Tform): Boolean;
begin
    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql.add(comando); 
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                     lbCadastrar.visible:=True;
                 End
            Else Self.lbcadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;


function TFpesquisaProjeto.PreparaPesquisa(comando: string;TitulodoForm:String;FormCad:Tform;BasedeDados:TIBDatabase): Boolean;
begin
    querypesq.Database:=basededados;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql.add(comando); 
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.lbCadastrar.visible:=True;
                 End
            Else Self.lbCadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

procedure TFpesquisaProjeto.BtcadastraClick(Sender: TObject);
begin


     If (Self.FormCadastro<>nil)
     Then Begin

               IF Self.FormCadastro.Visible=False
               Then Self.FormCadastro.showmodal
               Else Self.FormCadastro.show;

               Self.QueryPesq.close;
               Self.QueryPesq.open;
               Self.DBGrid.setfocus;
          End;
end;

procedure TFpesquisaProjeto.edtbuscaExit(Sender: TObject);
begin
     edtbusca.Visible:=False;
end;


function TFpesquisaProjeto.PreparaPesquisa(comando: TStringList;
  TitulodoForm: String; FormCad: Tform): Boolean;
begin
    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql:=comando;
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.lbCadastrar.visible:=True;
                 End
            Else Self.lbCadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;


end;


function TFpesquisaProjeto.PreparaPesquisa(comando: TStringList;
  TitulodoForm: String; FormCad: Tform;BasedeDados:TIBDatabase): Boolean;
begin
    querypesq.Database:=BasedeDados;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql:=comando;
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.lbCadastrar.visible:=True;
                 End
            Else Self.lbCadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;
end;



procedure TFpesquisaProjeto.DBGridDblClick(Sender: TObject);
begin
    if  Self.QueryPesq.recordcount<>0
    Then Self.modalresult:=mrok
    Else Self.modalresult:=mrCancel;
        
end;

function TFpesquisaProjeto.PreparaPesquisaN(comando: string; TitulodoForm:String;NomeForm: string): Boolean;
begin
    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql.add(comando);
            open;
            If (NomeForm<>'')
            Then Begin
                      //verificar se � possicel chamar um form pelo nome
                      FormCadastro:=Tform(Application.FindComponent(Nomeform));
                      Self.lbCadastrar.visible:=True;
                 End
            Else Self.lbCadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;
end;

procedure TFpesquisaProjeto.BTSAIRClick(Sender: TObject);
begin
     Self.Close;
end;

function TFpesquisaProjeto.PreparaPesquisaULTIMAPESQUISA(comando: TStringList;
  TitulodoForm: String; FormCad: Tform): Boolean;
begin

    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql:=comando;
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.lbCadastrar.visible:=True;
                 End
            Else Self.lbCadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;


procedure TFpesquisaProjeto.DBGridKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var CaminhoDesenho : string;
begin
      try
         ImageDesenho.Visible:=true;
         CaminhoDesenho:=Self.ResgataDesenho(querypesq.fieldbyname('Referencia').AsString);

         if (CaminhoDesenho<>'')then
         Begin
             ImageDesenho.Picture.LoadFromFile(CaminhoDesenho);

             LbNomeDesenho.Caption:=querypesq.fieldbyname('Referencia').AsString;
         end else
         ImageDesenho.Visible:=false;

      except
          MensagemErro('Erro ao tentar transferir o desenho.');
          ImageDesenho.Visible:=false;
      end;
end;

function TFPesquisaProjeto.ResgataDesenho(PReferencia : string): string;
Var
  LocalDesenho :string;
begin

  LocalDesenho := '';
  LocalDesenho := Self.ResgataLocalDesenhos+PReferencia+'.jpg';

  if (FileExists(LocalDesenho))then
    Result := LocalDesenho
  else
  begin

    LocalDesenho := '';
    LocalDesenho := Self.ResgataLocalDesenhos+PReferencia+'.bmp';

    if (FileExists(LocalDesenho)) then
      Result := LocalDesenho
    else
      Result := '';

  end;

end;

function TFPesquisaProjeto.ResgataLocalDesenhos: string;
var
  temp:string;
begin
     result:='';
     If (ObjParametroGlobal.ValidaParametro('CAMINHO DOS DESENHOS')=FALSE)
     Then Begin
               Messagedlg('Par�metro n�o encontrado "CAMINHO DOS DESENHOS"!', mterror,[mbok],0);
               exit;
     End;
     temp := VerificaBarraFinalDiretorio( ObjParametroGlobal.Get_Valor );
     result := temp;
end;


procedure TFpesquisaProjeto.DBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGrid.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGrid.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGrid.DefaultDrawDataCell(Rect, DBGrid.Columns[DataCol].Field, State);
          End;
end;

procedure TFpesquisaProjeto.lbSairMouseLeave(Sender: TObject);
begin
       TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFpesquisaProjeto.lbSairMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;


procedure TFpesquisaProjeto.DBGridCellClick(Column: TColumn);
Var
  CaminhoDesenho : string;
begin

  lblNomeCampo.Caption := dbgrid.SelectedField.FieldName; {Rodolfo}

  try

    ImageDesenho.Visible := true;
    CaminhoDesenho := Self.ResgataDesenho( querypesq.fieldbyname('Referencia').AsString );

    if (CaminhoDesenho <> '')then
    Begin

      ImageDesenho.Picture.LoadFromFile( CaminhoDesenho );
      LbNomeDesenho.Caption := querypesq.fieldbyname('Referencia').AsString;
      
    end
    else
      ImageDesenho.Visible := false;

  except

    MensagemErro('Erro ao tentar transferir o desenho.');
    ImageDesenho.Visible := false;

  end;
end;

end.


