object FEscolheOpcoesPersiana: TFEscolheOpcoesPersiana
  Left = 33
  Top = 27
  Width = 556
  Height = 483
  Caption = 'Escolha as Op'#231#245'es para a Persiana Atual'
  Color = clCaptionText
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 6
    Width = 39
    Height = 13
    Caption = 'Grupo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 211
    Top = 6
    Width = 22
    Height = 13
    Caption = 'Cor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 440
    Top = 6
    Width = 60
    Height = 13
    Caption = 'Diametro'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object RadioGroup1: TRadioGroup
    Left = 4
    Top = 20
    Width = 200
    Height = 429
    Color = clWhite
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Items.Strings = (
      'Rol'#244' 1'
      'Rol'#244' 2'
      'Rol'#244' 3')
    ParentColor = False
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
  end
  object CheckListBox1: TCheckListBox
    Left = 208
    Top = 26
    Width = 225
    Height = 423
    ItemHeight = 13
    Items.Strings = (
      'VER - Vermelho'
      'BRA - Branco'
      'PRA - Prata')
    TabOrder = 1
  end
  object CheckListBox2: TCheckListBox
    Left = 440
    Top = 25
    Width = 103
    Height = 216
    ItemHeight = 13
    Items.Strings = (
      '5 mm.'
      '10 mm.'
      '15 mm.')
    TabOrder = 2
  end
  object BitBtn1: TBitBtn
    Left = 440
    Top = 248
    Width = 105
    Height = 33
    Caption = '&OK'
    TabOrder = 3
  end
  object BitBtn2: TBitBtn
    Left = 440
    Top = 283
    Width = 105
    Height = 33
    Caption = '&Cancelar'
    TabOrder = 4
  end
end
