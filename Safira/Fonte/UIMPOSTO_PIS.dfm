object FIMPOSTO_PIS: TFIMPOSTO_PIS
  Left = 371
  Top = 105
  Width = 889
  Height = 525
  Caption = 'Cadastro de Impostos - PIS - EXCLAIM TECNOLOGIA'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lb1: TLabel
    Left = 429
    Top = 378
    Width = 178
    Height = 13
    Caption = 'F'#243'rmula Valor do Imposto (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb2: TLabel
    Left = 21
    Top = 382
    Width = 189
    Height = 13
    Caption = 'F'#243'rmula da Base de C'#225'lculo (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbALIQUOTAPERCENTUAL_ST: TLabel
    Left = 21
    Top = 296
    Width = 139
    Height = 13
    Caption = 'Al'#237'quota Percentual (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbALIQUOTAVALOR_ST: TLabel
    Left = 21
    Top = 336
    Width = 131
    Height = 13
    Caption = 'Al'#237'quota em Valor (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbTIPOCALCULO_ST: TLabel
    Left = 21
    Top = 247
    Width = 117
    Height = 13
    Caption = 'Tipo de C'#225'lculo (ST)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb3: TLabel
    Left = 429
    Top = 201
    Width = 149
    Height = 13
    Caption = 'F'#243'rmula Valor do Imposto'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb4: TLabel
    Left = 21
    Top = 203
    Width = 160
    Height = 13
    Caption = 'F'#243'rmula da Base de C'#225'lculo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbALIQUOTAPERCENTUAL: TLabel
    Left = 21
    Top = 165
    Width = 110
    Height = 13
    Caption = 'Al'#237'quota Percentual'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbTIPOCALCULO: TLabel
    Left = 21
    Top = 127
    Width = 88
    Height = 13
    Caption = 'Tipo de C'#225'lculo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbST: TLabel
    Left = 21
    Top = 89
    Width = 108
    Height = 13
    Caption = 'Situa'#231#227'o Tribut'#225'ria'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbNomeST_PIS: TLabel
    Left = 144
    Top = 105
    Width = 108
    Height = 13
    Caption = 'Situa'#231#227'o Tribut'#225'ria'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbALIQUOTAVALOR: TLabel
    Left = 216
    Top = 165
    Width = 102
    Height = 13
    Caption = 'Al'#237'quota em Valor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object imgrodape: TImage
    Left = 0
    Top = 443
    Width = 873
    Height = 44
    Align = alBottom
    Stretch = True
  end
  object edtFORMULA_VALOR_IMPOSTO_ST_PIS: TEdit
    Left = 429
    Top = 396
    Width = 400
    Height = 19
    TabOrder = 0
  end
  object edtFORMULABASECALCULO_ST_PIS: TEdit
    Left = 21
    Top = 396
    Width = 400
    Height = 19
    TabOrder = 1
  end
  object edtALIQUOTAPERCENTUAL_ST_PIS: TEdit
    Left = 21
    Top = 313
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 2
  end
  object edtALIQUOTAVALOR_ST_PIS: TEdit
    Left = 21
    Top = 352
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 3
  end
  object cbbTIPOCALCULO_ST_PIS: TComboBox
    Left = 21
    Top = 263
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 4
    Text = '  '
    Items.Strings = (
      'Percentual'
      'Valor')
  end
  object edtFORMULA_VALOR_IMPOSTO_PIS: TEdit
    Left = 429
    Top = 216
    Width = 412
    Height = 19
    TabOrder = 5
  end
  object edtFORMULABASECALCULO_PIS: TEdit
    Left = 21
    Top = 217
    Width = 396
    Height = 19
    TabOrder = 6
  end
  object edtALIQUOTAPERCENTUAL_PIS: TEdit
    Left = 21
    Top = 181
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 7
  end
  object cbbTIPOCALCULO_PIS: TComboBox
    Left = 21
    Top = 143
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 8
    Text = '  '
    Items.Strings = (
      'Percentual'
      'Valor')
  end
  object edtST_PIS: TEdit
    Left = 21
    Top = 105
    Width = 100
    Height = 19
    Color = 6073854
    MaxLength = 9
    TabOrder = 9
    OnDblClick = edtST_PISDblClick
    OnExit = edtST_PISExit
    OnKeyDown = edtST_PISKeyDown
  end
  object edtALIQUOTAVALOR_PIS: TEdit
    Left = 216
    Top = 181
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 10
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 873
    Height = 54
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 11
    DesignSize = (
      873
      54)
    object lbnomeformulario: TLabel
      Left = 619
      Top = 3
      Width = 104
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Cadastro de'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigo: TLabel
      Left = 734
      Top = 9
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb5: TLabel
      Left = 643
      Top = 25
      Width = 32
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'PIS'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
      Spacing = 0
    end
  end
end
