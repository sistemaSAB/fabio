unit UobjPERFILADOCOR;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJPERFILADO,UOBJCOR;

Type
   TObjPERFILADOCOR=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Perfilado:TOBJPERFILADO;
                Cor:TOBJCOR;
                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaReferencia(Parametro:string) :boolean;
                Function    Localiza_Perfilado_Cor(Pperfilado,Pcor:string):boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_PorcentagemAcrescimo(parametro: string);
                Function Get_PorcentagemAcrescimo: string;
                Procedure Submit_AcrescimoExtra(parametro: string);
                Function Get_AcrescimoExtra: string;

                Procedure Submit_ValorExtra(parametro: string);
                Function Get_ValorExtra: string;

                Procedure Submit_ValorFinal(parametro: string);
                Function Get_ValorFinal: string;

                Procedure Submit_classificacaofiscal(parametro: STRing);
                Function Get_classificacaofiscal: STRing;

                procedure Submit_Estoque(parametro:string);
                function Get_Estoque:string;

                procedure Submit_EstoqueMinimo(parametro:string);
                function Get_EstoqueMinimo:string;

                Function Get_PorcentagemAcrescimoFinal: string;
                procedure EdtPerfiladoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtPerfiladoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtCorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtCor_PorcentagemCor_Exit(Sender: TObject;Var PEdtCodigo:TEdit;  LABELNOME:TLABEL; EdtPorcentagemCor:TEdit);
                procedure EdtCorKeyDown(Sender: TObject; Var PEdtCodigo:TEdit; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                Procedure ResgataCorPerfilado(PPerfilado:string);
                Function CadastraPerfiladoEmTodasAsCores(PPerfilado:string):Boolean;

                procedure EdtCorDePerfiladoKeyDown(Sender: TObject; var PEdtCodigo: TEdit; var Key: Word; Shift: TShiftState;LABELNOME: Tlabel);
                procedure EdtPerfiladoDisponivelNaCorKeyDown(Sender: TObject; var PEdtCodigo: TEdit; PCor: string; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
                function Get_PerfiladoNaCor(PCor: string): TStringList;
                procedure Relatorioestoque;
                procedure EdtperfiladoCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
                function RetornaEstoque: String;
                procedure DiminuiEstoque(quantidade,perfiladocor:string);
                function AumentaEstoque(quantidade,perfiladocor:string):Boolean;

          Private
               Objquery:Tibquery;
               ObjqueryEstoque:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               PorcentagemAcrescimo:string;
               AcrescimoExtra:string;
               ValorExtra :string;
               ValorFinal :string;
               classificacaofiscal :STRing;
               estoque:string;

               PorcentagemAcrescimoFinal:string;
               EstoqueMinimo:string;

               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;



   End;


implementation
uses Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UEscolheCor, UReltxtRDPRINT,rdprint, UPERFILADO;

Function  TObjPERFILADOCOR.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If(FieldByName('Perfilado').asstring<>'')
        Then Begin
                 If (Self.Perfilado.LocalizaCodigo(FieldByName('Perfilado').asstring)=False)
                 Then Begin
                          Messagedlg('Perfilado N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Perfilado.TabelaparaObjeto;
        End;
        If(FieldByName('Cor').asstring<>'')
        Then Begin
                 If (Self.Cor.LocalizaCodigo(FieldByName('Cor').asstring)=False)
                 Then Begin
                          Messagedlg('Cor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Cor.TabelaparaObjeto;
        End;
        Self.PorcentagemAcrescimo:=fieldbyname('PorcentagemAcrescimo').asstring;
        
        Self.AcrescimoExtra:=fieldbyname('AcrescimoExtra').asstring;
        Self.PorcentagemAcrescimoFinal:=fieldbyname('PorcentagemAcrescimoFinal').asstring;
        Self.ValorExtra:=fieldbyname('ValorExtra').AsString;
        Self.ValorFinal:=fieldbyname('ValorFinal').AsString;
        Self.classificacaofiscal:=fieldbyname('classificacaofiscal').AsString;
        self.estoque:=fieldbyname('estoque').AsString;
        self.EstoqueMinimo := Objquery.fieldbyname('EstoqueMinimo').AsString;
        result:=True;
     End;
end;


Procedure TObjPERFILADOCOR.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Perfilado').asstring:=Self.Perfilado.GET_CODIGO;
        ParamByName('Cor').asstring:=Self.Cor.GET_CODIGO;
        ParamByName('PorcentagemAcrescimo').asstring:=virgulaparaponto(Self.PorcentagemAcrescimo);

        ParamByName('AcrescimoExtra').asstring:=virgulaparaponto(Self.AcrescimoExtra);
        ParamByName('ValorExtra').AsString:=virgulaparaponto(Self.ValorExtra);
        ParamByName('ValorFinal').AsString:=virgulaparaponto(Self.ValorFinal);
        ParamByName('classificacaofiscal').AsString:=Self.classificacaofiscal;
        //ParamByName('estoque').AsString:=virgulaparaponto(Self.estoque);
        ParamByName('estoqueminimo').AsString:=virgulaparaponto(Self.EstoqueMinimo);
  End;
End;

//***********************************************************************

function TObjPERFILADOCOR.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPERFILADOCOR.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Perfilado.ZerarTabela;
        Cor.ZerarTabela;
        PorcentagemAcrescimo:='';

        AcrescimoExtra:='';
        PorcentagemAcrescimoFinal:='';
        ValorExtra:='';
        ValorFinal:='';
        classificacaofiscal:='';
        estoque:='';
        EstoqueMinimo := '';
     End;
end;

Function TObjPERFILADOCOR.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPERFILADOCOR.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Perfilado.LocalizaCodigo(Self.Perfilado.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Perfilado n�o Encontrado!';

      If (Self.Cor.LocalizaCodigo(Self.Cor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Cor n�o Encontrado!'
      Else Begin
                Self.Cor.TabelaparaObjeto;
                Self.PorcentagemAcrescimo:=Self.cor.Get_PorcentagemAcrescimo;
      End;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjPERFILADOCOR.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.Perfilado.Get_Codigo<>'')
        Then Strtoint(Self.Perfilado.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Perfilado';
     End;
     try
        If (Self.Cor.Get_Codigo<>'')
        Then Strtoint(Self.Cor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Cor';
     End;
     try
        Strtofloat(Self.AcrescimoExtra);
     Except
           Mensagem:=mensagem+'/AcrescimoExtra';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPERFILADOCOR.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPERFILADOCOR.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPERFILADOCOR.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PERFILADOCOR vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Perfilado,Cor,PorcentagemAcrescimo,AcrescimoExtra,ValorExtra');
           SQL.ADD(' ,PorcentagemAcrescimoFinal,ValorFinal,classificacaofiscal,estoque,EstoqueMinimo');
           SQL.ADD(' from  TabPerfiladoCor');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPERFILADOCOR.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPERFILADOCOR.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
          Self.Objquery.close;
          Self.Objquery.SQL.clear;
          Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
          Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
          Self.Objquery.ExecSQL;
          If (ComCommit=True)
          Then FDataModulo.IBTransaction.CommitRetaining;
        End
        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPERFILADOCOR.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin

        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryEstoque:=TIBQuery.create(nil);
        Self.ObjqueryEstoque.Database:=FDataModulo.IbDatabase;



        Self.ObjqueryPesquisa:=Tibquery.Create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjqueryPesquisa;

        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Perfilado:=TOBJPERFILADO.create;
        Self.Cor:=TOBJCOR.create;



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabPerfiladoCor(Codigo,Perfilado,Cor,PorcentagemAcrescimo');
                InsertSQL.add(' ,AcrescimoExtra, ValorExtra, ValorFinal, classificacaofiscal');//,estoque');
                InsertSql.Add(',EstoqueMinimo');
                InsertSql.Add(')');
                InsertSQL.add('values (:Codigo,:Perfilado,:Cor,:PorcentagemAcrescimo');
                InsertSQL.add(' ,:AcrescimoExtra,:ValorExtra,:ValorFinal,:classificacaofiscal');//,:estoque');
                InsertSql.Add(',:EstoqueMinimo');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabPerfiladoCor set Codigo=:Codigo,Perfilado=:Perfilado');
                ModifySQL.add(',Cor=:Cor,PorcentagemAcrescimo=:PorcentagemAcrescimo');
                ModifySQL.add(', AcrescimoExtra=:AcrescimoExtra,ValorExtra=:ValorExtra,ValorFinal=:ValorFinal');
                ModifySQL.add(',classificacaofiscal=:classificacaofiscal');//,estoque=:estoque');
                ModifySQl.Add(',EstoqueMinimo=:EstoqueMinimo');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabPerfiladoCor where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPERFILADOCOR.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPERFILADOCOR.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewPERFILADOCOR');
     Result:=Self.ParametroPesquisa;
end;

function TObjPERFILADOCOR.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de PERFILADOCOR ';
end;


function TObjPERFILADOCOR.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPERFILADOCOR,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENPERFILADOCOR,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPERFILADOCOR.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ObjqueryEstoque);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Freeandnil(Self.ObjqueryPesquisa);
    Freeandnil(Self.Objdatasource);
    Self.Perfilado.FREE;
    Self.Cor.FREE;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPERFILADOCOR.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPERFILADOCOR.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPerfiladoCor.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjPerfiladoCor.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjPerfiladoCor.Submit_PorcentagemAcrescimo(parametro: string);
begin
        Self.PorcentagemAcrescimo:=tira_ponto(Parametro);
end;
function TObjPerfiladoCor.Get_PorcentagemAcrescimo: string;
begin
        Result:=Self.PorcentagemAcrescimo;
end;
procedure TObjPerfiladoCor.Submit_AcrescimoExtra(parametro: string);
begin
        Self.AcrescimoExtra:=tira_ponto(Parametro);
end;
function TObjPerfiladoCor.Get_AcrescimoExtra: string;
begin
        Result:=Self.AcrescimoExtra;
end;
function TObjPerfiladoCor.Get_PorcentagemAcrescimoFinal: string;
begin
        Result:=Self.PorcentagemAcrescimoFinal;
end;


procedure TObjPERFILADOCOR.EdtPerfiladoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Perfilado.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Perfilado.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Perfilado.Get_Descricao;
End;
procedure TObjPERFILADOCOR.EdtPerfiladoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPerfiladoLocal: TFPERFILADO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPerfiladoLocal := TFPERFILADO.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Perfilado.Get_Pesquisa2,Self.Perfilado.Get_TituloPesquisa,FPerfiladoLocal)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Perfilado.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Perfilado.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Perfilado.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeandNil(FPerfiladoLocal);
     End;
end;
procedure TObjPERFILADOCOR.EdtCorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Cor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Cor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Cor.Get_Descricao;
End;
procedure TObjPERFILADOCOR.EdtCorKeyDown(Sender: TObject; Var PEdtCodigo:TEdit;var Key: Word; Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cor.Get_Pesquisa,Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.Text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjPERFILADOCOR.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPERFILADOCOR';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjPERFILADOCOR.ResgataCorPerfilado(PPerfilado: string);
begin
     With Self.ObjqueryPesquisa do
     Begin
          close;
          SQL.clear;
          SQL.ADD('Select TPC.Cor,TabCor.Descricao as NomeCor,TPC.PorcentagemAcrescimo');
          sqL.Add(' ,TPC.AcrescimoExtra');
          SQL.ADD(' ,TPC.PorcentagemAcrescimoFinal,TPC.codigo');
          SQL.ADD(' from  TabPerfiladoCor TPC');
          SQL.ADD(' join TabCor on TPC.Cor=Tabcor.codigo');

          SQL.ADD(' where Perfilado ='+PPerfilado);

          if (PPerfilado='')
          Then exit;
          open;
     End;

end;

procedure TObjPERFILADOCOR.EdtCor_PorcentagemCor_Exit(Sender: TObject;Var PEdtCodigo:TEdit;
  LABELNOME: TLABEL; EdtPorcentagemCor: TEdit);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Cor.LocalizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     Self.Cor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Cor.Get_Descricao;
     EdtPorcentagemCor.Text:=Self.Cor.Get_PorcentagemAcrescimo;
     PEdtCodigo.Text:=Self.Cor.Get_Codigo;

end;

function TObjPERFILADOCOR.LocalizaReferencia(Parametro: string): boolean;
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PERFILADOCOR vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Perfilado,Cor,PorcentagemAcrescimo, AcrescimoExtra, ValorExtra');
           SQL.ADD(' ,PorcentagemAcrescimoFinal,ValorFinal,classificacaofiscal,estoque,EstoqueMinimo');
           SQL.ADD(' from  TabPerfiladoCor');
           SQL.ADD(' WHERE Referencia ='+#39+parametro+#39);


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjPERFILADOCOR.CadastraPerfiladoEmTodasAsCores(PPerfilado: string): Boolean;
Var   Cont : Integer;
      PPOrcentagemAcrescimo:string;
begin
     Result:=false;
     if (Self.Perfilado.LocalizaCodigo(PPerfilado)=false)then
     Begin
          MensagemErro('Perfilado n�o encontrado.');
          Result:=false;
          exit;
     end;

     With  Self.ObjqueryPesquisa  do
     Begin
          Close;
          SQl.Clear;
          Sql.Add('Select Codigo,Referencia, Descricao from TabCor');
          Open;
          First;

          FEscolheCor.CheckListBoxCor.Clear;
          FEscolheCor.CheckListBoxCodigoCor.Clear;
          While not (Eof) do
          Begin
               FEscolheCor.CheckListBoxCor.Items.Add(fieldbyname('Referencia').AsString+' - '+fieldbyname('Descricao').AsString);
               FEscolheCor.CheckListBoxCodigoCor.Items.Add(fieldbyname('Codigo').AsString);
          Next;
          end;
          FEscolheCor.ShowModal;

          if FEscolheCor.Tag = 0 then
          Begin
               Result:=false;
               exit;
          end;

          // Cadastrando os cores
          for Cont:=0 to FEscolheCor.CheckListBoxCor.Items.Count-1 do
          Begin
               if FEscolheCor.CheckListBoxCodigoCor.Checked[Cont]=true then
               Begin
                    Self.Cor.LocalizaCodigo(FEscolheCor.CheckListBoxCodigoCor.Items.Strings[Cont]);
                    Self.Cor.TabelaparaObjeto;
                    PPOrcentagemAcrescimo:=Self.Cor.Get_PorcentagemAcrescimo;

                    Self.ZerarTabela;
                    Self.Status:=dsInsert;
                    Self.Submit_Codigo(Get_NovoCodigo);
                    Self.Perfilado.Submit_Codigo(PPerfilado);
                    Self.Cor.Submit_Codigo(FEscolheCor.CheckListBoxCodigoCor.Items.Strings[Cont]);
                    //Self.Submit_Estoque('0');
                    Self.Submit_AcrescimoExtra('0');
                    Self.Submit_PorcentagemAcrescimo(PPOrcentagemAcrescimo);
                    if (Self.Salvar(false)=false)then
                    Begin
                         MensagemErro('Erro ao tentar Salvar os Perfilados por Cor');
                         Result:=false;
                         exit;
                    end;
               end;
          end;

          Result:=true;
     end;
end;

function TObjPERFILADOCOR.Localiza_Perfilado_Cor(Pperfilado,
  Pcor: string): boolean;
begin
       if (Pperfilado='')
       or (Pcor='')
       Then Begin
                 Messagedlg('Escolha um Perfilado e uma Cor',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Perfilado,Cor,PorcentagemAcrescimo,AcrescimoExtra, ValorExtra');
           SQL.ADD(' ,PorcentagemAcrescimoFinal, ValorFinal, classificacaofiscal,estoque,EstoqueMinimo');
           SQL.ADD(' from  TabPerfiladoCor');
           SQL.ADD(' WHERE Perfilado='+Pperfilado);
           SQL.ADD(' and Cor='+pcor);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjPerfiladoCOR.Get_ValorExtra: string;
begin
    Result:=Self.ValorExtra;
end;

function TObjPerfiladoCOR.Get_ValorFinal: string;
begin
    Result:=Self.ValorFinal;
end;


function TObjPerfiladoCOR.Get_classificacaofiscal: string;
begin
    Result:=Self.classificacaofiscal;
end;


procedure TObjPerfiladoCOR.Submit_ValorExtra(parametro: string);
begin
    Self.ValorExtra:=tira_ponto(parametro);
end;

procedure TObjPerfiladoCOR.Submit_ValorFinal(parametro: string);
begin
    Self.ValorFinal:=tira_ponto(parametro);
end;


procedure TObjPerfiladoCOR.Submit_classificacaofiscal(parametro: STRing);
begin
    Self.classificacaofiscal:=parametro ;
end;

procedure TObjPerfiladoCOR.EdtCorDePerfiladoKeyDown(Sender: TObject; var PEdtCodigo: TEdit; var Key: Word; Shift: TShiftState;
  LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cor.Get_PesquisaCorPerfilado,Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPerfiladoCOR.EdtPerfiladoDisponivelNaCorKeyDown(
  Sender: TObject; var PEdtCodigo: TEdit; PCor: string; var Key: Word;
  Shift: TShiftState; LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PerfiladoNaCor(PCor),Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjPerfiladoCOR.Get_PerfiladoNaCor(PCor: string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select Distinct(TabPerfilado.Referencia),TabPerfilado.DESCRICAO, TabPerfilado.Unidade,');
     Self.ParametroPesquisa.add('TabPerfilado.Peso, TabPerfiladoCor.Codigo,TabPerfiladoCor.estoque');
     Self.ParametroPesquisa.add('from TabPerfiladoCor');
     Self.ParametroPesquisa.add('Join TabPerfilado on TabPerfilado.Codigo = TabPerfiladoCor.Perfilado');
     if (PCor <> '')then
     Self.ParametroPesquisa.add('Where TabPerfiladoCor.Cor = '+PCor);

     Result:=Self.ParametroPesquisa;

end;


procedure TObjperfiladoCOR.Relatorioestoque;
var
Pgrupo:string;
PsomaEstoque:integer;
PsomaValorTotal:Currency;
pdata:string;
perfilado:string;
begin
     With FfiltroImp do
     begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.caption:='Grupo Perfilado';
          edtgrupo01.OnKeyDown:=Self.perfilado.EdtGrupoperfiladoKeyDown;
          edtgrupo01.Color:=$005CADFE;

          Grupo02.Enabled:=True;
          lbgrupo02.Caption:='Perfilado';
          edtgrupo02.OnKeyDown:=Self.Perfilado.EdtPerfiladoKeyDown;
          edtgrupo02.Color:=$005CADFE;

          Grupo03.Enabled:=True;
          edtgrupo03.EditMask:=MascaraData;
          LbGrupo03.caption:='Data Limite';

          Showmodal;

          if (tag=0)
          then exit;

          pGrupo:='';
          if (edtgrupo01.Text<>'')
          Then Begin
                    if (self.perfilado.Grupoperfilado.LocalizaCodigo(edtgrupo01.text)=False)
                    Then Begin
                              MensagemErro('Grupo n�o encontrado');
                              exit;
                    End;
                    self.perfilado.Grupoperfilado.TabelaparaObjeto;
                    pgrupo:=self.perfilado.Grupoperfilado.get_codigo;
          End;

          pdata:='';

          if(edtgrupo03.Text<>'  /  /    ')
          then pdata:=edtgrupo03.Text;

          perfilado:='';
          if(edtgrupo02.Text<>'')
          then perfilado:=edtgrupo02.Text;

         { if (comebarra(edtgrupo03.text)<>'')
          Then if (Validadata(2,pdata,False)=False)
               then exit;   }
     End;

     With Self.ObjQueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select');
          sql.add('Tabperfilado.codigo as perfilado,');
          sql.add('Tabperfilado.REferencia as REFperfilado,');
          sql.add('Tabperfilado.descricao as NOMEperfilado,');
          sql.add('Tabcor.codigo as COR,');
          sql.add('Tabcor.Referencia as REFCOR,');
          sql.add('tabcor.descricao as NOMECOR,');
          sql.add('tabperfilado.precocusto,');
          sql.add('coalesce(sum(TabEstoque.quantidade),0) as estoque,');//alterado aqui
          sql.add('(coalesce(sum(TabEstoque.quantidade),0)*tabperfilado.precocusto) as VALORTOTAL');
          sql.add('from tabperfiladocor');
          sql.add('join tabperfilado on tabperfiladocor.perfilado=tabperfilado.codigo');
          sql.add('join tabcor on Tabperfiladocor.cor=tabcor.codigo');
          //sql.add('left join tabestoque on tabestoque.perfiladocor=TabperfiladoCor.codigo');

          if (pdata<>'')
          then Begin
                    sql.add('left join tabestoque on (tabestoque.perfiladocor=TabperfiladoCor.codigo');
                    sql.add('and tabestoque.data<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(pdata))+#39+')');

                    //ParamByName('pdata').asdatetime:=FormatDateTime('mm/dd/yyyy',strtodate(pdata));
          End
          Else sql.add('left join tabestoque on tabestoque.perfiladocor=TabperfiladoCor.codigo');


          sql.add('Where tabperfiladocor.codigo<>-500');
          
         { if (pdata<>'')then
          Begin
              sql.add('and tabestoque.data<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(pdata))+#39);
          End;      }

          if (Pgrupo<>'')
          Then Sql.add('and Tabperfilado.Grupoperfilado='+pgrupo);

          if(perfilado<>'')
          then sql.Add('and tabperfilado.codigo='+perfilado);

          sql.add('group by Tabperfilado.codigo,');
          sql.add('Tabperfilado.REferencia,');
          sql.add('Tabperfilado.descricao,');
          sql.add('Tabcor.codigo,');
          sql.add('Tabcor.Referencia,');
          sql.add('tabcor.descricao,');
          sql.add('tabperfilado.precocusto');

          Sql.add('order by Tabperfilado.referencia,tabcor.referencia');
          open;

          if (Recordcount=0)
          then Begin
                    MensagemAviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;

          With FreltxtRDPRINT do
          begin
               ConfiguraImpressao;
               RDprint.FonteTamanhoPadrao:=S17cpp;
               RDprint.TamanhoQteColunas:=130;
               RDprint.Abrir;
               if (RDprint.Setup=False)
               then begin
                         RDprint.Fechar;
                         exit;
               end;
               LinhaLocal:=3;

               RDprint.ImpC(linhalocal,65,'RELAT�RIO DE ESTOQUE  - PERFILADO',[negrito]);
               IncrementaLinha(2);

               if (Pgrupo<>'')
               then begin
                         RDprint.ImpF(linhalocal,1,'Grupo: '+Self.perfilado.Grupoperfilado.Get_Codigo+'-'+Self.perfilado.Grupoperfilado.Get_Nome,[negrito]);
                         IncrementaLinha(1);
               End;

                if (Pdata<>'')
               then Begin
                         VerificaLinha;
                         RDprint.Impf(linhalocal,1,'Data: '+pdata,[negrito]);
                         IncrementaLinha(1);
               end;

               IncrementaLinha(1);
               
               VerificaLinha;
               RDprint.Impf(linhalocal,1,CompletaPalavra('CODIGO',6,' ')+' '+
                                             CompletaPalavra('REF.',10,' ')+' '+
                                             CompletaPalavra('NOME PERFILADO',42,' ')+' '+
                                             CompletaPalavra('REF.COR',10,' ')+' '+
                                             CompletaPalavra('NOME COR',19,' ')+' '+
                                             CompletaPalavra_a_Esquerda('ESTOQUE',12,' ')+' '+
                                             CompletaPalavra_a_Esquerda('CUSTO',12,' ')+' '+
                                             CompletaPalavra_a_Esquerda('TOTAL',12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               IncrementaLinha(1);

               PsomaEstoque:=0;
               PsomaValorTotal:=0;

               While not(self.ObjQueryPesquisa.eof) do
               Begin
                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('perfilado').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('refperfilado').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('nomeperfilado').asstring,42,' ')+' '+
                                             CompletaPalavra(fieldbyname('refcor').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('nomecor').asstring,19,' ')+' '+
                                             CompletaPalavra_a_Esquerda(fieldbyname('estoque').asstring,12,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('precocusto').asstring),12,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valortotal').asstring),12,' '));
                    IncrementaLinha(1);

                    PsomaEstoque:=PsomaEstoque+Fieldbyname('estoque').asinteger;
                    PsomaValorTotal:=PsomaValorTotal+Fieldbyname('valortotal').asfloat;


                    self.ObjQueryPesquisa.next;
               End;
               DesenhaLinha;
               VerificaLinha;
               RDprint.Impf(linhalocal,93,CompletaPalavra_a_Esquerda(inttostr(pSomaEstoque),12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(formata_valor(psomavalortotal),12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               RDprint.fechar;
          End;
     End;


end;

procedure TObjperfiladoCOR.EdtperfiladoCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,Self.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then begin
                                            if (Self.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring))
                                            Then Begin
                                                      Self.TabelaparaObjeto;
                                                      LABELNOME.caption:=completapalavra(Self.perfilado.Get_Descricao,50,' ')+' COR: ' +Self.Cor.Get_Descricao;
                                            End;
                                 End;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


function TObjPerfiladoCor.RetornaEstoque:String;
begin
        With Self.ObjQueryEstoque do
        begin
             close;
             sql.clear;
             sql.add('Select coalesce(sum(quantidade),0) as estoque from tabestoque where perfiladocor='+Self.Codigo);
             //SQL.Add('Select estoque from tabperfiladocor where codigo='+self.Codigo);
             open;
             result:=fieldbyname('estoque').asstring;
        End;
end;


function TObjPERFILADOCOR.Get_Estoque:string;
begin
    Result:=Self.estoque;
end;

procedure TObjPERFILADOCOR.Submit_Estoque(parametro:string);
begin
    Self.estoque:=parametro;
end;

procedure TObjPERFILADOCOR.DiminuiEstoque(quantidade,perfiladocor:string);
var
   ObjqueryEstoque:TIBQuery;
begin
   try
          ObjqueryEstoque:=TIBQuery.Create(nil);
          ObjqueryEstoque.Database:=FDataModulo.IBDatabase;
          with ObjqueryEstoque do
          begin
                Close;
                sql.Clear;
                sql.Add('update tabperfiladocor set estoque=estoque+'+quantidade);
                SQL.Add('where codigo='+perfiladocor);
                ExecSQL;
          end;
   finally
          FreeAndNil(ObjqueryEstoque);
   end;

end;

function TObjPERFILADOCOR.AumentaEstoque(quantidade,perfiladocor:string):Boolean;
var
   ObjqueryEstoque:TIBQuery;
begin
   Result:=True;
   try
        ObjqueryEstoque:=TIBQuery.Create(nil);
        ObjqueryEstoque.Database:=FDataModulo.IBDatabase;
        with ObjqueryEstoque do
        begin
                try
                      Close;
                      sql.Clear;
                      sql.Add('update tabperfiladocor set estoque=estoque+'+quantidade);
                      SQL.Add('where codigo='+perfiladocor);
                      ExecSQL;
                except
                      Result:=False;
                end;
        end;
   finally
                FreeAndNil(ObjqueryEstoque);
                FDataModulo.IBTransaction.CommitRetaining;
   end;

end;

function TObjPERFILADOCOR.Get_EstoqueMinimo: string;
begin
  Result := self.EstoqueMinimo;
end;

procedure TObjPERFILADOCOR.Submit_EstoqueMinimo(parametro: string);
begin
  self.EstoqueMinimo := parametro;
end;

end.





