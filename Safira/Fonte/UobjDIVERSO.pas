unit UobjDIVERSO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJGRUPODIVERSO,
UOBJTABELAA_ST,UOBJTABELAB_ST,UMostraBarraProgresso,Forms ;

Type
   TObjDIVERSO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                GrupoDiverso:TOBJGRUPODIVERSO;
                SituacaoTributaria_TabelaA:TOBJTABELAA_ST ;
                SituacaoTributaria_TabelaB:TOBJTABELAB_ST ;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaReferencia(Parametro:string) :boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Referencia(parametro: string);
                Function Get_Referencia: string;
                Procedure Submit_Descricao(parametro: string);
                Function Get_Descricao: string;
                Procedure Submit_Unidade(parametro: string);
                Function Get_Unidade: string;
                Procedure Submit_ContralaPorMetroQuadrado(parametro: string);
                Function Get_ContralaPorMetroQuadrado: string;

                Procedure Submit_PRECOCUSTO(Parametro:string);
                Procedure Submit_PORCENTAGEMINSTALADO(Parametro:string);
                Procedure Submit_PORCENTAGEMFORNECIDO(Parametro:string);
                Procedure Submit_PORCENTAGEMRETIRADO(Parametro:string);


                Procedure Submit_precovendaINSTALADO(Parametro:string);
                Procedure Submit_precovendaFORNECIDO(Parametro:string);
                Procedure Submit_precovendaRETIRADO(Parametro:string);



                Function Get_PRECOCUSTO:string;
                Function Get_PORCENTAGEMINSTALADO:string;
                Function Get_PORCENTAGEMFORNECIDO:string;
                Function Get_PORCENTAGEMRETIRADO:string ;
                Function Get_PRECOVENDAINSTALADO:string ;
                Function Get_PRECOVENDAFORNECIDO:string ;
                Function Get_PRECOVENDARETIRADO:string  ;

                Procedure Submit_IsentoICMS_Estado(parametro: string);
                Function Get_IsentoICMS_Estado: string;
                Procedure Submit_SubstituicaoICMS_Estado(parametro: string);
                Function Get_SubstituicaoICMS_Estado: string;
                Procedure Submit_ValorPauta_Sub_Trib_Estado(parametro: string);
                Function Get_ValorPauta_Sub_Trib_Estado: string;
                Procedure Submit_Aliquota_ICMS_Estado(parametro: string);
                Function Get_Aliquota_ICMS_Estado: string;
                Procedure Submit_Reducao_BC_ICMS_Estado(parametro: string);
                Function Get_Reducao_BC_ICMS_Estado: string;
                Procedure Submit_Aliquota_ICMS_Cupom_Estado(parametro: string);
                Function Get_Aliquota_ICMS_Cupom_Estado: string;
                Procedure Submit_IsentoICMS_ForaEstado(parametro: string);
                Function Get_IsentoICMS_ForaEstado: string;
                Procedure Submit_SubstituicaoICMS_ForaEstado(parametro: string);
                Function Get_SubstituicaoICMS_ForaEstado: string;
                Procedure Submit_ValorPauta_Sub_Trib_ForaEstado(parametro: string);
                Function Get_ValorPauta_Sub_Trib_ForaEstado: string;
                Procedure Submit_Aliquota_ICMS_ForaEstado(parametro: string);
                Function Get_Aliquota_ICMS_ForaEstado: string;
                Procedure Submit_Reducao_BC_ICMS_ForaEstado(parametro: string);
                Function Get_Reducao_BC_ICMS_ForaEstado: string;
                Procedure Submit_Aliquota_ICMS_Cupom_ForaEstado(parametro: string);
                Function Get_Aliquota_ICMS_Cupom_ForaEstado: string;

                Procedure Submit_percentualagregado(parametro: string);
                Function Get_percentualagregado: string;

                Procedure Submit_ipi(parametro: string);
                Function Get_ipi: string;

                Procedure Submit_ClassificacaoFiscal(parametro: string);
                Function Get_ClassificacaoFiscal: string;

                procedure EdtSituacaoTributaria_TabelaAExit(Sender: TObject);
                procedure EdtSituacaoTributaria_TabelaAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure EdtSituacaoTributaria_TabelaBExit(Sender: TObject);
                procedure EdtSituacaoTributaria_TabelaBKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);



                procedure EdtGrupoDiversoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtGrupoDiversoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtGrupoDiversoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;

                procedure EdtDiversoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtDiversoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;


                function PrimeiroRegistro: Boolean;
                function ProximoRegisto(PCodigo: Integer): Boolean;
                function RegistoAnterior(PCodigo: Integer): Boolean;
                function UltimoRegistro: Boolean;
                procedure AtualizaPrecos(PGrupoDiverso: string);
                Procedure Reajusta(PIndice,Pgrupo:string);

                procedure submit_NCM(parametro:string);
                function Get_NCM:string;

                procedure Submit_Ativo(parametro:string);
                function Get_Ativo:string;

                procedure Submit_cest(parametro:string);
                function Get_cest:string;

                procedure Submit_Fornecedor(parametro:string);
                function Get_Fornecedor:string;
                procedure EdtFornecedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure AtualizaMargens(PGrupoDiverso: string);
                function AtualizaTributospeloNCM:Boolean;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Referencia:string;
               Descricao:string;
               Unidade:string;
               ContralaPorMetroQuadrado:string;
               PRECOCUSTO:string;
               PORCENTAGEMINSTALADO:string;
               PORCENTAGEMFORNECIDO:string;
               PORCENTAGEMRETIRADO:string;

               PRECOVENDAINSTALADO:string;
               PRECOVENDAFORNECIDO:string;
               PRECOVENDARETIRADO:string;

               ParametroPesquisa:TStringList;


               IsentoICMS_Estado:string;
               SubstituicaoICMS_Estado:string;
               ValorPauta_Sub_Trib_Estado:string;
               Aliquota_ICMS_Estado:string;
               Reducao_BC_ICMS_Estado:string;
               Aliquota_ICMS_Cupom_Estado:string;
               IsentoICMS_ForaEstado:string;
               SubstituicaoICMS_ForaEstado:string;
               ValorPauta_Sub_Trib_ForaEstado:string;
               Aliquota_ICMS_ForaEstado:string;
               Reducao_BC_ICMS_ForaEstado:string;
               Aliquota_ICMS_Cupom_ForaEstado:string;
               percentualagregado:string;
               ipi:string;
               ClassificacaoFiscal:string;
               NCM:string;
               ATIVO:string;
               Fornecedor:string;
               cest:string;

               Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UFORNECEDOR;


{ TTabTitulo }


Function  TObjDIVERSO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Referencia:=fieldbyname('Referencia').asstring;
        Self.Descricao:=fieldbyname('Descricao').asstring;
        If(FieldByName('GrupoDiverso').asstring<>'')
        Then Begin
                 If (Self.GrupoDiverso.LocalizaCodigo(FieldByName('GrupoDiverso').asstring)=False)
                 Then Begin
                          Messagedlg('Grupo Diverso N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.GrupoDiverso.TabelaparaObjeto;
        End;
        Self.Unidade:=fieldbyname('Unidade').asstring;
        Self.ContralaPorMetroQuadrado:=fieldbyname('ContralaPorMetroQuadrado').asstring;
        Self.PRECOCUSTO:=fieldbyname('PRECOCUSTO')          .asString;
        Self.PORCENTAGEMINSTALADO:=fieldbyname('PORCENTAGEMINSTALADO').asString;
        Self.PORCENTAGEMFORNECIDO:=fieldbyname('PORCENTAGEMFORNECIDO').asString;
        Self.PORCENTAGEMRETIRADO:=fieldbyname('PORCENTAGEMRETIRADO' ).asString;
        Self.PRECOVENDAINSTALADO:=fieldbyname('PRECOVENDAINSTALADO' ).asString;
        Self.PRECOVENDAFORNECIDO:=fieldbyname('PRECOVENDAFORNECIDO' ).asString;
        Self.PRECOVENDARETIRADO:=fieldbyname('PRECOVENDARETIRADO'  ).asString;

        Self.IsentoICMS_Estado:=fieldbyname('IsentoICMS_Estado').asstring;
        Self.SubstituicaoICMS_Estado:=fieldbyname('SubstituicaoICMS_Estado').asstring;
        Self.ValorPauta_Sub_Trib_Estado:=fieldbyname('ValorPauta_Sub_Trib_Estado').asstring;
        Self.Aliquota_ICMS_Estado:=fieldbyname('Aliquota_ICMS_Estado').asstring;
        Self.Reducao_BC_ICMS_Estado:=fieldbyname('Reducao_BC_ICMS_Estado').asstring;
        Self.Aliquota_ICMS_Cupom_Estado:=fieldbyname('Aliquota_ICMS_Cupom_Estado').asstring;
        Self.IsentoICMS_ForaEstado:=fieldbyname('IsentoICMS_ForaEstado').asstring;
        Self.SubstituicaoICMS_ForaEstado:=fieldbyname('SubstituicaoICMS_ForaEstado').asstring;
        Self.ValorPauta_Sub_Trib_ForaEstado:=fieldbyname('ValorPauta_Sub_Trib_ForaEstado').asstring;
        Self.Aliquota_ICMS_ForaEstado:=fieldbyname('Aliquota_ICMS_ForaEstado').asstring;
        Self.Reducao_BC_ICMS_ForaEstado:=fieldbyname('Reducao_BC_ICMS_ForaEstado').asstring;
        Self.Aliquota_ICMS_Cupom_ForaEstado:=fieldbyname('Aliquota_ICMS_Cupom_ForaEstado').asstring;
        Self.percentualagregado:=fieldbyname('percentualagregado').asstring;
        Self.ipi:=fieldbyname('ipi').asstring;
        self.Fornecedor:=fieldbyname('fornecedor').AsString;

        If(FieldByName('SituacaoTributaria_TabelaA').asstring<>'')
        Then Begin
                 If (Self.SituacaoTributaria_TabelaA.LocalizaCodigo(FieldByName('SituacaoTributaria_TabelaA').asstring)=False)
                 Then Begin
                          Messagedlg('SituacaoTributaria_TabelaA N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.SituacaoTributaria_TabelaA.TabelaparaObjeto;
        End;
        If(FieldByName('SituacaoTributaria_TabelaB').asstring<>'')
        Then Begin
                 If (Self.SituacaoTributaria_TabelaB.LocalizaCodigo(FieldByName('SituacaoTributaria_TabelaB').asstring)=False)
                 Then Begin
                          Messagedlg('SituacaoTributaria_TabelaB N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.SituacaoTributaria_TabelaB.TabelaparaObjeto;
        End;
        Self.ClassificacaoFiscal:=fieldbyname('ClassificacaoFiscal').asstring;
        SELF.NCM:=fieldbyname('ncm').AsString;
        self.ATIVO:=fieldbyname('ativo').AsString;
        self.cest:=fieldbyname('cest').AsString;

        result:=True;
     End;
end;


Procedure TObjDIVERSO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Referencia').asstring:=Self.Referencia;
        ParamByName('Descricao').asstring:=Self.Descricao;
        ParamByName('GrupoDiverso').asstring:=Self.GrupoDiverso.GET_CODIGO;
        ParamByName('Unidade').asstring:=Self.Unidade;
        ParamByName('ContralaPorMetroQuadrado').asstring:=Self.ContralaPorMetroQuadrado;
        ParamByName('PRECOCUSTO').asString:=virgulaparaponto(Self.PRECOCUSTO);

        ParamByName('PORCENTAGEMINSTALADO').asString:=virgulaparaponto(Self.PORCENTAGEMINSTALADO);
        ParamByName('PORCENTAGEMFORNECIDO').asString:=virgulaparaponto(Self.PORCENTAGEMFORNECIDO);
        ParamByName('PORCENTAGEMRETIRADO').asString:=virgulaparaponto(Self.PORCENTAGEMRETIRADO);

        ParamByName('precovendaINSTALADO').asString:=virgulaparaponto(Self.precovendaINSTALADO);
        ParamByName('precovendaFORNECIDO').asString:=virgulaparaponto(Self.precovendaFORNECIDO);
        ParamByName('precovendaRETIRADO').asString:=virgulaparaponto(Self.precovendaRETIRADO);

        ParamByName('IsentoICMS_Estado').asstring:=Self.IsentoICMS_Estado;
        ParamByName('SubstituicaoICMS_Estado').asstring:=Self.SubstituicaoICMS_Estado;
        ParamByName('ValorPauta_Sub_Trib_Estado').asstring:=virgulaparaponto(Self.ValorPauta_Sub_Trib_Estado);
        ParamByName('Aliquota_ICMS_Estado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_Estado);
        ParamByName('Reducao_BC_ICMS_Estado').asstring:=virgulaparaponto(Self.Reducao_BC_ICMS_Estado);
        ParamByName('Aliquota_ICMS_Cupom_Estado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_Cupom_Estado);
        ParamByName('IsentoICMS_ForaEstado').asstring:=Self.IsentoICMS_ForaEstado;
        ParamByName('SubstituicaoICMS_ForaEstado').asstring:=Self.SubstituicaoICMS_ForaEstado;
        ParamByName('ValorPauta_Sub_Trib_ForaEstado').asstring:=virgulaparaponto(Self.ValorPauta_Sub_Trib_ForaEstado);
        ParamByName('Aliquota_ICMS_ForaEstado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_ForaEstado);
        ParamByName('Reducao_BC_ICMS_ForaEstado').asstring:=virgulaparaponto(Self.Reducao_BC_ICMS_ForaEstado);
        ParamByName('Aliquota_ICMS_Cupom_ForaEstado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_Cupom_ForaEstado);
        ParamByName('percentualagregado').asstring:=virgulaparaponto(Self.percentualagregado);
        ParamByName('ipi').asstring:=virgulaparaponto(Self.ipi);
        ParamByName('SituacaoTributaria_TabelaA').asstring:=Self.SituacaoTributaria_TabelaA.GET_CODIGO;
        ParamByName('SituacaoTributaria_TabelaB').asstring:=Self.SituacaoTributaria_TabelaB.GET_CODIGO;
        ParamByName('ClassificacaoFiscal').asstring:=Self.ClassificacaoFiscal;
        ParamByName('ncm').AsString:=self.NCM;
        ParamByName('Ativo').AsString:=self.ATIVO;
        ParamByName('cest').AsString:=self.cest;
        ParamByName('fornecedor').AsString:=Self.Fornecedor;
       
  End;
End;

//***********************************************************************

function TObjDIVERSO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjDIVERSO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Referencia:='';
        Descricao:='';
        GrupoDiverso.ZerarTabela;
        Unidade:='';
        ContralaPorMetroQuadrado:='';
        PRECOCUSTO            :='';
        PORCENTAGEMINSTALADO  :='';
        PORCENTAGEMFORNECIDO  :='';
        PORCENTAGEMRETIRADO   :='';
        PRECOVENDAINSTALADO   :='';
        PRECOVENDAFORNECIDO   :='';
        PRECOVENDARETIRADO    :='';

        IsentoICMS_Estado:='';
        SubstituicaoICMS_Estado:='';
        ValorPauta_Sub_Trib_Estado:='';
        Aliquota_ICMS_Estado:='';
        Reducao_BC_ICMS_Estado:='';
        Aliquota_ICMS_Cupom_Estado:='';
        IsentoICMS_ForaEstado:='';
        SubstituicaoICMS_ForaEstado:='';
        ValorPauta_Sub_Trib_ForaEstado:='';
        Aliquota_ICMS_ForaEstado:='';
        Reducao_BC_ICMS_ForaEstado:='';
        Aliquota_ICMS_Cupom_ForaEstado:='';
        percentualagregado:='';
        ipi:='';
        SituacaoTributaria_TabelaA.ZerarTabela;
        SituacaoTributaria_TabelaB.ZerarTabela;
        ClassificacaoFiscal:='';
        NCM:='';
        ATIVO:='';
        cest:='';
        Fornecedor:='';
     End;
end;

Function TObjDIVERSO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/C�digo';

       if (percentualagregado='')
      Then percentualagregado:='0';

      if (IPI='')
      Then Ipi:='0';

      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjDIVERSO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.GrupoDiverso.LocalizaCodigo(Self.GrupoDiverso.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Grupo Diverso n�o Encontrado!';

      If (Self.SituacaoTributaria_TabelaA.Get_CODIGO<>'')then
      If (Self.SituacaoTributaria_TabelaA.LocalizaCodigo(Self.SituacaoTributaria_TabelaA.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ SituacaoTributaria_TabelaA n�o Encontrado!';

      if (Self.SituacaoTributaria_TabelaB.Get_CODIGO<>'')then
      If (Self.SituacaoTributaria_TabelaB.LocalizaCodigo(Self.SituacaoTributaria_TabelaB.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ SituacaoTributaria_TabelaB n�o Encontrado!';

//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjDIVERSO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        If (Self.GrupoDiverso.Get_Codigo<>'')
        Then Strtoint(Self.GrupoDiverso.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Grupo Diverso';
     End;

     If(Self.ContralaPorMetroQuadrado = '')then
     Begin
         Mensagem:=mensagem+'/Controla por Metro Quadrado';
     end;


     try
        Strtofloat(Self.ValorPauta_Sub_Trib_Estado);
     Except
           Mensagem:=mensagem+'/ValorPauta_Sub_Trib_Estado';
     End;
     try
        Strtofloat(Self.Aliquota_ICMS_Estado);
     Except
           Mensagem:=mensagem+'/Aliquota_ICMS_Estado';
     End;
     try
        Strtofloat(Self.Reducao_BC_ICMS_Estado);
     Except
           Mensagem:=mensagem+'/Reducao_BC_ICMS_Estado';
     End;
     try
        Strtofloat(Self.Aliquota_ICMS_Cupom_Estado);
     Except
           Mensagem:=mensagem+'/Aliquota_ICMS_Cupom_Estado';
     End;
     try
        Strtofloat(Self.ValorPauta_Sub_Trib_ForaEstado);
     Except
           Mensagem:=mensagem+'/ValorPauta_Sub_Trib_ForaEstado';
     End;
     try
        Strtofloat(Self.Aliquota_ICMS_ForaEstado);
     Except
           Mensagem:=mensagem+'/Aliquota_ICMS_ForaEstado';
     End;
     try
        Strtofloat(Self.Reducao_BC_ICMS_ForaEstado);
     Except
           Mensagem:=mensagem+'/Reducao_BC_ICMS_ForaEstado';
     End;
     try
        Strtofloat(Self.Aliquota_ICMS_Cupom_ForaEstado);
     Except
           Mensagem:=mensagem+'/Aliquota_ICMS_Cupom_ForaEstado';
     End;

     try
        Strtofloat(Self.percentualagregado);
     Except
           Mensagem:=mensagem+'/percentual agregado';
     End;

     try
        Strtofloat(Self.ipi);
     Except
           Mensagem:=mensagem+'/Ipi';
     End;

     try
        If (Self.SituacaoTributaria_TabelaA.Get_Codigo<>'')
        Then Strtoint(Self.SituacaoTributaria_TabelaA.Get_Codigo);
     Except
           Mensagem:=mensagem+'/SituacaoTributaria_TabelaA';
     End;
     try
        If (Self.SituacaoTributaria_TabelaB.Get_Codigo<>'')
        Then Strtoint(Self.SituacaoTributaria_TabelaB.Get_Codigo);
     Except
           Mensagem:=mensagem+'/SituacaoTributaria_TabelaB';
     End;



      try
        Strtofloat(Self.PorcentagemInstalado);
     Except
           Mensagem:=mensagem+'/Porcentagem Instalado';
     End;
     try
        Strtofloat(Self.PorcentagemFornecido);
     Except
           Mensagem:=mensagem+'/Porcentagem Fornecido';
     End;
     try
        Strtofloat(Self.PorcentagemRetirado);
     Except
           Mensagem:=mensagem+'/Porcentagem Retirado';
     End;



     try
        Strtofloat(Self.precovendaInstalado);
     Except
           Mensagem:=mensagem+'/Pre�o de Venda Instalado';
     End;
     try
        Strtofloat(Self.precovendaFornecido);
     Except
           Mensagem:=mensagem+'/Pre�o de Venda Fornecido';
     End;
     try
        Strtofloat(Self.precovendaRetirado);
     Except
           Mensagem:=mensagem+'/Pre�o de Venda Retirado';
     End;


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjDIVERSO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjDIVERSO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjDIVERSO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro DIVERSO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Referencia,Descricao,GrupoDiverso,Estoque,Unidade');
           SQL.ADD(' ,ContralaPorMetroQuadrado,PRECOCUSTO,PORCENTAGEMINSTALADO');
           SQL.ADD(' ,PORCENTAGEMFORNECIDO, PORCENTAGEMRETIRADO, PRECOVENDAINSTALADO');
           SQL.ADD(' ,PRECOVENDAFORNECIDO,  PRECOVENDARETIRADO,IsentoICMS_Estado');
           SQL.ADD(' ,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado,Aliquota_ICMS_Estado');
           SQL.ADD(' ,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado,IsentoICMS_ForaEstado');
           SQL.ADD(' ,SubstituicaoICMS_ForaEstado,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
           SQL.ADD(' ,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado,SituacaoTributaria_TabelaA,SituacaoTributaria_TabelaB,ClassificacaoFiscal,percentualagregado,ipi');
           SQL.ADD(' ,NCM,ativo,fornecedor,cest from  TabDiverso');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


function TObjDIVERSO.LocalizaReferencia(Parametro: string): boolean;
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro DIVERSO vazio',mterror,[mbok],0);
                 exit;
       End;

       Parametro := StrReplaceRef(Parametro);  {rodolfo}

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Referencia,Descricao,GrupoDiverso,Estoque,Unidade');
           SQL.ADD(' ,ContralaPorMetroQuadrado,PRECOCUSTO,PORCENTAGEMINSTALADO');
           SQL.ADD(' ,PORCENTAGEMFORNECIDO, PORCENTAGEMRETIRADO, PRECOVENDAINSTALADO');
           SQL.ADD(' ,PRECOVENDAFORNECIDO,  PRECOVENDARETIRADO,IsentoICMS_Estado');
           SQL.ADD(' ,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado,Aliquota_ICMS_Estado');
           SQL.ADD(' ,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado,IsentoICMS_ForaEstado');
           SQL.ADD(' ,SubstituicaoICMS_ForaEstado,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
           SQL.ADD(' ,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado,SituacaoTributaria_TabelaA');
           SQL.ADD(' ,SituacaoTributaria_TabelaB,ClassificacaoFiscal,percentualagregado,ipi');
           SQL.ADD(' ,NCM,ativo,fornecedor,cest from  TabDiverso');
           SQL.ADD(' WHERE Referencia ='+#39+parametro+#39);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;


procedure TObjDIVERSO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjDIVERSO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjDIVERSO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.InsertSql:=TStringList.create;
        Self.DeleteSql:=TStringList.create;
        Self.ModifySQl:=TStringList.create;
        Self.GrupoDiverso:=TOBJGRUPODIVERSO.create;
        Self.SituacaoTributaria_TabelaA:=TOBJTABELAA_ST.Create ;
        Self.SituacaoTributaria_TabelaB:=TOBJTABELAB_ST.Create ;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabDiverso(Codigo,Referencia,Descricao,GrupoDiverso');
                InsertSQL.add(' ,Estoque,Unidade,ContralaPorMetroQuadrado');
                InsertSQL.add(' ,PRECOCUSTO,PORCENTAGEMINSTALADO,PORCENTAGEMFORNECIDO,PORCENTAGEMRETIRADO');
                InsertSQL.add(' ,IsentoICMS_Estado');
                InsertSQL.add(' ,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado');
                InsertSQL.add(' ,Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado');
                InsertSQL.add(' ,IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado');
                InsertSQL.add(' ,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
                InsertSQL.add(' ,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado');
                InsertSQL.add(' ,SituacaoTributaria_TabelaA,SituacaoTributaria_TabelaB');
                InsertSQL.add(' ,ClassificacaoFiscal,PRECOVENDAINSTALADO,PRECOVENDAFORNECIDO,PRECOVENDARETIRADO,percentualagregado,ipi,NCM,ativo,fornecedor,cest)');

                InsertSQL.add('values (:Codigo,:Referencia,:Descricao,:GrupoDiverso');
                InsertSQL.add(' ,:Estoque,:Unidade,:ContralaPorMetroQuadrado');
                InsertSQL.add(' ,:PRECOCUSTO,:PORCENTAGEMINSTALADO,:PORCENTAGEMFORNECIDO,:PORCENTAGEMRETIRADO');
                InsertSQL.add(' ,:IsentoICMS_Estado');
                InsertSQL.add(' ,:SubstituicaoICMS_Estado,:ValorPauta_Sub_Trib_Estado');
                InsertSQL.add(' ,:Aliquota_ICMS_Estado,:Reducao_BC_ICMS_Estado,:Aliquota_ICMS_Cupom_Estado');
                InsertSQL.add(' ,:IsentoICMS_ForaEstado,:SubstituicaoICMS_ForaEstado');
                InsertSQL.add(' ,:ValorPauta_Sub_Trib_ForaEstado,:Aliquota_ICMS_ForaEstado');
                InsertSQL.add(' ,:Reducao_BC_ICMS_ForaEstado,:Aliquota_ICMS_Cupom_ForaEstado');
                InsertSQL.add(' ,:SituacaoTributaria_TabelaA,:SituacaoTributaria_TabelaB');
                InsertSQL.add(' ,:ClassificacaoFiscal,:PRECOVENDAINSTALADO,:PRECOVENDAFORNECIDO,:PRECOVENDARETIRADO,:percentualagregado,:ipi,:NCM,:ativo,:fornecedor,:cest)');




//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabDiverso set Codigo=:Codigo,Referencia=:Referencia');
                ModifySQL.add(',Descricao=:Descricao,GrupoDiverso=:GrupoDiverso,Estoque=:Estoque');
                ModifySQL.add(',Unidade=:Unidade,ContralaPorMetroQuadrado=:ContralaPorMetroQuadrado');
                ModifySQl.add(',PRECOCUSTO=:PRECOCUSTO,PORCENTAGEMINSTALADO=:PORCENTAGEMINSTALADO');
                ModifySQl.add(',PORCENTAGEMFORNECIDO=:PORCENTAGEMFORNECIDO,PORCENTAGEMRETIRADO=:PORCENTAGEMRETIRADO');
                ModifySQL.add(',IsentoICMS_Estado=:IsentoICMS_Estado,SubstituicaoICMS_Estado=:SubstituicaoICMS_Estado');
                ModifySQL.add(',ValorPauta_Sub_Trib_Estado=:ValorPauta_Sub_Trib_Estado');
                ModifySQL.add(',Aliquota_ICMS_Estado=:Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado=:Reducao_BC_ICMS_Estado');
                ModifySQL.add(',Aliquota_ICMS_Cupom_Estado=:Aliquota_ICMS_Cupom_Estado');
                ModifySQL.add(',IsentoICMS_ForaEstado=:IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado=:SubstituicaoICMS_ForaEstado');
                ModifySQL.add(',ValorPauta_Sub_Trib_ForaEstado=:ValorPauta_Sub_Trib_ForaEstado');
                ModifySQL.add(',Aliquota_ICMS_ForaEstado=:Aliquota_ICMS_ForaEstado');
                ModifySQL.add(',Reducao_BC_ICMS_ForaEstado=:Reducao_BC_ICMS_ForaEstado');
                ModifySQL.add(',Aliquota_ICMS_Cupom_ForaEstado=:Aliquota_ICMS_Cupom_ForaEstado');
                ModifySQL.add(',SituacaoTributaria_TabelaA=:SituacaoTributaria_TabelaA');
                ModifySQL.add(',SituacaoTributaria_TabelaB=:SituacaoTributaria_TabelaB');
                ModifySQL.add(',ClassificacaoFiscal=:ClassificacaoFiscal');
                ModifySQL.add(',PRECOVENDAINSTALADO=:PRECOVENDAINSTALADO,PRECOVENDAFORNECIDO=:PRECOVENDAFORNECIDO,PRECOVENDARETIRADO=:PRECOVENDARETIRADO,percentualagregado=:percentualagregado,ipi=:ipi,NCM=:NCM');
                ModifySQl.Add(',ativo=:ativo,fornecedor=:fornecedor,cest=:cest where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabDiverso where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjDIVERSO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjDIVERSO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabDIVERSO');
     Result:=Self.ParametroPesquisa;
end;

function TObjDIVERSO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de DIVERSO ';
end;


function TObjDIVERSO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENDIVERSO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjDIVERSO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.GrupoDiverso.FREE;
    Self.SituacaoTributaria_TabelaA.Free;
    Self.SituacaoTributaria_TabelaB.Free;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjDIVERSO.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjDIVERSO.RetornaCampoNome: string;
begin
      result:='DESCRICAO';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjDiverso.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjDiverso.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjDiverso.Submit_Referencia(parametro: string);
begin
        Self.Referencia:=Parametro;
end;
function TObjDiverso.Get_Referencia: string;
begin
        Result:=Self.Referencia;
end;
procedure TObjDiverso.Submit_Descricao(parametro: string);
begin
        Self.Descricao:=Parametro;
end;
function TObjDiverso.Get_Descricao: string;
begin
        Result:=Self.Descricao;
end;
procedure TObjDiverso.Submit_Unidade(parametro: string);
begin
        Self.Unidade:=Parametro;
end;
function TObjDiverso.Get_Unidade: string;
begin
        Result:=Self.Unidade;
end;
procedure TObjDiverso.Submit_ContralaPorMetroQuadrado(parametro: string);
begin
        Self.ContralaPorMetroQuadrado:=Parametro;
end;
function TObjDiverso.Get_ContralaPorMetroQuadrado: string;
begin
        Result:=Self.ContralaPorMetroQuadrado;
end;
//CODIFICA GETSESUBMITS


procedure TObjDIVERSO.EdtGrupoDiversoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.GrupoDiverso.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.GrupoDiverso.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.GrupoDiverso.GET_NOME;
End;

procedure TObjDIVERSO.EdtGrupoDiversoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.GrupoDiverso.Get_Pesquisa,Self.GrupoDiverso.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoDiverso.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.GrupoDiverso.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoDiverso.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjDIVERSO.EdtDiversoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabdiverso','',Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.GrupoDiverso.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoDiverso.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjDIVERSO.EdtGrupoDiversoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.GrupoDiverso.Get_Pesquisa,Self.GrupoDiverso.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoDiverso.RETORNACAMPOCODIGO).asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjDIVERSO.EdtDiversoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabdiverso','',Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN

procedure TObjDIVERSO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJDIVERSO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;

function TObjDIVERSO.Get_PORCENTAGEMFORNECIDO: string;
begin
    Result:=Self.PORCENTAGEMFORNECIDO;
end;

function TObjDIVERSO.Get_PORCENTAGEMINSTALADO: string;
begin
    Result:=Self.PORCENTAGEMINSTALADO;
end;

function TObjDIVERSO.Get_PORCENTAGEMRETIRADO: string;
begin
    Result:=Self.PORCENTAGEMRETIRADO;
end;

function TObjDIVERSO.Get_PRECOCUSTO: string;
begin
    REsult:=Self.PRECOCUSTO;
end;

function TObjDIVERSO.Get_PRECOVENDAFORNECIDO: string;
begin
    Result:=Self.PRECOVENDAFORNECIDO;
end;

function TObjDIVERSO.Get_PRECOVENDAINSTALADO: string;
begin
    Result:=Self.PRECOVENDAINSTALADO;
end;

function TObjDIVERSO.Get_PRECOVENDARETIRADO: string;
begin
    Result:=Self.PRECOVENDARETIRADO;
end;

procedure TObjDIVERSO.Submit_PORCENTAGEMFORNECIDO(Parametro: string);
begin
    Self.PORCENTAGEMFORNECIDO:=Parametro;
end;

procedure TObjDIVERSO.Submit_PORCENTAGEMINSTALADO(Parametro: string);
begin
    Self.PORCENTAGEMINSTALADO:=Parametro;
end;

procedure TObjDIVERSO.Submit_PORCENTAGEMRETIRADO(Parametro: string);
begin
    Self.PORCENTAGEMRETIRADO:=Parametro;
end;

procedure TObjDIVERSO.Submit_PRECOCUSTO(Parametro: string);
begin
   Self.PRECOCUSTO:=Parametro;
end;

procedure TobjDiverso.Submit_IsentoICMS_Estado(parametro: string);
begin
        Self.IsentoICMS_Estado:=Parametro;
end;
function TobjDiverso.Get_IsentoICMS_Estado: string;
begin
        Result:=Self.IsentoICMS_Estado;
end;
procedure TobjDiverso.Submit_SubstituicaoICMS_Estado(parametro: string);
begin
        Self.SubstituicaoICMS_Estado:=Parametro;
end;
function TobjDiverso.Get_SubstituicaoICMS_Estado: string;
begin
        Result:=Self.SubstituicaoICMS_Estado;
end;
procedure TobjDiverso.Submit_ValorPauta_Sub_Trib_Estado(parametro: string);
begin
        Self.ValorPauta_Sub_Trib_Estado:=Parametro;
end;
function TobjDiverso.Get_ValorPauta_Sub_Trib_Estado: string;
begin
        Result:=Self.ValorPauta_Sub_Trib_Estado;
end;
procedure TobjDiverso.Submit_Aliquota_ICMS_Estado(parametro: string);
begin
        Self.Aliquota_ICMS_Estado:=Parametro;
end;
function TobjDiverso.Get_Aliquota_ICMS_Estado: string;
begin
        Result:=Self.Aliquota_ICMS_Estado;
end;
procedure TobjDiverso.Submit_Reducao_BC_ICMS_Estado(parametro: string);
begin
        Self.Reducao_BC_ICMS_Estado:=Parametro;
end;
function TobjDiverso.Get_Reducao_BC_ICMS_Estado: string;
begin
        Result:=Self.Reducao_BC_ICMS_Estado;
end;
procedure TobjDiverso.Submit_Aliquota_ICMS_Cupom_Estado(parametro: string);
begin
        Self.Aliquota_ICMS_Cupom_Estado:=Parametro;
end;
function TobjDiverso.Get_Aliquota_ICMS_Cupom_Estado: string;
begin
        Result:=Self.Aliquota_ICMS_Cupom_Estado;
end;
procedure TobjDiverso.Submit_IsentoICMS_ForaEstado(parametro: string);
begin
        Self.IsentoICMS_ForaEstado:=Parametro;
end;
function TobjDiverso.Get_IsentoICMS_ForaEstado: string;
begin
        Result:=Self.IsentoICMS_ForaEstado;
end;
procedure TobjDiverso.Submit_SubstituicaoICMS_ForaEstado(parametro: string);
begin
        Self.SubstituicaoICMS_ForaEstado:=Parametro;
end;
function TobjDiverso.Get_SubstituicaoICMS_ForaEstado: string;
begin
        Result:=Self.SubstituicaoICMS_ForaEstado;
end;
procedure TobjDiverso.Submit_ValorPauta_Sub_Trib_ForaEstado(parametro: string);
begin
        Self.ValorPauta_Sub_Trib_ForaEstado:=Parametro;
end;
function TobjDiverso.Get_ValorPauta_Sub_Trib_ForaEstado: string;
begin
        Result:=Self.ValorPauta_Sub_Trib_ForaEstado;
end;
procedure TobjDiverso.Submit_Aliquota_ICMS_ForaEstado(parametro: string);
begin
        Self.Aliquota_ICMS_ForaEstado:=Parametro;
end;
function TobjDiverso.Get_Aliquota_ICMS_ForaEstado: string;
begin
        Result:=Self.Aliquota_ICMS_ForaEstado;
end;
procedure TobjDiverso.Submit_Reducao_BC_ICMS_ForaEstado(parametro: string);
begin
        Self.Reducao_BC_ICMS_ForaEstado:=Parametro;
end;
function TobjDiverso.Get_Reducao_BC_ICMS_ForaEstado: string;
begin
        Result:=Self.Reducao_BC_ICMS_ForaEstado;
end;
procedure TobjDiverso.Submit_Aliquota_ICMS_Cupom_ForaEstado(parametro: string);
begin
        Self.Aliquota_ICMS_Cupom_ForaEstado:=Parametro;
end;
function TobjDiverso.Get_Aliquota_ICMS_Cupom_ForaEstado: string;
begin
        Result:=Self.Aliquota_ICMS_Cupom_ForaEstado;
end;

procedure TobjDiverso.Submit_percentualagregado(parametro: string);
begin
        Self.percentualagregado:=Parametro;
end;
function TobjDiverso.Get_percentualagregado: string;
begin
        Result:=Self.percentualagregado;
end;

procedure TobjDiverso.Submit_ipi(parametro: string);
begin
        Self.ipi:=Parametro;
end;
function TobjDiverso.Get_ipi: string;
begin
        Result:=Self.ipi;
end;

procedure TobjDiverso.Submit_ClassificacaoFiscal(parametro: string);
begin
        Self.ClassificacaoFiscal:=Parametro;
end;
function TobjDiverso.Get_ClassificacaoFiscal: string;
begin
        Result:=Self.ClassificacaoFiscal;
end;


procedure TobjDiverso.EdtSituacaoTributaria_TabelaAExit(Sender: TObject);
Begin
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.SituacaoTributaria_TabelaA.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.SituacaoTributaria_TabelaA.tabelaparaobjeto;
End;
procedure TobjDiverso.EdtSituacaoTributaria_TabelaAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa
            (Self.SituacaoTributaria_TabelaA.Get_Pesquisa,Self.SituacaoTributaria_TabelaA.Get_TituloPesquisa,
            nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SituacaoTributaria_TabelaA.RETORNACAMPOCODIGO).asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TobjDiverso.EdtSituacaoTributaria_TabelaBExit(Sender: TObject);
Begin
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.SituacaoTributaria_TabelaB.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.SituacaoTributaria_TabelaB.tabelaparaobjeto;
End;
procedure TobjDiverso.EdtSituacaoTributaria_TabelaBKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa
               (Self.SituacaoTributaria_TabelaB.Get_Pesquisa,Self.SituacaoTributaria_TabelaB.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SituacaoTributaria_TabelaB.RETORNACAMPOCODIGO).asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;



function TObjDiverso.PrimeiroRegistro: Boolean;
Var MenorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabDiverso') ;
           Open;

           if (FieldByName('MenorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MenorCodigo:=fieldbyname('MenorCodigo').AsString;
           Self.LocalizaCodigo(MenorCodigo);

           Result:=true;
     end;
end;

function TObjDiverso.UltimoRegistro: Boolean;
Var MaiorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabDiverso') ;
           Open;

           if (FieldByName('MaiorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MaiorCodigo:=fieldbyname('MaiorCodigo').AsString;
           Self.LocalizaCodigo(MaiorCodigo);

           Result:=true;
     end;
end;

function TObjDiverso.ProximoRegisto(PCodigo:Integer): Boolean;
Var MaiorValor:Integer;
begin
     Result:=false;

     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabDiverso') ;
           Open;

           MaiorValor:=fieldbyname('MaiorCodigo').AsInteger;
     end;

     if (MaiorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Inc(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MaiorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Inc(PCodigo,1);
     end;

     Result:=true;
end;

function TObjDiverso.RegistoAnterior(PCodigo: Integer): Boolean;
Var MenorValor:Integer;
begin
     Result:=false;
     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabDiverso') ;
           Open;

           MenorValor:=fieldbyname('MenorCodigo').AsInteger;
     end;

     if (MenorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Dec(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MenorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Dec(PCodigo,1);
     end;

     Result:=true;
end;

procedure TObjDiverso.AtualizaPrecos(PGrupoDiverso: string);
Var QueryLOcal :TIBQuery; PVAlorCusto, PPorcentagemReajuste:Currency;
begin

      if (PGrupoDiverso = '')then
      Begin
           MensagemErro('Pesquise um grupo de Diverso entes Atualizar os pre�os.');
           exit;
      end;

      if MessageDlg('Tem certeza que deseja alterar os valores dos PRE�OS DE CUSTO de '+#13+
                    'todas os Diversos pertencentes a este grupo?. Lembrando que esse processo � irrevers�vel.', mtConfirmation, [mbYes, mbNo], 0) = mrNo
      then Begin
           exit;
      end;

      With FfiltroImp do
      Begin
           DesativaGrupos;
           Grupo01.Enabled:=true;
           LbGrupo01.Caption:='Acr�scimo em (%)';
           edtgrupo01.Enabled:=True;

           ShowModal;

           if (Tag = 0)then
           Exit;

           try
                PPorcentagemReajuste:=StrToCurr(edtgrupo01.Text);
           except
                MensagemErro('Valor inv�lido.');
                exit;
           end;
      end;

      try
            try
                 QueryLocal:=TIBQuery.Create(nil);
                 QueryLOcal.Database:=FDataModulo.IBDatabase;
            except
                 MensagemErro('Erro ao tentar criar a Query Local');
                 exit;
            end;

            With  QueryLOcal do
            Begin
                 Close;
                 Sql.Clear;
                 Sql.Add('Select Codigo from TabDiverso Where GrupoDiverso = '+PGrupoDiverso);
                 Open;
                 First;

                 if (RecordCount = 0)then
                 Begin
                      MensagemErro('Nenhum Diverso encontrado para este grupo.');
                      exit;
                 end;


                 While Not (eof) do
                 Begin
                      PVAlorCusto:=0;
                      Self.ZerarTabela;
                      Self.LocalizaCodigo(fieldbyname('Codigo').AsString);
                      Self.TabelaparaObjeto;
                      PVAlorCusto:=StrToCurr(Self.Get_PrecoCusto);

                      Self.Status:=dsEdit;
                      Self.Submit_PrecoCusto(CurrToStr(PVAlorCusto+(PVAlorCusto*(PPorcentagemReajuste/100))));
                      try
                           Self.Salvar(false);
                      except
                           MensagemErro('Erro ao tentar Salvar o novo pre�o.');
                           FDataModulo.IBTransaction.RollbackRetaining;
                           exit;
                      end;
                 Next;
                 end;

                 FDataModulo.IBTransaction.CommitRetaining;
                 MensagemAviso('Pre�o de custo de diversos atualizado com Sucesso!');
            end;

      finally
            FreeAndNil(QueryLOcal);
      end;

end;


procedure TObjDIVERSO.Reajusta(PIndice,Pgrupo: string);
var
   Psinal:string;
begin
  try
    strtofloat(Pindice);

    if (strtofloat(Pindice)>=0) then
      Psinal:='+'
    else
    begin
      Pindice:=floattostr(strtofloat(Pindice)*-1);
      Psinal:='-';
    end;
        
  except
    mensagemerro('Valor Inv�lido no �ndice');
    exit;
  End;

  if (Pgrupo<>'') then
  begin
    try
      strtoint(Pgrupo);
      if (Self.GrupoDiverso.LocalizaCodigo(pgrupo)=False) then
      begin
        MensagemErro('Grupo de Diverso n�o encontrado');
        exit;
      End;
    Except
      mensagemerro('Grupo Inv�lido');
      exit;
    end;
  End;

  With Self.Objquery do
  Begin
    close;
    SQL.clear;
    SQL.add('Update TabDiverso Set PRECOCUSTO=PRECOCUSTO'+psinal+'((PRECOCUSTO*'+virgulaparaponto(Pindice)+')/100),');
    SQl.Add('precovendainstalado = precocusto + ( precocusto * porcentageminstalado/100),');
    SQl.Add('precovendafornecido = precocusto + ( precocusto * porcentagemfornecido/100),');
    SQl.Add('precovendaretirado = precocusto + ( precocusto * porcentagemretirado/100)');

    if (Pgrupo<>'')
    Then SQL.add('Where GrupoDiverso='+Pgrupo);

    try
      execsql;
      FDataModulo.IBTransaction.CommitRetaining;
      mensagemaviso('Conclu�do');
    Except
      on e:exception do
      begin
        FDataModulo.IBTransaction.RollbackRetaining;
        mensagemerro('Erro na tentativa de Reajustar o valor do DIVERSO'+#13+E.message);
        exit;
      End;
    End;
  End;
end;

procedure TObjDIVERSO.Submit_precovendaFORNECIDO(Parametro: string);
begin
     Self.PRECOVENDAFORNECIDO:=parametro;
end;

procedure TObjDIVERSO.Submit_precovendaINSTALADO(Parametro: string);
begin
     Self.PRECOVENDAINSTALADO:=parametro;
end;

procedure TObjDIVERSO.Submit_precovendaRETIRADO(Parametro: string);
begin
     Self.PRECOVENDARETIRADO:=parametro;
end;

procedure TObjDIVERSO.submit_NCM(parametro:string);
begin
    self.ncm:=parametro;
end;

function TObjDIVERSO.Get_NCM:string  ;
begin
  Result:=Self.NCM;
end;

procedure TObjDIVERSO.Submit_Ativo(parametro:string);
begin
    Self.ATIVO:=parametro;
end;

function TObjDIVERSO.Get_Ativo:string;
begin
    Result:=Self.ATIVO;
end;

procedure TObjDIVERSO.Submit_Fornecedor(parametro:string);
begin
  Self.Fornecedor:=parametro;
end;

function TObjDIVERSO.Get_Fornecedor:string;
begin
  Result:=Self.Fornecedor;
end;

procedure TObjDIVERSO.EdtFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FFORNECEDOR:TFFORNECEDOR;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FFORNECEDOR:=TFFORNECEDOR.create(nil);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabfornecedor','',FFornecedor)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If FpesquisaLocal.QueryPesq.fieldbyname('razaosocial').asstring<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('razaosocial').asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FFORNECEDOR);
     End;
end;

procedure TObjDIVERSO.AtualizaMargens(PGrupoDiverso: string);
Var QueryLOcal:TIBquery;
    PPorcentagemInstalado:string;
    PPorcentagemRetirado:string;
    PPorcentagemFornecido:string;
    NovoPreco:Currency;
begin
  if (PGrupoDiverso='')then
  Begin
    MensagemErro('Escolha um Grupo de Diverso');
    exit;
  end;

  if (Self.GrupoDiverso.LocalizaCodigo(PGrupoDiverso)=false)then
  Begin
    MensagemErro('Grupo de Diverso n�o encontrado');
    exit;
  end;
  Self.GrupoDiverso.TabelaparaObjeto;
  PPorcentagemInstalado:=Self.GrupoDiverso.Get_PorcentagemInstalado;
  PPorcentagemRetirado:=Self.GrupoDiverso.Get_PorcentagemRetirado;
  PPorcentagemFornecido:=Self.GrupoDiverso.Get_PorcentagemFornecido;

  try
    try
      QueryLOcal:=TIBQuery.Create(nil);
      QueryLOcal.Database:=FDataModulo.IBDatabase;
    except;
      MensagemErro('Erro ao tenar criar a QueryLocal');
      exit;
    end;

    With QueryLOcal  do
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select TabDiverso.Codigo from TabDiverso');
      Sql.Add('Where GrupoDiverso = '+PGrupoDiverso);
      Open;
      First;

      // Para cada Diverso desse grupo eu Altero o Valor das margns
      While not (eof) do
      Begin
        Self.ZerarTabela;
        if (Self.LocalizaCodigo(fieldbyname('Codigo').AsString)=false)then
        Begin
          MensagemErro('Diverso n�o encontrada.');
          exit;
        end;

        Self.TabelaparaObjeto;
        Self.Status:=dsEdit;
        Self.Submit_PorcentagemInstalado(PPorcentagemInstalado);
        Self.Submit_PorcentagemFornecido(PPorcentagemFornecido);
        Self.Submit_PorcentagemRetirado(PPorcentagemRetirado);

        NovoPreco:=(StrToCurr(Get_PrecoCusto)+((StrToCurr(Get_PrecoCusto)*StrToCurr(PPorcentagemInstalado))/100));
        Self.Submit_PrecoVendaInstalado(CurrToStr(NovoPreco));
        NovoPreco:=(StrToCurr(Get_PrecoCusto)+((StrToCurr(Get_PrecoCusto)*StrToCurr(PorcentagemFornecido))/100));
        self.Submit_PrecoVendaFornecido(CurrToStr(NovoPreco));
        NovoPreco:=(StrToCurr(Get_PrecoCusto)+((StrToCurr(Get_PrecoCusto)*StrToCurr(PorcentagemRetirado))/100));
        self.Submit_PrecoVendaRetirado(CurrToStr(NovoPreco));

        if (Self.Salvar(false)=false)then
        begin
          MensagemErro('Erro ao tentar Atualiza as margens na Tabela de Diverso');
          exit;
        end;
        Next;
      end;
    end;
    FDataModulo.IBTransaction.CommitRetaining;
    MensagemAviso('Margem Atualiada com Sucesso !');
  finally
    FreeAndNil(QueryLOcal);
  end;
end;

function TObjDIVERSO.AtualizaTributospeloNCM:Boolean;
var
  qryprod, qryaux: TIBQuery;
  percentual,NCM:string;
begin
  {procuro por produtos onde o ncm estiver preenchido
  depois localizo no cadastro de ncm , resgato o valor do imposto
  e atualiza o produto
  }

  Result := False;
  qryprod := TIBQuery.Create(nil);
  qryaux := TIBQuery.Create(nil);

  qryprod.Database := FDataModulo.IBDatabase;
  qryaux.Database := FDataModulo.IBDatabase;
  try
    qryprod.DisableControls;
    qryprod.Close;
    qryprod.SQL.Clear;
    qryprod.SQL.Add('SELECT COUNT(CODIGO) FROM TABdiverso WHERE NCM IS NOT NULL');
    qryprod.Open;

    FMostraBarraProgresso.ConfiguracoesIniciais(qryprod.Fields[0].AsInteger - 1,0);
    FMostraBarraProgresso.Lbmensagem.Caption := 'Atualizando percentuais de tributos (diverso)';
    FMostraBarraProgresso.btcancelar.Visible := True;
    FMostraBarraProgresso.Show;
    Application.ProcessMessages;

    qryprod.Close;
    qryprod.SQL.Clear;
    qryprod.SQL.Add('select ficms.codigo,icms.sta,f.ncm');
    qryprod.SQL.Add('from tabimposto_icms icms');
    qryprod.SQL.Add('join tabdiverso_icms ficms on ficms.imposto=icms.codigo');
    qryprod.SQL.Add('join tabdiverso f on f.codigo=ficms.diverso');
    qryprod.SQL.Add('where f.ncm is not null');
    qryprod.SQL.Add('group by ficms.codigo, icms.sta,f.ncm');
    qryprod.Open;

    while not(qryprod.Eof) do
    begin
      NCM := qryprod.Fields[2].AsString;
      if(qryprod.Fields[1].AsString = '0') then
        percentual := get_campoTabela('PERCENTUALTRIBUTO','CODIGONCM','TABNCM',NCM)
      else
        if( (qryprod.Fields[1].AsString = '1') or (qryprod.Fields[1].AsString = '2')) then
          percentual := get_campoTabela('PERCENTUALTRIBUTOIMP','CODIGONCM','TABNCM',NCM)
        else
          percentual := '0';

      if(StrToCurrDef(percentual,0) > 0) then
      begin
        qryaux.DisableControls;
        qryaux.Close;
        qryaux.SQL.Clear;
        qryaux.SQL.Add('UPDATE tabdiverso_icms SET PERCENTUALTRIBUTO=:PTRIBUTO WHERE CODIGO=:PPRODUTO');
        qryaux.Params[0].AsCurrency := StrToCurr(percentual);
        qryaux.Params[1].AsString := qryprod.Fields[0].AsString;

        try
          qryaux.ExecSQL;
        except
          raise Exception.Create('Erro ao atualizar percentual do tributo para o produto: '+qryprod.Fields[0].AsString);
        end;
      end;
      qryprod.Next;
      FMostraBarraProgresso.IncrementaBarra1(1);
      if(FMostraBarraProgresso.Cancelar) then
        Exit;      
      Application.ProcessMessages;
    end;
    qryaux.Transaction.CommitRetaining;
    //FDataModulo.IBTransaction.CommitRetaining;
    Result := True;
  finally
    FMostraBarraProgresso.Close;
    if(qryaux.Transaction.Active) then
      qryaux.Transaction.Rollback;
    if(qryprod <> nil) then
      FreeAndNil(qryprod);
    if(qryaux <> nil) then
      FreeAndNil(qryaux);
  end;

end;

function TObjDIVERSO.Get_cest: string;
begin
  result := self.cest;
end;

procedure TObjDIVERSO.Submit_cest(parametro: string);
begin
  Self.cest := parametro;
end;

end.



