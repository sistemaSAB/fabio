unit UIMPOSTO_PIS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjIMPOSTO_PIS,
  jpeg;

type
  TFIMPOSTO_PIS = class(TForm)
    edtFORMULA_VALOR_IMPOSTO_ST_PIS: TEdit;
    lb1: TLabel;
    edtFORMULABASECALCULO_ST_PIS: TEdit;
    lb2: TLabel;
    edtALIQUOTAPERCENTUAL_ST_PIS: TEdit;
    lbLbALIQUOTAPERCENTUAL_ST: TLabel;
    edtALIQUOTAVALOR_ST_PIS: TEdit;
    lbLbALIQUOTAVALOR_ST: TLabel;
    cbbTIPOCALCULO_ST_PIS: TComboBox;
    lbLbTIPOCALCULO_ST: TLabel;
    edtFORMULA_VALOR_IMPOSTO_PIS: TEdit;
    lb3: TLabel;
    edtFORMULABASECALCULO_PIS: TEdit;
    lb4: TLabel;
    edtALIQUOTAPERCENTUAL_PIS: TEdit;
    lbLbALIQUOTAPERCENTUAL: TLabel;
    cbbTIPOCALCULO_PIS: TComboBox;
    lbLbTIPOCALCULO: TLabel;
    edtST_PIS: TEdit;
    lbLbST: TLabel;
    lbNomeST_PIS: TLabel;
    edtALIQUOTAVALOR_PIS: TEdit;
    lbLbALIQUOTAVALOR: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb5: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
//DECLARA COMPONENTES
    procedure edtST_PISKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtST_PISExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtST_PISDblClick(Sender: TObject);
  private
         ObjIMPOSTO_PIS:TObjIMPOSTO_PIS;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FIMPOSTO_PIS: TFIMPOSTO_PIS;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFIMPOSTO_PIS.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjIMPOSTO_PIS do
    Begin
        Submit_CODIGO(lbCodigo.Caption);
        ST.Submit_codigo(edtST_PIS.text);
        Submit_TIPOCALCULO(cbbTIPOCALCULO_PIS.text);
        Submit_ALIQUOTAPERCENTUAL(edtALIQUOTAPERCENTUAL_PIS.text);
        Submit_ALIQUOTAVALOR(edtALIQUOTAVALOR_PIS.text);
        Submit_TIPOCALCULO_ST(cbbTIPOCALCULO_ST_PIS.text);
        Submit_ALIQUOTAPERCENTUAL_ST(edtALIQUOTAPERCENTUAL_ST_PIS.text);
        Submit_ALIQUOTAVALOR_ST(edtALIQUOTAVALOR_ST_PIS.text);
        Submit_FORMULABASECALCULO(EdtFORMULABASECALCULO_PIS.text);
        Submit_FORMULABASECALCULO_ST(EdtFORMULABASECALCULO_ST_PIS.text);


        Submit_formula_valor_imposto(Edtformula_valor_imposto_PIS.text);
        Submit_formula_valor_imposto_ST(Edtformula_valor_imposto_ST_PIS.text);
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFIMPOSTO_PIS.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjIMPOSTO_PIS do
     Begin
        lbCodigo.Caption:=Get_CODIGO;
        EdtST_PIS.text:=ST.Get_codigo;
        cbbTIPOCALCULO_PIS.text:=Get_TIPOCALCULO;
        EdtALIQUOTAPERCENTUAL_PIS.text:=Get_ALIQUOTAPERCENTUAL;
        EdtALIQUOTAVALOR_PIS.text:=Get_ALIQUOTAVALOR;
        cbbTIPOCALCULO_ST_PIS.text:=Get_TIPOCALCULO_ST;
        EdtALIQUOTAPERCENTUAL_ST_PIS.text:=Get_ALIQUOTAPERCENTUAL_ST;
        EdtALIQUOTAVALOR_ST_PIS.text:=Get_ALIQUOTAVALOR_ST;
        edtFORMULABASECALCULO_PIS.text:=Get_FORMULABASECALCULO;
        edtFORMULABASECALCULO_ST_PIS.text:=Get_FORMULABASECALCULO_ST;

        edtformula_valor_imposto_PIS.text:=Get_formula_valor_imposto;
        edtformula_valor_imposto_ST_PIS.text:=Get_formula_valor_imposto_ST;

//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFIMPOSTO_PIS.TabelaParaControles: Boolean;
begin
     If (Self.ObjIMPOSTO_PIS.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFIMPOSTO_PIS.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjIMPOSTO_PIS:=TObjIMPOSTO_PIS.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
      FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(imgrodape,'RODAPE');


end;

procedure TFIMPOSTO_PIS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjIMPOSTO_PIS=Nil)
     Then exit;

If (Self.ObjIMPOSTO_PIS.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjIMPOSTO_PIS.free;
end;

procedure TFIMPOSTO_PIS.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFIMPOSTO_PIS.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
    // desab_botoes(Self);

     lbCodigo.Caption:='0';
     //edtcodigo_PIS.text:=Self.ObjIMPOSTO_PIS.Get_novocodigo;


     
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjIMPOSTO_PIS.status:=dsInsert;
     edtST_PIS.setfocus;
     btnovo.Visible:=false;
     btalterar.Visible:=False;
     btexcluir.Visible:=false;
     btrelatorios.Visible:=false;
     btopcoes.Visible:=false;
     btsair.Visible:=False;
     btpesquisar.visible:=False;


end;


procedure TFIMPOSTO_PIS.btalterarClick(Sender: TObject);
begin
    If (Self.ObjIMPOSTO_PIS.Status=dsinactive) and (lbCodigo.Caption<>'')
    Then Begin
                MensagemAviso('N�o � permitido a altera��o de Impostos');
                (*habilita_campos(Self);
                EdtCodigo_PIS.enabled:=False;
                Self.ObjIMPOSTO_PIS.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtST_PIS.setfocus;*)
                 btnovo.Visible:=false;
     btalterar.Visible:=False;
     btexcluir.Visible:=false;
     btrelatorios.Visible:=false;
     btopcoes.Visible:=false;
     btsair.Visible:=False;
     btpesquisar.visible:=False;

    End;


end;

procedure TFIMPOSTO_PIS.btgravarClick(Sender: TObject);
begin

     If Self.ObjIMPOSTO_PIS.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjIMPOSTO_PIS.salvar(true)=False)
     Then exit;

     lbCodigo.Caption:=Self.ObjIMPOSTO_PIS.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
      btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;
     
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFIMPOSTO_PIS.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjIMPOSTO_PIS.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (Self.ObjIMPOSTO_PIS.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjIMPOSTO_PIS.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFIMPOSTO_PIS.btcancelarClick(Sender: TObject);
begin
     Self.ObjIMPOSTO_PIS.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
      btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;


end;

procedure TFIMPOSTO_PIS.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFIMPOSTO_PIS.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjIMPOSTO_PIS.Get_pesquisa,Self.ObjIMPOSTO_PIS.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjIMPOSTO_PIS.status<>dsinactive
                                  then exit;

                                  If (Self.ObjIMPOSTO_PIS.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjIMPOSTO_PIS.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFIMPOSTO_PIS.LimpaLabels;
begin
//LIMPA LABELS
     LbNomeST_PIS.Caption:='';
     lbCodigo.Caption;
end;

procedure TFIMPOSTO_PIS.FormShow(Sender: TObject);
begin
     //PegaCorForm(Self);
end;
procedure TFIMPOSTO_PIS.edtST_PISKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjIMPOSTO_PIS.edtSTkeydown(sender,key,shift,lbnomeST_PIS);
end;
 
procedure TFIMPOSTO_PIS.edtST_PISExit(Sender: TObject);
begin
    ObjIMPOSTO_PIS.edtSTExit(sender,lbnomeST_PIS);
end;
//CODIFICA ONKEYDOWN E ONEXIT

{r4mr}
procedure TFIMPOSTO_PIS.edtST_PISDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtST_PISKeyDown(Sender, Key, Shift);
end;

end.

