unit UBAIRRO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjBAIRRO,
  UessencialGlobal, UessencialLOcal, Tabs,IBQuery;

type
  TFBAIRRO = class(TForm)
    edtNome: TEdit;
    lbLbNome: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbcodigo: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    imgrodape: TImage;
    lb14: TLabel;
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    ObjBAIRRO:TObjBAIRRO;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    procedure MostraQuantidadeCadastrada;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FBAIRRO: TFBAIRRO;


implementation

uses Upesquisa, UDataModulo, UescolheImagemBotao;

{$R *.dfm}



procedure TFBAIRRO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjBAIRRO=Nil)
     Then exit;

     If (Self.ObjBAIRRO.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjBAIRRO.free;
    
end;


procedure TFBAIRRO.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbcodigo.Caption:='0';

     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjBAIRRO.status:=dsInsert;
      btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

     EdtNome.setfocus;

end;

procedure TFBAIRRO.btSalvarClick(Sender: TObject);
begin

     If ObjBAIRRO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjBAIRRO.salvar(true)=False)
     Then exit;

     lbcodigo.Caption:=ObjBAIRRO.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;
     MostraQuantidadeCadastrada;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFBAIRRO.btAlterarClick(Sender: TObject);
begin
    If (ObjBAIRRO.Status=dsinactive) and (lbcodigo.Caption<>'')
    Then Begin
                habilita_campos(Self);

                ObjBAIRRO.Status:=dsEdit;

                edtNome.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                 btnovo.Visible :=false;
                 btalterar.Visible:=false;
                 btpesquisar.Visible:=false;
                 btrelatorio.Visible:=false;
                 btexcluir.Visible:=false;
                 btsair.Visible:=false;
                 btopcoes.Visible :=false;
          End;

end;

procedure TFBAIRRO.btCancelarClick(Sender: TObject);
begin
     ObjBAIRRO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;

end;

procedure TFBAIRRO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjBAIRRO.Get_pesquisa,ObjBAIRRO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjBAIRRO.status<>dsinactive
                                  then exit;

                                  If (ObjBAIRRO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjBAIRRO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFBAIRRO.btExcluirClick(Sender: TObject);
begin
     If (ObjBAIRRO.status<>dsinactive) or (lbcodigo.Caption='')
     Then exit;

     If (ObjBAIRRO.LocalizaCodigo(lbcodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjBAIRRO.exclui(lbcodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFBAIRRO.btRelatorioClick(Sender: TObject);
begin
    ObjBAIRRO.Imprime(lbcodigo.Caption);
end;

procedure TFBAIRRO.btSairClick(Sender: TObject);
begin
    Self.close;
end;


procedure TFBAIRRO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFBAIRRO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFBAIRRO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;

end;

procedure TFBAIRRO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFBAIRRO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjBAIRRO do
    Begin
        Submit_Codigo(lbcodigo.Caption);
        Submit_Nome(edtNome.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFBAIRRO.LimpaLabels;
begin
  lbcodigo.Caption:='';
end;

function TFBAIRRO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjBAIRRO do
     Begin
       lbcodigo.Caption:=Get_Codigo;
        EdtNome.text:=Get_Nome;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFBAIRRO.TabelaParaControles: Boolean;
begin
     If (ObjBAIRRO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;


procedure TFBAIRRO.MostraQuantidadeCadastrada;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabbairro');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb14.Caption:='Existem '+IntToStr(contador)+' bairros cadastrados'
       else
       begin
            lb14.Caption:='Existe '+IntToStr(contador)+' bairro cadastrado';
       end;

    finally

    end;


end;

//CODIFICA ONKEYDOWN E ONEXIT


procedure TFBAIRRO.FormShow(Sender: TObject);
begin
       limpaedit(Self);
       Self.limpaLabels;
       desabilita_campos(Self);

       Try
          Self.ObjBAIRRO:=TObjBAIRRO.create;
       Except
             Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
             Self.close;
       End;

        FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
       FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
       lbcodigo.caption:='';

       if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE BAIRRO')=False)
       Then desab_botoes(Self)
       Else habilita_botoes(Self);
       MostraQuantidadeCadastrada;
end;

end.
