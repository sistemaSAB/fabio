object FPERSIANA: TFPERSIANA
  Left = 447
  Top = 256
  Width = 922
  Height = 641
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'CADASTRO DE PERSIANA'
  Color = 13421772
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object imgrodape: TImage
    Left = 0
    Top = 566
    Width = 906
    Height = 37
    Align = alBottom
    Stretch = True
  end
  object Notebook: TNotebook
    Left = 0
    Top = 85
    Width = 906
    Height = 481
    Align = alClient
    Color = 10643006
    PageIndex = 2
    ParentColor = False
    TabOrder = 1
    object TPage
      Left = 0
      Top = 0
      Caption = 'Principal'
      object LbNome: TLabel
        Left = 16
        Top = 63
        Width = 33
        Height = 13
        Caption = 'Nome'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbReferencia: TLabel
        Left = 16
        Top = 20
        Width = 61
        Height = 13
        Caption = 'Refer'#234'ncia'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object lb2: TLabel
        Left = 16
        Top = 108
        Width = 64
        Height = 13
        Caption = 'Fornecedor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeFornecedor: TLabel
        Left = 93
        Top = 125
        Width = 412
        Height = 13
        Cursor = crHandPoint
        AutoSize = False
        Caption = 'Fornecedor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clAqua
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = LbNomeFornecedorClick
        OnMouseMove = LbNomeFornecedorMouseMove
        OnMouseLeave = LbNomeFornecedorMouseLeave
      end
      object SpeedButtonPrimeiro: TSpeedButton
        Left = 159
        Top = 393
        Width = 65
        Height = 48
        Caption = '<<'
        Flat = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clLime
        Font.Height = -48
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btPrimeiroClick
      end
      object SpeedButtonAnterior: TSpeedButton
        Left = 322
        Top = 393
        Width = 63
        Height = 48
        Caption = '<'
        Flat = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clLime
        Font.Height = -48
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btAnteriorClick
      end
      object SpeedButtonProximo: TSpeedButton
        Left = 486
        Top = 393
        Width = 65
        Height = 48
        Caption = '>'
        Flat = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clLime
        Font.Height = -48
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btProximoClick
      end
      object SpeedButtonUltimo: TSpeedButton
        Left = 650
        Top = 393
        Width = 65
        Height = 48
        Caption = '>>'
        Flat = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clLime
        Font.Height = -48
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btUltimoClick
      end
      object lb3: TLabel
        Left = 16
        Top = 152
        Width = 26
        Height = 13
        Caption = 'NCM'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object lbAtivoInativo: TLabel
        Left = 775
        Top = 27
        Width = 77
        Height = 37
        Caption = 'Ativo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -32
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 16
        Top = 200
        Width = 26
        Height = 13
        Caption = 'Cest'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object EdtReferencia: TEdit
        Left = 16
        Top = 36
        Width = 72
        Height = 19
        MaxLength = 20
        TabOrder = 0
      end
      object EdtNome: TEdit
        Left = 16
        Top = 79
        Width = 489
        Height = 19
        MaxLength = 50
        TabOrder = 1
      end
      object EdtFornecedor: TEdit
        Left = 16
        Top = 122
        Width = 72
        Height = 19
        Color = 6073854
        MaxLength = 9
        TabOrder = 2
        OnDblClick = EdtFornecedorDblClick
        OnExit = EdtFornecedorExit
        OnKeyDown = EdtFornecedorKeyDown
      end
      object edtNCM: TEdit
        Left = 17
        Top = 168
        Width = 72
        Height = 19
        Color = 6073854
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        MaxLength = 20
        ParentFont = False
        TabOrder = 3
        OnKeyDown = edtNCMKeyDown
        OnKeyPress = edtNCMKeyPress
      end
      object chkAtivo: TCheckBox
        Left = 727
        Top = 34
        Width = 25
        Height = 25
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -32
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        OnClick = chkAtivoClick
      end
      object edtCest: TEdit
        Left = 17
        Top = 216
        Width = 72
        Height = 19
        Color = 6073854
        TabOrder = 5
        OnKeyDown = edtCestKeyDown
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Nota Fiscal'
      object LbSituacaoTributaria_TabelaA: TLabel
        Left = 376
        Top = 185
        Width = 108
        Height = 13
        Caption = 'Situa'#231#227'o Tribut'#225'ria'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object Label16: TLabel
        Left = 488
        Top = 185
        Width = 71
        Height = 13
        Caption = '% Agregado'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object EdtSituacaoTributaria_TabelaA: TEdit
        Left = 376
        Top = 203
        Width = 25
        Height = 19
        Color = clGradientActiveCaption
        MaxLength = 9
        TabOrder = 0
        Text = '1'
        Visible = False
      end
      object EdtSituacaoTributaria_TabelaB: TEdit
        Left = 408
        Top = 203
        Width = 57
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 1
        Text = '1'
        Visible = False
      end
      object PanelICMSForaEstado: TPanel
        Left = 340
        Top = 244
        Width = 293
        Height = 262
        TabOrder = 3
        Visible = False
        object Label46: TLabel
          Left = 14
          Top = 136
          Width = 72
          Height = 13
          Caption = 'Al'#237'quota (%)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label47: TLabel
          Left = 14
          Top = 182
          Width = 189
          Height = 13
          Caption = 'Redu'#231#227'o de Base de C'#225'lculo (%)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label48: TLabel
          Left = 16
          Top = 222
          Width = 117
          Height = 13
          Caption = 'Al'#237'quota Cupom (%)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label49: TLabel
          Left = 3
          Top = 4
          Width = 285
          Height = 13
          Alignment = taCenter
          AutoSize = False
          Caption = 'ICMS PARA VENDA FORA DO ESTADO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label50: TLabel
          Left = 6
          Top = 20
          Width = 244
          Height = 13
          Caption = 'Isento, diferido ou n'#227'o tributado no ICMS?'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label51: TLabel
          Left = 14
          Top = 60
          Width = 134
          Height = 13
          Caption = 'Substitui'#231#227'o Tribut'#225'ria?'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label52: TLabel
          Left = 14
          Top = 100
          Width = 191
          Height = 13
          Caption = 'Valor da Pauta (Regime Especial)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object EdtAliquota_ICMS_ForaEstado: TEdit
          Left = 14
          Top = 154
          Width = 72
          Height = 19
          MaxLength = 9
          TabOrder = 3
          Text = '0'
        end
        object EdtReducao_BC_ICMS_ForaEstado: TEdit
          Left = 14
          Top = 198
          Width = 72
          Height = 19
          MaxLength = 9
          TabOrder = 4
          Text = '0'
        end
        object EdtAliquota_ICMS_Cupom_ForaEstado: TEdit
          Left = 16
          Top = 238
          Width = 72
          Height = 19
          MaxLength = 9
          TabOrder = 5
          Text = '0'
        end
        object comboisentoicms_foraestado: TComboBox
          Left = 14
          Top = 36
          Width = 89
          Height = 21
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
          Text = 'N'#195'O'
          Items.Strings = (
            'N'#227'o'
            'Sim')
        end
        object comboSUBSTITUICAOICMS_FORAESTADO: TComboBox
          Left = 14
          Top = 76
          Width = 89
          Height = 21
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 1
          Text = 'SIM'
          Items.Strings = (
            'N'#227'o'
            'Sim')
        end
        object edtVALORPAUTA_SUB_TRIB_FORAESTADO: TEdit
          Left = 14
          Top = 117
          Width = 72
          Height = 19
          MaxLength = 9
          TabOrder = 2
          Text = '0'
        end
      end
      object PanelICMSEstado: TPanel
        Left = 316
        Top = 236
        Width = 293
        Height = 262
        TabOrder = 4
        Visible = False
        object LbAliquota_ICMS_Estado: TLabel
          Left = 14
          Top = 144
          Width = 72
          Height = 13
          Caption = 'Al'#237'quota (%)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object LbReducao_BC_ICMS_Estado: TLabel
          Left = 14
          Top = 188
          Width = 189
          Height = 13
          Caption = 'Redu'#231#227'o de Base de C'#225'lculo (%)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object LbAliquota_ICMS_Cupom_Estado: TLabel
          Left = 11
          Top = 224
          Width = 117
          Height = 13
          Caption = 'Al'#237'quota Cupom (%)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label42: TLabel
          Left = 3
          Top = 4
          Width = 286
          Height = 13
          Alignment = taCenter
          AutoSize = False
          Caption = 'ICMS PARA VENDA NO ESTADO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label43: TLabel
          Left = 14
          Top = 20
          Width = 244
          Height = 13
          Caption = 'Isento, diferido ou n'#227'o tributado no ICMS?'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label44: TLabel
          Left = 14
          Top = 60
          Width = 134
          Height = 13
          Caption = 'Substitui'#231#227'o Tribut'#225'ria?'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label45: TLabel
          Left = 14
          Top = 108
          Width = 191
          Height = 13
          Caption = 'Valor da Pauta (Regime Especial)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object EdtAliquota_ICMS_Estado: TEdit
          Left = 14
          Top = 162
          Width = 72
          Height = 19
          MaxLength = 9
          TabOrder = 3
          Text = '0'
        end
        object EdtReducao_BC_ICMS_Estado: TEdit
          Left = 14
          Top = 205
          Width = 72
          Height = 19
          MaxLength = 9
          TabOrder = 4
          Text = '0'
        end
        object EdtAliquota_ICMS_Cupom_Estado: TEdit
          Left = 11
          Top = 240
          Width = 72
          Height = 19
          MaxLength = 9
          TabOrder = 5
          Text = '0'
        end
        object comboisentoicms_estado: TComboBox
          Left = 14
          Top = 36
          Width = 89
          Height = 21
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
          Text = 'N'#195'O'
          Items.Strings = (
            'N'#227'o'
            'Sim')
        end
        object comboSUBSTITUICAOICMS_ESTADO: TComboBox
          Left = 14
          Top = 76
          Width = 89
          Height = 21
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 1
          Text = 'SIM'
          Items.Strings = (
            'N'#227'o'
            'Sim')
        end
        object edtVALORPAUTA_SUB_TRIB_ESTADO: TEdit
          Left = 14
          Top = 125
          Width = 72
          Height = 19
          MaxLength = 9
          TabOrder = 2
          Text = '0'
        end
      end
      object edtpercentualagregado: TEdit
        Left = 488
        Top = 203
        Width = 73
        Height = 19
        MaxLength = 15
        TabOrder = 2
        Text = '1'
        Visible = False
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 906
        Height = 31
        Align = alTop
        BevelOuter = bvNone
        Color = 10643006
        TabOrder = 5
        object LbClassificaoFiscal: TLabel
          Left = 4
          Top = 17
          Width = 113
          Height = 13
          Caption = 'Classifica'#231#227'o Fiscal '
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object EdtClassificacaoFiscal: TEdit
          Left = 228
          Top = 14
          Width = 60
          Height = 19
          MaxLength = 15
          TabOrder = 0
          Text = '1'
        end
      end
      inline FRImposto_ICMS1: TFRImposto_ICMS
        Left = 0
        Top = 31
        Width = 906
        Height = 450
        Align = alClient
        Color = clWhite
        ParentColor = False
        TabOrder = 6
        inherited Panel_TOP: TPanel
          Width = 906
          Height = 33
          BevelOuter = bvNone
          Color = 10643006
          inherited Label17: TLabel
            Left = 288
            Top = 11
            Width = 38
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Name = 'Verdana'
            ParentFont = False
          end
          inherited Label18: TLabel
            Left = 408
            Top = 11
            Width = 86
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Name = 'Verdana'
            ParentFont = False
          end
          inherited lbnometipocliente: TLabel
            Left = 558
            Top = 11
            Width = 86
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Name = 'Verdana'
            ParentFont = False
          end
          inherited lbcodigo_material_icms: TLabel
            Left = 61
            Top = 11
            Width = 41
            Height = 15
            Font.Charset = ANSI_CHARSET
            Font.Color = 6073854
            Font.Name = 'Arial Black'
          end
          inherited Label1: TLabel
            Top = 11
            Width = 45
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Name = 'Verdana'
            ParentFont = False
          end
          inherited Label33: TLabel
            Left = 160
            Top = 11
            Width = 55
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Name = 'Verdana'
            ParentFont = False
          end
          inherited btReplicarImpostos: TSpeedButton
            Left = 855
            Width = 48
            Height = 33
          end
          inherited edtestado_material_icms: TEdit
            Left = 336
            Top = 9
            Width = 60
          end
          inherited edttipocliente_material_icms: TEdit
            Left = 496
            Top = 9
            Width = 60
          end
          inherited edtoperacao: TEdit
            Left = 228
            Top = 9
            Width = 60
          end
          inherited Button1: TButton
            Left = 808
            Top = 36
          end
        end
        inherited STRG_Imposto: TStringGrid
          Top = 306
          Width = 906
          Height = 144
        end
        inherited Aba_Impostos: TPageControl
          Top = 33
          Width = 906
          inherited TabSheet2: TTabSheet
            inherited edtimposto_ipi_modelo: TEdit
              Height = 19
              Color = 6073854
            end
            inherited EdtST_IPI: TEdit
              Height = 19
              Color = 6073854
            end
            inherited EdtClasseEnq_IPI: TEdit
              Height = 19
            end
            inherited EdtCodigoEnquadramento_IPI: TEdit
              Height = 19
            end
            inherited EdtCnpjProdutor_IPI: TEdit
              Height = 19
            end
            inherited EdtCodigoSelo_IPI: TEdit
              Height = 19
            end
            inherited EdtQuantidadeTotalUP_IPI: TEdit
              Height = 19
            end
            inherited EdtValorUnidade_IPI: TEdit
              Height = 19
            end
            inherited EdtFORMULABASECALCULO_IPI: TEdit
              Height = 19
            end
            inherited edtaliquota_ipi: TEdit
              Height = 19
            end
            inherited edtFORMULA_VALOR_IMPOSTO_IPI: TEdit
              Height = 19
            end
          end
          inherited TabSheet3: TTabSheet
            inherited EdtST_PIS: TEdit
              Height = 19
              Color = 6073854
            end
            inherited EdtALIQUOTAPERCENTUAL_PIS: TEdit
              Height = 19
            end
            inherited EdtALIQUOTAVALOR_PIS: TEdit
              Height = 19
            end
            inherited EdtALIQUOTAPERCENTUAL_ST_PIS: TEdit
              Height = 19
            end
            inherited EdtALIQUOTAVALOR_ST_PIS: TEdit
              Height = 19
            end
            inherited EdtFORMULABASECALCULO_PIS: TEdit
              Height = 19
            end
            inherited EdtFORMULABASECALCULO_ST_PIS: TEdit
              Height = 19
            end
            inherited edtimposto_pis_modelo: TEdit
              Height = 19
              Color = 6073854
            end
            inherited edtFORMULA_VALOR_IMPOSTO_PIS: TEdit
              Height = 19
            end
            inherited edtFORMULA_VALOR_IMPOSTO_ST_PIS: TEdit
              Height = 19
            end
          end
          inherited TabSheet4: TTabSheet
            inherited EdtST_COFINS: TEdit
              Color = 6073854
            end
            inherited edtimposto_cofins_modelo: TEdit
              Color = 6073854
            end
          end
        end
        inherited PanelBotoes: TPanel
          Top = 278
          Width = 906
          Color = 10643006
          inherited BtGravar_material_icms: TButton
            Height = 22
          end
          inherited btcancelar_material_ICMS: TButton
            Height = 22
          end
          inherited btexcluir_material_ICMS: TButton
            Height = 22
          end
        end
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Classe'
      object PanelPersianaGDC: TPanel
        Left = 0
        Top = 0
        Width = 906
        Height = 204
        Align = alTop
        Color = 10643006
        TabOrder = 0
        object LbGrupoPersiana: TLabel
          Left = 6
          Top = 13
          Width = 84
          Height = 13
          Caption = 'GrupoPersiana'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object LbCor: TLabel
          Left = 6
          Top = 32
          Width = 21
          Height = 13
          Caption = 'Cor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object LbDiametro: TLabel
          Left = 6
          Top = 54
          Width = 53
          Height = 13
          Caption = 'Di'#226'metro'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object LbEstoque: TLabel
          Left = 6
          Top = 137
          Width = 54
          Height = 13
          Cursor = crHandPoint
          Caption = '1000 UN'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = LbEstoqueClick
          OnMouseMove = LbEstoqueMouseMove
          OnMouseLeave = LbEstoqueMouseLeave
        end
        object LbNomeGrupoPersianaGDC: TLabel
          Left = 203
          Top = 16
          Width = 82
          Height = 14
          Cursor = crHandPoint
          Caption = 'GrupoPersiana'
          Font.Charset = ANSI_CHARSET
          Font.Color = clAqua
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = LbNomeGrupoPersianaGDCClick
          OnMouseMove = LbEstoqueMouseMove
          OnMouseLeave = LbNomeFornecedorMouseLeave
        end
        object LbNomeCorGDC: TLabel
          Left = 205
          Top = 36
          Width = 20
          Height = 14
          Cursor = crHandPoint
          Caption = 'Cor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clAqua
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = LbNomeCorGDCClick
          OnMouseMove = LbEstoqueMouseMove
          OnMouseLeave = LbNomeFornecedorMouseLeave
        end
        object LbNomeDiametroGDC: TLabel
          Left = 206
          Top = 59
          Width = 50
          Height = 14
          Cursor = crHandPoint
          Caption = 'Di'#226'metro'
          Font.Charset = ANSI_CHARSET
          Font.Color = clAqua
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          OnMouseLeave = LbNomeFornecedorMouseLeave
        end
        object Label15: TLabel
          Left = 6
          Top = 79
          Width = 109
          Height = 13
          Caption = 'Classifica'#231#227'o Fiscal'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object lb4: TLabel
          Left = 6
          Top = 120
          Width = 52
          Height = 13
          Caption = 'Estoque'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lb5: TLabel
          Left = 6
          Top = 156
          Width = 77
          Height = 13
          Caption = 'Pre'#231'o Custo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label8: TLabel
          Left = 162
          Top = 120
          Width = 102
          Height = 13
          Caption = 'Estoque M'#237'nimo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EdtCodigoPersianaGDC: TEdit
          Left = 340
          Top = 16
          Width = 81
          Height = 19
          MaxLength = 9
          TabOrder = 5
          Visible = False
        end
        object EdtCorGDCReferencia: TEdit
          Left = 126
          Top = 34
          Width = 72
          Height = 19
          Color = 6073854
          Enabled = False
          MaxLength = 9
          TabOrder = 1
          OnDblClick = EdtCorGDCReferenciaDblClick
          OnExit = EdtCorGDCReferenciaExit
          OnKeyDown = EdtCorGDCReferenciaKeyDown
        end
        object EdtGrupoPersianaGDCReferencia: TEdit
          Left = 126
          Top = 13
          Width = 72
          Height = 19
          Color = 6073854
          MaxLength = 9
          TabOrder = 0
          OnDblClick = EdtGrupoPersianaGDCReferenciaDblClick
          OnExit = EdtGrupoPersianaGDCReferenciaExit
          OnKeyDown = EdtGrupoPersianaGDCReferenciaKeyDown
        end
        object EdtDiametroGDC: TEdit
          Left = 126
          Top = 56
          Width = 72
          Height = 19
          Color = 6073854
          MaxLength = 9
          TabOrder = 2
          OnDblClick = EdtDiametroGDCDblClick
          OnExit = EdtDiametroGDCExit
          OnKeyDown = EdtDiametroGDCKeyDown
        end
        object GroupBoxMargem: TGroupBox
          Left = 433
          Top = 10
          Width = 206
          Height = 97
          Caption = 'Margem'
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          object LbPorcentagemFornecido: TLabel
            Left = 11
            Top = 47
            Width = 55
            Height = 13
            Caption = 'Fornecido'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object LbPorcentagemRetirado: TLabel
            Left = 11
            Top = 72
            Width = 48
            Height = 13
            Caption = 'Retirado'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object LbPorcentagemInstalado: TLabel
            Left = 11
            Top = 21
            Width = 53
            Height = 13
            Caption = 'Instalado'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label2: TLabel
            Left = 185
            Top = 23
            Width = 12
            Height = 13
            Caption = '%'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 185
            Top = 50
            Width = 12
            Height = 13
            Caption = '%'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label4: TLabel
            Left = 185
            Top = 77
            Width = 12
            Height = 13
            Caption = '%'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object EdtPorcentagemInstaladoGDC: TEdit
            Left = 110
            Top = 20
            Width = 72
            Height = 19
            Color = 15663069
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 9
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            OnExit = EdtPorcentagemInstaladoGDCExit
          end
          object EdtPorcentagemFornecidoGDC: TEdit
            Left = 110
            Top = 46
            Width = 72
            Height = 19
            Color = 15663069
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 9
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            OnExit = EdtPorcentagemFornecidoGDCExit
          end
          object EdtPorcentagemRetiradoGDC: TEdit
            Left = 110
            Top = 73
            Width = 72
            Height = 19
            Color = 15663069
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 9
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            OnExit = EdtPorcentagemRetiradoGDCExit
          end
        end
        object GroupBoxPrecoVenda: TGroupBox
          Left = 658
          Top = 16
          Width = 205
          Height = 92
          Caption = 'Pre'#231'o Venda'
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          object LbPrecoVendaInstalado: TLabel
            Left = 11
            Top = 17
            Width = 53
            Height = 13
            Caption = 'Instalado'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object LbPrecoVendaFornecido: TLabel
            Left = 11
            Top = 44
            Width = 55
            Height = 13
            Caption = 'Fornecido'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object LbPrecoVendaRetirado: TLabel
            Left = 11
            Top = 70
            Width = 48
            Height = 13
            Caption = 'Retirado'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label5: TLabel
            Left = 92
            Top = 17
            Width = 15
            Height = 13
            Caption = 'R$'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label6: TLabel
            Left = 92
            Top = 44
            Width = 15
            Height = 13
            Caption = 'R$'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label7: TLabel
            Left = 92
            Top = 70
            Width = 15
            Height = 13
            Caption = 'R$'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object EdtPrecoVendaInstaladoGDC: TEdit
            Left = 110
            Top = 17
            Width = 72
            Height = 19
            Color = 15663069
            Ctl3D = False
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 9
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
          end
          object EdtPrecoVendaFornecidoGDC: TEdit
            Left = 110
            Top = 44
            Width = 72
            Height = 19
            Color = 15663069
            Ctl3D = False
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 9
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
          end
          object EdtPrecoVendaRetiradoGDC: TEdit
            Left = 110
            Top = 70
            Width = 72
            Height = 19
            Color = 15663069
            Ctl3D = False
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 9
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
          end
        end
        object EdtGrupoPersianaGDC: TEdit
          Left = 106
          Top = 13
          Width = 17
          Height = 19
          Color = clSkyBlue
          Enabled = False
          MaxLength = 9
          TabOrder = 7
          Visible = False
        end
        object EdtCorGDC: TEdit
          Left = 106
          Top = 33
          Width = 17
          Height = 19
          Color = clSkyBlue
          Enabled = False
          MaxLength = 9
          TabOrder = 8
          Visible = False
          OnExit = EdtGrupoPersianaGDCReferenciaExit
          OnKeyDown = EdtGrupoPersianaGDCReferenciaKeyDown
        end
        object edtclassificacaofiscal_cor: TEdit
          Left = 126
          Top = 77
          Width = 120
          Height = 19
          TabOrder = 3
        end
        object edtPrecoCustoGDC: TEdit
          Left = 6
          Top = 176
          Width = 121
          Height = 19
          TabOrder = 9
        end
        object edtEstoqueMinimo: TEdit
          Left = 162
          Top = 133
          Width = 120
          Height = 20
          TabOrder = 10
        end
      end
      object DBGridPersianaGDC: TDBGrid
        Left = 0
        Top = 244
        Width = 906
        Height = 237
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clBlack
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = DBGridPersianaGDCDrawColumnCell
        OnDblClick = DBGridPersianaGDCDblClick
      end
      object pnl2: TPanel
        Left = 0
        Top = 204
        Width = 906
        Height = 40
        Align = alTop
        BevelOuter = bvNone
        Color = 14024703
        TabOrder = 2
        DesignSize = (
          906
          40)
        object btgravarGDC: TBitBtn
          Left = 800
          Top = 1
          Width = 39
          Height = 39
          Anchors = [akRight]
          TabOrder = 0
          OnClick = btGravarGDCClick
        end
        object btExcluirGDC: TBitBtn
          Left = 839
          Top = 1
          Width = 39
          Height = 39
          Anchors = [akRight]
          TabOrder = 1
          OnClick = BtExcluirGDCClick
        end
        object btCancelarGDC: TBitBtn
          Left = 878
          Top = 1
          Width = 39
          Height = 39
          Anchors = [akRight]
          TabOrder = 2
          OnClick = btCancelarGDCClick
        end
      end
    end
  end
  object Guia: TTabSet
    Left = 0
    Top = 63
    Width = 906
    Height = 22
    Align = alTop
    BackgroundColor = clWhite
    DitherBackground = False
    EndMargin = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentBackground = True
    StartMargin = -3
    SelectedColor = 15921906
    Tabs.Strings = (
      '&1 - Principal'
      '&2 - Impostos'
      '&3 - Classe')
    TabIndex = 0
    UnselectedColor = 13421772
    OnChange = GuiaChange
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 906
    Height = 63
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 2
    DesignSize = (
      906
      63)
    object lbnomeformulario: TLabel
      Left = 540
      Top = 3
      Width = 104
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Cadastro de'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigo: TLabel
      Left = 671
      Top = 9
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb1: TLabel
      Left = 540
      Top = 25
      Width = 84
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Persianas'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object btAjuda: TSpeedButton
      Left = 857
      Top = -13
      Width = 53
      Height = 80
      Cursor = crHandPoint
      Hint = 'Ajuda'
      Anchors = [akTop, akRight]
      Flat = True
      Glyph.Data = {
        4A0D0000424D4A0D0000000000003600000028000000240000001F0000000100
        180000000000140D0000D8030000D80300000000000000000000FEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFAF6FBF9F2FEFEF4FFFFFAFF
        FCFDFFF9FFFEFEFDFCFFFFFEFEFFFDFEFCFEFEF8FFFFFBFEFCFFFFFCFFFDF6FB
        FFF7F8FFF7FBFFFBFBFDF7FFFFFCFFFFFEF7FDFCFBFFFFFFFBFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFF9FD
        F8FDFFFFFDFBFFFFFBFFFFFDFEFFFCFCFFFEFFFEFDFFFCF9FBF0EFF1F0EBEDF9
        F1F2F6F8F9F1FFFFF4FFFFFEFCFFFFFDFFFFFAFFFBFCFFFEFDFFFFFAFDFAFCFD
        F7FFFFFFFCFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFFFCFFFFFCFEFFFDFCFFFFFFFFFDFFF2E4E6DEBCBCCF
        9F99EB8688E68081EA7E7DF68686FE9092FDA2A5F7C3C3FFF0EDFFFAFFF8FFFF
        FFFDFFFFF5FDFCFDFFF6FFFFFBF8FAFFFDFEFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFBFFFCFEFFFBFFFCE7
        E1DCC29E9EBF7579DC7677F07B76FF8783FF8F88FD9388FB8F84FF837BFF7977
        F77576F98084FBB6B9F2F2ECE8FFFAF8FDFCFFFEFFFAFCFDFFFCFEFCFEFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFEFDFFFAFCD7B8BBB17879C87772F08E88FE9893F58E8CE7797DD76F70
        D26867DC716EE88581F69A95F99591FE8686FF7173F28484FFD9D4FFFFFBFDF8
        F7FFFCFFFFFEFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFF9B99D9CA76B6CDA8786F99B96E17D79
        B6484C98222FAB1122AA0F1EA5131FA41D25AA2226C53938E6625BFF9085FD9B
        91FF7B7CF2777BF4C8C7F8FFFEF9FEFDFFFEFFFFFBFCFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFC0A0A1A7706D
        E6928CFC9691B3484A87192589112398182BA32E31DC6F71EB8082CC5050BB28
        26D32A22EB2F24FE3727F36D69FF9D97FB7D83F7707EFEC5CDFFFEFFFFFDFDED
        FFFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFCFBFDFFFDFFFFFBFFFAFFFFF2FAFA
        FFFEFEDCBDBEA66D6BDB9092ED909996303C770E1B8D1E2C9B1C2BA11425D96F
        75FDA8B0FFA2B0FFA0AAC83F43BC2F26D73927FF3929FF2B1CEC5650FF9996FE
        7D7AFD7F7EFADFDBFFFEFFF4FEFEFEFDFFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFF
        FDF9FEFFFDFFF7FAFEFAFFFFF4E6E7A67F81CF8B8CEC9A9F92323C7B121D9123
        2F921C27A5232EA71C27DE837EFFB7B5F9A9AEFDB0B3BF4343B42821C9372BDA
        3326E33626CB2728CA5556FF9893FB7777DE9A9BFFFEFEFFFCFDFEFDFFFEFDFF
        FEFDFFFEFDFFFEFEFEFEFDFFFFFBFFFFFBFFFAFBFFFFFFFFC3AEADA77678F8A4
        A9A9515776131B8D202898242BA3282CA72629A82627B55047EBADA2FFBCB5CD
        7974A92E2AA13026A5372BBD332DBF3528BD2B2DA51E26DF7372FF918FE27377
        F0CCCCFFFFFEFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFAF9FBFFFDFFFFFAFFFFFE
        FFF5F0EFA38384DB9FA0E78A91791D22852428992E309E2827A72C28AC3028AB
        3229A3271FA13E36B4413E9E332CB03129A73126A23127B0322EA93026AD3131
        A91925B63239F2948FFD7C7FE9989BFBFFF9FEFDFFFEFDFFFEFDFFFEFDFFFFFE
        FEFCFBFDFFFEFFFFFDFFFFFEFFDACECEAE8687F0ABAEB156597E24248B2C2992
        2A25A93730A93328AE3B2EA33222D35E59EA928BF28C87E27D74AC362BA93A2A
        A23729A5342AA3342CA62728B02931A71821D8716FFF8A8ADE7D7FEFECE7FEFD
        FFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFFFDFCFEFFFCFEFFFEFFC0B3B1BF9392F0
        A5A78F3B35842A2396352B9F372AA93C2EAC3D2DAC3C2AA1311FD16D69F8CABF
        FBC0B7FCABA3AA3A2EA23827A33928A9372AA6352BAE302BA72C2EAA1922C550
        51FF9692DC7475E0D2D3FFFEFFFEFDFFFEFDFFFEFDFFFFFDFEFEFDFFFEFDFFFF
        FDFFFFFEFEB6A9A7CB9D9CEC9E9F8335288E3527A0392AAD4130A83C2AA73B29
        A93927AE3827CE6C66FFCDC1FABAB5FFBFB6D46B62AD3126B43427B13A2BA933
        27A93327A02A25AC1F28AF373BFC9D94E7797BD1B8BCFFFEFFFFFEFFFEFDFFFE
        FDFFFFFDFEFCFCFCFDFCFEFFFEFFFDFBFBB5A8A6DBAEABE19192813522983C29
        A73B29AF3E2AA83B26AA3F2AAD3B2AAF3224B05A4EFFE0D0FFCEC6F7BEB5FFBE
        B3DE8C7BA44434A33628AD3C2CAD3726A8342DA7262DAE2E33FA9B92E27A7BD4
        ABB3FFFEFFFFFEFFFEFDFFFEFDFFFBFCFFF9FEFFFFFCFDFBFFFFFCF9FBC9AAAB
        DEB1ADDF9A91813628B03C2BAB3D25A7412AA6362AB3362EA13726AE3C25A833
        24D79987FFF4E4F9D3CEFEBEBEFFC9C3EC9E91AD4436B137299C3B27AA3526B1
        2F28A4302FF7969AE67F86C7AAA5FFFEFFFFFEFFFEFDFFFEFDFFFAFBFFFCFEFF
        FFFEFFFCFEFFFCFEFFCEB4B4E6B6B2E6A99F98392AA13426B2382C9E3E2EAA3E
        33AD4338A74539A24536AB443B9B3D38CF8A87FFEBE6FEDED9F1C0B8FFC9BDEC
        8B81AA372AA73B2AA83F2CA52B1FA6403BF9A1A1E57E86C9B3AEFFFEFFFFFEFF
        FEFDFFFEFDFFFAFDFFFDFCFEFFFEFFFDFCFEFAFFFFD3BEC0ECB5B2F5C2B8AB40
        329F3D31B84D49A24F47AD554EAE574DAE5D55B1655FAD5654B34E50A13B40B9
        7B7BFFE1DBFED7CFF9CAC2FFBCB5BD5146AD3627A3402AA83526AD544AFEACAB
        D5787FD4C7C5FFFEFFFEFDFFFEFDFFFEFDFFFBFFFFFFFBFDFFFDFDFFFDFFF9FE
        FFDFD1D2E7B1B0FFDACEC26B61A74C45A55C54A85A54D3958FF2B8B3F9C2BFF4
        B4B3AD5352AB5C59AC5553A54343F7C0BBFDE0D9FFCCCAFBC9C9CC6A60B03429
        9F3923A63624C9776BFFADACC67E84E4E0DFFEFDFFFEFDFFFEFDFFFEFDFFFAFE
        FFFFFCFFFCFCFCFFFEFFFBFEFFF4ECEDE2B7B4FFDBD2DAB0ABA6504AA86153AB
        514AD59594FFF8F7FEF1EFFAD4D2C469649C5148A85950A14B45ECBBB3FBE1DA
        FFCED0FFCFD6D06E66AB352AAD3B2A9F3726F7A89DFBA4A7BB8E91F2F7F6FEFD
        FFFEFDFFFEFDFFFEFDFFFCFBFDFFFEFFFCFEFEFEFDFFFDFCFFFFFEFFE2C7C3F3
        CAC1EDDEDCBD7672AD5144CE6459D7797AF6F5F1F4FFFEF9F3EEE9B4AAB16860
        B45E58C99087FAD7CDFFDCD6F8D5D2FECBCFBE5A55A23127A12F22CB7264FFC3
        BBD28B8ECFB7B9FAFFFFFEFDFFFEFDFFFEFDFFFEFDFFFEFBFDFFFFFFFCFEFEFB
        FDFEFFFAFFFFFEFFEBE5DEF2C4BDFFE5E8EFC8C0AE6053E17164F47773F0BBB8
        FDFEFAFDFFFFFFF8F1FCD6D4FFCDCCFFDDD8FFDED8FFE0DAFFDDD7E7A1A2A339
        32A7352EAC4A3EF8B8ADECB2ADB68E90FAECEEFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFDFFFCFCFCFAFFFEFAFDFFFFFBFFFFFDFDFBFFF9FFCFC9FFC7CCFFF9EF
        E8BFB0D66D5FFF7C70F9807EEACECDFFFBFFFDFCFFF9FEFFFDFFFFFFE4E5FFE2
        DFF6E7DEEDB6AFB34648A1312BB03E37FCAA9FF7CDC1BC8F8CD5C7C8FFFDFFFC
        FEFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFFFEFFFFFEFF
        FDFCFEFCFBFDFCC8C8F6D7D6FFFCF6F0C9C1E57F7AFF7673F98481ECA5A1E6D6
        D7FCF1F3F9F1F1ECD7D9F0C1C3D78A8DA84646A03232A54B4BECABAAFFD1CFC9
        A09EC9ADACFEF9F8FBFFFFFCFCFCFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFF
        FDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFDFCFEFFF4EEF8CDCAFDD8D4FFFBF4FDE6
        E4FBA1A6FB7375FD6F68EA6E5CCE6855B76958AE6458A5514B993E3AA04D4BC8
        7F7BFCC3C1FFCBCCDAA4A4BFAEABF3F9F4FFFFFEFFFCFDFCFEFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFEFDFFFFFEFFF4FF
        FFFBF2EFF6D6D1FDD9D3FFEFE8FCFBF1FFE6DCF6BBB2EE9285D07C70B76F65B2
        6F66C0807BDDA39EF3C1BBFFD6CFF6C6C5D4ACADCEBBB8F9FAF6F7FFFDFAFCFC
        FFFEFFFCF9FBFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFC
        FEFDFCFEFEFDFFFFFEFFFFFAFCFBFFFFF9F9F9FCE0DFFFD3CCFEDDD4F8ECE6F8
        F6F5F5F8F6F4EFEEF6E4E5FADBDCFFDBDCFFD9D8FBCAC8F0C0BCD7B5B6E1D8D5
        FAFFFEFFFFFFFEFCFCFAFFFFF8FDFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFFFFFBFFFBFCFDFDFFFD
        FBFFFFEEF1FFE2E1FFD3D2FFD0D2FFD2CFFFD1CDFFD2CFFFCFCFFFC9C9F2BFC3
        E9C4C6EED6D8F5F7F7FFFBFCFFFEFFFFFCFEFDFFFFF9FCFFFCFBFFFFFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFFFEFDFFFEFDFFFDFCFEFDFCFEFD
        FCFEFFFBFFF0FDFBF1FFFBFFFEFFFFFDFFF9FEFFF9FCFAFEF2EEF9EAE1F4E5DC
        F3E3DCF3E3DDF6E7E5FCF1F3FEFAFFF9FCFFF4FEFEFFFEFFFFF7FAFFFDFFFCF9
        FBFFFDFFFFFCFFF5F8FCFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFF7F9FFFDFEFFFFFBF8FFFDF9FDFBFAF1FBFB
        F6FFFFFBFDFEFBFEFFFFFCFFFFFDFFFFFEFFFBFFFFF8FFFFFDFEFFFFFAFFFFFD
        FFFAFEFFF8FFFFF9FBFCFFFDFFFFF9FCFBFAFCFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFCFFFEFF
        F7FDFFF5FEFFFBFFFFFFFFFEFFFCFDFFFCFFFFFAFFFFFCFFF9FEFDF9FFFDFBFF
        FEF9FCFAFBFBFBFFFFFFFFFCFEFEFDFFFBFAFCFBFFFFF5FDFDF7FFFFFDFFFFFF
        FCFFFEFDFFFEFDFFFEFDFFFEFDFF}
      OnClick = btAjudaClick
    end
    object btrelatorio: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btRelatorioClick
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btPesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btExcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btCancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btSalvarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btAlterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btNovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btSairClick
      Spacing = 0
    end
  end
end
