unit UCalculaformula_Perfilado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RDFormula, StdCtrls, Buttons;

type
  TFcalculaformula_Perfilado = class(TForm)
    lblargura: TListBox;
    lbaltura: TListBox;
    lbvariavel: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    btCalcular: TButton;
    edtlargura: TEdit;
    edtaltura: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    edtvariavel: TEdit;
    edtformulalargura: TEdit;
    edtformulaaltura: TEdit;
    btinsereformula: TBitBtn;
    BTLIMPARTUDO: TBitBtn;
    LBVARIAVEL2: TListBox;
    LBLARGURA2: TListBox;
    LBALTURA2: TListBox;
    LbAreaTotal: TLabel;
    RDFormula: TRDFormula;
    Label6: TLabel;
    edtlarguralateral: TEdit;
    edtalturalateral: TEdit;
    Label7: TLabel;
    procedure btCalcularClick(Sender: TObject);
    procedure btinsereformulaClick(Sender: TObject);
    procedure BTLIMPARTUDOClick(Sender: TObject);
  private
    { Private declarations }
    Function VerificaOperadores_valores(PFormula:String):Boolean;
    Function Substitui_variaveis_valor(Pformula:String;Pvariavel,Pvalor:String):String;
    Function ApagaEspaco(Pformula:String):string;
    Procedure Substitui_Formula_por_Valor_Largura(Pvariavel,Pvalor:String);
    Procedure Substitui_Formula_por_Valor_altura(Pvariavel,Pvalor:String);
    Function VerificaListBoxVazio:Boolean;
    function VerificaSeAindaTemVariaveis: Boolean;

  public
    { Public declarations }
     QuantidadeExecucaoRecursiva:Integer;
     Function Calcula_Area:boolean;
     Function ResgataValorMetroQuadrado(var Pvalor:Currency):Boolean;
     function Calculaesquadria(Expressaoaltura:string;EspressaoLargura:string;ExpressaoQuantidade:string;projeto:string):String;

  end;

var
  Fcalculaformula_Perfilado: TFcalculaformula_Perfilado;

implementation

uses UCalculaformula_Vidro, UessencialGlobal;

{$R *.dfm}

function TFcalculaformula_Perfilado.ApagaEspaco(Pformula: String): string;
var
cont:integer;
ptemp:String;
begin
     Result:='';
     ptemp:='';

     for cont:=1 to length(pformula) do
     Begin
          if (pformula[cont]<>' ')
          Then Ptemp:=ptemp+pformula[cont];
     End;
     result:=ptemp;
end;

procedure TFcalculaformula_Perfilado.btCalcularClick(Sender: TObject);
begin
     Self.Calcula_Area;

end;

function TFcalculaformula_Perfilado.Calcula_Area: boolean;
var
cont:integer;
begin
     if (VerificaListBoxVazio = true)then
     exit;

     result:=False;

     //Antes de Tudo Substituo o valor da largura e da altura
     //nas formulas abaixo

     Substitui_Formula_por_Valor_Largura('L',edtlargura.Text);
     Substitui_Formula_por_Valor_Largura('A',edtaltura.Text);

     Substitui_Formula_por_Valor_Altura('L',edtlargura.Text);
     Substitui_Formula_por_Valor_Altura('A',edtaltura.Text);

     Substitui_Formula_por_Valor_Largura('L2',edtlarguralateral.Text);
     Substitui_Formula_por_Valor_Largura('A2',edtalturalateral.Text);

     Substitui_Formula_por_Valor_Altura('L2',edtlarguralateral.Text);
     Substitui_Formula_por_Valor_Altura('A2',edtalturalateral.Text);


     //Substituindo o Valor dos Componentes que foram Calculados
     //na FCalculaFormula_Vidro

     For cont:=0 to Fcalculaformula_Vidro.lbvariavel.Items.count-1 do
     Begin
          Substitui_Formula_por_Valor_Largura(Fcalculaformula_Vidro.lbvariavel.Items[cont],Fcalculaformula_Vidro.lblargura.Items[cont]);
          Substitui_Formula_por_Valor_Altura(Fcalculaformula_Vidro.lbvariavel.Items[cont],Fcalculaformula_Vidro.lbaltura.Items[cont]);
     End;

     //percorrendo todos os items
     for cont:=0 to lblargura.Items.count-1 do
     Begin

          if (Self.VerificaOperadores_valores(lblargura.Items[cont])=True)
          Then Begin//soh tem operadores e valores
                    //calcula com o rdformula

                    RDFormula.Expressao:=virgulaparaponto(lblargura.Items[cont]);
                    RDFormula.Execute;
                    if (RDFormula.Falhou=True)
                    Then Begin
                              Messagedlg('N�o � poss�vel calcular a f�rmula '+lblargura.Items[cont]+#13+'Express�o Inv�lida',mterror,[mbok],0);
                              exit;
                    End;
                    lblargura.Items[cont]:=floattostr(RDFormula.Resultado);
                    Self.Substitui_Formula_por_Valor_Largura(lbvariavel.Items[cont],lblargura.Items[cont]);
          End;


          if (Self.VerificaOperadores_valores(lbaltura.Items[cont])=True)
          Then Begin//soh tem operadores e valores
                    //calcula com o rdformula

                    RDFormula.Expressao:=virgulaparaponto(lbaltura.Items[cont]);
                    RDFormula.Execute;
                    if (RDFormula.Falhou=True)
                    Then Begin
                              Messagedlg('N�o � poss�vel calcular a f�rmula '+lbaltura.Items[cont]+#13+'Express�o Inv�lida',mterror,[mbok],0);
                              exit;
                    End;
                    lbaltura.Items[cont]:=floattostr(RDFormula.Resultado);
                    Self.Substitui_Formula_por_Valor_altura(lbvariavel.Items[cont],lbaltura.Items[cont]);
          End;
     End;


     if (QuantidadeExecucaoRecursiva >= 10)then
     Begin
          // S� posso chamar a recursivida no maximo 10 vezes, porque se naum resolveu
          // passnado 10 vezes pelo mesmo lugar, nao adianta ficar insistindo, portanto a
          // fornmula t� errada.
          MensagemErro('Erro na f�rmula.');
          exit;
     end;

     //Recursivo
     if (Self.VerificaSeAindaTemVariaveis=true)then
     Begin
          Self.Calcula_Area;
     end;

     Result:=True;
end;



procedure TFcalculaformula_Perfilado.Substitui_Formula_por_Valor_altura(Pvariavel,
  Pvalor: String);
var
cont:integer;
begin
     for cont:=0 to lbaltura.Items.count-1 do
     Begin
          lbaltura.Items[cont]:=Self.Substitui_variaveis_valor(lbaltura.Items[cont],Pvariavel,Pvalor);
     End;
end;

procedure TFcalculaformula_Perfilado.Substitui_Formula_por_Valor_Largura(Pvariavel,
  Pvalor: String);
var
cont:integer;
begin
     for cont:=0 to lblargura.Items.count-1 do
     Begin
          lblargura.Items[cont]:=Self.Substitui_variaveis_valor(lblargura.Items[cont],Pvariavel,Pvalor);
     End;
end;

function TFcalculaformula_Perfilado.Substitui_variaveis_valor(Pformula: String;
  Pvariavel, Pvalor: String): String;
var
cont:integer;
ptemp:String;
pformulafinal:String;
Begin
     result:='';
     pformulafinal:='';
     //tenho uma string pra trabalhar
     //primeiro preciso localizar a substring dentro da string, se for ela preciso
     //substituir
     pvariavel:=uppercase(pvariavel);
     pformula:=Self.apagaespaco(uppercase(pformula));
     ptemp:='';


     //2+3+BD*3
     for cont:=1 to  length(pformula) do
     Begin
          if (pformula[cont] in ['*','/','+','-','(',')'])
          Then BEgin
                    if (ptemp=pvariavel)//encontrou
                    Then Begin
                              //substitui
                               pformulafinal:=copy(pformulafinal,1,length(pformulafinal)-length(pvariavel));
                               pformulafinal:=pformulafinal+pvalor;
                    End;
                    pformulafinal:=pformulafinal+pformula[cont];
                    ptemp:='';
          End
          Else BEgin
                    pformulafinal:=pformulafinal+pformula[cont];
                    ptemp:=ptemp+pformula[cont];
          End;
     End;

     if (ptemp=pvariavel)//encontrou
     Then Begin
               //substitui
               pformulafinal:=copy(pformulafinal,1,length(pformulafinal)-length(pvariavel));
               pformulafinal:=pformulafinal+pvalor;
     End;
     result:=pformulafinal;
end;


function TFcalculaformula_Perfilado.VerificaOperadores_valores(PFormula: String): Boolean;
var
cont:integer;
begin
     result:=False;
     Pformula:=ApagaEspaco(pformula);
     for cont:=1 to length(pformula) do
     Begin
          if not(pformula[cont] in ['0'..'9','.','(',')','+','-','*','/',','])
          Then exit;//tem outro alem de numeros e operadores
     End;
     result:=True;
end;

procedure TFcalculaformula_Perfilado.btinsereformulaClick(Sender: TObject);
begin
     lbvariavel.Items.ADD(EDTVARIAVEL.TEXT);
     lblargura.Items.add(edtformulalargura.Text);
     lbaltura.items.add(edtformulaaltura.Text);

     lbvariavel2.Items.ADD(EDTVARIAVEL.TEXT);
     lblargura2.Items.add(edtformulalargura.Text);
     lbaltura2.items.add(edtformulaaltura.Text);
     //edtvariavel.setfocus;
end;

procedure TFcalculaformula_Perfilado.BTLIMPARTUDOClick(Sender: TObject);
begin
     lbvariavel.Items.clear;
     lbaltura.Items.clear;
     lblargura.Items.clear;

     lbvariavel2.Items.clear;
     lbaltura2.Items.clear;
     lblargura2.Items.clear;

end;

function TFcalculaformula_Perfilado.ResgataValorMetroQuadrado(var Pvalor:Currency):Boolean;

Var Linha : Integer;
    ValorTotal:Currency;
begin
     Result:=false;

     ValorTotal:=0;
     pvalor:=0;
     // Multiplica as linhas do listBox e no final
     // Soma as multiplicacoes
     for Linha:=0 to Self.lbaltura.Items.Count-1 do
     Begin
         if (Self.lbaltura.Items[Linha]<>'') and (Self.lblargura.Items[Linha]<>'')
         then BEgin
                  Try
                      ValorTotal:=ValorTotal+(StrToCurr(Self.lbaltura.Items[Linha]) *  StrToCurr(Self.lblargura.Items[Linha]));
                  Except
                     Messagedlg('Erro na resolu��o da F�rmula',mterror,[mbok],0);
                     exit;
                  End;
         End;
     end;
     Pvalor:=ValorTotal;
     Result:=true;
end;

function TFcalculaformula_Perfilado.VerificaListBoxVazio: Boolean;
begin
   // Se nuam tiver formul na prinmeira linha
   // nem tenta calcular
   try
       if (Self.lblargura.Items[Self.lblargura.Items.Count-1]='')then
       Result:=true
       else
       Result:=false;
   except
       Result:=false;
   end;
end;

function TFcalculaformula_Perfilado.VerificaSeAindaTemVariaveis: Boolean;
Var   Cont, Aux : Integer;
      Linha : String;
begin
     Result:=false;
     for Cont:=0 to lbaltura.Items.Count-1 do
     Begin
          // Altura
          Linha:='';
          Linha:=lbaltura.Items[cont];
          for Aux:=1 to (Length(Linha))do
          Begin
                if not(Linha[Aux] in ['0'..'9',',','.'])Then
                Begin
                      Result:=true;
                      inc(QuantidadeExecucaoRecursiva,1);
                      exit;//tem outro alem de numeros
                end;
          end;

          //Largura
          Linha:='';
          Linha:=lbLargura.Items[cont];
          for Aux:=1 to (Length(Linha))do
          Begin
                if not(Linha[Aux] in ['0'..'9',',','.'])Then
                Begin
                      Result:=true;
                      inc(QuantidadeExecucaoRecursiva,1);
                      exit;//tem outro alem de numeros
                end;
          end;

     end;
end;

function TFcalculaformula_Perfilado.Calculaesquadria(Expressaoaltura:string;EspressaoLargura:string;ExpressaoQuantidade:string;projeto:string):String;
begin
  RDFormula.Expressao:='100*100';
  RDFormula.Execute;
  if (RDFormula.Falhou=True)
  Then Begin
     Messagedlg('N�o � poss�vel calcular a f�rmula',mterror,[mbok],0);
     exit;
  End;
  ShowMessage(floattostr(RDFormula.Resultado));

end;

end.
