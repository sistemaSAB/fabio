object FRImposto_ICMS: TFRImposto_ICMS
  Left = 0
  Top = 0
  Width = 822
  Height = 463
  TabOrder = 0
  object bvl1: TBevel
    Left = 640
    Top = 64
    Width = 50
    Height = 42
  end
  object RICH_COPIA: TRichEdit
    Left = 448
    Top = 40
    Width = 185
    Height = 89
    Lines.Strings = (
      'RICH_COPIA')
    TabOrder = 4
    Visible = False
    WordWrap = False
  end
  object Panel_TOP: TPanel
    Left = 0
    Top = 0
    Width = 822
    Height = 41
    Align = alTop
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    DesignSize = (
      822
      41)
    object Label17: TLabel
      Left = 208
      Top = 3
      Width = 33
      Height = 13
      Caption = 'Estado'
    end
    object Label18: TLabel
      Left = 280
      Top = 3
      Width = 71
      Height = 13
      Caption = 'Tipo de Cliente'
    end
    object lbnometipocliente: TLabel
      Left = 342
      Top = 22
      Width = 71
      Height = 13
      Caption = 'Tipo de Cliente'
    end
    object lbcodigo_material_icms: TLabel
      Left = 13
      Top = 21
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 8
      Top = 3
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label33: TLabel
      Left = 88
      Top = 3
      Width = 47
      Height = 13
      Caption = 'Opera'#231#227'o'
    end
    object btReplicarImpostos: TSpeedButton
      Left = 773
      Top = 0
      Width = 49
      Height = 41
      Cursor = crHandPoint
      Hint = 'Replicar Pedidos'
      HelpType = htKeyword
      AllowAllUp = True
      Anchors = [akTop, akRight]
      Flat = True
      Glyph.Data = {
        36110000424D361100000000000036000000280000002D000000200000000100
        1800000000000011000000000000000000000000000000000000A2663EA2663E
        A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA266
        3EA2663EA2663EA46740B1774BB7764AA46239A2663EA2663EA2663EA2663EA2
        663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E
        A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA266
        3E00A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E
        A2663EA26740B1774DC28A5FD4A071E0AE81E6B689ECBF95D798699D5735A266
        3EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2
        663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E
        A2663EA2663EA2663E00A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2
        663FAF754DBE885FD09F75E0B188EFC096EFBF98EAB995E6B390E3AF8DE7B794
        E6B083C57C50C6875AAA643BA2663EA2663EA2663EA2663EA2663EA2663EA266
        3EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2
        663EA2663EA2663EA2663EA2663EA2663E00A2663EA2663EA2663EA0643DA971
        4BBB8660D09D75E2B288EBBB92EFBF98EBB995E7B490E2AE8CE3B08DE8B692ED
        BC96F0BE9AF3C5A2E7B285D18862DDA681C57B51A4643BA15E36A2663EA2663E
        A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA266
        3EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E00A3663DB9815A
        CB9874DEB08AE8B995EFC19BEBBA95E7B490E4B08DE5B18EE8B691EDBC97F0BF
        9BF2C19DF2C19EF1BF9CEFBD9BF2C2A2E6B186D08761DEA682D48C60DDA37ACF
        8B5FA25D37A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E
        A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA266
        3E00D3A787F5C9A5EEBF9AE8B592E5B18EE5B18EE8B693ECBB98F1C09CF3C29E
        F2C29EF1C09EF0BF9CF0BF9BF0BF9CF1C09DF2C19DF5C7A5E8B38AD68E69E3AE
        8CD38B60D79B7BDDA178AD62399F643DA1643DA2663EA2663EA2663EA2663EA2
        663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E
        A2663EA2663EA2663E00E5BEA2E7B28CE2AD89ECBA97F2C19EF3C29FF3C39FF2
        C19EF0BF9DF1BF9CF1BF9DF1C19EF2C19EF3C2A0F3C3A0F4C2A0F4C29EF4C7A6
        E8B58CD48C67E3AF8DD18A60D59875DCA077AE643AA1663EA0613AA1643CA266
        3EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2
        663EA2663EA2663EA2663EA2663EA2663E00E8BD9FE4B28CE7B794F5C6A4F1C1
        9DF1BF9CF1C19CF2C19FF3C2A0F4C49FF4C3A0F4C3A1F4C3A0F4C3A0F4C3A0F4
        C3A0F4C39FF6C8A7E7B58DD48D67E5B290D38D63D59977DDA27AAD6339A2663E
        A1633CA1633CA2653DA2663EA2663EA2663EA2663EA2663EA2663EA2663EA266
        3EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E00E7BFA0E4B28C
        EABA97F4C3A0F1BF9CF4C2A0F5C5A2F5C4A2F5C4A1F3C3A0F4C4A1F4C4A0F4C4
        A0F4C4A1F4C4A1F4C4A1F4C3A1F6C8A7E9B890D68F69E8B392D18C62D49977DD
        A37BAD6339A2663EA2643DA2653DA2663EA2663EA2663EA2663EA2663EA2663E
        A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA266
        3E00E8BFA2E3B18BEBBA98F3C4A1F1C09DF5C5A2F4C4A1F5C4A1F5C4A1F5C5A2
        F5C5A2F4C5A2F4C4A2F4C5A2F4C4A2F4C4A2F4C4A1F6C8A8E9B891D7906AE9B5
        94D28F65D59A78DFA67FAD643AA2663EA2663EA2663EA2663EA2663EA2663EA2
        663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E
        A2663EA2663EA2663E00E8C1A2E4B18BECBB99F4C4A1F2C09DF5C5A2F5C4A1F5
        C5A2F5C5A2F5C5A2F5C5A2F5C5A2F5C5A2F5C5A2F5C5A2F5C5A2F5C5A2F7C9A8
        EABA94D8926CEAB696D59168D79B79DFA781AB6339A2663EA2663EA2663EA266
        3EA2663EA2663EA2663EA2663EA2663EFFFFFFFFFFFFFFFFFFFFFFFFA2663EA2
        663EA2663EA2663EA2663EA2663EA2663E00E8C1A2E3B18BECBC99F4C4A1F2C1
        9DF5C6A3F6C5A2F6C5A2F6C5A2F6C4A2F6C5A2F5C4A2F5C4A2F6C4A2F5C4A2F5
        C4A2F5C4A1F6C9A8EBBB95D9956EEAB997D4926AD79C79DFA984AD663BA2663E
        A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EFFFFFFFFFFFFFFFF
        FFFFFFFFA2663EA2663EA2663EA2663EA2663EA2663EA2663E00E9C1A2E3B08A
        EEBD9BF4C4A2F2C29EF5C6A3F5C5A3F5C5A2F5C5A3F5C5A3F5C5A2F5C5A2F4C5
        A2F4C5A2F4C5A2F4C5A2F4C5A2F7C9A7EBBC95D99670EBBB99D5956DD79C7AE2
        AB86AD663BA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E
        FFFFFFFFFFFFFFFFFFFFFFFFA2663EA2663EA2663EA2663EA2663EA2663EA266
        3E00E9C1A1E2B08AEFBE9CF5C5A3F3C39FF6C7A4F5C5A3F5C5A3F5C5A3F5C5A3
        F5C5A3F5C5A3F5C5A3F5C5A3F5C5A3F5C5A3F4C5A3F7C9A9EBBD96DA9871ECBB
        9BD7976ED79D7AE3AE88AD663CA2663EA2663EA2663EA2663EA2663EA2663EA2
        663EA2663EA2663EFFFFFFFFFFFFFFFFFFFFFFFFA2663EA2663EA2663EA2663E
        A2663EA2663EA2663E00E9C0A1E2B08AEFBE9CF4C5A2F4C3A0F6C7A5F6C6A3F6
        C6A4F7C6A3F7C6A4F6C6A4F6C6A4F6C6A4F6C6A4F6C6A3F6C6A3F6C6A3F7CAA9
        ECBE97DC9A74EDBE9DD89971D89E7BE3B08AAE683DA2663EA2663EA2663EA266
        3EA2663EA2663EA2663EA2663EA2663EFFFFFFFFFFFFFFFFFFFFFFFFA2663EA2
        663EA2663EA2663EA2663EA2663EA2663E00E9BFA0E3B18BEEBE9CF5C6A3F3C3
        9FF6C8A5F6C6A4F6C6A4F7C7A5F6C7A4F7C7A4F7C7A4F7C7A4F7C7A4F6C7A5F6
        C7A5F6C6A4F7CBAAECBF98DD9A75EEC09FD99A71D99F7DE4B18CAF683EA2663E
        A2663EA2663EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2663EA2663E00EAC5A8E1AE86
        EEBE9DF4C5A3F3C3A0F7C8A5F6C7A4F6C7A5F7C7A5F6C7A5F6C7A5F7C7A5F7C8
        A5F7C7A5F7C8A5F6C7A5F6C7A4F8CCABECBF98DF9C76F0C1A2DA9C73DA9F7DE6
        B38DAF693FA2663EA2663EA2663EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2663EA266
        3E00F0D9C6E4B491EEBC99F4C6A3F3C3A0F7C8A6F6C8A5F6C7A5F7C7A5F8C8A5
        F8C7A5F8C7A5F7C8A5F7C8A5F7C8A5F7C8A5F7C7A5F9CDACECBF98E09E79F2C4
        A3DA9D73DAA07EE6B48EB06B40A2663EA2663EA2663EFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFA2663EA2663E00F1DBCBEED1BBF1C5A5F5C5A2F4C3A0F7C9A6F7C8A5F8
        C8A6F8C8A6F8C8A6F8C9A6F8C8A6F8C9A6F7C9A6F7C9A7F7C8A7F7C8A6F8CEAC
        ECC098E1A07BF2C5A4DB9E75DAA17FE7B590B06B40A2663EA2663EA2663EFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFA2663EA2663E00F0D8C6F0D6C1F9DDC9F8D2B6F4C3
        A1F7C7A3F7C6A4F7C8A6F8C9A6F8C9A6F8C9A6F8C9A6F8C9A7F7C9A7F8C9A7F8
        C9A7F7C9A6F9CCACEBBE96E1A27CF3C7A6DCA075DCA380E8B691B06B40A2663E
        A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EFFFFFFFFFFFFFFFF
        FFFFFFFFA2663EA2663EA2663EA2663EA2663EA2663EA2663E00EFD7C3EFD2BA
        F8DBC6FDE3CFFADAC2F9D2B4F7C9A7F6C7A4F6C7A4F6C8A4F6C8A4F6C8A5F7C8
        A5F7C8A5F6C8A6F6C9A6F6CAA7FAD2B4ECC39FE2A27DF3C7A6DCA076DCA483E8
        B892B06B40A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E
        FFFFFFFFFFFFFFFFFFFFFFFFA2663EA2663EA2663EA2663EA2663EA2663EA266
        3E00EFD5C1EECEB7F7D8C4FBDEC8FADDC8FCE1CCFCDDC5F9D8BCF8D2B5F8D0B2
        F7CFB1F8CFB0F7D0B1F8D1B3F8D3B4F9D4B7FAD6B9FCDCC2ECC7A4E2A47FF4C8
        A8DCA177DDA584E8B993B06C41A2663EA2663EA2663EA2663EA2663EA2663EA2
        663EA2663EA2663EFFFFFFFFFFFFFFFFFFFFFFFFA2663EA2663EA2663EA2663E
        A2663EA2663EA2663E00EFD3BEEDCBB2F7D7C1FBDEC6FADCC4FBDEC6FBDDC5FC
        DEC5FBDCC4FBDAC1FBD9C0F9D7BDF9D7BCF9D7BBF9D6BBF9D5B9F9D4B8FBD9BD
        EBC4A0E4A985F5CCACDDA279DEA785E9BA93AF6B40A2663EA2663EA2663EA266
        3EA2663EA2663EA2663EA2663EA2663EFFFFFFFFFFFFFFFFFFFFFFFFA2663EA2
        663EA2663EA2663EA2663EA2663EA2663E00EED2BCECC9AFF6D6BDFADBC4FADA
        C2FBDCC4FBDBC2FBDBC2FBD9C0F9D9BEFBD8BEF9D7BBF9D5BAF9D5B9F9D5B8F9
        D4B7F9D2B4FBD9BCEBC39EE5AC89F5CDADDCA37AE0AA8AE9BA95AF6C41A2663E
        A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EFFFFFFFFFFFFFFFF
        FFFFFFFFA2663EA2663EA2663EA2663EA2663EA2663EA2663E00EED1BAEAC7AC
        F5D4BCF9DAC1F9D8C0FBDBC2FADBC0FADAC0FAD8BEFAD7BDFAD8BCFAD6BBF9D5
        BAF8D4B8F8D3B7F8D3B6F9D2B3FBD7BBEAC19CE4AD89F4CDAEDCA57CE1AF90EA
        BC97AF6C41A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E
        A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA266
        3E00EECFB8E9C5AAF4D3BAF8D8C0F8D7BEFAD9C0FAD9BEFAD9BEF9D7BCF9D6BB
        F9D6BBF9D5B9F8D5B8F8D4B7F8D3B6F8D2B4F7D0B1FAD6B9E7BF9AE3AB88F5D0
        B1DCA77FE1AF91E9BC97AE6C41A2663EA2663EA2663EA2663EA2663EA2663EA2
        663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E
        A2663EA2663EA2663E00EDCEB6E9C3A8F3D2B8F8D7BFF8D7BCFAD8BFF9D8BCF8
        D8BCF8D5BAF8D4B8F8D2B7F8D1B4F7D1B2F6CFB0F6CFB0F7D0B1F7D0B3FADCC0
        E3B68FECBD9BF8D7B9DAA780E0AE8FE7BB96AD6B40A2663EA2663EA2663EA266
        3EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2
        663EA2663EA2663EA2663EA2663EA2663E00EDCEB5E8C3A7F3D1B8F7D7BDF6D4
        BAF8D7BBF8D4B9F7D5B9F6D4B9F6D5BAF7D5BBF8D9BEF9DAC1FADAC1FADCC3F9
        DAC2F6D5BAEEC3A0EBBB97F4CAABF7DABED8A37BE2B394E8BE9AAD6A40A2663E
        A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA266
        3EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E00F1D2BBE8C4A8
        F4D4BCF8D9C2F7DAC2F9DDC6FADFC7FBDFC8FBDEC7FADEC5FADBC3F8D8BEF7D5
        BAF6D3B9F5D1B6F4D0B3F4D1B4F7D4BAFADAC1F7D7BBEAC19DDCA781E5BB9CE8
        C19FA9673CA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E
        A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA266
        3E00ECCBB0F0D1B6F8D9C1F8D9BFF5D4BBFADCC3F8D8C0F7D7BFF7D7BEF8D8BF
        F8D9BFF9DAC1F8DAC0F7D7BFF6D6BCF5D3B9F2CEB3EEC7ABEAC1A4E6BB9CE4B8
        97EBC6A8F3D5B8DFB38DA36139A2663EA2663EA2663EA2663EA2663EA2663EA2
        663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E
        A2663EA2663EA2663E00A96F47AC734AD4A382FDE2CDF7D7BFFBDCC4F7D8BFF7
        D7BFF6D6BDF4D3BAF3D0B5F2CEB4F1CEB4F1CFB4F1CEB2F2CFB5F2D2B8F2D2B8
        F0CEB4ECC9AFE8C4ABE0B99BCD9973A96A40A2663EA2663EA2663EA2663EA266
        3EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2
        663EA2663EA2663EA2663EA2663EA2663E00A2663EA2663EA56B43BC8966C38D
        68F2D0B4F2D2B8F0CDB2EFCCB1EECCB1E9C6ABE6C1A5E1BA9EDAB193D2A484C8
        9674BE8966B7805BB0764FA86B43A1663EA2663EA2663EA2663EA2663EA2663E
        A2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA266
        3EA2663EA2663EA2663EA2663EA2663EA2663EA2663EA2663E00}
      ParentShowHint = False
      ShowHint = True
      OnClick = Button1Click
    end
    object edtestado_material_icms: TEdit
      Left = 208
      Top = 17
      Width = 57
      Height = 19
      CharCase = ecUpperCase
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 1
      Text = 'EDTESTADO_MATERIAL_ICMS'
    end
    object edttipocliente_material_icms: TEdit
      Left = 280
      Top = 17
      Width = 57
      Height = 19
      Color = 6073854
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 2
      OnDblClick = edttipocliente_material_icmsDblClick
      OnExit = edttipocliente_material_icmsExit
      OnKeyDown = edttipocliente_material_icmsKeyDown
    end
    object edtoperacao: TEdit
      Left = 88
      Top = 17
      Width = 57
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 0
      Text = 'edtoperacao'
      OnExit = edtoperacaoExit
      OnKeyDown = edtoperacaoKeyDown
    end
    object Button1: TButton
      Left = 544
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Replicar'
      TabOrder = 3
      Visible = False
      OnClick = Button1Click
    end
  end
  object STRG_Imposto: TStringGrid
    Left = 0
    Top = 314
    Width = 822
    Height = 149
    Align = alClient
    Ctl3D = False
    DefaultRowHeight = 19
    FixedCols = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = 4473924
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    Options = [goFixedHorzLine, goHorzLine, goRangeSelect]
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnDblClick = STRG_ImpostoDblClick
    ColWidths = (
      64
      64
      64
      64
      64)
  end
  object Aba_Impostos: TPageControl
    Left = 0
    Top = 41
    Width = 822
    Height = 245
    ActivePage = TabSheet1
    Align = alTop
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Style = tsButtons
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = '&6 - ICMS'
      object LbMODALIDADE: TLabel
        Left = 0
        Top = 63
        Width = 54
        Height = 14
        Caption = 'Modalidade'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbPERC_REDUCAO_BC: TLabel
        Left = 243
        Top = 61
        Width = 82
        Height = 14
        Caption = '% de Red. da BC'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbALIQUOTA: TLabel
        Left = 390
        Top = 61
        Width = 51
        Height = 14
        Caption = '% Al'#237'quota'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbIVA: TLabel
        Left = 538
        Top = 61
        Width = 33
        Height = 14
        Caption = ' % IVA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbPAUTA: TLabel
        Left = 664
        Top = 61
        Width = 51
        Height = 14
        Caption = 'Pauta (R$)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbMODALIDADE_ST: TLabel
        Left = 0
        Top = 86
        Width = 66
        Height = 14
        Caption = 'Modalide (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbPERC_REDUCAO_BC_ST: TLabel
        Left = 243
        Top = 86
        Width = 76
        Height = 14
        Caption = '% Red. BC (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbALIQUOTA_ST: TLabel
        Left = 390
        Top = 86
        Width = 75
        Height = 14
        Caption = '% Al'#237'quota (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbIVA_ST: TLabel
        Left = 538
        Top = 84
        Width = 56
        Height = 14
        Caption = '% IVA  (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbPAUTA_ST: TLabel
        Left = 664
        Top = 84
        Width = 67
        Height = 14
        Caption = 'Pauta R$ (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 473
        Top = 29
        Width = 113
        Height = 14
        Caption = 'CFOP Dentro do Estado'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object lbnomeCFOP_ICMS: TLabel
        Left = 667
        Top = 30
        Width = 143
        Height = 11
        AutoSize = False
        Caption = 'CFOP'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 0
        Top = 25
        Width = 90
        Height = 14
        Caption = 'Situa'#231#227'o Tribut'#225'ria'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeSTA_ICMS: TLabel
        Left = 165
        Top = 28
        Width = 80
        Height = 13
        AutoSize = False
        Caption = 'TAB A'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeSTB_ICMS: TLabel
        Left = 254
        Top = 28
        Width = 153
        Height = 13
        AutoSize = False
        Caption = 'TAB B'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 0
        Top = 120
        Width = 179
        Height = 14
        Caption = 'F'#243'rmula da Base de  C'#225'lculo do ICMS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 0
        Top = 169
        Width = 195
        Height = 14
        Caption = 'F'#243'rmula da Base de  C'#225'lculo do ICMS ST'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label19: TLabel
        Left = 0
        Top = 6
        Width = 82
        Height = 14
        Caption = 'Modelo (Imposto)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
      end
      object Label26: TLabel
        Left = 0
        Top = 145
        Width = 121
        Height = 14
        Caption = 'F'#243'rmula Valor do Imposto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label27: TLabel
        Left = 0
        Top = 194
        Width = 145
        Height = 14
        Caption = 'F'#243'rmula Valor do Imposto (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object lb1: TLabel
        Left = 473
        Top = 6
        Width = 103
        Height = 14
        Caption = 'CFOP Fora do Estado'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object lbCfopForaEstado: TLabel
        Left = 667
        Top = 8
        Width = 27
        Height = 14
        Caption = 'CFOP'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label34: TLabel
        Left = 286
        Top = 9
        Width = 36
        Height = 14
        Hint = 'C'#243'digo de Situa'#231#227'o da Opera'#231#227'o no Simples Nacional'
        Caption = 'CSOSN'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
      end
      object lb2: TLabel
        Left = 166
        Top = 9
        Width = 46
        Height = 14
        Hint = 'C'#243'digo de Situa'#231#227'o da Opera'#231#227'o no Simples Nacional'
        Caption = 'Perc. Trib'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
      end
      object EdtPERC_REDUCAO_BC_ICMS: TEdit
        Left = 330
        Top = 56
        Width = 60
        Height = 20
        Hint = 'F'#243'rmula: PERC_REDUCAO_BC_ICMS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 7
      end
      object EdtALIQUOTA_ICMS: TEdit
        Left = 473
        Top = 56
        Width = 60
        Height = 20
        Hint = 'F'#243'rmula: ALIQUOTA_ICMS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 8
      end
      object EdtIVA_ICMS: TEdit
        Left = 599
        Top = 56
        Width = 60
        Height = 20
        Hint = 'F'#243'rmula: IVA_ICMS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 9
      end
      object EdtPAUTA_ICMS: TEdit
        Left = 742
        Top = 56
        Width = 60
        Height = 20
        Hint = 'F'#243'rmula: PAUTA_ICMS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 10
      end
      object EdtPERC_REDUCAO_BC_ST_ICMS: TEdit
        Left = 330
        Top = 83
        Width = 60
        Height = 20
        Hint = 'F'#243'rmula: PERC_REDUCAO_BC_ST_ICMS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 12
      end
      object EdtALIQUOTA_ST_ICMS: TEdit
        Left = 473
        Top = 80
        Width = 60
        Height = 20
        Hint = 'F'#243'rmula: ALIQUOTA_ST_ICMS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 13
      end
      object EdtIVA_ST_ICMS: TEdit
        Left = 599
        Top = 81
        Width = 60
        Height = 20
        Hint = 'F'#243'rmula: IVA_ST_ICMS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 14
      end
      object EdtPAUTA_ST_ICMS: TEdit
        Left = 742
        Top = 81
        Width = 60
        Height = 20
        Hint = 'F'#243'rmula: PAUTA_ST_ICMS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 15
      end
      object EdtCFOP_ICMS: TEdit
        Left = 599
        Top = 26
        Width = 60
        Height = 20
        Color = 6073854
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 5
        OnDblClick = EdtCFOP_ICMSDblClick
        OnExit = EdtCFOP_ICMSExit
        OnKeyDown = EdtCFOP_ICMSKeyDown
      end
      object ComboMODALIDADE_ICMS: TComboBox
        Left = 96
        Top = 56
        Width = 146
        Height = 22
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ItemHeight = 14
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 6
        Text = '   '
        Items.Strings = (
          '0 - Margem Valor Agregado (%);'
          '1 - Pauta (Valor);'
          '2 - Pre'#231'o Tabelado M'#225'x. (valor);'
          '3 - valor da opera'#231#227'o.')
      end
      object ComboMODALIDADE_ST_ICMS: TComboBox
        Left = 96
        Top = 83
        Width = 147
        Height = 22
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ItemHeight = 14
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 11
        Text = '   '
        Items.Strings = (
          '0 - Pre'#231'o tabelado ou m'#225'ximo sugerido;'
          '1 - Lista Negativa (valor);'
          '2 - Lista Positiva (valor);'
          '3 - Lista Neutra (valor);'
          '4 - Margem Valor Agregado (%);'
          '5 - Pauta (valor);')
      end
      object edtSTA_ICMS: TEdit
        Left = 96
        Top = 25
        Width = 30
        Height = 20
        Color = 6073854
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
        OnDblClick = edtSTA_ICMSDblClick
        OnExit = edtSTA_ICMSExit
        OnKeyDown = edtSTA_ICMSKeyDown
      end
      object EdtSTB_ICMS: TEdit
        Left = 127
        Top = 25
        Width = 30
        Height = 20
        Color = 6073854
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 4
        OnDblClick = EdtSTB_ICMSDblClick
        OnExit = EdtSTB_ICMSExit
        OnKeyDown = EdtSTB_ICMSKeyDown
      end
      object edtformula_bc_ICMS: TEdit
        Left = 202
        Top = 114
        Width = 600
        Height = 20
        Hint = 'F'#243'rmula: BC_ICMS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = POPUPMenuVariaveisICMS
        TabOrder = 16
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
      object edtformula_bc_st_ICMS: TEdit
        Left = 202
        Top = 168
        Width = 600
        Height = 20
        Hint = 'F'#243'rmula: BC_ST_ICMS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = POPUPMenuVariaveisICMS_ST
        TabOrder = 18
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
      object EDTImpostoModelo_ICMS: TEdit
        Left = 96
        Top = 2
        Width = 61
        Height = 20
        Color = 6073854
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        OnDblClick = EDTImpostoModelo_ICMSDblClick
        OnExit = EDTImpostoModelo_ICMSExit
        OnKeyDown = EDTImpostoModelo_ICMSKeyDown
      end
      object edtFORMULA_VALOR_IMPOSTO_ICMS: TEdit
        Left = 202
        Top = 140
        Width = 600
        Height = 20
        Hint = 'F'#243'rmula: VALOR_ICMS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = POPUPMenuVariaveisICMS
        TabOrder = 17
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
      object edtFORMULA_VALOR_IMPOSTO_ST_ICMS: TEdit
        Left = 202
        Top = 193
        Width = 600
        Height = 20
        Hint = 'F'#243'rmula: VALOR_ST_ICMS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = POPUPMenuVariaveisICMS_ST
        TabOrder = 19
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
      object edtCFOPForaEstado: TEdit
        Left = 599
        Top = 4
        Width = 60
        Height = 20
        Color = 6073854
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        OnDblClick = edtCFOPForaEstadoDblClick
        OnExit = edtCFOPForaEstadoExit
        OnKeyDown = edtCFOPForaEstadoKeyDown
      end
      object edtCSOSN: TEdit
        Left = 330
        Top = 6
        Width = 60
        Height = 20
        Color = 6073854
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 1
        OnDblClick = edtCSOSNDblClick
        OnKeyDown = edtCSOSNKeyDown
      end
      object edtpercentualtributo: TEdit
        Left = 218
        Top = 4
        Width = 60
        Height = 20
        Color = clWhite
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 20
      end
    end
    object TabSheet2: TTabSheet
      Caption = '&7 - IPI'
      ImageIndex = 1
      object Label6: TLabel
        Left = 5
        Top = 6
        Width = 82
        Height = 14
        Caption = 'Modelo (Imposto)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbST: TLabel
        Left = 5
        Top = 36
        Width = 90
        Height = 14
        Caption = 'Situa'#231#227'o Tribut'#225'ria'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeST_IPI: TLabel
        Left = 269
        Top = 31
        Width = 90
        Height = 14
        Caption = 'Situa'#231#227'o Tribut'#225'ria'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbClasseEnq: TLabel
        Left = 448
        Top = 34
        Width = 192
        Height = 14
        Caption = 'Classe de Enquad. (Cigarros e bebidas)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbCodigoEnquadramento: TLabel
        Left = 5
        Top = 57
        Width = 155
        Height = 14
        Caption = 'C'#243'digo de Enquadramento Legal'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbCnpjProdutor: TLabel
        Left = 271
        Top = 56
        Width = 84
        Height = 14
        Caption = 'CNPJ do Produtor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbCodigoSelo: TLabel
        Left = 5
        Top = 90
        Width = 130
        Height = 14
        Caption = 'C'#243'digo do Selo de Controle'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbTipoCalculo: TLabel
        Left = 271
        Top = 87
        Width = 73
        Height = 14
        Caption = 'Tipo de C'#225'lculo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbQuantidadeTotalUP: TLabel
        Left = 271
        Top = 114
        Width = 168
        Height = 14
        Caption = 'Qtde  de unidade padr'#227'o para Trib.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbValorUnidade: TLabel
        Left = 546
        Top = 114
        Width = 86
        Height = 14
        Caption = 'Valor por Unidade'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbFORMULABASECALCULO: TLabel
        Left = 5
        Top = 140
        Width = 134
        Height = 14
        Caption = 'F'#243'rmula da Base de C'#225'lculo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 5
        Top = 116
        Width = 39
        Height = 14
        Caption = 'Al'#237'quota'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label28: TLabel
        Left = 5
        Top = 166
        Width = 121
        Height = 14
        Caption = 'F'#243'rmula Valor do Imposto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object edtimposto_ipi_modelo: TEdit
        Left = 163
        Top = 2
        Width = 100
        Height = 20
        Color = clSkyBlue
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        OnExit = edtimposto_ipi_modeloExit
        OnKeyDown = edtimposto_ipi_modeloKeyDown
      end
      object EdtST_IPI: TEdit
        Left = 163
        Top = 28
        Width = 100
        Height = 20
        Color = clSkyBlue
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        OnExit = EdtST_IPIExit
        OnKeyDown = EdtST_IPIKeyDown
      end
      object EdtClasseEnq_IPI: TEdit
        Left = 648
        Top = 33
        Width = 100
        Height = 20
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
      end
      object EdtCodigoEnquadramento_IPI: TEdit
        Left = 163
        Top = 57
        Width = 100
        Height = 20
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
      end
      object EdtCnpjProdutor_IPI: TEdit
        Left = 445
        Top = 57
        Width = 200
        Height = 20
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 20
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 4
      end
      object EdtCodigoSelo_IPI: TEdit
        Left = 163
        Top = 82
        Width = 100
        Height = 20
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 5
      end
      object EdtQuantidadeTotalUP_IPI: TEdit
        Left = 445
        Top = 109
        Width = 100
        Height = 20
        Hint = 'F'#243'rmula: QTDE_UP_IPI'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 8
      end
      object EdtValorUnidade_IPI: TEdit
        Left = 648
        Top = 109
        Width = 101
        Height = 20
        Hint = 'F'#243'rmula: VL_UP_IPI'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 9
      end
      object EdtFORMULABASECALCULO_IPI: TEdit
        Left = 163
        Top = 134
        Width = 585
        Height = 20
        Hint = 'F'#243'rmula: BC_IPI'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 500
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = PopupMenuVariaveisIPI
        TabOrder = 10
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
      object ComboTipoCalculo_IPI: TComboBox
        Left = 445
        Top = 82
        Width = 145
        Height = 22
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ItemHeight = 14
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 6
        Text = '  '
        Items.Strings = (
          'Percentual'
          'Valor')
      end
      object edtaliquota_ipi: TEdit
        Left = 163
        Top = 109
        Width = 100
        Height = 20
        Hint = 'F'#243'rmula: ALIQUOTA_IPI'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 7
      end
      object edtFORMULA_VALOR_IMPOSTO_IPI: TEdit
        Left = 163
        Top = 161
        Width = 583
        Height = 20
        Hint = 'F'#243'rmula: VALORIPI_PROD'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = PopupMenuVariaveisIPI
        TabOrder = 11
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
    end
    object TabSheet3: TTabSheet
      Caption = '&8 - PIS'
      ImageIndex = 2
      object Label9: TLabel
        Left = 3
        Top = 33
        Width = 90
        Height = 14
        Caption = 'Situa'#231#227'o Tribut'#225'ria'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeST_PIS: TLabel
        Left = 272
        Top = 33
        Width = 90
        Height = 14
        Caption = 'Situa'#231#227'o Tribut'#225'ria'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label10: TLabel
        Left = 3
        Top = 59
        Width = 73
        Height = 14
        Caption = 'Tipo de C'#225'lculo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbALIQUOTAPERCENTUAL: TLabel
        Left = 327
        Top = 59
        Width = 93
        Height = 14
        Caption = 'Al'#237'quota Percentual'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbALIQUOTAVALOR: TLabel
        Left = 551
        Top = 59
        Width = 84
        Height = 14
        Caption = 'Al'#237'quota em Valor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbTIPOCALCULO_ST: TLabel
        Left = 3
        Top = 138
        Width = 97
        Height = 14
        Caption = 'Tipo de C'#225'lculo (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbALIQUOTAPERCENTUAL_ST: TLabel
        Left = 327
        Top = 138
        Width = 117
        Height = 14
        Caption = 'Al'#237'quota Percentual (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbALIQUOTAVALOR_ST: TLabel
        Left = 551
        Top = 138
        Width = 108
        Height = 14
        Caption = 'Al'#237'quota em Valor (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 3
        Top = 85
        Width = 134
        Height = 14
        Caption = 'F'#243'rmula da Base de C'#225'lculo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 3
        Top = 165
        Width = 158
        Height = 14
        Caption = 'F'#243'rmula da Base de C'#225'lculo (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label13: TLabel
        Left = 3
        Top = 6
        Width = 82
        Height = 14
        Caption = 'Modelo (Imposto)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label29: TLabel
        Left = 3
        Top = 112
        Width = 121
        Height = 14
        Caption = 'F'#243'rmula Valor do Imposto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label30: TLabel
        Left = 3
        Top = 191
        Width = 145
        Height = 14
        Caption = 'F'#243'rmula Valor do Imposto (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object EdtST_PIS: TEdit
        Left = 165
        Top = 30
        Width = 100
        Height = 20
        Color = clSkyBlue
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        OnExit = EdtST_PISExit
        OnKeyDown = EdtST_PISKeyDown
      end
      object EdtALIQUOTAPERCENTUAL_PIS: TEdit
        Left = 448
        Top = 59
        Width = 100
        Height = 20
        Hint = 'F'#243'rmula: ALIQUOTAPERCENTUAL_PIS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
      end
      object EdtALIQUOTAVALOR_PIS: TEdit
        Left = 664
        Top = 59
        Width = 100
        Height = 20
        Hint = 'F'#243'rmula: ALIQUOTAVALOR_PIS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 4
      end
      object EdtALIQUOTAPERCENTUAL_ST_PIS: TEdit
        Left = 448
        Top = 137
        Width = 100
        Height = 20
        Hint = 'F'#243'rmula: ALIQUOTAPERCENTUAL_ST_PIS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 8
      end
      object EdtALIQUOTAVALOR_ST_PIS: TEdit
        Left = 664
        Top = 137
        Width = 100
        Height = 20
        Hint = 'F'#243'rmula: ALIQUOTAVALOR_ST_PIS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 9
      end
      object ComboTipoCalculo_PIS: TComboBox
        Left = 165
        Top = 57
        Width = 145
        Height = 22
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ItemHeight = 14
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        Text = '  '
        Items.Strings = (
          'Percentual'
          'Valor')
      end
      object comboTIPOCALCULO_ST_PIS: TComboBox
        Left = 165
        Top = 135
        Width = 145
        Height = 22
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ItemHeight = 14
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 7
        Text = '  '
        Items.Strings = (
          'Percentual'
          'Valor')
      end
      object EdtFORMULABASECALCULO_PIS: TEdit
        Left = 165
        Top = 83
        Width = 600
        Height = 20
        Hint = 'F'#243'rmula: BC_PIS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = PopupMenuVariaveisPIS
        TabOrder = 5
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
      object EdtFORMULABASECALCULO_ST_PIS: TEdit
        Left = 165
        Top = 162
        Width = 600
        Height = 20
        Hint = 'F'#243'rmula: BC_ST_PIS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = PopupMenuVariaveisPIS_ST
        TabOrder = 10
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
      object edtimposto_pis_modelo: TEdit
        Left = 165
        Top = 4
        Width = 57
        Height = 20
        Color = clSkyBlue
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        OnExit = edtimposto_pis_modeloExit
        OnKeyDown = edtimposto_pis_modeloKeyDown
      end
      object edtFORMULA_VALOR_IMPOSTO_PIS: TEdit
        Left = 165
        Top = 109
        Width = 600
        Height = 20
        Hint = 'F'#243'rmula: VALOR_PIS_PROD'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = PopupMenuVariaveisPIS
        TabOrder = 6
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
      object edtFORMULA_VALOR_IMPOSTO_ST_PIS: TEdit
        Left = 165
        Top = 188
        Width = 600
        Height = 20
        Hint = 'F'#243'rmula: VALOR_PIS_ST_PROD'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = PopupMenuVariaveisPIS_ST
        TabOrder = 11
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
    end
    object TabSheet4: TTabSheet
      Caption = '&9-COFINS'
      ImageIndex = 3
      object Label8: TLabel
        Left = 8
        Top = 32
        Width = 90
        Height = 14
        Caption = 'Situa'#231#227'o Tribut'#225'ria'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeST_COFINS: TLabel
        Left = 320
        Top = 32
        Width = 90
        Height = 14
        Caption = 'Situa'#231#227'o Tribut'#225'ria'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label14: TLabel
        Left = 8
        Top = 56
        Width = 73
        Height = 14
        Caption = 'Tipo de C'#225'lculo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label15: TLabel
        Left = 320
        Top = 56
        Width = 93
        Height = 14
        Caption = 'Al'#237'quota Percentual'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label16: TLabel
        Left = 556
        Top = 56
        Width = 84
        Height = 14
        Caption = 'Al'#237'quota em Valor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label20: TLabel
        Left = 8
        Top = 133
        Width = 97
        Height = 14
        Caption = 'Tipo de C'#225'lculo (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label21: TLabel
        Left = 320
        Top = 133
        Width = 117
        Height = 14
        Caption = 'Al'#237'quota Percentual (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label22: TLabel
        Left = 556
        Top = 133
        Width = 108
        Height = 14
        Caption = 'Al'#237'quota em Valor (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label23: TLabel
        Left = 8
        Top = 82
        Width = 134
        Height = 14
        Caption = 'F'#243'rmula da Base de C'#225'lculo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label24: TLabel
        Left = 8
        Top = 159
        Width = 158
        Height = 14
        Caption = 'F'#243'rmula da Base de C'#225'lculo (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label25: TLabel
        Left = 8
        Top = 6
        Width = 82
        Height = 14
        Caption = 'Modelo (Imposto)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label31: TLabel
        Left = 8
        Top = 107
        Width = 121
        Height = 14
        Caption = 'F'#243'rmula Valor do Imposto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label32: TLabel
        Left = 8
        Top = 185
        Width = 145
        Height = 14
        Caption = 'F'#243'rmula Valor do Imposto (ST)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object EdtST_COFINS: TEdit
        Left = 168
        Top = 29
        Width = 100
        Height = 20
        Color = clSkyBlue
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        OnExit = EdtST_COFINSExit
        OnKeyDown = EdtST_COFINSKeyDown
      end
      object EdtALIQUOTAPERCENTUAL_COFINS: TEdit
        Left = 445
        Top = 54
        Width = 100
        Height = 20
        Hint = 'F'#243'rmula: ALIQUOTAPERCENTUAL_COFINS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
      end
      object EdtALIQUOTAVALOR_COFINS: TEdit
        Left = 668
        Top = 54
        Width = 100
        Height = 20
        Hint = 'F'#243'rmula: ALIQUOTAVALOR_COFINS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 4
      end
      object EdtALIQUOTAPERCENTUAL_ST_COFINS: TEdit
        Left = 445
        Top = 132
        Width = 100
        Height = 20
        Hint = 'F'#243'rmula: ALIQUOTAPERCENTUAL_ST_COFINS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 8
      end
      object EdtALIQUOTAVALOR_ST_COFINS: TEdit
        Left = 668
        Top = 132
        Width = 100
        Height = 20
        Hint = 'F'#243'rmula: ALIQUOTAVALOR_ST_COFINS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 9
      end
      object ComboTipoCalculo_COFINS: TComboBox
        Left = 168
        Top = 52
        Width = 145
        Height = 22
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ItemHeight = 14
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        Text = '  '
        Items.Strings = (
          'Percentual'
          'Valor')
      end
      object comboTIPOCALCULO_ST_COFINS: TComboBox
        Left = 168
        Top = 130
        Width = 145
        Height = 22
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ItemHeight = 14
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 7
        Text = '  '
        Items.Strings = (
          'Percentual'
          'Valor')
      end
      object EdtFORMULABASECALCULO_COFINS: TEdit
        Left = 168
        Top = 79
        Width = 600
        Height = 20
        Hint = 'F'#243'rmula: BC_COFINS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = PopupMenuVariaveisCOFINS
        TabOrder = 5
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
      object EdtFORMULABASECALCULO_ST_COFINS: TEdit
        Left = 168
        Top = 157
        Width = 600
        Height = 20
        Hint = 'F'#243'rmula: BC_ST_COFINS'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = PopupMenuVariaveisCOFINS_ST
        TabOrder = 10
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
      object edtimposto_cofins_modelo: TEdit
        Left = 168
        Top = 2
        Width = 57
        Height = 20
        Color = clSkyBlue
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        OnExit = edtimposto_cofins_modeloExit
        OnKeyDown = edtimposto_cofins_modeloKeyDown
      end
      object edtFORMULA_VALOR_IMPOSTO_COFINS: TEdit
        Left = 168
        Top = 105
        Width = 600
        Height = 20
        Hint = 'F'#243'rmula: VALOR_COFINS_PROD'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = PopupMenuVariaveisCOFINS
        TabOrder = 6
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
      object edtFORMULA_VALOR_IMPOSTO_ST_COFINS: TEdit
        Left = 168
        Top = 182
        Width = 600
        Height = 20
        Hint = 'F'#243'rmula: VALOR_COFINS_ST_PROD'
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = PopupMenuVariaveisCOFINS_ST
        TabOrder = 11
        OnEnter = edtformula_bc_ICMSEnter
        OnExit = edtformula_bc_ICMSExit
      end
    end
  end
  object PanelBotoes: TPanel
    Left = 0
    Top = 286
    Width = 822
    Height = 28
    Align = alTop
    BevelOuter = bvNone
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    object BtGravar_material_icms: TButton
      Left = 11
      Top = 3
      Width = 70
      Height = 24
      Caption = 'Gravar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtGravar_material_icmsClick
    end
    object btcancelar_material_ICMS: TButton
      Left = 84
      Top = 3
      Width = 70
      Height = 24
      Caption = 'Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btcancelar_material_ICMSClick
    end
    object btexcluir_material_ICMS: TButton
      Left = 158
      Top = 3
      Width = 70
      Height = 24
      Caption = 'Excluir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btexcluir_material_ICMSClick
    end
  end
  object POPUPMenuVariaveisICMS: TPopupMenu
    Left = 608
    Top = 39
    object PModelo: TMenuItem
      Caption = 'Modelo'
      OnClick = PModeloClick
    end
  end
  object POPUPMenuVariaveisICMS_ST: TPopupMenu
    Left = 504
    Top = 39
  end
  object PopupMenuVariaveisIPI: TPopupMenu
    Left = 536
    Top = 39
  end
  object PopupMenuVariaveisPIS: TPopupMenu
    Left = 576
    Top = 39
  end
  object PopupMenuVariaveisPIS_ST: TPopupMenu
    Left = 672
    Top = 39
  end
  object PopupMenuVariaveisCOFINS: TPopupMenu
    Left = 640
    Top = 39
  end
  object PopupMenuVariaveisCOFINS_ST: TPopupMenu
    Left = 704
    Top = 39
  end
end
