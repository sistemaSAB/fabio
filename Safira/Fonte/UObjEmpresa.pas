unit UobjEMPRESA;
Interface
Uses Ibquery,windows,Classes,Db,UessencialGlobal,stdctrls,UobjCRT;

Type
   TObjEMPRESA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                CRT                                         :TObjCRT;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_RAZAOSOCIAL(parametro: string);
                Function Get_RAZAOSOCIAL: string;
                Procedure Submit_FANTASIA(parametro: string);
                Function Get_FANTASIA: string;
                Procedure Submit_CNPJ(parametro: string);
                Function Get_CNPJ: string;
                Procedure Submit_IE(parametro: string);
                Function Get_IE: string;
                Procedure Submit_ENDERECO(parametro: string);
                Function Get_ENDERECO: string;
                Procedure Submit_CIDADE(parametro: string);
                Function Get_CIDADE: string;
                Procedure Submit_ESTADO(parametro: string);
                Function Get_ESTADO: string;
                Procedure Submit_CEP(parametro: string);
                Function Get_CEP: string;
                Procedure Submit_COMPLEMENTO(parametro: string);
                Function Get_COMPLEMENTO: string;
                Procedure Submit_FONE(parametro: string);
                Function Get_FONE: string;
                Procedure Submit_FAX(parametro: string);
                Function Get_FAX: string;
                Procedure Submit_CELULAR(parametro: string);
                Function Get_CELULAR: string;
                Procedure Submit_EMAIL(parametro: string);
                Function Get_EMAIL: string;
                Procedure Submit_HOMEPAGE(parametro: string);
                Function Get_HOMEPAGE: string;
                Procedure Submit_DIRETOR(parametro: string);
                Function Get_DIRETOR: string;
                Procedure Submit_CELULARDIRETOR(parametro: string);
                Function Get_CELULARDIRETOR: string;

                Function get_Simples:string;
                Procedure Submit_simples(Parametro:string);

                Function get_bairro:string;
                Procedure Submit_bairro(parametro:string);

                Function Get_numero:string;
                Procedure Submit_numero(parametro:string);

                Function  Get_codigocidade:String;
                Procedure Submit_codigocidade(parametro:String);

                Function  Get_codigoestado:String;
                Procedure Submit_codigoestado(parametro:String);

                Function  Get_certificado:String;
                Procedure Submit_certificado(parametro:String);

                Function Get_codigoclientesite:String;
                Procedure Submit_codigoclientesite(parametro:String);

                function Get_ComissaoArquiteto:string;
                procedure Submit_ComissaoArquiteto(parametro:string);

                function Get_Pis:string;
                procedure Submit_pis(parametro:string);

                function Get_Cofins:string;
                procedure Submit_Cofins(parametro:string);

                function Get_CreditaPisCofins:string;
                procedure Submit_CreditaCofins(parametro:string);

                function Get_CreditaIPI:string;
                procedure Submit_CreditaIPI(parametro:String);

                function Get_CreditaICMSnaCompra : string;
                procedure Submit_CreditaICMSCompra(parametro:string);

                function Get_PagaIcmsnoSimples:string;
                procedure Submit_PagaIcmsnoSimples(parametro:string);

                function Get_icmsantecipadotodasdespesasnf:string;
                procedure Submit_icmsantecipadotodasdespesasnf(parametro:string);

                Function Get_AliquotaISS:string;
                procedure Submit_AliquotaISS(parametro:string);

                Function Get_AliquotaSimples:string;
                procedure Submit_AliquotaSimples(parametro:String);

                Function Get_aliquotaiss_substituto:string;
                Procedure Submit_Aliquotaiss_substituto(parametro:string);

                Function Get_SubstitutoTributario:String;
                procedure Submit_SubstitutoTributario(parametro:string);

                Function Get_AliquotaICMSGarantido:String;
                procedure Submit_AliquotaICMSGarantido(parametro:string);

                Function Get_IcmsGarantido:String;
                procedure Submit_ICMSGarantido(parametro:string);

                Function Get_utilizaicmssubstrecolhido_custo:string;
                procedure Submit_utilizaicmssubstrecolhido_custo(parametro:string);

                function carregaCidades(comboBoxCidade: TComboBox): boolean;
                function carregaCodigoPaises(comboBoxPais: TComboBox): boolean;
                function retornaUF(str: string): string;
                function retornaCidade(str: string): string;
                function retornaCodigoCidade(cidade, uf: string): string;

                procedure Submit_FraseNFe(parametro:string);
                function Get_FraseNFe:string;



         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               RAZAOSOCIAL:string;
               FANTASIA:string;
               CNPJ:string;
               IE:string;
               ENDERECO:string;
               Bairro:string;
               CIDADE:string;
               ESTADO:string;
               CEP:string;
               COMPLEMENTO:string;
               FONE:string;
               FAX:string;
               CELULAR:string;
               EMAIL:string;
               HOMEPAGE:string;
               DIRETOR:string;
               CELULARDIRETOR:string;
               Simples:string;
               Numero:String;
               CodigoCidade:String;
               CodigoEstado:String;
               certificado:String;
               ComissaoArquiteto:string;

               codigoclientesite:String;

               Pis:string;
               Cofins:string;
               CreditaPisCofins:string;
               CreditaIPI:string;
               CreditaICMSnaCompra : string;
               PagaIcmsnoSimples:string;
               icmsantecipadotodasdespesasnf:string;
               AliquotaISS:string;
               AliquotaSimples:string;
               aliquotaiss_substituto:string;
               SubstitutoTributario:String;
               AliquotaICMSGarantido:String;
               IcmsGarantido:String;

               FraseNotaFiscal:String;

               utilizaicmssubstrecolhido_custo : string;

               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls
//USES IMPLEMENTATION
;


{ TTabTitulo }


Function  TObjEMPRESA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.RAZAOSOCIAL:=fieldbyname('RAZAOSOCIAL').asstring;
        Self.FANTASIA:=fieldbyname('FANTASIA').asstring;
        Self.CNPJ:=fieldbyname('CNPJ').asstring;
        Self.IE:=fieldbyname('IE').asstring;
        Self.ENDERECO:=fieldbyname('ENDERECO').asstring;
        Self.bairro:=fieldbyname('bairro').asstring;
        Self.CIDADE:=fieldbyname('CIDADE').asstring;
        Self.ESTADO:=fieldbyname('ESTADO').asstring;
        Self.CEP:=fieldbyname('CEP').asstring;
        Self.COMPLEMENTO:=fieldbyname('COMPLEMENTO').asstring;
        Self.FONE:=fieldbyname('FONE').asstring;
        Self.FAX:=fieldbyname('FAX').asstring;
        Self.CELULAR:=fieldbyname('CELULAR').asstring;
        Self.EMAIL:=fieldbyname('EMAIL').asstring;
        Self.HOMEPAGE:=fieldbyname('HOMEPAGE').asstring;
        Self.DIRETOR:=fieldbyname('DIRETOR').asstring;
        Self.CELULARDIRETOR:=fieldbyname('CELULARDIRETOR').asstring;
        Self.simples:=fieldbyname('simples').asstring;

        Self.numero:=fieldbyname('numero').asstring;
        Self.codigocidade:=fieldbyname('codigocidade').asstring;
        Self.codigoestado:=fieldbyname('codigoestado').asstring;
        Self.certificado:=fieldbyname('certificado').asstring;
        Self.codigoclientesite:=fieldbyname('codigoclientesite').asstring;
        self.ComissaoArquiteto:=fieldbyname('comissaoarquiteto').AsString;

        If(FieldByName('CRT').asstring<>'')
        Then Begin
                 If (Self.CRT.LocalizaCodigo(FieldByName('CRT').asstring)=False)
                 Then Begin
                          Messagedlg('CRT N�o encontrado!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CRT.TabelaparaObjeto;
        End;

        Self.utilizaicmssubstrecolhido_custo:=fieldbyname('utilizaicmssubstrecolhido_custo').AsString;
        self.icmsantecipadotodasdespesasnf:=fieldbyname('icmsantecipadotodasdespesasnf').asstring;
        Self.CreditaICMSnaCompra:=fieldbyname('creditaicmsnacompra').AsString;
        self.SubstitutoTributario:=fieldbyname('substitutotributario').AsString;
        Self.PagaIcmsnoSimples:=fieldbyname('pagaicmsnosimples').AsString;
        self.Pis:=fieldbyname('pis').AsString;
        Self.Cofins:=fieldbyname('cofins').AsString;
        Self.CreditaPisCofins:=fieldbyname('creditapiscofins').AsString;
        self.CreditaIPI:=fieldbyname('creditaipi').AsString;
        self.AliquotaISS:=fieldbyname('aliquotaiss').AsString;
        Self.AliquotaSimples:=fieldbyname('aliquotasimples').AsString;
        self.aliquotaiss_substituto:=fieldbyname('aliquotaiss_substituto').AsString;
        self.IcmsGarantido:=fieldbyname('icmsgarantido').AsString;
        Self.AliquotaICMSGarantido:=fieldbyname('AliquotaICMSGarantido').AsString;
        Self.FraseNotaFiscal:=fieldbyname('FRASENOTAFISCAL').AsString;
        result:=True;
     End;
end;


Procedure TObjEMPRESA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('RAZAOSOCIAL').asstring:=Self.RAZAOSOCIAL;
        ParamByName('FANTASIA').asstring:=Self.FANTASIA;
        ParamByName('CNPJ').asstring:=Self.CNPJ;
        ParamByName('IE').asstring:=Self.IE;
        ParamByName('ENDERECO').asstring:=Self.ENDERECO;
        ParamByName('bairro').asstring:=Self.bairro;
        ParamByName('CIDADE').asstring:=Self.CIDADE;
        ParamByName('ESTADO').asstring:=Self.ESTADO;
        ParamByName('CEP').asstring:=Self.CEP;
        ParamByName('COMPLEMENTO').asstring:=Self.COMPLEMENTO;
        ParamByName('FONE').asstring:=Self.FONE;
        ParamByName('FAX').asstring:=Self.FAX;
        ParamByName('CELULAR').asstring:=Self.CELULAR;
        ParamByName('EMAIL').asstring:=Self.EMAIL;
        ParamByName('HOMEPAGE').asstring:=Self.HOMEPAGE;
        ParamByName('DIRETOR').asstring:=Self.DIRETOR;
        ParamByName('CELULARDIRETOR').asstring:=Self.CELULARDIRETOR;
        ParamByName('Simples').asstring:=Self.simples;

        ParamByName('Numero').asstring:=Self.Numero;
        ParamByName('CodigoCidade').asstring:=Self.CodigoCidade;
        ParamByName('codigoestado').asstring:=Self.codigoestado;
        ParamByName('certificado').asstring:=Self.certificado;
        ParamByName('codigoclientesite').asstring:=Self.codigoclientesite;
        ParamByName('ComissaoArquiteto').AsString:=self.ComissaoArquiteto;
        ParamByName('CRT').asstring:=self.CRT.Get_Codigo;

        ParamByName('utilizaicmssubstrecolhido_custo').AsString:=Self.utilizaicmssubstrecolhido_custo;
        ParamByName('icmsantecipadotodasdespesasnf').asstring:=self.icmsantecipadotodasdespesasnf;
        ParamByName('creditaicmsnacompra').AsString:=Self.CreditaICMSnaCompra;
        ParamByName('substitutotributario').AsString:=self.SubstitutoTributario;
        ParamByName('pagaicmsnosimples').AsString:=Self.PagaIcmsnoSimples;
        ParamByName('pis').AsString:=format_db(self.Pis);
        ParamByName('cofins').AsString:=format_db(Self.Cofins);
        ParamByName('creditapiscofins').AsString:=Self.CreditaPisCofins;
        ParamByName('creditaipi').AsString:=self.CreditaIPI;
        ParamByName('aliquotaiss').AsString:=format_db(self.AliquotaISS);
        ParamByName('aliquotasimples').AsString:=format_db(Self.AliquotaSimples);
        ParamByName('aliquotaiss_substituto').AsString:=format_db(self.aliquotaiss_substituto);
        ParamByName('icmsgarantido').AsString:=format_db(self.IcmsGarantido);
        ParamByName('AliquotaICMSGarantido').AsString:=format_db(self.AliquotaICMSGarantido);
        ParamByName('FRASENOTAFISCAL').AsString:=self.FraseNotaFiscal;
  End;
End;

//***********************************************************************

function TObjEMPRESA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;

 Try
    Self.Objquery.ExecSQL;

 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0);
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjEMPRESA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        RAZAOSOCIAL:='';
        FANTASIA:='';
        CNPJ:='';
        IE:='';
        ENDERECO:='';
        CIDADE:='';
        ESTADO:='';
        CEP:='';
        COMPLEMENTO:='';
        FONE:='';
        FAX:='';
        CELULAR:='';
        EMAIL:='';
        HOMEPAGE:='';
        DIRETOR:='';
        CELULARDIRETOR:='';
        bairro:='';
        Simples:='';
        Numero:='';
        CodigoCidade:='';
        codigoestado:='';
        certificado:='';
        codigoclientesite:='';
        ComissaoArquiteto:='';
        Pis:='';
        Cofins:='';
        CreditaPisCofins:='';
        CreditaIPI:='';
        CreditaICMSnaCompra :='';
        PagaIcmsnoSimples:='';
        icmsantecipadotodasdespesasnf:='';
        AliquotaISS:='';
        AliquotaSimples:='';
        aliquotaiss_substituto:='';
        Numero:='';
        SubstitutoTributario:='';
        AliquotaICMSGarantido:='';
        IcmsGarantido:='';
        CRT.ZerarTabela;
        utilizaicmssubstrecolhido_custo:='';
        FraseNotaFiscal:='';

     End;
end;

Function TObjEMPRESA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (RAZAOSOCIAL='')
      Then Mensagem:=mensagem+'/Raz�o Social';
      If (FANTASIA='')
      Then Mensagem:=mensagem+'/Fantasia';
      if (Simples='')
      Then mensagem:=mensagem+'/Simples';


  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjEMPRESA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjEMPRESA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;


     try
        if (Self.codigoclientesite<>'')
        Then Strtoint(Self.codigoclientesite);
     Except
           Mensagem:=mensagem+'/C�digo do Cliente no Site';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjEMPRESA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjEMPRESA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';

        if ((simples<>'S') and (simples<>'N'))
        Then mensagem:=mensagem+'/O valor da campo simples est� inv�lido';

//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjEMPRESA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       With Self.Objquery do
       Begin
       
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,RAZAOSOCIAL,FANTASIA,CNPJ,IE,ENDERECO,bairro,CIDADE,ESTADO');
           SQL.ADD(' ,CEP,COMPLEMENTO,FONE,FAX,CELULAR,EMAIL,HOMEPAGE,DIRETOR,CELULARDIRETOR,simples,Numero,CodigoCidade,CodigoEstado,certificado,codigoclientesite,ComissaoArquiteto,CRT');
           SQL.ADD(' ,utilizaicmssubstrecolhido_custo,icmsantecipadotodasdespesasnf,creditaicmsnacompra,substitutotributario,pagaicmsnosimples,pis,cofins,creditapiscofins,creditaipi');
           SQL.ADD(' ,aliquotaiss,aliquotasimples,aliquotaiss_substituto,icmsgarantido,AliquotaICMSGarantido,FRASENOTAFISCAL');
           SQL.ADD(' from  TABEMPRESA');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjEMPRESA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjEMPRESA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjEMPRESA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        self.CRT:=TObjCRT.Create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABEMPRESA(CODIGO,RAZAOSOCIAL,FANTASIA,CNPJ');
                InsertSQL.add(' ,IE,ENDERECO,bairro,CIDADE,ESTADO,CEP,COMPLEMENTO,FONE,FAX');
                InsertSQL.add(' ,CELULAR,EMAIL,HOMEPAGE,DIRETOR,CELULARDIRETOR,simples,Numero,CodigoCidade,CodigoEstado,certificado,codigoclientesite,ComissaoArquiteto,CRT)');
                InsertSQL.ADD(' ,utilizaicmssubstrecolhido_custo,icmsantecipadotodasdespesasnf,creditaicmsnacompra,substitutotributario,pagaicmsnosimples,pis,cofins,creditapiscofins,creditaipi');
                InsertSQL.ADD(' ,aliquotaiss,aliquotasimples,aliquotaiss_substituto,icmsgarantido,AliquotaICMSGarantido,FRASENOTAFISCAL');
                InsertSQL.add('values (:CODIGO,:RAZAOSOCIAL,:FANTASIA,:CNPJ,:IE,:ENDERECO,:bairro');
                InsertSQL.add(' ,:CIDADE,:ESTADO,:CEP,:COMPLEMENTO,:FONE,:FAX,:CELULAR');
                InsertSQL.add(' ,:EMAIL,:HOMEPAGE,:DIRETOR,:CELULARDIRETOR,:simples,:Numero,:CodigoCidade,:CodigoEstado,:certificado,:codigoclientesite,:ComissaoArquiteto,:CRT)');
                InsertSQL.ADD(' ,:utilizaicmssubstrecolhido_custo,:icmsantecipadotodasdespesasnf,:creditaicmsnacompra,:substitutotributario,:pagaicmsnosimples,:pis,:cofins,:creditapiscofins,:creditaipi');
                InsertSQL.ADD(' ,:aliquotaiss,:aliquotasimples,:aliquotaiss_substituto,:icmsgarantido,:AliquotaICMSGarantido,:FRASENOTAFISCAL');

//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABEMPRESA set CODIGO=:CODIGO,RAZAOSOCIAL=:RAZAOSOCIAL');
                ModifySQL.add(',FANTASIA=:FANTASIA,CNPJ=:CNPJ,IE=:IE,ENDERECO=:ENDERECO,bairro=:bairro');
                ModifySQL.add(',CIDADE=:CIDADE,ESTADO=:ESTADO,CEP=:CEP,COMPLEMENTO=:COMPLEMENTO');
                ModifySQL.add(',FONE=:FONE,FAX=:FAX,CELULAR=:CELULAR,EMAIL=:EMAIL,HOMEPAGE=:HOMEPAGE');
                ModifySQL.add(',DIRETOR=:DIRETOR,CELULARDIRETOR=:CELULARDIRETOR,simples=:simples');
                ModifySQL.add(',Numero=:numero,CodigoCidade=:codigocidade,CodigoEstado=:CodigoEstado,certificado=:certificado,codigoclientesite=:codigoclientesite, ComissaoArquiteto=:ComissaoArquiteto, CRT=:CRT');
                ModifySQL.ADD(',utilizaicmssubstrecolhido_custo=:utilizaicmssubstrecolhido_custo,icmsantecipadotodasdespesasnf=:icmsantecipadotodasdespesasnf,creditaicmsnacompra=:creditaicmsnacompra,substitutotributario=:substitutotributario');
                ModifySQl.Add(',pagaicmsnosimples=:pagaicmsnosimples,pis=:pis,cofins=:cofins,creditapiscofins=:creditapiscofins,creditaipi=:creditaipi');
                ModifySQL.ADD(',aliquotaiss=:aliquotaiss,aliquotasimples=:aliquotasimples,aliquotaiss_substituto=:aliquotaiss_substituto,icmsgarantido=:icmsgarantido,AliquotaICMSGarantido=:AliquotaICMSGarantido,FRASENOTAFISCAL=:FRASENOTAFISCAL where codigo=:codigo');
                //CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABEMPRESA where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjEMPRESA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjEMPRESA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabEMPRESA');
     Result:=Self.ParametroPesquisa;
end;

function TObjEMPRESA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de EMPRESA ';
end;


function TObjEMPRESA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENEMPRESA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENEMPRESA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjEMPRESA.Free;
begin

    self.crt.Free;
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjEMPRESA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjEMPRESA.RetornaCampoNome: string;
begin
      result:='Fantasia';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjempresa.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjempresa.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjempresa.Submit_RAZAOSOCIAL(parametro: string);
begin
        Self.RAZAOSOCIAL:=Parametro;
end;
function TObjempresa.Get_RAZAOSOCIAL: string;
begin
        Result:=Self.RAZAOSOCIAL;
end;
procedure TObjempresa.Submit_FANTASIA(parametro: string);
begin
        Self.FANTASIA:=Parametro;
end;
function TObjempresa.Get_FANTASIA: string;
begin
        Result:=Self.FANTASIA;
end;
procedure TObjempresa.Submit_CNPJ(parametro: string);
begin
        Self.CNPJ:=Parametro;
end;
function TObjempresa.Get_CNPJ: string;
begin
        Result:=Self.CNPJ;
end;
procedure TObjempresa.Submit_IE(parametro: string);
begin
        Self.IE:=Parametro;
end;
function TObjempresa.Get_IE: string;
begin
        Result:=Self.IE;
end;
procedure TObjempresa.Submit_ENDERECO(parametro: string);
begin
        Self.ENDERECO:=Parametro;
end;
function TObjempresa.Get_ENDERECO: string;
begin
        Result:=Self.ENDERECO;
end;
procedure TObjempresa.Submit_CIDADE(parametro: string);
begin
        Self.CIDADE:=Parametro;
end;
function TObjempresa.Get_CIDADE: string;
begin
        Result:=Self.CIDADE;
end;
procedure TObjempresa.Submit_ESTADO(parametro: string);
begin
        Self.ESTADO:=Parametro;
end;
function TObjempresa.Get_ESTADO: string;
begin
        Result:=Self.ESTADO;
end;
procedure TObjempresa.Submit_CEP(parametro: string);
begin
        Self.CEP:=Parametro;
end;
function TObjempresa.Get_CEP: string;
begin
        Result:=Self.CEP;
end;
procedure TObjempresa.Submit_COMPLEMENTO(parametro: string);
begin
        Self.COMPLEMENTO:=Parametro;
end;
function TObjempresa.Get_COMPLEMENTO: string;
begin
        Result:=Self.COMPLEMENTO;
end;
procedure TObjempresa.Submit_FONE(parametro: string);
begin
        Self.FONE:=Parametro;
end;
function TObjempresa.Get_FONE: string;
begin
        Result:=Self.FONE;
end;
procedure TObjempresa.Submit_FAX(parametro: string);
begin
        Self.FAX:=Parametro;
end;
function TObjempresa.Get_FAX: string;
begin
        Result:=Self.FAX;
end;
procedure TObjempresa.Submit_CELULAR(parametro: string);
begin
        Self.CELULAR:=Parametro;
end;
function TObjempresa.Get_CELULAR: string;
begin
        Result:=Self.CELULAR;
end;
procedure TObjempresa.Submit_EMAIL(parametro: string);
begin
        Self.EMAIL:=Parametro;
end;
function TObjempresa.Get_EMAIL: string;
begin
        Result:=Self.EMAIL;
end;
procedure TObjempresa.Submit_HOMEPAGE(parametro: string);
begin
        Self.HOMEPAGE:=Parametro;
end;
function TObjempresa.Get_HOMEPAGE: string;
begin
        Result:=Self.HOMEPAGE;
end;
procedure TObjempresa.Submit_DIRETOR(parametro: string);
begin
        Self.DIRETOR:=Parametro;
end;
function TObjempresa.Get_DIRETOR: string;
begin
        Result:=Self.DIRETOR;
end;
procedure TObjempresa.Submit_CELULARDIRETOR(parametro: string);
begin
        Self.CELULARDIRETOR:=Parametro;
end;
function TObjempresa.Get_CELULARDIRETOR: string;
begin
        Result:=Self.CELULARDIRETOR;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
function TObjEMPRESA.get_bairro: string;
begin
     Result:=Self.Bairro;
end;

procedure TObjEMPRESA.Submit_bairro(parametro: string);
begin
    Self.Bairro:=parametro;
end;

function TObjEMPRESA.get_Simples: string;
begin
     Result:=Self.Simples;
end;

procedure TObjEMPRESA.Submit_simples(Parametro: string);
begin
     Self.Simples:=uppercase(parametro);
end;

function TObjEMPRESA.Get_codigocidade: String;
begin
     Result:=Self.CodigoCidade;
end;

function TObjEMPRESA.Get_codigoestado: String;
begin
     Result:=Self.codigoestado;
end;


function TObjEMPRESA.Get_certificado: String;
begin
     Result:=Self.certificado;
end;

function TObjEMPRESA.Get_numero: string;
begin
     result:=Self.Numero;
end;

procedure TObjEMPRESA.Submit_codigocidade(parametro: String);
begin
     Self.CodigoCidade:=parametro;
end;

procedure TObjEMPRESA.Submit_codigoestado(parametro: String);
begin
     Self.codigoestado:=parametro;
end;

procedure TObjEMPRESA.Submit_certificado(parametro: String);
begin
     Self.certificado:=parametro;
end;

procedure TObjEMPRESA.Submit_numero(parametro: string);
begin
     Self.Numero:=Parametro;
end;

function TObjEMPRESA.Get_codigoclientesite: String;
begin
     Result:=Self.codigoclientesite;

end;

procedure TObjEMPRESA.Submit_codigoclientesite(parametro: String);
begin
     Self.codigoclientesite:=Parametro;
end;

procedure TObjEMPRESA.Submit_ComissaoArquiteto(parametro:String);
begin
     self.ComissaoArquiteto:=parametro;
end;

function TObjEMPRESA.Get_ComissaoArquiteto:string;
begin
    Result:=Self.ComissaoArquiteto;
end;

function TObjEMPRESA.carregaCidades(comboBoxCidade: TComboBox): boolean;
var
  cidade,uf:string;
  query:TIBQuery;
begin

      comboBoxCidade.Items.Append ('');// para o limpa labels.
      query:=TIBQuery.Create(nil);
      query.Database:= FDataModulo.IBDatabase;
      try
            try
              with query do
              begin

                Close;
                sql.Clear;
                SQL.Add('select codigo,nome,estado,codigocidade');
                SQL.Add('from tabcidade');
                SQL.Add('order by nome');
                try
                    Open;
                    first;

                    while not (eof) do
                    begin

                      cidade := Fieldbyname('nome').AsString;
                      uf     := Fieldbyname('estado').AsString;

                      comboBoxCidade.Items.Append (AlinhaTexto (cidade,'|'+uf,36));

                      Next;

                    end;

                    result:=True;

                except

                   result:=False;

                end;


              end;

            except

              result:=False;

            end;
      finally
            FreeAndNil(query);
      end;

end;

function TObjEMPRESA.carregaCodigoPaises(comboBoxPais: TComboBox): boolean;
var
  pais,codigopais:string;
  query:TIBQuery;
begin

  comboBoxPais.Items.Append ('');// para o limpa labels.
  try
    query:=TIBQuery.Create(nil);
    query.Database:= FDataModulo.IBDatabase;
  except

  end;
  try




      try

        with Query do
        begin

          Close;
          sql.Clear;
          SQL.Add('select pais,codigopais');
          SQL.Add('from tabcodigopais');
          SQL.Add('order by pais');


          try

            Open;
            first;

            while not (eof) do
            begin

              pais       := Fieldbyname('pais').AsString;
              codigopais := Fieldbyname('codigopais').AsString;

              comboBoxpais.Items.Append (AlinhaTexto (pais,'|'+codigopais,34));

              Next;

            end;
        
            result:=True;

          except

             result:=False;

          end;


        end;

      except

        result:=False;

      end;
  finally
        FreeAndNil(query);
  end;

end;

function TObjEMPRESA.retornaUF(str: string): string;
var
  i:Integer;
begin

  result:='';

  i:=1;

  while ((str[i] <> '|') and (i <= Length (str))) do i:=i+1;

  i:=i+1;
  while ( i <= Length(str) ) do
  begin

    result:=result+str[i];
    i:=i+1;

  end;


end;

function TObjEMPRESA.retornaCidade(str: string): string;
var
  i,j:Integer;
  aux:string;
begin

  if (str = '') then
    Exit;

  aux:='';
  
  i:=1;
  while ((str[i] <> '|') and (i <= Length (str))) do
  begin

    aux:=aux+str[i];
    i:=i+1;

  end;

  i:=Length(aux);
  result:='';

  while ( (aux[i] = ' ') and (i >= 1 ) ) do I:=I-1;


  for j := 1 to i  do
    result:=result+aux[j];

end;

function TObjEMPRESA.retornaCodigoCidade(cidade, uf: string): string;
begin

  with self.Objquery do
  begin

    close;
    sql.Clear;

    sql.Add('select codigocidade');
    sql.Add('from tabcidade');
    sql.Add('where nome = '+#39+uppercase(cidade)+#39+'and upper(estado) = '+#39+uppercase(uf)+#39);

    //InputBox('','',SQL.Text);

    try

      Open;

      if (recordcount > 1) then
      begin

        MensagemAviso('Aten��o, existe registro em duplicidade no cadastro de cidades');
        Exit;

      end;

      result:=Fieldbyname('codigocidade').AsString;

    except

      MensagemAviso('N�o foi possivel executar a fun��o retornaCodigoCidade');
      Exit;

    end;

  end;

end;

procedure TObjEMPRESA.Submit_pis(parametro:string);
begin
        Self.Pis:=parametro;
end;

function TObjEMPRESA.Get_Pis:string;
begin
        Result:=Self.Pis;
end;

procedure TObjEMPRESA.Submit_Cofins(parametro:string);
begin
        Self.Cofins:=parametro;
end;

function TObjEMPRESA.Get_Cofins:string;
begin
        result:=Self.Cofins;
end;

procedure TObjEMPRESA.Submit_CreditaCofins(parametro:string);
begin
        self.CreditaPisCofins:=parametro;
end;

function TObjEMPRESA.Get_CreditaPisCofins:string;
begin
        Result:=Self.CreditaPisCofins;
end;

procedure TObjEMPRESA.Submit_CreditaIPI(parametro:string);
begin
        Self.CreditaIPI:=parametro;
end;

function TObjEMPRESA.Get_CreditaIPI:string;
begin
        Result:=Self.CreditaIPI;
end;

procedure TObjEMPRESA.Submit_CreditaICMSCompra(parametro:string);
begin
        Self.CreditaICMSnaCompra:=parametro;
end;

function TObjEMPRESA.Get_CreditaICMSnaCompra:string;
begin
        Result:=Self.CreditaICMSnaCompra;
end;

procedure TObjEMPRESA.Submit_PagaIcmsnoSimples(parametro:string);
begin
        Self.PagaIcmsnoSimples:=parametro;
end;

function TObjEMPRESA.Get_PagaIcmsnoSimples:string;
begin
        Result:=Self.PagaIcmsnoSimples;
end;

procedure TObjEMPRESA.Submit_icmsantecipadotodasdespesasnf(parametro:string);
begin
       self.icmsantecipadotodasdespesasnf:=parametro;
end;

function TObjEMPRESA.Get_icmsantecipadotodasdespesasnf:string;
begin
       Result:=Self.icmsantecipadotodasdespesasnf;
end;

procedure TObjEMPRESA.Submit_AliquotaISS(parametro:string);
begin
      self.AliquotaISS:=parametro;
end;

function TObjEMPRESA.Get_AliquotaISS:string;
begin
      Result:=Self.AliquotaISS;
end;

procedure TObjEMPRESA.Submit_AliquotaSimples(parametro:string);
begin
     Self.AliquotaSimples:=parametro;
end;

function TObjEMPRESA.Get_AliquotaSimples:string;
begin
     Result:=Self.AliquotaSimples;
end;

procedure TObjEMPRESA.Submit_Aliquotaiss_substituto(parametro:string);
begin
     self.aliquotaiss_substituto:=parametro;
end;

function TObjEMPRESA.Get_aliquotaiss_substituto:string;
begin
     Result:=Self.aliquotaiss_substituto;
end;

procedure TObjEMPRESA.Submit_SubstitutoTributario(parametro:string);
begin
     self.SubstitutoTributario:=parametro;
end;

function TObjEMPRESA.Get_SubstitutoTributario:string;
begin
     Result:=self.SubstitutoTributario;
end;

procedure TObjEMPRESA.Submit_AliquotaICMSGarantido(parametro:string);
begin
     Self.AliquotaICMSGarantido:=parametro;
end;

function TObjEMPRESA.Get_AliquotaICMSGarantido:string;
begin
     result:=Self.AliquotaICMSGarantido;
end;

procedure TObjEMPRESA.Submit_ICMSGarantido(parametro:String);
begin
    self.IcmsGarantido:=parametro;
end;

function TObjEMPRESA.Get_IcmsGarantido:string;
begin
    Result:=Self.IcmsGarantido;
end;

procedure TObjEMPRESA.Submit_utilizaicmssubstrecolhido_custo(parametro:string);
begin
    Self.utilizaicmssubstrecolhido_custo:=parametro;
end;

function TObjEMPRESA.Get_utilizaicmssubstrecolhido_custo:string;
begin
    Result:=Self.utilizaicmssubstrecolhido_custo;
end;

procedure TObjEMPRESA.Submit_FraseNFe(parametro:string);
begin
   FraseNotaFiscal:=parametro;
end;

function TObjEMPRESA.Get_FraseNFe:string;
begin
  Result:=FraseNotaFiscal;
end;

end.



