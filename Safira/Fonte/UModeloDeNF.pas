unit UModeloDeNF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,UobjModeloNF,DB;

type
  TFModeloDeNF = class(TForm)
    pnlrodape: TPanel;
    lbquantidade: TLabel;
    bt1: TBitBtn;
    bt2: TBitBtn;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btexcluir: TBitBtn;
    btrelatorios: TBitBtn;
    btOpcoes: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    lbLbCODIGO: TLabel;
    lbLbMODELONF: TLabel;
    lbLbQuantidadeProdutos: TLabel;
    lb1: TLabel;
    lbCodigoFiscal: TLabel;
    edtCODIGO: TEdit;
    edtQuantidadeProdutos: TEdit;
    cbbNotaFatura: TComboBox;
    edtCodigoFiscal: TEdit;
    edtmodelo: TEdit;
    btpesquisar: TBitBtn;
    imgmRodape: TImage;
    procedure FormShow(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
  private
    function AtualizaQuantidade:string;
    function TabelaParaControles: Boolean;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;


  public
    { Public declarations }
  end;

var
  FModeloDeNF: TFModeloDeNF;
  objMODELONF: TObjMODELONF;

implementation

uses UescolheImagemBotao,UessencialGlobal, Upesquisa;

{$R *.dfm}

procedure TFModeloDeNF.FormShow(Sender: TObject);
begin
    limpaedit(Self);
    desabilita_campos(Self);

    Try
      ObjMODELONF:=TObjMODELONF.create;
    Except
      Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
      Self.close;
    End;
    FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
    FescolheImagemBotao.PegaFiguraImagem(ImgmRodape,'RODAPE');
    retira_fundo_labels(self);
    Self.Color:=clwhite;
    lbquantidade.Caption:=AtualizaQuantidade;

    if (self.Tag <> 0) then
    begin

      if (objMODELONF.LocalizaCodigo (IntToStr (Self.Tag))) then
      begin

        objMODELONF.TabelaparaObjeto;
        Self.ObjetoParaControles;

      end;

    end;
end;

function TFModeloDeNF.AtualizaQuantidade:string;
begin
    result:='Existem '+ ContaRegistros('TABMODELONF','codigo') + ' Modelos Cadastrados';
   // Result :='0';
end;


procedure TFModeloDeNF.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

       Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjMODELONF.Get_pesquisa,'',Nil)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjMODELONF.status<>dsinactive
                                  then exit;

                                  If (ObjMODELONF.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjMODELONF.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
            End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;

function TFMODELODENF.TabelaParaControles: Boolean;
begin
     If (ObjMODELONF.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;

function TFModeloDeNF.ControlesParaObjeto:Boolean;
Begin
  Try
    With ObjMODELONF do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_MODELO(edtMODELO.text);
        Submit_QuantidadeProdutos(edtQuantidadeProdutos.text);
        Submit_NotaFatura(Submit_ComboBox(CBbNotaFatura));
        Submit_CODIGO_FISCAL(EdtCodigoFiscal.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFModeloDeNF.ObjetoParaControles:Boolean;
Begin
  Try
     With ObjMODELONF do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtMODELO.text:=Get_MODELO;
        EdtQuantidadeProdutos.text:=Get_QuantidadeProdutos;
        EdtCodigoFiscal.Text:=Get_CODIGO_FISCAL;
        If(Get_NotaFatura='S')
        Then CBbNotaFatura.ItemIndex := 1
        Else cbbNotaFatura.ItemIndex := 0;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
End;



procedure TFModeloDeNF.btsairClick(Sender: TObject);
begin
      Self.Close;
end;

procedure TFModeloDeNF.btnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;


     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     ObjMODELONF.status:=dsInsert;
     EdtCodigoFiscal.setfocus;
end;

procedure TFModeloDeNF.btalterarClick(Sender: TObject);
begin
      If (ObjMODELONF.Status=dsinactive) and (EdtCodigo.text<>'')
      Then
      Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjMODELONF.Status:=dsEdit;
                edtmodelo.setfocus;
                esconde_botoes(Self);
                Btgravar.Visible:=True;
                BtCancelar.Visible:=True;
                btpesquisar.Visible:=True;
      End;
end;

procedure TFModeloDeNF.btgravarClick(Sender: TObject);
begin
     If ObjMODELONF.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjMODELONF.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjMODELONF.Get_codigo;
     mostra_botoes(Self);
     desabilita_campos(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFModeloDeNF.btcancelarClick(Sender: TObject);
begin
    ObjMODELONF.cancelar;

     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);
end;

procedure TFModeloDeNF.btexcluirClick(Sender: TObject);
begin
      If (ObjMODELONF.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjMODELONF.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjMODELONF.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

end.
