unit UobjNFE;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
;
//USES_INTERFACE


Type
   TObjNFE=class

          Public

                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_STATUSNOTA(parametro: string);
                Function Get_STATUSNOTA: string;
                Procedure Submit_CERTIFICADO(parametro: string);
                Function Get_CERTIFICADO: string;
                Procedure Submit_ARQUIVO(parametro: String);
                Function Get_ARQUIVO: String;


                Procedure Submit_PROTOCOLOCANCELAMENTO(parametro: String);
                Function Get_PROTOCOLOCANCELAMENTO: String;


                Procedure Submit_PROTOCOLOinutilizacao(parametro: String);
                Function Get_PROTOCOLOinutilizacao: String;

                Procedure Submit_ReciboEnvio(parametro: String);
                Function Get_ReciboEnvio: String;



                Procedure Submit_ARQUIVOCANCELAMENTO(parametro: String);
                Function Get_ARQUIVOCANCELAMENTO: String;

                Procedure Submit_ARQUIVOinutilizacao(parametro: String);
                Function Get_ARQUIVOinutilizacao: String;

                procedure submit_chave_acesso (parametro:string);
                function get_chave_acesso ():string;

                procedure submit_arquivo_xml (parametro:string);
                function get_arquivo_xml ():string;

                Function Get_DataCancelamento:String;
                Function Get_MotivoCancelamento:String;

                Function Get_Datainutilizacao:String;
                Function Get_Motivoinutilizacao:String;

                Procedure Submit_DataCancelamento(parametro:String);
                Procedure Submit_MotivoCancelamento(parametro:String);

                Procedure Submit_Datainutilizacao(parametro:String);
                Procedure Submit_Motivoinutilizacao(parametro:String);
                                
                //**************************************************************
                Function ExtraiArquivoNFe:boolean;overload;
                function ExtraiArquivoCancelamentoNFe: boolean;overload;
                function ExtraiArquivoinutilizacaoNFe: boolean;overload;

                Function ExtraiArquivoNFe(path:string):boolean;overload;
                function ExtraiArquivoCancelamentoNFe(path:string): boolean;overload;
                function ExtraiArquivoinutilizacaoNFe(path:string): boolean;overload;
                function GET_CAMINHO_DOS_ARQUIVOS_XML () :string;
                function GET_CAMINHO_DOS_ARQUIVOS_INUTILIZACAO () :string;

                procedure edtNfeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
                procedure extraiChaveKeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
                procedure extraiChave(Sender: TObject);overload;
                function extraiChave(path:string):string;overload;



                //**************************************************************

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               STATUSNOTA:string;
               CERTIFICADO:string;
               ARQUIVO:String;
               PROTOCOLOCANCELAMENTO:String;
               ReciboEnvio:String;
               ARQUIVOCANCELAMENTO:String;

               DataCancelamento:String;
               MotivoCancelamento:String;


               MotivoInutilizacao:String;
               DataInutilizacao:String;
               ProtocoloInutilizacao:String;
               ArquivoInutilizacao:String;
               chave_acesso:string;
               arquivo_xml:string;

               //ArquivoInutilizacaoB BLOB SUB_TYPE 0 SEGMENT SIZE 80;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UComponentesNfe;





Function  TObjNFE.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.STATUSNOTA:=fieldbyname('STATUSNOTA').asstring;
        Self.CERTIFICADO:=fieldbyname('CERTIFICADO').asstring;
        Self.ARQUIVO:=fieldbyname('ARQUIVO').asstring;

        Self.ReciboEnvio:=fieldbyname('ReciboEnvio').asstring;

        Self.PROTOCOLOCANCELAMENTO:=fieldbyname('PROTOCOLOCANCELAMENTO').asstring;
        Self.ARQUIVOCANCELAMENTO:=fieldbyname('ARQUIVOCANCELAMENTO').asstring;
        Self.DataCancelamento:=Fieldbyname('datacancelamento').asstring;
        Self.MotivoCancelamento:=Fieldbyname('MotivoCancelamento').asstring;


        Self.PROTOCOLOinutilizacao:=fieldbyname('PROTOCOLOinutilizacao').asstring;
        Self.ARQUIVOinutilizacao:=fieldbyname('ARQUIVOinutilizacao').asstring;
        Self.chave_acesso:=fieldbyname('chave_acesso').AsString;
        self.arquivo_xml:=fieldbyname('arquivo_xml').AsString;

        Self.Datainutilizacao:=Fieldbyname('datainutilizacao').asstring;
        Self.Motivoinutilizacao:=Fieldbyname('Motivoinutilizacao').asstring;


        result:=True;
     End;
end;


Procedure TObjNFE.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin

  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('STATUSNOTA').asstring:=Self.STATUSNOTA;
        ParamByName('CERTIFICADO').asstring:=Self.CERTIFICADO;
        ParamByName('ARQUIVO').asstring:=Self.ARQUIVO;

        ParamByName('ReciboEnvio').asstring:=Self.ReciboEnvio;


        if (Self.arquivo_xml <> '')
        Then begin
                if  FileExists(Self.arquivo_xml)
                Then ParamByName('ARQUIVOB').LoadFromFile(Self.arquivo_xml,ftBlob);
        End;

        ParamByName('ARQUIVOCANCELAMENTO').asstring:=Self.ARQUIVOCANCELAMENTO;
        ParamByName('PROTOCOLOCANCELAMENTO').asstring:=Self.PROTOCOLOCANCELAMENTO;
        ParamByName('datacancelamento').asstring:=Self.DataCancelamento;
        ParamByName('MotivoCancelamento').asstring:=Self.MotivoCancelamento;

        if (Self.ARQUIVOCANCELAMENTO<>'')
        Then Begin
                  if FileExists(Self.ARQUIVOCANCELAMENTO)
                  then ParamByName('ARQUIVOCANCELAMENTOB').LoadFromFile(Self.ARQUIVOCANCELAMENTO,ftBlob);
        End;

        ParamByName('ARQUIVOinutilizacao').asstring:=Self.ARQUIVOinutilizacao;
        ParamByName('chave_acesso').asstring:=Self.chave_acesso;
        ParamByName('arquivo_xml').asstring:=Self.arquivo_xml;
        ParamByName('PROTOCOLOinutilizacao').asstring:=Self.PROTOCOLOinutilizacao;
        ParamByName('datainutilizacao').asstring:=Self.Datainutilizacao;
        ParamByName('Motivoinutilizacao').asstring:=Self.Motivoinutilizacao;
        
        if (Self.ARQUIVOinutilizacao<>'')
        Then Begin
                  if FileExists(Self.ARQUIVOinutilizacao)
                  then ParamByName('ARQUIVOinutilizacaoB').LoadFromFile(Self.ARQUIVOinutilizacao,ftBlob);
        End;
  End;
End;

//***********************************************************************

function TObjNFE.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjNFE.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        STATUSNOTA:='';
        CERTIFICADO:='';
        ARQUIVO:='';
        //ARQUIVOB:='';

        ReciboEnvio:='';
        ARQUIVOCANCELAMENTO:='';
        PROTOCOLOCANCELAMENTO:='';
        DataCancelamento:='';
        MotivoCancelamento:='';

        ARQUIVOinutilizacao:='';
        chave_acesso:='';
        arquivo_xml:='';
        PROTOCOLOinutilizacao:='';
        Datainutilizacao:='';
        Motivoinutilizacao:='';
//CODIFICA ZERARTABELA








     End;
end;

Function TObjNFE.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      
      If (STATUSNOTA='')
      Then Mensagem:=mensagem+'/Status da Nota';

      if (trim(comebarra(DataCancelamento))='')
      Then Self.DataCancelamento:='';

      if (trim(comebarra(Datainutilizacao))='')
      Then Self.Datainutilizacao:='';

      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjNFE.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjNFE.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjNFE.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';

     if (Self.DataCancelamento<>'')
     Then Begin
               Try
                  strtodate(Self.DataCancelamento);
               Except
                     Mensagem:=Mensagem+'Data de Cancelamento';
               End;
     end;


     if (Self.Datainutilizacao<>'')
     Then Begin
               Try
                  strtodate(Self.Datainutilizacao);
               Except
                     Mensagem:=Mensagem+'Data de Inutiliza��o';
               End;
     end;

//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjNFE.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjNFE.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro NFE vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close; 
           Sql.Clear;
           SQL.ADD('Select CODIGO,STATUSNOTA,CERTIFICADO,ARQUIVO,ReciboEnvio,ARQUIVOB,PROTOCOLOCANCELAMENTO');
           SQL.ADD(',ARQUIVOCANCELAMENTO,ARQUIVOCANCELAMENTOB,DataCancelamento,MotivoCancelamento');
           SQL.ADD(',DataInutilizacao,ProtocoloInutilizacao,ArquivoInutilizacao,MotivoInutilizacao,ARQUIVOinutilizacaoB,chave_acesso,arquivo_xml from  TABNFE');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjNFE.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjNFE.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjNFE.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABNFE(CODIGO,STATUSNOTA,CERTIFICADO,ARQUIVO');
                InsertSQL.add(' ,ReciboEnvio,ARQUIVOB,PROTOCOLOCANCELAMENTO,ARQUIVOCANCELAMENTO');
                InsertSQL.add(' ,ARQUIVOCANCELAMENTOB,DataCancelamento,MotivoCancelamento,DataInutilizacao,ProtocoloInutilizacao,ArquivoInutilizacao,MotivoInutilizacao,ARQUIVOinutilizacaoB,chave_acesso,arquivo_xml)');
                InsertSQL.add('values (:CODIGO,:STATUSNOTA,:CERTIFICADO,:ARQUIVO,:ReciboEnvio,:ARQUIVOB');
                InsertSQL.add(' ,:PROTOCOLOCANCELAMENTO,:ARQUIVOCANCELAMENTO,:ARQUIVOCANCELAMENTOB,:DataCancelamento,:MotivoCancelamento');
                InsertSQL.add(',:DataInutilizacao,:ProtocoloInutilizacao,:ArquivoInutilizacao,:MotivoInutilizacao,:ARQUIVOinutilizacaoB,:chave_acesso,:arquivo_xml)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABNFE set CODIGO=:CODIGO,STATUSNOTA=:STATUSNOTA');
                ModifySQL.add(',CERTIFICADO=:CERTIFICADO,ARQUIVO=:ARQUIVO,ARQUIVOB=:ARQUIVOB');
                ModifySQL.add(',PROTOCOLOCANCELAMENTO=:PROTOCOLOCANCELAMENTO,ARQUIVOCANCELAMENTO=:ARQUIVOCANCELAMENTO');
                ModifySQL.add(',ARQUIVOCANCELAMENTOB=:ARQUIVOCANCELAMENTOB,DataCancelamento=:DataCancelamento,MotivoCancelamento=:MotivoCancelamento');
                ModifySQL.add(',ReciboEnvio=:ReciboEnvio');
                ModifySQL.add(',DataInutilizacao=:DataInutilizacao,ProtocoloInutilizacao=:ProtocoloInutilizacao,ArquivoInutilizacao=:ArquivoInutilizacao,MotivoInutilizacao=:MotivoInutilizacao,ARQUIVOinutilizacaoB=:ARQUIVOinutilizacaoB');
                ModifySQl.Add(',chave_acesso=:chave_acesso,arquivo_xml=:arquivo_xml');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL
                //,DataInutilizacao,ProtocoloInutilizacao,ArquivoInutilizacao,MotivoInutilizacao
                DeleteSQL.clear;
                DeleteSql.add('Delete from TABNFE where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjNFE.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjNFE.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabNFE');
     Result:=Self.ParametroPesquisa;
end;

function TObjNFE.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de NFE ';
end;


function TObjNFE.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENNFE,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENNFE,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjNFE.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjNFE.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjNFE.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjNFE.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjNFE.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjNFE.Submit_STATUSNOTA(parametro: string);
begin
        Self.STATUSNOTA:=Parametro;
end;
function TObjNFE.Get_STATUSNOTA: string;
begin
        Result:=Self.STATUSNOTA;
end;
procedure TObjNFE.Submit_CERTIFICADO(parametro: string);
begin
        Self.CERTIFICADO:=Parametro;
end;
function TObjNFE.Get_CERTIFICADO: string;
begin
        Result:=Self.CERTIFICADO;
end;
procedure TObjNFE.Submit_ARQUIVO(parametro: String);
begin
        Self.ARQUIVO:=Parametro;
end;
function TObjNFE.Get_ARQUIVO: String;
begin
        Result:=Self.ARQUIVO;
end;

procedure TObjNFE.Submit_PROTOCOLOCANCELAMENTO(parametro: String);
begin
        Self.PROTOCOLOCANCELAMENTO:=Parametro;
end;
function TObjNFE.Get_PROTOCOLOCANCELAMENTO: String;
begin
        Result:=Self.PROTOCOLOCANCELAMENTO;
end;


//*********************************************
procedure TObjNFE.Submit_PROTOCOLOinutilizacao(parametro: String);
begin
        Self.PROTOCOLOinutilizacao:=Parametro;
end;
function TObjNFE.Get_PROTOCOLOinutilizacao: String;
begin
        Result:=Self.PROTOCOLOinutilizacao;
end;



procedure TObjNFE.Submit_ReciboEnvio(parametro: STRing);
begin
        Self.ReciboEnvio:=Parametro;
end;

function TObjNFE.Get_ReciboEnvio: STRing;
begin
        Result:=Self.ReciboEnvio;
end;

procedure TObjNFE.Submit_ARQUIVOCANCELAMENTO(parametro: String);
begin
        Self.ARQUIVOCANCELAMENTO:=Parametro;
end;
function TObjNFE.Get_ARQUIVOCANCELAMENTO: String;
begin
        Result:=Self.ARQUIVOCANCELAMENTO;
end;



procedure TObjNFE.Submit_ARQUIVOinutilizacao(parametro: String);
begin
        Self.ARQUIVOinutilizacao:=Parametro;
end;

function TObjNFE.Get_ARQUIVOinutilizacao: String;
begin
        Result:=Self.ARQUIVOinutilizacao;
end;



function TObjNFE.ExtraiArquivoNFe: boolean;
Begin
     result:=Self.ExtraiArquivoNFe('');
     //result:=self.ExtraiArquivoNFe(self.arquivo_xml);
End;

function TObjNFE.ExtraiArquivoNFe(path:string): boolean;
var
   MemoryStream:TMemoryStream;
begin
     result:=False;

     try
        MemoryStream:=TMemoryStream.Create;
     except
        MensagemErro('Erro na tentativa de criar a Memory Stream');
        exit;
     end;

     Try
        Try

           deletefile(RetornaPastaSistema+'ARQUIVOB.XML');

        Except

        End;

        Try
           MemoryStream.LoadFromStream(Self.Objquery.CreateBlobStream(Self.Objquery.FieldByName('ARQUIVOB'),bmRead));

           if (path='')
           Then path:=RetornaPastaSistema+'ARQUIVOB.XML';

           MemoryStream.SaveToFile(path);
           
           Result:=True;
        Except
              Mensagemerro('Erro na tentativa de gravar o arquivo '+path);
              exit;
        End;
        
     finally
        freeandnil(memorystream);
     end;
end;

function TObjNFE.ExtraiArquivoCancelamentoNFe: boolean;
Begin
     Result:=Self.ExtraiArquivoCancelamentoNFe('');
End;

function TObjNFE.ExtraiArquivoCancelamentoNFe(path:string): boolean;
var
   MemoryStream:TMemoryStream;
begin
     result:=False;

     try
        MemoryStream:=TMemoryStream.Create;
     except
        MensagemErro('Erro na tentativa de criar a Memory Stream');
        exit;
     end;

     Try
        Try
           deletefile(RetornaPastaSistema+'ARQUIVOCANCELAMENTOB.XML');
        Except

        End;

        Try
           MemoryStream.LoadFromStream(Self.Objquery.CreateBlobStream(Self.Objquery.fieldbyname('ARQUIVOCANCELAMENTOB'),bmRead));
           
           if (path='')
           Then path:=RetornaPastaSistema+'ARQUIVOCANCELAMENTOB.XML';
           
           MemoryStream.SaveToFile(path);
           Result:=True;
        Except
              Mensagemerro('Erro na tentativa de gravar o arquivo '+path);
              exit;
        End;

     finally
        freeandnil(memorystream);
     end;
end;


function TObjNFE.ExtraiArquivoinutilizacaoNFe: boolean;
Begin
     Result:=Self.ExtraiArquivoinutilizacaoNFe('');
End;

function TObjNFE.ExtraiArquivoinutilizacaoNFe(path:String): boolean;
var
   MemoryStream:TMemoryStream;
begin
     result:=False;

     try
        MemoryStream:=TMemoryStream.Create;
     except
        MensagemErro('Erro na tentativa de criar a Memory Stream');
        exit;
     end;

     Try
         Try
           deletefile(RetornaPastaSistema+'ARQUIVOINUTILIZACAOB.XML');
        Except

        End;

        Try
           MemoryStream.LoadFromStream(Self.Objquery.CreateBlobStream(Self.Objquery.fieldbyname('ARQUIVOinutilizacaoB'),bmRead));

           if (path='')
           Then path:=RetornaPastaSistema+'ARQUIVOINUTILIZACAOB.XML';

           MemoryStream.SaveToFile(path);
           Result:=True;
        Except
              Mensagemerro('Erro na tentativa de gravar o arquivo '+path);
              exit;
        End;

     finally
        freeandnil(memorystream);
     end;
end;


function TObjNFE.Get_DataCancelamento: String;
begin
     Result:=Self.DataCancelamento;
end;

function TObjNFE.Get_MotivoCancelamento: String;
begin
     Result:=Self.MotivoCancelamento;
end;

procedure TObjNFE.Submit_DataCancelamento(parametro: String);
begin
     Self.DataCancelamento:=Parametro;
end;

procedure TObjNFE.Submit_MotivoCancelamento(parametro: String);
begin
     Self.MotivoCancelamento:=Parametro;
end;




function TObjNFE.Get_Datainutilizacao: String;
begin
     Result:=Self.Datainutilizacao;
end;

function TObjNFE.Get_Motivoinutilizacao: String;
begin
     Result:=Self.Motivoinutilizacao;
end;

procedure TObjNFE.Submit_Datainutilizacao(parametro: String);
begin
     Self.Datainutilizacao:=Parametro;
end;

procedure TObjNFE.Submit_Motivoinutilizacao(parametro: String);
begin
     Self.Motivoinutilizacao:=Parametro;
end;

function TObjNFE.get_chave_acesso: string;
begin

  Result := Self.chave_acesso;

end;

procedure TObjNFE.submit_chave_acesso(parametro: string);
begin

  self.chave_acesso := parametro;

end;

function TObjNFE.GET_CAMINHO_DOS_ARQUIVOS_XML: string;
begin

  if (ObjParametroGlobal.ValidaParametro('NFE CAMINHO DOS ARQUIVOS XML')=False) then
  begin

     MensagemAviso ('Par�metro: "NFE CAMINHO DOS ARQUIVOS XML" n�o encontrado');
     Exit;

  end;

  Result := ObjParametroGlobal.Get_Valor();

  if (Result = '') then
  begin

    MensagemAviso ('Par�metro: "NFE CAMINHO DOS ARQUIVOS XML" vazio ');
    Exit;

  end;

  if (Result[Length(result)] <> '\') then
    result:=result+'\';

end;

function TObjNFE.get_arquivo_xml: string;
begin

  Result := Self.arquivo_xml;

end;

procedure TObjNFE.submit_arquivo_xml(parametro: string);
begin

  Self.arquivo_xml := parametro;

end;

function TObjNFE.GET_CAMINHO_DOS_ARQUIVOS_INUTILIZACAO: string;
begin

  if (ObjParametroGlobal.ValidaParametro('NFE CAMINHO DOS ARQUIVOS DE INUTILIZACAO')=False) then
  begin

     MensagemAviso ('Par�metro: "NFE CAMINHO DOS ARQUIVOS DE INUTILIZACAO" n�o encontrado');
     Exit;

  end;

  Result := ObjParametroGlobal.Get_Valor();

  if (Result = '') then
  begin

    MensagemAviso ('Par�metro: "NFE CAMINHO DOS ARQUIVOS DE INUTILIZACAO" vazio ');
    Exit;

  end;

  if (Result[Length(result)] <> '\') then
    result:=result+'\';

end;

procedure TObjNFE.edtNfeKeyDown( Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:TFpesquisa;
begin

   if Key <> VK_f9 then
   Exit;


    Try
           FpesquisaLocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,Self.Get_TituloPesquisa,Nil)=True) then
            begin

                  Try

                    If (FpesquisaLocal.showmodal=mrok) Then
                    Begin


                              if (Self.status <> dsinactive)
                              then exit;

                              If (Self.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                              Then Begin
                                        Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                        exit;
                              End;

                              Self.ZerarTabela;
                              Self.TabelaparaObjeto;

                              TEdit(sender).Text := self.CODIGO;

                    End;

                  Finally
                    FpesquisaLocal.QueryPesq.close;
                  End;
            End;

    Finally
           FreeandNil(FPesquisaLocal);
    End;



end;


procedure TObjNFE.extraiChaveKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
  openDialog1:TOpenDialog;
begin

  if (Key = VK_F9) then
  begin

      openDialog1:=TOpenDialog.Create (nil);

      TRY

          openDialog1.Filter:='Arquivos XML (*.xml)';

          if (openDialog1.Execute ()) then
            TEdit (sender).Text:=RetornaValorCampos(openDialog1.FileName,'chNFe');


      FINALLY

          FreeAndNil (openDialog1);

      END;

  end;

end;

procedure TObjNFE.extraiChave(Sender: TObject);
var
  openDialog1:TOpenDialog;
begin


    openDialog1:=TOpenDialog.Create (nil);

    TRY

        openDialog1.Filter:='Arquivos XML (*.xml)';

        if (openDialog1.Execute ()) then
          TEdit (sender).Text:=RetornaValorCampos(openDialog1.FileName,'chNFe');


    FINALLY

        FreeAndNil (openDialog1);

    END;



end;

function TObjNFE.extraiChave(path: string):string;
begin
  result := RetornaValorCampos(path,'chNFe');
end;


end.



