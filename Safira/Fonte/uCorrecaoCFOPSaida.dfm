object fCorrecaoCFOPSaida: TfCorrecaoCFOPSaida
  Left = 450
  Top = 230
  Width = 852
  Height = 500
  Caption = 'Corre'#231#227'o CFOP, CSOSN'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 161
    Width = 836
    Height = 113
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 14
      Width = 45
      Height = 13
      Caption = 'Produto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbCodigoProduto: TLabel
      Left = 68
      Top = 14
      Width = 508
      Height = 13
      AutoSize = False
      Caption = 
        '99999999 - xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' +
        'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    end
    object Label15: TLabel
      Left = 20
      Top = 41
      Width = 77
      Height = 13
      Caption = 'Valor produto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = Label15DblClick
    end
    object lbValorProduto: TLabel
      Left = 20
      Top = 56
      Width = 63
      Height = 15
      AutoSize = False
      Caption = '9999,999,00'
    end
    object Label16: TLabel
      Left = 119
      Top = 41
      Width = 66
      Height = 13
      Caption = 'Quantidade'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = Label16DblClick
    end
    object lbQuantidadeProduto: TLabel
      Left = 119
      Top = 56
      Width = 57
      Height = 15
      AutoSize = False
      Caption = '999999999'
    end
    object Label17: TLabel
      Left = 207
      Top = 41
      Width = 55
      Height = 13
      Caption = 'Desconto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = Label17DblClick
    end
    object lbValorDescontoProduto: TLabel
      Left = 207
      Top = 56
      Width = 72
      Height = 15
      AutoSize = False
      Caption = '99999,9999,00'
    end
    object Label19: TLabel
      Left = 389
      Top = 41
      Width = 72
      Height = 13
      Caption = 'Redu'#231#227'o BC'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = Label19DblClick
    end
    object lbReducaoProduto: TLabel
      Left = 389
      Top = 56
      Width = 64
      Height = 15
      AutoSize = False
      Caption = '999,00%'
    end
    object Label21: TLabel
      Left = 478
      Top = 41
      Width = 74
      Height = 13
      Caption = 'Base c'#225'lculo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbValorBaseCalculoProduto: TLabel
      Left = 478
      Top = 58
      Width = 72
      Height = 13
      Caption = '99999,9999,00'
    end
    object Label23: TLabel
      Left = 569
      Top = 41
      Width = 47
      Height = 13
      Caption = 'Aliquota'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = Label23DblClick
    end
    object lbAliquotaProduto: TLabel
      Left = 569
      Top = 56
      Width = 78
      Height = 15
      AutoSize = False
      Caption = '99999,9999,00'
    end
    object Label25: TLabel
      Left = 657
      Top = 41
      Width = 31
      Height = 13
      Caption = 'ICMS'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbValorICMSProduto: TLabel
      Left = 657
      Top = 56
      Width = 78
      Height = 15
      AutoSize = False
      Caption = '99999,9999,00'
    end
    object Label9: TLabel
      Left = 20
      Top = 78
      Width = 33
      Height = 13
      Caption = 'CFOP'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = Label9DblClick
    end
    object Label11: TLabel
      Left = 119
      Top = 78
      Width = 43
      Height = 13
      Caption = 'CSOSN'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = Label11DblClick
    end
    object Label14: TLabel
      Left = 207
      Top = 78
      Width = 59
      Height = 13
      Caption = 'CST ICMS'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = Label14DblClick
    end
    object lbCFOP: TLabel
      Left = 20
      Top = 92
      Width = 27
      Height = 13
      Caption = '9.999'
    end
    object lbCSOSN: TLabel
      Left = 119
      Top = 92
      Width = 18
      Height = 13
      Caption = '999'
    end
    object lbCSTICMS: TLabel
      Left = 207
      Top = 92
      Width = 18
      Height = 13
      Caption = '999'
    end
    object Label18: TLabel
      Left = 295
      Top = 41
      Width = 61
      Height = 13
      Caption = 'Valor Final'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = Label18DblClick
    end
    object lbValorFinalProduto: TLabel
      Left = 296
      Top = 56
      Width = 72
      Height = 13
      Caption = '99999,9999,00'
    end
    object Label22: TLabel
      Left = 295
      Top = 78
      Width = 49
      Height = 13
      Caption = 'CODIGO'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbCodigoProdutoNF: TLabel
      Left = 295
      Top = 92
      Width = 18
      Height = 13
      Caption = '999'
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 836
    Height = 113
    ActivePage = TabSheet1
    Align = alTop
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI Symbol'
    Font.Style = []
    ParentFont = False
    Style = tsFlatButtons
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Dados da nota'
      object Label3: TLabel
        Left = 16
        Top = 8
        Width = 50
        Height = 15
        Caption = 'N'#250'mero'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Segoe UI Symbol'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbNumero: TLabel
        Left = 16
        Top = 24
        Width = 42
        Height = 15
        AutoSize = False
        Caption = '9999999'
      end
      object Label4: TLabel
        Left = 448
        Top = 8
        Width = 47
        Height = 15
        Caption = 'Modelo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Segoe UI Symbol'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbModeloNF: TLabel
        Left = 448
        Top = 24
        Width = 201
        Height = 15
        AutoSize = False
        Caption = '99 - xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
      end
      object Label6: TLabel
        Left = 121
        Top = 8
        Width = 53
        Height = 15
        Caption = 'Situa'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Segoe UI Symbol'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbSituacao: TLabel
        Left = 121
        Top = 24
        Width = 61
        Height = 15
        Caption = 'x - xxxxxxxxx'
      end
      object Label8: TLabel
        Left = 210
        Top = 8
        Width = 124
        Height = 15
        Caption = 'Nota complementar'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Segoe UI Symbol'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbNotaComplementar: TLabel
        Left = 210
        Top = 24
        Width = 108
        Height = 15
        AutoSize = False
        Caption = 'x - xxxxxxxxxxxxxxxxx'
      end
      object Label10: TLabel
        Left = 349
        Top = 50
        Width = 77
        Height = 15
        Caption = 'Base c'#225'lculo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Segoe UI Symbol'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = Label10DblClick
      end
      object lbValorBaseCalculo: TLabel
        Left = 349
        Top = 66
        Width = 78
        Height = 15
        AutoSize = False
        Caption = '99999,9999,00'
      end
      object Label12: TLabel
        Left = 349
        Top = 8
        Width = 50
        Height = 15
        Caption = 'Emiss'#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Segoe UI Symbol'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbDataEmissao: TLabel
        Left = 349
        Top = 24
        Width = 58
        Height = 15
        Caption = '99/99/9999'
      end
      object Label5: TLabel
        Left = 210
        Top = 50
        Width = 66
        Height = 15
        Caption = 'Valor Final'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Segoe UI Symbol'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = Label5Click
      end
      object lbValorNota: TLabel
        Left = 210
        Top = 66
        Width = 72
        Height = 15
        AutoSize = False
        Caption = '99999,9999,00'
      end
      object Label7: TLabel
        Left = 116
        Top = 50
        Width = 58
        Height = 15
        Caption = 'Desconto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Segoe UI Symbol'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = Label7DblClick
      end
      object lbValorDesconto: TLabel
        Left = 116
        Top = 66
        Width = 72
        Height = 15
        AutoSize = False
        Caption = '99999,9999,00'
      end
      object Label13: TLabel
        Left = 448
        Top = 50
        Width = 32
        Height = 15
        Caption = 'ICMS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Segoe UI Symbol'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = Label13DblClick
      end
      object lbValorICMS: TLabel
        Left = 448
        Top = 66
        Width = 78
        Height = 15
        AutoSize = False
        Caption = '99999,9999,00'
      end
      object Label24: TLabel
        Left = 16
        Top = 50
        Width = 65
        Height = 15
        Caption = 'Valor total'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Segoe UI Symbol'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = Label24DblClick
      end
      object lbValorTotal: TLabel
        Left = 16
        Top = 66
        Width = 72
        Height = 15
        AutoSize = False
        Caption = '99999,9999,00'
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Dados do cliente'
      ImageIndex = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 836
    Height = 48
    Align = alTop
    TabOrder = 2
    DesignSize = (
      836
      48)
    object Label2: TLabel
      Left = 16
      Top = 22
      Width = 56
      Height = 13
      Caption = 'Nota Fiscal'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI Symbol'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigoNota: TLabel
      Left = 742
      Top = 16
      Width = 72
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '00000000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Segoe UI Symbol'
      Font.Style = []
      ParentFont = False
    end
    object Label20: TLabel
      Left = 633
      Top = 12
      Width = 103
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Nota Fiscal'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI Symbol'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object editNota: TEdit
      Left = 75
      Top = 16
      Width = 87
      Height = 19
      Color = clSkyBlue
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 0
      OnExit = editNotaExit
      OnKeyDown = editNotaKeyDown
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 274
    Width = 836
    Height = 169
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object dbGridProdutos: TDBGrid
      Left = 0
      Top = 0
      Width = 836
      Height = 169
      Align = alClient
      Ctl3D = False
      DataSource = DataSource1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Segoe UI Symbol'
      TitleFont.Style = [fsBold]
      OnDblClick = dbGridProdutosDblClick
      OnKeyDown = dbGridProdutosKeyDown
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 443
    Width = 836
    Height = 19
    Panels = <>
  end
  object IBQuery1: TIBQuery
    Left = 712
    Top = 49
  end
  object DataSource1: TDataSource
    DataSet = IBQuery1
    Left = 680
    Top = 48
  end
end
