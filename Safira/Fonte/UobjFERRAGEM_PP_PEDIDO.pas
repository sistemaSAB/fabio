unit UobjFERRAGEM_PP_PEDIDO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJFERRAGEMCOR;

Type
   TObjFERRAGEM_PP_PEDIDO=class

          Public
                ObjDataSource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                FerragemCor:TOBJFERRAGEMCOR;

                Constructor Create;
                Destructor  Free;

                function    DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;PControlaNegativo: Boolean;Pdata:String): boolean;
                function    DiminuiAumentaEstoquePedido(NotaFiscal,PpedidoProjeto: String;PControlaNegativo: Boolean;Pdata:String;aumenta:String): boolean;
               // function    AumentaEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;Pdata:string): Boolean;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_PesquisaViewFerragemCor:TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure ResgataFERRAGEM_PP(PPedido, PPedidoProjeto:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Quantidade(parametro: string);
                Function Get_Quantidade: string;
                Procedure Submit_Valor(parametro: string);
                Function Get_Valor: string;

                Function  Get_ValorFinal:string;

                procedure EdtFerragemCorExit(Sender: TObject;Var PEdtCodigo:TEdit;LABELNOME:TLABEL);
                procedure EdtFerragemCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtFerragemCorViewKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;Var Key: Word;Shift: TShiftState;var LABELNOME:Tlabel);

                Function PreencheFerragemPP(PPedidoProjeto:string;var PValor:Currency;PprojetoemBRanco:Boolean):Boolean;
                Function DeletaFerragem_PP(PPedidoProjeto:string):Boolean;
                Function Soma_por_PP(PpedidoProjeto:string):Currency;
                Function ResgataValorFerragem(PCodigoFerregemCor:string):Currency;

                Procedure RetornaDadosRel(PpedidoProjeto:string;STrDados:TStrings;ComValor:Boolean);
                Procedure RetornaDadosRel2(PpedidoProjeto:string;STrDados:TStrings;ComValor:Boolean);
                Procedure RetornaDadosRelProjetoBranco(PpedidoProjeto:string;STrDados:TStrings; Pquantidade:integer;Var PTotal:Currency);

                function Get_PedidoProjeto:string;
                Procedure Submit_PedidoProjeto(parametro:string);
                procedure EdtFerragemCorExit_codigo(Sender: TObject;LABELNOME: TLABEL);
                function AlterCorFerragemPP(FerragemPP:String;PedidoProjeto:string;codigocor:string;commit:Boolean):Boolean;

         Private
               Objquery:Tibquery;
               ObjQueryPesquisa:TIBquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Quantidade:string;
               Valor:string;
               ValorFinal:string;
               PedidoProjeto:string;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                function ExcluiFerragemPP(PPedidoProjeto: string): Boolean;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UobjFERRAGEM, UmostraStringList;
{ TTabTitulo }


Function  TObjFERRAGEM_PP_PEDIDO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.PedidoProjeto:=fieldbyname('PedidoProjeto').asstring;

        If(FieldByName('FerragemCor').asstring<>'')
        Then Begin
                 If (Self.FerragemCor.LocalizaCodigo(FieldByName('FerragemCor').asstring)=False)
                 Then Begin
                          Messagedlg('FerragemCor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.FerragemCor.TabelaparaObjeto;
        End;
        Self.Quantidade:=fieldbyname('Quantidade').asstring;
        Self.Valor:=fieldbyname('Valor').asstring;
        Self.ValorFinal:=fieldbyname('ValorFinal').asstring;
        result:=True;
     End;
end;


Procedure TObjFERRAGEM_PP_PEDIDO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('PedidoProjeto').asstring:=Self.PedidoProjeto;
        ParamByName('FerragemCor').asstring:=Self.FerragemCor.GET_CODIGO;
        ParamByName('Quantidade').asstring:=virgulaparaponto(Self.Quantidade);
        ParamByName('Valor').asstring:=virgulaparaponto(Self.Valor);

  End;
End;

//***********************************************************************

function TObjFERRAGEM_PP_PEDIDO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjFERRAGEM_PP_PEDIDO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        PedidoProjeto:='';
        FerragemCor.ZerarTabela;
        Quantidade:='';
        Valor:='';
        ValorFinal:='';
     End;
end;

Function TObjFERRAGEM_PP_PEDIDO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjFERRAGEM_PP_PEDIDO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     If (Self.FerragemCor.LocalizaCodigo(Self.FerragemCor.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ FerragemCor n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjFERRAGEM_PP_PEDIDO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.PedidoProjeto<>'')
        Then Strtoint(Self.PedidoProjeto);
     Except
           Mensagem:=mensagem+'/PedidoProjeto';
     End;
     try
        If (Self.FerragemCor.Get_Codigo<>'')
        Then Strtoint(Self.FerragemCor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/FerragemCor';
     End;
     try
        Strtofloat(Self.Quantidade);
      
     Except
           Mensagem:=mensagem+'/Quantidade';
     End;
     try
        Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjFERRAGEM_PP_PEDIDO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjFERRAGEM_PP_PEDIDO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjFERRAGEM_PP_PEDIDO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro FERRAGEM_PP vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,PedidoProjeto,FerragemCor,Quantidade,Valor,ValorFinal');
           SQL.ADD(' from  TabFerragem_PP');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjFERRAGEM_PP_PEDIDO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjFERRAGEM_PP_PEDIDO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;

function TObjFERRAGEM_PP_PEDIDO.AlterCorFerragemPP(FerragemPP:String;PedidoProjeto:string;codigocor:string;commit:Boolean):Boolean;
begin
    Result:=True;
    self.LocalizaCodigo(FerragemPP);
    Self.TabelaparaObjeto;
   

end;


constructor TObjFERRAGEM_PP_PEDIDO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDataSource.DataSet:=Self.ObjQueryPesquisa;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.FerragemCor:=TOBJFERRAGEMCOR.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabFerragem_PP(Codigo,PedidoProjeto,FerragemCor');
                InsertSQL.add(' ,Quantidade,Valor)');
                InsertSQL.add('values (:Codigo,:PedidoProjeto,:FerragemCor,:Quantidade');
                InsertSQL.add(' ,:Valor)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabFerragem_PP set Codigo=:Codigo,PedidoProjeto=:PedidoProjeto');
                ModifySQL.add(',FerragemCor=:FerragemCor,Quantidade=:Quantidade,Valor=:Valor');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabFerragem_PP where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjFERRAGEM_PP_PEDIDO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjFERRAGEM_PP_PEDIDO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabFERRAGEM_PP');
     Result:=Self.ParametroPesquisa;
end;

function TObjFERRAGEM_PP_PEDIDO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de FERRAGEM_PP ';
end;


function TObjFERRAGEM_PP_PEDIDO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENFERRAGEM_PP,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENFERRAGEM_PP,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjFERRAGEM_PP_PEDIDO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(Self.ObjQueryPesquisa);
    Freeandnil(Self.ObjDataSource);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
Self.FerragemCor.FREE;
//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjFERRAGEM_PP_PEDIDO.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjFERRAGEM_PP_PEDIDO.RetornaCampoNome: string;
begin
      result:='Nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjFERRAGEM_PP_PEDIDO.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjFERRAGEM_PP_PEDIDO.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjFERRAGEM_PP_PEDIDO.Submit_Quantidade(parametro: string);
begin
        Self.Quantidade:=Parametro;
end;
function TObjFERRAGEM_PP_PEDIDO.Get_Quantidade: string;
begin
        Result:=Self.Quantidade;
end;
procedure TObjFERRAGEM_PP_PEDIDO.Submit_Valor(parametro: string);
begin
        Self.Valor:=Parametro;
end;
function TObjFERRAGEM_PP_PEDIDO.Get_Valor: string;
begin
        Result:=Self.Valor;
end;
//CODIFICA GETSESUBMITS


procedure TObjFERRAGEM_PP_PEDIDO.EdtFerragemCorExit(Sender: TObject;Var PEdtCodigo:TEdit; LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.FerragemCor.LocalizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     Self.FerragemCor.tabelaparaobjeto;
     LABELNOME.Caption:=Self.FerragemCor.Ferragem.Get_Descricao+' - '+Self.FerragemCor.Cor.Get_Descricao;
//     PEdtCodigo.Text:=Self.FerragemCor.Get_Codigo;

End;
procedure TObjFERRAGEM_PP_PEDIDO.EdtFerragemCorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.FerragemCor.Get_Pesquisa,Self.FerragemCor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FerragemCor.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.FerragemCor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FerragemCor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjFERRAGEM_PP_PEDIDO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJFERRAGEM_PP';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjFERRAGEM_PP_PEDIDO.ResgataFERRAGEM_PP(PPedido, PPEdidoProjeto: string);
begin
       With Self.ObjQueryPesquisa do
       Begin
           Close;
           SQL.Clear;
           Sql.add('Select  TabFerragem.Referencia, TabFerragem.Descricao as Ferragem,');
           Sql.add('TabCor.Descricao as Cor,');
           Sql.add('TabFerragem_PP.Quantidade,');
           Sql.add('TabFerragem_PP.Valor,TAbFerragem_PP.ValorFinal,');
           Sql.add('TabPedido_proj.Codigo as PedidoProjeto,');
           Sql.Add('TabFerragem_PP.Codigo');
           Sql.add('from TabFerragem_PP');
           Sql.add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabFerragem_PP.PedidoProjeto');
           Sql.add('Join TabPedido on TabPedido.Codigo = TabPedido_Proj.Pedido');
           Sql.add('Join TabProjeto on TabProjeto.Codigo = TabPedido_Proj.Projeto');
           Sql.add('Join TabFerragemCor on TabFerragemCor.Codigo = TabFerragem_PP.FerragemCor');
           Sql.add('join TabFerragem  on TabFerragem.Codigo = TabFerragemCor.Ferragem');
           Sql.add('Join TabCor on TabCor.Codigo = TabFerragemCor.Cor');

           Sql.add('Where TabPedido_Proj.Pedido = '+PPedido);
           if (PpedidoProjeto <> '' )
           then Sql.add('and   TabPedido_Proj.Codigo ='+PPedidoProjeto);
           
           if (PPedido = '')then
           exit;

           Open;

       end;
end;

Function TObjFERRAGEM_PP_PEDIDO.PreencheFerragemPP(PPedidoProjeto:string;var PValor:Currency;PprojetoemBRanco:Boolean):Boolean;
var
PcodigoProjeto,Pcorferragem:string;
PdescricaoCorferragem:String;
PtipoPedido:String;
begin
    Result:=false;
    Pvalor:=0;

    {//Localizo o Pedido Projeto
    if (Self.PedidoProjeto.LocalizaCodigo(PPedidoProjeto)=false)
    then exit;
    Self.PedidoProjeto.TabelaparaObjeto;
    //**********************************}

    With ObjQueryPesquisa do
    Begin
        close;
        sql.clear;
        sql.add('Select projeto,tipopedido from TabPedido_Proj where codigo='+PPedidoProjeto);
        open;
        if (recordcount=0)
        then exit;
        PcodigoProjeto:=fieldbyname('projeto').asstring;
        PtipoPedido:=fieldbyname('tipopedido').asstring;


        //Apaga Apenas as Ferragens ligadas a este Pedido Projeto
        { TODO -oCelio : Checar pq apaga }
        Self.DeletaFerragem_PP(PPedidoProjeto);

        if (PprojetoemBRanco=True)
        Then Begin
                  result:=True;
                  exit;
        End;

        close;
        sql.CLEAR;
        sql.add('Select TabPedido_Proj.CorFerragem,TabCor.descricao from TabPedido_Proj');
        sql.add('join tabcor on TabPedido_Proj.Corferragem=TabCor.codigo');
        sql.add('where tabpedido_proj.codigo='+PpedidoProjeto);
        open;
        Pcorferragem:=Fieldbyname('CorFerragem').asstring;
        PdescricaoCorferragem:=Fieldbyname('descricao').asstring;
        

        //esse sqls faz um relacionamento entre as ferragens de um projeto
        //e a tabela FerragemCor, para saber quais das ferragens de um determinado
        //projeto nao esto cadastar na COR X, esse sql abaixo retorna
        //as que estaum nesse caso, para testa-lo no ibconsole
        //retire o distinct e acrescente TabFerragemCor.Cor para visualizar
        //e retire a clausula where

        close;
        sql.clear;
        sql.add('Select distinct(TabFerragem_Proj.Ferragem)');
        sql.add('from TabFerragem_Proj');
        sql.add('left join TabFerragemCor on');
        sql.add('(TabFerragem_Proj.ferragem=TabFerragemCor.Ferragem and TabFerragemCor.Cor='+PcorFerragem+')');
        sql.add('Where TabFerragem_Proj.Projeto='+PcodigoProjeto);
        sql.add('and TabFerragemCor.Cor is null');
        open;
        last;
        if (recordcount>0)
        Then Begin
                  if (Messagedlg('Algumas Ferragens cadastradas para este projeto n�o existem na Cor Escolhida, n�o ser� poss�vel continuar.'+#13+'Deseja visualiza-las?',mtconfirmation,[mbyes,mbno],0)=mrno)
                  Then exit;
                  FmostraStringList.Memo.clear;
                  FmostraStringList.Memo.Lines.add('FERRAGENS N�O CADASTRADAS PARA A COR='+Pcorferragem+'-'+PdescricaoCorferragem);
                  FmostraStringList.Memo.Lines.add(' ');
                  first;
                  While not(eof) do
                  Begin
                       Self.FerragemCor.Ferragem.LocalizaCodigo(fieldbyname('ferragem').asstring);
                       Self.FerragemCor.Ferragem.TabelaparaObjeto;
                       FmostraStringList.Memo.Lines.add(Self.FerragemCor.Ferragem.Get_Codigo+' - '+Self.FerragemCor.Ferragem.Get_Descricao);
                       next;
                  End;
                  FmostraStringList.ShowModal;
                  exit;
        End;


        //Esse SQL Retorna o Codigo da FerragemCor,o PReco, e a quantidade (cadastrada no projeto)

        Close;
        SQL.Clear;
        SQL.Add('Select  TabFerragemCor.Codigo as FerragemCor, TabFerragem_Proj.Quantidade,');

        if (PTipoPedido='INSTALADO')
        Then SQL.Add('(tabFerragem.PRecoVendaInstalado+(TabFerragem.PrecoVendaInstalado*TabFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        if (PTipoPedido='RETIRADO')
        Then SQL.Add('(tabFerragem.PRecoVendaretirado+(TabFerragem.PrecoVendaretirado*TabFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        if (PTipoPedido='FORNECIDO')
        Then SQL.Add('(tabFerragem.PRecoVendafornecido+(TabFerragem.PrecoVendafornecido*TabFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        SQL.Add('from TabFerragem_Proj');
        SQL.Add('Join TabFerragemCor on TabFerragemCOr.Ferragem = TabFerragem_Proj.Ferragem');
        SQL.Add('join TabFerragem on TabFerragem.Codigo = TabFerragemCor.Ferragem');
        SQL.Add('Where TabFerragem_Proj.Projeto = '+PcodigoProjeto);
        SQL.Add('and   TabFerragemCor.Cor = '+Pcorferragem);
        Open;

        // Preenchendo a TabFerragm_PP
        While not (eof) do
        Begin
            Self.Status:=dsInsert;
            Self.Submit_Codigo(Self.Get_NovoCodigo);
            Self.Submit_PedidoProjeto(PPedidoProjeto);
            Self.FerragemCor.Submit_Codigo(fieldbyname('FerragemCor').AsString);
            Self.Submit_Quantidade(fieldbyname('Quantidade').AsString);
            Self.Submit_Valor(formata_valor(fieldbyname('Preco').AsString,2,True));
            if Self.Salvar(false)=false then
            Begin
                 Result:=false;
                 exit;
            end;
            Next;
        end;

        PValor:=0;
        Pvalor:=Self.Soma_por_PP(PPedidoProjeto);
        result:=true;
    end;
end;

function TObjFERRAGEM_PP_PEDIDO.ExcluiFerragemPP(PPedidoProjeto: string): Boolean;
Var  QueryLOcal : TIBQuery;
begin
     Result:=false;
     try
         QueryLOcal:=TIBQuery.Create(nil);
         QueryLOcal.Database:=FDataModulo.IBDatabase;
     except
         MensagemErro('Erro ao tentar criar a Query local');
         Result:=false;
         exit;
     end;

     With QueryLOcal  do
     Begin
          Close;
          SQL.Clear;
          SQL.Add('Delete from TabFerragem_PP' );
          SQL.Add('Where PedidoProjeto = '+PPedidoProjeto);


          if (PPedidoProjeto<>'')then
          Open;
     end;

     Self.Commit;
     Result:=true;

     if (QueryLOcal<>nil)then
     FreeAndNil(QueryLOcal);

end;

procedure TObjFERRAGEM_PP_PEDIDO.EdtFerragemCorViewKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;
  var Key: Word; Shift: TShiftState;Var  LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FTROCAMATERIALPEDIDOCONCLUIDO.BTTROCAR' ;
            FpesquisaLocal.PesquisaEmEstoque := True;
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaViewFerragemCor,Self.FerragemCor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('REF_FERRAGEM').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 LABELNOME.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('Ferragem').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
function TObjFERRAGEM_PP_PEDIDO.Get_PesquisaViewFerragemCor: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewFerragemCor');
     Result:=Self.ParametroPesquisa;
end;

function TObjFERRAGEM_PP_PEDIDO.DeletaFerragem_PP(PPedidoProjeto: string): Boolean;
begin
     Result:=False;
     With Objquery  do
     Begin
        Close;
        SQL.Clear;
        Sql.Add('Delete from TabFerragem_PP where PedidoProjeto='+ppedidoprojeto);
        Try
            execsql;
        Except
              On E:Exception do
              Begin
                   Messagedlg('Erro na tentativa de Excluir as Ferragens do Projeto '+#13+E.message,mterror,[mbok],0);
                   exit;
              End;
        End;
     end;
     Result:=true;
end;

function TObjFERRAGEM_PP_PEDIDO.Get_ValorFinal: string;
begin
     Result:=Self.ValorFinal;
end;


function TObjFERRAGEM_PP_PEDIDO.Soma_por_PP(PpedidoProjeto: string): Currency;
begin

     Self.Objquery.close;
     Self.Objquery.sql.clear;
     Self.Objquery.sql.add('Select sum(valorfinal) as SOMA from Tabferragem_PP where PedidoProjeto='+PpedidoProjeto);
     Try
         Self.Objquery.open;
         Result:=Self.Objquery.fieldbyname('soma').asfloat;
     Except
           Result:=0;
     End;

end;

function TObjFERRAGEM_PP_PEDIDO.ResgataValorFerragem(
  PCodigoFerregemCor: string): Currency;
begin
     With Objquery  do
     Begin
          Close;
          SQl.Clear;
          Sql.Add('Select ')
     end;
end;

procedure TObjFERRAGEM_PP_PEDIDO.RetornaDadosRel(PpedidoProjeto: string; STrDados: TStrings;ComValor:Boolean);
begin
     if (PpedidoProjeto='')
     Then exit;

     With Self.ObjQueryPesquisa do
     Begin

          close;
          SQL.clear;
          sql.add('Select codigo from TabFerragem_pp where PedidoProjeto='+PpedidoProjeto);
          open;
          first;

          if (RecordCount = 0)then
          exit;

          While not(eof) do
          Begin
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;

               if(ComValor=True) then
               begin

                   STrDados.Add(CompletaPalavra(Self.FerragemCor.Ferragem.Get_Referencia,10,' ')+' '+
                              CompletaPalavra(Self.FerragemCor.Cor.Get_Referencia+'-'+Self.FerragemCor.Cor.Get_Descricao,16,' ')+' '+
                              CompletaPalavra(Self.FerragemCor.Ferragem.Get_Descricao,45,' ')+' '+
                              CompletaPalavra(Self.Get_Quantidade+' '+Self.FerragemCor.Ferragem.Get_Unidade,6,' ')+' '+
                              CompletaPalavra_a_Esquerda(formata_valor(Self.Get_Valor),10,' ')+' '+
                              CompletaPalavra_a_Esquerda(formata_valor(StrToCurr(Self.Get_Quantidade)*StrToCurr(Self.Get_Valor)),8,' '));
               end
               else
               begin
                   STrDados.Add(CompletaPalavra(Self.FerragemCor.Ferragem.Get_Referencia,10,' ')+' '+
                              CompletaPalavra(Self.FerragemCor.Cor.Get_Referencia+'-'+Self.FerragemCor.Cor.Get_Descricao,16,' ')+' '+
                              CompletaPalavra(Self.FerragemCor.Ferragem.Get_Descricao,45,' ')+' '+
                              CompletaPalavra(Self.Get_Quantidade+' '+Self.FerragemCor.Ferragem.Get_Unidade,6,' '));
               end;
              next;
          end;
          if(ComValor=True) then
          begin
              close;
              sql.clear;
              sql.add('Select sum(ValorFinal) as SOMA FROM TabFerragem_PP where PedidoProjeto='+PpedidoProjeto);
              open;
              STrDados.Add('?'+CompletaPalavra_a_Esquerda('Total FERRAGENS: '+formata_valor(fieldbyname('soma').asstring),100,' '));
              STrDados.Add(' ');
          end;
     End;
end;

///*** N�O UTILIZA MAIS, POR CAUSA DA IMPLANTA��O DO ESTOQUE SAZIONAL
{function TObjFERRAGEM_PP_PEDIDO.AumentaEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;Pdata:string): Boolean;
var
PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          Sql.add('Select codigo,ferragemcor,quantidade from TabFerragem_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;
          first;
          While not(eof) do
          Begin
              Self.FerragemCor.LocalizaCodigo(fieldbyname('ferragemcor').asstring);
              Self.FerragemCor.TabelaparaObjeto;

              try
                 PestoqueAtual:=strtofloat(Self.ferragemCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual da ferragem '+Self.FerragemCor.Ferragem.Get_Descricao+' da cor '+Self.FerragemCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              Try
                PnovoEstoque:=PestoqueAtual+fieldbyname('quantidade').asfloat;
              Except
                    Messagedlg('Erro na gera��o do novo Estoque da ferragem '+Self.FerragemCor.Ferragem.Get_Descricao+' da cor '+Self.FerragemCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

             // self.FerragemCor.AumentaEstoque(fieldbyname('quantidade').asstring,fieldbyname('ferragemcor').asstring);
              next;
          End;
          result:=True;
          exit;
     End;
end;   }

function TObjFERRAGEM_PP_PEDIDO.DiminuiAumentaEstoquePedido(NotaFiscal,PpedidoProjeto: String;PControlaNegativo: Boolean;Pdata:String;aumenta:String): boolean;
var
PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          if(aumenta='N')
          then Sql.add('Select codigo,ferragemcor,(quantidade*-1) as quantidade from TabFerragem_PP where PedidoProjeto='+PpedidoProjeto)
          else Sql.add('Select codigo,ferragemcor,quantidade from TabFerragem_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;
          first;
          //Deve fazer um lan�amento de estoque na tabestoque
          While not(eof) do
          Begin
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Status:=dsinsert;
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(fieldbyname('quantidade').asstring);
              OBJESTOQUEGLOBAL.Submit_DATA(Pdata);
              OBJESTOQUEGLOBAL.Submit_FERRAGEMCOR(fieldbyname('ferragemcor').asstring);
              OBJESTOQUEGLOBAL.Submit_FERRAGEM_PP(fieldbyname('codigo').asstring);
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('PEDIDO (NOTA FISCAL)');
              OBJESTOQUEGLOBAL.Submit_NotaFiscal(NotaFiscal);

              Self.FerragemCor.LocalizaCodigo(fieldbyname('ferragemcor').asstring);
              Self.FerragemCor.TabelaparaObjeto;

              //Preciso ver se essa ferragem tem um estoque valido
              try
                 PestoqueAtual:=strtofloat(Self.ferragemCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual da ferragem '+Self.FerragemCor.Ferragem.Get_Descricao+' da cor '+Self.FerragemCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              //Calculo quanto ser� o novo toque
              Try
                PnovoEstoque:=PestoqueAtual+fieldbyname('quantidade').asfloat;
              Except
                    Messagedlg('Erro na gera��o do novo Estoque da ferragem '+Self.FerragemCor.Ferragem.Get_Descricao+' da cor '+Self.FerragemCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              //Caso o estoque fique negativo e se n�o pode vender com estoque negativo,
              //ent�o avisa e n�o deixa gerar nota fiscal
              if (Pnovoestoque<0)
              then Begin
                        if (PControlaNegativo=True)
                        Then begin
                                  Messagedlg('A ferragem '+Self.FerragemCor.Ferragem.Get_Descricao+' da cor '+Self.FerragemCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                  exit;
                        End;
              End;
              if (OBJESTOQUEGLOBAL.Salvar(False)=False)
              then exit;

              next;
          End;
          result:=True;
          exit;
     End;
end;


function TObjFERRAGEM_PP_PEDIDO.DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;PControlaNegativo: Boolean;Pdata:String): boolean;
var
PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          Sql.add('Select codigo,ferragemcor,(quantidade*-1) as quantidade from TabFerragem_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;
          first;
          While not(eof) do
          Begin
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Status:=dsinsert;
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(fieldbyname('quantidade').asstring);
              OBJESTOQUEGLOBAL.Submit_DATA(Pdata);
              OBJESTOQUEGLOBAL.Submit_FERRAGEMCOR(fieldbyname('ferragemcor').asstring);
              OBJESTOQUEGLOBAL.Submit_PEDIDOPROJETOROMANEIO(PpedidoProjetoRomaneio);
              OBJESTOQUEGLOBAL.Submit_FERRAGEM_PP(fieldbyname('codigo').asstring);
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('ROMANEIO');

              Self.FerragemCor.LocalizaCodigo(fieldbyname('ferragemcor').asstring);
              Self.FerragemCor.TabelaparaObjeto;

              try
                 PestoqueAtual:=strtofloat(Self.ferragemCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual da ferragem '+Self.FerragemCor.Ferragem.Get_Descricao+' da cor '+Self.FerragemCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              Try
                PnovoEstoque:=PestoqueAtual+fieldbyname('quantidade').asfloat;
              Except
                    Messagedlg('Erro na gera��o do novo Estoque da ferragem '+Self.FerragemCor.Ferragem.Get_Descricao+' da cor '+Self.FerragemCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              if (Pnovoestoque<0)
              then Begin
                        if (PControlaNegativo=True)
                        Then begin
                                  Messagedlg('A ferragem '+Self.FerragemCor.Ferragem.Get_Descricao+' da cor '+Self.FerragemCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                  exit;
                        End;
              End;
              //self.FerragemCor.DiminuiEstoque(fieldbyname('quantidade').asstring,fieldbyname('ferragemcor').asstring);
              if (OBJESTOQUEGLOBAL.Salvar(False)=False)
              then exit;

              next;
          End;
          result:=True;
          exit;
     End;
end;

procedure TObjFERRAGEM_PP_PEDIDO.RetornaDadosRelProjetoBranco(PpedidoProjeto: string; STrDados: TStrings;Pquantidade:integer; var PTotal: Currency);
begin
  if (PpedidoProjeto='') then
    exit;

  with Self.ObjQueryPesquisa do
  begin
    Close;
    SQL.clear;
    SQL.Text := ' select f.referencia, c.referencia || ' + QuotedStr('-') + ' || c.descricao as cor, '+
                ' f.descricao, f.unidade, fp.valor, sum(fp.quantidade) * cast(:qtde as numeric(9,2)) as quantidade, '+
                ' sum(fp.quantidade) * cast(:qtde as numeric(9,2)) * fp.valor as mult '+
                ' from tabferragem_pp fp '+
                ' inner join tabferragemcor fc on (fp.ferragemcor = fc.codigo) '+
                ' inner join tabcor c on (fc.cor = c.codigo) '+
                ' inner join tabferragem f on (fc.ferragem = f.codigo) '+
                ' where fp.pedidoprojeto = :pedidoprojeto '+
                ' group by 1,2,3,4,5 ';

    ParamByName('qtde').Value := Pquantidade;
    ParamByName('pedidoprojeto').Value := PpedidoProjeto;
    Open;
    First;

    if (RecordCount = 0) then
      Exit;

    // Cabe�alho ITEM , DESCRICAO, QTDE, VALOR
    STrDados.Add('?'+CompletaPalavra('�TEM',8,' ')+' '+
                CompletaPalavra('  COR',14,' ')+' '+
                CompletaPalavra('   DESCRI��O',39,' ')+' '+
                '          QTDE      VAL.UN     VALOR');

    while not(eof) do
    begin
      STrDados.Add(CompletaPalavra(Fieldbyname('referencia').asstring,10,' ')+' '+
                   CompletaPalavra(Fieldbyname('cor').asstring,15,' ')+' '+
                   CompletaPalavra(Fieldbyname('descricao').asstring,44,' ')+' '+
                   CompletaPalavra(Fieldbyname('quantidade').asstring+' '+Fieldbyname('unidade').asstring,6,' ')+' '+
                   CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valor').asstring),10,' ')+' '+
                   CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('mult').asstring),10,' '));
      Next;
    end;

    Close;
    SQL.Clear;
    SQL.add('Select sum(ValorFinal) as SOMA FROM TabFerragem_PP where PedidoProjeto='+PpedidoProjeto);
    Open;
    PTotal:=PTotal+(fieldbyname('soma').AsCurrency*Pquantidade);
  end;
end;

function TObjFERRAGEM_PP_PEDIDO.Get_PedidoProjeto: string;
begin
     Result:=Self.PedidoProjeto;
end;

procedure TObjFERRAGEM_PP_PEDIDO.Submit_PedidoProjeto(parametro: string);
begin
     Self.PedidoProjeto:=parametro;
end;

procedure TObjFerragem_PP_PEDIDO.EdtFerragemCorExit_codigo(Sender: TObject;LABELNOME: TLABEL);
begin
     labelnome.caption:='';
     If (Tedit(Sender).Text='')
     Then exit;

     If (Self.FerragemCor.localizacodigo(Tedit(Sender).Text)=False)
     Then Begin
               Tedit(Sender).Text:='';
               exit;
     End;
     Self.FerragemCor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.FerragemCor.Ferragem.Get_Descricao+'-'+Self.FerragemCor.Cor.Get_Descricao;
     Tedit(Sender).Text:=Self.FerragemCor.Get_Codigo;
end;

procedure TObjFERRAGEM_PP_PEDIDO.RetornaDadosRel2(PpedidoProjeto: string;
  STrDados: TStrings; ComValor: Boolean);
begin
  if (PpedidoProjeto='') then
    exit;

  with Self.ObjQueryPesquisa do
  begin
    Close;
    SQL.clear;
    SQL.Text := ' select f.referencia, c.referencia || ' + QuotedStr('-') + ' || c.descricao as cor, '+
                ' f.descricao, f.unidade, fp.valor, sum(fp.quantidade) as quantidade, '+
                ' sum(fp.quantidade) * fp.valor as mult '+
                ' from tabferragem_pp fp '+
                ' inner join tabferragemcor fc on (fp.ferragemcor = fc.codigo) '+
                ' inner join tabcor c on (fc.cor = c.codigo) '+
                ' inner join tabferragem f on (fc.ferragem = f.codigo) '+
                ' where fp.pedidoprojeto = :pedidoprojeto '+
                ' group by 1,2,3,4,5 ';

    ParamByName('pedidoprojeto').Value := PpedidoProjeto;
    Open;
    First;

    if (RecordCount = 0) then
      Exit;

    while not(eof) do
    begin
      if(ComValor=True) then
      begin
        STrDados.Add(CompletaPalavra(FieldByName('referencia').AsString, 10,' ')+' '+
                     CompletaPalavra(FieldByName('cor').AsString, 15,' ')+' '+
                     CompletaPalavra(FieldByName('descricao').AsString, 44,' ')+' '+
                     CompletaPalavra(FieldByName('Quantidade').AsString + ' ' + FieldByName('unidade').AsString ,6,' ')+' '+
                     CompletaPalavra_a_Esquerda(formata_valor(FieldByName('valor').AsCurrency),10,' ')+' '+
                     CompletaPalavra_a_Esquerda(formata_valor(FieldByName('mult').AsCurrency),10,' '));
      end
      else begin
        STrDados.Add(CompletaPalavra(FieldByName('referencia').AsString,10,' ')+' '+
                     CompletaPalavra(FieldByName('cor').AsString ,16,' ')+' '+
                     CompletaPalavra(FieldByName('descricao').AsString,45,' ')+' '+
                     CompletaPalavra(FieldByName('Quantidade').AsString + ' ' + FieldByName('unidade').AsString,6, ' '));
      end;
      next;
    end;
    if(ComValor=True) then
    begin
      close;
      sql.clear;
      sql.add('Select sum(ValorFinal) as SOMA FROM TabFerragem_PP where PedidoProjeto='+PpedidoProjeto);
      open;
      STrDados.Add('?'+CompletaPalavra_a_Esquerda('Total FERRAGENS: '+formata_valor(fieldbyname('soma').asstring),100,' '));
      STrDados.Add(' ');
    end;
  end;
end;

end.



