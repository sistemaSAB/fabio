unit UConfiguracaoSite;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ImgList, StdCtrls, ExtCtrls,UobjConfiguracaosite,UessencialGlobal;

type
  TFConfiguracaoSite = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    panelrodape: TPanel;
    imgRodape: TImage;
    panel2: TPanel;
    EdtDATABASESITE: TEdit;
    LbDATABASESITE: TLabel;
    LbHOSTNAMESITE: TLabel;
    EdtHOSTNAMESITE: TEdit;
    EdtPASSWORDSITE: TEdit;
    LbPASSWORDSITE: TLabel;
    LbPORTSITE: TLabel;
    EdtPORTSITE: TEdit;
    EdtPROTOCOLSITE: TEdit;
    LbPROTOCOLSITE: TLabel;
    LbUSERSITE: TLabel;
    EdtUSERSITE: TEdit;
    panelFTP1: TPanel;
    lbLbHOSTFTP: TLabel;
    lbLbUSERNAMEFTP: TLabel;
    lbLbPASSWORDFTP: TLabel;
    EdtHOSTFTP: TEdit;
    EdtUSERNAMEFTP: TEdit;
    EdtPASSWORDFTP: TEdit;
    lb2: TLabel;
    bt2: TSpeedButton;
    Label1: TLabel;
    Edit1: TEdit;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure bt2Click(Sender: TObject);
  private
    objConfiguracaoSite:TObjConfiguracaoSite;

  public
    { Public declarations }
  end;

var
  FConfiguracaoSite: TFConfiguracaoSite;

implementation

uses UescolheImagemBotao;

{$R *.dfm}

procedure TFConfiguracaoSite.FormShow(Sender: TObject);
begin
  try
    objConfiguracaoSite:=TObjConfiguracaoSite.Create;
  except
    free;
  end;   
  FescolheImagemBotao.PegaFiguraImagem(imgRodape,'RODAPE');
  FescolheImagemBotao.PegaFiguraBotoespequeno(nil,btalterar,btcancelar,btgravar,nil,nil,nil,btsair,nil);

  objConfiguracaoSite.__PesquisarConfiguracaoSite(
  EdtDATABASESITE,
  EdtHOSTNAMESITE,
  EdtPASSWORDSITE,
  EdtPORTSITE,
  EdtPROTOCOLSITE,
  EdtUSERSITE,
  EdtHOSTFTP,
  EdtUSERNAMEFTP,
  EdtPASSWORDFTP
  );

  desabilita_campos(panel2);
  desabilita_campos(panelFTP1);
end;

procedure TFConfiguracaoSite.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
   objConfiguracaoSite.Free;
end;

procedure TFConfiguracaoSite.btsairClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TFConfiguracaoSite.btalterarClick(Sender: TObject);
begin
  habilita_campos(panel2);
  habilita_campos(panelFTP1);
end;

procedure TFConfiguracaoSite.btcancelarClick(Sender: TObject);
begin
  desabilita_campos(panel2);
  desabilita_campos(panelFTP1);
end;

procedure TFConfiguracaoSite.btgravarClick(Sender: TObject);
begin
  desabilita_campos(panel2);
  desabilita_campos(panelFTP1);
  if(objConfiguracaoSite.__Submit(EdtDATABASESITE,EdtHOSTNAMESITE,EdtPASSWORDSITE,EdtPORTSITE,EdtPROTOCOLSITE,EdtUSERSITE,EdtHOSTFTP,EdtUSERNAMEFTP,EdtPASSWORDFTP)=True)
  then objConfiguracaoSite.__SalvarConfiguracaoSite(True);  
end;

procedure TFConfiguracaoSite.bt2Click(Sender: TObject);
begin
  if(objConfiguracaoSite.__Conectar=true)
  then MensagemSucesso('Conex�o Ativa!')
  else MensagemAviso('N�o houve sucesso na conex�o!');

  objConfiguracaoSite.__DesConectar;
end;

end.
