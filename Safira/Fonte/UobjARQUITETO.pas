unit UobjARQUITETO;
Interface
Uses Ibquery,windows,Classes,Db,Uessencialglobal;

Type
   TObjARQUITETO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Nome(parametro: string);
                Function Get_Nome: string;
                Procedure Submit_CPF(parametro: string);
                Function Get_CPF: string;
                Procedure Submit_RG(parametro: string);
                Function Get_RG: string;
                Procedure Submit_DataNascimento(parametro: string);
                Function Get_DataNascimento: string;
                Procedure Submit_Telefone(parametro: string);
                Function Get_Telefone: string;
                Procedure Submit_Celular(parametro: string);
                Function Get_Celular: string;
                Procedure Submit_Endereco(parametro: string);
                Function Get_Endereco: string;
                Procedure Submit_Numero(parametro: string);
                Function Get_Numero: string;
                Procedure Submit_Bairro(parametro: string);
                Function Get_Bairro: string;
                Procedure Submit_Cidade(parametro: string);
                Function Get_Cidade: string;
                Procedure Submit_Estado(parametro: string);
                Function Get_Estado: string;
                Procedure Submit_Observacao(parametro: String);
                Function Get_Observacao: String;
                procedure Submit_Email(parametro:String);
                function Get_Email:string;
                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Nome:string;
               CPF:string;
               RG:string;
               DataNascimento:string;
               Telefone:string;
               Celular:string;
               Endereco:string;
               Numero:string;
               Bairro:string;
               Cidade:string;
               Estado:string;
               Observacao:String;
               Email:string;
//CODIFICA VARIAVEIS PRIVADAS
               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls
//USES IMPLEMENTATION
, UMenuRelatorios;


{ TTabTitulo }


Function  TObjARQUITETO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.Nome:=fieldbyname('Nome').asstring;
        Self.CPF:=fieldbyname('CPF').asstring;
        Self.RG:=fieldbyname('RG').asstring;
        Self.DataNascimento:=fieldbyname('DataNascimento').asstring;
        Self.Telefone:=fieldbyname('Telefone').asstring;
        Self.Celular:=fieldbyname('Celular').asstring;
        Self.Endereco:=fieldbyname('Endereco').asstring;
        Self.Numero:=fieldbyname('Numero').asstring;
        Self.Bairro:=fieldbyname('Bairro').asstring;
        Self.Cidade:=fieldbyname('Cidade').asstring;
        Self.Estado:=fieldbyname('Estado').asstring;
        Self.Observacao:=fieldbyname('Observacao').asstring;
        Self.Email:=fieldbyname('Email').AsString;
//CODIFICA TABELAPARAOBJETO













        result:=True;
     End;
end;


Procedure TObjARQUITETO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Nome').asstring:=Self.Nome;
        ParamByName('CPF').asstring:=Self.CPF;
        ParamByName('RG').asstring:=Self.RG;
        ParamByName('DataNascimento').asstring:=Self.DataNascimento;
        ParamByName('Telefone').asstring:=Self.Telefone;
        ParamByName('Celular').asstring:=Self.Celular;
        ParamByName('Endereco').asstring:=Self.Endereco;
        ParamByName('Numero').asstring:=Self.Numero;
        ParamByName('Bairro').asstring:=Self.Bairro;
        ParamByName('Cidade').asstring:=Self.Cidade;
        ParamByName('Estado').asstring:=Self.Estado;
        ParamByName('Observacao').asstring:=Self.Observacao;
        ParamByName('Email').AsString:=Self.Email;
//CODIFICA OBJETOPARATABELA













  End;
End;

//***********************************************************************

function TObjARQUITETO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjARQUITETO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Nome:='';
        CPF:='';
        RG:='';
        DataNascimento:='';
        Telefone:='';
        Celular:='';
        Endereco:='';
        Numero:='';
        Bairro:='';
        Cidade:='';
        Estado:='';
        Observacao:='';
        Email:='';
//CODIFICA ZERARTABELA













     End;
end;

Function TObjARQUITETO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Nome='')
      Then Mensagem:=mensagem+'/Nome';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjARQUITETO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjARQUITETO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjARQUITETO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        if (Trim(comebarra(Self.DataNascimento))<>'')
        Then Strtodate(Self.DataNascimento)
        Else Self.DataNascimento:='';
     Except
           Mensagem:=mensagem+'/Data de Nascimento';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjARQUITETO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjARQUITETO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro ARQUITETO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Nome,CPF,RG,DataNascimento,Telefone,Celular,Endereco');
           SQL.ADD(' ,Numero,Bairro,Cidade,Estado,Observacao,email');
           SQL.ADD(' from  TabArquiteto');
           SQL.ADD(' WHERE codigo='+parametro);

//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjARQUITETO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjARQUITETO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjARQUITETO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabArquiteto(CODIGO,Nome,CPF,RG,DataNascimento');
                InsertSQL.add(' ,Telefone,Celular,Endereco,Numero,Bairro,Cidade,Estado');
                InsertSQL.add(' ,Observacao,email)');
                InsertSQL.add('values (:CODIGO,:Nome,:CPF,:RG,:DataNascimento,:Telefone');
                InsertSQL.add(' ,:Celular,:Endereco,:Numero,:Bairro,:Cidade,:Estado');
                InsertSQL.add(' ,:Observacao,:email)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabArquiteto set CODIGO=:CODIGO,Nome=:Nome,CPF=:CPF');
                ModifySQL.add(',RG=:RG,DataNascimento=:DataNascimento,Telefone=:Telefone');
                ModifySQL.add(',Celular=:Celular,Endereco=:Endereco,Numero=:Numero');
                ModifySQL.add(',Bairro=:Bairro,Cidade=:Cidade,Estado=:Estado,Observacao=:Observacao,email=:email');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabArquiteto where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjARQUITETO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjARQUITETO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabARQUITETO');
     Result:=Self.ParametroPesquisa;
end;

function TObjARQUITETO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de ARQUITETO ';
end;


function TObjARQUITETO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENARQUITETO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENARQUITETO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjARQUITETO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjARQUITETO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjARQUITETO.RetornaCampoNome: string;
begin
      result:='Nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjArquiteto.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjArquiteto.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjArquiteto.Submit_Nome(parametro: string);
begin
        Self.Nome:=Parametro;
end;
function TObjArquiteto.Get_Nome: string;
begin
        Result:=Self.Nome;
end;
procedure TObjArquiteto.Submit_CPF(parametro: string);
begin
        Self.CPF:=Parametro;
end;
function TObjArquiteto.Get_CPF: string;
begin
        Result:=Self.CPF;
end;
procedure TObjArquiteto.Submit_RG(parametro: string);
begin
        Self.RG:=Parametro;
end;
function TObjArquiteto.Get_RG: string;
begin
        Result:=Self.RG;
end;
procedure TObjArquiteto.Submit_DataNascimento(parametro: string);
begin
        Self.DataNascimento:=Parametro;
end;
function TObjArquiteto.Get_DataNascimento: string;
begin
        Result:=Self.DataNascimento;
end;
procedure TObjArquiteto.Submit_Telefone(parametro: string);
begin
        Self.Telefone:=Parametro;
end;
function TObjArquiteto.Get_Telefone: string;
begin
        Result:=Self.Telefone;
end;
procedure TObjArquiteto.Submit_Celular(parametro: string);
begin
        Self.Celular:=Parametro;
end;
function TObjArquiteto.Get_Celular: string;
begin
        Result:=Self.Celular;
end;
procedure TObjArquiteto.Submit_Endereco(parametro: string);
begin
        Self.Endereco:=Parametro;
end;
function TObjArquiteto.Get_Endereco: string;
begin
        Result:=Self.Endereco;
end;
procedure TObjArquiteto.Submit_Numero(parametro: string);
begin
        Self.Numero:=Parametro;
end;
function TObjArquiteto.Get_Numero: string;
begin
        Result:=Self.Numero;
end;
procedure TObjArquiteto.Submit_Bairro(parametro: string);
begin
        Self.Bairro:=Parametro;
end;
function TObjArquiteto.Get_Bairro: string;
begin
        Result:=Self.Bairro;
end;
procedure TObjArquiteto.Submit_Cidade(parametro: string);
begin
        Self.Cidade:=Parametro;
end;
function TObjArquiteto.Get_Cidade: string;
begin
        Result:=Self.Cidade;
end;
procedure TObjArquiteto.Submit_Estado(parametro: string);
begin
        Self.Estado:=Parametro;
end;
function TObjArquiteto.Get_Estado: string;
begin
        Result:=Self.Estado;
end;
procedure TObjArquiteto.Submit_Observacao(parametro: String);
begin
        Self.Observacao:=Parametro;
end;
function TObjArquiteto.Get_Observacao: String;
begin
        Result:=Self.Observacao;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjARQUITETO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJARQUITETO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of

          0:Begin
          End;

          End;
     end;

end;

function TObjARQUITETO.Get_Email:string;
begin
    Result:=Self.Email;
end;

procedure TObjARQUITETO.Submit_Email(parametro:string);
begin
    Self.Email:=parametro;
end;



end.



