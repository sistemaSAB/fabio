unit UOPERACAONF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjOPERACAONF,
  jpeg;

type
  TFOPERACAONF = class(TForm)
    edtNOME: TEdit;
    lbLbNOME: TLabel;
    edtCODIGO: TEdit;
    lbLbCODIGO: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    btNovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btCancelar: TBitBtn;
    btexcluir: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btOpcoes: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjOPERACAONF:TObjOPERACAONF;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FOPERACAONF: TFOPERACAONF;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFOPERACAONF.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjOPERACAONF do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_NOME(edtNOME.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFOPERACAONF.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjOPERACAONF do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNOME.text:=Get_NOME;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFOPERACAONF.TabelaParaControles: Boolean;
begin
     If (Self.ObjOPERACAONF.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFOPERACAONF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjOPERACAONF=Nil)
     Then exit;

If (Self.ObjOPERACAONF.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjOPERACAONF.free;
end;

procedure TFOPERACAONF.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFOPERACAONF.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
    // desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjOPERACAONF.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorios.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

     Self.ObjOPERACAONF.status:=dsInsert;
     edtNOME.setfocus;

end;


procedure TFOPERACAONF.btalterarClick(Sender: TObject);
begin
    If (Self.ObjOPERACAONF.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjOPERACAONF.Status:=dsEdit;
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtNOME.setfocus;
                btnovo.Visible :=false;
                btalterar.Visible:=false;
                btpesquisar.Visible:=false;
                btrelatorios.Visible:=false;
                btexcluir.Visible:=false;
                btsair.Visible:=false;
                btopcoes.Visible :=false;
                
          End;


end;

procedure TFOPERACAONF.btgravarClick(Sender: TObject);
begin

     If Self.ObjOPERACAONF.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjOPERACAONF.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjOPERACAONF.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorios.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;

end;

procedure TFOPERACAONF.btexcluirClick(Sender: TObject);
begin
     if(StrToInt(edtCODIGO.Text)<=4)
     then Exit;

     If (Self.ObjOPERACAONF.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjOPERACAONF.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjOPERACAONF.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFOPERACAONF.btcancelarClick(Sender: TObject);
begin
     Self.ObjOPERACAONF.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorios.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;

end;

procedure TFOPERACAONF.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFOPERACAONF.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjOPERACAONF.Get_pesquisa,Self.ObjOPERACAONF.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjOPERACAONF.status<>dsinactive
                                  then exit;

                                  If (Self.ObjOPERACAONF.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjOPERACAONF.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFOPERACAONF.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFOPERACAONF.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     
     Try
        Self.ObjOPERACAONF:=TObjOPERACAONF.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);

  if(Tag<>0) then
  begin
     if(ObjOPERACAONF.LocalizaCodigo(IntToStr(Tag)))then
     begin
      ObjOPERACAONF.TabelaparaObjeto;
      self.ObjetoParaControles;
     end;
  end;

end;
//CODIFICA ONKEYDOWN E ONEXIT


end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjOPERACAONF.OBJETO.Get_Pesquisa,Self.ObjOPERACAONF.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjOPERACAONF.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjOPERACAONF.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjOPERACAONF.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
