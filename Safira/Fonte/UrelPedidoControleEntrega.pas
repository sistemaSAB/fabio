unit UrelPedidoControleEntrega;

interface

uses dialogs,Windows, SysUtils, Messages, Classes, Graphics, Controls,
  ExtCtrls, Forms, QuickRpt, QRCtrls;

type
  TQRpedidoControleEntrega = class(TQuickRep)
    SB: TQRStringsBand;
    QRBand4: TQRBand;
    lbnomesuperior: TQRLabel;
    LbNumPag: TQRSysData;
    BandaDetail: TQRBand;
    QrImagemProjeto: TQRImage;
    LbReferenciaProjeto: TQRLabel;
    QRSysData1: TQRSysData;
    QRShape1: TQRShape;
    lbDescricao: TQRLabel;
    LbAltura: TQRLabel;
    lbLargura: TQRLabel;
    QRShape3: TQRShape;
    LBDADOS: TQRLabel;
    BANDATITULO: TQRBand;
    QRShape2: TQRShape;
    LbNome: TQRLabel;
    LBEndereco: TQRLabel;
    LbContato: TQRLabel;
    QRShape5: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel18: TQRLabel;
    Label20: TQRLabel;
    LBTelefone: TQRLabel;
    lbCNPJCPF: TQRLabel;
    LbVendedor: TQRLabel;
    lbRGIE: TQRLabel;
    QRLabel21: TQRLabel;
    QRImageLogotipo: TQRImage;
    LBCabecalhoLinha1: TQRLabel;
    LBCabecalhoLinha2: TQRLabel;
    LBCabecalhoLinha3: TQRLabel;
    LBCabecalhoLinha4: TQRLabel;
    lbTitulo: TQRLabel;
    LBProposta: TQRLabel;
    LbPedidoProjeto: TQRLabel;
    BandaBandSumario: TQRBand;
    qrshp5: TQRShape;
    lB12: TQRLabel;
    lBdata: TQRLabel;
    lBDataE: TQRLabel;
    lBDataEntrega: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    LBComplemento: TQRLabel;
    lbComplementoPedido: TQRLabel;
    LbObra: TQRLabel;
    LBEmail: TQRLabel;
    procedure LBDADOSPrint(sender: TObject; var Value: String);
  private

  public

  end;

var
  QRpedidoControleEntrega: TQRpedidoControleEntrega;

implementation



{$R *.DFM}


procedure TQRpedidoControleEntrega.LBDADOSPrint(sender: TObject; var Value: String);
var
apoio:string;
posicao:integer;
begin
     lbdados.font.Color:=clBLACK;
     LBDADOS.Font.Style:=[];
     LBDADOS.Font.size:=9;

     if (sb.Item='')
     Then Begin
               Value:=sb.item;
               exit;
     End;

     case sb.item[1] of
     '?':Begin//negrito
              LBDADOS.Font.Style:=[fsbold];
              value:=copy(sb.Item,2,length(sb.item));
         end;
     '�':Begin//cor

              apoio:=copy(sb.Item,2,length(sb.item));
              posicao:=pos('�',apoio);
              If (posicao=0)//tem que ter o fecho e entre eles a cor
              Then value:=sb.item
              Else Begin//s� 3 cores, vermelho,verde e azul
                        apoio:=copy(apoio,1,posicao-1);
                        if UPPERCASE(apoio)='VERMELHO'
                        tHEN lbdados.font.Color:=CLRED
                        eLSE
                                IF UPPERCASE(APOIO)='VERDE'
                                THEN lbdados.font.Color:=clGreen
                                ELSE
                                        IF UPPERCASE(APOIO)='AZUL'
                                        THEN lbdados.font.Color:=clBlue
                                        ELSE lbdados.font.Color:=clBLACK;


                        //pegando os dados fora a cor
                        value:=copy(sb.Item,posicao+2,length(sb.item));
              End;
         End;
     Else Begin
                value:=Sb.item;
     End;

     end;


end;






end.
