unit ULogin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls,  ExtCtrls, jpeg;

type
  TFlogin = class(TForm)
    edtnome: TEdit;
    edtsenha: TEdit;
    btok: TBitBtn;
    btcancel: TBitBtn;
    lbversao: TLabel;
    pnl1: TPanel;
    img1: TImage;
    lb3: TLabel;
    bvl1: TBevel;
    lb1: TLabel;
    lb2: TLabel;
    lb4: TLabel;
    lbCapsLock1: TLabel;
    lbCapslock2: TLabel;
    lbAtencaoCapslock: TLabel;
    procedure BtnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtsenhaKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure edtnomeExit(Sender: TObject);
    procedure edtsenhaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edtsenhaEnter(Sender: TObject);
    procedure edtsenhaExit(Sender: TObject);
    function ValidaVersao: Boolean;
    procedure btcancelClick(Sender: TObject);
    { Private declarations }

  public
    VERSAO:string;

  end;

var
  Flogin: TFlogin;

implementation

uses USeg, UDataModulo,UessencialGlobal, 
  UobjVersaoSistema, UobjVERSAOEXECUTAVEL, Uprincipal, 
  UobjREQUISICAOSENHA, umensagem, UobjParametros,
  UobjREQUISICAOSENHALOCAL, UobjRequisicaoLara,UobjICONES;

{$R *.DFM}



procedure TFlogin.BtnOkClick(Sender: TObject);
var
PcodigoBaseSite,tmpsenha,NomeOld,pathsemarq,path:string;
ObjRequisicaoSenha:TObjREQUISICAOSENHA;
ObjRequisicaoSenhaLocal:tobjrequisicaosenhalocal;
pcodigo:string;
ObjRequisicaoLara:tobjrequisicaolara;
begin

      path:='';
      FDataModulo.IbDatabase.close;
      //FDataModuloIBO.IboDatabase1.close;
      
      path:=PegaPathBanco;
      FDataModulo.IbDatabase.Databasename:=path;


      //considerar ao criar o ususario
      //do sistema ja cria do interbase
      //e da as devidas permissoes
      tmpsenha:='';
      if (edtnome.text<>'SYSDBA')
      Then tmpsenha:=useg.encriptasenha(edtsenha.Text)
      Else TmpSenha:=Edtsenha.text;


      FDataModulo.IbDatabase.close;
      FDataModulo.IbDatabase.Params.clear;
      FDataModulo.IbDatabase.Params.Add('User_name='+ edtnome.Text ) ;//pega o nome do usuario e a senha
      FDataModulo.IbDatabase.Params.Add('password='+tmpsenha);
      PcodigoBaseSite:='';

      try
         FDataModulo.IbDatabase.Open ;
         //FDataModuloNfe.IbDatabase.Open;

         if (Self.ValidaVersao=False)
         Then exit;

      except
            on e:exception do
            begin
                messagedlg('Erro na tentativa de Conectar ao banco de dados'+#13+'Erro: '+E.message,mterror,[mbok],0);
                edtsenha.Text :='';
                edtnome.SetFocus ;
                exit;
            End;
      end;



      If (ObjParametroGlobal=Nil)
      Then Begin
                Try
                    ObjParametroGlobal:=TObjParametros.create;
                Except
                      on e:exception do
                      Begin
                           mensagemerro('Erro na tentativa de criar o objeto de par�metros global '+e.message);
                           exit;
                      End;
                End;

      End;
      

      //ShowMessage('habilitar lara');

 {     //**********************REQUISICAO LARA*******************************
      Try
         ObjRequisicaoLara:=tobjrequisicaolara.create;
      Except
            Mensagemerro('Erro na tentativa de criar o objeto OBJREQUISICAOLARA');
            exit;
      End;

      Try
         if (objrequisicaolara.ValidaAcesso(3004)=False)
         then exit;
      Finally
             objrequisicaolara.free;
      End;

      if (SistemaemModoDemoGlobal=True) Then
        MensagemAviso('Sistema em Modo Demonstra��o');

                          }
      //****************FIM REQUISICAO LARA********************************



      Self.tag:=1;
      Self.close;
end;

procedure TFlogin.FormCreate(Sender: TObject);
begin
  ShortDateFormat:='dd/mm/yyyy';
  LongDateFormat:='dd/mm/yyyy';
  CurrencyString:='R$';
end;

procedure TFlogin.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFlogin.edtsenhaKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Self.BtnOkClick(sender);   

end;

procedure TFlogin.FormShow(Sender: TObject);
var
ObjVersaoSistema:TobjVersaoSistema;
begin

  Try
    ObjVersaoSistema:=TobjVersaoSistema.Create;
    lbversao.caption:='V '+ObjVersaoSistema.VersaodoArquivo;
    VERSAO:=lbversao.Caption;
  finally
    Objversaosistema.free;
  End;

  edtnome.setfocus;
  ModoCalculoSenhaGlobal:=1;
  Self.tag:=0;
  CorFormGlobal:=clbtnface;

  SistemaemModoDemoGlobal:=False;
  
end;

procedure TFlogin.edtnomeExit(Sender: TObject);
begin

  if Tedit(Sender).Text='3,'then Tedit(Sender).Text:='sysdba';
  
end;

function TFlogin.ValidaVersao: Boolean;
var
ObjVersaoSistema:TObjVersaosistema;
ObjversaoExecutavel_BD:TObjversaoExecutavel;
PversaoExe:Integer;
PversaoBD:integer;
PversaoExe_S,PversaoBD_S:String;
cont:integer;
Fmensagemx:TfMensagem;
begin
     Result:=False;

      try
               ObjVersaoSistema :=TObjVersaosistema.create;
      Except
               Messagedlg('Erro na tentativa de criar o objeto de vers�o',mterror,[mbok],0);
               exit;
      End;

      Try
         try
               PversaoExe:=Strtoint(trim(come(ObjVersaoSistema.VersaodoArquivo,'.')));
               PversaoExe_S:=ObjVersaoSistema.VersaodoArquivo;

         Except
               Messagedlg('Erro na tentativa de Recuperar a vers�o do execut�vel',mterror,[mbok],0);
               exit;
         End;
      finally
               ObjVersaoSistema.Free;
      End;

      Try
               ObjversaoExecutavel_BD:=TObjversaoExecutavel.Create;
      Except
               Messagedlg('Erro na tentativa de Criar o Objeto de Vers�o de Execut�vel',mterror,[mbok],0);
               exit;
      end;

     try
        if (ObjversaoExecutavel_BD.LocalizaCodigo('0')=False)
        Then Begin
                  Messagedlg('Erro na tentativa de Localizar a vers�o do execut�vel no banco de dados',mterror,[mbok],0);
                  exit;
        End;
        ObjversaoExecutavel_BD.TabelaparaObjeto;
        Try
          PversaoBD:=Strtoint(trim(come(ObjversaoExecutavel_BD.Get_VERSAO,'.')));
          PversaoBD_S:=ObjversaoExecutavel_BD.Get_VERSAO;
        Except
          Messagedlg('Erro na tentativa de resgatar a vers�o do execut�vel no banco de dados',mterror,[mbok],0);
          exit;
        end;
     Finally
            ObjversaoExecutavel_BD.free;
     End;



     if (PversaoExe<>PversaoBD)
     Then begin
             Fmensagemx:=TfMensagem.create(nil);
             Try
               fmensagemx.lbmensagem.Caption:='Vers�o do execut�vel '+PversaoExe_S+' Vers�o do BD '+PversaoBD_S+#13+'O sistema ser� atualizado';
               for cont:=1 to 20 do
               Begin
                    fmensagemx.show;
                    Application.ProcessMessages;
                    Sleep(100);
               End;
               fmensagemx.close;
             Finally
                    freeandnil(Fmensagemx);
             End;

             if (CarregaAtualizacao=True)
             Then Begin
                       PfechasistemaGlobal:=true;
                       Self.close;
             End
             Else result:=False;
     end
     Else result:=true;
end;



procedure TFlogin.edtsenhaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
       if (Key = 20) then {capslock}
        begin
            lbCapsLock1.Visible:=verificaCapsLock();
            lbCapslock2.Visible:=lbCapsLock1.Visible;
            lbAtencaoCapslock.Visible:=lbCapsLock1.Visible;
        end;
end;

procedure TFlogin.edtsenhaEnter(Sender: TObject);
begin
          lbCapsLock1.Visible:=verificaCapsLock();
          lbCapslock2.Visible:=lbCapsLock1.Visible;
          lbAtencaoCapslock.Visible:=lbCapsLock1.Visible;
end;

procedure TFlogin.edtsenhaExit(Sender: TObject);
begin
   self.lbCapsLock1.Visible:=False;
  self.lbCapslock2.Visible:=False;
  self.lbAtencaoCapslock.Visible:=False;
end;

procedure TFlogin.btcancelClick(Sender: TObject);
begin
    Self.tag:=0;
    Self.close;
end;

end.


