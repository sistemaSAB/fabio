unit UAlteraPrecoPErsiana;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,Uobjpersianagrupodiametrocor, ExtCtrls, Buttons;

type
  TFAlteraPrecoPersiana = class(TForm)
    EdtPrecoCusto: TEdit;
    LbPrecoCusto: TLabel;
    Label1: TLabel;
    lbprecopago: TLabel;
    btgravar: TButton;
    btcancelar: TButton;
    GroupBoxMargem: TGroupBox;
    LbPorcentagemFornecido: TLabel;
    LbPorcentagemRetirado: TLabel;
    LbPorcentagemInstalado: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdtPorcentagemInstaladoGDC: TEdit;
    EdtPorcentagemFornecidoGDC: TEdit;
    EdtPorcentagemRetiradoGDC: TEdit;
    GroupBoxPrecoVenda: TGroupBox;
    LbPrecoVendaInstalado: TLabel;
    LbPrecoVendaFornecido: TLabel;
    LbPrecoVendaRetirado: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdtPrecoVendaInstaladoGDC: TEdit;
    EdtPrecoVendaFornecidoGDC: TEdit;
    EdtPrecoVendaRetiradoGDC: TEdit;
    procedure FormShow(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtPorcentagemInstaladoGDCExit(Sender: TObject);
    procedure EdtPorcentagemFornecidoGDCExit(Sender: TObject);
    procedure EdtPorcentagemRetiradoGDCExit(Sender: TObject);
  private
    { Private declarations }
        AlteraPrecoPeloCusto:Boolean;

  public
    { Public declarations }
    Objpersianagrupodiametrocor:Tobjpersianagrupodiametrocor;
  end;

var
  FAlteraPrecoPersiana: TFAlteraPrecoPersiana;

implementation

uses UessencialGlobal;

{$R *.dfm}

procedure TFAlteraPrecoPersiana.FormShow(Sender: TObject);
begin
    Self.Tag:=0;

    AlteraPrecoPeloCusto:=True;

    (*if (ObjParametroGlobal.ValidaParametro('CALCULA PRECO DE VENDA DO VIDRO PELO CUSTO E PERCENTUAIS?')=True)
    then Begin
               if (ObjParametroGlobal.get_valor<>'SIM')
               then AlteraPrecoPeloCusto:=false;
    End;

    if (AlteraPrecoPeloCusto=True)
    Then Begin
              GroupBoxPrecoVenda.Enabled:=False;
              GroupBoxMargem.Enabled:=True;
    End
    Else BEgin
              GroupBoxPrecoVenda.Enabled:=True;
              GroupBoxMargem.Enabled:=False;
    End;

    Rg_forma_de_calculo_percentual.itemindex:=0;
    if (ObjParametroGlobal.ValidaParametro('FORMA DE CALCULO DE PERCENTUAL DE ACRESCIMO NAS CORES DO VIDRO')=True)
    then Begin
               if (ObjParametroGlobal.get_valor='1')
               then Rg_forma_de_calculo_percentual.itemindex:=1;
    End;
    Rg_forma_de_calculo_percentual.enabled:=False;
    *)
End;

procedure TFAlteraPrecoPersiana.btgravarClick(Sender: TObject);
begin
     Self.Tag:=1;
     Self.Close;
end;

procedure TFAlteraPrecoPersiana.btcancelarClick(Sender: TObject);
begin
     Self.tag:=0;
     Self.close;
end;

procedure TFAlteraPrecoPersiana.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);

end;

procedure TFAlteraPrecoPersiana.EdtPorcentagemInstaladoGDCExit(
  Sender: TObject);
Var
  PrecoCusto:Currency;
begin
  PrecoCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text));
  EdtPrecoVendaInstaladoGDC.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemInstaladoGDC.Text))/100)));
end;

procedure TFAlteraPrecoPersiana.EdtPorcentagemFornecidoGDCExit(
  Sender: TObject);
Var
  PrecoCusto:Currency;
begin
  PrecoCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text));
  EdtPrecoVendaFornecidoGDC.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemFornecidoGDC.Text))/100)));
end;

procedure TFAlteraPrecoPersiana.EdtPorcentagemRetiradoGDCExit(
  Sender: TObject);
Var
  PrecoCusto:Currency;
begin
  PrecoCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text));
  EdtPrecoVendaRetiradoGDC.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemRetiradoGDC.Text))/100)));
end;

end.
