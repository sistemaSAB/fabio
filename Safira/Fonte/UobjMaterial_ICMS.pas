unit UobjMaterial_ICMS;
Interface
Uses dialogs,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UOBJTIPOCLIENTE
,UOBJIMPOSTO_ICMS,GRIDS,uobjImposto_ipi,uobjImposto_pis,uobjImposto_cofins,uobjoperacaonf;
//USES_INTERFACE




Type
   TObjmaterial_ICMS=class

          Public
                Objquery:Tibquery;

                Status                                      :TDataSetState;
                SqlInicial                                  :String;
                MATERIALInsertSql:TStringList;
                MATERIALInsertSqlValues:TStringList;
                MATERIALModifySQl:TStringList;
                MaterialSelectSql:TStringList;
                Strl_Variaveis:TStringList;//usado na FrImposto_ICMS

                CODIGO:String;
                ESTADO:String;
                tipocliente:TOBJTIPOCLIENTE;
                IMPOSTO:TOBJIMPOSTO_ICMS;
                imposto_ipi:tobjImposto_ipi;
                imposto_pis:tobjImposto_pis;
                imposto_cofins:tobjImposto_cofins;
                operacao:Tobjoperacaonf;
                Owner:TComponent;
                PERCENTUALTRIBUTO: string;

                Constructor Create(Owner:TComponent);virtual;
                Destructor  Free;virtual;

                procedure EdttipoclienteExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdttipoclienteKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtIMPOSTOExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtIMPOSTOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure edtCSOSNkeydown(Sender: TObject; var Key: Word; Shift: TShiftState;LABELNOME:Tlabel);


                procedure Edtimposto_ipiExit(Sender: TObject;LABELNOME:TLABEL);
                procedure Edtimposto_ipiKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);


                procedure EdtoperacaoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtoperacaoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);


                procedure Edtimposto_pisExit(Sender: TObject;LABELNOME:TLABEL);
                procedure Edtimposto_pisKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                procedure Edtimposto_cofinsExit(Sender: TObject;LABELNOME:TLABEL);
                procedure Edtimposto_cofinsKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);



                Function    Salvar(ComCommit:Boolean)       :Boolean;virtual;abstract;
                Function    LocalizaCodigo(Parametro:String) :boolean;virtual;abstract;
                Function    Exclui(Pcodigo:String;ComCommit:boolean)            :Boolean;virtual;abstract;
                Function    Get_Pesquisa                    :TStringList;virtual;abstract;
                Function    Get_TituloPesquisa              :String;virtual;abstract;

                Function    TabelaparaObjeto:Boolean;virtual;
                procedure   ObjetoparaTabela;virtual;

                function VerificaBrancos: String;virtual;
                function VerificaRelacionamentos: String;virtual;
                function VerificaNumericos: String;virtual;
                function VerificaData: String;virtual;
                function VerificaFaixa: String;virtual;

                Procedure   ZerarTabela;virtual;
                Procedure   Cancelar;virtual;abstract;
                Procedure   Commit;virtual;abstract;

                Function  Get_NovoCodigo:String;virtual;abstract;
                Function  RetornaCampoCodigo:String;virtual;abstract;
                Function  RetornaCampoNome:String;virtual;abstract;
                Procedure Imprime(Pcodigo:String);virtual;

                Procedure Submit_CodigoMaterial(parametro:String);virtual;abstract;
                Function  Get_CodigoMaterial:String;virtual;abstract;

                Function  Get_NomeMaterial:String;virtual;abstract;

                Procedure Submit_CODIGO(parametro: String);virtual;
                Function Get_CODIGO: String;virtual;
                Procedure Submit_ESTADO(parametro: String);virtual;
                Function Get_ESTADO: String;virtual;

                Procedure Submit_PERCENTUALTRIBUTO(parametro: String);virtual;
                Function Get_PERCENTUALTRIBUTO: String;virtual;

                procedure EdtMaterialExit(Sender: TObject;LABELNOME:TLABEL);virtual;abstract;
                procedure EdtMaterialKeyDown(Shender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);virtual;abstract;
                Procedure RetornaImposto(PCodigo:String;STRGRID:TStringGrid);virtual;abstract;
                function  Localiza(Pestado,PtipoCliente,PcodigoMaterial,POperacao:String): boolean;virtual;abstract;
                Function  ValidaPermissaoImpostos:boolean;virtual;abstract;

                function RetornaMateriais(PMaterialAtual: String): boolean;virtual;abstract;
                function atualizaVersao( pCodigoMaterial:string ):Boolean;
         Private







   End;


implementation

uses Upesquisa, UTIPOCLIENTE, UIMPOSTO_ICMS,Controls,SysUtils,
  UIMPOSTO_IPI, Uimposto_pis,Uimposto_cofins,UDataModulo, UOPERACAONF,
  UCSOSN;

{ TObjmaterial_ICMS }

Function  TObjmaterial_ICMS.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;

        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.ESTADO:=fieldbyname('ESTADO').asstring;

        If(FieldByName('tipocliente').asstring<>'')
        Then Begin
                 If (Self.tipocliente.LocalizaCodigo(FieldByName('tipocliente').asstring)=False)
                 Then Begin
                          Messagedlg('Tipo de Cliente N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.tipocliente.TabelaparaObjeto;
        End;

        If(FieldByName('IMPOSTO').asstring<>'')
        Then Begin
                 If (Self.IMPOSTO.LocalizaCodigo(FieldByName('IMPOSTO').asstring)=False)
                 Then Begin
                          Messagedlg('Imposto N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.IMPOSTO.TabelaparaObjeto;
        End;

        If(FieldByName('IMPOSTO_IPI').asstring<>'')
        Then Begin
                 If (Self.IMPOSTO_IPI.LocalizaCodigo(FieldByName('IMPOSTO_IPI').asstring)=False)
                 Then Begin
                          Messagedlg('Imposto IPI N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.IMPOSTO_IPI.TabelaparaObjeto;
        End;


        If(FieldByName('imposto_pis').asstring<>'')
        Then Begin
                 If (Self.imposto_pis.LocalizaCodigo(FieldByName('imposto_pis').asstring)=False)
                 Then Begin
                          Messagedlg('Imposto PIS N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.imposto_pis.TabelaparaObjeto;
        End;

        If(FieldByName('imposto_cofins').asstring<>'')
        Then Begin
                 If (Self.imposto_cofins.LocalizaCodigo(FieldByName('imposto_cofins').asstring)=False)
                 Then Begin
                          Messagedlg('Imposto cofins N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.imposto_cofins.TabelaparaObjeto;
        End;

        If(FieldByName('operacao').asstring<>'')
        Then Begin
                 If (Self.operacao.LocalizaCodigo(FieldByName('operacao').asstring)=False)
                 Then Begin
                          Messagedlg('Opera��o N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.operacao.TabelaparaObjeto;
        End;

        Self.PERCENTUALTRIBUTO:=fieldbyname('PERCENTUALTRIBUTO').asstring;

        result:=True;
     End;
end;

Procedure TObjmaterial_ICMS.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('ESTADO').asstring:=Self.ESTADO;
        ParamByName('tipocliente').asstring:=Self.tipocliente.GET_CODIGO;
        ParamByName('IMPOSTO').asstring:=Self.IMPOSTO.GET_CODIGO;
        ParamByName('imposto_ipi').asstring:=Self.imposto_ipi.GET_CODIGO;
        ParamByName('imposto_pis').asstring:=Self.imposto_pis.GET_CODIGO;
        ParamByName('imposto_cofins').asstring:=Self.imposto_cofins.GET_CODIGO;
        ParamByName('operacao').asstring:=Self.operacao.GET_CODIGO;
        ParamByName('PERCENTUALTRIBUTO').asstring := virgulaparaponto( tira_ponto( Self.PERCENTUALTRIBUTO ) );
  End;
End;



constructor TObjmaterial_ICMS.Create(Owner:TComponent);
begin

     With Self do
     Begin
          Self.Owner := Owner;
          Strl_Variaveis:=TStringList.create;
          Objquery:=TIBQuery.create(nil);
          Objquery.Database:=FDataModulo.IbDatabase;

          tipocliente:=TOBJTIPOCLIENTE.create;
          IMPOSTO:=TOBJIMPOSTO_ICMS.create(Self.Owner);
          imposto_ipi:=TOBJimposto_ipi.create;
          imposto_pis:=TOBJimposto_pis.create;
          imposto_cofins:=TOBJimposto_cofins.create;
          Operacao:=TObjOPERACAONF.Create;

          MaterialInsertSql:=TStringList.create;
          MATERIALInsertSqlValues:=TStringList.create;
          MaterialModifySQl:=TStringList.create;
          MaterialSelectSql:=TStringList.create;

          MaterialSelectSql.clear;
          MaterialSelectSql.add('CODIGO,ESTADO,tipocliente,IMPOSTO,imposto_ipi,imposto_pis,imposto_cofins,operacao,percentualtributo');

          MaterialInsertSql.clear;
          MaterialInsertSql.add('CODIGO,ESTADO,tipocliente,IMPOSTO,imposto_ipi,imposto_pis,imposto_cofins,operacao,percentualtributo');

          MATERIALInsertSqlValues.clear;
          MATERIALInsertSqlValues.add(':CODIGO,:ESTADO,:tipocliente,:IMPOSTO,:imposto_ipi,:imposto_pis,:imposto_cofins,:operacao,:percentualtributo');

          MATERIALModifySQL.clear;
          MATERIALModifySQL.add('CODIGO=:CODIGO,ESTADO=:ESTADO');
          MATERIALModifySQL.add(',tipocliente=:tipocliente,IMPOSTO=:IMPOSTO,imposto_ipi=:imposto_ipi,imposto_pis=:imposto_pis,imposto_cofins=:imposto_cofins');
          MATERIALModifySQL.add(',operacao=:operacao,percentualtributo=:percentualtributo');
     End;


end;

destructor TObjmaterial_ICMS.Free;
begin
    FreeAndNil(Strl_Variaveis);
    FreeAndNil(MATERIALInsertSqlValues);
    Self.tipocliente.FREE;
    Self.IMPOSTO.FREE;
    Self.imposto_ipi.FREE;
    Self.imposto_pis.FREE;
    Self.imposto_cofins.FREE;
    Self.operacao.Free;
    FreeAndNil(Objquery);
    FreeAndNil(MaterialInsertSql);
    FreeAndNil(MaterialModifySQl);
    FreeAndNil(MaterialSelectSql);
end;

procedure TObjmaterial_ICMS.EdttipoclienteExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.tipocliente.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.tipocliente.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.tipocliente.GET_NOME;
End;

procedure TObjmaterial_ICMS.EdttipoclienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FTIPOCLIENTE:TFTIPOCLIENTE;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FTIPOCLIENTE:=TFTIPOCLIENTE.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.tipocliente.Get_Pesquisa,Self.tipocliente.Get_TituloPesquisa,Ftipocliente)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.tipocliente.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                    LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring

                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FTIPOCLIENTE);
     End;
end;

procedure TObjmaterial_ICMS.EdtIMPOSTOExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.IMPOSTO.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.IMPOSTO.tabelaparaobjeto;
End;

procedure TObjmaterial_ICMS.EdtIMPOSTOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FIMPOSTO_ICMS:TFIMPOSTO_ICMS;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FIMPOSTO_ICMS:=TFIMPOSTO_ICMS.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.IMPOSTO.Get_Pesquisa,Self.IMPOSTO.Get_TituloPesquisa,FIMPOSTO_ICMS)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.IMPOSTO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.IMPOSTO.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.IMPOSTO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FIMPOSTO_ICMS);
     End;
end;
//CODIFICA EXITONKEYDOWN

procedure TObjmaterial_ICMS.Edtimposto_ipiExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.imposto_ipi.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.imposto_ipi.tabelaparaobjeto;
End;

procedure TObjmaterial_ICMS.Edtimposto_ipiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Fimposto_ipi:TFimposto_ipi;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fimposto_ipi:=TFimposto_ipi.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.imposto_ipi.Get_Pesquisa,Self.imposto_ipi.Get_TituloPesquisa,Fimposto_ipi)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.imposto_ipi.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.imposto_ipi.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.imposto_ipi.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fimposto_ipi);
     End;
end;


procedure TObjmaterial_ICMS.EdtoperacaoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.operacao.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.operacao.tabelaparaobjeto;
End;

procedure TObjmaterial_ICMS.EdtoperacaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Foperacao:TFoperacaoNF;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Foperacao:=TFoperacaoNF.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.operacao.Get_Pesquisa,Self.operacao.Get_TituloPesquisa,Foperacao)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.operacao.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.operacao.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.operacao.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Foperacao);
     End;
end;


//CODIFICA EXITONKEYDOWN
procedure TObjmaterial_ICMS.Edtimposto_pisExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.imposto_pis.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.imposto_pis.tabelaparaobjeto;
End;

procedure TObjmaterial_ICMS.Edtimposto_pisKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Fimposto_pis:TFimposto_pis;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fimposto_pis:=TFimposto_pis.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.imposto_pis.Get_Pesquisa,Self.imposto_pis.Get_TituloPesquisa,Fimposto_pis)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.imposto_pis.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.imposto_pis.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.imposto_pis.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fimposto_pis);
     End;
end;

procedure TObjmaterial_ICMS.Edtimposto_cofinsExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.imposto_cofins.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.imposto_cofins.tabelaparaobjeto;
End;

procedure TObjmaterial_ICMS.Edtimposto_cofinsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Fimposto_cofins:TFimposto_cofins;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fimposto_cofins:=TFimposto_cofins.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.imposto_cofins.Get_Pesquisa,Self.imposto_cofins.Get_TituloPesquisa,Fimposto_cofins)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.imposto_cofins.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.imposto_cofins.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.imposto_cofins.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fimposto_cofins);
     End;
end;


procedure TObjmaterial_ICMS.ZerarTabela;
Begin
     With Self do
     Begin
        CODIGO:='';
        ESTADO:='';
        tipocliente.ZerarTabela;
        IMPOSTO.ZerarTabela;
        imposto_ipi.ZerarTabela;
        imposto_pis.ZerarTabela;
        imposto_cofins.ZerarTabela;
        operacao.ZerarTabela;
        PERCENTUALTRIBUTO := '';
     End;
end;

Function TObjmaterial_ICMS.VerificaBrancos:String;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      If (ESTADO='')
      Then Mensagem:=mensagem+'/Estado';

      If (IMPOSTO.get_Codigo='')
      Then Mensagem:=mensagem+'/Imposto';

      If (operacao.get_Codigo='')
      Then Mensagem:=mensagem+'/Opera��o';

      If (IMPOSTO_IPI.get_Codigo='')
      Then Mensagem:=mensagem+'/Imposto IPI';

      If (IMPOSTO_PIS.get_Codigo='')
      Then Mensagem:=mensagem+'/Imposto PIS';

      If (IMPOSTO_cofins.get_Codigo='')
      Then Mensagem:=mensagem+'/Imposto cofins';


       result:=mensagem;
  End;

end;

function TObjmaterial_ICMS.VerificaRelacionamentos: String;
var
mensagem:string;
Begin
     mensagem:='';

     if (Self.tipocliente.LocalizaCodigo(Self.tipocliente.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Tipo de Cliente n�o Encontrado!';

     If (Self.IMPOSTO.LocalizaCodigo(Self.IMPOSTO.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Imposto n�o Encontrado!';

     If (Self.IMPOSTO_IPI.LocalizaCodigo(Self.IMPOSTO_IPI.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Imposto IPI n�o Encontrado!';

     If (Self.imposto_pis.LocalizaCodigo(Self.imposto_pis.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Imposto PIS n�o Encontrado!';

     If (Self.imposto_cofins.LocalizaCodigo(Self.imposto_cofins.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Imposto cofins n�o Encontrado!';

     If (Self.operacao.LocalizaCodigo(Self.operacao.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Opera��o n�o Encontrada!';

     result:=Mensagem;
End;
function TObjmaterial_ICMS.VerificaNumericos: String;
var
   Mensagem:string;
begin

     Mensagem:='';

     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        If (Self.tipocliente.Get_Codigo<>'')
        Then Strtoint(Self.tipocliente.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Tipo de Cliente';
     End;


     try
        If (Self.IMPOSTO.Get_Codigo<>'')
        Then Strtoint(Self.IMPOSTO.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Imposto';
     End;

     try
        If (Self.imposto_ipi.Get_Codigo<>'')
        Then Strtoint(Self.imposto_ipi.Get_Codigo);
     Except
           Mensagem:=mensagem+'/imposto ipi';
     End;

     try
        If (Self.imposto_pis.Get_Codigo<>'')
        Then Strtoint(Self.imposto_pis.Get_Codigo);
     Except
           Mensagem:=mensagem+'/imposto pis';
     End;

     try
        If (Self.imposto_cofins.Get_Codigo<>'')
        Then Strtoint(Self.imposto_cofins.Get_Codigo);
     Except
           Mensagem:=mensagem+'/imposto cofins';
     End;

      try
        If (Self.operacao.Get_Codigo<>'')
        Then Strtoint(Self.operacao.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Opera��o';
     End;

     result:=mensagem;

end;

function TObjmaterial_ICMS.VerificaData: String;
var
Mensagem:string;
begin
     mensagem:='';

     result:=mensagem;
end;

function TObjmaterial_ICMS.VerificaFaixa: String;
var
   Mensagem:string;
begin
     Mensagem:='';
     With Self do
     Begin

        result:=mensagem;
     End;
end;

procedure TObjmaterial_ICMS.Submit_CODIGO(parametro: String);
begin
        Self.CODIGO:=Parametro;
end;
function TObjmaterial_ICMS.Get_CODIGO: String;
begin
        Result:=Self.CODIGO;
end;
procedure TObjmaterial_ICMS.Submit_ESTADO(parametro: String);
begin
        Self.ESTADO:=Parametro;
end;
function TObjmaterial_ICMS.Get_ESTADO: String;
begin
        Result:=Self.ESTADO;
end;

procedure TObjmaterial_ICMS.Imprime(Pcodigo: String);
begin

end;

procedure TObjmaterial_ICMS.edtCSOSNkeydown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Fcsosn:TFCSOSN;
begin

     If (key <>vk_f9)
     Then exit;

     Try

      Fpesquisalocal:=Tfpesquisa.create(Nil);
      Fcsosn :=TFCSOSN .create(nil);

      If (FpesquisaLocal.PreparaPesquisa('select * from tabcsosn','PESQUISA DE CSOSN',Fcsosn)=True) Then
      Begin

        Try

          If (FpesquisaLocal.showmodal=mrok) Then
          Begin

            TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

            If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) Then
              LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring


          End;

        Finally

          FpesquisaLocal.QueryPesq.close;

        End;
        
      End;

     Finally

      FreeandNil(FPesquisaLocal);
      Freeandnil(Fcsosn);

     End;

end;


function TObjmaterial_ICMS.Get_PERCENTUALTRIBUTO: String;
begin
  Result := self.PERCENTUALTRIBUTO;
end;

procedure TObjmaterial_ICMS.Submit_PERCENTUALTRIBUTO(parametro: String);
begin
  self.PERCENTUALTRIBUTO := parametro;
end;

function TObjmaterial_ICMS.atualizaVersao(pCodigoMaterial: string): Boolean;
var
  nomeClasse,nomeTabela:string;

begin
  Result := false;
  try
    nomeClasse := UpperCase( widestring(self.ClassName ) );
    //Tobjdiverso_ICMS, TObjferragem_ICMS, e assim por diante
    //precisa ciar tabdiverso
    nomeTabela := 'TAB'+StringReplace( StringReplace(nomeClasse,'TOBJ','',[rfReplaceAll] ), '_ICMS','', [rfReplaceAll]  );
    Objquery.Close;
    Objquery.SQL.Text := 'UPDATE '+nomeTabela+' SET VERSAO=-1 WHERE CODIGO=' + pCodigoMaterial;
    Objquery.ExecSQL;
  except
    Exit;
  end;
  Result := true;
end;


end.



