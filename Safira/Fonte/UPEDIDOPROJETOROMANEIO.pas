unit UPEDIDOPROJETOROMANEIO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjPEDIDOPROJETOROMANEIO,
  UessencialGlobal, Tabs;

type
  TFPEDIDOPROJETOROMANEIO = class(TForm)
    pnlbotes: TPanel;
    lbCodigo: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    lbLbRomaneio: TLabel;
    edtRomaneio: TEdit;
    lbLbPedidoProjeto: TLabel;
    edtPedidoProjeto: TEdit;
//DECLARA COMPONENTES

    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtEdtRomaneioExit(Sender: TObject);
    procedure edtEdtRomaneioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEdtPedidoProjetoExit(Sender: TObject);
  private
    ObjPEDIDOPROJETOROMANEIO:TObjPEDIDOPROJETOROMANEIO;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPEDIDOPROJETOROMANEIO: TFPEDIDOPROJETOROMANEIO;


implementation

uses Upesquisa, UessencialLocal, UDataModulo, UescolheImagemBotao,
  Uprincipal;

{$R *.dfm}


procedure TFPEDIDOPROJETOROMANEIO.FormCreate(Sender: TObject);
begin

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(imgrodape,'RODAPE');

     Try
       Self.ObjPEDIDOPROJETOROMANEIO:=TObjPEDIDOPROJETOROMANEIO.create(self);
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE PEDIDOS PROJETOS NO ROMANEIO')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);

end;


procedure TFPEDIDOPROJETOROMANEIO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjPEDIDOPROJETOROMANEIO=Nil)
     Then exit;

     If (Self.ObjPEDIDOPROJETOROMANEIO.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjPEDIDOPROJETOROMANEIO.free;
    Action := caFree;
    Self := nil;
end;

procedure TFPEDIDOPROJETOROMANEIO.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbCodigo.Caption:='0';
     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjPEDIDOPROJETOROMANEIO.status:=dsInsert;
     Edtromaneio.setfocus;
     btnovo.Visible:=false;
     btalterar.Visible:=False;
     btexcluir.Visible:=false;
     btrelatorio.Visible:=false;
     btopcoes.Visible:=false;
     btsair.Visible:=False;
     btpesquisar.visible:=False;

end;

procedure TFPEDIDOPROJETOROMANEIO.btSalvarClick(Sender: TObject);
begin

     If ObjPEDIDOPROJETOROMANEIO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjPEDIDOPROJETOROMANEIO.salvar(true)=False)
     Then exit;

     lbCodigo.Caption:=ObjPEDIDOPROJETOROMANEIO.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorio.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFPEDIDOPROJETOROMANEIO.btAlterarClick(Sender: TObject);
begin
    If (ObjPEDIDOPROJETOROMANEIO.Status=dsinactive) and (lbCodigo.Caption<>'')
    Then Begin
                habilita_campos(Self);

                ObjPEDIDOPROJETOROMANEIO.Status:=dsEdit;
                edtromaneio.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btnovo.Visible:=false;
               btalterar.Visible:=False;
               btexcluir.Visible:=false;
               btrelatorio.Visible:=false;
               btopcoes.Visible:=false;
               btsair.Visible:=False;
               btpesquisar.visible:=False;
          End;

end;

procedure TFPEDIDOPROJETOROMANEIO.btCancelarClick(Sender: TObject);
begin
     ObjPEDIDOPROJETOROMANEIO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorio.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;

end;

procedure TFPEDIDOPROJETOROMANEIO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjPEDIDOPROJETOROMANEIO.Get_pesquisa,ObjPEDIDOPROJETOROMANEIO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjPEDIDOPROJETOROMANEIO.status<>dsinactive
                                  then exit;

                                  If (ObjPEDIDOPROJETOROMANEIO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjPEDIDOPROJETOROMANEIO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFPEDIDOPROJETOROMANEIO.btExcluirClick(Sender: TObject);
begin
     If (ObjPEDIDOPROJETOROMANEIO.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (ObjPEDIDOPROJETOROMANEIO.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjPEDIDOPROJETOROMANEIO.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFPEDIDOPROJETOROMANEIO.btRelatorioClick(Sender: TObject);
begin
    ObjPEDIDOPROJETOROMANEIO.Imprime(lbCodigo.Caption);
end;

procedure TFPEDIDOPROJETOROMANEIO.btSairClick(Sender: TObject);
begin
    Self.close;
end;


procedure TFPEDIDOPROJETOROMANEIO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFPEDIDOPROJETOROMANEIO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFPEDIDOPROJETOROMANEIO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;

    if(key=vk_f1) then
    begin
        Fprincipal.chamaPesquisaMenu;
    end;

;
end;

procedure TFPEDIDOPROJETOROMANEIO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFPEDIDOPROJETOROMANEIO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjPEDIDOPROJETOROMANEIO do
    Begin
        Submit_CODIGO(lbCodigo.Caption);
        Romaneio.Submit_codigo(edtRomaneio.text);
        PedidoProjeto.Submit_codigo(edtPedidoProjeto.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFPEDIDOPROJETOROMANEIO.LimpaLabels;
begin
    lbCodigo.Caption:='';
end;

function TFPEDIDOPROJETOROMANEIO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjPEDIDOPROJETOROMANEIO do
     Begin
        lbCodigo.Caption:=Get_CODIGO;
        EdtRomaneio.text:=Romaneio.Get_codigo;
        EdtPedidoProjeto.text:=PedidoProjeto.Get_codigo;

//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFPEDIDOPROJETOROMANEIO.TabelaParaControles: Boolean;
begin
     If (ObjPEDIDOPROJETOROMANEIO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFPEDIDOPROJETOROMANEIO.edtEdtRomaneioExit(Sender: TObject);
begin
     ObjPEDIDOPROJETOROMANEIO.EdtRomaneioExit(sender,nil);
end;

procedure TFPEDIDOPROJETOROMANEIO.edtEdtRomaneioKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjPEDIDOPROJETOROMANEIO.EdtRomaneioKeyDown(sender,key,shift,nil);
end;

procedure TFPEDIDOPROJETOROMANEIO.edtEdtPedidoProjetoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjPEDIDOPROJETOROMANEIO.EdtPedidoProjetoKeyDown(sender,key,shift,nil);
end;

procedure TFPEDIDOPROJETOROMANEIO.edtEdtPedidoProjetoExit(Sender: TObject);
begin
     ObjPEDIDOPROJETOROMANEIO.EdtPedidoProjetoExit(sender,nil);
end;

end.
