REG0000
select e.razaosocial,e.cnpj,e.estado,e.ie,e.codigocidade
from tabempresa e
where e.codigo = 1;

REG0005
select e.fantasia,e.cep,e.endereco,e.numero,e.complemento,e.bairro,
e.fone,e.fax,e.email
from tabempresa e
where e.codigo = 1;

REG0100
select c.nome,c.cpf,c.crc,c.cnpj,c.cep,c.endereco,c.numero,c.complemento,
c.bairro,c.fone,c.fax,c.email,cid.codigocidade
from tabcontador c
inner join tabcidade cid on cid.codigo = c.cidade
where c.codigo = 1;

REG0150C
select c.nome,c.codigopais,c.rg_ie,c.codigocidade,c.endereco,
c.numero,c.bairro,c.cpf_cgc,c.estado
from tabcliente c
where c.codigo =:codigo;

REG0150F
select f.razaosocial,f.codigopais,ie,f.codigocidade,f.endereco,
f.numero,f.bairro,f.cgc,f.estado
from tabfornecedor f
where f.codigo =:codigo;

REG0190
select u.descricao
from tabunidademedida u
where upper(u.sigla) = upper(:psigla);

REG0200 PRODUTO
select p.descricao,p.cod_barras,p.unidade,p.ncm
from tabproduto p
where p.codigo = :codigo;

REG0200 SERVICO
vazio;

REG0400
select cfop.nome
from tabcfop cfop
where cfop.codigo =:pcfop;

REGC100 SAIDA
select NF.codigo,NF.cliente,NF.fornecedor,modNF.codigo_fiscal,NF.situacao,
nf.nfecomplementar,NF.numero,Nfe.chave_acesso,NF.dataemissao,
NF.datasaida, NF.valorfinal, NF.valortotal, nf.desconto,
NF.freteporcontatransportadora,nf.valorseguro,NF.outrasdespesas,
NF.valorfrete,NF.basecalculoicms,NF.valoricms,NF.basecalculoicms_substituicao,
NF.valoricms_substituicao, NF.valortotalipi, NF.indpag, NF.serienf,NF.valorpis,
NF.valorcofins,NF.nfentrada
from TABNOTAFISCAL NF
inner join tabmodelonf modNF on modNF.codigo = NF.modelo_nf
left  join tabnfe nfe on nfe.codigo = NF.nfe
where NF.dataemissao >= :DataIni and NF.dataemissao <= :dataFin
order by NF.dataemissao;

REGC170 SAIDA
select NF.codigo,SUBSTRING(pnf.descricao FROM 1 FOR 3)||pnf.material||'COR'||pnf.codigocor as cod_produto,
pnf.descricao,pnf.quantidade,pnf.unidade,
pnf.quantidade*pnf.valorunitario as valor,pnf.desconto,pnf.situacaotributaria_tabelaa,pnf.situacaotributaria_tabelab,
pnf.csosn,pnf.cfop,pnf.reducaobasecalculo,pnf.valorfinal,pnf.percentualicms,pnf.ncm,pnf.bc_icms_st,
pnf.valor_icms_st,pnf.percentualicmsst,pnf.percentual_pis,pnf.valor_pis,pnf.percentual_cofins,pnf.valor_cofins,
pnf.cst_pis,pnf.cst_cofins,pnf.cst_ipi,pnf.bc_ipi,pnf.percentual_ipi,pnf.valor_ipi,pnf.valor_icms,pnf.bc_icms
from tabnotafiscal NF
inner join viewprodutosvenda_sped pnf on pnf.notafiscal = NF.codigo
inner join tabmodelonf MODELO on MODELO.codigo = NF.modelo_nf
where (pnf.notafiscal = :pCodigoNota) and (MODELO.codigo_fiscal <> '55') and
(NF.situacao <> 'C') and (NF.situacao <> 'Z') and (NF.situacao <> 'D') and
(nf.nfecomplementar <> 'S');

REGC190 SAIDA CST
select pnf.situacaotributaria_tabelaa as origem,pnf.situacaotributaria_tabelab as cst,pnf.cfop as cfop,
pnf.percentualicms,pnf.valorfinal,coalesce(pnf.BC_ICMS,0)as bc_icms,pnf.valor_ipi,
pnf.valorfrete,pnf.bc_icms_st,pnf.valor_icms_st,pnf.valoroutros,pnf.valor_icms
from viewprodutosvenda_sped pnf
where pnf.notafiscal = :pCodigoNota
and pnf.situacaotributaria_tabelab is not null
order by origem,cst,cfop,percentualicms;

REGC190 SAIDA CSOSN
select pnf.csosn,pnf.cfop,pnf.percentualicms,pnf.valorfinal,pnf.BC_ICMS,pnf.valor_icms
from viewprodutosvenda_sped pnf
where pnf.notafiscal =:pCodigoNota
and pnf.csosn is not null
order by pnf.csosn, pnf.cfop, pnf.percentualicms;

REGC100 ENTRADA
select EP.codigo,EP.fornecedor,MODELO.codigo_fiscal,EP.nf,EP.emissaonf,EP.data,EP.valorfinal,
EP.descontos,EP.valorprodutos,EP.FRETEPARAFORNECEDOR,EP.valorfrete,
EP.valorseguro,EP.OUTROSGASTOS,EP.basecalculoicms,EP.valoricms,
EP.basecalculoicmssubstituicao,EP.valoripi,ICMSSUBSTITUTO,chavenfe,EP.indpag,EP.serienf,
ep.creditopis,ep.creditocofins
from tabentradaprodutos EP
left join tabmodelonf MODELO on MODELO.codigo = EP.modelonf
where EP.data >= :dataIni and EP.data <= :dataFIn
order by EP.data;

REGC170 ENTRADA
select pEP.codigo,pep.nomematerial||coalesce('-'||pep.nomecor,'') as nomeproduto,pep.ncm,pEP.quantidade,pEP.unidade,
pEP.quantidade * pEP.valor as valorTotal,
pEP.desconto,pEP.csta,pEP.cstb,pEP.cfop,pEP.reducaobasecalculo,
pEP.valorfinal,pEP.creditoicms as aliquota,csosn,coalesce(pep.bc_icms,0) as bc_icms,coalesce(pep.valor_icms,0) as valoricms,
0 as bc_icms_st,0 as valor_icms_st,0 as percentual_icms_st,
pep.ppis,pep.creditopis,pep.pcofins,pep.creditocofins,pEP.cstpis,pEP.cstcofins,pEP.cstipi,pEP.bcipi,pEP.creditoipi,pEP.valor_ipi
from proc_materiais_ep pEP
where pEP.entrada = :pCodigoEntrada;

REGC190 ENTRADA CST
select  pm_ep.csta,pm_ep.cstb,pm_ep.cfop,pm_ep.creditoicms as aliquota,pm_ep.valorfinal,
coalesce(pm_ep.bc_icms,0) as bc_icms,pm_ep.valor_ipi,pm_ep.valorfrete,pm_ep.bcicmsst,pm_ep.valoricmsst,
pm_ep.valoroutros,pm_ep.valor_icms
from proc_materiais_ep pm_ep
where pm_ep.entrada=:pCodigoNota
and pm_ep.cstb is not null
order by pm_ep.csta,pm_ep.cstb,pm_Ep.cfop,pm_ep.creditoicms;

REGC190 ENTRADA CSOSN
select pm_ep.csosn,pm_ep.cfop,pm_ep.creditoicms as aliquota,pm_ep.valorfinal,pm_ep.bc_icms,pm_ep.valor_icms
from proc_materiais_ep pm_ep
where (pm_ep.entrada = :pCodigoNota) and (pm_ep.csosn is not null)
order by pm_ep.csosn, pm_ep.cfop, pm_ep.creditoicms;

REGH010
select distinct(p.codigo) as produto,p.unidade,p.quantidade,'' as quantidadeconsumo
from proc_retornaprodutos_registroh(:datainicial,:dataFinal) p;

REG1010
vazio;

REG1600
vazio;