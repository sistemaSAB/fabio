unit UobjVIDRO_PP_PEDIDO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,
UOBJVIDROCOR;

Type
   TObjVIDRO_PP_PEDIDO=class

          Public
                ObjDataSource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                //PedidoProjeto:TOBJPEDIDO_PROJ;
                VidroCor:TOBJVIDROCOR;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure ResgataVIDRO_PP(PPedido, PPEdidoProjeto: string);

                Procedure Submit_Altura(parametro: string);
                Function Get_Altura: string;
                Procedure Submit_Largura(parametro: string);
                Function Get_Largura: string;

                Procedure Submit_AlturaArredondamento(parametro: string);
                Function Get_AlturaArredondamento: string;
                Procedure Submit_LarguraArredondamento(parametro: string);
                Function Get_LarguraArredondamento: string;

                Procedure Submit_Complemento(parametro: String);
                Function Get_Complemento: String;

                Procedure Submit_Codigo(parametro: string);
                Function  Get_Codigo: string;
                Procedure Submit_Quantidade(parametro: STRing);
                Function  Get_Quantidade: STRing;
                Procedure Submit_Valor(parametro: string);
                Function  Get_Valor: string;
                Function  Get_ValorFinal:string;
                Function  get_PedidoProjeto:string;
                Procedure Submit_PedidoProjeto(parametro:string);
                procedure EdtVidroCorExit(Sender: TObject;Var PEdtCodigo:TEdit; LABELNOME:TLABEL);
                procedure EdtVidroCorKeyDown(Sender: TObject; Var PEdtCodigo :TEdit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtVidroCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;

                Function  DeletaVidro_PP(PPedidoProjeto:string):Boolean;
                Function  Soma_por_PP(PpedidoProjeto:string):Currency;
                Procedure RetornaDadosRel(PpedidoProjeto:string;STrDados:TStrings;ComValor:Boolean);
                Procedure RetornaDadosRel2(PpedidoProjeto:string;STrDados:TStrings;ComValor:Boolean);
                Procedure RetornaDadosRelProjetoBranco(PpedidoProjeto:string;STrDados:TStrings; pquantidade:integer;Var PTotal:Currency);

                function    DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;PControlaNegativo: Boolean;Pdata:String): boolean;
                Function    DiminuiAumentaEstoquePedido(NotaFiscal,PpedidoProjeto: String;PControlaNegativo: Boolean;Pdata:String;aumenta:String): boolean;
                function    AumentaEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;Pdata:string): Boolean;


                Function RetornaVidroCor(PPedidoProjeto:string):string;
                procedure EdtvidroCorExit_codigo(Sender: TObject; LABELNOME: TLABEL);

         Private
               Objquery:Tibquery;
               ObjQueryPesquisa:TIBquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Quantidade:STRing;
               Valor:string;
               ValorFinal:string;
               Altura  :string;
               Largura :string;
               AlturaArredondamento  :string;
               LarguraArredondamento :string;
               Complemento:String;
               PedidoProjeto:string;


               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UessencialLocal;





Function  TObjVIDRO_PP_PEDIDO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.PedidoProjeto:=fieldbyname('PedidoProjeto').asstring;
        
        If(FieldByName('VidroCor').asstring<>'')
        Then Begin
                 If (Self.VidroCor.LocalizaCodigo(FieldByName('VidroCor').asstring)=False)
                 Then Begin
                          Messagedlg('VidroCor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.VidroCor.TabelaparaObjeto;
        End;
        Self.Quantidade:=fieldbyname('Quantidade').asstring;
        Self.Valor:=fieldbyname('Valor').asstring;
        Self.ValorFinal:=fieldbyname('ValorFinal').AsString;
        Self.Largura:=fieldbyname('LArgura').AsString;
        Self.Altura:=fieldbyname('Altura').AsString;
        Self.LarguraArredondamento:=fieldbyname('LArguraArredondamento').AsString;
        Self.AlturaArredondamento:=fieldbyname('AlturaArredondamento').AsString;
        Self.Complemento:=fieldbyname('Complemento').AsString;


        result:=True;
     End;
end;


Procedure TObjVIDRO_PP_PEDIDO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET

begin

  With Objquery do
  Begin
        ParamByName('Codigo').asstring                := Self.Codigo;
        ParamByName('PedidoProjeto').asstring         := Self.PedidoProjeto;
        ParamByName('VidroCor').asstring              := Self.VidroCor.GET_CODIGO;
        ParamByName('Quantidade').asstring            := virgulaparaponto(Self.Quantidade);
        ParamByName('Valor').asstring                 := format_db( formata_valor(self.Valor) ); {virgulaparaponto(Self.Valor);}
        ParamByName('Altura').AsString                := virgulaparaponto(self.Altura);
        ParamByName('Largura').AsString               := virgulaparaponto(self.Largura);
        ParamByName('AlturaArredondamento').AsString  := virgulaparaponto(self.AlturaArredondamento);
        ParamByName('LarguraArredondamento').AsString := virgulaparaponto(self.LarguraArredondamento);
        ParamByName('Complemento').AsString           := Self.Complemento;

  End;

End;

//***********************************************************************

function TObjVIDRO_PP_PEDIDO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;


   if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  {if (Self.VerificaRelacionamentos=False)
  Then Exit;  }


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0);
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjVIDRO_PP_PEDIDO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        PedidoProjeto:='';
        VidroCor.ZerarTabela;
        Quantidade:='';
        Valor:='';
        ValorFinal:='';
        Altura:='';
        LArgura:='';
        AlturaArredondamento:='';
        LArguraArredondamento:='';
        Complemento:='';
     End;
end;

Function TObjVIDRO_PP_PEDIDO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjVIDRO_PP_PEDIDO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.VidroCor.LocalizaCodigo(Self.VidroCor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ VidroCor n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjVIDRO_PP_PEDIDO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.PedidoProjeto<>'')
        Then Strtoint(Self.PedidoProjeto);
     Except
           Mensagem:=mensagem+'/PedidoProjeto';
     End;
     try
        If (Self.VidroCor.Get_Codigo<>'')
        Then Strtoint(Self.VidroCor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/VidroCor';
     End;

     try
        Strtofloat(Self.Quantidade);
     Except
           Mensagem:=mensagem+'/Quantidade';
     End;


     if (Self.AlturaArredondamento='')
     Then Self.AlturaArredondamento:='0';

     if (Self.LarguraArredondamento='')
     Then Self.LarguraArredondamento:='0';



     try
        Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjVIDRO_PP_PEDIDO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjVIDRO_PP_PEDIDO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjVIDRO_PP_PEDIDO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro VIDRO_PP vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,PedidoProjeto,VidroCor,Quantidade,Valor,ValorFinal,Altura, Largura,AlturaArredondamento,LarguraArredondamento, Complemento');
           SQL.ADD(' from  TabVidro_PP');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjVIDRO_PP_PEDIDO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjVIDRO_PP_PEDIDO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjVIDRO_PP_PEDIDO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase; 

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDataSource.DataSet:=Self.ObjQueryPesquisa;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        //Self.PedidoProjeto:=TOBJPEDIDO_PROJ.create;
        Self.VidroCor:=TOBJVIDROCOR.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabVidro_PP(Codigo,PedidoProjeto,VidroCor');
                InsertSQL.add(' ,Quantidade,Valor,Altura, Largura,AlturaArredondamento,LarguraArredondamento, Complemento)');
                InsertSQL.add('values (:Codigo,:PedidoProjeto,:VidroCor,:Quantidade');
                InsertSQL.add(' ,:Valor,:Altura, :Largura,:AlturaArredondamento,:LarguraArredondamento, :Complemento)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabVidro_PP set Codigo=:Codigo,PedidoProjeto=:PedidoProjeto');
                ModifySQL.add(',VidroCor=:VidroCor,Quantidade=:Quantidade,Valor=:Valor');
                ModifySQL.add(',Altura=:Altura, Largura=:Largura,AlturaArredondamento=:AlturaArredondamento,LarguraArredondamento=:LarguraArredondamento, Complemento=:Complemento');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabVidro_PP where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjVIDRO_PP_PEDIDO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjVIDRO_PP_PEDIDO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabVIDRO_PP');
     Result:=Self.ParametroPesquisa;
end;

function TObjVIDRO_PP_PEDIDO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de VIDRO_PP ';
end;


function TObjVIDRO_PP_PEDIDO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENVIDRO_PP,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENVIDRO_PP,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjVIDRO_PP_PEDIDO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(Self.ObjQueryPesquisa);
    Freeandnil(Self.ObjDataSource);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //Self.PedidoProjeto.FREE;
    Self.VidroCor.FREE;
//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjVIDRO_PP_PEDIDO.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjVIDRO_PP_PEDIDO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjVIDRO_PP_PEDIDO.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjVIDRO_PP_PEDIDO.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjVIDRO_PP_PEDIDO.Submit_Quantidade(parametro: string);
begin
        Self.Quantidade:=Parametro;
end;
function TObjVIDRO_PP_PEDIDO.Get_Quantidade: STRing;
begin
        Result:=Self.Quantidade;
end;
procedure TObjVIDRO_PP_PEDIDO.Submit_Valor(parametro: string);
begin
        Self.Valor:=Parametro;
end;
function TObjVIDRO_PP_PEDIDO.Get_Valor: string;
begin
        Result:=Self.Valor;
end;
//CODIFICA GETSESUBMITS


procedure TObjVIDRO_PP_PEDIDO.EdtVidroCorExit(Sender: TObject;Var PEdtCodigo:TEdit;  LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.VidroCor.localizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     Self.VidroCor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.VidroCor.Vidro.Get_Descricao+' - '+Self.VidroCor.Cor.Get_Descricao;
     PEdtCodigo.Text:=Self.VidroCor.Get_Codigo;

End;

procedure TObjVIDRO_PP_PEDIDO.EdtVidroCorKeyDown(Sender: TObject;Var PedtCodigo :TEdit; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   SQL:string;
begin

     If (key <>vk_f9)
     Then exit;


     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FpesquisaLocal.PesquisaEmEstoque := True;
            FpesquisaLocal.NomeCadastroPersonalizacao:='FTROCAMATERIALPEDIDOCONCLUIDO.BTTROCAR' ;
            If (FpesquisaLocal.PreparaPesquisa(Self.VidroCor.Get_Pesquisa,Self.VidroCor.Get_TituloPesquisa,Nil)=True) Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('REF_VIDRO').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 LABELNOME.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('Vidro').asstring;

                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;

            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjVIDRO_PP_PEDIDO.EdtVidroCorKeyDown(Sender: TObject;var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from  ViewVidroCor',Self.VidroCor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 LABELNOME.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('Vidro').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

//CODIFICA EXITONKEYDOWN


procedure TObjVIDRO_PP_PEDIDO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJVIDRO_PP';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjVIDRO_PP_PEDIDO.ResgataVIDRO_PP(PPedido, PPEdidoProjeto: string);
begin
       With Self.ObjQueryPesquisa do
       Begin
           Close;
           SQL.Clear;
           Sql.add('Select TabVidro.Referencia,TabVidro.Descricao as Vidro,');
           Sql.add('TabCor.Descricao as Cor,');
           Sql.add('TabVidro_PP.Quantidade,');
           Sql.add('TabVidro_PP.Valor,');
           Sql.add('TabVidro_PP.ValorFinal,TabPedido_Proj.Codigo as PedidoProjeto,');
           Sql.Add('TabVidro_PP.Altura, TabVidro_PP.Largura,TabVidro_PP.AlturaArredondamento, TabVidro_PP.LarguraArredondamento, TabVidro_PP.Complemento,');
           Sql.Add('TabVidro_PP.Codigo');
           Sql.add('from TabVidro_PP');
           Sql.add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabVidro_PP.PedidoProjeto');
           Sql.add('Join TabPedido on TabPedido.Codigo = TabPedido_Proj.Pedido');
           Sql.add('Join TabProjeto on TabProjeto.Codigo = TabPedido_Proj.Projeto');
           Sql.add('Join TabVidroCor on TabVidroCor.Codigo = TabVidro_PP.VidroCor');
           Sql.add('join TabVidro  on TabVidro.Codigo = TabVidroCor.Vidro');
           Sql.add('Join TabCor on TabCor.Codigo = TabVidroCor.Cor');

           Sql.add('Where TabPedido_Proj.Pedido = '+PPedido);
           if (PpedidoProjeto <> '' )
           then Sql.add('and   TabPedido_Proj.Codigo ='+PPedidoProjeto);

           if (PPedido = '')then
           exit;

           Open;
       end;
end;

function TObjVIDRO_PP_PEDIDO.DeletaVidro_PP(PPedidoProjeto: string): Boolean;
Begin
     Result:=False;
     With Objquery  do
     Begin
        Close;
        SQL.Clear;
        Sql.Add('Delete from TabVidro_PP where PedidoProjeto='+ppedidoprojeto);
        Try
            execsql;
        Except
              On E:Exception do
              Begin
                   Messagedlg('Erro na tentativa de Excluir os Vidros do Pedido '+#13+E.message,mterror,[mbok],0);
                   exit;
              End;
        End;
     end;
     Result:=true;
end;

function TObjVIDRO_PP_PEDIDO.Get_ValorFinal: string;
begin
    Result:=Self.ValorFinal;
end;


function TObjVIDRO_PP_PEDIDO.Soma_por_PP(PpedidoProjeto: string): Currency;
begin

     Self.Objquery.close;
     Self.Objquery.sql.clear;
     Self.Objquery.sql.add('Select sum(valorfinal) as SOMA from TabVidro_PP where PedidoProjeto='+PpedidoProjeto);
     Try
         Self.Objquery.open;
         Result:=Self.Objquery.fieldbyname('soma').asfloat;
     Except
           Result:=0;
     End;

end;

procedure TObjVIDRO_PP_PEDIDO.RetornaDadosRel(PpedidoProjeto: string;  STrDados: TStrings;ComValor:Boolean);
begin
     if (PpedidoProjeto='')
     Then exit;

     With Self.ObjQueryPesquisa do
     Begin
          close;
          SQL.clear;
          sql.add('Select codigo from TabVidro_pp where PedidoProjeto='+PpedidoProjeto);
          open;
          first;

          if (RecordCount = 0)then
          exit;

          While not(eof) do
          Begin
               if(ComValor=True) then
               begin
                     Self.LocalizaCodigo(fieldbyname('codigo').asstring);
                     Self.TabelaparaObjeto;

                     STrDados.Add(CompletaPalavra(Self.VidroCor.Vidro.Get_Referencia,10,' ')+' '+
                                    CompletaPalavra(Self.VidroCor.Cor.Get_Referencia+'-'+Self.VidroCor.Cor.Get_Descricao,16,' ')+' '+
                                    CompletaPalavra(Self.VidroCor.Vidro.Get_Descricao,45,' ')+' '+
                                    CompletaPalavra(Self.Get_Quantidade+' '+Self.VidroCor.Vidro.Get_Unidade,6,' ')+' '+
                                    CompletaPalavra_a_Esquerda(formata_valor(Self.Get_Valor),10,' ')+' '+
                                    CompletaPalavra_a_Esquerda(formata_valor(StrToCurr(Self.Get_Quantidade)*StrToCurr(Self.Get_Valor)),8,' '));

                     //A Camila pediu que no caso de projeto em branco saisse tmbm a largura e altura alem da quantidade
                     //do vidro (10/10/2008)
                     if (Self.Altura<>'') and (Self.Altura<>'')
                     Then STrDados.Add('ALTURA: '+Self.Get_Altura+' LARGURA: '+Self.Get_Largura);
               end
               else
               begin
                     Self.LocalizaCodigo(fieldbyname('codigo').asstring);
                     Self.TabelaparaObjeto;

                     STrDados.Add(CompletaPalavra(Self.VidroCor.Vidro.Get_Referencia,10,' ')+' '+
                                    CompletaPalavra(Self.VidroCor.Cor.Get_Referencia+'-'+Self.VidroCor.Cor.Get_Descricao,16,' ')+' '+
                                    CompletaPalavra(Self.VidroCor.Vidro.Get_Descricao,45,' ')+' '+
                                    CompletaPalavra(Self.Get_Quantidade+' '+Self.VidroCor.Vidro.Get_Unidade,6,' '));

                     //A Camila pediu que no caso de projeto em branco saisse tmbm a largura e altura alem da quantidade
                     //do vidro (10/10/2008)
                     if (Self.Altura<>'') and (Self.Altura<>'')
                     Then STrDados.Add('ALTURA: '+Self.Get_Altura+' LARGURA: '+Self.Get_Largura);
               end;

               next;
          end;
          if(ComValor=True) then
          begin
              close;
              sql.clear;
              sql.add('Select sum(ValorFinal) as SOMA FROM TabVidro_PP where PedidoProjeto='+PpedidoProjeto);
              open;
              STrDados.Add('?'+CompletaPalavra_a_Esquerda('Total VIDROS: '+formata_valor(fieldbyname('soma').asstring),100,' '));
              STrDados.Add(' ');
          end;
     End;

end;

function TObjVIDRO_PP_PEDIDO.AumentaEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;Pdata:String): boolean;
var
PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          Sql.add('Select codigo,Vidrocor,quantidade from TabVidro_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;
          first;
          While not(eof) do
          Begin
              Self.VidroCor.LocalizaCodigo(fieldbyname('Vidrocor').asstring);
              Self.VidroCor.TabelaparaObjeto;

              try
                 PestoqueAtual:=strtofloat(Self.VidroCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual do Vidro '+Self.VidroCor.Vidro.Get_Descricao+' da cor '+Self.VidroCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              Try
                  PnovoEstoque:=PestoqueAtual+fieldbyname('quantidade').asfloat;
              Except
                    Messagedlg('Erro na gera��o do novo Estoque do Vidro '+Self.VidroCor.Vidro.Get_Descricao+' da cor '+Self.VidroCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              if(self.VidroCor.AumentaEstoque(fieldbyname('quantidade').asstring,fieldbyname('vidrocor').asstring)=False)
              then Exit;

              next;
          End;
          result:=True;
          exit;
     End;
end;

function TObjVIDRO_PP_PEDIDO.DiminuiAumentaEstoquePedido(NotaFiscal,PpedidoProjeto: String;PControlaNegativo: Boolean;Pdata:String;aumenta:String): boolean;
var
PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          if(aumenta='N')
          then Sql.add('Select codigo,Vidrocor,(quantidade*-1) as quantidade from TabVidro_PP where PedidoProjeto='+PpedidoProjeto)
          else Sql.add('Select codigo,Vidrocor, quantidade from TabVidro_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;
          first;
          While not(eof) do
          Begin
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Status:=dsinsert;
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(fieldbyname('quantidade').asstring);
              OBJESTOQUEGLOBAL.Submit_DATA(Pdata);
              OBJESTOQUEGLOBAL.Submit_vidroCOR(fieldbyname('vidrocor').asstring);
              OBJESTOQUEGLOBAL.Submit_NotaFiscal(NotaFiscal);
              OBJESTOQUEGLOBAL.Submit_vidro_PP(fieldbyname('codigo').asstring);
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('PEDIDO (NOTA FISCAL)');
              Self.VidroCor.LocalizaCodigo(fieldbyname('Vidrocor').asstring);
              Self.VidroCor.TabelaparaObjeto;

              try
                 PestoqueAtual:=strtofloat(Self.VidroCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual do Vidro '+Self.VidroCor.Vidro.Get_Descricao+' da cor '+Self.VidroCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              Try
                  PnovoEstoque:=PestoqueAtual+fieldbyname('quantidade').asfloat;
              Except
                    Messagedlg('Erro na gera��o do novo Estoque do Vidro '+Self.VidroCor.Vidro.Get_Descricao+' da cor '+Self.VidroCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              if (Pnovoestoque<0)
              then Begin
                        if (PControlaNegativo=True)
                        Then begin
                                  Messagedlg('O Vidro '+Self.VidroCor.Vidro.Get_Descricao+' da cor '+Self.VidroCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                  exit;
                        End;
              End;
              //self.VidroCor.DiminuiEstoque(fieldbyname('quantidade').asstring,fieldbyname('vidrocor').asstring);
              if (OBJESTOQUEGLOBAL.Salvar(False)=False)
              then exit;

              next;
          End;
          result:=True;
          exit;
     End;
end;



function TObjVIDRO_PP_PEDIDO.DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;PControlaNegativo: Boolean;Pdata:String): boolean;
var
PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          Sql.add('Select codigo,Vidrocor,(quantidade*-1) as quantidade from TabVidro_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;
          first;
          While not(eof) do
          Begin
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Status:=dsinsert;
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(fieldbyname('quantidade').asstring);
              OBJESTOQUEGLOBAL.Submit_DATA(Pdata);
              OBJESTOQUEGLOBAL.Submit_vidroCOR(fieldbyname('vidrocor').asstring);
              OBJESTOQUEGLOBAL.Submit_PEDIDOPROJETOROMANEIO(PpedidoProjetoRomaneio);
              OBJESTOQUEGLOBAL.Submit_vidro_PP(fieldbyname('codigo').asstring);
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('ROMANEIO');
              

              Self.VidroCor.LocalizaCodigo(fieldbyname('Vidrocor').asstring);
              Self.VidroCor.TabelaparaObjeto;

              try
                 PestoqueAtual:=strtofloat(Self.VidroCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual do Vidro '+Self.VidroCor.Vidro.Get_Descricao+' da cor '+Self.VidroCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              Try
                  PnovoEstoque:=PestoqueAtual+fieldbyname('quantidade').asfloat;
              Except
                    Messagedlg('Erro na gera��o do novo Estoque do Vidro '+Self.VidroCor.Vidro.Get_Descricao+' da cor '+Self.VidroCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              if (Pnovoestoque<0)
              then Begin
                        if (PControlaNegativo=True)
                        Then begin
                                  Messagedlg('O Vidro '+Self.VidroCor.Vidro.Get_Descricao+' da cor '+Self.VidroCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                  exit;
                        End;
              End;
              //self.VidroCor.DiminuiEstoque(fieldbyname('quantidade').asstring,fieldbyname('vidrocor').asstring);
              if (OBJESTOQUEGLOBAL.Salvar(False)=False)
              then exit;

              next;
          End;
          result:=True;
          exit;
     End;
end;

procedure TObjVIDRO_PP_PEDIDO.RetornaDadosRelProjetoBranco(PpedidoProjeto: string; STrDados: TStrings; pquantidade:integer;var PTotal: Currency);
Var Cont:Integer;
    PStrList:TStringList;

begin
  if (PpedidoProjeto='') then
    exit;

  try
    try
      PStrList:=TStringList.Create;
    except
      MensagemErro('Erro ao tentar criar o PStrList');
      exit;
    end;
    with Self.ObjQueryPesquisa do
    begin
      Close;
      SQL.Clear;
      SQL.Text := ' select vp.complemento, v.referencia, c.referencia || ' + QuotedStr('-') +  ' || c.descricao as cor, ' +
                  ' v.descricao, v.unidade, vp.valor, sum(vp.quantidade) * cast(:qtde as numeric(9,2)) as quantidade, '+
                  ' sum(vp.quantidade) * cast(:qtde as numeric(9,2)) * vp.valor as mult '+
                  ' from tabvidro_pp vp '+
                  ' inner join tabvidrocor vc on (vp.vidrocor = vc.codigo) '+
                  ' inner join tabcor c on (vc.cor = c.codigo) '+
                  ' inner join tabvidro v on (vc.vidro = v.codigo) '+
                  ' where vp.pedidoprojeto = :pedidoprojeto '+
                  ' group by 1,2,3,4,5,6 ';

      ParamByName('qtde').AsInteger := pquantidade;
      ParamByName('pedidoprojeto').AsString := PpedidoProjeto;
      open;

      first;

      if (RecordCount = 0)then
      exit;

      // Cabe�alho ITEM , DESCRICAO, QTDE, VALOR
      STrDados.Add('?'+CompletaPalavra('�TEM',8,' ')+' '+
                       CompletaPalavra('  COR',14,' ')+' '+
                       CompletaPalavra('   DESCRI��O',39,' ')+' '+
                       '          QTDE      VAL.UN     VALOR');


      while not(eof) do
      begin
        PreencheStringList(PStrList, fieldbyname('complemento').asstring);

        STrDados.Add(CompletaPalavra(fieldbyname('referencia').asstring,10,' ')+' '+
                    CompletaPalavra(fieldbyname('cor').asstring,15,' ')+' '+
                    CompletaPalavra(fieldbyname('descricao').asstring,44,' ')+' '+
                    CompletaPalavra(fieldbyname('quantidade').asstring+' '+fieldbyname('unidade').asstring,6,' ')+' '+
                    CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),10,' ')+' '+
                    CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('mult').asstring),10,' '));

        for Cont:=0 to PStrList.Count-1 do
        begin
          STrDados.Add('                            '+ CompletaPalavra(PStrList.Strings[Cont],30,' '));
        end;
        next;
      end;
      STrDados.Add('');

      close;
      sql.clear;
      sql.add('Select sum(ValorFinal) as SOMA FROM TabVidro_PP where PedidoProjeto='+PpedidoProjeto);
      open;

      PTotal:=PTotal+(fieldbyname('soma').AsCurrency*pquantidade);
    end;
  finally
    FreeAndNil(PStrList);
  end;
end;

function TObjVIDRO_PP_PEDIDO.Get_Altura: string;
begin
    Result:= Self.Altura;
end;

function TObjVIDRO_PP_PEDIDO.Get_Complemento: String;
begin
    Result:=Self.Complemento;
end;

function TObjVIDRO_PP_PEDIDO.Get_Largura: string;
begin
    Result:=Self.Largura;
end;

procedure TObjVIDRO_PP_PEDIDO.Submit_Altura(parametro: string);
begin
    Self.Altura:=parametro;
end;

procedure TObjVIDRO_PP_PEDIDO.Submit_Complemento(parametro: String);
begin
    Self.Complemento:=parametro;
end;

procedure TObjVIDRO_PP_PEDIDO.Submit_Largura(parametro: string);
begin
    Self.Largura:=parametro;
end;

function TObjVIDRO_PP_PEDIDO.RetornaVidroCor(PPedidoProjeto: string): string;
begin
     Result:='';
     With Self.Objquery  do
     Begin
          Close;
          Sql.Clear;
          Sql.Add('Select TabVidro_PP.VidroCor from TabVidro_PP');
          Sql.Add('Where TabVidro_PP.PedidoProjeto = '+PPedidoProjeto);
          Open;

          Result:=fieldbyname('VidroCor').AsString;
     end;
end;

function TObjVIDRO_PP_PEDIDO.get_PedidoProjeto: string;
begin
     Result:=Self.PedidoProjeto;
end;

procedure TObjVIDRO_PP_PEDIDO.Submit_PedidoProjeto(parametro: string);
begin
     Self.PedidoProjeto:=parametro;
end;


procedure TObjvidro_PP_PEDIDO.EdtvidroCorExit_codigo(Sender: TObject;
LABELNOME: TLABEL);
begin
     labelnome.caption:='';
     If (Tedit(Sender).Text='')
     Then exit;

     If (Self.vidroCor.localizacodigo (Tedit(Sender).Text)=False)
     Then Begin
               Tedit(Sender).Text:='';
               exit;
     End;
     Self.vidroCor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.vidroCor.vidro.Get_Descricao+'-'+Self.vidroCor.Cor.Get_Descricao;
     Tedit(Sender).Text:=Self.vidroCor.Get_Codigo;
end;

function TObjVIDRO_PP_PEDIDO.Get_AlturaArredondamento: string;
begin
     Result:=Self.AlturaArredondamento;
end;

function TObjVIDRO_PP_PEDIDO.Get_LarguraArredondamento: string;
begin
     Result:=Self.LarguraArredondamento;
end;

procedure TObjVIDRO_PP_PEDIDO.Submit_AlturaArredondamento(
  parametro: string);
begin
     Self.AlturaArredondamento:=parametro;
end;

procedure TObjVIDRO_PP_PEDIDO.Submit_LarguraArredondamento(
  parametro: string);
begin
     Self.LarguraArredondamento:=parametro;
end;

procedure TObjVIDRO_PP_PEDIDO.RetornaDadosRel2(PpedidoProjeto: string;
  STrDados: TStrings; ComValor: Boolean);
begin
  if (PpedidoProjeto='') then
    exit;

  with Self.ObjQueryPesquisa do
  begin
    Close;
    SQL.Clear;
    SQL.Text := ' select vp.complemento, v.referencia, c.referencia || ' + QuotedStr('-') +  ' || c.descricao as cor, ' +
                ' v.descricao, v.unidade, vp.valor, vp.altura, vp.largura, sum(vp.quantidade) as quantidade,  '+
                ' sum(vp.quantidade) * vp.valor as mult, count(vp.codigo) as qtdpecas '+
                ' from tabvidro_pp vp '+
                ' inner join tabvidrocor vc on (vp.vidrocor = vc.codigo) '+
                ' inner join tabcor c on (vc.cor = c.codigo) '+
                ' inner join tabvidro v on (vc.vidro = v.codigo) '+
                ' where vp.pedidoprojeto = :pedidoprojeto '+
                ' group by 1,2,3,4,5,6,7,8 ';

    ParamByName('pedidoprojeto').AsString := PpedidoProjeto;
    open;

    first;

    if (RecordCount = 0)then
      exit;

    while not(eof) do
    begin
      if(ComValor=True) then
      begin
        STrDados.Add(CompletaPalavra(fieldbyname('referencia').asstring,10,' ')+' '+
                    CompletaPalavra(fieldbyname('cor').asstring,15,' ')+' '+
                    CompletaPalavra(fieldbyname('descricao').asstring,44,' ')+' '+
                    CompletaPalavra(fieldbyname('quantidade').asstring+' '+fieldbyname('unidade').asstring,6,' ')+' '+
                    CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),10,' ')+' '+
                    CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('mult').asstring),10,' '));

      end
      else begin
        STrDados.Add(CompletaPalavra(fieldbyname('referencia').asstring,10,' ')+' '+
                    CompletaPalavra(fieldbyname('cor').asstring,16,' ')+' '+
                    CompletaPalavra(fieldbyname('descricao').asstring,45,' ')+' '+
                    CompletaPalavra(fieldbyname('quantidade').asstring+' '+fieldbyname('unidade').asstring,6,' '));

      end;
      if (FieldByName('largura').AsString <> '') and (FieldByName('altura').AsString <> '') then
        STrDados.Add(fieldbyname('qtdpecas').AsString+' PE�A(S)  '+'ALTURA: '+ FieldByName('altura').AsString + ' LARGURA: '+FieldByName('largura').AsString);
        //STrDados.Add();
      next;
    end;
    if(ComValor=True) then
    begin
      close;
      sql.clear;
      sql.add('Select sum(ValorFinal) as SOMA FROM TabVidro_PP where PedidoProjeto='+PpedidoProjeto);
      open;
      STrDados.Add('?'+CompletaPalavra_a_Esquerda('Total VIDROS: '+formata_valor(fieldbyname('soma').asstring),100,' '));
      STrDados.Add(' ');
    end;
  end;
end;

end.



