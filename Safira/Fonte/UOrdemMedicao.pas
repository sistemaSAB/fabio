unit UOrdemMedicao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,UobjORDEMMEDICAO,UessencialGlobal,DB,IBQuery,UCLIENTE,UVENDEDOR
  ,UFuncionarios,Upesquisa, Mask,UPEDIDO,UpesquisaMenu,UobjPedidoObjetos,UDataModulo;

type
  TFOrdemMedicao = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    ImageRodape: TImage;
    lbLbDescricao: TLabel;
    lbLbCliente: TLabel;
    lbLbVendedor: TLabel;
    edtVendedor: TEdit;
    edtCliente: TEdit;
    edtDescricao: TEdit;
    lbNomeCliente: TLabel;
    lbNomeVendedor: TLabel;
    mmoObservacao: TMemo;
    lb9: TLabel;
    lb1: TLabel;
    edtMedidor: TEdit;
    lbNomeMedidor: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    edtdataemissao: TMaskEdit;
    edtDataMedicao: TMaskEdit;
    lb4: TLabel;
    edtendereco: TEdit;
    lb5: TLabel;
    edtcidade: TEdit;
    edtuf: TEdit;
    lb6: TLabel;
    edtbairro: TEdit;
    lb7: TLabel;
    lb8: TLabel;
    edtnumero: TEdit;
    lbconcluido: TLabel;
    lb10: TLabel;
    lb11: TLabel;
    lbnumorcamento: TLabel;
    btAjuda: TSpeedButton;
    lb12: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    edtHorarioIni: TMaskEdit;
    edtDuracao: TMaskEdit;
    lbl1: TLabel;
    lbHorarioTermino: TLabel;
    btAjudaVideo: TSpeedButton;
    Label1: TLabel;
    edtComplemento: TEdit;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btsalvarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure edtClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtVendedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMedidorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClienteExit(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edtVendedorExit(Sender: TObject);
    procedure edtMedidorExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btrelatorioClick(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
    procedure lbconcluidoMouseLeave(Sender: TObject);
    procedure lbconcluidoMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbconcluidoClick(Sender: TObject);
    procedure lb10Click(Sender: TObject);
    procedure lbNumOrcamentoClick(Sender: TObject);
    procedure lbNomeClienteClick(Sender: TObject);
    procedure lbNomeVendedorClick(Sender: TObject);
    procedure lbNomeMedidorClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClienteDblClick(Sender: TObject);
    procedure edtVendedorDblClick(Sender: TObject);
    procedure edtMedidorDblClick(Sender: TObject);
    procedure btAjudaClick(Sender: TObject);
    procedure edtClienteKeyPress(Sender: TObject; var Key: Char);
    procedure lb12Click(Sender: TObject);
    procedure edtHorarioIniExit(Sender: TObject);
    procedure edtDuracaoExit(Sender: TObject);
    procedure btAjudaVideoClick(Sender: TObject);
    procedure edtdataemissaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure medica(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtHorarioIniKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    ObjPedidoObjetos:TObjPedidoObjetos;
    ObjORDEMMEDICAO:TObjORDEMMEDICAO;
    procedure limpalabels;
    Function  ControlesParaObjeto:Boolean;
    function  TabelaParaControles: Boolean;
    Function  ObjetoParaControles:Boolean;
    procedure AjustaHorarioFim;
    Function  VerificaHorarioDisponivel(var ordemmedicao:string):Boolean;
  public
    { Public declarations }
  end;

var
  FOrdemMedicao: TFOrdemMedicao;

implementation

uses UescolheImagemBotao, UobjCLIENTE, UAjuda, Uprincipal, UvideosAjuda;

{$R *.dfm}

procedure TFOrdemMedicao.FormShow(Sender: TObject);
begin
      ObjORDEMMEDICAO:=TObjORDEMMEDICAO.Create;
      FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
      FescolheImagemBotao.PegaFiguraImagem(ImageRodape,'RODAPE');
      desabilita_campos(Self);
      limpalabels;
      ObjPedidoObjetos:=TObjPedidoObjetos.Create(self);

end;

procedure TFOrdemMedicao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
       ObjORDEMMEDICAO.Free;
       ObjPedidoObjetos.Free;
       limpalabels;
       desabilita_campos(Self);
end;

procedure TFOrdemMedicao.btnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     limpalabels;
     habilita_campos(Self);
     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;
     ObjORDEMMEDICAO.Status:=dsinsert;
     lbCodigo.Caption:='0';
end;

procedure TFOrdemMedicao.limpalabels;
begin
     lbNomeCliente.Caption  :='';
     lbNomeVendedor.Caption :='';
     lbNomeMedidor.caption  :='';
     lbCodigo.Caption:='';
     lbNumOrcamento.Caption:='';
end;

procedure TFOrdemMedicao.btalterarClick(Sender: TObject);
begin
     if(lbCodigo.Caption='')
     THEN Exit;

     if(lbconcluido.Caption<>'Concluir')
     then exit;


     habilita_campos(Self);
     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;
     ObjORDEMMEDICAO.Status:=dsEdit;
end;



procedure TFOrdemMedicao.btcancelarClick(Sender: TObject);
begin
     limpaedit(Self);
     limpalabels;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true ;
     btopcoes.Visible :=true;
     desabilita_campos(Self);
     lbCodigo.Caption:='';
     ObjORDEMMEDICAO.Status:=dsInactive;

end;

procedure TFOrdemMedicao.btsairClick(Sender: TObject);
begin
     self.Close;
end;

procedure TFOrdemMedicao.btsalvarClick(Sender: TObject);
var
  OrdemMedicao:string;
begin
       If ObjORDEMMEDICAO.Status=dsInactive
       Then exit;

       if(edtDataMedicao.Text='  /  /    ')then
       begin
          MensagemAviso('preencha a data de medi��o');
          Exit;
       end;
       if(edtHorarioIni.Text='  :  ')then
       begin
          MensagemAviso('preencha o hor�rio de inicio');
          Exit;
       end;
       if(edtdataemissao.Text='  /  /    ')then
       begin

          MensagemAviso('preencha a data de emiss�o');
          Exit;
       end;
       if(edtDuracao.Text='  :  ')then
       begin
          MensagemAviso('preencha a dura��o aproximada da medi��o');
          Exit;
       end;
       if(Trim(edtCliente.Text) = '') then
       begin
         MensagemAviso('Selecione um cliente');
         Exit;
       end;
       if(Trim(edtVendedor.Text) = '') then
       begin
         MensagemAviso('Selecione um vendedor');
         Exit;
       end;
       if(Trim(edtMedidor.Text) = '') then
       begin
         MensagemAviso('Selecione um medidor');
         Exit;
       end;


       if(VerificaHorarioDisponivel(OrdemMedicao)=False)then
       begin
           MensagemAviso('O Funcion�rio '+lbNomeMedidor.Caption+' esta escalado para a Ordem de Medi��o N�: '+OrdemMedicao+' neste hor�rio!');
           Exit;
       end;

       If ControlesParaObjeto=False
       Then Begin
                 Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
                 exit;
            End;

       If (ObjORDEMMEDICAO.salvar(true)=False)
       Then exit;
       lbCodigo.Caption:=ObjORDEMMEDICAO.Get_CODIGO;

       ObjORDEMMEDICAO.LocalizaCodigo(lbCodigo.Caption) ;
       ObjORDEMMEDICAO.TabelaparaObjeto;
       Self.TabelaParaControles;

       btnovo.Visible :=true;
       btalterar.Visible:=true;
       btpesquisar.Visible:=true;
       btrelatorio.Visible:=true;
       btexcluir.Visible:=true;
       btsair.Visible:=true;
       btopcoes.Visible :=true;
       desabilita_campos(self);
end;

function TFOrdemMedicao.ControlesParaObjeto:Boolean;
begin
    try
        with ObjORDEMMEDICAO do
        begin
              Submit_CODIGO(lbCodigo.Caption);
              VENDEDOR.Submit_Codigo(edtVendedor.Text);
              MEDIDOR.Submit_CODIGO(edtMedidor.Text);
              CLIENTE.Submit_Codigo(edtCliente.Text);
              PEDIDO.Submit_Codigo(lbnumorcamento.Caption);
              Submit_OBSERVACAO(mmoObservacao.Text);
              Submit_DESCRICAO(edtDescricao.Text);
              if(edtdataemissao.Text<>'  /  /    ')
              then  Submit_Datapedidoemissao(edtdataemissao.Text)
              else  Submit_Datapedidoemissao('');
              if(edtdataMedicao.Text<>'  /  /    ')
              then  Submit_DataMedicao(edtdatamedicao.Text)
              else  Submit_DataMedicao('');


              Submit_Endereco(edtendereco.Text);
              Submit_numero(edtnumero.Text);
              Submit_UF(edtuf.Text);
              Submit_Bairro(edtbairro.Text);
              Submit_Cidade(edtcidade.Text);
              Submit_CONCLUIDO('N');

              Submit_HorarioMedicao(edtHorarioIni.Text);
              Submit_Duracao(edtDuracao.Text);

              if(lbHorarioTermino.Visible=true)
              then Submit_HorarioTermino(lbHorarioTermino.Caption);

              Submit_Complemento(edtComplemento.Text);

              result:=True;


        end;

    except
         Result:=False;
    end;

end;

function TFOrdemMedicao.TabelaParaControles: Boolean;
begin
     If (ObjORDEMMEDICAO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

function TFOrdemMedicao.ObjetoParaControles:Boolean;
begin
      try
           with ObjORDEMMEDICAO do
           begin
                lbCodigo.Caption:=Get_CODIGO;
                edtVendedor.Text:=VENDEDOR.Get_Codigo;
                edtMedidor.Text:=MEDIDOR.Get_CODIGO;
                edtCliente.Text:=CLIENTE.Get_Codigo;
                edtDescricao.Text:=Get_DESCRICAO;
                mmoObservacao.Text:=Get_OBSERVACAO;
                lbNomeCliente.Caption:=CLIENTE.Get_Nome;
                lbNomeVendedor.Caption:=VENDEDOR.Get_Nome;
                lbNomeMedidor.Caption:=MEDIDOR.Get_Nome;
                edtdatamedicao.Text:=Get_DataMedicao;
                edtdataemissao.Text:=Get_DataPedidoEmissao;
                edtendereco.Text:= Get_Endereco;
                edtbairro.Text:=Get_Bairro;
                edtnumero.Text:=Get_Numero;
                edtcidade.Text:=Get_Cidade;
                edtuf.Text:=Get_UF;
                lbNumOrcamento.Caption:=pedido.Get_Codigo;

                edtHorarioIni.Text:=Get_HorarioMedicao;
                edtDuracao.Text:=Get_Duracao;

                if(Get_HorarioTermino<>'') then
                begin
                      lbHorarioTermino.Visible:=True;
                      lbHorarioTermino.Caption:=Get_HorarioTermino;
                end
                else lbHorarioTermino.Visible:=False;

                if(Get_CONCLUIDO='N')
                then lbconcluido.Caption:='Concluir'
                else lbconcluido.Caption:='Retornar';

                edtComplemento.Text:=Get_Complemento;
           end;
           result:=True;
      except
          result:=False;
      end;

end;



procedure TFOrdemMedicao.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
   //Pesquisa:string;
begin
        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjORDEMMEDICAO.Get_pesquisa,ObjORDEMMEDICAO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjORDEMMEDICAO.status<>dsinactive
                                  then exit;

                                  If (ObjORDEMMEDICAO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjORDEMMEDICAO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            //Self.limpaLabels;
                                            exit;
                                       End;
                                  desabilita_campos(self);

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFOrdemMedicao.edtClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjORDEMMEDICAO.PEDIDO.EdtClienteKeyDown(Sender,Key,Shift,lbnomecliente);
     
end;

procedure TFOrdemMedicao.edtVendedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjORDEMMEDICAO.PEDIDO.EdtVendedorKeyDown(Sender,Key,Shift,lbNomeVendedor);
     
end;

procedure TFOrdemMedicao.edtMedidorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      ObjORDEMMEDICAO.MEDIDOR.EdtFuncionariosKeyDown(Sender,Key,Shift,edtMedidor,lbnomemedidor);
end;

procedure TFOrdemMedicao.edtClienteExit(Sender: TObject);
begin
      if(edtCliente.Text='')
      then Exit;

      if(ObjORDEMMEDICAO.CLIENTE.LocalizaCodigo(edtCliente.Text)=false) then
      begin
          MensagemErro('Cliente n�o encontrado');
          Exit;
      end;

      ObjORDEMMEDICAO.CLIENTE.TabelaparaObjeto;

      lbNomeCliente.Caption:=ObjORDEMMEDICAO.CLIENTE.Get_Nome;
      edtendereco.Text:=ObjORDEMMEDICAO.CLIENTE.Get_Endereco;
      edtbairro.Text:=ObjORDEMMEDICAO.CLIENTE.Get_Bairro;
      edtnumero.text:=ObjORDEMMEDICAO.CLIENTE.Get_Numero;
      edtcidade.Text:=ObjORDEMMEDICAO.CLIENTE.Get_Cidade;
      edtuf.Text:=ObjORDEMMEDICAO.CLIENTE.Get_Estado;
      edtComplemento.Text := ObjORDEMMEDICAO.CLIENTE.Get_Complemento;

end;

procedure TFOrdemMedicao.btexcluirClick(Sender: TObject);
begin
     If (ObjORDEMMEDICAO.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (ObjORDEMMEDICAO.LocalizaCodigo(lbCodigo.Caption)=False)Then
     Begin
           Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
           exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjORDEMMEDICAO.exclui(lbCodigo.Caption,True)=False)Then
     Begin
           Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
           exit;
     End;
     limpaedit(Self);
     limpalabels;
end;

procedure TFOrdemMedicao.edtVendedorExit(Sender: TObject);
begin
      if(lbNomeVendedor.Caption<>'') or (edtVendedor.Text='')
      then Exit;

      if(ObjORDEMMEDICAO.VENDEDOR.LocalizaCodigo(edtVendedor.Text)=false) then
      begin
          MensagemErro('Cliente n�o encontrado');
          Exit;
      end;

      ObjORDEMMEDICAO.VENDEDOR.TabelaparaObjeto;

      lbNomeVendedor.Caption:=ObjORDEMMEDICAO.VENDEDOR.Get_Nome;
end;

procedure TFOrdemMedicao.edtMedidorExit(Sender: TObject);
begin
      if(lbNomeMedidor.Caption<>'') or (edtMedidor.Text='')
      then Exit;

      if(ObjORDEMMEDICAO.MEDIDOR.LocalizaCodigo(edtMedidor.Text)=false) then
      begin
          MensagemErro('Cliente n�o encontrado');
          Exit;
      end;

      ObjORDEMMEDICAO.MEDIDOR.TabelaparaObjeto;

      lbNomeMedidor.Caption:=ObjORDEMMEDICAO.MEDIDOR.Get_Nome;
end;

procedure TFOrdemMedicao.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Perform(Wm_NextDlgCtl,0,0);
     if (Key=#27)
     Then Self.Close;
end;

procedure TFOrdemMedicao.btrelatorioClick(Sender: TObject);
begin
      ObjORDEMMEDICAO.Imprime(lbCodigo.Caption);
end;

procedure TFOrdemMedicao.btopcoesClick(Sender: TObject);
begin
     ObjORDEMMEDICAO.BotaoOpcoes(lbCodigo.Caption);
end;

procedure TFOrdemMedicao.lbconcluidoMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFOrdemMedicao.lbconcluidoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
       TEdit(sender).Font.Style:=[fsBold,fsUnderline];

end;

procedure TFOrdemMedicao.lbconcluidoClick(Sender: TObject);
begin
    if(lbCodigo.Caption='')
    then Exit;

    if(lbconcluido.Caption='Concluir') then
    begin
        if (messagedlg('Certeza que deseja Concluir essa ORDEM DE MEDI��O?',mtconfirmation,[mbyes,mbno],0)=mrno)
        then exit ;

        ObjORDEMMEDICAO.Status:=dsEdit;
        ObjORDEMMEDICAO.Submit_CONCLUIDO('S');
        if(ObjORDEMMEDICAO.Salvar(True)=false) then
        begin
            MensagemErro('Erro na ao tentar concluir essa Ordem de Medi��o');
            Exit;
        end;
        lbconcluido.Caption:='Estornar';
    end
    else
    begin

        if (messagedlg('Certeza que deseja Estornar essa ORDEM DE MEDI��O?',mtconfirmation,[mbyes,mbno],0)=mrno)
        then exit ;

        ObjORDEMMEDICAO.Status:=dsEdit;
        ObjORDEMMEDICAO.Submit_CONCLUIDO('N');
        ObjORDEMMEDICAO.PEDIDO.Submit_Codigo('');
        if(ObjORDEMMEDICAO.Salvar(False)=false) then
        begin
            MensagemErro('Erro na ao tentar estornar essa Ordem de Medi��o');
            Exit;
        end;

        if(lbnumorcamento.Caption<>'') then
        begin
              if (messagedlg('Existe um pedido gerado pra essa ORDEM DE MEDI��O deseja estorna-lo?',mtconfirmation,[mbyes,mbno],0)=mrno)
              then exit ;

              //Estornando o pedido
              If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.LocalizaCodigo(lbnumorcamento.Caption)=False)Then
              Begin
                      Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
                      exit;
              End;
              Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.TabelaparaObjeto;

              if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.Get_Concluido='S')then
              Begin
                      MensagemErro('Este Pedido j� foi Conclu�do. Se deseja realmente exclu�-lo vec� deve retornar a venda.');
                      exit;
              end;

              if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.VerificaPedidoEmRomaneio(lbnumorcamento.Caption)=true)then
              Begin
                      Exit;
              end;

              // Exclui relacionamentos
              if (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.ExcluiRelacionamentos(lbnumorcamento.Caption)=false)then
              Begin
                      MensagemErro('Erro ao tentar excluir os relacionamentos');
                      exit;
              end;

              If (Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.PEDIDO.exclui(lbnumorcamento.Caption,false)=False)Then
              Begin
                      Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
                      exit;
              End;

        end;

        lbconcluido.Caption:='Concluir';
        lbnumorcamento.Caption:='';
        FDataModulo.IBTransaction.CommitRetaining;

    end;
end;

procedure TFOrdemMedicao.lb10Click(Sender: TObject);
var
  Fpedido:TFPEDIDO;
  PEdido:string;
begin
    if(lbconcluido.Caption='Concluir') then
    begin
         MensagemAviso('� preciso concluir a ordem de medi��o antes de gerar um or�amento');
         Exit;
    end;
    if(lbnumorcamento.Caption<>'') then
    begin
         MensagemErro('J� existe um or�amento para esta ordem de medi��o');
         Exit;
    end;
    FPEDIDO:=TFPEDIDO.Create(nil);

    try
        FPEDIDO.GeraOrcamentoAutomatico('PEDIDO GERADO POR UMA ORDEM DE MEDI�AO','',edtCliente.Text,edtVendedor.Text,Sender,PEdido,mmoObservacao.Text);

        ObjORDEMMEDICAO.Status:=dsEdit;
        ObjORDEMMEDICAO.PEDIDO.Submit_Codigo(PEdido);
        ObjORDEMMEDICAO.Salvar(true);
        ObjORDEMMEDICAO.LocalizaCodigo(lbCodigo.Caption) ;
        ObjORDEMMEDICAO.TabelaparaObjeto;
        Self.TabelaParaControles;
        
        FPEDIDO.Tag:=StrToInt(PEdido);
        FPEDIDO.ShowModal;




    finally
        FreeAndNil(FPEDIDO);
    end;




end;

procedure TFOrdemMedicao.lbNumOrcamentoClick(Sender: TObject);
var
Fpedido:TFpedido;
begin

     Try
        Fpedido:=TFpedido.Create(nil);

     Except
           Messagedlg('Erro na tentativa de Criar o Cadastro de Pedido',mterror,[mbok],0);
           exit;
     End;

     try
          if(lbNumOrcamento.Caption='')
          then Exit;
          Fpedido.Tag:=StrToInt(lbNumOrcamento.Caption);
          Fpedido.ShowModal;
     finally
          FreeAndNil(Fpedido);
     end;
End;

procedure TFOrdemMedicao.lbNomeClienteClick(Sender: TObject);
var
  Fcliente:TFCLIENTE;
begin
   if(EdtCliente.Text='')
   then Exit;

   try
     fcliente:=TFCLIENTE.Create(nil);

   except

   end;

   try
      Fcliente.Tag:=StrToInt(EdtCliente.Text);
      Fcliente.ShowModal;

   finally
      FreeAndNil(Fcliente);
   end;


end;

procedure TFOrdemMedicao.lbNomeVendedorClick(Sender: TObject);
var
  Fvendedor:TFVENDEDOR;
begin
      if(EdtVendedor.Text='')
      then Exit;

      try
        Fvendedor:=TFVENDEDOR.Create(nil);
      except

      end;

      try
        Fvendedor.Tag:=StrToInt(EdtVendedor.Text) ;
        Fvendedor.ShowModal;
      finally
         FreeAndNil(Fvendedor);
      end;



end;


procedure TFOrdemMedicao.lbNomeMedidorClick(Sender: TObject);
var
  Ffuncionarios:TFfuncionarios;
begin
      if(edtMedidor.Text='')
      then Exit;

      try
        Ffuncionarios:=TFfuncionarios.Create(nil);
      except

      end;

      try
        Ffuncionarios.Tag:=StrToInt(edtMedidor.Text) ;
        Ffuncionarios.ShowModal;
      finally
         FreeAndNil(Ffuncionarios);
      end;



end;

procedure TFOrdemMedicao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
     if (Key = VK_F1) then
    begin

      try
        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('ORDEM DE MEDI��O');
         FAjuda.ShowModal;
    end;
end;

procedure TFOrdemMedicao.edtClienteDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
       key:=VK_F9;
       ObjORDEMMEDICAO.PEDIDO.EdtClienteKeyDown(Sender,Key,Shift,lbnomecliente);
end;

procedure TFOrdemMedicao.edtVendedorDblClick(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
     key:=VK_F9;
     ObjORDEMMEDICAO.PEDIDO.EdtVendedorKeyDown(Sender,Key,Shift,lbNomeVendedor);
end;

procedure TFOrdemMedicao.edtMedidorDblClick(Sender: TObject);
var
  Key:Word;
  shift:TShiftState;
begin
      Key:=VK_F9;
      ObjORDEMMEDICAO.MEDIDOR.EdtFuncionariosKeyDown(Sender,Key,Shift,edtMedidor,lbnomemedidor);
end;

procedure TFOrdemMedicao.btAjudaClick(Sender: TObject);
begin
     FAjuda.PassaAjuda('ORDEM DE MEDI��O');
     FAjuda.ShowModal;
end;

procedure TFOrdemMedicao.edtClienteKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFOrdemMedicao.lb12Click(Sender: TObject);
begin
       ObjORDEMMEDICAO.EmitirOrdemMedicao(lbCodigo.Caption);
end;

procedure TFOrdemMedicao.edtHorarioIniExit(Sender: TObject);
begin
    AjustaHorarioFim;
end;

procedure TFOrdemMedicao.AjustaHorarioFim;
var
  FIni,FFim : TTime;
  OrdemMedicao:string;
begin
  if(edtHorarioIni.Text='  :  ') or (edtDuracao.Text='  :  ')
  then Exit;
  lbHorarioTermino.Visible:=True;
  Fini:=StrToTime(edtHorarioIni.Text);
  FFim:=StrToTime(edtDuracao.Text);
  lbHorarioTermino.Caption:=TimeToStr(Fini+FFim);
  if(VerificaHorarioDisponivel(OrdemMedicao)=False)then
  begin
       MensagemAviso('O Funcion�rio '+lbNomeMedidor.Caption+' esta escalado para a Ordem de Medi��o N�: '+OrdemMedicao+' neste hor�rio!');
  end;
end;

procedure TFOrdemMedicao.edtDuracaoExit(Sender: TObject);
begin
   AjustaHorarioFim;
end;

function TFOrdemMedicao.VerificaHorarioDisponivel(var ordemmedicao:string):Boolean;
var
  Query:TIBQuery;
  TempoInicialLocal,TempoFinalLocal:string;
begin
      Query:= TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;

      result:=False;
      with Query do
      begin
          Close;
          SQL.Clear;
          SQL.Add('select * from tabordemmedicao where medidor='+edtMedidor.Text);
          SQL.Add('and datamedicao='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataMedicao.Text))+#39);
          SQL.Add('and codigo<>'+lbCodigo.Caption);
          Open;

          while not eof do
          begin
                TempoInicialLocal:=fieldbyname('horariomedicao').AsString;
                TempoFinalLocal:=fieldbyname('horarioTermino').AsString;

                if(StrToTime (edtHorarioIni.Text) < StrToTime(TempoInicialLocal)) then
                begin
                     if(StrToTime(lbHorarioTermino.Caption)> StrToTime(TempoInicialLocal)) then
                     begin
                           result:=False;
                           ordemmedicao:=fieldbyname('codigo').AsString;
                           Exit;
                     end
                     else
                     begin
                            Result:=True;
                            Exit;
                     end;

                end
                else
                begin
                     if(StrToTime(edtHorarioIni.Text)>= StrToTime(TempoInicialLocal)) and (StrToTime(edtHorarioIni.Text)<= StrToTime(TempoFinalLocal))then
                     begin
                            Result:=False;
                            ordemmedicao:=fieldbyname('codigo').AsString;
                            Exit;
                     end;

                end;
                Next;
          end;

          Result:=True;

      end;

end;


procedure TFOrdemMedicao.btAjudaVideoClick(Sender: TObject);
var
   FVideosAjuda:TFvideos ;
begin
    FvideosAjuda:= TFvideos.Create(nil);
    try
      FVideosAjuda.PegaLink('','ORDEM DE MEDI��O');
      FVideosAjuda.ShowModal;
    finally
      FreeAndNil(FVideosAjuda);
    end;

end;

procedure TFOrdemMedicao.edtdataemissaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = vk_space) then
    self.edtdataemissao.Text := formatdatetime('dd/mm/yyyy',now);
end;

procedure TFOrdemMedicao.medica(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = vk_space) then
    self.edtDataMedicao.Text := formatdatetime('dd/mm/yyyy',now);
end;

procedure TFOrdemMedicao.edtHorarioIniKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = vk_space) then
    self.edtHorarioIni.Text := formatdatetime('hh:mm:ss',now);
end;

end.
