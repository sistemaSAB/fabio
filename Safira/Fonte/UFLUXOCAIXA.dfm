object FFLUXOCAIXA: TFFLUXOCAIXA
  Left = 323
  Top = 231
  Width = 608
  Height = 513
  Caption = 'Cadastro de Fluxo de Caixa - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 385
    Width = 592
    Height = 90
    Align = alBottom
    Color = 3355443
    TabOrder = 1
    object Btnovo: TBitBtn
      Left = 3
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Novo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 115
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Alterar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 227
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Gravar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 339
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 3
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Pesquisar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 115
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Relat'#243'rios'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object btexcluir: TBitBtn
      Left = 227
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Excluir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 451
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Sair'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btsairClick
    end
    object btgerarplanilha: TBitBtn
      Left = 339
      Top = 46
      Width = 108
      Height = 38
      Caption = 'Gerar P&lanilha'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btgerarplanilhaClick
    end
    object btrastrear: TBitBtn
      Left = 451
      Top = 6
      Width = 108
      Height = 38
      Caption = 'Ras&trear'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 9
      OnClick = btrastrearClick
    end
  end
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 592
    Height = 385
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    OnChange = GuiaChange
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Principal'
      object DBGridlinhas: TDBGrid
        Left = 0
        Top = 169
        Width = 584
        Height = 188
        Align = alClient
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clNavy
        TitleFont.Height = -11
        TitleFont.Name = 'Verdana'
        TitleFont.Style = []
        OnDblClick = DBGridlinhasDblClick
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 584
        Height = 169
        Align = alTop
        TabOrder = 1
        object LbDetalhaCredorDevedor: TLabel
          Left = 13
          Top = 118
          Width = 161
          Height = 13
          Caption = 'Detalha Credor/Devedor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LbNome: TLabel
          Left = 13
          Top = 65
          Width = 37
          Height = 13
          Caption = 'Nome'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LbCODIGO: TLabel
          Left = 13
          Top = 12
          Width = 44
          Height = 13
          Caption = 'C'#243'digo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LbOrdem: TLabel
          Left = 141
          Top = 12
          Width = 43
          Height = 13
          Caption = 'Ordem'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object comboDetalhaCredorDevedor: TComboBox
          Left = 13
          Top = 136
          Width = 100
          Height = 21
          ItemHeight = 13
          TabOrder = 3
          Text = '  '
          Items.Strings = (
            'N'#227'o'
            'Sim')
        end
        object EdtNome: TEdit
          Left = 13
          Top = 82
          Width = 428
          Height = 19
          MaxLength = 100
          TabOrder = 2
        end
        object EdtCODIGO: TEdit
          Left = 13
          Top = 28
          Width = 100
          Height = 19
          MaxLength = 9
          TabOrder = 0
        end
        object EdtOrdem: TEdit
          Left = 141
          Top = 28
          Width = 100
          Height = 19
          MaxLength = 9
          TabOrder = 1
        end
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Contas Gerenciais'
      object DbGridContaGerencial: TDBGrid
        Left = 0
        Top = 73
        Width = 584
        Height = 284
        Align = alClient
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clNavy
        TitleFont.Height = -11
        TitleFont.Name = 'Verdana'
        TitleFont.Style = []
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 584
        Height = 73
        Align = alTop
        TabOrder = 1
        object lbnomecontagerencial: TLabel
          Left = 8
          Top = 48
          Width = 90
          Height = 13
          Caption = 'Conta gerencial'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label1: TLabel
          Left = 8
          Top = 8
          Width = 102
          Height = 13
          Caption = 'Conta gerencial'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object btexcluircontagerencial: TButton
          Left = 504
          Top = 32
          Width = 75
          Height = 25
          Caption = 'Excluir'
          TabOrder = 2
          OnClick = btexcluircontagerencialClick
        end
        object btadicionarcontagerencial: TButton
          Left = 504
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Adicionar'
          TabOrder = 1
          OnClick = btadicionarcontagerencialClick
        end
        object edtcontagerencial: TEdit
          Left = 8
          Top = 24
          Width = 121
          Height = 19
          TabOrder = 0
          Text = 'edtcontagerencial'
          OnExit = edtcontagerencialExit
          OnKeyDown = edtcontagerencialKeyDown
        end
      end
    end
  end
end
