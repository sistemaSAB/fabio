unit UobjSpedFiscal;

interface

uses UComponentesSped,StdCtrls,IBQuery,ACBrEFDBlocos,UobjCONTADOR,DB,Classes,ComCtrls,Forms,Controls;

type
    TobjSpedFiscal = class

    private
      memoErros:TMemo;

      COD_FIN     :string; {C�digo da finalidade do arquivo}
      DATAINI     :string; {Data inicial das informa��es contidas no arquivo}
      DATAFIN     :string; {Data final das informa��es contidas no arquivo}
      IND_PERFIL  :string; {Perfil de apresenta��o do arquivo fiscal}
      IND_ATIV    :string; {Indicador de tipo de atividade}
      pathDestino :string; {caminho onde sera salvo o arquivo texto}
      BufferNotas :string; {as vezes o arquivo a processar � muito grande, ai a cada "buffernotas" deve gravar no arquivo (nao fechando a grava��o) e limpando a memoria}
      linhasBuffer:string;
      geraRegistroH:Boolean;
      DATAINI_ESTOQUE :string;
      DATAFIN_ESTOQUE :string;
      strItem,strUnidade,strNatOp:TStringList;


      {campo 2 do registro E110 VL_TOT_DEBITOS}
      valor_total_debitos:Currency;

      {campo 6 do registro E110 VL_TOT_CREDITOS}
      VL_TOT_CREDITOS_E110:Currency;

      {campo do registro E116. Somatorio do campo VL_ICMS_RECOLHER do registro E110}
      VL_OR:Currency;

      {campo 2 do registro E116}
      COD_OBRIGACOES_ICMS:string;

      {campo 5 do registro E116}
      COD_RECEITAESTADUAL:string;

      objContador :TObjCONTADOR;

      dataSourceNFEntrada :TDataSource;
      dataSourceNFSaida   :TDataSource;
      queryTemp:TIBQuery;

      procedure limpaRegistros();

      function getCOD_FIN      () :TACBrCodFinalidade;
      function get_IND_PERFIL  () :TACBrPerfil;
      function get_IND_ATIV    () :TACBrAtividade;
      
      function get_tipoFrete   (tipo:string)     :TACBrTipoFrete;
      function get_situacao    (situacao:string) :TACBrSituacaoDocto;

      function get_posseItem(pIndicadorPropriedade:integer):TACBrPosseItem;



      function get_valorPis    (BCpis:Currency)   :Currency;
      function get_valorCofins (BCCofins:Currency):Currency;

      function get_VL_BC_ICMS (pReducao,baseCalculo:Currency):Currency;
      function get_VL_ICMS    (bc_icms,aliquota:Currency):Currency;

      function possuiDados(query:TIBQuery):Boolean;

      function registroCorrespondenteCST(parametro:string;var valorFinal,BC_ICMS,VL_ICMS,VALOR_RED_BC,VALORIPI:Currency;query:TIBQuery):Boolean;
      function registroCorrespondenteCSOSN(parametro:string;var valorFinal,BC_ICMS,VL_ICMS,VALOR_RED_BC:Currency;query:TIBQuery):Boolean;


      procedure addMessageERRO(pErro:string);
      procedure addStr(str:TStringList;valor:string);
      procedure gravaTXT();


    public

      ComponentesSped    :TFComponentesSped;
      
      queryNFEntrada      :TIBQuery;
      queryNFSaida        :Tibquery;
      objquery            :TIBQuery;

      status:TStatusBar;

      strERROS:TstringList;
      strAdvertencia:TStringList;
      msgERRO:string;

      procedure carregaErrosMemo();
      procedure carregaErros(str:TStringList);

      function localizaNFSaida(pNumero,pCodigoFiscal:string):TIBQuery;
      function localizaNFEntrada(pNumero,pCodigoFiscal:string):TIBQuery;

      function pegaModeloTreeView(tv:TTreeView):string;
      function pegaTipoNFTreeView(tv:TTreeView):string;
      function get_descricaoRegistro(pTipoRegistro,pPosicao,nometabela:string):string;
      function arquivoValido_ECF(txt:string):Boolean;
      function arquivoValido_amanda(txt:string):Boolean;

      procedure teste();
      procedure criarTreeView(treeView:TTreeView);
      procedure criaListViewProdutosSaida(pNota:string; list:TListView);
      procedure criaListViewEstoque (list: TListView);
      procedure criaListViewProdutosEntrada(pNota:string; list:TListView);

      procedure LoadMemo              (memoTXT:TMemo);
      procedure submit_IND_PERFIL     (ind_perfil:string);
      procedure submit_COD_FIN        (cod_fin:string);
      procedure submit_DATAINI        (dataini:string);
      procedure submit_DATAFIN        (datafin:string);
      procedure submit_pathDestino    (pathDestino:string);
      procedure submit_IND_ATIV       (ind_ativ:string);
      procedure submit_linhasBuffer   (linhasBuffer:string);
      procedure submit_buffernotas    (notasBuffer:string);
      procedure submit_cod_obrigacoes_icms(codigoOBR:string);
      procedure submit_COD_RECEITAESTADUAL(codigoReceita:string);
      procedure submit_geraRegistroH (parametro:Boolean);
      procedure submit_DATAINI_ESTOQUE (parametro:string);
      procedure submit_DATAFIN_ESTOQUE (parametro:string);
      procedure submit_memoErros(memo: TMemo);
      procedure ValidaDados_antesDaGeracao();
      procedure submit_dataSourceNFEntrada(ds:TDataSource);
      procedure submit_dataSourceNFSaida  (ds:TDataSource);

      procedure mostraNFnoGrid(parametro:Boolean);

      procedure abreQueryCliente                ();
      procedure abreQueryFornecedorSaida        ();
      procedure abreQueryFornecedorEntrada      ();
      procedure abreQueryUnidadeMedidaSaida     ();
      procedure abreQueryUnidadeMedidaEntrada   ();
      procedure abreQueryUnidadeMedidaEstoque   ();
      procedure abreQueryProdutosSaida          ();
      procedure abreQueryProdutosEntrada        ();
      procedure abreQueryNaturezaOperacaoSaida  ();
      procedure abreQueryNaturezaOperacaoEntrada();
      procedure abreQueryProdutos_inventario    ();
      procedure abreQueryNFentrada              ();
      procedure abreQueryNFSaida                ();
      procedure abreQueryNFModelos              ();
      procedure abreQueryInformacaoComplementar ();


      procedure abreQueryRegC190_saida_cst     (pCodigoNota:string);
      procedure abreQueryRegC190_saida_csosn   (pCodigoNota:string);
      procedure abreQueryRegC190_entrada_cst   (pCodigoNota:string);
      procedure abreQueryRegC190_entrada_csosn (pCodigoNota:string);
      procedure abreQueryRegE510_saida_CFOPCST ();


      procedure abreItensNFSaida    (pCodigoNota:string);
      procedure abreItensNFSaidaComProduto(pCodigoNota:string);
      procedure abreItensNFEntrada  (pCodigoEntrada:string);

      function iniciaGeracao   ():Boolean;
      function procuraModeloNF (pCodigoFiscal:Integer;query:TIBQuery):Boolean;

      function gravaBlocos ():boolean;

      function  montaBloco_0():Boolean;
      function  montaRegistro_0000():Boolean;
      function  montaRegistro_0001():Boolean;
      function montaRegistro_0005():Boolean;
      function montaRegistro_0100():Boolean;
      function montaRegistro_0150():Boolean;
      function montaRegistro_0190():Boolean;
      function montaRegistro_0200():boolean;
      function montaRegistro_0400():boolean;
      function montaRegistro_0450():Boolean;


      function montaBloco_C():Boolean;
      procedure montaRegistro_C001(teveDados:TACBrIndicadorMovimento = imComDados);
      function montaRegistro_C100_C170_C190_SaidaEntrada():Boolean;

      function montaBloco_D():Boolean;
      procedure montaRegistro_D001(teveDados:TACBrIndicadorMovimento = imComDados);

      function montaBloco_E():Boolean;
      procedure montaRegistro_E001();
      function montaRegistro_E100():Boolean;
      function montaRegistro_E110():Boolean;
      function montaRegistro_E116():Boolean;
      function montaRegistro_E500():Boolean;
      function montaRegistro_E510():Boolean;


      function montaBloco_G():Boolean;
      function montaRegistro_G001(teveDados:TACBrIndicadorMovimento = imComDados):Boolean;

      function montaBloco_H():Boolean;
      function montaRegistro_H001(teveDados:TACBrIndicadorMovimento = imComDados):boolean;
      function montaRegistro_H005_H010():boolean;

      function montaBloco_1():Boolean;
      function montaRegistro_1001(teveDados:TACBrIndicadorMovimento = imComDados):Boolean;

      function montaBloco_9():Boolean;
      function montaRegistro_9001():Boolean;
      function montaRegistro_9900():Boolean;
      function montaRegistro_9999():Boolean;



      {---------------------------VALIDA��ES-------------------------------}
      procedure validaDados (memo:TMemo);
      procedure validaBloco_0();
      procedure validaRegistro_0000();
      procedure validaRegistro_0005();
      procedure validaRegistro_0100();
      procedure validaRegistro_0150();
      procedure validaRegistro_0190();
      procedure validaRegistro_0200();
      procedure validaRegistro_0400();

      procedure validaBloco_C();
      procedure validaBloco_C100_C170();

      procedure validaBloco_E();
      procedure validaRegistro_E116();

      procedure validaBloco_H();
      procedure validaRegistro_H005_H010();

      {OUTRAS VALIDA��ES}

      {verifica se tem CFOP de entrada nas saidas}
      procedure validaCFOPSaida();
      procedure validaCFOPEntrada();

      {verifica se tem cnson e cst do icms ao mesmo tempo. Pode ter um ou otro apenas - SImples � csosn - n�o Simples � cst}
      procedure validaCSOSN_CST();

      constructor create;
      destructor free;

end;


implementation

uses SysUtils,UDataModulo,UEmpresa, UobjEMPRESA, ACBrEFDBloco_0,dialogs,
  ACBrEFDBloco_0_Class, UessencialGlobal, ACBrEFDBloco_C_Class,
  ACBrEFDBloco_C, ACBrEFDBloco_E, ACBrSpedFiscal, ACBrEFDBloco_H,
  UspedFiscal, Math;

{ TobjSpedFiscal }

procedure TobjSpedFiscal.abreQueryCliente;
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select distinct(C.codigo),C.nome,C.codigoPais,C.cpf_cgc,');
  self.objquery.SQL.Add('C.fisica_juridica,C.rg_ie,C.codigoCidade,C.endereco,');
  self.objquery.SQL.Add('C.numero,C.bairro');

  self.objquery.SQL.Add('from TABNOTAFISCAL NF');
  self.objquery.SQL.Add('join TABCLIENTE C on C.codigo = NF.cliente');
  self.objquery.SQL.Add('where (NF.dataemissao >= '+#39+dataInicial+#39+') and (NF.dataemissao <= '+#39+datafinal+#39+') and (NF.situacao = ''I'')');
 
  self.objquery.Active:=True;
  self.objquery.First;


end;

procedure TobjSpedFiscal.abreQueryFornecedorSaida;
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select distinct(f.codigo),f.razaosocial,');
  self.objquery.SQL.Add('f.codigopais,f.cgc,f.ie,f.codigocidade,');
  self.objquery.SQL.Add('f.endereco,f.bairro');

  self.objquery.SQL.Add('from tabnotafiscal nf');
  self.objquery.SQL.Add('join tabfornecedor f on f.codigo = nf.fornecedor');

  self.objquery.SQL.Add('where NF.dataemissao >= '+#39+dataInicial+#39+' and NF.dataemissao <= '+#39+datafinal+#39);


  self.objquery.Active:=True;
  self.objquery.First;

end;

procedure TobjSpedFiscal.abreQueryFornecedorEntrada;
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select distinct(f.codigo),f.razaosocial,');
  self.objquery.SQL.Add('f.codigopais,f.cgc,f.ie,f.codigocidade,');
  self.objquery.SQL.Add('f.endereco,f.bairro');

  self.objquery.SQL.Add('from tabentradaprodutos ep');
  self.objquery.SQL.Add('join tabfornecedor f on f.codigo = ep.fornecedor');

  self.objquery.SQL.Add('where EP.data >= '+#39+dataInicial+#39+' and EP.data <= '+#39+datafinal+#39);

  self.objquery.Active:=True;
  self.objquery.First;

end;

procedure TobjSpedFiscal.abreQueryProdutosSaida;
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.sql.add('select * from');
  self.objquery.SQL.Add('proc_materiais_nf_distinct('+#39+dataInicial+#39+','+#39+datafinal+#39+')');

  //InputBox('','',objquery.SQL.Text);

  self.objquery.Active:=True;
  self.objquery.First;

end;


procedure TobjSpedFiscal.abreQueryProdutosEntrada;
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select distinct(pm_ep.codigomaterialcor),pm_ep.nomematerial,pm_ep.nomecor,pm_ep.unidade,pm_ep.nomecor,pm_ep.codigomaterial,pm_ep.ncm,pm_ep.material');
  self.objquery.sql.add('from proc_materiais_ep pm_ep');
  self.objquery.sql.add('join tabentradaprodutos ep on ep.codigo=pm_ep.entrada');
  self.objquery.SQL.Add('where EP.data >= '+#39+dataInicial+#39+' and EP.data <= '+#39+datafinal+#39);

  self.objquery.Active:=True;
  self.objquery.First;

end;

procedure TobjSpedFiscal.abreQueryNaturezaOperacaoSaida;
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select distinct(cfop.codigo),cfop.nome');
  self.objquery.sql.add('from tabcfop cfop');
  self.objquery.sql.add('join proc_materiais_nf pm_nf on pm_nf.cfop=cfop.codigo');
  self.objquery.sql.add('join tabnotafiscal nf on nf.codigo=pm_nf.notafiscal');
  self.objquery.SQL.Add('where (NF.dataemissao >= '+#39+dataInicial+#39+') and (NF.dataemissao <= '+#39+datafinal+#39+') and (NF.modelo_nf <> 2)');

//  InputBox    ('','',objquery.SQL.Text);

  self.objquery.Active:=True;

  {este registro � necessario quando ha registros C170 (itens do documento)}
  {No registro C170 campo 12 COD_NAT deve existir no registro 0400 campo 2}
  {como notas fiscais eletronicas de saida nao gera C170 coloquei: and NF.modelonf <> 26}

end;

procedure TobjSpedFiscal.abreQueryNaturezaOperacaoEntrada;
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select distinct(CFOP.codigo), CFOP.nome');

  self.objquery.sql.add('from TABCFOP CFOP');
  self.objquery.sql.add('join proc_materiais_ep pm_ep on pm_ep.cfop=cfop.codigo');
  self.objquery.sql.add('join tabentradaprodutos ep on ep.codigo=pm_EP.entrada');
  self.objquery.SQL.Add('where EP.data >= '+#39+dataInicial+#39+' and EP.data <= '+#39+datafinal+#39);
  self.objquery.Active:=True;

end;

procedure TobjSpedFiscal.abreQueryNFSaida;
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.queryNFSaida.Active:=False;
  self.queryNFSaida.SQL.Clear;

  self.queryNFSaida.SQL.Add('select NF.codigo,NF.cliente,NF.fornecedor,modNF.codigo_fiscal,NF.situacao,');
  self.queryNFSaida.sql.add('NF.numero,Nfe.chave_acesso,NF.dataemissao,');
  self.queryNFSaida.sql.add('NF.datasaida, NF.valorfinal, NF.valortotal, nf.desconto,');
  self.queryNFSaida.sql.add('NF.freteporcontatransportadora,nf.valorseguro,NF.outrasdespesas,');
  self.queryNFSaida.sql.add('NF.valorfrete,NF.basecalculoicms,NF.valoricms,NF.basecalculoicms_substituicao,');
  self.queryNFSaida.sql.add('NF.valoricms_substituicao, NF.valortotalipi /*NF.nfcomplementar,*/');
  self.queryNFSaida.sql.add('from TABNOTAFISCAL NF');
  self.queryNFSaida.sql.add('join tabmodelonf modNF on modNF.codigo = NF.modelo_nf');
  self.queryNFSaida.sql.add('left join tabnfe nfe on nfe.codigo = NF.nfe');

  self.queryNFSaida.SQL.Add('where NF.dataemissao >= '+#39+dataInicial+#39+' and NF.dataemissao <= '+#39+datafinal+#39);

  self.queryNFSaida.sql.add('order by NF.dataemissao');

//  InputBox('','',queryNFSaida.SQL.Text);

  self.queryNFSaida.Active:=True;
  self.queryNFSaida.First;

end;

procedure TobjSpedFiscal.abreQueryNFModelos;
begin



end;

procedure TobjSpedFiscal.abreQueryInformacaoComplementar ();
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select nf.codigo,nf.dadosadicionais');
  self.objquery.sql.add('from tabnotafiscal nf');
  self.objquery.sql.add('where (nf.dadosadicionais <> '''') and ((nf.nfdevolucao = ''S'') or (nf.nfcomplementar = ''S''))');
  self.objquery.SQL.Add('where NF.dataemissao >= '+#39+dataInicial+#39+' and NF.dataemissao <= '+#39+datafinal+#39);

  self.queryNFSaida.Active:=True;
  self.queryNFSaida.First;

end;

procedure TobjSpedFiscal.abreQueryNFentrada;
var
  dataInicial,dataFinal:string;
begin

  Application.ProcessMessages;

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.queryNFEntrada.Active:=False;
  self.queryNFEntrada.SQL.Clear;

  self.queryNFEntrada.SQL.Add('select ep.codigo,ep.fornecedor,modelo.codigo_fiscal,ep.nf,ep.data,ep.valorfinal,ep.descontos,');
  self.queryNFEntrada.sql.add('ep.valorprodutos,ep.freteparafornecedor,ep.valorfrete,ep.valorseguro,ep.basecalculoicms,ep.valoricms,');
  self.queryNFEntrada.sql.add('ep.basecalculoicmssubstituicao,ep.icmssubstituto,ep.valoripi,ep.chavenfe,ep.emissaonf,ep.freteparafornecedor');
  self.queryNFEntrada.sql.add('from tabentradaprodutos EP');
  self.queryNFEntrada.sql.add('left join tabmodelonf MODELO on MODELO.codigo = EP.modelonf');
  self.queryNFEntrada.SQL.Add('where EP.data >= '+#39+dataInicial+#39+' and EP.data <= '+#39+datafinal+#39);

  self.queryNFEntrada.sql.add('order by EP.data');

  self.queryNFEntrada.Active:=True;
  self.queryNFEntrada.first;

  Application.ProcessMessages;


end;

procedure TobjSpedFiscal.abreItensNFSaida(pCodigoNota:string);
var
  dataInicial,dataFinal:string;
begin

  Application.ProcessMessages;

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select nf.codigo, pm_nf.codigomaterial,pm_nf.nomematerial,pm_nf.nomecor, pm_nf.quantidade, pm_nf.unidade,');
  self.objquery.sql.add('pm_nf.valor,pm_nf.desconto,pm_nf.csta,pm_nf.cstb,pm_nf.csosn,');
  self.objquery.sql.add('pm_nf.cfop,pm_nf.reducaobasecalculo,pm_nf.valorfinal,pm_nf.aliquota,pm_nf.ncm, pm_nf.codigomaterialcor,pm_nf.material');

  self.objquery.sql.add('from tabnotafiscal nf');
  self.objquery.sql.add('join proc_materiais_nf pm_nf on pm_nf.notafiscal=nf.codigo');
  self.objquery.sql.add('join tabmodelonf modelo on modelo.codigo=nf.modelo_nf');

  self.objquery.SQL.Add('where (pm_nf.notafiscal ='+pCodigoNota+') and (MODELO.codigo_fiscal <> 55) and (NF.situacao <> ''C'') and (NF.situacao <> ''Z'')');
 { self.objquery.SQL.Add('and (nf.nfcomplementar <> ''S'')'); }

  //InputBox('','',objquery.SQL.Text);

  self.objquery.Active:=True;
  self.objquery.First;

  Application.ProcessMessages;

end;


procedure TobjSpedFiscal.abreItensNFSaidaComProduto(pCodigoNota: string);
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Text :=
       'select pm_nf.material,pm_nf.notafiscal,pm_nf.material as codigomaterial,pm_nf.descricao as nomematerial, '+
       'pm_nf.quantidade, '+
       'pm_nf.valorfinal as valor,pm_nf.situacaotributaria_tabelaa as csta,pm_nf.situacaotributaria_tabelab as cstb, '+
       'pm_nf.cfop,pm_nf.reducaobasecalculo,pm_nf.valorfinal,pm_nf.aliquota,pm_nf.ncm,pm_nf.unidade,pm_nf.csosn, '+
       'pm_nf.codigocor,pm_nf.cor as nomecor '+
       'from viewprodutosvenda PM_NF '+
       'where pm_nf.notafiscal='+pCodigoNota;

  self.objquery.Active:=True;
  self.objquery.First;

end;

procedure TobjSpedFiscal.abreItensNFEntrada(pCodigoEntrada:string);
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select vme.codigomaterial,vme.nomematerial,vme.ncm,vme.unidade,vme.quantidade,vme.valor,');
  self.objquery.sql.add('vme.desconto,vme.csta,vme.cstb,vme.cfop,vme.reducaobasecalculo,vme.valorfinal,vme.creditoicms as aliquota,');
  self.objquery.sql.add('vme.csosn,vme.bc_icms,vme.valor_icms,vme.valorfrete,vme.valor_ipi,VME.codigomaterialcor,vme.nomecor,vme.material');
  self.objquery.sql.add('from proc_materiais_ep  VME');
  self.objquery.SQL.Add('where  vme.entrada='+pCodigoEntrada);

  self.objquery.Active:=True;
  self.objquery.First;

  {nesta tabela o valor total � com o desconto, ent�o criei um apelido para o valor final e o valor total}


end;


procedure TobjSpedFiscal.abreQueryUnidadeMedidaSaida;
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select distinct(pm_nf.unidade)');
  self.objquery.SQL.Add('from TABNOTAFISCAL NF');
  self.objquery.SQL.Add('join proc_materiais_nf pm_nf on pm_nf.notafiscal=nf.codigo');

  self.objquery.SQL.Add('where NF.dataemissao >= '+#39+dataInicial+#39+' and NF.dataemissao <= '+#39+datafinal+#39);
  self.objquery.Active:=True;
  self.objquery.First;

end;

procedure TobjSpedFiscal.abreQueryUnidadeMedidaEntrada;
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select distinct(vme.unidade)');
  self.objquery.SQL.Add('from TABENTRADAPRODUTOS EP');
  self.objquery.SQL.Add('join proc_materiais_ep vme on vme.entrada=ep.codigo');
  self.objquery.SQL.Add('where EP.data >= '+#39+dataInicial+#39+' and EP.data <= '+#39+datafinal+#39);
  
  self.objquery.Active:=True;
  self.objquery.First;

end;

procedure TobjSpedFiscal.abreQueryUnidadeMedidaEstoque ();
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select distinct(pm_nf.unidade)');
  self.objquery.SQL.Add('from TABNOTAFISCAL NF');
  self.objquery.SQL.Add('join proc_materiais_nf pm_nf on pm_nf.notafiscal=nf.codigo');
  self.objquery.SQL.Add('');
  self.objquery.SQL.Add('where NF.dataemissao >= '+#39+dataInicial+#39+' and NF.dataemissao <= '+#39+datafinal+#39);

  self.objquery.Active:=True;
  self.objquery.First;


end;

constructor TobjSpedFiscal.create;
begin

  self.objContador     :=TObjCONTADOR.Create();

  if not (self.objContador.LocalizaCodigo('1')) then
  begin

    MensagemErro('Contador de c�digo 1 n�o encontrado');
    Exit;

  end else
    objContador.TabelaparaObjeto;

  self.queryNFEntrada       :=TIBQuery.Create(nil);
  self.queryNFSaida         :=TIBQuery.Create(nil);
  self.objquery             :=TIBQuery.Create(nil);
  self.queryTemp            :=TIBQuery.Create(nil);

  strERROS:=TStringList.Create;
  strAdvertencia:=TStringList.Create;

  strItem:=TStringList.Create;
  strUnidade:=TStringList.Create;
  strNatOp:=TStringList.Create;

  self.queryNFEntrada.Database       := fdataModulo.IbdataBase;
  self.queryNFSaida.Database         := FDataModulo.IBDatabase;
  self.objquery.Database             := FDataModulo.IBDatabase;
  self.queryTemp.Database            := FDataModulo.IBDatabase;
end;

destructor TobjSpedFiscal.free;
begin

  FreeAndNil (self.queryNFEntrada);
  FreeAndNil (self.queryNFSaida);
  Freeandnil (self.objquery);
  FreeAndNil (self.strERROS);
  FreeAndNil (self.stradvertencia);
  FreeAndNil (self.queryTemp);
  FreeAndNil (self.stritem);
  FreeAndNil (self.strunidade);
  FreeAndNil (self.strNatOp);

  if (self.ComponentesSped <> nil) then
    FreeAndNil (self.ComponentesSped);
  
  objContador.Free;

end;


function TobjSpedFiscal.getCOD_FIN(): TACBrCodFinalidade;
begin

  if (Self.COD_FIN = '0') then
    result := raOriginal
  else
    result := raSubstituto;

end;

function TobjSpedFiscal.get_IND_ATIV: TACBrAtividade;
begin

    if (self.IND_ATIV = '1') then
      result := atOutros
    else
      Result := atIndustrial;

end;

function TobjSpedFiscal.get_IND_PERFIL(): TACBrPerfil;
begin

  if (self.IND_PERFIL = 'A') then
    result := pfPerfilA
  else if (self.IND_PERFIL = 'B') then
    result := pfPerfilB
  else
    result := pfPerfilC;

end;

function TobjSpedFiscal.gravaBlocos():Boolean;
begin
                                              
  result := False;
  msgERRO:='';

  self.ComponentesSped:=TFCOmponentesSped.Create(nil);

  try

    if not (self.iniciaGeracao) then Exit;

    if not (self.montaBloco_0)  then Exit;
    if not (self.montaBloco_C)  then Exit;
    if not (Self.montaBloco_D)  then Exit;
    if not (self.montaBloco_E)  then Exit;
    if not (self.montaBloco_G)  then Exit;
    if not (self.montaBloco_H)  then Exit;
    if not (Self.montaBloco_1)  then Exit;
    if not (Self.montaBloco_9)  then Exit;

    if not (montaRegistro_0190) then Exit;
    if not (montaRegistro_0200) then Exit;
    if not (montaRegistro_0400) then Exit;

    Self.gravaTXT();

  finally

    FreeAndNil(ComponentesSped);

  end;

  Application.ProcessMessages;
  result := True;
end;

function TobjSpedFiscal.iniciaGeracao:Boolean;
begin

  result:=False;

  try

    self.ComponentesSped.ACBrSPEDFiscal1.LinhasBuffer:=StrToIntDef(linhasBuffer,0);

    self.ComponentesSped.ACBrSPEDFiscal1.Arquivo :=ExtractFileName(self.pathDestino);
    self.ComponentesSped.ACBrSPEDFiscal1.Path    :=ExtractFileDir(self.pathDestino)+'\';
    self.limpaRegistros;

    self.ComponentesSped.ACBrSPEDFiscal1.DT_INI:=StrToDate(self.DATAINI);
    self.ComponentesSped.ACBrSPEDFiscal1.DT_FIN:=StrToDate(self.DATAFIN);


    {zera campos}
    valor_total_debitos:=0;
    VL_TOT_CREDITOS_E110:=0;
    self.VL_OR:=0;
    strUnidade.Clear;
    strItem.Clear;
    strNatOp.Clear;

    self.ComponentesSped.ACBrSPEDFiscal1.IniciaGeracao;

  except

    addMessageERRO('Erro ao iniciar gera��o');
    Exit;

  end;

   result:=True;

end;

function TobjSpedFiscal.procuraModeloNF(pCodigoFiscal: Integer;query: TIBQuery): Boolean;
begin

  //self.

end;

procedure TobjSpedFiscal.LoadMemo(memoTXT:TMemo);
var
  path:string;
begin

  memoTXT.Lines.Clear;

  path := ComponentesSped.ACBrSpedFiscal1.Path + ComponentesSped.ACBrSpedFiscal1.Arquivo;
   
  if (FileExists (path)) then
    memoTXT.Lines.LoadFromFile (path);

end;

{REGISTROS DO BLOCO 0}

function TobjSpedFiscal.montaBloco_0:Boolean;
begin

  result:=False;

  Self.status.Panels[1].Text:='Gerando bloco 0 registro 0000...';
  Application.ProcessMessages;
  if not (self.montaRegistro_0000) then Exit;

  Self.status.Panels[1].Text:='Gerando bloco 0 registro 0001...';
  Application.ProcessMessages;
  if not (self.montaRegistro_0001) then Exit;

  Self.status.Panels[1].Text:='Gerando bloco 0 registro 0005...';
  Application.ProcessMessages;
  if not (self.montaRegistro_0005) then Exit;

  Self.status.Panels[1].Text:='Gerando bloco 0 registro 0100...';
  Application.ProcessMessages;
  if not (self.montaRegistro_0100) then Exit;

  Self.status.Panels[1].Text:='Gerando bloco 0 registro 0150...';
  Application.ProcessMessages;
  if not (self.montaRegistro_0150) then Exit;

  {Self.status.Panels[1].Text:='Gerando bloco 0 registro 0190...';
  Application.ProcessMessages;
  if not (self.montaRegistro_0190) then Exit;} {esta sendo gravado no final}

  {self.status.Panels[1].Text:='Gerando bloco 0 registro 0200...';
  Application.ProcessMessages;
  if not (Self.montaRegistro_0200) then Exit;} {esta sendo gravado no final}

  {self.status.Panels[1].Text:='Gerando bloco 0 registro 0400...';
  Application.ProcessMessages;
  if not (self.montaRegistro_0400) then Exit;} {esta sendo gravado no final}

  {self.status.Panels[1].Text:='Gerando bloco 0 registro 0450...';
  Application.ProcessMessages;
  if not (self.montaRegistro_0450) then Exit;}

  {self.status.Panels[1].Text:='Gravando bloco 0 no arquivo...';
  Application.ProcessMessages;
  self.ComponentesSped.ACBrSPEDFiscal1.WriteBloco_0;}

  result:=True;

end;

function TobjSpedFiscal.montaRegistro_0000:Boolean;
begin

  result:=False;

  {REGISTRO 0000: ABERTURA DO ARQUIVO DIGITAL E IDENTIFICA��O DA ENTIDADE}

  try

    with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0000New) do
    begin

      COD_VER    := vlVersao104;
      COD_FIN    := self.getCOD_FIN();
      DT_INI     := StrToDate(self.DATAINI);
      DT_FIN     := StrToDate(self.DATAFIN);
      NOME       := Trim (ObjEmpresaGlobal.Get_RAZAOSOCIAL());
      CNPJ       := RetornaSoNumeros(ObjEmpresaGlobal.Get_CNPJ());
      UF         := ObjEmpresaGlobal.Get_ESTADO();
      IE         := RetornaSoNumeros(ObjEmpresaGlobal.Get_IE());
      COD_MUN    := StrToIntDef (ObjEmpresaGlobal.get_codigoCidade(),-1);
      IND_PERFIL := self.get_IND_PERFIL();
      IND_ATIV   := self.get_IND_ATIV  ();

    end;

  except

    on e:Exception do
    begin

      addMessageERRO('Erro ao montar registro 0000. '+e.Message);
      Exit;

    end;                                            {}

  end;

   Result:=True;

end;

function TobjSpedFiscal.montaRegistro_0001:Boolean;
begin

  result:=False;

  {REGISTRO 0001: ABERTURA DO BLOCO 0}
  {Este registro deve ser gerado para abertura do Bloco 0 e indica se h� informa��es previstas para este bloco}

  try

   self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0001New.IND_MOV := imComDados;

  except

    on e:exception do
    begin
      addMessageERRO('Erro ao montar registro 0001. '+e.Message);
      Exit;
    end;
                                                   {}
  end;
                          
  result:=True;

end;

function TobjSpedFiscal.montaRegistro_0005:Boolean;
begin

  result:=False;

  {REGISTRO 0005: DADOS COMPLEMENTARES DA ENTIDADE}
  {Registro obrigat�rio utilizado para complementar as informa��es de identifica��o do informante do arquivo.}

  try

    with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0005New) do
    begin

        FANTASIA := Trim (ObjEmpresaGlobal.Get_FANTASIA());
        CEP      := Trim (RetornaSoNumeros(ObjEmpresaGlobal.Get_CEP()));
        ENDERECO := Trim (ObjEmpresaGlobal.Get_ENDERECO());
        NUM      := Trim (ObjEmpresaGlobal.Get_Numero());
        COMPL    := Trim (ObjEmpresaGlobal.Get_COMPLEMENTO());
        BAIRRO   := Trim (ObjEmpresaGlobal.get_bairro());
        FONE     := Trim (RetornaSoNumeros(ObjEmpresaGlobal.Get_FONE()));
        FAX      := Trim (ObjEmpresaGlobal.Get_FAX());
        EMAIL    := Trim (ObjEmpresaGlobal.Get_EMAIL());

    end;                    {}

  except

    on e:Exception do
    begin

      addMessageERRO('Erro ao montar registro 0005. '+e.Message);
      Exit;

    end;

  end;

  result:=True;

end;

function TobjSpedFiscal.montaRegistro_0100:Boolean;
begin

  result:=False;

  {REGISTRO 0100: DADOS DO CONTABILISTA}
  {Registro utilizado para identifica��o do contabilista respons�vel pela escritura��o fiscal do estabelecimento,
   mesmo que o contabilista seja funcion�rio da empresa ou prestador de servi�o.}

   try

     with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0100New) do
     begin

        objContador.LocalizaCodigo('1');
        objContador.TabelaparaObjeto;

        NOME     := Trim (objContador.Get_NOME());
        CPF      := objContador.Get_CPF();
        CRC      := objContador.Get_CRC();
        CNPJ     := objContador.Get_CNPJ();
        CEP      := objContador.Get_CEP();
        ENDERECO := objContador.Get_ENDERECO();
        NUM      := objContador.Get_NUMERO();
        COMPL    := objContador.Get_COMPLEMENTO();
        BAIRRO   := objContador.Get_BAIRRO();
        FONE     := objContador.Get_FONE();
        FAX      := objContador.Get_FAX();
        EMAIL    := objContador.Get_EMAIL();
        COD_MUN  := StrToIntDef (objContador.CIDADE.get_codigoCidade(),-1);

     end;

   except

    on e:Exception do
    begin

      addMessageERRO('Erro ao montar registro 0100. '+e.Message);
      Exit;

    end;

   end;

   result:=True;

end;

function TobjSpedFiscal.montaRegistro_0150:Boolean;
var
  pCNPJ_CPF:string;
begin

  Result:=False;

  {REGISTRO 0150: TABELA DE CADASTRO DO PARTICIPANTE}


  try

    self.abreQueryCliente;
    while not (self.objquery.Eof) do
    begin

      with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0150New) do
      begin

        COD_PART := self.objquery.fieldbyname('codigo').AsString+'C';
        NOME     := Trim(self.objquery.fieldbyname('nome').AsString);
        COD_PAIS := Trim(self.objquery.fieldbyname('codigopais').AsString);

        if (UpperCase (self.objquery.fieldbyname('fisica_juridica').AsString) = 'F') then
          CPF  :=  Trim(RetornaSoNumeros(self.objquery.fieldbyname('cpf_cgc').AsString))
        else
          CNPJ :=  Trim(RetornaSoNumeros(self.objquery.fieldbyname('cpf_cgc').AsString));

        IE       := Trim(RetornaSoNumeros(self.objquery.fieldbyname('rg_ie').AsString));
        COD_MUN  := self.objquery.fieldbyname('codigocidade').AsInteger;
        SUFRAMA  := '';
        ENDERECO := Trim(self.objquery.fieldbyname('endereco').AsString);
        NUM      := Trim(self.objquery.fieldbyname('numero').AsString);
        COMPL    := '';
        BAIRRO   := Trim(self.objquery.fieldbyname('bairro').AsString);

      end;

      self.objquery.Next;

    end;

  except

    on e:Exception do
    begin

      addMessageERRO('Erro ao montar registro 0150 (Cliente). '+e.Message);
      Exit;

    end;

  end;


  try

    self.abreQueryFornecedorSaida();
    while not (self.objquery.Eof) do
    begin

      with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0150New) do
      begin

        COD_PART := self.objquery.fieldbyname('codigo').AsString+'F';
        NOME     := Trim (self.objquery.fieldbyname('razaosocial').AsString);
        COD_PAIS := Trim (objquery.fieldbyname('codigopais').AsString);

        pCNPJ_CPF:=RetornaSoNumeros(self.objquery.fieldbyname('cgc').AsString);

        if (Length(pCNPJ_CPF) <= 11) then
          CPF := pCNPJ_CPF
        else
          CNPJ := pCNPJ_CPF;


        if (RetornaSoNumeros(self.objquery.fieldbyname('ieprodutorrural').AsString) <> '') then
          IE := RetornaSoNumeros(self.objquery.fieldbyname('ieprodutorrural').AsString)
        else
          IE := RetornaSoNumeros(self.objquery.fieldbyname('ie').AsString);

       COD_MUN  := self.objquery.fieldbyname('codigoCidade').AsInteger;
       SUFRAMA  := '';
       ENDERECO := self.objquery.fieldbyname('endereco').AsString;

       {NUM      := self.abreQueryFornecedor.fieldbyname('numero').AsString; // colocar campo numero na tabfornecedor}

       COMPL    := '';
       BAIRRO   := self.objquery.fieldbyname('bairro').AsString;

      end;

      self.objquery.Next;

    end;

  except

    on e:Exception do
    begin

      addMessageERRO('Erro ao montar registro 0150 (Fornecedor saida). '+e.Message);
      Exit;

    end;

  end;


  try

    self.abreQueryFornecedorEntrada();
    while not (objquery.Eof) do
    begin

      {este registro tem a finalidade de apresentar todos os participantes das transa��es fiscais, EX: posso emitir uma devolu��o para o fornecedor de codigo}
      {10 - ACC. de Mello, ai posso tamb�m comprar do fornecedor 10 - ACC. de Mello. E este registro n�o pode repetir}
      {ent�o na instru��o acima eu preencho os clientes e fornecedores de saida, e na instru��o abaixo preencho os fornecedores das entradas}
      {ent�o localizo ele antes de preencher no componente, se o fornecedor ainda nao estiver presente no componente eu preencho}

      if not (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0001.Registro0150.LocalizaRegistro(objquery.fieldbyname('codigo').AsString+'F')) then
      begin

        with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0150New) do
        begin

          COD_PART := self.objquery.fieldbyname('codigo').AsString+'F';
          NOME     := Trim (self.objquery.fieldbyname('razaosocial').AsString);
          COD_PAIS := self.objquery.fieldbyname('codigopais').AsString;


          CNPJ := RetornaSoNumeros(self.objquery.fieldbyname('cgc').AsString); {ver quando � CNPJ e CPF}

          IE       := RetornaSoNumeros(self.objquery.fieldbyname('ie').AsString); {ver quando � IE normal e IE produtor rural }

          COD_MUN  := self.objquery.fieldbyname('codigoCidade').AsInteger;
          SUFRAMA  := '';
          ENDERECO := self.objquery.fieldbyname('endereco').AsString;

          {NUM      := self.abreQueryFornecedor.fieldbyname('numero').AsString; // colocar campo numero na tabfornecedor}

          COMPL    := '';
          BAIRRO   := self.objquery.fieldbyname('bairro').AsString;

        end;

      end;


      objquery.Next;

    end;

  except

    on e:Exception do
    begin

      addMessageERRO('Erro ao montar registro 0150 (Fornecedor entrada). '+e.Message);
      Exit;

    end;

  end;


  result:=True;

end;

function TobjSpedFiscal.montaRegistro_0190:Boolean;
var
    _i:Integer;
begin

  result:=False;

  {REGISTRO 0190: IDENTIFICA��O DAS UNIDADES DE MEDIDA}
  {Este registro tem por objetivo descrever as unidades de medidas utilizadas no arquivo digital. N�o podem ser
   informados dois ou mais registros com o mesmo c�digo de unidade de medida. Somente devem constar as unidades de
   medidas informadas nos demais blocos.}


   (*
   {UNIDADES UTILIZADAS NA SAIDA}
   try

     Self.abreQueryUnidadeMedidaSaida();
     while not (objquery.Eof) do
     begin

      with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0190New) do
      begin

        UNID  := objquery.fieldbyname('UNIDADE').asstring;
        DESCR := get_campoTabela('descricao','sigla','TABUNIDADEMEDIDA',UNID);

      end;

      self.objquery.Next;

     end;

   except

    on e:Exception do
    begin

      addMessageERRO('Erro ao montar registro 0190 (Unidades utilizadas na saida). '+e.Message);
      Exit;

    end;

   end; *)


   {UNIDADES UTILIZADAS NA ENTRADA}
  { try

     self.abreQueryUnidadeMedidaEntrada();
     while not (objquery.Eof) do
     begin

      if not (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0001.Registro0190.LocalizaRegistro(objquery.fieldbyname('UNIDADE').AsString)) then
      begin

        with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0190New) do
        begin

          UNID  := objquery.fieldbyname('UNIDADE').AsString;
          DESCR := get_campoTabela('descricao','sigla','TABUNIDADEMEDIDA',UNID);

        end;

      end;

      self.objquery.Next;

     end;

   except

     on e:Exception do
     begin

      addMessageERRO('Erro ao montar registro 0190 (Unidades utilizadas na entrada). '+e.Message);
      Exit;

     end;

   end;  }


   {UNIDADE UTILIZADAS NO ESTOQUE REGISTRO H}
   {try

     self.abreQueryUnidadeMedidaEstoque();
     while not (objquery.Eof) do
     begin

      if not (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0001.Registro0190.LocalizaRegistro(objquery.fieldbyname('UNIDADE').AsString)) then
      begin

        with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0190New) do
        begin

          UNID  := objquery.fieldbyname('UNIDADE').AsString;
          DESCR := get_campoTabela('descricao','sigla','TABUNIDADEMEDIDA',UNID);

        end;

      end;

      self.objquery.Next;

     end;

   except

     on e:Exception do
     begin

      addMessageERRO('Erro ao montar registro 0190 (Unidades utilizadas no estoque). '+e.Message);
      Exit;

     end;

   end; }

    try

          for _i:=0  to strUnidade.Count-1  do
          begin

            with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0190New) do
            begin

              UNID  := strUnidade[_i];
              DESCR := get_campoTabela('descricao','sigla','TABUNIDADEMEDIDA',strUnidade[_i]);

            end;

          end;


    except

         on e:Exception do
         begin

          addMessageERRO('Erro ao montar registro 0190 (Unidades de medida). '+e.Message);
          Exit;

         end;

    end;

    Result:=True;

end;

function TobjSpedFiscal.montaRegistro_0200():Boolean;
var
  PrefixoMat:string;
  CodigoM,aux,aux2:string;
    _i:Integer;
begin

  result:=False;

  {REGISTRO 0200: TABELA DE IDENTIFICA��O DO ITEM (PRODUTO E SERVI�OS)}
  {Este registro tem por objetivo informar mercadorias, servi�os, produtos ou quaisquer outros itens concernentes �s 
   transa��es fiscais. S� devem ser apresentados itens referenciados nos demais blocos}

 { try

    self.abreQueryProdutosSaida();
    while not (self.objquery.eof) do
    begin

      with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0200New) do
      begin

        COD_ITEM   := Self.objquery.fieldbyname('codigomask').AsString+'COD'+Self.objquery.fieldbyname('material_codigo').AsString+'COR'+Self.objquery.fieldbyname('material_cor').AsString;
        DESCR_ITEM := Trim(self.objquery.fieldbyname('nomematerial').AsString);
        //COD_BARRA  := self.objquery.fieldbyname('cod_barras').AsString;
        UNID_INV   := self.objquery.fieldbyname('unidade').asstring;
        TIPO_ITEM  := tiMercadoriaRevenda;
        COD_NCM    := RetornaSoNumeros(self.objquery.fieldbyname('ncm').asstring);

      end;

      self.objquery.Next;

    end;

  except

    on e:Exception do
    begin

      addMessageERRO('Erro ao montar registro 0200 (produtos saida). '+e.Message);
      Exit;

    end;

  end;


  try

    self.abreQueryProdutosEntrada();
    while not (self.objquery.Eof) do
    begin

      if not (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0001.Registro0200.LocalizaRegistro(Self.objquery.fieldbyname('codigomaterial').AsString)) then
      begin

        with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0200New) do
        begin

          if(Self.objquery.fieldbyname('material').AsString='FERRAGEM')
          then PrefixoMat:='FER ';
          if(Self.objquery.fieldbyname('material').AsString='PERFILADO')
          then PrefixoMat:='PER ';
          if(Self.objquery.fieldbyname('material').AsString='VIDRO')
          then PrefixoMat:='VID ';
          if(Self.objquery.fieldbyname('material').AsString='KITBOX')
          then PrefixoMat:='KIT ';
          if(Self.objquery.fieldbyname('material').AsString='DIVERSO')
          then PrefixoMat:='DIV ';
          if(Self.objquery.fieldbyname('material').AsString='PERSIANA')
          then PrefixoMat:='PES ';

          COD_ITEM   := PrefixoMat+'COD'+Self.objquery.fieldbyname('codigomaterial').AsString+'COR'+Self.objquery.fieldbyname('codigomaterialcor').AsString;
          DESCR_ITEM := Trim(self.objquery.fieldbyname('nomematerial').AsString);
          //COD_BARRA  := self.objquery.fieldbyname('cod_barras').AsString;
          UNID_INV   := self.objquery.fieldbyname('unidade').asstring;
          TIPO_ITEM  := tiMercadoriaRevenda;
          COD_NCM    := RetornaSoNumeros(self.objquery.fieldbyname('ncm').asstring);

        end;

      end;

      self.objquery.Next;

    end;

  except

    on e:exception do
    begin
      addMessageERRO('Erro ao montar registro 0200 (Produtos entrada). '+e.Message);
      Exit;
    end

  end;    }
  try


    for _i:=0  to strItem.Count-1  do
    begin

      with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0200New) do
      begin
        COD_ITEM   := strItem[_i];

        //Desemembrando o codigo composto por materiais
        PrefixoMat:=PegaPrimeiraPalavra(COD_ITEM);
        aux2:=retornaPalavrasDepoisSimbolo(COD_ITEM,' ');
        aux:=retornaPalavrasDepoisSimbolo2(aux2,'D');
        CodigoM:=retornaPalavrasAntesSimbolo(aux,'C');

        if(PrefixoMat='FER') then
        begin
            DESCR_ITEM := get_campoTabela ('DESCRICAO','codigo','TABFERRAGEM',CodigoM);
            UNID_INV   := get_campoTabela ('UNIDADE','codigo','TABFERRAGEM',CodigoM);
            TIPO_ITEM  := tiSubproduto;
            COD_NCM    := get_campoTabela ('NCM','codigo','TABFERRAGEM',CodigoM);
        end;
        if(PrefixoMat='PER') then
        begin
            DESCR_ITEM := get_campoTabela ('DESCRICAO','codigo','TABPERFILADO',CodigoM);
            UNID_INV   := get_campoTabela ('UNIDADE','codigo','TABPERFILADO',CodigoM);
            TIPO_ITEM  := tiSubproduto;
            COD_NCM    := get_campoTabela ('NCM','codigo','TABPERFILADO',CodigoM);
        end;
        if(PrefixoMat='PES') then
        begin
            DESCR_ITEM := get_campoTabela ('NOME','codigo','TABPERSIANA',CodigoM);
            UNID_INV   := 'M�';
            TIPO_ITEM  := tiSubproduto;
            COD_NCM    := get_campoTabela ('NCM','codigo','TABPERSIANA',CodigoM);
        end;
        if(PrefixoMat='VID') then
        begin
            DESCR_ITEM := get_campoTabela ('DESCRICAO','codigo','TABVIDRO',CodigoM);
            UNID_INV   := get_campoTabela ('UNIDADE','codigo','TABVIDRO',CodigoM);
            TIPO_ITEM  := tiSubproduto;
            COD_NCM    := get_campoTabela ('NCM','codigo','TABVIDRO',CodigoM);
        end;
        if(PrefixoMat='DIV') then
        begin
            DESCR_ITEM := get_campoTabela ('DESCRICAO','codigo','TABDIVERSO',CodigoM);
            UNID_INV   := get_campoTabela ('UNIDADE','codigo','TABDIVERSO',CodigoM);
            TIPO_ITEM  := tiSubproduto;
            COD_NCM    := get_campoTabela ('NCM','codigo','TABDIVERSO',CodigoM);
        end;
        if(PrefixoMat='KIT') then
        begin
            DESCR_ITEM := get_campoTabela ('DESCRICAO','codigo','TABKITBOX',CodigoM);
            UNID_INV   := get_campoTabela ('UNIDADE','codigo','TABKITBOX',CodigoM);
            TIPO_ITEM  := tiSubproduto;
            COD_NCM    := get_campoTabela ('NCM','codigo','TABKITBOX',CodigoM);
        end;

        {  }

      end;

    end;

  except

    on e:exception do
    begin
      addMessageERRO('Erro ao montar registro 0200 (Produtos entrada). '+e.Message);
      Exit;
    end

  end;


  result:=True;

end;

function TobjSpedFiscal.montaRegistro_0400():boolean;
VAR
  _i:Integer;
begin

  result:=False;

  {REGISTRO 0400: TABELA DE NATUREZA DA OPERA��O/PRESTA��O}
  {Este registro tem por objetivo codificar os textos das diferentes naturezas da opera��o/presta��o discriminadas nos
   documentos fiscais. Esta codifica��o e suas descri��es s�o livremente criadas e mantidas pelo contribuinte.
   * N�o podem ser informados dois ou mais registros com o mesmo conte�do no campo COD_NAT}

  { try

     Self.abreQueryNaturezaOperacaoSaida();
     while not (self.objquery.Eof) do
     begin

      with (Self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0400New) do
      begin

        COD_NAT   := self.objquery.fieldbyname('codigo').AsString;
        DESCR_NAT := Trim (self.objquery.fieldbyname('nome').AsString);

      end;

      self.objquery.Next;

     end;

   except

     on e:Exception do
     begin

      addMessageERRO('Erro ao montar registro 0400 de saida. '+e.Message);
      Exit;

     end;

   end;

   try

     Self.abreQueryNaturezaOperacaoEntrada();
     while not (self.objquery.Eof) do
     begin

      with (Self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0400New) do
      begin

        COD_NAT   := self.objquery.fieldbyname('codigo').AsString;
        DESCR_NAT := self.objquery.fieldbyname('nome').AsString;

      end;

      self.objquery.Next;

     end;

   except

     on e:Exception do
     begin

      addMessageERRO('Erro ao montar registro 0400 de entrada. '+e.Message);
      Exit;

     end;

   end;   }
   for _i:=0  to strNatOp.Count-1  do
   begin

     with (Self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0400New) do
     begin

      COD_NAT   := strNatOp[_i];
      DESCR_NAT := get_campoTabela ('nome','codigo','TABCFOP',COD_NAT);

     end;

   end;

   result:=True;


   result:=True;

end;

function Tobjspedfiscal.montaRegistro_0450:Boolean;
begin

   result:=False;

  {EGISTRO 0450: TABELA DE INFORMA��O COMPLEMENTAR DO DOCUMENTO FISCAL}


   try        {}

     Self.abreQueryInformacaoComplementar();
     while not (self.objquery.Eof) do
     begin

      with (Self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0450New) do
      begin

        COD_INF := self.objquery.fieldbyname('').AsString;
        TXT     := Trim (self.objquery.fieldbyname('').AsString);

      end;

      self.objquery.Next;

     end;

   except

     on e:Exception do
     begin

      addMessageERRO('Erro ao montar registro 0450 de informa��es complementares. '+e.Message);
      Exit;

     end;

   end;

   result:=True;


end;

{REGISTROS DO BLOCO C}


function TobjSpedFiscal.montaBloco_C:Boolean;
begin

  result:=False;

  self.status.Panels[1].Text:='Gerando bloco C registro C001...';
  Application.ProcessMessages;
  self.montaRegistro_C001;

  if not (self.montaRegistro_C100_C170_C190_SaidaEntrada) then
    Exit;

 { self.status.Panels[1].Text:='Gravando bloco C no arquivo...';
  Application.ProcessMessages;
  self.ComponentesSped.ACBrSPEDFiscal1.WriteBloco_C(true);    }

  result:=True;

end;

procedure TobjSpedFiscal.montaRegistro_C001(teveDados:TACBrIndicadorMovimento);
begin

  {REGISTRO C001: ABERTURA DO BLOCO C}
  {Este registro tem por objetivo identificar a abertura do bloco C, indicando se h� informa��es sobre documentos 
  fiscais.}

  self.ComponentesSped.ACBrSPEDFiscal1.Bloco_C.RegistroC001New.IND_MOV:=teveDados;

end;

function TobjSpedFiscal.montaRegistro_C100_C170_C190_SaidaEntrada():Boolean;
var
  STA,STB,CSOSN,pCFOP,ALIQUOTA:string;
  VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,VALORIPI:Currency;
  situacaoNF:TACBrSituacaoDocto;
  PrefixoMat:string;
begin

  result:=False;

  {REGISTRO C100: NOTA FISCAL (C�DIGO 01), NOTA FISCAL AVULSA (C�DIGO 1B), NOTA FISCAL DE PRODUTOR (C�DIGO 04) E NF-e (C�DIGO 55).}

  {Para cada registro C100, obrigatoriamente deve ser  apresentado, pelo menos, um registro C170 e um registro C190, observadas
  as exce��es abaixo relacionadas:

  Exce��o 1: Para documentos com c�digo de situa��o (campo COD_SIT) cancelado (c�digo �02�)
             Nota Fiscal Eletr�nica  (NF-e) denegada (c�digo �04�) ou  numera��o inutilizada (c�digo�05�)

  Exce��o 2: Notas Fiscais Eletr�nicas - NF-e de emiss�o pr�pria: regra geral, devem ser apresentados somente os registros 
            C100 e C190

  Exce��o 3: Notas Fiscais Complementares e Notas Fiscais Extempor�neas

  Para NF-es de entrada tem que constar o registro C170
  }

  {REGISTRO C100: NOTA FISCAL saida}

  self.status.Panels[1].Text:='Gerando bloco C registro C100 C170 C190 de saida...';
  Application.ProcessMessages;
  
  self.abreQueryNFSaida();
  while not (self.queryNFSaida.Eof) do
  begin

    self.status.Panels[1].Text:='Gerando bloco C registro C100 saida...';
    Application.ProcessMessages;

    {C100}
    try

      with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_C.RegistroC100New) do
      begin

          IND_OPER   := tpSaidaPrestacao;
          IND_EMIT   := edEmissaoPropria;

          COD_MOD := CompletaPalavra_a_Esquerda(queryNFSaida.FieldByName('codigo_fiscal').AsString,2,'0');

        {  if (queryNFSaida.FieldByName('nfcomplementar').AsString = 'S') then
            COD_SIT := sdFiscalCompl
          else     }
            COD_SIT  := Self.get_situacao   (queryNFSaida.FieldByName('situacao').asstring);

          {usado no registro C190}
          situacaoNF:=cod_sit;


          if (queryNFSaida.FieldByName('codigo_fiscal').AsString = '55') then
            SER := '1'
          else
            SER := '';

         

          {se o codigo da situa��o do documento estiver em uma das situa��es abaixo:
           preencher somente os campos REG, IND_OPER, IND_EMIT, COD_MOD, COD_SIT, SER e NUM_DOC.
          Demais campos dever�o ser apresentados com conte�do VAZIO �||�. N�o informar registros filhos (C170 e C190)}

          if (COD_SIT = sdCancelado) or (COD_SIT = sdDoctoNumInutilizada) or (COD_SIT = sdCanceladoExtemp) or (COD_SIT = sdDoctoDenegado)  then
            begin

              NUM_DOC  := queryNFSaida.fieldbyname('numero').AsString;
              CHV_NFE  := queryNFSaida.fieldbyname('chave_acesso').AsString;

              {o componente estava assumindo valores, e neste caso o campo deve conter vazio, sen�o o validador reclama}
              IND_PGTO:=tpNenhum;
              IND_FRT :=tfNenhum;

            end
          else
          begin

            if (queryNFSaida.FieldByName('cliente').AsString <> '') then
              COD_PART := queryNFSaida.FieldByName('cliente').AsString+'C'
            else
              COD_PART := queryNFSaida.FieldByName('fornecedor').AsString+'F';

            NUM_DOC       := queryNFSaida.fieldbyname('numero').AsString;
            CHV_NFE       := queryNFSaida.fieldbyname('chave_acesso').AsString;
            DT_DOC        := queryNFSaida.fieldbyname('dataemissao').AsDateTime;
            DT_E_S        := queryNFSaida.fieldbyname('datasaida').AsDateTime;
            VL_DOC        := queryNFSaida.fieldbyname('valorfinal').AsCurrency;
            IND_PGTO      := tpVista;
            VL_DESC       := queryNFSaida.fieldbyname('desconto').AsCurrency;
            VL_MERC       := queryNFSaida.fieldbyname('valortotal').AsCurrency;
            IND_FRT       := self.get_tipoFrete(queryNFSaida.fieldbyname('freteporcontatransportadora').AsString);
            VL_FRT        := self.queryNFSaida.fieldbyname('valorfrete').AsCurrency;
            VL_SEG        := self.queryNFSaida.fieldbyname('valorseguro').AsCurrency;
            VL_OUT_DA     := self.queryNFSaida.fieldbyname('outrasdespesas').AsCurrency;
            VL_BC_ICMS    := self.queryNFSaida.fieldbyname('basecalculoicms').AsCurrency;
            VL_ICMS       := self.queryNFSaida.fieldbyname('valoricms').AsCurrency;
            VL_BC_ICMS_ST := self.queryNFSaida.fieldbyname('basecalculoicms_substituicao').AsCurrency;
            VL_ICMS_ST    := self.queryNFSaida.fieldbyname('valoricms_substituicao').AsCurrency;
            VL_IPI        := self.queryNFSaida.fieldbyname('valortotalipi').AsCurrency;
            VL_PIS        := Self.get_valorPis(queryNFSaida.fieldbyname('valorfinal').AsCurrency);
            VL_COFINS     := self.get_valorCofins(queryNFSaida.fieldbyname('valorfinal').AsCurrency);
            VL_COFINS_ST  := StrToCurr('0,00');
            VL_PIS_ST     := StrToCurr('0,00');

          end;



      end;

    except

      on e:Exception do
      begin

        addMessageERRO('Erro ao montar registro C100 (saida),nota: '+queryNFSaida.fieldbyname('numero').AsString+' C�digo fiscal: '+queryNFSaida.FieldByName('codigo_fiscal').AsString+'. '+e.Message);
        Exit;

      end;

    end;

    {registro C170 . Para NF-e de emiss�o propria(saidas) n�o entra neste registro}
    {codigo - chave prim�ria da tabela tabnotafiscal}
    try

      abreItensNFSaida(self.queryNFSaida.fieldbyname('codigo').AsString);
      while not (self.objquery.Eof) do
      begin
          if(Self.objquery.fieldbyname('material').AsString='FERRAGEM')
          then PrefixoMat:='FER ';
          if(Self.objquery.fieldbyname('material').AsString='PERFILADO')
          then PrefixoMat:='PER ';
          if(Self.objquery.fieldbyname('material').AsString='VIDRO')
          then PrefixoMat:='VID ';
          if(Self.objquery.fieldbyname('material').AsString='KITBOX')
          then PrefixoMat:='KIT ';
          if(Self.objquery.fieldbyname('material').AsString='DIVERSO')
          then PrefixoMat:='DIV ';
          if(Self.objquery.fieldbyname('material').AsString='PERSIANA')
          then PrefixoMat:='PES ';


        self.status.Panels[1].Text:='Gerando bloco C registro C170 saida. Item: '+PrefixoMat+'COD'+self.objquery.fieldbyname('codigomaterial').AsString+'COR'+self.objquery.fieldbyname('codigomaterialcor').AsString;
        Application.ProcessMessages;

        with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_C.RegistroC170New) do
        begin

          NUM_ITEM      :=  IntToStr(Self.objquery.RecNo);
          COD_ITEM      :=  PrefixoMat+'COD'+self.objquery.fieldbyname('codigomaterial').AsString+'COR'+self.objquery.fieldbyname('codigomaterialcor').AsString;
          addStr (strItem,COD_ITEM);

          DESCR_COMPL   :=  Trim(self.objquery.fieldbyname('nomematerial').AsString);
          QTD           :=  self.objquery.fieldbyname('quantidade').AsCurrency;
          UNID          :=  self.objquery.fieldbyname('unidade').AsString;
          addStr (strUnidade,UNID);

          VL_ITEM       :=  self.objquery.fieldbyname('valor').AsCurrency;
          VL_DESC       :=  self.objquery.fieldbyname('desconto').AsCurrency;
          IND_MOV       :=  mfSim;
          CFOP          :=  self.objquery.fieldbyname('cfop').AsString;
          COD_NAT       :=  self.objquery.fieldbyname('cfop').AsString;
          addStr (strNatOp,COD_NAT);

          IND_APUR      :=  iaMensal;

          {ICMS}

          if (self.objquery.fieldbyname('cstb').asstring <> '') then
            CST_ICMS      :=  self.objquery.fieldbyname('csta').AsString + self.objquery.fieldbyname('cstb').AsString
          else
            CST_ICMS      := self.objquery.fieldbyname('csosn').AsString;

          ALIQ_ICMS     :=  self.objquery.fieldbyname('aliquota').AsCurrency;

          if (ALIQ_ICMS >0) then
          begin

            VL_BC_ICMS    :=  self.get_VL_BC_ICMS(self.objquery.fieldbyname('reducaobasecalculo').AsCurrency,self.objquery.fieldbyname('valorfinal').AsCurrency);
            VL_ICMS       :=  self.get_VL_ICMS(VL_BC_ICMS,ALIQ_ICMS);

          end else
          begin

            VL_BC_ICMS := 0;
            VL_ICMS    := 0;

          end;



          {ICMS ST}
          VL_BC_ICMS_ST :=  0;
          ALIQ_ST       :=  0;
          VL_ICMS_ST    :=  0;

          {IPI}
          CST_IPI       :=  '';
          COD_ENQ       :=  '';
          {VL_BC_IPI     :=  '';}
          {ALIQ_IPI      :=  '';}
          {VL_IPI        := '';}

          {PIS}
          CST_PIS       :=  '';
          VL_BC_PIS     :=  self.objquery.fieldbyname('valorfinal').AsCurrency;
          ALIQ_PIS_PERC :=  StrToCurrDef(ObjEmpresaGlobal.Get_Pis,0);
          {QUANT_BC_PIS  :=  '';}
          {ALIQ_PIS_R    :=  '';}
          VL_PIS        :=  StrToCurrDef (FormatCurr ('0.00',(VL_BC_PIS * ALIQ_PIS_PERC / 100)),0);

          {COFINS}
          CST_COFINS        :=  '';
          VL_BC_COFINS      :=  self.objquery.fieldbyname('valorfinal').AsCurrency;
          ALIQ_COFINS_PERC  :=  StrToCurrDef(ObjEmpresaGlobal.Get_Cofins,0);
          {QUANT_BC_COFINS  := '';}
          {ALIQ_COFINS_R    := '';}
          VL_COFINS         := StrToCurrDef (FormatCurr ('0.00',(VL_BC_COFINS * ALIQ_COFINS_PERC / 100)),0);

          COD_CTA   := '';

        end;

        self.objquery.Next;

      end;

    except

      on e:Exception do
      begin

        addMessageERRO('Erro ao montar registro C170 saida, nota: '+queryNFSaida.fieldbyname('numero').AsString+' C�digo fiscal: '+queryNFSaida.FieldByName('codigo_fiscal').AsString+' Item: '+PrefixoMat+'COD'+self.objquery.fieldbyname('codigomaterial').AsString+'COR'+self.objquery.fieldbyname('codigomaterialcor').AsString+'. '+e.Message);
        Exit;
        
      end;

    end;

    {REGISTRO C190: SAIDA>> AGRUPADO POR STA STB CFOP E ALIQUOTA}

    try

      {notas canceladas, inutilizadas e nas demais situa��es sitadas no if n�o entram neste registro}
      if (situacaoNF <> sdCancelado) and (situacaoNF <> sdDoctoNumInutilizada) and (situacaoNF <> sdCanceladoExtemp) and (situacaoNF <> sdDoctoDenegado)  then
      begin

        abreQueryRegC190_saida_cst(self.queryNFSaida.fieldbyname('codigo').AsString);
        while not (objquery.Eof) do
        begin

          self.status.Panels[1].Text:='Gerando Bloco C registro C190 (CST). Nota: '+self.queryNFSaida.fieldbyname('numero').AsString;
          Application.ProcessMessages;

          VALORFINAL   := StrToCurr (tira_ponto (formata_valor (objquery.fieldbyname('valorfinal').asString)));
          BC_ICMS      := objquery.fieldbyname('BC_ICMS').AsCurrency;
          pVL_ICMS     := BC_ICMS * objquery.fieldbyname('aliquota').AsCurrency / 100;
          VALORIPI     := objquery.fieldbyname('valor_ipi').AsCurrency;

          if (BC_ICMS > VALORFINAL) then
            strERROS.Add('Base de calculo do ICMS n�o pode ser maior que o valor final do produto (BC ICMS: '+currToStr(BC_ICMS)+' Valor produto: '+currToStr(VALORFINAL)+')'+' Saida c�digo: '+self.queryNFSaida.fieldbyname('codigo').AsString);

          VALOR_RED_BC := VALORFINAL - BC_ICMS;

          repeat

            STA      := objquery.fieldbyname('csta').AsString;
            STB      := objquery.fieldbyname('cstb').AsString;
            pCFOP    := objquery.fieldbyname('cfop').AsString;
            ALIQUOTA := objquery.fieldbyname('aliquota').AsString;

          until not (registroCorrespondenteCST(STA+STB+pCFOP+ALIQUOTA,VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,VALORIPI,objquery) and not objquery.Eof);


          if (self.queryNFSaida.fieldbyname('valoricms').AsCurrency <= 0) then
          begin
            BC_ICMS:=0;
            pVL_ICMS:=0;
            VALOR_RED_BC:=0;
          end;

          with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_C.RegistroC190New) do
          begin

            CST_ICMS      := CompletaPalavra((STA+STB),3,'0');
            CFOP          := pCFOP;
            ALIQ_ICMS     := StrToCurrDef(ALIQUOTA,0);
            VL_OPR        := VALORFINAL;


            if (ALIQ_ICMS <= 0) then
              VL_BC_ICMS := 0
            else
              VL_BC_ICMS    := BC_ICMS;


            VL_ICMS       := pVL_ICMS;
            VL_BC_ICMS_ST :=0;
            VL_ICMS_ST    :=0;
            VL_RED_BC     :=VALOR_RED_BC;
            VL_IPI        :=0;

          end;

          {campo do registro E110 � o somatorio de todos os campos VL_ICMS do registro C190 - inclui somente as opera��es proprias}
           valor_total_debitos:=valor_total_debitos + StrToCurrDef(FormatFloat('0.00',pVL_ICMS),0);

          {aqui nao preciza do next, a fun��o registroCorrespondenteCST se encarrega}

        end;

      end;

    except

      on e:Exception do
      begin

        addMessageERRO('Erro ao montar registro C190 saida (CST), Nota: '+self.queryNFSaida.fieldbyname('codigo').AsString+' Codigo fiscal: '+queryNFSaida.FieldByName('codigo_fiscal').AsString+'. '+e.Message);
        Exit;

      end;                                         

    end;

    {REGISTRO C190: SAIDA>> AGRUPADO POR CSOSN CFOP E ALIQUOTA}

    try

      {notas canceladas, inutilizadas e nas demais situa��es sitadas no if n�o entram neste registro}
      if (situacaoNF <> sdCancelado) and (situacaoNF <> sdDoctoNumInutilizada) and (situacaoNF <> sdCanceladoExtemp) and (situacaoNF <> sdDoctoDenegado)  then
      begin
        
        Self.abreQueryRegC190_saida_csosn(self.queryNFSaida.fieldbyname('codigo').AsString);
        while not (objquery.Eof) do
        begin

          self.status.Panels[1].Text:='Gerando Bloco C registro C190 (CSOSN). Nota: '+self.queryNFSaida.fieldbyname('numero').AsString;
          Application.ProcessMessages;

          VALORFINAL   := StrToCurr (tira_ponto (formata_valor (objquery.fieldbyname('valorfinal').asString)));
          BC_ICMS      := objquery.fieldbyname('BC_ICMS').AsCurrency;
          pVL_ICMS      := BC_ICMS * objquery.fieldbyname('aliquota').AsCurrency / 100;

          if (BC_ICMS > VALORFINAL) then
            strERROS.Add('Base de calculo do ICMS n�o pode ser maior que o valor final do produto (BC ICMS: '+currtostr(BC_ICMS)+' Valor produto: '+currtostr(VALORFINAL)+')'+' Saida c�digo: '+self.queryNFSaida.fieldbyname('codigo').AsString);

          VALOR_RED_BC := VALORFINAL - BC_ICMS;

          repeat

            CSOSN     := objquery.fieldbyname('csosn').AsString;
            pCFOP     := objquery.fieldbyname('cfop').AsString;
            ALIQUOTA  := objquery.fieldbyname('aliquota').AsString;

          until not (registroCorrespondenteCSOSN(CSOSN+pCFOP+ALIQUOTA,VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,objquery) and not objquery.Eof);


          if (self.queryNFSaida.fieldbyname('valoricms').AsCurrency <= 0) then
          begin
            BC_ICMS:=0;
            pVL_ICMS:=0;
            VALOR_RED_BC:=0;
          end;


          with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_C.RegistroC190New) do
          begin

            CST_ICMS      := CSOSN;
            CFOP          := pCFOP;
            ALIQ_ICMS     := StrToCurrDef(ALIQUOTA,0);
            VL_OPR        := VALORFINAL;

            if (ALIQ_ICMS <= 0) then
              VL_BC_ICMS := 0
            else
              VL_BC_ICMS    := BC_ICMS;

            VL_ICMS       := pVL_ICMS;
            VL_BC_ICMS_ST :=0;
            VL_ICMS_ST    :=0;
            VL_RED_BC     :=VALOR_RED_BC;
            VL_IPI        :=0;

          end;

           {campo do registro E110 � o somatorio de todos os campos VL_ICMS do registro C190 - inclui somente as opera��es proprias}
            valor_total_debitos:=valor_total_debitos + StrToCurrDef(FormatFloat('0.00',pVL_ICMS),0);

          {aqui n�o preciza de next a fun��o registroCorrespondenteCSOSN se encarrega}

        end;

      end;

    except

      on e:Exception do
      begin

        addMessageERRO('Erro ao montar registro C190 saida (CSOSN), nota: '+self.queryNFSaida.fieldbyname('codigo').AsString+' Codigo fiscal: '+queryNFSaida.FieldByName('codigo_fiscal').AsString+'. '+e.Message);
        Exit;

      end;

    end;

    self.queryNFSaida.Next;

  end;
  

  {-------------------ENTRADA-------------------------}

  self.status.Panels[1].Text:='Gerando bloco C registro C100 C170 C190 de entrada...';
  Application.ProcessMessages;

  self.abreQueryNFentrada();
  while not (queryNFEntrada.Eof) do
  begin

    {REGISTRO C100: NOTA FISCAL entrada}
    try

      self.status.Panels[1].Text:='Gerando Bloco C registro C100 entrada. Nota: '+self.queryNFEntrada.fieldbyname('nf').AsString;
      Application.ProcessMessages;

      with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_C.RegistroC100New) do
      begin

        IND_OPER := tpEntradaAquisicao;
        IND_EMIT := edTerceiros;

        COD_PART := queryNFEntrada.fieldbyname('fornecedor').AsString+'F';
        COD_MOD  := CompletaPalavra_a_Esquerda(queryNFEntrada.fieldbyname('codigo_fiscal').AsString,2,'0');
        COD_SIT  := sdRegular;
        NUM_DOC  := queryNFEntrada.fieldbyname('nf').AsString;
        CHV_NFE  := queryNFEntrada.fieldbyname('chavenfe').AsString;
        DT_DOC   := queryNFEntrada.fieldbyname('emissaonf').AsDateTime;
        DT_E_S   := queryNFEntrada.fieldbyname('data').AsDateTime;
        VL_DOC   := queryNFEntrada.fieldbyname('valorfinal').AsCurrency;
        IND_PGTO := tpVista;
        VL_DESC  := queryNFEntrada.fieldbyname('descontos').AsCurrency;
        VL_MERC  := queryNFEntrada.fieldbyname('valorprodutos').AsCurrency;

        if (self.queryNFEntrada.fieldbyname('freteparafornecedor').asstring = 'S') then
          IND_FRT := tfPorContaEmitente
        else
          IND_FRT := tfPorContaDestinatario;

        VL_FRT        := queryNFEntrada.fieldbyname('valorfrete').AsCurrency;
        VL_SEG        := queryNFEntrada.fieldbyname('valorseguro').AsCurrency;
        //VL_OUT_DA     := queryNFEntrada.fieldbyname('despesasadministrativas').AsCurrency;
        VL_BC_ICMS    := queryNFEntrada.fieldbyname('basecalculoicms').AsCurrency;
        VL_ICMS       := queryNFEntrada.fieldbyname('valoricms').AsCurrency;
        VL_BC_ICMS_ST := queryNFEntrada.fieldbyname('basecalculoicmssubstituicao').AsCurrency;
        VL_ICMS_ST    := queryNFEntrada.fieldbyname('icmssubstituto').AsCurrency;
        VL_IPI        := queryNFEntrada.fieldbyname('valoripi').AsCurrency;

        VL_PIS        := get_valorPis(queryNFEntrada.fieldbyname('valorfinal').AsCurrency);

        {VL_COFINS     := self.get_valorCofins(objquery.fieldbyname('valorfinal').AsCurrency);
        VL_COFINS_ST  := StrToCurr('0,00');
        VL_PIS_ST     := StrToCurr('0,00');}


      end;

    except

      on e:Exception do
      begin

        addMessageERRO('Erro ao montar registro C100 entrada, Nota: '+queryNFEntrada.fieldbyname('nf').AsString+' C�digo fiscal: '+queryNFEntrada.fieldbyname('codigo_fiscal').AsString+'. '+e.Message);
        Exit;

      end;

    end;

    try

      self.abreItensNFEntrada(self.queryNFEntrada.FieldByName('codigo').asstring);
      while not (self.objquery.Eof) do
      begin
        if(Self.objquery.fieldbyname('material').AsString='FERRAGEM')
        then PrefixoMat:='FER ';
        if(Self.objquery.fieldbyname('material').AsString='PERFILADO')
        then PrefixoMat:='PER ';
        if(Self.objquery.fieldbyname('material').AsString='VIDRO')
        then PrefixoMat:='VID ';
        if(Self.objquery.fieldbyname('material').AsString='KITBOX')
        then PrefixoMat:='KIT ';
        if(Self.objquery.fieldbyname('material').AsString='DIVERSO')
        then PrefixoMat:='DIV ';
        if(Self.objquery.fieldbyname('material').AsString='PERSIANA')
        then PrefixoMat:='PES ';

        self.status.Panels[1].Text:='Gerando Bloco C registro C170 entrada. Item: '+PrefixoMat+'COD'+Self.objquery.fieldbyname('codigomaterial').AsString+'COR'+Self.objquery.fieldbyname('codigomaterialcor').AsString;;
        Application.ProcessMessages;

        {registro C170 . Para NF-e de emiss�o de ter�eiros(entradas) entra neste registro}
        with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_C.RegistroC170New) do
        begin

          NUM_ITEM      :=  IntToStr(Self.objquery.RecNo);
          COD_ITEM      :=  PrefixoMat+'COD'+Self.objquery.fieldbyname('codigomaterial').AsString+'COR'+Self.objquery.fieldbyname('codigomaterialcor').AsString;
          addStr (strItem,COD_ITEM);

          DESCR_COMPL   :=  Trim(self.objquery.fieldbyname('nomematerial').AsString);
          QTD           :=  self.objquery.fieldbyname('quantidade').AsCurrency;
          UNID          :=  self.objquery.fieldbyname('unidade').AsString;
          addStr (strUnidade,UNID);

          VL_ITEM       :=  self.objquery.fieldbyname('valor').AsCurrency;
          VL_DESC       :=  self.objquery.fieldbyname('desconto').AsCurrency;
          IND_MOV       :=  mfSim;

          CFOP          :=  self.objquery.fieldbyname('cfop').AsString;
          COD_NAT       :=  self.objquery.fieldbyname('cfop').AsString;
          addStr (strNatOp,COD_NAT);

          VL_BC_ICMS_ST :=  0;
          ALIQ_ST       :=  0;
          VL_ICMS_ST    :=  0;
          IND_APUR      :=  iaMensal;

          {ICMS}

          if (self.objquery.FieldByName('cstb').AsString <> '') then
            CST_ICMS      :=  self.objquery.fieldbyname('csta').AsString + self.objquery.fieldbyname('cstb').AsString
          else
            CST_ICMS      :=  self.objquery.fieldbyname('CSOSN').AsString;

          {VL_BC_ICMS    :=  self.get_VL_BC_ICMS(self.objquery.fieldbyname('porcentagemreducao').AsCurrency,self.objquery.fieldbyname('valorfinal').AsCurrency);
          ALIQ_ICMS     :=  self.objquery.fieldbyname('aliquota').AsCurrency;
          VL_ICMS       :=  self.get_VL_ICMS(VL_BC_ICMS,ALIQ_ICMS);}

          VL_BC_ICMS := self.objquery.fieldbyname('bc_icms').AsCurrency;
          ALIQ_ICMS  := self.objquery.fieldbyname('aliquota').AsCurrency;
          VL_ICMS    := self.objquery.fieldbyname('valor_icms').AsCurrency;

          {IPI}
          CST_IPI       :=  '';
          COD_ENQ       :=  '';
          {VL_BC_IPI     :=  '';}
          {ALIQ_IPI      :=  '';}
          {VL_IPI        := '';}

          {PIS}
          CST_PIS       :=  '';
          VL_BC_PIS     :=  self.objquery.fieldbyname('valorfinal').AsCurrency;
          ALIQ_PIS_PERC :=  StrToCurrDef(ObjEmpresaGlobal.Get_Pis,0);
          {QUANT_BC_PIS  :=  '';}
          {ALIQ_PIS_R    :=  '';}
          VL_PIS        :=  StrToCurrDef (FormatCurr ('0.00',(VL_BC_PIS * ALIQ_PIS_PERC / 100)),0);

          {COFINS}
          CST_COFINS        :=  '';
          VL_BC_COFINS      :=  self.objquery.fieldbyname('valorfinal').AsCurrency;
          ALIQ_COFINS_PERC  :=  StrToCurrDef(ObjEmpresaGlobal.Get_Cofins,0);
          {QUANT_BC_COFINS  := '';}
          {ALIQ_COFINS_R    := '';}
          VL_COFINS         := StrToCurrDef (FormatCurr ('0.00',(VL_BC_COFINS * ALIQ_COFINS_PERC / 100)),0);

          COD_CTA   := '';

        end;

        self.objquery.Next;

      end;

    except

      on e:Exception do
      begin

        addMessageERRO('Erro ao montar registro C170 entrada, Entrada: '+self.queryNFEntrada.FieldByName('codigo').asstring+' Item: '+PrefixoMat+'COD'+Self.objquery.fieldbyname('codigomaterial').AsString+'COR'+Self.objquery.fieldbyname('codigomaterialcor').AsString+'. '+e.Message);
        Exit;
        
      end

    end;

    {REGISTRO C190: ENTRADA>> AGRUPADO POR STA STB CFOP E ALIQUOTA}

    try

      self.abreQueryRegC190_entrada_cst(self.queryNFEntrada.fieldbyname('codigo').AsString);

      if (self.queryNFEntrada.fieldbyname('codigo').AsString = '1613') then
        MensagemAviso('paro');

      while not(self.objquery.Eof) do
      begin

        self.status.Panels[1].Text:='Gerando Bloco C registro C190 entrada (CST). Nota: '+self.queryNFEntrada.fieldbyname('nf').AsString;
        Application.ProcessMessages;

        VALORFINAL   := StrToCurr (tira_ponto (formata_valor (objquery.fieldbyname('valorfinal').asString)));
        BC_ICMS      := objquery.fieldbyname('BC_ICMS').AsCurrency;
        pVL_ICMS     := BC_ICMS * objquery.fieldbyname('aliquota').AsCurrency / 100;
        VALORIPI     := objquery.fieldbyname('VALOR_IPI').AsCurrency;

        if (BC_ICMS > 0) then
          begin

            if (trunca (VALORFINAL) < trunca (BC_ICMS)) then
              VALOR_RED_BC:=(VALORFINAL + (BC_ICMS - VALORFINAL)) - BC_ICMS
            else
              VALOR_RED_BC := VALORFINAL - BC_ICMS;
          end
        else
          VALOR_RED_BC:=0;

        repeat

          STA       := objquery.fieldbyname('csta').AsString;
          STB       := objquery.fieldbyname('cstb').AsString;
          pCFOP     := objquery.fieldbyname('cfop').AsString;
          ALIQUOTA  := objquery.fieldbyname('aliquota').AsString;

        until not (registroCorrespondenteCST(STA+STB+pCFOP+ALIQUOTA,VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,VALORIPI,objquery) and not objquery.Eof);

         //if (BC_ICMS > VALORFINAL) then
          //strERROS.Add('Base de calculo do ICMS n�o pode ser maior que o valor final do produto (BC ICMS: '+currtostr(BC_ICMS)+' Valor produto: '+currtostr(VALORFINAL)+')'+' Entrada c�digo: '+self.queryNFEntrada.fieldbyname('codigo').AsString);

        with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_C.RegistroC190New) do
        begin

          CST_ICMS      := CompletaPalavra((STA+STB),3,'0'); {fa�o isso porq o campo tem que ser tamanho 3 ex: cst a = 0 e cst b = 0, neste caso sao apenas 2 00 e tem que ser 000}
          CFOP          := pCFOP;
          ALIQ_ICMS     := StrToCurrDef(ALIQUOTA,0);
          VL_OPR        := VALORFINAL;

          if (ALIQ_ICMS <= 0) then
            VL_BC_ICMS := 0
          else
            VL_BC_ICMS    := BC_ICMS;

          VL_ICMS       := pVL_ICMS;
          VL_BC_ICMS_ST :=0;
          VL_ICMS_ST    :=0;
          VL_RED_BC     :=VALOR_RED_BC;
          VL_IPI        :=VALORIPI;

          {o valor deste campo deve corresponder ao somat�rio de todos os documentos fiscais de entrada que geram cr�dito de ICMS}
          {O valor neste campo deve ser igual � soma dos VL_ICMS de todos os registros C190}
          {este somat�rio, est�o exclu�dos os documentos fiscais com CFOP 1605 e inclu�dos os documentos fiscais com CFOP 5605}
          if (pCFOP <> '1605') then
            VL_TOT_CREDITOS_E110:=VL_TOT_CREDITOS_E110+VL_ICMS;

        end;

        {aqui n�o preciza de next a fun��o registroCorrespondenteCST se encarrega}

      end;

    except

      on e:Exception do
      begin

        addMessageERRO('Erro ao montar registro C190 entrada (CST), Entrada: '+self.queryNFEntrada.fieldbyname('codigo').AsString+'. '+e.Message);
        Exit;

      end;

    end;

    {REGISTRO C190: ENTRADA AGRUPADO POR CSOSN CFOP E ALIQUOTA}

    try

      Self.abreQueryRegC190_entrada_csosn(self.queryNFEntrada.fieldbyname('codigo').AsString);

      while not (self.objquery.Eof) do
      begin

        self.status.Panels[1].Text:='Gerando Bloco C registro C190 entrada (CSOSN). Nota: '+self.queryNFEntrada.fieldbyname('nf').AsString;
        Application.ProcessMessages;

        VALORFINAL   := StrToCurr (tira_ponto (formata_valor (objquery.fieldbyname('valorfinal').asString)));
        BC_ICMS      := objquery.fieldbyname('BC_ICMS').AsCurrency;
        pVL_ICMS      := BC_ICMS * objquery.fieldbyname('aliquota').AsCurrency / 100;

        VALOR_RED_BC := VALORFINAL - BC_ICMS;

        repeat

          CSOSN     := objquery.fieldbyname('csosn').AsString;
          pCFOP     := objquery.fieldbyname('cfop').AsString;
          ALIQUOTA  := objquery.fieldbyname('aliquota').AsString;

        until not (registroCorrespondenteCSOSN(CSOSN+pCFOP+ALIQUOTA,VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,objquery) and not objquery.Eof);

        if (BC_ICMS > VALORFINAL) then
          strERROS.Add('Base de calculo do ICMS n�o pode ser maior que o valor final do produto (BC ICMS: '+currtostr(BC_ICMS)+' Valor produto: '+currtostr(VALORFINAL)+')'+' Entrada c�digo: '+self.queryNFEntrada.fieldbyname('codigo').AsString);

        with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_C.RegistroC190New) do
        begin

          CST_ICMS      := CSOSN;
          CFOP          := pCFOP;
          ALIQ_ICMS     := StrToCurrDef(ALIQUOTA,0);
          VL_OPR        := VALORFINAL;

          if (ALIQ_ICMS <= 0) then
            VL_BC_ICMS := 0
          else
            VL_BC_ICMS    := BC_ICMS;

          VL_ICMS       := pVL_ICMS;
          VL_BC_ICMS_ST :=0;
          VL_ICMS_ST    :=0;
          VL_RED_BC     :=VALOR_RED_BC;
          VL_IPI        :=0;

           {o valor deste campo deve corresponder ao somat�rio de todos os documentos fiscais de entrada que geram cr�dito de ICMS}
          {O valor neste campo deve ser igual � soma dos VL_ICMS de todos os registros C190}
          {este somat�rio, est�o exclu�dos os documentos fiscais com CFOP 1605 e inclu�dos os documentos fiscais com CFOP 5605}
          if (pCFOP <> '1605') then
            VL_TOT_CREDITOS_E110:=VL_TOT_CREDITOS_E110+VL_ICMS;

        end;

        {aqui n�o preciza do next a fun��o registroCorrespondenteCSOSN se encarrega}

      end;

    except

      on e:Exception do
      begin

        addMessageERRO('Erro ao montar registro C190 entrada (CSOSN), Entrada: '+self.queryNFEntrada.fieldbyname('codigo').AsString+'. '+e.Message);
        Exit;

      end;

    end;

    self.queryNFEntrada.Next;

  end;

  if (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_C.RegistroC001.RegistroC100.Count <= 0) then
    montaRegistro_C001(imSemDados);

  strItem.SaveToFile('text.txt');
  result:=True;


end;

{REGISTROS DO BLOCO D}

function TobjSpedFiscal.montaBloco_D:Boolean;
begin

  result:=False;

  self.status.Panels[1].Text:='Gerando bloco D registro D001...';
  Application.ProcessMessages;
  self.montaRegistro_D001(imSemDados);

 { self.status.Panels[1].Text:='Gravando bloco D no arquivo...';
  Application.ProcessMessages;
  self.ComponentesSped.ACBrSPEDFiscal1.WriteBloco_D;   }

  result:=True;

end;

procedure TobjSpedFiscal.montaRegistro_D001(teveDados:TACBrIndicadorMovimento = imComDados);
begin

  {Este registro deve ser gerado para abertura do Bloco D e indica se h� informa��es sobre presta��es ou 
contrata��es de servi�os de comunica��o, transporte interestadual e intermunicipal, com o devido suporte do 
correspondente documento fiscal.}

  self.ComponentesSped.ACBrSPEDFiscal1.Bloco_D.RegistroD001New.IND_MOV:=teveDados;

end;

{REGISTROS DO BLOCO E}

function TobjSpedFiscal.montaBloco_E:Boolean;
begin

  result:=False;

  self.status.Panels[1].Text:='Gerando bloco E registro E001...';
  Application.ProcessMessages;
  self.montaRegistro_E001();

  self.status.Panels[1].Text:='Gerando bloco E registro E100...';
  Application.ProcessMessages;
  if not (self.montaRegistro_E100()) then Exit;

  self.status.Panels[1].Text:='Gerando bloco E registro E110...';
  Application.ProcessMessages;
  if not (self.montaRegistro_E110()) then Exit;

  self.status.Panels[1].Text:='Gerando bloco E registro E116';
  Application.ProcessMessages;
  if not (self.montaRegistro_E116) then Exit;

  if (self.IND_ATIV = '0') and (ObjEMPRESA.CRT.Get_Codigo = '3') then {0 � Industrial ou equiparado a industrial}  {e estiver no regime normal}
  begin

    {
    self.status.Panels[1].Text:='Gerando bloco E registro E500...';
    Application.ProcessMessages;
    if not (self.montaRegistro_E500) then Exit;

    self.status.Panels[1].Text:='Gerando bloco E registro E510...';
    Application.ProcessMessages;
    if not (self.montaRegistro_E510) then Exit;
    }

  end;


  {self.status.Panels[1].Text:='Gravando bloco E no arquivo...';
  Application.ProcessMessages;
  self.ComponentesSped.ACBrSPEDFiscal1.WriteBloco_E();  }

  result:=True;
  

end;

procedure TobjSpedFiscal.montaRegistro_E001();
begin

  self.ComponentesSped.ACBrSPEDFiscal1.Bloco_E.RegistroE001New.IND_MOV:=imComDados;

end;

function TobjSpedFiscal.montaRegistro_E100:boolean;
begin

  result:=False;

  {REGISTRO E100: PER�ODO DA APURA��O DO ICMS. }

  try

    with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_E.RegistroE100New) do
    begin

      DT_INI:=StrToDate(self.DATAINI);
      DT_FIN:=StrToDate(self.DATAFIN);

    end;

  except

    on e:Exception do
    begin
      addMessageERRO('Erro ao montar registro E100. '+e.Message);
      Exit;
    end;

  end;

  result:=True;

end;

function TobjSpedFiscal.montaRegistro_E110:Boolean;
var
  expressaoCampo13:Currency;
  expressaoCampo14:Currency;
  expressaoCampo11:Currency;
begin

  result:=false;

  {REGISTRO E110: APURA��O DO ICMS � OPERA��ES PR�PRIAS.}

  

  try

    with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_E.RegistroE110New) do
    begin

      {VL_TOT_DEBITOS:=self.valor_total_debitos;
      VL_TOT_CREDITOS:=self.VL_TOT_CREDITOS_E110;

      expressaoCampo11 := VL_TOT_DEBITOS + (VL_AJ_DEBITOS + VL_TOT_AJ_DEBITOS) + VL_ESTORNOS_CRED - VL_TOT_CREDITOS + (VL_AJ_CREDITOS + VL_TOT_AJ_CREDITOS) + VL_ESTORNOS_DEB + VL_SLD_CREDOR_ANT;
      expressaoCampo14 := VL_TOT_DEBITOS + (VL_AJ_DEBITOS + VL_TOT_AJ_DEBITOS) + VL_ESTORNOS_CRED - VL_TOT_CREDITOS + (VL_AJ_CREDITOS + VL_TOT_AJ_CREDITOS) + VL_ESTORNOS_DEB + VL_SLD_CREDOR_ANT;


      if (expressaoCampo11 >= 0) then
        VL_SLD_APURADO:= expressaoCampo11
      else
        VL_SLD_APURADO:=0;

     //VL_TOT_DED:='';

      VL_ICMS_RECOLHER:=VL_SLD_APURADO-VL_TOT_DED;


      if ( expressaoCampo14 > 0 ) then
        VL_SLD_CREDOR_TRANSPORTAR:=0
      else
        VL_SLD_CREDOR_TRANSPORTAR:=Abs(expressaoCampo14);}


      VL_TOT_DEBITOS:=self.valor_total_debitos;
      VL_TOT_CREDITOS:=self.VL_TOT_CREDITOS_E110;

      expressaoCampo11:=  VL_TOT_DEBITOS + (VL_AJ_DEBITOS + VL_TOT_AJ_DEBITOS) + VL_ESTORNOS_CRED - VL_TOT_CREDITOS + (VL_AJ_CREDITOS + VL_TOT_AJ_CREDITOS) + VL_ESTORNOS_DEB + VL_SLD_CREDOR_ANT;

      if (expressaoCampo11 >= 0) then
        VL_SLD_APURADO:= expressaoCampo11
      else
        VL_SLD_APURADO:= 0;

      expressaoCampo13:= VL_SLD_APURADO - VL_TOT_DED;

      if (expressaoCampo13 < 0) then
        VL_ICMS_RECOLHER:=0
      else
        VL_ICMS_RECOLHER := expressaoCampo13;


      expressaoCampo14 := VL_TOT_DEBITOS + (VL_AJ_DEBITOS + VL_TOT_AJ_DEBITOS) + VL_ESTORNOS_CRED - VL_TOT_CREDITOS + (VL_AJ_CREDITOS + VL_TOT_AJ_CREDITOS) + VL_ESTORNOS_DEB + VL_SLD_CREDOR_ANT;

      if ( expressaoCampo14 > 0 ) then
        VL_SLD_CREDOR_TRANSPORTAR:=0
      else
        VL_SLD_CREDOR_TRANSPORTAR:=Abs(expressaoCampo14);


      self.VL_OR:=self.VL_OR + VL_ICMS_RECOLHER;


    end;

  except

    on e:exception do
    begin

      addMessageERRO('Erro ao montar registro E110. '+e.Message);
      Exit;

    end;

  end;

  result:=True;

end;

function TobjSpedFiscal.montaRegistro_E116():Boolean;
begin

  result:=False;


  {REGISTRO E116: OBRIGA��ES DO ICMS A RECOLHER � OPERA��ES PR�PRIAS.}

  try

    with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_E.RegistroE116New) do
    begin

      COD_OR   :=self.COD_OBRIGACOES_ICMS;
      VL_OR    := self.VL_OR;
      DT_VCTO  := StrToDate(self.DATAFIN);
      COD_REC  := self.COD_RECEITAESTADUAL;
      IND_PROC := opNenhum;
      MES_REF := formatdatetime ('mmyyyy',StrToDate (self.DATAFIN));

    end;

  except

    on e:Exception do
    begin

      addMessageERRO('Erro ao montar registro E116. '+e.Message);
      Exit;

    end;

  end;

  result:=True;

end;

function TobjSpedFiscal.montaRegistro_E500: Boolean;
begin

  result:=False;

  try

    with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_E.RegistroE500New) do
    begin

      IND_APUR := iaMensal;
      DT_INI   := StrToDateTime(self.DATAINI);
      DT_FIN   := StrToDateTime(Self.DATAFIN);

    end;

  except

    on e:Exception do
    begin

      addMessageERRO('Erro ao montar registro E500. '+e.Message);
      Exit;

    end;

  end;

  result:=True;

end;

function TobjSpedFiscal.montaRegistro_E510: Boolean;
begin

  result:=False;

  try

    with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_E.RegistroE510New) do
    begin

      {CFOP
      CST_IPI
      VL_CONT_IPI
      VL_BC_IPI
      VL_IPI;}

    end;

  except

    on e:Exception do
    begin

      addMessageERRO('Erro ao montar registro E5010. '+e.Message);
      Exit;

    end;

  end;


  result:=True;

end;

{REGISTROS DO BLOCO G}

function TobjSpedFiscal.montaBloco_G():Boolean;
begin

  Result:=False;

  if not(self.montaRegistro_G001(imSemDados)) then Exit;

  self.status.Panels[1].Text:='Gravando bloco G no arquivo...';
  Application.ProcessMessages;

  {tem que mandar aqui porq no componente ele teste a data ini, se for maio que 01/01/2001 ele grava o bloco G senao nao grava}
  self.ComponentesSped.ACBrSPEDFiscal1.Bloco_G.DT_INI:=StrToDate(self.DATAINI);
  self.ComponentesSped.ACBrSPEDFiscal1.Bloco_G.DT_FIN:=StrToDate(self.DATAFIN);

  //self.ComponentesSped.ACBrSPEDFiscal1.WriteBloco_G();

  result:=True;

end;

function TobjSpedFiscal.montaRegistro_G001(teveDados:TACBrIndicadorMovimento = imComDados):Boolean;
begin

  self.status.Panels[1].Text:='Gerando bloco G registro G001...';
  Application.ProcessMessages;

  result:=False;

  self.ComponentesSped.ACBrSPEDFiscal1.Bloco_G.RegistroG001New.IND_MOV:=teveDados;

  result:=True;

end;

{REGISTROS DO BLOCO H}

function TobjSpedFiscal.montaBloco_H():boolean;
begin

  result:=False;

  self.status.Panels[1].Text:='Gerando bloco H registro H001...';
  Application.ProcessMessages;
  if not (self.montaRegistro_H001()) then Exit;

  self.status.Panels[1].Text:='Gerando bloco H registro H005 H010...';
  Application.ProcessMessages;
  if not (self.montaRegistro_H005_H010()) then;

  {self.status.Panels[1].Text:='Gravando bloco H no arquivo...';
  Application.ProcessMessages;
  self.ComponentesSped.ACBrSPEDFiscal1.WriteBloco_H();}

  result:=True;

end;

function TobjSpedFiscal.montaRegistro_H001(teveDados:TACBrIndicadorMovimento = imComDados):Boolean;
begin

  result:=False;

  try

    self.ComponentesSped.ACBrSPEDFiscal1.Bloco_H.RegistroH001New.IND_MOV:=teveDados;

  except

    on e:Exception do
    begin

      addMessageERRO('Erro ao montar registro H001. '+e.Message);
      Exit;

    end;

  end;

  result:=True;

end;

function TobjSpedFiscal.montaRegistro_H005_H010():boolean;
var
  valorTotalEstoque:Currency;
  _VL_UNIT,_QTD,_COD_ITEM,_UNID,codigocor:string;
  _VL_ITEM:Currency;
  dataInicial,dataFinal:string;
begin

  result:=False;

 if (geraRegistroH) then
 begin
   dataInicial :=troca(self.DATAINI_ESTOQUE,'/','.');
   dataFinal   :=troca(self.DATAFIN_ESTOQUE,'/','.');
   with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_H.RegistroH005New) do
   begin

      if not (validaData (self.DATAFIN_ESTOQUE)) then
      begin
        addMessageERRO ('Data do registro H invalida (relatorio de estoque)');
        Exit;
      end;

      DT_INV := StrToDate (self.DATAFIN_ESTOQUE);

      { registro (H010) n�o de ver informado se o campo "VL_INV do registro H005" for igual a 0}

      try

        valorTotalEstoque:=0;

        //BUSCO PRIMEIRO AS FERRAGENS
        self.objquery.SQL.Text:=
        'select distinct(codigomaterialcor),codigomaterial,nomematerial,nomecor,material '+
        'from procmovimentoestoque('+QuotedStr (dataInicial) + ',' + QuotedStr (dataFinal)+') '+
        'where material=''FER '' ' ;

        self.objquery.Active := True;
        self.objquery.First;
        while not (self.objquery.Eof) do
        begin            
            status.Panels[1].Text:='Gerando Bloco H registro H005 H010 Item: '+self.objquery.FieldByName('nomematerial').AsString;
            Application.ProcessMessages;
            codigocor:=objquery.fieldbyname('codigomaterialcor').AsString;

            queryTemp.Active:=False;
            queryTemp.SQL.Clear;
            queryTemp.SQL.Text:=
            'select tabferragem.descricao,tabferragem.codigo as item,tabcor.descricao as cor,tabferragem.referencia, SUM(e.quantidade) quantidade '+
            'from tabestoque e '+
            'join tabferragemcor on tabferragemcor.codigo=e.ferragemcor '+
            'join tabferragem on tabferragem.codigo=tabferragemcor.ferragem '+
            'join tabcor on tabcor.codigo=tabferragemcor.cor '+
            'where (e.data <= '+QuotedStr (dataFinal)+') and (e.ferragemcor ='+codigocor+') '+
            'group by tabferragem.descricao,tabferragem.codigo,tabcor.descricao,tabferragem.referencia';

            queryTemp.active:=True;
            queryTemp.First;
            with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_H.RegistroH010New) do
            begin
              _COD_ITEM := self.objquery.fieldbyname('material').AsString+'COD'+self.objquery.fieldbyname('codigomaterial').AsString+'COR'+self.objquery.fieldbyname('codigomaterialcor').AsString;
              _VL_UNIT  := get_campoTabela ('precocusto','codigo','TABFERRAGEM',self.objquery.fieldbyname('codigomaterial').AsString);
              _QTD      := queryTemp.fieldbyname ('quantidade').AsString;
              _UNID     := get_campoTabela ('unidade','codigo','TABFERRAGEM',self.objquery.fieldbyname('codigomaterial').AsString);
              _VL_ITEM  := StrToCurr (_QTD) * StrToCurr (_VL_UNIT);

              COD_ITEM := _COD_ITEM;
              addStr (strItem,COD_ITEM);

              UNID := _UNID;
              addStr (strUnidade,UNID);

              QTD      := StrToCurr (_QTD);
              VL_UNIT  := StrToCurr (_VL_UNIT);
              VL_ITEM  := _VL_ITEM;

              valorTotalEstoque:=valorTotalEstoque+VL_ITEM;
              IND_PROP := piInformante;

              COD_CTA:='1.01.03.01.01';

              if (QTD < 0) then
                strERROS.Add('Estoque negativo para o produto: '+COD_ITEM+' (Estoque: '+_QTD+')');

            end;
            Self.objquery.Next;
        end;

        //BUSCO OS PERFILADOS
        self.objquery.SQL.Text:=
        'select distinct(codigomaterialcor),codigomaterial,nomematerial,nomecor,material '+
        'from procmovimentoestoque('+QuotedStr (dataInicial) + ',' + QuotedStr (dataFinal)+') '+
        'where material=''PER '' ' ;

        self.objquery.Active := True;
        self.objquery.First;
        while not (self.objquery.Eof) do
        begin
            status.Panels[1].Text:='Gerando Bloco H registro H005 H010 Item: '+self.objquery.FieldByName('nomematerial').AsString;
            Application.ProcessMessages;
            codigocor:=objquery.fieldbyname('codigomaterialcor').AsString;

            queryTemp.Active:=False;
            queryTemp.SQL.Clear;
            queryTemp.SQL.Text:=
            'select tabperfilado.descricao,tabperfilado.codigo as item,tabcor.descricao as cor,tabperfilado.referencia, SUM(e.quantidade) quantidade '+
            'from tabestoque e '+
            'join tabperfiladocor on tabperfiladocor.codigo=e.perfiladocor '+
            'join tabperfilado on tabperfilado.codigo=tabperfiladocor.perfilado '+
            'join tabcor on tabcor.codigo=tabperfiladocor.cor '+
            'where (e.data <= '+QuotedStr (dataFinal)+') and (e.perfiladocor ='+codigocor+') '+
            'group by tabperfilado.descricao,tabperfilado.codigo,tabcor.descricao,tabperfilado.referencia';

            queryTemp.active:=True;
            queryTemp.First;
            with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_H.RegistroH010New) do
            begin
              _COD_ITEM := self.objquery.fieldbyname('material').AsString+'COD'+self.objquery.fieldbyname('codigomaterial').AsString+'COR'+self.objquery.fieldbyname('codigomaterialcor').AsString;
              _VL_UNIT  := get_campoTabela ('precocusto','codigo','TABPERFILADO',self.objquery.fieldbyname('codigomaterial').AsString);
              _QTD      := queryTemp.fieldbyname ('quantidade').AsString;
              _UNID     := get_campoTabela ('unidade','codigo','TABPERFILADO',self.objquery.fieldbyname('codigomaterial').AsString);
              _VL_ITEM  := StrToCurr (_QTD) * StrToCurr (_VL_UNIT);

              COD_ITEM := _COD_ITEM;
              addStr (strItem,COD_ITEM);

              UNID := _UNID;
              addStr (strUnidade,UNID);

              QTD      := StrToCurr (_QTD);
              VL_UNIT  := StrToCurr (_VL_UNIT);
              VL_ITEM  := _VL_ITEM;

              valorTotalEstoque:=valorTotalEstoque+VL_ITEM;
              IND_PROP := piInformante;

              COD_CTA:='1.01.03.01.01';

              if (QTD < 0) then
                strERROS.Add('Estoque negativo para o produto: '+COD_ITEM+' (Estoque: '+_QTD+')');

            end;
            Self.objquery.Next;
        end;

       //BUSCO OS VIDROS
        self.objquery.SQL.Text:=
        'select distinct(codigomaterialcor),codigomaterial,nomematerial,nomecor,material '+
        'from procmovimentoestoque('+QuotedStr (dataInicial) + ',' + QuotedStr (dataFinal)+') '+
        'where material=''VID '' ' ;

        self.objquery.Active := True;
        self.objquery.First;
        while not (self.objquery.Eof) do
        begin
            status.Panels[1].Text:='Gerando Bloco H registro H005 H010 Item: '+self.objquery.FieldByName('nomematerial').AsString;
            Application.ProcessMessages;
            codigocor:=objquery.fieldbyname('codigomaterialcor').AsString;

            queryTemp.Active:=False;
            queryTemp.SQL.Clear;
            queryTemp.SQL.Text:=
            'select tabvidro.descricao,tabvidro.codigo as item,tabcor.descricao as cor,tabvidro.referencia, SUM(e.quantidade) quantidade '+
            'from tabestoque e '+
            'join tabvidrocor on tabvidrocor.codigo=e.vidrocor '+
            'join tabvidro on tabvidro.codigo=tabvidrocor.vidro '+
            'join tabcor on tabcor.codigo=tabvidrocor.cor '+
            'where (e.data <= '+QuotedStr (dataFinal)+') and (e.vidrocor ='+codigocor+') '+
            'group by tabvidro.descricao,tabvidro.codigo,tabcor.descricao,tabvidro.referencia';

            queryTemp.active:=True;
            queryTemp.First;
            with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_H.RegistroH010New) do
            begin
              _COD_ITEM := self.objquery.fieldbyname('material').AsString+'COD'+self.objquery.fieldbyname('codigomaterial').AsString+'COR'+self.objquery.fieldbyname('codigomaterialcor').AsString;
              _VL_UNIT  := get_campoTabela ('precocusto','codigo','TABVIDRO',self.objquery.fieldbyname('codigomaterial').AsString);
              _QTD      := queryTemp.fieldbyname ('quantidade').AsString;
              _UNID     := get_campoTabela ('unidade','codigo','TABVIDRO',self.objquery.fieldbyname('codigomaterial').AsString);
              _VL_ITEM  := StrToCurr (_QTD) * StrToCurr (_VL_UNIT);

              COD_ITEM := _COD_ITEM;
              addStr (strItem,COD_ITEM);

              UNID := _UNID;
              addStr (strUnidade,UNID);

              QTD      := StrToCurr (_QTD);
              VL_UNIT  := StrToCurr (_VL_UNIT);
              VL_ITEM  := _VL_ITEM;

              valorTotalEstoque:=valorTotalEstoque+VL_ITEM;
              IND_PROP := piInformante;

              COD_CTA:='1.01.03.01.01';

              if (QTD < 0) then
                strERROS.Add('Estoque negativo para o produto: '+COD_ITEM+' (Estoque: '+_QTD+')');

            end;
            Self.objquery.Next;
        end;

        //BUSCO OS DIVERSOS
        self.objquery.SQL.Text:=
        'select distinct(codigomaterialcor),codigomaterial,nomematerial,nomecor,material '+
        'from procmovimentoestoque('+QuotedStr (dataInicial) + ',' + QuotedStr (dataFinal)+') '+
        'where material=''DIV '' ' ;

        self.objquery.Active := True;
        self.objquery.First;
        while not (self.objquery.Eof) do
        begin
            status.Panels[1].Text:='Gerando Bloco H registro H005 H010 Item: '+self.objquery.FieldByName('nomematerial').AsString;
            Application.ProcessMessages;
            codigocor:=objquery.fieldbyname('codigomaterialcor').AsString;

            queryTemp.Active:=False;
            queryTemp.SQL.Clear;
            queryTemp.SQL.Text:=
            'select tabdiverso.descricao,tabdiverso.codigo as item,tabcor.descricao as cor,tabdiverso.referencia, SUM(e.quantidade) quantidade '+
            'from tabestoque e '+
            'join tabdiversocor on tabdiversocor.codigo=e.diversocor '+
            'join tabdiverso on tabdiverso.codigo=tabdiversocor.diverso '+
            'join tabcor on tabcor.codigo=tabdiversocor.cor '+
            'where (e.data <= '+QuotedStr (dataFinal)+') and (e.diversocor ='+codigocor+') '+
            'group by tabdiverso.descricao,tabdiverso.codigo,tabcor.descricao,tabdiverso.referencia';

            queryTemp.active:=True;
            queryTemp.First;
            with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_H.RegistroH010New) do
            begin
              _COD_ITEM := self.objquery.fieldbyname('material').AsString+'COD'+self.objquery.fieldbyname('codigomaterial').AsString+'COR'+self.objquery.fieldbyname('codigomaterialcor').AsString;
              _VL_UNIT  := get_campoTabela ('precocusto','codigo','TABDIVERSO',self.objquery.fieldbyname('codigomaterial').AsString);
              _QTD      := queryTemp.fieldbyname ('quantidade').AsString;
              _UNID     := get_campoTabela ('unidade','codigo','TABDIVERSO',self.objquery.fieldbyname('codigomaterial').AsString);
              _VL_ITEM  := StrToCurr (_QTD) * StrToCurr (_VL_UNIT);

              COD_ITEM := _COD_ITEM;
              addStr (strItem,COD_ITEM);

              UNID := _UNID;
              addStr (strUnidade,UNID);

              QTD      := StrToCurr (_QTD);
              VL_UNIT  := StrToCurr (_VL_UNIT);
              VL_ITEM  := _VL_ITEM;

              valorTotalEstoque:=valorTotalEstoque+VL_ITEM;
              IND_PROP := piInformante;

              COD_CTA:='1.01.03.01.01';

              if (QTD < 0) then
                strERROS.Add('Estoque negativo para o produto: '+COD_ITEM+' (Estoque: '+_QTD+')');

            end;
            Self.objquery.Next;
        end;

        //BUSCO OS KITBOX
        self.objquery.SQL.Text:=
        'select distinct(codigomaterialcor),codigomaterial,nomematerial,nomecor,material '+
        'from procmovimentoestoque('+QuotedStr (dataInicial) + ',' + QuotedStr (dataFinal)+') '+
        'where material=''KIT '' ' ;

        self.objquery.Active := True;
        self.objquery.First;
        while not (self.objquery.Eof) do
        begin
            status.Panels[1].Text:='Gerando Bloco H registro H005 H010 Item: '+self.objquery.FieldByName('nomematerial').AsString;
            Application.ProcessMessages;
            codigocor:=objquery.fieldbyname('codigomaterialcor').AsString;

            queryTemp.Active:=False;
            queryTemp.SQL.Clear;
            queryTemp.SQL.Text:=
            'select tabkitbox.descricao,tabkitbox.codigo as item,tabcor.descricao as cor,tabkitbox.referencia, SUM(e.quantidade) quantidade '+
            'from tabestoque e '+
            'join tabkitboxcor on tabkitboxcor.codigo=e.kitboxcor '+
            'join tabkitbox on tabkitbox.codigo=tabkitboxcor.kitbox '+
            'join tabcor on tabcor.codigo=tabkitboxcor.cor '+
            'where (e.data <= '+QuotedStr (dataFinal)+') and (e.kitboxcor ='+codigocor+') '+
            'group by tabkitbox.descricao,tabkitbox.codigo,tabcor.descricao,tabkitbox.referencia';

            queryTemp.active:=True;
            queryTemp.First;
            with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_H.RegistroH010New) do
            begin
              _COD_ITEM := self.objquery.fieldbyname('material').AsString+'COD'+self.objquery.fieldbyname('codigomaterial').AsString+'COR'+self.objquery.fieldbyname('codigomaterialcor').AsString;
              _VL_UNIT  := get_campoTabela ('precocusto','codigo','TABKITBOX',self.objquery.fieldbyname('codigomaterial').AsString);
              _QTD      := queryTemp.fieldbyname ('quantidade').AsString;
              _UNID     := get_campoTabela ('unidade','codigo','TABKITBOX',self.objquery.fieldbyname('codigomaterial').AsString);
              _VL_ITEM  := StrToCurr (_QTD) * StrToCurr (_VL_UNIT);

              COD_ITEM := _COD_ITEM;
              addStr (strItem,COD_ITEM);

              UNID := _UNID;
              addStr (strUnidade,UNID);

              QTD      := StrToCurr (_QTD);
              VL_UNIT  := StrToCurr (_VL_UNIT);
              VL_ITEM  := _VL_ITEM;

              valorTotalEstoque:=valorTotalEstoque+VL_ITEM;
              IND_PROP := piInformante;

              COD_CTA:='1.01.03.01.01';

              if (QTD < 0) then
                strERROS.Add('Estoque negativo para o produto: '+COD_ITEM+' (Estoque: '+_QTD+')');

            end;
            Self.objquery.Next;
        end;

        //BUSCO OS PERSIANA
        self.objquery.SQL.Text:=
        'select distinct(codigomaterialcor),codigomaterial,nomematerial,nomecor,material '+
        'from procmovimentoestoque('+QuotedStr (dataInicial) + ',' + QuotedStr (dataFinal)+') '+
        'where material=''PES '' ' ;

        self.objquery.Active := True;
        self.objquery.First;
        while not (self.objquery.Eof) do
        begin
            status.Panels[1].Text:='Gerando Bloco H registro H005 H010 Item: '+self.objquery.FieldByName('nomematerial').AsString;
            Application.ProcessMessages;
            codigocor:=objquery.fieldbyname('codigomaterialcor').AsString;

            queryTemp.Active:=False;
            queryTemp.SQL.Clear;
            queryTemp.SQL.Text:=
            'select tabpersiana.nome,tabpersiana.codigo as item,tabcor.descricao as cor,tabpersiana.referencia, SUM(e.quantidade) quantidade '+
            'from tabestoque e '+
            'join tabpersianagrupodiametrocor on tabpersianagrupodiametrocor.codigo=e.persianagrupodiametrocor '+
            'join tabpersiana on tabpersiana.codigo=tabpersianagrupodiametrocor.persiana '+
            'join tabcor on tabcor.codigo=tabpersianagrupodiametrocor.cor '+
            'where (e.data <= '+QuotedStr (dataFinal)+') and (e.persianagrupodiametrocor ='+codigocor+') '+
            'group by tabpersiana.nome,tabpersiana.codigo,tabcor.descricao,tabpersiana.referencia';

            queryTemp.active:=True;
            queryTemp.First;
            with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_H.RegistroH010New) do
            begin
              _COD_ITEM := self.objquery.fieldbyname('material').AsString+'COD'+self.objquery.fieldbyname('codigomaterial').AsString+'COR'+self.objquery.fieldbyname('codigomaterialcor').AsString;
              _VL_UNIT  := get_campoTabela ('precocusto','codigo','TABPERSIANAGRUPODIAMETROCOR',self.objquery.fieldbyname('codigomaterialcor').AsString);
              _QTD      := queryTemp.fieldbyname ('quantidade').AsString;
              _UNID     := 'M�';     //get_campoTabela ('unidade','codigo','TABPERSIANA',self.objquery.fieldbyname('codigomaterialcor').AsString);
              _VL_ITEM  := StrToCurr (_QTD) * StrToCurr (_VL_UNIT);

              COD_ITEM := _COD_ITEM;
              addStr (strItem,COD_ITEM);

              UNID := _UNID;
              addStr (strUnidade,UNID);

              QTD      := StrToCurr (_QTD);
              VL_UNIT  := StrToCurr (_VL_UNIT);
              VL_ITEM  := _VL_ITEM;

              valorTotalEstoque:=valorTotalEstoque+VL_ITEM;
              IND_PROP := piInformante;

              COD_CTA:='1.01.03.01.01';

              if (QTD < 0) then
                strERROS.Add('Estoque negativo para o produto: '+COD_ITEM+' (Estoque: '+_QTD+')');

            end;
            Self.objquery.Next;
        end;
        if (valorTotalEstoque > 0) then
        begin

          VL_INV := valorTotalEstoque;

        end else
        begin

          montaRegistro_H001(imSemDados);

        end;

      except

        on e:Exception do
        begin

          addMessageERRO('Erro ao montar registro H010 H005. '+e.Message);
          Exit;

        end;

      end;

   end;

 end
 else
 begin

   self.montaRegistro_H001();

   with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_H.RegistroH005New) do
   begin

      DT_INV := StrToDate(self.DATAFIN);
      VL_INV := 0;

   end;

 end;

 result:=True;

end;

{var
  valorTotalEstoque:Currency;
  psigla:string;
begin

   result:=False;
   if(geraRegistroH) then
   begin
         with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_H.RegistroH005New) do
         begin

            DT_INV := StrToDate(self.DATAINI);
            {este registro (H010) n�o de ver informado se o campo "VL_INV do registro H005" for igual a 0}

           { try

              valorTotalEstoque:=0;
              abreQueryProdutos_inventario();
              while not (self.objquery.Eof) do
              begin

                status.Panels[1].Text:='Gerando Bloco H registro H005 H010 Item: '+self.objquery.FieldByName('item').AsString;
                Application.ProcessMessages;


                if (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_0.Registro0001.Registro0200.LocalizaRegistro(self.objquery.fieldbyname('item').AsString)) then
                begin

                  with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_H.RegistroH010New) do
                  begin

                    if (self.objquery.FieldByName('movimento').AsString = 'E') then
                      psigla:='F'
                    else if (self.objquery.FieldByName('movimento').AsString = 'S') then
                      psigla:='C'
                    else
                      strERROS.Add('Campo movimento invalido para o item: '+self.objquery.FieldByName('item').AsString+'. M�s: '+self.objquery.FieldByName('mes').AsString);

                    COD_ITEM := self.objquery.FieldByName('item').AsString;
                    UNID     := self.objquery.FieldByName('unidade').AsString;
                    QTD      := self.objquery.FieldByName('estoque').AsCurrency;
                    VL_UNIT  := self.objquery.FieldBYName('custo_consumidor').AsCurrency;
                    VL_ITEM  := self.objquery.FieldBYName('valorTotalItem').AsCurrency;
                    valorTotalEstoque:=valorTotalEstoque+VL_ITEM;
                    IND_PROP := self.get_posseItem(self.objquery.FieldBYName('indicadorpropriedade').AsInteger);

                    if (self.objquery.FieldBYName('indicadorpropriedade').AsInteger in[1..2]) then
                      COD_PART := self.objquery.FieldBYName('codigoparticipante').AsString+psigla;

                    COD_CTA:='1.01.03.01.01'; {aqui estou mandando o codigo de Mercadorias para Revenda}

                    {if (QTD < 0) then
                      strERROS.Add('Estoque negativo para o produto: '+COD_ITEM+' (Estoque: '+self.objquery.FieldByName('estoque').AsString+')');

                  end;

                end;

                self.objquery.Next;
          
              end;

              if (valorTotalEstoque > 0) then
              begin

                VL_INV := valorTotalEstoque;

              end else
              begin

                montaRegistro_H001(imSemDados);

              end;

            except

              on e:Exception do
              begin

                addMessageERRO('Erro ao montar registro H010 H005. '+e.Message);
                Exit;

              end;

            end;

         end;
   end else
   begin

     self.montaRegistro_H001();

     with (self.ComponentesSped.ACBrSPEDFiscal1.Bloco_H.RegistroH005New) do
     begin

        DT_INV := StrToDate(self.DATAFIN);
        VL_INV := 0;

     end;

   end;

   result:=True;


end;  }

{REGISTROS DO BLOCO 1}

function TobjSpedFiscal.montaBloco_1(): Boolean;
begin

  result:=False;

  if not (montaRegistro_1001(imSemDados)) then Exit;

  self.status.Panels[1].Text:='Gravando bloco 1 no arquivo...';
  Application.ProcessMessages;
  //self.ComponentesSped.ACBrSPEDFiscal1.WriteBloco_1;

  result:=True;
  
end;

function TobjSpedFiscal.montaRegistro_1001(teveDados: TACBrIndicadorMovimento): Boolean;
begin

  self.status.Panels[1].Text:='Gerando bloco 1 registro 1001...';
  Application.ProcessMessages;

  result:=False;

  self.ComponentesSped.ACBrSPEDFiscal1.Bloco_1.Registro1001New.IND_MOV:=teveDados;

  result:=True;

end;

{REGISTROS DO BLOCO 9}

function TobjSpedFiscal.montaBloco_9: Boolean;
begin

  result:=false;

  if not (montaRegistro_9001) then Exit;
  if not (montaRegistro_9900) then Exit;
  if not (montaRegistro_9999) then Exit;

  self.status.Panels[1].Text:='Gravando bloco 9 no arquivo...';
  Application.ProcessMessages;
  //self.ComponentesSped.ACBrSPEDFiscal1.WriteBloco_9;

  result:=True;

end;

function TobjSpedFiscal.montaRegistro_9001(): Boolean;
begin

  self.status.Panels[1].Text:='Gerando bloco 9 registro 9001...';
  Application.ProcessMessages;

  result:=False;

  self.ComponentesSped.ACBrSPEDFiscal1.Bloco_9.Registro9001.IND_MOV:=imComDados;

  result:=True;

end;

function TobjSpedFiscal.montaRegistro_9900():Boolean;
begin

  self.status.Panels[1].Text:='Gerando bloco 9 registro 9900...';
  Application.ProcessMessages;

  result:=False;

  result:=True;

end;

function TobjSpedFiscal.montaRegistro_9999():Boolean;
begin

  self.status.Panels[1].Text:='Gerando bloco 9 registro 9999...';
  Application.ProcessMessages;

  result:=False;

  result:=True;

end;


procedure TobjSpedFiscal.submit_buffernotas(notasBuffer: string);
begin
  self.BufferNotas:=notasBuffer;
end;

procedure TobjSpedFiscal.submit_COD_FIN(cod_fin: string);
begin
  self.COD_FIN:=cod_fin;
end;

procedure TobjSpedFiscal.submit_DATAFIN(datafin: string);
begin
  self.DATAFIN:=datafin;
end;

procedure TobjSpedFiscal.submit_DATAINI(dataini: string);
begin
  self.DATAINI:=dataini;
end;

procedure TobjSpedFiscal.submit_IND_ATIV(ind_ativ: string);
begin
  self.IND_ATIV:=ind_ativ;
end;

procedure TobjSpedFiscal.submit_IND_PERFIL(ind_perfil: string);
begin
  Self.IND_PERFIL:=ind_perfil;
end;

procedure TobjSpedFiscal.submit_linhasBuffer(linhasBuffer: string);
begin
  self.linhasBuffer:=linhasBuffer;
end;

procedure TobjSpedFiscal.submit_pathDestino(pathDestino: string);
begin
  self.pathDestino:=pathDestino;
end;

function TobjSpedFiscal.get_situacao(situacao: string): TACBrSituacaoDocto;
begin

  situacao:=UpperCase(situacao);

  if (situacao = 'I') or (situacao = 'G') then
    result := sdRegular

  else if (situacao = 'C') then
    result := sdCancelado

  else if (situacao = 'Z') then
    result := sdDoctoNumInutilizada;


end;

function TobjSpedFiscal.get_tipoFrete(tipo: string): TACBrTipoFrete;
begin

    if (tipo = '1') then
      result := tfPorContaEmitente

    else if (tipo = '2') then
      result := tfPorContaDestinatario

    else
      result := tfSemCobrancaFrete;

end;

function TobjSpedFiscal.get_valorPis(BCpis: Currency): Currency;
var
  pPis:Currency;
begin

  try

    pPIS       :=  StrToCurrDef (ObjEmpresaGlobal.get_pis(),0);
    result     :=  (bcPIS * pPIS / 100);

  except

    MensagemAviso ('N�o foi possivel cancular imposto PIS');
    result := 0;

  end;

end;

function TobjSpedFiscal.get_valorCofins(BCCofins: Currency): Currency;
var
  pCofins:Currency;
begin

  try

    //pCofins    :=  StrToCurrDef  (ObjEmpresaGlobal.Get_Cofins(),0);
    //result     :=  (BCCofins * pCofins / 100);

  except

    MensagemAviso ('N�o foi possivel cancular imposto COFINS');
    result := 0;

  end;

end;

function TobjSpedFiscal.get_VL_BC_ICMS(pReducao,baseCalculo: Currency): Currency;
begin

  Result := baseCalculo - (pReducao * baseCalculo/100);
  result := StrToCurrDef(FormatFloat('0.00',Result),0);

end;

function TobjSpedFiscal.get_VL_ICMS(bc_icms, aliquota:currency): Currency;
begin

  result := bc_icms * aliquota / 100;
  result := StrToCurrDef(FormatFloat('0.00',result),0);

end;

function TobjSpedFiscal.possuiDados(query: TIBQuery): Boolean;
begin

  query.Last;

  if (query.RecordCount > 0) then
    result := True
  else
    result := False;

  query.First;

end;

procedure TobjSpedFiscal.submit_dataSourceNFEntrada(ds: TDataSource);
begin
  Self.dataSourceNFEntrada:=ds;
end;

procedure TobjSpedFiscal.submit_dataSourceNFSaida(ds: TDataSource);
begin
  self.dataSourceNFSaida:=ds;
end;

procedure TobjSpedFiscal.mostraNFnoGrid(parametro: Boolean);
begin

  if (parametro) then
  begin

    self.dataSourceNFEntrada.DataSet := self.queryNFEntrada;
    self.dataSourceNFSaida  .DataSet := self.queryNFSaida;

  end else
  begin

    self.dataSourceNFEntrada.DataSet := nil;
    self.dataSourceNFSaida  .DataSet := nil;

  end;

end;


procedure TobjSpedFiscal.abreQueryRegC190_saida_cst(pCodigoNota:string);
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select pm_nf.csta,pm_nf.cstb,pm_nf.cfop,pm_nf.aliquota,');
  self.objquery.SQL.Add('pm_nf.valorfinal,pm_nf.BC_ICMS,');
  self.objquery.SQL.Add('''0'' as valor_ipi');
  self.objquery.SQL.Add('from proc_materiais_nf pm_nf');
  self.objquery.SQL.Add('where pm_nf.notafiscal = '+pCodigoNota+' and pm_nf.cstb is not null');
  self.objquery.SQL.Add('order by pm_nf.csta,cstb, pm_nf.cfop, pm_nf.aliquota');
  

  self.objquery.Active:=True;
  self.objquery.First;

end;

procedure TobjSpedFiscal.abreQueryRegC190_saida_csosn(pCodigoNota:string);
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select pm_nf.csosn,pm_nf.cfop,pm_nf.aliquota,');
  self.objquery.SQL.Add('pm_nf.BC_ICMS,''0'' as valor_ipi');
  self.objquery.SQL.Add('from proc_materiais_nf pm_nf');

  self.objquery.SQL.Add('where pm_nf.notafiscal = '+pCodigoNota+' and pm_nf.csosn <> ''''');
  self.objquery.SQL.Add('order by pm_nf.csosn, pm_nf.cfop, pm_nf.aliquota');
  ///InputBox('','',self.objquery.SQL.Text);
  self.objquery.Active:=True;
  self.objquery.First;

end;

procedure TobjSpedFiscal.abreQueryRegC190_entrada_csosn(pCodigoNota:string);
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select pm_ep.csta,pm_ep.cstb,pm_ep.cfop,pm_ep.creditoicms as aliquota,pm_ep.valorfinal,');
  self.objquery.SQL.Add('pm_ep.bc_icms');
  self.objquery.SQL.Add('from proc_materiais_ep pm_ep');

  self.objquery.SQL.Add('where (pm_ep.entrada = '+pCodigoNota+') and (pm_ep.csosn is not null)');
  self.objquery.SQL.Add('order by pm_ep.csosn, pm_ep.cfop, pm_ep.creditoicms');

  self.objquery.Active:=True;
  self.objquery.First;

end;

procedure TobjSpedFiscal.abreQueryRegE510_saida_CFOPCST ();
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('');
  self.objquery.SQL.Add('');
  self.objquery.SQL.Add('');

  self.objquery.SQL.Add('');
  self.objquery.SQL.Add('');

  self.objquery.Active:=True;
  self.objquery.First;

  {
       pnf.valorfinal - (pNF.reducaobasecalculo * pnf.valorfinal/100) as BC_ICMS
from tabprodutosnotafiscal pNF
join tabnotafiscal nf on nf.codigo = pNF.notafiscal

where (nf.dataemissao = '21.09.2011') and (pNF.cstipi <> '')

order by pNF.cfop, pNF.cstipi}

end;

procedure TobjSpedFiscal.abreQueryRegC190_entrada_cst(pCodigoNota:string);
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select  pm_ep.csta,pm_ep.cstb,pm_ep.cfop,pm_ep.creditoicms as aliquota,pm_ep.valorfinal,');
  self.objquery.SQL.Add('pm_ep.bc_icms,''0'' as valor_ipi');
  self.objquery.SQL.Add('from proc_materiais_ep pm_ep');

  self.objquery.SQL.Add('where pm_ep.entrada='+pCodigoNota+' and pm_ep.cstb is not null');
  self.objquery.SQL.Add('order by pm_ep.csta,pm_ep.cstb,pm_Ep.cfop,pm_ep.creditoicms');

  self.objquery.Active:=True;
  self.objquery.First;

end;

function TobjSpedFiscal.registroCorrespondenteCST(parametro: string;var valorFinal,BC_ICMS,VL_ICMS,VALOR_RED_BC,VALORIPI:Currency; query:TIBQuery): Boolean;
begin

  query.Next;

  if not (query.Eof) then
  begin

    if (parametro = query.FieldByName('csta').AsString + query.FieldByName('cstb').AsString + query.FieldByName('cfop').AsString + query.FieldByName('aliquota').AsString ) then
    begin

      valorFinal   := valorFinal   + trunca(query.fieldbyname('valorFInal').AsCurrency);
      BC_ICMS      := BC_ICMS      + trunca(query.fieldbyname('BC_ICMS').AsCurrency);
      VL_ICMS      := VL_ICMS      + trunca(query.fieldbyname('BC_ICMS').AsCurrency) * query.fieldbyname('aliquota').AsCurrency / 100;
      VALORIPI     := VALORIPI     + trunca(query.fieldbyname('VALOR_IPI').AsCurrency);

      if (query.fieldbyname('BC_ICMS').AsCurrency > 0) then
      begin

        if trunca (query.fieldbyname('valorFInal').AsCurrency) < trunca(query.fieldbyname('BC_ICMS').AsCurrency) then
          VALOR_RED_BC:= VALOR_RED_BC + (trunca(query.fieldbyname('valorFInal').AsCurrency) + (trunca(query.fieldbyname('BC_ICMS').AsCurrency) - trunca(query.fieldbyname('valorFInal').AsCurrency))) - trunca(query.fieldbyname('BC_ICMS').AsCurrency)
        else
          VALOR_RED_BC := VALOR_RED_BC + trunca(query.fieldbyname('valorFInal').AsCurrency) - trunca(query.fieldbyname('BC_ICMS').AsCurrency);

      end;

      Result := True;

    end else
      result := False;

  end;


end;

function TobjSpedFiscal.registroCorrespondenteCSOSN(parametro: string;var valorFinal, BC_ICMS, VL_ICMS, VALOR_RED_BC: Currency;query: TIBQuery): Boolean;
begin

  query.Next;

  if not (query.Eof) then
  begin

    if (parametro = query.FieldByName('csosn').AsString + query.FieldByName('cfop').AsString + query.FieldByName('aliquota').AsString ) then
    begin

      valorFinal   := valorFinal   + query.fieldbyname('valorFInal').AsCurrency;
      BC_ICMS      := BC_ICMS      + query.fieldbyname('BC_ICMS').AsCurrency;
      VL_ICMS      := VL_ICMS      + query.fieldbyname('BC_ICMS').AsCurrency * query.fieldbyname('aliquota').AsCurrency / 100;
      VALOR_RED_BC := VALOR_RED_BC + query.fieldbyname('valorFInal').AsCurrency - query.fieldbyname('BC_ICMS').AsCurrency;

      Result := True;

    end else
      result := False;

  end;

end;


procedure TobjSpedFiscal.teste;

begin





end;

procedure TobjSpedFiscal.abreQueryProdutos_inventario;
var
  dataInicial,dataFinal:string;
begin

  dataInicial :=troca(self.dataIni,'/','.');
  dataFinal   :=troca(self.DATAFIN,'/','.');

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;

  self.objquery.SQL.Add('select item.item,p.unidade,p.estoque,p.custo_consumidor,(p.custo_consumidor*estoque) as valorTotalItem,');
  self.objquery.SQL.Add('indicadorpropriedade,item.codigoparticipante,movimento,mes');

  self.objquery.SQL.Add('from tabmovimento_item item');
  self.objquery.SQL.Add('join tabproduto p on p.codigo = item.item');
  self.objquery.SQL.Add('where item.mes ='+FormatDateTime('mm',StrToDate(self.DATAINI)));

  {InputBox('','',self.objquery.SQL.Text);}

  self.objquery.Active:=True;
  self.objquery.First;

end;

function TobjSpedFiscal.get_posseItem(pIndicadorPropriedade: integer): TACBrPosseItem;
begin

  case (pIndicadorPropriedade) of
    0:result:=piInformante;
    1:result:=piInformanteNoTerceiro;
    2:result:=piTerceiroNoInformante;
  end

end;

procedure TobjSpedFiscal.validaDados(memo:TMemo);
begin

  strERROS.Clear;
  strAdvertencia.Clear;

  self.validaBloco_0();
  self.validaBloco_C();
  self.validaBloco_E();

  self.validaCFOPSaida();
  self.validaCFOPEntrada();
  self.validaCSOSN_CST();

  self.carregaErrosMemo();
  Application.ProcessMessages;

end;

{VALIDA REGISTROS DO BLOCO 0}

procedure TobjSpedFiscal.validaBloco_0();
begin

  Self.status.Panels[1].Text:='Validando dados do bloco 0 registro 0000...';
  self.validaRegistro_0000();

  Self.status.Panels[1].Text:='Validando dados do bloco 0 registro 0005...';
  self.validaRegistro_0005();

  Self.status.Panels[1].Text:='Validando dados do bloco 0 registro 0100...';
  self.validaRegistro_0100();

  Self.status.Panels[1].Text:='Validando dados do bloco 0 registro 0150...';
  self.validaRegistro_0150();

  Self.status.Panels[1].Text:='Validando dados do bloco 0 registro 0190...';
  Self.validaRegistro_0190();

  Self.status.Panels[1].Text:='Validando dados do bloco 0 registro 0200...';
  self.validaRegistro_0200();

  Self.status.Panels[1].Text:='Validando dados do bloco 0 registro 0400...';
  self.validaRegistro_0400();

end;

procedure TobjSpedFiscal.validaRegistro_0000;
begin

  Application.ProcessMessages;

  if (ObjEmpresaGlobal.Get_RAZAOSOCIAL = '') then
    strERROS.Add('Nome empresarial da entidade n�o informado');

  if (Length(ObjEmpresaGlobal.Get_RAZAOSOCIAL) > 100) then
    strAdvertencia.Add('Nome empresaria da entidade ultrapassa o limite reservado');

  if (ObjEmpresaGlobal.Get_CNPJ = '') then
    strERROS.Add('N�mero de inscri��o da empresa no CNPJ n�o informado');

  if not (ValidaCNPJ(ObjEmpresaGlobal.Get_CNPJ)) then
    strERROS.Add('N�mero de inscri��o da empresa no CNPJ inv�lido');

  if (ObjEmpresaGlobal.Get_ESTADO = '') then
    strERROS.Add('Sigla da unidade da federa��o da empresa n�o informada');

  if (ObjEmpresaGlobal.get_codigoCidade = '') then
    strERROS.Add('C�digo do munic�pio do domic�lio fiscal da empresa n�o informado');

end;

procedure TobjSpedFiscal.validaRegistro_0005();
begin

  Application.ProcessMessages;

  if (ObjEmpresaGlobal.Get_FANTASIA = '') then
    strERROS.Add('Nome de fantasia associado ao nome empresarial n�o informado');

  if (Length(ObjEmpresaGlobal.Get_FANTASIA) > 60) then
    strAdvertencia.Add('Nome de fantasia associado ao nome empresarial ultrapassa o limite reservado. Limite(60)');

  if (ObjEmpresaGlobal.Get_CEP = '') then
    strERROS.Add('C�digo de endere�amento postal da empresa n�o informado');

  if (ObjEmpresaGlobal.Get_ENDERECO = '') then
    strERROS.Add('Logradouro e endere�o da empresa n�o informado');

  if (ObjEmpresaGlobal.get_bairro = '') then
    strERROS.Add('Bairro em que a empresa est� situado n�o informado');

  if (ObjEmpresaGlobal.Get_FONE <> '') then
  begin

    if (length(Trim(RetornaSoNumeros(ObjEmpresaGlobal.Get_FONE))) <> 10) then
      strERROS.Add('Fone da empresa inv�lido. Formato v�lido: DDD+FONE (ex: 6734245874)');

  end;

end;

procedure TobjSpedFiscal.validaRegistro_0100();
begin

  Application.ProcessMessages;

  if (objContador.Get_NOME = '') then
    strERROS.Add('Nome do contabilista n�o informado');

  if (objContador.Get_CPF = '') then
    strERROS.Add('N�mero de inscri��o do contabilista no CPF n�o informado');

  if (Length(RetornaSoNumeros(objContador.Get_CPF)) <> 11) then
    strERROS.add('N�mero de inscri��o do contabilista no CPF inv�lido');

  if (objContador.Get_CRC = '') then
    strERROS.Add('N�mero de inscri��o do contalibista no Conselho Regional de Contabilidade n�o informado');

  if (Length(objContador.Get_ENDERECO) > 60) then
    strAdvertencia.Add('Logradouro e endere�o do im�vel do contador respons�vel ultrapassa o limite reservado. Limite(60)')

end;

procedure TobjSpedFiscal.validaRegistro_0150();
begin

  Application.ProcessMessages;

  self.abreQueryCliente;
  while not (self.objquery.Eof) do
  begin

    Application.ProcessMessages;

    if (self.objquery.FieldByName('nome').AsString = '') then
      strERROS.Add('Nome do cliente n�o informado. Cliente c�digo: '+self.objquery.fieldbyname('codigo').AsString);

    if (self.objquery.FieldByName('codigoPais').AsString = '') then
      self.strERROS.Add('C�digo do pa�s do cliente n�o informado. Cliente c�digo: '+self.objquery.fieldbyname('codigo').AsString);

    if (self.objquery.FieldByName('endereco').AsString = '') then
      self.strERROS.Add('Logradouro e endere�o do cliente n�o informado. Cliente c�digo: '+self.objquery.fieldbyname('codigo').AsString);

    if (self.objquery.FieldByName('codigoCidade').AsString = '') then
      self.strERROS.Add('C�digo da cidade n�o informado. Cliente c�digo: '+self.objquery.fieldbyname('codigo').AsString);


    if Trim(objquery.FieldByName('CPF_CGC').AsString) <> '' then
    begin

      if (objquery.FieldByName('fisica_juridica').AsString = 'F') then
        begin

          if (Length(RetornaSoNumeros(objquery.FieldByName('CPF_CGC').AsString)) <> 11) then
            self.strERROS.Add('Campo CPF_CGC inv�lido para cliente tipo pessoa Fisica. Cliente c�digo: '+objquery.fieldbyname('codigo').AsString);

        end
      else if (objquery.FieldByName('fisica_juridica').AsString = 'J') then
      begin

        if (Length(RetornaSoNumeros(objquery.FieldByName('CPF_CGC').AsString)) <> 14) then
            self.strERROS.Add('Campo CPF_CGC inv�lido para cliente tipo pessoa Juridica. Cliente c�digo: '+objquery.fieldbyname('codigo').AsString);

      end;
      
    end;


    self.objquery.Next;
  end;

  Self.abreQueryFornecedorSaida;
  while not (self.objquery.Eof) do
  begin

    Application.ProcessMessages;

    if (self.objquery.FieldByName('razaosocial').AsString = '') then
      strERROS.Add('Nome do fornecedor n�o informado. Fornecedor c�digo: '+self.objquery.fieldbyname('codigo').AsString);

    if (self.objquery.FieldByName('codigopais').AsString = '') then
      self.strERROS.Add('C�digo do pa�s do fornecedor n�o informado. Fornecedor c�digo: '+self.objquery.fieldbyname('codigo').AsString);

    if (self.objquery.FieldByName('endereco').AsString = '') then
      self.strERROS.Add('Logradouro e endere�o do fornecedor n�o informado. Fornecedor c�digo: '+self.objquery.fieldbyname('codigo').AsString);

    if (self.objquery.FieldByName('codigoCidade').AsString = '') then
      self.strERROS.Add('C�digo da cidade n�o informado. Fornecedor c�digo: '+self.objquery.fieldbyname('codigo').AsString);

    self.objquery.Next;
  end;



  self.abreQueryFornecedorEntrada;
  while not (self.objquery.Eof) do
  begin

    Application.ProcessMessages;

    if (self.objquery.FieldByName('razaosocial').AsString = '') then
      strERROS.Add('Nome do fornecedor n�o informado. Fornecedor c�digo: '+self.objquery.fieldbyname('codigo').AsString);

    if (self.objquery.FieldByName('codigopais').AsString = '') then
      self.strERROS.Add('C�digo do pa�s do fornecedor n�o informado. Fornecedor c�digo: '+self.objquery.fieldbyname('codigo').AsString);

    if (self.objquery.FieldByName('endereco').AsString = '') then
      self.strERROS.Add('Logradouro e endere�o do fornecedor n�o informado. Fornecedor c�digo: '+self.objquery.fieldbyname('codigo').AsString);

    if (self.objquery.FieldByName('codigoCidade').AsString = '') then
      self.strERROS.Add('C�digo da cidade n�o informado. Fornecedor c�digo: '+self.objquery.fieldbyname('codigo').AsString);

    self.objquery.Next;
  end;


end;

procedure TobjSpedFiscal.validaRegistro_0190();
var
  unidade,descricao:string;
begin

  Application.ProcessMessages;

  self.abreQueryUnidadeMedidaSaida;
  while not (self.objquery.Eof) do
  begin

    Application.ProcessMessages;

    unidade:=self.objquery.FieldByName('Unidade').AsString;
    descricao:=get_campoTabela('descricao','sigla','TABUNIDADEMEDIDA',unidade);

    if (descricao = '') then
      strERROS.Add('Descri��o da unidade de medida n�o informada. Unidade sigla: '+unidade);


    self.objquery.Next;
  end;

  self.abreQueryUnidadeMedidaEntrada;
  while not (self.objquery.Eof) do
  begin

    Application.ProcessMessages;

    unidade:=self.objquery.FieldByName('Unidade').AsString;
    descricao:=get_campoTabela('descricao','sigla','TABUNIDADEMEDIDA',unidade);

    if (descricao = '') then
      strERROS.Add('Descri��o da unidade de medida n�o informada. Unidade sigla: '+unidade);


    self.objquery.Next;
  end;

end;

procedure TobjSpedFiscal.validaRegistro_0200();
begin

  Application.ProcessMessages;

  self.abreQueryProdutosSaida;
  while not (self.objquery.Eof) do
  begin

    Application.ProcessMessages;

    if (RetornaSoNumeros(objquery.FieldByName('ncm').AsString) = '') then
      strERROS.Add('NCM do item n�o informado. Item de c�digo: '+self.objquery.fieldbyname('codigomask').AsString+self.objquery.fieldbyname('material_codigo').AsString);

    if (Length (RetornaSoNumeros (objquery.FieldByName('ncm').AsString)) > 8) then
      strERROS.Add('NCM do item invalido (Tamanho maior que 8). Item de c�digo: '+self.objquery.fieldbyname('codigomask').AsString+self.objquery.fieldbyname('material_codigo').AsString);

    if (self.objquery.FieldByName('nomematerial').AsString = '') then
      strERROS.Add('Descri��o do item n�o informada. Item de c�digo: '+self.objquery.fieldbyname('codigomask').AsString+self.objquery.fieldbyname('material_codigo').AsString);

    if (self.objquery.FieldByName('unidade').AsString = '') then
      strERROS.Add('Unidade de medida do item n�o informada. Item de c�digo: '+self.objquery.fieldbyname('codigomask').AsString+self.objquery.fieldbyname('material_codigo').AsString);

    self.objquery.Next;

  end;

  self.abreQueryProdutosEntrada;
  while not (self.objquery.Eof) do
  begin

    Application.ProcessMessages;

    if (self.objquery.FieldByName('ncm').AsString = '') then
      strERROS.Add('NCM do item n�o informado. Item de c�digo: '+self.objquery.fieldbyname('material').AsString+self.objquery.fieldbyname('codigomaterial').AsString);

    if (Length (RetornaSoNumeros (objquery.FieldByName('ncm').AsString)) > 8) then
      strERROS.Add('NCM do item invalido (Tamanho maior que 8). Item de c�digo: '+self.objquery.fieldbyname('material').AsString+self.objquery.fieldbyname('codigomaterial').AsString);

    if (self.objquery.FieldByName('nomematerial').AsString = '') then
      strERROS.Add('Descri��o do item n�o informada. Item de c�digo: '+self.objquery.fieldbyname('material').AsString+self.objquery.fieldbyname('codigomaterial').AsString);

    if (self.objquery.FieldByName('unidade').AsString = '') then
      strERROS.Add('Unidade de medida do item n�o informada. Item de c�digo: '+self.objquery.fieldbyname('material').AsString+self.objquery.fieldbyname('codigomaterial').AsString);

    self.objquery.Next;

  end;

end;

procedure TobjSpedFiscal.validaRegistro_0400();
begin

  Application.ProcessMessages;

  self.abreQueryNaturezaOperacaoSaida;
  while not (self.objquery.Eof) do
  begin

    Application.ProcessMessages;

    if (self.objquery.FieldByName('nome').AsString = '') then
      strERROS.Add('Natureza de opera��o do CFOP '+self.objquery.FieldByName('codigo').AsString+' n�o informada');

    self.objquery.Next;

  end;



end;

{VALIDA REGISTROS DO BLOCO C}

procedure TobjSpedFiscal.validaBloco_C;
begin

  Self.status.Panels[1].Text:='Validando dados do bloco C registro C100 e C170...';
  self.validaBloco_C100_C170();



end;

procedure TobjSpedFiscal.validaBloco_C100_C170;
var
  VL_NF,VL_PROD,BC_ICMS_PROD,BC_ICMS_NF,VL_ICMS_PROD,VL_ICMS_NF:Currency;
begin

  Application.ProcessMessages;

  {SAIDA}
  Self.queryNFSaida.First;
  while not (self.queryNFSaida.Eof) do
  begin

    Application.ProcessMessages;


    {BC ICMS DO PRODUTO}

    self.objquery.Close;
    self.objquery.SQL.Clear;
    self.objquery.SQL.Add('select SUM(pm_nf.valorfinal - (pm_nf.reducaobasecalculo * pm_nf.valorfinal/100)) as BC_ICMS');
    self.objquery.SQL.Add('from proc_materiais_nf pm_nf');
    self.objquery.SQL.Add('where pm_nf.notafiscal='+self.queryNFSaida.fieldbyname('codigo').AsString+' and pm_nf.aliquota > 0 and pm_nf.substituicaotributaria = ''N''');
    self.objquery.Active:=True;
    BC_ICMS_PROD:=StrToCurrDef(self.objquery.fieldbyname('BC_ICMS').AsString,0.00);
    BC_ICMS_PROD:= StrToCurrDef(tira_ponto(formata_valor(BC_ICMS_PROD)),0.00);


    {BC ICMS DA NOTA}
    BC_ICMS_NF:=StrToCurrDef(tira_ponto(formata_valor(self.queryNFSaida.FieldByName('basecalculoicms').AsCurrency)),0.00);

    if (BC_ICMS_NF > 0) then
    begin

      if (BC_ICMS_PROD <> BC_ICMS_NF) then
        strERROS.Add('Base de calculo ICMS da nota de saida: '+self.queryNFSaida.fieldbyname('codigo').AsString+' difere do somatorio dos itens. BC_ICMS da nota: '+CurrToStr(BC_ICMS_NF)+' BC_ICMS dos itens: '+CurrToStr(BC_ICMS_PROD));

    end;

    {VL ICMS}

    self.objquery.Close;
    self.objquery.SQL.Clear;
    {self.objquery.SQL.Add('select sum((valorfinal - (reducaobasecalculo * valorfinal/100)) * aliquota / 100) as VL_ICMS_PROD');}
    self.objquery.SQL.Add('select sum(valor_icms) as VL_ICMS_PROD');
    self.objquery.SQL.Add('from proc_materiais_nf');
    self.objquery.SQL.Add('where notafiscal='+self.queryNFSaida.fieldbyname('codigo').AsString+' and aliquota > 0 and substituicaotributaria = ''N''');

    self.objquery.Active:=True;

    VL_ICMS_PROD:=StrToCurrDef(self.objquery.fieldbyname('VL_ICMS_PROD').AsString,0.00);
    VL_ICMS_PROD:=StrToCurrDef(tira_ponto(formata_valor(VL_ICMS_PROD)),0.00);

    VL_ICMS_NF:=StrToCurrDef(tira_ponto(formata_valor(self.queryNFSaida.FieldByName('valoricms').AsCurrency)),0.00);

    if (VL_ICMS_NF > 0) then
    begin

      if (VL_ICMS_PROD <> VL_ICMS_NF) then
        strERROS.Add('Valor do ICMS da nota de saida: '+self.queryNFSaida.fieldbyname('codigo').AsString+' difere do somatorio dos itens. VL_ICMS da nota: '+CurrToStr(VL_ICMS_NF)+' VL_ICMS dos itens: '+CurrToStr(VL_ICMS_PROD));

    end;

    {verificar o total dos descontos}

    self.queryNFSaida.Next;

  end;


  {ENTRADA}
  self.queryNFEntrada.First;
  while not (self.queryNFEntrada.Eof) do
  begin

    VL_PROD:= StrToCurrDef (tira_ponto(get_campoTabela('SUM(valorfinal) as VL_PROD','entrada','proc_materiais_ep',queryNFEntrada.fieldbyname('codigo').AsString,'VL_PROD')),0);
    VL_NF  := queryNFEntrada.FieldByName('valorfinal').AsCurrency;

    if not (VL_NF >= VL_PROD) then
      strERROS.Add('O valor da nota deve ser maior ou igual � soma dos valores dos itens (valor inserido). Entrada de c�digo: '+querynfentrada.fieldbyname('codigo').AsString);

    queryNFEntrada.Next;

  end;      


  {ENTRADA}
  queryNFEntrada.First;
  while not (queryNFEntrada.Eof) do
  begin

    status.Panels[1].Text:='Validando registro C100 C170 de entrada. Entrada: '+queryNFEntrada.fieldbyname('codigo').AsString;
    Application.ProcessMessages;

    self.abreItensNFEntrada(queryNFEntrada.fieldbyname('codigo').AsString);
    if (objquery.RecordCount = 0) then
      strERROS.Add('A NF de entrada: '+queryNFEntrada.fieldbyname('nf').AsString+' n�o possui itens cadastrados. Entrada('+queryNFEntrada.fieldbyname('codigo').AsString+')');

    queryNFEntrada.Next;

  end;


  (*ENTRADA}
  {self.queryNFEntrada.First;
  while not (self.queryNFEntrada.Eof) do
  begin

    Application.ProcessMessages;


    if (queryNFEntrada.FieldByName('emissaonf').AsDateTime > StrToDate(self.DATAFIN)) then
      strERROS.Add('A data de emiss�o da NF de entrada: '+queryNFEntrada.FieldByName('nf').AsString+' deve ser menor ou igual ao periodo final da apura��o: '+self.DATAFIN);

    if (queryNFEntrada.FieldByName('data').AsDateTime > StrToDate(self.DATAFIN)) then
      strERROS.Add('A data de entrada da NF de entrada: '+queryNFEntrada.FieldByName('nf').AsString+' deve ser menor ou igual ao periodo final da apura��o: '+self.DATAFIN);


    {BC_ICMS}
    BC_ICMS_PROD := StrToCurrDef(get_campoTabela('SUM(basecalculoicms) as BC_ICMS','entrada','TABPRODUTOSENTRADAPRODUTOS',self.queryNFEntrada.fieldbyname('codigo').AsString,'BC_ICMS'),0.00);
    BC_ICMS_PROD := StrToCurrDef(tira_ponto(formata_valor(BC_ICMS_PROD)),0.00);

    BC_ICMS_NF := StrToCurrDef(tira_ponto(formata_valor(self.queryNFEntrada.fieldbyname('basecalculoicms').AsCurrency)),0.00);

    if (BC_ICMS_PROD <> BC_ICMS_NF) then
      strERROS.Add('Base de calculo ICMS da nota de entrada: '+self.queryNFEntrada.fieldbyname('codigo').AsString+' difere do somatorio dos itens. BC_ICMS da nota: '+CurrToStr(BC_ICMS_NF)+' BC_ICMS dos itens: '+CurrToStr(BC_ICMS_PROD));

    {VL_ICMS}
    VL_ICMS_PROD :=StrToCurrDef(get_campoTabela('SUM(valoricms) as VL_ICMS','entrada','TABPRODUTOSENTRADAPRODUTOS',self.queryNFEntrada.fieldbyname('codigo').AsString,'VL_ICMS'),0.00);
    VL_ICMS_PROD :=StrToCurrDef(tira_ponto(formata_valor(VL_ICMS_PROD)),0.00);

    VL_ICMS_NF :=StrToCurrDef(tira_ponto(formata_valor(self.queryNFEntrada.FieldByName('valoricms').AsCurrency)),0.00);

    if (VL_ICMS_PROD <> VL_ICMS_NF) then
      strERROS.Add('Valor do ICMS da nota de entrada: '+self.queryNFEntrada.fieldbyname('codigo').AsString+' difere do somatorio dos itens. VL_ICMS da nota: '+CurrToStr(VL_ICMS_NF)+' VL_ICMS dos itens: '+CurrToStr(VL_ICMS_PROD));


    {verificar o total dos descontos}

    self.queryNFEntrada.Next;

  end;*)

  {BC ICMS}


end;

procedure TobjSpedFiscal.validaBloco_H();
begin


end;

procedure TobjSpedFiscal.validaRegistro_H005_H010();
begin



end;

procedure TobjSpedFiscal.validaCFOPSaida();
begin

  self.status.Panels[1].Text:='Validando CFOP de saida...';
  Application.ProcessMessages;

  self.queryNFSaida.First;
  while not (self.queryNFSaida.Eof) do
  begin

    Self.abreItensNFSaida(self.queryNFSaida.fieldbyname('codigo').AsString);
    while not (Self.objquery.Eof) do
    begin

      if (self.objquery.FieldByName('cfop').AsString <> '') then
      begin

        if (StrToInt(self.objquery.FieldByName('cfop').AsString[1]) IN[1..3]) then
          strERROS.Add('A nota fiscal de saida n�mero: '+self.queryNFSaida.fieldbyname('numero').AsString+' cont�m um CFOP inv�lido. CFOP: '+self.objquery.fieldbyname('cfop').AsString);

      end else
        strERROS.Add('A nota fiscal de saida n�mero: '+self.queryNFSaida.fieldbyname('numero').AsString+' Produto: '+self.objquery.fieldbyname('codigomaterial').AsString+' material: '+self.objquery.fieldbyname('material').AsString+' n�o possui um CFOP');

      self.objquery.Next;

    end;

    self.queryNFSaida.Next;

  end;

  {cfop de entrada 1 2 ou 3}

end;

procedure TobjSpedFiscal.validaCFOPEntrada();
begin

  self.status.Panels[1].Text:='Validando CFOP de Entrada...';
  Application.ProcessMessages;

  self.queryNFEntrada.First;
  while not (self.queryNFEntrada.Eof) do
  begin

    Self.abreItensNFEntrada(self.queryNFEntrada.fieldbyname('codigo').AsString);
    while not (Self.objquery.Eof) do
    begin

      if (self.objquery.FieldByName('cfop').AsString <> '') then
      begin

        if (StrToInt(self.objquery.FieldByName('cfop').AsString[1]) IN[5..7]) then
          strERROS.Add('A nota fiscal de entrada n�mero: '+self.queryNFEntrada.fieldbyname('nf').AsString+' cont�m um CFOP inv�lido. CFOP: '+self.objquery.fieldbyname('cfop').AsString);

      end else
        strERROS.Add('A nota fiscal de entrada n�mero: '+self.queryNFEntrada.fieldbyname('nf').AsString+' Produto: '+self.objquery.fieldbyname('codigomaterial').AsString+' Material: '+self.objquery.fieldbyname('Material').AsString+' n�o possui um CFOP');

      self.objquery.Next;

    end;

    self.queryNFEntrada.Next;

  end;

  {cfop de saida 5 6 ou 7}

end;

procedure TobjSpedFiscal.validaCSOSN_CST();
begin

  self.queryNFSaida.First;
  while not (self.queryNFSaida.Eof) do
  begin

    self.abreItensNFSaida(self.queryNFSaida.fieldbyname('codigo').AsString);
    while not (self.objquery.Eof) do
    begin

      if (objquery.FieldByName('cstb').AsString <> '') and (objquery.FieldByName('csosn').AsString <> '') then
        strERROS.Add('A nota fiscal de saida n�mero: '+self.queryNFSaida.fieldbyname('numero').AsString+' possui csosn e cst ao mesmo tempo. CSOSN: '+objquery.FieldByName('csosn').AsString+' CST: '+objquery.FieldByName('sttabelab').AsString+'. Produto: '+self.objquery.fieldbyname('codigomaterial').AsString+'. Material: '+self.objquery.fieldbyname('material').AsString)

      else if (objquery.FieldByName('cstb').AsString = '') and (objquery.FieldByName('csosn').AsString = '') then
        strERROS.Add('A nota fiscal de saida n�mero: '+self.queryNFSaida.fieldbyname('numero').AsString+' esta com o campo CSOSN e CST vazio. Produto: '+self.objquery.fieldbyname('codigomaterial').AsString+'. Material: '+self.objquery.fieldbyname('material').AsString);

      self.objquery.Next;

    end;

    self.queryNFSaida.Next;

  end;

  self.queryNFEntrada.First;
  while not (self.queryNFEntrada.Eof) do
  begin

    self.abreItensNFEntrada(self.queryNFEntrada.fieldbyname('codigo').AsString);
    while not (self.objquery.Eof) do
    begin

      if (objquery.FieldByName('cstb').AsString <> '') and (objquery.FieldByName('csosn').AsString <> '') then
        strERROS.Add('A nota fiscal de entrada n�mero: '+self.queryNFEntrada.fieldbyname('nf').AsString+' possui csosn e cst ao mesmo tempo. CSOSN: '+objquery.FieldByName('csosn').AsString+' CST: '+objquery.FieldByName('cstb').AsString+'. Produto: '+self.objquery.fieldbyname('codigomaterial').AsString+'. Material: '+self.objquery.fieldbyname('material').AsString)

      else if (objquery.FieldByName('cstb').AsString = '') and (objquery.FieldByName('csosn').AsString = '') then
        strERROS.Add('A nota fiscal de entrada n�mero: '+self.queryNFEntrada.fieldbyname('nf').AsString+' esta com o campo CSOSN e CST vazio. Produto da nota: '+self.objquery.fieldbyname('codigomaterial').AsString+'. Material: '+self.objquery.fieldbyname('material').AsString);

      self.objquery.Next;

    end;

    self.queryNFEntrada.Next;

  end;


end;

{procedure TobjSpedFiscal.carregaErrosMemo(memo: TMemo);
begin

  self.carregaErros(memo,strERROS);
  self.carregaErros(memo,strAdvertencia);

end;}

procedure TobjSpedFiscal.carregaErrosMemo();
begin
  self.carregaErros(strERROS);
  self.carregaErros(strAdvertencia);
end;

{procedure TobjSpedFiscal.carregaErros(memo:TMemo;str: TStringList);
var
  i:integer;
begin

  for i := 0  to str.Count-1  do
    memo.Lines.Add(str[i]);

end; }

procedure TobjSpedFiscal.carregaErros(str: TStringList);
var
  i:integer;
begin

  for i := 0  to str.Count-1  do
    memoErros.Lines.Add(str[i]);

end;

procedure TobjSpedFiscal.addMessageERRO(pErro: string);
begin

  msgERRO:=msgERRO+pErro+#13;

end;


procedure TobjSpedFiscal.limpaRegistros;
begin

  with (self.ComponentesSped.ACBrSPEDFiscal1) do
  begin

    Bloco_0.LimpaRegistros;
    Bloco_C.LimpaRegistros;
    Bloco_D.LimpaRegistros;
    Bloco_E.LimpaRegistros;
    Bloco_G.LimpaRegistros;
    Bloco_H.LimpaRegistros;
    Bloco_1.LimpaRegistros;
    Bloco_9.LimpaRegistros;

    Bloco_0.Gravado:=False;
    Bloco_C.Gravado:=False;
    Bloco_D.Gravado:=False;
    Bloco_E.Gravado:=False;
    Bloco_G.Gravado:=False;
    Bloco_H.Gravado:=False;
    Bloco_1.Gravado:=False;
    Bloco_9.Gravado:=False;

  end;

end;


procedure TobjSpedFiscal.criarTreeView(treeView:TTreeView);
var
  noRaiz,noPai,noUltimo:TTreeNode;
begin

  Application.ProcessMessages;

  treeView.Items.Clear;

  {NOTAS FISCAL DE SAIDA}

  treeView.Items.AddChild(nil,'Emiss�o Pr�pria');
  noRaiz:=treeView.Items.GetFirstNode;
  noPai:=noRaiz;
  noUltimo:=noPai;
  noPai.ImageIndex:=9;


  treeView.Items.AddChild(noRaiz,'NF-e, modelo 55');
  noUltimo:=noUltimo.GetNext;
  noPai:=noUltimo;
  noPai.ImageIndex:=8;

  try

      self.queryNFSaida.First;
      while not (self.queryNFSaida.Eof) do
      begin

        if (queryNFSaida.FieldByName('codigo_fiscal').AsInteger = 55) then
        begin

          self.status.Panels[1].Text:='Carregando nota de saida: '+queryNFSaida.fieldbyname('numero').AsString+'...';
          Application.ProcessMessages;

          treeView.Items.AddChild(noPai,queryNFSaida.fieldbyname('numero').AsString);
          noUltimo:=noUltimo.GetNext;
          noUltimo.ImageIndex:=8;

        end;

        self.queryNFSaida.Next;

      end;

      self.status.Panels[1].Text:='Carregando notas modelo 1...';
      Application.ProcessMessages;

      treeView.Items.AddChild(noRaiz,'NF, modelo 1');
      noUltimo:=noUltimo.GetNext;
      noPai:=noUltimo;
      noPai.ImageIndex:=8;

      self.queryNFSaida.First;
      while not (self.queryNFSaida.Eof) do
      begin

        if (queryNFSaida.FieldByName('codigo_fiscal').AsInteger = 1) then
        begin

          self.status.Panels[1].Text:='Carregando nota de saida: '+queryNFSaida.fieldbyname('numero').AsString+'...';
          Application.ProcessMessages;

          treeView.Items.AddChild(noPai,queryNFSaida.fieldbyname('numero').AsString);
          noUltimo:=noUltimo.GetNext;
          noUltimo.ImageIndex:=8;

        end;

        self.queryNFSaida.Next;

      end;

      self.status.Panels[1].Text:='Carregando notas modelo 4...';
      Application.ProcessMessages;

      treeView.Items.AddChild(noRaiz,'NF de produtor, modelo 4');
      noUltimo:=noUltimo.GetNext;
      noPai:=noUltimo;
      noPai.ImageIndex:=8;

      self.queryNFSaida.First;
      while not (self.queryNFSaida.Eof) do
      begin

        if (queryNFSaida.FieldByName('codigo_fiscal').AsInteger = 4) then
        begin

          self.status.Panels[1].Text:='Carregando nota de saida: '+queryNFSaida.fieldbyname('numero').AsString+'...';
          Application.ProcessMessages;

          treeView.Items.AddChild(noPai,queryNFSaida.fieldbyname('numero').AsString);
          noUltimo:=noUltimo.GetNext;
          noUltimo.ImageIndex:=8;

        end;

        self.queryNFSaida.Next;

      end;

      {NOTAS FISCAL DE ENTRADA}

      treeView.Items.AddChild(nil,'Emiss�o de Terceiros');
      noUltimo:=noUltimo.GetNext;
      noRaiz:=noUltimo;
      noRaiz.ImageIndex:=9;

      treeView.Items.AddChild(noRaiz,'NF-e, modelo 55');
      noUltimo:=noUltimo.GetNext;
      noPai:=noUltimo;
      noPai.ImageIndex:=8;

      self.status.Panels[1].Text:='Carregando nota modelo 1...';
      Application.ProcessMessages;

      self.queryNFEntrada.First;
      while not (self.queryNFEntrada.Eof) do
      begin

        if (queryNFEntrada.FieldByName('codigo_fiscal').AsString = '') then
        begin

          MensagemAviso('A entrada de c�digo: '+queryNFEntrada.FieldByName('codigo').AsString+' n�o possui um modelonf. O processo ser� cancelado');
          Exit;

        end;

        if (queryNFEntrada.FieldByName('codigo_fiscal').AsInteger = 55) then
        begin

          self.status.Panels[1].Text:='Carregando nota de entrada: '+queryNFEntrada.fieldbyname('nf').AsString+'...';
          Application.ProcessMessages;

          treeView.Items.AddChild(noPai,queryNFEntrada.fieldbyname('nf').AsString);
          noUltimo:=noUltimo.GetNext;
          noUltimo.ImageIndex:=8;

        end;

        self.queryNFEntrada.Next;

      end;

      treeView.Items.AddChild(noRaiz,'NF, modelo 1');
      noUltimo:=noUltimo.GetNext;
      noPai:=noUltimo;
      noPai.ImageIndex:=8;

      self.queryNFEntrada.First;
      while not (self.queryNFEntrada.Eof) do
      begin

        if (queryNFEntrada.FieldByName('codigo_fiscal').AsString = '') then
        begin

          MensagemAviso('A entrada de c�digo: '+queryNFEntrada.FieldByName('codigo').AsString+' n�o possui um modelonf. O processo ser� cancelado');
          Exit;

        end;

        if (queryNFEntrada.FieldByName('codigo_fiscal').AsInteger = 1) then
        begin

          self.status.Panels[1].Text:='Carregando nota de entrada: '+queryNFEntrada.fieldbyname('nf').AsString+'...';
          Application.ProcessMessages;

          treeView.Items.AddChild(noPai,queryNFEntrada.fieldbyname('nf').AsString);
          noUltimo:=noUltimo.GetNext;
          noUltimo.ImageIndex:=8;

        end;



        self.queryNFEntrada.Next;

      end;

      treeView.Items.AddChild(noRaiz,'NF de produtor, modelo 4');
      noUltimo:=noUltimo.GetNext;
      noPai:=noUltimo;
      noPai.ImageIndex:=8;

      self.queryNFEntrada.First;
      while not (self.queryNFEntrada.Eof) do
      begin

        if (queryNFEntrada.FieldByName('codigo_fiscal').AsString = '') then
        begin

          MensagemAviso('A entrada de c�digo: '+queryNFEntrada.FieldByName('codigo').AsString+' n�o possui um modelonf. O processo ser� cancelado');
          Exit;

        end;

        if (queryNFEntrada.FieldByName('codigo_fiscal').AsInteger = 4) then
        begin

          self.status.Panels[1].Text:='Carregando nota de saida: '+queryNFEntrada.fieldbyname('nf').AsString+'...';
          Application.ProcessMessages;

          treeView.Items.AddChild(noPai,queryNFEntrada.fieldbyname('nf').AsString);
          noUltimo:=noUltimo.GetNext;
          noUltimo.ImageIndex:=8;

        end;

        self.queryNFEntrada.Next;

      end;

  finally

    self.status.Panels[1].Text:='';
    Application.ProcessMessages;

  end;

end;

procedure TobjSpedFiscal.criaListViewProdutosSaida(pNota: string;list: TListView);
var
  Item: TListItem;
begin

  list.Items.Clear;

  if (pNota = '') then pNota:='-1';

  self.abreItensNFSaidaComProduto(pNota);
  while not (self.objquery.Eof) do
  begin

    item:=list.Items.Add;
    Item.Caption:=objquery.fieldbyname('codigomaterial').AsString;
    Item.SubItems.Add(           (objquery.fieldbyname('nomematerial').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('nomecor').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('ncm').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('cfop').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('unidade').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('quantidade').AsString));
    Item.SubItems.Add(get_valorF (objquery.fieldbyname('valor').AsString));
    Item.SubItems.Add(get_valorF (objquery.fieldbyname('valorFinal').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('csta').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('cstb').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('csosn').AsString));


    self.objquery.Next;

  end;

end;

procedure TobjSpedFiscal.criaListViewEstoque(list: TListView);
var
  Item: TListItem;
  dataInicial,dataFinal,codigocor:string;
begin

  dataInicial :=troca(self.DATAINI_ESTOQUE,'/','.');
  dataFinal   :=troca(self.DATAFIN_ESTOQUE,'/','.');

  list.Items.Clear;

  self.objquery.Close;
  self.objquery.SQL.Clear;

  //BUSCO PRIMEIRO AS FERRAGENS
  self.objquery.SQL.Text:=
  'select distinct(codigomaterialcor),codigomaterial,nomematerial,nomecor '+
  'from procmovimentoestoque('+QuotedStr (dataInicial) + ',' + QuotedStr (dataFinal)+') '+
  'where material=''FER '' ' ;
  self.objquery.Active := True;
  self.objquery.First;
  while not (self.objquery.Eof) do
  begin
      item:=list.items.add;
      codigocor:=objquery.fieldbyname('codigomaterialcor').AsString;

      queryTemp.Active:=False;
      queryTemp.SQL.Clear;
      queryTemp.SQL.Text:=
      'select tabferragem.descricao,tabferragem.codigo,tabcor.descricao as cor,tabferragem.referencia, SUM(e.quantidade) quantidade '+
      'from tabestoque e '+
      'join tabferragemcor on tabferragemcor.codigo=e.ferragemcor '+
      'join tabferragem on tabferragem.codigo=tabferragemcor.ferragem '+
      'join tabcor on tabcor.codigo=tabferragemcor.cor '+
      'where (e.data <= '+QuotedStr (dataFinal)+') and (e.ferragemcor ='+codigocor+') '+
      'group by tabferragem.descricao,tabferragem.codigo,tabcor.descricao,tabferragem.referencia';

      queryTemp.active:=True;
      queryTemp.First;
      Item.Caption := queryTemp.fieldbyname ('referencia').AsString;
      Item.SubItems.Add ('FERRAGEM ');
      Item.SubItems.Add (queryTemp.fieldbyname ('descricao').AsString);
      Item.SubItems.Add (queryTemp.fieldbyname ('cor').AsString);
      Item.SubItems.Add (queryTemp.fieldbyname ('quantidade').AsString);
      self.objquery.Next;
  end;


  //PERFILADOS

  self.objquery.Close;
  self.objquery.SQL.Clear;
  self.objquery.SQL.Text:=
  'select distinct(codigomaterialcor),codigomaterial,nomematerial,nomecor '+
  'from procmovimentoestoque('+QuotedStr (dataInicial) + ',' + QuotedStr (dataFinal)+') '+
  'where material=''PER '' ' ;
  self.objquery.Active := True;
  self.objquery.First;
  while not (self.objquery.Eof) do
  begin
      item:=list.items.add;
      codigocor:=objquery.fieldbyname('codigomaterialcor').AsString;

      queryTemp.Active:=False;
      queryTemp.SQL.Clear;
      queryTemp.SQL.Text:=
      'select tabperfilado.descricao,tabperfilado.codigo,tabcor.descricao as cor,tabperfilado.referencia, SUM(e.quantidade) quantidade '+
      'from tabestoque e '+
      'join tabperfiladocor on tabperfiladocor.codigo=e.perfiladocor '+
      'join tabperfilado on tabperfilado.codigo=tabperfiladocor.perfilado '+
      'join tabcor on tabcor.codigo=tabperfiladocor.cor '+
      'where (e.data <= '+QuotedStr (dataFinal)+') and (e.perfiladocor ='+codigocor+') '+
      'group by tabperfilado.descricao,tabperfilado.codigo,tabcor.descricao,tabperfilado.referencia';

      queryTemp.active:=True;
      queryTemp.First;
      Item.Caption:=queryTemp.fieldbyname ('referencia').AsString;
      Item.SubItems.Add ('PERFILADO ');
      Item.SubItems.Add (queryTemp.fieldbyname ('descricao').AsString);
      Item.SubItems.Add (queryTemp.fieldbyname ('cor').AsString);
      Item.SubItems.Add (queryTemp.fieldbyname ('quantidade').AsString);
      self.objquery.Next;
  end;

  //VIDROS

  self.objquery.Close;
  self.objquery.SQL.Clear;
  self.objquery.SQL.Text:=
  'select distinct(codigomaterialcor),codigomaterial,nomematerial,nomecor '+
  'from procmovimentoestoque('+QuotedStr (dataInicial) + ',' + QuotedStr (dataFinal)+') '+
  'where material=''VID '' ' ;
  self.objquery.Active := True;
  self.objquery.First;
  while not (self.objquery.Eof) do
  begin
      item:=list.items.add;
      codigocor:=objquery.fieldbyname('codigomaterialcor').AsString;

      queryTemp.Active:=False;
      queryTemp.SQL.Clear;
      queryTemp.SQL.Text:=
      'select tabvidro.descricao,tabvidro.codigo,tabcor.descricao as cor,tabvidro.referencia, SUM(e.quantidade) quantidade '+
      'from tabestoque e '+
      'join tabvidrocor on tabvidrocor.codigo=e.vidrocor '+
      'join tabvidro on tabvidro.codigo=tabvidrocor.vidro '+
      'join tabcor on tabcor.codigo=tabvidrocor.cor '+
      'where (e.data <= '+QuotedStr (dataFinal)+') and (e.vidrocor ='+codigocor+') '+
      'group by tabvidro.descricao,tabvidro.codigo,tabcor.descricao,tabvidro.referencia';

      queryTemp.active:=True;
      queryTemp.First;
      Item.Caption:=queryTemp.fieldbyname ('referencia').AsString;
      Item.SubItems.Add ('VIDRO ');
      Item.SubItems.Add (queryTemp.fieldbyname ('descricao').AsString);
      Item.SubItems.Add (queryTemp.fieldbyname ('cor').AsString);
      Item.SubItems.Add (queryTemp.fieldbyname ('quantidade').AsString);
      self.objquery.Next;
  end;

  //DIVERSO

  self.objquery.Close;
  self.objquery.SQL.Clear;
  self.objquery.SQL.Text:=
  'select distinct(codigomaterialcor),codigomaterial,nomematerial,nomecor '+
  'from procmovimentoestoque('+QuotedStr (dataInicial) + ',' + QuotedStr (dataFinal)+') '+
  'where material=''DIV '' ' ;
  self.objquery.Active := True;
  self.objquery.First;
  while not (self.objquery.Eof) do
  begin
      item:=list.items.add;
      codigocor:=objquery.fieldbyname('codigomaterialcor').AsString;

      queryTemp.Active:=False;
      queryTemp.SQL.Clear;
      queryTemp.SQL.Text:=
      'select tabdiverso.descricao,tabdiverso.codigo,tabcor.descricao as cor,tabdiverso.referencia, SUM(e.quantidade) quantidade '+
      'from tabestoque e '+
      'join tabdiversocor on tabdiversocor.codigo=e.diversocor '+
      'join tabdiverso on tabdiverso.codigo=tabdiversocor.diverso '+
      'join tabcor on tabcor.codigo=tabdiversocor.cor '+
      'where (e.data <= '+QuotedStr (dataFinal)+') and (e.diversocor ='+codigocor+') '+
      'group by tabdiverso.descricao,tabdiverso.codigo,tabcor.descricao,tabdiverso.referencia';

      queryTemp.active:=True;
      queryTemp.First;
      Item.Caption:=queryTemp.fieldbyname ('referencia').AsString;
      Item.SubItems.Add ('DIVERSO ');
      Item.SubItems.Add (queryTemp.fieldbyname ('descricao').AsString);
      Item.SubItems.Add (queryTemp.fieldbyname ('cor').AsString);
      Item.SubItems.Add (queryTemp.fieldbyname ('quantidade').AsString);
      self.objquery.Next;
  end;

  //KITBOX
  self.objquery.Close;
  self.objquery.SQL.Clear;
  self.objquery.SQL.Text:=
  'select distinct(codigomaterialcor),codigomaterial,nomematerial,nomecor '+
  'from procmovimentoestoque('+QuotedStr (dataInicial) + ',' + QuotedStr (dataFinal)+') '+
  'where material=''KIT '' ' ;
  self.objquery.Active := True;
  self.objquery.First;
  while not (self.objquery.Eof) do
  begin
      item:=list.items.add;
      codigocor:=objquery.fieldbyname('codigomaterialcor').AsString;

      queryTemp.Active:=False;
      queryTemp.SQL.Clear;
      queryTemp.SQL.Text:=
      'select tabkitbox.descricao,tabkitbox.codigo,tabcor.descricao as cor,tabkitbox.referencia, SUM(e.quantidade) quantidade '+
      'from tabestoque e '+
      'join tabkitboxcor on tabkitboxcor.codigo=e.kitboxcor '+
      'join tabkitbox on tabkitbox.codigo=tabkitboxcor.kitbox '+
      'join tabcor on tabcor.codigo=tabkitboxcor.cor '+
      'where (e.data <= '+QuotedStr (dataFinal)+') and (e.kitboxcor ='+codigocor+') '+
      'group by tabkitbox.descricao,tabkitbox.codigo,tabcor.descricao,tabkitbox.referencia';

      queryTemp.active:=True;
      queryTemp.First;
      Item.Caption:=queryTemp.fieldbyname ('referencia').AsString;
      Item.SubItems.Add ('KITBOX ');
      Item.SubItems.Add (queryTemp.fieldbyname ('descricao').AsString);
      Item.SubItems.Add (queryTemp.fieldbyname ('cor').AsString);
      Item.SubItems.Add (queryTemp.fieldbyname ('quantidade').AsString);
      self.objquery.Next;
  end;

  //PERSIANA persiana
  self.objquery.Close;
  self.objquery.SQL.Clear;
  self.objquery.SQL.Text:=
  'select distinct(codigomaterialcor),codigomaterial,nomematerial,nomecor '+
  'from procmovimentoestoque('+QuotedStr (dataInicial) + ',' + QuotedStr (dataFinal)+') '+
  'where material=''KIT '' ' ;
  self.objquery.Active := True;
  self.objquery.First;
  while not (self.objquery.Eof) do
  begin
      item:=list.items.add;
      codigocor:=objquery.fieldbyname('codigomaterialcor').AsString;

      queryTemp.Active:=False;
      queryTemp.SQL.Clear;
      queryTemp.SQL.Text:=
      'select tabpersiana.nome,tabpersiana.codigo,tabcor.descricao as cor,tabpersiana.referencia, SUM(e.quantidade) quantidade '+
      'from tabestoque e '+
      'join tabpersianagrupodiametrocor on tabpersianagrupodiametrocor.codigo=e.persianagrupodiametrocor '+
      'join tabpersiana on tabpersiana.codigo=tabpersianagrupodiametrocor.persiana '+
      'join tabcor on tabcor.codigo=tabpersianagrupodiametrocor.cor '+
      'where (e.data <= '+QuotedStr (dataFinal)+') and (e.persianagrupodiametrocor ='+codigocor+') '+
      'group by tabpersiana.nome,tabpersiana.codigo,tabcor.descricao,tabpersiana.referencia';

      queryTemp.active:=True;
      queryTemp.First;
      Item.Caption:=queryTemp.fieldbyname ('referencia').AsString;
      Item.SubItems.Add ('PERSIANA ');
      Item.SubItems.Add (queryTemp.fieldbyname ('nome').AsString);
      Item.SubItems.Add (queryTemp.fieldbyname ('cor').AsString);
      Item.SubItems.Add (queryTemp.fieldbyname ('quantidade').AsString);
      self.objquery.Next;
  end;

end;


procedure TobjSpedFiscal.criaListViewProdutosEntrada(pNota: string; list: TListView);
var
  Item: TListItem;
begin

  list.Items.Clear;

   if (pNota = '') then pNota:='-1';

  self.abreItensNFEntrada(pNota);
  while not (self.objquery.Eof) do
  begin

    item:=list.Items.Add;
    Item.Caption:=objquery.fieldbyname('codigomaterial').AsString;
    Item.SubItems.Add(           (objquery.fieldbyname('nomematerial').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('nomecor').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('ncm').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('cfop').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('unidade').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('quantidade').AsString));
    Item.SubItems.Add(get_valorF (objquery.fieldbyname('valor').AsString));
    Item.SubItems.Add(get_valorF (objquery.fieldbyname('valorFinal').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('csta').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('cstb').AsString));
    Item.SubItems.Add(           (objquery.fieldbyname('csosn').AsString));


    self.objquery.Next;




  end;

end;


function TobjSpedFiscal.localizaNFSaida(pNumero,pCodigoFiscal: string): TIBQuery;
begin

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;
  self.objquery.SQL.Add('select nf.codigo,nf.numero,nf.dataemissao,nfe.chave_acesso,nfe.certificado,');
  self.objquery.SQL.Add('nfe.chave_acesso,nfe.arquivo_xml,nfe.reciboenvio,');
  self.objquery.SQL.Add('nf.situacao,nf.valorfinal,modelonf.codigo_fiscal,modelonf.modelo,');
  self.objquery.SQL.Add('nf.valortotal,nf.valorfrete,nf.basecalculoicms,nf.valoricms,nf.desconto,');

  {cliente}
  self.objquery.SQL.Add('c.codigo as codigo_cliente,c.nome as nome_cliente,c.fantasia as fantasia_cliente,c.cpf_cgc as cpf_cgc_cliente,');
  self.objquery.SQL.Add('c.endereco,c.bairro,c.cep,c.cidade,c.fone as fone_cliente,');
  self.objquery.SQL.Add('c.estado,c.rg_ie,');

  {fornecedor}
  self.objquery.SQL.Add('f.codigo as codigo_fornecedor,f.razaosocial,f.fantasia as fantasia_fornecedor,f.cgc as cgc_fornecedor,f.endereco as endereco_f,');
  self.objquery.SQL.Add('f.bairro as bairro_f,f.cep as cep_f,f.cidade as cidade_f,f.fone as fone_f, f.estado as estado_f, f.ie as ie_f,');

  {transportadora}
  self.objquery.SQL.Add('nf.nometransportadora,nf.freteporcontatransportadora,nf.placaveiculotransportadora, nf.ufveiculotransportadora,');
  self.objquery.SQL.Add('nf.cnpjtransportadora,nf.enderecotransportadora,nf.municipiotransportadora,nf.uftransportadora, nf.ietransportadora,');
  self.objquery.SQL.Add('nf.quantidade,nf.especie,nf.marca,nf.numerovolumes,nf.pesobruto,nf.pesoliquido');


  self.objquery.SQL.Add('from tabnotafiscal nf');

  self.objquery.SQL.Add('join tabmodelonf modelonf on modelonf.codigo = nf.modelo_nf');
  self.objquery.SQL.Add('left join tabnfe nfe on nfe.codigo = nf.nfe');
  self.objquery.SQL.Add('left join tabcliente c on c.codigo = nf.cliente');
  self.objquery.SQL.Add('left join tabfornecedor f on f.codigo = nf.fornecedor');

  self.objquery.SQL.Add('where nf.numero = '+#39+pNumero+#39 + 'and modelonf.codigo_fiscal = '+#39+pCodigoFiscal+#39);


  self.objquery.Active:=True;
  self.objquery.First;
  result:=self.objquery;


end;

function TobjSpedFiscal.localizaNFEntrada(pNumero,pCodigoFiscal: string): TIBQuery;
begin

  self.objquery.Active:=False;
  self.objquery.SQL.Clear;
  self.objquery.SQL.Add('select ep.codigo,ep.nf,ep.data,ep.emissaoNF,ep.valorprodutos,ep.valorfinal,modelo.codigo_fiscal,');
  self.objquery.SQL.Add('modelo.modelo,f.codigo as codigo_fornecedor,f.razaosocial,f.fantasia,f.cgc,f.endereco,f.bairro,f.cep,f.cidade,');
  self.objquery.SQL.Add('f.fone,f.estado,f.ie,ep.valorprodutos,ep.valorfrete,ep.basecalculoicms,ep.valoricms,ep.descontos');

  self.objquery.SQL.Add('from tabentradaprodutos ep');
  self.objquery.SQL.Add('join tabmodelonf modelo on modelo.codigo = ep.modelonf');
  self.objquery.SQL.Add('join tabfornecedor f on f.codigo = ep.fornecedor');
  self.objquery.SQL.Add('where ep.nf = '+#39+pNumero+#39);

  //InputBox('','',self.objquery.SQL.Text);

  self.objquery.Active:=True;
  self.objquery.First;
  result:=self.objquery;

end;

function TobjSpedFiscal.pegaModeloTreeView(tv: TTreeView): string;
  var NoRaiz:TTreeNode;
begin

  if tv.Selected = nil then
    Exit;

  NoRaiz := tv.Selected;

  While (Pos('MODELO',UpperCase(NoRaiz.Text)) = 0) do
    NoRaiz := NoRaiz.Parent;

  result:=RetornaSoNumeros(NoRaiz.Text);

end;

function TobjSpedFiscal.pegaTipoNFTreeView(tv: TTreeView): string;
var
  noRaiz:TTreeNode;
begin

   if tv.Selected = nil then
    Exit;

  NoRaiz := tv.Selected;

  While NoRaiz.Parent <> nil do
    NoRaiz := NoRaiz.Parent;

  if (Pos('Pr�pria',noRaiz.Text) <> 0) then
    result := 'S' {saida}
  else if (Pos('Terceiros',noRaiz.Text) <> 0) then
    result := 'E' {entrada}
  else
    Result := '';

end;

function TobjSpedFiscal.get_descricaoRegistro(pTipoRegistro,pPosicao,nometabela: string): string;
begin

  if (pTipoRegistro = '') or (pPosicao = '') then
  begin

    result:='Par�metro invalido: Tipo de registro: '+pTipoRegistro+' Posicao: '+pPosicao;
    Exit;

  end;

  self.objquery.Active:=False;
  Self.objquery.SQL.Clear;
  self.objquery.SQL.Add('select descricao,nomecampo');
  self.objquery.SQL.Add('from '+nometabela);
  self.objquery.SQL.Add('where tipo = '+#39+pTipoRegistro+#39+' and posicao = '+pPosicao);

  self.objquery.Active:=True;
  self.objquery.FetchAll;

  if (self.objquery.RecordCount > 0) then
    result:= Trim (self.objquery.fieldByName('nomecampo').AsString) + ' - '+Trim (self.objquery.fieldByName('descricao').AsString)
  else
    result:= '';

end;

function TobjSpedFiscal.arquivoValido_amanda(txt:string): Boolean;
begin
  result := (Pos ('|0000|',txt) <> 0);
end;

function TobjSpedFiscal.arquivoValido_ECF(txt:string): Boolean;
begin
  result := (Pos('|C400|',txt) <> 0);
end;

procedure TobjSpedFiscal.submit_cod_obrigacoes_icms(codigoOBR: string);
begin

  self.COD_OBRIGACOES_ICMS:= codigoOBR;

end;

procedure TobjSpedFiscal.submit_COD_RECEITAESTADUAL(codigoReceita: string);
begin

  Self.COD_RECEITAESTADUAL:=codigoReceita;

end;

procedure TobjSpedFiscal.validaBloco_E;
begin

  self.status.Panels[1].Text:='Validando dados do bloco E registro E116...';
  self.validaRegistro_E116();

end;



procedure TobjSpedFiscal.validaRegistro_E116;
begin

  Application.ProcessMessages;

  if (self.COD_OBRIGACOES_ICMS = '') then
    strERROS.Add('C�digo das obriga��es do ICMS n�o pode estar vazio');

  if (Length(self.COD_OBRIGACOES_ICMS) <> 3) then
    strERROS.Add('C�digo das obriga��es do ICMS inv�lido: '+self.COD_OBRIGACOES_ICMS);

  if (self.COD_RECEITAESTADUAL = '') then
    strERROS.Add('C�digo de Receita n�o pode estar vazio');

end;


procedure TobjSpedFiscal.submit_geraRegistroH(parametro: Boolean);
begin

  self.geraRegistroH:=parametro;

end;

procedure TobjSpedFiscal.submit_DATAFIN_ESTOQUE(parametro: string);
begin
  self.DATAFIN_ESTOQUE:=parametro;
end;

procedure TobjSpedFiscal.submit_DATAINI_ESTOQUE(parametro: string);
begin
  self.DATAINI_ESTOQUE:=parametro;
end;

procedure TobjSpedFiscal.addStr(str: TStringList; valor: string);
var
  pos,val:Integer;
begin

   { if not (str.Find (valor,pos)) then
    str.Add (valor); }

   pos:=0;
   val:=0;
   while pos<str.Count do
   begin
        if(str[pos]=valor)
        then val:=1;
        Inc(pos,1);
   end;
   if(val=0)
   then str.Add (valor);  
end;

procedure TobjSpedFiscal.gravaTXT();
begin

  self.ComponentesSped.ACBrSPEDFiscal1.SaveFileTXT;

end;

procedure TobjSpedFiscal.submit_memoErros(memo: TMemo);
begin

  Self.memoErros:=memo;

end;

procedure TobjSpedFiscal.validaDados_antesDaGeracao();
begin

  strERROS.Clear;
  strAdvertencia.Clear;

  self.validaBloco_0();
  self.validaBloco_C();
  self.validaBloco_E();

  self.validaCFOPSaida();
  self.validaCFOPEntrada();
  self.validaCSOSN_CST();

  self.carregaErrosMemo();
  Application.ProcessMessages;

end;

end.



