unit UGRUPOSERVICO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjSERVICO,
  UessencialGlobal, Tabs,UPLanodeContas;

type
  TFGRUPOSERVICO = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigoGrupoServico: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    EdtPlanodeContas: TEdit;
    lb2: TLabel;
    lbNomePlanoDeContas: TLabel;
    EdtNome: TEdit;
    lbLbNome: TLabel;
    btPrimeiro: TSpeedButton;
    btAnterior: TSpeedButton;
    btProximo: TSpeedButton;
    btUltimo: TSpeedButton;
    grp1: TGroupBox;
    lbLbPorcentagemRetirado: TLabel;
    lbLbPorcentagemFornecido: TLabel;
    lbLbPorcentagemInstalado: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    lb5: TLabel;
    edtEdtPorcentagemRetirado: TEdit;
    edtEdtPorcentagemInstalado: TEdit;
    edtEdtPorcentagemFornecido: TEdit;
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtPlanodeContasExit(Sender: TObject);
    procedure EdtPlanodeContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btOpcoesClick(Sender: TObject);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdtPlanodeContasDblClick(Sender: TObject);
    procedure EdtPlanodeContasKeyPress(Sender: TObject; var Key: Char);
    procedure edtEdtPorcentagemInstaladoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtEdtPorcentagemFornecidoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtEdtPorcentagemRetiradoKeyPress(Sender: TObject;
      var Key: Char);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FGRUPOSERVICO: TFGRUPOSERVICO;
  ObjSERVICO:TObjSERVICO;

implementation

uses Upesquisa, UessencialLocal, UMenuRelatorios, UDataModulo,
  UescolheImagemBotao, UobjGRUPOSERVICO;

{$R *.dfm}




procedure TFGRUPOSERVICO.FormShow(Sender: TObject);
begin

    limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     ColocaUpperCaseEdit(Self);

     Try
        ObjSERVICO:=TObjSERVICO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     habilita_botoes(Self);
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE GRUPO DE SERVICO')=False)
     Then desab_botoes(Self);
    FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
    FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
    lbCodigoGrupoServico.caption:='';

    if(Tag<>0)
   then begin
         if(ObjSERVICO.GrupoServico.LocalizaCodigo(IntToStr(Tag))=True)then
         begin
              ObjSERVICO.GrupoServico.TabelaparaObjeto;
              self.ObjetoParaControles;
         end;

   end;

end;

procedure TFGRUPOSERVICO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjSERVICO=Nil)
     Then exit;

     If (ObjSERVICO.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjSERVICO.free;
  
end;


procedure TFGRUPOSERVICO.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbCodigoGrupoServico.caption:='0';
     //edtcodigo.text:=ObjServico.GrupoServico.Get_novocodigo;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjServico.GrupoServico.status:=dsInsert;
     EdtNome.setfocus;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

end;

procedure TFGRUPOSERVICO.btSalvarClick(Sender: TObject);
begin

     If ObjServico.GrupoServico.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjServico.GrupoServico.salvar(true)=False)
     Then exit;

     lbCodigoGrupoServico.caption:=ObjServico.GrupoServico.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
   btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFGRUPOSERVICO.btAlterarClick(Sender: TObject);
begin
    If (ObjServico.GrupoServico.Status=dsinactive) and (lbCodigoGrupoServico.caption<>'')
    Then Begin
                habilita_campos(Self);
                ObjServico.GrupoServico.Status:=dsEdit;
                edtNome.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btnovo.Visible :=false;
                 btalterar.Visible:=false;
                 btpesquisar.Visible:=false;
                 btrelatorio.Visible:=false;
                 btexcluir.Visible:=false;
                 btsair.Visible:=false;
                 btopcoes.Visible :=false;
          End;

end;

procedure TFGRUPOSERVICO.btCancelarClick(Sender: TObject);
begin
     ObjServico.GrupoServico.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
    lbCodigoGrupoServico.caption:='';
end;

procedure TFGRUPOSERVICO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjServico.GrupoServico.Get_pesquisa,ObjServico.GrupoServico.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjServico.GrupoServico.status<>dsinactive
                                  then exit;

                                  If (ObjServico.GrupoServico.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjServico.GrupoServico.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFGRUPOSERVICO.btExcluirClick(Sender: TObject);
begin
     If (ObjServico.GrupoServico.status<>dsinactive) or (lbCodigoGrupoServico.Caption='')
     Then exit;

     If (ObjServico.GrupoServico.LocalizaCodigo(lbCodigoGrupoServico.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjServico.GrupoServico.exclui(lbCodigoGrupoServico.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFGRUPOSERVICO.btRelatorioClick(Sender: TObject);
begin
//    ObjServico.GrupoServico.Imprime;
end;

procedure TFGRUPOSERVICO.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFGRUPOSERVICO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFGRUPOSERVICO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFGRUPOSERVICO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;


end;

procedure TFGRUPOSERVICO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFGRUPOSERVICO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjServico.GrupoServico do
    Begin
        Submit_Codigo(lbCodigoGrupoServico.Caption);
        Submit_Nome(edtNome.text);
        PlanoDeContas.Submit_CODIGO(EdtPlanodeContas.Text);
        Submit_PORCENTAGEMINSTALADO(edtEdtPorcentagemInstalado.text);
        Submit_PORCENTAGEMFORNECIDO(edtEdtPorcentagemFornecido.text);
        Submit_PorcentagemRetirado(edtEdtPorcentagemRetirado.text);
        result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFGRUPOSERVICO.LimpaLabels;
begin
    lbNomePlanoDeContas.Caption:='';
end;

function TFGRUPOSERVICO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjServico.GrupoServico do
     Begin
        lbCodigoGrupoServico.Caption:=Get_Codigo;
        EdtNome.text:=Get_Nome;
        EdtPlanodeContas.Text:=PlanoDeContas.Get_CODIGO;
        lbNomePlanoDeContas.Caption:='TIPO = '+ObjServico.GrupoServico.PlanoDeContas.Get_Tipo+' | ';
        lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' CLASSIF. = '+ObjServico.GrupoServico.PlanoDeContas.Get_Classificacao+' | ';
        lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' NOME = '+ObjServico.GrupoServico.PlanoDeContas.Get_Nome;
        edtEdtPorcentagemRetirado.Text:=Get_PorcentagemRetirado;
        edtEdtPorcentagemInstalado.text:=Get_PORCENTAGEMINSTALADO;
        edtEdtPorcentagemFornecido.Text:=Get_Porcentagemfornecido;
        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFGRUPOSERVICO.TabelaParaControles: Boolean;
begin
     If (ObjServico.GrupoServico.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;


procedure TFGRUPOSERVICO.EdtPlanodeContasExit(Sender: TObject);
begin
    ObjServico.GrupoServico.EdtGrupoPlanoDeContasExit(Sender, lbNomePlanoDeContas);
    lbNomePlanoDeContas.Caption:='TIPO = '+ObjServico.GrupoServico.PlanoDeContas.Get_Tipo+' | ';
    lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' CLASSIF. = '+ObjServico.GrupoServico.PlanoDeContas.Get_Classificacao+' | ';
    lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' NOME = '+ObjServico.GrupoServico.PlanoDeContas.Get_Nome;

end;

procedure TFGRUPOSERVICO.EdtPlanodeContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjServico.GrupoServico.EdtGrupoPlanoDeContasKeyDown(Sender, Key, Shift, lbNomePlanoDeContas);
end;

procedure TFGRUPOSERVICO.btOpcoesClick(Sender: TObject);
begin
     With FmenuRelatorios do
     Begin
          //era no titulo antes
          NomeObjeto:='UOBJGRUPOFERRAGEM';

          With RgOpcoes do
          Begin
                items.clear;
                Items.Add('Atualiza Pre�o de Custo');
                Items.Add('Atualiza Margens');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0 : ObjSERVICO.AtualizaPrecos(lbCodigoGrupoServico.Caption);
                1 : ObjSERVICO.AtualizaMargens(lbCodigoGrupoServico.Caption);
          End;
     end;

end;

procedure TFGRUPOSERVICO.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjServico.GrupoServico.PrimeiroRegistro = false)then
    exit;

    ObjServico.GrupoServico.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOSERVICO.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigoGrupoServico.Caption='')then
    lbCodigoGrupoServico.Caption:='0';

    if  (ObjServico.GrupoServico.RegistoAnterior(StrToInt(lbCodigoGrupoServico.Caption)) = false)then
    exit;

    ObjServico.GrupoServico.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOSERVICO.btProximoClick(Sender: TObject);
begin
    if (lbCodigoGrupoServico.Caption='')then
    lbCodigoGrupoServico.Caption:='0';

    if  (ObjServico.GrupoServico.ProximoRegisto(StrToInt(lbCodigoGrupoServico.Caption)) = false)then
    exit;

    ObjServico.GrupoServico.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOSERVICO.btUltimoClick(Sender: TObject);
begin
    if  (ObjServico.GrupoServico.UltimoRegistro = false)then
    exit;

    ObjServico.GrupoServico.TabelaparaObjeto;
    Self.ObjetoParaControles;


end;



procedure TFGRUPOSERVICO.EdtPlanodeContasDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FplanoContas:TFplanodeContas;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FplanoContas:=TFplanodeContas.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabplanodecontas','',FplanoContas)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtPlanodeContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbNomePlanodeContas.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FplanoContas);

     End;
end;
procedure TFGRUPOSERVICO.EdtPlanodeContasKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFGRUPOSERVICO.edtEdtPorcentagemInstaladoKeyPress(
  Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFGRUPOSERVICO.edtEdtPorcentagemFornecidoKeyPress(
  Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFGRUPOSERVICO.edtEdtPorcentagemRetiradoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

end.
