unit UobjCARTACORRECAO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,
     ComCtrls,UobjNFe,uobjgeranfe,SHDocVw,ACBrUtil,Controls,Forms,UobjTransmiteNFE;
//USES_INTERFACE


Type
   TObjCARTACORRECAO=class

          Public
            Status :TDataSetState;
            SqlInicial:String;
            MemoResp:TMemo;
            WBResposta:TWebBrowser;
            memoRespWS:TMemo;

            Constructor Create;
            Destructor  Free;
            Function    Salvar (ComCommit : Boolean):Boolean;
            Function    LocalizaCodigo(Parametro:string) :boolean;
            Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
            Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
            Function    Get_Pesquisa   :TStringList;
            Function    Get_TituloPesquisa              :string;

            Function   TabelaparaObjeto:Boolean;
            Procedure   ZerarTabela;
            Procedure   Cancelar;
            Procedure   Commit;

            Function  Get_NovoCodigo:string;
            Function  RetornaCampoCodigo:string;
            Function  RetornaCampoNome:string;
            Procedure Imprime(Pcodigo:string);

            Procedure Submit_CODIGO(parametro: string);
            Function Get_CODIGO: string;
            Procedure Submit_CORRECAO(parametro: string);
            Function Get_CORRECAO: string;
            Procedure Submit_CHAVEACESSO(parametro: string);
            Function Get_CHAVEACESSO: string;
            Procedure Submit_CNPJCPF(parametro: string);
            Function Get_CNPJCPF: string;
            Procedure Submit_LOTEENVIO(parametro: string);
            Function Get_LOTEENVIO: string;
            Procedure Submit_UFRECEPCAO(parametro: string);
            Function Get_UFRECEPCAO: string;
            Procedure Submit_NFE(parametro: string);
            Function Get_NFE: string;
            Procedure Submit_ENVIADA(parametro: string);
            Function Get_ENVIADA: string;
            procedure submit_dataTransmissao(parametro:string);
            function get_dataTransmissao():string;
            procedure submit_horatranmissao(parametro:string);
            function get_horatransmissao():string;
            procedure submit_caminhoXml(parametro:string);
            function get_caminhoXML():string;

            procedure submit_caminho_nfe_origem(parametro:string);
            function get_caminho_nfe_origem():string;

            procedure criarTreeView(treeView : TTreeView;dataIni:string = '';dataFin:string = '');
            function extraiChaveAcess(Sender : TObject) : Boolean;
            function consultaChave(chave,UF : string):integer;
            function retornaUF(chaveAcesso : string):string;
            function retornaCNPJ(chaveAcesso : string):string;
            function getnSeqEvento(chaveAcesso:string) : Integer;
            function getnSeqEventoCampo() : Integer;
            function transmitir( pCodigo : string; var msgRej : string; pathXML:string) : Boolean;
            function rejeitada(info:string;var msgErro : string):Boolean;
            function getRejeicao(info : string;_indRej : integer) : string;
            function procuraItem(treeView : TTreeView;procura:string;noStart : TTreeNode = nil) : TTreeNode;


         Private
           Objquery:Tibquery;
           queryTemp:TIBQuery;
           InsertSql,DeleteSql,ModifySQl:TStringList;
           objNfe : TObjNFE;
           objGeraNFe : Tobjgeranfe;
           objTransmiteNFE:TObjTransmiteNFE;
           strXml:TStringList;

           CODIGO:string;
           CORRECAO:string;
           CHAVEACESSO:string;
           CNPJCPF:string;
           LOTEENVIO:string;
           UFRECEPCAO:string;
           NFE:string;
           ENVIADA:string;
           CAMINHOXML:string;
           nSeqEvento:string;
           dataTransmissao:string;
           horaTransmissao:string;
           caminho_nfe_origem:string;

          ParametroPesquisa:TStringList;

          Function  VerificaBrancos:Boolean;
          Function  VerificaRelacionamentos:Boolean;
          Function  VerificaNumericos:Boolean;
          Function  VerificaData:Boolean;
          Function  VerificaFaixa:boolean;
          Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,
     UmenuRelatorios, ComObj, UComponentesNfe, UobjEMPRESA;





Function  TObjCARTACORRECAO.TabelaparaObjeto:Boolean;
begin

  With Objquery do
  Begin
    Self.ZerarTabela;
    Self.CODIGO:=fieldbyname('CODIGO').asstring;
    Self.CORRECAO:=fieldbyname('CORRECAO').asstring;
    Self.CHAVEACESSO:=fieldbyname('CHAVEACESSO').asstring;
    Self.CNPJCPF:=fieldbyname('CNPJCPF').asstring;
    Self.LOTEENVIO:=fieldbyname('LOTEENVIO').asstring;
    Self.UFRECEPCAO:=fieldbyname('UFRECEPCAO').asstring;
    Self.NFE:=fieldbyname('NFE').asstring;
    Self.ENVIADA:=fieldbyname('ENVIADA').asstring;
    self.CAMINHOXML:=fieldbyname('CAMINHOXML').AsString;
    self.nSeqEvento:=fieldbyname('nSeqEvento').AsString;
    self.dataTransmissao:=fieldbyname('datatransmissao').AsString;
    self.horaTransmissao:=fieldbyname('horatransmissao').AsString;
    caminho_nfe_origem:=fieldbyname('caminho_nfe_origem').AsString;
    result:=True;
  End;
  
end;


Procedure TObjCARTACORRECAO.ObjetoparaTabela;
begin

  With Objquery do
  Begin
    ParamByName('CODIGO').asstring:=Self.CODIGO;
    ParamByName('CORRECAO').asstring:=Self.CORRECAO;
    ParamByName('CHAVEACESSO').asstring:=Self.CHAVEACESSO;
    ParamByName('CNPJCPF').asstring:=Self.CNPJCPF;
    ParamByName('LOTEENVIO').asstring:=Self.LOTEENVIO;
    ParamByName('UFRECEPCAO').asstring:=Self.UFRECEPCAO;
    ParamByName('NFE').asstring:=Self.NFE;
    ParamByName('ENVIADA').asstring:=Self.ENVIADA;
    ParamByName('CAMINHOXML').AsString := self.CAMINHOXML;
    ParamByName('nSeqEvento').AsString := self.nSeqEvento;
    ParamByName('datatransmissao').AsString := self.dataTransmissao;
    ParamByName('horatransmissao').AsString:=self.horaTransmissao;
    ParamByName('caminho_nfe_origem').AsString:= self.caminho_nfe_origem;

    if (Self.CAMINHOXML <> '') Then
    begin

      if  FileExists(Self.CAMINHOXML) Then
        ParamByName('XMLB').LoadFromFile(Self.CAMINHOXML,ftBlob);

    End;

  End;

End;


function TObjCARTACORRECAO.Salvar(ComCommit:Boolean): Boolean;
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


  if Self.CODIGO <> '0' then
  begin

    If Self.LocalizaCodigo(Self.CODIGO)=False Then
    Begin

      if(Self.Status=dsedit) Then
      Begin
        Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
        exit;
      End;

    End
    Else
    Begin

      if(Self.Status=dsinsert) Then
      Begin
        Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
        exit;
      End;

    End;
    
  end;

  if Self.status=dsinsert Then
  Begin

    Self.Objquery.SQL.Clear;
    Self.Objquery.SQL.text:=Self.InsertSql.Text;
    
    if (Self.Codigo='0') Then
      Self.codigo:=Self.Get_NovoCodigo;
  End
  Else
  Begin

    if (Self.Status=dsedit) Then
    Begin
      Self.Objquery.SQL.Clear;
      Self.Objquery.SQL.text:=Self.ModifySQl.Text;
    End
    Else
    Begin
      Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
      exit;
    End;

  End;

  Self.ObjetoParaTabela;

  Try
    Self.Objquery.ExecSQL;
  Except

    if (Self.Status=dsInsert) Then
      Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
    Else
      Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0);

    exit;

  End;

  If ComCommit=True Then
    FDataModulo.IBTransaction.CommitRetaining;

  Self.status := dsInactive;
  result := True;
end;

procedure TObjCARTACORRECAO.ZerarTabela;
Begin
  CODIGO:='';
  CORRECAO:='';
  CHAVEACESSO:='';
  CNPJCPF:='';
  LOTEENVIO:='';
  UFRECEPCAO:='';
  NFE:='';
  ENVIADA:='';
  CAMINHOXML:='';
  nSeqEvento:='';
  dataTransmissao:='';
  horaTransmissao:='';
  caminho_nfe_origem:='';
end;

Function TObjCARTACORRECAO.VerificaBrancos:boolean;
var
   Mensagem:string;
begin

  Result:=True;
  mensagem:='';

  If (CODIGO = '') Then
    Mensagem:=mensagem+'/C�digo';

  if (CHAVEACESSO = '') then
    Mensagem:=Mensagem+'/Chave de acesso';

  if (self.CNPJCPF = '') then
    Mensagem:=Mensagem+'/CNPJ do emitente'
  else if not ValidaCNPJ(CNPJCPF) then
    Mensagem:=Mensagem+'/CNPJ inv�lido';

  if (LOTEENVIO = '') then
    Mensagem:=Mensagem+'/Lote de envio';

  if (UFRECEPCAO = '') then
    Mensagem := Mensagem + '/UF';

  if (ENVIADA = '') then
    ENVIADA := 'N';


  if (CORRECAO = '') then
    Mensagem := Mensagem + '/Corre��o';

  if mensagem<>'' Then
  Begin
    messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
    exit;
  End;

  result:=false;

end;


function TObjCARTACORRECAO.VerificaRelacionamentos: Boolean;
var
  mensagem:string;
Begin

  Result:=False;
  mensagem:='';

  If (mensagem<>'') Then
  Begin
    Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
    exit;
  End;

  result:=true;
  
End;

function TObjCARTACORRECAO.VerificaNumericos: Boolean;
var
   Mensagem:string;
begin

  Result:=False;
  Mensagem:='';

  try
    Strtoint(Self.CODIGO);
  Except
    Mensagem:=mensagem+'/C�digo';
  End;

  try
    Strtoint(Self.LOTEENVIO);
  Except
    Mensagem:=mensagem+'/Lote de envio';
  End;

  if NFE <> '' then
    if not IsNumeric(NFE) then
      Mensagem := Mensagem + '/NFE';


  If Mensagem<>'' Then
  Begin
    Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
    exit;
  End;
  
  result:=true;

end;

function TObjCARTACORRECAO.VerificaData: Boolean;
var
  Mensagem:string;
begin

  Result:=False;
  mensagem:='';

  If Mensagem<>'' Then
  Begin
    Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
    exit;
  End;

  result:=true;

end;

function TObjCARTACORRECAO.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

  Result:=False;

  With Self do
  Begin
    Mensagem:='';

    If mensagem<>'' Then
    Begin
      Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
      exit;
    End;

    result:=true;

  End;

end;

function TObjCARTACORRECAO.LocalizaCodigo(parametro: string): boolean;
begin

  result := False;

  if (Parametro='') Then
  Begin
    Messagedlg('Par�metro CARTACORRECAO vazio',mterror,[mbok],0);
    exit;
  End;

  With Self.Objquery do
  Begin

    close;
    Sql.Clear;
    SQL.ADD('Select CODIGO,CORRECAO,CHAVEACESSO,CNPJCPF,LOTEENVIO,UFRECEPCAO,');
    SQL.ADD('NFE,ENVIADA,CAMINHOXML,nSeqEvento,XMLB,datatransmissao,horatransmissao,caminho_nfe_origem');
    SQL.ADD('from  TABCARTACORRECAO');
    SQL.ADD('WHERE codigo='+parametro);

    Open;

    If (recordcount>0) Then
      Result:=True
    Else
      Result:=False;

  End;

end;

procedure TObjCARTACORRECAO.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjCARTACORRECAO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjCARTACORRECAO.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
begin

  Try

    result:=true;

    If (Self.LocalizaCodigo(Pcodigo)=True) Then
    Begin

      Self.Objquery.close;
      Self.Objquery.SQL.clear;
      Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
      Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
      Self.Objquery.ExecSQL;

      If (ComCommit=True) Then
      FDataModulo.IBTransaction.CommitRetaining;

    End
    Else
    Begin
      result:=false;
      Perro:='C�digo '+Pcodigo+' n�o localizado';

    End;
  Except
    on e:exception do
    Begin
      perro:=E.message;
      result:=false;
    End;
  End;

end;


constructor TObjCARTACORRECAO.create;
begin


  Self.Objquery:=TIBQuery.create(nil);
  Self.Objquery.Database:=FDataModulo.IbDatabase;
  self.queryTemp:=TIBQuery.Create(nil);
  self.queryTemp.Database := FDataModulo.IBDatabase;
  Self.ParametroPesquisa:=TStringList.create;
  objNfe := TObjNFE.Create;
  objGeraNFe := Tobjgeranfe.create;
  objTransmiteNFE:=TObjTransmiteNFE.Create(nil,FDataModulo.IBDatabase);

  InsertSql:=TStringList.create;
  DeleteSql:=TStringList.create;
  ModifySQl:=TStringList.create;
  strXml:=TStringList.Create;

  Self.ZerarTabela;

  With Self do
  Begin

    InsertSQL.clear;
    InsertSQL.add('Insert Into TABCARTACORRECAO(CODIGO,CORRECAO,CHAVEACESSO');
    InsertSQL.add(' ,CNPJCPF,LOTEENVIO,UFRECEPCAO,NFE,ENVIADA,CAMINHOXML,nSeqEvento,XMLB,datatransmissao,horatransmissao,caminho_nfe_origem)');
    InsertSQL.add('values (:CODIGO,:CORRECAO,:CHAVEACESSO,:CNPJCPF,:LOTEENVIO');
    InsertSQL.add(' ,:UFRECEPCAO,:NFE,:ENVIADA,:CAMINHOXML,:nSeqEvento,:XMLB,:datatransmissao,:horatransmissao,:caminho_nfe_origem)');

    ModifySQL.clear;
    ModifySQL.add('Update TABCARTACORRECAO set CODIGO=:CODIGO,CORRECAO=:CORRECAO');
    ModifySQL.add(',CHAVEACESSO=:CHAVEACESSO,CNPJCPF=:CNPJCPF,LOTEENVIO=:LOTEENVIO');
    ModifySQL.add(',UFRECEPCAO=:UFRECEPCAO,NFE=:NFE,ENVIADA=:ENVIADA,CAMINHOXML=:CAMINHOXML,nSeqEvento=:nSeqEvento,XMLB=:XMLB,datatransmissao=:datatransmissao,horatransmissao=:horatransmissao,caminho_nfe_origem=:caminho_nfe_origem');
    ModifySQL.add('where codigo=:codigo');

    DeleteSQL.clear;
    DeleteSql.add('Delete from TABCARTACORRECAO where codigo=:codigo ');

    Self.status := dsInactive;
  End;

end;
procedure TObjCARTACORRECAO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCARTACORRECAO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCARTACORRECAO');
     Result:=Self.ParametroPesquisa;
end;

function TObjCARTACORRECAO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de CARTACORRECAO ';
end;


function TObjCARTACORRECAO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin

  Try

    Try

      IbqueryGen:=TIBquery.create(nil);
      IbqueryGen.database:=FdataModulo.IBDatabase;
      IbqueryGen.close;
      IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCARTACORRECAO,1) CODIGO FROM RDB$DATABASE');
      IbqueryGen.open;
      Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
    Except
      Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
      result:='0';
      exit;
    End;
  Finally
    FreeandNil(IbqueryGen);
  End;

End;


destructor TObjCARTACORRECAO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.queryTemp);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    FreeAndNil(strXML);
    FreeAndNil(ObjTransmiteNFE);
    objNfe.Free;
    objGeraNFe.free;


end;

function TObjCARTACORRECAO.RetornaCampoCodigo: string;
begin
  result:='codigo';
end;

function TObjCARTACORRECAO.RetornaCampoNome: string;
begin
  result:='CORRECAO';
end;

procedure TObjCARTACORRECAO.Submit_CODIGO(parametro: string);
begin
  Self.CODIGO:=Parametro;
end;
function TObjCARTACORRECAO.Get_CODIGO: string;
begin
  Result:=Self.CODIGO;
end;
procedure TObjCARTACORRECAO.Submit_CORRECAO(parametro: string);
begin
  Self.CORRECAO:=Parametro;
end;
function TObjCARTACORRECAO.Get_CORRECAO: string;
begin
  Result:=Self.CORRECAO;
end;
procedure TObjCARTACORRECAO.Submit_CHAVEACESSO(parametro: string);
begin
  Self.CHAVEACESSO:=Parametro;
end;
function TObjCARTACORRECAO.Get_CHAVEACESSO: string;
begin
  Result:=Self.CHAVEACESSO;
end;
procedure TObjCARTACORRECAO.Submit_CNPJCPF(parametro: string);
begin
  Self.CNPJCPF:=Parametro;
end;
function TObjCARTACORRECAO.Get_CNPJCPF: string;
begin
  Result:=Self.CNPJCPF;
end;
procedure TObjCARTACORRECAO.Submit_LOTEENVIO(parametro: string);
begin
  Self.LOTEENVIO:=Parametro;
end;
function TObjCARTACORRECAO.Get_LOTEENVIO: string;
begin
  Result:=Self.LOTEENVIO;
end;
procedure TObjCARTACORRECAO.Submit_UFRECEPCAO(parametro: string);
begin
  Self.UFRECEPCAO:=Parametro;
end;
function TObjCARTACORRECAO.Get_UFRECEPCAO: string;
begin
  Result:=Self.UFRECEPCAO;
end;
procedure TObjCARTACORRECAO.Submit_NFE(parametro: string);
begin
  Self.NFE:=Parametro;
end;
function TObjCARTACORRECAO.Get_NFE: string;
begin
  Result:=Self.NFE;
end;
procedure TObjCARTACORRECAO.Submit_ENVIADA(parametro: string);
begin
  Self.ENVIADA:=Parametro;
end;
function TObjCARTACORRECAO.Get_ENVIADA: string;
begin
  Result:=Self.ENVIADA;
end;

procedure TObjCARTACORRECAO.Imprime(Pcodigo: string);
begin

  With FmenuRelatorios do
  Begin
    NomeObjeto:='UOBJCARTACORRECAO';

    With RgOpcoes do
    Begin
      items.clear;
    End;

    showmodal;

    If (Tag=0)//indica botao cancel ou fechar
    Then exit;

    Case RgOpcoes.ItemIndex of
    0:;
    End;
  end;

end;

procedure TObjCARTACORRECAO.criarTreeView(treeView: TTreeView;dataIni:string;dataFin:string);
var
  mes:string;
  no : TTreeNode;
  noRslt : TTreeNode;
begin

  if (dataIni = '') or (dataFin = '') then
    primeiroUltimoDia(FormatDateTime('mm',Date),FormatDateTime('yyyy',date),dataIni,dataFin);

  queryTemp.Active := False;
  queryTemp.Params.Clear;
  queryTemp.SQL.Text := 'select nf.numero,nfe.chave_acesso,nf.dataemissao '+
                        'from tabnotafiscal nf '+
                        'inner join tabnfe nfe on nfe.codigo = nf.nfe '+
                        'where (nf.dataemissao >= :dataIni) and '+
                        '(nf.dataemissao <= :dataFin) and '+
                        '(nf.modelo_nf = :modelo) and '+
                        '((nf.situacao = :sit1) or (nf.situacao = :sit2)) '+
                        'order by nf.dataemissao';

  queryTemp.Params[0].AsDate     := StrToDate(dataIni);
  queryTemp.Params[1].AsDate     := StrToDate(dataFin);
  queryTemp.Params[2].AsInteger  := 2;
  queryTemp.Params[3].AsString   := 'G';
  queryTemp.Params[4].AsString   := 'I';

  queryTemp.DisableControls;
  queryTemp.Active := True;

  if queryTemp.IsEmpty then
    Exit;
    
  queryTemp.First;

  mes := FormatDateTime('mm',queryTemp.Fields[2].AsDateTime);
  treeView.Items.Clear;
  no := treeView.Items.Add(nil,queryTemp.Fields[2].AsString);
  no.ImageIndex := 8;
  no.SelectedIndex := no.ImageIndex;

  while not queryTemp.Eof do
  begin

    if mes <> FormatDateTime('mm',queryTemp.Fields[2].AsDateTime) then
    begin
      mes := FormatDateTime('mm',queryTemp.Fields[2].AsDateTime);
      no := treeView.Items.Add(nil,queryTemp.Fields[2].AsString);
      no.ImageIndex := 8;
      no.SelectedIndex := no.ImageIndex;
    end
    else
    begin
      noRslt := treeView.Items.AddChild(no,queryTemp.Fields[0].AsString + ' - ' + queryTemp.Fields[1].AsString);
      noRslt.ImageIndex := 31;
      noRslt.SelectedIndex := noRslt.ImageIndex;
    end;

    queryTemp.Next;

  end;

end;

function TObjCARTACORRECAO.extraiChaveAcess(Sender : TObject): Boolean;
begin

  objNfe.extraiChave(sender);

end;

function TObjCARTACORRECAO.consultaChave(chave,UF : string) : integer;
begin
  result := objTransmiteNFE.consultaPelaChave(chave,UF,false);
end;

function TObjCARTACORRECAO.retornaCNPJ(chaveAcesso: string): string;
begin
  Result := Copy(CHAVEACESSO,7,14);  
end;

function TObjCARTACORRECAO.retornaUF(chaveAcesso: string): string;
begin
  result := getUF(StrToInt(chaveAcesso[1]+chaveAcesso[2]));
end;

function TObjCARTACORRECAO.transmitir( pCodigo : string; var msgRej : string; pathXML:string) : Boolean;
var
  numeroSeqEvento:integer;
  condUso:string;
  horaTransm:string;
begin

  result := False;

  try

    if pCodigo = '' then Exit;
    if not LocalizaCodigo(pCodigo) then Exit;
    if not TabelaparaObjeto then Exit;

    condUso := '';(*'A Carta de Correcao e disciplinada pelo paragrafo '+
               '1�-A do art. 7� do Convenio S/N, de 15 de dezembro '+
               'de 1970 e pode ser utilizada para regularizacao '+
               'de erro ocorrido na emissao de documento fiscal, '+
               'desde que o erro nao esteja relacionado com: '+
               'I - as variaveis que determinam o valor do imposto '+
               'tais como: base de calculo, aliquota,diferenca de preco, '+
               'quantidade, valor da operacao ou da prestacao; II - a '+
               'corre��o de dados cadastrais que implique mudanca do '+
               'remetente ou do destinatario; III - a data de emissao ou de saida.';*)

    numeroSeqEvento := getnSeqEvento(self.CHAVEACESSO);
    Inc(numeroSeqEvento);

    objTransmiteNFE.cartaCorrecao := True;
    objTransmiteNFE.FCCe.condUso     := condUso;
    objTransmiteNFE.FCCe.idLote      := StrToInt(LOTEENVIO);
    objTransmiteNFE.FCCe.chaveacesso := self.CHAVEACESSO;
    objTransmiteNFE.FCCe.cOrgao      := getcodUF(self.UFRECEPCAO);
    objTransmiteNFE.FCCe.nSeqEvento  := numeroSeqEvento;
    objTransmiteNFE.FCCe.xCorrecao   := self.CORRECAO;
    objTransmiteNFE.FCCe.CNPJCPF     := self.CNPJCPF;
    objTransmiteNFE.FCCe.PathXML     := pathXML;

    horaTransm := TimeToStr(time);
    objTransmiteNFE.Transmite;
    if objTransmiteNFE.strErros.Count > 0 then
    begin
      msgRej := objTransmiteNFE.strErros.Text;
      Exit;
    end;

    MemoResp.Lines.Text := objTransmiteNFE.FCCe.getRetWs; //UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.RetWS);
    memoRespWS.Lines.Text := objTransmiteNFE.FCCe.getRetornoWS; //UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.RetornoWS);
    CAMINHOXML := objTransmiteNFE.FCCe.getCaminhoXML+IntToStr(numeroSeqEvento)+' - '+self.CHAVEACESSO+' CC-e.xml';

    if FileExists(CAMINHOXML) then
      DeleteFile(CAMINHOXML);

    memoRespWS.Lines.SaveToFile(CAMINHOXML);

    msgRej := '';
    if rejeitada(MemoResp.Lines.Text,msgRej) then
    begin
      MensagemErro('Erro ao transmitir');
      Exit;
    end;

  except
    on e:Exception do
    begin
      MensagemErro('Erro ao transmitir. '+e.Message);
      Exit;
    end;
  end;

  WBResposta.Navigate(PathWithDelim(CAMINHOXML));

  self.Status := dsEdit;
  self.ENVIADA := 'S';
  self.dataTransmissao := DateToStr(date);
  self.horaTransmissao := horaTransm;
  self.nSeqEvento := IntToStr(numeroSeqEvento);

  if not self.Salvar(false) then
  begin
    MensagemErro('Erro ao salvar carta no banco');
    Exit;
  end;

  result := True;

end;

function TObjCARTACORRECAO.getnSeqEvento(chaveAcesso:string): Integer;
begin
  queryTemp.Close;
  queryTemp.SQL.Text := 'select MAX(cc.NSEQEVENTO) from TABCARTACORRECAO cc where cc.chaveacesso = '+QuotedStr(CHAVEACESSO);
  queryTemp.Open;
  result := queryTemp.fieldbyname('max').AsInteger;
  {para cada carta vinculada a uma NF-e (chave de acesso) so podem ser emitidos nseqEvento de 1-20}
end;

function TObjCARTACORRECAO.get_dataTransmissao: string;
begin
  Result := Self.dataTransmissao;
end;

procedure TObjCARTACORRECAO.submit_dataTransmissao(parametro: string);
begin
  self.dataTransmissao := parametro;
end;

function TObjCARTACORRECAO.get_horatransmissao: string;
begin
  Result := self.horaTransmissao;
end;

procedure TObjCARTACORRECAO.submit_horatranmissao(parametro: string);
begin
  self.horaTransmissao := parametro;
end;

function TObjCARTACORRECAO.rejeitada(info: string;var msgErro : string): Boolean;
var
  _indRej : integer;
begin

  _indRej := Pos('Rejeicao',info);

  if _indRej = 0 then
    _indRej := Pos('Erro',info);

  result := _indRej <> 0;

  if Result then
    msgErro := getRejeicao(info,_indRej);

end;

function TObjCARTACORRECAO.getRejeicao(info: string;_indRej: integer): string;
begin

  result := '';

  if info = '' then Exit;

  while info[_indRej] <> '<' do
  begin
    result := Result + info[_indRej];
    _indRej := _indRej + 1;

    if _indRej > Length(info) then
      Exit;

  end;

end;

function TObjCARTACORRECAO.get_caminhoXML: string;
begin
  result := CAMINHOXML;
end;

procedure TObjCARTACORRECAO.submit_caminhoXml(parametro: string);
begin
  CAMINHOXML := parametro;
end;

function TObjCARTACORRECAO.getnSeqEventoCampo: Integer;
begin
  result := StrToIntDef(self.nSeqEvento,0);
end;

function TObjCARTACORRECAO.procuraItem(treeView: TTreeView;procura:string;noStart : TTreeNode): TTreeNode;
var
  noAux : TTreeNode;
begin

  result := nil;

  if Trim(procura) = '' then
    Exit;

  if noStart = nil then
    noAux := treeView.Items.GetFirstNode
  else
    noAux := noStart;

  while noAux <> nil do
  begin

    noAux.Selected := True;
    Application.ProcessMessages;

    if Pos(procura,noAux.Text) <> 0 then
    begin
      result := noAux;
      Exit;
    end;

    noAux := noAux.GetNext;
    
  end;

  result := nil;

end;


function TObjCARTACORRECAO.get_caminho_nfe_origem: string;
begin
  result := self.caminho_nfe_origem;
end;

procedure TObjCARTACORRECAO.submit_caminho_nfe_origem(parametro: string);
begin
  self.caminho_nfe_origem := parametro;
end;

end.



