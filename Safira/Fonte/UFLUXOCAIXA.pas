unit UFLUXOCAIXA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjContaGerencialFLUXOCAIXA,
  Grids, DBGrids;

type
  TFFLUXOCAIXA = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    DbGridContaGerencial: TDBGrid;
    btgerarplanilha: TBitBtn;
    btrastrear: TBitBtn;
    DBGridlinhas: TDBGrid;
    Panel2: TPanel;
    comboDetalhaCredorDevedor: TComboBox;
    LbDetalhaCredorDevedor: TLabel;
    EdtNome: TEdit;
    LbNome: TLabel;
    EdtCODIGO: TEdit;
    LbCODIGO: TLabel;
    LbOrdem: TLabel;
    EdtOrdem: TEdit;
    Panel3: TPanel;
    btexcluircontagerencial: TButton;
    btadicionarcontagerencial: TButton;
    lbnomecontagerencial: TLabel;
    edtcontagerencial: TEdit;
    Label1: TLabel;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure btadicionarcontagerencialClick(Sender: TObject);
    procedure btexcluircontagerencialClick(Sender: TObject);
    procedure edtcontagerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcontagerencialExit(Sender: TObject);
    procedure btgerarplanilhaClick(Sender: TObject);
    procedure btrastrearClick(Sender: TObject);
    procedure DBGridlinhasDblClick(Sender: TObject);
  private
         ObjContaGerencialFLUXOCAIXA:TObjContaGerencialFLUXOCAIXA;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         Procedure Retornacontas;
         Procedure AtualizaLInhas;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFLUXOCAIXA: TFFLUXOCAIXA;


implementation

uses UessencialGlobal, Upesquisa;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFFLUXOCAIXA.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_Nome(edtNome.text);
        Submit_Ordem(edtOrdem.text);
        Submit_DetalhaCredorDevedor(Submit_ComboBox(comboDetalhaCredorDevedor));
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFFLUXOCAIXA.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNome.text:=Get_Nome;
        EdtOrdem.text:=Get_Ordem;
        if (Get_DetalhaCredorDevedor='S')
        Then comboDetalhaCredorDevedor.ItemIndex:=1
        Else comboDetalhaCredorDevedor.ItemIndex:=0;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFFLUXOCAIXA.TabelaParaControles: Boolean;
begin
     If (Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFFLUXOCAIXA.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjContaGerencialFLUXOCAIXA:=TObjContaGerencialFLUXOCAIXA.create;
        Self.DbGridContaGerencial.DataSource:=Self.ObjContaGerencialFLUXOCAIXA.ObjDatasource;
        Self.DbgridLinhas.datasource:=Self.objcontagerencialfluxocaixa.fluxocaixa.objdatasource;
        Self.AtualizaLInhas;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

procedure TFFLUXOCAIXA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjContaGerencialFLUXOCAIXA=Nil)
     Then exit;

      If (Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.status<>dsinactive)
      Then Begin
              Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
              abort;
              exit;
     End;

    Self.ObjContaGerencialFLUXOCAIXA.free;
end;

procedure TFFLUXOCAIXA.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFFLUXOCAIXA.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.status:=dsInsert;
     Guia.pageindex:=0;
     EdtOrdem.setfocus;

end;


procedure TFFLUXOCAIXA.btalterarClick(Sender: TObject);
begin
    If (Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtordem.setfocus;

          End;


end;

procedure TFFLUXOCAIXA.btgravarClick(Sender: TObject);
begin

     If Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Self.AtualizaLInhas;

end;

procedure TFFLUXOCAIXA.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Self.AtualizaLInhas;


end;

procedure TFFLUXOCAIXA.btcancelarClick(Sender: TObject);
begin
     Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     Self.AtualizaLInhas;

end;

procedure TFFLUXOCAIXA.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFFLUXOCAIXA.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.Get_pesquisa,Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.status<>dsinactive
                                  then exit;

                                  If (Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFFLUXOCAIXA.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFFLUXOCAIXA.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFFLUXOCAIXA.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     if (NewTab=1)
     Then Begin
               if (Self.EdtCODIGO.Text='') or (Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.Status=dsInsert)
               Then Begin
                        AllowChange:=False;
                        exit;
               End;


               edtcontagerencial.text:='';
               lbnomecontagerencial.caption:='';
               edtcontagerencial.Enabled:=True;
               btadicionarcontagerencial.Enabled:=true;
               btexcluircontagerencial.Enabled:=true;

               Self.RetornaContas;
               
     End;
end;

procedure TFFLUXOCAIXA.Retornacontas;
begin
     Self.ObjContaGerencialFLUXOCAIXA.RetornaContas(EdtCODIGO.text);
     formatadbgrid(DbGridContaGerencial);
end;

procedure TFFLUXOCAIXA.btadicionarcontagerencialClick(Sender: TObject);
begin
     Self.ObjContaGerencialFLUXOCAIXA.Status:=dsinsert;
     Self.ObjContaGerencialFLUXOCAIXA.Submit_CODIGO('0');
     Self.ObjContaGerencialFLUXOCAIXA.Contagerencial.Submit_CODIGO(edtcontagerencial.text);
     Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.Submit_CODIGO(EdtCODIGO.Text);
     if (Self.ObjContaGerencialFLUXOCAIXA.Salvar(true)=false)
     Then exit;
     lbnomecontagerencial.caption:='';
     edtcontagerencial.text:='';
     
     Self.Retornacontas;
     edtcontagerencial.SetFocus;
end;

procedure TFFLUXOCAIXA.btexcluircontagerencialClick(Sender: TObject);
begin
     if (DbGridContaGerencial.DataSource.DataSet.RecordCount=0)
     then exit;

     if (MensagemPergunta('Tem certeza que deseja excluir?')=mrno)
     Then exit;

     self.ObjContaGerencialFLUXOCAIXA.Exclui(DbGridContaGerencial.DataSource.DataSet.fieldbyname('codigo').asstring,true);

     self.Retornacontas;
     edtcontagerencial.SetFocus;


end;

procedure TFFLUXOCAIXA.edtcontagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjContaGerencialFLUXOCAIXA.EdtContagerencialKeyDown(sender,key,shift,lbnomecontagerencial);
end;

procedure TFFLUXOCAIXA.edtcontagerencialExit(Sender: TObject);
begin
     Self.ObjContaGerencialFLUXOCAIXA.EdtContagerencialExit(sender,lbnomecontagerencial);
end;

procedure TFFLUXOCAIXA.btgerarplanilhaClick(Sender: TObject);
begin
     Self.ObjContaGerencialFLUXOCAIXA.GeraPlanilha;
end;

procedure TFFLUXOCAIXA.btrastrearClick(Sender: TObject);
begin
     Self.ObjContaGerencialFLUXOCAIXA.rastrearcontas;
end;

procedure TFFLUXOCAIXA.AtualizaLInhas;
begin
     Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.AtualizaLinhas;
     formatadbgrid(Self.DBGridlinhas);
end;

procedure TFFLUXOCAIXA.DBGridlinhasDblClick(Sender: TObject);
var
pcodigo:string;
begin
     if (Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.Status<>dsinactive)
     then exit;

     if (self.DBGridlinhas.DataSource.DataSet.RecordCount=0)
     Then exit;

     pcodigo:=self.DBGridlinhas.DataSource.DataSet.fieldbyname('codigo').asstring;

     Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.LocalizaCodigo(pcodigo);
     Self.ObjContaGerencialFLUXOCAIXA.FluxoCaixa.TabelaparaObjeto;
     Self.ObjetoParaControles;
     btalterarClick(sender);
end;

end.

