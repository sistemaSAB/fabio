unit UDevolucaoNFE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, Buttons, ExtCtrls, Grids, DBGrids, DB,
  IBCustomDataSet, IBDatabase, IBQuery, DBCtrls,UobjDevolucaoNfe,UDataModulo,
  Menus, ComCtrls, pngimage,UobjTransmiteNFE;

type
  TfDevolucaoNFE = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btexcluir: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btOpcoes: TBitBtn;
    BtSair: TBitBtn;
    pnlCabecalho: TPanel;
    lbBaixaEstoque: TLabel;
    lb2: TLabel;
    Label6: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    edtHoraSaida: TMaskEdit;
    edtDataSaida: TMaskEdit;
    edtDataEmissao: TMaskEdit;
    chkBaixaEstoque: TCheckBox;
    Label18: TLabel;
    lbNFE: TLabel;
    Label25: TLabel;
    lbNota: TLabel;
    Label5: TLabel;
    Splitter1: TSplitter;
    pnlProdutos: TPanel;
    v: TLabel;
    comboTipo: TComboBox;
    Label9: TLabel;
    edtProduto: TEdit;
    label55: TLabel;
    edtCor: TEdit;
    Label10: TLabel;
    edtQuantidade: TEdit;
    Label11: TLabel;
    edtValorproduto: TEdit;
    Label12: TLabel;
    edtDesconto: TEdit;
    Label23: TLabel;
    edtValorFrete: TEdit;
    Label20: TLabel;
    lbDescricaoProduto: TLabel;
    Label22: TLabel;
    lbCor: TLabel;
    btGravarProduto: TBitBtn;
    btCancelarProduto: TBitBtn;
    btExcluirProduto: TBitBtn;
    DBGridProdutos: TDBGrid;
    dts1: TIBDataSet;
    queryInsereProduto: TIBQuery;
    ds2: TDataSource;
    Label1: TLabel;
    DBCliente: TDBEdit;
    ds1: TDataSource;
    Label14: TLabel;
    DBFornecedor: TDBEdit;
    Label2: TLabel;
    DBTransp: TDBEdit;
    lbcliente: TLabel;
    lbfornecedor: TLabel;
    lbtransportadora: TLabel;
    OpenDialog1: TOpenDialog;
    DBChaveRef: TDBEdit;
    Label8: TLabel;
    lbChaveRef: TLabel;
    PopupMenu1: TPopupMenu;
    CriarReferencia1: TMenuItem;
    DBMemo1: TDBMemo;
    ComboIndPag: TComboBox;
    StatusBar1: TStatusBar;
    dts2: TIBDataSet;
    Label7: TLabel;
    edtcfop: TEdit;
    Panel1: TPanel;
    lbGera: TLabel;
    Label13: TLabel;
    lbTotalProdutos: TLabel;
    Label16: TLabel;
    lbTotalDescontos: TLabel;
    Label19: TLabel;
    lbTotalNF: TLabel;
    dts1CODIGO: TIntegerField;
    dts1NOTA: TIntegerField;
    dts1TRANSPORTADORA: TIntegerField;
    dts1FORNECEDOR: TIntegerField;
    dts1DATAEMISSAO: TDateField;
    dts1DATASAIDA: TDateField;
    dts1HORASAIDA: TTimeField;
    dts1INFORMACOES: TIBStringField;
    dts1CLIENTE: TIntegerField;
    dts1BAIXAESTOQUE: TIBStringField;
    dts1ICMSSOBREFRETE: TIBStringField;
    dts1AVISTA: TIBStringField;
    dts1CRT: TIBStringField;
    dts1INDPAG: TIBStringField;
    dts1CHAVE_DEVOLUCAO: TIBStringField;
    Label15: TLabel;
    Image2: TImage;
    procedure FormShow(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure BtSairClick(Sender: TObject);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure DBClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBClienteExit(Sender: TObject);
    procedure DBFornecedorExit(Sender: TObject);
    procedure DBTranspExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure DBFornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBTranspKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtChaveExit(Sender: TObject);
    procedure edtHoraSaidaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDataSaidaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDataEmissaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBChaveRefKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBChaveRefExit(Sender: TObject);
    procedure CriarReferencia1Click(Sender: TObject);
    procedure lbChaveRefContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure comboTipoEnter(Sender: TObject);
    procedure comboTipoExit(Sender: TObject);
    procedure comboTipoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtProdutoKeyPress(Sender: TObject; var Key: Char);
    procedure btGravarProdutoClick(Sender: TObject);
    procedure edtCorExit(Sender: TObject);
    procedure edtCorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btExcluirProdutoClick(Sender: TObject);
    procedure btCancelarProdutoClick(Sender: TObject);
    procedure edtQuantidadeKeyPress(Sender: TObject; var Key: Char);
    procedure edtValorprodutoKeyPress(Sender: TObject; var Key: Char);
    procedure edtDescontoKeyPress(Sender: TObject; var Key: Char);
    procedure edtValorFreteKeyPress(Sender: TObject; var Key: Char);
    procedure edtProdutoExit(Sender: TObject);
    procedure lbGeraClick(Sender: TObject);
  private

    objTransmiteNFE:TObjTransmiteNFE;
    chave_acesso:string;
    objDevolucaoNfe:TObjDevolucaoNfe;
    selectsql_dts1:string;
    insertSql_dts2:string;
    selectSql_dts2:string;

    procedure limpaLabels();
    procedure limpaLabelsRodape();
    procedure limpaLabelsProduto();
    procedure criaReferencia(const pChave:string);
    procedure dtsParaControles();
    procedure ControlesParaDts();
    procedure retornaProdutos(pCodigo:string);
    procedure validaPnlProdutos();
    procedure atualizaLabelsRodape();

    function pesquisa(const codigo:String):Boolean;
    function getTabela(index:Integer):string;

  public
    { Public declarations }
  end;

var
  fDevolucaoNFE: TfDevolucaoNFE;

implementation

uses UescolheImagemBotao, UessencialGlobal, Upesquisa,
  uAcertaImpostoSafira, UComponentesNfe, UobjExtraiXML, ufrmStatus;

{$R *.dfm}

procedure TfDevolucaoNFE.FormShow(Sender: TObject);
var
  nomeTabela:string;
begin

  FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);

  FescolheImagemBotao.PegaFiguraBotaopequeno(btgravarproduto,'BOTAOINSERIR.BMP');
  FescolheImagemBotao.PegaFiguraBotaopequeno(btcancelarproduto,'BOTAOCANCELARPRODUTO.BMP');
  FescolheImagemBotao.PegaFiguraBotaopequeno(btexcluirproduto,'BOTAORETIRAR.BMP');

  selectsql_dts1 := 'Select * from TABDEVOLUCAONFE';

  dts1.Database := FDataModulo.IBDatabase;
  dts2.Database := dts1.Database;
  queryInsereProduto.Database := dts1.Database;

  objDevolucaoNfe := TObjDevolucaoNfe.create(self);

  self.limpaLabels;
  self.limpaLabelsRodape;
  desabilita_campos(pnlProdutos);

end;

procedure TfDevolucaoNFE.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

  Try

    Fpesquisalocal:=Tfpesquisa.create(Self);
    FpesquisaLocal.NomeCadastroPersonalizacao:='FDEVOLUCAONFE.PESQUISA';

    dts1.SelectSQL.Text := self.selectsql_dts1;
    If (FpesquisaLocal.PreparaPesquisa(dts1.SelectSQL.Text,'Pesquisa de devolu��es (NF-e)',Nil)) Then
    Begin

      Try

        If (FpesquisaLocal.showmodal=mrok) Then
        Begin

          If (Self.dts1.State <> dsinactive) then exit;

          If not self.pesquisa(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring) then
          Begin
            MensagemErro ('Dados n�o encontrados!');
            exit;
          End;

          self.dtsParaControles();

          retornaProdutos(lbCodigo.Caption);
          limpaLabelsProduto;

        End;

      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
      
    End;

  Finally
    FreeandNil(FPesquisaLocal);
  End;

end;

procedure TfDevolucaoNFE.BtSairClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfDevolucaoNFE.BtnovoClick(Sender: TObject);
begin

  self.limpaLabels;
  limpaedit(pnlCabecalho);

  ComboIndPag.ItemIndex := 0;

  if not dts1.Active then
    dts1.Active := True;
    
  dts1.Insert;
  DBCliente.SetFocus;
  esconde_botoes(panelbotes);

  Btnovo.Visible     := True;
  btcancelar.Visible := True;
  btgravar.Visible   := true;

  self.edtHoraSaida.Text := formatdatetime('hh:mm:ss',now);
  self.edtDataSaida.Text := formatdatetime('dd/mm/yyyy',now);
  self.edtDataEmissao.Text := formatdatetime('dd/mm/yyyy',now);

  limpaedit(pnlProdutos);
  desabilita_campos(pnlProdutos);
  limpaLabelsProduto;
  dts2.Close;
  
end;

procedure TfDevolucaoNFE.btalterarClick(Sender: TObject);
begin
  dts1.Edit;
  esconde_botoes(panelbotes);

  btgravar.Visible   := True;
  btcancelar.Visible := true;
  
end;

procedure TfDevolucaoNFE.btgravarClick(Sender: TObject);
begin

  if dts1.FieldByName('chave_devolucao').AsString = '' then
  begin
    ShowMessage('Informe a referencia no campo: NF-e REF.');
    Exit;
  end;

  ControlesParaDts;

  dts1.Post;
  FDataModulo.IBTransaction.CommitRetaining;
  mostra_botoes(panelbotes);

  dtsParaControles;

  validaPnlProdutos;

end;

procedure TfDevolucaoNFE.btcancelarClick(Sender: TObject);
begin

  dts1.Cancel;
  dts1.Close;
  mostra_botoes(panelbotes);
  self.limpaLabels;
  limpaedit(pnlCabecalho);

  limpaedit(pnlProdutos);
  desabilita_campos(pnlProdutos);
  limpaLabelsProduto;
  limpaLabelsRodape;
  dts2.Close;

end;

procedure TfDevolucaoNFE.btexcluirClick(Sender: TObject);
begin

  if (lbNFE.Caption = '') and (lbNota.Caption = '') then
  begin

    try

      exec_sql('delete from tabproddevolucaonfe where nfedevolucao = '+lbCodigo.Caption);

      dts1.Delete;
      FDataModulo.IBTransaction.CommitRetaining;
      mostra_botoes(panelbotes);
      btcancelarClick(btcancelar);

    except
      on e:Exception do
      begin
        ShowMessage(e.Message);
        FDataModulo.IBTransaction.RollbackRetaining;
      end;
    end

  end

end;                                                

procedure TfDevolucaoNFE.DBClienteKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  objDevolucaoNfe.EdtCLIENTEKeyDown(Sender,Key,Shift,lbcliente);    
end;

procedure TfDevolucaoNFE.FormClose(Sender: TObject;var Action: TCloseAction);
begin
  FreeAndNil(objDevolucaoNfe);
  FreeAndNil(objTransmiteNFE);
end;

procedure TfDevolucaoNFE.DBClienteExit(Sender: TObject);
begin
  lbcliente.Caption := get_campoTabela('nome','codigo','TABCLIENTE',DBCliente.Text);
end;

procedure TfDevolucaoNFE.DBFornecedorExit(Sender: TObject);
begin
  lbfornecedor.Caption := get_campoTabela('RAZAOSOCIAL','codigo','TABFORNECEDOR',DBFornecedor.Text);
end;

procedure TfDevolucaoNFE.DBTranspExit(Sender: TObject);
begin
  lbtransportadora.Caption := get_campoTabela('nome','codigo','TABTRANSPORTADORA',DBTransp.Text);
end;

procedure TfDevolucaoNFE.FormKeyPress(Sender: TObject; var Key: Char);
begin

  if (key = #13) then
    Perform(Wm_NextDlgCtl,0,0)
  else if (Key = #27) then
    self.Close

end;

procedure TfDevolucaoNFE.limpaLabels;
begin

  lbCodigo.Caption         := '';
  lbNFE.Caption            := '';
  lbNota.Caption           := '';
  lbcliente.Caption        := '';
  lbfornecedor.Caption     := '';
  lbtransportadora.Caption := '';
  lbChaveRef.Caption       := '';
  
end;

procedure TfDevolucaoNFE.DBFornecedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  objDevolucaoNfe.EdtFORNECEDORKeyDown(Sender,Key,Shift,lbfornecedor);
end;

procedure TfDevolucaoNFE.DBTranspKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    objDevolucaoNfe.EdtTRANSPORTADORAKeyDown(Sender,Key,Shift,lbtransportadora);
end;

procedure TfDevolucaoNFE.criaReferencia(const pChave: string);
begin

  {if (self.chave_acesso <> '') or (lbChaveRef.Caption <> '') then
  begin
    memoInformacoes.Clear;
    memoInformacoes.Lines.Add('Devolu��o de NF-e referente a chave de acesso: ');
    memoInformacoes.Lines.Add(pChave);
  end}

  if (self.chave_acesso <> '') or (lbChaveRef.Caption <> '') then
  begin
    DBMemo1.Clear;
    DBMemo1.Lines.Add('Devolu��o de NF-e referente a chave de acesso: ');
    DBMemo1.Lines.Add(pChave);
  end
  
end;

procedure TfDevolucaoNFE.edtChaveExit(Sender: TObject);
begin
  self.criaReferencia(self.chave_acesso);
end;

procedure TfDevolucaoNFE.edtHoraSaidaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if (Key = vk_space) then
    self.edtHoraSaida.Text := formatdatetime('hh:mm:ss',now);
end;

procedure TfDevolucaoNFE.edtDataSaidaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = vk_space) then
    self.edtDataSaida.Text := formatdatetime('dd/mm/yyyy',now);
end;

procedure TfDevolucaoNFE.edtDataEmissaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = vk_space) then
    self.edtDataEmissao.Text := formatdatetime('dd/mm/yyyy',now);
end;

function TfDevolucaoNFE.pesquisa(const codigo:String): Boolean;
begin

  dts1.SelectSQL.Text := 'Select * from TABDEVOLUCAONFE where codigo = :codigo';
  dts1.Params[0].AsString := Codigo;
  dts1.Open;
  Result := (dts1.RecordCount > 0);
  //dts1.SelectSQL.Text := self.select_sql;

end;

procedure TfDevolucaoNFE.dtsParaControles;
begin

  DBClienteExit(DBCliente);
  DBFornecedorExit(DBFornecedor);
  DBTranspExit(DBTransp);
  DBChaveRefExit(DBChaveRef);

  lbCodigo.Caption := dts1.fieldbyname('codigo').AsString;

  edtHoraSaida.Text   := dts1.fieldbyname('horasaida').AsString;
  edtDataSaida.Text   := dts1.fieldbyname('datasaida').AsString;
  edtDataEmissao.Text := dts1.fieldbyname('dataemissao').AsString;

  if Trim(dts1.FieldByName('indpag').AsString) <> '' then
    ComboIndPag.ItemIndex := dts1.FieldByName('indpag').AsInteger;

  atualizaLabelsRodape;

end;

procedure TfDevolucaoNFE.DBChaveRefKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin

  chave_acesso := '';

  if Key = vk_f9 then
  begin

    if OpenDialog1.Execute then
    begin
      chave_acesso := RetornaValorCampos(openDialog1.FileName,'chNFe');
      lbChaveRef.Caption := chave_acesso;
      self.criaReferencia(chave_acesso);
      dts1.FieldByName('chave_devolucao').AsString := chave_acesso;
    end;

  end;
  
end;

procedure TfDevolucaoNFE.DBChaveRefExit(Sender: TObject);
begin
  if lbChaveRef.Caption = '' then
    lbChaveRef.Caption := dts1.fieldByName('chave_devolucao').AsString;
end;

procedure TfDevolucaoNFE.CriarReferencia1Click(Sender: TObject);
begin
  dts1.Edit;
  self.criaReferencia(lbChaveRef.Caption);
  dts1.Post;
  FDataModulo.IBTransaction.CommitRetaining;
end;

procedure TfDevolucaoNFE.lbChaveRefContextPopup(Sender: TObject;MousePos: TPoint; var Handled: Boolean);
begin
  CriarReferencia1.Enabled := (lbChaveRef.Caption <> '');
end;

procedure TfDevolucaoNFE.ControlesParaDts;
begin

  dts1.FieldByName('indpag').AsString      := ComboIndPag.Text;
  dts1.FieldByName('horasaida').AsDateTime := StrToTime(edtHoraSaida.Text);
  dts1.FieldByName('datasaida').AsDateTime := StrToDate(edtDataSaida.Text);
  dts1.FieldByName('dataemissao').AsDateTime := StrToDate(edtDataEmissao.Text);

end;                                                                             

procedure TfDevolucaoNFE.retornaProdutos(pCodigo: string);
begin

  dts2.Close;
  dts2.Params[0].AsString := pCodigo;
  dts2.Open;

  if lbNFE.Caption <> '' then
    pnlProdutos.Enabled := False;

  validaPnlProdutos();

  self.DBGridProdutos.Columns[1].Width:=200;

end;

procedure TfDevolucaoNFE.comboTipoEnter(Sender: TObject);
begin
  TComboBox(sender).Color:=clInactiveCaption;
end;

procedure TfDevolucaoNFE.comboTipoExit(Sender: TObject);
begin
  TComboBox(sender).Color:=$005CADFE;
end;

procedure TfDevolucaoNFE.comboTipoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  if (Key = 40) then {seta para baixo}
    self.comboTipo.DroppedDown := True;
    
end;

procedure TfDevolucaoNFE.edtProdutoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  if Key = vk_f9 then
  begin

    case (self.comboTipo.ItemIndex) of

      0:Exit;

      1:self.objDevolucaoNfe.EdtFERRAGEMKeyDown(sender,key,Shift,lbDescricaoProduto);
      2:self.objDevolucaoNfe.EdtPERFILADOKeyDown(sender,key,Shift,lbDescricaoProduto);
      3:self.objDevolucaoNfe.EdtVIDROKeyDown(sender,key,Shift,lbDescricaoProduto);
      4:self.objDevolucaoNfe.EdtKITBOXKeyDown(sender,key,Shift,lbDescricaoProduto);
      5:self.objDevolucaoNfe.EdtPERSIANAKeyDown(sender,key,Shift,lbDescricaoProduto);
      6:self.objDevolucaoNfe.EdtDIVERSOKeyDown(sender,key,Shift,lbDescricaoProduto);

    end;

  end

end;

procedure TfDevolucaoNFE.edtProdutoKeyPress(Sender: TObject;var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8)]) then
      Key:= #0;
end;

procedure TfDevolucaoNFE.btGravarProdutoClick(Sender: TObject);
var
  nomeTabela,sql1,sql2:string;
  valorTotal,valorFinal:Currency;
begin

  if not IsNumeric(edtQuantidade.Text) then Exit;
  if not IsNumeric(edtValorproduto.Text) then Exit;

  nomeTabela := getTabela(comboTipo.ItemIndex);
  nomeTabela := Copy(nomeTabela,4,length(nomeTabela));

  insertSql_dts2 := 'insert into tabproddevolucaonfe(material,'+nomeTabela+',cor,quantidade,valor,desconto,valorfrete,codigo,nome_produto,nfedevolucao,cfop,valortotal,valorfinal) '+
                 'values (:material,:'+nomeTabela+',:cor,:quantidade,:valor,:desconto,:valorfrete,:codigo,:nome_produto,:nfedevolucao,:cfop,:valortotal,:valorfinal)';

  selectSql_dts2 := 'select pnfe.material,pnfe.diverso,pnfe.perfilado,pnfe.ferragem,pnfe.vidro, '+
               'pnfe.componente,pnfe.kitbox,pnfe.persiana,pnfe.cor,pnfe.quantidade,pnfe.valor, '+
               'pnfe.desconto,pnfe.valorfrete,pnfe.codigo,pnfe.nome_produto '+
               'from tabproddevolucaonfe pnfe '+
               'where pnfe.codigo = -5';


  queryInsereProduto.Active := False;
  queryInsereProduto.SQL.Text := insertSql_dts2;


  queryInsereProduto.ParamByName('material').AsString     := IntToStr(comboTipo.ItemIndex);
  queryInsereProduto.ParamByName(nomeTabela).AsString     := edtProduto.Text;
  queryInsereProduto.ParamByName('cor').AsString          := edtCor.Text;
  queryInsereProduto.ParamByName('quantidade').AsString   := format_db(edtQuantidade.Text);
  queryInsereProduto.ParamByName('valor').AsString        := format_db(edtValorproduto.Text);
  queryInsereProduto.ParamByName('desconto').AsString     := format_db(edtDesconto.Text);
  queryInsereProduto.ParamByName('valorfrete').AsString   := format_db(edtValorFrete.Text);
  queryInsereProduto.ParamByName('codigo').AsString       := '0';
  queryInsereProduto.ParamByName('nome_produto').AsString := lbDescricaoProduto.Caption;
  queryInsereProduto.ParamByName('nfedevolucao').AsString := lbCodigo.Caption;
  queryInsereProduto.ParamByName('cfop').AsString         := edtcfop.Text;

  valorTotal := StrToCurr(edtQuantidade.Text) * StrToCurr(edtValorproduto.Text);
  valorFinal := valorTotal - StrToCurrDef(edtDesconto.Text,0);

  queryInsereProduto.ParamByName('valortotal').AsCurrency := valorTotal;
  queryInsereProduto.ParamByName('valorfinal').AsCurrency := valorFinal;
                                                                                     

  try

    queryInsereProduto.ExecSQL;
    FDataModulo.IBTransaction.CommitRetaining;
    retornaProdutos(lbCodigo.Caption);
    limpaLabelsProduto;
    comboTipo.SetFocus;
    comboTipo.ItemIndex := 0;
    
  except

    on e:Exception do
    begin
      ShowMessage(e.Message);
      FDataModulo.IBTransaction.RollbackRetaining;
    end
    
  end;

  limpaLabelsProduto();
  limpaedit(pnlProdutos);
  atualizaLabelsRodape;

end;

function TfDevolucaoNFE.getTabela(index: Integer): string;
begin
   case (comboTipo.ItemIndex) of
     1:result := 'TABFERRAGEM';
     2:result := 'TABPERFILADO';
     3:result := 'TABVIDRO';
     4:result := 'TABKITBOX';
     5:result := 'TABPERSIANA';
     6:result := 'TABDIVERSO';
   else ;
    result := '';
   end;
end;

procedure TfDevolucaoNFE.edtCorExit(Sender: TObject);
var
  QueryPreco:TIBQuery;
begin
     if(edtCor.Text='')
     then Exit;
     
     QueryPreco:=TIBQuery.Create(nil);
     QueryPreco.Database:=FDataModulo.IBDatabase;
     try
         with QueryPreco do
         begin
              case (Self.comboTipo.ItemIndex) of
                1:
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select (tabferragem.precovendainstalado+(tabferragem.precovendainstalado * tabferragemcor.porcentagemacrescimofinal)/100) as valor');
                      SQL.Add('from tabferragem');
                      SQL.Add('join tabferragemcor on tabferragemcor.ferragem=tabferragem.codigo');
                      SQL.Add('where tabferragem.codigo='+edtproduto.Text);
                      SQL.Add('and tabferragemcor.cor='+edtCor.Text);
                      Open;
                      edtValorproduto.Text:=fieldbyname('valor').asstring;
                end;
                2:
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select (tabperfilado.precovendainstalado+(tabperfilado.precovendainstalado * tabperfiladocor.porcentagemacrescimofinal)/100) as valor');
                      SQL.Add('from tabperfilado');
                      SQL.Add('join tabperfiladocor on tabperfiladocor.perfilado=tabperfilado.codigo');
                      SQL.Add('where tabperfilado.codigo='+edtproduto.Text);
                      SQL.Add('and tabperfiladocor.cor='+edtCor.Text);
                      Open;
                      edtValorproduto.Text:=fieldbyname('valor').asstring;
                end;
                3:
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select (tabvidro.precovendainstalado+(tabvidro.precovendainstalado * tabvidrocor.porcentagemacrescimofinal)/100) as valor');
                      SQL.Add('from tabvidro');
                      SQL.Add('join tabvidrocor on tabvidrocor.vidro=tabvidro.codigo');
                      SQL.Add('where tabvidro.codigo='+edtproduto.Text);
                      SQL.Add('and tabvidrocor.cor='+edtCor.Text);
                      Open;
                      edtValorproduto.Text:=fieldbyname('valor').asstring;
                end;
                4:
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select (tabkitbox.precovendainstalado+(tabkitbox.precovendainstalado * tabkitboxcor.porcentagemacrescimofinal)/100) as valor');
                      SQL.Add('from tabkitbox');
                      SQL.Add('join tabkitboxcor on tabkitboxcor.kitbox=tabkitbox.codigo');
                      SQL.Add('where tabkitbox.codigo='+edtproduto.Text);
                      SQL.Add('and tabkitboxcor.cor='+edtCor.Text);
                      Open;
                      edtValorproduto.Text:=fieldbyname('valor').asstring;
                end;
                5:
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select (tabpersiana.precovendainstalado+(tabpersiana.precovendainstalado * tabpersianagrupodiametrocor.porcentagemacrescimofinal)/100) as valor');
                      SQL.Add('from tabpersiana');
                      SQL.Add('join tabpersianagrupodiametrocor on tabpersianagrupodiametrocor.persiana=tabpersiana.codigo');
                      SQL.Add('where tabpersiana.codigo='+edtproduto.Text);
                      SQL.Add('and tabpersianagrupodiametrocor.cor='+edtCor.Text);
                      Open;
                      edtValorproduto.Text:=fieldbyname('valor').asstring;
                end;
                6:
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select (tabdiverso.precovendainstalado+(tabdiverso.precovendainstalado * tabdiversocor.porcentagemacrescimofinal)/100) as valor');
                      SQL.Add('from tabdiverso');
                      SQL.Add('join tabdiversocor on tabdiversocor.diverso=tabdiverso.codigo');
                      SQL.Add('where tabdiverso.codigo='+edtproduto.Text);
                      SQL.Add('and tabdiversocor.cor='+edtCor.Text);
                      Open;
                      edtValorproduto.Text:=fieldbyname('valor').asstring;
                end;
              else

                MensagemAviso('Tipo de material n�o conhecido. Tipo: '+IntToStr(self.comboTipo.ItemIndex));

              end;
         end;
     finally
          FreeAndNil(QueryPreco);
     end;

  edtQuantidade.Text := '1';
  edtDesconto.Text := '0';
  edtValorFrete.Text := '0';

end;

procedure TfDevolucaoNFE.limpaLabelsProduto;
begin

  comboTipo.ItemIndex := 0;
  lbDescricaoProduto.Caption := '';
  lbCor.Caption := '';

end;

procedure TfDevolucaoNFE.validaPnlProdutos;
begin

  pnlProdutos.Enabled := (lbNFE.Caption = '');

  if (pnlProdutos.Enabled) then
  begin
    habilita_campos(pnlProdutos);
    comboTipo.SetFocus;
  end;
  
end;

procedure TfDevolucaoNFE.edtCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
  nomeTabela:string;
begin

  if (Key <> vk_f9) or (comboTipo.ItemIndex = 5) then Exit;
  nomeTabela := getTabela(comboTipo.ItemIndex);
  self.objDevolucaoNfe.pesquisaCor(self.edtProduto.Text,Copy(nomeTabela,4,Length(nomeTabela)),'codigo',nomeTabela,lbCor,sender);

end;

procedure TfDevolucaoNFE.btExcluirProdutoClick(Sender: TObject);
begin

  if Trim(DBGridProdutos.DataSource.DataSet.fieldbyname('codigo').AsString) = '' then Exit;

  exec_sql('delete from tabproddevolucaonfe where codigo = '+DBGridProdutos.DataSource.DataSet.fieldbyname('codigo').AsString);
  FDataModulo.IBTransaction.CommitRetaining;

  retornaProdutos(lbCodigo.Caption);
  limpaLabelsProduto;
  limpaedit(pnlProdutos);

end;

procedure TfDevolucaoNFE.btCancelarProdutoClick(Sender: TObject);
begin

  limpaLabelsProduto;
  limpaedit(pnlProdutos);
  comboTipo.SetFocus;

end;

procedure TfDevolucaoNFE.edtQuantidadeKeyPress(Sender: TObject;
  var Key: Char);
begin
  ValidaNumeros(tedit(Sender),Key,'float');
end;

procedure TfDevolucaoNFE.edtValorprodutoKeyPress(Sender: TObject;
  var Key: Char);
begin
  ValidaNumeros(tedit(Sender),Key,'float');
end;

procedure TfDevolucaoNFE.edtDescontoKeyPress(Sender: TObject;
  var Key: Char);
begin
  ValidaNumeros(tedit(Sender),Key,'float');
end;

procedure TfDevolucaoNFE.edtValorFreteKeyPress(Sender: TObject;
  var Key: Char);
begin
  ValidaNumeros(tedit(Sender),Key,'float');
end;

procedure TfDevolucaoNFE.edtProdutoExit(Sender: TObject);
var
  nomeTabela:string;
begin

  nomeTabela := getTabela(comboTipo.ItemIndex);

  if comboTipo.ItemIndex <> 5 then
    lbDescricaoProduto.Caption := get_campoTabela('descricao','codigo',nomeTabela,edtProduto.Text)
  else
    lbDescricaoProduto.Caption := get_campoTabela('nome','codigo',nomeTabela,edtProduto.Text);

  if comboTipo.ItemIndex = 5 then
    edtQuantidade.SetFocus;

end;

procedure TfDevolucaoNFE.atualizaLabelsRodape;
begin

  lbTotalProdutos.Caption  :=  get_valorF (get_campoTabela('SUM(valortotal) as tot','nfedevolucao','TABPRODDEVOLUCAONFE',lbCodigo.Caption,'tot'));
  lbTotalDescontos.Caption :=  get_valorF (get_campoTabela('SUM(desconto) as tot','nfedevolucao','TABPRODDEVOLUCAONFE',lbCodigo.Caption,'tot'));
  lbTotalNF.Caption        :=  get_valorF (get_campoTabela('SUM(valorfinal) as tot','nfedevolucao','TABPRODDEVOLUCAONFE',lbCodigo.Caption,'tot'));

end;

procedure TfDevolucaoNFE.limpaLabelsRodape;
begin
  lbTotalProdutos.Caption  := '0,00 R$';
  lbTotalDescontos.Caption := '0,00 R$';
  lbTotalNF.Caption := '0,00 R$';
end;

procedure TfDevolucaoNFE.lbGeraClick(Sender: TObject);
begin

  if (UpperCase(lbGera.Caption) = 'IMPRIME DANFE') then
  begin
    objTransmiteNFE.imprimeDanfe(get_campoTabela('ARQUIVO_XML','CODIGO','TABNFE',lbNFE.Caption));
    Exit;
  end;

  if (lbNota.Caption <> '') then
    Exit;

  if (lbcodigo.Caption<>'') then
  begin

    (*if not (objp.GerarNF_para_NFE(lbcodigo.Caption)) then
    begin
      frmStatus.Close;
      MensagemErro('N�o foi possivel gerar NF-e');
      FDataModulo.IBTransaction.RollbackRetaining;
    end
    else
    begin
      frmStatus.Close;
      FDataModulo.IBTransaction.CommitRetaining;
    end;*)

    //objDevolucaoNfe

    dtsParaControles;

  end;
end;

end.
