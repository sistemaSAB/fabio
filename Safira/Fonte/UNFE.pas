unit UNFE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjNFE,
  jpeg, OleCtrls, SHDocVw;

type
  TFNFE = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbSTATUSNOTA: TLabel;
    EdtSTATUSNOTA: TEdit;
    LbARQUIVO: TLabel;
    EdtARQUIVO: TEdit;
    LbPROTOCOLOCANCELAMENTO: TLabel;
    EdtPROTOCOLOCANCELAMENTO: TEdit;
    LbARQUIVOCANCELAMENTO: TLabel;
    EdtARQUIVOCANCELAMENTO: TEdit;
    WBResposta: TWebBrowser;
    Panel2: TPanel;
    BTXMLNF: TButton;
    btXMLCANC: TButton;
    edtdatacancelamento: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtmotivocancelamento: TEdit;
    Label3: TLabel;
    edtcertificado: TEdit;
    Label4: TLabel;
    edtReciboEnvio: TEdit;
    BTXMLINU: TButton;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    edtprotocoloinutilizacao: TEdit;
    edtarquivoinutilizacao: TEdit;
    edtdatainutilizacao: TMaskEdit;
    edtmotivoinutilizacao: TEdit;
    btresgatacampo: TButton;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BTXMLNFClick(Sender: TObject);
    procedure btXMLCANCClick(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure BTXMLINUClick(Sender: TObject);
    procedure btresgatacampoClick(Sender: TObject);
  private
         ObjNFE:TObjNFE;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
         Procedure PassaObjeto(Objeto:TobjNfe);
  end;

var
  FNFE: TFNFE;


implementation

uses UessencialGlobal, Upesquisa, UDataModulo,  UComponentesNfe;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFNFE.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjNFE do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_STATUSNOTA(edtSTATUSNOTA.text);
        Submit_CERTIFICADO(edtCERTIFICADO.text);
        Submit_ARQUIVO(edtARQUIVO.text);
        Submit_reciboenvio(edtReciboEnvio.Text);

        Submit_PROTOCOLOCANCELAMENTO(edtPROTOCOLOCANCELAMENTO.text);
        Submit_ARQUIVOCANCELAMENTO(edtARQUIVOCANCELAMENTO.text);
        Submit_DataCancelamento(edtdatacancelamento.Text);
        Submit_MotivoCancelamento(edtmotivocancelamento.Text);

        Submit_PROTOCOLOinutilizacao(edtPROTOCOLOinutilizacao.text);
        Submit_ARQUIVOinutilizacao(edtARQUIVOinutilizacao.text);
        Submit_Datainutilizacao(edtdatainutilizacao.Text);
        Submit_Motivoinutilizacao(edtmotivoinutilizacao.Text);
        
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFNFE.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjNFE do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtSTATUSNOTA.text:=Get_STATUSNOTA;
        EdtCERTIFICADO.text:=Get_CERTIFICADO;
        EdtARQUIVO.text:=Get_ARQUIVO;
        edtReciboEnvio.Text:=Get_reciboenvio;

        EdtPROTOCOLOinutilizacao.text:=Get_PROTOCOLOinutilizacao;
        EdtARQUIVOinutilizacao.text:=Get_ARQUIVOinutilizacao;
        edtdatainutilizacao.text:=Get_Datainutilizacao;
        edtmotivoinutilizacao.Text:=Get_Motivoinutilizacao;

        EdtPROTOCOLOcancelamento.text:=Get_PROTOCOLOcancelamento;
        EdtARQUIVOcancelamento.text:=Get_ARQUIVOcancelamento;
        edtdatacancelamento.text:=Get_Datacancelamento;
        edtmotivocancelamento.Text:=Get_Motivocancelamento;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFNFE.TabelaParaControles: Boolean;
begin
     If (Self.ObjNFE.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFNFE.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     desab_botoes(self);
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE NFE')=True)
     Then habilita_botoes(self);

     PegaFiguraBotoes(nil,nil,btcancelar,nil,btpesquisar,nil,nil,btsair);
end;

procedure TFNFE.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjNFE=Nil)
     Then exit;

     If (Self.ObjNFE.status<>dsinactive)
     Then Begin
              Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
              abort;
              exit;
     End;
end;

procedure TFNFE.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFNFE.btcancelarClick(Sender: TObject);
begin
     Self.ObjNFE.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFNFE.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFNFE.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjNFE.Get_pesquisa,Self.ObjNFE.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjNFE.status<>dsinactive
                                  then exit;

                                  If (Self.ObjNFE.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjNFE.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;
                                  Guia.PageIndex:=0; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFNFE.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFNFE.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFNFE.PassaObjeto(Objeto: TobjNfe);
begin
     Self.ObjNFE:=Objeto;
end;

procedure TFNFE.BTXMLNFClick(Sender: TObject);
begin

     if (Self.ObjNFE.ExtraiArquivoNFe=True)
     Then Begin
               Self.WBResposta.Navigate(RetornaPastaSistema+'ARQUIVOB.xml');
     end;

end;

procedure TFNFE.btXMLCANCClick(Sender: TObject);
begin
     if (Self.ObjNFE.ExtraiArquivoCancelamentoNFe=True)
     Then Begin
               Self.WBResposta.Navigate(RetornaPastaSistema+'ARQUIVOCANCELAMENTOB.xml');
     End;
end;

procedure TFNFE.GuiaChange(Sender: TObject; NewTab: Integer;var AllowChange: Boolean);
begin

     if Newtab=1
     Then Begin
               if ((EdtCODIGO.Text='') or (Self.ObjNFE.Status=dsinsert))
               then Begin
                        AllowChange:=False;
                        exit;
               End;
               BTXMLNFClick(sender);
     End;

end;

procedure TFNFE.BTXMLINUClick(Sender: TObject);
begin
     if (Self.ObjNFE.ExtraiArquivoinutilizacaoNFe=True)
     Then Begin
               Self.WBResposta.Navigate(RetornaPastaSistema+'ARQUIVOinutilizacaoB.xml');
     End;
end;

procedure TFNFE.btresgatacampoClick(Sender: TObject);
begin

    if (Self.ObjNFE.ExtraiArquivoNFe=False)
    Then exit;

    Showmessage(RetornaValorCampos(RetornaPastaSistema+'ARQUIVOB.xml',''))
    
end;

end.

