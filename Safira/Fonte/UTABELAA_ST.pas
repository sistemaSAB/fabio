unit UTABELAA_ST;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjTABELAA_ST,
  jpeg;

type
  TFTABELAA_ST = class(TForm)
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    LbDescricao: TLabel;
    LbCODIGO: TLabel;
    EdtDescricao: TEdit;
    EdtCODIGO: TEdit;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btalterar: TBitBtn;
    btrelatorios: TBitBtn;
    btgravar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btOpcoes: TBitBtn;
    ImagemFundo: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         Function AtualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FTABELAA_ST: TFTABELAA_ST;
  ObjTABELAA_ST:TObjTABELAA_ST;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFTABELAA_ST.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjTABELAA_ST do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_Descricao(edtDescricao.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFTABELAA_ST.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjTABELAA_ST do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtDescricao.text:=Get_Descricao;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFTABELAA_ST.TabelaParaControles: Boolean;
begin
     If (ObjTABELAA_ST.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFTABELAA_ST.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjTABELAA_ST=Nil)
     Then exit;

    If (ObjTABELAA_ST.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    ObjTABELAA_ST.free;
end;

procedure TFTABELAA_ST.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFTABELAA_ST.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';



     BtCancelar.visible:=True;
     btgravar.visible:=True;

     ObjTABELAA_ST.status:=dsInsert;
     Edtcodigo.setfocus;

end;


procedure TFTABELAA_ST.btalterarClick(Sender: TObject);
begin
    If (ObjTABELAA_ST.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjTABELAA_ST.Status:=dsEdit;
                edtdescricao.setfocus;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
          End;


end;

procedure TFTABELAA_ST.btgravarClick(Sender: TObject);
begin

     If ObjTABELAA_ST.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjTABELAA_ST.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjTABELAA_ST.Get_codigo;
     mostra_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);     
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFTABELAA_ST.btexcluirClick(Sender: TObject);
begin
     If (ObjTABELAA_ST.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjTABELAA_ST.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjTABELAA_ST.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;  
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFTABELAA_ST.btcancelarClick(Sender: TObject);
begin
     ObjTABELAA_ST.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFTABELAA_ST.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFTABELAA_ST.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjTABELAA_ST.Get_pesquisa,ObjTABELAA_ST.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjTABELAA_ST.status<>dsinactive
                                  then exit;

                                  If (ObjTABELAA_ST.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjTABELAA_ST.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFTABELAA_ST.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFTABELAA_ST.FormShow(Sender: TObject);
begin

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        ObjTABELAA_ST:=TObjTABELAA_ST.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
     Self.Color:=clwhite;
     lbquantidade.Caption:=AtualizaQuantidade;
     PegaCorForm(Self);
end;

function TFTABELAA_ST.AtualizaQuantidade: string;
begin
     result:='Existem '+ ContaRegistros('TABTABELAA_ST','codigo') + ' Tabela(s) A Cadastrados';
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjTABELAA_ST.OBJETO.Get_Pesquisa,ObjTABELAA_ST.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)
=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(OBJTABELAA_ST.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If OBJTABELAA_ST.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(OBJTABELAA_ST.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
