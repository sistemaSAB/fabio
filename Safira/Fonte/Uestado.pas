unit UEstado;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjEstado;

type
  TFestado = class(TForm)
    EdtCodigo: TEdit;
    edtnome: TEdit;
    edtaliquota: TEdit;
    lbquantidade: TLabel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    BtNovo: TBitBtn;
    btalterar: TBitBtn;
    Btgravar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    btCancelar: TBitBtn;
    pnlrodape: TPanel;
    pnlbotes: TPanel;
    imgRodape: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edtaliquotaKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         Function  AtualizaQuantidade: string;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  Festado: TFestado;
  ObjEstado:TObjEstado;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFestado.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjEstado do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);
         Submit_nome            ( edtnome.text);
         Submit_Aliquota        (edtaliquota.text);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFestado.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjEstado do
     Begin

        edtCODIGO.text          :=Get_CODIGO          ;
        edtnome.text            :=Get_nome            ;
        edtaliquota.text        :=Get_Aliquota        ;

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFestado.TabelaParaControles: Boolean;
begin
     ObjEstado.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFestado.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjEstado=Nil)
     Then exit;

    If (ObjEstado.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    ObjEstado.free;
end;

procedure TFestado.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFestado.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFestado.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     ObjEstado.status:=dsInsert;
     Edtcodigo.setfocus;
end;

procedure TFestado.BtCancelarClick(Sender: TObject);
begin
     ObjEstado.cancelar;

     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFestado.BtgravarClick(Sender: TObject);
begin

     If ObjEstado.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjEstado.salvar=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFestado.PreparaAlteracao;
begin
     habilita_campos(Self);
     esconde_botoes(Self);
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;
     EdtCodigo.enabled:=False;
     ObjEstado.Status:=dsEdit;
     edtNome.setfocus;
end;

procedure TFestado.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjEstado.Get_pesquisa,ObjEstado.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjEstado.status<>dsinactive
                                  then exit;

                                  If (ObjEstado.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFestado.btalterarClick(Sender: TObject);
begin
    If (ObjEstado.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFestado.btexcluirClick(Sender: TObject);
begin
     If (ObjEstado.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjEstado.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;
     
     If (ObjEstado.exclui(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFestado.edtaliquotaKeyPress(Sender: TObject; var Key: Char);
begin
     If not (key in ['0'..'9',',',#8])
     Then If key='.'
          Then key:=','
          Else Key:=#0;
end;

procedure TFestado.FormShow(Sender: TObject);
begin

     limpaedit(Self);
     desabilita_campos(Self);

     Try
        ObjEstado:=TObjEstado.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto Tipo de Professor!',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     retira_fundo_labels(self);
     lbquantidade.Caption:=AtualizaQuantidade;
     
end;

function TFestado.AtualizaQuantidade: string;
begin
      result:='Existem '+ ContaRegistros('TABESTADO','codigo') + ' Estados Cadastrados';
end;

end.
