unit UEMPRESA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjEMPRESA;

type
  TFEMPRESA = class(TForm)
    imgrodape: TImage;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb7: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    lbLbRAZAOSOCIAL1: TLabel;
    lbLbFANTASIA1: TLabel;
    lbLbCNPJ1: TLabel;
    lbLbIE1: TLabel;
    lbLbENDERECO1: TLabel;
    lbLbCIDADE1: TLabel;
    lbLbESTADO1: TLabel;
    lbLbCEP1: TLabel;
    lbLbCOMPLEMENTO1: TLabel;
    lbLbFONE1: TLabel;
    lbLbFAX1: TLabel;
    lbLbCELULAR1: TLabel;
    lbLbEMAIL1: TLabel;
    lbLbHOMEPAGE1: TLabel;
    lbLbDIRETOR1: TLabel;
    lbLbCELULARDIRETOR1: TLabel;
    lb8: TLabel;
    lb9: TLabel;
    lb10: TLabel;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    lb14: TLabel;
    lb15: TLabel;
    lb16: TLabel;
    lb17: TLabel;
    shp1: TShape;
    lb18: TLabel;
    lbtalaohomologado: TLabel;
    lb19: TLabel;
    lb20: TLabel;
    lb21: TLabel;
    lb22: TLabel;
    lb23: TLabel;
    edtRazaosocial: TEdit;
    edtFantasia: TEdit;
    edtCNPJ: TEdit;
    edtIE: TEdit;
    edtEndereco: TEdit;
    edtEstado: TEdit;
    edtCEP: TEdit;
    edtComplemento: TEdit;
    edtFone: TEdit;
    edtFax: TEdit;
    edtcelular: TEdit;
    edtEmail: TEdit;
    edtHomePage: TEdit;
    edtDiretor: TEdit;
    edtCelularDiretor: TEdit;
    edtBairro: TEdit;
    pnlNaosimples: TPanel;
    lb24: TLabel;
    lb25: TLabel;
    lb26: TLabel;
    lb27: TLabel;
    edtpis: TEdit;
    edtcofins: TEdit;
    chkCheckCreditaIpi: TCheckBox;
    chkCheckCreditapiscofins: TCheckBox;
    edtfrasenotafiscal: TEdit;
    edtaliquotaiss: TEdit;
    edtaliquotasimples: TEdit;
    edtaliquotaiss_substituto: TEdit;
    chkSimples: TCheckBox;
    chkPagaicmsnosimples: TCheckBox;
    chkIcmsantecipadotodasdespesasnf: TCheckBox;
    chkCheckUtilizaICMSSubstRecolhido_Custo: TCheckBox;
    chkCheckCreditaIcmsNaCompra: TCheckBox;
    edtNumero: TEdit;
    chkCHECKSUBSTITUTOTRIBUTARIO: TCheckBox;
    cbbtalaohomologado: TComboBox;
    chkCheckGarantido: TCheckBox;
    pnlGarantido: TPanel;
    lb28: TLabel;
    edtAliquotaICMSGarantido: TEdit;
    edtCodigoCidade: TEdit;
    edtCertificado: TEdit;
    edtCodigoUF: TEdit;
    edtcodigoclientesite: TEdit;
    lb5: TLabel;
    cbbcidade: TComboBox;
    edtCRT: TEdit;
    Label1: TLabel;
    lbNomeCRT: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbbcidadeExit(Sender: TObject);
    procedure edtEstadoExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCRTExit(Sender: TObject);
    procedure edtCRTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEMPRESA: TFEMPRESA;
  ObjEMPRESA:TObjEMPRESA;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UDataModulo,
  Uprincipal;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFEMPRESA.ControlesParaObjeto: Boolean;
var
  temp: string;
  indice: Integer;
Begin
  Try
    With ObjEMPRESA do
    Begin
        Submit_CODIGO(lbCodigo.Caption);
        Submit_RAZAOSOCIAL(edtRAZAOSOCIAL.text);
        Submit_FANTASIA(edtFANTASIA.text);
        Submit_CNPJ(edtCNPJ.text);
        Submit_IE(edtIE.text);
        Submit_ENDERECO(edtENDERECO.text);
        temp := CbbCIDADE.text;
        indice := Pos( '|', temp )-1;
        temp := copy( temp, 1, indice ); // nome da cidade est� concatenado com |UF, precisa remover
        Submit_CIDADE( temp );
        Submit_ESTADO(edtESTADO.text);
        Submit_CEP(edtCEP.text);
        Submit_COMPLEMENTO(edtCOMPLEMENTO.text);
        Submit_FONE(edtFONE.text);
        Submit_FAX(edtFAX.text);
        Submit_CELULAR(edtCELULAR.text);
        Submit_EMAIL(edtEMAIL.text);
        Submit_HOMEPAGE(edtHOMEPAGE.text);
        Submit_DIRETOR(edtDIRETOR.text);
        Submit_CELULARDIRETOR(edtCELULARDIRETOR.text);
        Submit_bairro(edtbairro.text);

        CRT.Submit_Codigo(edtCRT.text);
        
        if (edtCRT.text = '1')
        then Submit_simples('S')
        else Submit_simples('N');

        if(chkCheckCreditaIpi.Checked=True)
        then Submit_CreditaIPI('S')
        else Submit_CreditaIPI('N');

        if(chkCheckCreditapiscofins.Checked=True)
        then Submit_CreditaCofins('S')
        else Submit_CreditaCofins('N');

        if(chkPagaicmsnosimples.Checked=True)
        then  Submit_PagaIcmsnoSimples('S')
        else  Submit_PagaIcmsnoSimples('N');

        if(chkIcmsantecipadotodasdespesasnf.Checked=True)
        then  Submit_icmsantecipadotodasdespesasnf('S')
        else  Submit_icmsantecipadotodasdespesasnf('N');

        if(chkCheckUtilizaICMSSubstRecolhido_Custo.Checked=True)
        then  Submit_utilizaicmssubstrecolhido_custo('S')
        else  Submit_utilizaicmssubstrecolhido_custo('N');

        if(chkCheckCreditaIcmsNaCompra.Checked=True)
        then  Submit_CreditaICMSCompra('S')
        else  Submit_CreditaICMSCompra('N');

        if(chkCHECKSUBSTITUTOTRIBUTARIO.Checked=True)
        then  Submit_SubstitutoTributario('S')
        else  Submit_SubstitutoTributario('N') ;

        if(chkCheckGarantido.Checked=True)
        then  Submit_ICMSGarantido('S')
        else  Submit_ICMSGarantido('N');

        Submit_pis(edtpis.Text);
        Submit_Cofins(edtcofins.Text);
        Submit_AliquotaISS(edtaliquotaiss.Text);
        Submit_Aliquotaiss_substituto(edtaliquotaiss_substituto.Text);
        Submit_AliquotaSimples(edtaliquotasimples.Text);
        Submit_AliquotaICMSGarantido(edtAliquotaICMSGarantido.Text);
        Submit_numero(edtnumero.Text);
        Submit_codigocidade(edtcodigocidade.Text);
        Submit_certificado(edtcertificado.Text);
        Submit_codigoclientesite(edtcodigoclientesite.Text);
        Submit_codigoestado(edtCodigoUF.Text);
        Submit_FraseNFe(edtfrasenotafiscal.Text);
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFEMPRESA.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjEMPRESA do
     Begin
        lbCodigo.Caption:=Get_CODIGO;
        edtpis.Text:=Get_Pis;
        edtcofins.text:=Get_Cofins;
        edtaliquotaiss.text:=Get_AliquotaISS;
        edtaliquotaiss_substituto.text:=Get_aliquotaiss_substituto;
        edtaliquotasimples.text:=Get_AliquotaSimples;

        EdtRAZAOSOCIAL.text:=Get_RAZAOSOCIAL;
        EdtFANTASIA.text:=Get_FANTASIA;
        EdtCNPJ.text:=Get_CNPJ;
        EdtIE.text:=Get_IE;
        EdtENDERECO.text:=Get_ENDERECO;
        CbbCIDADE.text:=Get_CIDADE;
        EdtESTADO.text:=Get_ESTADO;
        EdtCEP.text:=Get_CEP;
        EdtCOMPLEMENTO.text:=Get_COMPLEMENTO;
        EdtFONE.text:=Get_FONE;
        EdtFAX.text:=Get_FAX;
        EdtCELULAR.text:=Get_CELULAR;
        EdtEMAIL.text:=Get_EMAIL;
        EdtHOMEPAGE.text:=Get_HOMEPAGE;
        EdtDIRETOR.text:=Get_DIRETOR;
        EdtCELULARDIRETOR.text:=Get_CELULARDIRETOR;
        edtbairro.text:=get_bairro;
        edtCodigoUF.Text:=Get_codigoestado();

        lbNomeCRT.Caption:=CRT.Get_Descricao;
        edtCRT.Text:=crt.Get_Codigo;

        if(Get_CreditaIPI='S')
        then chkCheckCreditaIpi.Checked:=True
        else chkCheckCreditaIpi.Checked:=False;

        if(Get_CreditaPisCofins='S')
        then chkCheckCreditapiscofins.Checked:=True
        else chkCheckCreditapiscofins.Checked:=False;

        if(Get_PagaIcmsnoSimples='S')
        then chkPagaicmsnosimples.Checked:=True
        else chkPagaicmsnosimples.Checked:=False;

        if(Get_icmsantecipadotodasdespesasnf='S')
        then chkIcmsantecipadotodasdespesasnf.Checked:=True
        else chkIcmsantecipadotodasdespesasnf.Checked:=false;

        if(Get_utilizaicmssubstrecolhido_custo='S')
        then chkCheckUtilizaICMSSubstRecolhido_Custo.Checked:=True
        else chkCheckUtilizaICMSSubstRecolhido_Custo.Checked:=false;

        if(Get_CreditaICMSnaCompra='S')
        then chkCheckCreditaIcmsNaCompra.Checked:=True
        else chkCheckCreditaIcmsNaCompra.Checked:=False;

        if(Get_SubstitutoTributario='S')
        then chkCHECKSUBSTITUTOTRIBUTARIO.Checked:=True
        else chkCHECKSUBSTITUTOTRIBUTARIO.Checked:=false;

        if(Get_IcmsGarantido='S')
        then chkCheckGarantido.Checked:=True
        else chkCheckGarantido.Checked:=False;

        edtAliquotaICMSGarantido.Text:=Get_AliquotaICMSGarantido;
        edtfrasenotafiscal.Text:=Get_FraseNFe;

        edtnumero.Text:=Get_numero;
        edtcodigocidade.Text:=Get_codigocidade;
        edtcertificado.Text:=Get_certificado;

        edtcodigoclientesite.Text:=Get_codigoclientesite;

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFEMPRESA.TabelaParaControles: Boolean;
begin
     If (ObjEMPRESA.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFEMPRESA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjEMPRESA=Nil)
     Then exit;

     If (ObjEMPRESA.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

     ObjEMPRESA.free;

     if (ObjEmpresaGlobal.LocalizaCodigo('1')=False)
     then Begin
               mensagemerro('Configure a empresa c�digo 1 no cadastro de empresa');
               ObjEmpresaGlobal.status:=dsinsert;
               ObjEmpresaGlobal.submit_codigo('1');
               ObjEmpresaGlobal.Submit_RAZAOSOCIAL('RAZ�O SOCIAL DA EMPRESA');
               ObjEmpresaGlobal.Submit_FANTASIA('FANTASIA DA EMPRESA');
               ObjEmpresaGlobal.Submit_simples('N');
               ObjEmpresaGlobal.salvar(true);
     End
     else ObjEmpresaGlobal.TabelaparaObjeto;

end;

procedure TFEMPRESA.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFEMPRESA.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     //desab_botoes(Self);

    lbCodigo.Caption:='0';
     btnovo.Visible:=false;
     btalterar.Visible:=False;
     btexcluir.Visible:=false;
     btrelatorio.Visible:=false;
     btopcoes.Visible:=false;
     btsair.Visible:=False;
     btpesquisar.visible:=False;

     

     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjEMPRESA.status:=dsInsert;

     //chkSimples.SetFocus;
end;


procedure TFEMPRESA.btalterarClick(Sender: TObject);
begin
    If (ObjEMPRESA.Status=dsinactive) and (lbCodigo.Caption<>'')
    Then Begin
                habilita_campos(Self);

                ObjEMPRESA.Status:=dsEdit;

                //chkSimples.SetFocus;
                //desab_botoes(Self);
                 btnovo.Visible:=false;
                 btalterar.Visible:=False;
                 btexcluir.Visible:=false;
                 btrelatorio.Visible:=false;
                 btopcoes.Visible:=false;
                 btsair.Visible:=False;
                 btpesquisar.visible:=False;

                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;


end;

procedure TFEMPRESA.btgravarClick(Sender: TObject);
begin

     If ObjEMPRESA.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjEMPRESA.salvar(true)=False)
     Then exit;

     lbCodigo.Caption:=ObjEMPRESA.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorio.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFEMPRESA.btexcluirClick(Sender: TObject);
begin
     If (ObjEMPRESA.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (ObjEMPRESA.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjEMPRESA.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFEMPRESA.btcancelarClick(Sender: TObject);
begin
     ObjEMPRESA.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorio.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;


end;

procedure TFEMPRESA.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFEMPRESA.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjEMPRESA.Get_pesquisa,ObjEMPRESA.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjEMPRESA.status<>dsinactive
                                  then exit;

                                  If (ObjEMPRESA.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjEMPRESA.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFEMPRESA.LimpaLabels;
begin
    lbCodigo.Caption:='';
    lbNomeCRT.Caption:='';
end;


procedure TFEMPRESA.FormShow(Sender: TObject);
begin
      limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);


     Try
        ObjEMPRESA:=TObjEMPRESA.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(imgrodape,'RODAPE');
     if not (ObjEMPRESA.carregaCidades(cbbcidade)) then
     MensagemAviso('ERRO ao carregar as cidades');
     {if not (ObjEMPRESA.carregaCodigoPaises(cbbpais)) then
     MensagemAviso('ERRO ao carregar pa�ses');    }

end;

procedure TFEMPRESA.cbbcidadeExit(Sender: TObject);
begin
       if (cbbcidade.Text <> '') then
      begin
            edtestado.Text := ObjEMPRESA.retornaUF(cbbcidade.Text);
            if (edtestado.Text <> '') then
            begin
                edtestado.OnExit(edtestado);
                edtfone.Text;

            end;
      end;
end;

procedure TFEMPRESA.edtEstadoExit(Sender: TObject);
var
   cidade:string;
begin
         cidade := ObjEMPRESA.retornaCidade(cbbcidade.Text);
         edtCodigoCidade.Text := ObjEMPRESA.retornaCodigoCidade(cidade,edtestado.Text);
         edtCodigoUF.Text := Copy (edtCodigoCidade.Text,1,2);
end;

procedure TFEMPRESA.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
       if (Key = VK_F1) then
      Fprincipal.chamaPesquisaMenu();
end;

procedure TFEMPRESA.edtCRTExit(Sender: TObject);
begin

    self.lbNomeCRT.Caption := get_campoTabela('descricao','codigo','tabcrt',self.edtCRT.Text);

end;

procedure TFEMPRESA.edtCRTKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  ObjEmpresaGlobal.CRT.edtCRTkeydown(sender,key,shift,lbnomeCRT);

end;

end.

