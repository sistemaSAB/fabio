object FKITBOX: TFKITBOX
  Left = 637
  Top = 256
  Width = 916
  Height = 636
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'KITBOX'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object imgrodape: TImage
    Left = 0
    Top = 557
    Width = 900
    Height = 41
    Align = alBottom
    Stretch = True
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 900
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    DesignSize = (
      900
      57)
    object lbnomeformulario: TLabel
      Left = 550
      Top = 3
      Width = 104
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Cadastro de'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigo: TLabel
      Left = 665
      Top = 8
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb4: TLabel
      Left = 551
      Top = 25
      Width = 60
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Kit Box'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object bt1: TSpeedButton
      Left = 847
      Top = -3
      Width = 53
      Height = 80
      Cursor = crHandPoint
      Hint = 'Ajuda'
      Anchors = [akTop, akRight]
      Flat = True
      Glyph.Data = {
        4A0D0000424D4A0D0000000000003600000028000000240000001F0000000100
        180000000000140D0000D8030000D80300000000000000000000FEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFAF6FBF9F2FEFEF4FFFFFAFF
        FCFDFFF9FFFEFEFDFCFFFFFEFEFFFDFEFCFEFEF8FFFFFBFEFCFFFFFCFFFDF6FB
        FFF7F8FFF7FBFFFBFBFDF7FFFFFCFFFFFEF7FDFCFBFFFFFFFBFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFF9FD
        F8FDFFFFFDFBFFFFFBFFFFFDFEFFFCFCFFFEFFFEFDFFFCF9FBF0EFF1F0EBEDF9
        F1F2F6F8F9F1FFFFF4FFFFFEFCFFFFFDFFFFFAFFFBFCFFFEFDFFFFFAFDFAFCFD
        F7FFFFFFFCFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFFFCFFFFFCFEFFFDFCFFFFFFFFFDFFF2E4E6DEBCBCCF
        9F99EB8688E68081EA7E7DF68686FE9092FDA2A5F7C3C3FFF0EDFFFAFFF8FFFF
        FFFDFFFFF5FDFCFDFFF6FFFFFBF8FAFFFDFEFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFBFFFCFEFFFBFFFCE7
        E1DCC29E9EBF7579DC7677F07B76FF8783FF8F88FD9388FB8F84FF837BFF7977
        F77576F98084FBB6B9F2F2ECE8FFFAF8FDFCFFFEFFFAFCFDFFFCFEFCFEFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFEFDFFFAFCD7B8BBB17879C87772F08E88FE9893F58E8CE7797DD76F70
        D26867DC716EE88581F69A95F99591FE8686FF7173F28484FFD9D4FFFFFBFDF8
        F7FFFCFFFFFEFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFF9B99D9CA76B6CDA8786F99B96E17D79
        B6484C98222FAB1122AA0F1EA5131FA41D25AA2226C53938E6625BFF9085FD9B
        91FF7B7CF2777BF4C8C7F8FFFEF9FEFDFFFEFFFFFBFCFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFC0A0A1A7706D
        E6928CFC9691B3484A87192589112398182BA32E31DC6F71EB8082CC5050BB28
        26D32A22EB2F24FE3727F36D69FF9D97FB7D83F7707EFEC5CDFFFEFFFFFDFDED
        FFFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFCFBFDFFFDFFFFFBFFFAFFFFF2FAFA
        FFFEFEDCBDBEA66D6BDB9092ED909996303C770E1B8D1E2C9B1C2BA11425D96F
        75FDA8B0FFA2B0FFA0AAC83F43BC2F26D73927FF3929FF2B1CEC5650FF9996FE
        7D7AFD7F7EFADFDBFFFEFFF4FEFEFEFDFFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFF
        FDF9FEFFFDFFF7FAFEFAFFFFF4E6E7A67F81CF8B8CEC9A9F92323C7B121D9123
        2F921C27A5232EA71C27DE837EFFB7B5F9A9AEFDB0B3BF4343B42821C9372BDA
        3326E33626CB2728CA5556FF9893FB7777DE9A9BFFFEFEFFFCFDFEFDFFFEFDFF
        FEFDFFFEFDFFFEFEFEFEFDFFFFFBFFFFFBFFFAFBFFFFFFFFC3AEADA77678F8A4
        A9A9515776131B8D202898242BA3282CA72629A82627B55047EBADA2FFBCB5CD
        7974A92E2AA13026A5372BBD332DBF3528BD2B2DA51E26DF7372FF918FE27377
        F0CCCCFFFFFEFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFAF9FBFFFDFFFFFAFFFFFE
        FFF5F0EFA38384DB9FA0E78A91791D22852428992E309E2827A72C28AC3028AB
        3229A3271FA13E36B4413E9E332CB03129A73126A23127B0322EA93026AD3131
        A91925B63239F2948FFD7C7FE9989BFBFFF9FEFDFFFEFDFFFEFDFFFEFDFFFFFE
        FEFCFBFDFFFEFFFFFDFFFFFEFFDACECEAE8687F0ABAEB156597E24248B2C2992
        2A25A93730A93328AE3B2EA33222D35E59EA928BF28C87E27D74AC362BA93A2A
        A23729A5342AA3342CA62728B02931A71821D8716FFF8A8ADE7D7FEFECE7FEFD
        FFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFFFDFCFEFFFCFEFFFEFFC0B3B1BF9392F0
        A5A78F3B35842A2396352B9F372AA93C2EAC3D2DAC3C2AA1311FD16D69F8CABF
        FBC0B7FCABA3AA3A2EA23827A33928A9372AA6352BAE302BA72C2EAA1922C550
        51FF9692DC7475E0D2D3FFFEFFFEFDFFFEFDFFFEFDFFFFFDFEFEFDFFFEFDFFFF
        FDFFFFFEFEB6A9A7CB9D9CEC9E9F8335288E3527A0392AAD4130A83C2AA73B29
        A93927AE3827CE6C66FFCDC1FABAB5FFBFB6D46B62AD3126B43427B13A2BA933
        27A93327A02A25AC1F28AF373BFC9D94E7797BD1B8BCFFFEFFFFFEFFFEFDFFFE
        FDFFFFFDFEFCFCFCFDFCFEFFFEFFFDFBFBB5A8A6DBAEABE19192813522983C29
        A73B29AF3E2AA83B26AA3F2AAD3B2AAF3224B05A4EFFE0D0FFCEC6F7BEB5FFBE
        B3DE8C7BA44434A33628AD3C2CAD3726A8342DA7262DAE2E33FA9B92E27A7BD4
        ABB3FFFEFFFFFEFFFEFDFFFEFDFFFBFCFFF9FEFFFFFCFDFBFFFFFCF9FBC9AAAB
        DEB1ADDF9A91813628B03C2BAB3D25A7412AA6362AB3362EA13726AE3C25A833
        24D79987FFF4E4F9D3CEFEBEBEFFC9C3EC9E91AD4436B137299C3B27AA3526B1
        2F28A4302FF7969AE67F86C7AAA5FFFEFFFFFEFFFEFDFFFEFDFFFAFBFFFCFEFF
        FFFEFFFCFEFFFCFEFFCEB4B4E6B6B2E6A99F98392AA13426B2382C9E3E2EAA3E
        33AD4338A74539A24536AB443B9B3D38CF8A87FFEBE6FEDED9F1C0B8FFC9BDEC
        8B81AA372AA73B2AA83F2CA52B1FA6403BF9A1A1E57E86C9B3AEFFFEFFFFFEFF
        FEFDFFFEFDFFFAFDFFFDFCFEFFFEFFFDFCFEFAFFFFD3BEC0ECB5B2F5C2B8AB40
        329F3D31B84D49A24F47AD554EAE574DAE5D55B1655FAD5654B34E50A13B40B9
        7B7BFFE1DBFED7CFF9CAC2FFBCB5BD5146AD3627A3402AA83526AD544AFEACAB
        D5787FD4C7C5FFFEFFFEFDFFFEFDFFFEFDFFFBFFFFFFFBFDFFFDFDFFFDFFF9FE
        FFDFD1D2E7B1B0FFDACEC26B61A74C45A55C54A85A54D3958FF2B8B3F9C2BFF4
        B4B3AD5352AB5C59AC5553A54343F7C0BBFDE0D9FFCCCAFBC9C9CC6A60B03429
        9F3923A63624C9776BFFADACC67E84E4E0DFFEFDFFFEFDFFFEFDFFFEFDFFFAFE
        FFFFFCFFFCFCFCFFFEFFFBFEFFF4ECEDE2B7B4FFDBD2DAB0ABA6504AA86153AB
        514AD59594FFF8F7FEF1EFFAD4D2C469649C5148A85950A14B45ECBBB3FBE1DA
        FFCED0FFCFD6D06E66AB352AAD3B2A9F3726F7A89DFBA4A7BB8E91F2F7F6FEFD
        FFFEFDFFFEFDFFFEFDFFFCFBFDFFFEFFFCFEFEFEFDFFFDFCFFFFFEFFE2C7C3F3
        CAC1EDDEDCBD7672AD5144CE6459D7797AF6F5F1F4FFFEF9F3EEE9B4AAB16860
        B45E58C99087FAD7CDFFDCD6F8D5D2FECBCFBE5A55A23127A12F22CB7264FFC3
        BBD28B8ECFB7B9FAFFFFFEFDFFFEFDFFFEFDFFFEFDFFFEFBFDFFFFFFFCFEFEFB
        FDFEFFFAFFFFFEFFEBE5DEF2C4BDFFE5E8EFC8C0AE6053E17164F47773F0BBB8
        FDFEFAFDFFFFFFF8F1FCD6D4FFCDCCFFDDD8FFDED8FFE0DAFFDDD7E7A1A2A339
        32A7352EAC4A3EF8B8ADECB2ADB68E90FAECEEFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFDFFFCFCFCFAFFFEFAFDFFFFFBFFFFFDFDFBFFF9FFCFC9FFC7CCFFF9EF
        E8BFB0D66D5FFF7C70F9807EEACECDFFFBFFFDFCFFF9FEFFFDFFFFFFE4E5FFE2
        DFF6E7DEEDB6AFB34648A1312BB03E37FCAA9FF7CDC1BC8F8CD5C7C8FFFDFFFC
        FEFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFFFEFFFFFEFF
        FDFCFEFCFBFDFCC8C8F6D7D6FFFCF6F0C9C1E57F7AFF7673F98481ECA5A1E6D6
        D7FCF1F3F9F1F1ECD7D9F0C1C3D78A8DA84646A03232A54B4BECABAAFFD1CFC9
        A09EC9ADACFEF9F8FBFFFFFCFCFCFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFF
        FDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFDFCFEFFF4EEF8CDCAFDD8D4FFFBF4FDE6
        E4FBA1A6FB7375FD6F68EA6E5CCE6855B76958AE6458A5514B993E3AA04D4BC8
        7F7BFCC3C1FFCBCCDAA4A4BFAEABF3F9F4FFFFFEFFFCFDFCFEFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFEFDFFFFFEFFF4FF
        FFFBF2EFF6D6D1FDD9D3FFEFE8FCFBF1FFE6DCF6BBB2EE9285D07C70B76F65B2
        6F66C0807BDDA39EF3C1BBFFD6CFF6C6C5D4ACADCEBBB8F9FAF6F7FFFDFAFCFC
        FFFEFFFCF9FBFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFC
        FEFDFCFEFEFDFFFFFEFFFFFAFCFBFFFFF9F9F9FCE0DFFFD3CCFEDDD4F8ECE6F8
        F6F5F5F8F6F4EFEEF6E4E5FADBDCFFDBDCFFD9D8FBCAC8F0C0BCD7B5B6E1D8D5
        FAFFFEFFFFFFFEFCFCFAFFFFF8FDFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFFFFFBFFFBFCFDFDFFFD
        FBFFFFEEF1FFE2E1FFD3D2FFD0D2FFD2CFFFD1CDFFD2CFFFCFCFFFC9C9F2BFC3
        E9C4C6EED6D8F5F7F7FFFBFCFFFEFFFFFCFEFDFFFFF9FCFFFCFBFFFFFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFFFEFDFFFEFDFFFDFCFEFDFCFEFD
        FCFEFFFBFFF0FDFBF1FFFBFFFEFFFFFDFFF9FEFFF9FCFAFEF2EEF9EAE1F4E5DC
        F3E3DCF3E3DDF6E7E5FCF1F3FEFAFFF9FCFFF4FEFEFFFEFFFFF7FAFFFDFFFCF9
        FBFFFDFFFFFCFFF5F8FCFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFF7F9FFFDFEFFFFFBF8FFFDF9FDFBFAF1FBFB
        F6FFFFFBFDFEFBFEFFFFFCFFFFFDFFFFFEFFFBFFFFF8FFFFFDFEFFFFFAFFFFFD
        FFFAFEFFF8FFFFF9FBFCFFFDFFFFF9FCFBFAFCFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFCFFFEFF
        F7FDFFF5FEFFFBFFFFFFFFFEFFFCFDFFFCFFFFFAFFFFFCFFF9FEFDF9FFFDFBFF
        FEF9FCFAFBFBFBFFFFFFFFFCFEFEFDFFFBFAFCFBFFFFF5FDFDF7FFFFFDFFFFFF
        FCFFFEFDFFFEFDFFFEFDFFFEFDFF}
      ParentShowHint = False
      ShowHint = True
      OnClick = bt1Click
    end
    object btrelatorio: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btRelatorioClick
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btOpcoesClick
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btPesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btExcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btCancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btSalvarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btAlterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btNovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btSairClick
      Spacing = 0
    end
  end
  object pgcGuia: TPageControl
    Left = 0
    Top = 57
    Width = 900
    Height = 500
    ActivePage = tsCor
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Style = tsButtons
    TabOrder = 1
    OnChange = pgcGuiaChange
    object tsPrincipal: TTabSheet
      Caption = '&1 - Principal'
      object panelPrincipal: TPanel
        Left = 0
        Top = 0
        Width = 892
        Height = 468
        Align = alClient
        BevelOuter = bvNone
        Color = 10643006
        TabOrder = 0
        object SpeedButtonUltimo: TSpeedButton
          Left = 621
          Top = 399
          Width = 65
          Height = 48
          Caption = '>>'
          Flat = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clLime
          Font.Height = -48
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btUltimoClick
        end
        object SpeedButtonProximo: TSpeedButton
          Left = 457
          Top = 399
          Width = 65
          Height = 48
          Caption = '>'
          Flat = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clLime
          Font.Height = -48
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btProximoClick
        end
        object SpeedButtonAnterior: TSpeedButton
          Left = 293
          Top = 399
          Width = 63
          Height = 48
          Caption = '<'
          Flat = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clLime
          Font.Height = -48
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btAnteriorClick
        end
        object SpeedButtonPrimeiro: TSpeedButton
          Left = 130
          Top = 399
          Width = 65
          Height = 48
          Caption = '<<'
          Flat = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clLime
          Font.Height = -48
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btPrimeiroClick
        end
        object LbReferencia: TLabel
          Left = 17
          Top = 16
          Width = 53
          Height = 14
          Caption = 'Refer'#234'ncia'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb2: TLabel
          Left = 468
          Top = 16
          Width = 22
          Height = 14
          Caption = 'NCM'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lbAtivoInativo: TLabel
          Left = 775
          Top = 27
          Width = 69
          Height = 36
          Caption = 'Ativo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clLime
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object LbDescricao: TLabel
          Left = 17
          Top = 41
          Width = 49
          Height = 14
          Caption = 'Descri'#231#227'o'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object LbFornecedor: TLabel
          Left = 17
          Top = 66
          Width = 56
          Height = 14
          Caption = 'Fornecedor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object LbUnidade: TLabel
          Left = 17
          Top = 93
          Width = 39
          Height = 14
          Caption = 'Unidade'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object LbAlturaIdeal: TLabel
          Left = 17
          Top = 120
          Width = 54
          Height = 14
          Caption = 'Altura Ideal'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object LbPrecoCusto: TLabel
          Left = 17
          Top = 146
          Width = 59
          Height = 14
          Caption = 'Pre'#231'o Custo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object LbPeso: TLabel
          Left = 468
          Top = 91
          Width = 24
          Height = 14
          Caption = 'Peso'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object LbLarguraIdeal: TLabel
          Left = 468
          Top = 118
          Width = 63
          Height = 14
          Caption = 'Largura Ideal'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 468
          Top = 145
          Width = 78
          Height = 14
          Caption = 'Plano de Contas'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object LbNomeFornecedor: TLabel
          Left = 204
          Top = 69
          Width = 445
          Height = 13
          Cursor = crHandPoint
          AutoSize = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clAqua
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = LbNomeFornecedorClick
          OnMouseMove = LbNomeFornecedorMouseMove
          OnMouseLeave = LbNomeFornecedorMouseLeave
        end
        object label4545: TLabel
          Left = 468
          Top = 42
          Width = 22
          Height = 14
          Caption = 'Cest'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object EdtReferencia: TEdit
          Left = 129
          Top = 17
          Width = 70
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 0
        end
        object edtNCM: TEdit
          Left = 580
          Top = 15
          Width = 70
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 1
          OnKeyDown = edtNCMKeyDown
          OnKeyPress = edtNCMKeyPress
        end
        object chkAtivo: TCheckBox
          Left = 727
          Top = 34
          Width = 25
          Height = 25
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clLime
          Font.Height = -32
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 13
          OnClick = chkAtivoClick
        end
        object EdtDescricao: TEdit
          Left = 129
          Top = 42
          Width = 328
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 100
          ParentFont = False
          TabOrder = 2
        end
        object EdtPrecoCusto: TEdit
          Left = 129
          Top = 145
          Width = 72
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 8
          OnExit = EdtPrecoCustoExit
          OnKeyPress = EdtPrecoCustoKeyPress
        end
        object EdtAlturaIdeal: TEdit
          Left = 129
          Top = 119
          Width = 72
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 6
          OnKeyPress = EdtAlturaIdealKeyPress
        end
        object EdtUnidade: TEdit
          Left = 129
          Top = 93
          Width = 72
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 4
        end
        object EdtFornecedor: TEdit
          Left = 129
          Top = 66
          Width = 72
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 3
          OnDblClick = EdtFornecedorDblClick
          OnExit = EdtFornecedorExit
          OnKeyDown = EdtFornecedorKeyDown
          OnKeyPress = EdtFornecedorKeyPress
        end
        object EdtPlanodeContas: TEdit
          Left = 580
          Top = 142
          Width = 72
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 9
          OnDblClick = EdtPlanodeContasDblClick
          OnExit = EdtPlanodeContasExit
          OnKeyDown = EdtPlanodeContasKeyDown
          OnKeyPress = EdtPlanodeContasKeyPress
        end
        object EdtLarguraIdeal: TEdit
          Left = 580
          Top = 117
          Width = 72
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 7
          OnKeyPress = EdtLarguraIdealKeyPress
        end
        object EdtPeso: TEdit
          Left = 580
          Top = 91
          Width = 72
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 5
        end
        object GroupBox3: TGroupBox
          Left = 16
          Top = 168
          Width = 610
          Height = 101
          Caption = 'Medidas'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
          object LbAreaMinima: TLabel
            Left = 5
            Top = 23
            Width = 59
            Height = 14
            Caption = #193'rea M'#237'nima'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object LbAlturaMinima: TLabel
            Left = 5
            Top = 48
            Width = 64
            Height = 14
            Caption = 'Altura M'#237'nima'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object LbLarguraMinima: TLabel
            Left = 5
            Top = 72
            Width = 73
            Height = 14
            Caption = 'Largura M'#237'nima'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object LbAlturaMaxima: TLabel
            Left = 404
            Top = 48
            Width = 68
            Height = 14
            Caption = 'Altura M'#225'xima'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object LbLarguraMaxima: TLabel
            Left = 404
            Top = 72
            Width = 77
            Height = 14
            Caption = 'Largura M'#225'xima'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object EdtAreaMinima: TEdit
            Left = 114
            Top = 21
            Width = 72
            Height = 20
            Color = 15663069
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 0
            OnKeyPress = EdtAreaMinimaKeyPress
          end
          object EdtAlturaMinima: TEdit
            Left = 114
            Top = 45
            Width = 72
            Height = 20
            Color = 15663069
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 1
            OnKeyPress = EdtAlturaMinimaKeyPress
          end
          object EdtLarguraMinima: TEdit
            Left = 114
            Top = 69
            Width = 72
            Height = 20
            Color = 15663069
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 2
            OnKeyPress = EdtLarguraMinimaKeyPress
          end
          object EdtAlturaMaxima: TEdit
            Left = 515
            Top = 45
            Width = 71
            Height = 20
            Color = 15663069
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 3
            OnKeyPress = EdtAlturaMaximaKeyPress
          end
          object EdtLarguraMaxima: TEdit
            Left = 515
            Top = 69
            Width = 72
            Height = 20
            Color = 15663069
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 4
            OnKeyPress = EdtLarguraMaximaKeyPress
          end
        end
        object rgmargem: TGroupBox
          Left = 15
          Top = 281
          Width = 211
          Height = 97
          Caption = 'Margem'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 11
          object LbPorcentagemFornecido: TLabel
            Left = 16
            Top = 45
            Width = 48
            Height = 14
            Caption = 'Fornecido'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object LbPorcentagemRetirado: TLabel
            Left = 16
            Top = 74
            Width = 40
            Height = 14
            Caption = 'Retirado'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object LbPorcentagemInstalado: TLabel
            Left = 16
            Top = 16
            Width = 43
            Height = 14
            Caption = 'Instalado'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object Label1: TLabel
            Left = 189
            Top = 18
            Width = 10
            Height = 14
            Caption = '%'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object Label2: TLabel
            Left = 189
            Top = 45
            Width = 10
            Height = 14
            Caption = '%'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 189
            Top = 74
            Width = 10
            Height = 14
            Caption = '%'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object EdtPorcentagemInstalado: TEdit
            Left = 116
            Top = 15
            Width = 72
            Height = 20
            Color = 15663069
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 0
            OnExit = EdtPorcentagemInstaladoExit
            OnKeyPress = EdtLarguraMinimaKeyPress
          end
          object EdtPorcentagemFornecido: TEdit
            Left = 116
            Top = 42
            Width = 72
            Height = 20
            Color = 15663069
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 1
            OnExit = EdtPorcentagemFornecidoExit
            OnKeyPress = EdtLarguraMinimaKeyPress
          end
          object EdtPorcentagemRetirado: TEdit
            Left = 116
            Top = 71
            Width = 72
            Height = 20
            Color = 15663069
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 2
            OnExit = EdtPorcentagemRetiradoExit
            OnKeyPress = EdtLarguraMinimaKeyPress
          end
        end
        object rgpreco: TGroupBox
          Left = 414
          Top = 282
          Width = 211
          Height = 97
          Caption = 'Pre'#231'o Venda'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 12
          object LbPrecoVendaInstalado: TLabel
            Left = 11
            Top = 17
            Width = 43
            Height = 14
            Caption = 'Instalado'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object LbPrecoVendaFornecido: TLabel
            Left = 11
            Top = 44
            Width = 48
            Height = 14
            Caption = 'Fornecido'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object LbPrecoVendaRetirado: TLabel
            Left = 11
            Top = 73
            Width = 40
            Height = 14
            Caption = 'Retirado'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object Label4: TLabel
            Left = 92
            Top = 17
            Width = 13
            Height = 14
            Caption = 'R$'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object Label5: TLabel
            Left = 92
            Top = 44
            Width = 13
            Height = 14
            Caption = 'R$'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object Label6: TLabel
            Left = 92
            Top = 73
            Width = 13
            Height = 14
            Caption = 'R$'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object EdtPrecoVendaInstalado: TEdit
            Left = 116
            Top = 14
            Width = 72
            Height = 20
            Color = 15663069
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 0
            OnExit = EdtPrecoVendaInstaladoExit
          end
          object EdtPrecoVendaFornecido: TEdit
            Left = 116
            Top = 41
            Width = 72
            Height = 20
            Color = 15663069
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 1
            OnExit = EdtPrecoVendaFornecidoExit
          end
          object EdtPrecoVendaRetirado: TEdit
            Left = 116
            Top = 70
            Width = 72
            Height = 20
            Color = 15663069
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 2
            OnExit = EdtPrecoVendaRetiradoExit
          end
        end
        object edtCest: TEdit
          Left = 580
          Top = 42
          Width = 70
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 14
          OnKeyDown = edtCestKeyDown
        end
      end
    end
    object tsImposto: TTabSheet
      Caption = '&2 - Imposto'
      ImageIndex = 1
      object Panelimposto: TPanel
        Left = 0
        Top = 0
        Width = 892
        Height = 468
        Align = alClient
        TabOrder = 0
        object Label16: TLabel
          Left = 528
          Top = 297
          Width = 71
          Height = 13
          Caption = '% Agregado'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          Visible = False
        end
        object LbSituacaoTributaria_TabelaA: TLabel
          Left = 496
          Top = 353
          Width = 108
          Height = 13
          Caption = 'Situa'#231#227'o Tribut'#225'ria'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          Visible = False
        end
        object PanelICMSEstado: TPanel
          Left = 300
          Top = 206
          Width = 293
          Height = 262
          TabOrder = 6
          Visible = False
          object LbAliquota_ICMS_Estado: TLabel
            Left = 14
            Top = 145
            Width = 72
            Height = 13
            Caption = 'Al'#237'quota (%)'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object LbReducao_BC_ICMS_Estado: TLabel
            Left = 15
            Top = 186
            Width = 189
            Height = 13
            Caption = 'Redu'#231#227'o de Base de C'#225'lculo (%)'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object LbAliquota_ICMS_Cupom_Estado: TLabel
            Left = 15
            Top = 227
            Width = 117
            Height = 13
            Caption = 'Al'#237'quota Cupom (%)'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label42: TLabel
            Left = 3
            Top = 4
            Width = 286
            Height = 13
            Alignment = taCenter
            AutoSize = False
            Caption = 'ICMS PARA VENDA NO ESTADO'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label43: TLabel
            Left = 15
            Top = 22
            Width = 244
            Height = 13
            Caption = 'Isento, diferido ou n'#227'o tributado no ICMS?'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label44: TLabel
            Left = 15
            Top = 62
            Width = 134
            Height = 13
            Caption = 'Substitui'#231#227'o Tribut'#225'ria?'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label45: TLabel
            Left = 15
            Top = 104
            Width = 191
            Height = 13
            Caption = 'Valor da Pauta (Regime Especial)'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object EdtAliquota_ICMS_Estado: TEdit
            Left = 14
            Top = 158
            Width = 72
            Height = 20
            MaxLength = 9
            TabOrder = 3
          end
          object EdtReducao_BC_ICMS_Estado: TEdit
            Left = 14
            Top = 199
            Width = 72
            Height = 20
            MaxLength = 9
            TabOrder = 4
          end
          object EdtAliquota_ICMS_Cupom_Estado: TEdit
            Left = 14
            Top = 240
            Width = 72
            Height = 20
            MaxLength = 9
            TabOrder = 5
          end
          object comboisentoicms_estado: TComboBox
            Left = 14
            Top = 36
            Width = 89
            Height = 21
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            TabOrder = 0
            Text = 'comboisentoicms_estado'
            Items.Strings = (
              'N'#227'o'
              'Sim')
          end
          object comboSUBSTITUICAOICMS_ESTADO: TComboBox
            Left = 14
            Top = 76
            Width = 89
            Height = 21
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            TabOrder = 1
            Text = 'comboisentoicmsestado'
            Items.Strings = (
              'N'#227'o'
              'Sim')
          end
          object edtVALORPAUTA_SUB_TRIB_ESTADO: TEdit
            Left = 14
            Top = 117
            Width = 72
            Height = 20
            MaxLength = 9
            TabOrder = 2
          end
        end
        object Panel1: TPanel
          Left = 1
          Top = 1
          Width = 890
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          Color = 10643006
          TabOrder = 0
          object LbClassificaoFiscal: TLabel
            Left = 8
            Top = 7
            Width = 113
            Height = 13
            Caption = 'Classifica'#231#227'o Fiscal '
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object EdtClassificacaoFiscal: TEdit
            Left = 181
            Top = 4
            Width = 60
            Height = 20
            MaxLength = 15
            TabOrder = 0
            Text = '1'
          end
        end
        object PanelICMSForaEstado: TPanel
          Left = 132
          Top = 220
          Width = 293
          Height = 262
          TabOrder = 1
          Visible = False
          object Label46: TLabel
            Left = 14
            Top = 136
            Width = 72
            Height = 13
            Caption = 'Al'#237'quota (%)'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label47: TLabel
            Left = 14
            Top = 182
            Width = 189
            Height = 13
            Caption = 'Redu'#231#227'o de Base de C'#225'lculo (%)'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label48: TLabel
            Left = 16
            Top = 222
            Width = 117
            Height = 13
            Caption = 'Al'#237'quota Cupom (%)'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label49: TLabel
            Left = 1
            Top = 1
            Width = 291
            Height = 13
            Align = alTop
            Alignment = taCenter
            AutoSize = False
            Caption = 'ICMS PARA VENDA FORA DO ESTADO'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label50: TLabel
            Left = 6
            Top = 20
            Width = 244
            Height = 13
            Caption = 'Isento, diferido ou n'#227'o tributado no ICMS?'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label51: TLabel
            Left = 14
            Top = 60
            Width = 134
            Height = 13
            Caption = 'Substitui'#231#227'o Tribut'#225'ria?'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object Label52: TLabel
            Left = 14
            Top = 100
            Width = 191
            Height = 13
            Caption = 'Valor da Pauta (Regime Especial)'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
          end
          object EdtAliquota_ICMS_ForaEstado: TEdit
            Left = 14
            Top = 154
            Width = 72
            Height = 20
            MaxLength = 9
            TabOrder = 3
            Text = '0'
          end
          object EdtReducao_BC_ICMS_ForaEstado: TEdit
            Left = 14
            Top = 198
            Width = 72
            Height = 20
            MaxLength = 9
            TabOrder = 4
            Text = '0'
          end
          object EdtAliquota_ICMS_Cupom_ForaEstado: TEdit
            Left = 16
            Top = 238
            Width = 72
            Height = 20
            MaxLength = 9
            TabOrder = 5
            Text = '0'
          end
          object comboisentoicms_foraestado: TComboBox
            Left = 14
            Top = 36
            Width = 89
            Height = 21
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            TabOrder = 0
            Text = 'N'#195'O'
            Items.Strings = (
              'N'#227'o'
              'Sim')
          end
          object comboSUBSTITUICAOICMS_FORAESTADO: TComboBox
            Left = 14
            Top = 76
            Width = 89
            Height = 21
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            TabOrder = 1
            Text = 'SIM'
            Items.Strings = (
              'N'#227'o'
              'Sim')
          end
          object edtVALORPAUTA_SUB_TRIB_FORAESTADO: TEdit
            Left = 14
            Top = 117
            Width = 72
            Height = 20
            MaxLength = 9
            TabOrder = 2
            Text = '0'
          end
        end
        object edtpercentualagregado: TEdit
          Left = 528
          Top = 315
          Width = 73
          Height = 20
          MaxLength = 15
          TabOrder = 2
          Text = '1'
          Visible = False
        end
        object EdtSituacaoTributaria_TabelaA: TEdit
          Left = 496
          Top = 371
          Width = 25
          Height = 20
          Color = clGradientActiveCaption
          MaxLength = 9
          TabOrder = 3
          Text = '1'
          Visible = False
          OnExit = edtSituacaoTributaria_TabelaAExit
          OnKeyDown = edtSituacaoTributaria_TabelaAKeyDown
        end
        object EdtSituacaoTributaria_TabelaB: TEdit
          Left = 528
          Top = 371
          Width = 57
          Height = 20
          Color = clSkyBlue
          MaxLength = 9
          TabOrder = 4
          Text = '1'
          Visible = False
          OnExit = edtSituacaoTributaria_TabelaBExit
          OnKeyDown = edtSituacaoTributaria_TabelaBKeyDown
        end
        inline FRImposto_ICMS1: TFRImposto_ICMS
          Left = 1
          Top = 27
          Width = 890
          Height = 440
          Align = alClient
          Color = clWhite
          ParentColor = False
          TabOrder = 5
          inherited Panel_TOP: TPanel
            Width = 890
            Height = 35
            BevelOuter = bvNone
            Color = 10643006
            inherited Label17: TLabel
              Left = 252
              Top = 6
              Height = 14
              Font.Charset = ANSI_CHARSET
              Font.Color = clWhite
              Font.Name = 'Arial'
              ParentFont = False
            end
            inherited Label18: TLabel
              Left = 364
              Top = 6
              Width = 70
              Height = 14
              Font.Charset = ANSI_CHARSET
              Font.Color = clWhite
              Font.Name = 'Arial'
              ParentFont = False
            end
            inherited lbnometipocliente: TLabel
              Left = 522
              Top = 6
              Width = 70
              Height = 14
              Font.Charset = ANSI_CHARSET
              Font.Color = clWhite
              Font.Name = 'Arial'
              ParentFont = False
            end
            inherited lbcodigo_material_icms: TLabel
              Left = 60
              Top = 5
              Width = 41
              Height = 15
              Font.Charset = ANSI_CHARSET
              Font.Color = 6073854
              Font.Name = 'Arial Black'
            end
            inherited Label1: TLabel
              Top = 6
              Height = 14
              Font.Charset = ANSI_CHARSET
              Font.Color = clWhite
              Font.Name = 'Arial'
              ParentFont = False
            end
            inherited Label33: TLabel
              Left = 116
              Top = 6
              Width = 48
              Height = 14
              Font.Charset = ANSI_CHARSET
              Font.Color = clWhite
              Font.Name = 'Arial'
              ParentFont = False
            end
            inherited btReplicarImpostos: TSpeedButton
              Left = 841
              Height = 34
            end
            inherited edtestado_material_icms: TEdit
              Left = 300
              Top = 4
              Width = 60
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Verdana'
              ParentFont = False
            end
            inherited edttipocliente_material_icms: TEdit
              Left = 460
              Top = 4
              Width = 60
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Verdana'
              ParentFont = False
            end
            inherited edtoperacao: TEdit
              Left = 181
              Top = 4
              Width = 60
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Name = 'Verdana'
              ParentFont = False
            end
            inherited Button1: TButton
              Left = 760
              Top = 43
              Height = 27
            end
          end
          inherited STRG_Imposto: TStringGrid
            Top = 308
            Width = 890
            Height = 132
            Font.Color = clBlack
            Font.Name = 'Verdana'
          end
          inherited Aba_Impostos: TPageControl
            Top = 35
            Width = 890
            Images = il1
            inherited TabSheet1: TTabSheet
              ImageIndex = 5
              inherited LbMODALIDADE: TLabel
                Top = 56
              end
              inherited LbPERC_REDUCAO_BC: TLabel
                Top = 54
              end
              inherited LbALIQUOTA: TLabel
                Top = 54
              end
              inherited LbIVA: TLabel
                Left = 544
                Top = 54
              end
              inherited LbPAUTA: TLabel
                Left = 673
                Top = 54
              end
              inherited LbMODALIDADE_ST: TLabel
                Top = 79
              end
              inherited LbPERC_REDUCAO_BC_ST: TLabel
                Top = 79
              end
              inherited LbALIQUOTA_ST: TLabel
                Top = 79
              end
              inherited LbIVA_ST: TLabel
                Left = 544
                Top = 77
              end
              inherited LbPAUTA_ST: TLabel
                Left = 673
                Top = 77
              end
              inherited lbnomeCFOP_ICMS: TLabel
                Left = 675
              end
              inherited LbNomeSTA_ICMS: TLabel
                Left = 168
              end
              inherited Label4: TLabel
                Top = 113
              end
              inherited Label5: TLabel
                Top = 162
              end
              inherited Label26: TLabel
                Top = 138
              end
              inherited Label27: TLabel
                Top = 187
              end
              inherited lbCfopForaEstado: TLabel
                Left = 675
              end
              inherited EdtPERC_REDUCAO_BC_ICMS: TEdit
                Top = 49
                Width = 70
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtALIQUOTA_ICMS: TEdit
                Top = 49
                Width = 70
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtIVA_ICMS: TEdit
                Left = 602
                Top = 49
                Width = 70
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtPAUTA_ICMS: TEdit
                Top = 49
                Width = 70
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtPERC_REDUCAO_BC_ST_ICMS: TEdit
                Top = 76
                Width = 70
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtALIQUOTA_ST_ICMS: TEdit
                Top = 73
                Width = 70
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtIVA_ST_ICMS: TEdit
                Left = 602
                Top = 74
                Width = 70
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtPAUTA_ST_ICMS: TEdit
                Top = 74
                Width = 70
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtCFOP_ICMS: TEdit
                Left = 602
                Width = 70
                Height = 19
                Font.Color = clBlack
                Font.Name = 'Verdana'
                ParentFont = False
              end
              inherited ComboMODALIDADE_ICMS: TComboBox
                Top = 49
                Height = 21
                Font.Name = 'Verdana'
                ItemHeight = 13
              end
              inherited ComboMODALIDADE_ST_ICMS: TComboBox
                Top = 76
                Height = 21
                Font.Name = 'Verdana'
                ItemHeight = 13
              end
              inherited edtSTA_ICMS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtSTB_ICMS: TEdit
                Left = 136
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited edtformula_bc_ICMS: TEdit
                Top = 107
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited edtformula_bc_st_ICMS: TEdit
                Top = 161
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EDTImpostoModelo_ICMS: TEdit
                Width = 70
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited edtFORMULA_VALOR_IMPOSTO_ICMS: TEdit
                Top = 133
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited edtFORMULA_VALOR_IMPOSTO_ST_ICMS: TEdit
                Top = 186
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited edtCFOPForaEstado: TEdit
                Left = 602
                Width = 70
              end
              inherited edtCSOSN: TEdit
                Width = 70
                Height = 19
                Font.Name = 'Verdana'
              end
            end
            inherited TabSheet2: TTabSheet
              ImageIndex = 5
              inherited edtimposto_ipi_modelo: TEdit
                Width = 57
                Height = 19
                Color = 6073854
                Font.Name = 'Verdana'
              end
              inherited EdtST_IPI: TEdit
                Height = 19
                Color = 6073854
                Font.Name = 'Verdana'
              end
              inherited EdtClasseEnq_IPI: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtCodigoEnquadramento_IPI: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtCnpjProdutor_IPI: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtCodigoSelo_IPI: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtQuantidadeTotalUP_IPI: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtValorUnidade_IPI: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtFORMULABASECALCULO_IPI: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited ComboTipoCalculo_IPI: TComboBox
                Height = 21
                Font.Name = 'Verdana'
                ItemHeight = 13
              end
              inherited edtaliquota_ipi: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited edtFORMULA_VALOR_IMPOSTO_IPI: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
            end
            inherited TabSheet3: TTabSheet
              ImageIndex = 5
              inherited EdtST_PIS: TEdit
                Height = 19
                Color = 6073854
                Font.Name = 'Verdana'
              end
              inherited EdtALIQUOTAPERCENTUAL_PIS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtALIQUOTAVALOR_PIS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtALIQUOTAPERCENTUAL_ST_PIS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtALIQUOTAVALOR_ST_PIS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited ComboTipoCalculo_PIS: TComboBox
                Height = 21
                Font.Name = 'Verdana'
                ItemHeight = 13
              end
              inherited comboTIPOCALCULO_ST_PIS: TComboBox
                Height = 21
                Font.Name = 'Verdana'
                ItemHeight = 13
              end
              inherited EdtFORMULABASECALCULO_PIS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtFORMULABASECALCULO_ST_PIS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited edtimposto_pis_modelo: TEdit
                Height = 19
                Color = 6073854
                Font.Name = 'Verdana'
              end
              inherited edtFORMULA_VALOR_IMPOSTO_PIS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited edtFORMULA_VALOR_IMPOSTO_ST_PIS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
            end
            inherited TabSheet4: TTabSheet
              ImageIndex = 5
              inherited Label25: TLabel
                Width = 101
                Height = 13
                Font.Name = 'Verdana'
              end
              inherited EdtST_COFINS: TEdit
                Height = 19
                Color = 6073854
                Font.Name = 'Verdana'
              end
              inherited EdtALIQUOTAPERCENTUAL_COFINS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtALIQUOTAVALOR_COFINS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtALIQUOTAPERCENTUAL_ST_COFINS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtALIQUOTAVALOR_ST_COFINS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited ComboTipoCalculo_COFINS: TComboBox
                Height = 21
                Font.Name = 'Verdana'
                ItemHeight = 13
              end
              inherited comboTIPOCALCULO_ST_COFINS: TComboBox
                Height = 21
                Font.Name = 'Verdana'
                ItemHeight = 13
              end
              inherited EdtFORMULABASECALCULO_COFINS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited EdtFORMULABASECALCULO_ST_COFINS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited edtimposto_cofins_modelo: TEdit
                Height = 19
                Color = 6073854
                Font.Name = 'Verdana'
              end
              inherited edtFORMULA_VALOR_IMPOSTO_COFINS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
              inherited edtFORMULA_VALOR_IMPOSTO_ST_COFINS: TEdit
                Height = 19
                Font.Name = 'Verdana'
              end
            end
          end
          inherited PanelBotoes: TPanel
            Top = 280
            Width = 890
            inherited BtGravar_material_icms: TButton
              Height = 21
              Font.Color = clWhite
            end
            inherited btcancelar_material_ICMS: TButton
              Height = 21
              Font.Color = clWhite
            end
            inherited btexcluir_material_ICMS: TButton
              Height = 21
              Font.Color = clWhite
            end
          end
        end
      end
    end
    object tsCor: TTabSheet
      Caption = '&3 - Cor'
      ImageIndex = 2
      object panelCorFundo: TPanel
        Left = 0
        Top = 0
        Width = 892
        Height = 468
        Align = alClient
        TabOrder = 0
        object pnl2: TPanel
          Left = 1
          Top = 175
          Width = 890
          Height = 40
          Align = alTop
          BevelOuter = bvNone
          Color = 14024703
          TabOrder = 0
          DesignSize = (
            890
            40)
          object btgravarcor: TBitBtn
            Left = 769
            Top = 1
            Width = 39
            Height = 39
            Anchors = [akRight]
            TabOrder = 0
            OnClick = btGravarCorClick
          end
          object btexcluircor: TBitBtn
            Left = 808
            Top = 1
            Width = 39
            Height = 39
            Anchors = [akRight]
            TabOrder = 1
            OnClick = BtExcluirCorClick
          end
          object btcancelacor: TBitBtn
            Left = 847
            Top = 1
            Width = 39
            Height = 39
            Anchors = [akRight]
            TabOrder = 2
            OnClick = BtCancelarCorClick
          end
        end
        object DBGRIDCOR: TDBGrid
          Left = 1
          Top = 215
          Width = 890
          Height = 252
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          ParentFont = False
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clBlack
          TitleFont.Height = -11
          TitleFont.Name = 'Verdana'
          TitleFont.Style = []
          OnDrawColumnCell = DBGRIDCORDrawColumnCell
          OnDblClick = DBGRIDCORDblClick
          OnKeyPress = DBGRIDCORKeyPress
        end
        object panelCor: TPanel
          Left = 1
          Top = 1
          Width = 890
          Height = 174
          Align = alTop
          BevelOuter = bvNone
          Color = 10643006
          TabOrder = 2
          object LbCor: TLabel
            Left = 6
            Top = 6
            Width = 17
            Height = 14
            Caption = 'Cor'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object lbnomecor: TLabel
            Left = 95
            Top = 42
            Width = 453
            Height = 13
            Cursor = crHandPoint
            AutoSize = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clAqua
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = lbnomecorClick
            OnMouseMove = LbNomeFornecedorMouseMove
            OnMouseLeave = LbNomeFornecedorMouseLeave
          end
          object LbEstoque: TLabel
            Left = 6
            Top = 117
            Width = 54
            Height = 13
            Caption = '1000 UN'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = LbEstoqueClick
            OnMouseMove = LbEstoqueMouseMove
            OnMouseLeave = LbEstoqueMouseLeave
          end
          object Label8: TLabel
            Left = 711
            Top = 48
            Width = 43
            Height = 14
            Caption = 'Instalado'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object Label11: TLabel
            Left = 711
            Top = 68
            Width = 48
            Height = 14
            Caption = 'Fornecido'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object Label13: TLabel
            Left = 711
            Top = 89
            Width = 40
            Height = 14
            Caption = 'Retirado'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object LbCusto: TLabel
            Left = 6
            Top = 137
            Width = 76
            Height = 26
            Caption = 'Custo'#13#10'R$ 1.200,50'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label15: TLabel
            Left = 6
            Top = 57
            Width = 96
            Height = 14
            Caption = 'Classifica'#231#227'o Fiscal'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object lbestoque1: TLabel
            Left = 6
            Top = 100
            Width = 52
            Height = 13
            Caption = 'Estoque'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label12: TLabel
            Left = 162
            Top = 100
            Width = 102
            Height = 13
            Caption = 'Estoque M'#237'nimo'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EdtCorReferencia: TEdit
            Left = 6
            Top = 20
            Width = 66
            Height = 20
            Color = 6073854
            MaxLength = 9
            TabOrder = 0
            OnDblClick = EdtCorReferenciaDblClick
            OnExit = EdtCorReferenciaExit
            OnKeyDown = EdtCorReferenciaKeyDown
          end
          object edtcodigoCOR: TEdit
            Left = 289
            Top = 8
            Width = 72
            Height = 20
            MaxLength = 9
            TabOrder = 1
            Visible = False
          end
          object EdtCor: TEdit
            Left = 75
            Top = 20
            Width = 18
            Height = 20
            Color = 6073854
            Enabled = False
            MaxLength = 9
            TabOrder = 2
            Visible = False
          end
          object GroupBoxVidros: TGroupBox
            Left = 162
            Top = 44
            Width = 367
            Height = 53
            Caption = 'Margem de lucro'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            object LbPorcentagemAcrescimo: TLabel
              Left = 81
              Top = 15
              Width = 30
              Height = 14
              Caption = '% Cor'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWhite
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
            end
            object LbAcrescimoExtra: TLabel
              Left = 227
              Top = 15
              Width = 38
              Height = 14
              Caption = '% Extra'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWhite
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
            end
            object LbPorcentagemAcrescimoFinal: TLabel
              Left = 304
              Top = 15
              Width = 35
              Height = 14
              Caption = '% Final'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWhite
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
            end
            object Label7: TLabel
              Left = 153
              Top = 15
              Width = 41
              Height = 14
              Caption = 'R$ Extra'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWhite
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
            end
            object Label9: TLabel
              Left = 10
              Top = 15
              Width = 44
              Height = 14
              Caption = 'R$ Custo'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWhite
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
            end
            object EdtPorcentagemAcrescimo: TEdit
              Left = 82
              Top = 29
              Width = 54
              Height = 19
              Color = 15663069
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 9
              ParentFont = False
              TabOrder = 0
            end
            object EdtAcrescimoExtra: TEdit
              Left = 227
              Top = 29
              Width = 54
              Height = 19
              Color = 15663069
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 9
              ParentFont = False
              TabOrder = 3
              OnExit = EdtAcrescimoExtraExit
            end
            object EdtPorcentagemAcrescimoFinal: TEdit
              Left = 304
              Top = 29
              Width = 54
              Height = 19
              Color = 15663069
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 9
              ParentFont = False
              TabOrder = 1
            end
            object EdtValorExtra: TEdit
              Left = 153
              Top = 29
              Width = 54
              Height = 19
              Color = 15663069
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 9
              ParentFont = False
              TabOrder = 2
              OnExit = EdtValorExtraExit
            end
            object EdtValorCusto: TEdit
              Left = 9
              Top = 29
              Width = 54
              Height = 19
              Color = 15663069
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 9
              ParentFont = False
              TabOrder = 4
            end
          end
          object EdtValorFinal: TEdit
            Left = 266
            Top = 7
            Width = 54
            Height = 19
            Color = 15663069
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 4
            Visible = False
          end
          object edtinstaladofinal: TEdit
            Left = 815
            Top = 45
            Width = 74
            Height = 19
            Color = 15663069
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 5
          end
          object edtfornecidofinal: TEdit
            Left = 815
            Top = 65
            Width = 74
            Height = 19
            Color = 15663069
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 6
          end
          object edtretiradofinal: TEdit
            Left = 815
            Top = 86
            Width = 74
            Height = 19
            Color = 15663069
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            TabOrder = 7
          end
          object Rg_forma_de_calculo_percentual: TRadioGroup
            Left = 709
            Top = 5
            Width = 180
            Height = 33
            Caption = 'Calcular % de Acr'#233'scimo'
            Columns = 2
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ItemIndex = 0
            Items.Strings = (
              'Custo'
              'Valor Final')
            ParentFont = False
            TabOrder = 8
          end
          object edtclassificacaofiscal_cor: TEdit
            Left = 6
            Top = 71
            Width = 120
            Height = 20
            TabOrder = 9
          end
          object edtEstoqueMinimo: TEdit
            Left = 162
            Top = 114
            Width = 120
            Height = 20
            TabOrder = 10
          end
        end
      end
    end
  end
  object il1: TImageList
    Left = 864
    Top = 165
    Bitmap = {
      494C01011E002200040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000009000000001002000000000000090
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00F3F3F30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FEFE
      FE00F7F7F700EBEBEB00F2F2F20000000000FDFDFD0000000000FEFEFE00FDFD
      FD00FEFEFE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FCFEFE00FBFDFD00EEFE
      E600F8E8F300FFF5FC00E6DAD600E4D7C700FCF0CE00FFFFF900FFFFF700FFFF
      F300FFFCFF00FFFFF200F2FBFE00FFFBFF000000000000000000000000000000
      0000D8D5D400D7D4D400D7D4D400D7D4D400D7D4D400D7D4D400D7D4D400D8D4
      D400DCD9D8000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DED9
      E800860D7F00951CA2008D269F00E6CFFF00F2DFBE00F1ECAD00E8C47800D297
      4100B78A3900E8DCB800FCFFFB00FFFBFF000000000000000000000000009975
      4C00916A3F00A4856300A4846300A3846300A3846300A4846300A38360009A79
      520096795800DAD8D50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFFFF00FFFEFF00E6DBDD007321
      92008C23BA003D3FAB006067D6005C4DC000F6E1FF00F5DC9E00FAE49D00D49F
      4400D0A04000F7E3B300FDFFFC00FFFBFF000000000000000000000000009562
      2900B9966E00D4D2CF00CECAC600CECBC700CECBC700CECAC600D6D2CE00CFB5
      9800A6815800D7D3D00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFFFF00FFFEFF00EBE0E200A00F
      C9006E66F3006073EC003651C000810EBC00C281D000E6E89600FFE0A000D9AD
      5000EEB05200FAEAC500F8FEFF00FFFCFF000000000000000000000000009E66
      2800C19C7300CCCDCE00C4C4C400C4C4C400C4C4C400C3C4C400D0D1D100C099
      6D00925A1E00D8D5D00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DCDCDC007537
      AF003C74E3004D47BC006B53D5009A37CD00FFE3FF00FFDFB200EFD88A00F1C3
      5E00E9C95E00FFF7DF00FEFCFF00FDFFFC00000000000000000000000000A86B
      2800C79F7200CBCCCD00C2C2C200C2C3C300C2C3C300C2C2C200CFCFD000C79C
      6D009B5F1E00D8D5D00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFE00FFF9
      FF00AB5ABB009957D4009961C000DEC8F900B8D6CB00ACE6C300D7F1BB00F4FF
      CA00EBE7B700FFFFF300FFFEFF00FFFFFC00000000000000000000000000B170
      2800CEA37300C9C8C800C0BEBD00C0BFBD00C0BFBD00C0BEBD00CCCCCB00CE9F
      6B00A4641E00D9D5D00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EFFF
      FC00FFFEFA00FFF7FF00FFF5FF00A4DFD10072DAC30057D4AC003DAA7C0026A0
      7200F2FFFA00FAFEF800FEFCFF00FFFFFB00000000000000000000000000BE7C
      3300CE8D4600D3A16A00D09F6800D09F6800CF9E6800CE9D6700D2A06A00CB86
      3A00AF6B2200D9D5D00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFEFF00FFFFFE00FBFDFF00FFFF
      FC00FFFFFB00DBCAB000FEFEFE00F4FFFF00DEFFED0086D3A70064BD920050B1
      8F00FFFFFE00000000000000000000000000000000000000000000000000CC8D
      4A00D28633006D441700341F080038220900503110005233100052331000C57D
      2E00BC7A3200D9D5D00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFEFF0000000000FFFFFE00F8FF
      FF00FDDFA600FFE29B00F5EBC300FAD6A600F8C66E00EBAE5800EEFFE300EBFB
      FF00FEFFFB00000000000000000000000000000000000000000000000000D598
      5400DE923F005A3A16000F080000170D020098642A00AC71300041290F00CC87
      3900C88A4600D9D5D10000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FBFFFF0000000000FFFFF600FCEC
      B700E9AD4D00D2A04600DDAC4000E3AE4600C6A04000F1E2C100FFFFFE000000
      0000FFFFFE00000000000000000000000000000000000000000000000000DDA1
      5E00E99E4A00734A1C002F190200361F0500B4773500C7853C005D3A1400D992
      4400D1975600DEDAD60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FBFEFF00FFFEFF00FEFAE700FCD2
      9100DCA13E00DBA54600E1A53B00F7D58F00F5E7CA00FFFFF700000000000000
      000000000000000000000000000000000000000000000000000000000000DEAC
      7400E4A45F0090673A006043230063462500835D33008760340079572F00D69C
      5B00DABA98000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FBFFFE00FEFCFB00FFFCF700FFFC
      E500ECC07300FFD98700E5CFA500FFFFF200FAFDFF00FBFDFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFFFE00FDFFFC00FDFFFF00FDFE
      FF00FFFBE400FBDEAB00FFFFFC0000000000FFFEFF00FFFEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFEFF00FDFFFC00FBFFFF00FFFF
      F900F1FDFF00FFFFED00FDFFFF0000000000FDFFFF00FAFEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D9D9D900D4D4D400D5D5D500DADADA00946F6000854022008D645000D8D0
      CD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000053575500535755005357
      5500535755005357550053575500535755005357550053575500535755005357
      5500535755005357550053575500000000000000000000000000000000000000
      000000000000000000000000000000000000894324008A422200894222009865
      4C00000000000000000000000000000000000000000000000000C2C0C000A5A5
      A200BBBBBA00000000000000000000000000000000000000000000000000BBBB
      BA00A5A5A200C0C0C00000000000000000000000000000000000000000000000
      0000D9D9D900CECECE00BAC4C7008DBAC400AEBDC200C4C4C400CCCCCC00D6D6
      D6000000000000000000000000000000000092928E0053575500CBD3CF00CFD7
      D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7
      D300CFD7D300CFD7D3005357550092928E000000000000000000854122000000
      0000000000000000000000000000A2715B008B422200884122008C442400EBE4
      E10000000000000000000000000000000000000000006E6C6800434850005D67
      72004B525E006F6D6900000000000000000000000000000000006D6D69004E53
      5F00646B7200474A50006E6B690000000000000000000000000000000000D3D3
      D300C3C3C300B4B4B40056B6C80041C5DB0034BBD4005CBBCD0078ACB800BFBF
      BF00D0D0D0000000000000000000000000004547430053575500CBD3CF003634
      2E0036342E0036342E0036342E0036342E0036342E0036342E0036342E003634
      2E0036342E00CFD7D30053575500454743000000000000000000854122008456
      43000000000000000000000000008B4323008B4222008C442400B88D77000000
      00000000000000000000000000000000000099979400395081006B96F00079A7
      FF00658FF600415494009C9B980000000000000000009C9B9800495C97007BA2
      F7008DB6FF007AA2F2003F56840099979400000000000000000000000000D7D7
      D700C8C8C80083B4BE003BC5DB0024C3DA001AC2DB0030C2D9004CCBDF0040B4
      CB00AEC9CF00000000000000000000000000565A580053575500565A5800565B
      5900565B5900565B5900565B5900565B5900565B5900565B5900565B5900565B
      5900565B5900565B590053575500565A58000000000000000000854122008641
      21009A6B5400000000009C5E41008C4323008B4222008E462600000000000000
      000000000000000000000000000000000000797978002958D1003066FF003664
      F800315FFF002242CF00848483000000000000000000848383003D5EDA003E72
      FF004779FF004780FF002664DB00797978000000000000000000000000000000
      0000000000005BC3D50018C2DB001DC3DD0036D2E80024C1DA000FB8D3003BCF
      E2003FBDD500AEDCE6000000000000000000858A8800FFFFFF00CFD7D300CFD7
      D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7D300CFD7
      D300CFD7D300CFD7D300FFFFFF00858A88000000000000000000854122008541
      220084402100AB775F008C4424008D4222008D442400CFAB9800000000000000
      000000000000000000000000000000000000AEAEAB0033407800223AC4003248
      D7002236C2002F366D008D8D8B0000000000000000008D8D8C00434B78003D58
      D4004865E800254ACE00293F7B00AEAEAB000000000000000000000000000000
      0000000000002EC1D9001ACDE60025D1EB0020C5E0001DC1DB001EC6DC0028BE
      D7005BCEE0007CD0E1000000000000000000858A8800FFFFFF00D2D9D6003634
      2E0036342E0036342E0036342E0036342E0036342E0036342E0036342E003634
      2E0036342E0073AEE600FFFFFF00858A8800000000000000000095522E008E47
      2500873F2000904624008F452400904423009148280000000000000000000000
      000000000000000000000000000000000000000000007D7C77005F5E65006465
      780048484F002D2C2400A1A1A1000000000000000000A1A1A1002C2B25004C4C
      5200686A7D005C5D66007C7A7500000000000000000000000000000000000000
      0000C5E8F0003BD2E8002FD9F10037DAF20024CDE80016C5DE001FC0D80061CE
      E000BCE5EE00000000000000000000000000858A8800FFFFFF00DBE0DE003836
      310050504B0050504B0050504B0050504B0050504B0050504B0050504B005050
      4B0038363100DBE0DE00FFFFFF00858A8800000000000000000095522E00964C
      28009043220095452300994724009B4A2600A4522D00A4522D00AA5D3800AC60
      3B00B26F52000000000000000000000000000000000077777700727270008A8A
      85003E3E3A0011111100A6A6A6000000000000000000A9A9A900131313003939
      37008888830073726F0077777700000000000000000000000000000000000000
      00008FBBD50050E2F6004FE3F80044DCF30025D4EC001AC5DD0059CADE00D4ED
      F30000000000000000000000000000000000858A8800FFFFFF00E3E7E6003F3E
      3A006B6D68006B6D68006B6D68006B6D68006B6D68006B6D68006B6D68006B6D
      68003F3E3A00E3E7E600FFFFFF00858A8800000000000000000095522E009D51
      2C00974623009B4824009D4925009F4A2600A04C2700A14D2800AE643F00D0A6
      8D00000000000000000000000000000000000000000068686800656565008A8A
      8A0061616100111111002A2A2A00CDCDCD00D6D6D6002C2C2C00121212005959
      59007D7D7D00676767006363630000000000000000000000000000000000ADAD
      CF000000AA003058B80069CFEA004CE6FB0033D7ED0055C8DD00000000000000
      000000000000000000000000000000000000858A8800FFFFFF00ECEEEE004546
      4300858985008589850085898500858985008589850085898500858985008589
      850045464300ECEEEE00FFFFFF00858A8800000000000000000095522E00A95D
      3400A3532C00A6562D00A7582E00A85A3000A95B3100BC7F5D00E3CBBD000000
      00000000000000000000000000000000000000000000A8A8A800545454007474
      740060606000161616000A0A0A006F6F6F00797979000A0A0A00161616005656
      560066666600555555009F9F9F00000000000000000000000000000000005CC3
      D900216FB800272CC3003B3ECE003779BE007ED3E40000000000000000000000
      000000000000000000000000000000000000898E8C00F8F8F800FFFFFF004C4E
      4C004C4E4C004C4E4C004C4E4C004C4E4C004C4E4C004C4E4C004C4E4C004C4E
      4C004C4E4C00FFFFFF00F8F8F800898E8C000000000000000000A15E3700B46D
      3F00AF653700B1683900B36B3B00B8734300CB997800ECDDD700000000000000
      00000000000000000000000000000000000000000000000000006D6D6D005F5F
      5F004E4E4E0042424200333333004F4F4F005454540032323200414141004D4D
      4D005F5F5F006D6D6D00DCDCDC000000000000000000000000000000000070C8
      DC0050D3E9006DD4ED00375FB8004040A8000000000000000000000000000000
      000000000000000000000000000000000000C9CBCA00898E8C00858A8800696E
      6C00FFFFFF00E0E5E200DFE5E200DFE4E100DEE4E000DDE3E000DDE2DF00FFFF
      FF00696E6C00858A8800898E8C00C9CBCA000000000000000000A9673D00BA76
      4700B46D3C00B6703E00BD7C4D00D1A58A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000969696008F8F
      8F0077777700666666005D5D5D0047474700484848005D5D5D00646464007373
      7300909090009595950000000000000000000000000000000000000000005693
      BA000E76AD0046B2CF0058CAE000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000041423D008086
      8300FFFFFF00E8ECEA00E7EBE900989D9B00989D9B00E5EAE700E5E9E700FFFF
      FF008086830041423D0000000000000000000000000000000000AF6D4200C07F
      4E00B8734100C78E6400D9B6A200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C9C
      9C00A2A2A2008F8F8F008080800067676700636363007E7E7E008A8A8A009B9B
      9B009B9B9B0000000000000000000000000000000000000000009CBED4002182
      BE001978B100C9DBE600D5EEF300000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004E514E00979D
      9A00FFFFFF00F0F3F1009A9E9D009A9E9C009A9E9C00999E9C00EDF0EE00FFFF
      FF00979D9A004E514E0000000000000000000000000000000000B4724500C78D
      6000C7936F00E2C8B90000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CECE
      CE00696969007E7E7E00A3A3A3008F8F8F0086868600A2A2A2007B7B7B006B6B
      6B00C7C7C70000000000000000000000000000000000DBE6ED00398ABD00137E
      C1005899C1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A5A7A600AEB5
      B200FFFFFF009CA09E009CA09E009BA09E009BA09E009B9F9E009B9F9E00FFFF
      FF00AEB5B200A5A7A60000000000000000000000000000000000B4724500D5AD
      9300EDE0D7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009E9E9E0086868600C5C5C500B1B1B100ADADAD00C8C8C800868686009C9C
      9C000000000000000000000000000000000000000000ABC7D9005192BA003585
      B700AECADB000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B5BC
      B900FEFEFE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFE
      FE00B5BCB9000000000000000000000000000000000000000000A4725C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007BABC90096BB
      D200000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C8CD
      CB00B5BCB900B5BCB900B5BCB900B5BCB900B5BCB900B5BCB900B5BCB900B5BC
      B900C7CCCA000000000000000000000000000000000000000000000000000000
      00000000000000000000DEDEDE00DEDEDE00DEDEDE00DEDEDE00000000000000
      000000000000000000000000000000000000767676002A2A2A002E2E2E006262
      6200000000000000000000000000000000000000000000000000000000000000
      00005D5D5D002E2E2E0029292900878787000D7C9100087A8F00087A8F00087A
      8F00087A8F00087A8F00087A8F00087A8F00087A8F00087A8F00087A8F001481
      9600C3E1EA000000000000000000000000000000000000000000000000000000
      0000000000008080800080808000808080008080800080808000808080008080
      8000000000000000000000000000000000000000000000000000000000000000
      000000000000B6B6B6009E9E9E009E9E9E009E9E9E009E9E9E00B6B6B6000000
      0000000000000000000000000000000000009999990001010100010101000C0C
      0C00B7B7B700DCDCDC00DDDDDD000000000000000000DBDBDB00DEDEDE00B2B2
      B200050505000101010001010100B0B0B00007798E00A3E6FF0051D0FF0041CA
      FF0042CAFF0042CAFF0042CAFF0042CAFF0042CAFF0041CAFF0044CAFF0087E4
      FF004597A7000000000000000000000000000000000000000000000000000000
      00000000000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000DEDEDE000066B4000071CF000073CF000076CF000078CF000070B400DEDE
      DE00000000000000000000000000000000000000000017171700010101000101
      0100898989000000000000000000000000000000000000000000000000007B7B
      7B0001010100010101002B2B2B0000000000087A8F004AA6BC0054D1FF0015BC
      FB0014BBFB0015BBFB0015BBFB0015BBFB0015BBFB0015BBFB0014BCFB003AC8
      FE0075C7DF00C5E2EA0000000000000000000000000000000000000000000000
      00008080800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00808080000000000000000000000000000000000000000000000000000000
      0000DEDEDE00006ECF00007CF300007EF3000081F3000082F3000078CF00DEDE
      DE0000000000000000000000000000000000000000005F5F5F00010101000101
      010031313100A8A8A800A8A8A800A6A6A600A6A6A600A9A9A900A3A3A3001D1D
      1D0001010100010101007D7D7D0000000000087A8F00239DBF0089DCF8002DC7
      FE001CC1FD001EC1FD001EC1FD001EC1FD001EC1FD001EC1FD001DC1FD0023C3
      FD0080E2FF004697A60000000000000000000000000000000000000000000000
      000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00808080000000000000000000000000000000000000000000000000000000
      0000DEDEDE00006DCF00007BF300007CF300007FF3000081F3000077CF00DEDE
      DE000000000000000000000000000000000000000000AFAFAF00010101000101
      0100010101000000000000000000000000000000000000000000000000000101
      01000101010009090900CFCFCF0000000000087A8F0036C9FF0050A8B9005FD9
      FF0025C7FD0025C7FD0025C8FD0025C8FD0025C8FD0025C8FD0025C8FD0024C7
      FD0048D2FF0071C4DA00CBE5EB00000000000000000000000000000000008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008080800000000000000000000000000000000000000000000000
      0000DEDEDE00006BCF000079F300007BF300007EF300007FF3000074CF00DEDE
      DE000000000000000000000000000000000000000000000000002A2A2A000101
      0100010101000101010001010100010101000101010001010100010101000101
      010001010100575757000000000000000000087A8F0041D1FF000E94BC0090E3
      FB0039D1FD002BCDFC002DCEFC002DCEFC002DCEFC002DCEFC002DCEFC002CCD
      FC0031CEFD0088E8FF004897A800000000000000000000000000808080008080
      8000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00808080000000000000000000000000000000000000000000DEDE
      DE00C6C6C600006ACF000077F3000079F300007BF300007CF3000072CF00C6C6
      C600DEDEDE000000000000000000000000000000000000000000898989000101
      0100000000000606060040404000484848004949490038383800000000000000
      000000000000B6B6B6000000000000000000087A8F0045D1FF001AC3FB0058AB
      BD0069DFFF0037D3FE0036D2FE0036D2FE0036D2FE0036D2FE0036D2FE0036D2
      FE0035D1FE0057DBFF0074C6DD00CFE7EA00000000000000000080808000FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008080800000000000000000000000000000000000BEBEBE00A2A2
      A20096969600006CD8002086F5000A7DF3000079F300007BF3000073D8009696
      9600A1A1A100BDBDBD0000000000000000000000000000000000D9D9D9000E0E
      0E000A0A0A0005050500BCBCBC00000000000000000094949400070707000707
      07002C2C2C00000000000000000000000000087A8F0047D4FF0026CDFF001399
      BA00A6E6FA008AE4FE0087E4FD0088E4FD0088E4FD0088E4FD0087E4FD0086E5
      FF0089E7FF008EE7FF00C0F7FF004F9BAC000000000000000000808080008080
      80008080800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000000000000000000000000000000000000058AB000063
      CB000067D800006FEA0064AAF80056A4F700419AF6002E90F5000076EA000072
      D800006ECB000064AC0000000000000000000000000000000000000000004E4E
      4E00202020001C1C1C006B6B6B0000000000000000003D3D3D00212121001616
      16007E7E7E00000000000000000000000000087A8F004CD6FF002ACDFD002BCA
      F90009819900087A8F00087A8F00087A8F00087A8F00087A8F00087A8F001080
      96007CB8B4005AA88800268B9B00278B9B000000000000000000000000000000
      00000000000080808000FFFFFF0080808000FFFFFF0080808000FFFFFF008080
      8000FFFFFF0080808000000000000000000000000000000000006C99C4003D8B
      E20076B1F60075B1F90072B0F8006CAEF80065ACF8005DA7F70054A4F7004A9E
      F4002585E0005D95C00000000000000000000000000000000000000000009C9C
      9C00292929003C3C3C002828280000000000BFBFBF00272727003B3B3B001919
      1900CFCFCF00000000000000000000000000087A8F004FD8FF002DCEFE0030CF
      FE0031D2FF0030D1FF0034D3FF004CD9FF0053DDFF0053DDFF006BE4FF0069BC
      DF004AB3620042C94A0000000000000000000000000000000000000000000000
      00000000000080808000FFFFFF0080808000FFFFFF0080808000FFFFFF008080
      8000808080000000000000000000000000000000000000000000000000006C99
      C500448FE30082B7F7007EB7F90078B3F90070B0F80067ACF8005DA6F5002E87
      E1006096C4000000000000000000000000000000000000000000000000000000
      00002D2D2D005757570034343400A8A8A8007D7D7D0051515100424242004646
      4600000000000000000000000000000000001784990076E4FF0036D1FE0032D0
      FE0030D0FE0037D1FE0071E2FF001D899F00087A8F00087A8F0018829B002EA4
      690066E6680068EB6D0079C78E00000000000000000000000000000000000000
      00000000000080808000FFFFFF0080808000FFFFFF0080808000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006D99C6004A92E3008ABCF70083B9F9007AB4F90070AEF500368AE1006498
      C400000000000000000000000000000000000000000000000000000000000000
      00005E5E5E005A5A5A0066666600484848004C4C4C007272720033333300A5A5
      A50000000000000000000000000000000000AAD2DA002D93A8007EE7FF0054DC
      FF0053DCFF007AE7FF002C93A900BCDCE00000000000000000008ED29F0074DF
      74006EE1720064DF690030BD3F00000000000000000000000000000000000000
      00000000000080808000FFFFFF00808080008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000006E9BC6004B92E3008CBCF70083B8F7003E8CE2006999C4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BCBCBC00434343008C8C8C003C3C3C00626262007F7F7F003B3B3B000000
      00000000000000000000000000000000000000000000CBE6EC0014819500087A
      8F00087A8F0016829800C5E0E5000000000000000000000000006AC1770037A6
      470067D16D004BC355002AB03E0069BE78000000000000000000000000000000
      00000000000080808000FFFFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000709BC6004A90E000458CE0006E9BC600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000051515100999999008E8E8E009C9C9C006C6C6C007E7E7E000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C8E9D90029A731008DD0A200C6E7D400D1EBDB0096D3A7005BBD
      690088D9890035B1460000000000000000000000000000000000000000000000
      00000000000080808000FFFFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000078A2CD0078A2CD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000898989008F8F8F00C4C4C400BBBBBB003B3B3B00D0D0D0000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BAE1C5005BBD65009CD89C009DD8A100B4E2B400B2E3
      B2007FCD830082CA910000000000000000000000000000000000000000000000
      00000000000080808000FFFFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D8D8D800555555008A8A8A006D6D6D0058585800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A4D8B20065BE76005ABA6B0059B9
      6900A5D9AF000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C2C2C20070707000B0B0B0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E9BDA600E0A07000E9BD
      A600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C5C5C5009696960056565600494949000000000000000000000000000000
      0000000000000000000000000000C0C0C000BFBFBF0000000000000000000000
      00000000000000000000000000000000000000000000E0A07000E9B58E00E0A0
      700000000000A7A7A7002A2A2A00232323001D1D1D0016161600121212000C0C
      0C00070707000303030000000000000000000000000000000000000000000000
      00004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      2200000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C5C5
      C500969696005656560057575700DEDEDE000000000000000000000000000000
      00000000000000000000B6B6B6002E76A1002B76A100B4B4B400000000000000
      00000000000000000000000000000000000000000000E9BDA700E0A07000E9BD
      A600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB12200000000000000000000000000000000000000000000000000C8C2
      BD00978B8100928476009F918300A2948800AFA59B00D6D2CE00DFDFDF009696
      96005656560057575700E0E0E000000000000000000000000000000000000000
      000000000000B8B8B8003176A600007ADB00007CDC002879AA00B4B4B4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB12200000000000000000000000000000000009E948B008F80
      7100C3B8AD00E8E1DC00EBE1DA00ECDDD300DAC7BA00B3A496007C7065007877
      770057575700E0E0E00000000000000000000000000000000000000000000000
      0000B9B9B9003676A4000077DD000080F0000083F000007EE0002879AA00B4B4
      B400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000000000009C948A009E908200EEEB
      E700FFFFFF00FFFEFD00FFFBFA00FFF8F500FFEEE600F0D9CB00A19183007970
      6600DDDDDD00000000000000000000000000000000000000000000000000BBBB
      BB003B75A3000074DC00007DF0000080F3000082F3000083F000007EDF002A78
      A800B5B5B50000000000000000000000000000000000EDC8B100E4AB8200EBC2
      AB00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000D9D6D30085756600E8E1DC00FFFE
      FD00FFFEFD00FFFCFB00FFFBF900FFF9F600FFF4EF00FFE9DE00F4D8C900AD9C
      8D00D1CEC8000000000000000000000000000000000000000000C0C0C0004075
      A1000070DB000078EF00007CF300007FF3000080F3000082F3000082F000007D
      DF002E78A700BCBCBC00000000000000000000000000E8B69200EDC3A400E6AD
      830000000000B9B9B900575757005050500049494900424242003B3B3B003333
      33002C2C2C0025252500000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB1220000000000000000009C948A00B4A69A00FEF9F600FFFB
      F900FFFBFA00FFFBF900FFF9F700FFF8F400FFF6F100FFECE300FFE4D700CBB1
      9E009F948A0000000000000000000000000000000000000000005787B100227F
      DE00278AF1000B7DF400007AF300007DF300007EF300007FF3000081F3000081
      F000007BDE004186B100000000000000000000000000F0D0B800E9B89400EECA
      B100000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB1220000000000000000008A7E7200D3C6BC00FFF7F300FFF8
      F600FFF9F600FFF9F600FFF8F400FFF6F200FFF5F000FFEADF00FFE2D400DABA
      A70088796B000000000000000000000000000000000000000000005BAB000066
      CB00006CD8000073EA003996F600208BF5000B82F400007DF300007BEA000076
      D8000072CC000069AE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000877A6D00E2D4CB00FFF4EF00FFF6
      F200DD764A00D06D4300C3643B00B75B3400A9522C00FFE9DF00FFE1D200D8B7
      A300837364000000000000000000000000000000000000000000000000000000
      0000DEDEDE00006CD8006DB1F8006AB0F8005DAAF700469FF6000073D800DEDE
      DE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000A2988F00DACBC000FFF2EC00FFF4
      EF00FFF4EF00FFF0E900FFEBE100FFECE300FFF1EA00FFF1EB00FEDDCE00CAA6
      8F007D6E5F000000000000000000000000000000000000000000000000000000
      0000DEDEDE000068CF0079B5F90076B5F90072B4F8006BB1F800006DCF00DEDE
      DE000000000000000000000000000000000000000000F1D2BC00EBC09F00F1D2
      BC00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000B7B1AB00C1B2A600FEEFE800FFED
      E400FFEAE000FFEEE600FFF2EC00FFF2EC00FFF1EB00FFF1E900F0CBB600AA8D
      7700A0958C000000000000000000000000000000000000000000000000000000
      0000DEDEDE000065CF0085BCF90082BAF9007CB8F90075B5F900006BCF00DEDE
      DE000000000000000000000000000000000000000000EBC09F00F1CFB400EBC0
      9F0000000000C4C4C40076767600737373006F6F6F006A6A6A00646464005F5F
      5F00585858005252520000000000000000000000000000000000000000004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB12200000000000000000000000000EDEBE900A4998E00E3D3C900FEF0
      E900FFF2EB00FFF2EB00FFF1EB00FFF1EA00FFF0E900F8E3D700C49C83008575
      6500D9D6D3000000000000000000000000000000000000000000000000000000
      0000DEDEDE000064CF0091C1FA008CBFFA0086BCF9007EB8F9000068CF00DEDE
      DE000000000000000000000000000000000000000000F1D2BC00EBC09F00F1D2
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB1220000000000000000000000000000000000C8C3BD00B2A69B00E1D0
      C500F8E8E000FDEDE500FDEEE600FBEBE200F4E2D800CAAD99008C796900A49C
      9500000000000000000000000000000000000000000000000000000000000000
      0000000000000062CF009AC5FA0096C4FA008FC0FA0086BBF9000066CF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C9C4BE00A89E
      9400C3B5AA00D6C5B900DCC9BD00CFBBAD00B6A598008A7C6E00AEA69F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000059B4000061CF000062CF000063CF000064CF00005CB4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F0EF
      EE00C3BDB700B5AEA600B3A79B00ADA49B00B3ACA500DDDAD700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DBDBDB00CBCBCB00CBCBCB00CBCB
      CB00CBCBCB00CBCBCB00CBCBCB00CBCBCB00CBCBCB00DBDBDB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C5D6DD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FDFDFD00FBFBFC00F8F8FA00F9F9FB00FDFCFD00FDFDFD000000
      0000000000000000000000000000000000008383830063646400636363006464
      6400656565006565650064646400636363006364640083838300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B2C4CC005E8392002E697F00557C8C00BECFD600000000000000
      000000000000000000000000000000000000000000000000000000000000F6F7
      F700E0E2E300C2BDB400B9AD8D00B9B08400BAB08800BFB49E00CFCCCB00E8E9
      EB00F6F6F70000000000000000000000000066656500706E6D00797574007B77
      760066656500696968007B77760079767400716E6E0060626600000000000000
      0000000000000000000000000000000000009B9B9B004E4E4E000B0B0B005050
      50007B7B7B009B9B9B00BBBBBB00D8D8D80000000000D5D5D500B7B7B7007A7A
      7A000E0E0E004F4F4F00606060009B9B9B00000000000000000000000000A6BA
      C200587783004C657400738899003D748D00627C9200436273007D9BA900DFE9
      ED00000000000000000000000000000000000000000000000000000000000000
      000091795C00E3983900FEC05400FECD5D00FFD26000FABC5200C38537009892
      8B000000000000000000000000000000000070707000A19F9D007C7877006666
      66005555550054555500656464007C787700A19F9E00595D6500CBCBCB00CBCB
      CB00CBCBCB00CBCBCB00CBCBCB00DBDBDB00000000001E1E1E00CCCCCC001E1E
      1E00000000000000000000000000000000000000000000000000000000001E1E
      1E00CCCCCC001E1E1E000000000000000000000000009DB1BA004E6C7B004760
      72007A8A9B0098A3B300A2B6CB00427892008EA8C500879CB700546E82004B6F
      7F00A9C0C900000000000000000000000000000000000000000000000000B0A5
      9700F2761800EA701300E77F1F00E4AB7E00EA8D3000E87E1A00F77D1900CB75
      2C00DEDEDE0000000000000000000000000066666500C4C2C100656565007474
      74003131310041414100797979005B5B5B00C0BEBE0057575600B67C0800B67B
      0600B47A0700B47B0900B67E0E00BD984F0000139D00000F7F0000021300000F
      7F0000139D006C7376006C7376006C7376006C7376006C7376006C737600575D
      60000D0E0E00575D60006C7376006C7376009FB3BB00354F61006F8097006F7D
      9300667182007E88980099AFC6003F758E008CA5C3006C92AF0052819D007286
      9E003E627300C2CCD0000000000000000000000000000000000000000000AC61
      3000C23E0000C43C0000CF440000ECD4CA00F2D9CB00CC450600C1370000DC5A
      0B00AA938100000000000000000000000000C4C4C4006F6F6D0081817F00C3C2
      C1008E8D8D008E8D8D00C1C0C0006C6C6B006B6C6B00C8CACD00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B67E0E00006AE200006AE200006AE200006A
      E200006AE2006C737600D8D8D800AEAEAE00AEAEAE00AEAEAE00AEAEAE00AEAE
      AE00AEAEAE00AEAEAE00BEBEBE007B81830096A9B1004F637700728199005E6C
      80007A8BA5008597B200849DB8003C718B0084A0BF004D7994002E657F008399
      B60047657900B5C2C50000000000000000000000000000000000C5C3BF00932F
      0600992C0500B1491D00AE230000E4C1B300F1E9E900E7BFB100AB360700A029
      00009E613E0000000000000000000000000000000000000000006E6C6C00E9EE
      F000312F2F002B2A2900E3E9ED00807F8000E2E2E200F5F4F400FAFBFD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B47B0900006AE200006AE200006AE200006A
      E200006AE2006C737600D8D8D800B2B2B200AFAFAF00AEAEAE00AEAEAE00AEAE
      AE00AEAEAE00AEAEAE00BEBEBE0084898A0096A8B000485C70007F91AE008397
      B40072839C005D6B7F00758EAA00396D880087A2C200547A960021546B006F88
      A30048657700D3DDE30000000000000000000000000000000000A59B93006A0D
      00007B251500D6CFCF00A8543900D8B4AE00AA5A4B00F0E1DD00C38F82006C08
      00007D311900000000000000000000000000000000000000000000000000AFCC
      DB006EAFD0005392B60083835F00FFFFFF00C2BEBD00BEBBBB00D5D4D500EDED
      EE00ECEBED00EBEBED00FFFFFF00B47B0800006AE20044C4FF0044C4FF0044C4
      FF00117BDF006C737600DADADA00B6B6B600B2B2B200AFAFAF00AEAEAE00AEAE
      AE00AEAEAE00AEAEAE00BEBEBE008D90920096A8B000435669006E7E98006372
      89006A7B940065758D007892B100376A85007C99BA007A96B500406680006982
      9D0045637500D7E1E50000000000000000000000000000000000998981006E26
      2000681D140098615A00DED7D900EBE2E100E6D8D500D3B7B600610E08004900
      0000631A0C00E0D8D60000000000000000000000000000000000D4D4D50088CC
      EE0080C1E4006FAFD2005B95B300FBFFFF00DBDBE300D6D9E700D5D7E600D3D6
      E500D2D5E300D1D4DF00FFFFFF00B47B0900006AE200000D1A00000000000000
      0000004FA8006C737600DBDBDB00B9B9B900B6B6B600B2B2B200AFAFAF00AEAE
      AE00AEAEAE00AEAEAE00BEBEBE009598990096A7AE00485A6B005E6B80004C59
      6C00798DAB00889EBF00819CBE00366882007090B0006989A7006F8AA8007589
      A500425F7100D7E1E500000000000000000000000000000000009A887F00914C
      44009448410099362C00B96B6500FBFFFF00F1DCDC009A2F26007F1A15006006
      06005E0F0500DFD1CF00000000000000000000000000000000003A5A7F00A8EF
      FF008FD2F1007EBFDF006CAFD3004C6B9C00FFD40800F8CE0E00F6CC0F00F6CC
      0E00F6CB0C00F5C90200FFFFFF00B47B0A005EA0EB0000428C00000000000000
      0000004FA8006C737600DEDEDE00BCBCBC00B9B9B900B6B6B600B2B2B200AFAF
      AF00AEAEAE00AEAEAE00BEBEBE009E9FA00094A5AD00556573008C9AAE008A9E
      BC0090A8CC0086A0C4005D83A100255B74006285A4007C95B6006884A1005872
      8C003D5A6B00D7E1E40000000000000000000000000000000000A18C8400B35A
      5000BB534A00C44C4300D3A8A600F7FAFB00F7F1F100D0797200BF453A00AB46
      3E0092332400E4D4D300000000000000000000000000000000000A3B6B006AAE
      D20074ADD10088C6E50075B4D70000165600FCD84100ECCD4200E9CA4200E9CA
      4200E8CA4100E8C93900FFFFFF00B47B0A00000000005EA0EB00006AE200006A
      E200006AE2006C737600E1E1E100C0C0C000BCBCBC00B9B9B900B6B6B600B2B2
      B200AFAFAF00AEAEAE00BEBEBE00A6A7A70093A5AD0059687500A4B0C2008FA4
      BC005A7E99002C5E7700255F79002F6B8700225F7900386883005D7C99006377
      93003C586900D6E0E40000000000000000000000000000000000B0999100D269
      5D00D3625700D3B4B300E2C0BF00EDCDCB00EEA5A000F9E9E900DE8A8300CC51
      4800BE5B48000000000000000000000000000000000000000000114574002D79
      A9004E8AB900517AA7002442710006295F00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B47B09000000000000000000000000000000
      000000000000AEAEAE00E4E4E400E2E2E200E0E0E000DFDFDF00DDDDDD00DBDB
      DB00DADADA00D8D8D800E0E0E000ADADAD0094A5AD003F5664004C6D81002351
      670024577000487695007195B900819FC500618DAF0034708C00215E7700325F
      780033566900D7E0E40000000000000000000000000000000000C5B3AF00E876
      6600E67A7300DDB0AF00F8695C00F2CAC800F0A59F00F9EAE900F4999300E75F
      5500D16C590000000000000000000000000000000000000000008197AE00277A
      AD003F89BA003B7AAA00123E70008897B100C7C2BF00BEBABA00E1E1E300FFFF
      FF00FEEFAC00F9D94400FFFFFF00B47B0A000000000000000000000000000000
      000000000000AEAEAE00AEAEAE00AEAEAE00AEAEAE00AEAEAE00AEAEAE00AEAE
      AE00AEAEAE00AEAEAE00AEAEAE00AEAEAE009CADB600113D5100174156004D72
      90007998BE0089A4CC008AA5CD008AA5CD008CA6CF00819FC700527E9E00225F
      78002E677D00D9E2E60000000000000000000000000000000000E5DDDC00E37C
      6900FF847A00FE7B6F00FD796B00F4D5D300F8FBFC00F69D9500FF6B5E00FF7D
      7000DE8676000000000000000000000000000000000000000000000000007193
      B0001B5E92001152890055513600FFFFFF00C0BDBC00BEBCBB00BCBABA00B9B7
      BD00F5E18000E3B70000FFFFFF00B47C0B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099ABB40046606F00374E
      6300596F8C00768DB100809AC1007D96BC006981A2004A657F0040627500678E
      9E00B0C9D200000000000000000000000000000000000000000000000000C98B
      7E00FFA29400FF988A00FEA68F00EDD5D100F5C2B300FE927D00FF8C8000FD91
      7700EBBDB8000000000000000000000000000000000000000000000000000000
      00000000000000000000C8840300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B67E0F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000ADBB
      C00059717E00324B5D003A51670036506300536F7D0094ACB500D5E1E5000000
      000000000000000000000000000000000000000000000000000000000000ECDF
      DE00DC856D00FFCFB600FEE4C200F3DEBE00FFE8BF00FFDABA00FFB79800E583
      7300000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D2AA5C00B77F0E00B57C0900B57B0800B57B0900B57B
      0900B47B0900B57C0A00B67E0F00CDA85F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B7C5CA007F949E00B9C7CD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DA9B8E00EBB39500F6D1AB00F3C8A300EFA38900EEADA4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E1E4E600D2D9DE0000000000000000000000
      000000000000000000000000000000000000BB8A5E00BB8A5E00BB8A5E00BB8A
      5E00BB8A5E00BB8A5E00BB8A5E00BB8A5E00BB8A5E00BB8A5E00F3FBF400C4B7
      9C00CBAF8F00BB8A5E000000000000000000000000000000000000000000B4D1
      E00086BBD6006AABCD005DA3C800559EC400559EC3005EA4C8006BABCC0087BA
      D500B6D2E1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DBE0E400A0A5A800B4B4B500BDBDBD00A4AAAF00A7B5C0000000
      000000000000000000000000000000000000BC8B5E00FFFFFE00FFFCF700FFFE
      F500FFFEF600FFFBF400FFF7EE00FFF3E800FFF7EC00FFF8EA00F3FBF4004468
      4600DEE9DF00CCBDA400000000000000000000000000A8CBDD000D7BB300007A
      B900007EBF00007FC000007FC000007FC000007FC000007FC000007FC000007F
      C000007CBC000F7EB700AFD2E40000000000759BAF004A8EB1004A8EB1004A8E
      B1004A8EB1004A8EB1004A8EB1004A8EB1004A8EB1004A8EB1004A8EB1004A8E
      B1004A8EB1004A8EB1004A8EB100759BAF0000000000C2C2C200BFBFBF00BFBF
      BF00B3BABE00A1A5A700CBCBCB00B7C6B9008AB08F00D8D8D800C8C8C9008F9F
      AC00BFBFBF00BFBFBF00C0C0C00000000000BE8C5E00FFFFFE00FDFBF500FEFD
      F400FFFEF600FFFBF400F7F7F100EAEEE700E7ECE500E7ECE400EFF7EF002650
      280035663A00C3D2C200E4ECE4000000000000000000B0D2E4000079B700007C
      BC000080C2000080C3000080C3000080C3000080C3000080C3000080C3000080
      C300007EBF00007CBD00B2D6E90000000000579DC10086CFF00082CBED0082CB
      ED0082CBED0082CBED0082CBED0082CBED0082CBED0082CBED0082CBED0082CB
      ED0082CBED0082CBED0086CFF000579DC100B1CADC0077B3DF0075B3DF0075B3
      DF007594AA00C5C5C500D4D4D40092B7970029823400DCDCDC00DBDBDB00A4AC
      B3006AA7D30076B3DF0076B3E000B3CDE000BF8E5F00FFFFFE0078432B00FCFE
      F2007B432B007B432B00F5F9F5004A6C4B00547B5700547C5700517A54003B7E
      44004EA85D0033773C00A6BBA800E8F0E9000000000000000000127EB700007D
      BD000081C5000081C5000081C5000081C5000081C5000081C5000081C5000081
      C500007FC0000F83C000000000000000000064ABCF0086CFEE007DC8E8007DC8
      E8007DC8E8007DC8E8007DC8E8007DC8E8007DC8E8007DC8E8007DC8E8007DC8
      E8007DC8E8007DC8E80086CFEE0064ABCF00A6C9E3006FC0FA006EC0FC006EC0
      FC008BA0AF00C9CEC90056A4600044984F002B88370052975B0086B18B00CECE
      CE0066ACDD006EC0FC006FC0FA00A8CDE800C18F5F00FFFDFA00F7F9F400FDFE
      FA00FFFFFC00FFFFFC00F5F9F500214B23006DBF770072C67E006CC3790067C8
      780059C36B0047AD5A00296F3300839D850000000000000000009FC7DD00057A
      B700007FC1000081C4000081C5000082C6000082C6000082C6000082C6000081
      C4000481C0009ACCE400000000000000000069B0D4008AD3F00082CCEB0082CC
      EB0082CCEB0082CCEB0082CCEB0082CCEB0082CCEB0082CCEB0082CCEB0082CC
      EB0082CCEB0082CCEB008AD3F00069B0D40086BAE10044ADFA0041ADFB0041AD
      FB008AA9BF00DEE0DE00AAC9AE009CC0A00084B08900B5CCB800CCDACE00E0E0
      E0003E9DE00041ADFB0044ADFA0087BDE400C3916000FFFFFC0078462D00FCFE
      F3007B432B007B432B00F5F9F500234D250082E08E0082E08E007ADA87006DD1
      7C005DC66F004CB760002E7D3B0099AF9A000000000000000000000000000000
      00007DB9D9001C88C100238EC6005BA0C800519AC4001488C4001A8CC8007ABC
      DE00000000000000000000000000000000006DB5D8008FD7F20087D0ED0087D0
      ED0087D0ED0087D0ED0087D0ED0087D0ED0087D0ED0087D0ED0087D0ED0087D0
      ED0087D0ED0087D0ED008FD7F2006DB5D80072B1DE0032A5F8002FA5FA002FA5
      FA005CA1D300E1E1E100EBEBEB00CCDBCE009ABE9F00F2F2F200F3F3F300ACC2
      D3002DA1F5002FA5FA0032A5F80072B2E100C4926000FFFFFC00F7F8EF00FDFE
      F300FFFFFF00FFFFFF00F5F9F500244E26004D8E53004C8E53004B8E520058AA
      63005FC26F0040914D009FB7A200EFF6EF000000000000000000000000000000
      00000000000000000000C4DBEC00B5C7D800ADC0D300BBD4E600000000000000
      00000000000000000000000000000000000071B7DB0094DBF4008DD5F0008DD5
      F0008DD5F0008DD5F0008DD5F0008DD5F0008DD5F0008DD5F0008DD5F0008DD5
      F0008DD5F0008DD5F00094DBF40071B7DB0065AADC002CA2F60029A2F80029A2
      F8002AA0F5009DBED700ECECEC00ECEFED00E6EDE700F5F5F500DEE5EA003F9D
      E30029A2F80029A2F8002CA2F60064AADD00C7936100FFFEF6007A432B00FFFF
      F5007B432B007B432B00F5F9F500DDE5DD00DAD5CC00D7D5CC00CACDC2003062
      33003F7F4600ABC3AE00D4E0D500000000000000000000000000000000000000
      00000000000000000000C8D7E500ADC8E100A9C5DE00C7D5E100000000000000
      00000000000000000000000000000000000075BBDF0099E0F60092DAF30092DA
      F30092DAF30092DAF30092DAF30092DAF30092DAF30092DAF30092DAF30092DA
      F30092DAF30092DAF30099E0F60075BBDF00519FD700279FF400239EF600239E
      F600239EF600259DF4006CB2E600B3D1E900C2D9EA0094C4E900359EEC00239E
      F600239EF600239EF600279FF400519ED600C8956100FFFFFB00FFFFF500FFFF
      F500FFFFF500FFFFF500FAFCF400FCFDF400FDFEF500FDFEF500F9FBF1003459
      3500B3C5B400CCCFBE0000000000000000000000000000000000000000000000
      00000000000000000000BAD5EF00C0DBF400C3DDF400BFD6EB00000000000000
      00000000000000000000000000000000000079BEE2009FE5F90098DFF60098DF
      F60098DFF60098DFF60098DFF60098DFF60098DFF60098DFF60098DFF60098DF
      F60098DFF60098DFF6009FE5F90079BEE2004399D500219BF2001D9AF4001D9A
      F4001D9AF4001D9AF4001D9AF4001D9AF4001D9AF4001D9AF4001D9AF4001D9A
      F4001D9AF4001D9AF400219BF2004396D200CA966200FFFFFC007B432B00F7F2
      E8007B432B007B432B007B432B00FFFFFC007B432B007B432B0075432A009DB1
      9D00CFDACD00BB8D620000000000000000000000000000000000000000000000
      000000000000DFECF700C3DDF400D1E5F700D3E6F700CBE1F500EEF4FA000000
      0000000000000000000000000000000000007CC1E400A3E9FB009DE3F9009DE3
      F9009DE3F9009DE3F9009DE3F9009DE3F900A3E9FA00A3E9FA00A3E9FA00A3E9
      FA00A3E9FA00A3E9FA00A6ECFB007CC1E400308CCF001A96EF001796F1001796
      F1001796F1001796F1001796F1001796F1001796F1001796F1001796F1001796
      F1001796F1001796F1001A96EF002E8ACB00CC986200FFFBF800FFFFFC00FFFF
      FC00FFFFFC00FFFFFC00FFFFFC00FFFFFC00FFFFFC00FFFFFC00FCFDF900FCF3
      E900FFF9F300BB8A5E0000000000000000000000000000000000000000000000
      000000000000B8D3EA00CEE2F500DAE9F700DCEBF800D7E8F700CDE0F0000000
      0000000000000000000000000000000000007FC4E600A8EDFD00A2E7FB00A2E7
      FB00A2E7FB00A2E7FB00A2E7FB00ABF0FD008CD0ED0081C5E70081C5E70081C5
      E70081C5E70081C5E70081C5E7007FC4E6002084CC001392ED001092EF001092
      EF001092EF001092EF001092EF001092EF001092EF001092EF001092EF001092
      EF001092EF001092EF001392ED002080C700CD996300FFFFFC00FFFFFC00FFFF
      FC00FFFFFC00FFFFFC00FFFFFC00FFFFFC00FFFFFC00FFFFFC00D5D5D200C4C7
      C100DDDBDA00BB8A5E0000000000000000000000000000000000000000000000
      00000000000088A2C400D0E3F500DBEAF700E1EDF800DAE9F7009BB1CE000000
      00000000000000000000000000000000000081C6E900AEF3FF00ABF0FE00ABF0
      FE00ABF0FE00ABF0FE00AEF3FF008FD3EF008FD3EF00ABF0FE00ABF0FE00ABF0
      FE00ABF0FE00ABF0FE00AEF3FF0081C6E90097A9E5004B74DC00396DE100396D
      E100396DE100396DE100396DE100396DE100396DE100396DE100396DE100396D
      E100396DE100396DE1004B74DC0097A9E300CF9A6300FFFBF800FFF9F300FFF9
      F3000000A1000000A1000000A1000000A10000009E00FFFFFF00DDDBDA00DDDB
      DA00F5F7F800BB8A5E0000000000000000000000000000000000000000000000
      0000000000005F79A100CCE1F400D4E3F200CDDCEE00C0D2E90062769D000000
      000000000000000000000000000000000000A3D5EF0083C8EB0083C8EB0083C8
      EB0083C8EB0083C8EB0083C8EB0083C8EB00FEFEFD00F8F8F300F0F0E600E9E9
      DB00FEC94100F4B62E0083C8EB00A3D5EF00000000008DC7F00076C6FE0076C6
      FE0076C6FE006FBDF5006AB7EE006AB7EE006AB7EE006AB7EE006AB7EE006AB7
      EE006AB7EE006AB7EE008DC2EA0000000000D09C6400FFFBF800FEF5EF00FFFF
      F500FFFFF500FFFFF500FFFFF500FFFFF500FFFFF500FFFFF500E9E5D500F9FC
      FD00E7D7CA00D2B59E0000000000000000000000000000000000000000000000
      0000000000005E6F9400556D96002E4778002B4883002C4A85008392B1000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D3EBF70085C9EC0085C9EC0085C9EC0085C9
      EC0085C9EC0085C9EC00D3EBF700000000000000000096C8EC0074BCEF0074BC
      EF007ABBEA00D9E7F00000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D29D6400FFFFFF00FFFBF800FFFB
      F800FFFFFE00FFFFFE00FFFFFE00FFFFFE00FFFFFE00FFFFFF00EEEEEE00EDE0
      D800C9A78C000000000000000000000000000000000000000000000000000000
      000000000000D8DBE400374E7B00284379002F4E8D004E689E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D4A06700D19C6400CF9A6300CC98
      6200CA966200C8946100C4926000C2906000BF8E5F00BD8C5E00C2976F00CCAB
      9100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B9C0D100BFC7D90000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6D6D6009C9C9C009C9C9C00D5D5D500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D5D5D500C6C6C600B6B6
      B60081869000043464002F373A002F373A0000000000B4B4B4008B8B8B008B8B
      8B008B8B8B008B8B8B008B8B8B008B8B8B008B8B8B008B8B8B008B8B8B008B8B
      8B008B8B8B00B4B4B4000000000000000000BCBCBC009090900099999900A1A1
      A100A9A9A900AFAFAF00AAAAAA00B8B8B800B8B8B800ABABAB00BABABA00B5B5
      B500ADADAD00A6A6A6009F9F9F00C0C0C0000000000000000000000000000000
      0000C7C7C700CFCFCF0097979700BEBEBE00BEBEBE0097979700CFCFCF00C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C2C6
      CE0004346400053F7A00DDE1C0002F373A00000000007E7E7E00D4D4D400D4D4
      D400D4D4D400D4D4D400D4D4D400D4D4D400D4D4D400D4D4D400D4D4D400D4D4
      D400D4D4D4007E7E7E000000000000000000E4E4E4009D9D9D00ABABAB00B3B3
      B300BDBDBD00C6C6C600CFCFCF00D9D9D900D9D9D900D3D3D300D2D2D200CACA
      CA00C4C4C400BBBBBB00ACACAC00E6E6E6000000000000000000C7C7C7008F8F
      8F009B9B9B0085858500B4B4B400D6D6D600D6D6D600B6B6B600858585009B9B
      9B008F8F8F00C7C7C70000000000000000000000000000000000000000000000
      0000B9B6B1008B877E007E796D007F796F008D897F00BFBDB700C5CDD500013E
      7B00053F7A001764A5009FD7F400135C9C000000000090909000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0090909000000000000000000000000000AEAEAE00B5B5B500C3C3
      C300BFBFBF00C6C6C600C6C6C600C5C5C500C5C5C500C6C6C600C6C6C600C1C1
      C100BEBEBE00B2B2B200B8B8B800000000000000000000000000A5A5A500DBDB
      DB00E6E6E600C2C2C200BEBEBE00CACACA00CACACA00BEBEBE00C2C2C200E6E6
      E600DBDBDB00A5A5A50000000000000000000000000000000000D1CFCC00706A
      5E007B6C5900A98B6D00C8A27B00CAA47D00AB8F70007F705C005A585200273D
      5C0004326100CAF3FD00135C9C00000000000000000090909000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0090909000000000000000000000000000CDCDCD00BABABA00C5C5
      C500C8C8C800C5C5C500C9C9C900C6C6C600C8C8C800C7C7C700C7C7C700CACA
      CA00C4C4C400C0C0C000D3D3D3000000000000000000000000009B9B9B00EBEB
      EB00C4C4C400D6D6D600E7E7E700D3D3D300D1D1D100E7E7E700D6D6D600C4C4
      C400EBEBEB009B9B9B00000000000000000000000000D0CDCB00635D5000C09F
      7C00FDDFAB00FEFADE00FFFFF300FFFFF200FEF2DF00FED4A100B9997600635D
      5100273D5C00135C9C00C9D0DB00000000000000000090909000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0090909000000000000000000000000000EDEDED00C8C8C800CECE
      CE00CECECE00D0D0D000D0D0D000D3D3D300D5D5D500D3D3D300D2D2D200D1D1
      D100CFCFCF00CDCDCD00EFEFEF000000000000000000DBDBDB0098989800C5C5
      C500DFDFDF00D5D5D50094949400BCBCBC00BDBDBD0094949400D5D5D500DFDF
      DF00C5C5C50098989800DBDBDB0000000000000000006C675B00C8AA8400FEEC
      B900FFF6C300FEEDB400FDE9AF00FEEBB200FEF1BD00FEF0C100FEE0AF00B191
      71005B595100C5CBD30000000000000000000000000090909000FFFFFF00BBBB
      BB00D7D7D700BBBBBB00BBBBBB00D7D7D700BBBBBB00D7D7D700BBBBBB00BBBB
      BB00FFFFFF00909090000000000000000000000000000000000074818E005F78
      8F006E859A007A8FA3008398A900879AAA00889CAC00889EAE008199AB007591
      A50065849A007F8F9A0000000000000000000000000096969600B6B6B600C7C7
      C700F0F0F00094949400E7E7E7000000000000000000E7E7E70094949400F0F0
      F000C7C7C700B6B6B6009696960000000000ADA9A20091846B00FDDEAD00FCDD
      AD00FCD69F00FCD68E00FCD99200FCD88F00FBD7A200FCD8A600FCD8A600FDD8
      A900806F5B00B5B3AE0000000000000000000000000090909000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0090909000000000000000000000000000000000006F87A3003864
      9A00446DA400547BAE006388B6006C92BC006D95BE006493BD00558DBA003E82
      B4002775AC006F96B2000000000000000000C2C2C200CCCCCC00F7F7F700E9E9
      E900D8D8D800AFAFAF0000000000000000000000000000000000AFAFAF00D8D8
      D800E9E9E900F7F7F700CCCCCC00C2C2C200807C7000C2A17D00FBCB9A00F9BD
      8900F9C19000FACC9600F9CE9100F8CC9000F8C59300F8C39100FAC49100FDC8
      9600C599770089837A0000000000000000000000000090909000FFFFFF00BBBB
      BB00BBBBBB00D7D7D700BBBBBB00D7D7D700BBBBBB00BBBBBB00D7D7D700BBBB
      BB00FFFFFF0090909000000000000000000000000000000000007387A4002855
      97003666A9004676B5005081BB005587BF005588C0004D85BF004280BD003479
      BA00226EAE006F95B3000000000000000000C8C8C800CFCFCF00FCFCFC00F1F1
      F100DBDBDB00A2A2A20000000000000000000000000000000000A2A2A200DBDB
      DB00F1F1F100FCFCFC00CFCFCF00C8C8C8006A645600D8B08900F9BA8800F4B6
      8300F1B27E00EFB38200F0B58300EEB28000EEB07E00F0B28000F6BC8900F9B9
      8700DEAD8500706A5E0000000000000000000000000090909000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0090909000000000000000000000000000000000007689AA003364
      B200356FC500417ECF00468AD2004691D5004593D700428ED6003685D4002C7D
      CC00246CB1007293B20000000000000000000000000097979700C0C0C000CFCF
      CF00FFFFFF0096969600ADADAD000000000000000000ADADAD0096969600FFFF
      FF00CFCFCF00C0C0C0009797970000000000635D5000D6AA8400F8B98500EFAE
      7B00ECAA7800ECAA7800ECAA7800ECAA7800ECAA7800ECAA7800EEB07D00F5B5
      8200DAA67E006E695D0000000000000000000000000090909000FFFFFF00BBBB
      BB00BBBBBB00D7D7D700BBBBBB00BBBBBB00D7D7D700BBBBBB00D7D7D700BBBB
      BB00FFFFFF0090909000000000000000000000000000000000007389B0004071
      C6003D7CE4004893EF0053AAED005AB7EA005EBBE90062B6E8004096E4003085
      DF002974C3007192B70000000000000000000000000000000000A8A8A800D3D3
      D300FFFFFF00E7E7E70097979700868686008686860097979700E7E7E700FFFF
      FF00D3D3D300A8A8A800000000000000000077726700BC987800F5B58300F3B4
      8100EEAC7A00ECAA7800ECAA7800ECAA7800ECAA7800EDAB7800F0B07D00F0AF
      7F00BE987600807C710000000000000000000000000090909000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009090900000000000000000000000000000000000718AB3004172
      C700498AEA0053A5F0005CBCEE0060C9EB0065CDEA006DC7E9004FA4E2004189
      DD003274C7007392BA00000000000000000000000000000000009E9E9E00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00E9E9E900E9E9E900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00A0A0A0000000000000000000A3A098008E7F6800E9A87B00F9BC
      8800F6B88400F2B37F00F1B07D00F1B17E00F3B38000F4B68200F4B78400E09E
      75008F7E6600ADA9A20000000000000000000000000090909000FFFFFF00DBDB
      DB00EAEAEA00DBDBDB00EAEAEA00DBDBDB00EAEAEA00DBDBDB00EAEAEA00DBDB
      DB00FFFFFF009090900000000000000000000000000000000000738EBA004478
      D100599BEF005FB5F00068CCED006ADBEC006DDCEB0075D1E9005BADE100508F
      DC00417AD4007794C10000000000000000000000000000000000B3B3B300F3F3
      F300FFFFFF00D8D8D800D8D8D800FFFFFF00FFFFFF00D8D8D800D8D8D800FFFF
      FF00F3F3F300B3B3B3000000000000000000E7E6E400716D5C00AC846600EAAA
      7C00FBCF9E00FFFCF100FFFFFF00FFFFFF00FFFBEF00F8C39100DD9F7400A27D
      6100756F6000000000000000000000000000000000007E7E7E00FFFFFF007A7A
      7A00BCBCBC007A7A7A00B8B8B8007A7A7A00B8B8B8007A7A7A00B8B8B8007A7A
      7A00FFFFFF007E7E7E00000000000000000000000000000000007792C000477A
      D2005F9BE30068B6E90073CDE70077DBE7007ADCE6007ACFE30062ADDC00568F
      D600467AD1007B97C50000000000000000000000000000000000E5E5E500B8B8
      B8009E9E9E00BBBBBB00C4C4C400FFFFFF00FFFFFF00C4C4C400BBBBBB009E9E
      9E00B8B8B800E5E5E500000000000000000000000000B9B6B100665F5100AD84
      6600E7AC8100F8E0C700FFFDFB00FEFCFB00F6DCC100D8A07800A77F6200675F
      5200C6C5BF0000000000000000000000000000000000B7B7B7007A7A7A00C7C7
      C7007A7A7A00C7C7C7007A7A7A00C7C7C7007A7A7A00C7C7C7007A7A7A00C7C7
      C7007A7A7A00B7B7B70000000000000000000000000000000000D2D2D200DBDB
      DB00E4E4E400EAEAEA00EDEDED00EFEFEF00F0F0F000EFEFEF00EEEEEE00EAEA
      EA00E2E2E200D9D9D90000000000000000000000000000000000000000000000
      0000000000000000000097979700CFCFCF00CFCFCF0097979700000000000000
      0000000000000000000000000000000000000000000000000000C6C4BF006A66
      59007E6B5800AC836500C5906D00C38E6B00A98063007D6A57006E695D00CCCA
      C5000000000000000000000000000000000000000000DADADA007A7A7A00FFFF
      FF007A7A7A00FFFFFF007A7A7A00FFFFFF007A7A7A00FFFFFF007A7A7A00FFFF
      FF007A7A7A00DADADA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D1D1D100D1D1D10000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009E9A92006E695D00635D5000635D5000706A5E00A29F9600000000000000
      0000000000000000000000000000000000000000000000000000D1D1D1008989
      8900A9A9A90089898900A9A9A9008A8A8A00A9A9A90089898900A9A9A9008989
      8900D1D1D1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E1E1E1000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009F9F9F0000000000000000000000000000000000000000000000
      0000BFBFBF000000000000000000000000000000000089898900858585008181
      8100808080007D7D7D007E7E7E007E7E7E007E7D7D007E7E7E00828181000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D2D2D200BAB9B700ABA9A400908F8D008A898400B1B0
      AF00000000000000000000000000000000000000000000000000000000000000
      000000000000DFDFDF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000040404000000000000000000000000000000000000000000BFBF
      BF0004040400000000000000000000000000AFAEAD00D5D4D3008F8F8E00B0B0
      B0008C8C8C00B5B5B50095949400BCBCBC008E8D8D00AFB0AE00BDBBBC008F8F
      8E00CECECE00D5D6D600C3C3C300E5E5E5000000000000000000ECECEC00B8B8
      B700989898007D7D7D0071717100817F7B00A3A097009A99960093928D00A7A7
      9F00C8C8C6000000000000000000000000000000000000000000000000000000
      00009A9B9A00B5B4B4008F8F8F0092929100B1B1B100DDDDDD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000929292001515150000000000000000000000000000000000000000000000
      000008080800D1D1D1000000000000000000C6C6C600DBDBDA00C5C7C5009696
      9600C0BFC0009B9D9B00C5C5C50099999900B9B9B900C4C5C600D7D7D600B1AF
      B000999A9A008C8E900097989800929292000000000000000000E5E5E400B0AF
      AD0090908F008A8A89008F8F8F008C8B8B008A8A8900878684008D8C8900A1A2
      9D00C7C7C6000000000000000000000000000000000000000000000000009999
      9900C5C4C400C8C8C900C9C9CA00CACACA00C2C2C200B0AFAF00878887008686
      8600ADADAD00D5D5D5000000000000000000000000000000000000000000A3A3
      A3000D0D0D00A3A3A30000000000000000000000000000000000000000000000
      000048484800676767000000000000000000E7E7E600BCBBBB00CCCCCB00F7F7
      F600D5D5D500C7C7C700C7C7C700D5D5D500FBFBFC00CACACA00BDBDBD00D6D6
      D600CDCECE00A89B9900CDCECE00ACACAC00D2D1CF00A9A8A6009F9D9A00ABA9
      A600B2B1AF00D1D1D000B7B7B6009E9E9C0072727100525252007C7C7C00B4B3
      B100C1BDB700C3C1BE00000000000000000000000000000000009A9A9A00A2A2
      A3009999990095969500A3A3A300B3B4B300B3B3B300B1B1B100BDBDBD00D7D8
      D700DDDEDE00B8B9B80095959500979797000000000000000000878787001111
      1100A4A4A4000000000000000000000000000000000000000000000000000000
      0000C2C2C2001111110000000000000000000000000000000000000000009999
      9A004748480000000000000000004E4E4F00A7A7A7000000000000000000D8D9
      D800CCCFD000BCA79200C3C5C700A9A9A900C3C1BE00ADA8A300B0ACA700A5A2
      9E004948470080818200C5C5C500BCB7B20090837400816B5100C3A57B00D5AE
      7400DBAE6700C3B29700000000000000000000000000ACACAC00A9A8AA009598
      9B0084878A00818285007B7C7E007A7B7C00838483008E8F8E008C8D8D00B4B4
      B400E2E2E200E7E7E700E5E5E400EBEBEB00000000000000000016161600C3C3
      C300000000000000000000000000000000000000000000000000000000000000
      0000000000004E4E4E006C6C6C0000000000ACAEAE008E9193007E8385007074
      78006165690052565B0050565A005F6567006B6F7400777B7E007C7E7E007677
      7700D4D5D5009C939400CFD0D000A9A9A900CCCAC800B8AFA400B7B1A900B8B6
      B200666563006C513B00C58C5900CE894400D4873200DD903400E29C4000E5A5
      4800E7AC4F00D0B58B00000000000000000000000000949492008F7A5D008D7B
      62009688760091887D00918D8700909191008B8E900085898D007A7D80009B9C
      9C00CBCBCC00DEDEDE00000000000000000000000000D3D3D3001A1A1A00D3D3
      D300000000000000000000000000000000000000000000000000000000000000
      000000000000B7B7B7002727270000000000B1B3B500FDFCFB00F9F2EB00FCF6
      EF00FCF6F000FDF7F000FDF7F000FBF6EF00FAF4EE00F8F1EC00FCFBFA009EA0
      A200D4D5D60090919200DBDBDB00AAAAAA00D3D1CF0097959200A7A6A200C5C3
      BF00A8A8A600A2724C00D3722000D3762000D57B2000D8812100DA892500DE92
      2D00E29B3400D1A96A00EEEEEE000000000000000000B2B0AE00745836007C60
      3D008166420084684300755934006F573600745F440078685300877B6B00BDBB
      B800F5F5F5000000000000000000000000000000000000000000717171001F1F
      1F003A3A3A00D5D5D50000000000000000000000000000000000000000000000
      000000000000000000001F1F1F0000000000ADAEB000E5DAD200904F1D007B35
      0000823D0300823E04008845060086430500793500005D1B0000DBD1CC00A3A7
      AB00DDDEDE0091919100E1E1E100ABABAB00DBDBD800C9C7C400CAC8C500CBCA
      C700CFD0CD00C6AC9800D27A3600D1732300D3772100D57C2000D8812000DA87
      2000DD8C1F00D49E5000E8E6E3000000000000000000A8A49E00614623008269
      4900755E3D008A705000917856007E654200563B1900694E2A00AE916800D8D4
      CD00000000000000000000000000000000000000000000000000000000007474
      74002F2F2F000000000000000000000000000000000000000000000000000000
      00009F9F9F00000000002323230000000000ADAEAF00EAE2DC00EEE0D3007D3E
      0600894E1800864E1B008A4F1B0091521D0088481900692B0800DDD4CF00A4A8
      AC00E1E2E30093939300E6E6E600A7A7A700C7C6C50098979600878B8600BCBE
      B900DBDBD900D7CAC000DA9A6C00D7854500D6803700D7813000D8852D00DB8C
      3100DF953800D9A05400E3DDD5000000000000000000B0AAA200B69B7600BDA4
      8300B59C7C00A1886800AB927200A9906F00C1A88700D7BF9E00D0B49100D9D6
      D200000000000000000000000000000000000000000000000000000000003333
      33009F9F9F0000000000ADADAD00000000000000000079797900000000000000
      000028282800929292002828280000000000ABACAE00EAE0D600B7926A00975C
      1100985E1D008E571D008B521D00945821008F511F0071320B00DDD4CF00ABB0
      B500EEF0F2009E9E9E00F3F3F300AFAFAF009E9E9E005150500052585200B2B6
      B100E0E0DE00CFCAC400DAA78400DEA17400D9986400D4945D00D59B6400D7A5
      7000D9B07F00DBBE9800E8E5DF000000000000000000B6ADA000CFB59100D7BE
      9D00E4CBAA00D8BF9E00DBC2A100E4CBAA00E2C9A900D9BE9E00D3B89500E0DE
      DC00000000000000000000000000000000000000000000000000000000002C2C
      2C00CACACA00949494002C2C2C0000000000000000002C2C2C00CACACA00BCBC
      BC002C2C2C002C2C2C009393930000000000A3A5A600F0E9DD00D9CBAE00C094
      2300BD8F3100AA7A2C00956222008E541A00874A190076360C00E0D6D000AFB6
      BA00F4F6F600A5A5A500FEFEFE00B0B0B000A2A2A100575656005C625C00B1B6
      B000A7A7A6005A59580089796F00D2C2B600E9DFD600ECE5DF00F0ECE700F4F2
      F0000000000000000000000000000000000000000000B8AB9900C9AF8B00D4BA
      9A00DCC3A200D2B89800CFB69500D4BC9B00DCC3A300DAC29F00D8BF9E00E5E5
      E400000000000000000000000000000000000000000000000000000000007E7E
      7E0031313100313131004949490031313100313131003C3C3C00313131003131
      3100898989000000000000000000000000009F9FA200F0EADE00C0A66D00CBA5
      4300C89D4400BB8C4000AD7D3B00A5713500995C2300813C0500E1D6CE00ADB3
      B800C2C2C30081818100C4C4C400B5B5B500C8C8C6009494920091928F009797
      9500535352002D2D2D0037383800D0D0D0000000000000000000000000000000
      000000000000000000000000000000000000E7E7E800C2B29D00D1B79400DEC5
      A100DDC29D00D1B79100D2B79400D1B69200D5BA9800DBC19D00DAC2A300ECEC
      EC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000096959500F2EEE400F8F5EF00F4EE
      DF00F2EADE00F0E8DC00F0E7DC00EFE7DC00EFE6DB00EBE0D600E2D8D000A5AA
      AC000000000000000000000000000000000000000000B0B0B0003E3E3E002020
      20001F1F1F002E2E2E0037373700D0D0D0000000000000000000000000000000
      00000000000000000000000000000000000000000000CACAC900CAC8C600C9C4
      C000C9C4BC00C8BDB200CBBDAB00D4C2AA00D6C2A700DAC2A200DAC5A900F0F0
      F000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009B9B9C00A29F9A00A8A69E00ABAB
      AA00AEAEAE00B0AFB000B3B4B300B6B6B600B9B8B800BEBEBE00B0AFAD00AFB1
      B100000000000000000000000000000000000000000000000000C4C4C4005D5D
      5D00363636004343430060606000DCDCDC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EBECEC00E0E0E000D6D6D600D0CFCF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CACACA00DDDDDD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000900000000100010000000000800400000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000E003FFFF00000000E147FFFF00000000
      8000F00700000000E000E003000000000000E003000000000000E00300000000
      C000E00300000000C000E00300000000E000E003000000000007E00300000000
      4007E003000000004017E00300000000003FE00700000000003FFFFF00000000
      013FFFFF00000000013FFFFF00000000F00FFFFFFFFF8001FF0FC7E3F00F0000
      DE0F83C1E0070000CE1F0180E0070000C43F0180F8030000C03F0180F8030000
      C07F8181F0070000C0078181F00F0000C00F8001E03F0000C01F8001E07F0000
      C03FC001E0FF0000C0FFC003E1FFC003C1FFE007C1FFC003C3FFE00787FFC003
      C7FFF00F87FFE007DFFFFFFFCFFFE007FC3F0FF00007F80FF81F01800007F80F
      F00F87E10003F007F00F80010003F007F00F80010001E003F00FC0030001C003
      E007C0030000C003C003C1870000C003C003E1870000F803C003E1070003F807
      E007F00F0001F81FF00FF00F00C1F87FF81FF01F81C0F8FFFC3FF81FF803F8FF
      FE7FF81FFC03F8FFFFFFF83FFF07FDFFFFFFFFFFFFF8FFFF8FFFFFFFFFF0FE7F
      8803F00FFFE0FC3F8FFFE007E001F81FFFFFC003C003F00FFFFFC0038007E007
      8FFFC0030007C0038803C0030007C0038FFFC0030007C003FFFFC0030007F00F
      FFFFC0030007F00F8FFFC0030007F00F8803E0070007F00F8FFFE007800FF81F
      FFFFFFFFC01FF81FFFFFFFFFE03FFFFF003FFFFFFEFFF81F003FFFFFF83FE007
      003F0080E00FF00F00008FE38007E007000000000003E007000000000003C007
      C00000000003C007E00000000003C003C00000000003C003C00000000003C003
      C00080000003C007C000F8000003C007C000F8000003C007E000FFFF8007E007
      FC00FFFFE01FE00FFC00FFFFF8FFF81FFFFFFE7F0003E007FFFFF81F00038001
      0000800100018001000000000000C003000000000000C003000000000000F00F
      000000000000FC3F000000000001FC3F000000000003FC3F000000000003F81F
      000000000003F81F000000000003F81F000000000003F81F000080010003F81F
      FE0183FF0007F83FFFFFFFFF000FFE7FFFFFFC3FFF8080030000F00FFFE08003
      0000C003F00080038001C003C00180038001C003800180038001800180038003
      C003818100038003C00303C000038003C00303C000038003C003818100038003
      C003C00300038003C003C00300038003C003C00300078003C003C00380078003
      C003FC3FC00F8003FFFFFE7FF03FC007FFFFFFFFFFDFFFFFFBF7801FFC0FFBFF
      FBE70000C007F03FF3F30000C007E003E3F300000003C000C7F3E06000038000
      CFF90000000380038FF9000000018007C3FD00000001800FE7F500000001800F
      E5B100000001800FE1810000000F800FE007000000FF000FFFFF000F80FF800F
      FFFF000FC0FFFE1FFFFFFFFFF3FFFFFF00000000000000000000000000000000
      000000000000}
  end
end
