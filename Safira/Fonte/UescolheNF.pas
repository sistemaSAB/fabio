unit UescolheNF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Mask, StdCtrls, ExtCtrls, Buttons, Grids, Upesquisa,UobjNotaFiscalObjetos,UobjPedidoObjetos,UDataModulo,IBQuery,UessencialGlobal;

type
  TFescolheNF = class(TForm)
    lb1: TLabel;
    btBtsair: TBitBtn;
    pnl1: TPanel;
    lb2: TLabel;
    lbNomeCfop: TLabel;
    lb3: TLabel;
    edtnotafiscalatual: TEdit;
    edtEdtCfop: TEdit;
    bt1: TBitBtn;
    StrGridGrid: TStringGrid;
    btok: TBitBtn;
    lb4: TLabel;
    lbNumeropedido: TLabel;
    edtDadosAdicionais: TEdit;
    lb5: TLabel;
    procedure edtnotafiscalatualKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtnotafiscalatualKeyPress(Sender: TObject; var Key: Char);
    procedure edtEdtCfopKeyPress(Sender: TObject; var Key: Char);
    procedure edtEdtCfopKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btBtsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure bt1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure StrGridGridDblClick(Sender: TObject);
    procedure btokClick(Sender: TObject);
  private
    NumPedido:string;
    NumeroNota:string;
    procedure SelecionaProximaNota();
  public
    procedure PegaNumPedido(parametro:string);
    constructor create;

  end;

var
  FescolheNF: TFescolheNF;



implementation

{$R *.dfm}

constructor TFescolheNF.create;
begin
    edtnotafiscalatual.Text:='';
    edtEdtCfop.Text:='';

end;

procedure TFescolheNF.edtnotafiscalatualKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FPesquisaLocal:Tfpesquisa;
Begin

    If (key <> vk_f9)
    then exit;

    Try
        FPesquisaLocal:=Tfpesquisa.create(Self);
        If (FpesquisaLocal.PreparaPesquisa('select * from tabnotafiscal where situacao=''N'' and numpedido is null and modelo_nf=''1'' ', '',nil)=True)
        Then
        Begin
                If (FpesquisaLocal.showmodal=mrok)
                Then
                begin
                      edtnotafiscalatual.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      NumeroNota:=FpesquisaLocal.QueryPesq.fieldbyname('numero').asstring;
                end;
        End;
    Finally
           FreeandNil(FPesquisaLocal);

    End;

end;


procedure TFescolheNF.edtnotafiscalatualKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then Key:=#0;
end;


procedure TFescolheNF.edtEdtCfopKeyPress(Sender: TObject; var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then Key:=#0;
end;

procedure TFescolheNF.edtEdtCfopKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var
   FPesquisaLocal:Tfpesquisa;
begin
    If (key <> vk_f9)
    then exit;

    Try
        FPesquisaLocal:=Tfpesquisa.create(Self);

        If (FpesquisaLocal.PreparaPesquisa('Select * from TabOPERACAONF', '',nil)=True)
        Then Begin
                If (FpesquisaLocal.showmodal=mrok)
                Then edtEdtCfop.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
        End;
    Finally
           FreeandNil(FPesquisaLocal);

    End;
end;


procedure TFescolheNF.btBtsairClick(Sender: TObject);
var
  ObjNotaFiscal:TObjNotafiscalObjetos;
  ObjPedidoObjetos:TObjPedidoObjetos;
begin
    edtnotafiscalatual.Text:= StrGridGrid.Cells[1,1];

    self.Close;
end;

procedure TFescolheNF.PegaNumPedido(parametro:string);
begin
    self.NumPedido:=parametro;
    lbNumeropedido.Caption:=parametro;
end;

procedure TFescolheNF.FormKeyPress(Sender: TObject; var Key: Char);
begin
       if key=#13
       Then Perform(Wm_NextDlgCtl,0,0);

       if (Key=#27)
       Then Self.Close;
end;

procedure TFescolheNF.bt1Click(Sender: TObject);
begin
      self.edtEdtCfop.text:='';
      Self.close;
end;

procedure TFescolheNF.SelecionaProximaNota;
var
  QueryPesq:TIBQuery;
  Query:TIBQuery;
  Quantidade:Integer;
  quantidadeprodutos:Integer;
  quantidadenota:Integer;
  quantidaderestante:integer;
  Contador:Integer;
  GerarPorProjetos:string;
begin
      try
        query:=TIBQuery.Create(nil);
        query.Database:=FDataModulo.IBDatabase;
        QueryPesq:=TIBQuery.Create(nil);
        QueryPesq.Database:=FDataModulo.IBDatabase;
      except

      end;
      if(ObjParametroGlobal.ValidaParametro('VENDER PROJETOS COMO PRODUTOS')=false) then
      begin
              MensagemErro('Paramentro "VENDER PROJETOS COMO PRODUTOS" n�o encontrado');
              GerarPorProjetos:='NAO';
      end
      else GerarPorProjetos:=ObjParametroGlobal.Get_Valor;

      try
         quantidadenota:=0;
         quantidadeprodutos:=0;
          with QueryPesq do
          begin
                if(GerarPorProjetos='NAO')then
                begin
                      close;
                      SQL.Clear;
                      SQL.Add('select *  from tabmateriaisvenda where pedido='+NumPedido);
                      //se a vidra�aria gerar por produtos ent�o tenho q desconsiderar os projetos
                      SQL.Add('and projeto is null') ;
                      Open;
                      while not Eof do
                      begin
                          Inc(quantidadeprodutos,1);
                          Next;
                      end;
                end
                else
                begin
                      close;
                      SQL.Clear;
                      SQL.Add('select *  from tabmateriaisvenda where pedido='+NumPedido);
                      //se a vidra�aria gerar por projetos, desconsidero os projetos e pego somente os materiais
                      SQL.Add('and projeto is  not null') ;
                      Open;
                      while not Eof do
                      begin
                          Inc(quantidadeprodutos,1);
                          Next;
                      end;
                end;
          end;
          with QueryPesq do
          begin
               Close;
               sql.Clear;
               SQL.Add('select QUANTIDADEPRODUTOS from tabmodelonf where codigo_fiscal=''1''');
               Open;
               quantidade:=fieldbyname('quantidadeprodutos').AsInteger;
               if(quantidadeprodutos<quantidade)
               then
               begin
                   quantidadenota:=1;
               end
               else
               begin
                   quantidaderestante:=quantidadeprodutos;
                   while quantidaderestante>0 do
                   begin
                        if(quantidaderestante>quantidade)
                        then
                        begin
                              quantidaderestante:=quantidaderestante-quantidade;
                              quantidadenota:=quantidadenota+1;
                        end
                        else
                        begin
                             quantidadenota:=quantidadenota+1;
                             quantidaderestante:=0;
                        end;

                   end;
               end;
          end;

          with StrGridGrid do
          begin
                  RowCount:= 1;
                  ColCount:= 3;
                  ColWidths[0] := 35;
                  ColWidths[1] := 100;
                  ColWidths[2] := 250;
                  Cells[0,0] := ' ';
                  Cells[1,0] := 'CODIGO';
                  Cells[2,0] := 'NUMERO';
                  contador:=0;

                  with Query do
                  begin
                              Close;
                              sql.Clear;
                              sql.Add('select * from tabnotafiscal where situacao=''N'' and numpedido is null and modelo_nf=''1''');
                              SQL.Add('order by codigo');
                              Open;
                              while not Eof do
                              begin
                                      RowCount:=RowCount+1;
                                      Cells[0,contador+1]:=' ';
                                      Cells[1,contador+1]:=fieldbyname('codigo').AsString;
                                      Cells[2,contador+1]:=fieldbyname('numero').AsString;
                                      inc(Contador,1);
                                      if(Contador=quantidadenota)
                                      then Exit;
                                      Next;
                              end;
                  end;

          end;


      finally
           FreeAndNil(Query);
      end;

end;

procedure TFescolheNF.FormShow(Sender: TObject);
begin
      SelecionaProximaNota;
end;

procedure TFescolheNF.StrGridGridDblClick(Sender: TObject);
begin
    if(StrGridGrid.Cells[0,StrGridGrid.Row] = 'X')
    then
    begin
       StrGridGrid.Cells[0,StrGridGrid.Row] := ' ';
    end
    else
    begin
       StrGridGrid.Cells[0,StrGridGrid.Row] := 'X';
    end;
end;

procedure TFescolheNF.btokClick(Sender: TObject);
var
    i:Integer;
begin
     if(edtnotafiscalatual.Text) = ''
     then begin
       Exit;
     end;
     for i:=0 to StrGridGrid.RowCount do
     begin
          if(StrGridGrid.Cells[0,i]= 'X')
          then
          begin
              StrGridGrid.Cells[0,i]:='';
              StrGridGrid.Cells[1,i]:=edtnotafiscalatual.Text;
              StrGridGrid.Cells[2,i]:=NumeroNota;
              Exit;
          end;
     end;
end;

end.
