unit UVisualizaStatusProducao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, jpeg,Grids, Buttons, ImgList,IBQuery,
  ComCtrls;

type
  TFVisualizaStatusProducao = class(TForm)
    panelpnl1: TPanel;
    lbnomeformulario: TLabel;
    bt3: TSpeedButton;
    img1: TImage;
    btConfiguracoes: TSpeedButton;
    il1: TImageList;
    SCBXFundo: TScrollBox;
    panelpnl2: TPanel;
    img2: TImage;
    lbdiasemanarodape: TLabel;
    lbdatarodape: TLabel;
    lb6: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btConfiguracoesClick(Sender: TObject);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure bt3Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    _Panel:TPanel;
    _Imagem:TImage;
    _STRGStatusServico:TStringGrid;
    _LabelNomeProjeto:TLabel;
    _LabelPedido:TLabel;
    _LabelNomeCliente:TLabel;

    _QuantidadePanels:Integer;

    procedure __LimpaItens;
    procedure __CriaPanelsRegistros(Sender: TObject);
    procedure __STRGLinhaServicosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    function  __MostraServicos(PedidoProjeto:string):Boolean;
    procedure __MouseLeaveinLabel(Sender: TObject);
    procedure __MouseMoveinLabel(Sender: TObject; Shift: TShiftState;X, Y: Integer);
    procedure __OnClickLabel(Sender: TObject);
  public
    { Public declarations }
  end;

var
  FVisualizaStatusProducao: TFVisualizaStatusProducao;

implementation

uses UDataModulo, UescolheImagemBotao, UAjuda, UpesquisaMenu, Uprincipal;

{$R *.dfm}

procedure TFVisualizaStatusProducao.__CriaPanelsRegistros(Sender: TObject);
var
  qryLocalizaPP_QUERY:TIBQuery;
begin
  try
    qryLocalizaPP_QUERY:=TIBQuery.Create(nil);
    qryLocalizaPP_QUERY.Database:=FDataModulo.IBDatabase;
  except
    Exit;
  end;
  try
    //Criando Panel dinamicamente
    qryLocalizaPP_QUERY.Close;
    qryLocalizaPP_QUERY.SQL.Clear;
    qryLocalizaPP_QUERY.SQL.text:=
    'SELECT distinct(pp.codigo) as pedidoprojeto, P.CODIGO as pedido, proj.descricao,C.NOME,proj.referencia '+
    'FROM TABPEDIDOPROJ_ORDEMPRODUCAO PPO '+
    'JOIN TABPEDIDO_PROJ PP ON PP.CODIGO=PPO.PEDIDOPROJETO '+
    'JOIN TABPEDIDO P ON P.CODIGO=PP.PEDIDO '+
    'JOIN TABCLIENTE C ON C.CODIGO=P.CLIENTE '+
    'JOIN TABPROJETO Proj ON Proj.CODIGO=PP.PROJETO '+
    'ORDER BY P.CODIGO ';
    qryLocalizaPP_QUERY.Open;

    while not qryLocalizaPP_QUERY.Eof do
    begin
      _Panel := TPanel.Create(Self);
      _Panel.Parent := SCBXFundo;
      _Panel.Align:=alTop;
      _panel.Top:=panelpnl1.Top+(150*_QuantidadePanels);
      _Panel.Height:=300;
      _Panel.Visible:=true;
      _Panel.Enabled:=true;
      _Panel.Color:=clWhite;
      _Panel.BorderStyle:=bsNone;
      _Panel.BorderWidth:=0;
      _Panel.BevelInner:=bvNone;
      _Panel.BevelOuter:=bvNone;
      _Panel.Tag:=_QuantidadePanels;
      _QuantidadePanels:=_QuantidadePanels+1;

      _LabelNomeProjeto:=TLabel.Create(self);
      _LabelNomeProjeto.Parent:=_Panel;
      _LabelNomeProjeto.Left:=200;
      _LabelNomeProjeto.Font.Color:=$00A2663E;
      _LabelNomeProjeto.Font.Name:='Arial';
      _LabelNomeProjeto.Font.Size:= 14;
      _LabelNomeProjeto.Tag:=_QuantidadePanels;
      _LabelNomeProjeto.Font.Style:=[fsBold];
      _LabelNomeProjeto.Cursor:=crHandPoint;
      _LabelNomeProjeto.OnMouseMove:=__MouseMoveinLabel;
      _LabelNomeProjeto.OnMouseLeave:=__MouseLeaveinLabel;
      _LabelNomeProjeto.Enabled:=True;
      _LabelNomeProjeto.OnClick:=__OnClickLabel;
      _LabelNomeProjeto.Caption:=qryLocalizaPP_QUERY.Fields[2].AsString;
      _QuantidadePanels:=_QuantidadePanels+1;

      _LabelNomeCliente:=TLabel.Create(self);
      _LabelNomeCliente.Parent:=_Panel;
      _LabelNomeCliente.Left:=200;
      _LabelNomeCliente.Top:=20;
      _LabelNomeCliente.Font.Color:=clBlack;
      _LabelNomeCliente.Font.Name:='Arial';
      _LabelNomeCliente.Font.Size:= 11;
      _LabelNomeCliente.Tag:=_QuantidadePanels;
      _LabelNomeCliente.Font.Style:=[fsBold];
      _LabelNomeCliente.Cursor:=crHandPoint;
      _LabelNomeCliente.OnClick:=__OnClickLabel;
      _LabelNomeCliente.Caption:='CLIENTE :'+qryLocalizaPP_QUERY.Fields[3].AsString;
      _QuantidadePanels:=_QuantidadePanels+1;

      _LabelPedido:=TLabel.Create(Self);
      _LabelPedido.Parent:=_Panel;
      _LabelPedido.Left:=200;
      _LabelPedido.Top:=40;
      _LabelPedido.Font.Color:=clBlack;
      _LabelPedido.Font.Name:='Arial';
      _LabelPedido.Font.Size:= 11;
      _LabelPedido.Tag:=_QuantidadePanels;
      _LabelPedido.Font.Style:=[fsBold];
      _LabelPedido.Cursor:=crHandPoint;
      _LabelPedido.OnClick:=__OnClickLabel;
      _LabelPedido.Caption:='PEDIDO :'+qryLocalizaPP_QUERY.Fields[1].AsString;
      _QuantidadePanels:=_QuantidadePanels+1;

      _STRGStatusServico:=TStringGrid.Create(self);
      _STRGStatusServico.Parent:=_Panel;
      _STRGStatusServico.OnDrawCell:=__STRGLinhaServicosDrawCell;
      _STRGStatusServico.Align:=alTop;
      _STRGStatusServico.Tag:=_QuantidadePanels;
      _STRGStatusServico.Options:=[];
      _STRGStatusServico.BorderStyle:=bsNone;
      _STRGStatusServico.Font.Color:=$00A2663E;
      _STRGStatusServico.Font.Name:='Arial';
      __MostraServicos(qryLocalizaPP_QUERY.Fields[0].AsString);
      _STRGStatusServico.Row:=2;
      _QuantidadePanels:=_QuantidadePanels+1;
      _STRGStatusServico.Enabled:=False;

      _Imagem :=TImage.Create(self);
      _Imagem.Parent := _Panel;
      if (Fileexists(FDataModulo.LocalDesenhosGlobal+qryLocalizaPP_QUERY.Fields[4].AsString+'.jpg'))
      then _Imagem.Picture.LoadFromFile(FDataModulo.LocalDesenhosGlobal+qryLocalizaPP_QUERY.Fields[4].AsString+'.jpg')
      else _Imagem.Picture.LoadFromFile(FDataModulo.LocalDesenhosGlobal+qryLocalizaPP_QUERY.Fields[4].AsString+'.bmp');

      _Imagem.Tag:=_QuantidadePanels;
      _Imagem.Align:=alTop;
      _Imagem.Height:=140;
      _Imagem.Cursor:=crHandPoint;
      _QuantidadePanels:=_QuantidadePanels+1;

      qryLocalizaPP_QUERY.Next;
    end;
  finally
    FreeAndNil(qryLocalizaPP_QUERY);
  end;
end;

procedure TFVisualizaStatusProducao.__OnClickLabel(sender:TObject);
begin
   ShowMessage('HAHA!!!');
end;

procedure TFVisualizaStatusProducao.__MouseLeaveinLabel(Sender: TObject);
begin
  TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFVisualizaStatusProducao.__MouseMoveinLabel(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

function TFVisualizaStatusProducao.__MostraServicos(PedidoProjeto:string):Boolean;
var
  QryMostraServicos_QUERY:Tibquery;
  Executando:Boolean;
  Sender: TObject;
begin
   if(PedidoProjeto='')
   then Exit;

   Result:=False;

   QryMostraServicos_QUERY:=TIBQuery.Create(nil);
   QryMostraServicos_QUERY.Database:=FDataModulo.IBDatabase;
   Executando:=false;
   try
     QryMostraServicos_QUERY.Close;
     QryMostraServicos_QUERY.SQL.Clear;
     QryMostraServicos_QUERY.SQL.Text:=
     'select '+
     'tabpedidoproj_ordemproducao.estadoproducao, '+
     'tabservico.descricao as servico, '+
     'tabservico.referencia as referenciaservico, '+
     'tabprojeto.codigo, '+
     'tabpedido_proj.codigo as pedidoprojeto, '+
     'tabpedido.codigo, '+
     'tabprojeto.descricao as nomeprojeto, '+
     'tabpedidoproj_ordemproducao.codigo, '+
     'tabpedidoproj_ordemproducao.listadecomponentes '+
     'from tabpedidoproj_ordemproducao '+
     'join tabpedido_proj on tabpedido_proj.codigo=tabpedidoproj_ordemproducao.pedidoprojeto '+
     'join tabservico on tabpedidoproj_ordemproducao.servico=tabservico.codigo '+
     'join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto '+
     'join tabpedido on tabpedido.codigo=tabpedido_proj.pedido '+
     'where tabpedidoproj_ordemproducao.pedidoprojeto='+PedidoProjeto+' '+
     'order by tabpedidoproj_ordemproducao.codigo';

     _STRGStatusServico.FixedCols:=0;
     _STRGStatusServico.FixedRows:=0;
     _STRGStatusServico.ColCount:=1;
     _STRGStatusServico.RowCount:=5;
     _STRGStatusServico.ColWidths[0] := 128;
     _STRGStatusServico.RowHeights[1]:=40;
     _STRGStatusServico.RowHeights[2]:=0;
     _STRGStatusServico.RowHeights[3]:=0;
     _STRGStatusServico.RowHeights[4]:=0;
     _STRGStatusServico.Cells[0,0] := '';
     _STRGStatusServico.Cells[0,1] := '';
     _STRGStatusServico.Cells[0,2] := '';

     QryMostraServicos_QUERY.Open;

     while not QryMostraServicos_QUERY.Eof do
     begin
        _STRGStatusServico.Cells[QryMostraServicos_QUERY.RecordCount-1,0]:= QryMostraServicos_QUERY.Fields[2].AsString;
        _STRGStatusServico.Cells[QryMostraServicos_QUERY.RecordCount-1,1] := '';
        _STRGStatusServico.Cells[QryMostraServicos_QUERY.RecordCount-1,2] := QryMostraServicos_QUERY.Fields[0].AsString;
        _STRGStatusServico.Cells[QryMostraServicos_QUERY.RecordCount-1,3] := QryMostraServicos_QUERY.Fields[7].AsString;
        _STRGStatusServico.Cells[QryMostraServicos_QUERY.RecordCount-1,4] := QryMostraServicos_QUERY.Fields[8].AsString;
        if(QryMostraServicos_QUERY.Fields[0].AsString='P')then
        begin
          Executando:=True;
        end;

        QryMostraServicos_QUERY.Next;

        _STRGStatusServico.ColCount:=_STRGStatusServico.ColCount+1;
        _STRGStatusServico.ColWidths[_STRGStatusServico.ColCount-1] := 128;
     end;
     _STRGStatusServico.ColWidths[_STRGStatusServico.ColCount-2] := 125;
     _STRGStatusServico.ColCount:=_STRGStatusServico.ColCount-1;

     if(QryMostraServicos_QUERY.RecordCount>0) then
     begin
         Result:=True;
     end;

   finally
     FreeAndNil(QryMostraServicos_QUERY);
   end;
end;


procedure TFVisualizaStatusProducao.__STRGLinhaServicosDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
  if((ARow = 1))
  then begin
    if(TStringGrid(Sender).Cells[ACol,0] <> '' ) then
    begin
      if(TStringGrid(Sender).Cells[ACol,2] = 'A' )
      then Il1.Draw(TStringGrid(Sender).Canvas,Rect.Left ,Rect.Top,ACol)
      else
      begin
        if(TStringGrid(Sender).Cells[ACol,2] = 'P' )
        then Il1.Draw(TStringGrid(Sender).Canvas,Rect.Left ,Rect.Top,ACol+16)
        else Il1.Draw(TStringGrid(Sender).Canvas,Rect.Left ,Rect.Top,ACol+8)
      end;
    end
  end;
end;

procedure TFVisualizaStatusProducao.FormShow(Sender: TObject);
begin
  FescolheImagemBotao.PegaFiguraImagem(Img2,'RODAPE');
  _QuantidadePanels:=1;
  btConfiguracoesClick(Sender);
end;

procedure TFVisualizaStatusProducao.__LimpaItens;
var
  I:Integer;
begin
  for i:=ComponentCount-1 downto 0 do
  begin
    if (Components[I].Tag>0)
    then   TPanel(Components[i]).Free;
  end
end;

procedure TFVisualizaStatusProducao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  __LimpaItens;
end;

procedure TFVisualizaStatusProducao.btConfiguracoesClick(Sender: TObject);
begin
  __LimpaItens;
  __CriaPanelsRegistros(sender);
end;

procedure TFVisualizaStatusProducao.FormMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  With SCBXFundo.VertScrollBar Do
 Begin
  If (Position <= (Range - Increment))
  Then Position := Position + Increment
  Else Position := Range - Increment;
 End;
end;

procedure TFVisualizaStatusProducao.FormMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  With SCBXFundo.VertScrollBar Do
  Begin
   If (Position >= Increment)
   Then Position := Position - Increment
   Else Position := 0;
  End
end;

procedure TFVisualizaStatusProducao.bt3Click(Sender: TObject);
begin
  FAjuda.PassaAjuda('');
  FAjuda.ShowModal;
end;

procedure TFVisualizaStatusProducao.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
  if (Key = VK_F1) then
  begin
    try
      formPesquisaMenu:=TfPesquisaMenu.Create(nil);
      formPesquisaMenu.submit_formulario(Fprincipal);
      formPesquisaMenu.ShowModal;
    finally
      FreeAndNil(formPesquisaMenu);
    end;
  end;

  if (Key = VK_F2) then
  begin
    FAjuda.PassaAjuda('ORDEM DE MEDI��O');
    FAjuda.ShowModal;
  end;
  //Seta pra baixo
  if (Key=40) then
  begin
    With SCBXFundo.VertScrollBar Do
    Begin
      If (Position <= (Range - Increment))
      Then Position := Position + Increment
      Else Position := Range - Increment;
    End
  end;

  //Seta pra cima
  if (Key=38) then
  begin
    With SCBXFundo.VertScrollBar Do
    Begin
      If (Position >= Increment)
      Then Position := Position - Increment
      Else Position := 0;
    End
  end;
end;

end.
