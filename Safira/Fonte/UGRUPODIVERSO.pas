unit UGRUPODIVERSO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjDIVERSO,
  UessencialGlobal, Tabs,UPLanodeContas;

type
  TFGRUPODIVERSO = class(TForm)
    lbLbReferencia: TLabel;
    edtNome: TEdit;
    lbLbNome: TLabel;
    edtReferencia: TEdit;
    edtPlanodeContas: TEdit;
    lb1: TLabel;
    lbNomePlanoDeContas: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb2: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    btPrimeiro: TSpeedButton;
    btAnterior: TSpeedButton;
    btProximo: TSpeedButton;
    btUltimo: TSpeedButton;
    grp1: TGroupBox;
    lbLbPorcentagemFornecido: TLabel;
    lbLbPorcentagemRetirado: TLabel;
    lbLbPorcentagemInstalado: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    lb5: TLabel;
    edtPorcentagemInstalado: TEdit;
    edtPorcentagemFornecido: TEdit;
    edtPorcentagemRetirado: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtPlanodeContasExit(Sender: TObject);
    procedure edtPlanodeContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure edtPlanodeContasDblClick(Sender: TObject);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
    procedure edtPorcentagemInstaladoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemFornecidoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemRetiradoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPlanodeContasKeyPress(Sender: TObject; var Key: Char);
  private
    ObjDIVERSO:TOBJDIVERSO;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FGRUPODIVERSO: TFGRUPODIVERSO;


implementation

uses Upesquisa, UessencialLocal, UMenuRelatorios, UDataModulo,
  UescolheImagemBotao, UobjGRUPODIVERSO;

{$R *.dfm}


procedure TFGRUPODIVERSO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjDIVERSO=Nil)
     Then exit;

     If (Self.ObjDIVERSO.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjDIVERSO.free;
    Action := caFree;
    Self := nil;
end;



procedure TFGRUPODIVERSO.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbCodigo.Caption:='0';
     //edtcodigo.text:=ObjDiverso.GrupoDiverso.Get_novocodigo;



     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjDiverso.GrupoDiverso.status:=dsInsert;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;
     

     EdtReferencia.setfocus;

end;

procedure TFGRUPODIVERSO.btSalvarClick(Sender: TObject);
begin

     If ObjDiverso.GrupoDiverso.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjDiverso.GrupoDiverso.salvar(true)=False)
     Then exit;

     lbCodigo.Caption:=ObjDiverso.GrupoDiverso.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;

     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFGRUPODIVERSO.btAlterarClick(Sender: TObject);
begin
    If (ObjDiverso.GrupoDiverso.Status=dsinactive) and (lbCodigo.Caption<>'')
    Then Begin
                habilita_campos(Self);

                ObjDiverso.GrupoDiverso.Status:=dsEdit;

                EdtReferencia.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btnovo.Visible :=false;
               btalterar.Visible:=false;
               btpesquisar.Visible:=false;
               btrelatorio.Visible:=false;
               btexcluir.Visible:=false;
               btsair.Visible:=false;
               btopcoes.Visible :=false
          End;

end;

procedure TFGRUPODIVERSO.btCancelarClick(Sender: TObject);
begin
     ObjDiverso.GrupoDiverso.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;


end;

procedure TFGRUPODIVERSO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjDiverso.GrupoDiverso.Get_pesquisa,ObjDiverso.GrupoDiverso.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjDiverso.GrupoDiverso.status<>dsinactive
                                  then exit;

                                  If (ObjDiverso.GrupoDiverso.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjDiverso.GrupoDiverso.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFGRUPODIVERSO.btExcluirClick(Sender: TObject);
begin
     If (ObjDiverso.GrupoDiverso.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (ObjDiverso.GrupoDiverso.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjDiverso.GrupoDiverso.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFGRUPODIVERSO.btRelatorioClick(Sender: TObject);
begin
//    ObjDiverso.GrupoDiverso.Imprime(lbCodigo.Caption);
end;

procedure TFGRUPODIVERSO.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFGRUPODIVERSO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFGRUPODIVERSO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFGRUPODIVERSO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;


end;

procedure TFGRUPODIVERSO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFGRUPODIVERSO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjDiverso.GrupoDiverso do
    Begin
        Submit_Codigo(lbCodigo.Caption);
        Submit_Referencia(edtReferencia.text);
        Submit_Nome(edtNome.text);
        PlanoDeContas.Submit_CODIGO(EdtPlanodeContas.Text);
        Submit_PORCENTAGEMINSTALADO(edtPorcentagemInstalado.Text);
        Submit_PORCENTAGEMFORNECIDO(edtPorcentagemFornecido.Text);
        Submit_PorcentagemRetirado(edtPorcentagemRetirado.Text);
        result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFGRUPODIVERSO.LimpaLabels;
begin
     lbNomePlanoDeContas.Caption:='';
     lbCodigo.Caption:='';
end;

function TFGRUPODIVERSO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjDiverso.GrupoDiverso do
     Begin
       lbCodigo.Caption:=Get_Codigo;
        EdtReferencia.text:=Get_Referencia;
        EdtNome.text:=Get_Nome;
        EdtPlanodeContas.Text:=PlanoDeContas.Get_CODIGO;
        lbNomePlanoDeContas.Caption:='TIPO = '+ObjDiverso.GrupoDiverso.PlanoDeContas.Get_Tipo+' | ';
        lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' CLASSIF. = '+ObjDiverso.GrupoDiverso.PlanoDeContas.Get_Classificacao+' | ';
        lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' NOME = '+ObjDiverso.GrupoDiverso.PlanoDeContas.Get_Nome;
        edtPorcentagemInstalado.Text:=Get_PORCENTAGEMINSTALADO;
        edtPorcentagemRetirado.Text:=Get_PorcentagemRetirado;
        edtPorcentagemFornecido.Text:=Get_Porcentagemfornecido;
        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFGRUPODIVERSO.TabelaParaControles: Boolean;
begin
     If (ObjDiverso.GrupoDiverso.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;



procedure TFGRUPODIVERSO.edtPlanodeContasExit(Sender: TObject);
begin
    ObjDiverso.GrupoDiverso.EdtGrupoPlanoDeContasExit(Sender, lbNomePlanoDeContas);
    lbNomePlanoDeContas.Caption:='TIPO = '+ObjDiverso.GrupoDiverso.PlanoDeContas.Get_Tipo+' | ';
    lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' CLASSIF. = '+ObjDiverso.GrupoDiverso.PlanoDeContas.Get_Classificacao+' | ';
    lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' NOME = '+ObjDiverso.GrupoDiverso.PlanoDeContas.Get_Nome;
end;

procedure TFGRUPODIVERSO.edtPlanodeContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjDiverso.GrupoDiverso.EdtGrupoPlanoDeContasKeyDown(Sender, Key, Shift, lbNomePlanoDeContas);
end;

procedure TFGRUPODIVERSO.SpeedButton1Click(Sender: TObject);
begin
     With FmenuRelatorios do
     Begin
          //era no titulo antes
          NomeObjeto:='UObjDiverso.GrupoDiverso';

          With RgOpcoes do
          Begin
                items.clear;
                Items.Add('Atualiza Pre�o de Custo');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0 :  ObjDIVERSO.AtualizaPrecos(lbCodigo.Caption);
          End;
     end;

end;


procedure TFGRUPODIVERSO.edtPlanodeContasDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FplanoContas:TFplanodeContas;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FplanoContas:=TFplanodeContas.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabplanodecontas','',FplanoContas)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtPlanodeContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbNomePlanodeContas.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FplanoContas);

     End;
end;

procedure TFGRUPODIVERSO.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjDIVERSO.GrupoDiverso.PrimeiroRegistro = false)then
    exit;

    ObjDIVERSO.GrupoDiverso.TabelaparaObjeto;
    Self.ObjetoParaControles;
end;

procedure TFGRUPODIVERSO.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigo.Caption='')then
    lbCodigo.Caption:='0';

    if  (ObjDIVERSO.GrupoDiverso.RegistoAnterior(StrToInt(lbCodigo.Caption)) = false)then
    exit;

    ObjDIVERSO.GrupoDiverso.TabelaparaObjeto;
    Self.ObjetoParaControles;
end;

procedure TFGRUPODIVERSO.btProximoClick(Sender: TObject);
begin
    if (lbCodigo.Caption='')then
    lbCodigo.Caption:='0';

    if  (ObjDIVERSO.GrupoDiverso.ProximoRegisto(StrToInt(lbCodigo.Caption)) = false)then
    exit;

    ObjDIVERSO.GrupoDiverso.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPODIVERSO.btUltimoClick(Sender: TObject);
begin
      if  (ObjDIVERSO.GrupoDiverso.UltimoRegistro = false)then
    exit;

    ObjDIVERSO.GrupoDiverso.TabelaparaObjeto;
    Self.ObjetoParaControles;
end;

procedure TFGRUPODIVERSO.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     ColocaUpperCaseEdit(Self);

     Try
        Self.ObjDIVERSO:=TObjDIVERSO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');

     habilita_botoes(Self);
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE GRUPO DE DIVERSOS')=False)
     Then desab_botoes(Self);

     if(Tag<>0)
    then begin
         if(ObjDIVERSO.GrupoDiverso.LocalizaCodigo(IntToStr(Tag))=True)then
         begin
              ObjDIVERSO.GrupoDiverso.TabelaparaObjeto;
              self.ObjetoParaControles;
         end;

    end;
end;

procedure TFGRUPODIVERSO.btopcoesClick(Sender: TObject);
begin
  With FmenuRelatorios do
  Begin
    //era no titulo antes
    NomeObjeto:='UOBJGRUPOPERFILADO';

    With RgOpcoes do
    Begin
      items.clear;
      items.add('Atualiza Margens');
      Items.Add('Atualiza Pre�o de Custo');
    End;

    showmodal;

    if (Tag=0)//indica botao cancel ou fechar
    Then exit;

    Case RgOpcoes.ItemIndex of
      0 :  ObjDIVERSO.AtualizaMargens(lbCodigo.Caption);
      1 :  ObjDIVERSO.AtualizaPrecos(lbCodigo.Caption);
    End;
  end;
end;

procedure TFGRUPODIVERSO.edtPorcentagemInstaladoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFGRUPODIVERSO.edtPorcentagemFornecidoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFGRUPODIVERSO.edtPorcentagemRetiradoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFGRUPODIVERSO.edtPlanodeContasKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

end.
