unit UobjSERVICO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJGRUPOSERVICO, UObjCFOP,UobjTABELAB_ST;

Type
   TObjSERVICO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                ReferenciaAnterior                          :string;
                GrupoServico:TOBJGRUPOSERVICO;
                CFOP:TOBJCFOP;
                stb:TObjTABELAB_ST;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaReferencia(Parametro:string) :boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Referencia(parametro: string);
                Function Get_Referencia: string;
                Procedure Submit_Descricao(parametro: string);
                Function Get_Descricao: string;
                Procedure Submit_PrecoCusto(parametro: string);
                Function Get_PrecoCusto: string;
                Procedure Submit_PorcentagemInstalado(parametro: string);
                Function Get_PorcentagemInstalado: string;
                Procedure Submit_PorcentagemFornecido(parametro: string);
                Function Get_PorcentagemFornecido: string;
                Procedure Submit_PorcentagemRetirado(parametro: string);
                Function Get_PorcentagemRetirado: string;
                Procedure Submit_Arredondamento(parametro: string);
                Function Get_Arredondamento: string;
                Function Get_PrecoVendaInstalado:string;
                Function Get_PrecoVendaFornecido:string;
                Function Get_PrecoVendaRetirado:string;
                //procedure Submit_ControlaPorMilimetro(parametro: string); {Rodolfo}
                //function Get_ControlaPorMilimetro: string; {Rodolfo}
                procedure EdtGrupoServicoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtGrupoServicoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtCFOPKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtSTBKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Function  VerificaReferencia(PStatus:TDataSetState; PCodigo,PReferencia:string):Boolean; {Rodolfo}
                function PrimeiroRegistro: Boolean;
                function ProximoRegisto(PCodigo: Integer): Boolean;
                function RegistoAnterior(PCodigo: Integer): Boolean;
                function UltimoRegistro: Boolean;
                Procedure AtualizaPrecos(PGrupoServico:string);
                procedure EdtServicoKeyDown(Sender: TObject; var PEdtCodigo: TEdit; var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
                procedure Reajusta(PIndice, Pgrupo: string);

                procedure Submit_Ativo(parametro:String);
                Function Get_Ativo:String;
                procedure Submit_Tipoaliquota(parametro:string);
                Function Get_Tipoaliquota:string;
                procedure AtualizaMargens(PGrupoServico: string);
                
         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Referencia:string;
               Descricao:string;
               PrecoCusto:string;
               PorcentagemInstalado:string;
               PorcentagemFornecido:string;
               PorcentagemRetirado:string;
               Arredondamento:string;
               PrecoVendaInstalado:string;
               PrecoVendaFornecido:string;
               PrecoVendaRetirado:string;
               Ativo:string;
               TIPOALIQUOTA:string;



               //ControlaPorMilimetro:string;  {Rodolfo}
               
//CODIFICA VARIAVEIS PRIVADAS           

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                
   End;


implementation
uses Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
  UMenuRelatorios;


{ TTabTitulo }


Function  TObjSERVICO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Referencia:=fieldbyname('Referencia').asstring;
        If(FieldByName('GrupoServico').asstring<>'')
        Then Begin
                 If (Self.GrupoServico.LocalizaCodigo(FieldByName('GrupoServico').asstring)=False)
                 Then Begin
                          Messagedlg('Grupo Servi�o N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.GrupoServico.TabelaparaObjeto;
        End;
        If(FieldByName('CFOP').asstring<>'')
        Then Begin
                 If (Self.CFOP.LocalizaCodigo(FieldByName('CFOP').asstring)=False)
                 Then Begin
                          Messagedlg('Grupo Servi�o N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CFOP.TabelaparaObjeto;
        End;
        If(FieldByName('STB').asstring<>'')
        Then Begin
                 If (Self.STB.LocalizaCodigo(FieldByName('STB').asstring)=False)
                 Then Begin
                          Messagedlg('Grupo Servi�o N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.STB.TabelaparaObjeto;
        End;

        Self.Descricao:=fieldbyname('Descricao').asstring;
        Self.PrecoCusto:=fieldbyname('PrecoCusto').asstring;
        Self.PorcentagemInstalado:=fieldbyname('PorcentagemInstalado').asstring;
        Self.PorcentagemFornecido:=fieldbyname('PorcentagemFornecido').asstring;
        Self.PorcentagemRetirado:=fieldbyname('PorcentagemRetirado').asstring;
        Self.Arredondamento:=fieldbyname('Arredondamento').asstring;
        Self.PrecoVendaInstalado:=fieldbyname('PrecoVendaInstalado').AsString;
        Self.PrecoVendaFornecido:=fieldbyname('PrecoVendaFornecido').AsString;
        Self.PrecoVendaRetirado:=fieldbyname('PrecoVendaRetirado').AsString;
        self.Ativo:=fieldbyname('ativo').AsString;
        Self.TIPOALIQUOTA:=fieldbyname('tipoaliquota').AsString;

        //Self.ControlaPorMilimetro:=fieldbyname('ControlaPorMilimetro').AsString; {Rodolfo}

      //CODIFICA TABELAPARAOBJETO

        result:=True;
     End;
end;


Procedure TObjSERVICO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Referencia').asstring:=Self.Referencia;
        ParamByName('GrupoServico').asstring:=Self.GrupoServico.GET_CODIGO;
        ParamByName('Descricao').asstring:=Self.Descricao;
        ParamByName('PrecoCusto').asstring:=virgulaparaponto(Self.PrecoCusto);
        ParamByName('PorcentagemInstalado').asstring:=virgulaparaponto(Self.PorcentagemInstalado);
        ParamByName('PorcentagemFornecido').asstring:=virgulaparaponto(Self.PorcentagemFornecido);
        ParamByName('PorcentagemRetirado').asstring:=virgulaparaponto(Self.PorcentagemRetirado);
        ParamByName('Arredondamento').asstring:=virgulaparaponto(Self.Arredondamento);
        ParamByName('ativo').asstring:=Ativo;
        ParamByName('cfop').AsString:=CFOP.Get_CODIGO;
        ParamByName('stb').AsString:=stb.Get_CODIGO;
        ParamByName('tipoaliquota').AsString:=Self.TIPOALIQUOTA;


        //ParamByName('ControlaPorMilimetro').asstring:=ControlaPorMilimetro; {Rodolfo}



//CODIFICA OBJETOPARATABELA



  End;
End;

//***********************************************************************

function TObjSERVICO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

 { if (Self.VerificaNumericos=False)
  Then Exit;  }

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjSERVICO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Referencia:='';
        GrupoServico.ZerarTabela;
        Descricao:='';
        PrecoCusto:='';
        PorcentagemInstalado:='';
        PorcentagemFornecido:='';
        PorcentagemRetirado:='';
        Arredondamento:='';
        Ativo:='';
        CFOP.ZerarTabela;
        stb.ZerarTabela;
        TIPOALIQUOTA:='';

        //ControlaPorMilimetro:=''; {Rodolfo}



//CODIFICA ZERARTABELA



     End;
end;

Function TObjSERVICO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Referencia='')
      Then Mensagem:=mensagem+'/Refer�ncia';
      If (Descricao='')
      Then Mensagem:=mensagem+'/Descri��o';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjSERVICO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.GrupoServico.Get_CODIGO <> '')then
      If (Self.GrupoServico.LocalizaCodigo(Self.GrupoServico.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Grupo Servi�o n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS               

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjSERVICO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.GrupoServico.Get_Codigo<>'')
        Then Strtoint(Self.GrupoServico.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Grupo Servi�o';
     End;
     try
        Strtofloat(Self.PrecoCusto);
     Except
           Mensagem:=mensagem+'/Pre�o Custo';
     End;
     try
        Strtofloat(Self.PorcentagemInstalado);
     Except
           Mensagem:=mensagem+'/Instalado';
     End;
     try
        Strtofloat(Self.PorcentagemFornecido);
     Except
           Mensagem:=mensagem+'/Fornecido';
     End;
     try
        Strtofloat(Self.PorcentagemRetirado);
     Except
           Mensagem:=mensagem+'/Retirado';
     End;
     try
        Strtofloat(Self.Arredondamento);
     Except
           Mensagem:=mensagem+'/Arredondamento';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjSERVICO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjSERVICO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjSERVICO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro SERVICO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Referencia,GrupoServico,Descricao,PrecoCusto,PorcentagemInstalado');
           SQL.ADD(' ,PorcentagemFornecido,PorcentagemRetirado,Arredondamento');
           SQL.Add(', PrecoVendaInstalado, PrecoVendaRetirado, PrecoVendaFornecido, ativo, ControlaPorMilimetro,cfop, stb,tipoaliquota,versao');  {Rodolfo}
           SQL.ADD(' from  TABSERVICO');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjSERVICO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjSERVICO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjSERVICO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.GrupoServico:=TOBJGRUPOSERVICO.create;
        self.CFOP:=TObjCFOP.Create;
        Self.stb:=TObjTABELAB_ST.Create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABSERVICO(Codigo,Referencia,GrupoServico');
                InsertSQL.add(' ,Descricao,PrecoCusto,PorcentagemInstalado,PorcentagemFornecido');
                InsertSQL.add(' ,PorcentagemRetirado,Arredondamento,ativo, ControlaPorMilimetro,cfop,stb,tipoaliquota)');
                InsertSQL.add('values (:Codigo,:Referencia,:GrupoServico,:Descricao');
                InsertSQL.add(' ,:PrecoCusto,:PorcentagemInstalado,:PorcentagemFornecido');
                InsertSQL.add(' ,:PorcentagemRetirado,:Arredondamento,:ativo, :ControlaPorMilimetro,:cfop, :stb,:tipoaliquota)');   {Rodolfo}
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABSERVICO set Codigo=:Codigo,Referencia=:Referencia');
                ModifySQL.add(',GrupoServico=:GrupoServico,Descricao=:Descricao,PrecoCusto=:PrecoCusto');
                ModifySQL.add(',PorcentagemInstalado=:PorcentagemInstalado,PorcentagemFornecido=:PorcentagemFornecido');
                ModifySQL.add(',PorcentagemRetirado=:PorcentagemRetirado,Arredondamento=:Arredondamento');
                ModifySQL.add(',ativo=:ativo, ControlaPorMilimetro=:ControlaPorMilimetro,cfop=:cfop, stb=:stb,tipoaliquota=:tipoaliquota');  {Rodolfo}
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABSERVICO where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjSERVICO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjSERVICO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select Referencia,Descricao,GrupoServico,PrecoCusto,PorcentagemInstalado');
     Self.ParametroPesquisa.add(',PorcentagemFornecido,PorcentagemRetirado,Arredondamento');
     Self.ParametroPesquisa.add(', PrecoVendaInstalado, PrecoVendaRetirado, PrecoVendaFornecido,Codigo, ControlaPorMilimetro,cfop, stb,tipoaliquota');   {Rodolfo}
     Self.ParametroPesquisa.Add('From TabServico where ativo=''S'' ');
     Result:=Self.ParametroPesquisa;
end;

function TObjSERVICO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de SERVICO ';
end;


function TObjSERVICO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENSERVICO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENSERVICO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjSERVICO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.GrupoServico.FREE;
    self.CFOP.Free;
    self.stb.Free;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjSERVICO.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjSERVICO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjServico.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjServico.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjServico.Submit_Referencia(parametro: string);
begin
        Self.Referencia:=Parametro;
end;
function TObjServico.Get_Referencia: string;
begin
        Result:=Self.Referencia;
end;
procedure TObjServico.Submit_Descricao(parametro: string);
begin
        Self.Descricao:=Parametro;
end;
function TObjServico.Get_Descricao: string;
begin
        Result:=Self.Descricao;
end;
procedure TObjServico.Submit_PrecoCusto(parametro: string);
begin
        Self.PrecoCusto:=Parametro;
end;
function TObjServico.Get_PrecoCusto: string;
begin
        Result:=Self.PrecoCusto;
end;
procedure TObjServico.Submit_PorcentagemInstalado(parametro: string);
begin
        Self.PorcentagemInstalado:=Parametro;
end;
function TObjServico.Get_PorcentagemInstalado: string;
begin
        Result:=Self.PorcentagemInstalado;
end;
procedure TObjServico.Submit_PorcentagemFornecido(parametro: string);
begin
        Self.PorcentagemFornecido:=Parametro;
end;
function TObjServico.Get_PorcentagemFornecido: string;
begin
        Result:=Self.PorcentagemFornecido;
end;
procedure TObjServico.Submit_PorcentagemRetirado(parametro: string);
begin
        Self.PorcentagemRetirado:=Parametro;
end;
function TObjServico.Get_PorcentagemRetirado: string;
begin
        Result:=Self.PorcentagemRetirado;
end;
procedure TObjServico.Submit_Arredondamento(parametro: string);
begin
        Self.Arredondamento:=Parametro;
end;
function TObjServico.Get_Arredondamento: string;
begin
        Result:=Self.Arredondamento;
end;

{Rodolfo
procedure TObjServico.Submit_ControlaPorMilimetro(parametro: string);
begin
        Self.ControlaPorMilimetro:=Parametro;
end;

function TObjServico.Get_ControlaPorMilimetro: string;
begin
        Result:=Self.ControlaPorMilimetro;
end;   }
{=======}

//CODIFICA GETSESUBMITS


procedure TObjSERVICO.EdtGrupoServicoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.GrupoServico.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.GrupoServico.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.GrupoServico.GET_NOME;
End;
procedure TObjSERVICO.EdtGrupoServicoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FSERVICO.EDTGRUPOSERVICO';
            If (FpesquisaLocal.PreparaPesquisa(Self.GrupoServico.Get_Pesquisa,Self.GrupoServico.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoServico.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.GrupoServico.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoServico.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjSERVICO.EdtCFOPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('SELECT * FROM TABCFOP','CFOP',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.GrupoServico.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjSERVICO.EdtSTBKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('SELECT * FROM TABTABELAB_ST','STB',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.GrupoServico.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;



procedure TObjSERVICO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJSERVICO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



function TObjSERVICO.Get_PrecoVendaFornecido: string;
begin
   Result:=Self.PrecoVendaFornecido;
end;

function TObjSERVICO.Get_PrecoVendaInstalado: string;
begin
   Result:=Self.PrecoVendaInstalado;
end;

function TObjSERVICO.Get_PrecoVendaRetirado: string;
begin
   Result:=Self.PrecoVendaRetirado;
end;

function TObjSERVICO.VerificaReferencia(PStatus: TDataSetState; PCodigo,
  PReferencia: string): Boolean; {Rodolfo}
begin
     Result:=true;

     PReferencia  := StrReplaceRef(PReferencia); {Rodolfo}

     With Self.Objquery do
     Begin

         if (PStatus = dsInsert) then  // quando for insercao
         Begin
             close;
             SQL.Clear;
             SQL.Add('Select Codigo from TabServico Where Referencia = '+#39+UpperCase(PReferencia)+#39);
             Open;

             if (RecordCount > 0)then
             Begin
                 Result:=true;
                 exit;
             end else
             Result:=false;
             exit;
         end;


         if  (PStatus = dsEdit)then    // quando for edicao
         Begin
             if Self.ReferenciaAnterior = PReferencia then
             Begin                      // Se a Referencia  que esta chegando � a mesma da anteiror
                 Result:=false;         // eu nem preciso verificar nada
                 exit;
             end else
             Begin
                 close;
                 SQL.Clear;
                 SQL.Add('Select Codigo from TabServico Where Referencia = '+#39+UpperCase(PReferencia)+#39);
                 Open;

                 if (RecordCount > 0)then
                 Begin
                     Result:=true;
                     exit;
                 end else
                 Result:=false;
                 exit;
             end;
         end;
     end;

end;

function TObjSERVICO.LocalizaReferencia(Parametro: string): boolean;
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro SERVICO vazio',mterror,[mbok],0);
                 exit;
       End;

       parametro:=StrReplaceRef(parametro);{Rodolfo}

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Referencia,GrupoServico,Descricao,PrecoCusto,PorcentagemInstalado');
           SQL.ADD(' ,PorcentagemFornecido,PorcentagemRetirado,Arredondamento');
           SQL.Add(', PrecoVendaInstalado, PrecoVendaRetirado, PrecoVendaFornecido,ativo, ControlaPorMilimetro,cfop, stb,tipoaliquota');
           SQL.ADD(' from  TABSERVICO');
           SQL.ADD(' WHERE Referencia = '+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;
function TObjServico.PrimeiroRegistro: Boolean;
Var MenorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabServico') ;
           Open;

           if (FieldByName('MenorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MenorCodigo:=fieldbyname('MenorCodigo').AsString;
           Self.LocalizaCodigo(MenorCodigo);

           Result:=true;
     end;
end;

function TObjServico.UltimoRegistro: Boolean;
Var MaiorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabServico') ;
           Open;

           if (FieldByName('MaiorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MaiorCodigo:=fieldbyname('MaiorCodigo').AsString;
           Self.LocalizaCodigo(MaiorCodigo);

           Result:=true;
     end;
end;

function TObjServico.ProximoRegisto(PCodigo:Integer): Boolean;
Var MaiorValor:Integer;
begin
     Result:=false;

     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabServico') ;
           Open;

           MaiorValor:=fieldbyname('MaiorCodigo').AsInteger;
     end;

     if (MaiorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Inc(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MaiorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Inc(PCodigo,1);
     end;

     Result:=true;
end;

function TObjServico.RegistoAnterior(PCodigo: Integer): Boolean;
Var MenorValor:Integer;
begin
     Result:=false;
     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabServico') ;
           Open;

           MenorValor:=fieldbyname('MenorCodigo').AsInteger;
     end;

     if (MenorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Dec(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MenorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Dec(PCodigo,1);
     end;

     Result:=true;
end;

procedure TObjSERVICO.AtualizaPrecos(PGrupoServico: string);
Var QueryLOcal :TIBQuery; PVAlorCusto, PPorcentagemReajuste:Currency;
begin

      if (PGrupoServico = '')then
      Begin
           MensagemErro('Pesquise o um grupo de Servico entes Atualizar os pre�os.');
           exit;
      end;

      if MessageDlg('Tem certea que deseja alterar os valores dos PR�OS DE CUSTO de '+#13+
                    'todas os Servi�os pertencentes e este grupo?. Lembrando que esse processo � irrevers�vel.', mtConfirmation, [mbYes, mbNo], 0) = mrNo
      then Begin
           exit;
      end;

      With FfiltroImp do
      Begin
           DesativaGrupos;
           Grupo01.Enabled:=true;
           LbGrupo01.Caption:='Acr�scimo em (%)';
           edtgrupo01.Enabled:=True;

           ShowModal;

           if (Tag = 0)then
           Exit;

           try
                PPorcentagemReajuste:=StrToCurr(edtgrupo01.Text);
           except
                MensagemErro('Valor inv�lido.');
                exit;
           end;
      end;

      try
            try
                 QueryLocal:=TIBQuery.Create(nil);
                 QueryLOcal.Database:=FDataModulo.IBDatabase;
            except
                 MensagemErro('Erro ao tentar criar a Query Local');
                 exit;
            end;

            With  QueryLOcal do
            Begin
                 Close;
                 Sql.Clear;
                 Sql.Add('Select Codigo from TabServico Where GrupoServico = '+PGrupoServico);
                 Open;
                 First;

                 if (RecordCount = 0)then
                 Begin
                      MensagemErro('Nenhuma Servico encotrado para este grupo.');
                      exit;
                 end;


                 While Not (eof) do
                 Begin
                      PVAlorCusto:=0;
                      Self.ZerarTabela;
                      Self.LocalizaCodigo(fieldbyname('Codigo').AsString);
                      Self.TabelaparaObjeto;
                      PVAlorCusto:=StrToCurr(Self.Get_PrecoCusto);

                      Self.Status:=dsEdit;
                      Self.Submit_PrecoCusto(CurrToStr(PVAlorCusto+(PVAlorCusto*(PPorcentagemReajuste/100))));
                      try
                           Self.Salvar(false);
                      except
                           MensagemErro('Erro ao tentar Salvar o novo pre�o.');
                           FDataModulo.IBTransaction.RollbackRetaining;
                           exit;
                      end;
                 Next;
                 end;

                 FDataModulo.IBTransaction.CommitRetaining;
                 MensagemAviso('Pre�o de custo de servi�o atualizado com Sucesso!');
            end;

      finally
            FreeAndNil(QueryLOcal);
      end;

end;
procedure TObjServico.EdtServicoKeyDown(Sender: TObject; var PEdtCodigo: TEdit;var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,Self.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjservico.Reajusta(PIndice,Pgrupo: string);
var
   Psinal:string;
begin
  try
    strtofloat(Pindice);

    if (strtofloat(Pindice)>=0) then
      Psinal:='+'
    else
    begin
      Pindice:=floattostr(strtofloat(Pindice)*-1);
      Psinal:='-';
    End;

  except
    mensagemerro('Valor Inv�lido no �ndice');
    exit;
  End;

  if (Pgrupo<>'') then
  begin
    try
      strtoint(Pgrupo);
      if (Self.Gruposervico.LocalizaCodigo(pgrupo)=False) then
      begin
        MensagemErro('Grupo de servico n�o encontrado');
        exit;
      end;
    Except
      mensagemerro('Grupo Inv�lido');
      exit;
    end;
  End;

  With Self.Objquery do
  Begin
    close;
    SQL.clear;
    SQL.add('Update Tabservico Set PRECOCUSTO=PRECOCUSTO'+psinal+'((PRECOCUSTO*'+virgulaparaponto(Pindice)+')/100)');
    SQl.Add('precovendainstalado = precocusto + ( precocusto * porcentageminstalado/100),');
    SQl.Add('precovendafornecido = precocusto + ( precocusto * porcentagemfornecido/100),');
    SQl.Add('precovendaretirado = precocusto + ( precocusto * porcentagemretirado/100)');

    if (Pgrupo<>'') then
      SQL.add('Where Gruposervico='+Pgrupo);

    try
      execsql;
      FDataModulo.IBTransaction.CommitRetaining;
      mensagemaviso('Conclu�do');
    Except
      on e:exception do
      begin
        FDataModulo.IBTransaction.RollbackRetaining;
        mensagemerro('Erro na tentativa de Reajustar o valor do servico'+#13+E.message);
        exit;
      End;
    End;
  End;
end;

procedure TObjSERVICO.Submit_Ativo(parametro:string);
begin
    Ativo:=parametro;
end;

function TObjSERVICO.Get_Ativo:string;
begin
  result:=Ativo;
end;

procedure TObjSERVICO.Submit_Tipoaliquota(parametro:string);
begin
  Self.TIPOALIQUOTA:=parametro;
end;

function TObjSERVICO.Get_Tipoaliquota:string;
begin
  Result:=TIPOALIQUOTA;
end;

procedure TObjSERVICO.AtualizaMargens(PGrupoServico: string);
Var QueryLOcal:TIBquery;
    PPorcentagemInstalado:string;
    PPorcentagemRetirado:string;
    PPorcentagemFornecido:string;
begin
  if (PGrupoServico='')then
  Begin
    MensagemErro('Escolha um Grupo de Servico');
    exit;
  end;

  if (Self.GrupoServico.LocalizaCodigo(PGrupoServico)=false)then
  Begin
    MensagemErro('Grupo de Servico n�o encontrado');
    exit;
  end;
  Self.GrupoServico.TabelaparaObjeto;
  PPorcentagemInstalado:=Self.GrupoServico.Get_PorcentagemInstalado;
  PPorcentagemRetirado:=Self.GrupoServico.Get_PorcentagemRetirado;
  PPorcentagemFornecido:=Self.GrupoServico.Get_PorcentagemFornecido;

  try
    try
      QueryLOcal:=TIBQuery.Create(nil);
      QueryLOcal.Database:=FDataModulo.IBDatabase;
    except;
      MensagemErro('Erro ao tenar criar a QueryLocal');
      exit;
    end;

    With QueryLOcal  do
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select TabServico.Codigo from TabServico');
      Sql.Add('Where GrupoServico = '+PGrupoServico);
      Open;
      First;

      // Para cada Servico desse grupo eu Altero o Valor das margns
      While not (eof) do
      Begin
        Self.ZerarTabela;
        if (Self.LocalizaCodigo(fieldbyname('Codigo').AsString)=false)then
        Begin
          MensagemErro('Diverso n�o encontrada.');
          exit;
        end;

        Self.TabelaparaObjeto;
        Self.Status:=dsEdit;
        Self.Submit_PorcentagemInstalado(PPorcentagemInstalado);
        Self.Submit_PorcentagemFornecido(PPorcentagemFornecido);
        Self.Submit_PorcentagemRetirado(PPorcentagemRetirado);


        if (Self.Salvar(false)=false)then
        begin
          MensagemErro('Erro ao tentar Atualiza as margens na Tabela de Diverso');
          exit;
        end;
        Next;
      end;
    end;
    FDataModulo.IBTransaction.CommitRetaining;
    MensagemAviso('Margem Atualiada com Sucesso !');
  finally
    FreeAndNil(QueryLOcal);
  end;
end;


end.



