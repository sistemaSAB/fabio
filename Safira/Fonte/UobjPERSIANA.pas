unit UobjPERSIANA;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UobjTabelaA_ST, UObjTabelaB_ST,
     UobjFornecedor;

Type
   TObjPERSIANA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                SituacaoTributaria_TabelaA:TOBJTABELAA_ST ;
                SituacaoTributaria_TabelaB:TOBJTABELAB_ST ;
                Fornecedor:TOBJFORNECEDOR;


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaReferencia(Parametro:string):Boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Referencia(parametro: string);
                Function Get_Referencia: string;
                Procedure Submit_Nome(parametro: string);
                Function Get_Nome: string;

                Procedure Submit_IsentoICMS_Estado(parametro: string);
                Function Get_IsentoICMS_Estado: string;
                Procedure Submit_SubstituicaoICMS_Estado(parametro: string);
                Function Get_SubstituicaoICMS_Estado: string;
                Procedure Submit_ValorPauta_Sub_Trib_Estado(parametro: string);
                Function Get_ValorPauta_Sub_Trib_Estado: string;
                Procedure Submit_Aliquota_ICMS_Estado(parametro: string);
                Function Get_Aliquota_ICMS_Estado: string;
                Procedure Submit_Reducao_BC_ICMS_Estado(parametro: string);
                Function Get_Reducao_BC_ICMS_Estado: string;
                Procedure Submit_Aliquota_ICMS_Cupom_Estado(parametro: string);
                Function Get_Aliquota_ICMS_Cupom_Estado: string;
                Procedure Submit_IsentoICMS_ForaEstado(parametro: string);
                Function Get_IsentoICMS_ForaEstado: string;
                Procedure Submit_SubstituicaoICMS_ForaEstado(parametro: string);
                Function Get_SubstituicaoICMS_ForaEstado: string;
                Procedure Submit_ValorPauta_Sub_Trib_ForaEstado(parametro: string);
                Function Get_ValorPauta_Sub_Trib_ForaEstado: string;
                Procedure Submit_Aliquota_ICMS_ForaEstado(parametro: string);
                Function Get_Aliquota_ICMS_ForaEstado: string;
                Procedure Submit_Reducao_BC_ICMS_ForaEstado(parametro: string);
                Function Get_Reducao_BC_ICMS_ForaEstado: string;
                Procedure Submit_Aliquota_ICMS_Cupom_ForaEstado(parametro: string);
                Function Get_Aliquota_ICMS_Cupom_ForaEstado: string;

                Procedure Submit_percentualagregado(parametro: string);
                Function Get_percentualagregado: string;

                Procedure Submit_ipi(parametro: string);
                Function Get_ipi: string;



                Procedure Submit_ClassificacaoFiscal(parametro: string);
                Function Get_ClassificacaoFiscal: string;

                procedure EdtFornecedorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFornecedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtSituacaoTributaria_TabelaAExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtSituacaoTributaria_TabelaAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtSituacaoTributaria_TabelaBExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtSituacaoTributaria_TabelaBKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                function PrimeiroRegistro: Boolean;
                function ProximoRegisto(PCodigo: Integer): Boolean;
                function RegistoAnterior(PCodigo: Integer): Boolean;
                function UltimoRegistro: Boolean;

                procedure Submit_NCM(parametro:string);
                function Get_NCM:string;

                procedure Submit_cest(parametro:string);
                function Get_cest:string;


                procedure Submit_Ativo(parametro:string);
                function Get_Ativo:string;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Referencia:string;
               Nome:string;
               IsentoICMS_Estado:string;
               SubstituicaoICMS_Estado:string;
               ValorPauta_Sub_Trib_Estado:string;
               Aliquota_ICMS_Estado:string;
               Reducao_BC_ICMS_Estado:string;
               Aliquota_ICMS_Cupom_Estado:string;
               IsentoICMS_ForaEstado:string;
               SubstituicaoICMS_ForaEstado:string;
               ValorPauta_Sub_Trib_ForaEstado:string;
               Aliquota_ICMS_ForaEstado:string;
               Reducao_BC_ICMS_ForaEstado:string;
               Aliquota_ICMS_Cupom_ForaEstado:string;
               percentualagregado:string;
               ipi:string;
               ClassificacaoFiscal:string;  
               ncm:string;
               Ativo:string;
               cest:string;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios
//USES IMPLEMENTATION
;


{ TTabTitulo }


Function  TObjPERSIANA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Referencia:=fieldbyname('Referencia').asstring;
        Self.Nome:=fieldbyname('Nome').asstring;
        Self.IsentoICMS_Estado:=fieldbyname('IsentoICMS_Estado').asstring;
        Self.SubstituicaoICMS_Estado:=fieldbyname('SubstituicaoICMS_Estado').asstring;
        Self.ValorPauta_Sub_Trib_Estado:=fieldbyname('ValorPauta_Sub_Trib_Estado').asstring;
        Self.Aliquota_ICMS_Estado:=fieldbyname('Aliquota_ICMS_Estado').asstring;
        Self.Reducao_BC_ICMS_Estado:=fieldbyname('Reducao_BC_ICMS_Estado').asstring;
        Self.Aliquota_ICMS_Cupom_Estado:=fieldbyname('Aliquota_ICMS_Cupom_Estado').asstring;
        Self.IsentoICMS_ForaEstado:=fieldbyname('IsentoICMS_ForaEstado').asstring;
        Self.SubstituicaoICMS_ForaEstado:=fieldbyname('SubstituicaoICMS_ForaEstado').asstring;
        Self.ValorPauta_Sub_Trib_ForaEstado:=fieldbyname('ValorPauta_Sub_Trib_ForaEstado').asstring;
        Self.Aliquota_ICMS_ForaEstado:=fieldbyname('Aliquota_ICMS_ForaEstado').asstring;
        Self.Reducao_BC_ICMS_ForaEstado:=fieldbyname('Reducao_BC_ICMS_ForaEstado').asstring;
        Self.Aliquota_ICMS_Cupom_ForaEstado:=fieldbyname('Aliquota_ICMS_Cupom_ForaEstado').asstring;
        Self.percentualagregado:=fieldbyname('percentualagregado').asstring;
        Self.ipi:=fieldbyname('ipi').asstring;


        If(FieldByName('SituacaoTributaria_TabelaA').asstring<>'')
        Then Begin
                 If (Self.SituacaoTributaria_TabelaA.LocalizaCodigo(FieldByName('SituacaoTributaria_TabelaA').asstring)=False)
                 Then Begin
                          Messagedlg('SituacaoTributaria_TabelaA N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.SituacaoTributaria_TabelaA.TabelaparaObjeto;
        End;
        If(FieldByName('SituacaoTributaria_TabelaB').asstring<>'')
        Then Begin
                 If (Self.SituacaoTributaria_TabelaB.LocalizaCodigo(FieldByName('SituacaoTributaria_TabelaB').asstring)=False)
                 Then Begin
                          Messagedlg('SituacaoTributaria_TabelaB N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.SituacaoTributaria_TabelaB.TabelaparaObjeto;
        End;

        Self.ClassificacaoFiscal:=fieldbyname('ClassificacaoFiscal').asstring;
        self.ncm:=fieldbyname('NCM').AsString;
        self.cest:=fieldbyname('cest').AsString;
        self.Ativo:=fieldbyname('Ativo').AsString;

        If(FieldByName('Fornecedor').asstring<>'')
        Then Begin
                 If (Self.Fornecedor.LocalizaCodigo(FieldByName('Fornecedor').asstring)=False)
                 Then Begin
                          Messagedlg('Fornecedor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Fornecedor.TabelaparaObjeto;
        End;                                           
        result:=True;
     End;
end;


Procedure TObjPERSIANA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Referencia').asstring:=Self.Referencia;
        ParamByName('Nome').asstring:=Self.Nome;
        ParamByName('IsentoICMS_Estado').asstring:=Self.IsentoICMS_Estado;
        ParamByName('SubstituicaoICMS_Estado').asstring:=Self.SubstituicaoICMS_Estado;
        ParamByName('ValorPauta_Sub_Trib_Estado').asstring:=virgulaparaponto(Self.ValorPauta_Sub_Trib_Estado);
        ParamByName('Aliquota_ICMS_Estado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_Estado);
        ParamByName('Reducao_BC_ICMS_Estado').asstring:=virgulaparaponto(Self.Reducao_BC_ICMS_Estado);
        ParamByName('Aliquota_ICMS_Cupom_Estado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_Cupom_Estado);
        ParamByName('IsentoICMS_ForaEstado').asstring:=Self.IsentoICMS_ForaEstado;
        ParamByName('SubstituicaoICMS_ForaEstado').asstring:=Self.SubstituicaoICMS_ForaEstado;
        ParamByName('ValorPauta_Sub_Trib_ForaEstado').asstring:=virgulaparaponto(Self.ValorPauta_Sub_Trib_ForaEstado);
        ParamByName('Aliquota_ICMS_ForaEstado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_ForaEstado);
        ParamByName('Reducao_BC_ICMS_ForaEstado').asstring:=virgulaparaponto(Self.Reducao_BC_ICMS_ForaEstado);
        ParamByName('Aliquota_ICMS_Cupom_ForaEstado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_Cupom_ForaEstado);
        ParamByName('percentualagregado').asstring:=virgulaparaponto(Self.percentualagregado);
        ParamByName('ipi').asstring:=virgulaparaponto(Self.ipi);
        ParamByName('SituacaoTributaria_TabelaA').asstring:=Self.SituacaoTributaria_TabelaA.GET_CODIGO;
        ParamByName('SituacaoTributaria_TabelaB').asstring:=Self.SituacaoTributaria_TabelaB.GET_CODIGO;
        ParamByName('ClassificacaoFiscal').asstring:=Self.ClassificacaoFiscal;
        ParamByName('Fornecedor').AsString:=Fornecedor.Get_Codigo;
        ParamByName('NCM').AsString:=Get_NCM;
        ParamByName('cest').AsString:=cest;
        ParamByName('Ativo').AsString:=self.Ativo;
  End;
End;

//***********************************************************************

function TObjPERSIANA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPERSIANA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Referencia:='';
        Nome:='';
        IsentoICMS_Estado:='';
        SubstituicaoICMS_Estado:='';
        ValorPauta_Sub_Trib_Estado:='';
        Aliquota_ICMS_Estado:='';
        Reducao_BC_ICMS_Estado:='';
        Aliquota_ICMS_Cupom_Estado:='';
        IsentoICMS_ForaEstado:='';
        SubstituicaoICMS_ForaEstado:='';
        ValorPauta_Sub_Trib_ForaEstado:='';
        Aliquota_ICMS_ForaEstado:='';
        Reducao_BC_ICMS_ForaEstado:='';
        Aliquota_ICMS_Cupom_ForaEstado:='';
        percentualagregado:='';
        ipi:='';
        SituacaoTributaria_TabelaA.ZerarTabela;
        SituacaoTributaria_TabelaB.ZerarTabela;
        ClassificacaoFiscal:='';
        NCM:='';
        cest:='';
        Fornecedor.ZerarTabela;
        Ativo:='';
     End;
end;

Function TObjPERSIANA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/C�digo';


      if (percentualagregado='')
      Then percentualagregado:='0';

      if (IPI='')
      Then Ipi:='0';

      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPERSIANA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjPERSIANA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPERSIANA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPERSIANA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPERSIANA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;


       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PERSIANA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Referencia,Nome,IsentoICMS_Estado');
           SQL.ADD(' ,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado,Aliquota_ICMS_Estado');
           SQL.ADD(' ,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado,IsentoICMS_ForaEstado');
           SQL.ADD(' ,SubstituicaoICMS_ForaEstado,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
           SQL.ADD(' ,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado,SituacaoTributaria_TabelaA');
           SQL.ADD(' ,SituacaoTributaria_TabelaB,ClassificacaoFiscal, Fornecedor,percentualagregado,ipi');
           SQL.ADD(' ,NCM,Ativo,cest from  TabPersiana');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPERSIANA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPERSIANA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPERSIANA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        Self.Fornecedor:=TOBJFORNECEDOR.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;

        Self.SituacaoTributaria_TabelaA:=TOBJTABELAA_ST .create;
        Self.SituacaoTributaria_TabelaB:=TOBJTABELAB_ST .create;

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabPersiana(Codigo,Referencia,Nome,IsentoICMS_Estado');
                InsertSQL.add(' ,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado');
                InsertSQL.add(' ,Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado');
                InsertSQL.add(' ,IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado');
                InsertSQL.add(' ,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
                InsertSQL.add(' ,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado');
                InsertSQL.add(' ,SituacaoTributaria_TabelaA,SituacaoTributaria_TabelaB');
                InsertSQL.add(' ,ClassificacaoFiscal, Fornecedor,percentualagregado,ipi,NCM,Ativo,cest)');
                InsertSQL.add('values (:Codigo,:Referencia,:Nome,:IsentoICMS_Estado');
                InsertSQL.add(' ,:SubstituicaoICMS_Estado,:ValorPauta_Sub_Trib_Estado');
                InsertSQL.add(' ,:Aliquota_ICMS_Estado,:Reducao_BC_ICMS_Estado,:Aliquota_ICMS_Cupom_Estado');
                InsertSQL.add(' ,:IsentoICMS_ForaEstado,:SubstituicaoICMS_ForaEstado');
                InsertSQL.add(' ,:ValorPauta_Sub_Trib_ForaEstado,:Aliquota_ICMS_ForaEstado');
                InsertSQL.add(' ,:Reducao_BC_ICMS_ForaEstado,:Aliquota_ICMS_Cupom_ForaEstado');
                InsertSQL.add(' ,:SituacaoTributaria_TabelaA,:SituacaoTributaria_TabelaB');
                InsertSQL.add(' ,:ClassificacaoFiscal,:Fornecedor,:percentualagregado,:ipi,:NCM,:Ativo,:cest)');

                ModifySQL.clear;
                ModifySQL.add('Update TabPersiana set Codigo=:Codigo,Referencia=:Referencia');
                ModifySQL.add(',Nome=:Nome');
                ModifySQL.add(',IsentoICMS_Estado=:IsentoICMS_Estado,SubstituicaoICMS_Estado=:SubstituicaoICMS_Estado');
                ModifySQL.add(',ValorPauta_Sub_Trib_Estado=:ValorPauta_Sub_Trib_Estado');
                ModifySQL.add(',Aliquota_ICMS_Estado=:Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado=:Reducao_BC_ICMS_Estado');
                ModifySQL.add(',Aliquota_ICMS_Cupom_Estado=:Aliquota_ICMS_Cupom_Estado');
                ModifySQL.add(',IsentoICMS_ForaEstado=:IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado=:SubstituicaoICMS_ForaEstado');
                ModifySQL.add(',ValorPauta_Sub_Trib_ForaEstado=:ValorPauta_Sub_Trib_ForaEstado');
                ModifySQL.add(',Aliquota_ICMS_ForaEstado=:Aliquota_ICMS_ForaEstado');
                ModifySQL.add(',Reducao_BC_ICMS_ForaEstado=:Reducao_BC_ICMS_ForaEstado');
                ModifySQL.add(',Aliquota_ICMS_Cupom_ForaEstado=:Aliquota_ICMS_Cupom_ForaEstado');
                ModifySQL.add(',SituacaoTributaria_TabelaA=:SituacaoTributaria_TabelaA');
                ModifySQL.add(',SituacaoTributaria_TabelaB=:SituacaoTributaria_TabelaB');
                ModifySQL.add(',ClassificacaoFiscal=:ClassificacaoFiscal,Fornecedor=:Fornecedor');
                ModifySQL.add(',percentualagregado=:percentualagregado,ipi=:ipi,NCM=:NCM,Ativo=:Ativo,cest=:cest  where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabPersiana where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPERSIANA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPERSIANA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select Codigo,Referencia,Nome,IsentoICMS_Estado');
     Self.ParametroPesquisa.add(' ,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado,Aliquota_ICMS_Estado');
     Self.ParametroPesquisa.add(' ,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado,IsentoICMS_ForaEstado');
     Self.ParametroPesquisa.add(' ,SubstituicaoICMS_ForaEstado,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
     Self.ParametroPesquisa.add(' ,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado,SituacaoTributaria_TabelaA');
     Self.ParametroPesquisa.add(' ,SituacaoTributaria_TabelaB,ClassificacaoFiscal, Fornecedor,percentualagregado,ipi,NCM,Ativo,cest');
     Self.ParametroPesquisa.Add('from TabPersiana');
     Result:=Self.ParametroPesquisa;
end;

function TObjPERSIANA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de PERSIANA ';
end;


function TObjPERSIANA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPERSIANA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENPERSIANA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPERSIANA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Fornecedor.FREE;
    Self.SituacaoTributaria_TabelaA.FREE;
    Self.SituacaoTributaria_TabelaB.FREE;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPERSIANA.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPERSIANA.RetornaCampoNome: string;
begin
      result:='NOME';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPersiana.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjPersiana.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjPersiana.Submit_Referencia(parametro: string);
begin
        Self.Referencia:=Parametro;
end;
function TObjPersiana.Get_Referencia: string;
begin
        Result:=Self.Referencia;
end;
procedure TObjPersiana.Submit_Nome(parametro: string);
begin
        Self.Nome:=Parametro;
end;
function TObjPersiana.Get_Nome: string;
begin
        Result:=Self.Nome;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjPERSIANA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPERSIANA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



function TObjPERSIANA.LocalizaReferencia(Parametro: string): Boolean;
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PERSIANA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Referencia,Nome,IsentoICMS_Estado');
           SQL.ADD(' ,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado,Aliquota_ICMS_Estado');
           SQL.ADD(' ,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado,IsentoICMS_ForaEstado');
           SQL.ADD(' ,SubstituicaoICMS_ForaEstado,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
           SQL.ADD(' ,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado,SituacaoTributaria_TabelaA');
           SQL.ADD(' ,SituacaoTributaria_TabelaB,ClassificacaoFiscal,Fornecedor,percentualagregado,ipi,NCM,Ativo,cest');
           SQL.ADD(' from  TabPersiana');
           SQL.ADD(' WHERE Referencia='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;
procedure TObjPersiana.Submit_IsentoICMS_Estado(parametro: string);
begin
        Self.IsentoICMS_Estado:=Parametro;
end;
function TObjPersiana.Get_IsentoICMS_Estado: string;
begin
        Result:=Self.IsentoICMS_Estado;
end;
procedure TObjPersiana.Submit_SubstituicaoICMS_Estado(parametro: string);
begin
        Self.SubstituicaoICMS_Estado:=Parametro;
end;
function TObjPersiana.Get_SubstituicaoICMS_Estado: string;
begin
        Result:=Self.SubstituicaoICMS_Estado;
end;
procedure TObjPersiana.Submit_ValorPauta_Sub_Trib_Estado(parametro: string);
begin
        Self.ValorPauta_Sub_Trib_Estado:=Parametro;
end;
function TObjPersiana.Get_ValorPauta_Sub_Trib_Estado: string;
begin
        Result:=Self.ValorPauta_Sub_Trib_Estado;
end;
procedure TObjPersiana.Submit_Aliquota_ICMS_Estado(parametro: string);
begin
        Self.Aliquota_ICMS_Estado:=Parametro;
end;
function TObjPersiana.Get_Aliquota_ICMS_Estado: string;
begin
        Result:=Self.Aliquota_ICMS_Estado;
end;
procedure TObjPersiana.Submit_Reducao_BC_ICMS_Estado(parametro: string);
begin
        Self.Reducao_BC_ICMS_Estado:=Parametro;
end;
function TObjPersiana.Get_Reducao_BC_ICMS_Estado: string;
begin
        Result:=Self.Reducao_BC_ICMS_Estado;
end;
procedure TObjPersiana.Submit_Aliquota_ICMS_Cupom_Estado(parametro: string);
begin
        Self.Aliquota_ICMS_Cupom_Estado:=Parametro;
end;
function TObjPersiana.Get_Aliquota_ICMS_Cupom_Estado: string;
begin
        Result:=Self.Aliquota_ICMS_Cupom_Estado;
end;
procedure TObjPersiana.Submit_IsentoICMS_ForaEstado(parametro: string);
begin
        Self.IsentoICMS_ForaEstado:=Parametro;
end;
function TObjPersiana.Get_IsentoICMS_ForaEstado: string;
begin
        Result:=Self.IsentoICMS_ForaEstado;
end;
procedure TObjPersiana.Submit_SubstituicaoICMS_ForaEstado(parametro: string);
begin
        Self.SubstituicaoICMS_ForaEstado:=Parametro;
end;
function TObjPersiana.Get_SubstituicaoICMS_ForaEstado: string;
begin
        Result:=Self.SubstituicaoICMS_ForaEstado;
end;
procedure TObjPersiana.Submit_ValorPauta_Sub_Trib_ForaEstado(parametro: string);
begin
        Self.ValorPauta_Sub_Trib_ForaEstado:=Parametro;
end;
function TObjPersiana.Get_ValorPauta_Sub_Trib_ForaEstado: string;
begin
        Result:=Self.ValorPauta_Sub_Trib_ForaEstado;
end;
procedure TObjPersiana.Submit_Aliquota_ICMS_ForaEstado(parametro: string);
begin
        Self.Aliquota_ICMS_ForaEstado:=Parametro;
end;
function TObjPersiana.Get_Aliquota_ICMS_ForaEstado: string;
begin
        Result:=Self.Aliquota_ICMS_ForaEstado;
end;
procedure TObjPersiana.Submit_Reducao_BC_ICMS_ForaEstado(parametro: string);
begin
        Self.Reducao_BC_ICMS_ForaEstado:=Parametro;
end;
function TObjPersiana.Get_Reducao_BC_ICMS_ForaEstado: string;
begin
        Result:=Self.Reducao_BC_ICMS_ForaEstado;
end;
procedure TObjPersiana.Submit_Aliquota_ICMS_Cupom_ForaEstado(parametro: string);
begin
        Self.Aliquota_ICMS_Cupom_ForaEstado:=Parametro;
end;
function TObjPersiana.Get_Aliquota_ICMS_Cupom_ForaEstado: string;
begin
        Result:=Self.Aliquota_ICMS_Cupom_ForaEstado;
end;

procedure TObjPersiana.Submit_percentualagregado(parametro: string);
begin
        Self.percentualagregado:=Parametro;
end;
function TObjPersiana.Get_percentualagregado: string;
begin
        Result:=Self.percentualagregado;
end;


procedure TObjPersiana.Submit_ipi(parametro: string);
begin
        Self.ipi:=Parametro;
end;
function TObjPersiana.Get_ipi: string;
begin
        Result:=Self.ipi;
end;



procedure TObjPersiana.Submit_ClassificacaoFiscal(parametro: string);
begin
        Self.ClassificacaoFiscal:=Parametro;
end;
function TObjPersiana.Get_ClassificacaoFiscal: string;
begin
        Result:=Self.ClassificacaoFiscal;
end;

procedure TObjPersiana.EdtFornecedorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Fornecedor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Fornecedor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Fornecedor.Get_RazaoSocial;
End;
procedure TObjPersiana.EdtFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Fornecedor.Get_Pesquisa,Self.Fornecedor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Fornecedor.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Fornecedor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Fornecedor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjPersiana.EdtSituacaoTributaria_TabelaAExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.SituacaoTributaria_TabelaA.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.SituacaoTributaria_TabelaA.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.SituacaoTributaria_TabelaA.Get_DESCRICAO;
End;
procedure TObjPersiana.EdtSituacaoTributaria_TabelaAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa
            (Self.SituacaoTributaria_TabelaA.Get_Pesquisa,Self.SituacaoTributaria_TabelaA.Get_TituloPesquisa,
            Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SituacaoTributaria_TabelaA.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.SituacaoTributaria_TabelaA.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SituacaoTributaria_TabelaA.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjPersiana.EdtSituacaoTributaria_TabelaBExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.SituacaoTributaria_TabelaB.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.SituacaoTributaria_TabelaB.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.SituacaoTributaria_TabelaB.Get_DESCRICAO  ;
End;
procedure TObjPersiana.EdtSituacaoTributaria_TabelaBKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa
               (Self.SituacaoTributaria_TabelaB.Get_Pesquisa,Self.SituacaoTributaria_TabelaB.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SituacaoTributaria_TabelaB.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.SituacaoTributaria_TabelaB.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SituacaoTributaria_TabelaB.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjPersiana.PrimeiroRegistro: Boolean;
Var MenorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabPersiana') ;
           Open;

           if (FieldByName('MenorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MenorCodigo:=fieldbyname('MenorCodigo').AsString;
           Self.LocalizaCodigo(MenorCodigo);

           Result:=true;
     end;
end;

function TObjPersiana.UltimoRegistro: Boolean;
Var MaiorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabPersiana') ;
           Open;

           if (FieldByName('MaiorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MaiorCodigo:=fieldbyname('MaiorCodigo').AsString;
           Self.LocalizaCodigo(MaiorCodigo);

           Result:=true;
     end;
end;

function TObjPersiana.ProximoRegisto(PCodigo:Integer): Boolean;
Var MaiorValor:Integer;
begin
     Result:=false;

     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabPersiana') ;
           Open;

           MaiorValor:=fieldbyname('MaiorCodigo').AsInteger;
     end;

     if (MaiorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Inc(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MaiorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Inc(PCodigo,1);
     end;

     Result:=true;
end;

function TObjPersiana.RegistoAnterior(PCodigo: Integer): Boolean;
Var MenorValor:Integer;
begin
     Result:=false;
     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabPersiana') ;
           Open;

           MenorValor:=fieldbyname('MenorCodigo').AsInteger;
     end;

     if (MenorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Dec(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MenorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Dec(PCodigo,1);
     end;

     Result:=true;
end;

procedure TObjPERSIANA.Submit_NCM(parametro:string);
begin
    Self.ncm:=parametro;
end;

function TObjPERSIANA.Get_NCM:string;
begin
    result:=self.ncm;
end;

procedure TObjPERSIANA.Submit_Ativo(parametro:string);
begin
    Self.Ativo:=parametro;
end;

function TObjPERSIANA.Get_Ativo:string;
begin
    Result:=Self.Ativo;
end;

function TObjPERSIANA.Get_cest: string;
begin
  result := self.cest;
end;

procedure TObjPERSIANA.Submit_cest(parametro: string);
begin
  self.cest := parametro;
end;

end.



