unit UobjPerfilado_EP;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,Uessencialglobal
,UOBJENTRADAPRODUTOS
,UOBJPerfiladoCOR,UOBJFERRAGEMCOR,UOBJDiversoCOR,UObjCFOP,UobjCSOSN,UobjTABELAA_ST,UobjTABELAB_ST;


Type
   TObjPerfilado_EP=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               Entrada:TOBJENTRADAPRODUTOS;
               PerfiladoCor:TOBJPerfiladoCOR;
                CFOP:TObjCFOP;
                CSOSN:TObjCSOSN;
                STA:TObjTABELAA_ST;
                STB:TObjTABELAB_ST;
//CODIFICA VARIAVEIS PUBLICAS



                Constructor Create(Owner:TComponent);
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Quantidade(parametro: string);
                Function Get_Quantidade: string;
                Procedure Submit_Valor(parametro: string);
                Function Get_Valor: string;

                Procedure Submit_OrdemInsercao(Parametro:string);
                Function Get_OrdemInsercao:string;
                Function Get_ValorFinal:string;

                procedure EdtEntradaExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtEntradaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtPerfiladoCorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtPerfiladoCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure RetornaQuantidade_e_Soma(PEntrada: string; var Pquantidade,Psoma: Currency);

                function Get_CreditoCOFINS: string;
                function Get_CreditoICMS: string;
                function Get_CreditoIpi: string;
                function Get_IPIpago: string;
                function Get_CreditoPIS: string;
                procedure Submit_CreditoCOFINS(parametro: string);
                procedure Submit_CreditoICMS(parametro: string);
                procedure Submit_CreditoIpi(parametro: string);
                procedure Submit_IPIpago(parametro: string);
                procedure Submit_CreditoPIS(parametro: string);
                procedure Submit_Desconto(parametro:string);
                procedure Submit_Unidade(parametro:String);
                function Get_Desconto:string;
                Function Get_Unidade:string;

                procedure Submit_BCIcms(parametro:string);
                Function Get_BCIcms:string;
                procedure Submit_ValorIcms(parametro:string);
                Function Get_ValorIcms:string;
                procedure Submit_ValorFrete(parametro:string);
                Function Get_ValorFrete:string;
                Procedure Submit_ValorIpi(parametro:string);
                function Get_ValorIPI:string;
                Procedure Submit_RedBaseIcms(parametro:string);
                Function Get_RedBaseIcms:string;
                procedure Submit_UnidadeConversao(parametro:string);
                Function Get_UnidadeConversao:string;
                procedure Submit_QuantidadeConversao(parametro:string);
                function Get_QuantidadeConversao:string;

                procedure submit_pcofins(parametro:string);
                procedure submit_ppis(parametro:string);
                function get_ppis():string;
                function get_pcofins():string;

                function get_valoroutros:string;
                procedure submit_valoroutros(parametro:string);

                function get_aliquotast:string;
                procedure submit_aliquotast(parametro:string);

                function get_bcicmsst:string;
                procedure submit_bcicmsst(parametro:string);

                function get_valoricmsst:string;
                procedure submit_valoricmsst(parametro:string);

                function get_cstpis:string;
                procedure submit_cstpis(parametro:string);

                function get_cstcofins:string;
                procedure submit_cstcofins(parametro:string);

                function get_cstipi:string;
                procedure submit_cstipi(parametro:string);

                function get_bcipi:string;
                procedure submit_bcipi(parametro:string);


//CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               Owner:TComponent;
               
               CODIGO:string;
               Quantidade:string;
               Valor:string;
               OrdemInsercao:string;
               ValorFinal:string;
               Desconto:string;
               Unidade:string;

                 BCICMS:string;
                 VALORICMS:string;
                 VALORFRETE:string;
                 VALORIPI:string;
                 REDBASEICMS:string;

                CreditoIpi:string;
                IPIpago:string;
               CreditoICMS:string;
               CreditoPIS:string;
               CreditoCOFINS:string;
               quantidadeconversao:string;
               unidadeconversao:string;
               ppis,pcofins:string;
               valoroutros:string;

               aliquotast,bcicmsst,valoricmsst:string;
               cstpis,cstcofins,cstipi:string;
               bcipi:string;



               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                function Get_NovaOrdemInsercao: string;
                Function LancaEstoque(pquantidadeanterior:string):boolean;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
  UobjCOR, UMenuRelatorios;


Function  TObjPerfilado_EP.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.OrdemInsercao:=fieldbyname('OrdemInsercao').asstring;
        Self.ValorFinal:=fieldbyname('ValorFinal').asstring;
        
        If(FieldByName('Entrada').asstring<>'')
        Then Begin
                 If (Self.Entrada.LocalizaCodigo(FieldByName('Entrada').asstring)=False)
                 Then Begin
                          Messagedlg('Entrada N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Entrada.TabelaparaObjeto;
        End;
        If(FieldByName('PerfiladoCor').asstring<>'')
        Then Begin
                 If (Self.PerfiladoCor.LocalizaCodigo(FieldByName('PerfiladoCor').asstring)=False)
                 Then Begin
                          Messagedlg('Perfilado Cor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PerfiladoCor.TabelaparaObjeto;
        End;
        if(FieldByName('CFOP').AsString<>'') then
        begin
                 if (Self.CFOP.LocalizaCodigo(fieldbyname('cfop').AsString)=False) then
                 begin
                          MensagemAviso('CFOP n�o encontrado');
                          self.ZerarTabela;
                          Result:=false;
                          Exit;
                 end
                 else self.CFOP.TabelaparaObjeto;
        end;
        if(FieldByName('csosn').AsString<>'') then
        begin
                  if(Self.CSOSN.LocalizaCodigo(fieldbyname('csosn').AsString)=False) then
                  begin
                          MensagemAviso('CSOSN n�o encontrado');
                          self.ZerarTabela;
                          Result:=False;
                          Exit;
                  end
                  else self.CSOSN.TabelaparaObjeto;
        end;
        if(FieldByName('cstA').AsString<>'') then
        begin
                  if(self.STA.LocalizaCodigo(fieldbyname('cstA').AsString)=False)then
                  begin
                          MensagemAviso('Codigo Tabela A (cstA) n�o encontrado');
                          self.ZerarTabela;
                          Result:=False;
                          Exit;
                  end
                  else Self.STA.TabelaparaObjeto;
        end;
        if(FieldByName('cstB').AsString<>'')then
        begin
                  if(self.STB.LocalizaCodigo(fieldbyname('cstB').AsString)=False) then
                  begin
                          MensagemAviso('Codigo Tabela A (cstA) n�o encontrado');
                          self.ZerarTabela;
                          Result:=False;
                          Exit;
                  end
                  else Self.STB.TabelaparaObjeto;
        end;
        
        Self.Quantidade:=fieldbyname('Quantidade').asstring;
        Self.Valor:=fieldbyname('Valor').asstring;

        Self.CreditoIpi:=fieldbyname('creditoipi').asstring;
        Self.IPIpago:=fieldbyname('IPIpago').asstring;
        Self.CreditoIcms:=fieldbyname('creditoicms').asstring;
        Self.CreditoPIS:=fieldbyname('creditopis').asstring;
        Self.CreditoCofins:=fieldbyname('creditocofins').asstring;
        Self.Desconto:=fieldbyname('desconto').AsString;
        Self.Unidade:=fieldbyname('unidade').AsString;

        Self.BCICMS:=fieldbyname('bcicms').AsString;
        self.VALORICMS:=fieldbyname('valoricms').AsString;
        Self.VALORFRETE:=fieldbyname('valorfrete').AsString;
        Self.VALORIPI:=fieldbyname('valoripi').AsString;
        Self.REDBASEICMS:=fieldbyname('redbaseicms').AsString;
        self.unidadeconversao:=fieldbyname('unidadeconversao').AsString;
        Self.quantidadeconversao:=fieldbyname('quantidadeconversao').AsString;

        Self.ppis:=fieldbyname('ppis').AsString;
        Self.pcofins:=fieldbyname('pcofins').AsString;
        Self.valoroutros:=fieldbyname('valoroutros').AsString;

        Self.aliquotast:=fieldbyname('aliquotast').AsString;
        Self.bcicmsst:=fieldbyname('bcicmsst').AsString;
        Self.valoricmsst:=fieldbyname('valoricmsst').AsString;

        Self.cstpis:=fieldbyname('cstpis').AsString;
        Self.cstcofins:=fieldbyname('cstcofins').AsString;
        Self.cstipi:=fieldbyname('cstipi').AsString;
        Self.bcipi:=fieldbyname('bcipi').AsString;

        

        result:=True;
     End;
end;


Procedure TObjPerfilado_EP.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Entrada').asstring:=Self.Entrada.GET_CODIGO;
        ParamByName('PerfiladoCor').asstring:=Self.PerfiladoCor.GET_CODIGO;
        ParamByName('Quantidade').asstring:=virgulaparaponto(Self.Quantidade);
        ParamByName('Valor').asstring:=virgulaparaponto(Self.Valor);
        ParamByName('OrdemInsercao').asstring:=Self.OrdemInsercao;

        ParamByName('creditoipi').asstring:=virgulaparaponto(Self.CreditoIpi);
        ParamByName('IPIpago').asstring:=virgulaparaponto(Self.IPIpago);
        ParamByName('creditoicms').asstring:=virgulaparaponto(Self.CreditoIcms);
        ParamByName('creditopis').asstring:=virgulaparaponto(Self.CreditoPIS);
        ParamByName('creditocofins').asstring:=virgulaparaponto(Self.CreditoCofins);
        ParamByName('Desconto').AsString:=Self.Desconto;
        ParamByName('unidade').AsString:=self.Unidade;
        ParamByName('CFOP').AsString:=CFOP.Get_CODIGO;
        ParamByName('csosn').AsString:=CSOSN.Get_codigo;
        ParamByName('csta').AsString:=STA.Get_CODIGO;
        ParamByName('cstb').AsString:=STB.Get_CODIGO;

        ParamByName('bcicms').AsString:=virgulaparaponto(self.BCICMS);
        ParamByName('valoricms').AsString:=virgulaparaponto(self.VALORICMS);
        ParamByName('valorfrete').AsString:=virgulaparaponto(self.VALORFRETE);
        ParamByName('valoripi').AsString:=virgulaparaponto(self.VALORIPI);
        ParamByName('redbaseicms').AsString:=virgulaparaponto(self.REDBASEICMS);
        ParamByName('UnidadeConversao').AsString:=Self.unidadeconversao;
        ParamByName('quantidadeconversao').AsString:=virgulaparaponto(self.quantidadeconversao);

        ParamByName('ppis').AsString:=virgulaparaponto(self.ppis);
        ParamByName('pcofins').AsString:=virgulaparaponto(self.pcofins);
        ParamByName('valoroutros').AsString:=virgulaparaponto(self.valoroutros);

        ParamByName('aliquotast').AsString:=virgulaparaponto(self.aliquotast);
        ParamByName('bcicmsst').AsString:=virgulaparaponto(self.bcicmsst);
        ParamByName('valoricmsst').AsString:=virgulaparaponto(self.valoricmsst);

        ParamByName('cstpis').AsString:=(self.cstpis);
        ParamByName('cstcofins').AsString:=(self.cstcofins);
        ParamByName('cstipi').AsString:=(self.cstipi);
        ParamByName('bcipi').AsString:=virgulaparaponto(self.bcipi);

  End;
End;

//***********************************************************************

function TObjPerfilado_EP.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
var
pquantidadeanterior:string;
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
             //Guardo a quantidade antes da alteracao para situacoes
             //de alteracao de registro que nao existiam
             //no registro de estoque, assim preciso deste valor
             //para diminuir antes de lancar o novo registro de estoque
             if(Self.Objquery.fieldbyname('quantidadeconversao').asstring ='')
             then PquantidadeAnterior:=Self.Objquery.fieldbyname('quantidade').asstring
             else PquantidadeAnterior:=Self.Objquery.fieldbyname('quantidadeconversao').asstring
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;

              if (Self.OrdemInsercao='0')
              Then Self.OrdemInsercao:=Self.Get_NovaOrdemInsercao;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 if (Self.LancaEstoque(pquantidadeanterior)=False)
 Then Begin
           If ComCommit=True
           Then FDataModulo.IBTransaction.RollbackRetaining;
           exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPerfilado_EP.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Entrada.ZerarTabela;
        PerfiladoCor.ZerarTabela;
        Quantidade:='';
        Valor:='';
        OrdemInsercao:='';
        ValorFinal:='';

        Self.CreditoIpi:='';
        Self.IPIpago:='';
        Self.CreditoIcms:='';
        Self.CreditoPIS:='';
        Self.CreditoCofins:='';
        CFOP.ZerarTabela;
        CSOSN.ZerarTabela;
        STA.ZerarTabela;
        STB.ZerarTabela;

        Desconto:='';
        unidade:='';

         BCICMS:='';
        VALORICMS:='';
        VALORFRETE:='';
        VALORIPI:='';
        REDBASEICMS:='';
        quantidadeconversao:='';
        unidadeconversao:='';

        ppis:='';
        pcofins:='';
        valoroutros:='';
        aliquotast:='';
        bcicmsst:='';
        valoricmsst:='';
        cstpis:='';
        cstcofins:='';
        cstipi:='';
        bcipi:='';




     End;
end;

Function TObjPerfilado_EP.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Entrada.Get_CODIGO='')
      Then Mensagem:=mensagem+'/Entrada';
      If (PerfiladoCor.Get_Codigo='')
      Then Mensagem:=mensagem+'/Perfilado Cor';
      If (Quantidade='')
      Then Mensagem:=mensagem+'/Quantidade';
      If (Valor='')
      Then Mensagem:=mensagem+'/Valor';
      if (OrdemInsercao='')
      Then Mensagem:=mensagem+'/Ordem Inser��o';

      if (Self.CreditoIpi='')
      Then Self.CreditoIpi:='0';

      if (Self.IPIpago='')
      Then Self.IPIpago:='0';

      if (Self.CreditoIcms='')
      Then Self.CreditoIcms:='0';

      if (Self.CreditoPIS='')
      Then Self.CreditoPIS:='0';
      
      if (Self.CreditoCofins='')
      Then Self.CreditoCOFINS:='0';
      
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPerfilado_EP.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Entrada.LocalizaCodigo(Self.Entrada.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Entrada n�o Encontrado!'
      else Self.Entrada.TabelaparaObjeto;
      If (Self.PerfiladoCor.LocalizaCodigo(Self.PerfiladoCor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Perfilado Cor n�o Encontrado!'
      else self.PerfiladoCor.TabelaparaObjeto;

      if not CFOP.LocalizaCodigo(self.CFOP.Get_CODIGO) then
        mensagem := mensagem + '/ CFOP n�o encontrado';;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjPerfilado_EP.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.Entrada.Get_Codigo<>'')
        Then Strtoint(Self.Entrada.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Entrada';
     End;
     try
        If (Self.PerfiladoCor.Get_Codigo<>'')
        Then Strtoint(Self.PerfiladoCor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Perfilado Cor';
     End;
     try
        Strtofloat(Self.Quantidade);
     Except
           Mensagem:=mensagem+'/Quantidade';
     End;
     try
        Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;

     try
        Strtoint(Self.OrdemInsercao);
     Except
           Mensagem:=Mensagem+'/Ordem de Inser��o';
     End;

     
     Try
         strtofloat(Self.CreditoIpi);
     Except
           Mensagem:=Mensagem+'/Cr�dito de IPI';
     End;

     Try
         strtofloat(Self.IPIpago);
     Except
           Mensagem:=Mensagem+'/IPI Pago';
     End;

     Try
         strtofloat(Self.CreditoICMS);
     Except
           Mensagem:=Mensagem+'/Cr�dito de ICMS';
     End;

     Try
         strtofloat(Self.CreditoPIS);
     Except
           Mensagem:=Mensagem+'/Cr�dito de PI';
     End;

     Try
         strtofloat(Self.CreditoCOFINS);
     Except
           Mensagem:=Mensagem+'/Cr�dito de Cofins';
     End;
      

//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPerfilado_EP.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPerfilado_EP.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPerfilado_EP.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro Perfilado_EP vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Entrada,PerfiladoCor,Quantidade,Valor,ValorFinal,OrdemInsercao,IpiPago,CreditoIpi,CreditoICMS,CreditoPIS,CreditoCOFINS');
           SQL.ADD(' ,cfop,csosn,csta,cstb,unidade,desconto,bcicms,valoricms,valorfrete,valoripi,redbaseicms,quantidadeconversao,unidadeconversao,ppis,pcofins,valoroutros,aliquotast,bcicmsst,valoricmsst,cstpis,cstcofins,cstipi,bcipi from  TABPerfilado_EP');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPerfilado_EP.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPerfilado_EP.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin

      Try
        result:=false;

        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                  Self.TabelaparaObjeto;

                  OBJESTOQUEGLOBAL.ZerarTabela;

                  if (OBJESTOQUEGLOBAL.LocalizaporCampo('Perfilado_EP',PCODIGO)=False)
                  Then Begin
                            //em caso de exclusao de um registro
                            //que nao exista no estoque
                            //tenho que criar um registro negativo
                            OBJESTOQUEGLOBAL.Submit_CODIGO('0');
                            OBJESTOQUEGLOBAL.Status:=dsInsert;
                            OBJESTOQUEGLOBAL.Submit_PerfiladoCOR(Self.PerfiladoCor.get_codigo);
                            OBJESTOQUEGLOBAL.Submit_QUANTIDADE(floattostr(strtofloat(Self.Quantidade)*-1));
                            OBJESTOQUEGLOBAL.Submit_OBSERVACAO('EXCLUSAO ENTRADA '+sELF.Entrada.Get_CODIGO+' MATERIAL_EP '+Self.CODIGO);
                            OBJESTOQUEGLOBAL.Submit_DATA(formatdatetime ('dd/mm/yyyy',now));
                            if (OBJESTOQUEGLOBAL.Salvar(False)=False)
                            Then Begin
                                      MensagemErro('Erro ao tentar criar um registro negativo de estoque na exclus�o');
                                      exit;
                            End;
                  End
                  Else Begin
                            OBJESTOQUEGLOBAL.TabelaparaObjeto;
                            if (OBJESTOQUEGLOBAL.Exclui(OBJESTOQUEGLOBAL.get_codigo,False)=False)
                            Then Begin
                                      mensagemerro('Erro ao tentar excluir o registro do estoque');
                                      exit;
                            End;
                  End;

                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 result:=True;

                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
        End
     Except
           on e:exception do
           Begin
                mensagemerro('Erro ao tentar excluir '+e.message);
           End;
     End;
end;


constructor TObjPerfilado_EP.create(Owner:TComponent);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin

        Self.Owner := Owner;
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Entrada:=TOBJENTRADAPRODUTOS.create;
        Self.PerfiladoCor:=TOBJPerfiladoCOR.create;

        self.CFOP:=TObjCFOP.Create;
        Self.CSOSN:=TObjCSOSN.Create(Self.Owner);
        self.STA:=TObjTABELAA_ST.Create;
        self.STB:=TObjTABELAB_ST.Create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABPerfilado_EP(CODIGO,Entrada,PerfiladoCor');
                InsertSQL.add(' ,Quantidade,Valor,OrdemInsercao,IpiPago,CreditoIpi,CreditoICMS,CreditoPIS,CreditoCOFINS,cfop,csosn,csta,cstb,unidade,desconto');
                InsertSql.Add(',bcicms,valoricms,valorfrete,valoripi,redbaseicms,quantidadeconversao,unidadeconversao,ppis,pcofins,valoroutros,aliquotast,bcicmsst,valoricmsst,cstpis,cstcofins,cstipi,bcipi)');
                InsertSQL.add('values (:CODIGO,:Entrada,:PerfiladoCor,:Quantidade,:Valor,:OrdemInsercao,:IpiPago,:CreditoIpi,:CreditoICMS,:CreditoPIS,:CreditoCOFINS');
                InsertSQL.add(',:cfop,:csosn,:csta,:cstb,:unidade,:desconto,:bcicms,:valoricms,:valorfrete,:valoripi,:redbaseicms,:quantidadeconversao,:unidadeconversao,:ppis,:pcofins,:valoroutros,:aliquotast,:bcicmsst,:valoricmsst,:cstpis,:cstcofins,:cstipi,:bcipi )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABPerfilado_EP set CODIGO=:CODIGO,Entrada=:Entrada');
                ModifySQL.add(',PerfiladoCor=:PerfiladoCor,Quantidade=:Quantidade,Valor=:Valor');
                ModifySQL.add(',OrdemInsercao=:OrdemInsercao');
                ModifySQL.add(',IpiPago=:IpiPago,CreditoIpi=:CreditoIpi,CreditoICMS=:CreditoICMS,CreditoPIS=:CreditoPIS,CreditoCOFINS=:CreditoCOFINS');
                ModifySQl.Add(',cfop=:cfop,csosn=:csosn,csta=:csta,cstb=:cstb,unidade=:unidade,desconto=:desconto');
                ModifySQL.add(',bcicms=:bcicms,valoricms=:valoricms,valorfrete=:valorfrete,valoripi=:valoripi,redbaseicms=:redbaseicms');
                ModifySQl.Add(',quantidadeconversao=:quantidadeconversao,unidadeconversao=:unidadeconversao,ppis=:ppis,pcofins=:pcofins,valoroutros=:valoroutros');
                ModifySQl.Add(',aliquotast=:aliquotast,bcicmsst=:bcicmsst,valoricmsst=:valoricmsst,cstpis=:cstpis,cstcofins=:cstcofins,cstipi=:cstipi,bcipi=:bcipi where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABPerfilado_EP where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPerfilado_EP.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPerfilado_EP.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPerfilado_EP');
     Result:=Self.ParametroPesquisa;
end;

function TObjPerfilado_EP.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Perfilado_EP ';
end;


function TObjPerfilado_EP.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPerfilado_EP,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENPerfilado_EP,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPerfilado_EP.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Entrada.FREE;
    Self.PerfiladoCor.FREE;
    self.CFOP.Free;
    Self.CSOSN.Free;
    self.STA.Free;
    self.STB.Free;
//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPerfilado_EP.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPerfilado_EP.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPerfilado_EP.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjPerfilado_EP.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjPerfilado_EP.Submit_Quantidade(parametro: string);
begin
        Self.Quantidade:=Parametro;
end;
function TObjPerfilado_EP.Get_Quantidade: string;
begin
        Result:=Self.Quantidade;
end;
procedure TObjPerfilado_EP.Submit_Valor(parametro: string);
begin
        Self.Valor:=Parametro;
end;
function TObjPerfilado_EP.Get_Valor: string;
begin
        Result:=Self.Valor;
end;
//CODIFICA GETSESUBMITS


procedure TObjPerfilado_EP.EdtEntradaExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Entrada.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Entrada.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.Entrada.GET_NOME;
End;
procedure TObjPerfilado_EP.EdtEntradaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Entrada.Get_Pesquisa,Self.Entrada.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Entrada.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Entrada.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Entrada.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjPerfilado_EP.EdtPerfiladoCorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PerfiladoCor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PerfiladoCor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.PerfiladoCor.Perfilado.Get_Descricao+'-'+Self.PerfiladoCor.Cor.Get_Descricao;
End;
procedure TObjPerfilado_EP.EdtPerfiladoCorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PerfiladoCor.Get_Pesquisa,Self.PerfiladoCor.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PerfiladoCor.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.PerfiladoCor.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PerfiladoCor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjPerfilado_EP.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPerfilado_EP';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:Begin
            End;
          End;
     end;

end;



function TObjPerfilado_EP.Get_OrdemInsercao: string;
begin
     Result:=Self.OrdemInsercao;
end;

function TObjPerfilado_EP.Get_ValorFinal: string;
begin
     Result:=Self.ValorFinal;
end;

procedure TObjPerfilado_EP.Submit_OrdemInsercao(Parametro: string);
begin
     Self.OrdemInsercao:=Parametro;
end;
function TObjPerfilado_EP.Get_NovaOrdemInsercao: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENORDEMINSERCAO_EP,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo de Inser��o para a Ferragem',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;

procedure TObjperfilado_EP.RetornaQuantidade_e_Soma(PEntrada: string;
  var Pquantidade, Psoma: Currency);
begin
     With Self.Objquery do
     begin
          close;
          sql.clear;
          sql.add('Select entrada,sum(quantidade) as SOMAQUANTIDADE,');
          sql.add('SUM(VALORFINAL) as SOMAVALORFINAL');
          sql.add('from tabperfilado_ep');
          sql.add('where Entrada='+PEntrada);
          sql.add('group by entrada');
          open;
          Pquantidade:=fieldbyname('somaquantidade').asfloat;
          Psoma:=fieldbyname('somavalorfinal').asfloat;
          close;
     End;
end;


function TObjperfilado_EP.Get_CreditoCOFINS: string;
begin
     Result:=Self.CreditoCOFINS;
end;

function TObjperfilado_EP.Get_CreditoICMS: string;
begin
     Result:=Self.CreditoICMS;
end;

function TObjperfilado_EP.Get_CreditoIpi: string;
begin
     Result:=Self.CreditoIpi;
end;

function TObjperfilado_EP.Get_IPIpago: string;
begin
     Result:=Self.IPIpago;
end;


function TObjperfilado_EP.Get_CreditoPIS: string;
begin
     Result:=Self.CreditoPIS;
end;

procedure TObjperfilado_EP.Submit_CreditoCOFINS(parametro: string);
begin
     Self.CreditoCOFINS:=parametro;
end;

procedure TObjperfilado_EP.Submit_CreditoICMS(parametro: string);
begin
    Self.CreditoICMS:=parametro;
end;

procedure TObjperfilado_EP.Submit_CreditoIpi(parametro: string);
begin
     Self.CreditoIpi:=parametro;
end;

procedure TObjperfilado_EP.Submit_IPIpago(parametro: string);
begin
     Self.IPIpago:=parametro;
end;


procedure TObjperfilado_EP.Submit_CreditoPIS(parametro: string);
begin
     Self.CreditoPIS:=parametro;
end;

procedure TObjperfilado_EP.Submit_Desconto(parametro:string);
begin
    self.Desconto:=parametro;
end;

procedure TObjperfilado_EP.Submit_Unidade(parametro:string);
begin
    Self.Unidade:=parametro;
end;

function TObjperfilado_EP.Get_Desconto:string;
begin
    result:=Self.Desconto;
end;

function TObjperfilado_EP.Get_Unidade:string;
begin
    Result:=Self.Unidade;
end;

procedure  TObjperfilado_EP.Submit_BCIcms(parametro:string);
begin
    Self.BCICMS:=parametro;
end;

function  TObjperfilado_EP.Get_BCIcms:string;
begin
    result:=self.BCICMS;
end;

procedure  TObjperfilado_EP.Submit_ValorIcms(parametro:string);
begin
    Self.VALORICMS:=parametro;
end;

function  TObjperfilado_EP.Get_ValorIcms:string;
begin
    Result:=self.VALORICMS;
end;

procedure  TObjperfilado_EP.Submit_ValorFrete(parametro:string);
begin
    Self.VALORFRETE:=parametro;
end;

function  TObjperfilado_EP.Get_ValorFrete:string;
begin
    result:=self.VALORFRETE;
end;

procedure  TObjperfilado_EP.Submit_ValorIpi(parametro:string);
begin
    Self.VALORIPI:=parametro;
end;

function  TObjperfilado_EP.Get_ValorIPI:string;
begin
    result:=Self.VALORIPI;
end;

procedure TObjperfilado_EP.Submit_RedBaseIcms(parametro:String);
begin
     Self.REDBASEICMS:=parametro;
end;

function TObjperfilado_EP.Get_RedBaseIcms:string;
begin
    Result:=Self.REDBASEICMS;
end;

function TObjperfilado_EP.LancaEstoque(pquantidadeanterior:string): boolean;
begin
     (*Este procedimento � chamado no Salvar, ou seja, edi��o ou inser��o de produtos no estoque na entrada*)
     result:=false;

     OBJESTOQUEGLOBAL.ZerarTabela;

     if (Self.Status=dsedit)
     then Begin
               if (OBJESTOQUEGLOBAL.LocalizaporCampo('Perfilado_EP',Self.codigo)=False)
               Then Begin
                         (*n�o  encontrou o registro para edi��o
                         pode ser que seja um registro anterior a implantacao do estoque
                         //sazional

                         Caso seja, o estoque foi lan�ado no valor do dia da implantacao
                         entao deveria aumentar o valor anterior e diminuir o atual

                         Exemplo: No dia da implantacao tinha 100 no estoque

                         A entrada que esta sendo alterada tinha acrescentado
                         20 neste estoque de 100,00

                         Entao preciso

                         Diminuir 20,00  e aumentar o valor atual
                         por exemplo 30,00

                         Sendo assim o estoque passara para 110,00

                         Porem estes 20,00 nao podem estar ligado ao registro
                         pois na proxima alteracao o registro 30,00 que devera
                         ser alterado

                         *)

                         MensagemAviso('Este Material que est� sendo alterado foi adicionado antes do controle de estoque sazional, '+
                                       'por este motivo ser� gerado um registro negativo do estoque anterior');

                         OBJESTOQUEGLOBAL.ZerarTabela;
                         OBJESTOQUEGLOBAL.Status:=dsInsert;
                         OBJESTOQUEGLOBAL.submit_data(Self.entrada.get_data);
                         OBJESTOQUEGLOBAL.Submit_CODIGO('0');
                         OBJESTOQUEGLOBAL.Submit_QUANTIDADE(floattostr(strtofloat(pquantidadeanterior)*-1));
                         OBJESTOQUEGLOBAL.Submit_OBSERVACAO('ENTRADA ANTIGA ALTERADA - ENT '+Self.Entrada.Get_CODIGO+' COD MAT_EP '+Self.CODIGO);
                         OBJESTOQUEGLOBAL.Submit_PERFILADOCOR(Self.PERFILADOCor.Get_Codigo);
                         if (OBJESTOQUEGLOBAL.Salvar(False)=False)
                         then Begin
                                   MensagemErro('Erro ao gerar um registro negativo ref. ao material');
                                   exit;
                         End;
                         //mesmo estando em edicao, se nao foi encontrado
                         //um registro, ele gera um negativo com o valor anterior
                         //e gera um novo registro para o material_ep
                         OBJESTOQUEGLOBAL.ZerarTabela;
                         OBJESTOQUEGLOBAL.Status:=dsinsert;
                         OBJESTOQUEGLOBAL.submit_codigo('0');
               End
               Else Begin
                         //encontrou entra em edicao
                         OBJESTOQUEGLOBAL.TabelaparaObjeto;
                         OBJESTOQUEGLOBAL.Status:=dsedit;
               End;
     End
     Else Begin
               //novo
               
               OBJESTOQUEGLOBAL.ZerarTabela;
               OBJESTOQUEGLOBAL.Status:=dsInsert;
               OBJESTOQUEGLOBAL.Submit_CODIGO('0');
     End;

     OBJESTOQUEGLOBAL.Submit_DATA(Self.Entrada.Get_DATA);
     OBJESTOQUEGLOBAL.Submit_OBSERVACAO('ENTRADA '+Self.Entrada.Get_CODIGO);
     
     if( StrToCurrDef( Self.quantidadeconversao, 0 ) = 0)
     then OBJESTOQUEGLOBAL.Submit_QUANTIDADE(Self.Quantidade)
     else OBJESTOQUEGLOBAL.Submit_QUANTIDADE(Self.quantidadeconversao);

     OBJESTOQUEGLOBAL.Submit_PERFILADOCOR(Self.PERFILADOcor.Get_Codigo);
     OBJESTOQUEGLOBAL.Submit_PERFILADO_EP(Self.CODIGO);

     if (OBJESTOQUEGLOBAL.Salvar(False)=False)
     Then Begin
               MensagemErro('Erro na tentativa de salvar o registro de estoque do DIVERSO');
               exit;
     End;

     result:=True;
end;

procedure TObjPerfilado_EP.Submit_UnidadeConversao(parametro:string);
begin
    Self.unidadeconversao:=parametro;
end;

procedure TObjPerfilado_EP.Submit_QuantidadeConversao(parametro:string);
begin
    self.quantidadeconversao:=parametro;
end;

function TObjPerfilado_EP.Get_UnidadeConversao:string;
begin
    Result:=unidadeconversao;
end;

function TObjPerfilado_EP.Get_QuantidadeConversao:string;
begin
    Result:=quantidadeconversao;
end;


function TObjPerfilado_EP.get_pcofins: string;
begin
  Result := self.pcofins;
end;

function TObjPerfilado_EP.get_ppis: string;
begin
  Result := self.ppis;
end;

procedure TObjPerfilado_EP.submit_pcofins(parametro: string);
begin
  self.pcofins := default(parametro);
end;

procedure TObjPerfilado_EP.submit_ppis(parametro: string);
begin
  Self.ppis := default(parametro);
end;

function TObjPerfilado_EP.get_valoroutros: string;
begin
  Result := self.valoroutros;
end;

procedure TObjPerfilado_EP.submit_valoroutros(parametro: string);
begin
  self.valoroutros := parametro;
end;

function TObjPerfilado_EP.get_aliquotast: string;
begin
  Result := aliquotast;
end;

function TObjPerfilado_EP.get_bcicmsst: string;
begin
  Result := bcicmsst;
end;

function TObjPerfilado_EP.get_valoricmsst: string;
begin
  result := valoricmsst;
end;

procedure TObjPerfilado_EP.submit_aliquotast(parametro: string);
begin
  self.aliquotast := parametro;
end;

procedure TObjPerfilado_EP.submit_bcicmsst(parametro: string);
begin
  self.bcicmsst := parametro;
end;

procedure TObjPerfilado_EP.submit_valoricmsst(parametro: string);
begin
  self.valoricmsst := parametro;
end;

procedure TObjPerfilado_EP.submit_cstipi(parametro: string);
begin
  cstipi := parametro;
end;

function TObjPerfilado_EP.get_cstcofins: string;
begin
  result := cstcofins;
end;

function TObjPerfilado_EP.get_cstipi: string;
begin
  Result := cstipi;
end;

function TObjPerfilado_EP.get_cstpis: string;
begin
  result := cstpis;
end;

procedure TObjPerfilado_EP.submit_cstcofins(parametro: string);
begin
  cstcofins := parametro;
end;

procedure TObjPerfilado_EP.submit_cstpis(parametro: string);
begin
  cstpis := parametro;
end;

function TObjPerfilado_EP.get_bcipi: string;
begin
  result := bcipi;
end;

procedure TObjPerfilado_EP.submit_bcipi(parametro: string);
begin
  self.bcipi := parametro;
end;

end.



