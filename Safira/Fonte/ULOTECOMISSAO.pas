unit ULOTECOMISSAO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjComissaovendedores;

type
  TFLOTECOMISSAO = class(TForm)
    lb1: TLabel;
    cbbcomboconcluido: TComboBox;
    edtData: TMaskEdit;
    lbLbData: TLabel;
    edtHistorico: TEdit;
    lbLbHistorico: TLabel;
    pnlbotes: TPanel;
    lbCodigo: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    lbnomeformulario: TLabel;
    lb2: TLabel;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
         ObjComissaoVendedores:TObjComissaoVendedores;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLOTECOMISSAO: TFLOTECOMISSAO;


implementation

uses UessencialGlobal, Upesquisa, UDataModulo, UescolheImagemBotao,
  Uprincipal;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFLOTECOMISSAO.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjComissaoVendedores.Lotepagamento do
    Begin
        Submit_CODIGO(lbCodigo.Caption);
        Submit_Data(edtData.text);
        Submit_Historico(edtHistorico.text);
        Submit_concluido(Submit_ComboBox(cbbcomboconcluido));
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFLOTECOMISSAO.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjComissaoVendedores.Lotepagamento do
     Begin
        lbCodigo.Caption:=Get_CODIGO;
        EdtData.text:=Get_Data;
        EdtHistorico.text:=Get_Historico;

        if (get_concluido='S')
        then cbbcomboconcluido.itemindex:=1
        else cbbcomboconcluido.itemindex:=0;

//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFLOTECOMISSAO.TabelaParaControles: Boolean;
begin
     If (Self.ObjComissaoVendedores.Lotepagamento.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFLOTECOMISSAO.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);


     Try
        Self.ObjComissaoVendedores:=TObjComissaoVendedores.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;


     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE LOTE DE COMISS�O')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);


end;

procedure TFLOTECOMISSAO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjComissaoVendedores=Nil)
     Then exit;

If (Self.ObjComissaoVendedores.Lotepagamento.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjComissaoVendedores.free;
end;

procedure TFLOTECOMISSAO.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFLOTECOMISSAO.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbCodigo.Caption:='0';
     //edtcodigo.text:=Self.ObjComissaoVendedores.Lotepagamento.Get_novocodigo;

     cbbcomboconcluido.enabled:=False;

     
     btsalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjComissaoVendedores.Lotepagamento.status:=dsInsert;

     btnovo.Visible:=false;
     btalterar.Visible:=False;
     btexcluir.Visible:=false;
     btrelatorio.Visible:=false;
     btopcoes.Visible:=false;
     btsair.Visible:=False;
     btpesquisar.visible:=False;

     edtData.setfocus;

end;


procedure TFLOTECOMISSAO.btalterarClick(Sender: TObject);
begin
    If (Self.ObjComissaoVendedores.Lotepagamento.Status=dsinactive) and (lbCodigo.Caption<>'')
    Then Begin
                if (cbbcomboconcluido.itemindex=1)
                Then exit;

                habilita_campos(Self);

                cbbcomboconcluido.enabled:=False;
                Self.ObjComissaoVendedores.Lotepagamento.Status:=dsEdit;

                ///desab_botoes(Self);

                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtData.setfocus;

                btnovo.Visible:=false;
     btalterar.Visible:=False;
     btexcluir.Visible:=false;
     btrelatorio.Visible:=false;
     btopcoes.Visible:=false;
     btsair.Visible:=False;
     btpesquisar.visible:=False;
                
          End;


end;

procedure TFLOTECOMISSAO.btgravarClick(Sender: TObject);
begin

     If Self.ObjComissaoVendedores.Lotepagamento.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjComissaoVendedores.Lotepagamento.salvar(true)=False)
     Then exit;

     lbCodigo.Caption:=Self.ObjComissaoVendedores.Lotepagamento.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorio.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFLOTECOMISSAO.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjComissaoVendedores.Lotepagamento.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     
     if (cbbcomboconcluido.itemindex=1)
     Then exit;

     If (Self.ObjComissaoVendedores.Lotepagamento.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjComissaoVendedores.Lotepagamento.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFLOTECOMISSAO.btcancelarClick(Sender: TObject);
begin
     Self.ObjComissaoVendedores.Lotepagamento.cancelar;
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorio.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFLOTECOMISSAO.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFLOTECOMISSAO.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjComissaoVendedores.Lotepagamento.Get_pesquisa,Self.ObjComissaoVendedores.Lotepagamento.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjComissaoVendedores.Lotepagamento.status<>dsinactive
                                  then exit;

                                  If (Self.ObjComissaoVendedores.Lotepagamento.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjComissaoVendedores.Lotepagamento.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFLOTECOMISSAO.LimpaLabels;
begin
      lbCodigo.Caption:='';
end;

procedure TFLOTECOMISSAO.FormShow(Sender: TObject);
begin
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(imgrodape,'RODAPE');
     LimpaLabels;

end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFLOTECOMISSAO.btopcoesClick(Sender: TObject);
var
plote:string;
begin
     if (lbCodigo.Caption='')
     Then exit;
     
     plote:=lbCodigo.Caption;
     Self.ObjComissaoVendedores.opcoes(plote);


     if (Self.ObjComissaoVendedores.Lotepagamento.LocalizaCodigo(plote)=true)
     then Begin
               Self.ObjComissaoVendedores.Lotepagamento.TabelaparaObjeto;
               Self.ObjetoParaControles;
     End
     else limpaedit(Self);
end;

procedure TFLOTECOMISSAO.btrelatoriosClick(Sender: TObject);
begin
     Self.ObjComissaoVendedores.Imprime(lbCodigo.Caption);
end;

procedure TFLOTECOMISSAO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if(key=vk_f1) then
    begin
        Fprincipal.chamaPesquisaMenu;
    end;
end;

end.

