object FFORNECEDOR: TFFORNECEDOR
  Left = 399
  Top = 195
  Width = 936
  Height = 598
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'FORNECEDOR'
  Color = clWhite
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 920
    Height = 60
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    DesignSize = (
      920
      60)
    object lbnomeformulario: TLabel
      Left = 582
      Top = 3
      Width = 104
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Cadastro de'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb1: TLabel
      Left = 579
      Top = 25
      Width = 120
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Fornecedores'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigoFornecedor: TLabel
      Left = 739
      Top = 13
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btAjuda: TSpeedButton
      Left = 872
      Top = -13
      Width = 53
      Height = 80
      Cursor = crHandPoint
      Hint = 'Ajuda'
      Anchors = [akTop, akRight]
      Flat = True
      Glyph.Data = {
        4A0D0000424D4A0D0000000000003600000028000000240000001F0000000100
        180000000000140D0000D8030000D80300000000000000000000FEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFAF6FBF9F2FEFEF4FFFFFAFF
        FCFDFFF9FFFEFEFDFCFFFFFEFEFFFDFEFCFEFEF8FFFFFBFEFCFFFFFCFFFDF6FB
        FFF7F8FFF7FBFFFBFBFDF7FFFFFCFFFFFEF7FDFCFBFFFFFFFBFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFF9FD
        F8FDFFFFFDFBFFFFFBFFFFFDFEFFFCFCFFFEFFFEFDFFFCF9FBF0EFF1F0EBEDF9
        F1F2F6F8F9F1FFFFF4FFFFFEFCFFFFFDFFFFFAFFFBFCFFFEFDFFFFFAFDFAFCFD
        F7FFFFFFFCFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFFFCFFFFFCFEFFFDFCFFFFFFFFFDFFF2E4E6DEBCBCCF
        9F99EB8688E68081EA7E7DF68686FE9092FDA2A5F7C3C3FFF0EDFFFAFFF8FFFF
        FFFDFFFFF5FDFCFDFFF6FFFFFBF8FAFFFDFEFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFBFFFCFEFFFBFFFCE7
        E1DCC29E9EBF7579DC7677F07B76FF8783FF8F88FD9388FB8F84FF837BFF7977
        F77576F98084FBB6B9F2F2ECE8FFFAF8FDFCFFFEFFFAFCFDFFFCFEFCFEFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFEFDFFFAFCD7B8BBB17879C87772F08E88FE9893F58E8CE7797DD76F70
        D26867DC716EE88581F69A95F99591FE8686FF7173F28484FFD9D4FFFFFBFDF8
        F7FFFCFFFFFEFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFF9B99D9CA76B6CDA8786F99B96E17D79
        B6484C98222FAB1122AA0F1EA5131FA41D25AA2226C53938E6625BFF9085FD9B
        91FF7B7CF2777BF4C8C7F8FFFEF9FEFDFFFEFFFFFBFCFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFC0A0A1A7706D
        E6928CFC9691B3484A87192589112398182BA32E31DC6F71EB8082CC5050BB28
        26D32A22EB2F24FE3727F36D69FF9D97FB7D83F7707EFEC5CDFFFEFFFFFDFDED
        FFFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFCFBFDFFFDFFFFFBFFFAFFFFF2FAFA
        FFFEFEDCBDBEA66D6BDB9092ED909996303C770E1B8D1E2C9B1C2BA11425D96F
        75FDA8B0FFA2B0FFA0AAC83F43BC2F26D73927FF3929FF2B1CEC5650FF9996FE
        7D7AFD7F7EFADFDBFFFEFFF4FEFEFEFDFFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFF
        FDF9FEFFFDFFF7FAFEFAFFFFF4E6E7A67F81CF8B8CEC9A9F92323C7B121D9123
        2F921C27A5232EA71C27DE837EFFB7B5F9A9AEFDB0B3BF4343B42821C9372BDA
        3326E33626CB2728CA5556FF9893FB7777DE9A9BFFFEFEFFFCFDFEFDFFFEFDFF
        FEFDFFFEFDFFFEFEFEFEFDFFFFFBFFFFFBFFFAFBFFFFFFFFC3AEADA77678F8A4
        A9A9515776131B8D202898242BA3282CA72629A82627B55047EBADA2FFBCB5CD
        7974A92E2AA13026A5372BBD332DBF3528BD2B2DA51E26DF7372FF918FE27377
        F0CCCCFFFFFEFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFAF9FBFFFDFFFFFAFFFFFE
        FFF5F0EFA38384DB9FA0E78A91791D22852428992E309E2827A72C28AC3028AB
        3229A3271FA13E36B4413E9E332CB03129A73126A23127B0322EA93026AD3131
        A91925B63239F2948FFD7C7FE9989BFBFFF9FEFDFFFEFDFFFEFDFFFEFDFFFFFE
        FEFCFBFDFFFEFFFFFDFFFFFEFFDACECEAE8687F0ABAEB156597E24248B2C2992
        2A25A93730A93328AE3B2EA33222D35E59EA928BF28C87E27D74AC362BA93A2A
        A23729A5342AA3342CA62728B02931A71821D8716FFF8A8ADE7D7FEFECE7FEFD
        FFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFFFDFCFEFFFCFEFFFEFFC0B3B1BF9392F0
        A5A78F3B35842A2396352B9F372AA93C2EAC3D2DAC3C2AA1311FD16D69F8CABF
        FBC0B7FCABA3AA3A2EA23827A33928A9372AA6352BAE302BA72C2EAA1922C550
        51FF9692DC7475E0D2D3FFFEFFFEFDFFFEFDFFFEFDFFFFFDFEFEFDFFFEFDFFFF
        FDFFFFFEFEB6A9A7CB9D9CEC9E9F8335288E3527A0392AAD4130A83C2AA73B29
        A93927AE3827CE6C66FFCDC1FABAB5FFBFB6D46B62AD3126B43427B13A2BA933
        27A93327A02A25AC1F28AF373BFC9D94E7797BD1B8BCFFFEFFFFFEFFFEFDFFFE
        FDFFFFFDFEFCFCFCFDFCFEFFFEFFFDFBFBB5A8A6DBAEABE19192813522983C29
        A73B29AF3E2AA83B26AA3F2AAD3B2AAF3224B05A4EFFE0D0FFCEC6F7BEB5FFBE
        B3DE8C7BA44434A33628AD3C2CAD3726A8342DA7262DAE2E33FA9B92E27A7BD4
        ABB3FFFEFFFFFEFFFEFDFFFEFDFFFBFCFFF9FEFFFFFCFDFBFFFFFCF9FBC9AAAB
        DEB1ADDF9A91813628B03C2BAB3D25A7412AA6362AB3362EA13726AE3C25A833
        24D79987FFF4E4F9D3CEFEBEBEFFC9C3EC9E91AD4436B137299C3B27AA3526B1
        2F28A4302FF7969AE67F86C7AAA5FFFEFFFFFEFFFEFDFFFEFDFFFAFBFFFCFEFF
        FFFEFFFCFEFFFCFEFFCEB4B4E6B6B2E6A99F98392AA13426B2382C9E3E2EAA3E
        33AD4338A74539A24536AB443B9B3D38CF8A87FFEBE6FEDED9F1C0B8FFC9BDEC
        8B81AA372AA73B2AA83F2CA52B1FA6403BF9A1A1E57E86C9B3AEFFFEFFFFFEFF
        FEFDFFFEFDFFFAFDFFFDFCFEFFFEFFFDFCFEFAFFFFD3BEC0ECB5B2F5C2B8AB40
        329F3D31B84D49A24F47AD554EAE574DAE5D55B1655FAD5654B34E50A13B40B9
        7B7BFFE1DBFED7CFF9CAC2FFBCB5BD5146AD3627A3402AA83526AD544AFEACAB
        D5787FD4C7C5FFFEFFFEFDFFFEFDFFFEFDFFFBFFFFFFFBFDFFFDFDFFFDFFF9FE
        FFDFD1D2E7B1B0FFDACEC26B61A74C45A55C54A85A54D3958FF2B8B3F9C2BFF4
        B4B3AD5352AB5C59AC5553A54343F7C0BBFDE0D9FFCCCAFBC9C9CC6A60B03429
        9F3923A63624C9776BFFADACC67E84E4E0DFFEFDFFFEFDFFFEFDFFFEFDFFFAFE
        FFFFFCFFFCFCFCFFFEFFFBFEFFF4ECEDE2B7B4FFDBD2DAB0ABA6504AA86153AB
        514AD59594FFF8F7FEF1EFFAD4D2C469649C5148A85950A14B45ECBBB3FBE1DA
        FFCED0FFCFD6D06E66AB352AAD3B2A9F3726F7A89DFBA4A7BB8E91F2F7F6FEFD
        FFFEFDFFFEFDFFFEFDFFFCFBFDFFFEFFFCFEFEFEFDFFFDFCFFFFFEFFE2C7C3F3
        CAC1EDDEDCBD7672AD5144CE6459D7797AF6F5F1F4FFFEF9F3EEE9B4AAB16860
        B45E58C99087FAD7CDFFDCD6F8D5D2FECBCFBE5A55A23127A12F22CB7264FFC3
        BBD28B8ECFB7B9FAFFFFFEFDFFFEFDFFFEFDFFFEFDFFFEFBFDFFFFFFFCFEFEFB
        FDFEFFFAFFFFFEFFEBE5DEF2C4BDFFE5E8EFC8C0AE6053E17164F47773F0BBB8
        FDFEFAFDFFFFFFF8F1FCD6D4FFCDCCFFDDD8FFDED8FFE0DAFFDDD7E7A1A2A339
        32A7352EAC4A3EF8B8ADECB2ADB68E90FAECEEFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFDFFFCFCFCFAFFFEFAFDFFFFFBFFFFFDFDFBFFF9FFCFC9FFC7CCFFF9EF
        E8BFB0D66D5FFF7C70F9807EEACECDFFFBFFFDFCFFF9FEFFFDFFFFFFE4E5FFE2
        DFF6E7DEEDB6AFB34648A1312BB03E37FCAA9FF7CDC1BC8F8CD5C7C8FFFDFFFC
        FEFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFFFEFFFFFEFF
        FDFCFEFCFBFDFCC8C8F6D7D6FFFCF6F0C9C1E57F7AFF7673F98481ECA5A1E6D6
        D7FCF1F3F9F1F1ECD7D9F0C1C3D78A8DA84646A03232A54B4BECABAAFFD1CFC9
        A09EC9ADACFEF9F8FBFFFFFCFCFCFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFF
        FDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFDFCFEFFF4EEF8CDCAFDD8D4FFFBF4FDE6
        E4FBA1A6FB7375FD6F68EA6E5CCE6855B76958AE6458A5514B993E3AA04D4BC8
        7F7BFCC3C1FFCBCCDAA4A4BFAEABF3F9F4FFFFFEFFFCFDFCFEFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFEFDFFFFFEFFF4FF
        FFFBF2EFF6D6D1FDD9D3FFEFE8FCFBF1FFE6DCF6BBB2EE9285D07C70B76F65B2
        6F66C0807BDDA39EF3C1BBFFD6CFF6C6C5D4ACADCEBBB8F9FAF6F7FFFDFAFCFC
        FFFEFFFCF9FBFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFC
        FEFDFCFEFEFDFFFFFEFFFFFAFCFBFFFFF9F9F9FCE0DFFFD3CCFEDDD4F8ECE6F8
        F6F5F5F8F6F4EFEEF6E4E5FADBDCFFDBDCFFD9D8FBCAC8F0C0BCD7B5B6E1D8D5
        FAFFFEFFFFFFFEFCFCFAFFFFF8FDFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFFFFFBFFFBFCFDFDFFFD
        FBFFFFEEF1FFE2E1FFD3D2FFD0D2FFD2CFFFD1CDFFD2CFFFCFCFFFC9C9F2BFC3
        E9C4C6EED6D8F5F7F7FFFBFCFFFEFFFFFCFEFDFFFFF9FCFFFCFBFFFFFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFFFEFDFFFEFDFFFDFCFEFDFCFEFD
        FCFEFFFBFFF0FDFBF1FFFBFFFEFFFFFDFFF9FEFFF9FCFAFEF2EEF9EAE1F4E5DC
        F3E3DCF3E3DDF6E7E5FCF1F3FEFAFFF9FCFFF4FEFEFFFEFFFFF7FAFFFDFFFCF9
        FBFFFDFFFFFCFFF5F8FCFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFF7F9FFFDFEFFFFFBF8FFFDF9FDFBFAF1FBFB
        F6FFFFFBFDFEFBFEFFFFFCFFFFFDFFFFFEFFFBFFFFF8FFFFFDFEFFFFFAFFFFFD
        FFFAFEFFF8FFFFF9FBFCFFFDFFFFF9FCFBFAFCFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFCFFFEFF
        F7FDFFF5FEFFFBFFFFFFFFFEFFFCFDFFFCFFFFFAFFFFFCFFF9FEFDF9FFFDFBFF
        FEF9FCFAFBFBFBFFFFFFFFFCFEFEFDFFFBFAFCFBFFFFF5FDFDF7FFFFFDFFFFFF
        FCFFFEFDFFFEFDFFFEFDFFFEFDFF}
      OnClick = btAjudaClick
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btRelatorioClick
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btPesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btExcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btCancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btSalvarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btAlterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btNovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btSairClick
      Spacing = 0
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 515
    Width = 920
    Height = 45
    Align = alBottom
    Color = clMedGray
    TabOrder = 1
    DesignSize = (
      920
      45)
    object imgrodape: TImage
      Left = 1
      Top = 0
      Width = 918
      Height = 44
      Align = alBottom
    end
    object lb14: TLabel
      Left = 575
      Top = 11
      Width = 283
      Height = 20
      Anchors = [akTop]
      Caption = 'Existem X vendedores cadastrados'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object pgcGuia: TPageControl
    Left = 0
    Top = 60
    Width = 920
    Height = 455
    ActivePage = tsPrincipal
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Style = tsFlatButtons
    TabOrder = 2
    object tsPrincipal: TTabSheet
      Caption = '&1 - Principal'
      object panelPrincipal: TPanel
        Left = 0
        Top = 0
        Width = 912
        Height = 423
        Align = alClient
        Color = 10643006
        TabOrder = 0
        object lb11: TLabel
          Left = 9
          Top = 344
          Width = 26
          Height = 14
          Caption = 'Obs.:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb12: TLabel
          Left = 9
          Top = 326
          Width = 24
          Height = 14
          Caption = 'Fone'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb13: TLabel
          Left = 9
          Top = 256
          Width = 57
          Height = 14
          Caption = 'C'#243'd do Pa'#237's'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb15: TLabel
          Left = 9
          Top = 231
          Width = 13
          Height = 14
          Caption = 'UF'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb16: TLabel
          Left = 9
          Top = 208
          Width = 33
          Height = 14
          Caption = 'Cidade'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb17: TLabel
          Left = 9
          Top = 158
          Width = 46
          Height = 14
          Caption = 'Endere'#231'o'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb18: TLabel
          Left = 257
          Top = 326
          Width = 18
          Height = 14
          Caption = 'Fax'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb19: TLabel
          Left = 9
          Top = 303
          Width = 28
          Height = 14
          Caption = 'E-mail'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb20: TLabel
          Left = 459
          Top = 231
          Width = 19
          Height = 14
          Caption = 'CEP'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb21: TLabel
          Left = 9
          Top = 280
          Width = 37
          Height = 14
          Caption = 'Contato'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb22: TLabel
          Left = 9
          Top = 183
          Width = 29
          Height = 14
          Caption = 'Bairro'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb23: TLabel
          Left = 9
          Top = 115
          Width = 26
          Height = 14
          Caption = 'IE/RG'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb24: TLabel
          Left = 9
          Top = 67
          Width = 41
          Height = 14
          Caption = 'Fantasia'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lbdatacadastro: TLabel
          Left = 9
          Top = 19
          Width = 69
          Height = 14
          Caption = 'Data Cadastro'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb26: TLabel
          Left = 9
          Top = 44
          Width = 63
          Height = 14
          Caption = 'Raz'#227'o Social'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb27: TLabel
          Left = 9
          Top = 90
          Width = 47
          Height = 14
          Caption = 'CNPJ/CPF'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb28: TLabel
          Left = 257
          Top = 231
          Width = 54
          Height = 14
          Caption = 'Cod cidade'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label1: TLabel
          Left = 9
          Top = 137
          Width = 77
          Height = 14
          Caption = 'Tipo fornecedor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = False
        end
        object lbNomeTipoFornecedor: TLabel
          Left = 177
          Top = 135
          Width = 332
          Height = 15
          Cursor = crHandPoint
          AutoSize = False
          Caption = 'Tipo fornecedor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clAqua
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          OnClick = lbNomeTipoFornecedorClick
          OnMouseMove = lbNomeTipoFornecedorMouseMove
          OnMouseLeave = lbNomeTipoFornecedorMouseLeave
        end
        object lb25: TLabel
          Left = 459
          Top = 182
          Width = 37
          Height = 14
          Caption = 'N'#250'mero'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = False
        end
        object mmoobservacoes: TMemo
          Left = 101
          Top = 344
          Width = 576
          Height = 52
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            '')
          ParentFont = False
          TabOrder = 17
        end
        object edtendereco: TEdit
          Left = 101
          Top = 155
          Width = 690
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 50
          ParentFont = False
          TabOrder = 5
        end
        object cbbcidade: TComboBox
          Left = 101
          Top = 201
          Width = 352
          Height = 22
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ItemHeight = 14
          ParentFont = False
          TabOrder = 8
          OnExit = cbbcidadeExit
        end
        object edtestado: TEdit
          Left = 101
          Top = 226
          Width = 59
          Height = 20
          CharCase = ecUpperCase
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 2
          ParentFont = False
          TabOrder = 9
          OnExit = edtestadoExit
        end
        object edtCodigoCidade: TEdit
          Left = 313
          Top = 226
          Width = 140
          Height = 20
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
        end
        object cbbpais: TComboBox
          Left = 101
          Top = 249
          Width = 352
          Height = 22
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ItemHeight = 14
          ParentFont = False
          TabOrder = 12
        end
        object edtfax: TEdit
          Left = 313
          Top = 321
          Width = 140
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 16
        end
        object edtemail: TEdit
          Left = 101
          Top = 298
          Width = 352
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 14
        end
        object edtcep: TEdit
          Left = 499
          Top = 226
          Width = 138
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 11
        end
        object edtcontato: TEdit
          Left = 101
          Top = 275
          Width = 690
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 50
          ParentFont = False
          TabOrder = 13
        end
        object edtbairro: TEdit
          Left = 101
          Top = 178
          Width = 352
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 50
          ParentFont = False
          TabOrder = 6
        end
        object edtie: TEdit
          Left = 101
          Top = 109
          Width = 120
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object edtfantasia: TEdit
          Left = 101
          Top = 62
          Width = 690
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object edtdatacadastro: TMaskEdit
          Left = 101
          Top = 14
          Width = 70
          Height = 20
          EditMask = '!99/99/0000;'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 0
          Text = '  /  /    '
        end
        object edtrazaosocial: TEdit
          Left = 101
          Top = 39
          Width = 691
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object edtfone: TEdit
          Left = 101
          Top = 321
          Width = 140
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 15
        end
        object edtTipoFornecedor: TEdit
          Left = 101
          Top = 132
          Width = 70
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnDblClick = edtTipoFornecedorDblClick
          OnExit = edtTipoFornecedorExit
          OnKeyDown = edtTipoFornecedorKeyDown
          OnKeyPress = edtportadorKeyPress
        end
        object edtNumero: TEdit
          Left = 499
          Top = 179
          Width = 63
          Height = 20
          Color = clCream
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
        end
        object edtCGC: TMaskEdit
          Left = 101
          Top = 85
          Width = 118
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 18
        end
      end
    end
    object tsCredito: TTabSheet
      Caption = '&2 - Credito'
      ImageIndex = 1
      object panel2: TPanel
        Left = 0
        Top = 0
        Width = 912
        Height = 423
        Align = alClient
        Color = 10643006
        TabOrder = 0
        object lb2: TLabel
          Left = 12
          Top = 78
          Width = 40
          Height = 14
          Caption = 'Ag'#234'ncia'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb3: TLabel
          Left = 12
          Top = 55
          Width = 73
          Height = 14
          Caption = 'Conta Corrente'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lbnomeramoatividade: TLabel
          Left = 171
          Top = 219
          Width = 151
          Height = 14
          AutoSize = False
          Caption = 'Ramo de Atividade'
          Font.Charset = ANSI_CHARSET
          Font.Color = clAqua
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbprazopagamento: TLabel
          Left = 171
          Top = 196
          Width = 143
          Height = 14
          AutoSize = False
          Caption = 'Prazo de pagamento'
          Font.Charset = ANSI_CHARSET
          Font.Color = clAqua
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lb4: TLabel
          Left = 12
          Top = 219
          Width = 57
          Height = 14
          Caption = 'R. Atividade'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb5: TLabel
          Left = 12
          Top = 195
          Width = 52
          Height = 14
          Caption = 'Prazo pgto'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb6: TLabel
          Left = 12
          Top = 172
          Width = 47
          Height = 14
          Caption = 'Comiss'#227'o'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb7: TLabel
          Left = 12
          Top = 149
          Width = 47
          Height = 14
          Caption = 'Pl. contas'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb8: TLabel
          Left = 12
          Top = 125
          Width = 41
          Height = 14
          Caption = 'Portador'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb9: TLabel
          Left = 12
          Top = 101
          Width = 53
          Height = 14
          Caption = 'Represent.'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb10: TLabel
          Left = 12
          Top = 31
          Width = 31
          Height = 14
          Caption = 'Banco'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object edtagencia: TEdit
          Left = 96
          Top = 73
          Width = 150
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 30
          ParentFont = False
          TabOrder = 2
        end
        object edtcontacorrente: TEdit
          Left = 96
          Top = 50
          Width = 150
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 30
          ParentFont = False
          TabOrder = 1
        end
        object edtramoatividade: TEdit
          Left = 96
          Top = 216
          Width = 70
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
          OnDblClick = edtramoatividadeDblClick
          OnKeyDown = edtRamoAtividadeKeyDown
          OnKeyPress = edtCodigoPlanoContasKeyPress
        end
        object edtprazopagamento: TEdit
          Left = 96
          Top = 192
          Width = 70
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          OnDblClick = edtprazopagamentoDblClick
          OnKeyDown = edtPrazoPagamentoKeyDown
          OnKeyPress = edtportadorKeyPress
        end
        object edtcomissao: TEdit
          Left = 96
          Top = 169
          Width = 70
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object edtbanco: TEdit
          Left = 96
          Top = 26
          Width = 449
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 30
          ParentFont = False
          TabOrder = 0
        end
        object edtrepresentante: TEdit
          Left = 96
          Top = 97
          Width = 70
          Height = 20
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object edtportador: TEdit
          Left = 96
          Top = 121
          Width = 70
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnDblClick = edtportadorDblClick
          OnKeyDown = edtportadorKeyDown
          OnKeyPress = edtportadorKeyPress
        end
        object edtCodigoPlanoContas: TMaskEdit
          Left = 96
          Top = 145
          Width = 70
          Height = 20
          Hint = 
            'C'#243'digo do Plano de Contas Cont'#225'bil (Somente para Exporta'#231#227'o Cont' +
            #225'bil)'
          BiDiMode = bdRightToLeft
          CharCase = ecUpperCase
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentBiDiMode = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnDblClick = edtCodigoPlanoContasDblClick
          OnKeyDown = edtCodigoPlanoContasKeyDown
          OnKeyPress = edtCodigoPlanoContasKeyPress
        end
      end
    end
  end
end
