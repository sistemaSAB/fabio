unit UobjCOMPONENTE;
Interface
Uses Ibquery,windows,Classes,Db,UessencialGlobal;

Type
   TObjCOMPONENTE=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                ReferenciaAnterior                          :string;


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaReferencia(Parametro:string) :boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Referencia(parametro: string);
                Function Get_Referencia: string;
                Procedure Submit_Descricao(parametro: string);
                Function Get_Descricao: string;
                Procedure Submit_Unidade(parametro: string);
                Function Get_Unidade: string;
                Procedure Submit_AlturaIdeal(parametro: string);
                Function Get_AlturaIdeal: string;
                Procedure Submit_LarguraIdeal(parametro: string);
                Function Get_LarguraIdeal: string;
                Procedure Submit_AlturaMinima(parametro: string);
                Function Get_AlturaMinima: string;
                Procedure Submit_LarguraMinima(parametro: string);
                Function Get_LarguraMinima: string;
                Procedure Submit_AlturaMaxima(parametro: string);
                Function Get_AlturaMaxima: string;
                Procedure Submit_LarguraMaxima(parametro: string);
                Function Get_LarguraMaxima: string;
                Procedure Submit_AreaMinima(parametro: string);
                Function Get_AreaMinima: string;

                Function  VerificaReferencia(PStatus:TDataSetState; PCodigo,PReferencia:string):Boolean; {Rodolfo}
                function PrimeiroRegistro: Boolean;
                function ProximoRegisto(PCodigo: Integer): Boolean;
                function RegistoAnterior(PCodigo: Integer): Boolean;
                function UltimoRegistro: Boolean;

                Procedure fieldbyname(PCampo:String;PValor:String);overload;
                Function fieldbyname(PCampo:String):String;overload;


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Referencia:string;
               Descricao:string;
               Unidade:string;
               AlturaIdeal:string;
               LarguraIdeal:string;
               AlturaMinima:string;
               LarguraMinima:string;
               AlturaMaxima:string;
               LarguraMaxima:string;
               AreaMinima:string;
//CODIFICA VARIAVEIS PRIVADAS













               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios
//USES IMPLEMENTATION
;


{ TTabTitulo }


Function  TObjCOMPONENTE.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Referencia:=fieldbyname('Referencia').asstring;
        Self.Descricao:=fieldbyname('Descricao').asstring;
        Self.Unidade:=fieldbyname('Unidade').asstring;
        Self.AlturaIdeal:=fieldbyname('AlturaIdeal').asstring;
        Self.LarguraIdeal:=fieldbyname('LarguraIdeal').asstring;
        Self.AlturaMinima:=fieldbyname('AlturaMinima').asstring;
        Self.LarguraMinima:=fieldbyname('LarguraMinima').asstring;
        Self.AlturaMaxima:=fieldbyname('AlturaMaxima').asstring;
        Self.LarguraMaxima:=fieldbyname('LarguraMaxima').asstring;
        Self.AreaMinima:=fieldbyname('AreaMinima').asstring;
//CODIFICA TABELAPARAOBJETO











        result:=True;
     End;
end;


Procedure TObjCOMPONENTE.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Referencia').asstring:=Self.Referencia;
        ParamByName('Descricao').asstring:=Self.Descricao;
        ParamByName('Unidade').asstring:=Self.Unidade;
        ParamByName('AlturaIdeal').asstring:=virgulaparaponto(Self.AlturaIdeal);
        ParamByName('LarguraIdeal').asstring:=virgulaparaponto(Self.LarguraIdeal);
        ParamByName('AlturaMinima').asstring:=virgulaparaponto(Self.AlturaMinima);
        ParamByName('LarguraMinima').asstring:=virgulaparaponto(Self.LarguraMinima);
        ParamByName('AlturaMaxima').asstring:=virgulaparaponto(Self.AlturaMaxima);
        ParamByName('LarguraMaxima').asstring:=virgulaparaponto(Self.LarguraMaxima);
        ParamByName('AreaMinima').asstring:=virgulaparaponto(Self.AreaMinima);
  End;
End;


Procedure TObjCOMPONENTE.fieldbyname(PCampo:String;PValor:String);
Begin
     PCampo:=Uppercase(pcampo);

     If (Pcampo='CODIGO')
     Then Begin
              Self.Submit_CODIGO(pVALOR);
              exit;
     End;
     If (Pcampo='REFERENCIA')
     Then Begin
              Self.Submit_REFERENCIA(pVALOR);
              exit;
     End;
     If (Pcampo='DESCRICAO')
     Then Begin
              Self.Submit_DESCRICAO(pVALOR);
              exit;
     End;
     If (Pcampo='UNIDADE')
     Then Begin
              Self.Submit_UNIDADE(pVALOR);
              exit;
     End;
     If (Pcampo='ALTURAIDEAL')
     Then Begin
              Self.Submit_ALTURAIDEAL(pVALOR);
              exit;
     End;
     If (Pcampo='LARGURAIDEAL')
     Then Begin
              Self.Submit_LARGURAIDEAL(pVALOR);
              exit;
     End;
     If (Pcampo='ALTURAMINIMA')
     Then Begin
              Self.Submit_ALTURAMINIMA(pVALOR);
              exit;
     End;
     If (Pcampo='LARGURAMINIMA')
     Then Begin
              Self.Submit_LARGURAMINIMA(pVALOR);
              exit;
     End;
     If (Pcampo='ALTURAMAXIMA')
     Then Begin
              Self.Submit_ALTURAMAXIMA(pVALOR);
              exit;
     End;
     If (Pcampo='LARGURAMAXIMA')
     Then Begin
              Self.Submit_LARGURAMAXIMA(pVALOR);
              exit;
     End;
     If (Pcampo='AREAMINIMA')
     Then Begin
              Self.Submit_AREAMINIMA(pVALOR);
              exit;
     End;
end;

Function TObjCOMPONENTE.fieldbyname(PCampo:String):String;
Begin
     PCampo:=Uppercase(pcampo);                                
     Result:=''; 
     If (Pcampo='CODIGO')
     Then Begin
              Result:=Self.Get_CODIGO;
              exit;
     End;
     If (Pcampo='REFERENCIA')
     Then Begin
              Result:=Self.Get_REFERENCIA;
              exit;
     End;
     If (Pcampo='DESCRICAO')
     Then Begin
              Result:=Self.Get_DESCRICAO;
              exit;
     End;
     If (Pcampo='UNIDADE')
     Then Begin
              Result:=Self.Get_UNIDADE;
              exit;
     End;
     If (Pcampo='ALTURAIDEAL')
     Then Begin
              Result:=Self.Get_ALTURAIDEAL;
              exit;
     End;
     If (Pcampo='LARGURAIDEAL')
     Then Begin
              Result:=Self.Get_LARGURAIDEAL;
              exit;
     End;
     If (Pcampo='ALTURAMINIMA')
     Then Begin
              Result:=Self.Get_ALTURAMINIMA;
              exit;
     End;
     If (Pcampo='LARGURAMINIMA')
     Then Begin
              Result:=Self.Get_LARGURAMINIMA;
              exit;
     End;
     If (Pcampo='ALTURAMAXIMA')
     Then Begin
              Result:=Self.Get_ALTURAMAXIMA;
              exit;
     End;
     If (Pcampo='LARGURAMAXIMA')
     Then Begin
              Result:=Self.Get_LARGURAMAXIMA;
              exit;
     End;
     If (Pcampo='AREAMINIMA')
     Then Begin
              Result:=Self.Get_AREAMINIMA;
              exit;
     End;
end;

//***********************************************************************

function TObjCOMPONENTE.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCOMPONENTE.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Referencia:='';
        Descricao:='';
        Unidade:='';
        AlturaIdeal:='';
        LarguraIdeal:='';
        AlturaMinima:='';
        LarguraMinima:='';
        AlturaMaxima:='';
        LarguraMaxima:='';
        AreaMinima:='';
//CODIFICA ZERARTABELA











     End;
end;

Function TObjCOMPONENTE.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/C�digo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCOMPONENTE.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCOMPONENTE.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        Strtofloat(Self.AlturaIdeal);
     Except
           Mensagem:=mensagem+'/Altura Ideal';
     End;
     try
        Strtofloat(Self.LarguraIdeal);
     Except
           Mensagem:=mensagem+'/Largura Ideal';
     End;
     try
        Strtofloat(Self.AlturaMinima);
     Except
           Mensagem:=mensagem+'/Altura Minima';
     End;
     try
        Strtofloat(Self.LarguraMinima);
     Except
           Mensagem:=mensagem+'/Largura M�nima';
     End;
     try
        Strtofloat(Self.AlturaMaxima);
     Except
           Mensagem:=mensagem+'/Altura M�xima';
     End;
     try
        Strtofloat(Self.LarguraMaxima);
     Except
           Mensagem:=mensagem+'/Largura M�xima';
     End;
     try
        Strtofloat(Self.AreaMinima);
     Except
           Mensagem:=mensagem+'/�rea M�nima';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCOMPONENTE.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCOMPONENTE.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCOMPONENTE.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro COMPONENTE vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Referencia,Descricao,Unidade,AlturaIdeal,LarguraIdeal');
           SQL.ADD(' ,AlturaMinima,LarguraMinima,AlturaMaxima,LarguraMaxima,AreaMinima');
           SQL.ADD(' ');
           SQL.ADD(' from  TabComponente');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCOMPONENTE.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCOMPONENTE.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCOMPONENTE.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabComponente(Codigo,Referencia,Descricao');
                InsertSQL.add(' ,Unidade,AlturaIdeal,LarguraIdeal,AlturaMinima,LarguraMinima');
                InsertSQL.add(' ,AlturaMaxima,LarguraMaxima,AreaMinima)');
                InsertSQL.add('values (:Codigo,:Referencia,:Descricao,:Unidade,:AlturaIdeal');
                InsertSQL.add(' ,:LarguraIdeal,:AlturaMinima,:LarguraMinima,:AlturaMaxima');
                InsertSQL.add(' ,:LarguraMaxima,:AreaMinima)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabComponente set Codigo=:Codigo,Referencia=:Referencia');
                ModifySQL.add(',Descricao=:Descricao,Unidade=:Unidade,AlturaIdeal=:AlturaIdeal');
                ModifySQL.add(',LarguraIdeal=:LarguraIdeal,AlturaMinima=:AlturaMinima');
                ModifySQL.add(',LarguraMinima=:LarguraMinima,AlturaMaxima=:AlturaMaxima');
                ModifySQL.add(',LarguraMaxima=:LarguraMaxima,AreaMinima=:AreaMinima');
                ModifySQL.add('');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabComponente where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCOMPONENTE.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCOMPONENTE.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCOMPONENTE');
     Result:=Self.ParametroPesquisa;
end;

function TObjCOMPONENTE.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de COMPONENTE ';
end;


function TObjCOMPONENTE.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCOMPONENTE,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCOMPONENTE,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCOMPONENTE.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCOMPONENTE.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCOMPONENTE.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjComponente.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjComponente.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjComponente.Submit_Referencia(parametro: string);
begin
        Self.Referencia:=Parametro;
end;
function TObjComponente.Get_Referencia: string;
begin
        Result:=Self.Referencia;
end;
procedure TObjComponente.Submit_Descricao(parametro: string);
begin
        Self.Descricao:=Parametro;
end;
function TObjComponente.Get_Descricao: string;
begin
        Result:=Self.Descricao;
end;
procedure TObjComponente.Submit_Unidade(parametro: string);
begin
        Self.Unidade:=Parametro;
end;
function TObjComponente.Get_Unidade: string;
begin
        Result:=Self.Unidade;
end;
procedure TObjComponente.Submit_AlturaIdeal(parametro: string);
begin
        Self.AlturaIdeal:=Parametro;
end;
function TObjComponente.Get_AlturaIdeal: string;
begin
        Result:=Self.AlturaIdeal;
end;
procedure TObjComponente.Submit_LarguraIdeal(parametro: string);
begin
        Self.LarguraIdeal:=Parametro;
end;
function TObjComponente.Get_LarguraIdeal: string;
begin
        Result:=Self.LarguraIdeal;
end;
procedure TObjComponente.Submit_AlturaMinima(parametro: string);
begin
        Self.AlturaMinima:=Parametro;
end;
function TObjComponente.Get_AlturaMinima: string;
begin
        Result:=Self.AlturaMinima;
end;
procedure TObjComponente.Submit_LarguraMinima(parametro: string);
begin
        Self.LarguraMinima:=Parametro;
end;
function TObjComponente.Get_LarguraMinima: string;
begin
        Result:=Self.LarguraMinima;
end;
procedure TObjComponente.Submit_AlturaMaxima(parametro: string);
begin
        Self.AlturaMaxima:=Parametro;
end;
function TObjComponente.Get_AlturaMaxima: string;
begin
        Result:=Self.AlturaMaxima;
end;
procedure TObjComponente.Submit_LarguraMaxima(parametro: string);
begin
        Self.LarguraMaxima:=Parametro;
end;
function TObjComponente.Get_LarguraMaxima: string;
begin
        Result:=Self.LarguraMaxima;
end;
procedure TObjComponente.Submit_AreaMinima(parametro: string);
begin
        Self.AreaMinima:=Parametro;
end;
function TObjComponente.Get_AreaMinima: string;
begin
        Result:=Self.AreaMinima;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjCOMPONENTE.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCOMPONENTE';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



function TObjCOMPONENTE.VerificaReferencia(PStatus: TDataSetState; PCodigo,
  PReferencia: string): Boolean;   {Rodolfo}
begin
     Result:=true;

     PReferencia := StrReplaceRef(PReferencia); {Rodolfo}

     With Self.Objquery do
     Begin

         if (PStatus = dsInsert) then  // quando for insercao
         Begin
             close;
             SQL.Clear;
             SQL.Add('Select Codigo from TabComponente Where Referencia = '+#39+UpperCase(PReferencia)+#39);
             Open;

             if (RecordCount > 0)then
             Begin
                 Result:=true;
                 exit;
             end else
             Result:=false;
             exit;
         end;


         if  (PStatus = dsEdit)then    // quando for edicao
         Begin
             if Self.ReferenciaAnterior = PReferencia then
             Begin                      // Se a Referencia  que esta chegando � a mesma da anteiror
                 Result:=false;         // eu nem preciso verificar nada
                 exit;
             end else
             Begin
                 close;
                 SQL.Clear;
                 SQL.Add('Select Codigo from TabComponente Where Referencia = '+#39+UpperCase(PReferencia)+#39);
                 Open;

                 if (RecordCount > 0)then
                 Begin
                     Result:=true;
                     exit;
                 end else
                 Result:=false;
                 exit;
             end;
         end;
     end;

end;

function TObjCOMPONENTE.LocalizaReferencia(Parametro: string): boolean;
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro COMPONENTE vazio',mterror,[mbok],0);
                 exit;
       End;

       Parametro := StrReplaceRef(parametro); {Rodolfo}

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Referencia,Descricao,Unidade,AlturaIdeal,LarguraIdeal');
           SQL.ADD(' ,AlturaMinima,LarguraMinima,AlturaMaxima,LarguraMaxima,AreaMinima');
           SQL.ADD(' ');
           SQL.ADD(' from  TabComponente');
           SQL.ADD(' WHERE Referencia ='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;
function TObjComponente.PrimeiroRegistro: Boolean;
Var MenorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabComponente') ;
           Open;

           if (FieldByName('MenorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MenorCodigo:=fieldbyname('MenorCodigo').AsString;
           Self.LocalizaCodigo(MenorCodigo);

           Result:=true;
     end;
end;

function TObjComponente.UltimoRegistro: Boolean;
Var MaiorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabComponente') ;
           Open;

           if (FieldByName('MaiorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MaiorCodigo:=fieldbyname('MaiorCodigo').AsString;
           Self.LocalizaCodigo(MaiorCodigo);

           Result:=true;
     end;
end;

function TObjComponente.ProximoRegisto(PCodigo:Integer): Boolean;
Var MaiorValor:Integer;
begin
     Result:=false;

     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabComponente') ;
           Open;

           MaiorValor:=fieldbyname('MaiorCodigo').AsInteger;
     end;

     if (MaiorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Inc(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MaiorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Inc(PCodigo,1);
     end;

     Result:=true;
end;

function TObjComponente.RegistoAnterior(PCodigo: Integer): Boolean;
Var MenorValor:Integer;
begin
     Result:=false;
     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabComponente') ;
           Open;

           MenorValor:=fieldbyname('MenorCodigo').AsInteger;
     end;

     if (MenorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Dec(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MenorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Dec(PCodigo,1);
     end;

     Result:=true;
end;

end.



