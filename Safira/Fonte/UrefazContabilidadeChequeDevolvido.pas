unit UrefazContabilidadeChequeDevolvido;

interface

uses
  db,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,ibquery;

type
  TFrefazContabilidadeChequeDevolvido = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrefazContabilidadeChequeDevolvido: TFrefazContabilidadeChequeDevolvido;

implementation

uses UessencialGlobal, UDataModulo, UobjCHEQUEDEVOLVIDO,
  UMostraBarraProgresso;

{$R *.dfm}

procedure TFrefazContabilidadeChequeDevolvido.Button1Click(Sender: TObject);
var
QueryLocal,querycontabilidade:Tibquery;
ObjChequeDevolvido:TobjChequeDevolvido;
begin
     Try
        QueryLocal:=Tibquery.create(nil);
        querylocal.database:=Fdatamodulo.ibdatabase;

        QueryContabilidade:=Tibquery.create(nil);
        QueryContabilidade.database:=Fdatamodulo.ibdatabase;

        ObjChequeDevolvido:=TobjChequeDevolvido.create;
     Except
           mensagemerro('Erro na tentativa de criar a query local');
     End;

     Try
        With QueryLocal do
        begin
             close;
             sql.clear;
             sql.add('Select codigo,tituloapagar from TabChequeDevolvido where TituloaPagar is not null');
             open;
             last;
             FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
             FMostraBarraProgresso.Lbmensagem.caption:='';
             first;

             While not(eof) do
             Begin
                  FMostraBarraProgresso.IncrementaBarra1(1);
                  FMostraBarraProgresso.show;
                  Application.ProcessMessages;
                  
                  querycontabilidade.Close;
                  querycontabilidade.SQL.Clear;
                  querycontabilidade.SQL.Add('Select * from TabExportaContabilidade where Objgerador=''OBJCHEQUEDEVOLVIDO''');
                  querycontabilidade.SQL.Add('and codgerador='+fieldbyname('codigo').asstring);
                  querycontabilidade.open;
                  querycontabilidade.last;
                  if (querycontabilidade.recordcount=0)
                  Then Begin
                            //nao encontrou contabilidade
                            
                            ObjChequeDevolvido.LocalizaCodigo(fieldbyname('codigo').asstring);
                            ObjChequeDevolvido.TabelaparaObjeto;
                            //Nao Exporta no lancamento mas tem q exportar
                            //Debita Cheques Devolvidos
                            //Credita Fornecedor
                            //***************************************************
                            //lancando a contabilidade
                            //Debita banco e Credita Fornecedor
                            ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.ZerarTabela;
                            ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_CODIGO(ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Get_NovoCodigo);
                            ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Data(ObjChequeDevolvido.TituloaPagar.Get_EMISSAO);
                            ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_ContaDebite(ObjChequeDevolvido.Transferencia.PortadorDestino.Get_CodigoPlanodeContas);
                            ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_ContaCredite(ObjChequeDevolvido.TituloaPagar.CREDORDEVEDOR.Get_CampoContabilCredorDevedor(ObjChequeDevolvido.TituloaPagar.credordevedor.Get_CODIGO,ObjChequeDevolvido.TituloaPagar.Get_CODIGOCREDORDEVEDOR));
                            ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Valor(ObjChequeDevolvido.TituloaPagar.Get_VALOR);
                            

                            ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Historico('REF. CHEQUE DEVOLVIDO NUM '+ObjChequeDevolvido.TituloaPagar.Get_NUMDCTO);
                            ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_ObjGerador('OBJCHEQUEDEVOLVIDO');
                            ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_CodGerador(fieldbyname('codigo').asstring);
                            ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Exportado('N');
                            ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Exporta('S');
                            ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Status:=dsinsert;
                            
                            if (ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Salvar(False)=False)
                            Then Begin
                                      MensagemErro('Erro na tentativa de Gravar a Contabilidade');
                                      exit;
                            end;
                            //****************************************************



                  End;
                  
                  next;
             End;

        End;


     Finally
            FMostraBarraProgresso.close;
            freeandnil(querylocal);
            Freeandnil(QueryContabilidade);
            ObjChequeDevolvido.free;
     End;
end;

end.
