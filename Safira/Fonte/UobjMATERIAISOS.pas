unit UobjMATERIAISOS;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,UobjVIDRO;

Type
   TObjMATERIAISOS=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Objvidro                                    :TObjVIDRO;
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_FERRAGEM(parametro: string);
                Function Get_FERRAGEM: string;
                Procedure Submit_DIVERSO(parametro: string);
                Function Get_DIVERSO: string;
                Procedure Submit_KITBOX(parametro: string);
                Function Get_KITBOX: string;
                Procedure Submit_PERFILADO(parametro: string);
                Function Get_PERFILADO: string;
                Procedure Submit_PERSIANA(parametro: string);
                Function Get_PERSIANA: string;
                Procedure Submit_VIDRO(parametro: string);
                Function Get_VIDRO: string;
                Procedure Submit_ORDEMSERVICO(parametro: string);
                Function Get_ORDEMSERVICO: string;
                Procedure Submit_QUANTIDADE(parametro: string);
                Function Get_QUANTIDADE: string;
                Procedure Submit_VALOR(parametro: string);
                Function Get_VALOR: string;
                Procedure Submit_VALORFINAL(parametro: string);
                Function Get_VALORFINAL: string;
                Procedure Submit_DESCONTO(parametro: string);
                Function Get_DESCONTO: string;
                Procedure Submit_DATAM(parametro: string);
                Function Get_DATAM: string;
                Procedure Submit_DATAC(parametro: string);
                Function Get_DATAC: string;
                Procedure Submit_USERC(parametro: string);
                Function Get_USERC: string;
                Procedure Submit_USERM(parametro: string);
                Function Get_USERM: string;
                procedure ResgataMateriais(OrdemServ:string;Where:String);

                procedure Submit_Largura(parametro:string);
                procedure Submit_Altura(parametro:string);
                function Get_Largura:String;
                function Get_Altura:string;
                procedure Submit_CodigoBarras(parametro:string);
                function Get_CodigoBarras:string;

                procedure EdtVidroKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel;OS:string);

                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               ObjqueryPesquisa:Tibquery;

               CODIGO:string;
               FERRAGEM:string;
               DIVERSO:string;
               KITBOX:string;
               PERFILADO:string;
               PERSIANA:string;
               VIDRO:string;
               ORDEMSERVICO:string;
               QUANTIDADE:string;
               VALOR:string;
               VALORFINAL:string;
               DESCONTO:string;
               DATAM:string;
               DATAC:string;
               USERC:string;
               USERM:string;
               ALTURA:string;
               LARGURA:string;
               CODIGOBARRAS:string;
//CODIFICA VARIAVEIS PRIVADAS

               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls
//USES IMPLEMENTATION
, UMenuRelatorios;


{ TTabTitulo }


Function  TObjMATERIAISOS.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.FERRAGEM:=fieldbyname('FERRAGEM').asstring;
        Self.DIVERSO:=fieldbyname('DIVERSO').asstring;
        Self.KITBOX:=fieldbyname('KITBOX').asstring;
        Self.PERFILADO:=fieldbyname('PERFILADO').asstring;
        Self.PERSIANA:=fieldbyname('PERSIANA').asstring;
        Self.VIDRO:=fieldbyname('VIDRO').asstring;
        Self.ORDEMSERVICO:=fieldbyname('ORDEMSERVICO').asstring;
        Self.QUANTIDADE:=fieldbyname('QUANTIDADE').asstring;
        Self.VALOR:=fieldbyname('VALOR').asstring;
        Self.VALORFINAL:=fieldbyname('VALORFINAL').asstring;
        Self.DESCONTO:=fieldbyname('DESCONTO').asstring;
        Self.LARGURA:=fieldbyname('largura').AsString;
        Self.ALTURA:=fieldbyname('altura').AsString;
        Self.CODIGOBARRAS:=fieldbyname('codigobarras').AsString;
        //em caso de troca de nomes ou mesmo de vidro,
        //mostrar errado q seja, olhar se o problema n�o � aqui...
        Objvidro.LocalizaCodigo(VIDRO);
        Objvidro.TabelaparaObjeto;
        result:=True;
     End;
end;


Procedure TObjMATERIAISOS.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('FERRAGEM').asstring:=Self.FERRAGEM;
        ParamByName('DIVERSO').asstring:=Self.DIVERSO;
        ParamByName('KITBOX').asstring:=Self.KITBOX;
        ParamByName('PERFILADO').asstring:=Self.PERFILADO;
        ParamByName('PERSIANA').asstring:=Self.PERSIANA;
        ParamByName('VIDRO').asstring:=Self.VIDRO;
        ParamByName('ORDEMSERVICO').asstring:=Self.ORDEMSERVICO;
        ParamByName('QUANTIDADE').asstring:=virgulaparaponto(Self.QUANTIDADE);
        ParamByName('VALOR').asstring:=virgulaparaponto(Self.VALOR);
        ParamByName('VALORFINAL').asstring:=virgulaparaponto(Self.VALORFINAL);
        ParamByName('DESCONTO').asstring:=virgulaparaponto(Self.DESCONTO);
        ParamByName('altura').AsString:=self.ALTURA;
        ParamByName('largura').AsString:=Self.LARGURA;
        ParamByName('codigobarras').AsString:=Self.CODIGOBARRAS;
   End;
End;

//***********************************************************************

function TObjMATERIAISOS.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  {if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit; }

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjMATERIAISOS.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        FERRAGEM:='';
        DIVERSO:='';
        KITBOX:='';
        PERFILADO:='';
        PERSIANA:='';
        VIDRO:='';
        ORDEMSERVICO:='';
        QUANTIDADE:='';
        VALOR:='';
        VALORFINAL:='';
        DESCONTO:='';
        DATAM:='';
        DATAC:='';
        USERC:='';
        USERM:='';
        altura:='';
        largura:='';
        CODIGOBARRAS:='';
     End;
end;

Function TObjMATERIAISOS.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjMATERIAISOS.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjMATERIAISOS.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
     try
        Strtoint(Self.FERRAGEM);
     Except
           Mensagem:=mensagem+'/FERRAGEM';
     End;
     try
        Strtoint(Self.DIVERSO);
     Except
           Mensagem:=mensagem+'/DIVERSO';
     End;
     try
        Strtoint(Self.KITBOX);
     Except
           Mensagem:=mensagem+'/KITBOX';
     End;
     try
        Strtoint(Self.PERFILADO);
     Except
           Mensagem:=mensagem+'/PERFILADO';
     End;
     try
        Strtoint(Self.PERSIANA);
     Except
           Mensagem:=mensagem+'/PERSIANA';
     End;
     try
        Strtoint(Self.VIDRO);
     Except
           Mensagem:=mensagem+'/VIDRO';
     End;
     try
        Strtoint(Self.ORDEMSERVICO);
     Except
           Mensagem:=mensagem+'/ORDEMSERVICO';
     End;
     try
        Strtofloat(Self.QUANTIDADE);
     Except
           Mensagem:=mensagem+'/QUANTIDADE';
     End;
     try
        Strtofloat(Self.VALOR);
     Except
           Mensagem:=mensagem+'/VALOR';
     End;
     try
        Strtofloat(Self.VALORFINAL);
     Except
           Mensagem:=mensagem+'/VALORFINAL';
     End;
     try
        Strtofloat(Self.DESCONTO);
     Except
           Mensagem:=mensagem+'/DESCONTO';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjMATERIAISOS.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodatetime(Self.DATAM);
     Except
           Mensagem:=mensagem+'/DATAM';
     End;
     try
        Strtodatetime(Self.DATAC);
     Except
           Mensagem:=mensagem+'/DATAC';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjMATERIAISOS.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjMATERIAISOS.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro MATERIAISOS vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,FERRAGEM,DIVERSO,KITBOX,PERFILADO,PERSIANA,VIDRO');
           SQL.ADD(' ,ORDEMSERVICO,QUANTIDADE,VALOR,VALORFINAL,DESCONTO,DATAM,DATAC');
           SQL.ADD(' ,USERC,USERM,altura, largura,codigobarras');
           SQL.ADD(' from  TABMATERIAISOS');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjMATERIAISOS.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjMATERIAISOS.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjMATERIAISOS.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        ObjqueryPesquisa :=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        Self.Objvidro:=TObjVIDRO.Create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDataSource.DataSet:=Self.ObjqueryPesquisa;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABMATERIAISOS(CODIGO,FERRAGEM,DIVERSO,KITBOX');
                InsertSQL.add(' ,PERFILADO,PERSIANA,VIDRO,ORDEMSERVICO,QUANTIDADE,VALOR');
                InsertSQL.add(' ,VALORFINAL,DESCONTO,ALTURA,LARGURA,CODIGOBARRAS)');
                InsertSQL.add('values (:CODIGO,:FERRAGEM,:DIVERSO,:KITBOX,:PERFILADO');
                InsertSQL.add(' ,:PERSIANA,:VIDRO,:ORDEMSERVICO,:QUANTIDADE,:VALOR');
                InsertSQL.add(' ,:VALORFINAL,:DESCONTO,:ALTURA,:LARGURA,:CODIGOBARRAS');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABMATERIAISOS set CODIGO=:CODIGO,FERRAGEM=:FERRAGEM');
                ModifySQL.add(',DIVERSO=:DIVERSO,KITBOX=:KITBOX,PERFILADO=:PERFILADO');
                ModifySQL.add(',PERSIANA=:PERSIANA,VIDRO=:VIDRO,ORDEMSERVICO=:ORDEMSERVICO');
                ModifySQL.add(',QUANTIDADE=:QUANTIDADE,VALOR=:VALOR,VALORFINAL=:VALORFINAL');
                ModifySQL.add(',DESCONTO=:DESCONTO,ALTURA=:ALTURA,LARGURA=:LARGURA,CODIGOBARRAS=:CODIGOBARRAS');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABMATERIAISOS where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjMATERIAISOS.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjMATERIAISOS.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabMATERIAISOS');
     Result:=Self.ParametroPesquisa;
end;

function TObjMATERIAISOS.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de MATERIAISOS ';
end;


function TObjMATERIAISOS.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENMATERIAISOS,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENMATERIAISOS,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjMATERIAISOS.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Freeandnil(self.ObjqueryPesquisa);
    ObjDatasource.Free;
    Objvidro.Free;
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjMATERIAISOS.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjMATERIAISOS.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjMATERIAISOS.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjMATERIAISOS.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjMATERIAISOS.Submit_FERRAGEM(parametro: string);
begin
        Self.FERRAGEM:=Parametro;
end;
function TObjMATERIAISOS.Get_FERRAGEM: string;
begin
        Result:=Self.FERRAGEM;
end;
procedure TObjMATERIAISOS.Submit_DIVERSO(parametro: string);
begin
        Self.DIVERSO:=Parametro;
end;
function TObjMATERIAISOS.Get_DIVERSO: string;
begin
        Result:=Self.DIVERSO;
end;
procedure TObjMATERIAISOS.Submit_KITBOX(parametro: string);
begin
        Self.KITBOX:=Parametro;
end;
function TObjMATERIAISOS.Get_KITBOX: string;
begin
        Result:=Self.KITBOX;
end;
procedure TObjMATERIAISOS.Submit_PERFILADO(parametro: string);
begin
        Self.PERFILADO:=Parametro;
end;
function TObjMATERIAISOS.Get_PERFILADO: string;
begin
        Result:=Self.PERFILADO;
end;
procedure TObjMATERIAISOS.Submit_PERSIANA(parametro: string);
begin
        Self.PERSIANA:=Parametro;
end;
function TObjMATERIAISOS.Get_PERSIANA: string;
begin
        Result:=Self.PERSIANA;
end;
procedure TObjMATERIAISOS.Submit_VIDRO(parametro: string);
begin
        Self.VIDRO:=Parametro;
end;
function TObjMATERIAISOS.Get_VIDRO: string;
begin
        Result:=Self.VIDRO;
end;
procedure TObjMATERIAISOS.Submit_ORDEMSERVICO(parametro: string);
begin
        Self.ORDEMSERVICO:=Parametro;
end;
function TObjMATERIAISOS.Get_ORDEMSERVICO: string;
begin
        Result:=Self.ORDEMSERVICO;
end;
procedure TObjMATERIAISOS.Submit_QUANTIDADE(parametro: string);
begin
        Self.QUANTIDADE:=Parametro;
end;
function TObjMATERIAISOS.Get_QUANTIDADE: string;
begin
        Result:=Self.QUANTIDADE;
end;
procedure TObjMATERIAISOS.Submit_VALOR(parametro: string);
begin
        Self.VALOR:=Parametro;
end;
function TObjMATERIAISOS.Get_VALOR: string;
begin
        Result:=Self.VALOR;
end;
procedure TObjMATERIAISOS.Submit_VALORFINAL(parametro: string);
begin
        Self.VALORFINAL:=Parametro;
end;
function TObjMATERIAISOS.Get_VALORFINAL: string;
begin
        Result:=Self.VALORFINAL;
end;
procedure TObjMATERIAISOS.Submit_DESCONTO(parametro: string);
begin
        Self.DESCONTO:=Parametro;
end;
function TObjMATERIAISOS.Get_DESCONTO: string;
begin
        Result:=Self.DESCONTO;
end;
procedure TObjMATERIAISOS.Submit_DATAM(parametro: string);
begin
        Self.DATAM:=Parametro;
end;
function TObjMATERIAISOS.Get_DATAM: string;
begin
        Result:=Self.DATAM;
end;
procedure TObjMATERIAISOS.Submit_DATAC(parametro: string);
begin
        Self.DATAC:=Parametro;
end;
function TObjMATERIAISOS.Get_DATAC: string;
begin
        Result:=Self.DATAC;
end;
procedure TObjMATERIAISOS.Submit_USERC(parametro: string);
begin
        Self.USERC:=Parametro;
end;
function TObjMATERIAISOS.Get_USERC: string;
begin
        Result:=Self.USERC;
end;
procedure TObjMATERIAISOS.Submit_USERM(parametro: string);
begin
        Self.USERM:=Parametro;
end;
function TObjMATERIAISOS.Get_USERM: string;
begin
        Result:=Self.USERM;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjMATERIAISOS.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJMATERIAISOS';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

         { Case RgOpcoes.ItemIndex of

          End; }
     end;

end;

procedure TObjMATERIAISOS.ResgataMateriais(ordemserv:string;Where:String);
begin

    with ObjqueryPesquisa do
    begin
          Close;
          sql.Clear;
          SQL.Add('select tabmateriaisos.codigo, tabvidro.descricao, tabmateriaisos.valor, tabmateriaisos.desconto, tabmateriaisos.quantidade');
          SQL.Add(',tabmateriaisos.altura, tabmateriaisos.largura,tabmateriaisos.codigobarras from tabmateriaisos');
          sql.add('join tabvidro on tabvidro.codigo=tabmateriaisos.vidro');
          if(where='')
          then  sql.Add('where tabmateriaisos.ordemservico='+OrdemServ)
          else
          begin
                SQL.Add(where);
                sql.Add('and tabmateriaisos.ordemservico='+OrdemServ);
          end;
          if(OrdemServ='')
          then Exit;
          Open;

    end;

end;

procedure TObjMATERIAISOS.Submit_Largura(parametro:string);
begin
    Self.LARGURA:=parametro;
end;

function TObjMATERIAISOS.Get_Largura:string;
begin
    Result:=Self.LARGURA;
end;

procedure TObjMATERIAISOS.Submit_Altura(parametro:string);
begin
    self.ALTURA:=parametro;
end;

function TObjMATERIAISOS.Get_Altura:string;
begin
    Result:=self.ALTURA;
end;

procedure TObjMATERIAISOS.EdtVidroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel;OS:string);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('select tb.codigo,vidro.descricao as vidro,tb.quantidade from tabmateriaisos  tb join tabvidro vidro on vidro.codigo=tb.vidro where ordemservico='+OS,'Pesquisa de Vidros na OS',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If FpesquisaLocal.QueryPesq.fieldbyname('vidro').asstring<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('vidro').asstring
                                        Else LABELNOME.caption:='';
                                 End;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);

     End;
end;

procedure TObjMATERIAISOS.Submit_CodigoBarras(parametro:string);
begin
   Self.CODIGOBARRAS:=parametro;
end;

function TObjMATERIAISOS.Get_CodigoBarras:string;
begin
   result:=Self.CODIGOBARRAS;
end;

end.



