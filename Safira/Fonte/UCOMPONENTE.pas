unit UCOMPONENTE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjCOMPONENTE,
  UessencialGlobal, Tabs,UpesquisaMenu;

type
  TFCOMPONENTE = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    lbLbReferencia: TLabel;
    edtReferencia: TEdit;
    edtDescricao: TEdit;
    lbLbDescricao: TLabel;
    lbLbUnidade: TLabel;
    edtUnidade: TEdit;
    grpVidros: TGroupBox;
    lbLbAreaMinima: TLabel;
    lbLbLarguraIdeal: TLabel;
    lbLbAlturaMinima: TLabel;
    lbLbLarguraMinima: TLabel;
    lbLbAlturaIdeal: TLabel;
    lbLbAlturaMaxima: TLabel;
    lbLbLarguraMaxima: TLabel;
    edtAreaMinima: TEdit;
    edtLarguraIdeal: TEdit;
    edtAlturaMinima: TEdit;
    edtLarguraMinima: TEdit;
    edtAlturaIdeal: TEdit;
    edtAlturaMaxima: TEdit;
    edtLarguraMaxima: TEdit;
    SpeedButtonPrimeiro: TSpeedButton;
    SpeedButtonAnterior: TSpeedButton;
    SpeedButtonProximo: TSpeedButton;
    SpeedButtonUltimo: TSpeedButton;
    btAjuda: TSpeedButton;
//DECLARA COMPONENTES


    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btAjudaClick(Sender: TObject);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCOMPONENTE: TFCOMPONENTE;
  ObjCOMPONENTE:TObjCOMPONENTE;

implementation

uses Upesquisa, UessencialLocal, UDataModulo, UescolheImagemBotao,
  Uprincipal, UAjuda;

{$R *.dfm}





procedure TFCOMPONENTE.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjCOMPONENTE=Nil)
     Then exit;

     If (ObjCOMPONENTE.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;
    Self.Tag:=0;
    ObjCOMPONENTE.free;


end;


procedure TFCOMPONENTE.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbCodigo.caption:='0';


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjCOMPONENTE.status:=dsInsert;

     EdtReferencia.setfocus;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

end;

procedure TFCOMPONENTE.btSalvarClick(Sender: TObject);
begin

     If ObjCOMPONENTE.Status=dsInactive
     Then exit;

     If (EdtReferencia.Text<>'')then  begin
         If (ObjComponente.VerificaReferencia(ObjComponente.Status,lbCodigo.caption, edtReferencia.Text)=true) then
         Begin
             MensagemErro('Refer�ncia j� existe.');
             exit;
         end;
     end;


     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjCOMPONENTE.salvar(true)=False)
     Then exit;

     lbCodigo.caption:=ObjCOMPONENTE.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
        btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
      btopcoes.Visible :=true;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCOMPONENTE.btAlterarClick(Sender: TObject);
begin
    If (ObjCOMPONENTE.Status=dsinactive) and (lbCodigo.caption<>'')
    Then Begin
                habilita_campos(Self);

                ObjCOMPONENTE.Status:=dsEdit;
                EdtReferencia.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                ObjCOMPONENTE.ReferenciaAnterior:=EdtReferencia.Text;
                btnovo.Visible :=false;
                 btalterar.Visible:=false;
                 btpesquisar.Visible:=false;
                 btrelatorio.Visible:=false;
                 btexcluir.Visible:=false;
                 btsair.Visible:=false;
                 btopcoes.Visible :=false;
          End;

end;

procedure TFCOMPONENTE.btCancelarClick(Sender: TObject);
begin
     ObjCOMPONENTE.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
        btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
      btopcoes.Visible :=true;
      lbCodigo.Caption:='';

end;

procedure TFCOMPONENTE.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjCOMPONENTE.Get_pesquisa,ObjCOMPONENTE.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjCOMPONENTE.status<>dsinactive
                                  then exit;

                                  If (ObjCOMPONENTE.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjCOMPONENTE.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFCOMPONENTE.btExcluirClick(Sender: TObject);
begin
     If (ObjCOMPONENTE.status<>dsinactive) or (lbCodigo.caption='')
     Then exit;

     If (ObjCOMPONENTE.LocalizaCodigo(lbCodigo.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjCOMPONENTE.exclui(lbCodigo.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCOMPONENTE.btRelatorioClick(Sender: TObject);
begin
//    ObjCOMPONENTE.Imprime(lbCodigo.caption);
end;

procedure TFCOMPONENTE.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFCOMPONENTE.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFCOMPONENTE.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFCOMPONENTE.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    If Key=VK_Escape
    Then Self.Close;
       if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    If (Key = VK_F2) then
    begin

           FAjuda.PassaAjuda('CADASTRO DE COMPONENTES');
           FAjuda.ShowModal;
    end;

end ;

procedure TFCOMPONENTE.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFCOMPONENTE.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjCOMPONENTE do
    Begin
        Submit_Codigo(lbCodigo.caption);
        Submit_Referencia(edtReferencia.text);
        Submit_Descricao(edtDescricao.text);
        Submit_Unidade(edtUnidade.text);
        Submit_AlturaIdeal(edtAlturaIdeal.text);
        Submit_LarguraIdeal(edtLarguraIdeal.text);
        Submit_AlturaMinima(edtAlturaMinima.text);
        Submit_LarguraMinima(edtLarguraMinima.text);
        Submit_AlturaMaxima(edtAlturaMaxima.text);
        Submit_LarguraMaxima(edtLarguraMaxima.text);
        Submit_AreaMinima(edtAreaMinima.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFCOMPONENTE.LimpaLabels;
begin
//LIMPA LABELS
end;

function TFCOMPONENTE.ObjetoParaControles: Boolean;
begin
  Try
     With ObjCOMPONENTE do
     Begin
        lbCodigo.caption:=Get_Codigo;
        EdtReferencia.text:=Get_Referencia;
        EdtDescricao.text:=Get_Descricao;
        EdtUnidade.text:=Get_Unidade;
        EdtAlturaIdeal.text:=Get_AlturaIdeal;
        EdtLarguraIdeal.text:=Get_LarguraIdeal;
        EdtAlturaMinima.text:=Get_AlturaMinima;
        EdtLarguraMinima.text:=Get_LarguraMinima;
        EdtAlturaMaxima.text:=Get_AlturaMaxima;
        EdtLarguraMaxima.text:=Get_LarguraMaxima;
        EdtAreaMinima.text:=Get_AreaMinima;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFCOMPONENTE.TabelaParaControles: Boolean;
begin
     If (ObjCOMPONENTE.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;


procedure TFCOMPONENTE.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjComponente.PrimeiroRegistro = false)then
    exit;

    ObjComponente.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFCOMPONENTE.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigo.caption='')then
    lbCodigo.caption:='0';

    if  (ObjComponente.RegistoAnterior(StrToInt(lbCodigo.caption)) = false)then
    exit;

    ObjComponente.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFCOMPONENTE.btProximoClick(Sender: TObject);
begin
    if (lbCodigo.caption='')then
    lbCodigo.caption:='0';

    if  (ObjComponente.ProximoRegisto(StrToInt(lbCodigo.caption)) = false)then
    exit;

    ObjComponente.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFCOMPONENTE.btUltimoClick(Sender: TObject);
begin
    if  (ObjComponente.UltimoRegistro = false)then
    exit;

    ObjComponente.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;



procedure TFCOMPONENTE.FormShow(Sender: TObject);
begin
       
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     ColocaUpperCaseEdit(Self);

     Try
        ObjCOMPONENTE:=TObjCOMPONENTE.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

    FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     LbCodigo.caption:='';


     habilita_botoes(Self);
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE COMPONENTE')=False)
     Then desab_botoes(Self);

     if(Tag<>0)
     then
     begin
         if(ObjCOMPONENTE.LocalizaCodigo(IntToStr(Tag))= True)
         then begin
              ObjCOMPONENTE.TabelaparaObjeto;
              self.ObjetoParaControles;

         end;
     end;
end;

procedure TFCOMPONENTE.btAjudaClick(Sender: TObject);
begin
     FAjuda.PassaAjuda('CADASTRO DE COMPONENTE');
     FAjuda.ShowModal;
end;

end.
