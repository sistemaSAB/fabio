object FNFE: TFNFE
  Left = 616
  Top = 170
  Width = 619
  Height = 601
  Caption = 'Cadastro NFE - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 516
    Width = 603
    Height = 47
    Align = alBottom
    Color = 3355443
    TabOrder = 1
    object btcancelar: TBitBtn
      Left = 115
      Top = 4
      Width = 108
      Height = 38
      Caption = '&Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 3
      Top = 4
      Width = 108
      Height = 38
      Caption = '&Pesquisar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btpesquisarClick
    end
    object btsair: TBitBtn
      Left = 227
      Top = 4
      Width = 108
      Height = 38
      Caption = '&Sair'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btsairClick
    end
  end
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 603
    Height = 516
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    OnChange = GuiaChange
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Principal'
      object LbCODIGO: TLabel
        Left = 5
        Top = 12
        Width = 44
        Height = 13
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbSTATUSNOTA: TLabel
        Left = 5
        Top = 55
        Width = 95
        Height = 13
        Caption = 'Status da Nota'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbARQUIVO: TLabel
        Left = 5
        Top = 143
        Width = 51
        Height = 13
        Caption = 'Arquivo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbPROTOCOLOCANCELAMENTO: TLabel
        Left = 5
        Top = 186
        Width = 158
        Height = 13
        Caption = 'Protocolo Cancelamento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbARQUIVOCANCELAMENTO: TLabel
        Left = 5
        Top = 230
        Width = 147
        Height = 13
        Caption = 'Arquivo Cancelamento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 5
        Top = 274
        Width = 76
        Height = 13
        Caption = 'DT do Canc.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 101
        Top = 274
        Width = 159
        Height = 13
        Caption = 'Motivo do Cancelamento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 5
        Top = 99
        Width = 71
        Height = 13
        Caption = 'Certificado'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 120
        Top = 55
        Width = 103
        Height = 13
        Caption = 'Recibo de Envio'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 5
        Top = 322
        Width = 143
        Height = 13
        Caption = 'Protocolo Inutiliza'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 5
        Top = 366
        Width = 132
        Height = 13
        Caption = 'Arquivo Inutiliza'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 5
        Top = 410
        Width = 76
        Height = 13
        Caption = 'DT do Canc.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 101
        Top = 410
        Width = 159
        Height = 13
        Caption = 'Motivo do Cancelamento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdtCODIGO: TEdit
        Left = 5
        Top = 28
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object EdtSTATUSNOTA: TEdit
        Left = 5
        Top = 71
        Width = 57
        Height = 19
        MaxLength = 1
        TabOrder = 1
      end
      object EdtARQUIVO: TEdit
        Left = 5
        Top = 159
        Width = 500
        Height = 19
        MaxLength = 500
        TabOrder = 4
      end
      object EdtPROTOCOLOCANCELAMENTO: TEdit
        Left = 5
        Top = 202
        Width = 500
        Height = 19
        MaxLength = 100
        TabOrder = 5
      end
      object EdtARQUIVOCANCELAMENTO: TEdit
        Left = 5
        Top = 246
        Width = 500
        Height = 19
        MaxLength = 500
        TabOrder = 6
      end
      object edtdatacancelamento: TMaskEdit
        Left = 5
        Top = 290
        Width = 84
        Height = 19
        EditMask = '99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 7
        Text = '  /  /    '
      end
      object edtmotivocancelamento: TEdit
        Left = 101
        Top = 290
        Width = 404
        Height = 19
        MaxLength = 500
        TabOrder = 8
      end
      object edtcertificado: TEdit
        Left = 5
        Top = 115
        Width = 500
        Height = 19
        MaxLength = 100
        TabOrder = 3
      end
      object edtReciboEnvio: TEdit
        Left = 120
        Top = 71
        Width = 380
        Height = 19
        MaxLength = 100
        TabOrder = 2
      end
      object edtprotocoloinutilizacao: TEdit
        Left = 5
        Top = 338
        Width = 499
        Height = 19
        MaxLength = 100
        TabOrder = 9
      end
      object edtarquivoinutilizacao: TEdit
        Left = 5
        Top = 382
        Width = 499
        Height = 19
        MaxLength = 500
        TabOrder = 10
      end
      object edtdatainutilizacao: TMaskEdit
        Left = 5
        Top = 426
        Width = 84
        Height = 19
        EditMask = '99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 11
        Text = '  /  /    '
      end
      object edtmotivoinutilizacao: TEdit
        Left = 101
        Top = 426
        Width = 404
        Height = 19
        MaxLength = 500
        TabOrder = 12
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Arquivos'
      object WBResposta: TWebBrowser
        Left = 0
        Top = 39
        Width = 595
        Height = 449
        Align = alClient
        TabOrder = 0
        ControlData = {
          4C0000007F3D0000682E00000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 595
        Height = 39
        Align = alTop
        TabOrder = 1
        object BTXMLNF: TButton
          Left = 8
          Top = 8
          Width = 143
          Height = 25
          Caption = 'XML NF'
          TabOrder = 0
          OnClick = BTXMLNFClick
        end
        object btXMLCANC: TButton
          Left = 152
          Top = 8
          Width = 143
          Height = 25
          Caption = 'XML CANCELAMENTO'
          TabOrder = 1
          OnClick = btXMLCANCClick
        end
        object BTXMLINU: TButton
          Left = 296
          Top = 8
          Width = 143
          Height = 25
          Caption = 'XML INUTILIZA'#199#195'O'
          TabOrder = 2
          OnClick = BTXMLINUClick
        end
        object btresgatacampo: TButton
          Left = 448
          Top = 8
          Width = 145
          Height = 25
          Caption = 'Resgatar Campo NF'
          TabOrder = 3
          OnClick = btresgatacampoClick
        end
      end
    end
  end
end
