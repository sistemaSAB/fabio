//======================================================================
//= == == = == Unit que cria as figuras dinamicas na tela == = == = == =
//= == = == == = = = == Criado por Rodolfo Rodovalho = = == = == == == =
//======================================================================


unit CodigoFigura;

{Parte que cria um semicurlo}
{
//(Xinicial, Yinicial, largura, altura, posi��o da primeira linha, posi��o da segunda linha)
//PaintBoxTela.Canvas.Pie(80,80,155,156,180,118, 100,118);
//PaintBoxTela.Canvas.Pie(80,80,155,155,70,118,300,120);
//PaintBoxTela.Canvas.Pie(140,140,155,155,70,150,300,140);
//PaintBoxTela.Canvas.Pie(100,100,115,115,70,110,300,100);
//Espiral(Canvas,200,200,200,32);
}

//===================
//Esse abaixo �  op��o 3
//Canvas.pie(0, 0, Width, Height, width, round(height/2), 0, round(height/2)); //begin Canvas.MoveTo(0,0); Canvas.LineTo(0,height); end;  //linha


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Ferragens,UessencialGlobal{, unit11};

type
  TDesenho = class(TPaintBox)
  private
    FIsButtonDown: array[TMouseButton] of Boolean;
    FP1, FP2: TPoint;
    FP1new: TPoint;
    FGripId: Integer;
    FormPai: TForm;
  public
    ListaNomesMedidas:TStringList;
    PermiteARRASTAR:Boolean;
    ContFig : Integer;   //Identificador da figura dentro da tela
    TipoFigura : Integer;  // Tipo da Figura : Elipse, Retangulo, Reta
    Color : TColor;    // Cor da Figura
    Botao : Boolean;   //Indica se o botao foi clicado ou nao
    pt1 : TPoint;  // TopLeft da Figura
    pt2 : TPoint;  // BottomRight da Figura
    habilit : Boolean;
    escolha : Boolean;
    //ferras : Integer;
    ColorDialog: TColorDialog;
    tmpColor : Tcolor;
    move : Boolean ; //Indica que o mouse esta sendo movimentado em cima da figura
    pontos : array[0..200] of Tpoint;
    tam : integer; // Define aquantidade de pontos que deve ser desenhado
    NomeFigura:string;
    constructor Create(PaintBox1: TpaintBox; Cor: TColor; Fig: Integer; Cont: Integer; pt1:tpoint; pt2:tpoint; var points : array of Tpoint; tamanho:integer;Nome:string); virtual;
    //destructor free;
    procedure Paint(); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure PassaForm(Formulario: TForm);
    function NormalizeRect (ARect: TRect): TRect;
    procedure arrumaPontos();
    procedure Click();  override;
    function RetornaQuantidadeStringList():integer;
    //procedure DblClick(); override;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;

  end;

implementation

uses UescolheImagemBotao;

//Procedimento que  verifica o evento de mouse leave de uma figura
procedure TDesenho.CMMouseLeave(var Message: TMessage);
begin
  if(TipoFigura < 4) or (TipoFigura>=22) then
    Color := tmpColor
  else Color := ClBlack;
  move := false;

  Repaint;

end;
{
destructor TDesenho.free;
begin
    ListaNomesMedidas.Free;

end; }

//Construtor da classe
//paintbox, cor, tipo da figura, e o numero dela na contagem
constructor TDesenho.Create(PaintBox1: TPaintBox; Cor: TColor; Fig:Integer; Cont: Integer; pt1 :Tpoint; pt2: Tpoint; var points : array of Tpoint; tamanho: integer;Nome:string);
var    i : integer;
begin
  inherited
  Create(PaintBox1);
  Color := Cor;  //seta a cor
  TipoFigura := Fig;   //seta a figura a ser criada
  ContFig := cont;
  Self.pt1 := pt1;  //ponto inicial
  Self.pt2 := pt2;  //ponto final
  ColorDialog:= TColorDialog.Create(nil);
  ListaNomesMedidas:=TStringList.Create;
  //escolha := false;
  tmpColor := cor;
  //A principio ta vindo vazio, mas tem que mudar,
  //tem que procurar uma forma de que o texto n�o zue tudo...
  NomeFigura:=Nome;
  tam := tamanho;
  //Se a figura a ser criada for do tipo poligono entao recebo pontos e arrumo-os
  if(tipoFigura=3)then
  begin
    //showmessage(inttostr(tam));
    for i := 0 to tam do
        pontos[i] := points[i];
    arrumaPontos(); //chamada do procedimento que arruma os pontos do poligon
  end;
end;

Function TDesenho.RetornaQuantidadeStringList:Integer;
begin
    result:=ListaNomesMedidas.Count;
   { ShowMessage('');
    if(ListaNomesMedidas=nil)
    then ListaNomesMedidas:=TStringList.Create;
    ListaNomesMedidas.Add(Texto+';'+X+';'+Y+';');      }
end;

///Corrige a posi��o dos pontos da figura na hora da expans�o da figura
function TDesenho.NormalizeRect (ARect: TRect): TRect;
var
  tmp: Integer;
begin
      if (ARect.Bottom < ARect.Top) then
      begin
        tmp := ARect.Bottom;
        ARect.Bottom := ARect.Top;
        ARect.Top := tmp;
      end;
      if (ARect.Right < ARect.Left) then
      begin
        tmp := ARect.Right;
        ARect.Right := ARect.Left;
        ARect.Left := tmp;
      end;
      Result := ARect;
end;

//Arruma os pontos do poligon desenhado
procedure TDesenho.arrumaPontos();
var i: integer;
    menorX : integer;
    menorY : integer;
begin
    menorX := pontos[0].x;
    menorY := pontos[0].Y;
    //showmessage(inttostr(pontos[1].x));

    for i := 1 to tam do
    begin
       if(pontos[i].X < menorX)then
       begin
          menorX:= pontos[i].X;
       end;
       if(pontos[i].Y < menorY)then
       begin
          menorY:= pontos[i].Y;
       end;
    end;

    for i := 0 to tam do
    begin
        pontos[i].X := pontos[i].X - menorX;
        pontos[i].Y := pontos[i].Y - menorY;
    end;
end;

//Procedimento que cria os cantos de redimensionamento da Figura
procedure DrawGrip(const ACanvas: TCanvas; const ARect: TRect);
begin
  ACanvas.Pen.Width := 1;           //Largura da Caneta
  ACanvas.Brush.Color := clWhite;  //Cor de Preenchimento: Branco
  ACanvas.FillRect(ARect);
  ACanvas.Rectangle(ARect);
end;

//Desenha a Figura
procedure TDesenho.Paint();
var
   i: integer;
   Arect : Trect;
   Texto,aux:String;
   PosX,PosY:string;

begin
  inherited;
  Canvas.Pen.Width := 1;     //Largura da Caneta

  if(tipoFigura < 4) or (TipoFigura>=22) then
    Canvas.Brush.Color := Color   //Cor de Preenchimento
  else begin
      if (move = false) then
      begin
        Canvas.Brush.Color := ClBlack  //Se for uma ferragem entao a cor padrao � preta
      end
      else Canvas.Brush.Color := Color;
  end;

  //===== Parte para colocar o tracejado envolta da figura
  if(escolha = true) then
  begin
     //Se tipo da figura for 25, bolinhas de representa��o de objetos, n�o fica legal desenhar
     // os cantos do objeto.
     if(TipoFigura<>25) then
     begin
       arect.Left := 0;
       arect.Top := 0;
       arect.Right := width;
       arect.Bottom := height;
       Canvas.DrawFocusRect(ARect);
     end;
  end;
  //======== FimParte  

  //semi circulo: Canvas.chord(0, 0, Width, Height, 0, round(height/2), width, round(height/2));
  //Escolha da figura
  case tipoFigura of
        1: Canvas.Rectangle(0, 0, Width,Height);  //retangulo
        2: Canvas.Ellipse(0,0, Width, Height);    //circunferencia
        3: Canvas.Polygon(Slice(pontos,tam));      //poligon
        4: Canvas.Rectangle(0,0, Width, Height);  //ferragem
        5: Canvas.Ellipse(0,0, Width, Height);    //ferragem
        6: Canvas.Polygon([Point(0,7),Point(7, 7), Point(7, 0), Point(14, 0), Point(14, 7), Point(21, 7),Point(21, 14), Point(0, 14), Point(0, 7)]);
        7: Canvas.Polygon([Point(0,0),Point(21, 0), Point(21, 7), Point(14, 7), Point(14, 14), Point(7, 14),Point(7, 7), Point(0, 7), Point(0, 0)]);
        8: Canvas.Polygon([Point(0,0),Point(6, 0), Point(6, 12), Point(12, 12), Point(12, 18), Point(0, 18), Point(0, 0)]);
        9: Canvas.Polygon([Point(0,12),Point(6, 12), Point(6, 0), Point(12, 0), Point(12, 18), Point(0, 18), Point(0, 12)]);
       10: Canvas.Polygon([Point(0,0),Point(12, 0), Point(12, 6), Point(6, 6), Point(6, 18), Point(0, 18), Point(0, 0)]);
       11: Canvas.Polygon([Point(0,0),Point(12, 0), Point(12, 18), Point(6, 18), Point(6, 6), Point(0, 6), Point(0, 0)]);
       12: Canvas.Pie(0,0,15,15,0,7,60,10);
       13: Canvas.Pie(0,0,15,15,60,10,0,7);
       14: Canvas.Polygon([Point(0,0),Point(7, 0), Point(7, 18), Point(0, 18), Point(0, 0)]);
       15: Canvas.StretchDraw(ClientRect,Form9.Image12.Picture.Bitmap);
       16: Canvas.StretchDraw(ClientRect,Form9.Image13.Picture.Bitmap);
       17: Canvas.StretchDraw(ClientRect,Form9.Image14.Picture.Bitmap);
       18: Canvas.StretchDraw(ClientRect,Form9.Image15.Picture.Bitmap);
       19: Canvas.StretchDraw(ClientRect,Form9.Image16.Picture.Bitmap);
       20: Canvas.LineTo(Width, Height);
       21: Canvas.Rectangle(0,0, Width, Height);  //retangulo (linha de medida)
       22: Canvas.Pie(0, 0, Width, Width,Width, Height,0, Height); //meia esfera
       23: Canvas.Pie(0, 0, Width, Width,Trunc(Width/2),0,0, Height); //1/4 de esfera(Esquerda)
       24: Canvas.Pie(0, 0, Width, Width,Trunc(Width/2),0,0, Height);
       25: Canvas.Ellipse(0,0, 30, 30);
       26: Canvas.Ellipse(0,0, 9, 10); //Puxador
  end;

  //Desenha Cantos no retangulo e no circulo
  if(botao) and (tipoFigura<3) then
  begin
    DrawGrip(Canvas, Bounds(0, 0, 6, 6));  //Desenha o Canto Superior Esquerdo
    //DrawGrip(Canvas, Bounds(Width - 6, 0, 6, 6));  //Desenha Canto Superior Direito
    DrawGrip(Canvas, Bounds(Width - 6, Height - 6, 6, 6));  //Desenha Canto Inferior Direito
    //DrawGrip(Canvas, Bounds(0, Height - 6, 6, 6));   //Desenha Canto Inferior Esquerdo
  end;

  //Se tipo da figura for 25, bolinhas de representa��o de objetos, n�o fica legal desenhar
  // os cantos do objeto.
  if(botao) and (tipoFigura>=22) and (tipoFigura<>25) then
  begin
    DrawGrip(Canvas, Bounds(0, 0, 6, 6));  //Desenha o Canto Superior Esquerdo
    //DrawGrip(Canvas, Bounds(Width - 6, 0, 6, 6));  //Desenha Canto Superior Direito
    DrawGrip(Canvas, Bounds(Width - 6, Height - 6, 6, 6));  //Desenha Canto Inferior Direito
    //DrawGrip(Canvas, Bounds(0, Height - 6, 6, 6));   //Desenha Canto Inferior Esquerdo
  end;

  //====== Parte que desenha cantos em poligonos ===========
  //Desenha os cantos do poligono
  if(botao) and (tipoFigura=3)then
  begin
     for i := 0 to tam do
        DrawGrip(Canvas, Bounds(pontos[i].x, pontos[i].Y, 6, 6));
  end;
 /// ======= Fim da parte ========

  //========= Parte que escreve nome na figura ======
  //Adiciona nome na figura (funciona belezinha - s� descomentar a linha abaixo)
  if (TipoFigura <= 3)  or (TipoFigura>=22) then
  begin
    Canvas.Font.Color := clBlack;
    Canvas.Font.Style := [fsBold];

    //formato listanomesmedidas TEXTO;POSX;POSY
    //Sempre na posi��o 0 vai ser o do componente, depois as ferragens
    for i:=0 to ListaNomesMedidas.Count-1 do
    begin
       if(i=0) then
       begin
         Canvas.TextOut((trunc (width/2))-20,(trunc (height/2)-23),ListaNomesMedidas[0]);
       end
       else
       begin
         Texto:=retornaPalavrasAntesSimbolo(ListaNomesMedidas[i],';');
         PosX:=retornaPalavrasAntesSimbolo(retornaPalavrasDepoisSimbolo2(ListaNomesMedidas[i],';'),';');
         PosY:=RetornaPalavrasAntesSimbolo(retornaPalavrasDepoisSimbolo2(retornaPalavrasDepoisSimbolo2(ListaNomesMedidas[i],';'),';'),';');
         Canvas.TextOut(strtointDef(PosX,0),StrToIntDef(PosY,0),Texto);
       end;
    end;
  end;

  if(TipoFigura=25) then
  begin
       Canvas.Font.Color  :=  clWhite;
       Canvas.Font.Style  :=  [fsBold];
       Canvas.Pen.Color   :=  $00A2663E;

       Canvas.TextOut((trunc (width/2)-5),(trunc (height/2)-10),NomeFigura);
       Color:=$00A2663E;
  end;

  //========== Fim da parte ===========}
end;


// =========== Procedimento que no click da figura coloca tracejado na figura ======
procedure TDesenho.Click();
begin
   escolha := true;
   botao:=True;
   FormPai.Tag := ContFig;
   repaint;
end;
//============= Fim ===================================================================}

{//========== Procedimento que abre a op��o de cor no duplo click ========
procedure TDesenho.DblClick();
begin
    ColorDialog.Execute();
    Color := ColorDialog.Color;
    tmpColor := Color;
end; }
//=========== Fim ========================================================}

procedure TDesenho.PassaForm(Formulario: TForm);
begin
  Self.FormPai := Formulario;
end;

//Evento do MouseDown
procedure TDesenho.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
//var i:integer;
begin
  FIsButtonDown[Button] := True;  //Indica que o botao do mouse foi pressionado
  FP1 := Point(X, Y);   //Pega a posi��o de onde o mouse esta
  FP2 := Point(Width - X, Height - Y);

  //botao indica se a figura esta ou habilitada para edi��o
  if(botao)then
    FGripId := 0
  else FGripId := -1;

  //Verifica se o clique do mouse aconteceu em algum dos cantos
  //PtInRect verifica se um determinado ponto esta dentro de um retangulo
  if(botao) then
  begin
    if(tipoFigura <3 ) or (TipoFigura>=22) then
    begin
        if PtInRect(Bounds(0, 0, 6, 6), FP1) then
          FGripId := 1   //Quadrado Superior Esquerdo
        else
        if PtInRect(Bounds(Width - 6, 0, 6, 6), FP1) then
          FGripId := 2   //Quadrado Superior Direito
        else
        if PtInRect(Bounds(Width - 6, Height - 6, 6, 6), FP1) then
          FGripId := 3   //Quadrado Inferior Direito
        else
        if PtInRect(Bounds(0, Height - 6, 6, 6), FP1) then
          FGripId := 4;  //Quadrado Inferior Esquerdo
    end
  end;

  FormPai.Tag := ContFig;

end;

procedure TDesenho.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FIsButtonDown[Button] := False;    //Indica que o botao do mouse foi solto
  pt1 := BoundsRect.TopLeft;
  pt2 := BoundsRect.BottomRight;

  //Arruma a largura final da figura
  if(width < 1) then
      width := 1;

  //Arruma a altura final da figura
  if height < 1 then
      height := 1;


end;

procedure TDesenho.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  P: TPoint;
  R: TRect;
begin

  FP1new := Point(X, Y);   //Pega a posi��o de onde o mouse esta

  if FIsButtonDown[mbLeft] then     //Se o botao pressionado do mouse for o botao esquerdo
  begin
      if(width >= 6) then
      begin
        if(height >= 6) then
        begin
          //ScreenToClient converte pontos da tela para o cliente delphi
          P := Parent.ScreenToClient(ClientToScreen(Point(X, Y)));
          R := BoundsRect;
          case FGripId of
            //Arrasto da Figura
            0: R := Bounds(P.X - FP1.X, P.Y - FP1.Y, Width, Height);
            //Quadrado Superior Esquerdo
            1: R := Rect(P.X - FP1.X, P.Y - FP1.Y, BoundsRect.Right, BoundsRect.Bottom);
            //2: R := Rect(BoundsRect.Left, P.Y - FP1.Y, P.X + FP2.X, BoundsRect.Bottom);
            //Quadrado Inferior Direito
            3: R := Rect(BoundsRect.Left, BoundsRect.Top, P.X + FP2.X, P.Y + FP2.Y);
            //4: R := Rect(P.X - FP1.X, BoundsRect.Top, BoundsRect.Right, P.Y + FP2.Y);
            5: R := Rect(P.X - FP1.X, BoundsRect.Top, BoundsRect.Right, P.Y + FP2.Y);
          end;
          BoundsRect := NormalizeRect(R);  //Corre��o dos pontos
        end
        else height := 6;
      end
      else width := 6;
  end
  else
  begin
        //ate 24, pois 25 � tamanho fixo para jogar os servi�os
        if (TipoFigura < 4) or (TipoFigura=22) or (TipoFigura=23) or (TipoFigura=24)  then
        begin
          if PtInRect(Bounds(0, 0, 6, 6), FP1new) or PtInRect(Bounds(Width - 6, Height - 6, 6, 6), FP1new)  then
            screen.cursor := crSizeNWSE
          else
            screen.cursor := crHandPoint;
        end
        else
            screen.cursor := crHandPoint;

        if(TipoFigura<>25)
        then color := clAqua;  //quando mouse esta em cima da figura muda a cor da figura

        move := true;
        repaint;

  end;
end;

end.
