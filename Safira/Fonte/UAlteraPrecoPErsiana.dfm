object FAlteraPrecoPersiana: TFAlteraPrecoPersiana
  Left = 347
  Top = 317
  Width = 739
  Height = 149
  AutoSize = True
  Caption = 'Altera'#231#227'o de Pre'#231'o  - PERSIANA'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LbPrecoCusto: TLabel
    Left = 0
    Top = 9
    Width = 87
    Height = 13
    Caption = 'Pre'#231'o de Custo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 541
    Top = 0
    Width = 82
    Height = 16
    Caption = 'Pre'#231'o Pago: '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbprecopago: TLabel
    Left = 641
    Top = 0
    Width = 82
    Height = 16
    Alignment = taRightJustify
    Caption = 'Pre'#231'o Pago: '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object EdtPrecoCusto: TEdit
    Left = 100
    Top = 7
    Width = 80
    Height = 21
    MaxLength = 9
    TabOrder = 0
  end
  object btgravar: TButton
    Left = 431
    Top = 71
    Width = 75
    Height = 25
    Caption = '&Gravar'
    TabOrder = 3
    OnClick = btgravarClick
  end
  object btcancelar: TButton
    Left = 511
    Top = 71
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    TabOrder = 4
    OnClick = btcancelarClick
  end
  object GroupBoxMargem: TGroupBox
    Left = 0
    Top = 33
    Width = 206
    Height = 78
    Caption = 'Margem'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object LbPorcentagemFornecido: TLabel
      Left = 11
      Top = 36
      Width = 55
      Height = 13
      Caption = 'Fornecido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object LbPorcentagemRetirado: TLabel
      Left = 11
      Top = 56
      Width = 48
      Height = 13
      Caption = 'Retirado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object LbPorcentagemInstalado: TLabel
      Left = 11
      Top = 16
      Width = 53
      Height = 13
      Caption = 'Instalado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 158
      Top = 16
      Width = 12
      Height = 13
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 158
      Top = 36
      Width = 12
      Height = 13
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 158
      Top = 56
      Width = 12
      Height = 13
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object EdtPorcentagemInstaladoGDC: TEdit
      Left = 85
      Top = 12
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnExit = EdtPorcentagemInstaladoGDCExit
    end
    object EdtPorcentagemFornecidoGDC: TEdit
      Left = 85
      Top = 32
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      OnExit = EdtPorcentagemFornecidoGDCExit
    end
    object EdtPorcentagemRetiradoGDC: TEdit
      Left = 85
      Top = 52
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      OnExit = EdtPorcentagemRetiradoGDCExit
    end
  end
  object GroupBoxPrecoVenda: TGroupBox
    Left = 216
    Top = 34
    Width = 206
    Height = 77
    Caption = 'Pre'#231'o Venda'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object LbPrecoVendaInstalado: TLabel
      Left = 11
      Top = 15
      Width = 53
      Height = 13
      Caption = 'Instalado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object LbPrecoVendaFornecido: TLabel
      Left = 11
      Top = 35
      Width = 55
      Height = 13
      Caption = 'Fornecido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object LbPrecoVendaRetirado: TLabel
      Left = 11
      Top = 55
      Width = 48
      Height = 13
      Caption = 'Retirado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 79
      Top = 15
      Width = 15
      Height = 13
      Caption = 'R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 79
      Top = 35
      Width = 15
      Height = 13
      Caption = 'R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 79
      Top = 55
      Width = 15
      Height = 13
      Caption = 'R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object EdtPrecoVendaInstaladoGDC: TEdit
      Left = 97
      Top = 11
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
    end
    object EdtPrecoVendaFornecidoGDC: TEdit
      Left = 97
      Top = 31
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
    end
    object EdtPrecoVendaRetiradoGDC: TEdit
      Left = 97
      Top = 51
      Width = 72
      Height = 19
      Color = 15663069
      Ctl3D = False
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
    end
  end
end
