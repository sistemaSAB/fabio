unit UNfeDigitada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, Grids,
  DBGrids, DB, Menus,UmenuNfe,UobjPRODNFEDIGITADA,UobjPedidoObjetos,IBQuery,UobjTransmiteNFE,
  ImgList, ComCtrls,StrUtils, pngimage,FileCtrl,ShellAPI;

type
  TFNfeDigitada = class(TForm)
    pgcGuia: TPageControl;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    bt1: TSpeedButton;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btexcluir: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btOpcoes: TBitBtn;
    BtSair: TBitBtn;
    Panel4: TPanel;
    imagemRodape: TImage;
    lbl8: TLabel;
    lbTotalProdutos: TLabel;
    Label11: TLabel;
    lbtotalNota: TLabel;
    Label17: TLabel;
    lbTotalFrete: TLabel;
    Label21: TLabel;
    lbTotalDesconto: TLabel;
    Label24: TLabel;
    tsNFeDigitada: TTabSheet;
    panelCabecalho: TPanel;
    imagemFundo: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    lbNomeCliente: TLabel;
    lbNomeTransportadora: TLabel;
    Label18: TLabel;
    lbNFE: TLabel;
    Label25: TLabel;
    lbNota: TLabel;
    Label14: TLabel;
    lbNomeFornecedor: TLabel;
    lboperacao: TLabel;
    lb1: TLabel;
    lbBaixaEstoque: TLabel;
    memoInformacoes: TMemo;
    edtCliente: TEdit;
    edtTransportadora: TEdit;
    edtDataEmissao: TMaskEdit;
    edtDataSaida: TMaskEdit;
    edtHoraSaida: TMaskEdit;
    edtFornecedor: TEdit;
    chkBaixaEstoque: TCheckBox;
    panelProdutos: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    lbDescricaoProduto: TLabel;
    Label20: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    v: TLabel;
    label55: TLabel;
    lbCor: TLabel;
    edtProduto: TEdit;
    edtQuantidade: TEdit;
    edtValorproduto: TEdit;
    edtDesconto: TEdit;
    btGravarProduto: TBitBtn;
    btCancelarProduto: TBitBtn;
    btExcluirProduto: TBitBtn;
    edtValorFrete: TEdit;
    comboTipo: TComboBox;
    edtCor: TEdit;
    dsProdutos: TDataSource;
    menuItem: TPopupMenu;
    Servios1: TMenuItem;
    Produto1: TMenuItem;
    menuNFE: TPopupMenu;
    Consultarservio1: TMenuItem;
    Consultardoarquivo1: TMenuItem;
    Consultapelachavedeacesso1: TMenuItem;
    PopUpMenu2: TPopupMenu;
    NFeNova1: TMenuItem;
    il2: TImageList;
    DBGridProdutos: TDBGrid;
    tsProximoNNfe: TTabSheet;
    panelProximoNNfe: TPanel;
    bt_ok: TSpeedButton;
    comboStatus: TComboBox;
    Label32: TLabel;
    edtNumeroNFEAte: TEdit;
    edtNumeroNFEde: TEdit;
    Label31: TLabel;
    edtNF1: TEdit;
    Label30: TLabel;
    tsConfAarquivo: TTabSheet;
    pageArquivoConf: TPageControl;
    TabSheet5: TTabSheet;
    Labe45: TLabel;
    btCertificado: TSpeedButton;
    SpeedButton7: TSpeedButton;
    GroupBox4: TGroupBox;
    Label37: TLabel;
    ckVisualizar: TCheckBox;
    cbUF: TComboBox;
    rgTipoAmb: TRadioGroup;
    ckZeraImpostoSimples: TCheckBox;
    ckExpandirLogoMarca: TCheckBox;
    rgFormaEmissao: TRadioGroup;
    rgTipoDanfe: TRadioGroup;
    edtCertificado: TEdit;
    TabSheet6: TTabSheet;
    Label33: TLabel;
    sbtLogoMarca: TSpeedButton;
    btSalvarResp: TSpeedButton;
    Label34: TLabel;
    SpeedButton2: TSpeedButton;
    Label35: TLabel;
    SpeedButton3: TSpeedButton;
    Label36: TLabel;
    SpeedButton4: TSpeedButton;
    Label38: TLabel;
    SpeedButton5: TSpeedButton;
    Label39: TLabel;
    SpeedButton6: TSpeedButton;
    lbCaminhoXML: TLabel;
    SpeedButton8: TSpeedButton;
    Label46: TLabel;
    SpeedButton9: TSpeedButton;
    Label48: TLabel;
    Image2: TImage;
    Image3: TImage;
    Label49: TLabel;
    lbCartaCorrecao: TLabel;
    lbCancelamento: TLabel;
    lbInutilizacao: TLabel;
    lbBackup: TLabel;
    lbArquivoRave: TLabel;
    edtLogoMarca: TEdit;
    edtPathLogs: TEdit;
    ckSalvarResp: TCheckBox;
    edtCaminhoXML: TEdit;
    edtCartaCorrecao: TEdit;
    edtCancelamento: TEdit;
    edtInutilizacao: TEdit;
    edtArquivoRave: TEdit;
    edtXMLBackup: TEdit;
    TabSheet7: TTabSheet;
    PageControl2: TPageControl;
    rgSql: TRadioGroup;
    memoSQL: TMemo;
    Panel6: TPanel;
    btSalvarSql: TSpeedButton;
    ckQuebraLinha: TCheckBox;
    Timer1: TTimer;
    OpenDialog1: TOpenDialog;
    ComboIndPag1: TComboBox;
    lb2: TLabel;
    edtoperacao: TEdit;
    Image1: TImage;
    Label12: TLabel;
    edtcfop: TEdit;
    Label13: TLabel;
    lbTotalIpi: TLabel;
    Label16: TLabel;
    lbTotalICMSST: TLabel;
    checkImpostoAutomatico: TCheckBox;
    Label15: TLabel;
    edtHoraEmissao: TMaskEdit;
    Label19: TLabel;
    N1: TMenuItem;
    Recuperaxmlduplicidade1: TMenuItem;
    popMenu1: TPopupMenu;
    VincularNF1: TMenuItem;
    lbEnderecoEntrega: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbTotalBC_ICMSClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtClienteKeyPress(Sender: TObject; var Key: Char);
    procedure edtTransportadoraKeyPress(Sender: TObject; var Key: Char);
    procedure edtProdutoKeyPress(Sender: TObject; var Key: Char);
    procedure edtQuantidadeKeyPress(Sender: TObject; var Key: Char);
    procedure edtValorprodutoKeyPress(Sender: TObject; var Key: Char);
    procedure edtDescontoKeyPress(Sender: TObject; var Key: Char);
    procedure edtCfopKeyPress(Sender: TObject; var Key: Char);
    procedure BtSairClick(Sender: TObject);
    procedure edtValorFreteKeyPress(Sender: TObject; var Key: Char);
    procedure lbNFEMouseLeave(Sender: TObject);
    procedure lbNFEMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbNFEClick(Sender: TObject);
    procedure lbNotaMouseLeave(Sender: TObject);
    procedure lbNotaMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbNotaClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure Consultarservio1Click(Sender: TObject);
    procedure Consultardoarquivo1Click(Sender: TObject);
    procedure Consultapelachavedeacesso1Click(Sender: TObject);
    procedure menuItemPopup(Sender: TObject);
    procedure lbNomeTransportadoraMouseLeave(Sender: TObject);
    procedure lbNomeTransportadoraMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure lbNomeClienteMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure lbNomeClienteMouseLeave(Sender: TObject);
    procedure lbNomeClienteClick(Sender: TObject);
    procedure lbNomeTransportadoraClick(Sender: TObject);
    procedure edtFornecedorKeyPress(Sender: TObject; var Key: Char);
    procedure lbNomeFornecedorClick(Sender: TObject);
    procedure lbNomeFornecedorMouseLeave(Sender: TObject);
    procedure lbNomeFornecedorMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnovoClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btOpcoesClick(Sender: TObject);
    procedure comboTipoEnter(Sender: TObject);
    procedure comboTipoExit(Sender: TObject);
    procedure edtProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtProdutoExit(Sender: TObject);
    procedure edtClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClienteExit(Sender: TObject);
    procedure edtFornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtFornecedorExit(Sender: TObject);
    procedure edtTransportadoraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTransportadoraExit(Sender: TObject);
    procedure DBGridProdutosDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGridProdutosDblClick(Sender: TObject);
    procedure DBGridProdutosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btGravarProdutoClick(Sender: TObject);
    procedure comboTipoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridProdutosCellClick(Column: TColumn);
    procedure btExcluirProdutoClick(Sender: TObject);
    procedure edtDescontoExit(Sender: TObject);
    procedure edtValorFreteExit(Sender: TObject);
    procedure edtValorprodutoExit(Sender: TObject);
    procedure edtCorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Label24Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure bt1Click(Sender: TObject);
    procedure edtCorExit(Sender: TObject);
    procedure edtoperacaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtoperacaoExit(Sender: TObject);
    procedure pgcGuiaChange(Sender: TObject);
    procedure bt_okClick(Sender: TObject);
    procedure btCertificadoClick(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure pageArquivoConfChange(Sender: TObject);
    procedure sbtLogoMarcaClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure Label48Click(Sender: TObject);
    procedure Label49Click(Sender: TObject);
    procedure rgSqlClick(Sender: TObject);
    procedure btSalvarSqlClick(Sender: TObject);
    procedure ckQuebraLinhaClick(Sender: TObject);
    procedure edtDataSaidaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDataEmissaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtHoraSaidaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Image1Click(Sender: TObject);
    procedure edtcfopKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtHoraEmissaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSalvarRespClick(Sender: TObject);
    procedure Recuperaxmlduplicidade1Click(Sender: TObject);
    procedure edtNumeroNFEAteChange(Sender: TObject);
    procedure comboStatusKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure VincularNF1Click(Sender: TObject);
    procedure limpaEnderecoEntrega;
    procedure lbEnderecoEntregaClick(Sender: TObject);
    procedure checaEnderecoEntrega;
  private


     ObjPRODNFEDIGITADA:TObjPRODNFEDIGITADA;
     ObjPedidoObjetos:TObjPedidoObjetos;
     fmenunfe:TFmenuNfe;
     nfeDataAtual:string;

     function TabelaParaControles  ():Boolean;
     function ObjetoParaControles  ():Boolean;
     function controlesParaObjeto  ():Boolean;


     procedure limpaLabelsCabecalho ();
     procedure limpaLabelsProdutos  ();
     procedure limpaLabels ();
     procedure formataGrid ();
     procedure adicionaInfo ();
     procedure retiraMemo (pCodigoProduto:string);

     procedure atualizaLabelsRodape (operador:Char);


     function get_lbTotalProduto  ():string;
     function get_lbTotalDesconto ():string;
     function get_lbTotalFrete    ():string;
     function get_lbTotalNota     ():string;
     function get_lbTotalBC_ICMS  ():string;
     function get_lbTotalICMS     ():string;
     function get_lbTotalIPI      ():string;
     function get_lbTotalICMSST   ():string;

     function getField (CAMPO:string):Currency;
     function deletatabmateriaisvenda:Boolean;
     procedure pegaProximaNFE;
     procedure lerAbaGeral();
     procedure gravaAbaGeral;
     procedure habilitaCampoObrigatorio(edit:TEdit;lb:TLabel);
     procedure lerAbaCaminhos;
     procedure CaminhoXMLExit(Edit: TEdit);
     procedure gravaAbaCaminhos;
     procedure gravaSql;
     
  public
    objTransNFE:tobjtransmitenfe;
  end;

var
  FNfeDigitada: TFNfeDigitada;
  UtilizaServico:Boolean;

implementation

uses UescolheImagemBotao, UessencialGlobal, UDataModulo, Upesquisa,
  UObjTRANSPORTADORA, UobjNFE, Math, UObjNotaFiscal,
  UnotaFiscalEletronica, UNotaFiscal, UCliente, Utransportadora,
  uReferenciaNFe, UObjFornecedor, UFornecedor, UFERRAGEM, UPERFILADO,
  UVIDRO, UKITBOX, UPERSIANA, UDIVERSO, UpesquisaMenu, Uprincipal, UAjuda,
  UobjNFEDIGITADA, UobjParametros, uAcertaImpostoSafira;

{$R *.dfm}

procedure TFNfeDigitada.FormShow(Sender: TObject);
begin

  try

    self.nfeDataAtual := 'NAO';
    if not (ObjParametroGlobal.ValidaParametro('NFE COM DATA ATUAL')) then
      MensagemAviso('Par�metro n�o encontrado "NFE COM DATA ATUAL');
    self.nfeDataAtual := ObjParametroGlobal.Get_Valor;

    self.ObjPRODNFEDIGITADA:=TObjPRODNFEDIGITADA.Create(self);
    ObjPedidoObjetos:=TObjPedidoObjetos.Create(self);
    objTransNFE := TObjTransmiteNFE.Create(self,FDataModulo.IBDatabase);
    objPRODNFEDIGITADA.objTransNFE := objTransNFE;

    self.fmenunfe:=TFmenuNfe.Create(nil);
    self.fmenunfe.PassaObjeto(self.objPRODNFEDIGITADA.ObjNotafiscalObjetos);
    self.dsProdutos.DataSet:=self.objPRODNFEDIGITADA.queryProdutos;
    self.limpaLabels ();
                                                    
    desabilita_campos (panelCabecalho);
    desabilita_campos (panelProdutos);


    FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
    FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
    FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');

    FescolheImagemBotao.PegaFiguraBotaopequeno(btgravarproduto,'BOTAOINSERIR.BMP');
    FescolheImagemBotao.PegaFiguraBotaopequeno(btcancelarproduto,'BOTAOCANCELARPRODUTO.BMP');
    FescolheImagemBotao.PegaFiguraBotaopequeno(btexcluirproduto,'BOTAORETIRAR.BMP');

    retira_fundo_labels(self);
    
    ObjParametroGlobal.ValidaParametro('UTILIZA SERVICO NA NFE DIGITADA?');
    UtilizaServico:=False;
    if (ObjParametroGlobal.Get_Valor = 'SIM') then
      UtilizaServico:=True;
    pgcGuia.TabIndex:=0;

    comboIndPag1.ItemIndex := indicePagmentoNfe;
  finally

    Screen.Cursor:=crDefault;

  end;

end;

procedure TFNfeDigitada.FormClose(Sender: TObject;var Action: TCloseAction);
begin

  if (self.fmenunfe <> nil) then
    FreeAndNil(self.fmenunfe);

  if (self.ObjPRODNFEDIGITADA <> nil) then
    self.ObjPRODNFEDIGITADA.Free;

    ObjPedidoObjetos.Free;

end;

procedure TFNfeDigitada.lbTotalBC_ICMSClick(Sender: TObject);
begin
//
end;

procedure TFNfeDigitada.FormKeyPress(Sender: TObject; var Key: Char);
begin

   if (key = #13) then
        Perform(Wm_NextDlgCtl,0,0)

   else if (Key = #27) then
    self.Close


end;

procedure TFNfeDigitada.edtClienteKeyPress(Sender: TObject; var Key: Char);
begin

  if not (Key in['0'..'9',Chr(8)]) then
      Key:= #0;

end;

procedure TFNfeDigitada.edtTransportadoraKeyPress(Sender: TObject;var Key: Char);
begin

  if not (Key in['0'..'9',Chr(8)]) then
      Key:= #0;

end;

function TFNfeDigitada.TabelaParaControles: Boolean;
begin

   result:=False;

   If not (Self.objPRODNFEDIGITADA.NfeDigitada.TabelaparaObjeto) Then
    Exit;

   If not (ObjetoParaControles) Then
    Exit;

   Result:=True;


   
end;

function TFNfeDigitada.ObjetoParaControles: Boolean;
begin

 Try

     With (Self.objPRODNFEDIGITADA.NfeDigitada) do
     Begin

        edtCliente.Text              := Cliente.Get_CODIGO ();
        lbNomeCliente.Caption        := cliente.Get_Nome   ();
        edtTransportadora.Text       := Transportadora.Get_CODIGO ();
        edtFornecedor.Text           := fornecedor.Get_CODIGO();
        lbNomeFornecedor.Caption     := fornecedor.Get_Fantasia();
        lbNomeTransportadora.Caption := transportadora.Get_NOME ();
        edtoperacao.Text:=Get_Operacao;
        lboperacao.Caption:=get_campoTabela('nome','codigo','taboperacaonf',edtoperacao.Text);

        edtDataEmissao.Text    := get_dataEmissao     ();
        edtDataSaida.Text      := get_dataSaida       ();
        edtHoraSaida.Text      := get_horaSaida       ();
        edtHoraEmissao.Text    := get_HORAEMISSAO     ();
        memoInformacoes.Text   := get_informacoes     ();
        lbCodigo.Caption       := get_codigo          ();
        lbNFE.Caption          := nota.Nfe.Get_CODIGO ();
        lbnota.Caption         := nota.Get_CODIGO     ();

        if(Get_BaixaEstoque='S')
        then chkBaixaEstoque.Checked:=True
        else chkBaixaEstoque.Checked:=False;

        checkImpostoAutomatico.Checked := (get_IMPOSTOAUTOMATICO() = 'S');

        lbTotalProdutos.Caption := formata_valor (get_valorProduto);
        lbTotalDesconto.Caption := formata_valor (get_valorDesconto);
        lbtotalNota.Caption     := formata_valor (Get_valorNF);
        lbTotalFrete.Caption    := formata_valor (get_valorFrete);
        lbTotalIpi.Caption      := formata_valor (get_valoripi);
        lbTotalICMSST.Caption   := formata_valor (get_valoricms_st);

        comboIndPag1.ItemIndex := StrToInt(get_indpag);


        if (lbNota.Caption = '') then
          habilita_campos (panelProdutos)
        else
          desabilita_campos (panelProdutos);

        result:=True;

     End;

 Except
  Result:=False;
 End;

end;

procedure TFNfeDigitada.limpaLabelsCabecalho;
begin

  self.lbNomeCliente.Caption:='';
  self.lbNomeTransportadora.Caption:='';
  self.lbNomeFornecedor.Caption:='';
  self.lbNFE.Caption:='';
  self.lbNota.Caption:='';


end;

function TFNfeDigitada.controlesParaObjeto: Boolean;
begin


  try

    with (self.ObjPRODNFEDIGITADA.NFEDIGITADA) do
    begin


      Submit_CODIGO (lbCodigo.Caption);
      {integer}
      cliente.Submit_CODIGO        (edtCliente.Text);
      transportadora.Submit_CODIGO (edtTransportadora.Text);
      fornecedor.Submit_CODIGO     (edtFornecedor.Text);
      Submit_Operacao(edtoperacao.Text);

      if(chkBaixaEstoque.Checked=True)
      then Submit_BaixaEstoque('S')
      else Submit_BaixaEstoque('N');

      if (checkImpostoAutomatico.Checked) then
        submit_IMPOSTOAUTOMATICO('S')
      else
        submit_IMPOSTOAUTOMATICO('N');

      {data}
      submit_dataEmissao           (edtDataEmissao.Text);
      submit_dataSaida             (edtDataSaida.Text);
      submit_horaSaida             (edtHoraSaida.Text);
      submit_HORAEMISSAO           (edtHoraEmissao.Text);

      {varchar}
      submit_informacoes           (memoInformacoes.Text);

      {currency}
      submit_valorDesconto         (get_lbTotalDesconto);
      Submit_VALORFRETE            (get_lbTotalFrete);
      Submit_VALORNF               (tira_ponto(get_lbTotalNota));
      Submit_VALORPRODUTO          (tira_ponto(get_lbTotalProduto));
      submit_valoripi              (tira_ponto(get_lbTotalIPI));
      submit_valoricms_st          (tira_ponto(get_lbTotalICMSST));


      submit_indpag(comboIndPag1.Text[1]);

    end;

    result:=True;

  except

    result := False;

  end;

end;


procedure TFNfeDigitada.edtProdutoKeyPress(Sender: TObject; var Key: Char);
begin

  if not (Key in['0'..'9',Chr(8)]) then
      Key:= #0;

end;

procedure TFNfeDigitada.limpaLabels;
begin

  lbNomeCliente.Caption:='';
  lbNomeTransportadora.Caption:='';
  lbNomeFornecedor.Caption:='';
  lbDescricaoProduto.Caption:='';
  lbCodigo.Caption:='';
  lbNFE.Caption:='';
  lbNota.Caption:='';
  lbCor.Caption:='';
  lboperacao.Caption := '';

  lbTotalDesconto.Caption:='0,00';
  lbTotalProdutos.Caption:='0,00';
  lbTotalFrete.Caption   :='0,00';
  lbTotalIPI.Caption     :='0,00';
  lbTotalICMSST.Caption  :='0,00';
  lbtotalNota.Caption    :='0,00';
  lbEnderecoEntrega.visible := false;

end;

procedure TFNfeDigitada.edtQuantidadeKeyPress(Sender: TObject;
  var Key: Char);
begin

  ValidaNumeros(tedit(Sender),Key,'float');

end;

procedure TFNfeDigitada.edtValorprodutoKeyPress(Sender: TObject;
  var Key: Char);
begin

  ValidaNumeros(tedit(Sender),Key,'float');

end;

procedure TFNfeDigitada.edtDescontoKeyPress(Sender: TObject; var Key: Char);
begin

  ValidaNumeros(tedit(Sender),Key,'float');

end;

procedure TFNfeDigitada.edtCfopKeyPress(Sender: TObject; var Key: Char);
begin

   if not (Key in['0'..'9',Chr(8)]) then
      Key:= #0;


end;

procedure TFNfeDigitada.BtSairClick(Sender: TObject);
begin

  self.Close ();

end;

procedure TFNfeDigitada.atualizaLabelsRodape (operador:Char);
begin

  if (operador = '+') then
    begin

      lbTotalDesconto.Caption := formata_valor (CurrToStr (strToCurr (tira_ponto(get_lbtotalDesconto)) + strToCurr (tira_ponto(objPRODNFEDIGITADA.Get_Desconto))));
      lbTotalProdutos.Caption := formata_valor (CurrToStr (strToCurr (tira_ponto(get_lbTotalProduto))  + strToCurr (tira_ponto(objPRODNFEDIGITADA.get_valorTotal))));
      // atualizado pela trigger formata_valor (CurrToStr (strToCurr (tira_ponto(get_lbTotalProduto))  - strToCurr (tira_ponto(get_lbtotalDesconto))));
      lbTotalFrete   .Caption := formata_valor (CurrToStr (StrToCurr (tira_ponto(get_lbTotalFrete))    + StrToCurr (tira_ponto(objPRODNFEDIGITADA.Get_ValorFrete))));
      lbTotalIpi     .Caption := formata_valor (CurrToStr (StrToCurr (tira_ponto(get_lbTotalIPI))      + StrToCurr (tira_ponto(ObjPRODNFEDIGITADA.get_valoripi))));
      lbTotalICMSST  .Caption := formata_valor (CurrToStr (StrToCurr (tira_ponto(get_lbTotalICMSST))   + StrToCurr (tira_ponto(ObjPRODNFEDIGITADA.get_valoricms_st))));

    end
  else
  if (operador = '-') then
  begin

    lbTotalDesconto.Caption := formata_valor (CurrToStr (strToCurr (tira_ponto(get_lbtotalDesconto)) - getField('DESCONTO')));
    lbTotalProdutos.Caption := formata_valor (CurrToStr (strToCurr (tira_ponto(get_lbTotalProduto))  - getField('VALORTOTAL')));
    //atualizado pela trigger   lbtotalNota    .Caption := formata_valor (CurrToStr (strToCurr (tira_ponto(get_lbtotalNota))     - getField('VALORFINAL')));
    lbTotalFrete   .Caption := formata_valor (CurrToStr (StrToCurr (tira_ponto(get_lbTotalFrete))    - getField('VALORFRETE')));
    lbTotalIpi     .Caption := formata_valor (CurrToStr (StrToCurr (tira_ponto(get_lbTotalIPI))      - getField('valoripi')));
    lbTotalICMSST  .Caption := formata_valor (CurrToStr (StrToCurr (tira_ponto(get_lbTotalICMSST))   - getField('valoricms_st')));

  end;

  try

    objPRODNFEDIGITADA.NfeDigitada.updateDesconto         (lbTotalDesconto.Caption,lbCodigo.Caption);
    objPRODNFEDIGITADA.NfeDigitada.updateValorProdutos    (lbTotalProdutos.Caption,lbCodigo.Caption);
    //objPRODNFEDIGITADA.NfeDigitada.updateValorNF        (lbtotalNota.Caption,lbCodigo.Caption);
    objPRODNFEDIGITADA.NfeDigitada.updateValorFrete       (lbTotalFrete.Caption,lbCodigo.Caption);
    ObjPRODNFEDIGITADA.NFEDIGITADA.updateValorIPI         (lbTotalIpi.Caption,lbCodigo.Caption);
    ObjPRODNFEDIGITADA.NFEDIGITADA.updateValorICMSST      (lbTotalICMSST.Caption,lbCodigo.Caption);

    lbtotalNota.Caption := formata_valor(get_campoTabela('valornf','codigo','TABNFEDIGITADA',lbCodigo.Caption));

    FDataModulo.IBTransaction.CommitRetaining;

  except

    FDataModulo.IBTransaction.RollbackRetaining;

  end;


end;

procedure TFNfeDigitada.edtValorFreteKeyPress(Sender: TObject; var Key: Char);
begin

  ValidaNumeros(tedit(Sender),Key,'float');

end;

procedure TFNfeDigitada.limpaLabelsProdutos;
begin

  lbDescricaoProduto.Caption:='';
  lbCor.Caption:='';
  {lbDescricaoCFOP.Caption:='';}
  {lbEstoque.Caption:='';}


end;

function TFNfeDigitada.get_lbTotalProduto:string;
begin
  result := formata_valor (lbTotalProdutos.Caption);
end;

function TFNfeDigitada.get_lbtotalDesconto: string;
begin
  result := formata_valor (lbTotalDesconto.Caption);
end;

function TFNfeDigitada.get_lbTotalNota: string;
begin

  result := formata_valor (lbtotalNota.Caption);

end;

function TFNfeDigitada.get_lbTotalFrete: string;
begin

  result := formata_valor (lbTotalFrete.Caption);

end;

function TFNfeDigitada.getField(CAMPO: string): Currency;
begin
  result:= self.dbGridProdutos.DataSource.DataSet.FieldByName (CAMPO).AsCurrency;
end;

function TFNfeDigitada.get_lbTotalBC_ICMS: string;
begin


end;

function TFNfeDigitada.get_lbTotalICMS: string;
begin


end;

procedure TFNfeDigitada.formataGrid;
begin

  self.dbGridProdutos.Columns[1].Width:=200;

end;

//jonas
procedure TFNfeDigitada.lbNFEMouseLeave(Sender: TObject);
begin
  Tlabel(Sender).Font.Style:=[fsBold];
end;

procedure TFNfeDigitada.lbNFEMouseMove(Sender: TObject; Shift: TShiftState;X, Y: Integer);
begin
  Tlabel(Sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFNfeDigitada.lbNFEClick(Sender: TObject);
var
  fNotaFiscalEletronica:TFnotaFiscalEletronica;
begin

  fNotaFiscalEletronica:=TFnotaFiscalEletronica.Create(nil);

  try
    fNotaFiscalEletronica.Tag:=strtoint(self.lbNFE.caption);
    fNotaFiscalEletronica.ShowModal;
  finally
    FreeAndNil(fNotaFiscalEletronica);
  end;



end;

procedure TFNfeDigitada.lbNotaMouseLeave(Sender: TObject);
begin
  Tlabel(Sender).Font.Style:=[fsBold];
end;

procedure TFNfeDigitada.lbNotaMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  Tlabel(Sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFNfeDigitada.lbNotaClick(Sender: TObject);
var
  fNotaFiscal:TFNotaFiscal;
begin

  try
    fNotaFiscal:=TFNotaFiscal.Create(nil);
    fNotaFiscal.Tag:=strtoint(self.lbNota.caption);
    fNotaFiscal.ShowModal;
  finally
    FreeAndNil(fNotaFiscal);
  end;

end;

procedure TFNfeDigitada.btalterarClick(Sender: TObject);
begin
  pgcGuia.TabIndex:=0;

  if (lbnota.Caption <> '') or (lbNFE.Caption <> '') then
  begin
    MensagemAviso('N�o � possivel alterar com nota gerada');
    Exit;
  end;

   if (Self.objPRODNFEDIGITADA.NfeDigitada.Status <> dsinactive) then
    Exit;

  if (lbCodigo.Caption = '') then
    Exit;

  if (lbNota.Caption <> '') then
  begin

    MensagemAviso('N�o � possivel alterar com nota gerada');
    Exit;

  end;

  esconde_botoes(Self.panelbotes);
  habilita_campos(self.panelCabecalho);
  self.objPRODNFEDIGITADA.NfeDigitada.status:=dsEdit;
  Btgravar.visible:=True;
  BtCancelar.visible:=True;
  btpesquisar.visible:=True;
  edtCliente.SetFocus;

end;

procedure TFNfeDigitada.adicionaInfo;
var
  pRedBC:string;
begin

   if (edtProduto.Text = '') then
    Exit;

   pRedBC:=get_campoTabela('ICMS_REDUCAO_BC_CONSUMIDORFINAL','CODIGO','TABPRODUTO',edtProduto.Text);

   if (pRedBC = '0') then
    Exit;

   if (Pos('Produto: '+edtProduto.Text,memoInformacoes.Lines.Text) <> 0) then
    Exit;

   memoInformacoes.Lines.Append('Produto: '+edtProduto.Text+' percentual da redu��o '+pRedBC+'%');

end;



procedure TFNfeDigitada.retiraMemo(pCodigoProduto:string);
var
  i:integer;
begin

  for i:=0 to memoInformacoes.Lines.Count-1 do
  begin

     if (Pos('Produto: '+pCodigoProduto,memoInformacoes.Lines[i]) <> 0) then
      memoInformacoes.Lines.Delete(i);

  end;

end;

procedure TFNfeDigitada.Consultarservio1Click(Sender: TObject);
begin

  {consulta tervi�o}
  self.objTransNFE.consultaServico;

end;

procedure TFNfeDigitada.Consultardoarquivo1Click(Sender: TObject);
begin

  {consultar do arquivo}
  self.objTransNFE.consultaXML;

end;

procedure TFNfeDigitada.Consultapelachavedeacesso1Click(Sender: TObject);
begin      

  self.objTransNFE.consultaPelaChave('','',true);

end;

procedure TFNfeDigitada.menuItemPopup(Sender: TObject);
begin

  self.Servios1.Checked:=UtilizaServico;
  self.Produto1.Checked:=not(UtilizaServico);

end;

procedure TFNfeDigitada.lbNomeTransportadoraMouseLeave(Sender: TObject);
begin
Tlabel(Sender).Font.Style:=[fsBold];
end;

procedure TFNfeDigitada.lbNomeTransportadoraMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
Tlabel(Sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFNfeDigitada.lbNomeClienteMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
Tlabel(Sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFNfeDigitada.lbNomeClienteMouseLeave(Sender: TObject);
begin
Tlabel(Sender).Font.Style:=[fsBold];
end;

procedure TFNfeDigitada.lbNomeClienteClick(Sender: TObject);
var
  formCliente:TFcliente;
begin

  if (self.edtCliente.Text = '') then Exit;

  formCliente:=TFcliente.Create(nil);

  try
    formCliente.Tag:=StrToInt(edtCliente.text);
    formCliente.ShowModal;
  finally
    FreeAndNil(formCliente);
  end;


end;

procedure TFNfeDigitada.lbNomeTransportadoraClick(Sender: TObject);
var
  formTransportadora:TFTRANSPORTADORA;
begin


  if (self.edtTransportadora.Text = '') then Exit;
  formTransportadora:=TFTRANSPORTADORA.Create(nil);

  try
    formTransportadora.Tag:=StrToInt(edtTransportadora.text);
    formTransportadora.ShowModal;
  finally
    FreeAndNil(formTransportadora);
  end;


end;

function TFNfeDigitada.get_lbTotalIPI: string;
begin
  Result := formata_valor (lbTotalIpi.Caption);
end;

function TFNfeDigitada.get_lbTotalICMSST: string;
begin
  result := formata_valor(lbTotalICMSST.Caption);
end;

procedure TFNfeDigitada.edtFornecedorKeyPress(Sender: TObject;var Key: Char);
begin

  if not (Key in['0'..'9',Chr(8)]) then
      Key:= #0;

end;

procedure TFNfeDigitada.lbNomeFornecedorClick(Sender: TObject);
var
  formFornecedor:TFFORNECEDOR;
begin

  if (self.edtFornecedor.Text = '') then Exit;

  formFornecedor:=TFFORNECEDOR.Create(nil);

  try
    formFornecedor.Tag:=StrToInt(edtFornecedor.text);
    formFornecedor.ShowModal;
  finally
    FreeAndNil(formFornecedor);
  end;

end;

procedure TFNfeDigitada.lbNomeFornecedorMouseLeave(Sender: TObject);
begin

  Tlabel(Sender).Font.Style:=[fsBold];

end;

procedure TFNfeDigitada.lbNomeFornecedorMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
Tlabel(Sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFNfeDigitada.BtnovoClick(Sender: TObject);
begin
     pgcGuia.TabIndex:=0;
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);
     self.objPRODNFEDIGITADA.desativaGrid ();
     lbcodigo.Caption:='0';
     chkBaixaEstoque.Checked:=False;

     edtDataEmissao.Text := formatdatetime('dd/mm/yyyy',now);
     edtDataSaida  .Text := formatdatetime('dd/mm/yyyy',now);
     edtHoraSaida  .Text := formatdatetime('hh:mm:ss',now);
     edtHoraEmissao.Text := FormatDateTime('hh:mm:ss',now);

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     self.objPRODNFEDIGITADA.NfeDigitada.status:=dsInsert;
     self.objPRODNFEDIGITADA.NfeDigitada.zerarTabela;
     self.limpaEnderecoEntrega;

     comboIndPag1.ItemIndex := indicePagmentoNfe;

     edtCLIENTE.setfocus;
     checkImpostoAutomatico.Checked := True;

end;

procedure TFNfeDigitada.btgravarClick(Sender: TObject);
begin

  try

      Screen.Cursor:=crHourGlass;

      If (self.objPRODNFEDIGITADA.NfeDigitada.Status=dsInactive) then
        exit;

      If (ControlesParaObjeto=False) then
      Begin
          MensagemErro('Erro: controles para objeto');
          exit;
      End;


      if not (self.objPRODNFEDIGITADA.NfeDigitada.Salvar (true)) then
      begin

        MensagemErro ('Erro na tentativa de gravar');
        Exit;

      end;

      mostra_botoes(Self);

      desabilita_campos (panelCabecalho);
      habilita_campos   (panelProdutos);
      self.comboTipo.SetFocus ();

      self.objPRODNFEDIGITADA.NfeDigitada.LocalizaCodigo (self.objPRODNFEDIGITADA.NfeDigitada.get_codigo ());
      self.objPRODNFEDIGITADA.NfeDigitada.TabelaparaObjeto;
      Self.TabelaParaControles;

      self.objPRODNFEDIGITADA.NfeDigitada.status:=dsInactive;

  finally

      Screen.Cursor:=crDefault;

  end;

end;

procedure TFNfeDigitada.btcancelarClick(Sender: TObject);
begin

  Self.objPRODNFEDIGITADA.NfeDigitada.status:=dsInactive;
  
  limpaedit (panelCabecalho);
  limpaedit (panelProdutos);
  desabilita_campos (panelCabecalho);
  desabilita_campos (panelProdutos);

  self.objPRODNFEDIGITADA.desativaGrid ();
  limpaLabels ();
  self.limpaEnderecoEntrega;

  mostra_botoes(self.panelbotes);
  self.objPRODNFEDIGITADA.NfeDigitada.status:=dsInactive;
  comboIndPag1.ItemIndex := -1;

end;

procedure TFNfeDigitada.btexcluirClick(Sender: TObject);
begin

    if (lbnota.Caption <> '') or (lbNFE.Caption <> '') then
  begin
    MensagemAviso('N�o � possivel excluir com nota gerada');
    Exit;
  end;

  comboIndPag1.Enabled := False;

  if (lbCodigo.Caption <> '') then
  begin

    if (lbNFE.Caption = '') then
    begin

      self.objPRODNFEDIGITADA.excluiProdutos (lbCodigo.Caption);
      self.objPRODNFEDIGITADA.NfeDigitada.Exclui (lbCodigo.Caption,True);
      Self.btcancelarClick (btcancelar);

    end else
      MensagemAviso ('N�o � possivel excluir com Nf-e gerada');


  end;

end;

procedure TFNfeDigitada.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
           FpesquisaLocal.NomeCadastroPersonalizacao:='FNFEDIGITADA.PESQUISA';
            pgcGuia.TabIndex:=0;
            If (FpesquisaLocal.PreparaPesquisa(Self.objPRODNFEDIGITADA.NfeDigitada.Get_pesquisa,Self.objPRODNFEDIGITADA.NfeDigitada.get_tituloPesquisa,Nil)) Then
            Begin
                Try

                  If (FpesquisaLocal.showmodal=mrok) Then
                  Begin

                      If (Self.objPRODNFEDIGITADA.NfeDigitada.status <> dsinactive) then exit;

                      If not (Self.objPRODNFEDIGITADA.NfeDigitada.LocalizaCodigo (FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)) then
                      Begin
                        MensagemErro ('Dados n�o encontrados!');
                        exit;
                      End;

                      Self.objPRODNFEDIGITADA.NfeDigitada.ZERARTABELA;

                      If not (Self.TabelaParaControles) Then
                      Begin

                        MensagemErro ('Erro na Transfer�ncia dos Dados!');
                        limpaedit(Self);
                        Self.limpaLabels;
                        exit;

                      End;


                      Self.objPRODNFEDIGITADA.retornaProdutos (lbCodigo.Caption);
                      self.formataGrid();

                  End;

                  if (self.comboTipo.Enabled) then
                    self.comboTipo.SetFocus;

                Finally

                  FpesquisaLocal.QueryPesq.close;

                End;
            End;

        Finally

           FreeandNil(FPesquisaLocal);
           
        End;
        self.limpaEnderecoEntrega;
end;

procedure TFNfeDigitada.btOpcoesClick(Sender: TObject);
var
  pcodigo:string;
begin

  if (lbCodigo.Caption = '') then
    Exit;

  {if (lbNota.Caption <> '') then
    Exit; }

  pcodigo := lbCodigo.Caption;

  self.objPRODNFEDIGITADA.opcoes(pcodigo);

  self.objPRODNFEDIGITADA.NfeDigitada.LocalizaCodigo(pcodigo);
  self.objPRODNFEDIGITADA.NfeDigitada.tabelaparaObjeto;
  self.ObjetoParaControles; 
  checaEnderecoEntrega;
  
end;

procedure TFNfeDigitada.comboTipoEnter(Sender: TObject);
begin

  TComboBox(sender).Color:=clInactiveCaption;

end;

procedure TFNfeDigitada.comboTipoExit(Sender: TObject);
begin

    TComboBox(sender).Color:=$005CADFE;

end;

procedure TFNfeDigitada.edtProdutoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  if (self.comboTipo.ItemIndex > 0) then
  begin

    case (self.comboTipo.ItemIndex) of

      0:Exit;

      1:self.ObjPRODNFEDIGITADA.EdtFERRAGEMKeyDown(sender,key,Shift,lbDescricaoProduto);
      2:self.ObjPRODNFEDIGITADA.EdtPERFILADOKeyDown(sender,key,Shift,lbDescricaoProduto);
      3:self.ObjPRODNFEDIGITADA.EdtVIDROKeyDown(sender,key,Shift,lbDescricaoProduto);
      4:self.ObjPRODNFEDIGITADA.EdtKITBOXKeyDown(sender,key,Shift,lbDescricaoProduto);
      5:self.ObjPRODNFEDIGITADA.EdtPERSIANAKeyDown(sender,key,Shift,lbDescricaoProduto);
      6:self.ObjPRODNFEDIGITADA.EdtDIVERSOKeyDown(sender,key,Shift,lbDescricaoProduto);

    end;

  end;

end;

procedure TFNfeDigitada.edtProdutoExit(Sender: TObject);
var
  campoTabela:string;
begin


  if (self.comboTipo.ItemIndex > 0) then
  begin

    case (self.comboTipo.ItemIndex) of

      0:Exit;

      1:begin
          self.ObjPRODNFEDIGITADA.EdtFERRAGEMExit(sender,lbDescricaoProduto);
          campoTabela:='TABFERRAGEM';
        end;

      2:begin
          self.ObjPRODNFEDIGITADA.EdtPERFILADOExit(sender,lbDescricaoProduto);
          campoTabela:='TABPERFILADO';
        end;

      3:begin
          self.ObjPRODNFEDIGITADA.EdtVIDROExit(sender,lbDescricaoProduto);
          campoTabela:='TABVIDRO'
        end;

      4:begin

        self.ObjPRODNFEDIGITADA.EdtKITBOXExit(sender,lbDescricaoProduto);
        campoTabela:='TABKITBOX';
        
      end;

      5:begin
          self.ObjPRODNFEDIGITADA.EdtPERSIANAExit(sender,lbDescricaoProduto);
          campoTabela:='TABPERSIANA';
        end;

      6:begin
        self.ObjPRODNFEDIGITADA.EdtDIVERSOExit(sender,lbDescricaoProduto);
        campoTabela:='TABDIVERSO';
       end;

    end;

  end;

  if (edtProduto.Text <> '') then
  begin

     if (UtilizaServico) then
     begin

      {lbDescricaoProduto.Caption := get_campoTabela ('NOME','CODIGO','TABSERVICOS',TEdit(sender).Text);
      edtValorproduto.Text       := get_campoTabela ('VALOR','CODIGO','TABSERVICOS',TEdit(sender).Text);
      lbEstoque.Caption:='';}

     end
     else begin

      edtValorproduto.Text       := get_campoTabela ('','CODIGO',campoTabela,edtProduto.Text);
      
     end;

     edtQuantidade.Text := '1';

  end;



end;

procedure TFNfeDigitada.edtClienteKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  self.ObjPRODNFEDIGITADA.NFEDIGITADA.EdtCLIENTEKeyDown(sender,Key,Shift,lbNomeCliente);

end;

procedure TFNfeDigitada.edtClienteExit(Sender: TObject);
begin

  self.ObjPRODNFEDIGITADA.NFEDIGITADA.EdtCLIENTEExit(Sender,lbNomeCliente);

end;

procedure TFNfeDigitada.edtFornecedorKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin

  self.ObjPRODNFEDIGITADA.NFEDIGITADA.EdtFORNECEDORKeyDown(sender,Key,Shift,lbNomeFornecedor);

end;

procedure TFNfeDigitada.edtFornecedorExit(Sender: TObject);
begin

  self.ObjPRODNFEDIGITADA.NFEDIGITADA.EdtFORNECEDORExit(Sender,lbNomeFornecedor);

end;

procedure TFNfeDigitada.edtTransportadoraKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin

  self.ObjPRODNFEDIGITADA.NFEDIGITADA.EdtTRANSPORTADORAKeyDown(sender,Key,Shift,lbNomeTransportadora);

end;

procedure TFNfeDigitada.edtTransportadoraExit(Sender: TObject);
begin

    self.ObjPRODNFEDIGITADA.NFEDIGITADA.EdtTRANSPORTADORAExit(sender,lbNomeTransportadora);

end;

procedure TFNfeDigitada.DBGridProdutosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin

  (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL1;

  If not Odd(Self.objPRODNFEDIGITADA.queryProdutos.RecNo) then
  If not (gdselected in state)
  Then (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL2;

  (Sender as TdbGrid).canvas.FillRect(rect);
  (Sender as TdbGrid).DefaultDrawDataCell(rect, column.field,state);

end;

procedure TFNfeDigitada.DBGridProdutosDblClick(Sender: TObject);
var
  size:integer;
  a:string;
  campo:string;
begin

 try

  campo := dbGridProdutos.Columns[self.dbGridProdutos.SelectedIndex].Title.Caption;
  size  := Length (dbGridProdutos.DataSource.DataSet.FieldByName (campo).AsString);

  if (size >= 19) then
    size:=size*8
  else
    size:=size*10;


  self.dbGridProdutos.Columns[self.dbGridProdutos.SelectedIndex].Width:=size;

 except

 end


end;

procedure TFNfeDigitada.DBGridProdutosKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
  tipoMaterial:string;
  tag:string;
begin

   if (dbGridProdutos.DataSource.DataSet.RecordCount < 1) then
    Exit;

  tipoMaterial:=self.dbGridProdutos.DataSource.DataSet.FieldByName('MATERIAL').AsString;
  tag := self.dbGridProdutos.DataSource.DataSet.FieldByName('CODIGO_PRODUTO').AsString;

  try
    self.comboTipo.ItemIndex := StrToInt(tipoMaterial);
  except
  end;

  if (tipoMaterial = '') then
    Exit;

  if (Key = VK_RETURN) then
  begin

    case (StrToInt(tipoMaterial)) of

      1:chamaFormulario(TFFERRAGEM,self,tag);
      2:chamaFormulario(TFPERFILADO,self,tag);
      3:chamaFormulario(TFVIDRO,self,tag);
      4:chamaFormulario(TFKITBOX,self,tag);
      5:chamaFormulario(TFPERSIANA,self,tag);
      6:chamaFormulario(TFDIVERSO,self,tag);

    else

      MensagemAviso('Tipo de material n�o conhecido. Tipo: '+tipoMaterial);

    end;

  end;

end;

procedure TFNfeDigitada.btGravarProdutoClick(Sender: TObject);
var
  indiciItem:Integer;
begin

  try

    if (edtQuantidade.Text   = '') or (edtQuantidade.Text   = '0') then
    begin
      MensagemAviso('Especifique a quantidade');
      edtQuantidade.SetFocus;
      Exit;
    end;

    if (edtValorproduto.Text = '') or (edtValorproduto.Text = '0') then
    begin
      MensagemAviso('Especifique um valor');
      edtValorproduto.SetFocus;
      Exit;
    end;

    if (edtProduto.Text      = '') then Exit;

    if (Trim(edtCor.Text) = '') then
    begin
      if (comboTipo.ItemIndex <> 5 ) and (comboTipo.ItemIndex <> 6 ) then
      begin
        MensagemAviso('Escolha a cor');
        edtCor.SetFocus;
        Exit;
      end;
      //se for 5 ent�o � persiana e esse nao tem cor
    end;


    if (edtValorFrete.Text = '') then edtValorFrete.Text := '0';
    if (edtDesconto.Text   = '') then edtDesconto  .Text := '0';

    try

        Screen.Cursor:=crHourGlass;

        objPRODNFEDIGITADA.Status := dsInsert;
        objPRODNFEDIGITADA.ZerarTabela;

        If (objPRODNFEDIGITADA.Status=dsInactive) then
          exit;


        objPRODNFEDIGITADA.Submit_Codigo         ('0');
        ObjPRODNFEDIGITADA.submit_DESCRICAO (lbDescricaoProduto.Caption);
        ObjPRODNFEDIGITADA.submit_MATERIAL (IntToStr(self.comboTipo.itemIndex));

        case (Self.comboTipo.ItemIndex) of
          1:ObjPRODNFEDIGITADA.FERRAGEM.Submit_Codigo(edtProduto.Text);
          2:ObjPRODNFEDIGITADA.PERFILADO.Submit_Codigo(edtProduto.Text);
          3:ObjPRODNFEDIGITADA.VIDRO.Submit_Codigo(edtProduto.Text);
          4:ObjPRODNFEDIGITADA.KITBOX.Submit_Codigo(edtProduto.Text);
          5:ObjPRODNFEDIGITADA.PERSIANA.Submit_Codigo(edtProduto.Text);
          6:ObjPRODNFEDIGITADA.DIVERSO.Submit_Codigo(edtProduto.Text);
        else

          MensagemAviso('Tipo de material n�o conhecido. Tipo: '+IntToStr(self.comboTipo.ItemIndex));

        end;

        ObjPRODNFEDIGITADA.COR.Submit_Codigo(self.edtCor.Text);
        objPRODNFEDIGITADA.Submit_Quantidade     (edtQuantidade.Text);
        objPRODNFEDIGITADA.Submit_Valor          (edtValorproduto.Text);
        objPRODNFEDIGITADA.Submit_Desconto       (edtDesconto.Text);
        objPRODNFEDIGITADA.Submit_ValorFrete     (edtValorFrete.Text);
        ObjPRODNFEDIGITADA.submit_cfop           (edtcfop.Text);

        objPRODNFEDIGITADA.submit_valorTotal     (objPRODNFEDIGITADA.get_valorTotal_2 ());
        objPRODNFEDIGITADA.submit_valorFinal     (objPRODNFEDIGITADA.get_valorTotal_2 ());
        objPRODNFEDIGITADA.NfeDigitada.submit_codigo (lbCodigo.Caption);

        if not (objPRODNFEDIGITADA.Salvar (false)) then
        begin

          MensagemErro ('Erro na tentativa de gravar');
          Exit;

        end;

        indiciItem:=self.comboTipo.ItemIndex;

        self.objPRODNFEDIGITADA.retornaProdutos (lbCodigo.Caption);
        Self.formataGrid();
        self.atualizaLabelsRodape ('+');
        limpaedit (panelProdutos);
        limpaLabelsProdutos ();

        self.comboTipo.SetFocus();
        self.comboTipo.ItemIndex:=indiciItem;

        FDataModulo.IBTransaction.CommitRetaining;

    finally

      Screen.Cursor:=crDefault;

    end;

  except
    on e:Exception do
    begin
      FDataModulo.IBTransaction.RollbackRetaining;
      ShowMessage(e.Message);
    end;
  end


end;

procedure TFNfeDigitada.comboTipoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if (Key = 40) then {seta para baixo}
    self.comboTipo.DroppedDown:=True;

end;


procedure TFNfeDigitada.DBGridProdutosCellClick(Column: TColumn);
begin

   if (dbGridProdutos.DataSource.DataSet.RecordCount < 1) then
    Exit;
    
  try
    self.comboTipo.ItemIndex := self.dbGridProdutos.DataSource.DataSet.fieldbyname('MATERIAL').AsInteger;
  except
  end;

end;

procedure TFNfeDigitada.btExcluirProdutoClick(Sender: TObject);
begin

  if (dbGridProdutos.DataSource.DataSet.RecordCount < 1) then
  Exit;

  if (lbNota.Caption <> '') then
  begin

    MensagemAviso('N�o � possivel excluir com nota gerada');
    Exit;

  end;

  if (dbGridProdutos.DataSource.DataSet.RecordCount > 0) then
  begin

    self.atualizaLabelsRodape ('-');
    {self.retiraMemo(dbGridProdutos.DataSource.DataSet.FieldByName ('CODIGO_PRODUTO').AsString);}
    //deletar da tabmateriaisvenda


    self.objPRODNFEDIGITADA.Exclui (dbGridProdutos.DataSource.DataSet.FieldByName ('CODIGO').AsString,true);
    self.objPRODNFEDIGITADA.retornaProdutos (lbCodigo.Caption);
    self.formataGrid();

  end;

  {self.objPRODNFEDIGITADA.NfeDigitada.updateInformacoes(lbCodigo.Caption,memoInformacoes.Text);}

end;

function TFNfeDigitada.deletatabmateriaisvenda:Boolean;
var
  query:TIBQuery;
begin
    query:=TIBQuery.Create(nil);
    query.Database:=FDataModulo.IBDatabase;
    result:=True;
    try
      query.Close;
      query.sql.Clear;
      query.SQL.Add('delete from tabmateriaisvenda where nfedigitada='+lbCodigo.Caption);
      if(dbGridProdutos.DataSource.DataSet.FieldByName ('material').AsString='1')
      then query.SQL.Add('and ferragem='+dbGridProdutos.DataSource.DataSet.FieldByName ('codigo_produto').AsString);
      if(dbGridProdutos.DataSource.DataSet.FieldByName ('material').AsString='2')
      then query.SQL.Add('and perfilado='+dbGridProdutos.DataSource.DataSet.FieldByName ('codigo_produto').AsString);
      if(dbGridProdutos.DataSource.DataSet.FieldByName ('material').AsString='3')
      then query.SQL.Add('and vidro='+dbGridProdutos.DataSource.DataSet.FieldByName ('codigo_produto').AsString);
      if(dbGridProdutos.DataSource.DataSet.FieldByName ('material').AsString='4')
      then query.SQL.Add('and kitbox='+dbGridProdutos.DataSource.DataSet.FieldByName ('codigo_produto').AsString);
      if(dbGridProdutos.DataSource.DataSet.FieldByName ('material').AsString='5')
      then query.SQL.Add('and persiana='+dbGridProdutos.DataSource.DataSet.FieldByName ('codigo_produto').AsString);
      if(dbGridProdutos.DataSource.DataSet.FieldByName ('material').AsString='6')
      then query.SQL.Add('and diverso='+dbGridProdutos.DataSource.DataSet.FieldByName ('codigo_produto').AsString);

      query.ExecSQL;
    finally
      FreeAndNil(query);
    end;

end;

procedure TFNfeDigitada.edtDescontoExit(Sender: TObject);
begin

  if (self.edtDesconto.Text = '') then
    self.edtDesconto.Text:='0';

end;

procedure TFNfeDigitada.edtValorFreteExit(Sender: TObject);
begin

  if (self.edtValorFrete.Text = '') then
    self.edtValorFrete.Text:='0';

end;

procedure TFNfeDigitada.edtValorprodutoExit(Sender: TObject);
begin

  if (self.edtValorproduto.Text = '') then
    self.edtValorproduto.Text:='0';

end;

{jonas}
procedure TFNfeDigitada.edtCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  if (Key <> vk_f9) then Exit;

  if (self.comboTipo.ItemIndex > 0) then
  begin

    case (self.comboTipo.ItemIndex) of

      1:self.ObjPRODNFEDIGITADA.pesquisaCor(self.edtProduto.Text,'ferragem','codigo','TABFERRAGEM',lbcor,sender);
      2:self.ObjPRODNFEDIGITADA.pesquisaCor(self.edtProduto.Text,'perfilado','codigo','TABPERFILADO',lbCor,sender);
      3:self.ObjPRODNFEDIGITADA.pesquisaCor(self.edtProduto.Text,'vidro','codigo','TABVIDRO',lbCor,sender);
      4:self.ObjPRODNFEDIGITADA.pesquisaCor(self.edtProduto.Text,'kitbox','codigo','TABKITBOX',lbCor,sender);
      6:self.ObjPRODNFEDIGITADA.pesquisaCor(self.edtProduto.Text,'diverso','codigo','TABDIVERSO',lbCor,sender);

    end;

  end;

end;
{}

procedure TFNfeDigitada.Label24Click(Sender: TObject);
var
  strNota:TStringList;
  pNota,dtAtual,hrAtual:string;
  msg:string;
begin

  if (lbNota.Caption <> '') then
    Exit;

  if (lbcodigo.Caption<>'') and (objPRODNFEDIGITADA.NfeDigitada.status=dsinactive) then
  begin

    self.objPRODNFEDIGITADA.ObjNotafiscalObjetos.NfeEntrada := False;

    strNota:=TStringList.Create;

    try

      pnota:=self.ObjPRODNFEDIGITADA.get_proximaNFE();
      strNota.Add(pnota);

      if (pnota = '') then
      begin

        MensagemAviso('Verifique se h� modelo de nota fiscal eletr�nica cadastrado');
        Exit;

      end;

      msg:='';
      if(edtoperacao.Text='')
      then edtoperacao.Text:='1';

      if(chkBaixaEstoque.Checked=True) then
      begin
        if(Self.ObjPRODNFEDIGITADA.NFEDIGITADA.BaixarEstoque(lbCodigo.Caption,strNota[0])=False)
        then Exit;
      end;

      self.objPRODNFEDIGITADA.submit_nfeEntrada(false);
      self.objPRODNFEDIGITADA.submit_nfeDevolucao(false);
      self.objPRODNFEDIGITADA.submit_nfeComplementar(false);

      if (nfeDataAtual = 'SIM') then
      begin
        dtAtual := QuotedStr( FormatDateTime('yyyy/mm/dd',now) );
        hrAtual := QuotedStr( formatdatetime('hh:mm:ss',now) );
        exec_sql('update tabnfedigitada set DATASAIDA = '+dtAtual+', DATAEMISSAO = '+dtAtual+', HORASAIDA = '+hrAtual+', HORAEMISSAO = '+hrAtual+' where codigo = '+lbCodigo.Caption);
      end;

      if(objPRODNFEDIGITADA.geraNotaFiscalParaNFe(strNota,lbCodigo.Caption,edtoperacao.Text,999)=false) then
      begin
        MensagemErro('Erro na Gera��o da NF-e');
        FDataModulo.IBTransaction.RollbackRetaining;
        ObjPRODNFEDIGITADA.estornaMateriaisVenda (self.lbCodigo.Caption);
      end
      else
      begin

        if(Self.ObjPRODNFEDIGITADA.ObjNotafiscalObjetos.GravaProdutosNF_NfeDigitada(lbCodigo.Caption,strNota[0])=false) then
        begin
          ShowMessage( 'Erro fun��o: GravaProdutosNF_NfeDigitada' );
          Exit;
        end;

        FDataModulo.IBTransaction.CommitRetaining;

      end;
      objPRODNFEDIGITADA.NfeDigitada.LocalizaCodigo(lbcodigo.Caption);
      objPRODNFEDIGITADA.NfeDigitada.tabelaparaObjeto;
      self.ObjetoParaControles;
      self.limpaEnderecoEntrega;
    finally

      FreeAndNil(strNota);  

    end;

  end;

end;

procedure TFNfeDigitada.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
  formPesquisamenu:TfPesquisaMenu;
begin

   if (Key = 112) then {f1}
   begin

     formPesquisamenu:=TfPesquisaMenu.Create(nil);

     try
      formPesquisamenu.submit_formulario(Fprincipal);
      formPesquisamenu.ShowModal;
     finally
      FreeAndNil(formPesquisamenu);
     end;


   end;


   if (Key = VK_F2) then
   begin

         FAjuda.PassaAjuda('NFE DIGITADA');
         FAjuda.ShowModal;
   end;

end;

procedure TFNfeDigitada.bt1Click(Sender: TObject);
begin
      FAjuda.PassaAjuda('NFE DIGITADA');
      FAjuda.ShowModal;
end;

procedure TFNfeDigitada.edtCorExit(Sender: TObject);
var
  QueryPreco:TIBQuery;
begin
     if(edtCor.Text='')
     then Exit;
     
     QueryPreco:=TIBQuery.Create(nil);
     QueryPreco.Database:=FDataModulo.IBDatabase;
     try
         with QueryPreco do
         begin
              case (Self.comboTipo.ItemIndex) of
                1:
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select (tabferragem.precovendainstalado+(tabferragem.precovendainstalado * tabferragemcor.porcentagemacrescimofinal)/100) as valor');
                      SQL.Add('from tabferragem');
                      SQL.Add('join tabferragemcor on tabferragemcor.ferragem=tabferragem.codigo');
                      SQL.Add('where tabferragem.codigo='+edtproduto.Text);
                      SQL.Add('and tabferragemcor.cor='+edtCor.Text);
                      Open;
                      edtValorproduto.Text:=fieldbyname('valor').asstring;
                end;
                2:
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select (tabperfilado.precovendainstalado+(tabperfilado.precovendainstalado * tabperfiladocor.porcentagemacrescimofinal)/100) as valor');
                      SQL.Add('from tabperfilado');
                      SQL.Add('join tabperfiladocor on tabperfiladocor.perfilado=tabperfilado.codigo');
                      SQL.Add('where tabperfilado.codigo='+edtproduto.Text);
                      SQL.Add('and tabperfiladocor.cor='+edtCor.Text);
                      Open;
                      edtValorproduto.Text:=fieldbyname('valor').asstring;
                end;
                3:
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select (tabvidro.precovendainstalado+(tabvidro.precovendainstalado * tabvidrocor.porcentagemacrescimofinal)/100) as valor');
                      SQL.Add('from tabvidro');
                      SQL.Add('join tabvidrocor on tabvidrocor.vidro=tabvidro.codigo');
                      SQL.Add('where tabvidro.codigo='+edtproduto.Text);
                      SQL.Add('and tabvidrocor.cor='+edtCor.Text);
                      Open;
                      edtValorproduto.Text:=fieldbyname('valor').asstring;
                end;
                4:
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select (tabkitbox.precovendainstalado+(tabkitbox.precovendainstalado * tabkitboxcor.porcentagemacrescimofinal)/100) as valor');
                      SQL.Add('from tabkitbox');
                      SQL.Add('join tabkitboxcor on tabkitboxcor.kitbox=tabkitbox.codigo');
                      SQL.Add('where tabkitbox.codigo='+edtproduto.Text);
                      SQL.Add('and tabkitboxcor.cor='+edtCor.Text);
                      Open;
                      edtValorproduto.Text:=fieldbyname('valor').asstring;
                end;
                5:
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select (tabpersiana.precovendainstalado+(tabpersiana.precovendainstalado * tabpersianagrupodiametrocor.porcentagemacrescimofinal)/100) as valor');
                      SQL.Add('from tabpersiana');
                      SQL.Add('join tabpersianagrupodiametrocor on tabpersianagrupodiametrocor.persiana=tabpersiana.codigo');
                      SQL.Add('where tabpersiana.codigo='+edtproduto.Text);
                      SQL.Add('and tabpersianagrupodiametrocor.cor='+edtCor.Text);
                      Open;
                      edtValorproduto.Text:=fieldbyname('valor').asstring;
                end;
                6:
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select (tabdiverso.precovendainstalado+(tabdiverso.precovendainstalado * tabdiversocor.porcentagemacrescimofinal)/100) as valor');
                      SQL.Add('from tabdiverso');
                      SQL.Add('join tabdiversocor on tabdiversocor.diverso=tabdiverso.codigo');
                      SQL.Add('where tabdiverso.codigo='+edtproduto.Text);
                      SQL.Add('and tabdiversocor.cor='+edtCor.Text);
                      Open;
                      edtValorproduto.Text:=fieldbyname('valor').asstring;
                end;
              else

                MensagemAviso('Tipo de material n�o conhecido. Tipo: '+IntToStr(self.comboTipo.ItemIndex));

              end;
         end;
     finally
          FreeAndNil(QueryPreco);
     end;

end;

procedure TFNfeDigitada.edtoperacaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    ObjPRODNFEDIGITADA.NFEDIGITADA.NOTA.EdtoperacaoKeyDown(Sender,Key,Shift);
end;

procedure TFNfeDigitada.edtoperacaoExit(Sender: TObject);
begin
     if(edtoperacao.Text='')
     then Exit;

     if(ObjPRODNFEDIGITADA.NFEDIGITADA.NOTA.Operacao.LocalizaCodigo(edtoperacao.Text)=False) then
     begin
        edtoperacao.text:='';
        Exit;
     end;

     ObjPRODNFEDIGITADA.NFEDIGITADA.NOTA.Operacao.TabelaparaObjeto;
     lboperacao.Caption:=ObjPRODNFEDIGITADA.NFEDIGITADA.NOTA.Operacao.Get_NOME;

end;

procedure TFNfeDigitada.pgcGuiaChange(Sender: TObject);
begin
  case (pgcGuia.TabIndex) of
    0:;
    1:
    begin
      if(ObjUsuarioGlobal.Get_nome='SYSDBA')
      then  self.pegaProximaNFE
      else
      begin
        MensagemAviso('Somente usu�rio SYSDBA autorizado!');
        pgcGuia.TabIndex:=0;
      end;

      if edtNumeroNFEAte.Visible then
      begin
        edtNumeroNFEAte.SetFocus;
        edtNumeroNFEAte.SelStart := length(edtNumeroNFEAte.text)-2;
        edtNumeroNFEAte.SelLength :=2;
      end;

    end;
    2:
    begin
      if(ObjUsuarioGlobal.Get_nome='SYSDBA') then
      begin
        pageArquivoConf.TabIndex := 0;
        self.lerAbaGeral;
      end
      else
      begin
        MensagemAviso('Somente usu�rio SYSDBA autorizado!');
        pgcGuia.TabIndex:=0;
      end;


    end;
  else;
  end;

end;

procedure TFNfeDigitada.pegaProximaNFE;
var
  qery:TIBQuery;
  statusNF,statusNFE:string;
begin

  FDataModulo.IBTransaction.RollbackRetaining;
  
  qery := TIBQuery.Create(nil);
  try

    qery.Database := FDataModulo.IBDatabase;

    qery.SQL.Clear;
    qery.Active := False;
    qery.SQL.Text := objTransNFE.getCampoSQL2('PROXIMO NUMERO');
    qery.Active := True;
    edtNF1.Text := qery.Fields[0].Text;
    edtNumeroNFEde.Text  := qery.Fields[1].Text;
    edtNumeroNFEAte.Text := qery.Fields[1].Text;

    statusNF  := get_campoTabela('situacao','codigo','TABNOTAFISCAL',edtNF1.Text);
    statusNFE := get_campoTabela('statusnota','codigo','TABNFE',edtNumeroNFEde.Text);

    comboStatus.ItemIndex := AnsiIndexStr(UpperCase(statusNF),['I','N','C','Z','P','T']);

  finally
    FreeAndNil(qery);
  end;

end;

procedure TFNfeDigitada.bt_okClick(Sender: TObject);
var
  statusNFE,sql:string;
begin

  if not IsNumeric(edtNumeroNFEde.Text) then
  begin
    MensagemAviso('Campo numero NF-e com valor inv�lido');
    Exit;
  end;

  if not IsNumeric(edtNumeroNFEAte.Text) then
  begin
    MensagemAviso('Campo numeroNF-e com valor inv�lido');
    Exit;
  end;

  if (edtNumeroNFEde.Text = '') or (edtNumeroNFEAte.Text = '') then
  begin
    MensagemAviso('Campo numero NF-e vazio');
    Exit;
  end;

  if comboStatus.Text[1] = 'N' then
    statusNFE := 'A'
  else if comboStatus.Text[1] = 'I' then
    statusNFE := 'G'
  else
    statusNFE := comboStatus.Text[1];

  try
    sql := 'update TABNOTAFISCAL set situacao = '+QuotedStr(comboStatus.Text[1])+' where numero >= '+edtNumeroNFEde.Text + ' and numero <= '+edtNumeroNFEAte.Text + ' and modelo_nf = 2';
    exec_sql(sql);

    sql := 'update TABNFE set statusnota = '+QuotedStr(statusNFE)+' where codigo >= '+edtNumeroNFEde.Text + ' and codigo <= '+edtNumeroNFEAte.Text;
    exec_sql(sql);

    MensagemSucesso('Situa��o alterada com sucesso');
    FDataModulo.IBTransaction.CommitRetaining;
  except
    on e:Exception do
    begin
      FDataModulo.IBTransaction.RollbackRetaining;
      MensagemErro(e.Message);
    end;
  end;

end;

procedure TFNfeDigitada.lerAbaGeral;
var
  objConf:TobjSqlTxt;
  inf:string;
begin

  objConf := TobjSqlTxt.Create(nil,ExtractFilePath(Application.ExeName)+'sqlNFE.txt');
  
  try

    rgTipoDanfe.ItemIndex := AnsiIndexStr(objConf.readString('NFE ORIENTACAO DANFE',true),['RETRATO','PAISAGEM']);
    rgFormaEmissao.ItemIndex := AnsiIndexStr(objConf.readString('NFE FORMA DE EMISAO DO DANFE',true),['NORMAL ON-LINE','CONTINGENCIA','SCAN','DPEC','FSDA']);
    rgTipoAmb.ItemIndex := AnsiIndexStr(objConf.readString('NFE AMBIENTE',true),['HOMOLOGACAO','PRODUCAO']);
    cbUF.ItemIndex := cbUF.Items.Add(objConf.readString('NFE UF WEBSERVICE',true));
    ckVisualizar.Checked := StrToBoolDef(objConf.readString('NFE VISUALIZAR MENSAGEM DE TRANSMISSAO',true),false);
    edtCertificado.Text := objConf.readString('CERTIFICADO',true);
    ckZeraImpostoSimples.Checked := StrToBoolDef(objConf.readString('ZERA IMPOSTO NO SIMPLES',true),false);
    ckExpandirLogoMarca.Checked  := StrToBoolDef(objconf.readString('EXPANDIR LOGO MARCA',true),false);

  finally
    FreeAndNil(objConf);
  end
end;

procedure TFNfeDigitada.btCertificadoClick(Sender: TObject);
begin
   {$IFNDEF ACBrNFeOpenSSL}
    if objTransNFE.selecionaCertificado <> '' then
      edtCertificado.Text := objTransNFE.selecionaCertificado;
   {$ENDIF}  
end;

procedure TFNfeDigitada.SpeedButton7Click(Sender: TObject);
begin
  self.gravaAbaGeral;
end;

procedure TFNfeDigitada.gravaAbaGeral;
var
  objConf:TobjSqlTxt;
begin

  objConf := TobjSqlTxt.Create(nil,ExtractFilePath(Application.ExeName)+'sqlNFE.txt');

  try
    try
      objConf.writeString('NFE UF WEBSERVICE',Trim(cbUF.Text));
      objConf.writeString('NFE AMBIENTE',rgTipoAmb.Items[rgTipoAmb.itemIndex]);
      objConf.writeString('NFE VISUALIZAR MENSAGEM DE TRANSMISSAO',BoolToStr(ckVisualizar.Checked));
      objConf.writeString('ZERA IMPOSTO NO SIMPLES',BoolToStr(ckZeraImpostoSimples.Checked));
      objConf.writeString('NFE FORMA DE EMISAO DO DANFE',rgFormaEmissao.Items[rgFormaEmissao.itemIndex]);
      objConf.writeString('NFE ORIENTACAO DANFE',rgTipoDanfe.Items[rgTipoDanfe.itemIndex]);
      objConf.writeString('CERTIFICADO',edtCertificado.Text);
      objConf.writeString('EXPANDIR LOGO MARCA',BoolToStr(ckExpandirLogoMarca.Checked));
    except
      on e:exception do
      begin
        MensagemErro(e.Message);
        Exit;
      end;
    end;
  finally
    FreeAndNil(objConf);
  end;

  MensagemSucesso('Configurado com sucesso');

end;

procedure TFNfeDigitada.Timer1Timer(Sender: TObject);
begin
  habilitaCampoObrigatorio(edtCaminhoXML,lbCaminhoXml);
  habilitaCampoObrigatorio(edtCartaCorrecao,lbCartaCorrecao);
  habilitaCampoObrigatorio(edtCancelamento,lbCancelamento);
  habilitaCampoObrigatorio(edtInutilizacao,lbInutilizacao);
  habilitaCampoObrigatorio(edtXMLBackup,lbBackup);
  habilitaCampoObrigatorio(edtArquivoRave,lbArquivoRave);
end;

procedure TFNfeDigitada.habilitaCampoObrigatorio(edit:TEdit;lb:TLabel);
begin
  if edit.Text = '' then
    lb.Visible := not (lb.Visible)
  else
    lb.Visible := False;
end;

procedure TFNfeDigitada.pageArquivoConfChange(Sender: TObject);
begin
  if pageArquivoConf.TabIndex <> 1 then
    Timer1.Enabled := False;

  case pageArquivoConf.TabIndex of
    0:;
    1:
    begin
      self.lerAbaCaminhos;
      self.Timer1.Enabled := True;
    end;
    2:;
  else;
  end;
end;

procedure TFNfeDigitada.lerAbaCaminhos;
var
  objConf:TobjSqlTxt;
  inf:string;
begin

   objConf := TobjSqlTxt.Create(nil,ExtractFilePath(Application.ExeName)+'sqlNFE.txt');

   try
    edtLogoMarca.Text     := objConf.readString('NFE CAMINHO LOGO MARCA DANFE',true);
    ckSalvarResp.Checked  := StrToBoolDef(objConf.readString('NFE SALVAR ARQUIVOS DE ENVIO E RESPOSTA',true),false);
    edtPathLogs.Text      := objConf.readString('NFE CAMINHO DOS ARQUIVOS DE ENVIO E RESPOSTA',true);
    edtCaminhoXML.Text    := objConf.readString('NFE CAMINHO DOS ARQUIVOS XML',true);
    edtCartaCorrecao.Text := objConf.readString('CAMINHO XML DA CARTA DE CORRECAO',true);
    edtCancelamento.Text  := objConf.readString('NFE CAMINHO DOS ARQUIVOS DE CANCELAMENTO',true);
    edtInutilizacao.Text  := objConf.readString('NFE CAMINHO DOS ARQUIVOS DE INUTILIZACAO',true);
    edtXMLBackup.Text     := objConf.readString('NFE DIRETORIO BACKUP',true);
    edtArquivoRave.Text   := objConf.readString('NFE ARQUIVO RAVE',true);
   finally
     FreeAndNil(objConf);
   end;

end;

procedure TFNfeDigitada.sbtLogoMarcaClick(Sender: TObject);
begin

  OpenDialog1.Filter := 'Arquivos de imagem (*.BMP)|*.BMP';
  if OpenDialog1.Execute then
    edtLogoMarca.Text := OpenDialog1.FileName;
    
end;

procedure TFNfeDigitada.SpeedButton2Click(Sender: TObject);
var
  info:string;
begin
  SelectDirectory(info,[sdAllowCreate],0);
  if info <> '' then
    edtCaminhoXML.Text := info;
end;

procedure TFNfeDigitada.SpeedButton3Click(Sender: TObject);
var
  info:string;
begin
  SelectDirectory(info,[sdPerformCreate],0);
  if info <> '' then
   edtCartaCorrecao.Text := info;
end;

procedure TFNfeDigitada.SpeedButton4Click(Sender: TObject);
var
  info:string;
begin

  SelectDirectory(info,[sdPerformCreate],0);
  if info <> '' then
    edtCancelamento.Text := info;

end;


procedure TFNfeDigitada.SpeedButton5Click(Sender: TObject);
var
  info:string;
begin

  SelectDirectory(info,[sdPerformCreate],0);
  if info <> '' then
    edtInutilizacao.Text := info;

end;

procedure TFNfeDigitada.SpeedButton9Click(Sender: TObject);
var
  info:string;
begin

  SelectDirectory(info,[sdPerformCreate],0);
  if info <> '' then
    edtXMLBackup.Text := info;

end;

procedure TFNfeDigitada.SpeedButton6Click(Sender: TObject);
var
  info:string;
begin

  OpenDialog1.Filter := 'Arquivos rave (*.RAV)|*.RAV';
  if OpenDialog1.Execute then
    edtArquivoRave.Text := OpenDialog1.FileName;
    
end;


procedure TFNfeDigitada.SpeedButton8Click(Sender: TObject);
begin
  CaminhoXMLExit(edtPathLogs);
  CaminhoXMLExit(edtCaminhoXML);
  CaminhoXMLExit(edtCartaCorrecao);
  CaminhoXMLExit(edtCancelamento);
  CaminhoXMLExit(edtInutilizacao);
  CaminhoXMLExit(edtXMLBackup);
  self.gravaAbaCaminhos;
end;

procedure TFNfeDigitada.CaminhoXMLExit(Edit: TEdit);
begin
  if edit.Text <> '' then
    if edit.Text[Length(Edit.Text)] <> '\' then
      edit.Text := Edit.Text + '\';
end;

procedure TFNfeDigitada.gravaAbaCaminhos;
var
  objConf:TobjSqlTxt;
begin

  if (ckSalvarResp.Checked) and (Trim(edtPathLogs.Text) = '') then
  begin
    MensagemAviso('Se optar por salvar arquivos de envio e resposta preencha o diret�rio');
    Exit;
  end;

  objConf := TobjSqlTxt.Create(nil,ExtractFilePath(Application.ExeName)+'sqlNFE.txt');
  try

    try

      objConf.writeString('NFE CAMINHO LOGO MARCA DANFE',edtLogoMarca.Text);
      objConf.writeString('NFE SALVAR ARQUIVOS DE ENVIO E RESPOSTA',BoolToStr(ckSalvarResp.checked));
      objConf.writeString('NFE CAMINHO DOS ARQUIVOS DE ENVIO E RESPOSTA',edtPathLogs.Text);
      objConf.writeString('NFE CAMINHO DOS ARQUIVOS XML',edtCaminhoXML.Text);
      objConf.writeString('CAMINHO XML DA CARTA DE CORRECAO',edtCartaCorrecao.Text);
      objConf.writeString('NFE CAMINHO DOS ARQUIVOS DE CANCELAMENTO',edtCancelamento.Text);
      objConf.writeString('NFE CAMINHO DOS ARQUIVOS DE INUTILIZACAO',edtInutilizacao.Text);
      objConf.writeString('NFE DIRETORIO BACKUP',edtXMLBackup.Text);
      objConf.writeString('NFE ARQUIVO RAVE',edtArquivoRave.Text);
    except
      on e:Exception do
      begin
        MensagemErro(e.Message);
        Exit;
      end;
    end;

  finally
    FreeAndNil(objConf);
  end;

  MensagemSucesso('Configurado com sucesso');

end;

procedure TFNfeDigitada.Label48Click(Sender: TObject);
var
  path:string;
begin

  path := ExtractFilePath(Application.ExeName)+'sqlNFE.txt';

  if not (FileExists(path)) then
    MensagemAviso('O arquivo ou diret�rio n�o existe')
  else
    ShellExecute(Application.Handle,PChar('open'),PChar('explorer.exe'),PChar(path),nil,SW_SHOWMAXIMIZED);

end;


procedure TFNfeDigitada.Label49Click(Sender: TObject);
var
  msg:string;
begin

  if edtPathLogs.Text <> '' then
    criarDiretorio(edtPathLogs.Text,msg);

  criarDiretorio(edtCaminhoXML.Text,msg);
  criarDiretorio(edtCartaCorrecao.Text,msg);
  criarDiretorio(edtCancelamento.Text,msg);
  criarDiretorio(edtInutilizacao.Text,msg);
  criarDiretorio(edtXMLBackup.Text,msg);

  mensagemSucesso(msg);

end;

procedure TFNfeDigitada.rgSqlClick(Sender: TObject);
begin
  memoSQL.Text := objTransNFE.getCampoSQL2(rgSql.Items[rgSql.ItemIndex]);
end;

procedure TFNfeDigitada.btSalvarSqlClick(Sender: TObject);
begin
  self.gravaSql;
end;

procedure TFNfeDigitada.gravaSql;
var
  objConf:TobjSqlTxt;
begin

  if memoSQL.Text[Length(memoSQL.Text)] = ';' then
  begin
    MensagemAviso('Retire o ponto e virgula no final do sql ";"');
    Exit;
  end;

  objConf := TobjSqlTxt.Create(nil,ExtractFilePath(Application.ExeName)+'sqlNFE.txt');
  try
    objConf.writeString(rgSql.Items[rgSql.itemIndex],memoSQL.Text);
  finally
    FreeAndNil(objConf);
  end;

  MensagemSucesso('Configurado com sucesso');
end;

procedure TFNfeDigitada.ckQuebraLinhaClick(Sender: TObject);
begin
  memoSQL.WordWrap := ckQuebraLinha.Checked;
  if memoSQL.WordWrap then
    memoSQL.ScrollBars := ssVertical
  else
    memoSQL.ScrollBars := ssBoth;
end;

procedure TFNfeDigitada.edtDataSaidaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = vk_space) then
    self.edtDataSaida.Text := formatdatetime('dd/mm/yyyy',now);
end;

procedure TFNfeDigitada.edtDataEmissaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = vk_space) then
    self.edtDataEmissao.Text := formatdatetime('dd/mm/yyyy',now);
end;

procedure TFNfeDigitada.edtHoraSaidaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = vk_space) then
    self.edtHoraSaida.Text := formatdatetime('hh:mm:ss',now);
end;

procedure TFNfeDigitada.Image1Click(Sender: TObject);
var
  fAcertaImposto:TfAcertaImpostoSafira;
begin

  if lbNota.Caption <> '' then
  begin
    MensagemAviso('N�o � possivel alterar os impostos com nota gerada');
    Exit;
  end;

  if lbCodigo.Caption = '' then Exit;

  fAcertaImposto := TfAcertaImpostoSafira.Create(nil);
  fAcertaImposto.nfedigitada := lbCodigo.Caption;
  fAcertaImposto.ShowModal;

  if fAcertaImposto.Tag = 1 then
  begin
    ObjPRODNFEDIGITADA.NFEDIGITADA.LocalizaCodigo(lbCodigo.Caption);
    TabelaParaControles;
    objPRODNFEDIGITADA.retornaProdutos (lbCodigo.Caption);
    formataGrid();
  end;

  Application.ProcessMessages;
  FreeAndNil(fAcertaImposto);

end;


procedure TFNfeDigitada.edtcfopKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  ObjPRODNFEDIGITADA.EdtCFOPKeyDown(Sender,Key,Shift);
end;

procedure TFNfeDigitada.edtHoraEmissaoKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin

  if (Key = vk_space) then
    self.edtHoraEmissao.Text := formatdatetime('hh:mm:ss',now);
    
end;

procedure TFNfeDigitada.btSalvarRespClick(Sender: TObject);
var
  info:string;
begin
  SelectDirectory(info,[sdAllowCreate],0);
  if info <> '' then
    edtPathLogs.Text := info;
end;

procedure TFNfeDigitada.Recuperaxmlduplicidade1Click(Sender: TObject);
begin
  ObjPRODNFEDIGITADA.recuperaXmlDuplicidade;
end;

procedure TFNfeDigitada.edtNumeroNFEAteChange(Sender: TObject);
begin
  comboStatus.ItemIndex := 0;
end;

procedure TFNfeDigitada.comboStatusKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if Key = vk_return then
    bt_okClick(bt_ok);
    
end;

procedure TFNfeDigitada.VincularNF1Click(Sender: TObject);
begin

  ObjPRODNFEDIGITADA.vincularNFE(lbcodigo.Caption);

  self.objPRODNFEDIGITADA.NfeDigitada.LocalizaCodigo(lbcodigo.caption);
  self.objPRODNFEDIGITADA.NfeDigitada.tabelaparaObjeto;
  self.ObjetoParaControles;

end;

procedure TFNfeDigitada.limpaEnderecoEntrega;
begin
  ObjPRODNFEDIGITADA.limpaEnderecoEntrega;
  checaEnderecoEntrega;
end;

procedure TFNfeDigitada.lbEnderecoEntregaClick(Sender: TObject);
begin
  if(Trim(ObjPRODNFEDIGITADA.end_Cpf_Cnpj) = '') then
    exit;

  MensagemSucesso('Novo endere�o de entrega para o CPF/CNPJ: ' + ObjPRODNFEDIGITADA.end_Cpf_Cnpj +
    ' Endere�o: ' + ObjPRODNFEDIGITADA.end_Rua + ' N�mero: ' + ObjPRODNFEDIGITADA.end_Numero +
    ' Compl: ' + ObjPRODNFEDIGITADA.end_Complemento + ' Bairro: ' + ObjPRODNFEDIGITADA.end_Bairro +
    ' Cidade: ' + ObjPRODNFEDIGITADA.end_Municipio + ' Estado: ' + ObjPRODNFEDIGITADA.end_UF);
end;

procedure TFNfeDigitada.checaEnderecoEntrega;
begin
  if( ObjPRODNFEDIGITADA.end_Cpf_Cnpj <> '' ) then
    lbEnderecoEntrega.Visible := true
  else
    lbEnderecoEntrega.Visible := false;
end;

end.



