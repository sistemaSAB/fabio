object FCOMPONENTE_PP: TFCOMPONENTE_PP
  Left = 295
  Top = 124
  Width = 875
  Height = 295
  Caption = 'Cadastro de Componentes do Pedido Projeto - EXCLAIM TECNOLOGIA'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbLbPEDIDOPROJETO: TLabel
    Left = 21
    Top = 60
    Width = 96
    Height = 13
    Caption = 'Pedido Projeto'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbLbCOMPONENTE: TLabel
    Left = 21
    Top = 113
    Width = 81
    Height = 13
    Caption = 'Componente'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbLbLARGURA: TLabel
    Left = 21
    Top = 166
    Width = 51
    Height = 13
    Caption = 'Largura'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbLbALTURA: TLabel
    Left = 133
    Top = 166
    Width = 40
    Height = 13
    Caption = 'Altura'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object imgrodape: TImage
    Left = 0
    Top = 215
    Width = 859
    Height = 42
    Align = alBottom
    Stretch = True
  end
  object edtPEDIDOPROJETO: TEdit
    Left = 21
    Top = 76
    Width = 100
    Height = 19
    Color = 6073854
    MaxLength = 9
    TabOrder = 0
    OnDblClick = edtPEDIDOPROJETODblClick
    OnExit = edtPEDIDOPROJETOExit
    OnKeyDown = edtPEDIDOPROJETOKeyDown
  end
  object edtCOMPONENTE: TEdit
    Left = 21
    Top = 129
    Width = 100
    Height = 19
    Color = 6073854
    MaxLength = 9
    TabOrder = 1
    OnDblClick = edtCOMPONENTEDblClick
    OnExit = edtCOMPONENTEExit
    OnKeyDown = edtCOMPONENTEKeyDown
  end
  object edtALTURA: TEdit
    Left = 133
    Top = 182
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 2
  end
  object edtLARGURA: TEdit
    Left = 21
    Top = 182
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 3
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 859
    Height = 53
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 4
    DesignSize = (
      859
      53)
    object lbnomeformulario: TLabel
      Left = 525
      Top = 3
      Width = 147
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Componentes do'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbcodigo: TLabel
      Left = 680
      Top = 9
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb1: TLabel
      Left = 525
      Top = 25
      Width = 127
      Height = 44
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Pedido Projeto'#13#10
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object btrelatorio: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
      Spacing = 0
    end
  end
end
