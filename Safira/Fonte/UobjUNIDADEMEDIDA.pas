unit UobjUNIDADEMEDIDA;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
;
//USES_INTERFACE


Type
   TObjUNIDADEMEDIDA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_SIGLA(parametro: string);
                Function Get_SIGLA: string;
                Procedure Submit_DESCRICAO(parametro: string);
                Function Get_DESCRICAO: string;

                function existeUnidade(pUnidade:string):Boolean;
                function insereUnidade:Boolean;

         Private
               Objquery:Tibquery;
               queryTemp:TIBQuery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               SIGLA:string;
               DESCRICAO:string;
//CODIFICA VARIAVEIS PRIVADAS





               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;





Function  TObjUNIDADEMEDIDA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.SIGLA:=fieldbyname('SIGLA').asstring;
        Self.DESCRICAO:=fieldbyname('DESCRICAO').asstring;
//CODIFICA TABELAPARAOBJETO



        result:=True;
     End;
end;


Procedure TObjUNIDADEMEDIDA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('SIGLA').asstring:=Self.SIGLA;
        ParamByName('DESCRICAO').asstring:=Self.DESCRICAO;
//CODIFICA OBJETOPARATABELA



  End;
End;

//***********************************************************************

function TObjUNIDADEMEDIDA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjUNIDADEMEDIDA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        SIGLA:='';
        DESCRICAO:='';
//CODIFICA ZERARTABELA



     End;
end;

Function TObjUNIDADEMEDIDA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/C�digo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjUNIDADEMEDIDA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjUNIDADEMEDIDA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjUNIDADEMEDIDA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjUNIDADEMEDIDA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjUNIDADEMEDIDA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro UNIDADEMEDIDA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,SIGLA,DESCRICAO');
           SQL.ADD(' from  TABUNIDADEMEDIDA');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjUNIDADEMEDIDA.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjUNIDADEMEDIDA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjUNIDADEMEDIDA.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjUNIDADEMEDIDA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        self.queryTemp:=TIBQuery.Create(nil);
        Self.queryTemp.Database:=FDataModulo.IBDatabase;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABUNIDADEMEDIDA(Codigo,SIGLA,DESCRICAO');
                InsertSQL.add(' )');
                InsertSQL.add('values (:Codigo,:SIGLA,:DESCRICAO)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABUNIDADEMEDIDA set Codigo=:Codigo,SIGLA=:SIGLA');
                ModifySQL.add(',DESCRICAO=:DESCRICAO');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABUNIDADEMEDIDA where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjUNIDADEMEDIDA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjUNIDADEMEDIDA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabUNIDADEMEDIDA');
     Result:=Self.ParametroPesquisa;
end;

function TObjUNIDADEMEDIDA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de UNIDADEMEDIDA ';
end;


function TObjUNIDADEMEDIDA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENUNIDADEMEDIDA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENUNIDADEMEDIDA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjUNIDADEMEDIDA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(self.queryTemp);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjUNIDADEMEDIDA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjUNIDADEMEDIDA.RetornaCampoNome: string;
begin
      result:='descricao';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjUNIDADEMEDIDA.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjUNIDADEMEDIDA.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjUNIDADEMEDIDA.Submit_SIGLA(parametro: string);
begin
        Self.SIGLA:=Parametro;
end;
function TObjUNIDADEMEDIDA.Get_SIGLA: string;
begin
        Result:=Self.SIGLA;
end;
procedure TObjUNIDADEMEDIDA.Submit_DESCRICAO(parametro: string);
begin
        Self.DESCRICAO:=Parametro;
end;
function TObjUNIDADEMEDIDA.Get_DESCRICAO: string;
begin
        Result:=Self.DESCRICAO;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjUNIDADEMEDIDA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJUNIDADEMEDIDA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



function TObjUNIDADEMEDIDA.existeUnidade(pUnidade: string): Boolean;
begin

  self.Objquery.Active:=False;
  self.Objquery.SQL.Clear;
  self.Objquery.SQL.Add('select codigo from tabunidademedida where upper(sigla) = upper('+#39+pUnidade+#39+')');

  self.Objquery.Active:=True;
  self.Objquery.FetchAll;

  if (self.Objquery.RecordCount > 0) then
    result:=true
  else
    result:=False;

end;

function TObjUNIDADEMEDIDA.insereUnidade:Boolean;
begin

  result:= false;

  try

    {UNIDADES UTILIZADAS NA ENTRADA}

    self.queryTemp.SQL.Clear;

    self.queryTemp.SQL.Add('select distinct(pm_ep.unidade) as unidademedida');
    self.queryTemp.SQL.Add('from proc_materiais_ep  pm_ep');
    self.queryTemp.SQL.Add('where pm_ep.unidade <> '''' and pm_ep.unidade is not null');

    self.queryTemp.Active:=True;
    self.queryTemp.First;

    while not (self.queryTemp.Eof) do
    begin

      if not (existeUnidade (self.queryTemp.fieldbyname('unidadeMedida').AsString)) then
      begin

        self.Status:=dsInsert;
        self.Codigo:='0';
        self.SIGLA:=self.queryTemp.fieldbyname('unidadeMedida').AsString;

        if not (self.Salvar(false)) then
        begin

          MensagemErro('N�o foi possivel salvar unidade de medida: '+self.SIGLA);
          Exit;

        end;
                      
      end;

      self.queryTemp.Next;

    end;

    {UNIDADES UTILIZADAS NO PRODUTO}

    self.queryTemp.SQL.Clear;

    //seleciona unidades usando nas ferragens
    self.queryTemp.SQL.Add('select distinct(unidade) as unidademedida');
    self.queryTemp.SQL.Add('from tabferragem');
    self.queryTemp.SQL.Add('where unidade <> '''' and unidade is not null');
    self.queryTemp.Active:=True;
    self.queryTemp.First;
    while not (self.queryTemp.Eof) do
    begin

      if not (existeUnidade (self.queryTemp.fieldbyname('unidadeMedida').AsString)) then
      begin

        self.Status:=dsInsert;
        self.Codigo:='0';
        self.SIGLA:=self.queryTemp.fieldbyname('unidadeMedida').AsString;

        if not (self.Salvar(false)) then
        begin

          MensagemErro('N�o foi possivel salvar unidade de medida: '+self.SIGLA);
          Exit;

        end;

      end;

      self.queryTemp.Next;

    end;

    //seleciona unidades usando nos perfilados
    self.queryTemp.SQL.Clear;
    self.queryTemp.SQL.Add('select distinct(unidade) as unidademedida');
    self.queryTemp.SQL.Add('from tabperfilado');
    self.queryTemp.SQL.Add('where unidade <> '''' and unidade is not null');
    self.queryTemp.Active:=True;
    self.queryTemp.First;
    while not (self.queryTemp.Eof) do
    begin

      if not (existeUnidade (self.queryTemp.fieldbyname('unidadeMedida').AsString)) then
      begin

        self.Status:=dsInsert;
        self.Codigo:='0';
        self.SIGLA:=self.queryTemp.fieldbyname('unidadeMedida').AsString;

        if not (self.Salvar(false)) then
        begin

          MensagemErro('N�o foi possivel salvar unidade de medida: '+self.SIGLA);
          Exit;

        end;

      end;

      self.queryTemp.Next;

    end;
    //seleciona unidades usando nos kitbox
    self.queryTemp.SQL.Clear;
    self.queryTemp.SQL.Add('select distinct(unidade) as unidademedida');
    self.queryTemp.SQL.Add('from tabkitbox');
    self.queryTemp.SQL.Add('where unidade <> '''' and unidade is not null');
    self.queryTemp.Active:=True;
    self.queryTemp.First;
    while not (self.queryTemp.Eof) do
    begin

      if not (existeUnidade (self.queryTemp.fieldbyname('unidadeMedida').AsString)) then
      begin

        self.Status:=dsInsert;
        self.Codigo:='0';
        self.SIGLA:=self.queryTemp.fieldbyname('unidadeMedida').AsString;

        if not (self.Salvar(false)) then
        begin

          MensagemErro('N�o foi possivel salvar unidade de medida: '+self.SIGLA);
          Exit;

        end;

      end;

      self.queryTemp.Next;

    end;
    {//seleciona unidades usando nas persiana
    self.queryTemp.SQL.Clear;
    self.queryTemp.SQL.Add('select distinct(unidade) as unidademedida');
    self.queryTemp.SQL.Add('from tabferragem');
    self.queryTemp.Active:=True;
    self.queryTemp.First;
    while not (self.queryTemp.Eof) do
    begin

      if not (existeUnidade (self.queryTemp.fieldbyname('unidadeMedida').AsString)) then
      begin

        self.Status:=dsInsert;
        self.Codigo:='0';
        self.SIGLA:=self.queryTemp.fieldbyname('unidadeMedida').AsString;

        if not (self.Salvar(false)) then
        begin

          MensagemErro('N�o foi possivel salvar unidade de medida: '+self.SIGLA);
          Exit;

        end;

      end;

      self.queryTemp.Next;

    end;   }
    //seleciona unidades usando nas diversos
    self.queryTemp.SQL.Clear;
    self.queryTemp.SQL.Add('select distinct(unidade) as unidademedida');
    self.queryTemp.SQL.Add('from tabdiverso');
    self.queryTemp.SQL.Add('where unidade <> '''' and unidade is not null');
    self.queryTemp.Active:=True;
    self.queryTemp.First;
    while not (self.queryTemp.Eof) do
    begin

      if not (existeUnidade (self.queryTemp.fieldbyname('unidadeMedida').AsString)) then
      begin

        self.Status:=dsInsert;
        self.Codigo:='0';
        self.SIGLA:=self.queryTemp.fieldbyname('unidadeMedida').AsString;

        if not (self.Salvar(false)) then
        begin

          MensagemErro('N�o foi possivel salvar unidade de medida: '+self.SIGLA);
          Exit;

        end;

      end;

      self.queryTemp.Next;

    end;
    //seleciona unidades usando nas vidros
    self.queryTemp.SQL.Clear;
    self.queryTemp.SQL.Add('select distinct(unidade) as unidademedida');
    self.queryTemp.SQL.Add('from tabvidro');
    self.queryTemp.SQL.Add('where unidade <> '''' and unidade is not null');
    self.queryTemp.Active:=True;
    self.queryTemp.First;
    while not (self.queryTemp.Eof) do
    begin

      if not (existeUnidade (self.queryTemp.fieldbyname('unidadeMedida').AsString)) then
      begin

        self.Status:=dsInsert;
        self.Codigo:='0';
        self.SIGLA:=self.queryTemp.fieldbyname('unidadeMedida').AsString;

        if not (self.Salvar(false)) then
        begin

          MensagemErro('N�o foi possivel salvar unidade de medida: '+self.SIGLA);
          Exit;

        end;

      end;

      self.queryTemp.Next;

    end;

  except

    on e:Exception do
    begin

      MensagemErro(e.Message);
      Exit;
      
    end;

  end;

  result:= True;

end;

end.



