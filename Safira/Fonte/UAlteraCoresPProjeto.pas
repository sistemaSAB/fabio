unit UAlteraCoresPProjeto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, StdCtrls, Buttons, ImgList,IBQuery, UobjCOR,
  Menus,UUtils;

type
  TFAlterarCoresPProjetos = class(TForm)
    pnl1: TPanel;
    btCancelar: TSpeedButton;
    pnl6: TPanel;
    lbmensagem: TLabel;
    pnl2: TPanel;
    Img1: TImage;
    pnl3: TPanel;
    btProcessar: TSpeedButton;
    STRGMateriais: TStringGrid;
    ilProdutos: TImageList;
    PopUpCadastros: TPopupMenu;
    MarcarProjetosIguais: TMenuItem;
    CopiarCorProjetosMarcados: TMenuItem;
    Proximoprojeto: TMenuItem;
    Projetoanterior: TMenuItem;
    DesmarcarProjetosIguais1: TMenuItem;
    chk1: TCheckBox;
    lb1: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    lb5: TLabel;
    lb7: TLabel;
    bt3: TSpeedButton;
    lb8: TLabel;
    AlterarCor1: TMenuItem;
    lb6: TLabel;
    MarcarRplicasdesseProjeto1: TMenuItem;
    btProximo: TSpeedButton;
    btAnterior: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure STRGMateriaisDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure tamClose(Sender: TObject; var Action: TCloseAction);
    procedure STRGMateriaisDblClick(Sender: TObject);
    procedure STRGMateriaisKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtCorVidroKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure btCancelarClick(Sender: TObject);
    procedure STRGMateriaisMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure MarcarProjetosIguaisClick(Sender: TObject);
    procedure DesmarcarProjetosIguais1Click(Sender: TObject);
    procedure CopiarCorProjetosMarcadosClick(Sender: TObject);
    procedure ProximoprojetoClick(Sender: TObject);
    procedure ProjetoanteriorClick(Sender: TObject);
    procedure AlterarCor1Click(Sender: TObject);
    procedure btProcessarClick(Sender: TObject);
    procedure STRGMateriaisClick(Sender: TObject);
    procedure MarcarRplicasdesseProjeto1Click(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);

  private
         Pedido:string;
         Query:TIBQuery;
         CorVidro:TOBJCOR;
         procedure GeraStringGrid;
         function VerificaPedidoProjetoPrincipal:Boolean;

  public
    { Public declarations }
  end;

var
  FAlterarCoresPProjetos: TFAlterarCoresPProjetos;

implementation

uses UescolheImagemBotao,UDataModulo, Upesquisa,UessencialGlobal;

{$R *.dfm}

procedure TFAlterarCoresPProjetos.FormShow(Sender: TObject);
begin
     FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');
     Pedido:=IntToStr(Tag);
     lbmensagem.Caption:='Projetos do Pedido : '+Pedido;
     Query:=TIBQuery.Create(nil);
     Query.Database:=FDataModulo.IBDatabase;
     Self.CorVidro:=TOBJCOR.create;
     GeraStringGrid;
end;

procedure TFAlterarCoresPProjetos.STRGMateriaisDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}  
begin


    if((ARow <> 0) and (ACol = 0))
    then begin
        if(STRGMateriais.Cells[ACol,ARow] = '            X' ) or (STRGMateriais.Cells[ACol,ARow] = 'X' )then
        begin
              Ilprodutos.Draw(STRGMateriais.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 6); //check box marcado
        end
        else
        begin
              Ilprodutos.Draw(STRGMateriais.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 5); //check box sem marcar    5
        end;
    end;

    //Colocando a imagem com a cor da cor escolhida
    if((ARow <> 0) and (ACol = 2))
    then begin

        // ShowMessage(STRGMateriais.Cells[2,ARow]+'-');
         if(STRGMateriais.Cells[2,ARow]='VERDE') or (PegaPrimeiraPalavra(STRGMateriais.Cells[2,ARow])='VERDE')
         then Ilprodutos.Draw(STRGMateriais.Canvas, LM*0 + Rect.Left+1 , TM + Rect.Top+1, 0)
         else begin
              if(STRGMateriais.Cells[2,ARow]='INCOLOR') or (STRGMateriais.Cells[2,ARow]='BRANCO')or (PegaPrimeiraPalavra(STRGMateriais.Cells[2,ARow])='INCOLOR')
              then Ilprodutos.Draw(STRGMateriais.Canvas,LM*0 + Rect.Left+1 , TM + Rect.Top+1, 1)
              else begin
                if(STRGMateriais.Cells[2,ARow]='VERMELHO')or (PegaPrimeiraPalavra(STRGMateriais.Cells[2,ARow])='VERMELHO')
                then Ilprodutos.Draw(STRGMateriais.Canvas, LM*0 + Rect.Left+1 , TM + Rect.Top+1, 7)
                else
                begin
                     if(STRGMateriais.Cells[2,ARow]='CINZA') or (STRGMateriais.Cells[2,ARow]='PRATA') or (PegaPrimeiraPalavra(STRGMateriais.Cells[2,ARow])='CINZA')
                     then Ilprodutos.Draw(STRGMateriais.Canvas, LM*0 + Rect.Left+1 , TM + Rect.Top+1, 12)
                     else begin
                         if(STRGMateriais.Cells[2,ARow]='CROMADO') or (PegaPrimeiraPalavra(STRGMateriais.Cells[2,ARow])='CROMADO')
                         then Ilprodutos.Draw(STRGMateriais.Canvas, LM*0 + Rect.Left+1 , TM + Rect.Top+1, 11)
                         else begin
                             if(STRGMateriais.Cells[2,ARow]='BRONZE') or (PegaPrimeiraPalavra(STRGMateriais.Cells[2,ARow])='BRONZE')
                             then Ilprodutos.Draw(STRGMateriais.Canvas, LM*0 + Rect.Left+1 , TM + Rect.Top+1, 13)
                             else Ilprodutos.Draw(STRGMateriais.Canvas, LM*0 + Rect.Left+1 , TM + Rect.Top+1, 1);
                         end;

                     end;

                end;

              end;
         end  
    end;

    //Colocando a imagem com a cor da cor anterior
    if((ARow <> 0) and (ACol = 3))
    then begin
        // ShowMessage(STRGMateriais.Cells[2,ARow]+'-');
         if(STRGMateriais.Cells[3,ARow]='VERDE')or (PegaPrimeiraPalavra(STRGMateriais.Cells[3,ARow])='VERDE')
         then Ilprodutos.Draw(STRGMateriais.Canvas, LM*0 + Rect.Left+1 , TM + Rect.Top+1, 0)
         else begin
              if(STRGMateriais.Cells[3,ARow]='INCOLOR') or (STRGMateriais.Cells[3,ARow]='BRANCO')or (PegaPrimeiraPalavra(STRGMateriais.Cells[3,ARow])='INCOLOR')
              then Ilprodutos.Draw(STRGMateriais.Canvas, LM*0 + Rect.Left+1 , TM + Rect.Top+1, 1)
              else begin
                if(STRGMateriais.Cells[3,ARow]='VERMELHO')or (PegaPrimeiraPalavra(STRGMateriais.Cells[3,ARow])='VERMELHO')
                then Ilprodutos.Draw(STRGMateriais.Canvas, LM*0 + Rect.Left+1 , TM + Rect.Top+1, 7)
                else
                begin
                     if(STRGMateriais.Cells[3,ARow]='CINZA') or (STRGMateriais.Cells[3,ARow]='PRATA')or (PegaPrimeiraPalavra(STRGMateriais.Cells[3,ARow])='CINZA')
                     then Ilprodutos.Draw(STRGMateriais.Canvas, LM*0 + Rect.Left+1 , TM + Rect.Top+1, 12)
                     else begin
                         if(STRGMateriais.Cells[3,ARow]='CROMADO') or (PegaPrimeiraPalavra(STRGMateriais.Cells[3,ARow])='CROMADO')
                         then Ilprodutos.Draw(STRGMateriais.Canvas, LM*0 + Rect.Left+1 , TM + Rect.Top+1, 11)
                         else begin
                             if(STRGMateriais.Cells[3,ARow]='BRONZE') or (PegaPrimeiraPalavra(STRGMateriais.Cells[3,ARow])='BRONZE')
                             then Ilprodutos.Draw(STRGMateriais.Canvas, LM*0 + Rect.Left+1 , TM + Rect.Top+1, 13)
                             else Ilprodutos.Draw(STRGMateriais.Canvas, LM*0 + Rect.Left+1 , TM + Rect.Top+1, 1);
                         end;

                     end;

                end;

              end;
         end  
    end;

    {if (gdSelected in State) and (ACol<>0) then
    begin
      (Sender as TStringGrid).Canvas.brush.Color := $00A2663E;
      (Sender as TStringGrid).Canvas.Font.Style := (Sender as TStringGrid).Canvas.Font.Style +[fsBold];

      (Sender as TStringGrid).canvas.FillRect(rect);
      (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
    end;    }

end;


procedure TFAlterarCoresPProjetos.GeraStringGrid;
var
  cont,quantidade:Integer;
begin
     with STRGMateriais do
     begin
           RowCount:= 2;
           ColCount:= 11;
           FixedRows:=1;
           FixedCols:=0;

           ColWidths[0] := 50;
           ColWidths[1] := 400;
           ColWidths[2] := 150;
           ColWidths[3] := 150;
           ColWidths[4] := 0;   //80
           ColWidths[5] := 0;   //80
           ColWidths[6] := 0;   //80
           ColWidths[7] := 0;
           ColWidths[8] := 200;
           ColWidths[9] := 0;
           ColWidths[10] := 0;


           {           ColWidths[0] := 50;
           ColWidths[1] := 400;
           ColWidths[2] := 150;
           ColWidths[3] := 150;
           ColWidths[4] := 0;   //80
           ColWidths[5] := 0;   //80
           ColWidths[6] := 0;   //80
           ColWidths[7] := 100;
           ColWidths[8] := 200;
           ColWidths[9] := 80;
           ColWidths[10] := 80;}

           {ColWidths[11] := 80;
           ColWidths[12] := 100;
           ColWidths[13] := 50; }

           Cells[0,0] := '';
           Cells[1,0] := 'NOME ';
           Cells[2,0] := 'COR DO VIDRO';
           Cells[3,0] := 'COR ANTIGA';
           Cells[4,0] := 'CODIGOPROJ';
           Cells[5,0] := 'CODIGOCOR';
           Cells[6,0] := 'CODIGOPP';
           Cells[7,0] := 'CODIGOCOR ANT';
           Cells[8,0] := 'VIDRO' ;
           Cells[9,0] := 'CODVIDRO';
           Cells[10,0]:= 'PPPrincipal' ;

           
           {

           Cells[8,0] := 'ACR�SCIMO';
           Cells[9,0] := 'C�DIGO';
           Cells[10,0] :='COD MATERIAL';
           Cells[11,0] :='COD COR';
           Cells[12,0] :='VALOR FINAL';
           Cells[13,0] :='STATUS';  }
           cont:=1;
           quantidade:=0;
           with query do
           begin
                Close;
                SQL.Clear;
                SQL.Add('select projeto.codigo,projeto.descricao,pp.codigo as codigopp,cor.codigo as codigocor, cor.descricao as nomecor,vidro.codigo as codigovidro, vidro.descricao as nomevidro');
                SQL.Add(',pp.pedidoprojetoprincipal from tabpedido pedido');
                SQL.Add('join tabpedido_proj pp on pp.pedido=pedido.codigo');
                SQL.Add('join tabprojeto projeto on projeto.codigo=pp.projeto');
                SQL.Add('join tabcor cor on cor.codigo=pp.corvidro');
                SQL.Add('join tabvidro_pp vpp on vpp.pedidoprojeto=pp.codigo');
                sql.Add('join tabvidrocor vcor on vcor.codigo=vpp.vidrocor');
                SQL.Add('join tabvidro vidro on vidro.codigo=vcor.vidro');
                SQL.Add('where pedido='+pedido);
                SQL.Add('order by pp.projeto,vidro.codigo,pp.codigo');
                //InputBox('','',sql.Text);
                Open;
                while not Eof do
                begin
                    Cells[1,cont] := fieldbyname('descricao').AsString;
                    Cells[2,cont] := '     '+tira_espacos_final(fieldbyname('nomecor').AsString);
                    Cells[3,cont] := '     '+tira_espacos_final(fieldbyname('nomecor').AsString);
                    Cells[4,cont] := fieldbyname('codigo').AsString;
                    Cells[5,cont] := fieldbyname('codigocor').AsString;
                    Cells[6,cont] := fieldbyname('codigopp').AsString;
                    Cells[7,cont] := fieldbyname('codigocor').AsString;
                    Cells[8,cont] := fieldbyname('nomevidro').AsString;
                    Cells[9,cont] := fieldbyname('codigovidro').AsString;
                    Cells[10,cont]:= fieldbyname('pedidoprojetoprincipal').AsString;
                    RowCount:=RowCount+1;
                    Inc(cont,1);
                    Next;
                end;
                RowCount:=RowCount-1;
           end;

     end;
end;

procedure TFAlterarCoresPProjetos.tamClose(Sender: TObject;
  var Action: TCloseAction);
begin
     FreeAndNil(query);
     CorVidro.Free;
end;

procedure TFAlterarCoresPProjetos.STRGMateriaisDblClick(Sender: TObject);
begin
    if (TStringGrid(sender).cells[0,TStringgrid(sender).row]='')
    Then TStringGrid(sender).cells[0,TStringgrid(sender).row]:='            X'
    Else TStringGrid(sender).cells[0,TStringgrid(sender).row]:='';
end;

procedure TFAlterarCoresPProjetos.STRGMateriaisKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      if(Key= VK_F9) then
      begin
           //Escolhe cor
           Self.EdtCorVidroKeyDown(Sender,Key,Shift);
           //Caso j� tenha projetos iguais selecionado, troca a cor neles tambem
           self.CopiarCorProjetosMarcadosClick(Sender);
      end;
      if(Key= VK_F4) then
      begin
           self.MarcarProjetosIguaisClick(Sender);
      end;
      if(Key= VK_F5) then
      begin
           Self.DesmarcarProjetosIguais1Click(Sender);
      end;
      if(Key= VK_F6) then
      begin
           self.CopiarCorProjetosMarcadosClick(Sender);
      end;
      if(Key= VK_F7) then
      begin
           Self.ProximoprojetoClick(Sender);
      end;
      if(Key= VK_F8) then
      begin
           Self.ProjetoanteriorClick(Sender);
      end;
      if(Key= 13) then
      begin
           Self.STRGMateriaisDblClick(Sender);
      end;
      if(Key= VK_F12) then
      begin
           //Mostra ou esconde codigos
           if(STRGMateriais.ColWidths[4]= 0) then
           begin

               STRGMateriais.ColWidths[4] := 80;
               STRGMateriais.ColWidths[5] := 80;
               STRGMateriais.ColWidths[6] := 80;
               STRGMateriais.ColWidths[7] := 100;
               STRGMateriais.ColWidths[9] := 80;
               STRGMateriais.ColWidths[10] := 80;
           end
           else
          begin
               STRGMateriais.ColWidths[4] := 0;
               STRGMateriais.ColWidths[5] := 0;
               STRGMateriais.ColWidths[6] := 0;
               STRGMateriais.ColWidths[7] := 0;
               STRGMateriais.ColWidths[9] := 0;
               STRGMateriais.ColWidths[10] := 0;
           end;
      end;

end;

procedure TFAlterarCoresPProjetos.EdtCorVidroKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     if(VerificaPedidoProjetoPrincipal=False)
     then Exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CorVidro.Get_PesquisaCorVidro(StrToInt(STRGMateriais.Cells[9,STRGMateriais.row])),Self.CorVidro.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 STRGMateriais.Cells[5,STRGMateriais.row]:='     '+FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 STRGMateriais.Cells[2,STRGMateriais.row]:='     '+tira_espacos_final(FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring )

                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;

end;

procedure TFAlterarCoresPProjetos.btCancelarClick(Sender: TObject);
begin
      Self.Close;
end;

procedure TFAlterarCoresPProjetos.STRGMateriaisMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
     if(STRGMateriais.Col=0)
     then STRGMateriais.Hint:='Copiar para todos os projetos iguais'
     else STRGMateriais.Hint:='';
end;

procedure TFAlterarCoresPProjetos.MarcarProjetosIguaisClick(Sender: TObject);
var
  cont:Integer;
  codigoProjeto:string;
  codigovidro:string;
begin
     cont:=1;
     codigoProjeto:=STRGMateriais.Cells[4,STRGMateriais.row];
     codigovidro:=STRGMateriais.Cells[9,STRGMateriais.Row];
     while cont<=STRGMateriais.RowCount do
     begin
            STRGMateriais.Cells[0,cont]:='';
            if(STRGMateriais.Cells[4,cont]=codigoProjeto) and (STRGMateriais.Cells[9,cont]=codigovidro)
            then STRGMateriais.Cells[0,cont]:='            X';

            Inc(cont,1);
     end;
     //4CopiarCorProjetosMarcadosClick(Sender);
end;

procedure TFAlterarCoresPProjetos.DesmarcarProjetosIguais1Click(
  Sender: TObject);
var
  cont:Integer;
  codigoProjeto:string;
  codigovidro:string;
begin
     cont:=1;
     codigoProjeto:=STRGMateriais.Cells[4,STRGMateriais.row];
     codigovidro:=STRGMateriais.Cells[9,STRGMateriais.Row];
     while cont<=STRGMateriais.RowCount do
     begin
            //Pra saber se posso considerar um projeto igual preciso saber se o projeto � o mesmo e utiliza o mesmo vidro...
            if(STRGMateriais.Cells[4,cont]=codigoProjeto) and (STRGMateriais.Cells[9,cont]=codigovidro)
            then STRGMateriais.Cells[0,cont]:='';

            Inc(cont,1);
     end;
end;

procedure TFAlterarCoresPProjetos.CopiarCorProjetosMarcadosClick(Sender: TObject);
var
  cont:Integer;
  codigoProjeto:string;
  linhaSelecionda:integer;
  codigovidro:string;
begin
     cont:=1;
     codigoProjeto:=STRGMateriais.Cells[4,STRGMateriais.row];
     linhaSelecionda:=STRGMateriais.Row;
     codigovidro:=STRGMateriais.Cells[9,STRGMateriais.Row];
     //Procuro por projetos iguais ao selecionado para alterar as cores
     while cont<=STRGMateriais.RowCount do
     begin
            // Added by Jonatan 28/03/2012 10:33:55;
            // Encontrei um projeto igual ao da linha selecionada
            if(STRGMateriais.Cells[4,cont]=codigoProjeto) and (STRGMateriais.Cells[9,cont]=codigovidro) then
            begin
                 //se estiver marcado para trocar a cor, ent�o troco
                 if(STRGMateriais.Cells[0,cont]='            X') then
                 begin
                     STRGMateriais.Cells[2,cont]:=STRGMateriais.Cells[2,linhaSelecionda];
                     STRGMateriais.Cells[5,cont]:=STRGMateriais.Cells[5,linhaSelecionda];
                     STRGMateriais.Cells[0,cont]:='';
                 end;
            end;

            Inc(cont,1);
     end;
end;

procedure TFAlterarCoresPProjetos.ProximoprojetoClick(Sender: TObject);
var
  cont:Integer;
  codigoProjeto:string;
  codigovidro:string;
begin

     codigoProjeto:=STRGMateriais.Cells[4,STRGMateriais.row];
     cont:=STRGMateriais.Row;
     codigovidro:=STRGMateriais.Cells[9,STRGMateriais.Row];
     //Procuro por projetos iguais ao selecionado para alterar as cores
     while cont<STRGMateriais.RowCount do
     begin
            // Added by Jonatan 28/03/2012 10:33:55;
            // Encontrei um projeto diferente ao da linha selecionada
            if(STRGMateriais.Cells[4,cont]<>codigoProjeto)or (STRGMateriais.Cells[9,cont]<>codigovidro) then
            begin
                STRGMateriais.Row:=cont;
                Exit;
            end;

            Inc(cont,1);
     end;
end;


procedure TFAlterarCoresPProjetos.ProjetoanteriorClick(Sender: TObject);
var
  cont:Integer;
  codigoProjeto:string;
  codigovidro:string;
begin

     codigoProjeto:=STRGMateriais.Cells[4,STRGMateriais.row];
     cont:=STRGMateriais.Row;
     codigovidro:=STRGMateriais.Cells[9,STRGMateriais.Row];
     //Procuro por projetos iguais ao selecionado para alterar as cores
     while cont>0 do
     begin
            // Added by Jonatan 28/03/2012 10:33:55;
            // Encontrei um projeto diferente ao da linha selecionada
            if(STRGMateriais.Cells[4,cont]<>codigoProjeto)or (STRGMateriais.Cells[9,cont]<>codigovidro) then
            begin
                STRGMateriais.Row:=cont;
                Exit;
            end;

            Dec(cont,1);
     end;
end;

procedure TFAlterarCoresPProjetos.AlterarCor1Click(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
begin
       key:=VK_F9;
       //Escolhe cor
       Self.EdtCorVidroKeyDown(Sender,Key,Shift);
       //Caso j� tenha projetos iguais selecionado, troca a cor neles tambem
       self.CopiarCorProjetosMarcadosClick(Sender);
end;

procedure TFAlterarCoresPProjetos.btProcessarClick(Sender: TObject);
var
  Cont:Integer;
  Cont2:integer;
  vidrocor:string;
  CorPPprincipal:string;
begin
      //Muda a cor nos projetos...
     try
        cont:=1;
        while Cont<STRGMateriais.RowCount do
        begin
              //se for um pp principal ent�o s� atualizo as novas cores
              //isso levando em considera��o de que quando � um projeto principal
              //eu n�o deixo altera-lo sem alterar as demais replicas...

              //� um pedidoprojeto que n�o � r�plica de nenhum outro ou � um pp principal
              if(STRGMateriais.Cells[10,cont]='') then
              begin
                  Query.Close;
                  query.SQL.Clear;
                  Query.SQL.Add('update tabpedido_proj set corvidro='+STRGMateriais.Cells[5,cont]);
                  Query.SQL.Add('where codigo='+STRGMateriais.Cells[6,Cont]);
                  Query.ExecSQL;
                  //Guardo a cor desse projeto, se tiver replicas, as r�plicas v�o estar abaixo desse
                  //ent�o ae verifico se a cor do vidro foi mudada

                  Query.Close;
                  Query.SQL.Clear;
                  Query.SQL.Add('select codigo from tabvidrocor where cor='+STRGMateriais.Cells[5,cont]);
                  Query.SQL.Add('and vidro='+STRGMateriais.Cells[9,cont]);
                  Query.Open;
                  vidrocor:=Query.fieldbyname('codigo').asstring;

                  Query.Close;
                  Query.SQL.Clear;
                  Query.SQL.Add('update tabvidro_pp set vidrocor='+vidrocor);
                  Query.SQL.Add('where pedidoprojeto='+STRGMateriais.Cells[6,Cont]);
                  Query.ExecSQL;

                  Inc(Cont,1);
              end
              else
              begin
                  //Se este pedidoprojeto for um r�plica, preciso verificar se foi alterado a cor do vidro...
                  //caso foi, ent�o o mesmo deixa de ser uma replica do pedidoprojeto principal e passa a ser um
                  //pedidoprojeto independente
                  cont2:=1;
                  while Cont2<STRGMateriais.RowCount do
                  begin
                        if(STRGMateriais.Cells[10,cont]=STRGMateriais.Cells[6,Cont2])then
                        begin
                             //Pego a cor do vidro do projeto principal
                             CorPPprincipal:=STRGMateriais.Cells[5,cont2];
                             Cont2:=STRGMateriais.RowCount;
                        end;
                        Inc(Cont2,1);
                  end;
                  //Se a cor for diferente, ent�o esse pedidoprojeto deixa de ser uma r�plica
                  if(CorPPprincipal<>STRGMateriais.Cells[5,cont]) then
                  begin
                        Query.Close;
                        query.SQL.Clear;
                        Query.SQL.Add('update tabpedido_proj set pedidoprojetoprincipal=null ');
                        Query.SQL.Add('where codigo='+STRGMateriais.Cells[6,Cont]);
                        Query.ExecSQL;
                  end;
                  //troca a cor
                  Query.Close;
                  query.SQL.Clear;
                  Query.SQL.Add('update tabpedido_proj set corvidro='+STRGMateriais.Cells[5,cont]);
                  Query.SQL.Add('where codigo='+STRGMateriais.Cells[6,Cont]);
                  Query.ExecSQL;

                  Query.Close;
                  Query.SQL.Clear;
                  Query.SQL.Add('select codigo from tabvidrocor where cor='+STRGMateriais.Cells[5,cont]);
                  Query.SQL.Add('and vidro='+STRGMateriais.Cells[9,cont]);
                  Query.Open;
                  vidrocor:=Query.fieldbyname('codigo').asstring;

                  Query.Close;
                  Query.SQL.Clear;
                  Query.SQL.Add('update tabvidro_pp set vidrocor='+vidrocor);
                  Query.SQL.Add('where pedidoprojeto='+STRGMateriais.Cells[6,Cont]);
                  Query.ExecSQL;

                  Inc(Cont,1);
              end;

        end;
        FDataModulo.IBTransaction.CommitRetaining;
     finally
         FDataModulo.IBTransaction.RollbackRetaining;
         MensagemAviso('Cores Alteradas!!!');
         self.Close;
     end;


end;

procedure TFAlterarCoresPProjetos.STRGMateriaisClick(Sender: TObject);
begin
      //MarcarProjetosIguaisClick(Sender);
end;

function TFAlterarCoresPProjetos.VerificaPedidoProjetoPrincipal:Boolean;
var
  cont:Integer;
  codigoProjeto:string;
  codigovidro:string;
  pedidoprojeto:string;
begin
    //Preciso verificar se este pedidoprojeto � um pedidoprojeto principal, ou
    //seja se existe algum pedidoprojeto replicado a partir desse...
    //Caso esteja, verifico se os outros pedidoprojetos r�plicas est�o marcados
    //se estiver, posso alterar de boa, sen�o, n�o posso alterar...
    Result:=True;
    with Query do
    begin
          pedidoprojeto:=STRGMateriais.Cells[6,STRGMateriais.Row];

          if(STRGMateriais.Cells[10,STRGMateriais.Row]='') then
          begin
               cont:=1;
               codigoProjeto:=STRGMateriais.Cells[4,STRGMateriais.row];
               codigovidro:=STRGMateriais.Cells[9,STRGMateriais.Row];
               while cont<=STRGMateriais.RowCount do
               begin
                      if(STRGMateriais.Cells[10,cont]=pedidoprojeto) then
                      begin
                            if(STRGMateriais.Cells[0,cont]='') then
                            begin
                                 MensagemAviso('Existem projetos replicados � partir deste, para altera-lo � preciso alterar todos as r�plicas tamb�m'+
                                 ' ou alterar uma r�plica');
                                 Result:=False;
                                 Exit;
                            end;

                      end;

                      Inc(cont,1);
               end;
          end;

    end;
end;

procedure TFAlterarCoresPProjetos.MarcarRplicasdesseProjeto1Click(
  Sender: TObject);
var
  cont:Integer;
  codigoProjeto:string;
  codigovidro:string;
  pedidoprojeto:string;
begin
     cont:=1;
     codigoProjeto:=STRGMateriais.Cells[4,STRGMateriais.row];
     codigovidro:=STRGMateriais.Cells[9,STRGMateriais.Row];
     //se for pp principal, ent�o a coluna 10 (pedidoprojetoprincipal) vai estar vazia, ent�o pego o codigo do pedido projeto
     if(STRGMateriais.Cells[10,STRGMateriais.Row]='')
     then pedidoprojeto:=STRGMateriais.Cells[6,STRGMateriais.Row]
     else pedidoprojeto:=STRGMateriais.Cells[10,STRGMateriais.Row]; //n�o pp principal, pego o codigo do pp principal

     while cont<=STRGMateriais.RowCount do
     begin
            STRGMateriais.Cells[0,cont]:='';
            if(STRGMateriais.Cells[4,cont]=codigoProjeto) and (STRGMateriais.Cells[9,cont]=codigovidro) and (STRGMateriais.Cells[10,cont]=pedidoprojeto)
            then STRGMateriais.Cells[0,cont]:='            X';
            
            if(STRGMateriais.Cells[6,cont]=pedidoprojeto)
            then STRGMateriais.Cells[0,cont]:='            X';

            Inc(cont,1);
     end;
     //4CopiarCorProjetosMarcadosClick(Sender);
end;

procedure TFAlterarCoresPProjetos.btProximoClick(Sender: TObject);
begin
     ProximoprojetoClick(Sender);
end;

procedure TFAlterarCoresPProjetos.btAnteriorClick(Sender: TObject);
begin
    ProjetoanteriorClick(Sender);
end;

end.
