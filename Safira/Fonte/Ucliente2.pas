unit Ucliente2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjCliente;

type
  TFcliente2 = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    GroupBox3: TGroupBox;
    RadioPessoaFisica: TRadioButton;
    RadioPessoaJuridica: TRadioButton;
    EdtNome: TEdit;
    ComboSexo: TComboBox;
    LbSexo: TLabel;
    LbNome: TLabel;
    LbFantasia: TLabel;
    EdtFantasia: TEdit;
    EdtRG_IE: TEdit;
    LbRG_IE: TLabel;
    EdtCPF_CGC: TMaskEdit;
    LbCPF_CGC: TLabel;
    LbRamoAtividade: TLabel;
    EdtRamoAtividade: TEdit;
    LbNomeRamoAtividade: TLabel;
    Lbemail: TLabel;
    Edtemail: TEdit;
    LbFone: TLabel;
    EdtFone: TEdit;
    LbCelular: TLabel;
    EdtCelular: TEdit;
    LbFax: TLabel;
    EdtFax: TEdit;
    edtContato: TEdit;
    Label4: TLabel;
    Label2: TLabel;
    MemoObservacao: TMemo;
    edtNumero: TEdit;
    Label5: TLabel;
    ComboEndereco: TComboBox;
    EdtComplemento: TEdit;
    LbEnderecoCobranca: TLabel;
    Label6: TLabel;
    LbBairroCobranca: TLabel;
    LbCidadeCobranca: TLabel;
    LbCEPCobranca: TLabel;
    EdtCEP: TEdit;
    ComboEstado: TComboBox;
    LbEstadoCobranca: TLabel;
    ComboCidade: TComboBox;
    ComboBairro: TComboBox;
    LbDataUltimaCompra: TLabel;
    LbValorUltimaCompra: TLabel;
    EdtDataUltimaCompra: TMaskEdit;
    EdtValorUltimaCompra: TEdit;
    LbDataMaiorFatura: TLabel;
    EdtDataMaiorFatura: TMaskEdit;
    EdtValorMaiorFatura: TEdit;
    LbValorMaiorFatura: TLabel;
    LbSituacaoSerasa: TLabel;
    EdtSituacaoSerasa: TEdit;
    EdtLimiteCredito: TEdit;
    EdtCodigoPlanoContas: TEdit;
    EdtBanco: TEdit;
    EdtAgencia: TEdit;
    EdtContaCorrente: TEdit;
    EdtDataNascimento: TMaskEdit;
    ComboPermiteVendaAPrazo: TComboBox;
    ComboAtivo: TComboBox;
    EdtDataCadastro: TMaskEdit;
    Label3: TLabel;
    LbAtivo: TLabel;
    LbPermiteVendaAPrazo: TLabel;
    LbDataNascimento: TLabel;
    LbContaCorrente: TLabel;
    LbAgencia: TLabel;
    LbBanco: TLabel;
    LbCodigoPlanoContas: TLabel;
    LbLimiteCredito: TLabel;
    LbNomePlanodeContas: TLabel;
//DECLARA COMPONENTES
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdtRamoAtividadeExit(Sender: TObject);
    procedure EdtRamoAtividadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtCodigoPlanoContasExit(Sender: TObject);
    procedure EdtCodigoPlanoContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ColocaAtalhoBotoes;
  private
         ObjCliente:TObjCliente;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fcliente2: TFcliente2;


implementation

uses UessencialGlobal, Upesquisa,UessencialLocal;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFcliente2.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjCLIENTE do
    Begin
        Submit_Codigo(edtCodigo.text);
        Submit_Nome(edtNome.text);
        Submit_Sexo(ComboSexo.text);
        Submit_Fantasia(edtFantasia.text);

        if (RadioPessoaFisica.Checked = true)then
        Submit_Fisica_Juridica('F')
        else if (RadioPessoaJuridica.Checked = true)then
        Submit_Fisica_Juridica('J');

        Submit_CPF_CGC(edtCPF_CGC.text);
        Submit_RG_IE(edtRG_IE.text);
        RamoAtividade.Submit_codigo(edtRamoAtividade.text);
        Submit_email(edtemail.text);
        Submit_Fone(edtFone.text);
        Submit_Celular(edtCelular.text);
        Submit_Fax(edtFax.text);
        Submit_Endereco(ComboEndereco.text);
        Submit_Bairro(ComboBairro.text);
        Submit_Cidade(ComboCidade.text);
        Submit_CEP(edtCEP.text);
        Submit_Estado(ComboEstado.text);
        Submit_DataCadastro(edtDataCadastro.text);

        if (EdtDataUltimaCompra.Text='  /  /    ')then
        Submit_DataUltimaCompra('01/01/1500')
        else Submit_DataUltimaCompra(edtDataUltimaCompra.text);

        Submit_ValorUltimaCompra(edtValorUltimaCompra.text);

        if (EdtDataMaiorFatura.Text='  /  /    ')then
        Submit_DataMaiorFatura('01/01/1500')
        else Submit_DataMaiorFatura(edtDataMaiorFatura.text);

        Submit_ValorMaiorFatura(edtValorMaiorFatura.text);
        Submit_SituacaoSerasa(edtSituacaoSerasa.text);
        Submit_LimiteCredito(edtLimiteCredito.text);
        CodigoPlanoContas.Submit_CODIGO(edtCodigoPlanoContas.text);
        Submit_Banco(edtBanco.text);
        Submit_Agencia(edtAgencia.text);
        Submit_ContaCorrente(edtContaCorrente.text);

        if (EdtDataNascimento.Text='  /  /    ')then
        Submit_DataNascimento('01/01/1500')
        else Submit_DataNascimento(edtDataNascimento.text);

        Submit_PermiteVendaAPrazo(ComboPermiteVendaAPrazo.text);
        Submit_Ativo(ComboAtivo.text);
        Submit_Observacao(MemoObservacao.text);
        Submit_Contato(edtContato.Text);
        Submit_Numero(edtNumero.Text);
        Submit_Complemento(EdtComplemento.Text);
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFcliente2.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCLIENTE do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtNome.text:=Get_Nome;
        ComboSexo.text:=Get_Sexo;
        EdtFantasia.text:=Get_Fantasia;
        if (Get_Fisica_Juridica = 'F')then
        RadioPessoaFisica.Checked:=true
        else if (Get_Fisica_Juridica = 'J')then
        RadioPessoaJuridica.Checked:=true;
        EdtCPF_CGC.text:=Get_CPF_CGC;
        EdtRG_IE.text:=Get_RG_IE;
        EdtRamoAtividade.text:=RamoAtividade.Get_codigo;
        Edtemail.text:=Get_email;
        EdtFone.text:=Get_Fone;
        EdtCelular.text:=Get_Celular;
        EdtFax.text:=Get_Fax;
        ComboEndereco.text:=Get_Endereco;
        ComboBairro.text:=Get_Bairro;
        ComboCidade.text:=Get_Cidade;
        EdtCEP.text:=Get_CEP;
        ComboEstado.text:=Get_Estado;
        EdtDataCadastro.text:=Get_DataCadastro;

        if (Get_DataUltimaCompra = '01/01/1500')then
        EdtDataUltimaCompra.text:=''
        else EdtDataUltimaCompra.text:=Get_DataUltimaCompra;

        EdtValorUltimaCompra.text:=Get_ValorUltimaCompra;

        if (Get_DataMaiorFatura='01/01/1500')then
        EdtDataMaiorFatura.text:=''
        else EdtDataMaiorFatura.text:=Get_DataMaiorFatura;

        EdtValorMaiorFatura.text:=Get_ValorMaiorFatura;
        EdtSituacaoSerasa.text:=Get_SituacaoSerasa;
        EdtLimiteCredito.text:=Get_LimiteCredito;
        EdtCodigoPlanoContas.text:=CodigoPlanoContas.Get_CODIGO;
        EdtBanco.text:=Get_Banco;
        EdtAgencia.text:=Get_Agencia;
        EdtContaCorrente.text:=Get_ContaCorrente;

        if (Get_DataNascimento='01/01/1500')then
        EdtDataNascimento.text:=''
        else EdtDataNascimento.text:=Get_DataNascimento;

        ComboPermiteVendaAPrazo.text:=Get_PermiteVendaAPrazo;
        ComboAtivo.text:=Get_Ativo;
        MemoObservacao.text:=Get_Observacao;
        edtContato.Text:=Get_Contato;
        edtNumero.Text:=Get_Numero;
        EdtComplemento.Text:=Get_Complemento;

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFcliente2.TabelaParaControles: Boolean;
begin
     If (Self.ObjCliente.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFcliente2.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjCliente:=TObjCliente.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

procedure TFcliente2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCliente=Nil)
     Then exit;

If (Self.ObjCliente.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjCliente.free;
end;

procedure TFcliente2.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFcliente2.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjCliente.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjCliente.status:=dsInsert;
     Guia.pageindex:=0;
     Edtnome.setfocus;

end;


procedure TFcliente2.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCliente.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjCliente.Status:=dsEdit;
                guia.pageindex:=0;
                edtnome.setfocus;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;


end;

procedure TFcliente2.btgravarClick(Sender: TObject);
begin

     If Self.ObjCliente.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjCliente.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjCliente.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFcliente2.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjCliente.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCliente.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCliente.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFcliente2.btcancelarClick(Sender: TObject);
begin
     Self.ObjCliente.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFcliente2.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFcliente2.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCliente.Get_pesquisa,Self.ObjCliente.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCliente.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCliente.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjCliente.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFcliente2.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFcliente2.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT

procedure TFcliente2.EdtRamoAtividadeExit(Sender: TObject);
begin
     ObjCliente.EdtRamoAtividadeExit(sender,LbNomeRamoAtividade);
end;

procedure TFcliente2.EdtRamoAtividadeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjCliente.EdtRamoAtividadeKeyDown(sender,key,shift,LbNomeRamoAtividade);
end;

procedure TFcliente2.EdtCodigoPlanoContasExit(Sender: TObject);
begin
ObjCLIENTE.EdtCodigoPlanoContasExit(Sender, LbNomePlanodeContas);
end;

procedure TFcliente2.EdtCodigoPlanoContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
ObjCLIENTE.EdtCodigoPlanoContasKeyDown(Sender, Key, Shift, LbNomePlanodeContas);
end;

procedure TFCLIENTE2.ColocaAtalhoBotoes;
begin
   // Coloca_Atalho_Botoes(BtNovo, 'N');
   //Coloca_Atalho_Botoes(BtSalvar, 'S');
   //Coloca_Atalho_Botoes(BtAlterar, 'A');
   //Coloca_Atalho_Botoes(BtCancelar, 'C');
   //Coloca_Atalho_Botoes(BtExcluir, 'E');
   //Coloca_Atalho_Botoes(BtPesquisar, 'P');
   //Coloca_Atalho_Botoes(BtRelatorio, 'R');
   //Coloca_Atalho_Botoes(BtSair, 'I');
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCliente.OBJETO.Get_Pesquisa,Self.ObjCliente.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjCliente.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjCliente.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjCliente.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
