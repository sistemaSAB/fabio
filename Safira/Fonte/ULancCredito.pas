unit ULancCredito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,UobjCREDITO,UDataModulo,DB,UobjLANCAMENTOCREDITO,IBQuery;

type
  TFlancCredito = class(TForm)
    lbLbObservacao: TLabel;
    lbLbValor: TLabel;
    lbLbHistorico: TLabel;
    lbLbCLIENTE: TLabel;
    lbLbCODIGO: TLabel;
    lbNomeCLIENTE: TLabel;
    lb1: TLabel;
    shp1: TShape;
    lbTitulo: TLabel;
    pnlrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    mmoObservacao: TMemo;
    edtValor: TEdit;
    edtHistorico: TEdit;
    edtCLIENTE: TEdit;
    edtCODIGO: TEdit;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    btrelatorios: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btgravar: TBitBtn;
    btalterar: TBitBtn;
    btBtnovo: TBitBtn;
    pnl1: TPanel;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    lbCreditoUtilizados: TLabel;
    lb5: TLabel;
    lbCreditoRestantes: TLabel;
    lbCreditoLancados: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    procedure btsairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btBtnovoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btgravarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
    procedure edtCLIENTEExit(Sender: TObject);
    procedure edtCLIENTEKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCLIENTEKeyPress(Sender: TObject; var Key: Char);
    procedure lbTituloClick(Sender: TObject);
    procedure lbNomeCLIENTEClick(Sender: TObject);
    procedure lbNomeCLIENTEMouseLeave(Sender: TObject);
    procedure lbNomeCLIENTEMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure edtCLIENTEDblClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
             ObjCREDITO:TObjCREDITO;
             ObjLancamentoCredito:TObjLANCAMENTOCREDITO;
             Procedure LimpaLabels;
             procedure AtualizaEstatistica;
             Procedure ZeraLabelsCredito;
             Function  AtualizaQuantidade: string;
             function  ControlesParaObjeto: Boolean;
             function  TabelaParaControles: Boolean;
             function  ObjetoParaControles: Boolean;
  public
    { Public declarations }
  end;

var
  FlancCredito: TFlancCredito;

implementation

uses UescolheImagemBotao, UessencialGlobal, UObjPermissoesUso,
  UObjLancamento, Upesquisa, UTitulo_novo, UCLIENTE, Uprincipal;

{$R *.dfm}

procedure TFlancCredito.btsairClick(Sender: TObject);
begin
     self.Close;
end;

procedure TFlancCredito.FormShow(Sender: TObject);
begin
         if (ObjPermissoesUsoGlobal.ValidaPermissao('LAN�AR CR�DITO')=False)
         then exit;

         limpaedit(Self);
         Self.limpaLabels;
         desabilita_campos(Self);

         Try
            ObjCREDITO:=TObjCREDITO.Create;
         Except
               Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
               esconde_botoes(self);
               exit;
         End;
         ObjLancamentoCredito:=TObjLANCAMENTOCREDITO.Create;

         ZeraLabelsCredito;
         lbquantidade.Caption:=AtualizaQuantidade;
         FescolheImagemBotao.PegaFiguraBotoespequeno(btbtnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
         FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
end;

procedure TFlancCredito.btBtnovoClick(Sender: TObject);
begin
       limpaedit(Self);
       Self.limpaLabels;
       zeralabelscredito;

       habilita_campos(Self);
       esconde_botoes(Self);

       edtcodigo.text:='0';
       edtcodigo.enabled:=False;

       Btgravar.visible:=True;
       BtCancelar.visible:=True;
       btpesquisar.visible:=True;

       Self.ObjCREDITO.status:=dsInsert;
       edtCLIENTE.setfocus;
end;

procedure TFlancCredito.LimpaLabels;
begin
      LbNomeCLIENTE.Caption:='';
      lbTitulo.Caption:='';
end;

procedure TFlancCredito.AtualizaEstatistica;
var
      creditototal,creditoatual:string;
begin
      If(edtcliente.text = '')
      then exit;


      creditototal:=self.ObjCREDITO.EstatisticaCredito(edtcliente.text);
      creditoatual:=CurrToStr(ObjLancamentoCredito.RetornaSaldoCliente(EdtCLIENTE.Text));

      lbCreditoLancados.Caption:=formata_valor(ObjLancamentoCredito.RetornaCreditoLancadosCliente(EdtCLIENTE.Text));
      lbCreditoUtilizados.Caption:=formata_valor(ObjLancamentoCredito.RetornaCreditoUtilizadosCliente(EdtCLIENTE.Text));
      lbCreditoRestantes.Caption:=formata_valor(ObjLancamentoCredito.RetornaSaldoCliente(EdtCLIENTE.Text));
end;

procedure TFlancCredito.ZeraLabelsCredito;
begin
     lbCreditoUtilizados.caption:='0';
     lbCreditoRestantes.caption:='0';
     lbCreditoLancados.caption:='0';
end;

function TFlancCredito.AtualizaQuantidade;
begin
     result:='Existem '+ ContaRegistros('TABCREDITO','codigo') + ' Cr�ditos Cadastrados';
end;

procedure TFlancCredito.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     If (Self.ObjCREDITO=Nil)
     Then exit;

     If (Self.ObjCREDITO.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjCREDITO.free;
     ObjLancamentoCredito.Free;
end;

procedure TFlancCredito.btgravarClick(Sender: TObject);
var  codigotitulotemp,codigotemp:string;
     Inserir:Boolean;
begin

     If Self.ObjCREDITO.Status=dsInactive
     Then exit;

     if (Self.ObjCREDITO.Status  = dsInsert)
     then Inserir := true
     else Inserir :=false;


     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
     End;

     If (Self.ObjCREDITO.salvar(false)=False)
     Then exit;


     try
         if (Inserir =  true)
         then Begin
                  if (ObjLancamentoCredito.AumentaCreditoComCredito(Self.ObjCREDITO.Get_CODIGO, Self.ObjCREDITO.CLIENTE.Get_CODIGO,Self.ObjCREDITO.Get_Valor,DateToStr(Now))=false)
                  then exit;
         end;
         //se for edi��o
         //no modulo feito pro amanda, pelo fabio, o op��o de editar estava
         //desabilitada, ver e entender porque....
         if (Inserir = false)
         then Begin
                  if (ObjLancamentoCredito.AlteraCreditoComCredito(Self.ObjCREDITO.Get_CODIGO, Self.ObjCREDITO.CLIENTE.Get_CODIGO,Self.ObjCREDITO.Get_Valor,DateToStr(Now))=false)
                  then exit;
         end;

     except
          MensagemErro('Erro ao tentar criar o Objeto Lan�amento Credito.');
          FDataModulo.IBTransaction.RollbackRetaining;
          exit;
     end;

     FDataModulo.IBTransaction.CommitRetaining;


     codigotemp:=self.ObjCREDITO.Get_CODIGO;

     mostra_botoes(Self);
     desabilita_campos(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
     self.ObjCREDITO.LocalizaCodigo(codigotemp);
     self.TabelaParaControles;
     AtualizaEstatistica;
end;


procedure TFlancCredito.btcancelarClick(Sender: TObject);
begin
        Self.ObjCREDITO.cancelar;
         limpaedit(Self);
         Self.limpaLabels;
         desabilita_campos(Self);
         mostra_botoes(Self);
end;

procedure TFlancCredito.btexcluirClick(Sender: TObject);
var
erro:String;
objlancamento:Tobjlancamento;
Query:TIBQuery;
CodigoLancamento:string;
CodigoPendencia:string;
CodigoTituloligadoLancamento:string;
CodigoTituloTabCredito:string;
begin
     If (Self.ObjCREDITO.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCREDITO.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;
     ObjCREDITO.TabelaparaObjeto;
     CodigoLancamento:=ObjCREDITO.LANCAMENTO.Get_CODIGO;
     CodigoPendencia:=ObjCREDITO.LANCAMENTO.Pendencia.Get_CODIGO;
     CodigoTituloligadoLancamento:=ObjCREDITO.LANCAMENTO.Pendencia.Titulo.Get_CODIGO;
     CodigoTituloTabCredito:=ObjCREDITO.TITULO.Get_CODIGO;

     if(CodigoTituloTabCredito<>CodigoTituloligadoLancamento)then
     begin
           MensagemErro('Este lan�amento de cr�dito foi gerado pelo modo de "CR�DITO GERADO PELO TROCO"'+#13+'e n�o pode ser excluido sem estornar o recebimento do titulo'+#13+'que gerou esse cr�dito');
           Exit;
     end;


     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     Try
          //Desvinculo o t�tulo da tabcredito
          Self.ObjCREDITO.Status:=dsEdit;
          self.ObjCREDITO.TITULO.Submit_CODIGO('');

          //Desvinculo o lancamento efetuado no titulo
          self.ObjCREDITO.LANCAMENTO.Submit_CODIGO('');

          If(Self.ObjCREDITO.Salvar(false)=false)
          Then Begin
               MensagemErro('Erro na tentativa de desvincular o T�tulo gerado pelo Cr�dito');
               Exit;
          End;


          If(lbTitulo.Caption<>'') //Se nao for vazio
          Then Begin
               //verifica se o cliente possui credito suficiente
               If(ObjLancamentoCredito.RetornaSaldoCliente(Self.ObjCREDITO.CLIENTE.Get_CODIGO))<strtofloat(come(EdtValor.Text,'.'))
               Then Begin
                     MensagemErro('Cr�dito do Cliente Insuficiente');
                     Exit;
               End;

               Try
                   ObjLancamento:= TObjLancamento.Create;
               Except
                   Messagedlg('Erro na tentativa de Gerar o Objeto de Lan�amento!',mterror,[mbok],0);
                   exit;
               End;

               Try
                     If(objlancamento.ExcluiTitulo(lbTitulo.Caption,false)=false)
                     Then Begin
                                MensagemErro('Erro na Exclus�o do T�tulo');
                                Exit;
                     End;

                FDataModulo.IBTransaction.CommitRetaining;
               Finally
                     objlancamento.free;
               End;

          End;

          try
              if (ObjLancamentoCredito.DiminuiCreditoDeCredito(Self.ObjCREDITO.Get_CODIGO)=false)
              then exit;
          except
               MensagemErro('Erro ao tentar criar o Objeto Lan�amento Credito.');
               exit;
          end;

          Self.ObjCREDITO.Exclui(edtcodigo.text,true);
          ZeralabelsCredito;

     Finally
          FDataModulo.IBTransaction.RollbackRetaining;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFlancCredito.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCREDITO.Get_pesquisa,Self.ObjCREDITO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCREDITO.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCREDITO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjCREDITO.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
        If(edtcodigo.text<>'')
        Then AtualizaEstatistica;

end;


procedure TFlancCredito.btrelatoriosClick(Sender: TObject);
begin
   ObjCREDITO.Imprime(EdtCODIGO.Text);
end;

procedure TFlancCredito.btopcoesClick(Sender: TObject);
begin
      Self.ObjCREDITO.Opcoes(EdtCODIGO.text);
end;

function TFlancCredito.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjCREDITO do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        CLIENTE.Submit_codigo(edtCLIENTE.text);
        TITULO.Submit_codigo(lbTitulo.Caption);
        Submit_Valor(tira_ponto(edtValor.text));
        Submit_Historico(edtHistorico.text);
        Submit_Observacao(MmoObservacao.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFlancCredito.TabelaParaControles: Boolean;
begin
     If (Self.ObjCREDITO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;

function TFlancCredito.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCREDITO do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtCLIENTE.text:=CLIENTE.Get_codigo;
        lbnomecliente.caption:=cliente.get_nome;
        lbTitulo.Caption:=TITULO.Get_codigo;
        EdtValor.text:=Get_Valor;
        EdtHistorico.text:=Get_Historico;
        MmoObservacao.text:=Get_Observacao;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

procedure TFlancCredito.edtCLIENTEExit(Sender: TObject);
begin
          ObjCREDITO.edtCLIENTEExit(sender,lbnomeCLIENTE);
end;

procedure TFlancCredito.edtCLIENTEKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
       ObjCREDITO.edtCLIENTEkeydown(sender,key,shift,lbnomeCLIENTE);
end;

procedure TFlancCredito.edtCLIENTEKeyPress(Sender: TObject; var Key: Char);
begin
       ValidaNumeros(Tedit(Sender),Key,'INT');
end;

procedure TFlancCredito.lbTituloClick(Sender: TObject);
var
Tmptitulo:TFTitulo_novo;
begin
      If ( EdtCodigo.text='' )
      Then Exit;

      if ( Lbtitulo.Caption='' )
      Then begin
                MensagemErro('N�o existe T�tulo para essa Compra!');
                Exit;
      End;

      Try
            Tmptitulo:=TFTitulo_novo.create(nil);
      Except
            Messagedlg('Erro na tentativa de Criar o Formul�rio de T�tulo',mterror,[mbok],0);
            exit;
      End;
      Try
            TmpTitulo.quitaPrimeiraParcela:=False;
            TmpTitulo.LocalizaCodigoInicial( Lbtitulo.Caption );
            TmpTitulo.showmodal;
      Finally
            Freeandnil(TmpTitulo);
      End;
End;

procedure TFlancCredito.lbNomeCLIENTEClick(Sender: TObject);
var
  Fcliente:TFCLIENTE;
begin
   if(EdtCliente.Text='')
   then Exit;

   try
     fcliente:=TFCLIENTE.Create(nil);

   except

   end;

   try
      Fcliente.Tag:=StrToInt(EdtCliente.Text);
      Fcliente.ShowModal;

   finally
      FreeAndNil(Fcliente);
   end;


end;

procedure TFlancCredito.lbNomeCLIENTEMouseLeave(Sender: TObject);
begin
        TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFlancCredito.lbNomeCLIENTEMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
       TEdit(sender).Font.Style:=[fsBold,fsUnderline];
       
end;

procedure TFlancCredito.edtCLIENTEDblClick(Sender: TObject);
VAR
  key:Word;
  shift:TShiftState;
begin
        key:=VK_F9;
        ObjCREDITO.edtCLIENTEkeydown(sender,key,shift,lbnomeCLIENTE);
end;

procedure TFlancCredito.FormKeyPress(Sender: TObject; var Key: Char);
begin
         if key=#13
         Then Perform(Wm_NextDlgCtl,0,0);
         if (Key=#27)
         Then Self.Close;

end;

procedure TFlancCredito.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      if (Key = VK_F1) then
      Fprincipal.chamaPesquisaMenu();
end;

end.
