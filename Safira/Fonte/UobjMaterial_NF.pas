unit UobjMaterial_NF;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal
,UOBJNOTAFISCAL
,UOBJTABELAA_ST,UOBJTABELAB_ST,uobjcfop,uobjIMPOSTO_ICMS,
UOBJIMPOSTO_IPI
,UOBJIMPOSTO_PIS
,UOBJIMPOSTO_COFINS
,UobjCSOSN;


Type
   TObjmaterial_NF=class

          Public
                //ObjDatasource                               :TDataSource;
                Objquery:Tibquery;
                MaterialInsertSql:TStringList;
                MaterialInsertSqlValues:TStringList;
                MaterialModifySQl:TStringList;


                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                NOTAFISCAL:TOBJNOTAFISCAL;
                SituacaoTributaria_TabelaA:TOBJTABELAA_ST ;
                SituacaoTributaria_TabelaB:TOBJTABELAB_ST ;
                IMPOSTO_ICMS_ORIGEM:TobjIMPOSTO_ICMS;
                IMPOSTO_ICMS_DESTINO:TobjIMPOSTO_ICMS;
                CFOP:TOBJCFOP;

                IMPOSTO_IPI :tobjimposto_IPI;
                IMPOSTO_PIS_ORIGEM :Tobjimposto_PIS;
                IMPOSTO_PIS_DESTINO :Tobjimposto_PIS;
                IMPOSTO_COFINS_ORIGEM:Tobjimposto_COFINS;
                IMPOSTO_COFINS_DESTINO:Tobjimposto_COFINS;
                CSOSN:TObjCSOSN;

//CODIFICA VARIAVEIS PUBLICAS



                Function RetornaCamposSelectSQL:String;
                Constructor Create(Owner:TComponent);
                Destructor  Free;

                Function   TabelaparaObjeto:Boolean;
                Procedure  ZerarTabela;

                Function  VerificaBrancos:String;
                Function  VerificaRelacionamentos:String;
                Function  VerificaNumericos:String;
                Function  VerificaData:String;
                Function  VerificaFaixa:String;
                Procedure ObjetoparaTabela;


                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_QUANTIDADE(parametro: string);
                Function Get_QUANTIDADE: string;
                Procedure Submit_VALOR(parametro: string);
                Function Get_VALOR: string;
                Function Get_VALORFINAL: string;
                Procedure Submit_ALIQUOTA(parametro: string);
                Function Get_ALIQUOTA: string;
                Procedure Submit_REDUCAOBASECALCULO(parametro: string);
                Function Get_REDUCAOBASECALCULO: string;
                Procedure Submit_ALIQUOTACUPOM(parametro: string);
                Function Get_ALIQUOTACUPOM: string;
                Procedure Submit_ISENTO(parametro: string);
                Function Get_ISENTO: string;
                Procedure Submit_SUBSTITUICAOTRIBUTARIA(parametro: string);
                Function Get_SUBSTITUICAOTRIBUTARIA: string;
                Procedure Submit_IPI(parametro: string);
                Function Get_IPI: string;
                Procedure Submit_valorseguro(parametro: string);
                Function Get_valorseguro: string;
                Procedure Submit_valorfrete(parametro: string);
                Function Get_valorfrete: string;
                Procedure Submit_valorpauta(parametro: string);
                Function Get_valorpauta: string;
                Procedure Submit_percentualagregado(parametro:string);
                Function Get_percentualagregado:string;
                Procedure Submit_Margemvaloragregadoconsumidor(parametro:string);
                Function Get_Margemvaloragregadoconsumidor:string;
                Function Get_BC_ICMS:String;
                Function Get_VALOR_ICMS:String;
                Function Get_BC_ICMS_ST:String;
                Function Get_VALOR_ICMS_ST:String;
                Procedure Submit_BC_ICMS(parametro:string);
                Procedure Submit_VALOR_ICMS(parametro:string);
                Procedure Submit_BC_ICMS_ST(parametro:String);
                Procedure Submit_VALOR_ICMS_ST(parametro:string);

               Function get_BC_IPI:STRING;
               Function get_VALOR_IPI:string;

               Function get_BC_PIS:STRING;
               Function get_VALOR_PIS:string;
               Function get_BC_PIS_ST:STRING;
               Function get_VALOR_PIS_ST:string;

               Function get_BC_COFINS:STRING;
               Function get_VALOR_COFINS:string;
               Function get_BC_COFINS_ST:STRING;
               Function get_VALOR_COFINS_ST:string;

               Procedure Submit_BC_IPI(parametro:string);
               Procedure Submit_VALOR_IPI(parametro:string);

               Procedure Submit_BC_PIS(parametro:string);
               Procedure Submit_VALOR_PIS(parametro:string);
               Procedure Submit_BC_PIS_ST(parametro:string);
               Procedure Submit_VALOR_PIS_ST(parametro:string);

               Procedure Submit_BC_COFINS(parametro:string);
               Procedure Submit_VALOR_COFINS(parametro:string);
               Procedure Submit_BC_COFINS_ST(parametro:string);
               Procedure Submit_VALOR_COFINS_ST(parametro:string);

               Procedure Submit_referencia(parametro:String);
               Function  Get_referencia:string;

               procedure Submit_desconto(parametro:string);
               procedure submit_UNIDADE(parametro:string);
               function Get_Desconto:string;
               function Get_Unidade:String;


                procedure EdtNOTAFISCALExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtNOTAFISCALKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                procedure EdtIMPOSTO_COFINS_DESTINOExit(Sender: TObject;LABELNOME: TLABEL);
                procedure EdtIMPOSTO_COFINS_DESTINOKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
                procedure EdtIMPOSTO_COFINS_ORIGEMExit(Sender: TObject;LABELNOME: TLABEL);
                procedure EdtIMPOSTO_COFINS_ORIGEMKeyDown(Sender: TObject;  var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
                procedure EdtIMPOSTO_IPIExit(Sender: TObject; LABELNOME: TLABEL);
                procedure EdtIMPOSTO_IPIKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState; LABELNOME: Tlabel);
                procedure EdtIMPOSTO_PIS_DESTINOExit(Sender: TObject;LABELNOME: TLABEL);
                procedure EdtIMPOSTO_PIS_DESTINOKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState; LABELNOME: Tlabel);
                procedure EdtIMPOSTO_PIS_ORIGEMExit(Sender: TObject;LABELNOME: TLABEL);
                procedure EdtIMPOSTO_PIS_ORIGEMKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState; LABELNOME: Tlabel);





         Private
               Owner:TComponent;
               CODIGO:string;
               QUANTIDADE:string;
               VALOR:string;
               VALORFINAL:string;

               ALIQUOTA:string;
               REDUCAOBASECALCULO:string;
               ALIQUOTACUPOM:string;
               ISENTO:string;
               SUBSTITUICAOTRIBUTARIA:string;
               valorpauta:string;
               IPI:string;
               valorseguro:string;
               valorfrete:string;

               percentualagregado:string;
               Margemvaloragregadoconsumidor:string;

               BC_ICMS:String;
               VALOR_ICMS:String;
               BC_ICMS_ST:String;
               VALOR_ICMS_ST:String;

               BC_IPI:STRING;
               VALOR_IPI:string;

               BC_PIS:STRING;
               VALOR_PIS:string;
               BC_PIS_ST:STRING;
               VALOR_PIS_ST:string;

               BC_COFINS:STRING;
               VALOR_COFINS:string;
               BC_COFINS_ST:STRING;
               VALOR_COFINS_ST:string;

               unidade:string;
               desconto:string;

               referencia:string;




   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls, UIMPOSTO_IPI,
  UIMPOSTO_PIS, UIMPOSTO_COFINS;


function TObjmaterial_NF.RetornaCamposSelectSQL: String;
begin
     result:='CODIGO,NOTAFISCAL,QUANTIDADE,VALOR,valorpauta,VALORFINAL,ALIQUOTA,REDUCAOBASECALCULO,ALIQUOTACUPOM,ISENTO'+
             ' ,SUBSTITUICAOTRIBUTARIA,IPI,valorseguro,valorfrete,SituacaoTributaria_TabelaA,SituacaoTributaria_TabelaB,'+
             'CFOP,IMPOSTO_ICMS_ORIGEM,IMPOSTO_ICMS_DESTINO,percentualagregado,Margemvaloragregadoconsumidor,BC_ICMS,'+
             'VALOR_ICMS,BC_ICMS_ST,VALOR_ICMS_ST'+
             ',BC_IPI,VALOR_IPI,BC_PIS,VALOR_PIS,BC_PIS_ST,VALOR_PIS_ST,BC_COFINS,VALOR_COFINS,BC_COFINS_ST,VALOR_COFINS_ST'+
             ',IMPOSTO_IPI,IMPOSTO_PIS_ORIGEM,IMPOSTO_PIS_DESTINO,IMPOSTO_COFINS_ORIGEM,IMPOSTO_COFINS_DESTINO,referencia,unidade,csosn,desconto';
end;



Function  TObjmaterial_NF.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('NOTAFISCAL').asstring<>'')
        Then Begin
                 If (Self.NOTAFISCAL.LocalizaCodigo(FieldByName('NOTAFISCAL').asstring)=False)
                 Then Begin
                          Messagedlg('Nota Fiscal N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.NOTAFISCAL.TabelaparaObjeto;
        End;

        Self.QUANTIDADE:=fieldbyname('QUANTIDADE').asstring;
        Self.VALOR:=fieldbyname('VALOR').asstring;
        Self.VALORFINAL:=fieldbyname('VALORFINAL').asstring;

        Self.ALIQUOTA:=fieldbyname('ALIQUOTA').asstring;
        Self.REDUCAOBASECALCULO:=fieldbyname('REDUCAOBASECALCULO').asstring;
        Self.ALIQUOTACUPOM:=fieldbyname('ALIQUOTACUPOM').asstring;
        Self.ISENTO:=fieldbyname('ISENTO').asstring;
        Self.SUBSTITUICAOTRIBUTARIA:=fieldbyname('SUBSTITUICAOTRIBUTARIA').asstring;
        Self.IPI:=fieldbyname('IPI').asstring;
        Self.valorseguro:=fieldbyname('valorseguro').asstring;
        Self.valorfrete:=fieldbyname('valorfrete').asstring;
        Self.valorpauta:=fieldbyname('valorpauta').asstring;
        Self.percentualagregado:=fieldbyname('percentualagregado').asstring;


        Self.BC_ICMS:=fieldbyname('BC_ICMS').asstring;
        Self.VALOR_ICMS:=fieldbyname('VALOR_ICMS').asstring;
        Self.BC_ICMS_ST:=fieldbyname('BC_ICMS_ST').asstring;
        Self.VALOR_ICMS_ST:=fieldbyname('VALOR_ICMS_ST').asstring;

        Self.BC_IPI:=fieldbyname('BC_IPI').asstring;
        Self.VALOR_IPI:=fieldbyname('VALOR_IPI').asstring;
        Self.BC_PIS:=fieldbyname('BC_PIS').asstring;
        Self.VALOR_PIS:=fieldbyname('VALOR_PIS').asstring;
        Self.BC_PIS_ST:=fieldbyname('BC_PIS_ST').asstring;
        Self.VALOR_PIS_ST:=fieldbyname('VALOR_PIS_ST').asstring;
        Self.BC_COFINS:=fieldbyname('BC_COFINS').asstring;
        Self.VALOR_COFINS:=fieldbyname('VALOR_COFINS').asstring;
        Self.BC_COFINS_ST:=fieldbyname('BC_COFINS_ST').asstring;
        Self.VALOR_COFINS_ST:=fieldbyname('VALOR_COFINS_ST').asstring;
        Self.referencia:=fieldbyname('referencia').asstring;
        self.desconto:=fieldbyname('desconto').AsString;
        self.unidade:=fieldbyname('unidade').AsString;
//CODIFICA TABELAPARAOBJETO

        If(FieldByName('SituacaoTributaria_TabelaA').asstring<>'')
        Then Begin
                 If (Self.SituacaoTributaria_TabelaA.LocalizaCodigo(FieldByName('SituacaoTributaria_TabelaA').asstring)=False)
                 Then Begin
                          Messagedlg('SituacaoTributaria_TabelaA N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.SituacaoTributaria_TabelaA.TabelaparaObjeto;
        End;

        If(FieldByName('SituacaoTributaria_TabelaB').asstring<>'')
        Then Begin
                 If (Self.SituacaoTributaria_TabelaB.LocalizaCodigo(FieldByName('SituacaoTributaria_TabelaB').asstring)=False)
                 Then Begin
                          Messagedlg('SituacaoTributaria_TabelaB N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.SituacaoTributaria_TabelaB.TabelaparaObjeto;
        End;

        If(FieldByName('CFOP').asstring<>'')
        Then Begin
                 If (Self.CFOP.LocalizaCodigo(FieldByName('CFOP').asstring)=False)
                 Then Begin
                          Messagedlg('CFOP N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CFOP.TabelaparaObjeto;
        End;

        If(FieldByName('IMPOSTO_ICMS_ORIGEM').asstring<>'')
        Then Begin
                 If (Self.IMPOSTO_ICMS_ORIGEM.LocalizaCodigo(FieldByName('IMPOSTO_ICMS_ORIGEM').asstring)=False)
                 Then Begin
                          Messagedlg('IMPOSTO_ICMS_ORIGEM N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.IMPOSTO_ICMS_ORIGEM.TabelaparaObjeto;
        End;

        If(FieldByName('IMPOSTO_ICMS_DESTINO').asstring<>'')
        Then Begin
                 If (Self.IMPOSTO_ICMS_DESTINO.LocalizaCodigo(FieldByName('IMPOSTO_ICMS_DESTINO').asstring)=False)
                 Then Begin
                          Messagedlg('IMPOSTO ICMS ST N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.IMPOSTO_ICMS_DESTINO.TabelaparaObjeto;
        End;


        If(FieldByName('IMPOSTO_IPI').asstring<>'')
        Then Begin
                 If (Self.IMPOSTO_IPI.LocalizaCodigo(FieldByName('IMPOSTO_IPI').asstring)=False)
                 Then Begin
                          Messagedlg('Imposto IPI N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.IMPOSTO_IPI.TabelaparaObjeto;
        End;
        If(FieldByName('IMPOSTO_PIS_ORIGEM').asstring<>'')
        Then Begin
                 If (Self.IMPOSTO_PIS_ORIGEM.LocalizaCodigo(FieldByName('IMPOSTO_PIS_ORIGEM').asstring)=False)
                 Then Begin
                          Messagedlg('Imposto PIs Origem N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.IMPOSTO_PIS_ORIGEM.TabelaparaObjeto;
        End;
        If(FieldByName('IMPOSTO_PIS_DESTINO').asstring<>'')
        Then Begin
                 If (Self.IMPOSTO_PIS_DESTINO.LocalizaCodigo(FieldByName('IMPOSTO_PIS_DESTINO').asstring)=False)
                 Then Begin
                          Messagedlg('Imposto Pis Destino N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.IMPOSTO_PIS_DESTINO.TabelaparaObjeto;
        End;
        If(FieldByName('IMPOSTO_COFINS_ORIGEM').asstring<>'')
        Then Begin
                 If (Self.IMPOSTO_COFINS_ORIGEM.LocalizaCodigo(FieldByName('IMPOSTO_COFINS_ORIGEM').asstring)=False)
                 Then Begin
                          Messagedlg('Imposto Cofins Origem N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.IMPOSTO_COFINS_ORIGEM.TabelaparaObjeto;
        End;
        If(FieldByName('IMPOSTO_COFINS_DESTINO').asstring<>'')
        Then Begin
                 If (Self.IMPOSTO_COFINS_DESTINO.LocalizaCodigo(FieldByName('IMPOSTO_COFINS_DESTINO').asstring)=False)
                 Then Begin
                          Messagedlg('Imposto Cofins Destino N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.IMPOSTO_COFINS_DESTINO.TabelaparaObjeto;
        End;
        if(FieldByName('csosn').AsString<>'') then
        begin
              if(self.CSOSN.LocalizaCodigo(fieldbyname('csosn').AsString)=False) then
              begin
                      MensagemErro('CSOSN n�o encontrado');
                      self.ZerarTabela;
                      result:=False;
                      Exit;
              end
              else Self.CSOSN.TabelaparaObjeto;
        end;





        result:=True;
     End;
end;


Procedure TObjmaterial_NF.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('NOTAFISCAL').asstring:=Self.NOTAFISCAL.GET_CODIGO;
        ParamByName('QUANTIDADE').asstring:=virgulaparaponto(Self.QUANTIDADE);
        ParamByName('VALOR').asstring:=virgulaparaponto(Self.VALOR);

        ParamByName('ALIQUOTA').asstring:=virgulaparaponto(Self.ALIQUOTA);
        ParamByName('REDUCAOBASECALCULO').asstring:=virgulaparaponto(Self.REDUCAOBASECALCULO);
        ParamByName('ALIQUOTACUPOM').asstring:=virgulaparaponto(Self.ALIQUOTACUPOM);
        ParamByName('ISENTO').asstring:=Self.ISENTO;
        ParamByName('SUBSTITUICAOTRIBUTARIA').asstring:=Self.SUBSTITUICAOTRIBUTARIA;
        ParamByName('IPI').asstring:=virgulaparaponto(Self.IPI);
        ParamByName('valorseguro').asstring:=virgulaparaponto(Self.valorseguro);
        ParamByName('valorfrete').asstring:=virgulaparaponto(Self.valorfrete);
        ParamByName('valorpauta').asstring:=virgulaparaponto(Self.valorpauta);
        ParamByName('percentualagregado').asstring:=virgulaparaponto(Self.percentualagregado);
        ParamByName('Margemvaloragregadoconsumidor').asstring:=virgulaparaponto(Self.Margemvaloragregadoconsumidor);

        ParamByName('BC_ICMS').asstring:=virgulaparaponto(Self.BC_ICMS);
        ParamByName('VALOR_ICMS').asstring:=virgulaparaponto(Self.VALOR_ICMS);
        ParamByName('BC_ICMS_ST').asstring:=virgulaparaponto(Self.BC_ICMS_ST);
        ParamByName('VALOR_ICMS_ST').asstring:=virgulaparaponto(Self.VALOR_ICMS_ST);

        Parambyname('BC_IPI').asstring:=virgulaparaponto(Self.BC_IPI         );
        Parambyname('VALOR_IPI').asstring:=virgulaparaponto(Self.VALOR_IPI      );
        Parambyname('BC_PIS').asstring:=virgulaparaponto(Self.BC_PIS         );
        Parambyname('VALOR_PIS').asstring:=virgulaparaponto(Self.VALOR_PIS      );
        Parambyname('BC_PIS_ST').asstring:=virgulaparaponto(Self.BC_PIS_ST      );
        Parambyname('VALOR_PIS_ST').asstring:=virgulaparaponto(Self.VALOR_PIS_ST   );
        Parambyname('BC_COFINS').asstring:=virgulaparaponto(Self.BC_COFINS      );
        Parambyname('VALOR_COFINS').asstring:=virgulaparaponto(Self.VALOR_COFINS   );
        Parambyname('BC_COFINS_ST').asstring:=virgulaparaponto(Self.BC_COFINS_ST   );
        Parambyname('VALOR_COFINS_ST').asstring:=virgulaparaponto(Self.VALOR_COFINS_ST);


        ParamByName('SituacaoTributaria_TabelaA').asstring:=Self.SituacaoTributaria_TabelaA.GET_CODIGO;
        ParamByName('SituacaoTributaria_TabelaB').asstring:=Self.SituacaoTributaria_TabelaB.GET_CODIGO;
        ParamByName('CFOP').asstring:=Self.CFOP.GET_CODIGO;
        ParamByName('IMPOSTO_ICMS_ORIGEM').asstring:=Self.IMPOSTO_ICMS_ORIGEM.GET_CODIGO;
        ParamByName('IMPOSTO_ICMS_DESTINO').asstring:=Self.IMPOSTO_ICMS_DESTINO.GET_CODIGO;

         ParamByName('IMPOSTO_IPI').asstring:=Self.IMPOSTO_IPI.GET_CODIGO;
        ParamByName('IMPOSTO_PIS_ORIGEM').asstring:=Self.IMPOSTO_PIS_ORIGEM.GET_CODIGO;
        ParamByName('IMPOSTO_PIS_DESTINO').asstring:=Self.IMPOSTO_PIS_DESTINO.GET_CODIGO;
        ParamByName('IMPOSTO_COFINS_ORIGEM').asstring:=Self.IMPOSTO_COFINS_ORIGEM.GET_CODIGO;
        ParamByName('IMPOSTO_COFINS_DESTINO').asstring:=Self.IMPOSTO_COFINS_DESTINO.GET_CODIGO;
        ParamByName('referencia').asstring:=Self.referencia;
        ParamByName('unidade').AsString:=Self.unidade;
        ParamByName('desconto').AsString:=virgulaparaponto(Self.desconto);
  End;
End;

//***********************************************************************



procedure TObjmaterial_NF.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        NOTAFISCAL.ZerarTabela;
        QUANTIDADE:='';
        VALOR:='';
        VALORFINAL:='';

        ALIQUOTA:='';
        REDUCAOBASECALCULO:='';
        ALIQUOTACUPOM:='';
        ISENTO:='';
        SUBSTITUICAOTRIBUTARIA:='';
        IPI:='';
        valorseguro:='';
        valorfrete:='';
        valorpauta:='';
        percentualagregado:='';
        Margemvaloragregadoconsumidor:='';
        SituacaoTributaria_TabelaA.ZerarTabela;
        SituacaoTributaria_TabelaB.ZerarTabela;
        CFOP.ZerarTabela;
        IMPOSTO_ICMS_ORIGEM.ZerarTabela;
        IMPOSTO_ICMS_DESTINO.ZerarTabela;
        BC_ICMS:='';
        VALOR_ICMS:='';
        BC_ICMS_ST:='';
        VALOR_ICMS_ST:='';

        BC_IPI:='';
        VALOR_IPI:='';

        BC_PIS:='';
        VALOR_PIS:='';
        BC_PIS_ST:='';
        VALOR_PIS_ST:='';

        BC_COFINS:='';
        VALOR_COFINS:='';
        BC_COFINS_ST:='';
        VALOR_COFINS_ST:='';

         IMPOSTO_IPI.ZerarTabela;
        IMPOSTO_PIS_ORIGEM.ZerarTabela;
        IMPOSTO_PIS_DESTINO.ZerarTabela;
        IMPOSTO_COFINS_ORIGEM.ZerarTabela;
        IMPOSTO_COFINS_DESTINO.ZerarTabela;
        referencia:='';
        desconto:='';
        unidade:='';
        CSOSN.ZerarTabela;

     End;
end;

Function TObjmaterial_NF.VerificaBrancos:String;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      If (NOTAFISCAL.get_codigo='')
      Then Mensagem:=mensagem+'/Nota Fiscal';

      If (QUANTIDADE='')
      Then Mensagem:=mensagem+'/Quantidade';

      If (VALOR='')
      Then Mensagem:=mensagem+'/Valor';

      if (ALIQUOTA='')
      then Aliquota:='0';

      if (REDUCAOBASECALCULO='')
      Then ReducaoBaseCalculo:='0';

      if (ALIQUOTACUPOM='')
      Then AliquotaCupom:='0';

      if (ISENTO='')
      Then Self.isento:='N';

      if (self.SUBSTITUICAOTRIBUTARIA='')
      Then self.SUBSTITUICAOTRIBUTARIA:='N';

      if (self.IPI='')
      Then Self.ipi:='0';

      if (self.valorseguro='')
      Then Self.valorseguro:='0';

      if (self.valorfrete='')
      Then Self.valorfrete:='0';

      if (self.valorpauta='')
      Then Self.valorpauta:='0';

      if (self.percentualagregado='')
      Then Self.percentualagregado:='0';

      if (self.Margemvaloragregadoconsumidor='')
      Then Self.Margemvaloragregadoconsumidor:='0';

      if (Self.BC_ICMS='')
      Then Self.bc_icms:='0';
      
      if (Self.VALOR_ICMS='')
      then Self.VALOR_ICMS:='0';

      if (Self.BC_ICMS_ST='')
      then Self.BC_ICMS_ST:='0';

      if (Self.VALOR_ICMS_ST='')
      then Self.VALOR_ICMS_ST:='0';


      if (BC_IPI='')
      then BC_IPI:='0';

      if (VALOR_IPI='')
      then Valor_ipi:='0';

      if (BC_PIS='')
      Then bc_pis:='0';

      if (VALOR_PIS='')
      Then Valor_PIS:='0';
      

      if (BC_PIS_ST='')
      Then BC_PIS_ST:='0';

      if (VALOR_PIS_ST='')
      Then Valor_PIS_ST:='0';

      if (BC_COFINS='')
      Then BC_COFINS:='0';

      if (VALOR_COFINS='')
      Then Valor_COFINS:='0';

      if (BC_COFINS_ST='')
      Then BC_COFINS_ST:='0';

      if (VALOR_COFINS_ST='')
      Then Valor_Cofins_St:='0';
  End;

  result:=mensagem;
end;


function TObjmaterial_NF.VerificaRelacionamentos: String;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';
      If (Self.NOTAFISCAL.LocalizaCodigo(Self.NOTAFISCAL.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Nota Fiscal n�o Encontrado!';

      If (Self.SituacaoTributaria_TabelaA.Get_CODIGO<>'')then
      If (Self.SituacaoTributaria_TabelaA.LocalizaCodigo(Self.SituacaoTributaria_TabelaA.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ SituacaoTributaria_TabelaA n�o Encontrado!';

      if (Self.SituacaoTributaria_TabelaB.Get_CODIGO<>'')
      then
          If (Self.SituacaoTributaria_TabelaB.LocalizaCodigo(Self.SituacaoTributaria_TabelaB.Get_CODIGO)=False)
          Then Mensagem:=mensagem+'/ SituacaoTributaria_TabelaB n�o Encontrado!';

      if (Self.CFOP.Get_CODIGO<>'')
      then
          If (Self.CFOP.LocalizaCodigo(Self.CFOP.Get_CODIGO)=False)
          Then Mensagem:=mensagem+'/ CFOP n�o Encontrado!';

      if (Self.IMPOSTO_ICMS_ORIGEM.Get_CODIGO<>'')
      then
          If (Self.IMPOSTO_ICMS_ORIGEM.LocalizaCodigo(Self.IMPOSTO_ICMS_ORIGEM.Get_CODIGO)=False)
          Then Mensagem:=mensagem+'/ IMPOSTO_ICMS_ORIGEM n�o Encontrado!';

      if (Self.IMPOSTO_ICMS_DESTINO.Get_CODIGO<>'')
      then
          If (Self.IMPOSTO_ICMS_DESTINO.LocalizaCodigo(Self.IMPOSTO_ICMS_DESTINO.Get_CODIGO)=False)
          Then Mensagem:=mensagem+'/ IMPOSTO_ICMS_ORIGEM ST n�o Encontrado!';

      if (Self.IMPOSTO_IPI.get_codigo<>'')
      Then If (Self.IMPOSTO_IPI.LocalizaCodigo(Self.IMPOSTO_IPI.Get_CODIGO)=False)
           Then Mensagem:=mensagem+'/ Imposto IPI n�o Encontrado!';

      if (Self.IMPOSTO_PIS_ORIGEM.get_codigo<>'')
      Then If (Self.IMPOSTO_PIS_ORIGEM.LocalizaCodigo(Self.IMPOSTO_PIS_ORIGEM.Get_CODIGO)=False)
           Then Mensagem:=mensagem+'/ Imposto PIs Origem n�o Encontrado!';

      if (Self.IMPOSTO_PIS_DESTINO.get_codigo<>'')
      Then If (Self.IMPOSTO_PIS_DESTINO.LocalizaCodigo(Self.IMPOSTO_PIS_DESTINO.Get_CODIGO)=False)
           Then Mensagem:=mensagem+'/ Imposto Pis Destino n�o Encontrado!';

      if (Self.IMPOSTO_COFINS_ORIGEM.get_codigo<>'')
      Then If (Self.IMPOSTO_COFINS_ORIGEM.LocalizaCodigo(Self.IMPOSTO_COFINS_ORIGEM.Get_CODIGO)=False)
           Then Mensagem:=mensagem+'/ Imposto Cofins Origem n�o Encontrado!';

      if (Self.IMPOSTO_COFINS_DESTINO.get_codigo<>'')
      Then If (Self.IMPOSTO_COFINS_DESTINO.LocalizaCodigo(Self.IMPOSTO_COFINS_DESTINO.Get_CODIGO)=False)
           Then Mensagem:=mensagem+'/ Imposto Cofins Destino n�o Encontrado!';



   result:=Mensagem;
End;

function TObjmaterial_NF.VerificaNumericos: String;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.NOTAFISCAL.Get_Codigo<>'')
        Then Strtoint(Self.NOTAFISCAL.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Nota Fiscal';
     End;
     try
        Strtofloat(Self.QUANTIDADE);
     Except
           Mensagem:=mensagem+'/Quantidade';
     End;
     try
        Strtofloat(Self.VALOR);
     Except
           Mensagem:=mensagem+'/Valor';
     End;

     try
        Strtofloat(Self.ALIQUOTA);
     Except
           Mensagem:=mensagem+'/Al�quota';
     End;
     try
        Strtofloat(Self.REDUCAOBASECALCULO);
     Except
           Mensagem:=mensagem+'/Redu��o na Base de C�lculo';
     End;
     try
        Strtofloat(Self.ALIQUOTACUPOM);
     Except
           Mensagem:=mensagem+'/Al�quota do Cupom';
     End;

     try
        Strtofloat(Self.IPI);
     Except
           Mensagem:=mensagem+'/Ipi';
     End;

     try
        Strtofloat(Self.valorseguro);
     Except
           Mensagem:=mensagem+'/valorseguro';
     End;

     try
        Strtofloat(Self.valorfrete);
     Except
           Mensagem:=mensagem+'/valorfrete';
     End;


     try
        Strtofloat(Self.valorpauta);
     Except
           Mensagem:=mensagem+'/Valor pauta';
     End;

     try
        Strtofloat(Self.percentualagregado);
     Except
           Mensagem:=mensagem+'/Percentual Agregado';
     End;

     try
        Strtofloat(Self.Margemvaloragregadoconsumidor);
     Except
           Mensagem:=mensagem+'/Margem de Valor Agregado Consumidor Final';
     End;

     try
        Strtofloat(Self.BC_ICMS);
     Except
           Mensagem:=mensagem+'/Base de C�lculo do ICMS';
     End;

     try
        Strtofloat(Self.BC_ICMS_ST);
     Except
           Mensagem:=mensagem+'/Base de C�lculo do ICMS ST';
     End;

     try
        Strtofloat(Self.VALOR_ICMS);
     Except
           Mensagem:=mensagem+'/Valor do ICMS';
     End;

     try
        Strtofloat(Self.VALOR_ICMS_ST);
     Except
           Mensagem:=mensagem+'/Valor do ICMS ST';
     End;

     try
        Strtofloat(Self.BC_IPI);
     Except
           Mensagem:=mensagem+'/BC_IPI';
     End;

     try
        Strtofloat(Self.VALOR_IPI);
     Except
           Mensagem:=mensagem+'/VALOR_IPI';
     End;

     try
        Strtofloat(Self.BC_PIS);
     Except
           Mensagem:=mensagem+'/BC_PIS';
     End;

     try
        Strtofloat(Self.VALOR_PIS);
     Except
           Mensagem:=mensagem+'/VALOR_PIS';
     End;

      try
        Strtofloat(Self.BC_PIS_ST);
     Except
           Mensagem:=mensagem+'/BC_PIS_ST';
     End;

     try
        Strtofloat(Self.VALOR_PIS_ST);
     Except
           Mensagem:=mensagem+'/VALOR_PIS_ST';
     End;


     try
        Strtofloat(Self.BC_COFINS);
     Except
           Mensagem:=mensagem+'/BC_COFINS';
     End;

     try
        Strtofloat(Self.VALOR_COFINS);
     Except
           Mensagem:=mensagem+'/VALOR_COFINS';
     End;

      try
        Strtofloat(Self.BC_COFINS_ST);
     Except
           Mensagem:=mensagem+'/BC_COFINS_ST';
     End;

     try
        Strtofloat(Self.VALOR_COFINS_ST);
     Except
           Mensagem:=mensagem+'/VALOR_COFINS_ST';
     End;

     try
        If (Self.referencia<>'')
        Then Strtoint(Self.referencia);
     Except
           Mensagem:=mensagem+'/Refer�ncia';
     End;

     try
        If (Self.SituacaoTributaria_TabelaA.Get_Codigo<>'')
        Then Strtoint(Self.SituacaoTributaria_TabelaA.Get_Codigo);
     Except
           Mensagem:=mensagem+'/SituacaoTributaria_TabelaA';
     End;

     try
        If (Self.SituacaoTributaria_TabelaB.Get_Codigo<>'')
        Then Strtoint(Self.SituacaoTributaria_TabelaB.Get_Codigo);
     Except
           Mensagem:=mensagem+'/SituacaoTributaria_TabelaB';
     End;

     try
        If (Self.CFOP.Get_Codigo<>'')
        Then Strtoint(Self.CFOP.Get_Codigo);
     Except
           Mensagem:=mensagem+'/CFOP';
     End;


     try
        If (Self.IMPOSTO_ICMS_ORIGEM.Get_Codigo<>'')
        Then Strtoint(Self.IMPOSTO_ICMS_ORIGEM.Get_Codigo);
     Except
           Mensagem:=mensagem+'/IMPOSTO_ICMS_ORIGEM';
     End;

     try
        If (Self.IMPOSTO_ICMS_DESTINO.Get_Codigo<>'')
        Then Strtoint(Self.IMPOSTO_ICMS_DESTINO.Get_Codigo);
     Except
           Mensagem:=mensagem+'/IMPOSTO_ICMS_ORIGEM ST';
     End;

     try
        If (Self.IMPOSTO_IPI.Get_Codigo<>'')
        Then Strtoint(Self.IMPOSTO_IPI.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Imposto IPI';
     End;
     try
        If (Self.IMPOSTO_PIS_ORIGEM.Get_Codigo<>'')
        Then Strtoint(Self.IMPOSTO_PIS_ORIGEM.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Imposto PIs Origem';
     End;
     try
        If (Self.IMPOSTO_PIS_DESTINO.Get_Codigo<>'')
        Then Strtoint(Self.IMPOSTO_PIS_DESTINO.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Imposto Pis Destino';
     End;
     try
        If (Self.IMPOSTO_COFINS_ORIGEM.Get_Codigo<>'')
        Then Strtoint(Self.IMPOSTO_COFINS_ORIGEM.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Imposto Cofins Origem';
     End;
     try
        If (Self.IMPOSTO_COFINS_DESTINO.Get_Codigo<>'')
        Then Strtoint(Self.IMPOSTO_COFINS_DESTINO.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Imposto Cofins Destino';
     End;

     result:=Mensagem;

end;

function TObjmaterial_NF.VerificaData: String;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';

     result:=mensagem;

end;

function TObjmaterial_NF.VerificaFaixa: String;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
Try
   With Self do
   Begin
        Mensagem:='';

        result:=mensagem;
  End;
Finally

end;

end;




constructor TObjmaterial_NF.create(Owner:TComponent);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin

        Self.Owner := Owner;
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        MaterialInsertSql:=TStringList.create;
        MaterialInsertSqlValues:=TStringList.create;
        MaterialModifySQl:=TStringList.create;

        Self.NOTAFISCAL:=TOBJNOTAFISCAL.create;
        Self.SituacaoTributaria_TabelaA:=TOBJTABELAA_ST.Create ;
        Self.SituacaoTributaria_TabelaB:=TOBJTABELAB_ST.Create ;
        Self.IMPOSTO_ICMS_ORIGEM:=TobjIMPOSTO_ICMS.Create(self.Owner);
        Self.IMPOSTO_ICMS_DESTINO:=TobjIMPOSTO_ICMS.Create(Self.Owner);
        Self.CFOP:=TOBJCFOP.Create;

        Self.IMPOSTO_IPI:=TOBJIMPOSTO_IPI.create;
        Self.IMPOSTO_PIS_ORIGEM:=TOBJIMPOSTO_PIS.create;
        Self.IMPOSTO_PIS_DESTINO:=TOBJIMPOSTO_PIS.create;
        Self.IMPOSTO_COFINS_ORIGEM:=TOBJIMPOSTO_COFINS.create;
        Self.IMPOSTO_COFINS_DESTINO:=TOBJIMPOSTO_COFINS.create;
        self.CSOSN:=TObjCSOSN.Create(Self.Owner);

        Self.ZerarTabela;

        With Self do
        Begin

                MaterialInsertSQL.clear;
                MaterialInsertSQL.add('CODIGO,NOTAFISCAL,QUANTIDADE');
                MaterialInsertSQL.add(',VALOR,valorpauta,ALIQUOTA,REDUCAOBASECALCULO');
                MaterialInsertSQL.add(',ALIQUOTACUPOM,ISENTO,SUBSTITUICAOTRIBUTARIA,IPI,');
                MaterialInsertSQL.add('valorseguro,valorfrete,SituacaoTributaria_TabelaA,');
                MaterialInsertSQL.add('SituacaoTributaria_TabelaB,CFOP,IMPOSTO_ICMS_ORIGEM,');
                MaterialInsertSQL.add('IMPOSTO_ICMS_DESTINO,percentualagregado,Margemvaloragregadoconsumidor,BC_ICMS,VALOR_ICMS,BC_ICMS_ST,VALOR_ICMS_ST');
                MaterialInsertSQL.add(',BC_IPI,VALOR_IPI,BC_PIS,VALOR_PIS,BC_PIS_ST,VALOR_PIS_ST,BC_COFINS,VALOR_COFINS,BC_COFINS_ST,VALOR_COFINS_ST');
                MaterialInsertSQL.add(',IMPOSTO_IPI,IMPOSTO_PIS_ORIGEM,IMPOSTO_PIS_DESTINO,IMPOSTO_COFINS_ORIGEM,IMPOSTO_COFINS_DESTINO,referencia');
                MaterialInsertSql.Add(',UNIDADE,CSOSN,DESCONTO');


                MaterialInsertSQLVALUES.add(':CODIGO,:NOTAFISCAL,:QUANTIDADE,');
                MaterialInsertSQLVALUES.add(':VALOR,:valorpauta,:ALIQUOTA,:REDUCAOBASECALCULO,:ALIQUOTACUPOM,');
                MaterialInsertSQLVALUES.add(':ISENTO,:SUBSTITUICAOTRIBUTARIA,:IPI,:valorseguro,:valorfrete,');
                MaterialInsertSQLVALUES.add(':SituacaoTributaria_TabelaA,:SituacaoTributaria_TabelaB,:CFOP,');
                MaterialInsertSQLVALUES.add(':IMPOSTO_ICMS_ORIGEM,:IMPOSTO_ICMS_DESTINO,:percentualagregado,:Margemvaloragregadoconsumidor,:BC_ICMS,:VALOR_ICMS,:BC_ICMS_ST,:VALOR_ICMS_ST');
                MaterialInsertSQLVALUES.add(',:BC_IPI,:VALOR_IPI,:BC_PIS,:VALOR_PIS,:BC_PIS_ST,:VALOR_PIS_ST,:BC_COFINS,:VALOR_COFINS,:BC_COFINS_ST,:VALOR_COFINS_ST');
                MaterialInsertSQLVALUES.add(',:IMPOSTO_IPI,:IMPOSTO_PIS_ORIGEM,:IMPOSTO_PIS_DESTINO,:IMPOSTO_COFINS_ORIGEM,:IMPOSTO_COFINS_DESTINO,:referencia');
                MaterialInsertSQLVALUES.add(',:UNIDADE,:CSOSN,:DESCONTO');
//CODIFICA INSERTSQL

                MaterialModifySQL.clear;
                MaterialModifySQL.add('CODIGO=:CODIGO,NOTAFISCAL=:NOTAFISCAL');
                MaterialModifySQL.add(',QUANTIDADE=:QUANTIDADE,VALOR=:VALOR,valorpauta=:valorpauta,ALIQUOTA=:ALIQUOTA');
                MaterialModifySQL.add(',REDUCAOBASECALCULO=:REDUCAOBASECALCULO,ALIQUOTACUPOM=:ALIQUOTACUPOM');
                MaterialModifySQL.add(',ISENTO=:ISENTO,SUBSTITUICAOTRIBUTARIA=:SUBSTITUICAOTRIBUTARIA');
                MaterialModifySQL.add(',IPI=:IPI,valorseguro=:valorseguro,valorfrete=:valorfrete,');
                MaterialModifySQL.add('SituacaoTributaria_TabelaA=:SituacaoTributaria_TabelaA,');
                MaterialModifySQL.add('SituacaoTributaria_TabelaB=:SituacaoTributaria_TabelaB,');
                MaterialModifySQL.add('IMPOSTO_ICMS_ORIGEM=:IMPOSTO_ICMS_ORIGEM,IMPOSTO_ICMS_DESTINO=:IMPOSTO_ICMS_DESTINO,');
                MaterialModifySQL.add('CFOP=:CFOP,percentualagregado=:percentualagregado,');
                MaterialModifySQL.add('Margemvaloragregadoconsumidor=:Margemvaloragregadoconsumidor,');
                MaterialModifySQL.add('BC_ICMS=:BC_ICMS,VALOR_ICMS=:VALOR_ICMS,BC_ICMS_ST=:BC_ICMS_ST,VALOR_ICMS_ST=:VALOR_ICMS_ST');
                MaterialModifySQL.add(',BC_IPI=:BC_IPI,VALOR_IPI=:VALOR_IPI,BC_PIS=:BC_PIS,VALOR_PIS=:VALOR_PIS,BC_PIS_ST=:BC_PIS_ST,');
                MaterialModifySQL.add('VALOR_PIS_ST=:VALOR_PIS_ST,BC_COFINS=:BC_COFINS,VALOR_COFINS=:VALOR_COFINS,BC_COFINS_ST=:BC_COFINS_ST,VALOR_COFINS_ST=:VALOR_COFINS_ST');
                MaterialModifySQL.add(',IMPOSTO_IPI=:IMPOSTO_IPI');
                MaterialModifySQL.add(',IMPOSTO_PIS_ORIGEM=:IMPOSTO_PIS_ORIGEM,IMPOSTO_PIS_DESTINO=:IMPOSTO_PIS_DESTINO');
                MaterialModifySQL.add(',IMPOSTO_COFINS_ORIGEM=:IMPOSTO_COFINS_ORIGEM,IMPOSTO_COFINS_DESTINO=:IMPOSTO_COFINS_DESTINO,referencia=:referencia');
                MaterialModifySQl.Add(',UNIDADE=:UNIDADE,CSOSN=:CSOSN,DESCONTO=:DESCONTO');


                Self.status          :=dsInactive;
        End;

end;

destructor TObjmaterial_NF.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(MaterialInsertSql);
    Freeandnil(MaterialInsertSqlValues);
    Freeandnil(materialModifySQl);
    Self.NOTAFISCAL.FREE;
    Self.SituacaoTributaria_TabelaA.free;
    Self.SituacaoTributaria_TabelaB.free;
    Self.IMPOSTO_ICMS_ORIGEM.Free;
    Self.IMPOSTO_ICMS_DESTINO.Free;
    Self.CFOP.Free;

    Self.IMPOSTO_IPI.FREE;
    Self.IMPOSTO_PIS_ORIGEM.FREE;
    Self.IMPOSTO_PIS_DESTINO.FREE;
    Self.IMPOSTO_COFINS_ORIGEM.FREE;
    Self.IMPOSTO_COFINS_DESTINO.FREE;
    self.CSOSN.Free;

    
end;

procedure TObjmaterial_NF.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjmaterial_NF.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjmaterial_NF.Submit_QUANTIDADE(parametro: string);
begin
        Self.QUANTIDADE:=Parametro;
end;
function TObjmaterial_NF.Get_QUANTIDADE: string;
begin
        Result:=Self.QUANTIDADE;
end;
procedure TObjmaterial_NF.Submit_VALOR(parametro: string);
begin
        Self.VALOR:=Parametro;
end;
function TObjmaterial_NF.Get_VALOR: string;
begin
        Result:=Self.VALOR;
end;


procedure TObjmaterial_NF.Submit_valorpauta(parametro: string);
begin
        Self.valorpauta:=Parametro;
end;
function TObjmaterial_NF.Get_valorpauta: string;
begin
        Result:=Self.valorpauta;
end;


function TObjmaterial_NF.Get_VALORFINAL: string;
begin
        Result:=Self.VALORFINAL;
end;


procedure TObjmaterial_nf.Submit_ALIQUOTA(parametro: string);
begin
        Self.ALIQUOTA:=Parametro;
end;
function TObjmaterial_nf.Get_ALIQUOTA: string;
begin
        Result:=Self.ALIQUOTA;
end;
procedure TObjmaterial_nf.Submit_REDUCAOBASECALCULO(parametro: string);
begin
        Self.REDUCAOBASECALCULO:=Parametro;
end;
function TObjmaterial_nf.Get_REDUCAOBASECALCULO: string;
begin
        Result:=Self.REDUCAOBASECALCULO;
end;
procedure TObjmaterial_nf.Submit_ALIQUOTACUPOM(parametro: string);
begin
        Self.ALIQUOTACUPOM:=Parametro;
end;
function TObjmaterial_nf.Get_ALIQUOTACUPOM: string;
begin
        Result:=Self.ALIQUOTACUPOM;
end;
procedure TObjmaterial_nf.Submit_ISENTO(parametro: string);
begin
        Self.ISENTO:=Parametro;
end;
function TObjmaterial_nf.Get_ISENTO: string;
begin
        Result:=Self.ISENTO;
end;
procedure TObjmaterial_nf.Submit_SUBSTITUICAOTRIBUTARIA(parametro: string);
begin
        Self.SUBSTITUICAOTRIBUTARIA:=Parametro;
end;
function TObjmaterial_nf.Get_SUBSTITUICAOTRIBUTARIA: string;
begin
        Result:=Self.SUBSTITUICAOTRIBUTARIA;
end;
procedure TObjmaterial_nf.Submit_IPI(parametro: string);
begin
        Self.IPI:=Parametro;
end;
function TObjmaterial_nf.Get_IPI: string;
begin
        Result:=Self.IPI;
end;



procedure TObjmaterial_nf.Submit_valorseguro(parametro: string);
begin
        Self.valorseguro:=Parametro;
end;
function TObjmaterial_nf.Get_valorseguro: string;
begin
        Result:=Self.valorseguro;
end;

procedure TObjmaterial_nf.Submit_valorfrete(parametro: string);
begin
        Self.valorfrete:=Parametro;
end;
function TObjmaterial_nf.Get_valorfrete: string;
begin
        Result:=Self.valorfrete;
end;

//CODIFICA GETSESUBMITS


procedure TObjmaterial_NF.EdtNOTAFISCALExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.NOTAFISCAL.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.NOTAFISCAL.tabelaparaobjeto;
End;
procedure TObjmaterial_NF.EdtNOTAFISCALKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.NOTAFISCAL.Get_Pesquisa,Self.NOTAFISCAL.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.NOTAFISCAL.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.NOTAFISCAL.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.NOTAFISCAL.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjmaterial_NF.Get_percentualagregado: string;
begin
     Result:=Self.percentualagregado;
end;

procedure TObjmaterial_NF.Submit_percentualagregado(parametro: string);
begin
     Self.percentualagregado:=parametro;
end;

function TObjmaterial_NF.Get_Margemvaloragregadoconsumidor: string;
begin
     Result:=Self.Margemvaloragregadoconsumidor;
end;

procedure TObjmaterial_NF.Submit_Margemvaloragregadoconsumidor(parametro: string);
begin
     Self.Margemvaloragregadoconsumidor:=parametro;
end;


function TObjmaterial_NF.Get_BC_ICMS: String;
begin
     Result:=Self.BC_ICMS;
end;

function TObjmaterial_NF.Get_BC_ICMS_ST: String;
begin
     Result:=Self.BC_ICMS_ST;
end;

function TObjmaterial_NF.Get_VALOR_ICMS: String;
begin
     Result:=Self.VALOR_ICMS;
end;

function TObjmaterial_NF.Get_VALOR_ICMS_ST: String;
begin
     Result:=Self.VALOR_ICMS_ST;
end;

procedure TObjmaterial_NF.Submit_BC_ICMS(parametro: string);
begin
     Self.BC_ICMS:=parametro;
end;

procedure TObjmaterial_NF.Submit_BC_ICMS_ST(parametro: String);
begin
     Self.BC_ICMS_ST:=parametro;
end;

procedure TObjmaterial_NF.Submit_VALOR_ICMS(parametro: string);
begin
     Self.VALOR_ICMS:=parametro;
end;

procedure TObjmaterial_NF.Submit_VALOR_ICMS_ST(parametro: string);
begin
     Self.VALOR_ICMS_ST:=parametro;
end;


function TObjmaterial_NF.get_BC_COFINS: STRING;
begin
     Result:=Self.BC_COFINS;
end;

function TObjmaterial_NF.get_BC_COFINS_ST: STRING;
begin
     Result:=Self.BC_COFINS_ST;
end;

function TObjmaterial_NF.get_BC_IPI: STRING;
begin
     Result:=Self.BC_IPI;
end;

function TObjmaterial_NF.get_BC_PIS: STRING;
begin
     Result:=Self.BC_PIS;
end;

function TObjmaterial_NF.get_BC_PIS_ST: STRING;
begin
     Result:=Self.BC_PIS_ST;
end;

function TObjmaterial_NF.get_VALOR_COFINS: string;
begin
     Result:=Self.VALOR_COFINS;
end;

function TObjmaterial_NF.get_VALOR_COFINS_ST: string;
begin
     Result:=Self.VALOR_COFINS_ST;
end;

function TObjmaterial_NF.get_VALOR_IPI: string;
begin
     Result:=Self.VALOR_IPI;
end;

function TObjmaterial_NF.get_VALOR_PIS: string;
begin
     Result:=Self.VALOR_PIS;
end;

function TObjmaterial_NF.get_VALOR_PIS_ST: string;
begin
     Result:=Self.VALOR_PIS_ST;
end;

procedure TObjmaterial_NF.Submit_BC_COFINS(parametro: string);
begin
     Self.BC_COFINS:=Parametro;
end;

procedure TObjmaterial_NF.Submit_BC_COFINS_ST(parametro: string);
begin
     Self.BC_COFINS_ST:=Parametro;
end;

procedure TObjmaterial_NF.Submit_BC_IPI(parametro: string);
begin
     Self.BC_IPI:=Parametro;
end;

procedure TObjmaterial_NF.Submit_BC_PIS(parametro: string);
begin
     Self.BC_PIS:=Parametro;
end;

procedure TObjmaterial_NF.Submit_BC_PIS_ST(parametro: string);
begin
     Self.BC_PIS_ST:=Parametro;
end;

procedure TObjmaterial_NF.Submit_VALOR_COFINS(parametro: string);
begin
     Self.VALOR_COFINS:=Parametro;
end;

procedure TObjmaterial_NF.Submit_VALOR_COFINS_ST(parametro: string);
begin
     Self.VALOR_COFINS_ST:=Parametro;
end;

procedure TObjmaterial_NF.Submit_VALOR_IPI(parametro: string);
begin
     Self.VALOR_IPI:=Parametro;
end;

procedure TObjmaterial_NF.Submit_VALOR_PIS(parametro: string);
begin
     Self.VALOR_PIS:=Parametro;
end;

procedure TObjmaterial_NF.Submit_VALOR_PIS_ST(parametro: string);
begin
     Self.VALOR_PIS_ST:=Parametro;
end;


procedure TobjMaterial_nf.EdtIMPOSTO_IPIExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.IMPOSTO_IPI.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.IMPOSTO_IPI.tabelaparaobjeto;
     LABELNOME.CAPTION:=''
End;
procedure TobjMaterial_nf.EdtIMPOSTO_IPIKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FIMPOSTO_IPI:TFIMPOSTO_IPI;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FIMPOSTO_IPI:=TFIMPOSTO_IPI.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.IMPOSTO_IPI.Get_Pesquisa,Self.IMPOSTO_IPI.Get_TituloPesquisa,FIMPOSTO_IPI)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.IMPOSTO_IPI.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.IMPOSTO_IPI.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.IMPOSTO_IPI.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FIMPOSTO_IPI);
     End;
end;
procedure TobjMaterial_nf.EdtIMPOSTO_PIS_ORIGEMExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.IMPOSTO_PIS_ORIGEM.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.IMPOSTO_PIS_ORIGEM.tabelaparaobjeto;
     LABELNOME.CAPTION:='';
End;
procedure TobjMaterial_nf.EdtIMPOSTO_PIS_ORIGEMKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FIMPOSTO_PIS:TFIMPOSTO_PIS;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FIMPOSTO_PIS:=TFIMPOSTO_PIS.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.IMPOSTO_PIS_ORIGEM.Get_Pesquisa,Self.IMPOSTO_PIS_ORIGEM.Get_TituloPesquisa,FIMPOSTO_PIS)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.IMPOSTO_PIS_ORIGEM.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.IMPOSTO_PIS_ORIGEM.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.IMPOSTO_PIS_ORIGEM.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FIMPOSTO_PIS);
     End;
end;
procedure TobjMaterial_nf.EdtIMPOSTO_PIS_DESTINOExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.IMPOSTO_PIS_DESTINO.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.IMPOSTO_PIS_DESTINO.tabelaparaobjeto;
     LABELNOME.CAPTION:='';
End;
procedure TobjMaterial_nf.EdtIMPOSTO_PIS_DESTINOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FIMPOSTO_PIS:TFIMPOSTO_PIS;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FIMPOSTO_PIS:=TFIMPOSTO_PIS.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.IMPOSTO_PIS_DESTINO.Get_Pesquisa,Self.IMPOSTO_PIS_DESTINO.Get_TituloPesquisa,FIMPOSTO_PIS)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.IMPOSTO_PIS_DESTINO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.IMPOSTO_PIS_DESTINO.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.IMPOSTO_PIS_DESTINO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FIMPOSTO_PIS);
     End;
end;
procedure TobjMaterial_nf.EdtIMPOSTO_COFINS_ORIGEMExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.IMPOSTO_COFINS_ORIGEM.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.IMPOSTO_COFINS_ORIGEM.tabelaparaobjeto;
     LABELNOME.CAPTION:='';
End;
procedure TobjMaterial_nf.EdtIMPOSTO_COFINS_ORIGEMKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FIMPOSTO_COFINS:TFIMPOSTO_COFINS;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FIMPOSTO_COFINS:=TFIMPOSTO_COFINS.create(nil);

            If (FpesquisaLocal.PreparaPesquisa
(Self.IMPOSTO_COFINS_ORIGEM.Get_Pesquisa,Self.IMPOSTO_COFINS_ORIGEM.Get_TituloPesquisa,FIMPOSTO_COFINS)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.IMPOSTO_COFINS_ORIGEM.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.IMPOSTO_COFINS_ORIGEM.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.IMPOSTO_COFINS_ORIGEM.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FIMPOSTO_COFINS);
     End;
end;
procedure TobjMaterial_nf.EdtIMPOSTO_COFINS_DESTINOExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.IMPOSTO_COFINS_DESTINO.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.IMPOSTO_COFINS_DESTINO.tabelaparaobjeto;
     LABELNOME.CAPTION:='';
End;
procedure TobjMaterial_nf.EdtIMPOSTO_COFINS_DESTINOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FIMPOSTO_COFINS:TFIMPOSTO_COFINS;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FIMPOSTO_COFINS:=TFIMPOSTO_COFINS.create(nil);

            If (FpesquisaLocal.PreparaPesquisa
(Self.IMPOSTO_COFINS_DESTINO.Get_Pesquisa,Self.IMPOSTO_COFINS_DESTINO.Get_TituloPesquisa,FIMPOSTO_COFINS)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.IMPOSTO_COFINS_DESTINO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.IMPOSTO_COFINS_DESTINO.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.IMPOSTO_COFINS_DESTINO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FIMPOSTO_COFINS);
     End;
end;


function TObjmaterial_NF.Get_referencia: string;
begin
     Result:=Self.referencia;
end;

procedure TObjmaterial_NF.Submit_referencia(parametro: String);
begin
     Self.referencia:=Parametro;
end;

procedure TObjmaterial_NF.Submit_desconto(parametro:string);
begin
    Self.desconto:=parametro;
end;

procedure TObjmaterial_NF.submit_UNIDADE(parametro:string);
begin
    Self.unidade:=parametro;
end;

function TObjmaterial_NF.Get_Desconto:string;
begin
    Result:=Self.desconto;
end;

function TObjmaterial_NF.Get_Unidade:string;
begin
    Result:=self.unidade;
end;                     

end.



