object FOrdemMedicao: TFOrdemMedicao
  Left = 510
  Top = 197
  Width = 878
  Height = 507
  Caption = 'Ordem de Medi'#231#227'o'
  Color = 10643006
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    862
    469)
  PixelsPerInch = 96
  TextHeight = 13
  object lbLbDescricao: TLabel
    Left = 7
    Top = 74
    Width = 49
    Height = 14
    Caption = 'Descri'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lbLbCliente: TLabel
    Left = 7
    Top = 100
    Width = 32
    Height = 14
    Caption = 'Cliente'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lbLbVendedor: TLabel
    Left = 7
    Top = 127
    Width = 47
    Height = 14
    Caption = 'Vendedor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lbNomeCliente: TLabel
    Left = 207
    Top = 103
    Width = 313
    Height = 13
    Cursor = crHandPoint
    AutoSize = False
    Caption = 'Cliente'
    Font.Charset = ANSI_CHARSET
    Font.Color = clAqua
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    OnClick = lbNomeClienteClick
    OnMouseMove = lbconcluidoMouseMove
    OnMouseLeave = lbconcluidoMouseLeave
  end
  object lbNomeVendedor: TLabel
    Left = 207
    Top = 128
    Width = 313
    Height = 13
    Cursor = crHandPoint
    AutoSize = False
    Caption = 'Vendedor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clAqua
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    OnClick = lbNomeVendedorClick
    OnMouseMove = lbconcluidoMouseMove
    OnMouseLeave = lbconcluidoMouseLeave
  end
  object lb9: TLabel
    Left = 6
    Top = 320
    Width = 240
    Height = 14
    Caption = 
      'Observa'#231#245'es.....................................................' +
      '.....'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object lb1: TLabel
    Left = 7
    Top = 152
    Width = 38
    Height = 14
    Caption = 'Medidor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lbNomeMedidor: TLabel
    Left = 207
    Top = 153
    Width = 313
    Height = 13
    Cursor = crHandPoint
    AutoSize = False
    Caption = 'Medidor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clAqua
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    OnClick = lbNomeMedidorClick
    OnMouseMove = lbconcluidoMouseMove
    OnMouseLeave = lbconcluidoMouseLeave
  end
  object lb2: TLabel
    Left = 207
    Top = 176
    Width = 90
    Height = 14
    Caption = 'Data para Medi'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lb3: TLabel
    Left = 7
    Top = 177
    Width = 40
    Height = 14
    Caption = 'Emiss'#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lb4: TLabel
    Left = 7
    Top = 224
    Width = 46
    Height = 14
    Caption = 'Endere'#231'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lb5: TLabel
    Left = 7
    Top = 248
    Width = 33
    Height = 14
    Caption = 'Cidade'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lb6: TLabel
    Left = 7
    Top = 296
    Width = 14
    Height = 13
    Caption = 'UF'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lb7: TLabel
    Left = 207
    Top = 272
    Width = 29
    Height = 14
    Caption = 'Bairro'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lb8: TLabel
    Left = 7
    Top = 273
    Width = 37
    Height = 14
    Caption = 'Numero'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lb11: TLabel
    Left = 752
    Top = 67
    Width = 73
    Height = 23
    Cursor = crHandPoint
    Anchors = [akTop, akRight]
    Caption = 'Pedido'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object lbnumorcamento: TLabel
    Left = 783
    Top = 99
    Width = 42
    Height = 23
    Cursor = crHandPoint
    Anchors = [akTop, akRight]
    Caption = '470'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    OnClick = lbNumOrcamentoClick
    OnMouseMove = lbconcluidoMouseMove
    OnMouseLeave = lbconcluidoMouseLeave
  end
  object lb12: TLabel
    Left = 688
    Top = 335
    Width = 137
    Height = 46
    Cursor = crHandPoint
    Alignment = taCenter
    Anchors = [akTop, akRight]
    Caption = 'Gerar Ordem de Medi'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    WordWrap = True
    OnClick = lb12Click
    OnMouseMove = lbconcluidoMouseMove
    OnMouseLeave = lbconcluidoMouseLeave
  end
  object lbl2: TLabel
    Left = 7
    Top = 198
    Width = 77
    Height = 14
    Caption = 'Hor'#225'rio de '#205'nicio'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lbl3: TLabel
    Left = 207
    Top = 197
    Width = 41
    Height = 14
    Caption = 'Dura'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lbl1: TLabel
    Left = 701
    Top = 195
    Width = 124
    Height = 16
    Cursor = crHandPoint
    Anchors = [akTop, akRight]
    Caption = 'Hor'#225'rio de T'#233'rmino'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    OnClick = lbNumOrcamentoClick
  end
  object lbHorarioTermino: TLabel
    Left = 725
    Top = 215
    Width = 100
    Height = 23
    Cursor = crHandPoint
    Anchors = [akTop, akRight]
    Caption = '15:00:00'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    Visible = False
    OnClick = lbNumOrcamentoClick
  end
  object Label1: TLabel
    Left = 207
    Top = 294
    Width = 35
    Height = 14
    Caption = 'Compl.:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 862
    Height = 58
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    DesignSize = (
      862
      58)
    object lbnomeformulario: TLabel
      Left = 458
      Top = 13
      Width = 177
      Height = 24
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Ordem de Medi'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -21
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigo: TLabel
      Left = 642
      Top = 9
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btAjuda: TSpeedButton
      Left = 809
      Top = -11
      Width = 53
      Height = 80
      Cursor = crHandPoint
      Hint = 'Ajuda'
      Anchors = [akTop, akRight]
      Flat = True
      Glyph.Data = {
        4A0D0000424D4A0D0000000000003600000028000000240000001F0000000100
        180000000000140D0000D8030000D80300000000000000000000FEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFAF6FBF9F2FEFEF4FFFFFAFF
        FCFDFFF9FFFEFEFDFCFFFFFEFEFFFDFEFCFEFEF8FFFFFBFEFCFFFFFCFFFDF6FB
        FFF7F8FFF7FBFFFBFBFDF7FFFFFCFFFFFEF7FDFCFBFFFFFFFBFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFF9FD
        F8FDFFFFFDFBFFFFFBFFFFFDFEFFFCFCFFFEFFFEFDFFFCF9FBF0EFF1F0EBEDF9
        F1F2F6F8F9F1FFFFF4FFFFFEFCFFFFFDFFFFFAFFFBFCFFFEFDFFFFFAFDFAFCFD
        F7FFFFFFFCFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFFFCFFFFFCFEFFFDFCFFFFFFFFFDFFF2E4E6DEBCBCCF
        9F99EB8688E68081EA7E7DF68686FE9092FDA2A5F7C3C3FFF0EDFFFAFFF8FFFF
        FFFDFFFFF5FDFCFDFFF6FFFFFBF8FAFFFDFEFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFBFFFCFEFFFBFFFCE7
        E1DCC29E9EBF7579DC7677F07B76FF8783FF8F88FD9388FB8F84FF837BFF7977
        F77576F98084FBB6B9F2F2ECE8FFFAF8FDFCFFFEFFFAFCFDFFFCFEFCFEFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFEFDFFFAFCD7B8BBB17879C87772F08E88FE9893F58E8CE7797DD76F70
        D26867DC716EE88581F69A95F99591FE8686FF7173F28484FFD9D4FFFFFBFDF8
        F7FFFCFFFFFEFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFF9B99D9CA76B6CDA8786F99B96E17D79
        B6484C98222FAB1122AA0F1EA5131FA41D25AA2226C53938E6625BFF9085FD9B
        91FF7B7CF2777BF4C8C7F8FFFEF9FEFDFFFEFFFFFBFCFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFC0A0A1A7706D
        E6928CFC9691B3484A87192589112398182BA32E31DC6F71EB8082CC5050BB28
        26D32A22EB2F24FE3727F36D69FF9D97FB7D83F7707EFEC5CDFFFEFFFFFDFDED
        FFFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFCFBFDFFFDFFFFFBFFFAFFFFF2FAFA
        FFFEFEDCBDBEA66D6BDB9092ED909996303C770E1B8D1E2C9B1C2BA11425D96F
        75FDA8B0FFA2B0FFA0AAC83F43BC2F26D73927FF3929FF2B1CEC5650FF9996FE
        7D7AFD7F7EFADFDBFFFEFFF4FEFEFEFDFFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFF
        FDF9FEFFFDFFF7FAFEFAFFFFF4E6E7A67F81CF8B8CEC9A9F92323C7B121D9123
        2F921C27A5232EA71C27DE837EFFB7B5F9A9AEFDB0B3BF4343B42821C9372BDA
        3326E33626CB2728CA5556FF9893FB7777DE9A9BFFFEFEFFFCFDFEFDFFFEFDFF
        FEFDFFFEFDFFFEFEFEFEFDFFFFFBFFFFFBFFFAFBFFFFFFFFC3AEADA77678F8A4
        A9A9515776131B8D202898242BA3282CA72629A82627B55047EBADA2FFBCB5CD
        7974A92E2AA13026A5372BBD332DBF3528BD2B2DA51E26DF7372FF918FE27377
        F0CCCCFFFFFEFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFAF9FBFFFDFFFFFAFFFFFE
        FFF5F0EFA38384DB9FA0E78A91791D22852428992E309E2827A72C28AC3028AB
        3229A3271FA13E36B4413E9E332CB03129A73126A23127B0322EA93026AD3131
        A91925B63239F2948FFD7C7FE9989BFBFFF9FEFDFFFEFDFFFEFDFFFEFDFFFFFE
        FEFCFBFDFFFEFFFFFDFFFFFEFFDACECEAE8687F0ABAEB156597E24248B2C2992
        2A25A93730A93328AE3B2EA33222D35E59EA928BF28C87E27D74AC362BA93A2A
        A23729A5342AA3342CA62728B02931A71821D8716FFF8A8ADE7D7FEFECE7FEFD
        FFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFFFDFCFEFFFCFEFFFEFFC0B3B1BF9392F0
        A5A78F3B35842A2396352B9F372AA93C2EAC3D2DAC3C2AA1311FD16D69F8CABF
        FBC0B7FCABA3AA3A2EA23827A33928A9372AA6352BAE302BA72C2EAA1922C550
        51FF9692DC7475E0D2D3FFFEFFFEFDFFFEFDFFFEFDFFFFFDFEFEFDFFFEFDFFFF
        FDFFFFFEFEB6A9A7CB9D9CEC9E9F8335288E3527A0392AAD4130A83C2AA73B29
        A93927AE3827CE6C66FFCDC1FABAB5FFBFB6D46B62AD3126B43427B13A2BA933
        27A93327A02A25AC1F28AF373BFC9D94E7797BD1B8BCFFFEFFFFFEFFFEFDFFFE
        FDFFFFFDFEFCFCFCFDFCFEFFFEFFFDFBFBB5A8A6DBAEABE19192813522983C29
        A73B29AF3E2AA83B26AA3F2AAD3B2AAF3224B05A4EFFE0D0FFCEC6F7BEB5FFBE
        B3DE8C7BA44434A33628AD3C2CAD3726A8342DA7262DAE2E33FA9B92E27A7BD4
        ABB3FFFEFFFFFEFFFEFDFFFEFDFFFBFCFFF9FEFFFFFCFDFBFFFFFCF9FBC9AAAB
        DEB1ADDF9A91813628B03C2BAB3D25A7412AA6362AB3362EA13726AE3C25A833
        24D79987FFF4E4F9D3CEFEBEBEFFC9C3EC9E91AD4436B137299C3B27AA3526B1
        2F28A4302FF7969AE67F86C7AAA5FFFEFFFFFEFFFEFDFFFEFDFFFAFBFFFCFEFF
        FFFEFFFCFEFFFCFEFFCEB4B4E6B6B2E6A99F98392AA13426B2382C9E3E2EAA3E
        33AD4338A74539A24536AB443B9B3D38CF8A87FFEBE6FEDED9F1C0B8FFC9BDEC
        8B81AA372AA73B2AA83F2CA52B1FA6403BF9A1A1E57E86C9B3AEFFFEFFFFFEFF
        FEFDFFFEFDFFFAFDFFFDFCFEFFFEFFFDFCFEFAFFFFD3BEC0ECB5B2F5C2B8AB40
        329F3D31B84D49A24F47AD554EAE574DAE5D55B1655FAD5654B34E50A13B40B9
        7B7BFFE1DBFED7CFF9CAC2FFBCB5BD5146AD3627A3402AA83526AD544AFEACAB
        D5787FD4C7C5FFFEFFFEFDFFFEFDFFFEFDFFFBFFFFFFFBFDFFFDFDFFFDFFF9FE
        FFDFD1D2E7B1B0FFDACEC26B61A74C45A55C54A85A54D3958FF2B8B3F9C2BFF4
        B4B3AD5352AB5C59AC5553A54343F7C0BBFDE0D9FFCCCAFBC9C9CC6A60B03429
        9F3923A63624C9776BFFADACC67E84E4E0DFFEFDFFFEFDFFFEFDFFFEFDFFFAFE
        FFFFFCFFFCFCFCFFFEFFFBFEFFF4ECEDE2B7B4FFDBD2DAB0ABA6504AA86153AB
        514AD59594FFF8F7FEF1EFFAD4D2C469649C5148A85950A14B45ECBBB3FBE1DA
        FFCED0FFCFD6D06E66AB352AAD3B2A9F3726F7A89DFBA4A7BB8E91F2F7F6FEFD
        FFFEFDFFFEFDFFFEFDFFFCFBFDFFFEFFFCFEFEFEFDFFFDFCFFFFFEFFE2C7C3F3
        CAC1EDDEDCBD7672AD5144CE6459D7797AF6F5F1F4FFFEF9F3EEE9B4AAB16860
        B45E58C99087FAD7CDFFDCD6F8D5D2FECBCFBE5A55A23127A12F22CB7264FFC3
        BBD28B8ECFB7B9FAFFFFFEFDFFFEFDFFFEFDFFFEFDFFFEFBFDFFFFFFFCFEFEFB
        FDFEFFFAFFFFFEFFEBE5DEF2C4BDFFE5E8EFC8C0AE6053E17164F47773F0BBB8
        FDFEFAFDFFFFFFF8F1FCD6D4FFCDCCFFDDD8FFDED8FFE0DAFFDDD7E7A1A2A339
        32A7352EAC4A3EF8B8ADECB2ADB68E90FAECEEFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFDFFFCFCFCFAFFFEFAFDFFFFFBFFFFFDFDFBFFF9FFCFC9FFC7CCFFF9EF
        E8BFB0D66D5FFF7C70F9807EEACECDFFFBFFFDFCFFF9FEFFFDFFFFFFE4E5FFE2
        DFF6E7DEEDB6AFB34648A1312BB03E37FCAA9FF7CDC1BC8F8CD5C7C8FFFDFFFC
        FEFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFFFEFFFFFEFF
        FDFCFEFCFBFDFCC8C8F6D7D6FFFCF6F0C9C1E57F7AFF7673F98481ECA5A1E6D6
        D7FCF1F3F9F1F1ECD7D9F0C1C3D78A8DA84646A03232A54B4BECABAAFFD1CFC9
        A09EC9ADACFEF9F8FBFFFFFCFCFCFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFF
        FDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFDFCFEFFF4EEF8CDCAFDD8D4FFFBF4FDE6
        E4FBA1A6FB7375FD6F68EA6E5CCE6855B76958AE6458A5514B993E3AA04D4BC8
        7F7BFCC3C1FFCBCCDAA4A4BFAEABF3F9F4FFFFFEFFFCFDFCFEFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFEFDFFFFFEFFF4FF
        FFFBF2EFF6D6D1FDD9D3FFEFE8FCFBF1FFE6DCF6BBB2EE9285D07C70B76F65B2
        6F66C0807BDDA39EF3C1BBFFD6CFF6C6C5D4ACADCEBBB8F9FAF6F7FFFDFAFCFC
        FFFEFFFCF9FBFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFC
        FEFDFCFEFEFDFFFFFEFFFFFAFCFBFFFFF9F9F9FCE0DFFFD3CCFEDDD4F8ECE6F8
        F6F5F5F8F6F4EFEEF6E4E5FADBDCFFDBDCFFD9D8FBCAC8F0C0BCD7B5B6E1D8D5
        FAFFFEFFFFFFFEFCFCFAFFFFF8FDFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFFFFFBFFFBFCFDFDFFFD
        FBFFFFEEF1FFE2E1FFD3D2FFD0D2FFD2CFFFD1CDFFD2CFFFCFCFFFC9C9F2BFC3
        E9C4C6EED6D8F5F7F7FFFBFCFFFEFFFFFCFEFDFFFFF9FCFFFCFBFFFFFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFFFEFDFFFEFDFFFDFCFEFDFCFEFD
        FCFEFFFBFFF0FDFBF1FFFBFFFEFFFFFDFFF9FEFFF9FCFAFEF2EEF9EAE1F4E5DC
        F3E3DCF3E3DDF6E7E5FCF1F3FEFAFFF9FCFFF4FEFEFFFEFFFFF7FAFFFDFFFCF9
        FBFFFDFFFFFCFFF5F8FCFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFF7F9FFFDFEFFFFFBF8FFFDF9FDFBFAF1FBFB
        F6FFFFFBFDFEFBFEFFFFFCFFFFFDFFFFFEFFFBFFFFF8FFFFFDFEFFFFFAFFFFFD
        FFFAFEFFF8FFFFF9FBFCFFFDFFFFF9FCFBFAFCFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFCFFFEFF
        F7FDFFF5FEFFFBFFFFFFFFFEFFFCFDFFFCFFFFFAFFFFFCFFF9FEFDF9FFFDFBFF
        FEF9FCFAFBFBFBFFFFFFFFFCFEFEFDFFFBFAFCFBFFFFF5FDFDF7FFFFFDFFFFFF
        FCFFFEFDFFFEFDFFFEFDFFFEFDFF}
      OnClick = btAjudaClick
    end
    object btAjudaVideo: TSpeedButton
      Left = 763
      Top = -11
      Width = 53
      Height = 80
      Cursor = crHandPoint
      Hint = 'Ajuda'
      Anchors = [akTop, akRight]
      Flat = True
      Glyph.Data = {
        FA0E0000424DFA0E000000000000360000002800000023000000230000000100
        180000000000C40E0000C40E0000C40E00000000000000000000FFFFFEFFFFFF
        F4F2ECCCCBC3B3B1ADA8A8A5A8A8A6A8A8A6A8A8A6A8A8A6A8A8A6A8A8A6A8A8
        A6A8A8A6A8A8A6A8A8A6A8A8A6A8A8A6A8A8A6A8A8A6A8A8A6A8A8A6A8A8A6A8
        A8A6A8A8A6A8A8A6A8A8A6A8A8A6A8A8A6A7A8A6ABACA5BDBCB6DBDCD2FFFFFF
        FFFFFFFFFFFFFFFFFC9293932D2C540C0B5C00006500006800006C00006D0000
        6C00006C00006B00006B00006C00006B00006A00006C00006C00006B00006A00
        006B00006A00006900006800006B00006A00006A00006A00006A00006A00006B
        000068040062181852555661DDDDD7FFFFFFA0A09508045C0607B30E0EC60E0D
        CA0D0DCA0D0CC90E0DC90E0ECB1110CB1412C71310CB1011C90A0BCA0809C90E
        11C60C0ECA0A0BC90F0DC71111C70C0CC80D0ECA0C0CC80909C50B0AC70E0ECA
        1210C71211C51211C60F0DC80908C60909C30B08C1000083555468FFFFFF514F
        521815B01B1EE91E22DA2E30D72B25DC2724D92C2AD82E2BDA211DDC1E1DDC1F
        1EDA1617DB312CD73C3ED81D20D8221FD92A26D62C28D81B1ED82B27DB1918D4
        2B28D74A4ADB3534D51714D51B18DC1C1CD81515DA1F1DD53E3ED73C3ADB191A
        DA1313DA35336EFFFFFF5754521F1EC82727FB3331EF6768ECC8C7F6D7D8F9A9
        A8F64F4EEC2B2BF02627F42424F15252EDD6D8F4D8D9F89592EAC0BFF1D9D9F6
        6F6FEC2320F08A8DF3ADB0F1A5A4F07F7DEDBCBDF48386EA2725ED2223F13F3C
        E8BABBF1F2F4FBD4D6F68D8EEE2D27EE38357DFFFFFF5555532322CB2C2DFF2B
        29FF312EF8DBDBFBFFFFFF9898F82424FC2A2BFE2A2BFE2724FC9EA2F7FFFFFD
        908EF85256F5EEF1FAFFFFFF595AF51F1FFBA2A1FAFFFFFF9494F51917FB8D8A
        F6FFFFFD7070F42422F9AAAFF5FFFFFFB7BCF94C46F96568F6575CF238367FFF
        FFFF5655552220CC2929FF2527FD2B2AF8CFCEFBFFFFFF908DF62321FD2728FD
        2728FD2521F9ABAAF6FFFFFC7978F72323F7D3D2F9FCFDFF5554F41D1DFA9D9C
        F9FFFFFE7F7FF3171CFD6164F6FFFFFCBFBDF73636F6E0E0FAF4F5FC4848F514
        15FA1714FB2923FA3D3A82FFFFFF5655541F1ECA2427FF2325FB2A2AF7CFCFF9
        FFFFFE8C8DF8201FFC2426FC2426FC1F1FFAAAA8F7FFFFFE7879F82726F7D2D4
        FBFCFDFD5152F71A1BFB9B9BF9FFFFFE807FF01A18FD5C5FF5FFFFFFD3D1F941
        41F5E4E5F7E8EDF97C79F57377F68C8EF65E5FF735347EFFFFFF5655551D1CCA
        2124FF2222FC2928F7CFCFF9FFFFFE898AF91E1CFC2223FD2223FD1A1CFCA6A8
        F5FFFFFE7777F92222F6D2D4FAFDFEFC4E4FF81718FA9A9AF9FFFFFE7B79F10C
        0CFA6869F4FFFFFDC1C0F72926F2CDCCF8E9E9F6474CF56C6BF8FFFFFC9497F6
        2C2C7FFFFFFF5654581919CB1E1FFF1F20FD2724F7CECDF9FFFFFE898BF81C1A
        FC2220FD1E1CFE201FF9B8BAF7FFFFFE7B7BF72B2AF5E1E3F9FFFFFD5051F516
        15F99999F8FFFFFFAEAFF15055EFC0C2F8FDFFFD6B6CF31615FA6965F3EBEBFA
        5352F2736EF4EDEEF9413BF52E2E83FFFFFF5451591C1DC92324FF181BFD2222
        F4D0CDF8FFFFFE8888F81717FC1E1CFB2526F63835F28784F29391F74949F540
        43F58D8AF68D8DF43534F71215F99897F8FFFFFF9B99ED8988F4AFB0F96E69F4
        1D1DF81D1FFB1719F95959F48485F18B8AF45455F62727F8404080FFFFFF514D
        563436CA6A68FB110DF71414F6CDCAF8FFFFFD8282F60808FA1D21F4757BF11F
        1CF71512FA0E0FFA1617F91E1BFA0E12FA0D11F71A18FC1514F99594F8FFFFFF
        7473F1110CFA1513F71210F9171AFA1E1AFC191AFD0B10FA2023F73D3BF64947
        FA8583FD636184FFFFFF4B4D5A3534C8D5D5FD7676F25656F2DEDFF9FFFFFEA8
        A9F6524EF6A1A3F2B7B9F61414F81617FC1816FC1717FC1618FD1918FB1618FC
        1717FC1E1BF9AFAFF8FFFFFF7C79F01010FC1819F91817FB1A18F91818FC1917
        FD2422FA4D4EFC7677FE9390FCB1ADFC666689FFFFFF4A4B4F1B19CE6565FE76
        73F66D69F56E6CF5706FF56D6AF66F6BF47B76F94F54F41314FB1616FE1616FD
        1516FD1516FE1616FD1616FD1414FE2327F86364F47370F43D3AF01413FA1517
        FA1615FE1616FD1516FC1316FC2F32FC6C6DFCA1A4FED1D3FEC0C1FF4E4A83FF
        FFFF6A6D6406019E0607FF0608FF080AFF0706FF0506FF0709FF0809FF0807FF
        0A0AFF1413FF1512FF1513FF1512FF1513FF1413FF1512FF1513FF1111FF0A0A
        FF0505FF0F0EFF1714FF1312FF1412FF1412FF1412FF1717FF4040FE7D7AFFC0
        BEFFEAEAFF7474DF383368FFFFFFECEDE64B4B6208027F0303B40103CB0003D4
        0204D70102D90303D90202D90203D90304D80202D90203D90202D90202D90203
        D90203D90303DA0303D90203D80202D80004D80403D90203D90203D90202D902
        00D92120D66D6DDC8B8BDA908ECB5D599A25245AA8AAA9FFFFFFFFFFFFF9F9F0
        BBBAB188878968657C5C5C745858775756775756775756775756765756765756
        765756765756765756765756765958785A587958577757567657567657567657
        56765756765856775A58795756785D5E767673807B7983807F8B989998DCDDD4
        FFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFBF2FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0EFEFB3B4
        B48384846A6A696A6A696F6F6EA1A2A0CECFCDFFFFFFFFFFFFFFFFFFFFFFFFFD
        FEFD999A99555554717170575857B4B5B4FFFFFFFFFFFFFFFFFEF8F9F8828381
        3738364D4E4CBABABA878787696768767675D5D5D5FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFCFCFCF9F9F9A1A0A0000000000000303031EEEEEEFBFBFBFF
        FFFFFFFFFFFFFFFFFFFFFF7C7C7C000000868687F2F1F2515151030303A2A2A2
        FFFFFFFFFFFF9F9E9F0000000202027D7D7D656565020202000000525252FAFA
        FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCBCBCB00000000
        0000505050FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF070707020202CFCFCF
        FFFFFF9C9C9C000000212121EAEAEAFFFFFF6969690000002E2E2EFFFFFFE7E7
        E71C1C1C000000717171FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFC9C9C90202020000004E4E4EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        9595950000000C0C0CD3D3D3FFFFFFA6A6A6000000000000BDBDBDFFFFFF6565
        650000002F2F2FF6F6F6F5F5F52C2C2C0000006E6E6EFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1C1C10101010000004D4D4DFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF8282820000000D0D0DD2D2D2FFFFFFACACAC0000
        00000000B0B0B0FFFFFF6666660000002E2E2EF5F5F5F2F2F22A2A2A0000006E
        6E6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5A5A5A
        0000000000002B2B2BE9E9E9FFFFFFFFFFFFFFFFFFFFFFFFA2A2A20000000606
        06CCCCCCFFFFFFABABAB000000040404C9C9C9FFFFFF6666660000002E2E2EF5
        F5F5F2F2F22A2A2A0000006E6E6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFB8B8B80202020000002F2F2F929292787878FFFFFFFEFEFEFFFF
        FFFFFFFFE3E3E31B1B1B000000C2C2C2FFFFFF9C9C9C0000003B3B3BF6F6F6FF
        FFFF6A6A6A0000002D2D2DF5F5F5F8F8F82D2D2D0000006D6D6DFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F6393939000000000000A4A4A4FFFF
        FF666666BDBDBDFFFFFFFFFFFFFFFFFFFFFFFFAAAAAA0B0B0B5F5F5FCCCCCC3B
        3B3B1F1F1FC8C8C8FFFFFFE9E9E93C3C3C000000252525F5F5F5C7C7C70D0D0D
        000000666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9898980000
        00000000434343FAFAFAFFFFFFD7D7D75C5C5CF4F4F4FFFFFFFFFFFFFFFFFFFF
        FFFFCBCBCB7D7D7D7D7D7D898989E0E0E0FFFFFFFFFFFFD9D9D99E9E9E9C9C9C
        B0B0B0FAFAFAC0C0C09B9B9B9C9C9CC8C8C8FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFE8E8E8212121000000060606C1C1C1FFFFFFFFFFFFFFFFFF92929295
        9595FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE666666000000000000585858FFFFFFFF
        FFFFFFFFFFFFFFFFF6F6F6424242C9C9C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F896969625252523
        2323222222707070DADADAFFFFFFFFFFFFF7F7F7B6B6B63A3A3A4C4C4CCCCCCC
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF7F7F7DDDDDDE7E7E7EBEBEBEAEAEAE2E2E2EAEAEAFFFFFFFFFFFFFBFBFB
        E0E0E0E7E7E7E1E1E1E8E8E8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      OnClick = btAjudaVideoClick
    end
    object btrelatorio: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btrelatorioClick
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 350
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btopcoesClick
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '()'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btsalvarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btnovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 399
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
      Spacing = 0
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 416
    Width = 862
    Height = 53
    Align = alBottom
    TabOrder = 16
    DesignSize = (
      862
      53)
    object ImageRodape: TImage
      Left = 1
      Top = 1
      Width = 860
      Height = 51
      Align = alBottom
    end
    object lbconcluido: TLabel
      Left = 744
      Top = 14
      Width = 89
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = 'Concluir'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnClick = lbconcluidoClick
      OnMouseMove = lbconcluidoMouseMove
      OnMouseLeave = lbconcluidoMouseLeave
    end
    object lb10: TLabel
      Left = 8
      Top = 14
      Width = 184
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = 'Gerar Or'#231'amento'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnClick = lb10Click
      OnMouseMove = lbconcluidoMouseMove
      OnMouseLeave = lbconcluidoMouseLeave
    end
  end
  object edtVendedor: TEdit
    Left = 121
    Top = 123
    Width = 80
    Height = 20
    Color = 6073854
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 9
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 3
    OnDblClick = edtVendedorDblClick
    OnExit = edtVendedorExit
    OnKeyDown = edtVendedorKeyDown
    OnKeyPress = edtClienteKeyPress
  end
  object edtCliente: TEdit
    Left = 121
    Top = 98
    Width = 80
    Height = 20
    Color = 6073854
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 9
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 2
    OnDblClick = edtClienteDblClick
    OnExit = edtClienteExit
    OnKeyDown = edtClienteKeyDown
    OnKeyPress = edtClienteKeyPress
  end
  object edtDescricao: TEdit
    Left = 121
    Top = 74
    Width = 470
    Height = 20
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 100
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 1
  end
  object mmoObservacao: TMemo
    Left = 120
    Top = 317
    Width = 472
    Height = 79
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 15
  end
  object edtMedidor: TEdit
    Left = 121
    Top = 147
    Width = 80
    Height = 20
    Color = 6073854
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 9
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 4
    OnDblClick = edtMedidorDblClick
    OnExit = edtMedidorExit
    OnKeyDown = edtMedidorKeyDown
    OnKeyPress = edtClienteKeyPress
  end
  object edtdataemissao: TMaskEdit
    Left = 121
    Top = 171
    Width = 80
    Height = 20
    Ctl3D = False
    EditMask = '!99/99/9999;1;_'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 10
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 5
    Text = '  /  /    '
    OnKeyDown = edtdataemissaoKeyDown
  end
  object edtDataMedicao: TMaskEdit
    Left = 305
    Top = 171
    Width = 80
    Height = 20
    Ctl3D = False
    EditMask = '!99/99/9999;1;_'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 10
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 6
    Text = '  /  /    '
    OnKeyDown = medica
  end
  object edtendereco: TEdit
    Left = 121
    Top = 219
    Width = 470
    Height = 20
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 100
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 9
  end
  object edtcidade: TEdit
    Left = 121
    Top = 243
    Width = 470
    Height = 20
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 100
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 10
  end
  object edtuf: TEdit
    Left = 121
    Top = 291
    Width = 80
    Height = 20
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 100
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 13
  end
  object edtbairro: TEdit
    Left = 241
    Top = 267
    Width = 349
    Height = 20
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 100
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 12
  end
  object edtnumero: TEdit
    Left = 121
    Top = 267
    Width = 80
    Height = 20
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 100
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 11
  end
  object edtHorarioIni: TMaskEdit
    Left = 121
    Top = 195
    Width = 78
    Height = 20
    Ctl3D = False
    EditMask = '!90:00;1;_'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 5
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 7
    Text = '  :  '
    OnExit = edtHorarioIniExit
    OnKeyDown = edtHorarioIniKeyDown
  end
  object edtDuracao: TMaskEdit
    Left = 305
    Top = 195
    Width = 80
    Height = 20
    Ctl3D = False
    EditMask = '!90:00;1;_'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 5
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 8
    Text = '  :  '
    OnExit = edtDuracaoExit
  end
  object edtComplemento: TEdit
    Left = 241
    Top = 291
    Width = 349
    Height = 20
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 100
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 14
  end
end
