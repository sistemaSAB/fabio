unit UobjGRUPODIVERSO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UObjPlanoDeContas;

Type
   TObjGRUPODIVERSO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                PlanoDeContas  : TObjPlanoDeContas;


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure  ZerarTabela;
                Procedure  Cancelar;
                Procedure  Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Referencia(parametro: string);
                Function Get_Referencia: string;
                Procedure Submit_Nome(parametro: string);
                Function Get_Nome: string;
                Procedure Submit_PORCENTAGEMINSTALADO(Parametro:String);
                Function Get_PORCENTAGEMINSTALADO:string;
                Procedure Submit_PORCENTAGEMFORNECIDO(parametro:string);
                Function Get_Porcentagemfornecido:string;
                procedure Submit_PorcentagemRetirado(Parametro:string);
                function Get_PorcentagemRetirado:string;

                procedure EdtGrupoPlanoDeContasExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtGrupoPlanoDeContasKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                function PrimeiroRegistro: Boolean;
                function ProximoRegisto(PCodigo: Integer): Boolean;
                function RegistoAnterior(PCodigo: Integer): Boolean;
                function UltimoRegistro: Boolean;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Referencia:string;
               Nome:string;
               PORCENTAGEMINSTALADO:string;
               PORCENTAGEMFORNECIDO:string;
               PORCENTAGEMRETIRADO:string;
//CODIFICA VARIAVEIS PRIVADAS





               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios
//USES IMPLEMENTATION
;


{ TTabTitulo }


Function  TObjGRUPODIVERSO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Referencia:=fieldbyname('Referencia').asstring;
        Self.Nome:=fieldbyname('Nome').asstring;

        If(FieldByName('PlanoDeContas').asstring<>'')
        Then Begin
                 If (Self.PlanoDeContas.LocalizaCodigo(FieldByName('PlanoDeContas').asstring)=False)
                 Then Begin
                          Messagedlg('PlanoDeContas N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PlanoDeContas.TabelaparaObjeto;
        End;

        PORCENTAGEMINSTALADO:=fieldbyname('PORCENTAGEMINSTALADO').AsString;
        PORCENTAGEMFORNECIDO:=Fieldbyname('PORCENTAGEMFORNECIDO').AsString;
        PORCENTAGEMRETIRADO:=Fieldbyname('PORCENTAGEMRETIRADO').AsString;

        result:=True;
     End;
end;


Procedure TObjGRUPODIVERSO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
    ParamByName('Codigo').asstring:=Self.Codigo;
    ParamByName('Referencia').asstring:=Self.Referencia;
    ParamByName('Nome').asstring:=Self.Nome;
    ParamByName('PlanoDeContas').AsString:=Self.PlanoDeContas.Get_CODIGO;
    ParamByName('PORCENTAGEMINSTALADO').AsString:= Self.PORCENTAGEMINSTALADO;
    ParamByName('PORCENTAGEMFORNECIDO').AsString:= Self.PORCENTAGEMFORNECIDO;
    ParamByName('PORCENTAGEMRETIRADO').AsString := Self.PORCENTAGEMRETIRADO;
  End;
End;

//***********************************************************************

function TObjGRUPODIVERSO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjGRUPODIVERSO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Referencia:='';
        Nome:='';
        PlanoDeContas.ZerarTabela;        
     End;
end;

Function TObjGRUPODIVERSO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/C�digo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjGRUPODIVERSO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjGRUPODIVERSO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjGRUPODIVERSO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjGRUPODIVERSO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjGRUPODIVERSO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro GRUPODIVERSO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Referencia,Nome, PlanodeContas,PORCENTAGEMINSTALADO,PORCENTAGEMFORNECIDO,PORCENTAGEMRETIRADO');
           SQL.ADD(' from  TABGRUPODIVERSO');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjGRUPODIVERSO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjGRUPODIVERSO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjGRUPODIVERSO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        Self.PlanoDeContas:=TObjPlanodeContas.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABGRUPODIVERSO(Codigo,Referencia,Nome, PlanodeContas,PORCENTAGEMINSTALADO,PORCENTAGEMFORNECIDO,PORCENTAGEMRETIRADO)');
                InsertSQL.add('values (:Codigo,:Referencia,:Nome,:PlanodeContas,:PORCENTAGEMINSTALADO,:PORCENTAGEMFORNECIDO,:PORCENTAGEMRETIRADO)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABGRUPODIVERSO set Codigo=:Codigo,Referencia=:Referencia');
                ModifySQL.add(',Nome=:Nome,PlanodeContas=:PlanodeContas');
                ModifySQl.Add(',PORCENTAGEMINSTALADO=:PORCENTAGEMINSTALADO,PORCENTAGEMFORNECIDO=:PORCENTAGEMFORNECIDO,PORCENTAGEMRETIRADO=:PORCENTAGEMRETIRADO');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABGRUPODIVERSO where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjGRUPODIVERSO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjGRUPODIVERSO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabGRUPODIVERSO');
     Result:=Self.ParametroPesquisa;
end;

function TObjGRUPODIVERSO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de GRUPODIVERSO ';
end;


function TObjGRUPODIVERSO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENGRUPODIVERSO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjGRUPODIVERSO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.PlanoDeContas.free;
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjGRUPODIVERSO.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjGRUPODIVERSO.RetornaCampoNome: string;
begin
      result:='nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjGrupoDiverso.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjGrupoDiverso.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjGrupoDiverso.Submit_Referencia(parametro: string);
begin
        Self.Referencia:=Parametro;
end;
function TObjGrupoDiverso.Get_Referencia: string;
begin
        Result:=Self.Referencia;
end;
procedure TObjGrupoDiverso.Submit_Nome(parametro: string);
begin
        Self.Nome:=Parametro;
end;
function TObjGrupoDiverso.Get_Nome: string;
begin
        Result:=Self.Nome;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjGRUPODIVERSO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJGRUPODIVERSO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjGRUPODIVERSO.EdtGrupoPlanoDeContasExit(Sender: TObject;
  LABELNOME: TLABEL);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PlanoDeContas.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PlanoDeContas.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.PlanoDeContas.GET_NOME;

end;

procedure TObjGRUPODIVERSO.EdtGrupoPlanoDeContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.PlanoDeContas.Get_Pesquisa,Self.PlanoDeContas.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PlanoDeContas.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.PlanoDeContas.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PlanoDeContas.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;
     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjGrupoDiverso.PrimeiroRegistro: Boolean;
Var MenorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabGrupoDiverso') ;
           Open;

           if (FieldByName('MenorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MenorCodigo:=fieldbyname('MenorCodigo').AsString;
           Self.LocalizaCodigo(MenorCodigo);

           Result:=true;
     end;
end;

function TObjGrupoDiverso.UltimoRegistro: Boolean;
Var MaiorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabGrupoDiverso') ;
           Open;

           if (FieldByName('MaiorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MaiorCodigo:=fieldbyname('MaiorCodigo').AsString;
           Self.LocalizaCodigo(MaiorCodigo);

           Result:=true;
     end;
end;

function TObjGrupoDiverso.ProximoRegisto(PCodigo:Integer): Boolean;
Var MaiorValor:Integer;
begin
     Result:=false;

     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabGrupoDiverso') ;
           Open;

           MaiorValor:=fieldbyname('MaiorCodigo').AsInteger;
     end;

     if (MaiorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Inc(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MaiorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Inc(PCodigo,1);
     end;

     Result:=true;
end;

function TObjGrupoDiverso.RegistoAnterior(PCodigo: Integer): Boolean;
Var MenorValor:Integer;
begin
     Result:=false;
     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabGrupoDiverso') ;
           Open;

           MenorValor:=fieldbyname('MenorCodigo').AsInteger;
     end;

     if (MenorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Dec(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MenorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Dec(PCodigo,1);
     end;

     Result:=true;
end;


procedure TObjGRUPODIVERSO.Submit_PORCENTAGEMINSTALADO(parametro:string);
begin
  PORCENTAGEMINSTALADO:=Parametro;
end;

function TObjGRUPODIVERSO.Get_PORCENTAGEMINSTALADO:string;
begin
  Result:=Self.PORCENTAGEMINSTALADO;
end;

procedure TObjGRUPODIVERSO.Submit_PORCENTAGEMFORNECIDO(parametro:string);
begin
  Self.PORCENTAGEMFORNECIDO:=parametro;
end;

function TObjGRUPODIVERSO.Get_Porcentagemfornecido:string;
begin
  Result:=Self.PORCENTAGEMFORNECIDO;
end;

procedure TObjGRUPODIVERSO.Submit_PorcentagemRetirado(parametro:string);
begin
  Self.PORCENTAGEMRETIRADO:=Parametro;
end;

function TObjGRUPODIVERSO.Get_PorcentagemRetirado:string;
begin
  Result:=Self.PORCENTAGEMRETIRADO;
end;

end.



