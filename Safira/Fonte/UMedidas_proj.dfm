object Fmedidas_proj: TFmedidas_proj
  Left = 648
  Top = 282
  Width = 505
  Height = 351
  AutoSize = True
  BorderIcons = [biSystemMenu]
  Caption = 'Medidas do Projeto'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelComponentes: TPanel
    Left = 0
    Top = 81
    Width = 489
    Height = 65
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label5: TLabel
      Left = 5
      Top = 27
      Width = 60
      Height = 13
      Caption = 'Componente'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 157
      Top = 27
      Width = 27
      Height = 13
      Caption = 'Altura'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 261
      Top = 27
      Width = 36
      Height = 13
      Caption = 'Largura'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object EdtAlturaComponente: TEdit
      Left = 155
      Top = 40
      Width = 76
      Height = 19
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnKeyPress = edtAlturaFrontalKeyPress
    end
    object edtlarguracomponente: TEdit
      Left = 259
      Top = 40
      Width = 76
      Height = 19
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      OnKeyPress = edtAlturaFrontalKeyPress
    end
    object ComboComponente: TComboBox
      Left = 6
      Top = 4
      Width = 75
      Height = 21
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
      Text = ' '
    end
    object btcancelarcomponente: TBitBtn
      Left = 401
      Top = 34
      Width = 72
      Height = 25
      Caption = 'Cancelar'
      TabOrder = 4
      OnClick = btcancelarcomponenteClick
    end
    object edtcomponente: TEdit
      Left = 6
      Top = 39
      Width = 121
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 5
      Text = 'edtcomponente'
    end
    object edtcodigo: TEdit
      Left = 88
      Top = 4
      Width = 65
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 6
      Text = 'edtcodigo'
    end
    object btgravarcomponente: TBitBtn
      Left = 400
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Gravar'
      TabOrder = 3
      OnClick = btgravarcomponenteClick
    end
  end
  object pnlMedidasUnicasDuplas: TPanel
    Left = 0
    Top = 0
    Width = 489
    Height = 81
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object panelMedidaFrontal: TPanel
      Left = 8
      Top = 8
      Width = 233
      Height = 65
      BevelOuter = bvNone
      Caption = 'panelMedidaFrontal'
      TabOrder = 0
      object grpMedidasFrontais: TGroupBox
        Left = -5
        Top = 5
        Width = 209
        Height = 48
        Caption = 'Medidas Frontais'
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        object lb1: TLabel
          Left = 7
          Top = 12
          Width = 27
          Height = 13
          Caption = 'Altura'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lb2: TLabel
          Left = 109
          Top = 11
          Width = 36
          Height = 13
          Caption = 'Largura'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lb3: TLabel
          Left = 83
          Top = 29
          Width = 19
          Height = 13
          Caption = 'mm.'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lb4: TLabel
          Left = 185
          Top = 29
          Width = 19
          Height = 13
          Caption = 'mm.'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object edtLarguraFrontal: TEdit
          Left = 107
          Top = 24
          Width = 76
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          OnKeyPress = edtAlturaFrontalKeyPress
        end
        object edtAlturaFrontal: TEdit
          Left = 5
          Top = 24
          Width = 76
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnKeyPress = edtAlturaFrontalKeyPress
        end
      end
    end
    object panelMedidaLateral: TPanel
      Left = 248
      Top = 10
      Width = 230
      Height = 62
      BevelOuter = bvNone
      Caption = 'PanelMedidaFrontal'
      TabOrder = 1
      object grp1: TGroupBox
        Left = 0
        Top = 0
        Width = 230
        Height = 49
        Align = alTop
        Caption = 'Medidas Laterais'
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        object lb5: TLabel
          Left = 7
          Top = 12
          Width = 27
          Height = 13
          Caption = 'Altura'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lb6: TLabel
          Left = 109
          Top = 11
          Width = 36
          Height = 13
          Caption = 'Largura'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lb7: TLabel
          Left = 83
          Top = 29
          Width = 19
          Height = 13
          Caption = 'mm.'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lb8: TLabel
          Left = 185
          Top = 29
          Width = 19
          Height = 13
          Caption = 'mm.'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object edtlarguralateral: TEdit
          Left = 107
          Top = 24
          Width = 76
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          OnKeyPress = edtAlturaFrontalKeyPress
        end
        object edtalturalateral: TEdit
          Left = 5
          Top = 24
          Width = 76
          Height = 19
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnKeyPress = edtAlturaFrontalKeyPress
        end
      end
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 273
    Width = 489
    Height = 40
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object bt1: TBitBtn
      Left = 141
      Top = 9
      Width = 75
      Height = 25
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 0
      OnClick = btokClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object bt2: TBitBtn
      Left = 221
      Top = 8
      Width = 94
      Height = 26
      TabOrder = 1
      OnClick = btcancelarClick
      Kind = bkCancel
    end
  end
  object dbgridMedidas: TDBGrid
    Left = 0
    Top = 146
    Width = 489
    Height = 127
    Align = alTop
    BorderStyle = bsNone
    Ctl3D = False
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = dbgridMedidasDblClick
  end
end
