unit UConsultaRapida;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls,UobjFERRAGEMCOR, UobjVIDROCOR,
  UobjPERFILADOCOR, UobjKITBOXCOR, UobjPERSIANAGRUPODIAMETROCOR,
  UobjDIVERSOCOR, UobjSERVICO, ExtCtrls;

type
  TFConsultaRapida = class(TForm)
    lbnomeproduto: TLabel;
    label10: TLabel;
    lbNomeCor: TLabel;
    EdtCodigoCor: TEdit;
    EdtCodigoProduto: TEdit;
    PanelQuantidade: TPanel;
    lbAltura: TLabel;
    lbLArgura: TLabel;
    Label56: TLabel;
    EdtAltura: TEdit;
    EdtLargura: TEdit;
    EdtQuantidade: TEdit;
    Label3: TLabel;
    lbPreco: TLabel;
    lbPRecoTotal: TLabel;
    pnl1: TPanel;
    imgrodape: TImage;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    edtReferenciaCor: TEdit;
    lbCor: TLabel;
    lb1: TLabel;
    edtreferenciaproduto: TEdit;
    lb5: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    lbMaterial: TLabel;
    lbTipo: TLabel;
    lb10: TLabel;
    bvl1: TBevel;
    ComboMateriais: TComboBox;
    ComboTipo: TComboBox;
    lbCalcular: TLabel;
    procedure btMinimizarClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure edtreferenciaprodutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtReferenciaCorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtReferenciaCorExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtreferenciaprodutoExit(Sender: TObject);
    procedure ComboMateriaisExit(Sender: TObject);
    procedure EdtAlturaExit(Sender: TObject);
    procedure EdtLarguraExit(Sender: TObject);
    procedure btCalcularClick(Sender: TObject);
    procedure EdtQuantidadeExit(Sender: TObject);
    procedure ComboTipoExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbCalcularMouseLeave(Sender: TObject);
    procedure lbCalcularMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    ObjFerragemCor : TOBJFerragemCor;
    ObjVidroCor    : TOBJVidroCor;
    ObjPerfiladoCor: TObjPerfiladoCor;
    ObjKitBoxCor   : TObjKitBoxCor;
    ObjPersianaGrupoDiametroCor : TObjPersianaGrupoDiametroCor;
    ObjDiversoCor : TObjDiversoCor;
    ObjServico     : TObjServico;

    Procedure CalculaFerragem;
    Procedure CalculaVidro;
    procedure CalculaPerfilado;
    procedure CalculaKitBox;
    procedure CalculaDiverso;
    procedure CalculaPersianaGrupoDiametro;
    procedure CalculaServico;
    Procedure HabilitaMedidas;
    Procedure DesbilitaMedidas;
    procedure EscondeLabels;
    procedure limpalabels;



    { Private declarations }
  public
    { Public declarations }
  end;

var
  FConsultaRapida: TFConsultaRapida;

implementation

uses UessencialGlobal, UobjFERRAGEM, UobjPERSIANA,
  UDataModulo;

{$R *.dfm}

procedure TFConsultaRapida.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFConsultaRapida.btFecharClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFConsultaRapida.edtreferenciaprodutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if (Key <> VK_F9)then
    exit;

    Case ComboMateriais.ItemIndex of

           0 : Begin //Vidro
               Self.ObjVidroCor.EdtVidroDisponivelNaCorKeyDown(Sender,EdtCodigoProduto,EdtCodigoCor.Text,Key,Shift,lbnomeproduto);
           end;

           1 : Begin //Ferragem
               Self.ObjFerragemCor.EdtFerragemDisponivelNaCorKeyDown(Sender,EdtCodigoProduto,EdtCodigoCor.Text,Key,Shift,lbnomeproduto);
           end;

           2 : Begin//Perfilado
               Self.ObjPerfiladoCor.EdtPerfiladoDisponivelNaCorKeyDown(Sender,EdtCodigoProduto,EdtCodigoCor.Text,Key,Shift,lbnomeproduto);
           end;

           3 : Begin//KitBox
               Self.ObjKItBoxCor.EdtKitBoxDisponivelNaCorKeyDown(Sender,EdtCodigoProduto,EdtCodigoCor.Text,Key,Shift,lbnomeproduto);
           end;

           4 : Begin//Persiana
               Self.ObjPersianaGrupoDiametroCor.EdtPersianaGrupoDiametroDisponivelNaCorKeyDown(Sender,EdtCodigoProduto,EdtCodigoCor.Text,Key,Shift,lbnomeproduto);
           end;

           5 : Begin//Diversos
               Self.ObjDiversoCor.EdtDiversoDisponivelNaCorKeyDown(Sender,EdtCodigoProduto,EdtCodigoCor.Text,Key,Shift,lbnomeproduto);
           end;

           6 : Begin//Servi�o
               Self.ObjServico.EdtServicoKeyDown(Sender,EdtCodigoProduto,Key,Shift,lbnomeproduto);
           end;

    End;


end;

procedure TFConsultaRapida.edtReferenciaCorKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if (Key <> VK_F9)then
    exit;

    Case ComboMateriais.ItemIndex of

           0 : Begin //Vidro
                  Self.ObjVidroCor.EdtCorDeVidroKeyDown(Sender, EdtCodigoCor, Key, Shift, lbnomeCor);
           end;

           1 : Begin //Ferragem
                  Self.ObjFerragemCor.EdtCorDeFerragemKeyDown(Sender, EdtCodigoCor, Key, Shift, lbnomeCor);
           end;

           2 : Begin//Perfilado
                  Self.ObjPerfiladoCor.EdtCorDePerfiladoKeyDown(Sender, EdtCodigoCor, Key, Shift, lbnomeCor);
           end;

           3 : Begin//KitBox
                  Self.ObjKitBoxCor.EdtCorDeKitBoxKeyDown(Sender, EdtCodigoCor, Key, Shift, lbnomeCor);
           end;

           4 : Begin//Persiana
                  Self.ObjPersianaGrupoDiametroCor.EdtCorDePersianaGrupoDiametroKeyDown(Sender, EdtCodigoCor, Key, Shift, lbnomeCor);
           end;

           5 : Begin//Diversos
                  Self.ObjDiversoCor.EdtCorDeDiversoKeyDown(Sender, EdtCodigoCor, Key, Shift, lbnomeCor);
           end;

           6 : Begin//Servi�o

           end;

    End;

end;

procedure TFConsultaRapida.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
      try
          ObjFerragemCor.Free;
          ObjVidroCor.Free;
          ObjPerfiladoCor.Free;
          ObjKitBoxCor.Free;
          ObjPersianaGrupoDiametroCor.Free;
          ObjDiversoCor.Free;
          ObjServico.Free;
      except
           MensagemErro('Erro ao tentar destruir os Objetos');
      end;

      Action := caFree;
      Self := nil;

end;

procedure TFConsultaRapida.edtReferenciaCorExit(Sender: TObject);
begin
   if (EdtReferenciaCor.Text = '')then
   Begin
       EdtCodigoCor.Text:='';
       exit;
   end;


  lbNomeCor.Caption:='';
  edtreferenciaproduto.Clear;
  EdtCodigoProduto.Clear;
  lbPreco.Caption:='';

  lb6.Visible:=True;
  lbNomeCor.Visible:=True;

  // PReciso do Objeto cor entaum eu peguei o FerratemCor
  // mas podria ser qualquer um
  Self.ObjFerragemCor.Cor.ZerarTabela;


  Case ComboMateriais.ItemIndex of

           0 : Begin //Vidro
                   if (Self.ObjFerragemCor.Cor.LocalizaReferenciaCorVidro(EdtReferenciaCor.Text) = false)then
                   Begin
                        MensagemErro('Cor n�o encontrada');
                        EdtReferenciaCor.Clear;
                        exit;
                   end;
           end;

           1 : Begin //Ferragem
                   if (Self.ObjFerragemCor.Cor.LocalizaReferenciaCorFerragem(EdtReferenciaCor.Text) = false)then
                   Begin
                        MensagemErro('Cor n�o encontrada');
                        EdtReferenciaCor.Clear;
                        exit;
                   end;
           end;

           2 : Begin//Perfilado
                   if (Self.ObjFerragemCor.Cor.LocalizaReferenciaCorPerfilado(EdtReferenciaCor.Text) = false)then
                   Begin
                        MensagemErro('Cor n�o encontrada');
                        EdtReferenciaCor.Clear;
                        exit;
                   end;
           end;

           3 : Begin//KitBox
                   if (Self.ObjFerragemCor.Cor.LocalizaReferenciaCorKitBox(EdtReferenciaCor.Text) = false)then
                   Begin
                        MensagemErro('Cor n�o encontrada');
                        EdtReferenciaCor.Clear;
                        exit;
                   end;

           end;

           4 : Begin//Persiana
                   if (Self.ObjFerragemCor.Cor.LocalizaReferenciaCorPersiana(EdtReferenciaCor.Text) = false)then
                   Begin
                        MensagemErro('Cor n�o encontrada');
                        EdtReferenciaCor.Clear;
                        exit;
                   end;
           end;

           5 : Begin//Diversos
                   if (Self.ObjFerragemCor.Cor.LocalizaReferenciaCorDiverso(EdtReferenciaCor.Text) = false)then
                   Begin
                        MensagemErro('Cor n�o encontrada');
                        EdtReferenciaCor.Clear;
                        exit;
                   end;
           end;

           6 : Begin//Servi�o
                    Self.CalculaServico;
           end;
     End;


     Self.ObjFerragemCor.Cor.TabelaparaObjeto;
     EdtCodigoCor.Text:=Self.ObjFerragemCor.Cor.Get_Codigo;
     lbNomeCor.Caption:=Self.ObjFerragemCor.Cor.Get_Descricao;


end;

procedure TFConsultaRapida.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);

   If (KEY=#27)
   then Self.Close;
end;

procedure TFConsultaRapida.edtreferenciaprodutoExit(Sender: TObject);
begin
     if (edtreferenciaproduto.Text = '')then
     Begin
         EdtCodigoProduto.Text:='';
         exit;
     end;
     lb5.Visible:=True;
     lbnomeproduto.Visible:=True;
     label10.Visible:=true;
     lbPreco.Visible:=True;
     

     Case ComboMateriais.ItemIndex of

           0 : Begin //Vidro
                    Self.CalculaVidro;
           end;

           1 : Begin //Ferragem
                    Self.CalculaFerragem;
           end;

           2 : Begin//Perfilado
                    Self.CalculaPerfilado;
           end;

           3 : Begin//KitBox
                    Self.CalculaKitBox;
           end;

           4 : Begin//Persiana
                    Self.CalculaPersianaGrupoDiametro;
           end;

           5 : Begin//Diversos
                    Self.CalculaDiverso;
           end;

           6 : Begin//Servi�o
                    Self.CalculaServico;
           end;
     End;

     EdtQuantidade.Text:='1';

end;

procedure TFConsultaRapida.CalculaFerragem;
Var PValor, PValorVenda:Currency;
begin
       With Self.ObjFerragemCor do
      Begin
           ZerarTabela;
           if (LocalizaCodigo(EdtCodigoProduto.Text)=false)then
           Begin
                MensagemErro('Ferragem na cor especificada n�o encontrada.');
                exit;
           end;
           TabelaparaObjeto;
           lbnomeproduto.Caption:=Ferragem.Get_Descricao;
           PValor:=0;


           if (UpperCase(ComboTipo.Text) = 'INSTALADO')then
           Begin
              PValor := StrToCurr(Ferragem.Get_PrecoVendaInstalado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

           if (UpperCase(ComboTipo.Text) = 'FORNECIDO')then
           Begin
              PValor := StrToCurr(Ferragem.Get_PrecoVendaFornecido);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

           if (UpperCase(ComboTipo.Text) = 'RETIRADO')then
           Begin
              PValor := StrToCurr(Ferragem.Get_PrecoVendaRetirado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;                                                   

      end;
end;

procedure TFConsultaRapida.ComboMateriaisExit(Sender: TObject);
begin
     EscondeLabels;
     limpalabels;
     Case ComboMateriais.ItemIndex of

           0 : Begin //Vidro
                    HabilitaMedidas;
           end;

           1 : Begin //Ferragem
                    DesbilitaMedidas;
           end;

           2 : Begin//Perfilado
                    DesbilitaMedidas;
           end;

           3 : Begin//KitBox
                    DesbilitaMedidas;
           end;

           4 : Begin//Persiana
                    HabilitaMedidas;
           end;

           5 : Begin//Diversos
                    HabilitaMedidas;
           end;

           6 : Begin//Servi�o
                    DesbilitaMedidas;
           end;

     End;

     if (ComboMateriais.ItemIndex = 6) //Servico naum tem cor
     then begin
          EdtReferenciaCor.Visible:=false;
          lbNomeCor.Visible:=false;
          lbCor.Visible:=false;
          edtreferenciaproduto.SetFocus;
     end else
     begin
          EdtReferenciaCor.Visible:=true;
          //lbNomeCor.Visible:=true;
          //lbCor.Visible:=true;
          //EdtReferenciaCor.SetFocus;
     end;
     lbMaterial.Visible:=True;
     lb7.Visible:=True;
     lbMaterial.Caption:=ComboMateriais.Text;

end;

procedure TFConsultaRapida.CalculaVidro;
Var PValor, PValorVenda:Currency;
begin
      With Self.ObjVidroCor do
      Begin
           ZerarTabela;
           if (LocalizaCodigo(EdtCodigoProduto.Text)=false)then
           Begin
                MensagemErro('Vidro na cor especificada n�o encontrada.');
                exit;
           end;
           TabelaparaObjeto;
           lbnomeproduto.Caption:=Vidro.Get_Descricao;
           PValor:=0;


           if (UpperCase(ComboTipo.Text) = 'INSTALADO')then
           Begin
              PValor := StrToCurr(Vidro.Get_PrecoVendaInstalado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

           if (UpperCase(ComboTipo.Text) = 'FORNECIDO')then
           Begin
              PValor := StrToCurr(Vidro.Get_PrecoVendaFornecido);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

           if (UpperCase(ComboTipo.Text) = 'RETIRADO')then
           Begin
              PValor := StrToCurr(Vidro.Get_PrecoVendaRetirado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

      end;
end;


procedure TFConsultaRapida.CalculaPerfilado;
Var PValor, PValorVenda:Currency;
begin
      With Self.ObjPerfiladoCor do
      Begin
           ZerarTabela;
           if (LocalizaCodigo(EdtCodigoProduto.Text)=false)then
           Begin
                MensagemErro('Perfilado na cor especificada n�o encontrada.');
                exit;
           end;
           TabelaparaObjeto;
           lbnomeproduto.Caption:=Perfilado.Get_Descricao;
           PValor:=0;


           if (UpperCase(ComboTipo.Text) = 'INSTALADO')then
           Begin
              PValor := StrToCurr(Perfilado.Get_PrecoVendaInstalado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

           if (UpperCase(ComboTipo.Text) = 'FORNECIDO')then
           Begin
              PValor := StrToCurr(Perfilado.Get_PrecoVendaFornecido);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

           if (UpperCase(ComboTipo.Text) = 'RETIRADO')then
           Begin
              PValor := StrToCurr(Perfilado.Get_PrecoVendaRetirado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

      end;
end;

procedure TFConsultaRapida.CalculaKitBox;
Var PValor, PValorVenda:Currency;
begin
      With Self.ObjKitBoxCor do
      Begin
           ZerarTabela;
           if (LocalizaCodigo(EdtCodigoProduto.Text)=false)then
           Begin
                MensagemErro('KitBox na cor especificada n�o encontrada.');
                exit;
           end;
           TabelaparaObjeto;
           lbnomeproduto.Caption:=KitBox.Get_Descricao;
           PValor:=0;


           if (UpperCase(ComboTipo.Text) = 'INSTALADO')then
           Begin
              PValor := StrToCurr(KitBox.Get_PrecoVendaInstalado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

           if (UpperCase(ComboTipo.Text) = 'FORNECIDO')then
           Begin
              PValor := StrToCurr(KitBox.Get_PrecoVendaFornecido);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

           if (UpperCase(ComboTipo.Text) = 'RETIRADO')then
           Begin
              PValor := StrToCurr(KitBox.Get_PrecoVendaRetirado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

      end;
end;

procedure TFConsultaRapida.CalculaDiverso;
Var PValor, PValorVenda:Currency;
begin
      With Self.ObjDiversoCor do
      Begin
           ZerarTabela;
           if (LocalizaCodigo(EdtCodigoProduto.Text)=false)then
           Begin
                MensagemErro('Diverso na cor especificada n�o encontrada.');
                exit;
           end;
           TabelaparaObjeto;
           lbnomeproduto.Caption:=Diverso.Get_Descricao;
           PValor:=0;


           if (UpperCase(ComboTipo.Text) = 'INSTALADO')then
           Begin
              PValor := StrToCurr(Diverso.Get_PrecoVendaInstalado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

           if (UpperCase(ComboTipo.Text) = 'FORNECIDO')then
           Begin
              PValor := StrToCurr(Diverso.Get_PrecoVendaFornecido);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

           if (UpperCase(ComboTipo.Text) = 'RETIRADO')then
           Begin
              PValor := StrToCurr(Diverso.Get_PrecoVendaRetirado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              lbPreco.Caption:=Formata_VAlor(PValorVenda);
           end;

      end;
end;

procedure TFConsultaRapida.CalculaPersianaGrupoDiametro;
Var PValor, PValorVenda:Currency;
begin
      With Self.ObjPersianaGrupoDiametroCor do
      Begin
           ZerarTabela;
           if (LocalizaCodigo(EdtCodigoProduto.Text)=false)then
           Begin
                MensagemErro('PersianaGrupoDiametro na cor especificada n�o encontrada.');
                exit;
           end;
           TabelaparaObjeto;
           lbnomeproduto.Caption:='Produto  :'+Persiana.Get_Nome+#13+
                                  'Grupo    :'+GrupoPersiana.Get_Nome+#13+
                                  'Diametro :'+Diametro.Get_Codigo;
           PValor:=0;


           if (UpperCase(ComboTipo.Text) = 'INSTALADO')then
           Begin
              lbPreco.Caption:=Formata_VAlor(Get_PrecoVendaInstalado);
           end;

           if (UpperCase(ComboTipo.Text) = 'FORNECIDO')then
           Begin
              lbPreco.Caption:=Formata_VAlor(Get_PrecoVendaFornecido);
           end;

           if (UpperCase(ComboTipo.Text) = 'RETIRADO')then
           Begin
              lbPreco.Caption:=Formata_VAlor(Get_PrecoVendaRetirado);
           end;

      end;
end;


procedure TFConsultaRapida.CalculaServico;
Var PValor, PValorVenda:Currency;
begin
      With Self.ObjServico do
      Begin
           ZerarTabela;
           if (LocalizaCodigo(EdtCodigoProduto.Text)=false)then
           Begin
                MensagemErro('Servico na cor especificada n�o encontrada.');
                exit;
           end;
           TabelaparaObjeto;
           lbnomeproduto.Caption:=Get_Descricao;
           PValor:=0;


           if (UpperCase(ComboTipo.Text) = 'INSTALADO')then
           Begin
              lbPreco.Caption:=Formata_VAlor(Get_PrecoVendaInstalado);
           end;

           if (UpperCase(ComboTipo.Text) = 'FORNECIDO')then
           Begin
              lbPreco.Caption:=Formata_VAlor(Get_PrecoVendaFornecido);
           end;

           if (UpperCase(ComboTipo.Text) = 'RETIRADO')then
           Begin
              lbPreco.Caption:=Formata_VAlor(Get_PrecoVendaRetirado);
           end;

      end;
end;


procedure TFConsultaRapida.EdtAlturaExit(Sender: TObject);
begin
     if (edtAltura.Text='') or (edtLargura.Text='')then
     exit;

     

     EdtQuantidade.Text:=CurrToStr((StrToCurr(tira_ponto(edtAltura.Text)) *
                                              StrToCurr(tira_ponto(edtLargura.Text)))/1000000);

end;

procedure TFConsultaRapida.EdtLarguraExit(Sender: TObject);
begin
     if (edtAltura.Text='') or (edtLargura.Text='')then
     exit;

     EdtQuantidade.Text:=CurrToStr((StrToCurr(tira_ponto(edtAltura.Text)) *
                                              StrToCurr(tira_ponto(edtLargura.Text)))/1000000);

end;

procedure TFConsultaRapida.btCalcularClick(Sender: TObject);
Var Valor, Quantidade : Currency;
begin

    try
         Valor:=StrToCurr(tira_ponto(lbPreco.Caption));
         Quantidade:=StrToCurr(tira_ponto(EdtQuantidade.Text));
         lbPRecoTotal.Caption:=formata_valor(CurrToStr(Valor*Quantidade));
    except
         MensagemErro('Erro ao tentar calcular a Quantidade pelo valor do produto ');
    end;

end;

procedure TFConsultaRapida.EdtQuantidadeExit(Sender: TObject);
begin
    Label3.Visible:=True;
    lbPRecoTotal.Visible:=True;
    btCalcularClick(Sender);

end;

procedure TFConsultaRapida.DesbilitaMedidas;
begin
    lbAltura.Visible:=false;
    lbLArgura.Visible:=false;
    EdtAltura.Visible:=false;
    EdtLargura.Visible:=false;
end;

procedure TFConsultaRapida.HabilitaMedidas;
begin
    lbAltura.Visible:=true;
    lbLArgura.Visible:=true;
    EdtAltura.Visible:=true;
    EdtLargura.Visible:=true;
end;

procedure TFConsultaRapida.EscondeLabels;
begin
     lbMaterial.Visible:=False;
     lb7.Visible:=False;
     lbnomeproduto.Visible:=False;
     lbNomeCor.Visible:=False;
     lbPreco.Visible:=False;
     lbPRecoTotal.Visible:=False;
     lb10.Visible:=False;
     lb7.Visible:=False;
     lb5.Visible:=False;
     lb6.Visible:=False;
     label10.Visible:=False;
     Label3.Visible:=False;
     lbTipo.Visible:=False;
end;


procedure TFConsultaRapida.ComboTipoExit(Sender: TObject);
begin
     lb10.Visible:=True;
     lbTipo.Caption:=ComboTipo.Text;
     lbTipo.Visible:=True;
     
end;

procedure TFConsultaRapida.limpalabels;
begin
       EdtCodigoCor.Clear;
       EdtCodigoProduto.Clear;
       EdtLargura.Clear;
       EdtQuantidade.Clear;
       edtReferenciaCor.Clear;
       edtreferenciaproduto.Clear;
       EdtAltura.Clear;
       lbPreco.Caption:='';
       lbPRecoTotal.Caption:='';

end;

procedure TFConsultaRapida.FormShow(Sender: TObject);
begin
      try
          ObjFerragemCor              := TOBJFerragemCor.Create;
          ObjVidroCor                 := TOBJVidroCor.Create;
          ObjPerfiladoCor             := TObjPerfiladoCor.Create;
          ObjKitBoxCor                := TObjKitBoxCor.Create;
          ObjPersianaGrupoDiametroCor := TObjPersianaGrupoDiametroCor.Create;
          ObjDiversoCor               := TObjDiversoCor.Create;
          ObjServico                  := TObjServico.Create;
      except
          MensagemErro('Erro ao tentar criar os Objetos.');
          exit;
      end;
      ComboMateriais.ItemIndex:=0;
      ComboTipo.ItemIndex:=0;

      habilita_botoes(Self);
      if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE CONSULTA RAPIDA')=False)
      Then desab_botoes(Self);

      EscondeLabels;
      lb2.Caption:='';
end;

procedure TFConsultaRapida.lbCalcularMouseLeave(Sender: TObject);
begin
     TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFConsultaRapida.lbCalcularMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
       TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

end.
