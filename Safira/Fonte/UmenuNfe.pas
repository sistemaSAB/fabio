unit UmenuNfe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,db,IBQuery,UobjNFE,
  OleCtrls, SHDocVw,UnotaFiscalEletronica,UobjNotaFiscalObjetos,UobjTransmiteNFE;

type
  TFmenuNfe = class(TForm)
    btn2: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    OpenDialog1: TOpenDialog;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton4: TSpeedButton;
    btRecuperaProtocolo: TSpeedButton;
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure btRecuperaProtocoloClick(Sender: TObject);

  private

    formnfe:TFnotaFiscalEletronica;

    
  public
    ObjNfObjetos:TObjNotafiscalObjetos;
    objTransmitenfe :TObjTransmiteNFE;
    
    function PassaObjeto(Parametro: TObjNotafiscalObjetos): boolean;
    procedure passaFormulario(fnfe:TFnotaFiscalEletronica);
    procedure ImprimeNFeArquivo(Parquivo: string);
    Procedure CancelaNfe(Parquivo:String);
    procedure inutiliza_faixa_na_tabNotaFiscal (pnf:string);

    function get_pesquisa_NFE ():string;
    function CANCELA_NF_POR_NFE (pcodigoNFE:string):boolean;
    procedure cancelaPelaChave (pChave,pProtocolo,pCodigo:string);

  end;

var
  FmenuNfe: TFmenuNfe;

implementation

uses UNFE, UessencialGlobal, UComponentesNfe, ACBrNFe,
  ACBrNFeConfiguracoes, UobjEMPRESA, UDataModulo, UFiltraImp,
  UobjNOTAFISCALCFOP,pcnLeitor,pcnConversao, uobjgeranfe,
  ACBrNFeWebServices, Upesquisa;

function TFmenuNFE.PassaObjeto(Parametro: TObjNotafiscalObjetos): boolean;
begin
  result:=False;
  Self.ObjNfObjetos:=Parametro;
  result:=True;
end;



{$R *.dfm}

procedure TFmenuNfe.BitBtn5Click(Sender: TObject);
var
  Pnfe:String;
  PsemNf:boolean;
  certificado:string;
  //caminhoArquivoXML:string;
  //reciboEnvio:string;
  chaveAcesso:string;
  protocolo:string;
begin

  FComponentesNfe.ACBrNFe.NotasFiscais.Clear;

  Self.ObjNfObjetos.objgeranfe.ConfiguraComponente;

  FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie:=ObjEmpresaGlobal.get_certificado;

  OpenDialog1.Title := 'Selecione a NFE';
  OpenDialog1.DefaultExt := '*-nfe.XML';
  OpenDialog1.Filter := 'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|Todos os Arquivos (*.*)|*.*';

  //OpenDialog1.InitialDir := FcomponentesNfe.ACBrNFe.Configuracoes.Geral.PathSalvar;
  OpenDialog1.InitialDir := ObjNfObjetos.objgeranfe.Nfe.GET_CAMINHO_DOS_ARQUIVOS_XML();

  if not (OpenDialog1.Execute)
  then exit;

  if (Self.ObjNfObjetos.objgeranfe.EscolherNota(Pnfe)=False) then
  begin

    MensagemErro('N�o foi possivel escolher a nota fiscal');
    exit;

  end;


  if (Self.ObjNfObjetos.objgeranfe.Nfe.LocalizaCodigo(Pnfe)=False) Then
  Begin

    MessageDlg('Nota Fiscal n�o localizada',mterror,[mbok],0);
    exit;

  End;

  Self.ObjNfObjetos.ObjgeraNfe.Nfe.TabelaparaObjeto;

  if (Self.ObjNfObjetos.ObjgeraNfe.Nfe.Get_STATUSNOTA <> 'A') Then
  Begin

    MensagemAviso('A NFe escolhida n�o se encontra Aberta para uso. Status Atual '+Self.ObjNfObjetos.ObjgeraNfe.Nfe.Get_StatusNota);
    exit;

  End;


  With FfiltroImp do
  Begin
  
       DesativaGrupos;
       Grupo01.Enabled:=true;
       LbGrupo01.Caption:='Nota Fiscal';

       edtgrupo01.OnKeyDown:=Self.ObjNfObjetos.ObjNotaFiscalCfop.EdtNotaFiscal_naoUsada_KeyDown;

       ShowModal;

       if (tag=0)
       then exit;

       PsemNf:=False;

       if (edtgrupo01.Text='')
       Then Begin

                 if (MensagemPergunta('Certeza que deseja importar o XML e n�o vincular a Nfe a nenhuma NF?')=mrno)
                 Then exit;

                 PsemNf:=True;
       End
       Else BEgin
       
               if (Self.ObjNfObjetos.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(edtgrupo01.Text)=False)
               then Begin
                         mensagemErro('NF n�o localizada');
                         exit;
               End;

               Self.ObjNfObjetos.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;

               if (Self.ObjNfObjetos.ObjNotaFiscalCfop.NOTAFISCAL.nfe.Get_CODIGO<>'')
               then Begin
                     mensagemErro('Esta NF j� possui uma NFE, n�o � poss�vel adicionar outra NFe');
                     exit;
               End;
       End;
  End;

  Try


      with FfiltroImp do
      begin

        DesativaGrupos;

        Grupo15.Enabled:=True;
        LbGrupo15.Caption:='Selecione o status';

        ComboGrupo15.Clear;
        ComboGrupo15.Items.add ('');
        ComboGrupo15.Items.add ('G - Gerada');
        ComboGrupo15.Items.add ('C - Cancelada');
        ComboGrupo15.Items.add ('Z - Inutilizada');
        ComboGrupo15.ItemIndex := 0;


        showmodal;

        if (tag=0) then
          exit;

        if (ComboGrupo15.Text = '') then
        begin

          MensagemAviso ('Escolha um status');
          Exit;

        end;

        chaveAcesso := RetornaValorCampos(OpenDialog1.FileName,'chNFe');
        certificado := ObjEmpresaGlobal.get_certificado;
        protocolo   := RetornaValorCampos(OpenDialog1.FileName,'nProt');

        Self.ObjNfObjetos.objgeranfe.Nfe.Status:=dsedit;
        Self.ObjNfObjetos.ObjgeraNfe.Nfe.Submit_STATUSNOTA (ComboGrupo15.Text[1]);
        Self.ObjNfObjetos.ObjgeraNfe.Nfe.Submit_ARQUIVO(OpenDialog1.FileName);
        Self.ObjNfObjetos.ObjgeraNfe.Nfe.submit_chave_acesso (chaveAcesso);
        Self.ObjNfObjetos.ObjgeraNfe.Nfe.Submit_CERTIFICADO (certificado);
        Self.ObjNfObjetos.ObjgeraNfe.Nfe.submit_arquivo_xml(Self.ObjNfObjetos.ObjgeraNfe.Nfe.GET_CAMINHO_DOS_ARQUIVOS_XML () + OpenDialog1.FileName);

        if (ComboGrupo15.Text[1] = 'C') then
          Self.ObjNfObjetos.ObjgeraNfe.Nfe.Submit_PROTOCOLOCANCELAMENTO (protocolo)

        else if (ComboGrupo15.Text[1] = 'Z') then
          Self.ObjNfObjetos.ObjgeraNfe.Nfe.Submit_PROTOCOLOinutilizacao (protocolo);


      end;



      if (Self.ObjNfObjetos.ObjgeraNfe.Nfe.Salvar(True)=False)
      Then MensagemErro ('N�o foi possivel salvar as altera��es na NF-e');

      //MensagemErro('N�o foi poss�vel alterar o status  para "GERADA" na nota ID '+pnfe+' Cancele a Nfe manualmente ou altere manualmente este status para GERADA');

      if (PsemNf=False)
      Then Begin

              Self.ObjNfObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsedit;

              if (FfiltroImp.ComboGrupo15.Text[1] = 'G') then
                Self.ObjNfObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao('I')
              else
                Self.ObjNfObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Submit_Situacao(FfiltroImp.ComboGrupo15.Text[1]);



              Self.ObjNfObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Submit_codigo(Pnfe);
              Self.ObjNfObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Submit_OBSERVACOES(Self.ObjNfObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Get_OBSERVACOES+#13+'XML IMPORTADO');
                
              if (Self.ObjNfObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Salvar(true)=False)
              then Begin
                            MensagemErro('Erro na tentativa de Salvar a Situa��o para Impressa na Nota Fiscal do amanda, altere manualmente');
                            exit;
              End;
      End;

      mensagemaviso('Conclu�do');
  Finally
         FDataModulo.IBTransaction.RollbackRetaining;
  End;

end;

procedure TFmenuNfe.BitBtn2Click(Sender: TObject);
begin
  
  OpenDialog1.Title := 'Selecione a NFE';
  OpenDialog1.DefaultExt := '*-nfe.XML';

  OpenDialog1.Filter := 'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|Todos os Arquivos (*.*)|*.*';

  if OpenDialog1.Execute then
    objTransmitenfe.imprimeDanfe(OpenDialog1.FileName);

  {OpenDialog1.Title := 'Selecione a NFE';
  OpenDialog1.DefaultExt := '*-nfe.XML';

  OpenDialog1.Filter := 'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|Todos os Arquivos (*.*)|*.*';

  Self.ObjNfObjetos.objgeranfe.ConfiguraComponente;

  //OpenDialog1.InitialDir := FcomponentesNfe.ACBrNFe.Configuracoes.Geral.PathSalvar;
  OpenDialog1.InitialDir := ObjNfObjetos.objgeranfe.Nfe.GET_CAMINHO_DOS_ARQUIVOS_XML();

  if OpenDialog1.Execute
  then Self.ImprimeNFeArquivo(OpenDialog1.FileName);
  }


end;

procedure TFmenuNfe.ImprimeNFeArquivo(Parquivo: string);
var
   wnProt: TLeitor;
Begin

  try

    FComponentesNfe.ACBrNFe.NotasFiscais.Clear;

    Self.ObjNfObjetos.objgeranfe.ConfiguraComponente;
    FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie:=ObjEmpresaGlobal.Get_certificado;

    FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(Parquivo);

    wnProt:=TLeitor.Create;
    wnProt.CarregarArquivo(Parquivo);
    wnProt.Grupo:=wnProt.Arquivo;
    FcomponentesNfe.ACBrNFe.DANFE.ProtocoloNFe:=wnProt.rCampo(tcStr,'nProt');
    FcomponentesNfe.ACBrNFe.NotasFiscais.Imprimir;

  finally

    wnProt.Free;

  end;


end;

procedure TFmenuNfe.BitBtn3Click(Sender: TObject);
begin
 objTransmitenfe.consultaXML;
end;

procedure TFmenuNfe.btn2Click(Sender: TObject);
begin

     Self.ObjNfObjetos.objgeranfe.ConfiguraComponente;

     FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie:=ObjEmpresaGlobal.get_certificado;

     OpenDialog1.Title := 'Selecione a NFE';
     OpenDialog1.DefaultExt := '*-nfe.XML';
     OpenDialog1.Filter := 'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|Todos os Arquivos (*.*)|*.*';

     //OpenDialog1.InitialDir := Self.ObjNfObjetos.objgeranfe.LocalArquivos;
     OpenDialog1.InitialDir := ObjNfObjetos.objgeranfe.Nfe.GET_CAMINHO_DOS_ARQUIVOS_XML();

     if (OpenDialog1.Execute) then
        Self.CancelaNfe(OpenDialog1.FileName);


end;

procedure TFmenuNfe.CancelaNfe(Parquivo: String);
var
  PArquivoCancela,PNFSistema,pcodigo,vAux : String;
  xmlCancelamento:TStringList;
begin
  PNFSistema:='';
  pcodigo:='';    //cancelamento pelo objeto

  if not(InputQuery('WebServices Cancelamento', 'Justificativa do cancelamento', vAux)) then
  begin
    self.Tag := -1;
    exit;
  end;

  if (Length(vaux) < 15) then
  begin
    MensagemErro ('A justificativa deve ser maior ou igual a 15 caracteres');
    self.Tag := -1;
    Exit;
  end;

  if not objTransmitenfe.cancelaNFe(Parquivo,vAux) then
  begin
    MensagemErro('NF-e n�o cancelada');
    Exit;
  end;

  Pcodigo:=inttostr(FcomponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.cNF);

  //Deu Certo
  //Ent�o tenho que guardar os protocolos de cancelamento e o arquivo de cancelamento

  //FcomponentesNfe.MemoResp.Text :=  UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.EnvEvento.RetWS);
  //FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);

  //PArquivoCancela := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathCan + StringReplace(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.chNFe,'Nfe','',[rfIgnoreCase])+'-can.xml'; celio0712
  PArquivoCancela := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathEvento + StringReplace(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.chNFe,'Nfe','',[rfIgnoreCase])+'-can.xml';
  FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.EnvEvento.RetWS);
  FComponentesNfe.MemoResp.Lines.SaveToFile(PArquivoCancela);

  Try

    {agora preciso localizar a NF e cancelar na tabnotafiscal. OBS: o codigo da NF-e � o numero nda NF
    ent�o localizo tabnotafiscal aonde nf.numero = codigoNF-e e nf.modelo = 2 (NF-e)}


    if not (CANCELA_NF_POR_NFE(pcodigo)) then
      MensagemAviso('N�o foi possivel alterar o status para ''C'' na tabnotaFiscal. Deixe o erro na tela e informe o suporte');

    //Localizando na TABNFE
    if (Self.ObjNfObjetos.objgeranfe.NFE.LocalizaCodigo(pcodigo)=True)
    then Begin
      Self.ObjNfObjetos.objgeranfe.NFE.TabelaparaObjeto;
      Self.ObjNfObjetos.objgeranfe.NFE.Status:= dsedit;
      Self.ObjNfObjetos.objgeranfe.NFE.Submit_StatusNota('C');//cancelada
      Self.ObjNfObjetos.objgeranfe.NFE.Submit_ARQUIVOCANCELAMENTO(PArquivoCancela);
      Self.ObjNfObjetos.objgeranfe.NFE.Submit_PROTOCOLOCANCELAMENTO(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.nProt);
      Self.ObjNfObjetos.objgeranfe.NFE.Submit_DataCancelamento(datetostr(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.dhRegEvento));
      Self.ObjNfObjetos.objgeranfe.NFE.Submit_MotivoCancelamento(vaux);

      if (Self.ObjNfObjetos.objgeranfe.NFE.Salvar(false)=False)
      then MensagemErro('Erro na tentativa de Alterar o Status da NFe para Cancelada e guardar o protocolo');
    End;

    FDataModulo.IBTransaction.CommitRetaining;
    MensagemSucesso ('Processo concluido');

  Finally
    FDataModulo.IBTransaction.RollbackRetaining;
  End;
     
end;

procedure TFmenuNfe.BitBtn1Click(Sender: TObject);
var
  CNPJ:string;
  pprotocolo,Modelo, Serie, Ano, NumeroInicial, NumeroFinal, Justificativa : String;
  cont:integer;

  {gambiarra}
   a:TShiftState;
   b:Word;
  {end gambiarra. Nem so de leite vive a vaca, mais nunca viverei sem a carne de uma perereca }

begin


     With FfiltroImp do
     Begin

          DesativaGrupos;
          Grupo01.Enabled:=True;
          LbGrupo01.caption:='Ano';

          //Grupo02.Enabled:=True;
          //LbGrupo02.caption:='Modelo';

          //Grupo03.Enabled:=True;
          //LbGrupo03.caption:='S�rie';

          Grupo04.Enabled:=True;
          LbGrupo04.caption:='N�mero Inicial';
          edtgrupo04.OnKeyDown :=  Self.ObjNfObjetos.objgeranfe.Nfe.edtNfeKeyDown;
           


          Grupo05.Enabled:=True;
          LbGrupo05.caption:='N�mero Final';
          edtgrupo05.OnKeyDown := Self.ObjNfObjetos.objgeranfe.Nfe.edtNfeKeyDown;
          showmodal;

          if (tag=0)
          then exit;

          Ano:=edtgrupo01.Text;
          //modelo:=edtgrupo02.Text;
          //serie:=edtgrupo03.Text;
          modelo:='55';
          serie:='1';

          Try
            strtoint(edtgrupo04.Text);
            numeroinicial:=edtgrupo04.Text;
          Except
                Mensagemerro('N�mero Inicial Inv�lido');
                exit;
          End;
          
          Try
            strtoint(edtgrupo05.text);
            NumeroFinal:=edtgrupo05.Text;
          Except
               Mensagemerro('N�mero Final Inv�lido');
               exit;
          End;

          if (Strtoint(numeroinicial)>strtoint(numerofinal))
          Then Begin
                    MensagemErro('O N�mero inicial n�o pode ser maior que o n�mero final');
                    exit;
          End;

     End;
     
     if not(InputQuery('WebServices Inutiliza��o ', 'Justificativa', Justificativa))
     then exit;

     if (Length (Justificativa) < 15) then
     begin

        MensagemErro ('A justificativa deve ser maior ou igual a 15 caracteres');
        Exit;


     end;

      Try

          FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
          Self.ObjNfObjetos.objgeranfe.ConfiguraComponente;
          FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie:=ObjEmpresaGlobal.get_certificado;
          FcomponentesNfe.ACBrNFe.NotasFiscais.Clear;

          CNPJ := Trim(come(come(come(ObjEmpresaGlobal.Get_CNPJ,'.'),'/'),'-'));

          FComponentesNfe.ACBrNFe.WebServices.Inutiliza(CNPJ,Justificativa, StrToInt(Ano), StrToInt(Modelo), StrToInt(Serie), StrToInt(NumeroInicial), StrToInt(NumeroFinal));

          pprotocolo:=FcomponentesNfe.ACBrNFe.WebServices.Inutilizacao.Protocolo;


          FcomponentesNfe.MemoResp.Text :=  UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.Inutilizacao.RetWS);
          FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);
      Except
          on e:exception do
          Begin
               Mensagemerro('Erro na tentativa de inutilizar a sequ�ncia '+#13+E.message);
               exit;
          End;

      End;

      Try

        DeleteFile(RetornaPastaSistema+'ARQUIVOINU_E.XML');

      Except
      
      End;

      FComponentesNfe.MemoResp.Lines.clear;
      FComponentesNfe.MemoResp.Lines.text:=FcomponentesNfe.ACBrNFe.WebServices.Inutilizacao.DadosMsg;
      FComponentesNfe.MemoResp.Lines.SaveToFile(RetornaPastaSistema+'ARQUIVOINU_E.XML');
      //FComponentesNfe.showmodal;

      Try
         deletefile(RetornaPastaSistema+'ARQUIVOINU_R.XML');
      Except
      End;
      FComponentesNfe.MemoResp.Lines.clear;
      FComponentesNfe.MemoResp.Lines.text:=FcomponentesNfe.ACBrNFe.WebServices.Inutilizacao.RetWS;
      FComponentesNfe.MemoResp.Lines.SaveToFile(RetornaPastaSistema+'ARQUIVOINU_R.XML');

      try

        FComponentesNfe.MemoResp.Lines.SaveToFile (ObjNfObjetos.objgeranfe.Nfe.GET_CAMINHO_DOS_ARQUIVOS_INUTILIZACAO()+'ARQUIVOINU_R.XML');
        
      except

        on e:Exception do
        begin
          MensagemErro ('N�o foi possivel salvar arquivo de inutiliza��o. '+e.Message);
        end;

      end;

      //FComponentesNfe.showmodal;

      for cont:=strtoint(numeroinicial) to strtoint(numerofinal) do
      Begin
           //percorrendo NF a NF para guardar data, protocolo, motivo e o arquivo de cancelamento
           With Self.ObjNfObjetos.objgeranfe.Nfe do
           Begin

               ZerarTabela;

               if (LocalizaCodigo(inttostr(cont))=False)
               Then Begin
                         //n�o existe essa Nf na Base de dados do sistema
                         //vou criar e cancelar
                         Status:=dsinsert;
                         Submit_CODIGO(inttostr(cont));
                         Submit_CERTIFICADO(ObjEmpresaGlobal.Get_certificado);
                         Submit_ARQUIVO('');
                         Submit_PROTOCOLOCANCELAMENTO('');
                         Submit_ReciboEnvio('');
                         Submit_ARQUIVOCANCELAMENTO('');
                         Submit_DataCancelamento('');
                         Submit_MotivoCancelamento('');
               End
               Else Begin
                         TabelaparaObjeto;
                         Status:=dsEdit;
               End;
               Submit_STATUSNOTA('Z');//inutilizada
               Submit_Datainutilizacao(datetostr(now));
               Submit_Motivoinutilizacao(Justificativa);
               Submit_ARQUIVOinutilizacao(RetornaPastaSistema+'ARQUIVOINU_R.XML');
               Submit_PROTOCOLOinutilizacao(pprotocolo);

               if (Salvar(true)=true) then
               begin

                  {marcando as nf na tabnotafiscal como inutilizada "Z"}
                  Self.inutiliza_faixa_na_tabNotaFiscal (IntToStr(cont));


               end
               else
                 MensagemErro('Erro na tentativa de salvar a  a NFE '+get_codigo);


           End;
      End;
      MensagemAviso('Conclu�do');

end;

procedure TFmenuNfe.inutiliza_faixa_na_tabNotaFiscal(pnf:string);
var
  query1:tibquery;
begin

  try


      query1 := TIBQuery.Create(nil);
      query1.Database := FDataModulo.IBDatabase;


      with query1 do
      begin

        Close;
        sql.Clear;
        sql.Add ('update tabnotafiscal set situacao = ''Z'' where numero = '+pnf+' and modelonf = 26');

        //InputBox('','',sql.text);

        try
          ExecSQL;
          FDataModulo.IBTransaction.CommitRetaining;
        except
          MensagemAviso ('N�o foi possivel alterar a situacao para inutilizada na TabNotaFiscal. NF de n�mero: '+pnf);
          Exit;
        end;

      end;

  finally

    FreeAndNil (query1);

  end;

end;

procedure TFmenuNfe.BitBtn6Click(Sender: TObject);
var
  Precibo:string;
  pnfe:string;
  PForcaProcessamento:Boolean;
  querytemp:TIBQuery;
begin

  PForcaProcessamento := False;

  FComponentesNfe.ACBrNFe.NotasFiscais.Clear;

  Self.ObjNfObjetos.objgeranfe.ConfiguraComponente;

  FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie:=ObjEmpresaGlobal.get_certificado;

  pnfe := self.get_pesquisa_NFE();

  if (pnfe = '') then
  Exit;

  self.ObjNfObjetos.objgeranfe.Nfe.LocalizaCodigo (pnfe);
  Self.ObjNfObjetos.objgeranfe.Nfe.TabelaparaObjeto ();

  if not FileExists(Self.ObjNfObjetos.objgeranfe.Nfe.get_arquivo_xml ()) then
  begin

    MensagemAviso ('Essa Nfe n�o possui arquivo XML');
    Exit;

  end;

  FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
  FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile (Self.ObjNfObjetos.objgeranfe.Nfe.get_arquivo_xml());

    try


        if not (FComponentesNfe.ACBrNFe.WebServices.StatusServico.Executar) then
           raise Exception.Create(FcomponentesNfe.ACBrNFe.WebServices.StatusServico.Msg);

        FComponentesNfe.ACBrNFe.WebServices.Enviar.Lote := '0';

        if not (FComponentesNfe.ACBrNFe.WebServices.Enviar.Executar) then
           raise Exception.Create(FcomponentesNfe.ACBrNFe.WebServices.Enviar.Msg);


    except

      on e:Exception do
      Begin

        mensagemerro('Erro na tentativa de enviar a NFE'+#13+E.message);
        exit;

      end;

    end;

    precibo := FComponentesNfe.ACBrNFe.WebServices.Enviar.Recibo;


    if (PForcaProcessamento = False) then
    begin

        FComponentesNfe.ACBrNFe.WebServices.Retorno.Recibo := FComponentesNfe.ACBrNFe.WebServices.Enviar.Recibo;

        if not (FComponentesNfe.ACBrNFe.WebServices.Retorno.Executar) then
           raise Exception.Create(FComponentesNfe.ACBrNFe.WebServices.Retorno.Msg);

    end else
    begin

        raise Exception.Create('Lote de processamento for�ado pelo sistema Amanda');

    end;


    Try

        {Se chegou at� aqui � porque a Nfe foi enviada e confirmada}
        if (FComponentesNfe.ACBrNFe.DANFE <> nil) then
        begin

          if FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].Confirmada then
          begin

            FComponentesNfe.ACBrNFe.DANFE.ProtocoloNFe :=  FComponentesNfe.ACBrNFe.WebServices.Retorno.NFeRetorno.ProtNFe.Items[0].nProt;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].Imprimir;

          end;

        end;

    Except

      MensagemErro('Erro na tentativa de Imprimir, tente imprimir direto do arquivo');

    End;

    {alterando nfe - enviando o recibo de envio e alterando o status para gerada}
    Self.ObjNfObjetos.objgeranfe.Nfe.Status := dsEdit;
    Self.ObjNfObjetos.objgeranfe.Nfe.Submit_ReciboEnvio (Precibo);
    self.ObjNfObjetos.objgeranfe.Nfe.Submit_STATUSNOTA ('G');

    try

      querytemp:=TIBQuery.Create (nil);
      querytemp.Database:=FDataModulo.IBDatabase;

      querytemp.Close;
      querytemp.SQL.Clear;

      querytemp.SQL.Add ('update tabnfe set statusnota='+#39+'G'+#39+',reciboenvio='+#39+Precibo+#39+' where codigo = '+pnfe);

      try

        //InputBox ('','',querytemp.SQL.Text);
        querytemp.ExecSQL;

      except

        MensagemErro ('N�o foi possivel alterar a situa��o para G na tabnfe. Informe ao suporte do sistema');
        Exit;

      end;

    finally

       FreeAndNil (querytemp);

    end;



    {alterando nf - enviando o status para G}
    try

      querytemp:=TIBQuery.Create (nil);
      querytemp.Database:=FDataModulo.IBDatabase;

      querytemp.Close;
      querytemp.SQL.Clear;

      querytemp.SQL.Add ('update tabnotafiscal set situacao = '+#39+'G'+#39+' where numero = '+pnfe+' and modelonf = 26');


      try

        //InputBox ('','',querytemp.SQL.Text);
        querytemp.ExecSQL;

      except

        MensagemErro ('N�o foi possivel alterar a situa��o para G na tabnotafiscal. Informe ao suporte do sistema');
        Exit;

      end;


    finally

      FreeAndNil (querytemp);

    end;

    mensagemaviso ('Processo concluido');

end;

function TFmenuNfe.get_pesquisa_NFE: string;
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           FpesquisaLocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(self.ObjNfObjetos.objgeranfe.Nfe.Get_Pesquisa,self.ObjNfObjetos.objgeranfe.Nfe.Get_TituloPesquisa,Nil)=True)Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin


                                  if (self.ObjNfObjetos.objgeranfe.Nfe.status <> dsinactive)
                                  then exit;

                                  If (self.ObjNfObjetos.objgeranfe.Nfe.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  self.ObjNfObjetos.objgeranfe.Nfe.TabelaparaObjeto();
                                  result:=self.ObjNfObjetos.objgeranfe.Nfe.Get_CODIGO();

                                  //Self.ObjNFE.ZerarTabela;

                                 { If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                                  }

                        End;

                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

function TFmenuNfe.CANCELA_NF_POR_NFE (pcodigoNFE:string): boolean;
var
  queryTEMP:TIBQuery;
begin

  result:=False;

      try

        queryTEMP := TIBQuery.Create (nil);
        queryTEMP.Database := FDataModulo.IBDatabase;

        with queryTEMP do
        begin

          Close;
          SQL.Clear;
          SQL.Add ('update tabnotafiscal set situacao = '+#39+'C'+#39+' where numero = '+#39+pcodigoNFE+#39+' and MODELO_NF = 2');

          try

            ExecSQL;

          except

            MensagemErro ('Erro ao alterar o status da NF para Cancelada na tabnotafiscal. Anote o n�mero da NF-e e comunique o suporte do sistema');
            Exit;

          end;

          result:=True;

        end;

      finally

        FreeAndNil (queryTEMP);

      end;


end;

procedure TFmenuNfe.SpeedButton1Click(Sender: TObject);
begin
  objTransmitenfe.consultaServico;
end;

procedure TFmenuNfe.SpeedButton2Click(Sender: TObject);
begin
   objTransmitenfe.consultaPelaChave('','',true);
end;

procedure TFmenuNfe.cancelaPelaChave(pChave, pProtocolo,pCodigo: string);
var
  Justificativa,PArquivoCancela:string;
  xmlCancelamento:TStringList;
begin
  Justificativa := 'Justificativa do Cancelamento';
  if not(InputQuery('WebServices Eventos: Cancelamento', 'Justificativa do Cancelamento', Justificativa)) then
  exit;

  if not objTransmitenfe.cancelaPelaChave(pChave,pProtocolo,Justificativa,PArquivoCancela) then
  begin
    MensagemErro('Erro ao cancelar a NF-e. C�digo do status: '+IntToStr(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat)+'. Motivo: '+FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.xMotivo);
    Exit;
  end;

  Try
    {agora preciso localizar a NF e cancelar na tabnotafiscal. OBS: o codigo da NF-e � o numero nda NF
    ent�o localizo tabnotafiscal aonde nf.numero = codigoNF-e e nf.modelo = 26 (NF-e)}


    CANCELA_NF_POR_NFE(pcodigo);

    //Localizando na TABNFE
    if (Self.ObjNfObjetos.objgeranfe.NFE.LocalizaCodigo(pcodigo)=True) then
    Begin
      Self.ObjNfObjetos.objgeranfe.NFE.TabelaparaObjeto;
      Self.ObjNfObjetos.objgeranfe.NFE.Status:= dsedit;
      Self.ObjNfObjetos.objgeranfe.NFE.Submit_StatusNota('C');//cancelada
      Self.ObjNfObjetos.objgeranfe.NFE.Submit_ARQUIVOCANCELAMENTO(PArquivoCancela);
      Self.ObjNfObjetos.objgeranfe.NFE.Submit_PROTOCOLOCANCELAMENTO(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.nProt);
      Self.ObjNfObjetos.objgeranfe.NFE.Submit_DataCancelamento(datetostr(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.dhRegEvento));
      Self.ObjNfObjetos.objgeranfe.NFE.Submit_MotivoCancelamento(Justificativa);

      if (Self.ObjNfObjetos.objgeranfe.NFE.Salvar(false)=False) then
        MensagemErro('Erro na tentativa de Alterar o Status da NFe para Cancelada e guardar o protocolo');
        
    End;

    FDataModulo.IBTransaction.CommitRetaining;
    MensagemSucesso ('Processo concluido');

  Finally
    FDataModulo.IBTransaction.RollbackRetaining;
  End;

    {antigo}
   {
  if (pChave = '') then
    if not(InputQuery('WebServices Cancelamento', 'Chave da NF-e', pChave)) then
      exit;

  if (pProtocolo = '') then
    if not(InputQuery('WebServices Cancelamento', 'Protocolo de Autoriza��o', pProtocolo)) then
     exit;

  if (pCodigo = '') then
    if not(InputQuery('WebServices Cancelamento', 'N�mero da NF-e', pCodigo)) then
     exit;

  if not(InputQuery('WebServices Cancelamento', 'Justificativa', Justificativa)) then
     exit;

  if (Length(Justificativa) < 15) then
  begin

    MensagemErro ('A justificativa deve ser maior ou igual a 15 caracteres');
    Exit;

  end;


  FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
  Self.ObjNfObjetos.objgeranfe.ConfiguraComponente;
  FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie:=ObjEmpresaGlobal.get_certificado;

  FComponentesNfe.ACBrNFe.WebServices.Cancelamento.NFeChave      := pChave;
  FComponentesNfe.ACBrNFe.WebServices.Cancelamento.Protocolo     := pProtocolo;
  FComponentesNfe.ACBrNFe.WebServices.Cancelamento.Justificativa := Justificativa;

  try
    FComponentesNfe.ACBrNFe.WebServices.Cancelamento.Executar;
  except

    on e:Exception do
    begin

      MensagemAviso(e.Message);
      Exit;

    end;

  end;


  FcomponentesNfe.MemoResp.Text :=  UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.Cancelamento.RetWS);
  FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);


  PArquivoCancela:=Self.ObjNfObjetos.objgeranfe.LocalArquivoCancelamento+StringReplace(FcomponentesNfe.ACBrNFe.WebServices.Cancelamento.NFeChave,'Nfe','',[rfIgnoreCase])+'-can.xml';

     try

         try

              xmlCancelamento := TStringList.Create;
              xmlCancelamento.Text := FComponentesNfe.ACBrNFe.WebServices.Cancelamento.XML_ProcCancNFe;
              //ShowMessage (FComponentesNfe.ACBrNFe.WebServices.Cancelamento.XML_ProcCancNFe);


              xmlCancelamento.SaveToFile (PArquivoCancela);

         except

            on e:exception
            do begin

                MensagemErro ('N�o foi possivel salvar o XML cancelamento no diret�rio: '+PArquivoCancela+' Mensagem: '+e.message);

            end;

         end;

     finally

        FreeAndNil (xmlCancelamento);

     end;


     }
end;

procedure TFmenuNfe.SpeedButton4Click(Sender: TObject);
var
  pProtocolo:string;
begin

  if (formnfe.edtArquivoXML.Text <> '') then
  begin
  
    try
      pProtocolo := RetornaValorCampos(formnfe.edtArquivoXML.Text,'nProt');
    except
      pProtocolo:='';
    end;

  end;

  self.cancelaPelaChave(formnfe.edtChaveAcesso.Text,pProtocolo,formnfe.lbCodigo.Caption);

end;

procedure TFmenuNfe.passaFormulario(fnfe: TFnotaFiscalEletronica);
begin
  self.formnfe := fnfe;
end;

procedure TFmenuNfe.btRecuperaProtocoloClick(Sender: TObject);
begin
  objTransmitenfe.retornaProtocolo;
end;

end.
