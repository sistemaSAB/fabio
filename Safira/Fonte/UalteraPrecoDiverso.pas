unit UalteraPrecoDiverso;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,UobjDiversoCor, ExtCtrls, Buttons;

type
  TFalteraPrecoDiverso = class(TForm)
    GroupBoxMargem: TGroupBox;
    LbPorcentagemFornecido: TLabel;
    LbPorcentagemRetirado: TLabel;
    LbPorcentagemInstalado: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdtPorcentagemInstalado: TEdit;
    EdtPorcentagemFornecido: TEdit;
    EdtPorcentagemRetirado: TEdit;
    EdtPrecoCusto: TEdit;
    LbPrecoCusto: TLabel;
    GroupBoxPrecoVenda: TGroupBox;
    LbPrecoVendaInstalado: TLabel;
    LbPrecoVendaFornecido: TLabel;
    LbPrecoVendaRetirado: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdtPrecoVendaInstalado: TEdit;
    EdtPrecoVendaFornecido: TEdit;
    EdtPrecoVendaRetirado: TEdit;
    Label1: TLabel;
    lbprecopago: TLabel;
    Panel1: TPanel;
    Label8: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    GroupBoxLucro: TGroupBox;
    LbPorcentagemAcrescimo: TLabel;
    LbAcrescimoExtra: TLabel;
    LbPorcentagemAcrescimoFinal: TLabel;
    EdtPorcentagemAcrescimo: TEdit;
    EdtAcrescimoExtra: TEdit;
    EdtPorcentagemAcrescimoFinal: TEdit;
    Rg_forma_de_calculo_percentual: TRadioGroup;
    edtinstaladofinal: TEdit;
    edtfornecidofinal: TEdit;
    edtretiradofinal: TEdit;
    btgravar: TButton;
    btcancelar: TButton;
    procedure FormShow(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure EdtPorcentagemInstaladoExit(Sender: TObject);
    procedure EdtPorcentagemFornecidoExit(Sender: TObject);
    procedure EdtPorcentagemRetiradoExit(Sender: TObject);
    procedure EdtPrecoVendaInstaladoExit(Sender: TObject);
    procedure EdtPrecoVendaFornecidoExit(Sender: TObject);
    procedure EdtPrecoVendaRetiradoExit(Sender: TObject);
    procedure EdtAcrescimoExtraExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
        AlteraPrecoPeloCusto:Boolean;

  public
    { Public declarations }
    ObjdiversoCor:TobjDiversoCor;
  end;

var
  FalteraPrecoDiverso: TFalteraPrecoDiverso;

implementation

uses UDataModulo, UessencialGlobal;

{$R *.dfm}

procedure TFalteraPrecoDiverso.FormShow(Sender: TObject);
begin
    Self.Tag:=0;

     AlteraPrecoPeloCusto:=True;

    if (ObjParametroGlobal.ValidaParametro('CALCULA PRECO DE VENDA DO DIVERSO PELO CUSTO E PERCENTUAIS?')=True)
    then Begin
               if (ObjParametroGlobal.get_valor<>'SIM')
               then AlteraPrecoPeloCusto:=false;
    End;

    if (AlteraPrecoPeloCusto=True)
    Then Begin
              GroupBoxPrecoVenda.Enabled:=False;
              GroupBoxMargem.Enabled:=True;
    End
    Else BEgin
              GroupBoxPrecoVenda.Enabled:=True;
              GroupBoxMargem.Enabled:=False;
    End;

    Rg_forma_de_calculo_percentual.itemindex:=0;
    if (ObjParametroGlobal.ValidaParametro('FORMA DE CALCULO DE PERCENTUAL DE ACRESCIMO NAS CORES DO DIVERSO')=True)
    then Begin
               if (ObjParametroGlobal.get_valor='1')
               then Rg_forma_de_calculo_percentual.itemindex:=1;
    End;
    Rg_forma_de_calculo_percentual.enabled:=False;
End;

procedure TFalteraPrecoDiverso.btgravarClick(Sender: TObject);
begin
     Self.Tag:=1;
     Self.Close;
end;

procedure TFalteraPrecoDiverso.btcancelarClick(Sender: TObject);
begin
     Self.tag:=0;
     Self.close;
end;

procedure TFalteraPrecoDiverso.EdtPorcentagemInstaladoExit(
  Sender: TObject);
Var
  PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaInstalado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemInstalado.Text))/100)));
end;

procedure TFalteraPrecoDiverso.EdtPorcentagemFornecidoExit(
  Sender: TObject);
Var
  PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaFornecido.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemFornecido.Text))/100)));
end;

procedure TFalteraPrecoDiverso.EdtPorcentagemRetiradoExit(Sender: TObject);
Var
  PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaRetirado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemRetirado.Text))/100)));
end;

procedure TFalteraPrecoDiverso.EdtPrecoVendaInstaladoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemInstalado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaInstalado.Text))/precocusto));
end;
procedure TFalteraPrecoDiverso.EdtPrecoVendaFornecidoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemfornecido.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendafornecido.Text))/precocusto));
end;

procedure TFalteraPrecoDiverso.EdtPrecoVendaRetiradoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemretirado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaretirado.Text))/precocusto));
End;

procedure TFalteraPrecoDiverso.EdtAcrescimoExtraExit(Sender: TObject);
Var
PValorCusto, PAcrescimoExtra, PValorExtra :Currency;
begin
    if ((TEdit(Sender).Text=''))
    then exit;

    PValorCusto:=0;
    PAcrescimoExtra:=0;
    PValorExtra:=0;

    try
          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));

          PAcrescimoExtra:=StrTOCurr(tira_ponto(EdtAcrescimoExtra.Text));
          PValorExtra:=((PValorCusto*PAcrescimoExtra)/100);
    except;
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais.');
    end;

    EdtPorcentagemAcrescimoFinal.Text:=CurrToStr(StrToCurr(tira_ponto(EdtPorcentagemAcrescimo.Text))+StrToCurr(tira_ponto(EdtAcrescimoExtra.Text)));

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(tira_ponto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
end;

procedure TFalteraPrecoDiverso.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);

end;

end.
