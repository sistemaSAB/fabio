unit URAMOATIVIDADE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls,db, UObjRAMOATIVIDADE,
  UessencialGlobal, Tabs,IBQuery;

type
  TFRAMOATIVIDADE = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodRamoAtividade: TLabel;
    lb1: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    lbLbObservacoes: TLabel;
    mmoObservacoes: TMemo;
    edtDataCadastro: TMaskEdit;
    lbDataCadastro: TLabel;
    lbLbAtivo: TLabel;
    lbLbNome: TLabel;
    EdtNome: TEdit;
    grpAtivo: TGroupBox;
    rbRadioSIM: TRadioButton;
    rbRadioNao: TRadioButton;
    pnl1: TPanel;
    imgrodape: TImage;
    lb9: TLabel;
//DECLARA COMPONENTES

    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtNomeExit(Sender: TObject);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    procedure MostraQuntidadeCadastrada;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRAMOATIVIDADE: TFRAMOATIVIDADE;
  ObjRAMOATIVIDADE:TObjRAMOATIVIDADE;

implementation

uses Upesquisa, UessencialLocal, UDataModulo, UescolheImagemBotao,
  Uprincipal;

{$R *.dfm}


procedure TFRAMOATIVIDADE.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     ColocaUpperCaseEdit(Self);

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     lbCodRamoAtividade.caption:='';


     Try
        ObjRAMOATIVIDADE:=TObjRAMOATIVIDADE.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE RAMO DE ATIVIDADE')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);
     MostraQuntidadeCadastrada;

     if(Tag<>0)
     then
     begin
          if(ObjRAMOATIVIDADE.LocalizaCodigo(IntToStr(tag))=True)
          then
          begin
             ObjRAMOATIVIDADE.TabelaparaObjeto;
             self.ObjetoParaControles;
          end;

     end;
     rbRadioSIM.Enabled:=True;
     rbRadioNao.Enabled:=True;

end;

procedure TFRAMOATIVIDADE.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjRAMOATIVIDADE=Nil)
     Then exit;

     If (ObjRAMOATIVIDADE.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjRAMOATIVIDADE.free;
    Self.Tag:=0;
   
end;

procedure TFRAMOATIVIDADE.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);


     lbCodRamoAtividade.caption:='0';
     //edtcodigo.text:=ObjRAMOATIVIDADE.Get_novocodigo;
     EdtDataCadastro.Text:=datetostr(now);


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjRAMOATIVIDADE.status:=dsInsert;
     EdtNome.setfocus;

     btalterar.visible:=false;
     btrelatorios.visible:=false;
     btsair.Visible:=false;
     btopcoes.visible:=false;
     btexcluir.Visible:=false;
     btnovo.Visible:=false;
     btopcoes.visible:=false;

end;

procedure TFRAMOATIVIDADE.btSalvarClick(Sender: TObject);
begin

     If ObjRAMOATIVIDADE.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
     End;

     If (ObjRAMOATIVIDADE.salvar(true)=False)
     Then exit;

     lbCodRamoAtividade.caption:=ObjRAMOATIVIDADE.Get_codigo;
     habilita_botoes(Self);
     desabilita_campos(Self);
     btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
     btopcoes.visible:=true;
     MostraQuntidadeCadastrada;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFRAMOATIVIDADE.btAlterarClick(Sender: TObject);
begin
    If (ObjRAMOATIVIDADE.Status=dsinactive) and (lbCodRamoAtividade.caption<>'')
    Then Begin
                habilita_campos(Self);
                ObjRAMOATIVIDADE.Status:=dsEdit;
                edtNome.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btalterar.visible:=false;
                btrelatorios.visible:=false;
                btsair.Visible:=false;
                btopcoes.visible:=false;
                btexcluir.Visible:=false;
                btnovo.Visible:=false;
                btopcoes.visible:=false;
          End;

end;

procedure TFRAMOATIVIDADE.btCancelarClick(Sender: TObject);
begin
     ObjRAMOATIVIDADE.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
      btopcoes.visible:=true;

end;

procedure TFRAMOATIVIDADE.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjRAMOATIVIDADE.Get_pesquisa,ObjRAMOATIVIDADE.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjRAMOATIVIDADE.status<>dsinactive
                                  then exit;

                                  If (ObjRAMOATIVIDADE.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjRAMOATIVIDADE.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFRAMOATIVIDADE.btExcluirClick(Sender: TObject);
begin
     If (ObjRAMOATIVIDADE.status<>dsinactive) or (lbCodRamoAtividade.caption='')
     Then exit;

     If (ObjRAMOATIVIDADE.LocalizaCodigo(lbCodRamoAtividade.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjRAMOATIVIDADE.exclui(lbCodRamoAtividade.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFRAMOATIVIDADE.btRelatorioClick(Sender: TObject);
begin
    ObjRAMOATIVIDADE.Imprime;
end;

procedure TFRAMOATIVIDADE.btSairClick(Sender: TObject);
begin
    Self.close;
end;


procedure TFRAMOATIVIDADE.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFRAMOATIVIDADE.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFRAMOATIVIDADE.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
    if(key=vk_f1) then
    begin
        Fprincipal.chamaPesquisaMenu;
    end;
end;

procedure TFRAMOATIVIDADE.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFRAMOATIVIDADE.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjRAMOATIVIDADE do
    Begin
        Submit_Codigo(lbCodRamoAtividade.caption);
        Submit_Nome(edtNome.text);
        Submit_Observacoes(MmoObservacoes.text);

        if (rbRadioSIM.Checked = true)then
        Submit_Ativo('S')
        else if (rbRadioNao.Checked = true)then
        Submit_Ativo('N');
        Submit_DataCadastro(edtDataCadastro.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFRAMOATIVIDADE.LimpaLabels;
begin
//LIMPA LABELS
end;

function TFRAMOATIVIDADE.ObjetoParaControles: Boolean;
begin
  Try
     With ObjRAMOATIVIDADE do
     Begin
        lbCodRamoAtividade.caption:=Get_Codigo;
        EdtNome.text:=Get_Nome;
        MmoObservacoes.text:=Get_Observacoes;

        if (Get_Ativo='S')then
        rbRadioSIM.Checked:=true
        else if (Get_Ativo='N')then
        rbRadioNao.Checked:=true;

        EdtDataCadastro.text:=Get_DataCadastro;
//CODIFICA GETS 
        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFRAMOATIVIDADE.TabelaParaControles: Boolean;
begin
     If (ObjRAMOATIVIDADE.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     rbRadioSIM.Enabled:=True;
     rbRadioNao.Enabled:=True;
     Result:=True;

end;


procedure TFRAMOATIVIDADE.EdtNomeExit(Sender: TObject);
begin
    if (rbRadioSIM.Enabled = true)then
    rbRadioSIM.SetFocus;
end;

procedure TFRAMOATIVIDADE.MostraQuntidadeCadastrada;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabramoatividade');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb9.Caption:='Existem '+IntToStr(contador)+' Ramos de atividade cadastrados'
       else
       begin
            lb9.Caption:='Existe '+IntToStr(contador)+' Ramo de atividade cadastrado';
       end;

    finally

    end;


end;

end.
