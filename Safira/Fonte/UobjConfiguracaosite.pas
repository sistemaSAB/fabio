unit UobjConfiguracaosite;

interface
uses IBQuery,UDataModulo,StdCtrls,Dialogs,ZAbstractRODataset, ZAbstractDataset, ZDataset, ZConnection,IdFTP,
Forms,UUtils,UessencialGlobal
;

type
  TObjConfiguracaoSite = class
    private
        // Componentes de Acesso ao site
        ZConnection: TZConnection;
        ObjQuerySite: TZQuery;
        FTP:TIdFtp;
    public
        QUERY_ConfiguracaoSite:TIBQuery;

        constructor Create;
        destructor Free;
        Function __Submit(EdtDATABASESITE,EdtHOSTNAMESITE,EdtPASSWORDSITE,EdtPORTSITE,EdtPROTOCOLSITE
        ,EdtUSERSITE,EdtHOSTFTP,EdtUSERNAMEFTP,EdtPASSWORDFTP :TEdit):Boolean;
        procedure __SalvarConfiguracaoSite(Salvar:Boolean);
        procedure __PesquisarConfiguracaoSite(EdtDATABASESITE,EdtHOSTNAMESITE,EdtPASSWORDSITE,EdtPORTSITE,EdtPROTOCOLSITE
        ,EdtUSERSITE,EdtHOSTFTP,EdtUSERNAMEFTP,EdtPASSWORDFTP :TEdit);

       Function __Conectar:Boolean;
       Function __DesConectar:Boolean;
  end;

implementation

uses SysUtils;

constructor TObjConfiguracaoSite.Create;
begin
  try
    QUERY_ConfiguracaoSite:=TIBQuery.Create(nil);
    QUERY_ConfiguracaoSite.Database:=FDataModulo.IBDatabase;

    Self.ZConnection:=TZConnection.Create(nil);
    ObjQuerySite:=TZQuery.Create(nil);
    ObjQuerySite.Connection:=Self.ZConnection;
    Self.FTP:=TIdFtp.Create(nil);
  except
     Exit;
  end;
end;

destructor TObjConfiguracaoSite.Free;
begin
  FreeAndNil(QUERY_ConfiguracaoSite);
  FreeAndNil(Self.ZConnection);
  FreeAndNil(Self.FTP);
  FreeAndNil(ObjQuerySite);
end;

procedure TObjConfiguracaoSite.__SalvarConfiguracaoSite(Salvar:Boolean);
begin
   if(Salvar)
   then FDataModulo.IBTransaction.CommitRetaining
   else FDataModulo.IBTransaction.RollbackRetaining;
end;

procedure TObjConfiguracaoSite.__PesquisarConfiguracaoSite(EdtDATABASESITE,EdtHOSTNAMESITE,EdtPASSWORDSITE,EdtPORTSITE,EdtPROTOCOLSITE
,EdtUSERSITE,EdtHOSTFTP,EdtUSERNAMEFTP,EdtPASSWORDFTP :TEdit);
begin
  QUERY_ConfiguracaoSite.Close;
  QUERY_ConfiguracaoSite.SQL.Clear;
  QUERY_ConfiguracaoSite.SQL.Text:=
  'select codigo,banco,hostname,senha,porta,protocolo, '+
  'usuario,usuarioftp,hostftp,senhaftp '+
  'from tabconfiguracaosite where codigo=''1'' ';

  QUERY_ConfiguracaoSite.Open;
  EdtDATABASESITE.Text :=  QUERY_ConfiguracaoSite.Fields[1].AsString;
  EdtHOSTNAMESITE.Text :=  QUERY_ConfiguracaoSite.Fields[2].AsString;
  EdtPASSWORDSITE.text :=  QUERY_ConfiguracaoSite.Fields[3].AsString;
  EdtPORTSITE.Text     :=  QUERY_ConfiguracaoSite.Fields[4].AsString;
  EdtPROTOCOLSITE.Text :=  QUERY_ConfiguracaoSite.Fields[5].AsString;
  EdtUSERSITE.Text     :=  QUERY_ConfiguracaoSite.Fields[6].AsString;
  EdtHOSTFTP.Text      :=  QUERY_ConfiguracaoSite.Fields[8].AsString;
  EdtUSERNAMEFTP.Text  :=  QUERY_ConfiguracaoSite.Fields[7].AsString;
  EdtPASSWORDFTP.Text  :=  QUERY_ConfiguracaoSite.Fields[9].AsString;

end;

Function TObjConfiguracaoSite.__Submit(EdtDATABASESITE,EdtHOSTNAMESITE,EdtPASSWORDSITE,EdtPORTSITE,EdtPROTOCOLSITE
,EdtUSERSITE,EdtHOSTFTP,EdtUSERNAMEFTP,EdtPASSWORDFTP :TEdit):Boolean;
begin
  Result:=True;

  QUERY_ConfiguracaoSite.Close;
  QUERY_ConfiguracaoSite.SQL.Clear;
  QUERY_ConfiguracaoSite.SQL.Text:=
  'update tabconfiguracaosite set codigo=:codigo,banco=:banco,hostname=:hostname,senha=:senha,porta=:porta,protocolo=:protocolo, '+
  'usuario=:usuario,usuarioftp=:usuarioftp,hostftp=:hostftp,senhaftp=:senhaftp '+
  'where codigo=''1'' ';

  {
    0-codigo
    1-banco
    2-hostname
    3-senha
    4-porta
    5-protocolo
    6-usuario
    7-usuarioftp
    8-hostftp
  }
  QUERY_ConfiguracaoSite.Params[0].AsString :='1';
  QUERY_ConfiguracaoSite.Params[1].AsString :=EdtDATABASESITE.Text;
  QUERY_ConfiguracaoSite.Params[2].AsString:=EdtHOSTNAMESITE.Text;
  QUERY_ConfiguracaoSite.Params[3].AsString:=EdtPASSWORDSITE.Text;
  QUERY_ConfiguracaoSite.Params[4].AsString:=EdtPORTSITE.Text;
  QUERY_ConfiguracaoSite.Params[5].AsString:=EdtPROTOCOLSITE.Text;
  QUERY_ConfiguracaoSite.Params[6].AsString:=EdtUSERSITE.Text;
  QUERY_ConfiguracaoSite.Params[7].AsString:=EdtUSERNAMEFTP.Text;
  QUERY_ConfiguracaoSite.Params[8].AsString:=EdtHOSTFTP.Text;
  QUERY_ConfiguracaoSite.Params[9].AsString:=EdtPASSWORDFTP.Text;
  try
    QUERY_ConfiguracaoSite.ExecSQL;
  except
    Result:=False;
  end;      
end;

Function TObjConfiguracaoSite.__Conectar:Boolean;
begin
  Result:=false;
    Try
      Self.ZConnection.Database:=QUERY_ConfiguracaoSite.Fields[1].AsString;
      Self.ZConnection.HostName:=QUERY_ConfiguracaoSite.Fields[2].AsString;
      Self.ZConnection.Password:=QUERY_ConfiguracaoSite.Fields[3].AsString; //DesincriptaSenha
      Self.ZConnection.Port:=QUERY_ConfiguracaoSite.Fields[4].AsInteger;
      Self.ZConnection.Protocol:=QUERY_ConfiguracaoSite.Fields[5].AsString;
      Self.ZConnection.User:=QUERY_ConfiguracaoSite.Fields[6].AsString;
      Self.ZConnection.ReadOnly:=false;

    Except
      on e:Exception do
      begin
        Application.HandleException(Self);
        MensagemErro('Erro ao resgatar informa��es de conex�o'+#13+e.Message);
        exit;
      end;
    end;
    try
      Self.ZConnection.Connect;
    except
      Application.HandleException(Self);
      MensagemErro('Erro ao tenta Conectar com o banco de dados on-line');
      exit;
    end;

    // Agora  o FTP
    {if (FTP.Connected) then
      FTP.Disconnect;

    ftp.Host := Get_HostFTP;
    Ftp.Username := Get_UsernameFTP;
    ftp.Password := Get_PasswordFTP;
    FTP.Passive := True; }

  {try
    Ftp.Connect;
  except
    Application.HandleException(Self);
    MensagemErro('Erro ao tentar conectar com o Servidor FTP');
    exit;
  end;   }
  Result:=true;
end;

Function TObjConfiguracaoSite.__DesConectar:Boolean;
begin
   Result:=false;

   try
       Self.ZConnection.Disconnect;
   except
       MensagemErro('Erro ao tenta DesConectar com o banco de dados on-line');
       exit;
   end;

   try
       Self.FTP.Disconnect;
   except
       MensagemErro('Erro ao tenta DesConectar com o servidor FTP');
       exit;
   end;

   Result:=true;

end;


end.
