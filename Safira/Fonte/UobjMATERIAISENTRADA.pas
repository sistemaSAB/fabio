unit UobjMATERIAISENTRADA;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,
UobjFERRAGEM,UobjPERFILADO,UobjPERSIANA,UobjVIDRO,UobjDIVERSO,UobjKITBOX,
UobjENTRADAPRODUTOS,UobjTABELAA_ST,UobjTABELAB_ST,UObjCFOP,UobjCSOSN;
//USES_INTERFACE


Type
   TObjMATERIAISENTRADA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                FERRAGEM:TObjFERRAGEM;
                PERFILADO:TObjPERFILADO;
                PERSIANA:TObjPERSIANA;
                VIDRO:TObjVIDRO;
                DIVERSO:TObjDIVERSO;
                KITBOX:TObjKITBOX;
                ENTRADA:TObjENTRADAPRODUTOS;
                CSTA:TObjTABELAA_ST;
                CSTB:TObjTABELAB_ST;
                CFOP:TObjCFOP;
                CSOSN:TObjCSOSN;

//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create(Owner:TComponent);
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;

                Procedure Submit_QUANTIDADE(parametro: string);
                Function Get_QUANTIDADE: string;
                Procedure Submit_VALOR(parametro: string);
                Function Get_VALOR: string;
                Procedure Submit_ORDEMINSERCAO(parametro: string);
                Function Get_ORDEMINSERCAO: string;
                Procedure Submit_CREDITOIPI(parametro: string);
                Function Get_CREDITOIPI: string;
                Procedure Submit_CREDITOICMS(parametro: string);
                Function Get_CREDITOICMS: string;
                Procedure Submit_CREDITOPIS(parametro: string);
                Function Get_CREDITOPIS: string;
                Procedure Submit_CREDITOCOFINS(parametro: string);
                Function Get_CREDITOCOFINS: string;
                Procedure Submit_IPIPAGO(parametro: string);
                Function Get_IPIPAGO: string;

                Procedure Submit_UNIDADE(parametro: string);
                Function Get_UNIDADE: string;
                Procedure Submit_DESCONTO(parametro: string);
                Function Get_DESCONTO: string;

                procedure Submit_Materaial(parametro:String);
                Function Get_Material:string;

                procedure Submit_Material_ep(parametro:string);
                Function Get_Material_ep:string;

                procedure Submit_BCIcms(parametro:string);
                Function Get_BCIcms:string;
                procedure Submit_ValorIcms(parametro:string);
                Function Get_ValorIcms:string;
                procedure Submit_ValorFrete(parametro:string);
                Function Get_ValorFrete:string;
                Procedure Submit_ValorIpi(parametro:string);
                function Get_ValorIPI:string;
                Procedure Submit_RedBaseIcms(parametro:string);
                Function Get_RedBaseIcms:string;
                procedure Submit_NCM(parametro:string);
                Function Get_NCM:string;
                procedure Submit_Descricao(parametro:string);
                Function Get_Descricao:string;


                //CODIFICA DECLARA GETSESUBMITS

         Private
                 Objquery:Tibquery;
                 InsertSql,DeleteSql,ModifySQl:TStringList;
                 Owner:TComponent;
                 
                 CODIGO:string;
                 QUANTIDADE:string;
                 VALOR:string;
                 ORDEMINSERCAO:string;
                 CREDITOIPI:string;
                 CREDITOICMS:string;
                 CREDITOPIS:string;
                 CREDITOCOFINS:string;
                 IPIPAGO:string;
                 UNIDADE:string;
                 DESCONTO:string;
                 MATERIAL:string;
                 MATERIAL_EP:string;
                 BCICMS:string;
                 VALORICMS:string;
                 VALORFRETE:string;
                 VALORIPI:string;
                 REDBASEICMS:string;
                 NCM:string;
                 descricao:String;

                 ParametroPesquisa:TStringList;

                 Function  VerificaBrancos:Boolean;
                 Function  VerificaRelacionamentos:Boolean;
                 Function  VerificaNumericos:Boolean;
                 Function  VerificaData:Boolean;
                 Function  VerificaFaixa:boolean;
                 Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;





Function  TObjMATERIAISENTRADA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        if(fieldbyname('FERRAGEM').asstring<>'') then
        begin
              if(Self.FERRAGEM.LocalizaCodigo(fieldbyname('FERRAGEM').asstring)=False) then
              begin
                      MensagemErro('Parametro ferragem n�o encontrado');
                      self.ZerarTabela;
                      Result:=False;
                      exit;
              end
              else Self.FERRAGEM.TabelaparaObjeto;

        end;

        if(fieldbyname('PERFILADO').asstring<>'') then
        begin
              if(self.PERFILADO.LocalizaCodigo(fieldbyname('PERFILADO').asstring)=False) then
              begin
                      MensagemErro('Parametro perfilado n�o encontrado');
                      self.ZerarTabela;
                      Result:=False;
                      Exit;
              end
              else Self.PERFILADO.TabelaparaObjeto;
        end;

        if(fieldbyname('PERSIANA').asstring <>'') then
        begin
              if(Self.PERSIANA.LocalizaCodigo(fieldbyname('PERSIANA').asstring)=False)then
              begin
                      MensagemErro('Parametro persiana n�o encontrado');
                      self.ZerarTabela;
                      Result:=False;
                      Exit;
              end
              else self.PERSIANA.TabelaparaObjeto;
        end;

        if(fieldbyname('VIDRO').asstring<>'') then
        begin
              if(self.VIDRO.LocalizaCodigo(fieldbyname('VIDRO').asstring)=False)then
              begin
                      MensagemErro('Parametro vidro n�o encontrado');
                      self.ZerarTabela;
                      Result:=False;
                      Exit;
              end
              else Self.VIDRO.TabelaparaObjeto;
        end;

        if(fieldbyname('DIVERSO').asstring<>'') then
        begin
              if(self.DIVERSO.LocalizaCodigo(fieldbyname('DIVERSO').asstring)=False) then
              begin
                      MensagemErro('Parametro diverso n�o encontrado');
                      self.ZerarTabela;
                      Result:=false;
                      Exit;
              end
              else Self.DIVERSO.TabelaparaObjeto;
        end;

        if(fieldbyname('KITBOX').asstring<>'') then
        begin
              if(Self.DIVERSO.LocalizaCodigo(fieldbyname('KITBOX').asstring)=False) then
              begin
                      MensagemErro('Parametro kitbox n�o encontrado');
                      self.ZerarTabela;
                      Result:=False;
                      Exit;
              end
              else Self.DIVERSO.TabelaparaObjeto;
        end;

        if(fieldbyname('ENTRADA').asstring<>'') then
        begin
              if(self.ENTRADA.LocalizaCodigo(fieldbyname('ENTRADA').asstring)=False) then
              begin
                      MensagemErro('Parametro ENTRADA n�o encontrado');
                      self.ZerarTabela;
                      Result:=false;
                      Exit;
              end
              else Self.ENTRADA.TabelaparaObjeto;
        end;

        if(FieldByName('csta').AsString<>'') then
        begin
              if(self.CSTA.LocalizaCodigo(fieldbyname('CSTA').asstring)=False) then
              begin
                    MensagemErro('Parametro CSTA n�o encontrado');
                    self.ZerarTabela;
                    Result:=False;
                    Exit;
              end
              else self.CSTA.TabelaparaObjeto;
        end;

        if(fieldbyname('CSTB').asstring<>'') then
        begin
             if(self.CSTB.LocalizaCodigo(fieldbyname('CSTB').asstring)=False)then
             begin
                    MensagemErro('Parametro CSTB n�o encontrado');
                    Self.ZerarTabela;
                    Result:=False;
                    Exit;
             end
             else Self.CSTB.TabelaparaObjeto;
        end;

        if(fieldbyname('CFOP').asstring<>'') then
        begin
             if(self.CFOP.LocalizaCodigo(fieldbyname('CFOP').asstring)=False) then
             begin
                    MensagemErro('Parametro CFOP n�o encontrado');
                    Self.ZerarTabela;
                    Result:=False;
                    Exit;
             end
             else self.CFOP.TabelaparaObjeto;
        end;

        if(fieldbyname('CSOSN').asstring<>'') then
        begin
             if(self.CSOSN.LocalizaCodigo(fieldbyname('CSOSN').asstring)=False) then
             begin
                    MensagemErro('Parametro CSOSN n�o encontrado');
                    self.ZerarTabela;
                    Result:=False;
                    Exit;
             end
             else self.CSOSN.TabelaparaObjeto ;
        end;

        Self.QUANTIDADE:=fieldbyname('QUANTIDADE').asstring;
        Self.ORDEMINSERCAO:=fieldbyname('ORDEMINSERCAO').asstring;
        Self.CREDITOIPI:=fieldbyname('CREDITOIPI').asstring;
        Self.CREDITOICMS:=fieldbyname('CREDITOICMS').asstring;
        Self.CREDITOPIS:=fieldbyname('CREDITOPIS').asstring;
        Self.CREDITOCOFINS:=fieldbyname('CREDITOCOFINS').asstring;
        Self.IPIPAGO:=fieldbyname('IPIPAGO').asstring;
        Self.UNIDADE:=fieldbyname('UNIDADE').asstring;
        Self.DESCONTO:=fieldbyname('DESCONTO').asstring;
        self.MATERIAL:=fieldbyname('material').AsString;
        self.MATERIAL_EP:=fieldbyname('material_ep').AsString;
        Self.BCICMS:=fieldbyname('bcicms').AsString;
        self.VALORICMS:=fieldbyname('valoricms').AsString;
        Self.VALORFRETE:=fieldbyname('valorfrete').AsString;
        Self.VALORIPI:=fieldbyname('valoripi').AsString;
        Self.REDBASEICMS:=fieldbyname('redbaseicms').AsString;
        self.descricao:=fieldbyname('descricao').AsString;
        self.NCM:=fieldbyname('ncm').AsString;
        result:=True;
     End;
end;


Procedure TObjMATERIAISENTRADA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('FERRAGEM').asstring:=Self.FERRAGEM.Get_Codigo;
        ParamByName('PERFILADO').asstring:=Self.PERFILADO.Get_Codigo;
        ParamByName('PERSIANA').asstring:=Self.PERSIANA.Get_Codigo;
        ParamByName('VIDRO').asstring:=Self.VIDRO.Get_Codigo;
        ParamByName('DIVERSO').asstring:=Self.DIVERSO.Get_Codigo;
        ParamByName('KITBOX').asstring:=Self.KITBOX.Get_Codigo;
        ParamByName('ENTRADA').asstring:=Self.ENTRADA.Get_Codigo;

        ParamByName('QUANTIDADE').asstring:=virgulaparaponto(Self.QUANTIDADE);
        ParamByName('VALOR').asstring:=virgulaparaponto(Self.VALOR);

        ParamByName('ORDEMINSERCAO').asstring:=Self.ORDEMINSERCAO;
        ParamByName('CREDITOIPI').asstring:=virgulaparaponto(Self.CREDITOIPI);
        ParamByName('CREDITOICMS').asstring:=virgulaparaponto(Self.CREDITOICMS);
        ParamByName('CREDITOPIS').asstring:=virgulaparaponto(Self.CREDITOPIS);
        ParamByName('CREDITOCOFINS').asstring:=virgulaparaponto(Self.CREDITOCOFINS);
        ParamByName('IPIPAGO').asstring:=virgulaparaponto(Self.IPIPAGO);

        ParamByName('CSTA').asstring:=Self.CSTA.Get_Codigo;
        ParamByName('CSTB').asstring:=Self.CSTB.Get_Codigo;
        ParamByName('CFOP').asstring:=Self.CFOP.Get_Codigo;
        ParamByName('CSOSN').asstring:=Self.CSOSN.Get_Codigo;

        ParamByName('UNIDADE').asstring:=Self.UNIDADE;
        ParamByName('DESCONTO').asstring:=virgulaparaponto(Self.DESCONTO);
        ParamByName('material').AsString:=Self.MATERIAL;
        ParamByName('material_ep').AsString:=self.MATERIAL_EP;

        ParamByName('bcicms').AsString:=virgulaparaponto(self.BCICMS);
        ParamByName('valoricms').AsString:=virgulaparaponto(self.VALORICMS);
        ParamByName('valorfrete').AsString:=virgulaparaponto(self.VALORFRETE);
        ParamByName('valoripi').AsString:=virgulaparaponto(self.VALORIPI);
        ParamByName('redbaseicms').AsString:=virgulaparaponto(self.REDBASEICMS);
        ParamByName('ncm').AsString:=self.NCM;
        ParamByName('descricao').AsString:=Self.descricao;

  End;
End;

//***********************************************************************

function TObjMATERIAISENTRADA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try

    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjMATERIAISENTRADA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        FERRAGEM.ZerarTabela;
        PERFILADO.ZerarTabela;
        PERSIANA.ZerarTabela;
        VIDRO.ZerarTabela;
        DIVERSO.ZerarTabela;
        KITBOX.ZerarTabela;
        ENTRADA.ZerarTabela;

        QUANTIDADE:='';
        VALOR:='';

        ORDEMINSERCAO:='';
        CREDITOIPI:='';
        CREDITOICMS:='';
        CREDITOPIS:='';
        CREDITOCOFINS:='';
        IPIPAGO:='';

        CSTA.ZerarTabela;
        CSTB.ZerarTabela;
        CFOP.ZerarTabela;
        CSOSN.ZerarTabela;

        UNIDADE:='';
        DESCONTO:='';
        MATERIAL:='';
        MATERIAL_EP:='';
        BCICMS:='';
        VALORICMS:='';
        VALORFRETE:='';
        VALORIPI:='';
        REDBASEICMS:='';
        NCM:='';
        descricao:='';
     End;
end;

Function TObjMATERIAISENTRADA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';

      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjMATERIAISENTRADA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjMATERIAISENTRADA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
      try
        Strtofloat(Self.QUANTIDADE);
     Except
           Mensagem:=mensagem+'/QUANTIDADE';
     End;
     try
        Strtofloat(Self.VALOR);
     Except
           Mensagem:=mensagem+'/VALOR';
     End;

     try
        Strtofloat(Self.CREDITOIPI);
     Except
           Mensagem:=mensagem+'/CREDITOIPI';
     End;
     try
        Strtofloat(Self.CREDITOICMS);
     Except
           Mensagem:=mensagem+'/CREDITOICMS';
     End;
     try
        Strtofloat(Self.CREDITOPIS);
     Except
           Mensagem:=mensagem+'/CREDITOPIS';
     End;
     try
        Strtofloat(Self.CREDITOCOFINS);
     Except
           Mensagem:=mensagem+'/CREDITOCOFINS';
     End;
     try
        Strtofloat(Self.IPIPAGO);
     Except
           Mensagem:=mensagem+'/IPIPAGO';
     End;
     try
        Strtofloat(Self.DESCONTO);
     Except
           Mensagem:=mensagem+'/DESCONTO';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjMATERIAISENTRADA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';

//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjMATERIAISENTRADA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjMATERIAISENTRADA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro MATERIAISENTRADA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,FERRAGEM,PERFILADO,PERSIANA,VIDRO,DIVERSO,KITBOX');
           SQL.ADD(' ,ENTRADA,QUANTIDADE,VALOR,DATAC,USERC,DATAM,USERM,ORDEMINSERCAO');
           SQL.ADD(' ,CREDITOIPI,CREDITOICMS,CREDITOPIS,CREDITOCOFINS,IPIPAGO,CSTA');
           SQL.ADD(' ,CSTB,CFOP,CSOSN,UNIDADE,DESCONTO,material,material_ep,bcicms,valoricms,valorfrete,valoripi,redbaseicms,ncm,descricao');
           SQL.ADD(' from  TABMATERIAISENTRADA');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjMATERIAISENTRADA.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjMATERIAISENTRADA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjMATERIAISENTRADA.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjMATERIAISENTRADA.create(Owner:TComponent);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin

        Self.Owner := Owner;
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;


        FERRAGEM:=TObjFERRAGEM.Create;
        PERFILADO:=TObjPERFILADO.Create;
        PERSIANA:=TObjPERSIANA.Create;
        VIDRO:=TObjVIDRO.Create;
        DIVERSO:=TObjDIVERSO.Create;
        KITBOX:=TObjKITBOX.Create;
        ENTRADA:=TObjENTRADAPRODUTOS.Create;
        CSTB:=TObjTABELAB_ST.Create;
        CSTA:=TObjTABELAA_ST.Create;
        CSOSN:=TObjCSOSN.Create(Self.Owner);
        CFOP:=TObjCFOP.Create;


//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABMATERIAISENTRADA(CODIGO,FERRAGEM,PERFILADO');
                InsertSQL.add(' ,PERSIANA,VIDRO,DIVERSO,KITBOX,ENTRADA,QUANTIDADE,VALOR');
                InsertSQL.add(' ,ORDEMINSERCAO,CREDITOIPI,CREDITOICMS');
                InsertSQL.add(' ,CREDITOPIS,CREDITOCOFINS,IPIPAGO,CSTA,CSTB,CFOP,CSOSN');
                InsertSQL.add(' ,UNIDADE,DESCONTO,MATERIAL,material_ep,bcicms,valoricms,valorfrete,valoripi,redbaseicms,ncm,descricao)');
                InsertSQL.add('values (:CODIGO,:FERRAGEM,:PERFILADO,:PERSIANA,:VIDRO');
                InsertSQL.add(' ,:DIVERSO,:KITBOX,:ENTRADA,:QUANTIDADE,:VALOR');
                InsertSQL.add(' ,:ORDEMINSERCAO,:CREDITOIPI,:CREDITOICMS');
                InsertSQL.add(' ,:CREDITOPIS,:CREDITOCOFINS,:IPIPAGO,:CSTA,:CSTB,:CFOP');
                InsertSQL.add(' ,:CSOSN,:UNIDADE,:DESCONTO,:MATERIAL,:material_ep,:bcicms,:valoricms,:valorfrete,:valoripi,:redbaseicms,:ncm,:descricao)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABMATERIAISENTRADA set CODIGO=:CODIGO,FERRAGEM=:FERRAGEM');
                ModifySQL.add(',PERFILADO=:PERFILADO,PERSIANA=:PERSIANA,VIDRO=:VIDRO');
                ModifySQL.add(',DIVERSO=:DIVERSO,KITBOX=:KITBOX,ENTRADA=:ENTRADA,QUANTIDADE=:QUANTIDADE');
                ModifySQL.add(',VALOR=:VALOR');
                ModifySQL.add(',ORDEMINSERCAO=:ORDEMINSERCAO,CREDITOIPI=:CREDITOIPI');
                ModifySQL.add(',CREDITOICMS=:CREDITOICMS,CREDITOPIS=:CREDITOPIS,CREDITOCOFINS=:CREDITOCOFINS');
                ModifySQL.add(',IPIPAGO=:IPIPAGO,CSTA=:CSTA,CSTB=:CSTB,CFOP=:CFOP,CSOSN=:CSOSN');
                ModifySQL.add(',UNIDADE=:UNIDADE,DESCONTO=:DESCONTO,MATERIAL=:MATERIAL,material_ep=:material_ep');
                ModifySQL.add(',bcicms=:bcicms,valoricms=:valoricms,valorfrete=:valorfrete,valoripi=:valoripi,redbaseicms=:redbaseicms');
                ModifySQl.Add('ncm=:ncm,descricao=:descricao where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABMATERIAISENTRADA where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjMATERIAISENTRADA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjMATERIAISENTRADA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabMATERIAISENTRADA');
     Result:=Self.ParametroPesquisa;
end;

function TObjMATERIAISENTRADA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de MATERIAISENTRADA ';
end;


function TObjMATERIAISENTRADA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENMATERIAISENTRADA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENMATERIAISENTRADA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjMATERIAISENTRADA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    FERRAGEM.Free;
    PERFILADO.Free;
    PERSIANA.Free;
    VIDRO.Free;
    KITBOX.Free;
    DIVERSO.Free;
    ENTRADA.Free;
    CSTA.Free;
    CSTB.Free;
    CFOP.Free;
    CSOSN.Free;
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjMATERIAISENTRADA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjMATERIAISENTRADA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjMATERIAISENTRADA.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjMATERIAISENTRADA.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;

procedure TObjMATERIAISENTRADA.Submit_QUANTIDADE(parametro: string);
begin
        Self.QUANTIDADE:=Parametro;
end;
function TObjMATERIAISENTRADA.Get_QUANTIDADE: string;
begin
        Result:=Self.QUANTIDADE;
end;
procedure TObjMATERIAISENTRADA.Submit_VALOR(parametro: string);
begin
        Self.VALOR:=Parametro;
end;
function TObjMATERIAISENTRADA.Get_VALOR: string;
begin
        Result:=Self.VALOR;
end;

procedure TObjMATERIAISENTRADA.Submit_ORDEMINSERCAO(parametro: string);
begin
        Self.ORDEMINSERCAO:=Parametro;
end;
function TObjMATERIAISENTRADA.Get_ORDEMINSERCAO: string;
begin
        Result:=Self.ORDEMINSERCAO;
end;
procedure TObjMATERIAISENTRADA.Submit_CREDITOIPI(parametro: string);
begin
        Self.CREDITOIPI:=Parametro;
end;
function TObjMATERIAISENTRADA.Get_CREDITOIPI: string;
begin
        Result:=Self.CREDITOIPI;
end;
procedure TObjMATERIAISENTRADA.Submit_CREDITOICMS(parametro: string);
begin
        Self.CREDITOICMS:=Parametro;
end;
function TObjMATERIAISENTRADA.Get_CREDITOICMS: string;
begin
        Result:=Self.CREDITOICMS;
end;
procedure TObjMATERIAISENTRADA.Submit_CREDITOPIS(parametro: string);
begin
        Self.CREDITOPIS:=Parametro;
end;
function TObjMATERIAISENTRADA.Get_CREDITOPIS: string;
begin
        Result:=Self.CREDITOPIS;
end;
procedure TObjMATERIAISENTRADA.Submit_CREDITOCOFINS(parametro: string);
begin
        Self.CREDITOCOFINS:=Parametro;
end;
function TObjMATERIAISENTRADA.Get_CREDITOCOFINS: string;
begin
        Result:=Self.CREDITOCOFINS;
end;
procedure TObjMATERIAISENTRADA.Submit_IPIPAGO(parametro: string);
begin
        Self.IPIPAGO:=Parametro;
end;
function TObjMATERIAISENTRADA.Get_IPIPAGO: string;
begin
        Result:=Self.IPIPAGO;
end;

procedure TObjMATERIAISENTRADA.Submit_UNIDADE(parametro: string);
begin
        Self.UNIDADE:=Parametro;
end;
function TObjMATERIAISENTRADA.Get_UNIDADE: string;
begin
        Result:=Self.UNIDADE;
end;
procedure TObjMATERIAISENTRADA.Submit_DESCONTO(parametro: string);
begin
        Self.DESCONTO:=Parametro;
end;
function TObjMATERIAISENTRADA.Get_DESCONTO: string;
begin
        Result:=Self.DESCONTO;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjMATERIAISENTRADA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJMATERIAISENTRADA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;

procedure TObjMATERIAISENTRADA.Submit_Materaial(parametro:string);
begin
    self.MATERIAL:=parametro;
end;

function TObjMATERIAISENTRADA.Get_Material:string;
begin
    Result:=Self.MATERIAL;
end;

procedure TObjMATERIAISENTRADA.Submit_Material_ep(parametro:string);
begin
    self.MATERIAL_EP:=parametro;
end;

function TObjMATERIAISENTRADA.Get_Material_ep:string;
begin
     Result:=Self.MATERIAL_EP;
end;

procedure TObjMATERIAISENTRADA.Submit_BCIcms(parametro:string);
begin
    Self.BCICMS:=parametro;
end;

function TObjMATERIAISENTRADA.Get_BCIcms:string;
begin
    result:=self.BCICMS;
end;

procedure TObjMATERIAISENTRADA.Submit_ValorIcms(parametro:string);
begin
    Self.VALORICMS:=parametro;
end;

function TObjMATERIAISENTRADA.Get_ValorIcms:string;
begin
    Result:=self.VALORICMS;
end;

procedure TObjMATERIAISENTRADA.Submit_ValorFrete(parametro:string);
begin
    Self.VALORFRETE:=parametro;
end;

function TObjMATERIAISENTRADA.Get_ValorFrete:string;
begin
    result:=self.VALORFRETE;
end;

procedure TObjMATERIAISENTRADA.Submit_ValorIpi(parametro:string);
begin
    Self.VALORIPI:=parametro;
end;

function TObjMATERIAISENTRADA.Get_ValorIPI:string;
begin
    result:=Self.VALORIPI;
end;

procedure TObjMATERIAISENTRADA.Submit_RedBaseIcms(parametro:String);
begin
     Self.REDBASEICMS:=parametro;
end;

function TObjMATERIAISENTRADA.Get_RedBaseIcms:string;
begin
    Result:=Self.REDBASEICMS;
end;

procedure TObjMATERIAISENTRADA.Submit_NCM(parametro:string);
begin
    Self.NCM:=parametro;
end;

procedure TObjMATERIAISENTRADA.Submit_Descricao(parametro:string);
begin
    self.descricao:=parametro;
end;

function TObjMATERIAISENTRADA.Get_NCM:string;
begin
    Result:=Self.NCM;
end;

function TObjMATERIAISENTRADA.Get_Descricao:string;
begin
    Result:=Self.descricao;
end;

end.



