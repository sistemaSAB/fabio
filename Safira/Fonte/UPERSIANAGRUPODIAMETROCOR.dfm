object FPERSIANAGRUPODIAMETROCOR: TFPERSIANAGRUPODIAMETROCOR
  Left = -5
  Top = 119
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsNone
  Caption = 'CADASTRO DE PERSIANAGRUPODIAMETROCOR'
  ClientHeight = 409
  ClientWidth = 792
  Color = 13421772
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object ImageGuiaPrincipal: TImage
    Left = 25
    Top = -1
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaPrincipalClick
  end
  object ImageGuiaExtra: TImage
    Left = 25
    Top = 76
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaExtraClick
  end
  object btFechar: TSpeedButton
    Left = 754
    Top = 6
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btFecharClick
  end
  object btMinimizar: TSpeedButton
    Left = 754
    Top = 32
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btMinimizarClick
  end
  object lbAjuda: TLabel
    Left = 649
    Top = 351
    Width = 50
    Height = 13
    Caption = 'Ajuda...'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btAjuda: TSpeedButton
    Left = 719
    Top = 336
    Width = 28
    Height = 38
    Flat = True
  end
  object PainelExtra: TPanel
    Left = 40
    Top = 27
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 0
    object ImagePainelExtra: TImage
      Left = 0
      Top = 14
      Width = 98
      Height = 352
    end
  end
  object PainelPrincipal: TPanel
    Left = 40
    Top = -13
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 1
    object ImagePainelPrincipal: TImage
      Left = 1
      Top = 14
      Width = 98
      Height = 352
    end
    object btNovo: TSpeedButton
      Left = 15
      Top = 25
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btNovoClick
    end
    object btSalvar: TSpeedButton
      Left = 15
      Top = 66
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSalvarClick
    end
    object btAlterar: TSpeedButton
      Left = 15
      Top = 107
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btAlterarClick
    end
    object btCancelar: TSpeedButton
      Left = 15
      Top = 148
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btCancelarClick
    end
    object btSair: TSpeedButton
      Left = 15
      Top = 311
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSairClick
    end
    object btRelatorio: TSpeedButton
      Left = 15
      Top = 270
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btRelatorioClick
    end
    object btExcluir: TSpeedButton
      Left = 15
      Top = 229
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btExcluirClick
    end
    object btPesquisar: TSpeedButton
      Left = 15
      Top = 188
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      Layout = blGlyphRight
      OnClick = btPesquisarClick
    end
  end
  object Notebook: TNotebook
    Left = 139
    Top = 0
    Width = 612
    Height = 337
    TabOrder = 3
    object TPage
      Left = 0
      Top = 0
      Caption = 'Default'
      object Bevel1: TBevel
        Left = 0
        Top = 0
        Width = 612
        Height = 337
        Align = alClient
        Shape = bsFrame
        Style = bsRaised
      end
      object LbCodigo: TLabel
        Left = 3
        Top = 26
        Width = 40
        Height = 13
        Caption = 'Codigo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbPersiana: TLabel
        Left = 3
        Top = 50
        Width = 49
        Height = 13
        Caption = 'Persiana'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomePersiana: TLabel
        Left = 210
        Top = 54
        Width = 49
        Height = 13
        Caption = 'Persiana'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbGrupoPersiana: TLabel
        Left = 3
        Top = 74
        Width = 84
        Height = 13
        Caption = 'GrupoPersiana'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeGrupoPersiana: TLabel
        Left = 210
        Top = 77
        Width = 84
        Height = 13
        Caption = 'GrupoPersiana'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbCor: TLabel
        Left = 3
        Top = 98
        Width = 21
        Height = 13
        Caption = 'Cor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeCor: TLabel
        Left = 210
        Top = 101
        Width = 21
        Height = 13
        Caption = 'Cor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbDiametro: TLabel
        Left = 3
        Top = 122
        Width = 53
        Height = 13
        Caption = 'Di'#226'metro'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeDiametro: TLabel
        Left = 212
        Top = 123
        Width = 53
        Height = 13
        Caption = 'Di'#226'metro'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbEstoque: TLabel
        Left = 3
        Top = 146
        Width = 45
        Height = 13
        Caption = 'Estoque'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbPrecoCusto: TLabel
        Left = 219
        Top = 146
        Width = 65
        Height = 13
        Caption = 'PrecoCusto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object EdtCodigo: TEdit
        Left = 136
        Top = 26
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object EdtPersiana: TEdit
        Left = 136
        Top = 50
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 1
        OnExit = edtPersianaExit
        OnKeyDown = edtPersianaKeyDown
      end
      object EdtGrupoPersiana: TEdit
        Left = 136
        Top = 74
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 2
        OnExit = edtGrupoPersianaExit
        OnKeyDown = edtGrupoPersianaKeyDown
      end
      object EdtCor: TEdit
        Left = 136
        Top = 98
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 3
        OnExit = edtCorExit
        OnKeyDown = edtCorKeyDown
      end
      object EdtDiametro: TEdit
        Left = 136
        Top = 122
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 4
        OnExit = edtDiametroExit
        OnKeyDown = edtDiametroKeyDown
      end
      object EdtEstoque: TEdit
        Left = 136
        Top = 146
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 5
      end
      object EdtPrecoCusto: TEdit
        Left = 288
        Top = 146
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 6
      end
      object GroupBox1: TGroupBox
        Left = 4
        Top = 167
        Width = 206
        Height = 108
        Caption = 'Margem'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
        object LbPorcentagemFornecido: TLabel
          Left = 11
          Top = 50
          Width = 55
          Height = 13
          Caption = 'Fornecido'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object LbPorcentagemRetirado: TLabel
          Left = 11
          Top = 78
          Width = 48
          Height = 13
          Caption = 'Retirado'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object LbPorcentagemInstalado: TLabel
          Left = 11
          Top = 23
          Width = 53
          Height = 13
          Caption = 'Instalado'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label1: TLabel
          Left = 158
          Top = 25
          Width = 12
          Height = 13
          Caption = '%'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 158
          Top = 53
          Width = 12
          Height = 13
          Caption = '%'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 158
          Top = 83
          Width = 12
          Height = 13
          Caption = '%'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object EdtPorcentagemInstalado: TEdit
          Left = 85
          Top = 22
          Width = 72
          Height = 19
          Color = 15663069
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 0
        end
        object EdtPorcentagemFornecido: TEdit
          Left = 85
          Top = 49
          Width = 72
          Height = 19
          Color = 15663069
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 1
        end
        object EdtPorcentagemRetirado: TEdit
          Left = 85
          Top = 79
          Width = 72
          Height = 19
          Color = 15663069
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 2
        end
      end
      object GroupBox2: TGroupBox
        Left = 220
        Top = 168
        Width = 206
        Height = 108
        Caption = 'Pre'#231'o Venda'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
        object LbPrecoVendaInstalado: TLabel
          Left = 11
          Top = 18
          Width = 53
          Height = 13
          Caption = 'Instalado'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object LbPrecoVendaFornecido: TLabel
          Left = 11
          Top = 47
          Width = 55
          Height = 13
          Caption = 'Fornecido'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object LbPrecoVendaRetirado: TLabel
          Left = 11
          Top = 76
          Width = 48
          Height = 13
          Caption = 'Retirado'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 79
          Top = 21
          Width = 15
          Height = 13
          Caption = 'R$'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 79
          Top = 51
          Width = 15
          Height = 13
          Caption = 'R$'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 79
          Top = 80
          Width = 15
          Height = 13
          Caption = 'R$'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object EdtPrecoVendaInstalado: TEdit
          Left = 97
          Top = 18
          Width = 72
          Height = 19
          Color = 15663069
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 0
        end
        object EdtPrecoVendaFornecido: TEdit
          Left = 97
          Top = 48
          Width = 72
          Height = 19
          Color = 15663069
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 1
        end
        object EdtPrecoVendaRetirado: TEdit
          Left = 97
          Top = 78
          Width = 72
          Height = 19
          Color = 15663069
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 2
        end
      end
    end
  end
  object Guia: TTabSet
    Left = 141
    Top = 1
    Width = 607
    Height = 22
    BackgroundColor = 6710886
    DitherBackground = False
    EndMargin = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentBackground = True
    StartMargin = -3
    SelectedColor = 15921906
    Tabs.Strings = (
      '&1 - Principal')
    TabIndex = 0
    UnselectedColor = 13421772
    OnClick = GuiaClick
  end
end
