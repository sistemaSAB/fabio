unit UobjPERFILADO_PP_PEDIDO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJPERFILADOCOR,Math;

Type
   TObjPERFILADO_PP_PEDIDO=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                //PedidoProjeto:TOBJPEDIDO_PROJ;
                PerfiladoCor:TOBJPERFILADOCOR;

                Constructor Create;
                Destructor  Free;

                Function  DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;PControlaNegativo: Boolean;Pdata:String): boolean;
                Function  DiminuiAumentaEstoquePedido(NotaFiscal,PpedidoProjeto: String;PControlaNegativo: Boolean;Pdata:String;aumenta:String): boolean;
                function  AumentaEstoqueRomaneio(PedidoProjetoRomaneio,PpedidoProjeto: string;Pdata: String): Boolean;
                Function  Salvar(ComCommit:Boolean)       :Boolean;
                Function  LocalizaCodigo(Parametro:string) :boolean;
                Function  Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function  Get_Pesquisa                    :TStringList;
                Function  Get_TituloPesquisa              :string;

                Function  TabelaparaObjeto:Boolean;
                Procedure ZerarTabela;
                Procedure Cancelar;
                Procedure Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function  Get_Codigo: string;
                Procedure Submit_Quantidade(parametro: string);
                Function  Get_Quantidade: string;
                Procedure Submit_Valor(parametro: string);
                Function  Get_Valor: string;
                Function  Get_ValorFinal: string;
                Function  Get_PedidoProjeto:string;
                Procedure Submit_PedidoProjeto(parametro:string);


                //procedure EdtPedidoProjetoExit(Sender: TObject;LABELNOME:TLABEL);
                //procedure EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtPerfiladoCorExit(Sender: TObject;Var PEdtCodigo:TEdit;LABELNOME:TLABEL);
                procedure EdtPerfiladoCorKeyDown(Sender: TObject;var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                Procedure ResgataPerfilado_PP(PPedido, PPedidoProjeto:string);

                Function PreenchePerfiladoPP(PPedidoProjeto:string;var PValor:Currency;var Pquantidade:Currency;Paltura,Plargura,PAlturaLateral,PLarguraLateral:string;PprojetoemBranco:boolean):Boolean;
                Function DeletaPerfilado_PP(PPedidoProjeto:string):Boolean;
                Function Soma_por_PP(PpedidoProjeto:string):Currency;
                Procedure RetornaDadosRel(PpedidoProjeto:string;STrDados:TStrings;ComValor: Boolean);
                Procedure RetornaDadosRel2(PpedidoProjeto:string;STrDados:TStrings;ComValor: Boolean);
                Procedure RetornaDadosRelProjetoBranco(PpedidoProjeto:string;STrDados:TStrings; pquantidade:INTEGER;Var PTotal:Currency);
                procedure EdtperfiladoCorExit_codigo(Sender: TObject;LABELNOME: TLABEL);


         Private
               Objquery:Tibquery;
               ObjQueryPesquisa:TIBQuery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Quantidade:string;
               Valor:string;
               ValorFinal:string;
               pedidoprojeto:string;
               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UmostraStringList, UCalculaformula_Perfilado;

Function  TObjPERFILADO_PP_PEDIDO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.PedidoProjeto:=fieldbyname('PedidoProjeto').asstring;

        If(FieldByName('PerfiladoCor').asstring<>'')
        Then Begin
                 If (Self.PerfiladoCor.LocalizaCodigo(FieldByName('PerfiladoCor').asstring)=False)
                 Then Begin
                          Messagedlg('PerfiladoCor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PerfiladoCor.TabelaparaObjeto;
        End;
        Self.Quantidade:=fieldbyname('Quantidade').asstring;
        Self.Valor:=fieldbyname('Valor').asstring;
        Self.ValorFinal:=fieldbyname('ValorFinal').asstring;

        result:=True;
     End;
end;


Procedure TObjPERFILADO_PP_PEDIDO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('PedidoProjeto').asstring:=Self.PedidoProjeto;
        ParamByName('PerfiladoCor').asstring:=Self.PerfiladoCor.GET_CODIGO;
        ParamByName('Quantidade').asstring:=virgulaparaponto(Self.Quantidade);
        ParamByName('Valor').asstring:=virgulaparaponto(Self.Valor);
  End;
End;

//***********************************************************************

function TObjPERFILADO_PP_PEDIDO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPERFILADO_PP_PEDIDO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        PedidoProjeto:='';
        PerfiladoCor.ZerarTabela;
        Quantidade:='';
        Valor:='';
        ValorFinal:='';

     End;
end;

Function TObjPERFILADO_PP_PEDIDO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPERFILADO_PP_PEDIDO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     If (Self.PerfiladoCor.LocalizaCodigo(Self.PerfiladoCor.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ PerfiladoCor n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjPERFILADO_PP_PEDIDO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.PedidoProjeto<>'')
        Then Strtoint(Self.PedidoProjeto);
     Except
           Mensagem:=mensagem+'/PedidoProjeto';
     End;
     try
        If (Self.PerfiladoCor.Get_Codigo<>'')
        Then Strtoint(Self.PerfiladoCor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/PerfiladoCor';
     End;
     try
        Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPERFILADO_PP_PEDIDO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPERFILADO_PP_PEDIDO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPERFILADO_PP_PEDIDO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PERFILADO_PP vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,PedidoProjeto,PerfiladoCor,Quantidade,Valor,ValorFinal');
           SQL.ADD(' from  TABPERFILADO_PP');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPERFILADO_PP_PEDIDO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPERFILADO_PP_PEDIDO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPERFILADO_PP_PEDIDO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjQueryPesquisa;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        //Self.PedidoProjeto:=TOBJPEDIDO_PROJ.create;
        Self.PerfiladoCor:=TOBJPERFILADOCOR.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABPERFILADO_PP(Codigo,PedidoProjeto,PerfiladoCor');
                InsertSQL.add(' ,Quantidade,Valor)');
                InsertSQL.add('values (:Codigo,:PedidoProjeto,:PerfiladoCor,:Quantidade');
                InsertSQL.add(' ,:Valor)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABPERFILADO_PP set Codigo=:Codigo,PedidoProjeto=:PedidoProjeto');
                ModifySQL.add(',PerfiladoCor=:PerfiladoCor,Quantidade=:Quantidade,Valor=:Valor');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABPERFILADO_PP where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPERFILADO_PP_PEDIDO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPERFILADO_PP_PEDIDO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPERFILADO_PP');
     Result:=Self.ParametroPesquisa;
end;

function TObjPERFILADO_PP_PEDIDO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de PERFILADO_PP ';
end;


function TObjPERFILADO_PP_PEDIDO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPERFILADO_PP,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENPERFILADO_PP,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPERFILADO_PP_PEDIDO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ObjQueryPesquisa);
    Freeandnil(Self.ObjDataSource);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //Self.PedidoProjeto.FREE;
Self.PerfiladoCor.FREE;
//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPERFILADO_PP_PEDIDO.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPERFILADO_PP_PEDIDO.RetornaCampoNome: string;
begin
      result:='Nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPERFILADO_PP_PEDIDO.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjPERFILADO_PP_PEDIDO.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjPERFILADO_PP_PEDIDO.Submit_Quantidade(parametro: string);
begin
        Self.Quantidade:=Parametro;
end;
function TObjPERFILADO_PP_PEDIDO.Get_Quantidade: string;
begin
        Result:=Self.Quantidade;
end;
procedure TObjPERFILADO_PP_PEDIDO.Submit_Valor(parametro: string);
begin
        Self.Valor:=Parametro;
end;
function TObjPERFILADO_PP_PEDIDO.Get_Valor: string;
begin
        Result:=Self.Valor;
end;
//CODIFICA GETSESUBMITS


procedure TObjPERFILADO_PP_PEDIDO.EdtPerfiladoCorExit(Sender: TObject;Var PEdtCodigo:TEdit;  LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PerfiladoCor.localizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     Self.PerfiladoCor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.PerfiladoCor.Perfilado.Get_Descricao+' - '+Self.PerfiladoCor.Cor.Get_Descricao;
     PEdtCodigo.Text:=Self.PerfiladoCor.Get_Codigo;

End;
procedure TObjPERFILADO_PP_PEDIDO.EdtPerfiladoCorKeyDown(Sender: TObject;var PEdtCodigo:TEdit; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FTROCAMATERIALPEDIDOCONCLUIDO.BTTROCAR' ;
            FpesquisaLocal.PesquisaEmEstoque := True;
            If (FpesquisaLocal.PreparaPesquisa(SELF.PerfiladoCor.Get_Pesquisa,Self.PerfiladoCor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('REF_PERFILADO').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 LABELNOME.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('Perfilado').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjPERFILADO_PP_PEDIDO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPERFILADO_PP';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;
procedure TObjPERFILADO_PP_PEDIDO.ResgataPerfilado_PP(PPedido, PPedidoProjeto: string);
begin
       With Self.ObjQueryPesquisa do
       Begin
           Close;
           SQL.Clear;
           Sql.add('Select TabPerfilado.Referencia,TabPerfilado.Descricao as Perfilado,');
           Sql.add('TabCor.Descricao as Cor,');
           Sql.add('TabPerfilado_PP.Quantidade,');
           Sql.add('TabPerfilado_PP.Valor,');
           Sql.add('TabPerfilado_PP.ValorFinal,TabPedido_Proj.Codigo as PedidoProjeto,');
           Sql.Add('TabPerfilado_PP.Codigo');
           Sql.add('from TabPerfilado_PP');
           Sql.add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabPerfilado_PP.PedidoProjeto');
           Sql.add('Join TabPedido on TabPedido.Codigo = TabPedido_Proj.Pedido');
           Sql.add('Join TabProjeto on TabProjeto.Codigo = TabPedido_Proj.Projeto');
           Sql.add('Join TabPerfiladoCor on TabPerfiladoCor.Codigo = TabPerfilado_PP.PerfiladoCor');
           Sql.add('join TabPerfilado  on TabPerfilado.Codigo = TabPerfiladoCor.Perfilado');
           Sql.add('Join TabCor on TabCor.Codigo = TabPerfiladoCor.Cor');

           Sql.add('Where TabPedido_Proj.Pedido = '+PPedido);
           if (PpedidoProjeto <> '' )
           then Sql.add('and   TabPedido_Proj.Codigo ='+PPedidoProjeto);

           if (PPedido = '')then
           exit;

           Open;
       end;
end;

function TObjPERFILADO_PP_PEDIDO.DeletaPerfilado_PP(PPedidoProjeto: string): Boolean;
Begin
     Result:=False;
     With Objquery  do
     Begin
        Close;
        SQL.Clear;
        Sql.Add('Delete from TabPerfilado_PP where PedidoProjeto='+ppedidoprojeto);
        Try
            execsql;
        Except
              On E:Exception do
              Begin
                   Messagedlg('Erro na tentativa de Excluir as Perfilado do Projeto '+#13+E.message,mterror,[mbok],0);
                   exit;
              End;
        End;
     end;
     Result:=true;

end;

function TObjPERFILADO_PP_PEDIDO.PreenchePerfiladoPP(PPedidoProjeto: string;var PValor: Currency;var Pquantidade:Currency;
Paltura,Plargura,PAlturaLateral,PLarguraLateral:string;PprojetoemBranco:boolean): Boolean;
var
Pprojeto,PCorPerfilado:string;
ptipopedido,PdescricaoPerfilado:String;
Begin
    Result:=false;
    Pvalor:=0;
    Pquantidade:=0;

    //para forcar o erro caso tenho usado ondenao pode

    if (Paltura='0')
    Then Paltura:='A';

    if (Plargura='0')
    Then PLargura:='L';

    if (Palturalateral='0')
    Then Palturalateral:='A2';

    if (Plarguralateral='0')
    Then Plarguralateral:='L2';

    //Localizo o Pedido Projeto
    {if (Self.PedidoProjeto.LocalizaCodigo(PPedidoProjeto)=false)
    then exit;
    Self.PedidoProjeto.TabelaparaObjeto;}
    //**********************************

    With ObjQueryPesquisa do
    Begin

        //Apaga Apenas os Perfilados ligadas a este Pedido Projeto
        Close;
        Sql.clear;
        Sql.add('Select TabPedido_Proj.CorPerfilado,tabpedido_proj.projeto,tabCor.descricao,Tabpedido_proj.TipoPedido From Tabpedido_Proj');
        Sql.add('join TabCor on TabPedido_Proj.CorPerfilado=TabCor.codigo');


        Sql.add('Where TabPedido_Proj.Codigo='+PPedidoProjeto);
        open;
        PcorPerfilado:=Fieldbyname('corperfilado').asstring;
        Pprojeto:=Fieldbyname('projeto').asstring;
        PdescricaoPerfilado:=Fieldbyname('descricao').asstring;
        PtipoPedido:=Fieldbyname('tipopedido').asstring;


        Self.DeletaPerfilado_PP(PPedidoProjeto);
        
        if (PprojetoemBranco=True)
        Then Begin
                  result:=True;
                  exit;
        End;

        //esse sqls faz um relacionamento entre as perfilado de um projeto
        //e a tabela PerfiladoCor, para saber quais dos perfilados de um determinado
        //projeto nao esto cadastar na COR X, esse sql abaixo retorna
        //as que estaum nesse caso, para testa-lo no ibconsole
        //retire o distinct e acrescente TabPerfiladoCor.Cor para visualizar
        //e retire a clausula where

        close;
        sql.clear;
        sql.add('Select distinct(TabPerfilado_Proj.Perfilado)');
        sql.add('from TabPerfilado_Proj');
        sql.add('left join TabPerfiladoCor on');
        sql.add('(TabPerfilado_Proj.Perfilado=TabPerfiladoCor.Perfilado and TabPerfiladoCor.Cor='+PcorPerfilado+')');
        sql.add('Where TabPerfilado_Proj.Projeto='+Pprojeto);
        sql.add('and TabPerfiladoCor.Cor is null');
        open;
        last;
        if (recordcount>0)
        Then Begin
                  if (Messagedlg('Alguns Perfilados cadastrados para este projeto n�o existem na Cor Escolhida, n�o ser� poss�vel continuar.'+#13+'Deseja visualiza-las?',mtconfirmation,[mbyes,mbno],0)=mrno)
                  Then exit;
                  FmostraStringList.Memo.clear;
                  FmostraStringList.Memo.Lines.add('PERFILADOS N�O CADASTRADAO PARA A COR='+PCorPerfilado+'-'+PdescricaoPerfilado);
                  FmostraStringList.Memo.Lines.add(' ');
                  first;
                  While not(eof) do
                  Begin
                       Self.PerfiladoCor.Perfilado.LocalizaCodigo(fieldbyname('Perfilado').asstring);
                       Self.PerfiladoCor.Perfilado.TabelaparaObjeto;
                       FmostraStringList.Memo.Lines.add(Self.PerfiladoCor.Perfilado.Get_Codigo+' - '+Self.PerfiladoCor.Perfilado.Get_Descricao);
                       next;
                  End;
                  FmostraStringList.ShowModal;
                  exit;
        End;

        //Esse SQL Retorna o Codigo da PerfiladoCor,o PReco, e a quantidade (cadastrada no projeto)

        Close;
        SQL.Clear;
        SQL.Add('Select  TabPerfiladoCor.Codigo as PerfiladoCor, TabPerfilado_Proj.FormulaAltura,TabPerfilado_Proj.FormulaLargura,');

        if (PTipoPedido='INSTALADO')
        Then SQL.Add('(tabPerfilado.PRecoVendaInstalado+(Tabperfilado.PrecoVendaInstalado*TabperfiladoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        if (PTipoPedido='RETIRADO')
        Then SQL.Add('(tabperfilado.PRecoVendaretirado+(Tabperfilado.PrecoVendaretirado*TabperfiladoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        if (PTipoPedido='FORNECIDO')
        Then SQL.Add('(tabperfilado.PRecoVendafornecido+(Tabperfilado.PrecoVendafornecido*TabperfiladoCor.PorcentagemAcrescimoFinal)/100) as PRECO');
        SQL.Add(',tabperfilado_proj.quantidade, tabperfilado.kitperfilado,tabperfilado_proj.esquadria');
        SQL.Add('from  Tabperfilado_Proj');
        SQL.Add('Join  TabperfiladoCor on TabperfiladoCOr.Perfilado = Tabperfilado_Proj.perfilado');
        SQL.Add('join  Tabperfilado on Tabperfilado.Codigo = TabperfiladoCor.perfilado');
        SQL.Add('Where Tabperfilado_Proj.Projeto = '+Pprojeto);
        SQL.Add('and   TabperfiladoCor.Cor = '+PCorPerfilado);
        Open;
        first;


        // Preenchendo a TabPerfilado_PP
        While not (eof) do
        Begin
            //antes de criar um novo registro na PerfiladoPP
            //preciso calcular parea cada rregistro pelas formulas que
            //estao no perfilado_proj
            // Zero a funcao recursiva

            //Verifico se o perfilado � um kit perfilado
            //Se n�o for, preciso calcular a quantidade
            //se for n�o precisa, pois no projeto se coloca a quantidade de kitperfilado que o projeto utiliza
            if(FieldByName('kitperfilado').AsString='N')then
            begin

                  if(FieldByName('esquadria').AsString='S') then
                  begin
                      //Para o calculo das esquadrias foi preciso refazer a forma de calcular as medidas do perfilado
                      //Fcalculaformula_Perfilado.Calculaesquadria(fieldbyname('formulaaltura').asstring,fieldbyname('formulalargura').asstring,fieldbyname('formulaquantidade').asstring,Pprojeto);

                      Fcalculaformula_Perfilado.QuantidadeExecucaoRecursiva:=0;

                      Fcalculaformula_Perfilado.BTLIMPARTUDOClick(nil);
                      Fcalculaformula_Perfilado.edtvariavel.Text:='QTDE';

                      Fcalculaformula_Perfilado.edtformulaaltura.TEXT:=fieldbyname('formulaaltura').asstring;
                      Fcalculaformula_Perfilado.edtformulalargura.text:=fieldbyname('formulalargura').asstring;


                      Fcalculaformula_Perfilado.edtaltura.text:=Paltura;
                      Fcalculaformula_Perfilado.edtlargura.text:=Plargura;

                      Fcalculaformula_Perfilado.edtalturalateral.text:=PAlturaLateral;
                      Fcalculaformula_Perfilado.edtlarguralateral.text:=PLarguraLateral;

                      Fcalculaformula_Perfilado.btinsereformulaClick(nil);


                      if (Fcalculaformula_Perfilado.Calcula_Area=False)
                      Then Begin
                                Messagedlg('Erro no C�lcula da F�rmula do Perfilado!',mterror,[mbok],0);
                                exit;
                      End;
                      //tenho a primeira linha dos dois string list com os resultados, basta soma-los
                      //Fcalculaformula_Perfilado.SHOWMODAL;

                      Pquantidade:=0;
                      Try
                          Pquantidade:=strtofloat(Fcalculaformula_Perfilado.lbaltura.Items[0])*strtofloat(Fcalculaformula_Perfilado.lblargura.Items[0]);
                      Except
                          Messagedlg('Erro na F�rmula',mterror,[mbok],0);
                          exit;
                      End;
                      //essa qtde esta em milimetros, mas preciso converte-la
                      //para metro pois o valor � calculado por metro

                      Try
                         Pquantidade:=Pquantidade/1000;
                         //Arredondo para cima
                         Pquantidade:=Ceil(Pquantidade);

                      Except
                            Pquantidade:=0;
                      End;
                  end
                  else
                  begin
                      Fcalculaformula_Perfilado.QuantidadeExecucaoRecursiva:=0;

                      Fcalculaformula_Perfilado.BTLIMPARTUDOClick(nil);
                      Fcalculaformula_Perfilado.edtvariavel.Text:='QTDE';

                      Fcalculaformula_Perfilado.edtformulaaltura.TEXT:=fieldbyname('formulaaltura').asstring;
                      Fcalculaformula_Perfilado.edtformulalargura.text:=fieldbyname('formulalargura').asstring;


                      Fcalculaformula_Perfilado.edtaltura.text:=Paltura;
                      Fcalculaformula_Perfilado.edtlargura.text:=Plargura;

                      Fcalculaformula_Perfilado.edtalturalateral.text:=PAlturaLateral;
                      Fcalculaformula_Perfilado.edtlarguralateral.text:=PLarguraLateral;

                      Fcalculaformula_Perfilado.btinsereformulaClick(nil);


                      if (Fcalculaformula_Perfilado.Calcula_Area=False)
                      Then Begin
                                Messagedlg('Erro no C�lcula da F�rmula do Perfilado!',mterror,[mbok],0);
                                exit;
                      End;
                      //tenho a primeira linha dos dois string list com os resultados, basta soma-los
                      //Fcalculaformula_Perfilado.SHOWMODAL;

                      Pquantidade:=0;
                      Try
                          Pquantidade:=strtofloat(Fcalculaformula_Perfilado.lbaltura.Items[0])+strtofloat(Fcalculaformula_Perfilado.lblargura.Items[0]);
                      Except
                          Messagedlg('Erro na F�rmula',mterror,[mbok],0);
                          exit;
                      End;
                      //essa qtde esta em milimetros, mas preciso converte-la
                      //para metro pois o valor � calculado por metro

                      Try
                         Pquantidade:=Pquantidade/1000;
                      Except
                            Pquantidade:=0;
                      End;
                  end;


            end
            else
            begin
                Pquantidade:=fieldbyname('quantidade').AsCurrency;
            end;

            Self.Status:=dsInsert;
            Self.Submit_Codigo(Self.Get_NovoCodigo);
            Self.Submit_PedidoProjeto(PPedidoProjeto);
            Self.perfiladoCor.Submit_Codigo(fieldbyname('perfiladoCor').AsString);
            Self.Submit_Quantidade(floattostr(pquantidade));
            Self.Submit_Valor(formata_valor(fieldbyname('Preco').AsString,2,True));
            if Self.Salvar(false)=false then
            Begin
                 Result:=false;
                 exit;
            end;
            Next;
        end;
        pvalor:=0;
        Pvalor:=Self.Soma_por_PP(PPedidoProjeto);
        result:=true;
    end;
end;
function TObjPERFILADO_PP_PEDIDO.Get_ValorFinal: string;
begin
     Result:=Self.ValorFinal;
end;


function TObjPERFILADO_PP_PEDIDO.Soma_por_PP(PpedidoProjeto: string): Currency;
begin

     Self.Objquery.close;
     Self.Objquery.sql.clear;
     Self.Objquery.sql.add('Select sum(valorfinal) as SOMA from TabPerfilado_PP where PedidoProjeto='+PpedidoProjeto);
     Try
         Self.Objquery.open;
         Result:=Self.Objquery.fieldbyname('soma').asfloat;
     Except
           Result:=0;
     End;

end;

procedure TObjPERFILADO_PP_PEDIDO.RetornaDadosRel(PpedidoProjeto: string;STrDados: TStrings;ComValor: Boolean);
begin
     if (PpedidoProjeto='')
     Then exit;

     With Self.ObjQueryPesquisa do
     Begin
          close;
          SQL.clear;
          sql.add('Select codigo from TabPerfilado_pp where PedidoProjeto='+PpedidoProjeto);
          open;
          first;

          if (RecordCount = 0)then
          exit;

          While not(eof) do
          Begin
               if(ComValor=True) then
               begin
                   Self.LocalizaCodigo(fieldbyname('codigo').asstring);
                   Self.TabelaparaObjeto;
                   STrDados.Add(CompletaPalavra(Self.PerfiladoCor.Perfilado.Get_Referencia,10,' ')+' '+
                                CompletaPalavra(Self.PerfiladoCor.Cor.Get_Referencia+'-'+Self.PerfiladoCor.Cor.Get_Descricao,16,' ')+' '+
                                CompletaPalavra(Self.PerfiladoCor.Perfilado.Get_Descricao,45,' ')+' '+
                                CompletaPalavra(Self.Get_Quantidade+' '+Self.PerfiladoCor.Perfilado.Get_Unidade,6,' ')+' '+
                                CompletaPalavra_a_Esquerda(formata_valor(Self.Get_Valor),10,' ')+' '+
                                CompletaPalavra_a_Esquerda(formata_valor(StrToCurr(Self.Get_Quantidade)*StrToCurr(Self.Get_Valor)),8,' '));
               end
               else
               begin
                   Self.LocalizaCodigo(fieldbyname('codigo').asstring);
                   Self.TabelaparaObjeto;
                   STrDados.Add(CompletaPalavra(Self.PerfiladoCor.Perfilado.Get_Referencia,10,' ')+' '+
                                CompletaPalavra(Self.PerfiladoCor.Cor.Get_Referencia+'-'+Self.PerfiladoCor.Cor.Get_Descricao,16,' ')+' '+
                                CompletaPalavra(Self.PerfiladoCor.Perfilado.Get_Descricao,45,' ')+' '+
                                CompletaPalavra(Self.Get_Quantidade+' '+Self.PerfiladoCor.Perfilado.Get_Unidade,6,' '));
               end;
              next;
          end;
          if(ComValor=True) then
          begin
                close;
                sql.clear;
                sql.add('Select sum(ValorFinal) as SOMA FROM TabPerfilado_PP where PedidoProjeto='+PpedidoProjeto);
                open;
                STrDados.Add('?'+CompletaPalavra_a_Esquerda('Total PERFILADOS: '+formata_valor(fieldbyname('soma').asstring),100,' '));
                STrDados.Add(' ');
          end;
    end;

end;

function TObjPERFILADO_PP_PEDIDO.AumentaEstoqueRomaneio(PedidoProjetoRomaneio,PpedidoProjeto: string;Pdata: String): boolean;
var
PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          Sql.add('Select codigo,perfiladocor,quantidade from Tabperfilado_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;
          first;
          While not(eof) do
          Begin

              Self.perfiladoCor.LocalizaCodigo(fieldbyname('perfiladocor').asstring);
              Self.perfiladoCor.TabelaparaObjeto;

              try
                 PestoqueAtual:=strtofloat(Self.perfiladoCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual do perfilado '+Self.perfiladoCor.perfilado.Get_Descricao+' da cor '+Self.perfiladoCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              Try
                PnovoEstoque:=PestoqueAtual+fieldbyname('quantidade').asfloat
              Except
                    Messagedlg('Erro na gera��o do novo Estoque do perfilado '+Self.perfiladoCor.perfilado.Get_Descricao+' da cor '+Self.perfiladoCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              if(PerfiladoCor.AumentaEstoque(fieldbyname('quantidade').asstring,fieldbyname('perfiladocor').asstring)=False)
              then Exit;

              next;
          End;
          result:=True;
          exit;
     End;
end;

function TObjPERFILADO_PP_PEDIDO.DiminuiAumentaEstoquePedido(NotaFiscal,PpedidoProjeto: String;PControlaNegativo: Boolean;Pdata:String;aumenta:String): boolean;
var
PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          if(aumenta='N')
          then Sql.add('Select codigo,perfiladocor,(quantidade*-1) as quantidade from Tabperfilado_PP where PedidoProjeto='+PpedidoProjeto)
          else Sql.add('Select codigo,perfiladocor,quantidade from Tabperfilado_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;
          first;
          While not(eof) do
          Begin
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Status:=dsinsert;
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(fieldbyname('quantidade').asstring);
              OBJESTOQUEGLOBAL.Submit_DATA(Pdata);
              OBJESTOQUEGLOBAL.Submit_PERFILADOCOR(fieldbyname('perfiladocor').asstring);
              OBJESTOQUEGLOBAL.Submit_PERFILADO_PP(fieldbyname('codigo').asstring);
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('PEDIDO (NOTA FISCAL)');
              OBJESTOQUEGLOBAL.Submit_NotaFiscal(NotaFiscal);

              Self.perfiladoCor.LocalizaCodigo(fieldbyname('perfiladocor').asstring);
              Self.perfiladoCor.TabelaparaObjeto;

              try
                 PestoqueAtual:=strtofloat(Self.perfiladoCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual do perfilado '+Self.perfiladoCor.perfilado.Get_Descricao+' da cor '+Self.perfiladoCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              Try
                PnovoEstoque:=PestoqueAtual-fieldbyname('quantidade').asfloat
              Except
                    Messagedlg('Erro na gera��o do novo Estoque do perfilado '+Self.perfiladoCor.perfilado.Get_Descricao+' da cor '+Self.perfiladoCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              if (Pnovoestoque<0)
              then Begin
                        if (PControlaNegativo=True)
                        Then begin
                                  Messagedlg('O perfilado '+Self.perfiladoCor.perfilado.Get_Descricao+' da cor '+Self.perfiladoCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                  exit;
                        End;
              End;

              if (OBJESTOQUEGLOBAL.Salvar(False)=False)
              then exit;

              next;
          End;
          result:=True;
          exit;
     End;
end;



function TObjPERFILADO_PP_PEDIDO.DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;PControlaNegativo: Boolean;Pdata:String): boolean;
var
PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          Sql.add('Select codigo,perfiladocor,(quantidade*-1) as quantidade from Tabperfilado_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;
          first;
          While not(eof) do
          Begin
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Status:=dsinsert;
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(fieldbyname('quantidade').asstring);
              OBJESTOQUEGLOBAL.Submit_DATA(Pdata);
              OBJESTOQUEGLOBAL.Submit_PERFILADOCOR(fieldbyname('perfiladocor').asstring);
              OBJESTOQUEGLOBAL.Submit_PEDIDOPROJETOROMANEIO(PpedidoProjetoRomaneio);
              OBJESTOQUEGLOBAL.Submit_PERFILADO_PP(fieldbyname('codigo').asstring);
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('ROMANEIO');

              Self.perfiladoCor.LocalizaCodigo(fieldbyname('perfiladocor').asstring);
              Self.perfiladoCor.TabelaparaObjeto;

              try
                 PestoqueAtual:=strtofloat(Self.perfiladoCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual do perfilado '+Self.perfiladoCor.perfilado.Get_Descricao+' da cor '+Self.perfiladoCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              Try
                PnovoEstoque:=PestoqueAtual+fieldbyname('quantidade').asfloat
              Except
                    Messagedlg('Erro na gera��o do novo Estoque do perfilado '+Self.perfiladoCor.perfilado.Get_Descricao+' da cor '+Self.perfiladoCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              if (Pnovoestoque<0)
              then Begin
                        if (PControlaNegativo=True)
                        Then begin
                                  Messagedlg('O perfilado '+Self.perfiladoCor.perfilado.Get_Descricao+' da cor '+Self.perfiladoCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                  exit;
                        End;
              End;

             // PerfiladoCor.DiminuiEstoque(fieldbyname('quantidade').asstring,fieldbyname('perfiladocor').asstring);
              if (OBJESTOQUEGLOBAL.Salvar(False)=False)
              then exit;

              next;
          End;
          result:=True;
          exit;
     End;
end;

procedure TObjPERFILADO_PP_PEDIDO.RetornaDadosRelProjetoBranco(PpedidoProjeto: string; STrDados: TStrings;pquantidade:INTEGER; Var PTotal: Currency);
begin
  if (PpedidoProjeto='') then
    Exit;

  with Self.ObjQueryPesquisa do
  begin
    Close;
    SQL.Clear;
    SQL.Text := ' select f.referencia, c.referencia || ' + QuotedStr('-') + ' || c.descricao as cor, '+
                ' f.descricao, f.unidade, fp.valor, sum(fp.quantidade) * cast(:qtde as numeric(9,2)) as quantidade, '+
                ' sum(fp.quantidade) * cast(:qtde as numeric(9,2)) * fp.valor as mult '+
                ' from tabperfilado_pp fp '+
                ' inner join tabperfiladocor fc on (fp.perfiladocor = fc.codigo) '+
                ' inner join tabcor c on (fc.cor = c.codigo) '+
                ' inner join tabperfilado f on (fc.perfilado = f.codigo) '+
                ' where fp.pedidoprojeto = :pedidoprojeto '+
                ' group by 1,2,3,4,5 ';

    ParamByName('qtde').AsInteger := pquantidade;
    ParamByName('pedidoprojeto').AsString := PpedidoProjeto;
    Open;
    First;
    if (RecordCount = 0)then
      Exit;

    STrDados.Add('?'+CompletaPalavra('�TEM',8,' ')+' '+
                 CompletaPalavra('  COR',14,' ')+' '+
                 CompletaPalavra('   DESCRI��O',39,' ')+' '+
                 '          QTDE      VAL.UN     VALOR');


    while not(eof) do
    begin
      STrDados.Add(CompletaPalavra(fieldbyname('referencia').asstring,10,' ')+' '+
                   CompletaPalavra(fieldbyname('cor').asstring,15,' ')+' '+
                   CompletaPalavra(fieldbyname('descricao').asstring,44,' ')+' '+
                   CompletaPalavra(fieldbyname('quantidade').asstring+' '+fieldbyname('unidade').asstring,6,' ')+' '+
                   CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),10,' ')+' '+
                   CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('mult').asstring),10,' '));
      Next;
    end;
    Close;
    SQL.Clear;
    SQL.add('Select sum(ValorFinal) as SOMA FROM TabPerfilado_PP where PedidoProjeto='+PpedidoProjeto);
    open;
    PTotal:=PTotal+(fieldbyname('soma').AsCurrency*pquantidade);
  end;
end;

function TObjPERFILADO_PP_PEDIDO.Get_PedidoProjeto: string;
begin
     Result:=Self.pedidoprojeto;
end;

procedure TObjPERFILADO_PP_PEDIDO.Submit_PedidoProjeto(parametro: string);
begin
     Self.pedidoprojeto:=parametro;
end;


procedure TObjperfilado_PP_PEDIDO.EdtperfiladoCorExit_codigo(Sender: TObject;LABELNOME: TLABEL);
begin
     labelnome.caption:='';

     If (Tedit(Sender).Text='')
     Then exit;

     If (Self.perfiladoCor.localizacodigo (Tedit(Sender).Text)=False)
     Then Begin
               Tedit(Sender).Text:='';
               exit;
     End;
     Self.perfiladoCor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.perfiladoCor.perfilado.Get_Descricao+'-'+Self.perfiladoCor.Cor.Get_Descricao;
     Tedit(Sender).Text:=Self.perfiladoCor.Get_Codigo;
end;

procedure TObjPERFILADO_PP_PEDIDO.RetornaDadosRel2(PpedidoProjeto: string;
  STrDados: TStrings; ComValor: Boolean);
var
  projeto:string;
begin
  if (PpedidoProjeto='') then
    Exit;

  projeto:=get_campoTabela('projeto','codigo','tabpedido_proj',PpedidoProjeto);

  with Self.ObjQueryPesquisa do
  begin
    Close;
    SQL.Clear;
    SQL.Text := ' select f.referencia, c.referencia || ' + QuotedStr('-') + ' || c.descricao as cor, '+
                ' f.descricao, f.unidade, fp.valor, sum(fp.quantidade) as quantidade, '+
                ' sum(fp.quantidade) * fp.valor as mult, (f.peso*fp.quantidade) as kilo '+
                ' from tabperfilado_pp fp '+
                ' inner join tabperfiladocor fc on (fp.perfiladocor = fc.codigo) '+
                ' inner join tabcor c on (fc.cor = c.codigo) '+
                ' inner join tabperfilado f on (fc.perfilado = f.codigo) '+
                ' where fp.pedidoprojeto = :pedidoprojeto '+
                ' group by 1,2,3,4,5,kilo ';

    ParamByName('pedidoprojeto').AsString := PpedidoProjeto;
    //inputbox('','',SQL.Text);
    Open;
    First;
    if (RecordCount = 0)then
      Exit;

    while not(eof) do
    begin
      if(ComValor=True) then
      begin

        if(get_campoTabela('esquadria','codigo','tabprojeto',projeto)='S') then
        begin
          STrDados.Add(CompletaPalavra(fieldbyname('referencia').asstring,10,' ')+' '+
                     CompletaPalavra(fieldbyname('cor').asstring,15,' ')+' '+
                     CompletaPalavra(fieldbyname('descricao').asstring,35,' ')+' '+
                     CompletaPalavra(fieldbyname('quantidade').asstring+' '+fieldbyname('unidade').asstring+' ('+ArredondaValor(fieldbyname('kilo').Ascurrency)+' kg)',15,' ')+' '+
                     CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),10,' ')+' '+
                     CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('mult').asstring),10,' '));
        end
        else
        begin
          STrDados.Add(CompletaPalavra(fieldbyname('referencia').asstring,10,' ')+' '+
                     CompletaPalavra(fieldbyname('cor').asstring,15,' ')+' '+
                     CompletaPalavra(fieldbyname('descricao').asstring,44,' ')+' '+
                     CompletaPalavra(fieldbyname('quantidade').asstring+' '+fieldbyname('unidade').asstring,6,' ')+' '+
                     CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),10,' ')+' '+
                     CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('mult').asstring),10,' '));
        end;
      end
      else begin
        STrDados.Add(CompletaPalavra(fieldbyname('referencia').asstring,10,' ')+' '+
                     CompletaPalavra(fieldbyname('cor').asstring,16,' ')+' '+
                     CompletaPalavra(fieldbyname('descricao').asstring,45,' ')+' '+
                     CompletaPalavra(fieldbyname('quantidade').asstring+' '+fieldbyname('unidade').asstring,6,' '));
      end;
      Next;
    end;
    if(ComValor=True) then
    begin
      Close;
      sql.clear;
      sql.add('Select sum(ValorFinal) as SOMA FROM TabPerfilado_PP where PedidoProjeto='+PpedidoProjeto);
      open;
      STrDados.Add('?'+CompletaPalavra_a_Esquerda('Total PERFILADOS: '+formata_valor(fieldbyname('soma').asstring),100,' '));
      STrDados.Add(' ');
    end;
  end;
end;

end.



