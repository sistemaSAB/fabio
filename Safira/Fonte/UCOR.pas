unit UCOR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjCOR,
  UessencialGlobal, Tabs;

type
  TFCOR = class(TForm)
    ColorDialog: TColorDialog;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigoCor: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    edtReferencia: TEdit;
    edtDescricao: TEdit;
    edtPorcentagemAcrescimo: TEdit;
    edtCodigoCorDelphi: TEdit;
    lb2: TLabel;
    lbLbPorcentagemAcrescimo: TLabel;
    lbLbDescricao: TLabel;
    lbLbReferencia: TLabel;
    SpeedButton1: TSpeedButton;
    btAjuda: TSpeedButton;
//DECLARA COMPONENTES

    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure btAjudaClick(Sender: TObject);
    procedure edtPorcentagemAcrescimoKeyPress(Sender: TObject;
      var Key: Char);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCOR: TFCOR;
  ObjCOR:TObjCOR;

implementation

uses Upesquisa, UessencialLocal, UDataModulo, UescolheImagemBotao,
  UpesquisaMenu, Uprincipal, UAjuda;

{$R *.dfm}



procedure TFCOR.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     ColocaUpperCaseEdit(Self);

     Try
        ObjCOR:=TObjCOR.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     habilita_botoes(Self);
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE COR')=False)
     Then desab_botoes(Self);

    habilita_botoes(Self);
    LimpaLabels;
    FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
    FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
    if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE COR')=False)
    Then desab_botoes(Self);

    if(Tag<>0)
    then begin
        if(ObjCOR.LocalizaCodigo(IntToStr(tag))=True)
        then begin
              ObjCOR.TabelaparaObjeto;
              Self.ObjetoParaControles;

        end;
    end;

end;

procedure TFCOR.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjCOR=Nil)
     Then exit;

     If (ObjCOR.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjCOR.free;

end;


procedure TFCOR.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);
     SpeedButton1.Enabled:=True;
     edtCodigoCorDelphi.Color:=clWhite;
     edtCodigoCorDelphi.Text:='clWhite';
     lbCodigoCor.caption:='0';


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjCOR.status:=dsInsert;
     EdtReferencia.setfocus;
     edtCodigoCorDelphi.Enabled:=false;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;


end;

procedure TFCOR.btSalvarClick(Sender: TObject);
begin

     If ObjCOR.Status=dsInactive
     Then exit;

     If (EdtReferencia.Text<>'')then begin
         If (ObjCor.VerificaReferencia(ObjCor.Status, lbCodigoCor.caption, edtReferencia.Text)=true) then  
         Begin
             MensagemErro('Refer�ncia j� existe.');
             exit;
         end;
     end;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
     End;

     If (ObjCOR.salvar(true)=False)
     Then exit;

     lbCodigoCor.caption:=ObjCOR.Get_codigo;
     habilita_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
     edtCodigoCorDelphi.Enabled:=false;
     ObjetoParaControles;
     //edtCodigoCorDelphi.Font.Color:=
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;

end;

procedure TFCOR.btAlterarClick(Sender: TObject);
begin
    If (ObjCOR.Status=dsinactive) and (lbCodigoCor.caption<>'')
    Then
    Begin
                habilita_campos(Self);
                ObjCOR.Status:=dsEdit;
                edtReferencia.setfocus;
                desab_botoes(Self);
                SpeedButton1.Enabled:=True;
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtCodigoCorDelphi.Enabled:=false;
                ObjCOR.ReferenciaAnterior:=EdtReferencia.Text;
                btnovo.Visible :=false;
                btalterar.Visible:=false;
                btpesquisar.Visible:=false;
                btrelatorio.Visible:=false;
                btexcluir.Visible:=false;
                btsair.Visible:=false;
                btopcoes.Visible :=false;

     End;

end;

procedure TFCOR.btCancelarClick(Sender: TObject);
begin
     ObjCOR.cancelar;
     edtCodigoCorDelphi.Color:=clWindow;
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     edtCodigoCorDelphi.Enabled:=false;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;

end;

procedure TFCOR.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjCOR.Get_pesquisa,ObjCOR.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjCOR.status<>dsinactive
                                  then exit;

                                  If (ObjCOR.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjCOR.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
        edtCodigoCorDelphi.Enabled:=false;
end;

procedure TFCOR.btExcluirClick(Sender: TObject);
begin
     If (ObjCOR.status<>dsinactive) or (lbCodigoCor.caption='')
     Then exit;

     If (ObjCOR.LocalizaCodigo(lbCodigoCor.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjCOR.exclui(lbCodigoCor.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
     edtCodigoCorDelphi.Enabled:=false;

end;

procedure TFCOR.btRelatorioClick(Sender: TObject);
begin
//    ObjCOR.Imprime;
    edtCodigoCorDelphi.Enabled:=false;
end;

procedure TFCOR.btSairClick(Sender: TObject);
begin
    Self.close;
    edtCodigoCorDelphi.Enabled:=false;
end;

procedure TFCOR.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFCOR.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFCOR.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    If Key=VK_Escape
    Then Self.Close;

    if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    If (Key = VK_F2) then
    begin

           FAjuda.PassaAjuda('CADASTRO DE COMPONENTES');
           FAjuda.ShowModal;
    end;


end;

procedure TFCOR.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFCOR.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjCOR do
    Begin
        Submit_Codigo(lbCodigoCor.caption);
        Submit_Referencia(edtReferencia.text);
        Submit_Descricao(edtDescricao.text);
        Submit_PorcentagemAcrescimo(edtPorcentagemAcrescimo.text);
        Submit_CodigoCorDelphi(edtCodigoCorDelphi.Text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFCOR.LimpaLabels;
begin
   lbCodigoCor.Caption:='';
end;

function TFCOR.ObjetoParaControles: Boolean;
begin
  Try
     With ObjCOR do
     Begin
        lbCodigoCor.caption:=Get_Codigo;
        EdtReferencia.text:=Get_Referencia;
        EdtDescricao.text:=Get_Descricao;
        EdtPorcentagemAcrescimo.text:=Get_PorcentagemAcrescimo;
        EdtCodigoCorDelphi.Color:=StringToColor(Get_CodigoCorDelphi);
        edtCodigoCorDelphi.Font.Color:=StringToColor(Get_CodigoCorDelphi);
        edtCodigoCorDelphi.Text:='';
        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFCOR.TabelaParaControles: Boolean;
begin
     If (ObjCOR.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFCOR.SpeedButton1Click(Sender: TObject);
begin
if ColorDialog.Execute
then
begin
    edtCodigoCorDelphi.Color:=ColorDialog.Color;
    edtCodigoCorDelphi.Text:= ColorToString(ColorDialog.Color);
    edtCodigoCorDelphi.Font.Color:= ColorDialog.Color;
end;
end;

procedure TFCOR.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjCor.PrimeiroRegistro = false)then
    exit;

    ObjCor.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFCOR.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigoCor.caption='')then
    lbCodigoCor.caption:='0';

    if  (ObjCor.RegistoAnterior(StrToInt(lbCodigoCor.caption)) = false)then
    exit;

    ObjCor.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFCOR.btProximoClick(Sender: TObject);
begin
    if (lbCodigoCor.caption='')then
    lbCodigoCor.caption:='0';

    if  (ObjCor.ProximoRegisto(StrToInt(lbCodigoCor.caption)) = false)then
    exit;

    ObjCor.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFCOR.btUltimoClick(Sender: TObject);
begin
    if  (ObjCor.UltimoRegistro = false)then
    exit;

    ObjCor.TabelaparaObjeto;
    Self.ObjetoParaControles;
    
end;

procedure TFCOR.btAjudaClick(Sender: TObject);
begin
        FAjuda.PassaAjuda('CADASTRO DE COR');
        FAjuda.ShowModal;
end;

procedure TFCOR.edtPorcentagemAcrescimoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

end.
