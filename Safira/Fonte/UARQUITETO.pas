unit UARQUITETO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls,db, UObjARQUITETO,
  UessencialGlobal,UessencialLocal, Tabs,IBQuery;

type
  TFARQUITETO = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigoArquiteto: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    edtNome: TEdit;
    lbNome: TLabel;
    edtCPF: TEdit;
    lbCPF: TLabel;
    edtRG: TEdit;
    lbRG: TLabel;
    edtDataNascimento: TMaskEdit;
    lbDataNascimento: TLabel;
    edtCelular: TEdit;
    lbCelular: TLabel;
    edtTelefone: TEdit;
    lbTelefone: TLabel;
    lbEndereco: TLabel;
    edtEndereco: TEdit;
    edtNumero: TEdit;
    lbNumero: TLabel;
    mmoObservacao: TMemo;
    lbLbObservacao: TLabel;
    edtEstado: TEdit;
    lbEstado: TLabel;
    edtCidade: TEdit;
    lbCidade: TLabel;
    edtBairro: TEdit;
    lbBairro: TLabel;
    edtEmail: TEdit;
    lb2: TLabel;
    pnl1: TPanel;
    imgrodape: TImage;
    lb14: TLabel;
    btAjuda: TSpeedButton;
//DECLARA COMPONENTES

    procedure FormShow(Sender: TObject);
    procedure li(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btAjudaClick(Sender: TObject);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    procedure MostraQuantidadeArquitetos;
    { Private declarations }
  public

  end;

var
  FARQUITETO: TFARQUITETO;
  ObjARQUITETO:TObjARQUITETO;

implementation

uses Upesquisa, UDataModulo, UescolheImagemBotao, Uprincipal, UAjuda;

{$R *.dfm}





procedure TFARQUITETO.FormShow(Sender: TObject);
begin
    limpaedit(Self);
    Self.limpaLabels;
    desabilita_campos(Self);

    if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE ARQUITETO')=False)
    Then  desab_botoes(Self)
    Else  habilita_botoes(Self);

    Try
       ObjARQUITETO:=TObjARQUITETO.create;
    Except
          Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
          Self.close;
    End;

   FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
   FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
   lbCodigoArquiteto.caption:='';

   MostraQuantidadeArquitetos;

   if(Tag<>0)
   then begin
         if(ObjARQUITETO.LocalizaCodigo(IntToStr(Tag))=True)then
         begin
              ObjARQUITETO.TabelaparaObjeto;
              self.ObjetoParaControles;
         end;

   end;

end;



procedure TFARQUITETO.li(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjARQUITETO=Nil)
     Then exit;

     If (ObjARQUITETO.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjARQUITETO.free;
    self.Tag:=0;
   
end;





procedure TFARQUITETO.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);
     lbCodigoArquiteto.caption:='0';
     //edtcodigo.text:=ObjARQUITETO.Get_novocodigo;
     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;
      btalterar.visible:=false;
     btrelatorio.visible:=false;
     btsair.Visible:=false;
     btopcoes.visible:=false;
     btexcluir.Visible:=false;
     btnovo.Visible:=false;
     btpesquisar.Visible:=false;

     ObjARQUITETO.status:=dsInsert;
     EdtNome.setfocus;

end;

procedure TFARQUITETO.btSalvarClick(Sender: TObject);
begin

     If ObjARQUITETO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjARQUITETO.salvar(true)=False)
     Then exit;

     lbCodigoArquiteto.caption:=ObjARQUITETO.Get_codigo;
     habilita_botoes(Self);
     btalterar.visible:=true;
     btrelatorio.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:= true;
     btpesquisar.Visible:=true;
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     MostraQuantidadeArquitetos;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFARQUITETO.btAlterarClick(Sender: TObject);
begin
    If (ObjARQUITETO.Status=dsinactive) and (lbCodigoArquiteto.caption<>'')
    Then Begin
                habilita_campos(Self);

                ObjARQUITETO.Status:=dsEdit;
                EdtNome.setfocus;
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btalterar.visible:=false;
                btrelatorio.visible:=false;
                btsair.Visible:=false;
                btopcoes.visible:=false;
                btexcluir.Visible:=false;
                btnovo.Visible:=false;
                btpesquisar.Visible:=false;

    End;

end;

procedure TFARQUITETO.btCancelarClick(Sender: TObject);
begin
     ObjARQUITETO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btalterar.visible:=true;
     btrelatorio.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:= true;
     btpesquisar.Visible:=true;

end;

procedure TFARQUITETO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjARQUITETO.Get_pesquisa,ObjARQUITETO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjARQUITETO.status<>dsinactive
                                  then exit;

                                  If (ObjARQUITETO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjARQUITETO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFARQUITETO.btExcluirClick(Sender: TObject);
begin
     If (ObjARQUITETO.status<>dsinactive) or (lbCodigoArquiteto.caption='')
     Then exit;

     If (ObjARQUITETO.LocalizaCodigo(lbCodigoArquiteto.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjARQUITETO.exclui(lbCodigoArquiteto.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFARQUITETO.btRelatorioClick(Sender: TObject);
begin
//    ObjARQUITETO.Imprime(lbCodigoArquiteto.caption);
end;

procedure TFARQUITETO.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFARQUITETO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFARQUITETO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFARQUITETO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;

    if(key=vk_f1) then
    begin
        Fprincipal.chamaPesquisaMenu;
    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('ARQUITETO');
         FAjuda.ShowModal;
    end;
end;

procedure TFARQUITETO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFARQUITETO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjARQUITETO do
    Begin
        Submit_CODIGO(lbCodigoArquiteto.caption);
        Submit_Nome(edtNome.text);
        Submit_CPF(edtCPF.text);
        Submit_RG(edtRG.text);
        Submit_DataNascimento(edtDataNascimento.text);
        Submit_Telefone(edtTelefone.text);
        Submit_Celular(edtCelular.text);
        Submit_Endereco(edtEndereco.text);
        Submit_Numero(edtNumero.text);
        Submit_Bairro(edtBairro.text);
        Submit_Cidade(edtCidade.text);
        Submit_Estado(edtEstado.text);
        Submit_Observacao(MmoObservacao.text);
        Submit_Email(edtEmail.Text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFARQUITETO.LimpaLabels;
begin
//LIMPA LABELS
end;

function TFARQUITETO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjARQUITETO do
     Begin
        lbCodigoArquiteto.caption:=Get_CODIGO;
        EdtNome.text:=Get_Nome;
        EdtCPF.text:=Get_CPF;
        EdtRG.text:=Get_RG;
        EdtDataNascimento.text:=Get_DataNascimento;
        EdtTelefone.text:=Get_Telefone;
        EdtCelular.text:=Get_Celular;
        EdtEndereco.text:=Get_Endereco;
        EdtNumero.text:=Get_Numero;
        EdtBairro.text:=Get_Bairro;
        EdtCidade.text:=Get_Cidade;
        EdtEstado.text:=Get_Estado;
        MmoObservacao.text:=Get_Observacao;
        edtEmail.text:=Get_Email;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFARQUITETO.TabelaParaControles: Boolean;
begin
     If (ObjARQUITETO.TabelaparaObjeto=False) Then
     Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)Then
     Begin
                result:=False;
                exit;
     End;
     Result:=True;

end;

procedure TFARQUITETO.MostraQuantidadeArquitetos;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabarquiteto');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb14.Caption:='Existem '+IntToStr(contador)+' arquitetos cadastrados'
       else
       begin
            lb14.Caption:='Existe '+IntToStr(contador)+' arquiteto cadastrado';
       end;

    finally

    end;
end;

procedure TFARQUITETO.btAjudaClick(Sender: TObject);
begin
         FAjuda.PassaAjuda('ARQUITETO');
         FAjuda.ShowModal;
end;

end.
