object FEMPRESA: TFEMPRESA
  Left = 522
  Top = 184
  Width = 924
  Height = 655
  Caption = 'Cadastro de Empresa - EXCLAIM TECNOLOGIA'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object imgrodape: TImage
    Left = 0
    Top = 568
    Width = 908
    Height = 49
    Align = alBottom
    Stretch = True
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 908
    Height = 54
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    DesignSize = (
      908
      54)
    object lbnomeformulario: TLabel
      Left = 590
      Top = 3
      Width = 103
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Cadastro da'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigo: TLabel
      Left = 721
      Top = 9
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb7: TLabel
      Left = 590
      Top = 25
      Width = 77
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Empresa'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object btrelatorio: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
      Spacing = 0
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 54
    Width = 908
    Height = 514
    Align = alClient
    BevelOuter = bvNone
    Color = 10643006
    TabOrder = 1
    DesignSize = (
      908
      514)
    object lbLbRAZAOSOCIAL1: TLabel
      Left = 18
      Top = 200
      Width = 67
      Height = 14
      Caption = 'Raz'#227'o Social'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbFANTASIA1: TLabel
      Left = 18
      Top = 223
      Width = 45
      Height = 14
      Caption = 'Fantasia'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbCNPJ1: TLabel
      Left = 18
      Top = 246
      Width = 25
      Height = 14
      Caption = 'Cnpj'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbIE1: TLabel
      Left = 357
      Top = 246
      Width = 99
      Height = 14
      Caption = 'Inscri'#231#227'o Estadual'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbENDERECO1: TLabel
      Left = 18
      Top = 268
      Width = 52
      Height = 14
      Caption = 'Endere'#231'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbCIDADE1: TLabel
      Left = 357
      Top = 292
      Width = 38
      Height = 14
      Caption = 'Cidade'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbESTADO1: TLabel
      Left = 357
      Top = 317
      Width = 13
      Height = 14
      Caption = 'UF'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbCEP1: TLabel
      Left = 18
      Top = 317
      Width = 22
      Height = 14
      Caption = 'Cep'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbCOMPLEMENTO1: TLabel
      Left = 357
      Top = 268
      Width = 79
      Height = 14
      Caption = 'Complemento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbFONE1: TLabel
      Left = 357
      Top = 342
      Width = 27
      Height = 14
      Caption = 'Fone'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbFAX1: TLabel
      Left = 357
      Top = 366
      Width = 18
      Height = 14
      Caption = 'Fax'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbCELULAR1: TLabel
      Left = 18
      Top = 342
      Width = 39
      Height = 14
      Caption = 'Celular'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbEMAIL1: TLabel
      Left = 357
      Top = 391
      Width = 29
      Height = 14
      Caption = 'Email'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbHOMEPAGE1: TLabel
      Left = 18
      Top = 366
      Width = 59
      Height = 14
      Caption = 'Homepage'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbDIRETOR1: TLabel
      Left = 18
      Top = 414
      Width = 38
      Height = 14
      Caption = 'Diretor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbLbCELULARDIRETOR1: TLabel
      Left = 18
      Top = 391
      Width = 80
      Height = 14
      Caption = 'Celular Diretor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb8: TLabel
      Left = 18
      Top = 292
      Width = 33
      Height = 14
      Caption = 'Bairro'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb9: TLabel
      Left = 357
      Top = 413
      Width = 92
      Height = 14
      Caption = 'Frase Nota Fiscal'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb10: TLabel
      Left = 670
      Top = 18
      Width = 125
      Height = 14
      Caption = 'Optante pelo Simples?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object lb11: TLabel
      Left = 39
      Top = 17
      Width = 128
      Height = 14
      Caption = 'Paga Icms no Simples?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb12: TLabel
      Left = 404
      Top = 18
      Width = 226
      Height = 14
      Caption = 'Icms Antec. Sobre Todas as Desp. da NF?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb13: TLabel
      Left = 19
      Top = 116
      Width = 97
      Height = 14
      Caption = 'ISS (%)...................:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb14: TLabel
      Left = 384
      Top = 116
      Width = 113
      Height = 14
      Caption = 'Al'#237'quota Simples (%)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb15: TLabel
      Left = 197
      Top = 116
      Width = 97
      Height = 14
      Caption = 'ISS Substituto (%)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb16: TLabel
      Left = 404
      Top = 34
      Width = 141
      Height = 14
      Caption = 'Credita ICMS na Compra?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb17: TLabel
      Left = 38
      Top = 34
      Width = 265
      Height = 14
      Caption = 'Utiliza ICMS Subst. recolhido no c'#225'lculo do custo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object shp1: TShape
      Left = 0
      Top = 180
      Width = 908
      Height = 1
      Anchors = [akLeft, akTop, akRight]
      Pen.Color = clWhite
      Pen.Mode = pmMergePenNot
      Pen.Style = psDash
    end
    object lb18: TLabel
      Left = 268
      Top = 268
      Width = 44
      Height = 14
      Caption = 'N'#250'mero'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbtalaohomologado: TLabel
      Left = 584
      Top = 116
      Width = 100
      Height = 14
      Caption = 'Tal'#227'o Homologado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb19: TLabel
      Left = 584
      Top = 34
      Width = 113
      Height = 14
      Caption = 'Substituto Tribut'#225'rio'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb20: TLabel
      Left = 403
      Top = 52
      Width = 278
      Height = 14
      Caption = 'Paga ICMS Garantido nas compras Interestaduais?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb21: TLabel
      Left = 607
      Top = 319
      Width = 64
      Height = 14
      Caption = 'C'#243'd .cidade'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb22: TLabel
      Left = 18
      Top = 438
      Width = 210
      Height = 14
      Caption = 'N'#250'mero de s'#233'rie  do certificado digital'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb23: TLabel
      Left = 504
      Top = 319
      Width = 38
      Height = 14
      Caption = 'C'#243'd UF'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb5: TLabel
      Left = 16
      Top = 460
      Width = 108
      Height = 14
      Caption = 'C'#243'digo Cliente Site '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 19
      Top = 55
      Width = 22
      Height = 14
      Caption = 'CRT'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbNomeCRT: TLabel
      Left = 112
      Top = 56
      Width = 257
      Height = 14
      AutoSize = False
      Caption = 'lbNomeCRT'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object edtRazaosocial: TEdit
      Left = 101
      Top = 195
      Width = 657
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 150
      ParentFont = False
      TabOrder = 11
    end
    object edtFantasia: TEdit
      Left = 101
      Top = 218
      Width = 657
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 150
      ParentFont = False
      TabOrder = 12
    end
    object edtCNPJ: TEdit
      Left = 101
      Top = 241
      Width = 246
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 30
      ParentFont = False
      TabOrder = 13
    end
    object edtIE: TEdit
      Left = 458
      Top = 241
      Width = 300
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 30
      ParentFont = False
      TabOrder = 14
    end
    object edtEndereco: TEdit
      Left = 101
      Top = 263
      Width = 164
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 150
      ParentFont = False
      TabOrder = 15
    end
    object edtEstado: TEdit
      Left = 458
      Top = 314
      Width = 43
      Height = 20
      CharCase = ecUpperCase
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 2
      ParentFont = False
      TabOrder = 20
      OnExit = edtEstadoExit
    end
    object edtCEP: TEdit
      Left = 101
      Top = 312
      Width = 247
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      TabOrder = 19
    end
    object edtComplemento: TEdit
      Left = 458
      Top = 263
      Width = 300
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      TabOrder = 17
    end
    object edtFone: TEdit
      Left = 458
      Top = 337
      Width = 300
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 15
      ParentFont = False
      TabOrder = 24
    end
    object edtFax: TEdit
      Left = 458
      Top = 361
      Width = 300
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 15
      ParentFont = False
      TabOrder = 26
    end
    object edtcelular: TEdit
      Left = 101
      Top = 337
      Width = 246
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 15
      ParentFont = False
      TabOrder = 23
    end
    object edtEmail: TEdit
      Left = 458
      Top = 386
      Width = 300
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 150
      ParentFont = False
      TabOrder = 28
    end
    object edtHomePage: TEdit
      Left = 101
      Top = 361
      Width = 246
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 100
      ParentFont = False
      TabOrder = 25
    end
    object edtDiretor: TEdit
      Left = 101
      Top = 409
      Width = 246
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 150
      ParentFont = False
      TabOrder = 29
    end
    object edtCelularDiretor: TEdit
      Left = 101
      Top = 386
      Width = 246
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 150
      ParentFont = False
      TabOrder = 27
    end
    object edtBairro: TEdit
      Left = 101
      Top = 287
      Width = 246
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 50
      ParentFont = False
      TabOrder = 18
    end
    object pnlNaosimples: TPanel
      Left = 0
      Top = 80
      Width = 908
      Height = 26
      Anchors = [akLeft, akTop, akRight]
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Color = clSilver
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 7
      object lb24: TLabel
        Left = 18
        Top = 6
        Width = 37
        Height = 14
        Caption = 'Pis (%)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object lb25: TLabel
        Left = 197
        Top = 6
        Width = 53
        Height = 14
        Caption = 'Cofins(%)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object lb26: TLabel
        Left = 404
        Top = 6
        Width = 116
        Height = 14
        Caption = 'Credita Pis e Cofins?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object lb27: TLabel
        Left = 560
        Top = 6
        Width = 63
        Height = 14
        Caption = 'Credita Ipi?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object edtpis: TEdit
        Left = 114
        Top = 3
        Width = 78
        Height = 20
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Text = 'edtpis'
      end
      object edtcofins: TEdit
        Left = 300
        Top = 3
        Width = 78
        Height = 20
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = 'edtcofins'
      end
      object chkCheckCreditaIpi: TCheckBox
        Left = 538
        Top = 6
        Width = 13
        Height = 13
        TabStop = False
        TabOrder = 3
      end
      object chkCheckCreditapiscofins: TCheckBox
        Left = 384
        Top = 6
        Width = 13
        Height = 13
        TabStop = False
        TabOrder = 2
      end
    end
    object edtfrasenotafiscal: TEdit
      Left = 458
      Top = 408
      Width = 300
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 500
      ParentFont = False
      TabOrder = 30
    end
    object edtaliquotaiss: TEdit
      Left = 115
      Top = 116
      Width = 78
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 8
    end
    object edtaliquotasimples: TEdit
      Left = 504
      Top = 116
      Width = 78
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 10
    end
    object edtaliquotaiss_substituto: TEdit
      Left = 300
      Top = 116
      Width = 78
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 9
    end
    object chkSimples: TCheckBox
      Left = 651
      Top = 18
      Width = 13
      Height = 13
      TabStop = False
      TabOrder = 0
      Visible = False
    end
    object chkPagaicmsnosimples: TCheckBox
      Left = 19
      Top = 17
      Width = 13
      Height = 13
      TabStop = False
      TabOrder = 1
    end
    object chkIcmsantecipadotodasdespesasnf: TCheckBox
      Left = 384
      Top = 18
      Width = 13
      Height = 13
      TabStop = False
      TabOrder = 2
    end
    object chkCheckUtilizaICMSSubstRecolhido_Custo: TCheckBox
      Left = 19
      Top = 34
      Width = 13
      Height = 13
      TabStop = False
      TabOrder = 3
    end
    object chkCheckCreditaIcmsNaCompra: TCheckBox
      Left = 384
      Top = 34
      Width = 13
      Height = 13
      TabStop = False
      TabOrder = 4
    end
    object edtNumero: TEdit
      Left = 313
      Top = 263
      Width = 33
      Height = 20
      Hint = 'Se n'#227'o tiver numero coloque S/N nesse campo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 150
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 16
    end
    object chkCHECKSUBSTITUTOTRIBUTARIO: TCheckBox
      Left = 560
      Top = 34
      Width = 15
      Height = 15
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object cbbtalaohomologado: TComboBox
      Left = 688
      Top = 114
      Width = 68
      Height = 22
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ItemHeight = 14
      ParentFont = False
      TabOrder = 32
      Items.Strings = (
        'Sim'
        'N'#227'o')
    end
    object chkCheckGarantido: TCheckBox
      Left = 384
      Top = 52
      Width = 13
      Height = 13
      TabStop = False
      TabOrder = 6
    end
    object pnlGarantido: TPanel
      Left = -1
      Top = 143
      Width = 909
      Height = 29
      Anchors = [akLeft, akTop, akRight]
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Color = clSilver
      TabOrder = 33
      object lb28: TLabel
        Left = 19
        Top = 11
        Width = 281
        Height = 14
        Caption = 
          'Al'#237'quota ICMS Garantido (%).....................................' +
          '.....:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object edtAliquotaICMSGarantido: TEdit
        Left = 301
        Top = 6
        Width = 78
        Height = 20
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 9
        ParentFont = False
        TabOrder = 0
      end
    end
    object edtCodigoCidade: TEdit
      Left = 672
      Top = 314
      Width = 85
      Height = 20
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 22
    end
    object edtCertificado: TEdit
      Left = 232
      Top = 436
      Width = 526
      Height = 19
      TabOrder = 31
    end
    object edtCodigoUF: TEdit
      Left = 546
      Top = 314
      Width = 56
      Height = 20
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 21
    end
    object edtcodigoclientesite: TEdit
      Left = 16
      Top = 477
      Width = 100
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 150
      ParentFont = False
      TabOrder = 34
      Text = '262'
    end
    object cbbcidade: TComboBox
      Left = 457
      Top = 284
      Width = 299
      Height = 22
      CharCase = ecUpperCase
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ItemHeight = 14
      ParentFont = False
      TabOrder = 35
      OnExit = cbbcidadeExit
    end
    object edtCRT: TEdit
      Left = 48
      Top = 52
      Width = 57
      Height = 20
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 36
      OnExit = edtCRTExit
      OnKeyDown = edtCRTKeyDown
    end
  end
end
