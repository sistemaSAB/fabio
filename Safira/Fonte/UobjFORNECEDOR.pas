unit UobjFORNECEDOR;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJRAMOATIVIDADE
,UOBJPRAZOPAGAMENTO,UobjTIPOCLIENTE;

Type
   TObjFORNECEDOR=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               RamoAtividade:TOBJRAMOATIVIDADE;
               PrazoPagamento:TOBJPRAZOPAGAMENTO;
               tipo:TObjTIPOCLIENTE;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime;
                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_RazaoSocial(parametro: string);
                Function Get_RazaoSocial: string;
                Procedure Submit_Fantasia(parametro: string);
                Function Get_Fantasia: string;
                Procedure Submit_CGC(parametro: string);
                Function Get_CGC: string;
                Procedure Submit_IE(parametro: string);
                Function Get_IE: string;
                Procedure Submit_Endereco(parametro: string);
                Function Get_Endereco: string;
                Procedure Submit_Fone(parametro: string);
                Function Get_Fone: string;
                Procedure Submit_Fax(parametro: string);
                Function Get_Fax: string;
                Procedure Submit_Cidade(parametro: string);
                Function Get_Cidade: string;
                Procedure Submit_Estado(parametro: string);
                Function Get_Estado: string;
                Procedure Submit_Contato(parametro: string);
                Function Get_Contato: string;
                Procedure Submit_Email(parametro: string);
                Function Get_Email: string;
                Procedure Submit_DataCadastro(parametro: string);
                Function Get_DataCadastro: string;
                Procedure Submit_CodigoPlanoContas(parametro: string);
                Function Get_CodigoPlanoContas: string;
                Procedure Submit_Banco(parametro: string);
                Function Get_Banco: string;
                Procedure Submit_Agencia(parametro: string);
                Function Get_Agencia: string;
                Procedure Submit_ContaCorrente(parametro: string);
                Function Get_ContaCorrente: string;
                Procedure Submit_Bairro(parametro: string);
                Function Get_Bairro: string;
                Procedure Submit_CEP(parametro: string);
                Function Get_CEP: string;
                Procedure Submit_Observacoes(parametro: string);
                Function Get_Observacoes: string;
                Procedure Submit_codigocidade(parametro: String);
                Function Get_codigocidade: String;
                procedure Submit_CodigoPais(parametro:string);
                function Get_CodigoPais:string;
                procedure Submit_Numero(parametro:string);
                function Get_Numero:string;

                procedure EdtRamoAtividadeExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtRamoAtividadeKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                procedure EdtPrazoPagamentoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtPrazoPagamentoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                procedure EdtTipoExit(Sender:TObject;LABELNOME:TLabel);
                procedure edtTipoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);


                Function  fieldbyname(PCampo:String):String;OVERLOAD;
                Procedure fieldbyname(PCampo:String;PValor:String);OVERLOAD;

                function carregaCidades(comboBoxCidade: TComboBox): boolean;
                function carregaCodigoPaises(comboBoxPais: TComboBox): boolean;
                function retornaUF(str: string): string;
                function retornaCidade(str: string): string;
                function retornaCodigoCidade(cidade, uf: string): string;
                

//CODIFICA DECLARA GETSESUBMITS




                
         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               RazaoSocial:string;
               Fantasia:string;
               CGC:string;
               IE:string;
               Endereco:string;
               Fone:string;
               Fax:string;
               Cidade:string;
               Estado:string;
               Contato:string;
               Email:string;
               DataCadastro:string;
               CodigoPlanoContas:string;
               Banco:string;
               Agencia:string;
               ContaCorrente:string;
               Bairro:string;
               CEP:string;
               Observacoes:string;
               CodigoCidade:String;
               codigopais:string;
               numero:string;

//CODIFICA VARIAVEIS PRIVADAS

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls, UMenuRelatorios,
  URAMOATIVIDADE, UPrazoPagamento, UTIPOCLIENTE;
{ TTabTitulo }


Function  TObjFORNECEDOR.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.RazaoSocial:=fieldbyname('RazaoSocial').asstring;
        Self.Fantasia:=fieldbyname('Fantasia').asstring;
        Self.CGC:=fieldbyname('CGC').asstring;
        Self.IE:=fieldbyname('IE').asstring;
        Self.Endereco:=fieldbyname('Endereco').asstring;
        Self.Fone:=fieldbyname('Fone').asstring;
        Self.Fax:=fieldbyname('Fax').asstring;
        Self.Cidade:=fieldbyname('Cidade').asstring;
        Self.Estado:=fieldbyname('Estado').asstring;
        Self.Contato:=fieldbyname('Contato').asstring;
        Self.Email:=fieldbyname('Email').asstring;
        Self.DataCadastro:=fieldbyname('DataCadastro').asstring;
        self.numero:=fieldbyname('numero').AsString;
        If(FieldByName('RamoAtividade').asstring<>'')
        Then Begin
                 If (Self.RamoAtividade.LocalizaCodigo(FieldByName('RamoAtividade').asstring)=False)
                 Then Begin
                          Messagedlg('Ramo Atividade N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.RamoAtividade.TabelaparaObjeto;
        End;
        Self.CodigoPlanoContas:=fieldbyname('CodigoPlanoContas').asstring;
        Self.Banco:=fieldbyname('Banco').asstring;
        Self.Agencia:=fieldbyname('Agencia').asstring;
        Self.ContaCorrente:=fieldbyname('ContaCorrente').asstring;
        Self.Bairro:=fieldbyname('Bairro').asstring;
        Self.CEP:=fieldbyname('CEP').asstring;
        Self.codigocidade:=fieldbyname('codigocidade').asstring;
        Self.Observacoes:=fieldbyname('Observacoes').asstring;
        self.codigopais:=fieldbyname('codigopais').AsString;
        If(FieldByName('PrazoPagamento').asstring<>'')
        Then Begin
                 If (Self.PrazoPagamento.LocalizaCodigo(FieldByName('PrazoPagamento').asstring)=False)
                 Then Begin
                          Messagedlg('Prazo Pagamento N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PrazoPagamento.TabelaparaObjeto;
        End;

        If(FieldByName('tipo').asstring<>'')
        Then Begin
                 If (Self.tipo.LocalizaCodigo(FieldByName('tipo').asstring)=False)
                 Then Begin
                          Messagedlg('Tipo n�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.tipo.TabelaparaObjeto;
        End;

        result:=True;
     End;
end;


Procedure TObjFORNECEDOR.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('RazaoSocial').asstring:=Self.RazaoSocial;
        ParamByName('Fantasia').asstring:=Self.Fantasia;
        ParamByName('CGC').asstring:=Self.CGC;
        ParamByName('IE').asstring:=Self.IE;
        ParamByName('Endereco').asstring:=Self.Endereco;
        ParamByName('Fone').asstring:=Self.Fone;
        ParamByName('Fax').asstring:=Self.Fax;
        ParamByName('Cidade').asstring:=Self.Cidade;
        ParamByName('Estado').asstring:=Self.Estado;
        ParamByName('Contato').asstring:=Self.Contato;
        ParamByName('Email').asstring:=Self.Email;
        ParamByName('DataCadastro').asstring:=Self.DataCadastro;
        ParamByName('RamoAtividade').asstring:=Self.RamoAtividade.GET_CODIGO;
        ParamByName('CodigoPlanoContas').asstring:=Self.CodigoPlanoContas;
        ParamByName('Banco').asstring:=Self.Banco;
        ParamByName('Agencia').asstring:=Self.Agencia;
        ParamByName('ContaCorrente').asstring:=Self.ContaCorrente;
        ParamByName('Bairro').asstring:=Self.Bairro;
        ParamByName('CEP').asstring:=Self.CEP;
        ParamByName('codigocidade').asstring:=Self.codigocidade;
        ParamByName('Observacoes').asstring:=Self.Observacoes;
        ParamByName('PrazoPagamento').asstring:=Self.PrazoPagamento.GET_CODIGO;
        ParamByName('tipo').AsString:=self.tipo.Get_CODIGO;
        ParamByName('codigopais').asstring:=self.codigopais;
        ParamByName('numero').AsString:=self.numero;
//CODIFICA OBJETOPARATABELA
  End;
End;

Procedure TObjFORNECEDOR.fieldbyname(PCampo:String;PValor:String);
Begin
     PCampo:=Uppercase(pcampo);                                

     If (Pcampo='CODIGO')
     Then Begin
              Self.Submit_CODIGO(pVALOR);
              exit;
     End;
     If (Pcampo='RAZAOSOCIAL')
     Then Begin
              Self.Submit_RAZAOSOCIAL(pVALOR);
              exit;
     End;
     If (Pcampo='FANTASIA')
     Then Begin
              Self.Submit_FANTASIA(pVALOR);
              exit;
     End;
     If (Pcampo='CGC')
     Then Begin
              Self.Submit_CGC(pVALOR);
              exit;
     End;
     If (Pcampo='IE')
     Then Begin
              Self.Submit_IE(pVALOR);
              exit;
     End;
     If (Pcampo='ENDERECO')
     Then Begin
              Self.Submit_ENDERECO(pVALOR);
              exit;
     End;
     If (Pcampo='FAX')
     Then Begin
              Self.Submit_FAX(pVALOR);
              exit;
     End;
     If (Pcampo='CIDADE')
     Then Begin
              Self.Submit_CIDADE(pVALOR);
              exit;
     End;
     If (Pcampo='ESTADO')
     Then Begin
              Self.Submit_ESTADO(pVALOR);
              exit;
     End;
     If (Pcampo='CONTATO')
     Then Begin
              Self.Submit_CONTATO(pVALOR);
              exit;
     End;
     If (Pcampo='EMAIL')
     Then Begin
              Self.Submit_EMAIL(pVALOR);
              exit;
     End;
     If (Pcampo='RAMOATIVIDADE')
     Then Begin
              Self.RAMOATIVIDADE.Submit_codigo(pVALOR);
              exit;
     End;
     If (Pcampo='CODIGOPLANOCONTAS')
     Then Begin
              Self.Submit_CODIGOPLANOCONTAS(pVALOR);
              exit;
     End;
     If (Pcampo='BANCO')
     Then Begin
              Self.Submit_BANCO(pVALOR);
              exit;
     End;
     If (Pcampo='AGENCIA')
     Then Begin
              Self.Submit_AGENCIA(pVALOR);
              exit;
     End;
     If (Pcampo='CONTACORRENTE')
     Then Begin
              Self.Submit_CONTACORRENTE(pVALOR);
              exit;
     End;
     If (Pcampo='BAIRRO')
     Then Begin
              Self.Submit_BAIRRO(pVALOR);
              exit;
     End;
     If (Pcampo='CEP')
     Then Begin
              Self.Submit_CEP(pVALOR);
              exit;
     End;
     If (Pcampo='CODIGOCIDADE')
     Then Begin
              Self.Submit_codigocidade(pVALOR);
              exit;
     End;
     If (Pcampo='OBSERVACOES')
     Then Begin
              Self.Submit_OBSERVACOES(pVALOR);
              exit;
     End;

     If (Pcampo='PRAZOPAGAMENTO')
     Then Begin
              Self.PRAZOPAGAMENTO.Submit_codigo(pVALOR);
              exit;
     End;

     if (PCampo = 'TIPO') then
     begin
       self.tipo.Submit_CODIGO(PValor);
       Exit;
     end;


     If (Pcampo='FONE')
     Then Begin
              Self.Submit_FONE(pVALOR);
              exit;
     End;
     If (Pcampo='DATACADASTRO')
     Then Begin
              Self.Submit_DATACADASTRO(pVALOR);
              exit;
     End;
end;
Function TObjFORNECEDOR.fieldbyname(PCampo:String):String;
Begin                                                          
     PCampo:=Uppercase(pcampo);                                
     Result:=''; 
     If (Pcampo='CODIGO')
     Then Begin
              Result:=Self.Get_CODIGO;
              exit;
     End;
     If (Pcampo='RAZAOSOCIAL')
     Then Begin
              Result:=Self.Get_RAZAOSOCIAL;
              exit;
     End;
     If (Pcampo='FANTASIA')
     Then Begin
              Result:=Self.Get_FANTASIA;
              exit;
     End;
     If (Pcampo='CGC')
     Then Begin
              Result:=Self.Get_CGC;
              exit;
     End;
     If (Pcampo='IE')
     Then Begin
              Result:=Self.Get_IE;
              exit;
     End;
     If (Pcampo='ENDERECO')
     Then Begin
              Result:=Self.Get_ENDERECO;
              exit;
     End;
     If (Pcampo='FAX')
     Then Begin
              Result:=Self.Get_FAX;
              exit;
     End;
     If (Pcampo='CIDADE')
     Then Begin
              Result:=Self.Get_CIDADE;
              exit;
     End;
     If (Pcampo='ESTADO')
     Then Begin
              Result:=Self.Get_ESTADO;
              exit;
     End;
     If (Pcampo='CONTATO')
     Then Begin
              Result:=Self.Get_CONTATO;
              exit;
     End;
     If (Pcampo='EMAIL')
     Then Begin
              Result:=Self.Get_EMAIL;
              exit;
     End;
     If (Pcampo='RAMOATIVIDADE')
     Then Begin
              Result:=Self.RAMOATIVIDADE.Get_codigo;
              exit;
     End;
     If (Pcampo='CODIGOPLANOCONTAS')
     Then Begin
              Result:=Self.Get_CODIGOPLANOCONTAS;
              exit;
     End;
     If (Pcampo='BANCO')
     Then Begin
              Result:=Self.Get_BANCO;
              exit;
     End;
     If (Pcampo='AGENCIA')
     Then Begin
              Result:=Self.Get_AGENCIA;
              exit;
     End;
     If (Pcampo='CONTACORRENTE')
     Then Begin
              Result:=Self.Get_CONTACORRENTE;
              exit;
     End;
     If (Pcampo='BAIRRO')
     Then Begin
              Result:=Self.Get_BAIRRO;
              exit;
     End;
     If (Pcampo='CEP')
     Then Begin
              Result:=Self.Get_CEP;
              exit;
     End;
     If (Pcampo='CODIGOCIDADE')
     Then Begin
              Result:=Self.Get_codigocidade;
              exit;
     End;
     If (Pcampo='OBSERVACOES')
     Then Begin
              Result:=Self.Get_OBSERVACOES;
              exit;
     End;

     If (Pcampo='PRAZOPAGAMENTO')
     Then Begin
              Result:=Self.PRAZOPAGAMENTO.Get_codigo;
              exit;
     End;

     if (PCampo='TIPO') then
     begin

      result:=self.tipo.Get_CODIGO();
      Exit;

     end;

     If (Pcampo='FONE')
     Then Begin
              Result:=Self.Get_FONE;
              exit;
     End;
     If (Pcampo='DATACADASTRO')
     Then Begin
              Result:=Self.Get_DATACADASTRO;
              exit;
     End;
end;

//***********************************************************************

function TObjFORNECEDOR.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjFORNECEDOR.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        RazaoSocial:='';
        Fantasia:='';
        CGC:='';
        IE:='';
        Endereco:='';
        Fone:='';
        Fax:='';
        Cidade:='';
        Estado:='';
        Contato:='';
        Email:='';
        DataCadastro:='';
        RamoAtividade.ZerarTabela;
        CodigoPlanoContas:='';
        Banco:='';
        Agencia:='';
        ContaCorrente:='';
        Bairro:='';
        CEP:='';
        CodigoCidade:='';
        Observacoes:='';
        PrazoPagamento.ZerarTabela;
        self.tipo.ZerarTabela;
        CodigoPais:='';
        numero:='';

     End;
end;

Function TObjFORNECEDOR.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/C�digo';
      if(RazaoSocial='')
      then Mensagem:=Mensagem+'/Raz�o Social'

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjFORNECEDOR.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin

     mensagem:='';
   {  If (Self.RamoAtividade.LocalizaCodigo(Self.RamoAtividade.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Ramo Atividade n�o Encontrado!';

     If (Self.PrazoPagamento.LocalizaCodigo(Self.PrazoPagamento.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Prazo Pagamento n�o Encontrado!';     }

     if (self.tipo.LocalizaCodigo(self.tipo.Get_CODIGO)=false) then
      mensagem:=mensagem+'/ Tipo Fornecedor n�o encontrado';


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjFORNECEDOR.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        if (Self.CodigoCidade<>'')
        then Strtoint(Self.CodigoCidade);
     Except
           Mensagem:=mensagem+'/C�digo da Cidade';
     End;
     {
     try
        If (Self.RamoAtividade.Get_Codigo<>'')
        Then Strtoint(Self.RamoAtividade.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Ramo Atividade';
     End;
    { try
        Strtoint(Self.CodigoPlanoContas);
     Except
           Mensagem:=mensagem+'/Codigo Plano Contas';
     End;

     try
        If (Self.PrazoPagamento.Get_Codigo<>'')
        Then Strtoint(Self.PrazoPagamento.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Prazo Pagamento';
     End;
                 }
     try

       if (Self.tipo.Get_CODIGO <> '') then
        StrToInt(Self.tipo.Get_CODIGO);

     except
        Mensagem:=Mensagem + '/Tipo';
     end;



     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjFORNECEDOR.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
     try
        Strtodate(Self.DataCadastro);
     Except
           Mensagem:=mensagem+'/Data Cadastro';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjFORNECEDOR.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';
//CODIFICA VERIFICAFAIXA


     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjFORNECEDOR.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabFORNECEDOR est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,RazaoSocial,Fantasia,CGC,IE,Endereco,CodigoCidade,Fone,Fax,Cidade');
           SQL.ADD(' ,Estado,Contato,Email,DataCadastro,RamoAtividade,CodigoPlanoContas');
           SQL.ADD(' ,Banco,Agencia,ContaCorrente,Bairro,CEP,Observacoes,PrazoPagamento,codigocidade,codigopais,tipo,numero');
           SQL.ADD(' ');
           SQL.ADD(' from  TABFORNECEDOR');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjFORNECEDOR.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjFORNECEDOR.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjFORNECEDOR.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        self.tipo:=TObjTIPOCLIENTE.Create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.RamoAtividade:=TOBJRAMOATIVIDADE.create;
        Self.PrazoPagamento:=TOBJPRAZOPAGAMENTO.create;

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABFORNECEDOR(Codigo,RazaoSocial,Fantasia');
                InsertSQL.add(' ,CGC,IE,Endereco,Fone,Fax,Cidade,Estado,Contato,Email');
                InsertSQL.add(' ,DataCadastro,RamoAtividade,CodigoPlanoContas,Banco');
                InsertSQL.add(' ,Agencia,ContaCorrente,Bairro,CEP,Observacoes,PrazoPagamento,codigocidade,codigopais,tipo,numero');
                InsertSQL.add(' )');
                InsertSQL.add('values (:Codigo,:RazaoSocial,:Fantasia,:CGC,:IE,:Endereco');
                InsertSQL.add(' ,:Fone,:Fax,:Cidade,:Estado,:Contato,:Email,:DataCadastro');
                InsertSQL.add(' ,:RamoAtividade,:CodigoPlanoContas,:Banco,:Agencia');
                InsertSQL.add(' ,:ContaCorrente,:Bairro,:CEP,:Observacoes,:PrazoPagamento,:codigocidade,:codigopais,:tipo,:numero');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABFORNECEDOR set Codigo=:Codigo,RazaoSocial=:RazaoSocial');
                ModifySQL.add(',Fantasia=:Fantasia,CGC=:CGC,IE=:IE,Endereco=:Endereco,CodigoCidade=:CodigoCidade');
                ModifySQL.add(',Fone=:Fone,Fax=:Fax,Cidade=:Cidade,Estado=:Estado,Contato=:Contato');
                ModifySQL.add(',Email=:Email,DataCadastro=:DataCadastro,RamoAtividade=:RamoAtividade');
                ModifySQL.add(',CodigoPlanoContas=:CodigoPlanoContas,Banco=:Banco,Agencia=:Agencia');
                ModifySQL.add(',ContaCorrente=:ContaCorrente,Bairro=:Bairro,CEP=:CEP');
                ModifySQL.add(',Observacoes=:Observacoes,PrazoPagamento=:PrazoPagamento,codigopais=:codigopais,tipo=:tipo,numero=:numero');
                ModifySQL.add('');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABFORNECEDOR where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjFORNECEDOR.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjFORNECEDOR.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabFORNECEDOR');
     Result:=Self.ParametroPesquisa;
end;

function TObjFORNECEDOR.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de FORNECEDOR ';
end;


function TObjFORNECEDOR.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENFORNECEDOR,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENFORNECEDOR,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjFORNECEDOR.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.RamoAtividade.FREE;
    Self.PrazoPagamento.FREE;
    self.tipo.Free;

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjFORNECEDOR.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjFORNECEDOR.RetornaCampoNome: string;
begin
      result:='FANTASIA';

end;

procedure TObjFornecedor.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjFornecedor.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjFornecedor.Submit_RazaoSocial(parametro: string);
begin
        Self.RazaoSocial:=Parametro;
end;
function TObjFornecedor.Get_RazaoSocial: string;
begin
        Result:=Self.RazaoSocial;
end;
procedure TObjFornecedor.Submit_Fantasia(parametro: string);
begin
        Self.Fantasia:=Parametro;
end;
function TObjFornecedor.Get_Fantasia: string;
begin
        Result:=Self.Fantasia;
end;
procedure TObjFornecedor.Submit_CGC(parametro: string);
begin
        Self.CGC:=Parametro;
end;
function TObjFornecedor.Get_CGC: string;
begin
        Result:=Self.CGC;
end;
procedure TObjFornecedor.Submit_IE(parametro: string);
begin
        Self.IE:=Parametro;
end;
function TObjFornecedor.Get_IE: string;
begin
        Result:=Self.IE;
end;
procedure TObjFornecedor.Submit_Endereco(parametro: string);
begin
        Self.Endereco:=Parametro;
end;
function TObjFornecedor.Get_Endereco: string;
begin
        Result:=Self.Endereco;
end;

procedure TObjFornecedor.Submit_codigocidade(parametro: String);
begin
        Self.codigocidade:=Parametro;
end;
function TObjFornecedor.Get_codigocidade: String;
begin
        Result:=Self.codigocidade;
end;


procedure TObjFornecedor.Submit_Fone(parametro: string);
begin
        Self.Fone:=Parametro;
end;
function TObjFornecedor.Get_Fone: string;
begin
        Result:=Self.Fone;
end;
procedure TObjFornecedor.Submit_Fax(parametro: string);
begin
        Self.Fax:=Parametro;
end;
function TObjFornecedor.Get_Fax: string;
begin
        Result:=Self.Fax;
end;
procedure TObjFornecedor.Submit_Cidade(parametro: string);
begin
        Self.Cidade:=Parametro;
end;
function TObjFornecedor.Get_Cidade: string;
begin
        Result:=Self.Cidade;
end;
procedure TObjFornecedor.Submit_Estado(parametro: string);
begin
        Self.Estado:=Parametro;
end;
function TObjFornecedor.Get_Estado: string;
begin
        Result:=Self.Estado;
end;
procedure TObjFornecedor.Submit_Contato(parametro: string);
begin
        Self.Contato:=Parametro;
end;
function TObjFornecedor.Get_Contato: string;
begin
        Result:=Self.Contato;
end;
procedure TObjFornecedor.Submit_Email(parametro: string);
begin
        Self.Email:=Parametro;
end;
function TObjFornecedor.Get_Email: string;
begin
        Result:=Self.Email;
end;
procedure TObjFornecedor.Submit_DataCadastro(parametro: string);
begin
        Self.DataCadastro:=Parametro;
end;
function TObjFornecedor.Get_DataCadastro: string;
begin
        Result:=Self.DataCadastro;
end;
procedure TObjFornecedor.Submit_CodigoPlanoContas(parametro: string);
begin
        Self.CodigoPlanoContas:=Parametro;
end;
function TObjFornecedor.Get_CodigoPlanoContas: string;
begin
        Result:=Self.CodigoPlanoContas;
end;
procedure TObjFornecedor.Submit_Banco(parametro: string);
begin
        Self.Banco:=Parametro;
end;
function TObjFornecedor.Get_Banco: string;
begin
        Result:=Self.Banco;
end;
procedure TObjFornecedor.Submit_Agencia(parametro: string);
begin
        Self.Agencia:=Parametro;
end;
function TObjFornecedor.Get_Agencia: string;
begin
        Result:=Self.Agencia;
end;
procedure TObjFornecedor.Submit_ContaCorrente(parametro: string);
begin
        Self.ContaCorrente:=Parametro;
end;
function TObjFornecedor.Get_ContaCorrente: string;
begin
        Result:=Self.ContaCorrente;
end;
procedure TObjFornecedor.Submit_Bairro(parametro: string);
begin
        Self.Bairro:=Parametro;
end;
function TObjFornecedor.Get_Bairro: string;
begin
        Result:=Self.Bairro;
end;
procedure TObjFornecedor.Submit_CEP(parametro: string);
begin
        Self.CEP:=Parametro;
end;
function TObjFornecedor.Get_CEP: string;
begin
        Result:=Self.CEP;
end;
procedure TObjFornecedor.Submit_Observacoes(parametro: string);
begin
        Self.Observacoes:=Parametro;
end;
function TObjFornecedor.Get_Observacoes: string;
begin
        Result:=Self.Observacoes;
end;
//CODIFICA GETSESUBMITS


procedure TObjFORNECEDOR.EdtRamoAtividadeExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.RamoAtividade.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.RamoAtividade.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.RamoAtividade.GET_NOME;
End;
procedure TObjFORNECEDOR.EdtRamoAtividadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FRAMOATIVIDADE:TFRAMOATIVIDADE;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FRAMOATIVIDADE:=TFRAMOATIVIDADE.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.RamoAtividade.Get_Pesquisa,Self.RamoAtividade.Get_TituloPesquisa,FRamoAtividade)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.RamoAtividade.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.RamoAtividade.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.RamoAtividade.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeandNil(FRAMOATIVIDADE);

     End;
end;
procedure TObjFORNECEDOR.EdtPrazoPagamentoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PrazoPagamento.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PrazoPagamento.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.PrazoPagamento.GET_NOME;
End;
procedure TObjFORNECEDOR.EdtPrazoPagamentoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPRAZOPAGAMENTO:TFPRAZOPAGAMENTO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPRAZOPAGAMENTO:=TFPRAZOPAGAMENTO.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PrazoPagamento.Get_Pesquisa,Self.PrazoPagamento.Get_TituloPesquisa,FPrazoPagamento)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PrazoPagamento.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.PrazoPagamento.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PrazoPagamento.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FPRAZOPAGAMENTO);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjFORNECEDOR.Imprime;
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJFORNECEDOR';
          With RgOpcoes do
          Begin
               items.clear;
                // Addicionar os Items.Add
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;       // Chamada de Func�es
          end;
     end;
end;

function TObjfornecedor.carregaCidades(comboBoxCidade: TComboBox): boolean;
var
  cidade,uf:string;
  query:TIBQuery;
begin

  comboBoxCidade.Items.Append ('');// para o limpa labels.
  try
    query:=TIBQuery.Create(nil);
    query.Database:= FDataModulo.IBDatabase;
  except

  end;

  try



        try

          with query do
          begin

            Close;
            sql.Clear;
            SQL.Add('select codigo,nome,estado,codigocidade');
            SQL.Add('from tabcidade');
            SQL.Add('order by nome');


            try

              Open;
              first;

              while not (eof) do
              begin

                cidade := Fieldbyname('nome').AsString;
                uf     := Fieldbyname('estado').AsString;

                comboBoxCidade.Items.Append (AlinhaTexto (cidade,'|'+uf,36));

                Next;

              end;
        
              result:=True;

            except

               result:=False;

            end;


          end;

        except

          result:=False;

        end;
  finally
        FreeAndNil(query);
  end;

end;

function TObjfornecedor.carregaCodigoPaises(comboBoxPais: TComboBox): boolean;
var
  pais,codigopais:string;
  query:TIBQuery;
begin

  comboBoxPais.Items.Append ('');// para o limpa labels.
  try
    query:=TIBQuery.Create(nil);
    query.Database:= FDataModulo.IBDatabase;
  except

  end;
  try




      try

        with Query do
        begin

          Close;
          sql.Clear;
          SQL.Add('select pais,codigopais');
          SQL.Add('from tabcodigopais');
          SQL.Add('order by pais');


          try

            Open;
            first;

            while not (eof) do
            begin

              pais       := Fieldbyname('pais').AsString;
              codigopais := Fieldbyname('codigopais').AsString;

              comboBoxpais.Items.Append (AlinhaTexto (pais,'|'+codigopais,34));

              Next;

            end;
        
            result:=True;

          except

             result:=False;

          end;


        end;

      except

        result:=False;

      end;
  finally
        FreeAndNil(query);
  end;

end;


function TObjFORNECEDOR.Get_CodigoPais:string;
begin
    Result:=self.codigopais
end;

procedure TObjFORNECEDOR.Submit_CodigoPais(parametro:String);
begin
  self.codigopais:=parametro;
end;

function TObjfornecedor.retornaCidade(str: string): string;
var
  i,j:Integer;
  aux:string;
begin

  if (str = '') then
    Exit;

  aux:='';
  
  i:=1;
  while ((str[i] <> '|') and (i <= Length (str))) do
  begin

    aux:=aux+str[i];
    i:=i+1;

  end;

  i:=Length(aux);
  result:='';

  while ( (aux[i] = ' ') and (i >= 1 ) ) do I:=I-1;


  for j := 1 to i  do
    result:=result+aux[j];

end;


function TObjfornecedor.retornaUF(str: string): string;
var
  i:Integer;
begin

  result:='';

  i:=1;

  while ((str[i] <> '|') and (i <= Length (str))) do i:=i+1;

  i:=i+1;
  while ( i <= Length(str) ) do
  begin

    result:=result+str[i];
    i:=i+1;

  end;


end;

function TObjfornecedor.retornaCodigoCidade(cidade, uf: string): string;
begin

  with self.Objquery do
  begin

    close;
    sql.Clear;

    sql.Add('select codigocidade');
    sql.Add('from tabcidade');
    sql.Add('where nome = '+#39+uppercase(cidade)+#39+'and upper(estado) = '+#39+uppercase(uf)+#39);

    //InputBox('','',SQL.Text);

    try

      Open;

      if (recordcount > 1) then
      begin

        MensagemAviso('Aten��o, existe registro em duplicidade no cadastro de cidades');
        Exit;

      end;

      result:=Fieldbyname('codigocidade').AsString;

    except

      MensagemAviso('N�o foi possivel executar a fun��o retornaCodigoCidade');
      Exit;

    end;

  end;

end;



procedure TObjFORNECEDOR.EdtTipoExit(Sender: TObject; LABELNOME: TLabel);
begin

  labelnome.caption:='';

  If (Tedit(Sender).text='') Then
    exit;

  If (Self.tipo.localizacodigo(TEdit(Sender).text)=False) Then
  Begin
    TEdit(Sender).text:='';
    exit;
  End;

  Self.tipo.tabelaparaobjeto;
  LABELNOME.CAPTION:=Self.tipo.Get_NOME;

end;

procedure TObjFORNECEDOR.edtTipoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Ftipo:TFTIPOCLIENTE;
begin

     If (key <>vk_f9) Then
      exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Ftipo:=TFTIPOCLIENTE.create(nil);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabtipocliente','Pesquisa de tipo',FTIPOCLIENTE)=True)
            Then Begin

              Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(SELF.tipo.RetornaCampoCodigo).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.TIPO.RetornaCampoNome<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TIPO.RetornaCampoNome).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                        End;
              Finally
                             FpesquisaLocal.QueryPesq.close;
              End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Ftipo);
     End;

end;

procedure TObjFORNECEDOR.Submit_Numero(parametro:string);
begin
    Self.numero:=parametro;
end;

function TObjFORNECEDOR.Get_Numero:string;
begin
    Result:=Self.numero;
end;

end.



