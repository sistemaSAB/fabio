object FescolheNF: TFescolheNF
  Left = 509
  Top = 246
  Width = 426
  Height = 299
  BorderIcons = [biSystemMenu]
  Caption = 'Escolha de Nota Fiscal'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lb1: TLabel
    Left = 7
    Top = 41
    Width = 29
    Height = 14
    Caption = 'CFOP'
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object btBtsair: TBitBtn
    Left = 73
    Top = 213
    Width = 118
    Height = 37
    Caption = '&OK'
    TabOrder = 0
    OnClick = btBtsairClick
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 410
    Height = 97
    Align = alTop
    Color = 10643006
    TabOrder = 1
    object lb2: TLabel
      Left = 8
      Top = 10
      Width = 65
      Height = 14
      Caption = '"C'#243'digo" NF'
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object lbNomeCfop: TLabel
      Left = 171
      Top = 59
      Width = 279
      Height = 14
      AutoSize = False
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lb3: TLabel
      Left = 13
      Top = 36
      Width = 52
      Height = 14
      Caption = 'Opera'#231#227'o'
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lb4: TLabel
      Left = 231
      Top = 7
      Width = 85
      Height = 24
      Caption = 'Pedido N'#176
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbNumeropedido: TLabel
      Left = 335
      Top = 7
      Width = 20
      Height = 24
      Caption = 'N'#176
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lb5: TLabel
      Left = 13
      Top = 62
      Width = 126
      Height = 14
      Caption = 'Dados Adicionais na NF'
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object edtnotafiscalatual: TEdit
      Left = 85
      Top = 8
      Width = 83
      Height = 19
      Color = 6073854
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 0
      Visible = False
      OnKeyDown = edtnotafiscalatualKeyDown
      OnKeyPress = edtnotafiscalatualKeyPress
    end
    object edtEdtCfop: TEdit
      Left = 85
      Top = 34
      Width = 83
      Height = 19
      Color = 6073854
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 1
      OnKeyDown = edtEdtCfopKeyDown
      OnKeyPress = edtEdtCfopKeyPress
    end
    object btok: TBitBtn
      Left = 169
      Top = 8
      Width = 24
      Height = 22
      Caption = '&Trocar'
      Default = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      Visible = False
      OnClick = btokClick
    end
    object edtDadosAdicionais: TEdit
      Left = 169
      Top = 59
      Width = 237
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 3
    end
  end
  object bt1: TBitBtn
    Left = 213
    Top = 213
    Width = 118
    Height = 37
    Caption = '&Sair'
    TabOrder = 2
    OnClick = bt1Click
  end
  object StrGridGrid: TStringGrid
    Left = -1
    Top = 96
    Width = 410
    Height = 108
    ColCount = 3
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 3
    OnDblClick = StrGridGridDblClick
    ColWidths = (
      64
      64
      64)
  end
end
