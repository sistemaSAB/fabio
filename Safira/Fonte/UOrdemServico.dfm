object FOrdemServico: TFOrdemServico
  Left = 540
  Top = 228
  Width = 866
  Height = 572
  Caption = 'Ordem de Servico'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 850
    Height = 58
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    DesignSize = (
      850
      58)
    object lbnomeformulario: TLabel
      Left = 463
      Top = 13
      Width = 187
      Height = 24
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Ordem de Produ'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -21
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigo: TLabel
      Left = 671
      Top = 9
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object bt1: TSpeedButton
      Left = 797
      Top = -14
      Width = 53
      Height = 80
      Cursor = crHandPoint
      Hint = 'Ajuda'
      Anchors = [akTop, akRight]
      Flat = True
      Glyph.Data = {
        4A0D0000424D4A0D0000000000003600000028000000240000001F0000000100
        180000000000140D0000D8030000D80300000000000000000000FEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFAF6FBF9F2FEFEF4FFFFFAFF
        FCFDFFF9FFFEFEFDFCFFFFFEFEFFFDFEFCFEFEF8FFFFFBFEFCFFFFFCFFFDF6FB
        FFF7F8FFF7FBFFFBFBFDF7FFFFFCFFFFFEF7FDFCFBFFFFFFFBFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFF9FD
        F8FDFFFFFDFBFFFFFBFFFFFDFEFFFCFCFFFEFFFEFDFFFCF9FBF0EFF1F0EBEDF9
        F1F2F6F8F9F1FFFFF4FFFFFEFCFFFFFDFFFFFAFFFBFCFFFEFDFFFFFAFDFAFCFD
        F7FFFFFFFCFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFFFCFFFFFCFEFFFDFCFFFFFFFFFDFFF2E4E6DEBCBCCF
        9F99EB8688E68081EA7E7DF68686FE9092FDA2A5F7C3C3FFF0EDFFFAFFF8FFFF
        FFFDFFFFF5FDFCFDFFF6FFFFFBF8FAFFFDFEFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFBFFFCFEFFFBFFFCE7
        E1DCC29E9EBF7579DC7677F07B76FF8783FF8F88FD9388FB8F84FF837BFF7977
        F77576F98084FBB6B9F2F2ECE8FFFAF8FDFCFFFEFFFAFCFDFFFCFEFCFEFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFEFDFFFAFCD7B8BBB17879C87772F08E88FE9893F58E8CE7797DD76F70
        D26867DC716EE88581F69A95F99591FE8686FF7173F28484FFD9D4FFFFFBFDF8
        F7FFFCFFFFFEFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFF9B99D9CA76B6CDA8786F99B96E17D79
        B6484C98222FAB1122AA0F1EA5131FA41D25AA2226C53938E6625BFF9085FD9B
        91FF7B7CF2777BF4C8C7F8FFFEF9FEFDFFFEFFFFFBFCFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFC0A0A1A7706D
        E6928CFC9691B3484A87192589112398182BA32E31DC6F71EB8082CC5050BB28
        26D32A22EB2F24FE3727F36D69FF9D97FB7D83F7707EFEC5CDFFFEFFFFFDFDED
        FFFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFCFBFDFFFDFFFFFBFFFAFFFFF2FAFA
        FFFEFEDCBDBEA66D6BDB9092ED909996303C770E1B8D1E2C9B1C2BA11425D96F
        75FDA8B0FFA2B0FFA0AAC83F43BC2F26D73927FF3929FF2B1CEC5650FF9996FE
        7D7AFD7F7EFADFDBFFFEFFF4FEFEFEFDFFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFF
        FDF9FEFFFDFFF7FAFEFAFFFFF4E6E7A67F81CF8B8CEC9A9F92323C7B121D9123
        2F921C27A5232EA71C27DE837EFFB7B5F9A9AEFDB0B3BF4343B42821C9372BDA
        3326E33626CB2728CA5556FF9893FB7777DE9A9BFFFEFEFFFCFDFEFDFFFEFDFF
        FEFDFFFEFDFFFEFEFEFEFDFFFFFBFFFFFBFFFAFBFFFFFFFFC3AEADA77678F8A4
        A9A9515776131B8D202898242BA3282CA72629A82627B55047EBADA2FFBCB5CD
        7974A92E2AA13026A5372BBD332DBF3528BD2B2DA51E26DF7372FF918FE27377
        F0CCCCFFFFFEFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFAF9FBFFFDFFFFFAFFFFFE
        FFF5F0EFA38384DB9FA0E78A91791D22852428992E309E2827A72C28AC3028AB
        3229A3271FA13E36B4413E9E332CB03129A73126A23127B0322EA93026AD3131
        A91925B63239F2948FFD7C7FE9989BFBFFF9FEFDFFFEFDFFFEFDFFFEFDFFFFFE
        FEFCFBFDFFFEFFFFFDFFFFFEFFDACECEAE8687F0ABAEB156597E24248B2C2992
        2A25A93730A93328AE3B2EA33222D35E59EA928BF28C87E27D74AC362BA93A2A
        A23729A5342AA3342CA62728B02931A71821D8716FFF8A8ADE7D7FEFECE7FEFD
        FFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFFFDFCFEFFFCFEFFFEFFC0B3B1BF9392F0
        A5A78F3B35842A2396352B9F372AA93C2EAC3D2DAC3C2AA1311FD16D69F8CABF
        FBC0B7FCABA3AA3A2EA23827A33928A9372AA6352BAE302BA72C2EAA1922C550
        51FF9692DC7475E0D2D3FFFEFFFEFDFFFEFDFFFEFDFFFFFDFEFEFDFFFEFDFFFF
        FDFFFFFEFEB6A9A7CB9D9CEC9E9F8335288E3527A0392AAD4130A83C2AA73B29
        A93927AE3827CE6C66FFCDC1FABAB5FFBFB6D46B62AD3126B43427B13A2BA933
        27A93327A02A25AC1F28AF373BFC9D94E7797BD1B8BCFFFEFFFFFEFFFEFDFFFE
        FDFFFFFDFEFCFCFCFDFCFEFFFEFFFDFBFBB5A8A6DBAEABE19192813522983C29
        A73B29AF3E2AA83B26AA3F2AAD3B2AAF3224B05A4EFFE0D0FFCEC6F7BEB5FFBE
        B3DE8C7BA44434A33628AD3C2CAD3726A8342DA7262DAE2E33FA9B92E27A7BD4
        ABB3FFFEFFFFFEFFFEFDFFFEFDFFFBFCFFF9FEFFFFFCFDFBFFFFFCF9FBC9AAAB
        DEB1ADDF9A91813628B03C2BAB3D25A7412AA6362AB3362EA13726AE3C25A833
        24D79987FFF4E4F9D3CEFEBEBEFFC9C3EC9E91AD4436B137299C3B27AA3526B1
        2F28A4302FF7969AE67F86C7AAA5FFFEFFFFFEFFFEFDFFFEFDFFFAFBFFFCFEFF
        FFFEFFFCFEFFFCFEFFCEB4B4E6B6B2E6A99F98392AA13426B2382C9E3E2EAA3E
        33AD4338A74539A24536AB443B9B3D38CF8A87FFEBE6FEDED9F1C0B8FFC9BDEC
        8B81AA372AA73B2AA83F2CA52B1FA6403BF9A1A1E57E86C9B3AEFFFEFFFFFEFF
        FEFDFFFEFDFFFAFDFFFDFCFEFFFEFFFDFCFEFAFFFFD3BEC0ECB5B2F5C2B8AB40
        329F3D31B84D49A24F47AD554EAE574DAE5D55B1655FAD5654B34E50A13B40B9
        7B7BFFE1DBFED7CFF9CAC2FFBCB5BD5146AD3627A3402AA83526AD544AFEACAB
        D5787FD4C7C5FFFEFFFEFDFFFEFDFFFEFDFFFBFFFFFFFBFDFFFDFDFFFDFFF9FE
        FFDFD1D2E7B1B0FFDACEC26B61A74C45A55C54A85A54D3958FF2B8B3F9C2BFF4
        B4B3AD5352AB5C59AC5553A54343F7C0BBFDE0D9FFCCCAFBC9C9CC6A60B03429
        9F3923A63624C9776BFFADACC67E84E4E0DFFEFDFFFEFDFFFEFDFFFEFDFFFAFE
        FFFFFCFFFCFCFCFFFEFFFBFEFFF4ECEDE2B7B4FFDBD2DAB0ABA6504AA86153AB
        514AD59594FFF8F7FEF1EFFAD4D2C469649C5148A85950A14B45ECBBB3FBE1DA
        FFCED0FFCFD6D06E66AB352AAD3B2A9F3726F7A89DFBA4A7BB8E91F2F7F6FEFD
        FFFEFDFFFEFDFFFEFDFFFCFBFDFFFEFFFCFEFEFEFDFFFDFCFFFFFEFFE2C7C3F3
        CAC1EDDEDCBD7672AD5144CE6459D7797AF6F5F1F4FFFEF9F3EEE9B4AAB16860
        B45E58C99087FAD7CDFFDCD6F8D5D2FECBCFBE5A55A23127A12F22CB7264FFC3
        BBD28B8ECFB7B9FAFFFFFEFDFFFEFDFFFEFDFFFEFDFFFEFBFDFFFFFFFCFEFEFB
        FDFEFFFAFFFFFEFFEBE5DEF2C4BDFFE5E8EFC8C0AE6053E17164F47773F0BBB8
        FDFEFAFDFFFFFFF8F1FCD6D4FFCDCCFFDDD8FFDED8FFE0DAFFDDD7E7A1A2A339
        32A7352EAC4A3EF8B8ADECB2ADB68E90FAECEEFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFDFFFCFCFCFAFFFEFAFDFFFFFBFFFFFDFDFBFFF9FFCFC9FFC7CCFFF9EF
        E8BFB0D66D5FFF7C70F9807EEACECDFFFBFFFDFCFFF9FEFFFDFFFFFFE4E5FFE2
        DFF6E7DEEDB6AFB34648A1312BB03E37FCAA9FF7CDC1BC8F8CD5C7C8FFFDFFFC
        FEFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFFFEFFFFFEFF
        FDFCFEFCFBFDFCC8C8F6D7D6FFFCF6F0C9C1E57F7AFF7673F98481ECA5A1E6D6
        D7FCF1F3F9F1F1ECD7D9F0C1C3D78A8DA84646A03232A54B4BECABAAFFD1CFC9
        A09EC9ADACFEF9F8FBFFFFFCFCFCFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFF
        FDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFDFCFEFFF4EEF8CDCAFDD8D4FFFBF4FDE6
        E4FBA1A6FB7375FD6F68EA6E5CCE6855B76958AE6458A5514B993E3AA04D4BC8
        7F7BFCC3C1FFCBCCDAA4A4BFAEABF3F9F4FFFFFEFFFCFDFCFEFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFEFDFFFFFEFFF4FF
        FFFBF2EFF6D6D1FDD9D3FFEFE8FCFBF1FFE6DCF6BBB2EE9285D07C70B76F65B2
        6F66C0807BDDA39EF3C1BBFFD6CFF6C6C5D4ACADCEBBB8F9FAF6F7FFFDFAFCFC
        FFFEFFFCF9FBFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFC
        FEFDFCFEFEFDFFFFFEFFFFFAFCFBFFFFF9F9F9FCE0DFFFD3CCFEDDD4F8ECE6F8
        F6F5F5F8F6F4EFEEF6E4E5FADBDCFFDBDCFFD9D8FBCAC8F0C0BCD7B5B6E1D8D5
        FAFFFEFFFFFFFEFCFCFAFFFFF8FDFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFFFFFBFFFBFCFDFDFFFD
        FBFFFFEEF1FFE2E1FFD3D2FFD0D2FFD2CFFFD1CDFFD2CFFFCFCFFFC9C9F2BFC3
        E9C4C6EED6D8F5F7F7FFFBFCFFFEFFFFFCFEFDFFFFF9FCFFFCFBFFFFFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFFFEFDFFFEFDFFFDFCFEFDFCFEFD
        FCFEFFFBFFF0FDFBF1FFFBFFFEFFFFFDFFF9FEFFF9FCFAFEF2EEF9EAE1F4E5DC
        F3E3DCF3E3DDF6E7E5FCF1F3FEFAFFF9FCFFF4FEFEFFFEFFFFF7FAFFFDFFFCF9
        FBFFFDFFFFFCFFF5F8FCFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFF7F9FFFDFEFFFFFBF8FFFDF9FDFBFAF1FBFB
        F6FFFFFBFDFEFBFEFFFFFCFFFFFDFFFFFEFFFBFFFFF8FFFFFDFEFFFFFAFFFFFD
        FFFAFEFFF8FFFFF9FBFCFFFDFFFFF9FCFBFAFCFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFCFFFEFF
        F7FDFFF5FEFFFBFFFFFFFFFEFFFCFDFFFCFFFFFAFFFFFCFFF9FEFDF9FFFDFBFF
        FEF9FCFAFBFBFBFFFFFFFFFCFEFEFDFFFBFAFCFBFFFFF5FDFDF7FFFFFDFFFFFF
        FCFFFEFDFFFEFDFFFEFDFFFEFDFF}
      ParentShowHint = False
      ShowHint = True
      OnClick = bt1Click
    end
    object btrelatorio: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btrelatorioClick
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 350
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btopcoesClick
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btsalvarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btnovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 399
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
      Spacing = 0
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 481
    Width = 850
    Height = 53
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      850
      53)
    object ImageRodape: TImage
      Left = 1
      Top = 1
      Width = 848
      Height = 51
      Align = alBottom
      Stretch = True
    end
    object lbDataOS: TLabel
      Left = 9
      Top = 14
      Width = 148
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '21/12/2012'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb13: TLabel
      Left = 529
      Top = 15
      Width = 188
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Valor Final....R$'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object lbValorFinalOS: TLabel
      Left = 737
      Top = 15
      Width = 98
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '1000,00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Visible = False
    end
  end
  object panel2: TPanel
    Left = 0
    Top = 58
    Width = 850
    Height = 150
    Align = alTop
    Color = 10643006
    TabOrder = 2
    DesignSize = (
      850
      150)
    object lbLbCliente: TLabel
      Left = 7
      Top = 40
      Width = 32
      Height = 14
      Caption = 'Cliente'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbNomeCliente: TLabel
      Left = 207
      Top = 43
      Width = 313
      Height = 13
      Cursor = crHandPoint
      AutoSize = False
      Caption = 'Cliente'
      Font.Charset = ANSI_CHARSET
      Font.Color = clAqua
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnClick = lbNomeClienteClick
      OnMouseMove = lbNomeClienteMouseMove
      OnMouseLeave = lbNomeVendedorMouseLeave
    end
    object lbNomeVendedor: TLabel
      Left = 207
      Top = 63
      Width = 313
      Height = 13
      Cursor = crHandPoint
      AutoSize = False
      Caption = 'Vendedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clAqua
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnClick = lbNomeVendedorClick
      OnMouseMove = lbNomeClienteMouseMove
      OnMouseLeave = lbNomeVendedorMouseLeave
    end
    object lbLbVendedor: TLabel
      Left = 7
      Top = 62
      Width = 47
      Height = 14
      Caption = 'Vendedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbLbDescricao: TLabel
      Left = 7
      Top = 18
      Width = 49
      Height = 14
      Caption = 'Descri'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbconcluido: TLabel
      Left = 633
      Top = 105
      Width = 191
      Height = 33
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = 'OS Concluida?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -24
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnClick = lbconcluidoClick
    end
    object lb9: TLabel
      Left = 7
      Top = 81
      Width = 240
      Height = 14
      Caption = 
        'Observa'#231#245'es.....................................................' +
        '.....'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb12: TLabel
      Left = 769
      Top = 13
      Width = 69
      Height = 18
      Anchors = [akTop, akRight]
      Caption = 'Pedido N'#176
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -13
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbNumPedido: TLabel
      Left = 769
      Top = 34
      Width = 54
      Height = 18
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = '171718'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -13
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Visible = False
      OnClick = lbNumPedidoClick
      OnMouseMove = lbNomeClienteMouseMove
      OnMouseLeave = lbNomeVendedorMouseLeave
    end
    object edtCliente: TEdit
      Left = 121
      Top = 40
      Width = 80
      Height = 20
      Color = 6073854
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnDblClick = edtClienteDblClick
      OnExit = edtClienteExit
      OnKeyDown = edtClienteKeyDown
      OnKeyPress = edtClienteKeyPress
    end
    object edtVendedor: TEdit
      Left = 121
      Top = 61
      Width = 80
      Height = 20
      Color = 6073854
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 9
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      OnDblClick = edtVendedorDblClick
      OnExit = edtVendedorExit
      OnKeyDown = edtVendedorKeyDown
      OnKeyPress = edtClienteKeyPress
    end
    object edtDescricao: TEdit
      Left = 121
      Top = 18
      Width = 400
      Height = 20
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 100
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
    end
    object memoObservacao: TMemo
      Left = 121
      Top = 83
      Width = 399
      Height = 46
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      Lines.Strings = (
        '')
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
    end
  end
  object panel3: TPanel
    Left = 0
    Top = 208
    Width = 850
    Height = 273
    Align = alClient
    Caption = 'panel3'
    TabOrder = 3
    object pgcPagServicos: TPageControl
      Left = 1
      Top = 1
      Width = 848
      Height = 271
      ActivePage = tsServicosVidros
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Style = tsFlatButtons
      TabOrder = 0
      OnChange = pgcPagServicosChange
      object ts2: TTabSheet
        Caption = 'Vidros'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        object pnl3: TPanel
          Left = 0
          Top = 0
          Width = 840
          Height = 61
          Align = alTop
          Color = 14024703
          TabOrder = 0
          object lb4: TLabel
            Left = 4
            Top = 18
            Width = 30
            Height = 14
            Caption = 'Vidro'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lbLbNomeProduto: TLabel
            Left = 4
            Top = 44
            Width = 316
            Height = 16
            AutoSize = False
            Caption = 'Produto'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lb5: TLabel
            Left = 343
            Top = 18
            Width = 26
            Height = 14
            Caption = 'Qtde'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lb6: TLabel
            Left = 445
            Top = 18
            Width = 28
            Height = 14
            Caption = 'Valor'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object lb7: TLabel
            Left = 200
            Top = 63
            Width = 76
            Height = 14
            Caption = 'Desconto (R$)'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object lb8: TLabel
            Left = 341
            Top = 64
            Width = 72
            Height = 14
            Caption = 'Desconto (%)'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object lb10: TLabel
            Left = 9
            Top = 88
            Width = 44
            Height = 14
            Caption = 'Material'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object lbCodigoMaterial: TLabel
            Left = 672
            Top = 16
            Width = 3
            Height = 13
            Visible = False
          end
          object lb11: TLabel
            Left = 115
            Top = 18
            Width = 33
            Height = 14
            Caption = 'Altura'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lb14: TLabel
            Left = 225
            Top = 18
            Width = 43
            Height = 14
            Caption = 'Largura'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object imgCodBar: TImage
            Left = 594
            Top = 4
            Width = 105
            Height = 53
            AutoSize = True
          end
          object btGravaprodutos: TBitBtn
            Left = 720
            Top = 18
            Width = 39
            Height = 39
            TabOrder = 7
            OnClick = btGravaprodutosClick
          end
          object btExcluiProdutos: TBitBtn
            Left = 759
            Top = 18
            Width = 39
            Height = 39
            TabOrder = 8
            OnClick = btExcluiProdutosClick
          end
          object btCancelaProduto: TBitBtn
            Left = 798
            Top = 18
            Width = 39
            Height = 39
            TabOrder = 9
            OnClick = btCancelaProdutoClick
          end
          object edtproduto: TEdit
            Left = 39
            Top = 15
            Width = 60
            Height = 20
            Color = 6073854
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            OnDblClick = edtprodutoDblClick
            OnExit = edtprodutoExit
            OnKeyDown = edtprodutoKeyDown
            OnKeyPress = edtClienteKeyPress
          end
          object edtquantidade_pedido: TEdit
            Left = 373
            Top = 15
            Width = 60
            Height = 20
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
          end
          object edtvalor_pedido: TEdit
            Left = 476
            Top = 15
            Width = 60
            Height = 20
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            Visible = False
            OnExit = edtvalor_pedidoExit
          end
          object edtdesconto_reais_pedido: TEdit
            Left = 278
            Top = 62
            Width = 60
            Height = 20
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            Visible = False
          end
          object edtdesconto_porcento_pedido: TEdit
            Left = 418
            Top = 61
            Width = 60
            Height = 20
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            Visible = False
          end
          object cbbMateriais: TComboBox
            Left = 72
            Top = 82
            Width = 145
            Height = 21
            ItemHeight = 13
            TabOrder = 10
            Visible = False
            OnClick = cbbMateriaisClick
            Items.Strings = (
              'Vidro'
              'Ferragens'
              'Diversos'
              'Kit Box'
              'Perfilados'
              'Persianas')
          end
          object edtaltura: TEdit
            Left = 151
            Top = 15
            Width = 60
            Height = 19
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
          end
          object edtlargura: TEdit
            Left = 271
            Top = 15
            Width = 60
            Height = 19
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            OnExit = edtlarguraExit
          end
        end
        object dbgrdMaterias: TDBGrid
          Left = 0
          Top = 61
          Width = 840
          Height = 146
          TabStop = False
          Align = alClient
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial Narrow'
          Font.Style = []
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          PopupMenu = PopUp1
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = 10643006
          TitleFont.Height = -11
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          OnDrawColumnCell = dbgrdMateriasDrawColumnCell
          OnDblClick = dbgrdMateriasDblClick
          OnKeyPress = dbgrdMateriasKeyPress
        end
        object pnl7: TPanel
          Left = 0
          Top = 207
          Width = 840
          Height = 32
          Align = alBottom
          TabOrder = 2
          Visible = False
          object edtPesquisaVidroOS: TEdit
            Left = 16
            Top = 5
            Width = 393
            Height = 19
            CharCase = ecUpperCase
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 0
            Visible = False
            OnExit = edtPesquisaVidroOSExit
            OnKeyPress = edtPesquisaVidroOSKeyPress
          end
        end
      end
      object ts1: TTabSheet
        Caption = 'Servi'#231'os'
        object pnl2: TPanel
          Left = 0
          Top = 0
          Width = 840
          Height = 81
          Align = alTop
          BevelOuter = bvNone
          Color = 14024703
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
          object lb1: TLabel
            Left = 4
            Top = 7
            Width = 43
            Height = 15
            Caption = 'Servi'#231'o'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lb2: TLabel
            Left = 261
            Top = 57
            Width = 27
            Height = 15
            Caption = 'Qtde'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lb3: TLabel
            Left = 380
            Top = 57
            Width = 29
            Height = 15
            Caption = 'Valor'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lbnomefuncservico: TLabel
            Left = 4
            Top = 32
            Width = 30
            Height = 15
            Caption = 'Func.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lbNomeFuncionarioServico: TLabel
            Left = 124
            Top = 30
            Width = 177
            Height = 15
            AutoSize = False
            Caption = 'Funcion'#225'rio'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lbnomeservico: TLabel
            Left = 124
            Top = 8
            Width = 54
            Height = 13
            Caption = 'lbServico'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lbCodigoServiOS: TLabel
            Left = 688
            Top = 0
            Width = 18
            Height = 14
            Caption = 'cod'
            Visible = False
          end
          object lblAltura: TLabel
            Left = 4
            Top = 57
            Width = 34
            Height = 15
            Caption = 'Altura'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lblLarg: TLabel
            Left = 128
            Top = 57
            Width = 45
            Height = 15
            Caption = 'Largura'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lblControla: TLabel
            Left = 380
            Top = 8
            Width = 140
            Height = 15
            Caption = 'Controla Por Milimetros: '
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object btGravaServico: TBitBtn
            Left = 720
            Top = 18
            Width = 39
            Height = 39
            TabOrder = 5
            OnClick = btGravaServicoClick
          end
          object btExcluiServico: TBitBtn
            Left = 759
            Top = 18
            Width = 39
            Height = 39
            TabOrder = 6
            OnClick = btExcluiServicoClick
          end
          object btCancelaServico: TBitBtn
            Left = 798
            Top = 18
            Width = 39
            Height = 39
            TabOrder = 7
            OnClick = btCancelaServicoClick
          end
          object edtservico: TEdit
            Left = 52
            Top = 4
            Width = 60
            Height = 20
            Color = 6073854
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = edtservicoClick
            OnDblClick = edtservicoDblClick
            OnExit = edtservicoExit
            OnKeyDown = edtservicoKeyDown
            OnKeyPress = edtClienteKeyPress
          end
          object edtquantidadeservico: TEdit
            Left = 299
            Top = 54
            Width = 60
            Height = 20
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 4
            ParentFont = False
            TabOrder = 3
            OnKeyPress = edtquantidadeservicoKeyPress
          end
          object edtvalorservico: TEdit
            Left = 419
            Top = 54
            Width = 60
            Height = 20
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 6
            ParentFont = False
            TabOrder = 8
            OnExit = edtvalorservicoExit
            OnKeyPress = edtvalorservicoKeyPress
          end
          object edtFuncionarioServico: TEdit
            Left = 52
            Top = 29
            Width = 60
            Height = 20
            Color = 6073854
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            OnDblClick = edtFuncionarioServicoDblClick
            OnExit = edtFuncionarioServicoExit
            OnKeyDown = edtFuncionarioServicoKeyDown
            OnKeyPress = edtClienteKeyPress
          end
          object edtAltServ: TEdit
            Left = 52
            Top = 54
            Width = 60
            Height = 20
            MaxLength = 4
            TabOrder = 1
            OnExit = edtAltServExit
            OnKeyPress = edtAltServKeyPress
          end
          object edtLargServ: TEdit
            Left = 184
            Top = 54
            Width = 60
            Height = 20
            MaxLength = 4
            TabOrder = 2
            OnExit = edtLargServExit
            OnKeyPress = edtLargServKeyPress
          end
          object rbSim: TRadioButton
            Left = 528
            Top = 8
            Width = 44
            Height = 17
            Caption = 'Sim'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 9
            OnClick = rbSimClick
          end
          object rbNao: TRadioButton
            Left = 576
            Top = 8
            Width = 46
            Height = 17
            Caption = 'Nao'
            Checked = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 10
            TabStop = True
            OnClick = rbNaoClick
          end
        end
        object dbgrdServicos: TDBGrid
          Left = 0
          Top = 81
          Width = 840
          Height = 126
          TabStop = False
          Align = alClient
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 10643006
          TitleFont.Height = -11
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          OnDrawColumnCell = dbgrdServicosDrawColumnCell
          OnDblClick = dbgrdServicosDblClick
          OnKeyPress = dbgrdServicosKeyPress
        end
        object pnl6: TPanel
          Left = 0
          Top = 207
          Width = 840
          Height = 32
          Align = alBottom
          TabOrder = 2
          Visible = False
          object edtpesquisa: TEdit
            Left = 16
            Top = 5
            Width = 393
            Height = 20
            CharCase = ecUpperCase
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 0
            Visible = False
            OnExit = edtpesquisaExit
            OnKeyPress = edtpesquisaKeyPress
          end
        end
      end
      object tsServicosVidros: TTabSheet
        Caption = 'Servi'#231'os por Vidros'
        ImageIndex = 4
        object STRGVidrosServicos: TStringGrid
          Left = 420
          Top = 25
          Width = 420
          Height = 214
          Align = alClient
          Ctl3D = False
          FixedCols = 0
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Options = [goHorzLine]
          ParentCtl3D = False
          ParentFont = False
          PopupMenu = PopUpCadastros
          TabOrder = 1
          OnDblClick = STRGVidrosServicosDblClick
          OnDrawCell = STRGVidrosServicosDrawCell
          OnKeyDown = STRGVidrosServicosKeyDown
          ColWidths = (
            64
            64
            64
            64
            64)
        end
        object STRGListaVidros: TStringGrid
          Left = 0
          Top = 25
          Width = 420
          Height = 214
          Align = alLeft
          Ctl3D = False
          FixedCols = 0
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Options = [goHorzLine]
          ParentCtl3D = False
          ParentFont = False
          PopupMenu = PopUpVidros
          TabOrder = 0
          OnClick = STRGListaVidrosClick
          OnDblClick = StrGridVidrosDblClick
          OnKeyDown = STRGListaVidrosKeyDown
          ColWidths = (
            64
            64
            64
            64
            64)
        end
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 840
          Height = 25
          Align = alTop
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 2
          object lb20: TLabel
            Left = 124
            Top = 3
            Width = 52
            Height = 20
            Caption = 'Vidros'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 10643006
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lb21: TLabel
            Left = 492
            Top = 2
            Width = 141
            Height = 20
            Caption = 'Servi'#231'os no Vidro'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 10643006
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
        end
      end
      object tsControleCod: TTabSheet
        Caption = 'Controle de Execu'#231#227'o'
        ImageIndex = 3
        object panel5: TPanel
          Left = 0
          Top = 0
          Width = 840
          Height = 239
          Align = alClient
          Color = clWhite
          TabOrder = 0
          DesignSize = (
            840
            239)
          object shp2: TShape
            Left = 312
            Top = 136
            Width = 505
            Height = 81
            Pen.Color = 10643006
            Shape = stRoundRect
          end
          object img1: TImage
            Left = 85
            Top = 66
            Width = 105
            Height = 53
            AutoSize = True
          end
          object shpVidro: TShape
            Left = 312
            Top = 32
            Width = 505
            Height = 73
            Pen.Color = 10643006
            Shape = stRoundRect
          end
          object btProcessar: TSpeedButton
            Left = 10
            Top = 194
            Width = 102
            Height = 29
            Cursor = crHandPoint
            Anchors = [akRight, akBottom]
            BiDiMode = bdLeftToRight
            Caption = 'Confirmar'
            Flat = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            Glyph.Data = {
              A2070000424DA207000000000000360000002800000019000000190000000100
              1800000000006C070000130B0000130B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4F0F1F3EFF2EEEEEEE8EDE8E7ED
              E8ECEFECF6F3F6FBF5F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFEE6DFE5BFC5
              BD84AF885EA86748A95746AF574FB86475C585AAD4B2F0F2F0FEFCFEFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFD
              FEF3F2F2BECCBF579A5A18861F078911039216009C1C00A62100B22605BC280C
              C42D2ECC518FDD9FE2E7E3FDFBFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFE5E1E389AC89278628007D0901840F008C1400931800
              9B1D01A52100AE2601B82A02C32E03CE3106D43256D66FCCE1D1FDFCFDFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFDE5E0E37CAB7D0E7F11007E0B00
              820F018611018C1601921801991A00A21F00AB2301B52901BD2C05C73105D035
              0AD83436D556CADECEFDFCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFD8D789
              AF8B0F881300840C00820D00840F00860D008A0F00911700981A019F1E00A723
              01AF2501B72902C12E03C73107CE3505D53140D15CD2D8D2FBF9FAFFFFFFFFFF
              FFFFFFFFFFFDFCFDB2BBAE28952E018B0C00890F00860E00860E23932B319635
              068911009518009C1B00A41F00AA2301B22600BA2900C02D04C63005CB3309CE
              3172C983F0F1EFFFFFFFFFFFFFFFFFFFFFE0D4D763A86C00920C009010018E10
              018B0F03890C89BC8BDCDADC93BC95339837079315009D1A00A52001AA2301B2
              2500B72900BC2B02C02E02C32E1DC23CAFC8B2FFFFFFFFFFFFFFFFFFFFD3D5D2
              30A349089A1A009510019311008F10048B0D87BC8ADCD7DBDBD7DAC4D0C576B4
              7919962201991501A62100AC2200AF2500B42700B72800BA2A02B92576C082E8
              DFE4FFFFFFFFEFE8EA94B29521A73E1CA337059B1600971100951104910F8AC1
              8DDFDCDFD8D9D9D9D8D8DFDAE0CAD5CC65B16B1D9C2801A01901A72000AB2301
              AD2401AF2601B02232B048DDD6DAFFFFFFFFEDEAEB76B28121AB4124A94317A4
              32039C140099120495108DC591E6E3E5DFE0DEDEDEDEDADBDBDEDBDEDEDCDEB3
              CEB54CAA530B981800A01A00A52000A62100A72113A52CCDD5CEFFFFFFFFE8E9
              E864B37526B24625AE4623AB4418A533069E19049A1090CA94EDEAECE6E6E6E4
              E4E5E2E2E2E1E1E1E0E0E0E0DFDFDEDEDEADCDAE34A241019A1A009E1C009E1D
              0D9B1DBCD1BDFFFFFFFFE7E9E761B57429B84A29B44927B04726AD451DA83B0C
              A01E92CF97F3F1F2ECEDECEAEAEBE8E8E8E6E7E7E6E6E6E5E5E3E6E4E5F2E7F1
              8CC3920593160097180197190C9317B7CEB8FFFFFFFFEAE9E968B87A2CBD4D2D
              BA4D2CB64B2AB34A26AF4721AB3F9DD7A9F7F7F7F2F3F3F1F2F1F1EFEFEFEEEF
              EEEEEEEFEDF0D0DFD072B67418941E0292140092150091160D8E18BCCEBCFFFF
              FFFFEEEAEB7DB8892FC15130BF512EBC4E2DB94D2BB64A27B047A3DBB0FDFDFD
              FAFAFAF7F7F7F6F6F6F7F7F7DBEADB88C4881E9620008E09009211019012008E
              12008D10148E20CDD2CBFFFFFFFFF1E9EA9CB89D33C65533C45532C15431BE51
              2FBB4E2BB64CA5DEB2FDFEFDFCFCFCFDFDFDF4FAF6ACDBB73DAA4F0F961F0692
              18049217039115018E15028D14028911369440DAD4D7FFFFFFFFFFFFFFD8D7D7
              4EC66A36CC5937C75736C65633C15330BE51A8E3B5FDFDFDF1F9F3B6E3C152BB
              691BA53B21A64122A4421FA03E1D9C3B1A98361795341591310F8D2C7CAB86E6
              DEDFFFFFFFFFFFFFFFE4D9DC86C4933AD05D3ACD5D39C95B37C65933C45497E0
              A7CFEED677CE8C33B75127B24729B04928AE4725AA4423A74222A3401F9F3C1C
              9B391898342C9944B1BBB0FFFFFFFFFFFFFFFFFFFFFDFDFDD1D9D265D07E3DD3
              5E3ED05E3DCD5E39CA5A3AC85A38C35731C05233BE5331BB502FB64E2CB44C2A
              B14A28AD4727A94524A64322A340209E3C7EAF88EFEEEEFFFFFFFFFFFFFFFFFF
              FFFFFFFFF9F8F8C0DDC64FD76F3ED56040D2613ECF5F3CCC5C3AC85B39C75A36
              C45735C15533BE5331BB512FB84F2CB34D2BB04A29AD4821A84055AD69CFCECC
              FAF8F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFBF9FABBE5C64FD86E40D76242
              D56440D2613DCF603DCC5E3BCA5C38C75A37C45836C25635BF5432BA532FB750
              2BB44B52B56AC7CFC8FCFAFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFAF9F9C0DEC569D78242DB6542D76441D36440D2623FD0603ECC5F3CCA5D
              3BC75B39C45933C15336BC5772BE84CAD2CCFBFBFBFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEEEE6E5CCD1C88FCE9D5ED57B47D969
              42D76441D5633FD2623ECF603BCC5C40C96062C67AA0C6A8D0CBC9FAF9FAFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FEFDFDE5DADCDCDCDCA9C4AB8FC99B7ECB8E78C88A82C99297C69FC1D6C4DFD9
              DCF4F0F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1EBECEFECEDECEBEBEBEB
              EAEDECECF0EBEDFBF9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF}
            ParentFont = False
            ParentBiDiMode = False
            Visible = False
          end
          object btCancelarExecucao: TSpeedButton
            Left = 5
            Top = 226
            Width = 107
            Height = 27
            Cursor = crHandPoint
            Hint = 'Ajuda'
            Anchors = [akRight, akBottom]
            Caption = 'Cancelar'
            Flat = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            Glyph.Data = {
              A2070000424DA207000000000000360000002800000019000000190000000100
              1800000000006C070000130B0000130B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFFFFFFFFFFFFFEFFFEFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFBFCFEECEEFAEF
              F0F8FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF
              FFFFFDFDFFD0D3F3717CE77079D4DADBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFDFDFEF7F7FAFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F8FD9CA4F25469F1364ADF959ADAFDFDFE
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFD1D1EFC5C5E6FDFDFDFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFF6F6FD919AF3
              485DE9243BDB6E76D7FBFBFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9
              FA7777DFC1C2EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFAFAFEA7ADF5475DEC2139D84E5ACEEFEFF4FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFEFEFFAEB1EF6066E2ECEEF9FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFDDE0FB6475F01E38DA1B30
              CFA0A3D6FAFAFCFFFFFFFFFFFEFFFFFFFEFEFFDCDFF44254EA99A1EEFDFDFEFF
              FFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFFFFFFFFFF
              FFFBFBFFB5BBF52F46E21831D84754CCD1D1E7FEFEFEFEFEFEFFFFFFEBECF877
              82EB4C5DEBDADDF5FFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7FD909AEF2B43DF1831D5616BCBE6
              E6F2FEFFFFEFF0F88590E73249EDAFB6EFFBFCFEFFFFFFFFFFFFFFFEFFFFFFFE
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFFFFFFFFFFFFFFFFFFEB
              EDFB8793EF1832DB2239D7888FD2E1E2ED939BE52942EC6574E8F5F6FCFEFEFF
              FFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFEFEFFEEEFFD7583E9223BDC2F41CF525EC8283FE1
              3E53E6CED0F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFEDECFA
              6E79DC0D24CD0820CC1A2ECDB1B5E9FBFBFEFFFFFFFFFFFFFEFFFFFFFFFEFDFE
              FEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFEFEFFF0F1F79AA0E23246D6031DCD041DCD1327C3989ACCF3F3F6FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
              FFFFFFFFFFFFFEFEFEFFFFFFF5F5F9C7C9EA8088E43247EA1431ED1F39E7737D
              E34B5DE01D33DB777FD2EBEBF2FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFEE4E6F1B2B5E68894F07484
              F45065F13F56F6919CF6EFEFFBD7DBF86C7DF84356F18389D4DBDCE9FEFEFEFF
              FFFFFEFEFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFD9DA
              F48692E98392F58B9AF48493F78695FBBDC3FAF8F8FEFFFFFFFEFFFFF2F3FDBC
              C3FA818DF48088DCC0C2DFF1F1F7FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFEFFFFFFE5E7FB9CA7FA7888F28C99F6AAB3FCDDE0FCFBFBFEFF
              FFFFFFFFFFFFFFFFFFFFFFFDFCFEE0E3FCBAC0FB868DE49598D7D7D7EDFEFEFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFEFF1FDD4D9FCDB
              DEFDF3F3FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFAFAFF
              E1E4FCB2B7F0BABAEBFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFEFFFFFEFEFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
              FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF}
            ParentFont = False
            Visible = False
          end
          object lbVidro: TLabel
            Left = 313
            Top = 5
            Width = 135
            Height = 22
            Cursor = crHandPoint
            Caption = 'Nome do vidro'
            Font.Charset = ANSI_CHARSET
            Font.Color = 10643006
            Font.Height = -19
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            Transparent = True
            OnClick = lbconcluidoClick
          end
          object lbServico: TLabel
            Left = 313
            Top = 109
            Width = 158
            Height = 22
            Cursor = crHandPoint
            Caption = 'Nome do Servi'#231'o'
            Font.Charset = ANSI_CHARSET
            Font.Color = 10643006
            Font.Height = -19
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            Transparent = True
            OnClick = lbconcluidoClick
          end
          object lbNomedoVidro: TLabel
            Left = 321
            Top = 43
            Width = 135
            Height = 22
            Cursor = crHandPoint
            Caption = 'Nome do vidro'
            Font.Charset = ANSI_CHARSET
            Font.Color = 6073854
            Font.Height = -19
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            Transparent = True
            OnClick = lbconcluidoClick
          end
          object lbNomedoServico: TLabel
            Left = 321
            Top = 155
            Width = 158
            Height = 22
            Cursor = crHandPoint
            Caption = 'Nome do Servi'#231'o'
            Font.Charset = ANSI_CHARSET
            Font.Color = 6073854
            Font.Height = -19
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            Transparent = True
            OnClick = lbconcluidoClick
          end
          object edtCodigoBarras: TEdit
            Left = 8
            Top = 136
            Width = 295
            Height = 33
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'Arial'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            OnKeyPress = edtCodigoBarrasKeyPress
          end
        end
      end
    end
  end
  object ilProdutos: TImageList
    Tag = 1
    Left = 8
    Top = 161
    Bitmap = {
      494C01010E001300040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000005000000001002000000000000050
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D2D2
      D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2
      D200000000000000000000000000000000000000000000000000000000002EB1
      C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1
      C50062C4D3000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D2D2D200D2D2
      D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2
      D200D2D2D20000000000000000000000000000000000000000002EB1C5002EB1
      C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1
      C5002EB1C5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D1D1D100D1D1
      D100D1D1D100D1D1D100D1D1D100D1D1D100D1D1D100D1D1D100D1D1D100D1D1
      D100D1D1D10000000000000000000000000000000000000000002EB1C5002EB1
      C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1
      C5002EB1C5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D0D0D000D0D0
      D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0
      D000D0D0D00000000000000000000000000000000000000000002EB1C5002EB1
      C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1
      C5002EB1C5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D0D0D000D0D0
      D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0
      D000D0D0D00000000000000000000000000000000000000000002EB1C5002EB1
      C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1
      C5002EB1C5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CFCFCF00CFCF
      CF00CFCFCF00CFCFCF00CFCFCF00CFCFCF00CFCFCF00CFCFCF00CFCFCF00CFCF
      CF00CFCFCF0000000000000000000000000000000000000000002EB1C5002EB1
      C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1
      C5002EB1C5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CFCFCF00CFCF
      CF00CFCFCF00CFCFCF00CFCFCF00CFCFCF00CFCFCF00CFCFCF00CFCFCF00CFCF
      CF00CFCFCF0000000000000000000000000000000000000000002EB1C5002EB1
      C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1
      C5002EB1C5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE0000000000000000000000000000000000000000002EB1C5002EB1
      C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1
      C5002EB1C5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE0000000000000000000000000000000000000000002EB1C5002EB1
      C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1
      C5002EB1C5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CDCDCD00CDCD
      CD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCD
      CD00CDCDCD0000000000000000000000000000000000000000002EB1C5002EB1
      C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1
      C5002EB1C5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC0000000000000000000000000000000000000000002EB1C5002EB1
      C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1
      C5002EB1C5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DFDFDF00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00DFDFDF000000000000000000000000000000000000000000000000002EB1
      C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1C5002EB1
      C50062C4D3000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F3F1
      F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1
      F000F4F2F1000000000000000000000000000000000000000000000000000101
      0100020202000202020002020200020202000202020001010100010101000101
      0100313131000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00002F2F2F00000000000000000000000000000000000000000000000000E3E3
      E300DADADA00D0D0D000C7C7C700BEBEBE00B6B6B600ADADAD00A5A5A5009D9D
      9D00BEBEBE000000000000000000000000000000000000000000F3F1F000F3F1
      F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1
      F000F3F1F0000000000000000000000000000000000000000000111111006969
      6900FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FBFBFB00C3C3
      C3000F0F0F0000000000000000000000000000000000000000000F0F0F000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E6E6
      E600DBDBDB00CFCFCF00C1C1C100B4B4B400A7A7A7009B9B9B00919191008787
      87009E9E9E000000000000000000000000000000000000000000F3F1F000F3F1
      F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1
      F000F3F1F000000000000000000000000000000000000000000001010100C4C4
      C400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001F1F1F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F6F6F600F2F2
      F200EBEBEB00E1E1E100D2D2D200C2C2C200B4B4B400A9A9A9009D9D9D009292
      9200A7A7A7000000000000000000000000000000000000000000F3F1F000F3F1
      F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1
      F000F3F1F000000000000000000000000000000000000000000001010100C6C6
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001E1E1E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FAFAFA00F8F8
      F800F4F4F400EEEEEE00E2E2E200D2D2D200C3C3C300B6B6B600A9A9A9009D9D
      9D00AFAFAF000000000000000000000000000000000000000000F3F1F000F3F1
      F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1
      F000F3F1F000000000000000000000000000000000000000000001010100C6C6
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001E1E1E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FCFCFC00FCFC
      FC00FAFAFA00F8F8F800F0F0F000E2E2E200D3D3D300C5C5C500B6B6B600A8A8
      A800B6B6B6000000000000000000000000000000000000000000F3F1F000F3F1
      F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1
      F000F3F1F000000000000000000000000000000000000000000001010100C6C6
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001E1E1E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FDFDFD00FDFD
      FD00FDFDFD00FCFCFC00F9F9F900EFEFEF00E1E1E100D2D2D200C4C4C400B3B3
      B300BEBEBE000000000000000000000000000000000000000000F3F1F000F3F1
      F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1
      F000F3F1F000000000000000000000000000000000000000000001010100C6C6
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001E1E1E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FDFDFD00FDFD
      FD00FDFDFD00FDFDFD00FCFCFC00F7F7F700EDEDED00DFDFDF00D0D0D000BEBE
      BE00C5C5C5000000000000000000000000000000000000000000F3F1F000F3F1
      F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1
      F000F3F1F000000000000000000000000000000000000000000001010100C6C6
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001E1E1E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FDFDFD00FEFE
      FE00FEFEFE00FDFDFD00FDFDFD00FCFCFC00F6F6F600EBEBEB00DCDCDC00CACA
      CA00CDCDCD000000000000000000000000000000000000000000F3F1F000F3F1
      F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1
      F000F3F1F000000000000000000000000000000000000000000001010100C6C6
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001E1E1E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFE00FEFE
      FE00FEFEFE00FEFEFE00FDFDFD00FDFDFD00FBFBFB00F4F4F400E6E6E600D5D5
      D500D5D5D5000000000000000000000000000000000000000000F3F1F000F3F1
      F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1
      F000F3F1F000000000000000000000000000000000000000000001010100C6C6
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001D1D1D000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFE00FEFE
      FE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FAFAFA0000000000E2E2
      E200E0E0E0000000000000000000000000000000000000000000F4F2F100F3F1
      F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1F000F3F1
      F000F3F1F0000000000000000000000000000000000000000000111111006E6E
      6E00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFEFE00D3D3
      D3000C0C0C0000000000000000000000000000000000000000000F0F0F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFEFE00F9F9F900EDED
      ED00EAEAEA000000000000000000000000000000000000000000FDFCFC00F7F6
      F600F6F4F400F6F4F400F6F4F400F6F4F400F6F4F400F6F4F400F6F4F400F7F5
      F500FBFBFA000000000000000000000000000000000000000000A0A0A0000000
      0000010101000101010001010100010101000101010001010100010101000101
      01003131310000000000000000000000000000000000000000008F8F8F000000
      0000000000000000000000000000000000000000000000000000000000000000
      00002F2F2F000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFCFC00F7F7
      F700F5F5F5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F6F6F6003737BB00FBFB
      FB00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF001A1A1A000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000ACACAC00FFFFFF00FFFFFF00FFFFFF001A1A1A000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000ACACAC00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000576BE5003B51DF002122
      BD00000000000000000000000000000000000000000000000000000000000000
      000000000000DFDFDF000000000000000000FFFFFF0000000000040404000404
      0400030303000303030003030300030303000303030003030300030303000303
      0300030303000404040001010100ADADAD00FFFFFF0000000000040404000404
      0400030303000303030003030300030303000303030003030300030303000303
      0300030303000404040001010100ADADAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A6DE8003147DB001E37
      D400000000000000000000000000000000000000000000000000000000000000
      0000EBECE700FDFDFE0000000000000000007878780006060600010101009D9D
      9D00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E1E1E1000707070007070700000000007878780006060600010101009D9D
      9D00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00EAEAEA001313130007070700000000000000000000000000000000004C1F
      D2004C1FD2004C1FD2004C1FD2004C1FD2004C1FD2004C1FD2004C1FD2004C1F
      D20000000000000000000000000000000000000000005064E6002B42D9001932
      D200F9F9F9000000000000000000000000000000000000000000000000000000
      00001E24D2000000000000000000000000006A6A6A000A0A0A0036363600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E0E0E00009090900000000006A6A6A000A0A0A0036363600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00EAEAEA0009090900000000000000000000000000491CD200491C
      D200491CD200491CD200491CD200491CD200491CD200491CD200491CD200491C
      D200491CD200000000000000000000000000000000005A6BE900283FD5001831
      D0009494AC00000000000000000000000000000000000000000000000000393A
      CD002529C2000000000000000000000000006C6C6C000D0D0D0055555500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000C0C0C00000000006C6C6C000D0D0D0055555500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000C0C0C000000000000000000000000004719D1004719
      D1004719D1004719D1004719D1004719D1004719D1004719D1004719D1004719
      D1004719D1000000000000000000000000000000000000000000243CD4001730
      CF001731D200FCFCFC0000000000000000000000000000000000F4F4F4000924
      DA00000000000000000000000000000000006E6E6E001010100055555500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000F0F0F00000000006E6E6E001010100055555500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000202020000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000F0F0F000000000000000000000000004516D0004516
      D0004516D0004516D0004516D0004516D0004516D0004516D0004516D0004516
      D0004516D00000000000000000000000000000000000000000005C5EDE001E36
      CF00162FD0003030C20000000000000000000000000000000000273FE200262A
      CF0000000000000000000000000000000000717171001414140058585800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001313130000000000717171001414140058585800FFFF
      FF00FFFFFF00FFFFFF000808080014141400141414003B3B3B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00131313000000000000000000000000004213D0004213
      D0004213D0004213D0004213D0004213D0004213D0004213D0004213D0004213
      D0004213D0000000000000000000000000000000000000000000000000005164
      DB00102ACD001A33D400BCBCBC0000000000000000002C3DC400122CD2000000
      00000000000000000000000000000000000073737300171717005A5A5A00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00161616000202020073737300171717005A5A5A00FFFF
      FF00FFFFFF000A0A0A0017171700171717001717170016161600ABABAB00FFFF
      FF00FFFFFF00FFFFFF001A1A1A000202020000000000000000004111CF004111
      CF004111CF004111CF004111CF004111CF004111CF004111CF004111CF004111
      CF004111CF000000000000000000000000000000000000000000000000000000
      0000606BE100112ACA002F45DC00D1D1D1002631BB000A25D400B3B2DE000000
      000000000000000000000000000000000000757575001C1C1C005E5E5E00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001B1B1B0008080800757575001C1C1C005E5E5E00E2E2
      E200171717001C1C1C001C1C1C001B1B1B001A1A1A001C1C1C001C1C1C006E6E
      6E00FFFFFF00FFFFFF00FFFFFF00AAAAAA0000000000000000003E0ECF003E0E
      CF003E0ECF003E0ECF003E0ECF003E0ECF003E0ECF003E0ECF003E0ECF003E0E
      CF003E0ECF000000000000000000000000000000000000000000000000000000
      0000000000004357D3000F26C7001831D4000C27CE002137CD00000000000000
      000000000000000000000000000000000000777777001F1F1F0060606000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001E1E1E000B0B0B00777777001F1F1F0060606000FFFF
      FF00494949001F1F1F001F1F1F004D4D4D00C9C9C900191919001F1F1F001B1B
      1B00CFCFCF00FFFFFF00FFFFFF00FFFFFF0000000000000000003C0BCE003C0B
      CE003C0BCE003C0BCE003C0BCE003C0BCE003C0BCE003C0BCE003C0BCE003C0B
      CE003C0BCE000000000000000000000000000000000000000000000000000000
      000000000000EAEAEA00001AC6000019C0000019C00000000000000000000000
      000000000000000000000000000000000000797979002222220062626200FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00212121000E0E0E00797979002222220062626200FFFF
      FF00FFFFFF005050500033333300FFFFFF00FFFFFF00D3D3D3001B1B1B002222
      220016161600FFFFFF00FFFFFF00FFFFFF0000000000000000003908CE003908
      CE003908CE003908CE003908CE003908CE003908CE003908CE003908CE003908
      CE003908CE000000000000000000000000000000000000000000000000000000
      00005050C0000724E300031FD600011BC700001AC40008118900E8E8E8000000
      0000000000000000000000000000000000007B7B7B00262626005E5E5E00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0025252500121212007B7B7B00262626005E5E5E00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CFCFCF002020
      20002626260017171700FFFFFF00FFFFFF0000000000000000003705CD003705
      CD003705CD003705CD003705CD003705CD003705CD003705CD003705CD003705
      CD003705CD000000000000000000000000000000000000000000D7D7D7005061
      D7004D62EA002741E6000A27E40000000000DEDEF800031FD6000B1CB400EBEB
      EB0000000000000000000000000000000000767676002828280035353500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00AAAAAA002828280015151500767676002828280035353500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C9C9
      C90022222200282828008C8C8C00FFFFFF0000000000000000003502CC003502
      CC003502CC003502CC003502CC003502CC003502CC003502CC003502CC003502
      CC003502CC00000000000000000000000000F5F5F5004F53BA008C99EF00808F
      EE007081ED00455BE900000000000000000000000000000000005A6EEC005669
      EA00B5B5B500000000000000000000000000BFBFBF002A2A2A002B2B2B003838
      3800565656005656560056565600565656005656560056565600565656005656
      560053535300262626002B2B2B0041414100BFBFBF002A2A2A002B2B2B003838
      380056565600565656005555550087878700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00D0D0D00023232300C8C8C800FFFFFF0000000000000000007F5FDF003300
      CC003300CC003300CC003300CC003300CC003300CC003300CC003300CC003300
      CC007F5FDF000000000000000000000000005B6FE5006778E5008492EC008291
      EE007585ED00000000000000000000000000000000000000000000000000585B
      DF008795F1004141BE000000000000000000FFFFFF003F3F3F002E2E2E002F2F
      2F002F2F2F002F2F2F002F2F2F002F2F2F002F2F2F002F2F2F002F2F2F002F2F
      2F002F2F2F002F2F2F001C1C1C00FFFFFF00FFFFFF003F3F3F002E2E2E002F2F
      2F002F2F2F002F2F2F002F2F2F002F2F2F00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CCCCCC00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005B6DE3006778E6009097
      EE00000000000000000000000000000000000000000000000000000000000000
      000000000000828AEA0021259E00C6C6C600FFFFFF00FFFFFF00C1C1C1008282
      8200838383008383830083838300838383008383830083838300838383008383
      8300838383008F8F8F00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C1C1C1008282
      8200838383008383830083838300838383006F6F6F00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFDFF00FBFB
      FF00D2D3FF009890E700655CCA004B43C000443FC0004E46C3006E6AD400A2A2
      EE00FBF6FF00FFFCFF00FDFFFE00FDFFF8000000000000000000000000000000
      000000000000FBFBFB00E8E8E800D8D8D800D8D8D800E8E8E800FBFBFB000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F3F3F300D3D3D300B8B8B800B8B8B800D3D3D300F3F3F3000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FAFFFF00F8FBFF00EFEEFF007976
      BE001E1B99005F5CD000BCB6FF00C0B5FF00877CD800CDC5FF00ABA7FF00504B
      CE0028248B009D9ADF00F5F4FF00FBFBFF00000000000000000000000000FDFD
      FD00DADADA00006C0000007400000078000000770000006D0000005B0000DADA
      DA00FDFDFD00000000000000000000000000000000000000000000000000F8F8
      F800AFAFAF00070956000B0384000B0384000B0384000B03840006064400AFAF
      AF00F8F8F8000000000000000000000000000000000000000000000000000000
      00004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      220000000000000000000000000000000000FFFBFF00F7F2FF00847CDB001B18
      86009FA3F1008884D000EEE5FF00DED5FF008C86BD00F1F0FF00E4E3FF00817D
      C8002B29A00025228F00B1ABEC00FAF2FF000000000000000000F6F6F6002C85
      2C00008600000085000000820000007E0000007C0000007C0000007C00000073
      000038733800F7F7F70000000000000000000000000000000000EEEEEE001717
      65000B038B000A038B000A038B000A028B000A038B000A028B000A038B000A03
      8B0012124100EEEEEE0000000000000000000000000000000000000000004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB12200000000000000000000000000F3EDFF006A65B0002A2491006660
      CB00E4E6FF00F5F8FF00F3F3FF00FAF8FF00FCFBFF00FBFCFF00EEEFFD00FAF7
      FF00898DDC004F4EB5001F1488009F8DFF0000000000FDFDFD00008C00000093
      0000008D0000008A00000085000000810000007F0000007C0000007C0000007C
      0000007B0000005B0000FDFDFD000000000000000000F8F8F800060972000903
      960009039600090396000903960009039600090296000A039600090296000903
      96000A02960000003A00F8F8F8000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000A7A7FF00221F86007770D100EFE7
      FF00F4F2FF00FDFFFF00FBFFFF00F0F6FF00F0F6FF00F8FBFF00FDFEFF00FFFA
      FF00F8F4FF00E9E6FF00403EB6003330C20000000000D5D5D500009800000096
      000000930000008D000001890100C8C8C80012901200007F0000007C0000007C
      0000007C0000007A0000DDDDDD000000000000000000AFAFAF000802A1000803
      A2000802A200544FBE000902A2000802A2000802A1000802A2001C17A9000903
      A2000802A1000902A100AFAFAF000000000000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB1220000000000000000008687FF001F1D8E006C64C1009289
      CC00FDFBFF00FFFFFE00F9FCFF00C3C7E00065678900E3E7FF00FAFAFF00FBF7
      FF00ABA7C000A8A6E200484AC0002327BB00FAFAFA0000A10000009D0000009A
      000000960000008D0000E0E0E000DBDBDB00D7D7D70000820000007F0000007C
      0000007C0000007C000000690000FBFBFB00F3F3F300221FAE000702B1000702
      B100120F6800FAFAFA007774D4000702B1000702B0001813B500FBFBFB00706E
      C6000602B1000701B100201DA300F3F3F30000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000ABA5FF00221C8B007971D000EFE9
      FF00ECECFA00FDFFFF00FBFDFE00FBF8FF00F6F2FF009291CF006263A000D7D4
      FF00F1EFFF00EAE9FF00413EBC003937D000F9F9F90001A4010000A00000009D
      0000019B0100F5F5F500F1F1F100ECECEC00E8E8E800108F100000820000007E
      0000007C0000007C000000780000F4F4F400D3D3D3000502BE000601BF000602
      BE000501BE00C7C7D600FAFAFA005B59D400130FC100FBFBFB00000000000601
      BE000502BE000602BE000602BE00D3D3D30000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000E0DAFF00403A99004C46B100B1AD
      FF00BABCDB00F7F7FF00FDFDFF00F9FAFF00FAFAFF00F4F6FF00AEADDF00615D
      A9009E95E500ACA0FF00291BA2006252ED00A1CAA10000A6000000A4000001A0
      0100FEFEFE00FCFCFC000B820B00A5A2A500F3F3F300FCF5FC00008500000081
      0000007E0000007C0000007C0000B3C4B300BABABA000401CC000401CC000503
      CC000907CD000D0ACE00C8C8D700E9E9E900F2F2F200000000000703CD000501
      CC000401CC000401CC000402CC00BABABA0000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000FAF9FF00C9C9FF002B29A4002F2C
      B100EBE9FF00797CA800D6D9FF00E5E9FF00C1C3E200FBF8FF00A3A0BA00B2B6
      E600938DFF00261C98005B4FAF00E4D7FF009BCF9B0000A9000000A6000000A5
      000000A20000009E0000009A00000098000000000000F9F9F90068B968000085
      000000820000007F0000007C00008BB08B00B8B8B8000401DC000806DD000F0D
      DD001513DF001C1AE1002929E20000000000FEFEFE006866EA001211DE000A08
      DC000402DC000301DB000301DB00B8B8B80000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000FDFDFD00F3F4FF008E8DEF001C1A
      9200847FD4009F9FEB00E7E9FF00D2D3FF008381B500FAF6FF00E1DDFA008587
      BD0023219900342DA000C1B9FA00FAF2FF00BBD5BB0000AB000000A9000000A6
      000000A4000000A00000009D00000099000056775600FDFDFD00FCFCFC000089
      00000085000000820000007E0000D3DAD300CECECE000706E8001110EA001A19
      EB002322EB003535ED00F3F3F30000000000CBCBDA00E9E9E9006C6CF1001413
      EA000B0AE8000302E7000201E700CECECE0000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000FDFFFE00FBFEFF00FAFBFF00F4F1
      FF00493DB300231D9400201C9900322BB100403AA700D4D1FF00F4EFFF00887D
      EB00615ED200E0E1FF00F6F7FF00FEFEFE00FBFBFB000EB10E0000AB000000A8
      000000A6000000A4000000A00000009C0000009C0000FEFEFE00FEFEFE00269E
      26000089000000850000007F0000F9F9F900EFEFEF000E0DF2001B1BF2002727
      F3003838F400F2F2F200000000004344F4003F40F40000000000E9E9E9004B4C
      F5001313F3000908F2000100F200EFEFEF0000000000000000004CB122004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB122004CB122000000000000000000F6FFFF00F7FFFF00FAFFFF00F9F9
      FF00E5E1FF00AAA6ED006F6BD2002B23B600241BA700B7AFFC00F4ECFF00E5DA
      FF0015129D006060D200EAEBFF00FDFFFE00000000004CC64C0000AC000000AB
      000000A9000000A6000000A4000000A00000009E00007E8C7E00000000000000
      0000008B0000008A0000027F020000000000000000001B23BA002728FA003737
      FA002D2EA600000000005859F900595BF9005556F9004A4BF900CCCCDB002B2C
      F2001E1EFA001010FB0015187800000000000000000000000000000000004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB12200000000000000000000000000FDF9FE0000000000FDFFFC00FEFF
      FB00FBF6FF007B72C200534AC200554FE4001E1BAD006B6ADE0045459F00A9AA
      EE00625FAE00AEAAEB00F4F2FF00FFFFFF0000000000000000000DB10D0000AC
      000000AB000000A8000000A6000000A4000000A00000009C00001C641C001C62
      1C0012931200018D0100000000000000000000000000F2F2F2003132FB004344
      FA005254FA006264F9006A6CF9006C6EF9006668F900595BF9004A4CFA003738
      FB002627FB001717FC00F2F2F200000000000000000000000000000000004CB1
      22004CB122004CB122004CB122004CB122004CB122004CB122004CB122004CB1
      22004CB12200000000000000000000000000FDFDFD00FFFEFE0000000000FFFD
      FE00D1CCEC00635BB2009891FF001810B0001F18B5003B38B3009D9CFC005959
      A500F9F8FF0000000000FDFBFB00FFFEFD000000000000000000B3DAB30013B3
      130000AC000000AB000000A9000000A6000000A4000000A00000009E00000099
      000007980700AFD3AF0000000000000000000000000000000000E0E0E0004D4F
      FA006062F9007274F8007D7FF8007F81F800787AF800676AF9005658F9004142
      FA003132FB00E0E0E00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FEFEFE00FFFEFE00FFFD
      FF00E9E7FF005855A500847FF8004C45DC003E37CE006C69E4007776D4007270
      B600FCFBFF00FFFFF900FFFFFE00FFFFFE00000000000000000000000000F2F7
      F2003AC03A0001AD010000AB000000A8000000A7000000A4000000A0000019A6
      1900F5F8F500000000000000000000000000000000000000000000000000EEEE
      EE000E16C1007A7CF800878AF7008B8EF7008183F8006E71F8005C5EF9000B11
      A400EEEEEE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFE00FAFFFE00F7FF
      FB00F8F9FF00ECEBFF008785E90028239C002E2BA9003030A400AEAFFF00F6F1
      FF00FFFBFF000000000000000000FEFDFF000000000000000000000000000000
      000000000000A6DCA6004FC74F002DBA2D002DB82D003BBA3B0092D192000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FEFEFE00E2E2E200262DBF000E15BD000D15B900242BB600E2E2E200FEFE
      FE0000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000500000000100010000000000800200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFF00000000FFFFFFFF00000000
      E00FE00700000000C007C00700000000C007C00700000000C007C00700000000
      C007C00700000000C007C00700000000C007C00700000000C007C00700000000
      C007C00700000000C007C00700000000C007C00700000000C007E00700000000
      FFFFFFFF00000000FFFFFFFF00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      E007E007E007E007C007C007C007E007C007C007C007C007C007C007C007C007
      C007C007C007C007C007C007C007C007C007C007C007C007C007C007C007C007
      C007C007C007C007C007C007C007C027C007C007C007C007C007C007C007C007
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FFF00000000FFFF8FFB00000000FFFF
      8FF300000000E00F87F700000000C00787E700000000C007C3CF00000000C007
      C3CF00000000C007E19F00000000C007F01F00000000C007F83F00000000C007
      F87F00000000C007F01F00000000C007C10F00000000C00703C700000000C007
      07E300000000FFFF8FF800000000FFFFFFFFC000F81FF81FFFFF0000E007E007
      F00F0000C003C003E007000080018001C003000080018001C003000000000000
      C003000000000020C003000000000040C003000000800100C003000000000100
      C003000000000240C003000080318401E0074000C0038001E0072004C003C003
      FFFF0000E007E007FFFF8006F81FF00F00000000000000000000000000000000
      000000000000}
  end
  object PopUpCadastros: TPopupMenu
    Left = 53
    Top = 169
    object AtualizarServiosParaoVidro1: TMenuItem
      Caption = 'Atualizar Servi'#231'os no Vidro'
      OnClick = AtualizarServiosParaoVidro1Click
    end
    object AlterarCor1: TMenuItem
      Caption = 'Retornar/Concluir (F5)'
      OnClick = AlterarCor1Click
    end
    object MostrarTodososServios2: TMenuItem
      Caption = 'Mostrar Todos os Servi'#231'os'
      OnClick = MostrarTodososServios2Click
    end
    object MostrarApenasosServiosdoVidro2: TMenuItem
      Caption = 'Mostrar Apenas os Servi'#231'os do Vidro'
      OnClick = MostrarApenasosServiosdoVidro2Click
    end
    object MarcarTodos1: TMenuItem
      Caption = 'Marcar Todos'
      OnClick = MarcarTodos1Click
    end
    object Desmarcartodos1: TMenuItem
      Caption = 'Desmarcar todos'
      OnClick = Desmarcartodos1Click
    end
    object Moverpracima1: TMenuItem
      Caption = 'Mover para Cima'
      OnClick = Moverpracima1Click
    end
    object MoverpraBaixo1: TMenuItem
      Caption = 'Mover para Baixo'
      OnClick = MoverpraBaixo1Click
    end
    object MoverPLinha1: TMenuItem
      Caption = 'Mover para Linha'
      OnClick = MoverPLinha1Click
    end
  end
  object PopUpVidros: TPopupMenu
    Left = 88
    Top = 169
    object MenuItem1: TMenuItem
      Caption = 'Atualizar (F5)'
      OnClick = MenuItem1Click
    end
    object MostrarTodososServios1: TMenuItem
      Caption = 'Mostrar Todos os Servi'#231'os'
      OnClick = MostrarTodososServios1Click
    end
    object MostrarApenasosServiosdoVidro1: TMenuItem
      Caption = 'Mostrar Apenas os Servi'#231'os do Vidro'
      OnClick = MostrarApenasosServiosdoVidro1Click
    end
  end
  object PopUp1: TPopupMenu
    Left = 581
    Top = 73
    object MenuItem2: TMenuItem
      Caption = 'Imprimir Codigo de Barras'
      OnClick = MenuItem2Click
    end
  end
end
