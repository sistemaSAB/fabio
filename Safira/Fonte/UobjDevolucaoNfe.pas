unit UobjDevolucaoNfe;

interface

uses Classes,StdCtrls,Windows,Controls,SysUtils,UobjFERRAGEM,UobjPERFILADO,
     UobjVIDRO,UobjKITBOX,UobjPERSIANA,UobjDIVERSO,IBQuery,UDataModulo;



type
  TObjDevolucaoNfe = class(TComponent)

  private

    ObjQuery:TIBQuery;


  public

    FERRAGEM  :TObjFERRAGEM;
    PERFILADO :TObjPERFILADO;
    VIDRO     :TObjVIDRO;
    KITBOX    :TObjKITBOX;
    PERSIANA  :TObjPERSIANA;
    DIVERSO   :TObjDIVERSO;
    calculatotaltributos:Boolean;

    procedure EdtCLIENTEKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
    procedure EdtFORNECEDORKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
    procedure EdtTRANSPORTADORAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
    procedure EdtFERRAGEMKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
    procedure pesquisaCor(codigoFerragem, chaveEstrangeira, chavePrimaria,tabela: string; LABELNOME: TLabel; sender: TObject);
    procedure EdtPERFILADOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
    procedure EdtVIDROKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
    procedure EdtKITBOXKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
    procedure EdtPERSIANAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
    procedure EdtDIVERSOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);

    function localizaNfeDevolucao(pCodigo:string):TIBQuery;

    constructor create(Owner:TComponent);
    destructor destroy();override;

  end;

  
implementation

uses Upesquisa, UCLIENTE, UFORNECEDOR, UTRANSPORTADORA, UFERRAGEM,
  UPERFILADO, UVIDRO, UKITBOX, UPERSIANA, UDIVERSO, UessencialGlobal;


constructor TobjDevolucaoNfe.create(Owner: TComponent);
var
  nomeTabela:string;
begin

  FERRAGEM  := TObjFERRAGEM.Create;
  PERFILADO := TObjPERFILADO.Create;
  VIDRO     := TObjVIDRO.Create;
  KITBOX    := TObjKITBOX.Create;
  PERSIANA  := TObjPERSIANA.Create;
  DIVERSO   := TObjDIVERSO.Create;

  ObjQuery := TIBQuery.Create(nil);
  
end;

destructor TobjDevolucaoNfe.destroy;
begin

  FERRAGEM.Free;
  PERFILADO.Free;
  VIDRO.Free;
  KITBOX.Free;
  PERSIANA.Free;
  DIVERSO.Free;

  FreeAndNil(ObjQuery);

  inherited;
  
end;

procedure TobjDevolucaoNfe.EdtCLIENTEKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
var
  FpesquisaLocal:Tfpesquisa;
  FCLIENTE:TFCLIENTE;
begin

  If (key <> vk_f9) Then
    exit;

  Try
    Fpesquisalocal:=Tfpesquisa.create(Nil);
    FCLIENTE        :=TFCLIENTE        .create(nil);

    If (FpesquisaLocal.PreparaPesquisa('select * from tabcliente where ativo=''S'' ','Pesquisa de Clientes',FCLIENTE))Then
    Begin

      Try

        If (FpesquisaLocal.showmodal=mrok) Then
        Begin

          TEdit(Sender).text := FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

          If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) Then
            LABELNOME.caption := FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring


        End;

      Finally
        FpesquisaLocal.QueryPesq.close;
      End;

    End;

  Finally
    FreeandNil(FPesquisaLocal);
    Freeandnil(FCLIENTE);
  End;

end;

procedure TObjDevolucaoNfe.EdtFORNECEDORKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
var
  FpesquisaLocal:Tfpesquisa;
  FFORNECEDOR:TFFORNECEDOR;
begin

  If (key <> vk_f9) Then
    exit;

  Try
    Fpesquisalocal := Tfpesquisa.create(Nil);
    FFORNECEDOR       := TFFORNECEDOR.create(nil);

    If (FpesquisaLocal.PreparaPesquisa('select * from tabfornecedor','Pesquisa de Fornecedores',FFORNECEDOR))Then
    Begin

      Try

        If (FpesquisaLocal.showmodal=mrok) Then
        Begin

          TEdit(Sender).text := FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

          If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) Then
            LABELNOME.caption := FpesquisaLocal.QueryPesq.fieldbyname('RAZAOSOCIAL').asstring


        End;

      Finally
        FpesquisaLocal.QueryPesq.close;
      End;

    End;

  Finally
    FreeandNil(FPesquisaLocal);
    Freeandnil(FFORNECEDOR);
  End;

end;

procedure TObjDevolucaoNfe.EdtTRANSPORTADORAKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
var
  FpesquisaLocal:Tfpesquisa;
  FTRANSPORTADORA:TFTRANSPORTADORA;
begin

  If (key <> vk_f9) Then
    exit;

  Try
    Fpesquisalocal  := Tfpesquisa.create(Nil);
    FTRANSPORTADORA := TFTRANSPORTADORA.create(nil);

    If (FpesquisaLocal.PreparaPesquisa('select * from TABTRANSPORTADORA','Pesquisa de Transportadora',FTRANSPORTADORA))Then
    Begin
                          
      Try

        If (FpesquisaLocal.showmodal=mrok) Then
        Begin

          TEdit(Sender).text := FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

          If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) Then
            LABELNOME.caption := FpesquisaLocal.QueryPesq.fieldbyname('NOME').asstring


        End;

      Finally
        FpesquisaLocal.QueryPesq.close;
      End;

    End;

  Finally
    FreeandNil(FPesquisaLocal);
    Freeandnil(FTRANSPORTADORA);
  End;

end;

procedure TObjDevolucaoNfe.EdtFERRAGEMKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FFERRAGEM:TFFERRAGEM;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FFERRAGEM       :=TFFERRAGEM       .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.FERRAGEM.Get_Pesquisa,Self.FERRAGEM.Get_TituloPesquisa,FFERRAGEM)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FERRAGEM.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.FERRAGEM.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FERRAGEM.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FFERRAGEM       );
     End;
end;

procedure TObjDevolucaoNfe.EdtPERFILADOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPERFILADO      :TFPERFILADO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPERFILADO      :=TFPERFILADO      .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PERFILADO.Get_Pesquisa,Self.PERFILADO.Get_TituloPesquisa,FPERFILADO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PERFILADO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.PERFILADO.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PERFILADO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FPERFILADO      );
     End;
end;

procedure TObjDevolucaoNfe.EdtVIDROKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FVIDRO          :TFVIDRO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FVIDRO          :=TFVIDRO          .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.VIDRO.Get_Pesquisa,Self.VIDRO.Get_TituloPesquisa,FVIDRO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.VIDRO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.VIDRO.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.VIDRO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FVIDRO          );
     End;
end;

procedure TObjDevolucaoNfe.EdtKITBOXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FKITBOX         :TFKITBOX;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FKITBOX         :=TFKITBOX         .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.KITBOX.Get_Pesquisa,Self.KITBOX.Get_TituloPesquisa,FKITBOX)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.KITBOX.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.KITBOX.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.KITBOX.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FKITBOX);
     End;
end;




procedure TObjDevolucaoNfe.pesquisaCor(codigoFerragem,chaveEstrangeira,chavePrimaria,tabela:string;LABELNOME:TLabel;sender:TObject);
var
  fpesquisaLocal:TFpesquisa;
begin


  with (Self.Objquery) do
  begin

    Active:=False;
    sql.Clear;
    sql.Add('select cor.codigo as codigo_cor, cor.descricao as nome_cor');
    sql.Add('from '+tabela);
    sql.Add('join '+tabela+'cor '+'on '+tabela+'cor'+'.'+chaveEstrangeira+' = '+tabela+'.'+chavePrimaria);
    sql.Add('join tabcor cor on cor.codigo = '+tabela+'cor .cor');
    sql.Add('where '+tabela+'.codigo = '+codigoFerragem);

  end;

  Try

    Fpesquisalocal:=Tfpesquisa.create(Nil);

    If (FpesquisaLocal.PreparaPesquisa(Objquery.SQL.Text,'CORES ('+uppercase(chaveEstrangeira)+')',nil)) Then
    Begin

      Try

        If (FpesquisaLocal.showmodal=mrok) Then
        Begin

          TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo_cor').asstring;

          if ( LabelNome <> NIl) Then
            LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome_cor').asstring

        End;

      Finally

        FpesquisaLocal.QueryPesq.close;
        
      End;

    End;


    
  finally

    FreeandNil(FPesquisaLocal);

  end;


end;

procedure TObjDevolucaoNfe.EdtPERSIANAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPERSIANA       :TFPERSIANA;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPERSIANA       :=TFPERSIANA       .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PERSIANA.Get_Pesquisa,Self.PERSIANA.Get_TituloPesquisa,FPERSIANA)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PERSIANA.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.PERSIANA.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PERSIANA.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FPERSIANA);
     End;
end;

procedure TObjDevolucaoNfe.EdtDIVERSOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FDIVERSO:TFDIVERSO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FDIVERSO        :=TFDIVERSO        .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.DIVERSO.Get_Pesquisa,Self.DIVERSO.Get_TituloPesquisa,FDIVERSO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.DIVERSO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.DIVERSO.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.DIVERSO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FDIVERSO        );
     End;
end;


function TObjDevolucaoNfe.localizaNfeDevolucao(pCodigo: string): TIBQuery;
begin
  Result := TIBQuery.Create(nil);
  Result.Database := FDataModulo.IBDatabase;

  Result.SQL.Text :=
  'select * from tabnfedevolucao where codigo = '+pCodigo;

  Result.Open;
  Result.First;
end;

end.
