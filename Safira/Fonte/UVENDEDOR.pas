unit UVENDEDOR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjVENDEDOR,
  jpeg, UessencialGlobal, Tabs;

type
  TFVENDEDOR = class(TForm)
    btNovo: TSpeedButton;
    btFechar: TSpeedButton;
    btMinimizar: TSpeedButton;
    btSalvar: TSpeedButton;
    btAlterar: TSpeedButton;
    btCancelar: TSpeedButton;
    btSair: TSpeedButton;
    btRelatorio: TSpeedButton;
    btExcluir: TSpeedButton;
    btPesquisar: TSpeedButton;
    lbAjuda: TLabel;
    btAjuda: TSpeedButton;
    PainelExtra: TPanel;
    ImageGuiaPrincipal: TImage;
    ImageGuiaExtra: TImage;
    PainelPrincipal: TPanel;
    ImagePainelPrincipal: TImage;
    ImagePainelExtra: TImage;
    Guia: TTabSet;
    Notebook: TNotebook;
    Bevel1: TBevel;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    LbNome: TLabel;
    EdtNome: TEdit;
    LbSexo: TLabel;
    LbEmail: TLabel;
    EdtEmail: TEdit;
    LbFone: TLabel;
    EdtFone: TEdit;
    LbCelular: TLabel;
    EdtCelular: TEdit;
    LbDataCadastro: TLabel;
    EdtDataCadastro: TMaskEdit;
    EdtDataNascimento: TMaskEdit;
    LbDataNascimento: TLabel;
    LbAtivo: TLabel;
    LbObservacao: TLabel;
    ComboSexo: TComboBox;
    ComboAtivo: TComboBox;
    MemoObservacao: TMemo;
    Label1: TLabel;
    edtcodigofuncionario: TEdit;
    Label2: TLabel;
    edtfaixadesconto: TEdit;
//DECLARA COMPONENTES

    procedure FormCreate(Sender: TObject);
    Procedure PosicionaPaineis;
    Procedure PegaFiguras;
    Procedure ColocaAtalhoBotoes;
    Procedure PosicionaForm;
    procedure ImageGuiaPrincipalClick(Sender: TObject);
    procedure ImageGuiaExtraClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GuiaClick(Sender: TObject);
    procedure edtcodigofuncionarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigofuncionarioExit(Sender: TObject);
    procedure edtfaixadescontoExit(Sender: TObject);
    procedure edtfaixadescontoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    ObjVENDEDOR:TObjVENDEDOR;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FVENDEDOR: TFVENDEDOR;


implementation

uses Upesquisa, UessencialLocal;

{$R *.dfm}


procedure TFVENDEDOR.FormCreate(Sender: TObject);
var
  Points: array [0..15] of TPoint;
  Regiao1:HRgn;
begin

Points[0].X :=305;      Points[0].Y := 22;
Points[1].X :=286;      Points[1].Y := 3;
Points[2].X :=41;       Points[2].Y := 3;
Points[3].X :=41;       Points[3].Y := 22;
Points[4].X :=28;       Points[4].Y := 35;
Points[5].X :=28;       Points[5].Y := 164;
Points[6].X :=41;       Points[6].Y := 177;
Points[7].X :=41;       Points[7].Y := 360;
Points[8].X :=62;       Points[8].Y := 381;
Points[9].X :=632;      Points[9].Y := 381;
Points[10].X :=647;     Points[10].Y := 396;
Points[11].X :=764;     Points[11].Y := 396;
Points[12].X :=764;     Points[12].Y := 95;
Points[13].X :=780;     Points[13].Y := 79;
Points[14].X :=780;     Points[14].Y := 22;
Points[15].X :=305;     Points[15].Y := 22;

Regiao1:=CreatePolygonRgn(Points, 16, WINDING);
SetWindowRgn(Self.Handle, regiao1, False);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.TabIndex:=0;
     Notebook.PageIndex:=0;
     ColocaUpperCaseEdit(Self);

     Try
        Self.ObjVENDEDOR:=TObjVENDEDOR.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     PegaCorForm(Self);
     Self.PosicionaPaineis;
     Self.PosicionaForm;
     Self.PegaFiguras;
     Self.ColocaAtalhoBotoes;
end;

procedure TFVENDEDOR.PosicionaPaineis;
begin
  With Self.PainelPrincipal do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelPrincipal do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  With Self.PainelExtra  do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelExtra  do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  Self.PainelPrincipal.Enabled:=true;
  Self.PainelPrincipal.Visible:=true;
  Self.PainelExtra.Enabled:=false;
  Self.PainelExtra.Visible:=false;
end;

procedure TFVENDEDOR.ImageGuiaPrincipalClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=true;
PainelPrincipal.Visible:=true;
PainelExtra.Enabled:=false;
PainelExtra.Visible:=false;
end;

procedure TFVENDEDOR.ImageGuiaExtraClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=false;
PainelPrincipal.Visible:=false;
PainelExtra.Enabled:=true;
PainelExtra.Visible:=true;

end;

procedure TFVENDEDOR.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjVENDEDOR=Nil)
     Then exit;

     If (Self.ObjVENDEDOR.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjVENDEDOR.free;
    Action := caFree;
    Self := nil;
end;

procedure TFVENDEDOR.PegaFiguras;
begin
    PegaFigura(ImageGuiaPrincipal,'guia_principal.jpg');
    PegaFigura(ImageGuiaExtra,'guia_extra.jpg');
    PegaFigura(ImagePainelPrincipal,'fundo_menu.jpg');
    PegaFigura(ImagePainelExtra,'fundo_menu.jpg');
    PegaFiguraBotao(btNovo,'botao_novo.bmp');
    PegaFiguraBotao(btSalvar,'botao_salvar.bmp');
    PegaFiguraBotao(btAlterar,'botao_alterar.bmp');
    PegaFiguraBotao(btCancelar,'botao_Cancelar.bmp');
    PegaFiguraBotao(btPesquisar,'botao_Pesquisar.bmp');
    PegaFiguraBotao(btExcluir,'botao_excluir.bmp');
    PegaFiguraBotao(btRelatorio,'botao_relatorios.bmp');
    PegaFiguraBotao(btSair,'botao_sair.bmp');
    PegaFiguraBotao(btFechar,'botao_close.bmp');
    PegaFiguraBotao(btMinimizar,'botao_minimizar.bmp');
    PegaFiguraBotao(btAjuda,'lampada.bmp');

end;

procedure TFVENDEDOR.ColocaAtalhoBotoes;
begin
   Coloca_Atalho_Botoes(BtNovo, 'N');
   Coloca_Atalho_Botoes(BtSalvar, 'S');
   Coloca_Atalho_Botoes(BtAlterar, 'A');
   Coloca_Atalho_Botoes(BtCancelar, 'C');
   Coloca_Atalho_Botoes(BtExcluir, 'E');
   Coloca_Atalho_Botoes(BtPesquisar, 'P');
   Coloca_Atalho_Botoes(BtRelatorio, 'R');
   Coloca_Atalho_Botoes(BtSair, 'I');
end;

procedure TFVENDEDOR.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjVENDEDOR.Get_novocodigo;
     edtcodigo.enabled:=False;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjVENDEDOR.status:=dsInsert;
     Guia.TabIndex:=0;
     Notebook.PageIndex:=0;

     EdtCodigoFuncionario.setfocus;

end;

procedure TFVENDEDOR.btSalvarClick(Sender: TObject);
begin

     If ObjVENDEDOR.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjVENDEDOR.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjVENDEDOR.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFVENDEDOR.btAlterarClick(Sender: TObject);
begin
    If (ObjVENDEDOR.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjVENDEDOR.Status:=dsEdit;
                guia.TabIndex:=0;
                edtCodigoFuncionario.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;

end;

procedure TFVENDEDOR.btCancelarClick(Sender: TObject);
begin
     ObjVENDEDOR.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFVENDEDOR.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjVENDEDOR.Get_pesquisa,ObjVENDEDOR.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjVENDEDOR.status<>dsinactive
                                  then exit;

                                  If (ObjVENDEDOR.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjVENDEDOR.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFVENDEDOR.btExcluirClick(Sender: TObject);
begin
     If (ObjVENDEDOR.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjVENDEDOR.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjVENDEDOR.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFVENDEDOR.btRelatorioClick(Sender: TObject);
begin
    ObjVENDEDOR.Imprime(edtCodigo.text);
end;

procedure TFVENDEDOR.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFVENDEDOR.PosicionaForm;
begin
    Self.Caption:='      '+Self.Caption;
    Self.Left:=-18;
    Self.Top:=0;
end;
procedure TFVENDEDOR.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFVENDEDOR.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFVENDEDOR.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
end;

procedure TFVENDEDOR.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFVENDEDOR.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjVENDEDOR do
    Begin
        Submit_Codigo(edtCodigo.text);
        Submit_Nome(edtNome.text);
        Submit_Sexo(ComboSexo.text);
        Submit_Email(edtEmail.text);
        Submit_Fone(edtFone.text);
        Submit_Celular(edtCelular.text);
        Submit_DataCadastro(edtDataCadastro.text);
        Submit_DataNascimento(edtDataNascimento.text);
        Submit_Ativo(ComboAtivo.text);
        Submit_Observacao(MemoObservacao.text);
        CodigoFuncionario.Submit_CODIGO(edtcodigofuncionario.text);
        FaixaDesconto.Submit_CODIGO(edtfaixadesconto.Text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFVENDEDOR.LimpaLabels;
begin
//LIMPA LABELS
end;

function TFVENDEDOR.ObjetoParaControles: Boolean;
begin
  Try
     With ObjVENDEDOR do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtNome.text:=Get_Nome;
        ComboSexo.text:=Get_Sexo;
        EdtEmail.text:=Get_Email;
        EdtFone.text:=Get_Fone;
        EdtCelular.text:=Get_Celular;
        EdtDataCadastro.text:=Get_DataCadastro;
        EdtDataNascimento.text:=Get_DataNascimento;
        ComboAtivo.text:=Get_Ativo;
        MemoObservacao.text:=Get_Observacao;
        edtcodigofuncionario.text:=CodigoFuncionario.Get_CODIGO;
        edtfaixadesconto.Text:=FaixaDesconto.Get_CODIGO;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFVENDEDOR.TabelaParaControles: Boolean;
begin
     If (ObjVENDEDOR.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFVENDEDOR.GuiaClick(Sender: TObject);
begin
Notebook.PageIndex:=Guia.TabIndex;
end;

procedure TFVENDEDOR.edtcodigofuncionarioKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjVENDEDOR.EdtCodigoFuncionarioKeyDown(sender,key,shift,nil);
end;

procedure TFVENDEDOR.edtcodigofuncionarioExit(Sender: TObject);
begin
     ObjVENDEDOR.EdtCodigoFuncionarioExit(sender,nil);
end;

procedure TFVENDEDOR.edtfaixadescontoExit(Sender: TObject);
begin
     ObjVENDEDOR.EdtFaixaDescontoExit(sender,nil);
end;

procedure TFVENDEDOR.edtfaixadescontoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjVENDEDOR.EdtFaixaDescontoKeyDown(sender,key,shift,nil); 
end;

end.
