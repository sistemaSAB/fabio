unit UescolheVidro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, ExtCtrls;

type
  TFescolheVidro = class(TForm)
    STRGGRID: TStringGrid;
    pnl1: TPanel;
    lbnomeformulario: TLabel;
    pnl2: TPanel;
    Image1: TImage;
    lbValorTotal: TLabel;
    lb1: TLabel;
    lb2: TLabel;
    pnl6: TPanel;
    lb3: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btokClick(Sender: TObject);
    procedure STRGGRIDKeyPress(Sender: TObject; var Key: Char);
    procedure STRGGRIDDblClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure STRGGRIDKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure STRGGRIDClick(Sender: TObject);
    procedure lb1MouseLeave(Sender: TObject);
    procedure lb1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FescolheVidro: TFescolheVidro;

implementation

uses UescolheImagemBotao;

{$R *.dfm}

procedure TFescolheVidro.FormShow(Sender: TObject);
begin
     Self.STRGGRID.SetFocus;
     Self.Tag:=0;
     lbValorTotal.Caption:='Valor Total R$  '+STRGGRID.Cells[13,STRGGRID.row];
     FescolheImagemBotao.PegaFiguraImagem(Image1,'RODAPE');
end;

procedure TFescolheVidro.btcancelarClick(Sender: TObject);
begin
     Self.tag:=0;
     Self.close;
end;

procedure TFescolheVidro.btokClick(Sender: TObject);
begin
     Self.tag:=1;
     Self.Close;
end;

procedure TFescolheVidro.STRGGRIDKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Self.STRGGRIDDblClick(sender);
     
end;

procedure TFescolheVidro.STRGGRIDDblClick(Sender: TObject);
begin
     btokclick(Sender);
end;

procedure TFescolheVidro.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#27
     Then btcancelarclick(Sender);


end;

procedure TFescolheVidro.STRGGRIDKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     lbValorTotal.Caption:='Valor Total R$  '+STRGGRID.Cells[13,STRGGRID.row];
end;

procedure TFescolheVidro.STRGGRIDClick(Sender: TObject);
begin
     lbValorTotal.Caption:='Valor Total R$  '+STRGGRID.Cells[13,STRGGRID.row];
end;

procedure TFescolheVidro.lb1MouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFescolheVidro.lb1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
        TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

end.
