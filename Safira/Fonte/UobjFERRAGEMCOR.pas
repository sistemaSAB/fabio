unit UobjFERRAGEMCOR;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJFERRAGEM
,UOBJCOR;

Type
   TObjFERRAGEMCOR=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Ferragem:TOBJFERRAGEM;
                Cor:TOBJCOR;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaReferencia(Parametro:string) :boolean;

                Function    Localiza_Ferragem_Cor(Pferragem,PCor:string):boolean;
                Function    LocalizaReferencia_Ferragem_Cor(PReferencia,PCor:string):boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Function Get_PorcentagemAcrescimo: string;
                Procedure Submit_PorcentagemAcrescimo(parametro:string);

                Procedure Submit_ClassificacaoFiscal(parametro:String);
                Function  Get_ClassificacaoFiscal:String;

                Procedure Submit_ValorExtra(parametro: string);
                Function Get_ValorExtra: string;

                Procedure Submit_ValorFinal(parametro: string);
                Function Get_ValorFinal: string;

                procedure Submit_Estoque(parametro:string);
                function Get_Estoque:string;

                procedure Submit_EstoqueMinimo(parametro:string);
                function Get_EstoqueMinimo:string;



                Function RetornaEstoque: STRing;

                
                Procedure Submit_AcrescimoExtra(parametro: string);
                Function Get_AcrescimoExtra: string;
                Function Get_PorcentagemAcrescimoFinal: string;
                procedure EdtFerragemExit(Sender: TObject;LABELNOME:TLABEL);

                procedure EdtCor_PorcentagemCor_Exit(Sender: TObject;Var EdtCodigo: TEdit; LABELNOME: TLABEL; Var EdtPorcentagemCor:TEdit);Overload;

                procedure EdtFerragemKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtCorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtCor_PorcentagemCor_Exit(Sender: TObject;LABELNOME:TLABEL; EdtPorcentagemCor:TEdit);Overload;

                procedure EdtCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtCorKeyDown(Sender: TObject;var EdtCodigo : TEdit;  var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);Overload;

                Procedure ResgataCorFerragem(Pferragem:string);
                Function CadastraFerragemEmTodasAsCores(PFerragem:string):Boolean;

                procedure EdtCorDeFerragemKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtFerragemDisponivelNaCorKeyDown(Sender: TObject;Var PEdtCodigo:TEdit; PCor :string; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Function  Get_FerragemNaCor(PCor : string) :TStringList;
                procedure Relatorioestoque;

                procedure EdtferragemCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
              ///  procedure DiminuiEstoque(quantidade,ferragemcor:string);
              ///  function AumentaEstoque(quantidade,ferragemcor:string):Boolean;



         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               ObjqueryEstoque:Tibquery;

               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               PorcentagemAcrescimo:string;
               AcrescimoExtra:string;
               PorcentagemAcrescimoFinal:string;
               ValorExtra :string;
               ValorFinal :string;
               ClassificacaoFiscal:String;
               Estoque:string;
               EstoqueMinimo:string;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;


   End;


implementation
uses Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UCOR, UEscolheCor, UReltxtRDPRINT,rdprint, DateUtils, UFERRAGEM;

Function  TObjFERRAGEMCOR.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.classificacaofiscal:=fieldbyname('classificacaofiscal').asstring;

        If(FieldByName('Ferragem').asstring<>'')
        Then Begin
                 If (Self.Ferragem.LocalizaCodigo(FieldByName('Ferragem').asstring)=False)
                 Then Begin
                          Messagedlg('Ferragem N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Ferragem.TabelaparaObjeto;
        End;
        
        If(FieldByName('Cor').asstring<>'')
        Then Begin
                 If (Self.Cor.LocalizaCodigo(FieldByName('Cor').asstring)=False)
                 Then Begin
                          Messagedlg('Cor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Cor.TabelaparaObjeto;
        End;
        Self.PorcentagemAcrescimo:=fieldbyname('PorcentagemAcrescimo').asstring;
        Self.AcrescimoExtra:=fieldbyname('AcrescimoExtra').asstring;
        Self.PorcentagemAcrescimoFinal:=fieldbyname('PorcentagemAcrescimoFinal').asstring;
        Self.ValorExtra:=fieldbyname('ValorExtra').AsString;
        Self.ValorFinal:=fieldbyname('ValorFinal').AsString;
        self.Estoque:=fieldbyname('estoque').AsString;
        self.EstoqueMinimo := Objquery.fieldbyname('EstoqueMinimo').AsString;
        result:=True;
     End;
end;


Procedure TObjFERRAGEMCOR.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('classificacaofiscal').asstring:=Self.classificacaofiscal;
        ParamByName('Ferragem').asstring:=Self.Ferragem.GET_CODIGO;
        ParamByName('Cor').asstring:=Self.Cor.GET_CODIGO;
        ParamByName('PorcentagemAcrescimo').asstring:=virgulaparaponto(Self.PorcentagemAcrescimo);
        ParamByName('AcrescimoExtra').asstring:=virgulaparaponto(Self.AcrescimoExtra);
        ParamByName('ValorExtra').AsString:=virgulaparaponto(Self.ValorExtra);
        ParamByName('ValorFinal').AsString:=virgulaparaponto(Self.ValorFinal);
        //ParamByName('estoque').AsString:=virgulaparaponto(Self.Estoque);
        ParamByName('estoqueminimo').AsString:=virgulaparaponto(Self.EstoqueMinimo);
  End;
End;

//***********************************************************************

function TObjFERRAGEMCOR.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjFERRAGEMCOR.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Ferragem.ZerarTabela;
        ClassificacaoFiscal:='';
        Cor.ZerarTabela;
        PorcentagemAcrescimo:='';
        AcrescimoExtra:='';
        PorcentagemAcrescimoFinal:='';
        ValorExtra:='';
        ValorFinal:='';
        Estoque:='';
        EstoqueMinimo := '';
     End;
end;

Function TObjFERRAGEMCOR.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
       If (Codigo='')
      Then Mensagem:=mensagem+'/C�digo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjFERRAGEMCOR.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Ferragem.LocalizaCodigo(Self.Ferragem.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Ferragem n�o Encontrado!';

      If (Self.Cor.LocalizaCodigo(Self.Cor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Cor n�o Encontrado!'
      Else Begin
                Self.Cor.TabelaparaObjeto;
                Self.PorcentagemAcrescimo:=Self.cor.Get_PorcentagemAcrescimo;
      End;

      If (mensagem<>'')
      Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
     End
     Else Begin
               if (Self.Status=dsInsert)
               then Begin
                       if (Self.Localiza_Ferragem_Cor(Self.Ferragem.Get_Codigo,Self.Cor.Get_Codigo)=True)
                       then Begin
                               MensagemErro('Essa ferragem j� foi cadastrada nessa cor');
                               result:=False;
                               exit;
                       End;
               End;
     End;
     result:=true;
End;

function TObjFERRAGEMCOR.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.Ferragem.Get_Codigo<>'')
        Then Strtoint(Self.Ferragem.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Ferragem';
     End;
     try
        If (Self.Cor.Get_Codigo<>'')
        Then Strtoint(Self.Cor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Cor';
     End;


     try
        Strtofloat(Self.AcrescimoExtra);
     Except
           Mensagem:=mensagem+'/Acr�scimo Extra';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjFERRAGEMCOR.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjFERRAGEMCOR.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjFERRAGEMCOR.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro FERRAGEMCOR vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Ferragem,Cor,PorcentagemAcrescimo,AcrescimoExtra,ValorExtra');
           SQL.ADD(' ,PorcentagemAcrescimoFinal, ValorFinal,ClassificacaoFiscal,estoque,EstoqueMinimo');   
           SQL.ADD(' from  TabFerragemCor');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjFERRAGEMCOR.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjFERRAGEMCOR.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjFERRAGEMCOR.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryEstoque:=TIBQuery.create(nil);
        Self.ObjqueryEstoque.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjqueryPesquisa;


        Self.ParametroPesquisa:=TStringList.create;


        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Ferragem:=TOBJFERRAGEM.create;
        Self.Cor:=TOBJCOR.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabFerragemCor(Codigo,Ferragem,Cor,PorcentagemAcrescimo');
                InsertSQL.add(' ,AcrescimoExtra, ValorExtra, ValorFinal,ClassificacaoFiscal');//,estoque');
                InsertSql.Add(',EstoqueMinimo');
                InsertSql.Add(')');
                InsertSQL.add('values (:Codigo,:Ferragem,:Cor,:PorcentagemAcrescimo');
                InsertSQL.add(' ,:AcrescimoExtra,:ValorExtra, :ValorFinal,:ClassificacaoFiscal');//,:estoque');
                InsertSql.Add(',:EstoqueMinimo');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabFerragemCor set Codigo=:Codigo,Ferragem=:Ferragem');
                ModifySQL.add(',Cor=:Cor,PorcentagemAcrescimo=:PorcentagemAcrescimo');
                ModifySQL.add(',AcrescimoExtra=:AcrescimoExtra,ValorExtra=:ValorExtra, ValorFinal=:ValorFinal');
                ModifySQL.add(',ClassificacaoFiscal=:ClassificacaoFiscal');//,estoque=:estoque');
                ModifySQl.Add(',EstoqueMinimo=:EstoqueMinimo');
                ModifySQl.Add(' where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabFerragemCor where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjFERRAGEMCOR.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjFERRAGEMCOR.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewFERRAGEMCOR');
     Result:=Self.ParametroPesquisa;
end;

function TObjFERRAGEMCOR.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de FERRAGEMCOR ';
end;


function TObjFERRAGEMCOR.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENFERRAGEMCOR,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENFERRAGEMCOR,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjFERRAGEMCOR.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ObjqueryEstoque);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Freeandnil(Self.ObjqueryPesquisa);
    Freeandnil(Self.Objdatasource);
    Self.Ferragem.FREE;
    Self.Cor.FREE;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjFERRAGEMCOR.RetornaCampoCodigo: string;
begin
      result:='Codigo';
end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjFERRAGEMCOR.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjFerragemCor.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjFerragemCor.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;

function TObjFerragemCor.Get_PorcentagemAcrescimo: string;
begin
        Result:=Self.PorcentagemAcrescimo;
end;

function TObjFerragemCor.RetornaEstoque:String;
begin
        With Self.ObjQueryEstoque do
        begin
             close;
             sql.clear;
             sql.add('Select coalesce(sum(quantidade),0) as estoque from tabestoque where ferragemcor='+Self.Codigo);
             //SQL.Add('Select estoque from tabferragemcor where codigo='+self.Codigo);
             open;
             result:=fieldbyname('estoque').asstring;
        End;
end;


procedure TObjFerragemCor.Submit_AcrescimoExtra(parametro: string);
begin
        Self.AcrescimoExtra:=tira_ponto(Parametro);
end;
function TObjFerragemCor.Get_AcrescimoExtra: string;
begin
        Result:=Self.AcrescimoExtra;
end;

function TObjFerragemCor.Get_PorcentagemAcrescimoFinal: string;
begin
        Result:=Self.PorcentagemAcrescimoFinal;
end;
procedure TObjFERRAGEMCOR.EdtFerragemExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Ferragem.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Ferragem.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Ferragem.Get_Descricao;
End;
procedure TObjFERRAGEMCOR.EdtFerragemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FFerragemLocal: TFFERRAGEM;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FFerragemLocal := TFFERRAGEM.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Ferragem.Get_Pesquisa2,Self.Ferragem.Get_TituloPesquisa,FFerragemLocal)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Ferragem.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Ferragem.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Ferragem.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeandNil(FFerragemLocal);
     End;
end;
procedure TObjFERRAGEMCOR.EdtCorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Cor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Cor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Cor.Get_Descricao;
End;
procedure TObjFERRAGEMCOR.EdtCorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FCOR:TFCOR;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCOR:=TFCOR.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Cor.Get_Pesquisa,Self.Cor.Get_TituloPesquisa,FCor)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCOR);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjFERRAGEMCOR.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJFERRAGEMCOR';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjFERRAGEMCOR.ResgataCorFerragem(Pferragem: string);
begin
     With Self.ObjqueryPesquisa do
     Begin
          close;
          SQL.clear;
          SQL.ADD('Select TFC.Cor,TabCor.Descricao as NomeCor,TFC.PorcentagemAcrescimo,TFC.AcrescimoExtra');
          SQL.ADD(' ,TFC.PorcentagemAcrescimoFinal,TFC.codigo,TFC.ClassificacaoFiscal');
          SQL.ADD(' from  TabFerragemCor TFC');
          SQL.ADD(' join tabCor on TFC.Cor=Tabcor.codigo');

          SQL.ADD(' where TFC.Ferragem='+pferragem);

          if (Pferragem='')
          Then exit;
          open;
     End;
end;

procedure TObjFERRAGEMCOR.EdtCor_PorcentagemCor_Exit(Sender: TObject;
  LABELNOME: TLABEL; EdtPorcentagemCor: TEdit);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Cor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Cor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Cor.Get_Descricao;
     EdtPorcentagemCor.Text:=Self.Cor.Get_PorcentagemAcrescimo;

end;

function TObjFERRAGEMCOR.LocalizaReferencia(Parametro: string): boolean;
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro FERRAGEMCOR vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Ferragem,Cor,PorcentagemAcrescimo,AcrescimoExtra,ValorExtra');
           SQL.ADD(' ,PorcentagemAcrescimoFinal, ValorFinal,TabFerragemCor.ClassificacaoFiscal,estoque,EstoqueMinimo');
           SQL.ADD(' from  TabFerragemCor');
           SQL.Add('join TabFerragem on TabFerragem.Codigo = TabFerragemCor.Ferragem');
           SQL.ADD(' WHERE TabFerragem.Referencia ='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

procedure TObjFERRAGEMCOR.EdtCor_PorcentagemCor_Exit(Sender: TObject; var EdtCodigo: TEdit; LABELNOME: TLABEL; var EdtPorcentagemCor: TEdit);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Cor.LocalizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               EdtCodigo.text:='';
               exit;
     End;
     Self.Cor.tabelaparaobjeto;
     
     LABELNOME.CAPTION:=Self.Cor.Get_Descricao;
     EdtPorcentagemCor.Text:=Self.Cor.Get_PorcentagemAcrescimo;
     EdtCodigo.text:=Self.Cor.Get_Codigo;
end;

procedure TObjFERRAGEMCOR.EdtCorKeyDown(Sender: TObject;
  var EdtCodigo: TEdit; var Key: Word; Shift: TShiftState;
  LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cor.Get_Pesquisa,Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 EdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjFERRAGEMCOR.CadastraFerragemEmTodasAsCores(PFerragem: string): Boolean;
Var   Cont : Integer;
      PPOrcentagemAcrescimo:string;
begin
     Result:=false;
     if (Self.Ferragem.LocalizaCodigo(PFerragem)=false)then
     Begin
          MensagemErro('Ferragem n�o encontrado.');
          Result:=false;
          exit;
     end;

     With  Self.ObjqueryPesquisa  do
     Begin
          Close;
          SQl.Clear;
          Sql.Add('Select Codigo,Referencia, Descricao from TabCor');
          Open;
          First;

          FEscolheCor.CheckListBoxCor.Clear;
          FEscolheCor.CheckListBoxCodigoCor.Clear;
          While not (Eof) do
          Begin
               FEscolheCor.CheckListBoxCor.Items.Add(fieldbyname('Referencia').AsString+' - '+fieldbyname('Descricao').AsString);
               FEscolheCor.CheckListBoxCodigoCor.Items.Add(fieldbyname('Codigo').AsString);
          Next;
          end;
          FEscolheCor.ShowModal;

          if FEscolheCor.Tag = 0 then
          Begin
               Result:=false;
               exit;
          end;

          // Cadastrando os cores
          for Cont:=0 to FEscolheCor.CheckListBoxCor.Items.Count-1 do
          Begin
               if FEscolheCor.CheckListBoxCodigoCor.Checked[Cont]=true then
               Begin
                    Self.Cor.LocalizaCodigo(FEscolheCor.CheckListBoxCodigoCor.Items.Strings[Cont]);
                    Self.Cor.TabelaparaObjeto;
                    PPOrcentagemAcrescimo:=Self.Cor.Get_PorcentagemAcrescimo;

                    Self.ZerarTabela;
                    Self.Status:=dsInsert;
                    Self.Submit_Codigo(Get_NovoCodigo);
                    Self.Ferragem.Submit_Codigo(PFerragem);
                    Self.Cor.Submit_Codigo(FEscolheCor.CheckListBoxCodigoCor.Items.Strings[Cont]);
                    
                    Self.Submit_AcrescimoExtra('0');
                    Self.Submit_PorcentagemAcrescimo(PPOrcentagemAcrescimo);
                    if (Self.Salvar(false)=false)then
                    Begin
                         MensagemErro('Erro ao tentar Salvar a FerragmeCor');
                         Result:=false;
                         exit;
                    end;
               end;
          end;

          Result:=true;
     end;
end;

procedure TObjFERRAGEMCOR.Submit_PorcentagemAcrescimo(parametro: string);
begin
     Self.PorcentagemAcrescimo:=tira_ponto(parametro);
end;

function TObjFERRAGEMCOR.Localiza_Ferragem_Cor(Pferragem, PCor: string): boolean;
begin
       if (Pferragem='')
       or(Pcor='')
       Then Begin
                 Messagedlg('Escolha uma ferragem e uma cor',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Ferragem,Cor,PorcentagemAcrescimo,AcrescimoExtra, ValorExtra');
           SQL.ADD(' ,PorcentagemAcrescimoFinal, ValorFinal,TabFerragemCor.classificacaofiscal,estoque,EstoqueMinimo');
           SQL.ADD(' from  TabFerragemCor');
           SQL.ADD(' WHERE ferragem='+pferragem);
           SQL.ADD(' and Cor='+Pcor);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjFerragemCOR.Get_ValorExtra: string;
begin
    Result:=Self.ValorExtra;
end;

function TObjFerragemCOR.Get_ValorFinal: string;
begin
    Result:=Self.ValorFinal;
end;

procedure TObjFerragemCOR.Submit_ValorExtra(parametro: string);
begin
    Self.ValorExtra:=tira_ponto(parametro);
end;

procedure TObjFerragemCOR.Submit_ValorFinal(parametro: string);
begin                                     
    Self.ValorFinal:=tira_ponto(parametro);
end;
procedure TObjFERRAGEMCOR.EdtCorDeFerragemKeyDown(Sender: TObject; var PEdtCodigo: TEdit; var Key: Word; Shift: TShiftState;
  LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cor.Get_PesquisaCorFerragem,Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjFERRAGEMCOR.EdtFerragemDisponivelNaCorKeyDown(
  Sender: TObject; var PEdtCodigo: TEdit; PCor: string; var Key: Word;
  Shift: TShiftState; LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_FerragemNaCor(PCor),Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjFERRAGEMCOR.Get_FerragemNaCor(PCor: string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select Distinct(TabFerragem.Referencia),TabFerragem.DESCRICAO, TabFerragem.Unidade,');
     Self.ParametroPesquisa.add('TabFerragem.Peso, TabFerragemCor.Codigo');
     Self.ParametroPesquisa.add('from TabFerragemCor');
     Self.ParametroPesquisa.add('Join TabFerragem on TabFerragem.Codigo = TabFerragemCor.Ferragem');
     if (PCor <> '')then
     Self.ParametroPesquisa.add('Where TabFerragemCor.Cor = '+PCor);

     Result:=Self.ParametroPesquisa;

end;


function TObjFERRAGEMCOR.LocalizaReferencia_Ferragem_Cor(PReferencia,PCor: string): boolean;
begin
       if (Pcor='')
       Then Begin
                 Messagedlg('Escolha uma cor',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           Sql.Add(' Select TabFerragemCor.Codigo,TabFerragemCor.Ferragem,TabFerragemCor.Cor,');
           Sql.Add(' TabFerragemCor.PorcentagemAcrescimo,TabFerragemCor.AcrescimoExtra,');
           Sql.Add(' TabFerragemCor.ValorExtra,TabFerragemCor.PorcentagemAcrescimoFinal,');
           sql.Add(' TabFerragemCor.ValorFinal,TabFerragemCor.ClassificacaoFiscal,TabFerragemCor.estoque');
           sql.Add(',tabferragemCor.EstoqueMinimo');
           Sql.Add(' from  TabFerragemCor');
           Sql.Add(' Join TabFerragem on TabFerragem.Codigo = TabFerragemCor.Ferragem');
           Sql.Add(' WHERE Tabferragem.Referencia = '+PReferencia);
           Sql.Add(' and TabFerragemCor.Cor = '+PCor);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

procedure TObjferragemCOR.Relatorioestoque;
var
Pgrupo:string;
PsomaEstoque:integer;
PsomaValorTotal:Currency;
pdata:string;
ferragem:string;
begin
     With FfiltroImp do
     begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.caption:='Grupo Ferragem';
          edtgrupo01.OnKeyDown:=Self.ferragem.EdtGrupoferragemKeyDown;
          edtgrupo01.Color:=$005CADFE;

          Grupo02.Enabled:=True;
          lbgrupo02.Caption:='Ferragem';
          edtgrupo02.OnKeyDown:=Self.Ferragem.EdtFerragemKeyDown;
          edtgrupo02.Color:=$005CADFE;

          Grupo03.Enabled:=True;
          edtgrupo03.EditMask:=MascaraData;
          LbGrupo03.caption:='Data Limite';

          Showmodal;

          if (tag=0)
          then exit;

          pGrupo:='';
          if (edtgrupo01.Text<>'')
          Then Begin
                    if (self.ferragem.Grupoferragem.LocalizaCodigo(edtgrupo01.text)=False)
                    Then Begin
                              MensagemErro('Grupo n�o encontrado');
                              exit;
                    End;
                    self.ferragem.Grupoferragem.TabelaparaObjeto;
                    pgrupo:=self.ferragem.Grupoferragem.get_codigo;
          End;
          ferragem:='';
          if(edtgrupo02.Text<>'')
          then ferragem:=edtgrupo02.Text;


          pdata:='';

          if(edtgrupo03.Text<>'  /  /    ')
          then pdata:=edtgrupo03.Text;

          {if (comebarra(edtgrupo03.text)<>'')
          Then if (Validadata(2,pdata,False)=False)
               then exit; }
     End;

     With Self.ObjQueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select');
          sql.add('Tabferragem.codigo as ferragem,');
          sql.add('Tabferragem.REferencia as REFferragem,');
          sql.add('Tabferragem.descricao as NOMEferragem,');
          sql.add('Tabcor.codigo as COR,');
          sql.add('Tabcor.Referencia as REFCOR,');
          sql.add('tabcor.descricao as NOMECOR,');
          sql.add('tabferragem.precocusto,');

          sql.add('coalesce(sum(TabEstoque.quantidade),0) as estoque,');//alterado aqui
          sql.add('(coalesce(sum(TabEstoque.quantidade),0)*tabferragem.precocusto) as VALORTOTAL');

          sql.add('from tabferragemcor');
          sql.add('join tabferragem on tabferragemcor.ferragem=tabferragem.codigo');
          sql.add('join tabcor on Tabferragemcor.cor=tabcor.codigo');
          //sql.add('left join tabestoque on tabestoque.ferragemcor=TabferragemCor.codigo');

          if (pdata<>'')
          then Begin
                    sql.add('left join tabestoque on (tabestoque.ferragemcor=TabferragemCor.codigo');
                    sql.add('and tabestoque.data<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(pdata))+#39+')');

                    //ParamByName('pdata').asdatetime:=strtodate(pdata);
          End
          Else sql.add('left join tabestoque on tabestoque.ferragemcor=TabferragemCor.codigo');


          Sql.add('Where Tabferragem.codigo<>-500');

         {}

          if (Pgrupo<>'')
          Then Sql.add('and Tabferragem.Grupoferragem='+pgrupo);

          if(ferragem<>'')
          then SQL.Add('and tabferragem.codigo='+ferragem);

          Sql.add('group by Tabferragem.codigo,');
          sql.add('Tabferragem.REferencia,');
          sql.add('Tabferragem.descricao,');
          sql.add('Tabcor.codigo,');
          sql.add('Tabcor.Referencia,');
          sql.add('tabcor.descricao,');
          sql.add('tabferragem.precocusto');



          Sql.add('order by Tabferragem.referencia,tabcor.referencia');


          open;

          if (Recordcount=0)
          then Begin
                    MensagemAviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;

          With FreltxtRDPRINT do
          begin
               ConfiguraImpressao;
               RDprint.FonteTamanhoPadrao:=S17cpp;
               RDprint.TamanhoQteColunas:=130;
               RDprint.Abrir;

               if (RDprint.Setup=False)
               then begin
                         RDprint.Fechar;
                         exit;
               end;

               LinhaLocal:=3;

               RDprint.ImpC(linhalocal,65,'RELAT�RIO DE ESTOQUE  - FERRAGENS',[negrito]);
               IncrementaLinha(2);

               if (Pgrupo<>'')
               then begin
                         RDprint.ImpF(linhalocal,1,'Grupo: '+Self.ferragem.Grupoferragem.Get_Codigo+'-'+Self.ferragem.Grupoferragem.Get_Nome,[negrito]);
                         IncrementaLinha(1);
               End;

               if (Pdata<>'')
               then Begin
                         VerificaLinha;
                         RDprint.Impf(linhalocal,1,'Data: '+pdata,[negrito]);
                         IncrementaLinha(1);
               end;


               IncrementaLinha(1);
               VerificaLinha;
               RDprint.Impf(linhalocal,1,CompletaPalavra('CODIGO',6,' ')+' '+
                                             CompletaPalavra('REF.',10,' ')+' '+
                                             CompletaPalavra('NOME FERRAGEM',42,' ')+' '+
                                             CompletaPalavra('REF.COR',10,' ')+' '+
                                             CompletaPalavra('NOME COR',19,' ')+' '+
                                             CompletaPalavra_a_Esquerda('ESTOQUE',12,' ')+' '+
                                             CompletaPalavra_a_Esquerda('CUSTO',12,' ')+' '+
                                             CompletaPalavra_a_Esquerda('TOTAL',12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               IncrementaLinha(1);

               PsomaEstoque:=0;
               PsomaValorTotal:=0;

               While not(self.ObjQueryPesquisa.eof) do
               Begin
                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('ferragem').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('refferragem').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('nomeferragem').asstring,42,' ')+' '+
                                             CompletaPalavra(fieldbyname('refcor').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('nomecor').asstring,19,' ')+' '+
                                             CompletaPalavra_a_Esquerda(fieldbyname('estoque').asstring,12,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('precocusto').asstring),12,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valortotal').asstring),12,' '));
                    IncrementaLinha(1);

                    PsomaEstoque:=PsomaEstoque+Fieldbyname('estoque').asinteger;
                    PsomaValorTotal:=PsomaValorTotal+Fieldbyname('valortotal').asfloat;


                    self.ObjQueryPesquisa.next;
               End;
               DesenhaLinha;
               VerificaLinha;
               RDprint.Impf(linhalocal,93,CompletaPalavra_a_Esquerda(inttostr(pSomaEstoque),12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(formata_valor(psomavalortotal),12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               RDprint.fechar;
          End;
     End;


end;

procedure TObjferragemCOR.EdtferragemCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,Self.Get_TituloPesquisa,Nil)=True)
            Then Begin                                                                         
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then begin
                                            if (Self.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring))
                                            Then Begin
                                                      Self.TabelaparaObjeto;
                                                      LABELNOME.caption:=completapalavra(Self.Ferragem.Get_Descricao,50,' ')+' COR: ' +Self.Cor.Get_Descricao;
                                            End;
                                 End;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


function TObjFERRAGEMCOR.Get_ClassificacaoFiscal: String;
begin
     Result:=Self.ClassificacaoFiscal;
end;

procedure TObjFERRAGEMCOR.Submit_ClassificacaoFiscal(parametro: String);
begin
     Self.ClassificacaoFiscal:=Parametro;
end;

//***COM O ESTOQUE SAZIONAL O CAMPO ESTOQUE PERDE A UTILIDADE...
//LOGO N�O TEM PORQUE MANTER ESSAS FUNC��ES...
//JONATAN MEDINA

{procedure TObjFERRAGEMCOR.DiminuiEstoque(quantidade,ferragemcor:string);
begin
    with ObjqueryEstoque do
    begin
          Close;
          sql.Clear;
          sql.Add('update tabferragemcor set estoque=estoque+'+quantidade);
          SQL.Add('where codigo='+ferragemcor);
          ExecSQL;
    end;
end;

function TObjFERRAGEMCOR.AumentaEstoque(quantidade,ferragemcor:string):Boolean;
begin
    Result:=True;
    try
            with ObjqueryEstoque do
            begin
                  Close;
                  sql.Clear;
                  sql.Add('update tabferragemcor set estoque=estoque+'+quantidade);
                  SQL.Add('where codigo='+ferragemcor);
                  ExecSQL;
                  FDataModulo.IBTransaction.CommitRetaining;
            end;
    except
            result:=False;
    end;



end; }

procedure TObjFERRAGEMCOR.Submit_Estoque(parametro:string);
begin
    Self.Estoque:=parametro;
end;

function TObjFERRAGEMCOR.Get_Estoque:string;
begin
    Result:=Self.Estoque;
end;


function TObjFERRAGEMCOR.Get_EstoqueMinimo: string;
begin
  Result := self.EstoqueMinimo;
end;

procedure TObjFERRAGEMCOR.Submit_EstoqueMinimo(parametro: string);
begin
  self.EstoqueMinimo := parametro;
end;

end.




