object FNotaFiscal: TFNotaFiscal
  Left = 542
  Top = 165
  Width = 955
  Height = 711
  Caption = 'CADASTRO DE NOTAS FISCAIS'
  Color = 14024703
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object imgrodape: TImage
    Left = 0
    Top = 626
    Width = 939
    Height = 47
    Align = alBottom
    Stretch = True
  end
  object Guia: TTabbedNotebook
    Left = 0
    Top = 366
    Width = 939
    Height = 260
    Align = alBottom
    PageIndex = 1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    OnChange = GuiaChange
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Fatura/Transportadora'
      object Bevel1: TBevel
        Left = 9
        Top = 56
        Width = 660
        Height = 152
        Shape = bsFrame
        Style = bsRaised
      end
      object LbNomeTransportadora: TLabel
        Left = 18
        Top = 71
        Width = 98
        Height = 11
        Caption = 'NOME/RAZ'#195'O SOCIAL'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbFreteporContaTransportadora: TLabel
        Left = 266
        Top = 71
        Width = 86
        Height = 11
        Caption = 'FRETE POR CONTA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 266
        Top = 85
        Width = 52
        Height = 10
        Caption = '1. EMITENTE'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 266
        Top = 95
        Width = 71
        Height = 10
        Caption = '2. DESTINAT'#193'RIO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbPlacaVeiculoTransportadora: TLabel
        Left = 378
        Top = 71
        Width = 91
        Height = 11
        Caption = 'PLACA DO VE'#205'CULO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbUFVeiculoTransportadora: TLabel
        Left = 496
        Top = 71
        Width = 13
        Height = 11
        Caption = 'UF'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbCNPJTransportadora: TLabel
        Left = 548
        Top = 71
        Width = 47
        Height = 11
        Caption = 'CNPJ/CPF'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbIETransportadora: TLabel
        Left = 548
        Top = 116
        Width = 101
        Height = 11
        Caption = 'INSCRI'#199#195'O ESTADUAL'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbUFTransportadora: TLabel
        Left = 496
        Top = 116
        Width = 13
        Height = 11
        Caption = 'UF'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbMunicipioTransportadora: TLabel
        Left = 258
        Top = 116
        Width = 50
        Height = 11
        Caption = 'MUNIC'#205'PIO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbEnderecoTransportadora: TLabel
        Left = 18
        Top = 116
        Width = 52
        Height = 11
        Caption = 'ENDERE'#199'O'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbQuantidade: TLabel
        Left = 18
        Top = 164
        Width = 59
        Height = 11
        Caption = 'QUANTIDADE'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbMarca: TLabel
        Left = 186
        Top = 164
        Width = 31
        Height = 11
        Caption = 'MARCA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbEspecie: TLabel
        Left = 90
        Top = 164
        Width = 40
        Height = 11
        Caption = 'ESP'#201'CIE'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbNumero: TLabel
        Left = 258
        Top = 164
        Width = 40
        Height = 11
        Caption = 'N'#218'MERO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbPesoBruto: TLabel
        Left = 458
        Top = 164
        Width = 61
        Height = 11
        Caption = 'PESO BRUTO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbPesoLiquido: TLabel
        Left = 586
        Top = 164
        Width = 68
        Height = 11
        Caption = 'PESO L'#205'QUIDO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 19
        Top = 49
        Width = 275
        Height = 15
        Caption = 'TRANSPORTADOR / VOLUMES TRANSPORTADOS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 13
        Top = 7
        Width = 70
        Height = 11
        Caption = 'Transportadora'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbtransportadora: TLabel
        Left = 125
        Top = 24
        Width = 85
        Height = 14
        Caption = 'Transportadora'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label21: TLabel
        Left = 682
        Top = 63
        Width = 67
        Height = 13
        Caption = 'Vencimento'
      end
      object Label24: TLabel
        Left = 682
        Top = 92
        Width = 67
        Height = 13
        Caption = 'Vencimento'
      end
      object Label25: TLabel
        Left = 682
        Top = 121
        Width = 67
        Height = 13
        Caption = 'Vencimento'
      end
      object Label28: TLabel
        Left = 682
        Top = 148
        Width = 67
        Height = 13
        Caption = 'Vencimento'
      end
      object Label29: TLabel
        Left = 682
        Top = 174
        Width = 67
        Height = 13
        Caption = 'Vencimento'
      end
      object Label30: TLabel
        Left = 825
        Top = 92
        Width = 30
        Height = 13
        Caption = 'Valor'
      end
      object Label31: TLabel
        Left = 825
        Top = 121
        Width = 30
        Height = 13
        Caption = 'Valor'
      end
      object Label32: TLabel
        Left = 825
        Top = 148
        Width = 30
        Height = 13
        Caption = 'Valor'
      end
      object Label33: TLabel
        Left = 825
        Top = 174
        Width = 30
        Height = 13
        Caption = 'Valor'
      end
      object Label34: TLabel
        Left = 825
        Top = 63
        Width = 30
        Height = 13
        Caption = 'Valor'
      end
      object EdtNomeTransportadora: TEdit
        Left = 18
        Top = 84
        Width = 227
        Height = 19
        MaxLength = 100
        TabOrder = 1
      end
      object EdtFreteporContaTransportadora: TEdit
        Left = 343
        Top = 84
        Width = 32
        Height = 19
        MaxLength = 2
        TabOrder = 2
      end
      object EdtPlacaVeiculoTransportadora: TEdit
        Left = 378
        Top = 84
        Width = 104
        Height = 19
        MaxLength = 100
        TabOrder = 3
      end
      object EdtUFVeiculoTransportadora: TEdit
        Left = 496
        Top = 84
        Width = 50
        Height = 19
        MaxLength = 100
        TabOrder = 4
      end
      object EdtCNPJTransportadora: TEdit
        Left = 548
        Top = 84
        Width = 116
        Height = 19
        MaxLength = 100
        TabOrder = 5
      end
      object EdtIETransportadora: TEdit
        Left = 548
        Top = 132
        Width = 116
        Height = 19
        MaxLength = 100
        TabOrder = 9
      end
      object EdtUFTransportadora: TEdit
        Left = 496
        Top = 132
        Width = 50
        Height = 19
        MaxLength = 100
        TabOrder = 8
      end
      object EdtMunicipioTransportadora: TEdit
        Left = 258
        Top = 132
        Width = 224
        Height = 19
        MaxLength = 100
        TabOrder = 7
      end
      object EdtEnderecoTransportadora: TEdit
        Left = 18
        Top = 132
        Width = 219
        Height = 19
        MaxLength = 100
        TabOrder = 6
      end
      object EdtQuantidade: TEdit
        Left = 18
        Top = 180
        Width = 56
        Height = 19
        MaxLength = 9
        TabOrder = 10
      end
      object EdtEspecie: TEdit
        Left = 90
        Top = 180
        Width = 88
        Height = 19
        MaxLength = 50
        TabOrder = 11
      end
      object EdtMarca: TEdit
        Left = 186
        Top = 180
        Width = 64
        Height = 19
        MaxLength = 50
        TabOrder = 12
      end
      object edtnumerovolumes: TEdit
        Left = 258
        Top = 180
        Width = 192
        Height = 19
        MaxLength = 50
        TabOrder = 13
      end
      object EdtPesoBruto: TEdit
        Left = 458
        Top = 180
        Width = 124
        Height = 19
        MaxLength = 9
        TabOrder = 14
      end
      object EdtPesoLiquido: TEdit
        Left = 586
        Top = 180
        Width = 78
        Height = 19
        MaxLength = 9
        TabOrder = 15
      end
      object edttransportadora: TEdit
        Left = 13
        Top = 20
        Width = 104
        Height = 19
        MaxLength = 100
        TabOrder = 0
        OnExit = edttransportadoraExit
        OnKeyDown = edttransportadoraKeyDown
      end
      object edtvencimentofatura1: TMaskEdit
        Left = 752
        Top = 60
        Width = 69
        Height = 19
        EditMask = '99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 16
        Text = '  /  /    '
      end
      object edtvencimentofatura2: TMaskEdit
        Left = 752
        Top = 89
        Width = 70
        Height = 19
        EditMask = '99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 18
        Text = '  /  /    '
      end
      object edtvencimentofatura3: TMaskEdit
        Left = 752
        Top = 118
        Width = 71
        Height = 19
        EditMask = '99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 20
        Text = '  /  /    '
      end
      object edtvencimentofatura4: TMaskEdit
        Left = 752
        Top = 145
        Width = 72
        Height = 19
        EditMask = '99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 22
        Text = '  /  /    '
      end
      object edtvencimentofatura5: TMaskEdit
        Left = 752
        Top = 171
        Width = 73
        Height = 19
        EditMask = '99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 24
        Text = '  /  /    '
      end
      object edtvalorfatura1: TMaskEdit
        Left = 860
        Top = 59
        Width = 54
        Height = 19
        TabOrder = 17
      end
      object edtvalorfatura2: TMaskEdit
        Left = 860
        Top = 88
        Width = 54
        Height = 19
        TabOrder = 19
      end
      object edtvalorfatura3: TMaskEdit
        Left = 859
        Top = 117
        Width = 55
        Height = 19
        TabOrder = 21
      end
      object edtvalorfatura4: TMaskEdit
        Left = 860
        Top = 144
        Width = 54
        Height = 19
        TabOrder = 23
      end
      object edtvalorfatura5: TMaskEdit
        Left = 860
        Top = 170
        Width = 54
        Height = 19
        TabOrder = 25
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Items'
      object PanelProdutos: TPanel
        Left = 0
        Top = 0
        Width = 920
        Height = 116
        BevelOuter = bvNone
        Color = 14024703
        TabOrder = 0
        object Label5: TLabel
          Left = 102
          Top = 39
          Width = 30
          Height = 13
          Caption = 'Valor'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label18: TLabel
          Left = 7
          Top = 39
          Width = 66
          Height = 13
          Caption = 'Quantidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label19: TLabel
          Left = 7
          Top = 2
          Width = 55
          Height = 13
          Caption = 'Categoria'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label20: TLabel
          Left = 288
          Top = 2
          Width = 77
          Height = 13
          Caption = 'Produto/COR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbnomeproduto: TLabel
          Left = 352
          Top = 20
          Width = 358
          Height = 14
          AutoSize = False
          Caption = 'Produto'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label35: TLabel
          Left = 182
          Top = 39
          Width = 63
          Height = 13
          Caption = 'Valor Frete'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label37: TLabel
          Left = 278
          Top = 39
          Width = 74
          Height = 13
          Caption = 'Valor Seguro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object BtCancelar_nf: TBitBtn
          Left = 829
          Top = 7
          Width = 40
          Height = 39
          TabOrder = 20
          OnClick = BtCancelar_nfClick
        end
        object btGravar_nf: TBitBtn
          Left = 781
          Top = 7
          Width = 40
          Height = 39
          TabOrder = 19
          OnClick = btGravar_nfClick
        end
        object BtExcluir_nf: TBitBtn
          Left = 877
          Top = 6
          Width = 40
          Height = 39
          TabOrder = 21
          OnClick = BtExcluir_nfClick
        end
        object edtvalor_nf: TEdit
          Left = 102
          Top = 54
          Width = 70
          Height = 19
          TabOrder = 5
          OnKeyPress = edtvalor_nfKeyPress
        end
        object edtquantidade_nf: TEdit
          Left = 7
          Top = 54
          Width = 70
          Height = 19
          TabOrder = 4
          OnKeyPress = edtquantidade_nfKeyPress
        end
        object combotipoprodutos_nf: TComboBox
          Left = 7
          Top = 16
          Width = 145
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Text = '   '
          OnChange = combotipoprodutos_nfChange
          Items.Strings = (
            'Diversos'
            'Ferragem'
            'KitBox'
            'Perfilado'
            'Persiana'
            'Vidro')
        end
        object edtproduto_nf: TEdit
          Left = 288
          Top = 16
          Width = 50
          Height = 19
          Color = 6073854
          TabOrder = 3
          Text = 'edtproduto_nf'
          OnDblClick = edtproduto_nfDblClick
          OnExit = edtproduto_nfExit
          OnKeyDown = edtproduto_nfKeyDown
          OnKeyPress = edtclienteKeyPress
        end
        object edtcodigo_nf: TEdit
          Left = 640
          Top = 14
          Width = 70
          Height = 19
          TabStop = False
          TabOrder = 0
          Text = 'edtcodigo_nf'
          Visible = False
        end
        object combotributacao: TComboBox
          Left = 624
          Top = 52
          Width = 89
          Height = 21
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 10
          Text = '   '
          Visible = False
          Items.Strings = (
            'Isento'
            'Substituto'
            'Tributado')
        end
        object EdtAliquota: TEdit
          Left = 629
          Top = 60
          Width = 72
          Height = 19
          MaxLength = 9
          TabOrder = 11
          Visible = False
        end
        object edtreducaobasecalculo: TEdit
          Left = 625
          Top = 76
          Width = 72
          Height = 19
          MaxLength = 9
          TabOrder = 12
          Visible = False
        end
        object edtaliquotacupom: TEdit
          Left = 606
          Top = 76
          Width = 72
          Height = 19
          MaxLength = 9
          TabOrder = 13
          Visible = False
        end
        object edtipi: TEdit
          Left = 655
          Top = 15
          Width = 70
          Height = 19
          TabOrder = 16
          Text = 'edtipi'
          Visible = False
          OnKeyPress = edtvalor_nfKeyPress
        end
        object EdtSituacaoTributaria_TabelaA: TEdit
          Left = 616
          Top = 60
          Width = 25
          Height = 19
          Color = clGradientActiveCaption
          MaxLength = 9
          TabOrder = 8
          Text = '1'
          Visible = False
        end
        object EdtSituacaoTributaria_TabelaB: TEdit
          Left = 632
          Top = 44
          Width = 57
          Height = 19
          Color = clSkyBlue
          MaxLength = 9
          TabOrder = 9
          Text = '1'
          Visible = False
        end
        object edtpercentualagregado: TEdit
          Left = 622
          Top = 48
          Width = 70
          Height = 19
          TabOrder = 6
          Text = 'edtpercentualagregado'
          Visible = False
          OnKeyPress = edtvalor_nfKeyPress
        end
        object edtmargemvaloragregadoconsumidor: TEdit
          Left = 654
          Top = 24
          Width = 70
          Height = 19
          TabOrder = 7
          Text = 'edtpercentualagregado'
          Visible = False
          OnKeyPress = edtvalor_nfKeyPress
        end
        object edtvalorfrete_produto: TEdit
          Left = 182
          Top = 54
          Width = 70
          Height = 19
          TabOrder = 14
          OnKeyPress = edtvalor_nfKeyPress
        end
        object edtvalorseguro_produto: TEdit
          Left = 278
          Top = 54
          Width = 70
          Height = 19
          TabOrder = 15
          OnKeyPress = edtvalor_nfKeyPress
        end
        object panelimpostos: TPanel
          Left = 593
          Top = 48
          Width = 329
          Height = 57
          TabOrder = 18
          object Label17: TLabel
            Left = 7
            Top = 9
            Width = 31
            Height = 13
            Caption = 'ICMS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label22: TLabel
            Left = 87
            Top = 9
            Width = 17
            Height = 13
            Caption = 'IPI'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label43: TLabel
            Left = 167
            Top = 9
            Width = 21
            Height = 13
            Caption = 'PIS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label44: TLabel
            Left = 247
            Top = 9
            Width = 46
            Height = 13
            Caption = 'COFINS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object edticms_fornecedor: TEdit
            Left = 7
            Top = 22
            Width = 70
            Height = 19
            Color = 6073854
            TabOrder = 0
            OnDblClick = edticms_fornecedorDblClick
            OnExit = edticms_fornecedorExit
            OnKeyDown = edticms_fornecedorKeyDown
            OnKeyPress = edtclienteKeyPress
          end
          object edtipi_fornecedor: TEdit
            Left = 87
            Top = 22
            Width = 70
            Height = 19
            Color = 6073854
            TabOrder = 1
            OnDblClick = edtipi_fornecedorDblClick
            OnExit = edtipi_fornecedorExit
            OnKeyDown = edtipi_fornecedorKeyDown
            OnKeyPress = edtclienteKeyPress
          end
          object edtpis_fornecedor: TEdit
            Left = 167
            Top = 22
            Width = 70
            Height = 19
            Color = 6073854
            TabOrder = 2
            OnDblClick = edtpis_fornecedorDblClick
            OnExit = edtpis_fornecedorExit
            OnKeyDown = edtpis_fornecedorKeyDown
            OnKeyPress = edtclienteKeyPress
          end
          object edtcofins_fornecedor: TEdit
            Left = 247
            Top = 22
            Width = 70
            Height = 19
            Color = 6073854
            TabOrder = 3
            OnDblClick = edtcofins_fornecedorDblClick
            OnExit = edtcofins_fornecedorExit
            OnKeyDown = edtcofins_fornecedorKeyDown
            OnKeyPress = edtclienteKeyPress
          end
        end
        object panelcfop: TPanel
          Left = 504
          Top = 48
          Width = 89
          Height = 57
          TabOrder = 17
          object Label45: TLabel
            Left = 7
            Top = 11
            Width = 33
            Height = 13
            Caption = 'CFOP'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object edtcfop_fornecedor: TEdit
            Left = 7
            Top = 24
            Width = 70
            Height = 19
            Color = 6073854
            TabOrder = 0
            OnDblClick = edtcfop_fornecedorDblClick
            OnExit = edtcfop_fornecedorExit
            OnKeyDown = edtcfop_fornecedorKeyDown
            OnKeyPress = edtclienteKeyPress
          end
        end
        object panelreferencia: TPanel
          Left = 371
          Top = 56
          Width = 129
          Height = 41
          TabOrder = 2
          object Label47: TLabel
            Left = 54
            Top = 4
            Width = 59
            Height = 13
            Caption = 'Prod. Nef.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label46: TLabel
            Left = 8
            Top = 4
            Width = 40
            Height = 13
            Caption = 'NF ref.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object edtproduto_ref: TEdit
            Left = 63
            Top = 16
            Width = 50
            Height = 19
            TabOrder = 1
            Text = 'edtnf_referencia'
            OnExit = edtproduto_refExit
            OnKeyDown = edtproduto_refKeyDown
          end
          object edtnf_referencia: TEdit
            Left = 8
            Top = 16
            Width = 50
            Height = 19
            TabOrder = 0
            Text = 'edtnf_referencia'
            OnExit = edtnf_referenciaExit
            OnKeyDown = edtnf_referenciaKeyDown
          end
        end
      end
      object StrgProdutos: TStringGrid
        Left = 0
        Top = 76
        Width = 931
        Height = 137
        Align = alBottom
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goDrawFocusSelected]
        TabOrder = 1
        OnDblClick = StrgProdutosDblClick
        OnDrawCell = StrgProdutosDrawCell
        OnEnter = StrgProdutosEnter
        OnKeyDown = StrgProdutosKeyDown
        OnKeyPress = StrgProdutosKeyPress
      end
      object StatusBar1: TStatusBar
        Left = 0
        Top = 213
        Width = 931
        Height = 19
        Font.Charset = ANSI_CHARSET
        Font.Color = clBtnText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Panels = <
          item
            Text = 
              '<ESPA'#199'O>=PESQUISAR | F12=ORDENA |USE % PARA PESQUISAR TEXTOS PAR' +
              'CIAIS'
            Width = 50
          end>
        UseSystemFont = False
      end
      object edtpesquisa_STRG_GRID: TMaskEdit
        Left = 1
        Top = 394
        Width = 121
        Height = 19
        TabOrder = 3
        Text = 'edtpesquisa_STRG_GRID_CP'
        OnKeyPress = edtpesquisa_STRG_GRIDKeyPress
      end
      object LbtipoCampoProduto: TListBox
        Left = 7
        Top = 302
        Width = 33
        Height = 33
        ItemHeight = 13
        TabOrder = 4
        Visible = False
      end
    end
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 939
    Height = 54
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 1
    DesignSize = (
      939
      54)
    object lbnomeformulario: TLabel
      Left = 621
      Top = 3
      Width = 104
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Cadastro de'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb22: TLabel
      Left = 636
      Top = 25
      Width = 49
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Notas'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object bt1: TSpeedButton
      Left = 887
      Top = -18
      Width = 53
      Height = 80
      Cursor = crHandPoint
      Hint = 'Ajuda'
      Anchors = [akTop, akRight]
      Flat = True
      Glyph.Data = {
        4A0D0000424D4A0D0000000000003600000028000000240000001F0000000100
        180000000000140D0000D8030000D80300000000000000000000FEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFAF6FBF9F2FEFEF4FFFFFAFF
        FCFDFFF9FFFEFEFDFCFFFFFEFEFFFDFEFCFEFEF8FFFFFBFEFCFFFFFCFFFDF6FB
        FFF7F8FFF7FBFFFBFBFDF7FFFFFCFFFFFEF7FDFCFBFFFFFFFBFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFF9FD
        F8FDFFFFFDFBFFFFFBFFFFFDFEFFFCFCFFFEFFFEFDFFFCF9FBF0EFF1F0EBEDF9
        F1F2F6F8F9F1FFFFF4FFFFFEFCFFFFFDFFFFFAFFFBFCFFFEFDFFFFFAFDFAFCFD
        F7FFFFFFFCFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFFFCFFFFFCFEFFFDFCFFFFFFFFFDFFF2E4E6DEBCBCCF
        9F99EB8688E68081EA7E7DF68686FE9092FDA2A5F7C3C3FFF0EDFFFAFFF8FFFF
        FFFDFFFFF5FDFCFDFFF6FFFFFBF8FAFFFDFEFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFBFFFCFEFFFBFFFCE7
        E1DCC29E9EBF7579DC7677F07B76FF8783FF8F88FD9388FB8F84FF837BFF7977
        F77576F98084FBB6B9F2F2ECE8FFFAF8FDFCFFFEFFFAFCFDFFFCFEFCFEFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFEFDFFFAFCD7B8BBB17879C87772F08E88FE9893F58E8CE7797DD76F70
        D26867DC716EE88581F69A95F99591FE8686FF7173F28484FFD9D4FFFFFBFDF8
        F7FFFCFFFFFEFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFF9B99D9CA76B6CDA8786F99B96E17D79
        B6484C98222FAB1122AA0F1EA5131FA41D25AA2226C53938E6625BFF9085FD9B
        91FF7B7CF2777BF4C8C7F8FFFEF9FEFDFFFEFFFFFBFCFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFC0A0A1A7706D
        E6928CFC9691B3484A87192589112398182BA32E31DC6F71EB8082CC5050BB28
        26D32A22EB2F24FE3727F36D69FF9D97FB7D83F7707EFEC5CDFFFEFFFFFDFDED
        FFFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFCFBFDFFFDFFFFFBFFFAFFFFF2FAFA
        FFFEFEDCBDBEA66D6BDB9092ED909996303C770E1B8D1E2C9B1C2BA11425D96F
        75FDA8B0FFA2B0FFA0AAC83F43BC2F26D73927FF3929FF2B1CEC5650FF9996FE
        7D7AFD7F7EFADFDBFFFEFFF4FEFEFEFDFFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFF
        FDF9FEFFFDFFF7FAFEFAFFFFF4E6E7A67F81CF8B8CEC9A9F92323C7B121D9123
        2F921C27A5232EA71C27DE837EFFB7B5F9A9AEFDB0B3BF4343B42821C9372BDA
        3326E33626CB2728CA5556FF9893FB7777DE9A9BFFFEFEFFFCFDFEFDFFFEFDFF
        FEFDFFFEFDFFFEFEFEFEFDFFFFFBFFFFFBFFFAFBFFFFFFFFC3AEADA77678F8A4
        A9A9515776131B8D202898242BA3282CA72629A82627B55047EBADA2FFBCB5CD
        7974A92E2AA13026A5372BBD332DBF3528BD2B2DA51E26DF7372FF918FE27377
        F0CCCCFFFFFEFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFAF9FBFFFDFFFFFAFFFFFE
        FFF5F0EFA38384DB9FA0E78A91791D22852428992E309E2827A72C28AC3028AB
        3229A3271FA13E36B4413E9E332CB03129A73126A23127B0322EA93026AD3131
        A91925B63239F2948FFD7C7FE9989BFBFFF9FEFDFFFEFDFFFEFDFFFEFDFFFFFE
        FEFCFBFDFFFEFFFFFDFFFFFEFFDACECEAE8687F0ABAEB156597E24248B2C2992
        2A25A93730A93328AE3B2EA33222D35E59EA928BF28C87E27D74AC362BA93A2A
        A23729A5342AA3342CA62728B02931A71821D8716FFF8A8ADE7D7FEFECE7FEFD
        FFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFFFDFCFEFFFCFEFFFEFFC0B3B1BF9392F0
        A5A78F3B35842A2396352B9F372AA93C2EAC3D2DAC3C2AA1311FD16D69F8CABF
        FBC0B7FCABA3AA3A2EA23827A33928A9372AA6352BAE302BA72C2EAA1922C550
        51FF9692DC7475E0D2D3FFFEFFFEFDFFFEFDFFFEFDFFFFFDFEFEFDFFFEFDFFFF
        FDFFFFFEFEB6A9A7CB9D9CEC9E9F8335288E3527A0392AAD4130A83C2AA73B29
        A93927AE3827CE6C66FFCDC1FABAB5FFBFB6D46B62AD3126B43427B13A2BA933
        27A93327A02A25AC1F28AF373BFC9D94E7797BD1B8BCFFFEFFFFFEFFFEFDFFFE
        FDFFFFFDFEFCFCFCFDFCFEFFFEFFFDFBFBB5A8A6DBAEABE19192813522983C29
        A73B29AF3E2AA83B26AA3F2AAD3B2AAF3224B05A4EFFE0D0FFCEC6F7BEB5FFBE
        B3DE8C7BA44434A33628AD3C2CAD3726A8342DA7262DAE2E33FA9B92E27A7BD4
        ABB3FFFEFFFFFEFFFEFDFFFEFDFFFBFCFFF9FEFFFFFCFDFBFFFFFCF9FBC9AAAB
        DEB1ADDF9A91813628B03C2BAB3D25A7412AA6362AB3362EA13726AE3C25A833
        24D79987FFF4E4F9D3CEFEBEBEFFC9C3EC9E91AD4436B137299C3B27AA3526B1
        2F28A4302FF7969AE67F86C7AAA5FFFEFFFFFEFFFEFDFFFEFDFFFAFBFFFCFEFF
        FFFEFFFCFEFFFCFEFFCEB4B4E6B6B2E6A99F98392AA13426B2382C9E3E2EAA3E
        33AD4338A74539A24536AB443B9B3D38CF8A87FFEBE6FEDED9F1C0B8FFC9BDEC
        8B81AA372AA73B2AA83F2CA52B1FA6403BF9A1A1E57E86C9B3AEFFFEFFFFFEFF
        FEFDFFFEFDFFFAFDFFFDFCFEFFFEFFFDFCFEFAFFFFD3BEC0ECB5B2F5C2B8AB40
        329F3D31B84D49A24F47AD554EAE574DAE5D55B1655FAD5654B34E50A13B40B9
        7B7BFFE1DBFED7CFF9CAC2FFBCB5BD5146AD3627A3402AA83526AD544AFEACAB
        D5787FD4C7C5FFFEFFFEFDFFFEFDFFFEFDFFFBFFFFFFFBFDFFFDFDFFFDFFF9FE
        FFDFD1D2E7B1B0FFDACEC26B61A74C45A55C54A85A54D3958FF2B8B3F9C2BFF4
        B4B3AD5352AB5C59AC5553A54343F7C0BBFDE0D9FFCCCAFBC9C9CC6A60B03429
        9F3923A63624C9776BFFADACC67E84E4E0DFFEFDFFFEFDFFFEFDFFFEFDFFFAFE
        FFFFFCFFFCFCFCFFFEFFFBFEFFF4ECEDE2B7B4FFDBD2DAB0ABA6504AA86153AB
        514AD59594FFF8F7FEF1EFFAD4D2C469649C5148A85950A14B45ECBBB3FBE1DA
        FFCED0FFCFD6D06E66AB352AAD3B2A9F3726F7A89DFBA4A7BB8E91F2F7F6FEFD
        FFFEFDFFFEFDFFFEFDFFFCFBFDFFFEFFFCFEFEFEFDFFFDFCFFFFFEFFE2C7C3F3
        CAC1EDDEDCBD7672AD5144CE6459D7797AF6F5F1F4FFFEF9F3EEE9B4AAB16860
        B45E58C99087FAD7CDFFDCD6F8D5D2FECBCFBE5A55A23127A12F22CB7264FFC3
        BBD28B8ECFB7B9FAFFFFFEFDFFFEFDFFFEFDFFFEFDFFFEFBFDFFFFFFFCFEFEFB
        FDFEFFFAFFFFFEFFEBE5DEF2C4BDFFE5E8EFC8C0AE6053E17164F47773F0BBB8
        FDFEFAFDFFFFFFF8F1FCD6D4FFCDCCFFDDD8FFDED8FFE0DAFFDDD7E7A1A2A339
        32A7352EAC4A3EF8B8ADECB2ADB68E90FAECEEFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFDFFFCFCFCFAFFFEFAFDFFFFFBFFFFFDFDFBFFF9FFCFC9FFC7CCFFF9EF
        E8BFB0D66D5FFF7C70F9807EEACECDFFFBFFFDFCFFF9FEFFFDFFFFFFE4E5FFE2
        DFF6E7DEEDB6AFB34648A1312BB03E37FCAA9FF7CDC1BC8F8CD5C7C8FFFDFFFC
        FEFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFFFEFFFFFEFF
        FDFCFEFCFBFDFCC8C8F6D7D6FFFCF6F0C9C1E57F7AFF7673F98481ECA5A1E6D6
        D7FCF1F3F9F1F1ECD7D9F0C1C3D78A8DA84646A03232A54B4BECABAAFFD1CFC9
        A09EC9ADACFEF9F8FBFFFFFCFCFCFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFF
        FDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFDFCFEFFF4EEF8CDCAFDD8D4FFFBF4FDE6
        E4FBA1A6FB7375FD6F68EA6E5CCE6855B76958AE6458A5514B993E3AA04D4BC8
        7F7BFCC3C1FFCBCCDAA4A4BFAEABF3F9F4FFFFFEFFFCFDFCFEFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFEFDFFFFFEFFF4FF
        FFFBF2EFF6D6D1FDD9D3FFEFE8FCFBF1FFE6DCF6BBB2EE9285D07C70B76F65B2
        6F66C0807BDDA39EF3C1BBFFD6CFF6C6C5D4ACADCEBBB8F9FAF6F7FFFDFAFCFC
        FFFEFFFCF9FBFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFC
        FEFDFCFEFEFDFFFFFEFFFFFAFCFBFFFFF9F9F9FCE0DFFFD3CCFEDDD4F8ECE6F8
        F6F5F5F8F6F4EFEEF6E4E5FADBDCFFDBDCFFD9D8FBCAC8F0C0BCD7B5B6E1D8D5
        FAFFFEFFFFFFFEFCFCFAFFFFF8FDFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFFFFFBFFFBFCFDFDFFFD
        FBFFFFEEF1FFE2E1FFD3D2FFD0D2FFD2CFFFD1CDFFD2CFFFCFCFFFC9C9F2BFC3
        E9C4C6EED6D8F5F7F7FFFBFCFFFEFFFFFCFEFDFFFFF9FCFFFCFBFFFFFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFFFEFDFFFEFDFFFDFCFEFDFCFEFD
        FCFEFFFBFFF0FDFBF1FFFBFFFEFFFFFDFFF9FEFFF9FCFAFEF2EEF9EAE1F4E5DC
        F3E3DCF3E3DDF6E7E5FCF1F3FEFAFFF9FCFFF4FEFEFFFEFFFFF7FAFFFDFFFCF9
        FBFFFDFFFFFCFFF5F8FCFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFF7F9FFFDFEFFFFFBF8FFFDF9FDFBFAF1FBFB
        F6FFFFFBFDFEFBFEFFFFFCFFFFFDFFFFFEFFFBFFFFF8FFFFFDFEFFFFFAFFFFFD
        FFFAFEFFF8FFFFF9FBFCFFFDFFFFF9FCFBFAFCFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFCFFFEFF
        F7FDFFF5FEFFFBFFFFFFFFFEFFFCFDFFFCFFFFFAFFFFFCFFF9FEFDF9FFFDFBFF
        FEF9FCFAFBFBFBFFFFFFFFFCFEFEFDFFFBFAFCFBFFFFF5FDFDF7FFFFFDFFFFFF
        FCFFFEFDFFFEFDFFFEFDFFFEFDFF}
      ParentShowHint = False
      ShowHint = True
      OnClick = bt1Click
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btrelatoriosClick
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btopcoesClick
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
      Spacing = 0
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 54
    Width = 939
    Height = 312
    Align = alClient
    Color = 10643006
    TabOrder = 2
    DesignSize = (
      939
      312)
    object lbLbDadosAdicionais: TLabel
      Left = 23
      Top = 231
      Width = 83
      Height = 14
      Caption = 'Dados Adicionais'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb1: TLabel
      Left = 390
      Top = 184
      Width = 58
      Height = 14
      Caption = 'Valor do PIS'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbVALORFRETE: TLabel
      Left = 389
      Top = 142
      Width = 68
      Height = 14
      Caption = 'Valor do Frete'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbBASECALCULOICMS: TLabel
      Left = 590
      Top = 97
      Width = 90
      Height = 14
      Caption = 'Base C'#225'lculo ICMS'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb2: TLabel
      Left = 489
      Top = 52
      Width = 32
      Height = 14
      Caption = 'Cliente'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbnomecliente: TLabel
      Left = 566
      Top = 69
      Width = 339
      Height = 14
      AutoSize = False
      Caption = 'Cliente'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbNATUREZAOPERACAO: TLabel
      Left = 790
      Top = 14
      Width = 110
      Height = 14
      Caption = 'Natureza de Opera'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb3: TLabel
      Left = 24
      Top = 14
      Width = 33
      Height = 14
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb4: TLabel
      Left = 101
      Top = 14
      Width = 23
      Height = 14
      Caption = 'NF-e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb5: TLabel
      Left = 165
      Top = 14
      Width = 48
      Height = 14
      Caption = 'Opera'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbNomeoperacao: TLabel
      Left = 231
      Top = 33
      Width = 148
      Height = 14
      AutoSize = False
      Caption = 'lbNomeoperacao'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb6: TLabel
      Left = 396
      Top = 14
      Width = 61
      Height = 14
      Caption = 'N Nota Inicial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb7: TLabel
      Left = 467
      Top = 14
      Width = 57
      Height = 14
      Caption = 'N Nota Final'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb8: TLabel
      Left = 549
      Top = 13
      Width = 42
      Height = 14
      Caption = 'Situa'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb9: TLabel
      Left = 715
      Top = 14
      Width = 24
      Height = 13
      Caption = 'Lote'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object lb10: TLabel
      Left = 388
      Top = 100
      Width = 68
      Height = 14
      Caption = 'Hora de Sa'#237'da'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb11: TLabel
      Left = 282
      Top = 100
      Width = 67
      Height = 14
      Caption = 'Data de Sa'#237'da'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb12: TLabel
      Left = 172
      Top = 100
      Width = 56
      Height = 14
      Caption = 'Emiss'#227'o NF'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb13: TLabel
      Left = 26
      Top = 100
      Width = 110
      Height = 14
      Caption = 'NF. de Dev. de Cliente?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb14: TLabel
      Left = 24
      Top = 52
      Width = 56
      Height = 14
      Caption = 'Fornecedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbnomefornecedor: TLabel
      Left = 105
      Top = 69
      Width = 272
      Height = 14
      AutoSize = False
      Caption = 'Cliente'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb15: TLabel
      Left = 489
      Top = 97
      Width = 77
      Height = 14
      Caption = 'Tabela de Pre'#231'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb16: TLabel
      Left = 267
      Top = 145
      Width = 111
      Height = 14
      Caption = 'Valor ICMS Subst. Rec.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb17: TLabel
      Left = 153
      Top = 145
      Width = 100
      Height = 14
      Caption = 'BC.ICMS Subst. Rec.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbVALORICMS_SUBSTITUICAO: TLabel
      Left = 23
      Top = 145
      Width = 101
      Height = 14
      Caption = 'Valor do ICMS Subst.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbBASECALCULOICMS_SUBSTITUICAO: TLabel
      Left = 800
      Top = 96
      Width = 99
      Height = 14
      Caption = 'Base C. ICMS Subst.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbVALORICMS: TLabel
      Left = 710
      Top = 97
      Width = 67
      Height = 14
      Caption = 'Valor do ICMS'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbVALORSEGURO: TLabel
      Left = 483
      Top = 142
      Width = 78
      Height = 14
      Caption = 'Valor do Seguro'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbOUTRASDESPESAS: TLabel
      Left = 599
      Top = 142
      Width = 85
      Height = 14
      Caption = 'Outras Despesas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbVALORTOTALIPI: TLabel
      Left = 713
      Top = 142
      Width = 63
      Height = 14
      Caption = 'Valor Total IPI'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb18: TLabel
      Left = 23
      Top = 184
      Width = 46
      Height = 14
      Caption = 'Desconto'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbVALORTOTAL: TLabel
      Left = 150
      Top = 184
      Width = 89
      Height = 14
      Caption = 'Vl Total (Produtos)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbVALORFINAL: TLabel
      Left = 285
      Top = 184
      Width = 50
      Height = 14
      Caption = 'Valor Final'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb19: TLabel
      Left = 687
      Top = 186
      Width = 104
      Height = 14
      Caption = 'Valor do COFINS (ST)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb20: TLabel
      Left = 597
      Top = 186
      Width = 80
      Height = 14
      Caption = 'Valor do COFINS'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb21: TLabel
      Left = 485
      Top = 186
      Width = 82
      Height = 14
      Caption = 'Valor do PIS (ST)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbOBSERVACOES: TLabel
      Left = 427
      Top = 231
      Width = 66
      Height = 14
      Caption = 'Observa'#231#245'es'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object bvl1: TBevel
      Left = 3
      Top = 227
      Width = 936
      Height = 1
      Anchors = [akLeft, akTop, akRight]
    end
    object bvl2: TBevel
      Left = 0
      Top = 8
      Width = 942
      Height = 1
      Anchors = [akLeft, akTop, akRight]
    end
    object bvl3: TBevel
      Left = 0
      Top = 298
      Width = 939
      Height = 2
      Anchors = [akLeft, akTop, akRight]
    end
    object lb23: TLabel
      Left = 399
      Top = 51
      Width = 50
      Height = 14
      Caption = 'Modelo NF'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object bvl4: TBevel
      Left = 3
      Top = 90
      Width = 936
      Height = 1
      Anchors = [akLeft, akTop, akRight]
    end
    object Label1: TLabel
      Left = 641
      Top = 15
      Width = 25
      Height = 14
      Caption = 'Serie'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object mmoDadosAdicionais: TMemo
      Left = 22
      Top = 247
      Width = 389
      Height = 44
      Lines.Strings = (
        'memoDadosAdicionais')
      TabOrder = 34
    end
    object edtVALORPIS: TEdit
      Left = 390
      Top = 202
      Width = 66
      Height = 19
      MaxLength = 9
      TabOrder = 29
    end
    object edtVALORFRETE: TEdit
      Left = 390
      Top = 158
      Width = 66
      Height = 19
      MaxLength = 9
      TabOrder = 22
    end
    object edtBASECALCULOICMS: TEdit
      Left = 600
      Top = 113
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 16
    end
    object edtcliente: TEdit
      Left = 489
      Top = 66
      Width = 76
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 11
      OnDblClick = edtclienteDblClick
      OnExit = edtclienteExit
      OnKeyDown = edtclienteKeyDown
      OnKeyPress = edtclienteKeyPress
    end
    object edtNATUREZAOPERACAO: TEdit
      Left = 794
      Top = 28
      Width = 111
      Height = 19
      MaxLength = 50
      TabOrder = 8
    end
    object edtCodigo: TEdit
      Left = 24
      Top = 28
      Width = 66
      Height = 19
      MaxLength = 9
      TabOrder = 0
    end
    object edtnfe: TEdit
      Left = 101
      Top = 28
      Width = 51
      Height = 19
      Color = clMoneyGreen
      TabOrder = 1
      Text = 'edtnfe'
    end
    object edtoperacao: TEdit
      Left = 165
      Top = 28
      Width = 51
      Height = 19
      Color = 6073854
      TabOrder = 2
      OnDblClick = edtoperacaoDblClick
      OnExit = edtoperacaoExit
      OnKeyDown = edtoperacaoKeyDown
      OnKeyPress = edtoperacaoKeyPress
    end
    object edtNumero: TEdit
      Left = 396
      Top = 28
      Width = 55
      Height = 19
      MaxLength = 10
      TabOrder = 3
      OnKeyPress = edtNumeroKeyPress
    end
    object edtnotafinal: TEdit
      Left = 467
      Top = 28
      Width = 76
      Height = 19
      MaxLength = 10
      TabOrder = 4
      OnKeyPress = edtNumeroKeyPress
    end
    object cbbSituacao: TComboBox
      Left = 549
      Top = 26
      Width = 76
      Height = 21
      ItemHeight = 13
      TabOrder = 5
      Text = '  '
      Items.Strings = (
        'Impressa'
        'Rasurada'
        'N'#227'o impressa'
        'Cancelada'
        'Manual'
        'Processando - Enviada'
        'Conting'#234'ncia'
        'T-Conting'#234'ncia')
    end
    object edtlote: TEdit
      Left = 713
      Top = 28
      Width = 72
      Height = 19
      Color = clMoneyGreen
      MaxLength = 10
      TabOrder = 7
      OnKeyPress = edtNumeroKeyPress
    end
    object edthorasaida: TMaskEdit
      Left = 391
      Top = 115
      Width = 64
      Height = 19
      EditMask = '!90:00;1;_'
      MaxLength = 5
      TabOrder = 14
      Text = '  :  '
    end
    object edtdatasaida: TMaskEdit
      Left = 278
      Top = 115
      Width = 73
      Height = 19
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 13
      Text = '  /  /    '
    end
    object edtdataemissao: TMaskEdit
      Left = 162
      Top = 115
      Width = 74
      Height = 19
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 12
      Text = '  /  /    '
    end
    object cbbComboDevolucaoCliente: TComboBox
      Left = 26
      Top = 114
      Width = 73
      Height = 21
      ItemHeight = 13
      TabOrder = 36
      Text = '   '
      Items.Strings = (
        'N'#227'o'
        'Sim')
    end
    object edtfornecedor: TEdit
      Left = 24
      Top = 67
      Width = 71
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 9
      OnDblClick = edtfornecedorDblClick
      OnExit = edtfornecedorExit
      OnKeyDown = edtfornecedorKeyDown
      OnKeyPress = edtfornecedorKeyPress
    end
    object cbbcombotabeladepreco: TComboBox
      Left = 490
      Top = 112
      Width = 74
      Height = 21
      ItemHeight = 13
      TabOrder = 15
      Text = '   '
      Items.Strings = (
        'Instalado'
        'Fornecido'
        'Retirado')
    end
    object edtVALORICMS_SUBST_recolhido: TEdit
      Left = 279
      Top = 160
      Width = 71
      Height = 19
      MaxLength = 9
      TabOrder = 21
    end
    object edtBASECALCULOICMS_SUBST_recolhido: TEdit
      Left = 164
      Top = 160
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 20
    end
    object edtVALORICMS_SUBSTITUICAO: TEdit
      Left = 24
      Top = 160
      Width = 73
      Height = 19
      MaxLength = 9
      TabOrder = 19
    end
    object edtBASECALCULOICMS_SUBSTITUICAO: TEdit
      Left = 807
      Top = 113
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 18
    end
    object edtVALORICMS: TEdit
      Left = 711
      Top = 114
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 17
    end
    object edtVALORSEGURO: TEdit
      Left = 491
      Top = 158
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 23
    end
    object edtOUTRASDESPESAS: TEdit
      Left = 599
      Top = 159
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 24
    end
    object edtVALORTOTALIPI: TEdit
      Left = 711
      Top = 158
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 25
    end
    object edtdesconto: TEdit
      Left = 23
      Top = 202
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 26
    end
    object edtVALORTOTAL: TEdit
      Left = 165
      Top = 202
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 27
    end
    object edtVALORFINAL: TEdit
      Left = 279
      Top = 202
      Width = 71
      Height = 19
      MaxLength = 9
      TabOrder = 28
    end
    object rgRGTIPO: TRadioGroup
      Left = 805
      Top = 141
      Width = 75
      Height = 52
      Caption = 'Tipo'
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      Items.Strings = (
        'Entrada'
        'Sa'#237'da')
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 33
    end
    object lstLbCfops: TListBox
      Left = 816
      Top = 245
      Width = 90
      Height = 46
      ItemHeight = 13
      TabOrder = 37
      Visible = False
      OnKeyDown = lstLbCfopsKeyDown
    end
    object edtVALORCOFINS_ST: TEdit
      Left = 711
      Top = 202
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 32
    end
    object edtVALORCOFINS: TEdit
      Left = 601
      Top = 202
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 31
    end
    object edtVALORPIS_ST: TEdit
      Left = 492
      Top = 202
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 30
    end
    object mmoOBSERVACOES: TMemo
      Left = 425
      Top = 245
      Width = 379
      Height = 47
      Lines.Strings = (
        'memoOBSERVACOES')
      TabOrder = 35
    end
    object edtModeloNF: TEdit
      Left = 397
      Top = 65
      Width = 55
      Height = 19
      Color = 6073854
      MaxLength = 10
      TabOrder = 10
      OnDblClick = edtModeloNFDblClick
      OnKeyDown = edtModeloNFKeyDown
      OnKeyPress = edtNumeroKeyPress
    end
    object edtSerieNF: TEdit
      Left = 640
      Top = 28
      Width = 65
      Height = 19
      TabOrder = 6
    end
  end
end
