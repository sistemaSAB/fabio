unit UrelControleEntrega;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls;

type
  TQrControleEntrega = class(TQuickRep)
    Bandac: TQRBand;
    lBlbnomesuperior: TQRLabel;
    qrsysdtLbNumPag: TQRSysData;
    qrsysdt1: TQRSysData;
    qrshp1: TQRShape;
    BandaTITULO: TQRBand;
    qrshp2: TQRShape;
    lBNome: TQRLabel;
    lBEndereco: TQRLabel;
    lBContato: TQRLabel;
    lBObra: TQRLabel;
    qrshpQrlinhaSuperiorProposta: TQRShape;
    lBQrLbProposta: TQRLabel;
    lB1: TQRLabel;
    lB2: TQRLabel;
    lB3: TQRLabel;
    lB4: TQRLabel;
    lB5: TQRLabel;
    lB6: TQRLabel;
    lBLabel20: TQRLabel;
    lBTelefone: TQRLabel;
    lBCNPJCPF: TQRLabel;
    lBVendedor: TQRLabel;
    lBRGIE: TQRLabel;
    lB7: TQRLabel;
    imgLogotipo: TQRImage;
    lBCabecalhoLinha1: TQRLabel;
    lBCabecalhoLinha2: TQRLabel;
    lBCabecalhoLinha3: TQRLabel;
    lBCabecalhoLinha4: TQRLabel;
    lBProposta: TQRLabel;
    lBTitulo: TQRLabel;
    BandaDetail: TQRBand;
    imgmProjeto: TQRImage;
    lBReferenciaProjeto: TQRLabel;
    lBDescricao: TQRLabel;
    lBComplemento: TQRLabel;
    lBAltura: TQRLabel;
    lBLocal: TQRLabel;
    lBLargura: TQRLabel;
    qrshp3: TQRShape;
    qrshp4: TQRShape;
    lBITEMQTDEValor: TQRLabel;
    lB8: TQRLabel;
    lB9: TQRLabel;
    lBLado: TQRLabel;
    lBlado1: TQRLabel;
    QrStrBandSB: TQRStringsBand;
    lBDADOS: TQRLabel;
    lB10: TQRLabel;
    lBcomplemento1: TQRLabel;
    BandaBandSumario: TQRBand;
    lBNomeVendedorAssinatura1: TQRLabel;
    qrmObservacao: TQRMemo;
    qrshp5: TQRShape;
    lB12: TQRLabel;
    lBdata: TQRLabel;
    lBDataE: TQRLabel;
    lBDataEntrega: TQRLabel;
    lBObservacaoProjeto: TQRLabel;
    QRLabel1: TQRLabel;
    LBEmail: TQRLabel;
    QRLabel2: TQRLabel;
    lbComplementoPedido: TQRLabel;

    procedure LBDADOSPrint(sender: TObject; var Value: String);
    procedure BANDATITULOBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private

  public

  end;

var
  QrControleEntrega: TQrControleEntrega;

implementation

{$R *.DFM}


procedure TQrControleEntrega.LBDADOSPrint(sender: TObject; var Value: String);
var
apoio:string;
posicao:integer;
begin
     lbdados.font.Color:=clBLACK;
     LBDADOS.Font.Style:=[];
     LBDADOS.Font.size:=9;

     case QrStrBandSB.item[1] of
     '?':Begin//negrito
              LBDADOS.Font.Style:=[fsbold];
              value:=copy(QrStrBandsb.Item,2,length(QrStrBandsb.item));
         end;
     '�':Begin//cor

              apoio:=copy(QrStrBandsb.Item,2,length(QrStrBandsb.item));
              posicao:=pos('�',apoio);
              If (posicao=0)//tem que ter o fecho e entre eles a cor
              Then value:=QrStrBandsb.item
              Else Begin//s� 3 cores, vermelho,verde e azul
                        apoio:=copy(apoio,1,posicao-1);
                        if UPPERCASE(apoio)='VERMELHO'
                        tHEN lbdados.font.Color:=CLRED
                        eLSE
                                IF UPPERCASE(APOIO)='VERDE'
                                THEN lbdados.font.Color:=clGreen
                                ELSE
                                        IF UPPERCASE(APOIO)='AZUL'
                                        THEN lbdados.font.Color:=clBlue
                                        ELSE lbdados.font.Color:=clBLACK;


                        //pegando os dados fora a cor
                        value:=copy(QrStrBandsb.Item,posicao+2,length(QrStrBandsb.item));
              End;
         End;
     Else Begin
                value:=QrStrBandSb.item;
     End;

     end;


end;






procedure TQrControleEntrega.BANDATITULOBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

//ALTERADO POR RONNEI**********************************************************
{
testes feitos para posicionar a linha e a palavra proposta
e aumentar o tamanho da banda de acordo com o memo de pendencias

Banda titulo
244 para 1 linha
258 para 2 linhas
ou seja 14 por linha
----------------------------
linha proposta
207 para 1 linha
224 para 2 linhas
ou seja 17 por linha
----------------------------
palavra proposta
213 para 1 linha
230 para 2 linhas
ou seja 17 por linha
-----------------------------
}
//BANDATITULO.Height:=224+(QrMemoPendencias.Lines.count*14);
//QrlinhaSuperiorProposta.Top:=192+(QrMemoPendencias.Lines.count*17);
//QrLbProposta.Top:=198+(QrMemoPendencias.Lines.count*17);

//*****************************************************************

end;


end.
