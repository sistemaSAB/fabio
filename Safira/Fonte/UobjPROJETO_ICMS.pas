unit UobjPROJETO_ICMS;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UobjPROJETO
,UOBJTIPOCLIENTE
,UOBJIMPOSTO_ICMS
,GRIDS,uobjmaterial_ICMS
;
//USES_INTERFACE


Type
   TObjPROJETO_ICMS=class(TObjMaterial_ICMS)

          Public
               
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create(Owner:TComponent);override;
                Destructor  Free;override;
                Function    Salvar(ComCommit:Boolean)       :Boolean; override;
                Function    LocalizaCodigo(Parametro:String) :boolean; override;
                Function    Exclui(Pcodigo:String;ComCommit:boolean):Boolean;override;
                Function    Get_Pesquisa                    :TStringList;override;
                Function    Get_TituloPesquisa              :String; override;

                Function   TabelaparaObjeto:Boolean;override;
                Procedure   ZerarTabela;override;
                Procedure   Cancelar;override;
                Procedure   Commit;override;

                Function  Get_NovoCodigo:String;override;
                Function  RetornaCampoCodigo:String; override;
                Function  RetornaCampoNome:String;override;
                Procedure Imprime(Pcodigo:String);override;
                function  Localiza(Pestado,PtipoCliente,PcodigoMaterial,POperacao:String): boolean;override;
                procedure EdtMaterialKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);override;
                procedure RetornaImposto(PCodigo: String;STRGRID: TStringGrid);override;
                function  Get_CodigoMaterial: String; override;
                procedure Submit_CodigoMaterial(parametro: String);override;
                function  Get_NomeMaterial: String;override;
                function  ValidaPermissaoImpostos: boolean ; override;
                function  RetornaMateriais(PMaterialAtual:String): boolean; override;


                //CODIFICA DECLARA GETSESUBMITS

         Private
               InsertSql,DeleteSql,ModifySQl:TStringList;
               PROJETO:TObjPROJETO;


//CODIFICA VARIAVEIS PRIVADAS

                ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                function  verificaduplicidade: boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;





Function  TObjPROJETO_ICMS.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        result:=False;
        
        Self.ZerarTabela;

        if (inherited TabelaparaObjeto=false)
        Then exit;

        If(FieldByName('projeto').asstring<>'')
        Then Begin
                 If (Self.PROJETO.LocalizaCodigo(FieldByName('projeto').asstring)=False)
                 Then Begin
                          Messagedlg('ferragem N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PROJETO.TabelaparaObjeto;
        End;

        result:=True;
     End;
end;


Procedure TObjPROJETO_ICMS.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        inherited ObjetoparaTabela;

        ParamByName('projeto').asstring:=Self.PROJETO.GET_CODIGO;
  End;
End;

//***********************************************************************

function TObjPROJETO_ICMS.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;

  if(verificaduplicidade=False)
  then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 atualizaVersao(Get_CodigoMaterial);

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPROJETO_ICMS.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        inherited ZerarTabela;
        PROJETO.ZerarTabela;
     End;
end;

Function TObjPROJETO_ICMS.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
      Result:=False;
      mensagem:='';

      With Self do
      Begin
          mensagem:=inherited VerificaBrancos;

          if (PROJETO.Get_codigo='')
          Then mensagem:=mensagem+'/ferragem';
      End;

      if mensagem<>''
      Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
                messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
                exit;
      End;
      result:=true;
end;


function TObjPROJETO_ICMS.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:=inherited verificarelacionamentos;


     If (Self.PROJETO.LocalizaCodigo(Self.PROJETO.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ PROJETO n�o Encontrado!';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
     End;
     
     result:=true;
End;

function TObjPROJETO_ICMS.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:=inherited VerificaNUmericos;

     try
        If (Self.PROJETO.Get_Codigo<>'')
        Then Strtoint(Self.PROJETO.Get_Codigo);
     Except
           Mensagem:=mensagem+'/ferragem';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPROJETO_ICMS.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
      Result:=False;
     mensagem:=inherited VerificaData;

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPROJETO_ICMS.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
  Result:=False;
     With Self do
     Begin
          Mensagem:=Inherited VerificaFaixa;

          If mensagem<>''
          Then Begin
                   Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                   exit;
          End;
        result:=true;
     End;
end;

function TObjPROJETO_ICMS.LocalizaCodigo(parametro:String): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro projeto ICMS vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select '+Self.MaterialSelectSql.text+',projeto');
           SQL.ADD(' from  TABprojeto_ICMS');
           SQL.ADD(' WHERE codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPROJETO_ICMS.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjPROJETO_ICMS.Exclui(Pcodigo:String;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
End;


constructor TObjPROJETO_ICMS.create(Owner:TComponent);
begin
        Self.Owner := Owner;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;

        PROJETO:=TObjPROJETO.create;

        inherited;

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABPROJETO_ICMS('+MATERIALInsertSql.text+',PROJETO)');
                InsertSQL.add('values ('+MATERIALInsertSqlValues.Text+',:PROJETO)');

                ModifySQL.clear;
                ModifySQL.add('Update TABPROJETO_ICMS set PROJETO=:PROJETO,'+MATERIALModifySQl.Text);
                ModifySQL.add('where codigo=:codigo');

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABPROJETO_ICMS where codigo=:codigo ');

                Self.status          :=dsInactive;
        End;


end;

procedure TObjPROJETO_ICMS.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPROJETO_ICMS.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPROJETO_ICMS');
     Result:=Self.ParametroPesquisa;
end;

function TObjPROJETO_ICMS.Get_TituloPesquisa:String;
begin
     Result:=' Pesquisa de PROJETO_ICMS ';
end;


function TObjPROJETO_ICMS.Get_NovoCodigo:String;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPROJETO_ICMS,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENPROJETO_ICMS,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPROJETO_ICMS.Free;
begin
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.PROJETO.FREE;

    inherited;
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPROJETO_ICMS.RetornaCampoCodigo:String;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPROJETO_ICMS.RetornaCampoNome:String;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;


//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjPROJETO_ICMS.Imprime(Pcodigo:String);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPROJETO_ICMS';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;

function TObjPROJETO_ICMS.verificaduplicidade: boolean;
begin
     result:=False;
     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select * from tabprojeto_icms where tipocliente=:tipocliente and projeto=:projeto and estado=:estado and operacao=:operacao');
          ParamByName('tipocliente').asstring:=Self.tipocliente.Get_CODIGO;
          ParamByName('projeto').asstring:=Self.PROJETO.Get_Codigo;
          ParamByName('estado').asstring:=Self.ESTADO;
          ParamByName('operacao').asstring:=Self.operacao.Get_CODIGO;

          open;

          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;

          if (fieldbyname('codigo').asstring<>Self.CODIGO)//mesmo que seja insercao ou edicao tem q verificar
          Then Begin
                    MensagemErro('J� existe um registro para com estas informa��es (Estado, Tipo de Cliente, Opera��o e Produto). C�digo '+fieldbyname('codigo').asstring);
                    exit;
          End
          Else result:=True;
     End;
end;

function TObjPROJETO_ICMS.Localiza(Pestado,PtipoCliente,PcodigoMaterial,POperacao:String): boolean;//ok
begin

      //Um Caso a Parte na OO, pois nao compensava fazer uma funcao soh pra preencher 2 parametros
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select * from TABPROJETO_ICMS');
           SQL.ADD(' WHERE PROJETO=:PROJETO and Estado=:Estado and TipoCliente=:TipoCliente and operacao=:operacao');

           parambyname('PROJETO').asstring:=pcodigomaterial;
           parambyname('estado').asstring:=pestado;
           parambyname('tipocliente').asstring:=ptipocliente;
           parambyname('operacao').asstring:=poperacao;

           
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPROJETO_ICMS.EdtMaterialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PROJETO.Get_Pesquisa,Self.PROJETO.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PROJETO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.PROJETO.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PROJETO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPROJETO_ICMS.RetornaImposto(PCodigo: String;
  STRGRID: TStringGrid);
var
cont,cont2:integer;
begin
     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select tabPROJETO_icms.Estado,tabtipocliente.nome as TipoCliente,');
          sql.add('tabPROJETO_icms.codigo,tabPROJETO_icms.imposto,tabPROJETO_icms.imposto_ipi,tabPROJETO_icms.imposto_pis,tabPROJETO_icms.imposto_cofins,taboperacaonf.nome as operacao');
          sql.add('from TabPROJETO_ICMS join tabtipocliente on tabtipocliente.codigo=tabPROJETO_icms.tipocliente');
          sql.add('join tabimposto_icms on tabPROJETO_icms.imposto=tabimposto_icms.codigo');
          sql.add('join taboperacaonf on tabPROJETO_icms.operacao=taboperacaonf.codigo');
          sql.add('Where TabPROJETO_ICMS.PROJETO='+Pcodigo);
          sql.add('order by TabPROJETO_ICMS.Estado');
          open;
          last;
          STRGRID.RowCount:=1;
          STRGRID.ColCount:=1;
          STRGRID.Cols[0].clear;

          if (recordcount=0)
          Then exit;
          first;

          STRGRID.ColCount:=fields.Count;
          STRGRID.RowCount:=RecordCount+1;
          STRGRID.FixedRows:=1;


          for cont:=0 to Fields.count -1 do
          Begin
               STRGRID.Cells[cont,0]:=Fields[cont].DisplayName;
          End;

          cont2:=1;
          While not(eof) do
          Begin
               for cont:=0 to Fields.count -1 do
               Begin
                   STRGRID.Cells[cont,cont2]:=Fields[cont].AsString;
               End;
               cont2:=cont2+1;
               next;
          End;
          AjustaLArguraColunaGrid(STRGRID);
     End;
end;

function TObjPROJETO_ICMS.Get_CodigoMaterial: String;
begin
     Result:=Self.PROJETO.Get_Codigo;
end;

procedure TObjPROJETO_ICMS.Submit_CodigoMaterial(parametro: String);
begin
     Self.PROJETO.Submit_Codigo(parametro);
end;

function TObjPROJETO_ICMS.Get_NomeMaterial: String;
begin
     Result:=Self.PROJETO.Get_Descricao;
end;

function TObjPROJETO_ICMS.ValidaPermissaoImpostos: boolean;
begin
   result:=ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('ALTERAR IMPOSTOS DO PROJETO');
end;

function TObjPROJETO_ICMS.RetornaMateriais(PMaterialAtual:String): boolean;//ok
begin

      //Um Caso a Parte na Or.Obj. pois nao compensava fazer uma funcao soh pra preencher 2 parametros
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select');
           SQL.ADD('tabPROJETO.codigo,');
           SQL.ADD('tabPROJETO.descricao,');
           SQL.ADD('tabPROJETO.referencia,');
           SQL.ADD('tabPROJETO.grupoVIDRO as GRUPO,');
           SQL.ADD('tabgrupoVIDRO.nome as NOMEGRUPO');
           SQL.ADD('from tabPROJETO');
           SQL.ADD('join tabgrupoVIDRO on tabPROJETO.grupoVIDRO=tabgrupoVIDRO.codigo');
           SQL.ADD('where tabPROJETO.codigo<>:pcodigo');
           SQL.ADD('order by tabPROJETO.grupoVIDRO,tabPROJETO.descricao');
           ParamByName('PCODIGO').AsString:=PMaterialAtual;
           Open;
       End;
end;


end.



