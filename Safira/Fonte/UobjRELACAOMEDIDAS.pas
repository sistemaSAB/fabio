unit UobjRELACAOMEDIDAS;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
;
//USES_INTERFACE


Type
   TObjRELACAOMEDIDAS=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_TIPOMATERIAL(parametro: string);
                Function Get_TIPOMATERIAL: string;
                Procedure Submit_UNIDADEMEDIDAORIGEM(parametro: string);
                Function Get_UNIDADEMEDIDAORIGEM: string;
                Procedure Submit_UNIDADEMEDIDACONVERSAO(parametro: string);
                Function Get_UNIDADEMEDIDACONVERSAO: string;
                Procedure Submit_USERC(parametro: string);
                Function Get_USERC: string;
                Procedure Submit_USERM(parametro: string);
                Function Get_USERM: string;
                Procedure Submit_VALOR(parametro: string);
                Function Get_VALOR: string;
                Procedure Submit_DATAC(parametro: string);
                Function Get_DATAC: string;
                Procedure Submit_DATAM(parametro: string);
                Function Get_DATAM: string;
                function RetornaEquivalencia(UnidadeEntrada,UnidadeCadastro,Material:string):Boolean;
                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               TIPOMATERIAL:string;
               UNIDADEMEDIDAORIGEM:string;
               UNIDADEMEDIDACONVERSAO:string;
               USERC:string;
               USERM:string;
               VALOR:string;
               DATAC:string;
               DATAM:string;
//CODIFICA VARIAVEIS PRIVADAS
               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;





Function  TObjRELACAOMEDIDAS.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.TIPOMATERIAL:=fieldbyname('TIPOMATERIAL').asstring;
        Self.UNIDADEMEDIDAORIGEM:=fieldbyname('UNIDADEMEDIDAORIGEM').asstring;
        Self.UNIDADEMEDIDACONVERSAO:=fieldbyname('UNIDADEMEDIDACONVERSAO').asstring;
        Self.VALOR:=fieldbyname('VALOR').asstring;

//CODIFICA TABELAPARAOBJETO
        result:=True;
     End;
end;


Procedure TObjRELACAOMEDIDAS.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('TIPOMATERIAL').asstring:=Self.TIPOMATERIAL;
        ParamByName('UNIDADEMEDIDAORIGEM').asstring:=Self.UNIDADEMEDIDAORIGEM;
        ParamByName('UNIDADEMEDIDACONVERSAO').asstring:=Self.UNIDADEMEDIDACONVERSAO;
        ParamByName('VALOR').asstring:=virgulaparaponto(Self.VALOR);
  End;
End;

//***********************************************************************

function TObjRELACAOMEDIDAS.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  {if (Self.VerificaData=False)
  Then Exit; }

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjRELACAOMEDIDAS.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        TIPOMATERIAL:='';
        UNIDADEMEDIDAORIGEM:='';
        UNIDADEMEDIDACONVERSAO:='';
        USERC:='';
        USERM:='';
        VALOR:='';
        DATAC:='';
        DATAM:='';
//CODIFICA ZERARTABELA
     End;
end;

Function TObjRELACAOMEDIDAS.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjRELACAOMEDIDAS.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjRELACAOMEDIDAS.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
     try
        Strtoint(Self.UNIDADEMEDIDAORIGEM);
     Except
           Mensagem:=mensagem+'/UNIDADEMEDIDAORIGEM';
     End;
     try
        Strtoint(Self.UNIDADEMEDIDACONVERSAO);
     Except
           Mensagem:=mensagem+'/UNIDADEMEDIDACONVERSAO';
     End;
     try
        Strtofloat(Self.VALOR);
     Except
           Mensagem:=mensagem+'/VALOR';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjRELACAOMEDIDAS.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodatetime(Self.DATAC);
     Except
           Mensagem:=mensagem+'/DATAC';
     End;
     try
        Strtodatetime(Self.DATAM);
     Except
           Mensagem:=mensagem+'/DATAM';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjRELACAOMEDIDAS.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjRELACAOMEDIDAS.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro RELACAOMEDIDAS vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,TIPOMATERIAL,UNIDADEMEDIDAORIGEM,UNIDADEMEDIDACONVERSAO');
           SQL.ADD(' ,USERC,USERM,VALOR,DATAC,DATAM');
           SQL.ADD(' from  TABRELACAOMEDIDAS');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjRELACAOMEDIDAS.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjRELACAOMEDIDAS.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjRELACAOMEDIDAS.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjRELACAOMEDIDAS.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABRELACAOMEDIDAS(CODIGO,TIPOMATERIAL,UNIDADEMEDIDAORIGEM');
                InsertSQL.add(' ,UNIDADEMEDIDACONVERSAO,VALOR');
                InsertSQL.add(' )');
                InsertSQL.add('values (:CODIGO,:TIPOMATERIAL,:UNIDADEMEDIDAORIGEM,:UNIDADEMEDIDACONVERSAO,');
                InsertSQL.add(':VALOR)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABRELACAOMEDIDAS set CODIGO=:CODIGO,TIPOMATERIAL=:TIPOMATERIAL');
                ModifySQL.add(',UNIDADEMEDIDAORIGEM=:UNIDADEMEDIDAORIGEM,UNIDADEMEDIDACONVERSAO=:UNIDADEMEDIDACONVERSAO,VALOR=:VALOR');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABRELACAOMEDIDAS where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjRELACAOMEDIDAS.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjRELACAOMEDIDAS.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabRELACAOMEDIDAS');
     Result:=Self.ParametroPesquisa;
end;

function TObjRELACAOMEDIDAS.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de RELACAOMEDIDAS ';
end;


function TObjRELACAOMEDIDAS.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENRELACAOMEDIDAS,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENRELACAOMEDIDAS,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjRELACAOMEDIDAS.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjRELACAOMEDIDAS.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjRELACAOMEDIDAS.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjRELACAOMEDIDAS.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjRELACAOMEDIDAS.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjRELACAOMEDIDAS.Submit_TIPOMATERIAL(parametro: string);
begin
        Self.TIPOMATERIAL:=Parametro;
end;
function TObjRELACAOMEDIDAS.Get_TIPOMATERIAL: string;
begin
        Result:=Self.TIPOMATERIAL;
end;
procedure TObjRELACAOMEDIDAS.Submit_UNIDADEMEDIDAORIGEM(parametro: string);
begin
        Self.UNIDADEMEDIDAORIGEM:=Parametro;
end;
function TObjRELACAOMEDIDAS.Get_UNIDADEMEDIDAORIGEM: string;
begin
        Result:=Self.UNIDADEMEDIDAORIGEM;
end;
procedure TObjRELACAOMEDIDAS.Submit_UNIDADEMEDIDACONVERSAO(parametro: string);
begin
        Self.UNIDADEMEDIDACONVERSAO:=Parametro;
end;
function TObjRELACAOMEDIDAS.Get_UNIDADEMEDIDACONVERSAO: string;
begin
        Result:=Self.UNIDADEMEDIDACONVERSAO;
end;
procedure TObjRELACAOMEDIDAS.Submit_USERC(parametro: string);
begin
        Self.USERC:=Parametro;
end;
function TObjRELACAOMEDIDAS.Get_USERC: string;
begin
        Result:=Self.USERC;
end;
procedure TObjRELACAOMEDIDAS.Submit_USERM(parametro: string);
begin
        Self.USERM:=Parametro;
end;
function TObjRELACAOMEDIDAS.Get_USERM: string;
begin
        Result:=Self.USERM;
end;
procedure TObjRELACAOMEDIDAS.Submit_VALOR(parametro: string);
begin
        Self.VALOR:=Parametro;
end;
function TObjRELACAOMEDIDAS.Get_VALOR: string;
begin
        Result:=Self.VALOR;
end;
procedure TObjRELACAOMEDIDAS.Submit_DATAC(parametro: string);
begin
        Self.DATAC:=Parametro;
end;
function TObjRELACAOMEDIDAS.Get_DATAC: string;
begin
        Result:=Self.DATAC;
end;
procedure TObjRELACAOMEDIDAS.Submit_DATAM(parametro: string);
begin
        Self.DATAM:=Parametro;
end;
function TObjRELACAOMEDIDAS.Get_DATAM: string;
begin
        Result:=Self.DATAM;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjRELACAOMEDIDAS.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJRELACAOMEDIDAS';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;

function TObjRELACAOMEDIDAS.RetornaEquivalencia(UnidadeEntrada,UnidadeCadastro,Material:string):Boolean;
begin
   With Self.Objquery do
   Begin
      close;
      Sql.Clear;
      SQL.ADD('Select CODIGO,TIPOMATERIAL,UNIDADEMEDIDAORIGEM,UNIDADEMEDIDACONVERSAO');
      SQL.ADD(',VALOR');
      SQL.ADD(' from  TABRELACAOMEDIDAS');
      SQL.ADD(' WHERE UNIDADEMEDIDAORIGEM='+UnidadeEntrada);
      sql.Add(' AND UNIDADEMEDIDACONVERSAO='+UnidadeCadastro);
      sql.Add(' AND TIPOMATERIAL='+#39+Material+#39);
      Open;

      If (recordcount>0)
      Then Result:=True
      Else Result:=False;
   End;
end;

end.




