unit UobjORDEMSERVICO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,UobjCLIENTE,UobjVENDEDOR;

Type
   TObjORDEMSERVICO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                ObjCLIENTE                                  :TObjCLIENTE;
                ObjVENDEDOR                                 :TObjVENDEDOR;

//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_DESCRICAO(parametro: string);
                Function Get_DESCRICAO: string;
                Procedure Submit_CLIENTE(parametro: string);
                Function Get_CLIENTE: string;
                Procedure Submit_VENDEDOR(parametro: string);
                Function Get_VENDEDOR: string;
                Procedure Submit_OBSERVACAO(parametro: string);
                Function Get_OBSERVACAO: string;
                Procedure Submit_VALOR(parametro: string);
                Function Get_VALOR: string;
                Procedure Submit_DESCONTO(parametro: string);
                Function Get_DESCONTO: string;
                Procedure Submit_ACRESCIMO(parametro: string);
                Function Get_ACRESCIMO: string;
                Procedure Submit_VALORFINAL(parametro: string);
                Function Get_VALORFINAL: string;
                Procedure Submit_DATAC(parametro: string);
                Function Get_DATAC: string;
                Procedure Submit_DATAM(parametro: string);
                Function Get_DATAM: string;
                Procedure Submit_USERC(parametro: string);
                Function Get_USERC: string;
                Procedure Submit_USERM(parametro: string);
                Function Get_USERM: string;
                Procedure Submit_CONCLUIDO(parametro: string);
                Function Get_CONCLUIDO: string;
                Procedure Submit_DATAOS(parametro: string);
                Function Get_DATAOS: string;
                procedure Submit_Pedido(parametro:string);
                function Get_Pedido:string;
                function LocalizaPedido(pedido:string):Boolean;
                function VerificaOSConcluida(OrdemServ:string):Boolean;
                procedure BotaoOpcoes(pcodigo: string);
                procedure GeraOrdemCorte(parametro:string);
                procedure ImprimeServicoPorVidro(pcodigo:string);
                procedure ImprimeCodigosBarras(pcodigo:string;pcodigovidro:string);
                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               DESCRICAO:string;
               CLIENTE:string;
               VENDEDOR:string;
               OBSERVACAO:string;
               VALOR:string;
               DESCONTO:string;
               ACRESCIMO:string;
               VALORFINAL:string;
               DATAC:string;
               DATAM:string;
               USERC:string;
               USERM:string;
               CONCLUIDO:string;
               DATAOS:string;
               PEDIDO:string;
//CODIFICA VARIAVEIS PRIVADAS

















               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls
//USES IMPLEMENTATION
, UMenuRelatorios, UobjRELPERSREPORTBUILDER, UReltxtRDPRINT,rdprint;


{ TTabTitulo }


Function  TObjORDEMSERVICO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.DESCRICAO:=fieldbyname('DESCRICAO').asstring;
        Self.CLIENTE:=fieldbyname('CLIENTE').asstring;
        Self.VENDEDOR:=fieldbyname('VENDEDOR').asstring;
        Self.OBSERVACAO:=fieldbyname('OBSERVACAO').asstring;
        Self.VALOR:=fieldbyname('VALOR').asstring;
        Self.DESCONTO:=fieldbyname('DESCONTO').asstring;
        Self.ACRESCIMO:=fieldbyname('ACRESCIMO').asstring;
        Self.VALORFINAL:=fieldbyname('VALORFINAL').asstring;
        Self.DATAC:=fieldbyname('DATAC').asstring;
        Self.DATAM:=fieldbyname('DATAM').asstring;
        Self.USERC:=fieldbyname('USERC').asstring;
        Self.USERM:=fieldbyname('USERM').asstring;
        Self.CONCLUIDO:=fieldbyname('CONCLUIDO').asstring;
        Self.DATAOS:=fieldbyname('DATAOS').asstring;
        self.PEDIDO:=fieldbyname('pedido').AsString;
//CODIFICA TABELAPARAOBJETO

        result:=True;
     End;
end;


Procedure TObjORDEMSERVICO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('DESCRICAO').asstring:=Self.DESCRICAO;
        ParamByName('CLIENTE').asstring:=Self.CLIENTE;
        ParamByName('VENDEDOR').asstring:=Self.VENDEDOR;
        ParamByName('OBSERVACAO').asstring:=Self.OBSERVACAO;
        ParamByName('VALOR').asstring:=virgulaparaponto(Self.VALOR);
        ParamByName('DESCONTO').asstring:=virgulaparaponto(Self.DESCONTO);
        ParamByName('ACRESCIMO').asstring:=virgulaparaponto(Self.ACRESCIMO);
        ParamByName('VALORFINAL').asstring:=virgulaparaponto(Self.VALORFINAL);
        ParamByName('CONCLUIDO').asstring:=Self.CONCLUIDO;
        ParamByName('DATAOS').asstring:=Self.DATAOS;
        ParamByName('pedido').AsString:=self.PEDIDO
//CODIFICA OBJETOPARATABELA

  End;
End;

//***********************************************************************

function TObjORDEMSERVICO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  {if (Self.VerificaNumericos=False)
  Then Exit; }

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;

   Self.ObjetoParaTabela;
   Try
      Self.Objquery.ExecSQL;
   Except
         if (Self.Status=dsInsert)
         Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
         Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
         exit;
   End;

   If ComCommit=True
   Then FDataModulo.IBTransaction.CommitRetaining;

   Self.status          :=dsInactive;
   result:=True;
end;

procedure TObjORDEMSERVICO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        DESCRICAO:='';
        CLIENTE:='';
        VENDEDOR:='';
        OBSERVACAO:='';
        VALOR:='';
        DESCONTO:='';
        ACRESCIMO:='';
        VALORFINAL:='';
        DATAC:='';
        DATAM:='';
        USERC:='';
        USERM:='';
        CONCLUIDO:='';
        DATAOS:='';
        PEDIDO:='';
//CODIFICA ZERARTABELA




     End;
end;

{Rodolfo}
Function TObjORDEMSERVICO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/ C�digo ';

      if(CLIENTE = '' )
      then Mensagem := Mensagem + '/ Cliente ' ;

      if(VENDEDOR = '' )
      then Mensagem:=mensagem+'/ Vendedor ';


  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjORDEMSERVICO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjORDEMSERVICO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        Strtoint(Self.CLIENTE);
     Except
           Mensagem:=mensagem+'/CLIENTE';
     End;
     try
        Strtoint(Self.VENDEDOR);
     Except
           Mensagem:=mensagem+'/VENDEDOR';
     End;
     try
        Strtofloat(Self.VALOR);
     Except
           Mensagem:=mensagem+'/VALOR';
     End;
     try
        Strtofloat(Self.DESCONTO);
     Except
           Mensagem:=mensagem+'/DESCONTO';
     End;
     try
        Strtofloat(Self.ACRESCIMO);
     Except
           Mensagem:=mensagem+'/ACRESCIMO';
     End;
     try
        Strtofloat(Self.VALORFINAL);
     Except
           Mensagem:=mensagem+'/VALORFINAL';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjORDEMSERVICO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.DATAOS);
     Except
           Mensagem:=mensagem+'/DATAOS';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjORDEMSERVICO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjORDEMSERVICO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro ORDEMSERVICO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,DESCRICAO,CLIENTE,VENDEDOR,OBSERVACAO,VALOR,DESCONTO');
           SQL.ADD(' ,ACRESCIMO,VALORFINAL,DATAC,DATAM,USERC,USERM,CONCLUIDO,DATAOS,pedido');
           SQL.ADD(' ');
           SQL.ADD(' from  TABORDEMSERVICO');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjORDEMSERVICO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjORDEMSERVICO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
var
  QueryExcluiDependencias:TIBQuery;
begin
   try
     QueryExcluiDependencias:=TIBQuery.Create(nil);
     QueryExcluiDependencias.Database:=FDataModulo.IBDatabase;
     with QueryExcluiDependencias do
     begin
           Close;
           SQL.Clear;
           SQL.Add('delete from tabvidroservicoos where ordemdeservico='+Pcodigo);
           ExecSQL;

           Close;
           SQL.Clear;
           SQL.Add('delete from tabmateriaisos where ordemservico='+Pcodigo);
           ExecSQL;

           Close;
           SQL.Clear;
           SQL.Add('delete from tabservicosos where ordemservico='+Pcodigo);
           ExecSQL;  

     end;
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
   finally
         FreeAndNil(QueryExcluiDependencias);
   end;
end;


constructor TObjORDEMSERVICO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        ObjCLIENTE:=TObjCLIENTE.Create;
        ObjVENDEDOR:=TObjVENDEDOR.Create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABORDEMSERVICO(CODIGO,DESCRICAO,CLIENTE');
                InsertSQL.add(' ,VENDEDOR,OBSERVACAO,VALOR,DESCONTO,ACRESCIMO,VALORFINAL');
                InsertSQL.add(' ,CONCLUIDO,DATAOS,PEDIDO)');
                InsertSQL.add('values (:CODIGO,:DESCRICAO,:CLIENTE,:VENDEDOR,:OBSERVACAO');
                InsertSQL.add(' ,:VALOR,:DESCONTO,:ACRESCIMO,:VALORFINAL');
                InsertSQL.add(' ,:CONCLUIDO,:DATAOS,:PEDIDO)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABORDEMSERVICO set CODIGO=:CODIGO,DESCRICAO=:DESCRICAO');
                ModifySQL.add(',CLIENTE=:CLIENTE,VENDEDOR=:VENDEDOR,OBSERVACAO=:OBSERVACAO');
                ModifySQL.add(',VALOR=:VALOR,DESCONTO=:DESCONTO,ACRESCIMO=:ACRESCIMO');
                ModifySQL.add(',VALORFINAL=:VALORFINAL');
                ModifySQL.add(',CONCLUIDO=:CONCLUIDO,DATAOS=:DATAOS,PEDIDO=:PEDIDO');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABORDEMSERVICO where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjORDEMSERVICO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjORDEMSERVICO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabORDEMSERVICO');
     Result:=Self.ParametroPesquisa;
end;

function TObjORDEMSERVICO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de ORDEMSERVICO ';
end;


function TObjORDEMSERVICO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENORDEMSERVICO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENORDEMSERVICO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjORDEMSERVICO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    ObjCLIENTE.Free;
    ObjVENDEDOR.Free;
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjORDEMSERVICO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjORDEMSERVICO.RetornaCampoNome: string;
begin
      result:='DESCRICAO';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjORDEMSERVICO.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjORDEMSERVICO.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjORDEMSERVICO.Submit_DESCRICAO(parametro: string);
begin
        Self.DESCRICAO:=Parametro;
end;
function TObjORDEMSERVICO.Get_DESCRICAO: string;
begin
        Result:=Self.DESCRICAO;
end;
procedure TObjORDEMSERVICO.Submit_CLIENTE(parametro: string);
begin
        Self.CLIENTE:=Parametro;
end;
function TObjORDEMSERVICO.Get_CLIENTE: string;
begin
        Result:=Self.CLIENTE;
end;
procedure TObjORDEMSERVICO.Submit_VENDEDOR(parametro: string);
begin
        Self.VENDEDOR:=Parametro;
end;
function TObjORDEMSERVICO.Get_VENDEDOR: string;
begin
        Result:=Self.VENDEDOR;
end;
procedure TObjORDEMSERVICO.Submit_OBSERVACAO(parametro: string);
begin
        Self.OBSERVACAO:=Parametro;
end;
function TObjORDEMSERVICO.Get_OBSERVACAO: string;
begin
        Result:=Self.OBSERVACAO;
end;
procedure TObjORDEMSERVICO.Submit_VALOR(parametro: string);
begin
        Self.VALOR:=Parametro;
end;
function TObjORDEMSERVICO.Get_VALOR: string;
begin
        Result:=Self.VALOR;
end;
procedure TObjORDEMSERVICO.Submit_DESCONTO(parametro: string);
begin
        Self.DESCONTO:=Parametro;
end;
function TObjORDEMSERVICO.Get_DESCONTO: string;
begin
        Result:=Self.DESCONTO;
end;
procedure TObjORDEMSERVICO.Submit_ACRESCIMO(parametro: string);
begin
        Self.ACRESCIMO:=Parametro;
end;
function TObjORDEMSERVICO.Get_ACRESCIMO: string;
begin
        Result:=Self.ACRESCIMO;
end;
procedure TObjORDEMSERVICO.Submit_VALORFINAL(parametro: string);
begin
        Self.VALORFINAL:=Parametro;
end;
function TObjORDEMSERVICO.Get_VALORFINAL: string;
begin
        Result:=Self.VALORFINAL;
end;
procedure TObjORDEMSERVICO.Submit_DATAC(parametro: string);
begin
        Self.DATAC:=Parametro;
end;
function TObjORDEMSERVICO.Get_DATAC: string;
begin
        Result:=Self.DATAC;
end;
procedure TObjORDEMSERVICO.Submit_DATAM(parametro: string);
begin
        Self.DATAM:=Parametro;
end;
function TObjORDEMSERVICO.Get_DATAM: string;
begin
        Result:=Self.DATAM;
end;
procedure TObjORDEMSERVICO.Submit_USERC(parametro: string);
begin
        Self.USERC:=Parametro;
end;
function TObjORDEMSERVICO.Get_USERC: string;
begin
        Result:=Self.USERC;
end;
procedure TObjORDEMSERVICO.Submit_USERM(parametro: string);
begin
        Self.USERM:=Parametro;
end;
function TObjORDEMSERVICO.Get_USERM: string;
begin
        Result:=Self.USERM;
end;
procedure TObjORDEMSERVICO.Submit_CONCLUIDO(parametro: string);
begin
        Self.CONCLUIDO:=Parametro;
end;
function TObjORDEMSERVICO.Get_CONCLUIDO: string;
begin
        Result:=Self.CONCLUIDO;
end;
procedure TObjORDEMSERVICO.Submit_DATAOS(parametro: string);
begin
        Self.DATAOS:=Parametro;
end;
function TObjORDEMSERVICO.Get_DATAOS: string;
begin
        Result:=Self.DATAOS;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjORDEMSERVICO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJORDEMSERVICO';

          With RgOpcoes do
          Begin
                items.clear;
                Items.add('Relatorio de Servi�os por Vidro');
                Items.add('Imprimir Codigo de Barras dessa Ordem de Produ��o');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;
          Case RgOpcoes.ItemIndex of
               0:Self.ImprimeServicoPorVidro(pcodigo);
               1:self.ImprimeCodigosBarras(Pcodigo,'');
          End;


     end;

end;

procedure TObjORDEMSERVICO.Submit_Pedido(parametro:string);
begin
    self.PEDIDO:=parametro;
end;

function TObjORDEMSERVICO.Get_Pedido:string;
begin
    result:=self.PEDIDO;
end;

function TObjORDEMSERVICO.LocalizaPedido(pedido:string):Boolean;
var
    QueryPesQ:TIBQuery;
begin
      Result:=False;
      try
            QueryPesQ:=TIBQuery.Create(nil);
            QueryPesQ.Database:=FDataModulo.IBDatabase;
      except
      end;

      try
            with QueryPesQ do
            begin
                 Close;
                 sql.Clear;
                 sql.Add('select * from tabordemservico where pedido='+pedido);
                 Open;
                 Last;
                 if(recordcount>0)
                 then Result:=True;

            end;
      finally
            FreeAndNil(QueryPesQ);
      end;



end;

function TObjORDEMSERVICO.VerificaOSConcluida(ordemserv:string):Boolean;
var
  Query:TIBQuery;
begin
     result:=False;
     Query:=TIBQuery.Create(nil);
     Query.Database:=FDataModulo.IBDatabase;
     try
            with Query do
            begin
                 Close;
                 sql.Clear;
                 sql.Add('select concluido from tabvidroservicoos where concluido=''N''');
                 sql.Add('and ORDEMDESERVICO='+OrdemServ) ;
                 Open;
                 Last;
                 if(recordcount=0)
                 then result:=True;

            end;
     finally
       FreeAndNil(Query);
     end;

end;

procedure TObjORDEMSERVICO.BotaoOpcoes(pcodigo: string);
begin

     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Gerar Ordem de Corte');

          showmodal;
          if (tag=0)
          Then exit;
          Case RgOpcoes.ItemIndex of
               0:Self.GeraOrdemCorte(pcodigo);
          End;
     End;
end;

procedure TObjORDEMSERVICO.GeraOrdemCorte(parametro:string);
var
  ObjRelPersReportBuilder:TObjRELPERSREPORTBUILDER;
begin
      if(parametro='')
      then Exit;
      try

            try
                   ObjRelPersreportBuilder:=TObjRELPERSREPORTBUILDER.Create;
            except
                   MensagemErro('Erro ao tentar criar o Objeto ObjRelPersreportBuilder');
                   exit;
            end;

            if (ObjRelPersReportBuilder.LocalizaNOme('ORDEM DE CORTE') = false)
            then Begin
                       MensagemErro('O Relatorio de ReporteBuilder "ORDEM DE CORTE" n�o foi econtrado');
                       exit;
            end;

            ObjRelPersReportBuilder.TabelaparaObjeto;
            ObjRelPersReportBuilder.SQLCabecalhoPreenchido:=StrReplace(ObjRelPersReportBuilder.Get_SQLCabecalho,':PCODIGO',parametro);
            ObjRelPersReportBuilder.SQLRepeticaoPreenchido:=StrReplace(ObjRelPersReportBuilder.get_SQLRepeticao,':PCODIGO',parametro);
            ObjRelPersReportBuilder.SQLRepeticao_2Preenchido:=StrReplace(ObjRelPersReportBuilder.Get_SQLRepeticao_2,':PCODIGO',parametro);
            ObjRelPersReportBuilder.ChamaRelatorio(false);


      finally
            ObjRelPersreportBuilder.Free;
      end;

end;

procedure TObjORDEMSERVICO.ImprimeServicoPorVidro(pcodigo:string);
var
  Query:TIBQuery;
  Query2:TIBQuery;
  CodigoVidro:string;
  status:string;
  statuspesquisa:string;
begin
     With FfiltroImp do
     Begin
         Query:=TIBQuery.Create(nil);
         Query.Database:=FDataModulo.IBDatabase;
         Query2:=TIBQuery.Create(nil);
         Query2.Database:=FDataModulo.IBDatabase;

         try

                  CodigoVidro:='';
                  DesativaGrupos;
                  Grupo01.Enabled:=true;
                  LbGrupo01.caption:='Ordem de Servi�o';
                  if(pcodigo<>'')
                  then  edtgrupo01.Text:=pcodigo;
                  Grupo06.Enabled:=True;
                  LbGrupo06.Caption:='Status do servi�o';
                  ComboGrupo06.Items.Clear;
                  ComboGrupo06.Items.Add('ABERTO') ;
                  ComboGrupo06.Items.Add('FECHADO');

                  edtgrupo01.color:=$005CADFE;

                  ShowModal;

                  if(Tag=0)
                  then Exit;

                  if(ComboGrupo06.Text = 'ABERTO')
                  then statuspesquisa:='N';
                  if(ComboGrupo06.Text = 'FECHADO')
                  then statuspesquisa:='S';
                  if(ComboGrupo06.Text='')
                  then statuspesquisa:='';

                  pcodigo:=edtgrupo01.Text;

                  with Query do
                  begin
                        Close;
                        SQL.Clear;
                        SQL.Add('SELECT Distinct (mos.codigo),mos.vidro, vidro.referencia, vidro.descricao as nomevidro,os.codigo as OS, os.descricao as OSNOME');
                        sql.Add(',cliente.nome as cliente,vendedor.nome as vendedor');
                        SQL.Add('from tabvidroservicoos vsos');
                        SQL.Add('join tabordemservico os on os.codigo=vsos.ordemdeservico');
                        SQL.Add('join tabmateriaisos mos on mos.codigo=vsos.vidroos');
                        SQL.Add('join tabvidro Vidro on Vidro.codigo=mos.vidro');
                        SQL.Add('join tabservicosos sos  on sos.codigo=vsos.servicoos');
                        sql.Add('join tabcliente cliente on cliente.codigo=os.cliente');
                        SQL.Add('join tabvendedor vendedor on vendedor.codigo=os.vendedor');
                        SQL.Add('where vsos.ordemdeservico='+pcodigo);
                        Open;
                        FreltxtRDPRINT.ConfiguraImpressao;
                        FreltxtRDPRINT.LinhaLocal:=3;
                        FreltxtRDPRINT.RDprint.Abrir;
                        if (FreltxtRDPRINT.RDprint.Setup=False) then
                        begin
                              FreltxtRDPRINT.rdprint.Fechar;
                              exit;
                        end;
                        FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,1,fieldbyname('OS').AsString,[negrito]);
                        FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,10,fieldbyname('OSNOME').AsString,[negrito]);
                        FreltxtRDPRINT.incrementalinha(1);
                        FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,1,'Vendedor : ',[negrito]);
                        FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,fieldbyname('vendedor').AsString,[negrito]);
                        FreltxtRDPRINT.incrementalinha(1);
                        FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,1,'Cliente : ',[negrito]);
                        FreltxtRDPRINT.rdprint.ImpF(FreltxtRDPRINT.linhalocal,11,fieldbyname('cliente').AsString,[negrito]);
                        FreltxtRDPRINT.incrementalinha(2);
                        FreltxtRDPRINT.rdprint.ImpC(FreltxtRDPRINT.linhalocal,45,'RELA��O DE SERVI�OS POR VIDROS',[negrito]);
                        FreltxtRDPRINT.incrementalinha(2);
                        while not Eof do
                        begin
                                CodigoVidro:=fieldbyname('codigo').AsString;
                                FreltxtRDPRINT.RDprint.ImpC(FreltxtRDPRINT.linhalocal,1,CompletaPalavra(Fieldbyname('referencia').asstring,8,' ')+' '+
                                              CompletaPalavra(Fieldbyname('nomevidro').asstring,50,' '),[negrito]);
                                FreltxtRDPRINT.incrementalinha(1);
                                Query2.close;
                                Query2.sql.Clear;
                                Query2.sql.Add('SELECT os.codigo as CodigoOrdemServico, os.descricao as NomeOrdemServico, mos.vidro, vidro.referencia, vidro.descricao as nomevidro,') ;
                                Query2.sql.Add('sos.servico,sos.descricao as nomeservico, SOS.funcionario,func.nome as NomeFuncionario, vsos.concluido');
                                Query2.sql.Add('from tabvidroservicoos vsos');
                                Query2.sql.Add('join tabordemservico os on os.codigo=vsos.ordemdeservico');
                                Query2.sql.Add('join tabmateriaisos mos on mos.codigo=vsos.vidroos');
                                Query2.sql.Add('join tabvidro Vidro on Vidro.codigo=mos.vidro');
                                Query2.sql.Add('join tabservicosos sos  on sos.codigo=vsos.servicoos');
                                Query2.SQL.Add('join tabfuncionarios Func on Func.codigo=sos.funcionario');
                                Query2.sql.Add('where vsos.ordemdeservico='+Pcodigo);
                                Query2.SQL.Add('and vsos.vidroos='+CodigoVidro);
                                if(statuspesquisa<>'')
                                then Query2.SQL.Add('and vsos.concluido='+#39+statuspesquisa+#39);
                                Query2.Open;
                                while not query2.Eof do
                                begin
                                      if(Query2.FieldByName('concluido').AsString = 'N')
                                      then status:='Aberto'
                                      else status:='Fechado';

                                      FreltxtRDPRINT.RDprint.Imp(FreltxtRDPRINT.linhalocal,10,CompletaPalavra(Query2.Fieldbyname('nomeservico').asstring,50,' ')+' '+
                                              CompletaPalavra(Query2.Fieldbyname('NomeFuncionario').asstring,25,' ')+' '+
                                              CompletaPalavra(status,10,' '));
                                      FreltxtRDPRINT.incrementalinha(1);
                                      Query2.Next;
                                end;
                                FreltxtRDPRINT.incrementalinha(1);
                                Next;
                        end;
                        FreltxtRDPRINT.RDprint.Fechar;
                  end;

         finally
              FreeAndNil(Query);
              FreeAndNil(Query2);
         end;


     end;
end;


procedure TObjORDEMSERVICO.ImprimeCodigosBarras(pcodigo:string;pcodigovidro:string);
var
    pgrupo:string;
    pproduto:TStringList;
    tempstr:string;
    PosicaoFolha,temp:Integer;
    ObjQueryLocal : tibquery;
    ObjQueryLocal2 : tibquery;
    ObjRelPersReportBuilder:TObjRELPERSREPORTBUILDER;
begin
    if(pcodigo='')
    then Exit;

    if(pcodigovidro='')
    then pcodigovidro:='0';

    ObjQueryLocal:= TIBQuery.Create(nil);                    //criando um query pra executar SQL
    ObjQueryLocal.Database:= FDataModulo.IBDatabase;
    ObjQueryLocal2:= TIBQuery.Create(nil);                    //criando um query pra executar SQL
    ObjQueryLocal2.Database:= FDataModulo.IBDatabase;
    Try
      pproduto:=TStringList.create;
    Except
      Messagedlg('Erro na tentativa de Criar a String List "PPRODUTO" ',mterror,[mbok],0);
      exit;
    End;
    Try

         //escolhendo o grupo ou o produto
         limpaedit(Ffiltroimp);
         With FfiltroImp do
         Begin
              DesativaGrupos;
              Grupo04.Enabled:=True;
              LbGrupo04.caption:='Posi��o na Folha';
              edtGrupo04.EditMask:='';
              edtGrupo04.text:='1';
             // edtgrupo04.OnKeyPress := EdtcodPress;
              showmodal;

              If tag=0
              Then exit;

              posicaofolha:=1;
              Try
               
                 If(edtgrupo04.text<>'')
                 Then Begin
                         strtoint(edtgrupo04.text);
                         posicaofolha:=strtoint(edtgrupo04.text);
                         if (PosicaoFolha>0)
                         Then dec(posicaofolha,1)
                         Else Begin
                                   if (posicaofolha<0)
                                   Then PosicaoFolha:=0;
                         End;
                 End;
              Except

              End;

         End;
         try

             try
                 ObjRelPersreportBuilder:=TObjRELPERSREPORTBUILDER.Create;
             except
                 MensagemErro('Erro ao tentar criar o Objeto ObjRelPersreportBuilder');
                 exit;
             end;

             if (ObjRelPersReportBuilder.LocalizaNOme('CODIGO DE BARRAS') = false)
             then Begin
                     MensagemErro('O Relatorio de ReporteBuilder "CODIGO DE BARRAS OS" n�o foi econtrado');
                     exit;
             end;

             ObjRelPersReportBuilder.TabelaparaObjeto;
             ObjRelPersReportBuilder.SQLCabecalhoPreenchido:=StrReplace(ObjRelPersReportBuilder.Get_SQLCabecalho,':P1',(inttostr(posicaofolha)+','+pcodigovidro+','+'0'+','+pcodigo));
             ObjRelPersReportBuilder.SQLRepeticaoPreenchido:=StrReplace(ObjRelPersReportBuilder.get_SQLRepeticao,':P1',(inttostr(posicaofolha)+','+pcodigovidro+','+'0'+','+pcodigo));
             //InputBox('','',ObjRelPersReportBuilder.SQLRepeticaoPreenchido);
             ObjRelPersReportBuilder.ChamaRelatorio(false,true);

         finally
             ObjRelPersreportBuilder.Free;
         end;


    Finally
           freeandnil(pproduto);

    End;
end;
end.



