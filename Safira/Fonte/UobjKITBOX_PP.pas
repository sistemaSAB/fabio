unit UobjKITBOX_PP;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJPEDIDO_PROJ,
UOBJKITBOXCOR;

Type
   TObjKITBOX_PP=class

          Public
                ObjDataSource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                PedidoProjeto:TOBJPEDIDO_PROJ;
                KitBoxCor:TOBJKITBOXCOR;

                Constructor Create;
                Destructor  Free;

                function    DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string; PControlaNegativo: Boolean;Pdata: String): boolean;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure ResgataKITBOX_PP(PPedido, PPEdidoProjeto: string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Quantidade(parametro: string);
                Function Get_Quantidade: string;
                Procedure Submit_Valor(parametro: string);
                Function Get_Valor: string;
                Function Get_ValorFinal:string;
                procedure EdtPedidoProjetoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtKitBoxCorExit(Sender: TObject;Var PEdtCodigo:TEdit; LABELNOME:TLABEL);
                procedure EdtKitBoxCorKeyDown(Sender: TObject;Var PEdtCodigo:TEdit; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                Function PreencheKitBoxPP(PPedidoProjeto:string;var PValor:Currency;pprojetoembranco:Boolean):Boolean;
                Function DeletaKitBox_PP(PPedidoProjeto:string):Boolean;
                Function Soma_por_PP(PpedidoProjeto:string):Currency;
                Procedure RetornaDadosRel(PpedidoProjeto:string;STrDados:TStrings);
                Procedure RetornaDadosRelProjetoBranco(PpedidoProjeto:string;STrDados:TStrings;Var PTotal:Currency);

                

         Private
               Objquery:Tibquery;
               ObjQueryPesquisa:TIBquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Quantidade:string;
               Valor:string;
               ValorFinal:string;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UmostraStringList;


Function  TObjKITBOX_PP.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If(FieldByName('PedidoProjeto').asstring<>'')
        Then Begin
                 If (Self.PedidoProjeto.LocalizaCodigo(FieldByName('PedidoProjeto').asstring)=False)
                 Then Begin
                          Messagedlg('PedidoProjeto N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PedidoProjeto.TabelaparaObjeto;
        End;
        If(FieldByName('KitBoxCor').asstring<>'')
        Then Begin
                 If (Self.KitBoxCor.LocalizaCodigo(FieldByName('KitBoxCor').asstring)=False)
                 Then Begin
                          Messagedlg('KitBoxCor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.KitBoxCor.TabelaparaObjeto;
        End;
        Self.Quantidade:=fieldbyname('Quantidade').asstring;
        Self.Valor:=fieldbyname('Valor').asstring;
        Self.ValorFinal:=fieldbyname('ValorFinal').AsString;

        result:=True;
     End;
end;


Procedure TObjKITBOX_PP.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('PedidoProjeto').asstring:=Self.PedidoProjeto.GET_CODIGO;
        ParamByName('KitBoxCor').asstring:=Self.KitBoxCor.GET_CODIGO;
        ParamByName('Quantidade').asstring:=virgulaparaponto(Self.Quantidade);
        ParamByName('Valor').asstring:=virgulaparaponto(Self.Valor);
//CODIFICA OBJETOPARATABELA





  End;
End;

//***********************************************************************

function TObjKITBOX_PP.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjKITBOX_PP.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        PedidoProjeto.ZerarTabela;
        KitBoxCor.ZerarTabela;
        Quantidade:='';
        Valor:='';
        ValorFinal:='';

     End;
end;

Function TObjKITBOX_PP.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjKITBOX_PP.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.PedidoProjeto.LocalizaCodigo(Self.PedidoProjeto.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ PedidoProjeto n�o Encontrado!';
      If (Self.KitBoxCor.LocalizaCodigo(Self.KitBoxCor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ KitBoxCor n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjKITBOX_PP.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.PedidoProjeto.Get_Codigo<>'')
        Then Strtoint(Self.PedidoProjeto.Get_Codigo);
     Except
           Mensagem:=mensagem+'/PedidoProjeto';
     End;
     try
        If (Self.KitBoxCor.Get_Codigo<>'')
        Then Strtoint(Self.KitBoxCor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/KitBoxCor';
     End;
     try
        Strtofloat(Self.Quantidade);
     Except
           Mensagem:=mensagem+'/Quantidade';
     End;
     try
        Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjKITBOX_PP.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjKITBOX_PP.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjKITBOX_PP.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro KITBOX_PP vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,PedidoProjeto,KitBoxCor,Quantidade,Valor,ValorFinal');
           SQL.ADD(' from  TabKitBox_PP');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjKITBOX_PP.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjKITBOX_PP.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjKITBOX_PP.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDataSource.DataSet:=Self.ObjQueryPesquisa;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.PedidoProjeto:=TOBJPEDIDO_PROJ.create;
        Self.KitBoxCor:=TOBJKITBOXCOR.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabKitBox_PP(Codigo,PedidoProjeto,KitBoxCor');
                InsertSQL.add(' ,Quantidade,Valor)');
                InsertSQL.add('values (:Codigo,:PedidoProjeto,:KitBoxCor,:Quantidade');
                InsertSQL.add(' ,:Valor)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabKitBox_PP set Codigo=:Codigo,PedidoProjeto=:PedidoProjeto');
                ModifySQL.add(',KitBoxCor=:KitBoxCor,Quantidade=:Quantidade,Valor=:Valor');
                ModifySQL.add('');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabKitBox_PP where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjKITBOX_PP.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjKITBOX_PP.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabKITBOX_PP');
     Result:=Self.ParametroPesquisa;
end;

function TObjKITBOX_PP.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de KITBOX_PP ';
end;


function TObjKITBOX_PP.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENKITBOX_PP,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENKITBOX_PP,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjKITBOX_PP.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(Self.ObjQueryPesquisa);
    Freeandnil(Self.ObjDataSource);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.PedidoProjeto.FREE;
Self.KitBoxCor.FREE;
//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjKITBOX_PP.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjKITBOX_PP.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjKitBox_PP.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjKitBox_PP.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjKitBox_PP.Submit_Quantidade(parametro: string);
begin
        Self.Quantidade:=Parametro;
end;
function TObjKitBox_PP.Get_Quantidade: string;
begin
        Result:=Self.Quantidade;
end;
procedure TObjKitBox_PP.Submit_Valor(parametro: string);
begin
        Self.Valor:=Parametro;
end;
function TObjKitBox_PP.Get_Valor: string;
begin
        Result:=Self.Valor;
end;
//CODIFICA GETSESUBMITS


procedure TObjKITBOX_PP.EdtPedidoProjetoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PedidoProjeto.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PedidoProjeto.tabelaparaobjeto;
End;
procedure TObjKITBOX_PP.EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.PedidoProjeto.Get_Pesquisa,Self.PedidoProjeto.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoProjeto.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.PedidoProjeto.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoProjeto.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjKITBOX_PP.EdtKitBoxCorExit(Sender: TObject;Var PEdtCodigo:TEdit;  LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.KitBoxCor.localizaReferencia (TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     Self.KitBoxCor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.KitBoxCor.KitBox.Get_Descricao+' - '+Self.KitBoxCor.Cor.Get_Descricao;
     PEdtCodigo.Text:=Self.KitBoxCor.Get_Codigo;

End;
procedure TObjKITBOX_PP.EdtKitBoxCorKeyDown(Sender: TObject;Var PEdtCodigo:TEdit; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from  ViewKitBoxCor',Self.KitBoxCor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Ref_KitBox').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 LABELNOME.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('KitBox').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjKITBOX_PP.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJKITBOX_PP';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjKITBOX_PP.ResgataKITBOX_PP(PPedido, PPEdidoProjeto: string);
begin
       With Self.ObjQueryPesquisa do
       Begin
           Close;
           SQL.Clear;
           Sql.add('Select TabKitBox.Referencia, TabKitBox.Descricao as KitBox,');
           Sql.add('TabCor.Descricao as Cor,');
           Sql.add('TabKitBox_PP.Quantidade,');
           Sql.add('TabKitBox_PP.Valor,');
           Sql.add('TabKitBox_PP.ValorFinal,TabPedido_Proj.Codigo as PedidoProjeto,');
           Sql.Add('TabKitBox_PP.Codigo');
           Sql.add('from TabKitBox_PP');
           Sql.add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabKitBox_PP.PedidoProjeto');
           Sql.add('Join TabPedido on TabPedido.Codigo = TabPedido_Proj.Pedido');
           Sql.add('Join TabProjeto on TabProjeto.Codigo = TabPedido_Proj.Projeto');
           Sql.add('Join TabKitBoxCor on TabKitBoxCor.Codigo = TabKitBox_PP.KitBoxCor');
           Sql.add('join TabKitBox  on TabKitBox.Codigo = TabKitBoxCor.KitBox');
           Sql.add('Join TabCor on TabCor.Codigo = TabKitBoxCor.Cor');

           Sql.add('Where TabPedido_Proj.Pedido = '+PPedido);
           if (PpedidoProjeto <> '' )
           then Sql.add('and   TabPedido_Proj.Codigo ='+PPedidoProjeto);

           if (PPedido = '')then
           exit;

           Open;
       end;
end;

function TObjKITBOX_PP.DeletaKitBox_PP(PPedidoProjeto:string): Boolean;
begin
     Result:=False;
     With Objquery  do
     Begin
        Close;
        SQL.Clear;
        Sql.Add('Delete from TabKitBox_PP where PedidoProjeto='+ppedidoprojeto);
        Try
            execsql;
        Except
              On E:Exception do
              Begin
                   Messagedlg('Erro na tentativa de Excluir as Perfilado do Projeto '+#13+E.message,mterror,[mbok],0);
                   exit;
              End;
        End;
     end;
     Result:=true;

end;

function TObjKITBOX_PP.Get_ValorFinal: string;
begin
   Result:=Self.ValorFinal;
end;

function TObjKITBOX_PP.PreencheKitBoxPP(PPedidoProjeto: string; var PValor: Currency;pprojetoembranco:Boolean): Boolean;
begin
    Result:=false;
    Pvalor:=0;

    //Localizo o Pedido Projeto
    if (Self.PedidoProjeto.LocalizaCodigo(PPedidoProjeto)=false)
    then exit;
    Self.PedidoProjeto.TabelaparaObjeto;
    //**********************************

    With ObjQueryPesquisa do
    Begin
        //Apaga Apenas os KitBox ligadas a este Pedido Projeto

        Self.DeletaKitBox_PP(PPedidoProjeto);
        if (pprojetoembranco=True)
        Then Begin
                  Result:=True;
                  exit;
        End;

        //esse sqls faz um relacionamento entre os KitBox de um projeto
        //e a tabela KitBoxCor, para saber quais das KitBox de um determinado
        //projeto nao esto cadastar na COR X, esse sql abaixo retorna
        //as que estaum nesse caso, para testa-lo no ibconsole
        //retire o distinct e acrescente TabKitBoxCor.Cor para visualizar
        //e retire a clausula where

        close;
        sql.clear;
        sql.add('Select distinct(TabKitBox_Proj.KitBox)');
        sql.add('from TabKitBox_Proj');
        sql.add('left join TabKitBoxCor on');
        sql.add('(TabKitBox_Proj.KitBox=TabKitBoxCor.KitBox and TabKitBoxCor.Cor='+Self.PedidoProjeto.CorKitBox.Get_Codigo+')');
        sql.add('Where TabKitBox_Proj.Projeto='+Self.PedidoProjeto.Projeto.Get_Codigo);
        sql.add('and TabKitBoxCor.Cor is null');
        open;
        last;
        if (recordcount>0)
        Then Begin
                  if (Messagedlg('Alguns KitBoxs cadastrados para este projeto n�o existem na Cor Escolhida, n�o ser� poss�vel continuar.'+#13+'Deseja visualiza-las?',mtconfirmation,[mbyes,mbno],0)=mrno)
                  Then exit;
                  FmostraStringList.Memo.clear;
                  FmostraStringList.Memo.Lines.add('KitBoxS N�O CADASTRADAO PARA A COR='+Self.PedidoProjeto.CorKitBox.Get_Codigo+'-'+Self.PedidoProjeto.CorKitBox.Get_Descricao);
                  FmostraStringList.Memo.Lines.add(' ');
                  first;
                  While not(eof) do
                  Begin
                       Self.KitBoxCor.KitBox.LocalizaCodigo(fieldbyname('KitBox').asstring);
                       Self.KitBoxCor.KitBox.TabelaparaObjeto;
                       FmostraStringList.Memo.Lines.add(Self.KitBoxCor.KitBox.Get_Codigo+' - '+Self.KitBoxCor.KitBox.Get_Descricao);
                       next;
                  End;
                  FmostraStringList.ShowModal;
                  exit;
        End;

        //Esse SQL Retorna o Codigo da KitBoxCor,o PReco, e a quantidade (cadastrada no projeto)

        Close;
        SQL.Clear;
        SQL.Add('Select  TabKitBoxCor.Codigo as KitBoxCor, TabKitBox_Proj.Quantidade,');

        if (Self.PedidoProjeto.Get_TipoPedido='INSTALADO')
        Then SQL.Add('(tabKitBox.PRecoVendaInstalado+(TabKitBox.PrecoVendaInstalado*TabKitBoxCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        if (Self.PedidoProjeto.Get_TipoPedido='RETIRADO')
        Then SQL.Add('(tabKitBox.PRecoVendaretirado+(TabKitBox.PrecoVendaretirado*TabKitBoxCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        if (Self.PedidoProjeto.Get_TipoPedido='FORNECIDO')
        Then SQL.Add('(tabKitBox.PRecoVendafornecido+(TabKitBox.PrecoVendafornecido*TabKitBoxCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        SQL.Add('from TabKitBox_Proj');
        SQL.Add('Join TabKitBoxCor on TabKitBoxCOr.KitBox = TabKitBox_Proj.KitBox');
        SQL.Add('join TabKitBox on TabKitBox.Codigo = TabKitBoxCor.KitBox');
        SQL.Add('Where TabKitBox_Proj.Projeto = '+Self.PedidoProjeto.Projeto.Get_Codigo);
        SQL.Add('and   TabKitBoxCor.Cor = '+Self.PedidoProjeto.CorKitBox.Get_Codigo);
        Open;
        first;

        // Preenchendo a TabKitBox_PP
        While not (eof) do
        Begin
            Try
               Pvalor:=Pvalor/1000;
            Except
                  Pvalor:=0;
            End;

            Self.Status:=dsInsert;
            Self.Submit_Codigo(Self.Get_NovoCodigo);
            Self.PedidoProjeto.Submit_Codigo(PPedidoProjeto);
            Self.KitBoxCor.Submit_Codigo(fieldbyname('KitBoxCor').AsString);
            Self.Submit_Quantidade(fieldbyname('Quantidade').AsString);
            Self.Submit_Valor(formata_valor(fieldbyname('Preco').AsString,4,True));
            if Self.Salvar(false)=false then
            Begin
                 Result:=false;
                 exit;
            end;
            Next;
        end;
        pvalor:=0;
        Pvalor:=Self.Soma_por_PP(Self.PedidoProjeto.Get_Codigo);
        result:=true;
    end;
end;

function TObjKITBOX_PP.Soma_por_PP(PpedidoProjeto: string): Currency;
begin
     Self.Objquery.close;
     Self.Objquery.sql.clear;
     Self.Objquery.sql.add('Select sum(valorfinal) as SOMA from TabKItBox_PP where PedidoProjeto='+PpedidoProjeto);
     Try
         Self.Objquery.open;
         Result:=Self.Objquery.fieldbyname('soma').asfloat;
     Except
           Result:=0;
     End;

end;

procedure TObjKITBOX_PP.RetornaDadosRel(PpedidoProjeto: string; STrDados: TStrings);
begin
     if (PpedidoProjeto='')
     Then exit;

     With Self.ObjQueryPesquisa do
     Begin
          close;
          SQL.clear;
          sql.add('Select codigo from TabKitBox_pp where PedidoProjeto='+PpedidoProjeto);
          open;
          first;

          if (RecordCount = 0)then
          exit;

          While not(eof) do
          Begin
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;
               STrDados.Add(CompletaPalavra(Self.KitBoxCor.KitBox.Get_Referencia,10,' ')+' '+
                            CompletaPalavra(Self.KitBoxCor.Cor.Get_Referencia+'-'+Self.KitBoxCor.Cor.Get_Descricao,16,' ')+' '+
                            CompletaPalavra(Self.KitBoxCor.KitBox.Get_Descricao,45,' ')+' '+
                            CompletaPalavra(Self.Get_Quantidade+' '+Self.KitBoxCor.KitBox.Get_Unidade,6,' ')+' '+
                            CompletaPalavra_a_Esquerda(formata_valor(Self.Get_Valor),10,' ')+' '+
                            CompletaPalavra_a_Esquerda(formata_valor(StrToCurr(Self.Get_Quantidade)*StrToCurr(Self.Get_Valor)),8,' '));
              next;
          end;
          close;
          sql.clear;
          sql.add('Select sum(ValorFinal) as SOMA FROM TabKitBox_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          STrDados.Add('?'+CompletaPalavra_a_Esquerda('Total: PERSIANAS: '+formata_valor(fieldbyname('soma').asstring),100,' '));
          STrDados.Add(' ');
     End;

end;


function TobjKitbox_PP.DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;PControlaNegativo: Boolean;Pdata:String): boolean;var
PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          Sql.add('Select codigo,KitBoxcor,(quantidade*-1) as quantidade from TabKitBox_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;

          first;
          
          While not(eof) do
          Begin
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Status:=dsinsert;
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(fieldbyname('quantidade').asstring);
              OBJESTOQUEGLOBAL.Submit_DATA(Pdata);
              OBJESTOQUEGLOBAL.Submit_kitboxCOR(fieldbyname('kitboxcor').asstring);
              OBJESTOQUEGLOBAL.Submit_PEDIDOPROJETOROMANEIO(PpedidoProjetoRomaneio);
              OBJESTOQUEGLOBAL.Submit_kitbox_PP(fieldbyname('codigo').asstring);
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('ROMANEIO');


              Self.KitBoxCor.LocalizaCodigo(fieldbyname('KitBoxcor').asstring);
              Self.KitBoxCor.TabelaparaObjeto;

              try
                 PestoqueAtual:=strtofloat(Self.KitBoxCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual do KitBox '+Self.KitBoxCor.KitBox.Get_Descricao+' da cor '+Self.KitBoxCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              Try
                PnovoEstoque:=PestoqueAtual+fieldbyname('quantidade').asfloat;
              Except
                    Messagedlg('Erro na gera��o do novo Estoque do KitBox '+Self.KitBoxCor.KitBox.Get_Descricao+' da cor '+Self.KitBoxCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              if (Pnovoestoque<0)
              then Begin
                        if (PControlaNegativo=True)
                        Then begin
                                  Messagedlg('O KitBox '+Self.KitBoxCor.KitBox.Get_Descricao+' da cor '+Self.KitBoxCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                  exit;
                        End;
              End;

              if (OBJESTOQUEGLOBAL.Salvar(False)=False)
              then exit;

              next;
          End;
          result:=True;
          exit;
     End;
end;

procedure TObjKITBOX_PP.RetornaDadosRelProjetoBranco(PpedidoProjeto: string;
  STrDados: TStrings; var PTotal: Currency);
begin
     if (PpedidoProjeto='')
     Then exit;

     With Self.ObjQueryPesquisa do
     Begin
          {close;
          SQL.clear;
          sql.add('Select codigo from TabKitBox_pp where PedidoProjeto='+PpedidoProjeto);
          open;
          first;

          if (RecordCount = 0)then
          exit;

          While not(eof) do
          Begin
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;
               STrDados.Add(CompletaPalavra(Self.KitBoxCor.KitBox.Get_Referencia,10,' ')+' '+
                            CompletaPalavra(Self.KitBoxCor.Cor.Get_Referencia+'-'+Self.KitBoxCor.Cor.Get_Descricao,16,' ')+' '+
                            CompletaPalavra(Self.KitBoxCor.KitBox.Get_Descricao,45,' ')+' '+
                            CompletaPalavra(Self.Get_Quantidade+' '+Self.KitBoxCor.KitBox.Get_Unidade,6,' ')+' '+
                            CompletaPalavra_a_Esquerda(formata_valor(Self.Get_Valor),10,' ')+' '+
                            CompletaPalavra_a_Esquerda(formata_valor(StrToCurr(Self.Get_Quantidade)*StrToCurr(Self.Get_Valor)),8,' '));
              next;
          end;}
          close;
          sql.clear;
          sql.add('Select Tabkitbox_pp.codigo,tabkitbox.referencia,');
          sql.add('TabCor.referencia||''-''||tabCor.Descricao as COR,');
          sql.add('Tabkitbox.Descricao,');
          sql.add('Tabkitbox_PP.Quantidade,');
          sql.add('Tabkitbox.Unidade,');
          sql.add('Tabkitbox_pp.valor,');
          sql.add('Tabkitbox_PP.quantidade*Tabkitbox_pp.valor as MULT');
          sql.add('from Tabkitbox_pp');
          sql.add('join TabkitboxCor on Tabkitbox_pp.kitboxCor=Tabkitboxcor.codigo');
          sql.add('join tabcor on TabkitboxCor.Cor=Tabcor.codigo');
          sql.add('join tabkitbox on Tabkitboxcor.kitbox=tabkitbox.codigo');
          sql.add('where TabKitBox_pp.PedidoProjeto='+PpedidoProjeto);
          open;
          first;

          if (RecordCount = 0)then
          exit;

          While not(eof) do
          Begin
               //Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               //Self.TabelaparaObjeto;
               STrDados.Add(CompletaPalavra(fieldbyname('referencia').asstring,10,' ')+' '+
                            CompletaPalavra(fieldbyname('cor').asstring,16,' ')+' '+
                            CompletaPalavra(fieldbyname('descricao').asstring,45,' ')+' '+
                            CompletaPalavra(fieldbyname('quantidade').asstring+' '+fieldbyname('unidade').asstring,6,' ')+' '+
                            CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),10,' ')+' '+
                            CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('mult').asstring),8,' '));
              next;
          end;

          close;
          sql.clear;
          sql.add('Select sum(ValorFinal) as SOMA FROM TabKitBox_PP where PedidoProjeto='+PpedidoProjeto);
          open;

          PTotal:=PTotal+fieldbyname('soma').AsCurrency
     End;

end;

end.



