unit UobjModeloNF;

Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc;
//USES_INTERFACE

Type
   TObjMODELONF=class
   

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_MODELO(parametro: string);
                Function Get_MODELO: string;
                Procedure Submit_QuantidadeProdutos(parametro: string);
                Function Get_QuantidadeProdutos: string;
                Function PegaModelo:Boolean;
                Function Get_NotaFatura : string;
                Procedure Submit_NotaFatura(Parametro : string);
                Procedure Submit_CODIGO_FISCAL(parametro: string);
                Function Get_CODIGO_FISCAL: string;
                procedure edtModeloNFKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               MODELO:string;
               QuantidadeProdutos:string;
               NotaFatura:string;
               CODIGO_FISCAL:string;
//CODIFICA VARIAVEIS PRIVADAS





               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UescolheStringGrid, Math;





Function  TObjMODELONF.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.MODELO:=fieldbyname('MODELO').asstring;
        Self.QuantidadeProdutos:=fieldbyname('QuantidadeProdutos').asstring;
        Self.NotaFatura:= fieldbyname('NotaFatura').asstring;
        Self.CODIGO_FISCAL:= fieldbyname('CODIGO_FISCAL').asstring;
//CODIFICA TABELAPARAOBJETO



        result:=True;
     End;
end;

procedure TObjMODELONF.edtModeloNFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabmodelonf','',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                TEdit(Sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').AsString;
                                LABELNOME.Caption:=FpesquisaLocal.querypesq.fieldbyname('modelo').AsString;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;


Procedure TObjMODELONF.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring := Self.CODIGO;
        ParamByName('MODELO').asstring := Self.MODELO;
        ParamByName('QuantidadeProdutos').asstring := Self.QuantidadeProdutos;
        Parambyname('NotaFatura').AsString := Self.NotaFatura;
        Parambyname('CODIGO_FISCAL').AsString := Self.CODIGO_FISCAL;
//CODIFICA OBJETOPARATABELA



  End;
End;

//***********************************************************************

function TObjMODELONF.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjMODELONF.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        MODELO:='';
        QuantidadeProdutos:='';
        NotaFatura := '';
        CODIGO_FISCAL:='';
        //CODIFICA ZERARTABELA



     End;
end;

Function TObjMODELONF.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      If (MODELO='')
      Then Mensagem:=mensagem+'/MODELONF';

      If (QuantidadeProdutos='')
      Then Mensagem:=mensagem+'/Quantidade de Produtos';

      If (NotaFatura='')
      Then Mensagem:=mensagem+'/Nota Fatura';

      If (CODIGO_FISCAL='')
      Then Mensagem:=mensagem+'/ C�digo Fiscal';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjMODELONF.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjMODELONF.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        Strtoint(Self.QuantidadeProdutos);
     Except
           Mensagem:=mensagem+'/Quantidade de Produtos';
     End;


//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjMODELONF.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjMODELONF.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjMODELONF.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro MODELONF vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,MODELO,QuantidadeProdutos,NotaFatura,CODIGO_FISCAL');
           SQL.ADD(' from  Tabmodelonf');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjMODELONF.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjMODELONF.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjMODELONF.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into Tabmodelonf(CODIGO,MODELO,QuantidadeProdutos,NotaFatura,CODIGO_FISCAL');
                InsertSQL.add(' )');
                InsertSQL.add('values (:CODIGO,:MODELO,:QuantidadeProdutos,:NotaFatura,:CODIGO_FISCAL)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update Tabmodelonf set CODIGO=:CODIGO,MODELO=:MODELO');
                ModifySQL.add(',QuantidadeProdutos=:QuantidadeProdutos,NotaFatura=:NotaFatura,CODIGO_FISCAL=:CODIGO_FISCAL');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from Tabmodelonf where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjMODELONF.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjMODELONF.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabMODELONF');
     Result:=Self.ParametroPesquisa;
end;


function TObjMODELONF.Get_NovoCodigo: string;
var
  ObjQuery:TIBQuery;
  codigo:Integer;
begin
    try
      ObjQuery:=TIBQuery.Create(nil);
      ObjQuery.Database:=FDataModulo.IBDatabase;
    Except

    end;

    try
           with ObjQuery do
           begin
                    Close;
                    sql.Clear;
                    sql.Add('select codigo from tabmodelonf order by codigo');
                    Open;
                    Last;
                    if(recordcount =0)
                    then begin
                           codigo:=1;
                    end
                    else
                    begin
                           codigo:=fieldbyname('codigo').AsInteger;
                           Inc(codigo,1);
                    end;
                    result:=IntToStr(codigo);
           end;
    finally
           FreeAndNil(ObjQuery);
    end;



end;

destructor TObjMODELONF.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjMODELONF.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjMODELONF.RetornaCampoNome: string;
begin
      result:='MODELO';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjMODELONF.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjMODELONF.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;

procedure TObjMODELONF.Submit_MODELO(parametro: string);
begin
        Self.MODELO:=Parametro;
end;

function TObjMODELONF.Get_MODELO: string;
begin
        Result:=Self.MODELO;
end;
procedure TObjMODELONF.Submit_QuantidadeProdutos(parametro: string);
begin
        Self.QuantidadeProdutos:=Parametro;
end;
function TObjMODELONF.Get_QuantidadeProdutos: string;
begin
        Result:=Self.QuantidadeProdutos;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjMODELONF.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJMODELONF';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



function TObjMODELONF.PegaModelo: Boolean;
var
cont:integer;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select * from TabModeloNf');
          open;
          last;
          if (recordcount=0)
          Then exit;

          if (recordcount=1)
          Then Begin
                    Self.LocalizaCodigo(fieldbyname('codigo').asstring);
                    Self.TabelaparaObjeto;
                    result:=true;
                    exit;
          End;

          //em casos de mais de um convenio tenho que escolher qual usar
          FEscolheStringGrid.Caption:='Escolha o Modelo de NF';
          FEscolheStringGrid.STRGConvenio.ColCount:=2;
          FEscolheStringGrid.STRGConvenio.FixedCols:=1;
          FEscolheStringGrid.STRGConvenio.FixedRows:=1;
          FEscolheStringGrid.STRGConvenio.Cols[0].clear;
          FEscolheStringGrid.STRGConvenio.Cols[1].clear;
          FEscolheStringGrid.STRGConvenio.RowCount:=Self.Objquery.RecordCount+1;
          FEscolheStringGrid.STRGConvenio.Cells[0,0]:='C�DIGO';
          FEscolheStringGrid.STRGConvenio.Cells[1,0]:='Modelo de NF';
          Self.Objquery.First;
          cont:=1;
          while not(Self.Objquery.Eof) do
          begin
               FEscolheStringGrid.STRGConvenio.Cells[0,cont]:=Self.Objquery.fieldbyname('codigo').asstring;
               FEscolheStringGrid.STRGConvenio.Cells[1,cont]:=Self.Objquery.fieldbyname('Modelo').asstring;
               inc(cont,1);
               Self.Objquery.next;
          end;
          AjustaLArguraColunaGrid(FEscolheStringGrid.STRGConvenio);

          FEscolheStringGrid.showmodal;
          
          if (FEscolheStringGrid.tag=-1)
          Then exit;
          
          Self.LocalizaCodigo(FEscolheStringGrid.STRGConvenio.Cells[0,FescolheStringGrid.tag]);
          Self.TabelaparaObjeto;
          result:=true;
          exit;
     End;
end;

function TObjMODELONF.Get_NotaFatura: string;
begin
      Result := self.NotaFatura;
end;

procedure TObjMODELONF.Submit_NotaFatura(Parametro: string);
begin
      Self.NotaFatura := Parametro;
end;

function TObjMODELONF.Get_CODIGO_FISCAL: string;
begin
   Result := self.CODIGO_FISCAL;
end;

procedure TObjMODELONF.Submit_CODIGO_FISCAL(parametro: string);
begin
   Self.CODIGO_FISCAL := Parametro;
end;

end.



end.
