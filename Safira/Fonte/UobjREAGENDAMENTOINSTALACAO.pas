unit UobjREAGENDAMENTOINSTALACAO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
;
//USES_INTERFACE


Type
   TObjREAGENDAMENTOINSTALACAO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_DATAANTERIOR(parametro: string);
                Function Get_DATAANTERIOR: string;
                Procedure Submit_HORARIOANTERIOR(parametro: string);
                Function Get_HORARIOANTERIOR: string;
                Procedure Submit_PEDIDOPROJETO(parametro: string);
                Function Get_PEDIDOPROJETO: string;
                Procedure Submit_ROMANEIO(parametro: string);
                Function Get_ROMANEIO: string;
                Procedure Submit_NOVADATA(parametro: string);
                Function Get_NOVADATA: string;
                Procedure Submit_NOVOHORARIO(parametro: string);
                Function Get_NOVOHORARIO: string;
                Procedure Submit_OBSERVACAO(parametro: String);
                Function Get_OBSERVACAO: string;
                procedure Submit_Colocador(parametro:string);
                function Get_Colocador:string;
                Procedure Submit_Motivoreagendamento(parametro:string);
                Function Get_Motivoreagendamento:string;


                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               DATAANTERIOR:string;
               HORARIOANTERIOR:string;
               PEDIDOPROJETO:string;
               ROMANEIO:string;
               NOVADATA:string;
               NOVOHORARIO:string;
               OBSERVACAO:string;
               MOTIVOREAGENDAMENTO:string;
               COLOCADOR:string;

//CODIFICA VARIAVEIS PRIVADAS
               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;


Function  TObjREAGENDAMENTOINSTALACAO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.DATAANTERIOR:=fieldbyname('DATAANTERIOR').asstring;
        Self.HORARIOANTERIOR:=fieldbyname('HORARIOANTERIOR').asstring;
        Self.PEDIDOPROJETO:=fieldbyname('PEDIDOPROJETO').asstring;
        Self.ROMANEIO:=fieldbyname('ROMANEIO').asstring;
        Self.NOVADATA:=fieldbyname('NOVADATA').asstring;
        Self.NOVOHORARIO:=fieldbyname('NOVOHORARIO').asstring;
        Self.OBSERVACAO:=fieldbyname('OBSERVACAO').asstring;
        self.MOTIVOREAGENDAMENTO:=fieldbyname('motivoreagendamento').AsString;
        self.COLOCADOR:=fieldbyname('colocador').AsString;

//CODIFICA TABELAPARAOBJETO
        result:=True;
     End;
end;


Procedure TObjREAGENDAMENTOINSTALACAO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('DATAANTERIOR').asstring:=Self.DATAANTERIOR;
        ParamByName('HORARIOANTERIOR').asstring:=Self.HORARIOANTERIOR;
        ParamByName('PEDIDOPROJETO').asstring:=Self.PEDIDOPROJETO;
        ParamByName('ROMANEIO').asstring:=Self.ROMANEIO;
        ParamByName('NOVADATA').asstring:=Self.NOVADATA;
        ParamByName('NOVOHORARIO').asstring:=Self.NOVOHORARIO;
        ParamByName('OBSERVACAO').asstring:=Self.OBSERVACAO;
        ParamByName('MOTIVOREAGENDAMENTO').AsString:=self.MOTIVOREAGENDAMENTO;
        ParamByName('COLOCADOR').AsString:=Self.COLOCADOR;

//CODIFICA OBJETOPARATABELA

  End;
End;

//***********************************************************************

function TObjREAGENDAMENTOINSTALACAO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjREAGENDAMENTOINSTALACAO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        DATAANTERIOR:='';
        HORARIOANTERIOR:='';
        PEDIDOPROJETO:='';
        ROMANEIO:='';
        NOVADATA:='';
        NOVOHORARIO:='';
        OBSERVACAO:='';
        MOTIVOREAGENDAMENTO:='';
        COLOCADOR:='';

//CODIFICA ZERARTABELA

     End;
end;

Function TObjREAGENDAMENTOINSTALACAO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjREAGENDAMENTOINSTALACAO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjREAGENDAMENTOINSTALACAO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
     try
        Strtoint(Self.PEDIDOPROJETO);
     Except
           Mensagem:=mensagem+'/PEDIDOPROJETO';
     End;
     try
        Strtoint(Self.ROMANEIO);
     Except
           Mensagem:=mensagem+'/ROMANEIO';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjREAGENDAMENTOINSTALACAO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.DATAANTERIOR);
     Except
           Mensagem:=mensagem+'/DATAANTERIOR';
     End;
     try
        Strtotime(Self.HORARIOANTERIOR);
     Except
           Mensagem:=mensagem+'/HORARIOANTERIOR';
     End;
     try
        Strtodate(Self.NOVADATA);
     Except
           Mensagem:=mensagem+'/NOVADATA';
     End;
     try
        Strtotime(Self.NOVOHORARIO);
     Except
           Mensagem:=mensagem+'/NOVOHORARIO';
     End;

//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjREAGENDAMENTOINSTALACAO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjREAGENDAMENTOINSTALACAO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro REAGENDAMENTOINSTALACAO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,DATAANTERIOR,HORARIOANTERIOR,PEDIDOPROJETO,ROMANEIO');
           SQL.ADD(' ,NOVADATA,NOVOHORARIO,OBSERVACAO,DATAC,DATAM,USERC,USERM,motivoreagendamento,colocador');
           SQL.ADD(' from  TABREAGENDAMENTOINSTALACAO');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjREAGENDAMENTOINSTALACAO.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjREAGENDAMENTOINSTALACAO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjREAGENDAMENTOINSTALACAO.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjREAGENDAMENTOINSTALACAO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABREAGENDAMENTOINSTALACAO(CODIGO,DATAANTERIOR');
                InsertSQL.add(' ,HORARIOANTERIOR,PEDIDOPROJETO,ROMANEIO,NOVADATA,NOVOHORARIO');
                InsertSQL.add(' ,OBSERVACAO,DATAC,DATAM,USERC,USERM,MOTIVOREAGENDAMENTO,COLOCADOR)');
                InsertSQL.add('values (:CODIGO,:DATAANTERIOR,:HORARIOANTERIOR,:PEDIDOPROJETO');
                InsertSQL.add(' ,:ROMANEIO,:NOVADATA,:NOVOHORARIO,:OBSERVACAO,:DATAC');
                InsertSQL.add(' ,:DATAM,:USERC,:USERM,:MOTIVOREAGENDAMENTO,:COLOCADOR)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABREAGENDAMENTOINSTALACAO set CODIGO=:CODIGO');
                ModifySQL.add(',DATAANTERIOR=:DATAANTERIOR,HORARIOANTERIOR=:HORARIOANTERIOR');
                ModifySQL.add(',PEDIDOPROJETO=:PEDIDOPROJETO,ROMANEIO=:ROMANEIO,NOVADATA=:NOVADATA');
                ModifySQL.add(',NOVOHORARIO=:NOVOHORARIO,OBSERVACAO=:OBSERVACAO,DATAC=:DATAC');
                ModifySQL.add(',DATAM=:DATAM,USERC=:USERC,USERM=:USERM,MOTIVOREAGENDAMENTO=:MOTIVOREAGENDAMENTO,COLOCADOR=:COLOCADOR');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABREAGENDAMENTOINSTALACAO where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjREAGENDAMENTOINSTALACAO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjREAGENDAMENTOINSTALACAO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabREAGENDAMENTOINSTALACAO');
     Result:=Self.ParametroPesquisa;
end;

function TObjREAGENDAMENTOINSTALACAO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de REAGENDAMENTOINSTALACAO ';
end;


function TObjREAGENDAMENTOINSTALACAO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENREAGENDAMENTOINSTALACAO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENREAGENDAMENTOINSTALACAO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjREAGENDAMENTOINSTALACAO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjREAGENDAMENTOINSTALACAO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjREAGENDAMENTOINSTALACAO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjREAGENDAMENTOINSTALACAO.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjREAGENDAMENTOINSTALACAO.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjREAGENDAMENTOINSTALACAO.Submit_DATAANTERIOR(parametro: string);
begin
        Self.DATAANTERIOR:=Parametro;
end;
function TObjREAGENDAMENTOINSTALACAO.Get_DATAANTERIOR: string;
begin
        Result:=Self.DATAANTERIOR;
end;
procedure TObjREAGENDAMENTOINSTALACAO.Submit_HORARIOANTERIOR(parametro: string);
begin
        Self.HORARIOANTERIOR:=Parametro;
end;
function TObjREAGENDAMENTOINSTALACAO.Get_HORARIOANTERIOR: string;
begin
        Result:=Self.HORARIOANTERIOR;
end;
procedure TObjREAGENDAMENTOINSTALACAO.Submit_PEDIDOPROJETO(parametro: string);
begin
        Self.PEDIDOPROJETO:=Parametro;
end;
function TObjREAGENDAMENTOINSTALACAO.Get_PEDIDOPROJETO: string;
begin
        Result:=Self.PEDIDOPROJETO;
end;
procedure TObjREAGENDAMENTOINSTALACAO.Submit_ROMANEIO(parametro: string);
begin
        Self.ROMANEIO:=Parametro;
end;
function TObjREAGENDAMENTOINSTALACAO.Get_ROMANEIO: string;
begin
        Result:=Self.ROMANEIO;
end;
procedure TObjREAGENDAMENTOINSTALACAO.Submit_NOVADATA(parametro: string);
begin
        Self.NOVADATA:=Parametro;
end;
function TObjREAGENDAMENTOINSTALACAO.Get_NOVADATA: string;
begin
        Result:=Self.NOVADATA;
end;
procedure TObjREAGENDAMENTOINSTALACAO.Submit_NOVOHORARIO(parametro: string);
begin
        Self.NOVOHORARIO:=Parametro;
end;
function TObjREAGENDAMENTOINSTALACAO.Get_NOVOHORARIO: string;
begin
        Result:=Self.NOVOHORARIO;
end;
procedure TObjREAGENDAMENTOINSTALACAO.Submit_OBSERVACAO(parametro: string);
begin
        Self.OBSERVACAO:=Parametro;
end;
function TObjREAGENDAMENTOINSTALACAO.Get_OBSERVACAO: string;
begin
        Result:=Self.OBSERVACAO;
end;


//CODIFICA EXITONKEYDOWN
procedure TObjREAGENDAMENTOINSTALACAO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJREAGENDAMENTOINSTALACAO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;

procedure TObjREAGENDAMENTOINSTALACAO.Submit_Colocador(parametro:string);
begin
       COLOCADOR:=parametro;
end;

function TObjREAGENDAMENTOINSTALACAO.Get_Colocador:string;
begin
       Result:=COLOCADOR;
end;

function TObjREAGENDAMENTOINSTALACAO.Get_Motivoreagendamento:string;
begin
       Result:=MOTIVOREAGENDAMENTO;
end;

procedure TObjREAGENDAMENTOINSTALACAO.Submit_Motivoreagendamento(parametro:String);
begin
       MOTIVOREAGENDAMENTO:=parametro;
end;



end.



