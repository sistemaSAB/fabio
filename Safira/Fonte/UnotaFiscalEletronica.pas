unit UnotaFiscalEletronica;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, Buttons,UobjNFE,DB,UessencialGlobal,
  OleCtrls, SHDocVw,UobjEMPRESA,IBQuery,pcnLeitor,pcnConversao,
  Grids,Controls, xmldom, XMLIntf, msxmldom,
  XMLDoc, RDFormula,UobjExtraiXML,UobjNotaFiscalObjetos,UobjTransmiteNFE, ShellAPI;

type
  TFnotaFiscalEletronica = class(TForm)
    panelbotes: TPanel;
    painelRodape: TPanel;
    lbrodape: TLabel;
    pag1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Image1: TImage;
    Label8: TLabel;
    lbCodigo: TLabel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    edtCertificado: TEdit;
    Label4: TLabel;
    edtArquivoXML: TEdit;
    Label2: TLabel;
    edtChaveAcesso: TEdit;
    Label5: TLabel;
    edtArquivoEnvioResposta: TEdit;
    Label1: TLabel;
    edtProtocoloCancelamento: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    lbDataCancelamento: TLabel;
    edtArquivoCancelamento: TEdit;
    Label11: TLabel;
    EdtMotivoCancelamento: TEdit;
    GroupBox3: TGroupBox;
    Label12: TLabel;
    lbDataInutilizacao: TLabel;
    Label14: TLabel;
    edtProtocoloInutilizacao: TEdit;
    edtArquivoInutilizacao: TEdit;
    edtMotivoInutilizacao: TEdit;
    Label15: TLabel;
    Label16: TLabel;
    btnPesquisar: TBitBtn;
    btnCancelaSefaz: TBitBtn;
    btnConsultaSefaz: TBitBtn;
    btnInutilizarFaixa: TBitBtn;
    btnImprimirDanfe: TBitBtn;
    btnOpcoes: TBitBtn;
    Label9: TLabel;
    edtReciboEnvio: TEdit;
    Label10: TLabel;
    lbStatus: TLabel;
    Panel1: TPanel;
    radioXMLenvio: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    WBResposta: TWebBrowser;
    btnSair: TBitBtn;
    XMLDocument: TXMLDocument;
    btAjuda: TSpeedButton;
    Label47: TLabel;
    Label48: TLabel;
    tsEmail: TTabSheet;
    grp1: TGroupBox;
    lbl1: TLabel;
    lbl2: TLabel;
    chkEmailSSL: TCheckBox;
    edtServidorSMTP: TEdit;
    edtPorta: TEdit;
    grp2: TGroupBox;
    lbl3: TLabel;
    lbl4: TLabel;
    edtSmtpUser: TEdit;
    grp3: TGroupBox;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    btn1: TSpeedButton;
    edtEmailAssunto: TEdit;
    mmoEmailMsg: TMemo;
    edtPara: TEdit;
    btn2: TBitBtn;
    btn3: TBitBtn;
    edtAnexo: TEdit;
    edtSmtpPass: TEdit;
    btConfirmaStatusNota: TButton;
    SpeedButton2: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure pag1Change(Sender: TObject);
    procedure radioXMLenvioClick(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure btnCancelaSefazClick(Sender: TObject);
    procedure btnConsultaSefazClick(Sender: TObject);
    procedure btnInutilizarFaixaClick(Sender: TObject);
    procedure btnImprimirDanfeClick(Sender: TObject);
    procedure btnOpcoesClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btAjudaClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Label47Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btConfirmaStatusNotaClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private

    ObjNFE:TObjNFE;
    ObjNfObjetos:TObjNotafiscalObjetos;
    objExtraiXML:TobjExtraiXML;
    objTransmiteNFE:TObjTransmiteNFE;

    function TabelaParaControles: Boolean;
    function ObjetoParaControles: Boolean;

    procedure atualizaCOmboStatus (str:string);

    procedure desabilitaLabels ();
    procedure habilitaLabels   ();

    procedure limpaEdits    ();
    procedure limpaLabels   ();
    procedure esconde_mostra_Labels (parametro:boolean);

    procedure inutiliza_faixa_na_tabNotaFiscal(pnf:string);

    function MenuNfe: boolean;   

    procedure zeraStringGrid ();

    procedure marca (parametro:string);
    procedure zeraCampos ();
    procedure cancela ();
    procedure limpaStringGrid ();

    procedure atualizaICMTot (operador:string);overload;
    function  atualizaICMTot (aux1,aux2,op:string):string;overload;

    function formataNFe (valor:string):string;
    function formata    (valor:string):string;
    function enviaDevolucao ():Boolean;overload;
    function enviaDevolucao (Pnota: string; PmodoContigencia,PForcaProcessamento: boolean):Boolean;overload;

    function retorna_codigo_fornecedor (pCNPJ:string):string;
    function formata_CNPJ (pCNPJ:string):string;
    function pesquisa_produto ():string;
    function pesquisa_cfop    ():string;
    procedure get_dadosProduto (var pIsento,pSubstituto:string; pCodigoProduto:string);

    function Valida_dados_NF ():Boolean;
    function get_orig (indiceProduto:integer):TpcnOrigemMercadoria;
    function get_cst_icms   (indiceProduto:integer):TpcnCSTIcms;
    function get_cst_pis    (indiceProduto:integer):TpcnCstPis;
    function get_cst_cofins (indiceProduto:integer):TpcnCstCofins;

    function get_modalidade_bc (indiceProduto:integer):TpcnDeterminacaoBaseIcms;
    function get_modalidade_bc_cst(indiceProduto: integer): TpcnDeterminacaoBaseIcmsST;
    procedure retorna_nf (pnf:string);
//    function existe_refnfe (var pObservacao:string):Boolean;
    procedure cancelaNFeAntigo;

  public


    procedure PassaObjeto(Objeto: TobjNfe);
    procedure CancelaNfe(Parquivo: String);
    procedure ImprimeNFeArquivo(Parquivo: string);

    function CANCELA_NF_POR_NFE (pcodigoNFE:string):boolean;

  end;

var
  FnotaFiscalEletronica: TFnotaFiscalEletronica;

implementation

uses UescolheImagemBotao, Upesquisa, UComponentesNfe, UDataModulo,
  UFiltraImp, UmenuNfe, UobjMODELONF, UObjNotaFiscal,
  UobjNOTAFISCALCFOP, UAjuda, ACBrNFeConfiguracoes;

{$R *.dfm}

procedure TFnotaFiscalEletronica.FormShow(Sender: TObject);
begin

  self.ObjNFE := TObjNFE.Create;
  Self.ObjNfObjetos := TObjNotafiscalObjetos.Create(self);
  objExtraiXML := TobjExtraiXML.create ();
  objTransmiteNFE:=TObjTransmiteNFE.Create(nil,FDataModulo.IBDatabase);

  FescolheImagemBotao.PegaFiguraBotaopequeno(btnPesquisar,'BOTAOPESQUISAR.BMP');
  FescolheImagemBotao.PegaFiguraBotaopequeno(btnCancelaSefaz,'BOTAOCANCELASEFAZ.BMP');
  FescolheImagemBotao.PegaFiguraBotaopequeno(btnConsultaSefaz,'BOTAOCONSULTASEFAZ.BMP');
  FescolheImagemBotao.PegaFiguraBotaopequeno(btnInutilizarFaixa,'BOTAOINUTILIZARFAIXA.BMP');
  FescolheImagemBotao.PegaFiguraBotaopequeno(btnImprimirDanfe,'BOTAOIMPRIMIRDANFE.BMP');
  FescolheImagemBotao.PegaFiguraBotaopequeno(btnOpcoes,'BOTAOMENUNFE.BMP');
  FescolheImagemBotao.PegaFiguraBotaopequeno(btnSair,'BOTAOSAIR.BMP');

  desabilitaLabels();


  if (self.Tag <> -1) then
  begin

    self.ObjNFE.LocalizaCodigo(IntToStr(Self.tag));
    self.ObjNFE.TabelaparaObjeto;
    self.ObjetoParaControles;

  end;


  pag1.TabIndex:=0;


end;

procedure TFnotaFiscalEletronica.btnPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           FpesquisaLocal:=Tfpesquisa.create(Self);

            if (FpesquisaLocal.PreparaPesquisa(Self.ObjNFE.Get_Pesquisa,Self.ObjNFE.Get_TituloPesquisa,Nil)=True) then
            begin
                      Try
                      
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin


                                  if (Self.ObjNFE.status <> dsinactive)
                                  then exit;

                                  If (Self.ObjNFE.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjNFE.ZerarTabela;

                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                                  pag1.TabIndex:=0;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;

end;

procedure TFnotaFiscalEletronica.PassaObjeto(Objeto: TobjNfe);
begin

  self.ObjNFE:=Objeto;

end;

function TFnotaFiscalEletronica.TabelaParaControles: Boolean;
begin

     if (Self.ObjNFE.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;

     if (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;

     Result:=True;

end;

function TFnotaFiscalEletronica.ObjetoParaControles: Boolean;
var
  str:string;
begin

  Try

     With Self.ObjNFE do
     Begin

        lbcodigo.Caption:=Get_CODIGO;

        atualizaCOmboStatus (Get_STATUSNOTA);

        edtCertificado.text:=Get_CERTIFICADO;
        edtReciboEnvio.Text:=Get_reciboenvio;

        edtProtocoloInutilizacao.text:=Get_PROTOCOLOinutilizacao;
        edtArquivoInutilizacao.text:=Get_ARQUIVOinutilizacao;

        {edtdatainutilizacao.text:=Get_Datainutilizacao;}
         lbDataInutilizacao.Caption := Get_Datainutilizacao;

        edtMotivoInutilizacao.Text:=Get_Motivoinutilizacao;

        edtProtocoloCancelamento.text:=Get_PROTOCOLOcancelamento;
        edtArquivoCancelamento.text:=Get_ARQUIVOcancelamento;

        {edtdatacancelamento.text:=Get_Datacancelamento;}
        lbDataCancelamento.Caption:=Get_DataCancelamento;

        EdtMotivoCancelamento.Text:=Get_Motivocancelamento;
        edtChaveAcesso.Text:=get_chave_acesso;
        edtArquivoXML.Text :=ObjNFE.get_arquivo_xml;

        habilitaLabels;
        result:=True;
     End;

  Except

        Result:=False;

  End;

end;

procedure TFnotaFiscalEletronica.FormClose(Sender: TObject;var Action: TCloseAction);
begin

     self.ObjNFE.Free;
     self.ObjNfObjetos.Free;
     FreeAndNil(objtransmitenfe);

     if (self.objExtraiXML <> nil) then
      self.objExtraiXML.free;

end;

procedure TFnotaFiscalEletronica.atualizaCOmboStatus(str: string);
begin

  str := UpperCase (str);

  if (str = 'P') then
    btConfirmaStatusNota.Enabled := true
  else
    btConfirmaStatusNota.Enabled := False;

  if (str = 'P') then
     str := 'Em Processamento'

  else if (str = 'G') then
     str := 'Gerada'

  else if (str = 'T') then
    str := 'Conting�ncia'

  else if (str = 'A') then
    str := 'Aberta para uso'

  else if (str = 'Z') then
    str := 'Inutilizada'

  else if (str = 'C') then
    str := 'Cancelada'
  else if (str = 'D') then
    str := 'Denegada o uso';

  lbStatus.Caption := str;

end;

procedure TFnotaFiscalEletronica.desabilitaLabels;
begin

  lbStatus.Visible:=False;
  lbDataCancelamento.Visible:=False;
  lbDataInutilizacao.Visible:=False;
  lbCodigo.Visible:=False;

end;

procedure TFnotaFiscalEletronica.habilitaLabels;
begin

  lbStatus.Visible:=true;
  lbDataCancelamento.Visible:=true;
  lbDataInutilizacao.Visible:=true;
  lbCodigo.Visible:=true;

end;

procedure TFnotaFiscalEletronica.limpaEdits;
begin

  edtCertificado.Text:='';
  edtArquivoXML.Text:='';
  edtArquivoEnvioResposta.Text:='';
  edtReciboEnvio.Text:='';
  edtChaveAcesso.Text:='';

  edtProtocoloCancelamento.Text:='';
  edtArquivoCancelamento.Text:='';
  EdtMotivoCancelamento.Text:='';

  edtProtocoloInutilizacao.Text:='';
  edtArquivoInutilizacao.Text:='';
  edtMotivoInutilizacao.Text:='';

end;

procedure TFnotaFiscalEletronica.limpaLabels ();
begin



end;

procedure TFnotaFiscalEletronica.pag1Change(Sender: TObject);
begin

  if pag1.TabIndex = 1 then
  begin

      if (edtArquivoXML.Text = '') and (edtArquivoCancelamento.Text = '') and (edtArquivoInutilizacao.Text = '') then
      begin

        pag1.TabIndex:=0;
        Exit;

      end;

      radioXMLenvio.OnClick(nil);

  end
  else if (pag1.TabIndex = 2) then
  begin


    esconde_mostra_Labels (false);

    ObjParametroGlobal.ValidaParametro('NFE SENHA DO EMAIL');
    edtSmtpPass.text:=ObjParametroGlobal.Get_Valor;
    ObjParametroGlobal.ValidaParametro('NFE SERVIDOR SMTP');
    edtServidorSMTP.text:=ObjParametroGlobal.Get_Valor;
    ObjParametroGlobal.ValidaParametro('NFE PORTA');
    edtPorta.text:=ObjParametroGlobal.Get_Valor;
    ObjParametroGlobal.ValidaParametro('NFE ASSUNTO DO EMAIL ENVIADO');
    edtEmailAssunto.text:=ObjParametroGlobal.Get_Valor;
    ObjParametroGlobal.ValidaParametro('NFE MENSAGEM DO EMAIL ENVIADO');
    mmoEmailMsg.text:=ObjParametroGlobal.Get_Valor;

    edtSmtpUser.text := ObjEmpresaGlobal.Get_RAZAOSOCIAL;



  end else if (pag1.TabIndex = 3) then
  begin

    MensagemAviso ('M�dulo em desenvolvimento');
    Exit;

  end



end;

procedure TFnotaFiscalEletronica.radioXMLenvioClick(Sender: TObject);
begin

   if (edtArquivoXML.Text = '') then
   begin
    self.WBResposta.Navigate(''+'arquivo.xml');
    //Self.WBResposta.Refresh2;
    Exit;
   end;

   if (Self.ObjNFE.ExtraiArquivoNFe=True) then
   begin

      if (FileExists (RetornaPastaSistema+'ARQUIVOB.xml')) then
        self.WBResposta.Navigate(RetornaPastaSistema+'ARQUIVOB.xml');

   end;

end;

procedure TFnotaFiscalEletronica.RadioButton2Click(Sender: TObject);
begin

  if (edtArquivoCancelamento.Text = '') then
  begin
    self.WBResposta.Navigate(''+'arquivo.xml');
    //Self.WBResposta.Refresh2;
    Exit;
  end;

  if (Self.ObjNFE.ExtraiArquivoCancelamentoNFe = True) then
  begin

    Self.WBResposta.Navigate(RetornaPastaSistema+'ARQUIVOCANCELAMENTOB.xml');
    
  end;
  

end;

procedure TFnotaFiscalEletronica.RadioButton3Click(Sender: TObject);
begin

  if (edtArquivoInutilizacao.Text = '') then
  begin
    self.WBResposta.Navigate(''+'arquivo.xml');
    //Self.WBResposta.Refresh2;
    Exit;
  end;

  if (Self.ObjNFE.ExtraiArquivoinutilizacaoNFe=True) then
  begin

      Self.WBResposta.Navigate(RetornaPastaSistema+'ARQUIVOinutilizacaoB.xml');
      
  end;


end;

procedure TFnotaFiscalEletronica.btnCancelaSefazClick(Sender: TObject);
var
  PArquivoCancela,PNFSistema,vAux : String;
  xmlCancelamento:TStringList;
begin
 if ( FileExists (edtArquivoCancelamento.Text) ) then
 begin
  MensagemAviso ('Esta NF-e ja possui um arquivo de cancelamento');
  Exit;
 end;

 if not(InputQuery('WebServices Cancelamento', 'Justificativa do cancelamento', vAux)) then
  exit;

  if (Length(vAux) < 15) then
  begin
    MensagemErro ('A justificativa deve ser maior ou igual a 15 caracteres');
    Exit;
  end;


 try

   Screen.Cursor := crHourGlass;

   objTransmiteNFE.cancelaNFe(edtArquivoXML.Text,vAux);

   if objTransmiteNFE.strErros.Count > 0  then
   begin
     //MensagemErro('Erro ao cancelar a NF-e. C�digo do status: '+IntToStr(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat)+'. Motivo: '+FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.xMotivo);
     MensagemErro( objTransmiteNFE.strErros.Text );
     Exit;
   end
   else
   begin

    {Deu Certo}
    {Ent�o tenho que guardar os protocolos de cancelamento e o arquivo de cancelamento}
     MensagemSucesso(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.xMotivo);
     FcomponentesNfe.MemoResp.Text :=  UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.EnvEvento.RetWS);

     //PArquivoCancela:=FComponentesNfe.ACBrNFe.NotasFiscais.Configuracoes.Arquivos.PathCan+StringReplace(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.chNFe,'Nfe','',[rfIgnoreCase])+'-can.xml'; celio0712
     PArquivoCancela:=FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathEvento+StringReplace(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.chNFe,'Nfe','',[rfIgnoreCase])+'-can.xml';
     //PArquivoCancela:=objTransmiteNFE.pathCancelamento;
     //FComponentesNfe.MemoResp.Lines.SaveToFile(PArquivoCancela);
     MensagemSucesso('NF-e Cancelada com sucesso');

     Try

        {agora preciso localizar a NF e cancelar na tabnotafiscal. OBS: o codigo da NF-e � o numero da NF
        ent�o localizo tabnotafiscal aonde nf.numero = codigoNF-e e nf.modelo = 26 (NF-e)}

        CANCELA_NF_POR_NFE(lbCodigo.Caption);

        //Localizando na TABNFE
        if (Self.ObjNfObjetos.objgeranfe.NFE.LocalizaCodigo(lbCodigo.Caption)=True) then
        Begin
          Self.ObjNfObjetos.objgeranfe.NFE.TabelaparaObjeto;
          Self.ObjNfObjetos.objgeranfe.NFE.Status:= dsedit;
          Self.ObjNfObjetos.objgeranfe.NFE.Submit_StatusNota('C');
          Self.ObjNfObjetos.objgeranfe.NFE.Submit_ARQUIVOCANCELAMENTO(PArquivoCancela);
          Self.ObjNfObjetos.objgeranfe.NFE.Submit_PROTOCOLOCANCELAMENTO((FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.nProt));
          Self.ObjNfObjetos.objgeranfe.NFE.Submit_DataCancelamento(datetostr(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.dhRegEvento));
          Self.ObjNfObjetos.objgeranfe.NFE.Submit_MotivoCancelamento(vaux);

          if (Self.ObjNfObjetos.objgeranfe.NFE.Salvar(false)=False) then
            MensagemErro('Erro na tentativa de Alterar o Status da NFe para Cancelada e guardar o protocolo');

        End;

        FDataModulo.IBTransaction.CommitRetaining;
        MensagemSucesso ('Processo concluido');

     Finally
      FDataModulo.IBTransaction.RollbackRetaining;
     End;

    if (self.ObjNFE.LocalizaCodigo (lbCodigo.Caption)) then
      Self.TabelaParaControles;



   end;

 finally
   Screen.Cursor := crDefault;
 end;


 Exit;

{Antigo}
 if ( FileExists (edtArquivoCancelamento.Text) ) then
 begin
     MensagemAviso ('Esta NF-e ja possui um arquivo de cancelamento');
     Exit;
 end;



 try

      Screen.Cursor:=crHourGlass;

      if (edtArquivoXML.Text = '') then
      begin

        MensagemAviso ('Par�metro arquivo XML vazio');
        Exit;

      end;

      Self.ObjNfObjetos.objgeranfe.ConfiguraComponente;

      FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie:=ObjEmpresaGlobal.get_certificado;


      if not (FileExists (edtArquivoXML.Text)) then
      begin

        MensagemAviso ('Arquivo XML n�o encontrado');
        Exit;

      end;

      Self.CancelaNfe(edtArquivoXML.Text);

      if (self.ObjNFE.LocalizaCodigo (lbCodigo.Caption)) then
      begin

        Self.TabelaParaControles;

      end;

 finally

    Screen.Cursor:=crDefault;

 end;

end;

procedure TFnotaFiscalEletronica.CancelaNfe(Parquivo: String);
var
  PArquivoCancela,PNFSistema,pcodigo,vAux : String;
  xmlCancelamento:TStringList;
begin

   (*  //xmlCancelamento.sa

     PNFSistema:='';
     pcodigo:='';

     FComponentesNfe.ACBrNFe.NotasFiscais.Clear;

     Self.ObjNfObjetos.objgeranfe.ConfiguraComponente;

     FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie:=ObjEmpresaGlobal.Get_certificado;


     FcomponentesNfe.ACBrNFe.NotasFiscais.Clear;
     FcomponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(Parquivo);

     if not(InputQuery('WebServices Cancelamento', 'Justificativa do cancelamento', vAux)) then
     begin

        self.Tag := -1; //erro
        exit;

     end;

     if (Length(vaux) < 15) then
     begin

        MensagemErro ('A justificativa deve ser maior ou igual a 15 caracteres');
        self.Tag := -1; //erro
        Exit;

     end;

     Pcodigo:=inttostr(FcomponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.cNF);

     if (Pcodigo='')
     then Begin
               mensagemErro('Erro! C�digo n�o encontrado');
               self.Tag := -1; //erro
               exit;
     End;

     if (MensagemPergunta('C�digo da NFe: '+pcodigo+#13+'Deseja Continuar?') = mrno)
     Then exit;


     //Procurando na TABNFE
     if (Self.ObjNfObjetos.objgeranfe.NFE.LocalizaCodigo(pcodigo)=False)
     then Begin
     
               if (MensagemPergunta('Esta Nfe n�o est� cadastrada no sistema. Certeza  que deseja cancelar?')=mrno) then
               begin

                  self.Tag := -1; //erro
                  exit;

               end
     End;

     if (Self.ObjNfObjetos.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo_por_NFE(pcodigo)=False)then
     begin

       if (MensagemPergunta('Esta NFE n�o foi gerada pelo Sistema, certeza que Deseja cancelar?')=mrno) then
       begin

          self.Tag := -1; //erro
          exit;
       end

     end;

   try

     Screen.Cursor := crHourGlass;

     Try
         FcomponentesNfe.ACBrNFe.Cancelamento(vAux);
     Except
           on e:exception do
           Begin
               mensagemErro('Erro na tentativa de cancelar a NFE'+#13+E.message);
               self.Tag := -1; //erro
               exit;
           End;
     End;

   finally

     Screen.Cursor := crDefault;

   end;

     //Deu Certo
     //Ent�o tenho que guardar os protocolos de cancelamento e o arquivo de cancelamento

     FcomponentesNfe.MemoResp.Text :=  UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.Cancelamento.RetWS);
     FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);

     PArquivoCancela:=Self.ObjNfObjetos.objgeranfe.LocalArquivoCancelamento+StringReplace(FcomponentesNfe.ACBrNFe.WebServices.Cancelamento.NFeChave,'Nfe','',[rfIgnoreCase])+'-can.xml';



     try

         try

              xmlCancelamento := TStringList.Create;
              xmlCancelamento.Text := FComponentesNfe.ACBrNFe.WebServices.Cancelamento.XML_ProcCancNFe;
              //ShowMessage (FComponentesNfe.ACBrNFe.WebServices.Cancelamento.XML_ProcCancNFe);


              xmlCancelamento.SaveToFile (PArquivoCancela);

         except

            on e:exception
            do begin

                MensagemErro ('N�o foi possivel salvar o XML cancelamento no diret�rio: '+PArquivoCancela+' Mensagem: '+e.message);

            end;

         end;

     finally

        FreeAndNil (xmlCancelamento);

     end;


     Try

        {agora preciso localizar a NF e cancelar na tabnotafiscal. OBS: o codigo da NF-e � o numero nda NF
        ent�o localizo tabnotafiscal aonde nf.numero = codigoNF-e e nf.modelo = 26 (NF-e)}


        CANCELA_NF_POR_NFE(pcodigo);

        //Localizando na TABNFE
        if (Self.ObjNfObjetos.objgeranfe.NFE.LocalizaCodigo(pcodigo)=True)
        then Begin
                  Self.ObjNfObjetos.objgeranfe.NFE.TabelaparaObjeto;
                  Self.ObjNfObjetos.objgeranfe.NFE.Status:= dsedit;
                  Self.ObjNfObjetos.objgeranfe.NFE.Submit_StatusNota('C');//cancelada
                  Self.ObjNfObjetos.objgeranfe.NFE.Submit_ARQUIVOCANCELAMENTO(PArquivoCancela);
                  Self.ObjNfObjetos.objgeranfe.NFE.Submit_PROTOCOLOCANCELAMENTO(FcomponentesNfe.ACBrNFe.WebServices.Cancelamento.Protocolo);
                  Self.ObjNfObjetos.objgeranfe.NFE.Submit_DataCancelamento(datetostr(FcomponentesNfe.ACBrNFe.WebServices.Cancelamento.DhRecbto));
                  Self.ObjNfObjetos.objgeranfe.NFE.Submit_MotivoCancelamento(vaux);

                  if (Self.ObjNfObjetos.objgeranfe.NFE.Salvar(false)=False)
                  then MensagemErro('Erro na tentativa de Alterar o Status da NFe para Cancelada e guardar o protocolo');
        End;

        FDataModulo.IBTransaction.CommitRetaining;
        MensagemSucesso ('Processo concluido');

     Finally
            FDataModulo.IBTransaction.RollbackRetaining;
     End; *)



end;

function TFnotaFiscalEletronica.CANCELA_NF_POR_NFE(pcodigoNFE: string): boolean;
var
  queryTEMP:TIBQuery;
begin

      try

        queryTEMP := TIBQuery.Create (nil);
        queryTEMP.Database := FDataModulo.IBDatabase;

        with queryTEMP do
        begin

          Close;
          SQL.Clear;
          SQL.Add ('update tabnotafiscal set situacao = '+#39+'C'+#39+' where numero = '+pcodigoNFE+' and modelo_nf = 2');

          try

            ExecSQL;
            FDataModulo.IBTransaction.CommitRetaining;
            result := True;

          except

            MensagemErro ('Erro ao alterar o status da NF para Cancelada na tabnotafiscal. Anote o n�mero da NF-e e comunique o suporte do sistema');
            result:=False;

          end;

        end;

      finally

        FreeAndNil (queryTEMP);

      end;

  

end;

procedure TFnotaFiscalEletronica.btnConsultaSefazClick(Sender: TObject);
begin
  objTransmiteNFE.consultaSituacao(edtArquivoXML.Text);
end;

procedure TFnotaFiscalEletronica.btnInutilizarFaixaClick(Sender: TObject);
var
  CNPJ:string;
  pprotocolo,Modelo, Serie, Ano, NumeroInicial, NumeroFinal, Justificativa : String;
  cont:integer;

  {gambiarra}

  {end gambiarra. Nem so de leite vive a vaca, mais nunca viverei sem a carne de uma perereca }
  
begin
  With FfiltroImp do
  Begin

    DesativaGrupos;
    Grupo01.Enabled:=True;
    LbGrupo01.caption:='Ano';

    Grupo04.Enabled:=True;
    LbGrupo04.caption:='N�mero Inicial';
    edtgrupo04.OnKeyDown :=  Self.ObjNfObjetos.objgeranfe.Nfe.edtNfeKeyDown;

    Grupo05.Enabled:=True;
    LbGrupo05.caption:='N�mero Final';
    edtgrupo05.OnKeyDown := Self.ObjNfObjetos.objgeranfe.Nfe.edtNfeKeyDown;
    showmodal;

    if (tag=0) then
      exit;

    Ano:=edtgrupo01.Text;
    modelo:='55';
    serie:='1';

    Try
      strtoint(edtgrupo04.Text);
      numeroinicial:=edtgrupo04.Text;
    Except
      Mensagemerro('N�mero Inicial Inv�lido');
      exit;
    End;

    Try
      strtoint(edtgrupo05.text);
      NumeroFinal:=edtgrupo05.Text;
    Except
      Mensagemerro('N�mero Final Inv�lido');
      exit;
    End;

    if (Strtoint(numeroinicial)>strtoint(numerofinal)) Then
    Begin
      MensagemErro('O N�mero inicial n�o pode ser maior que o n�mero final');
      exit;
    End;

  End;
     
  if not(InputQuery('WebServices Inutiliza��o ', 'Justificativa', Justificativa)) then
    exit;

  if (Length (Justificativa) < 15) then
  begin
    MensagemErro ('A justificativa deve ser maior ou igual a 15 caracteres');
    Exit;
  end;

  CNPJ := ObjEmpresaGlobal.Get_CNPJ;

  if objTransmiteNFE.inutilizaFaixa(pprotocolo,CNPJ,Justificativa,Ano,Modelo,Serie,NumeroInicial,NumeroFinal) then
  begin

    {percorrendo NF a NF para guardar data, protocolo, motivo e o arquivo de cancelamento}

    for cont:=strtoint(numeroinicial) to strtoint(numerofinal) do
    Begin

      With Self.ObjNfObjetos.objgeranfe.Nfe do
      Begin

        ZerarTabela;

        LocalizaCodigo(inttostr(cont));
        TabelaparaObjeto;
        Status:=dsEdit;


        Submit_STATUSNOTA('Z');//inutilizada
        Submit_Datainutilizacao(datetostr(now));
        Submit_Motivoinutilizacao(Justificativa);
        Submit_ARQUIVOinutilizacao(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu + numeroinicial+' - '+numerofinal+' ARQUIVOINU_E.XML');
        Submit_PROTOCOLOinutilizacao(pprotocolo);

        if (Salvar(false)) then
        begin
          {marcando as nf na tabnotafiscal como inutilizada "Z"}
          Self.inutiliza_faixa_na_tabNotaFiscal (IntToStr(cont));
        end
        else
        begin
          MensagemErro('Erro na tentativa de salvar a NFE '+get_codigo);
          FDataModulo.IBTransaction.CommitRetaining;
        end;

      End;

    End;
      MensagemAviso('Conclu�do');

  end;

end;

procedure TFnotaFiscalEletronica.inutiliza_faixa_na_tabNotaFiscal(pnf:string);
var
  data,hora:string;
begin

  data := troca(DateToStr(now),'/','.');
  hora := troca(TimeToStr(time),'/','.');


  if not exec_sql('update tabnotafiscal set situacao = ''Z'', dataemissao='+QuotedStr(data) + ',datasaida='+QuotedStr(data)+',horasaida='+QuotedStr(hora)+',nfe = '+pnf+' where numero = '+pnf+' and modelo_nf = 2') then
    MensagemErro('Erro ao alterar status para inutilizada. Contate o suporte antes de prosseguir')
  else
    FDataModulo.IBTransaction.CommitRetaining;

end;

procedure TFnotaFiscalEletronica.btnImprimirDanfeClick(Sender: TObject);
begin

   if (edtArquivoXML.Text = '') then
   begin

    MensagemAviso ('Par�metro arquivo XML vazio');
    Exit;

   end;


   if not (FileExists (edtArquivoXML.Text)) then
   begin

    MensagemAviso ('Arquivo XML n�o encontrado');
    Exit;

   end;

   self.objTransmiteNFE.imprimeDanfe(edtArquivoXML.Text);

end;

procedure TFnotaFiscalEletronica.ImprimeNFeArquivo(Parquivo: string);
var
   wnProt: TLeitor;
Begin

  try

    FComponentesNfe.ACBrNFe.NotasFiscais.Clear;

    Self.ObjNfObjetos.objgeranfe.ConfiguraComponente;
    FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie:=ObjEmpresaGlobal.Get_certificado;

    FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(Parquivo);

    wnProt:=TLeitor.Create;
    wnProt.CarregarArquivo(Parquivo);
    wnProt.Grupo:=wnProt.Arquivo;
    FcomponentesNfe.ACBrNFe.DANFE.ProtocoloNFe:=wnProt.rCampo(tcStr,'nProt');
    FcomponentesNfe.ACBrNFe.NotasFiscais.Imprimir;

  finally

    wnProt.Free;

  end;


end;

function TFnotaFiscalEletronica.MenuNfe: boolean;
begin

      FmenuNfe.PassaObjeto(Self.ObjNfObjetos);
      FmenuNfe.passaFormulario(self);
      FmenuNfe.objTransmitenfe := self.objTransmiteNFE;
      FmenuNfe.ShowModal;

end;

procedure TFnotaFiscalEletronica.btnOpcoesClick(Sender: TObject);
begin

  Self.MenuNfe;

end;

procedure TFnotaFiscalEletronica.btnSairClick(Sender: TObject);
begin

  Self.Close;

end;



procedure TFnotaFiscalEletronica.zeraStringGrid;

begin




end;


procedure TFnotaFiscalEletronica.marca(parametro: string);

begin




end;

procedure TFnotaFiscalEletronica.RadioButton4Click(Sender: TObject);
begin

  self.marca ('');

end;

procedure TFnotaFiscalEletronica.RadioButton1Click(Sender: TObject);
begin

  self.marca ('X');

end;

procedure TFnotaFiscalEletronica.esconde_mostra_Labels (parametro:boolean);

begin



end;

procedure TFnotaFiscalEletronica.atualizaICMTot (operador:string);

begin


end;

function TFnotaFiscalEletronica.formataNFe(valor: string): string;
begin

   if (Trim (valor) <> '') then
   begin

    Result :=  formata_valor (troca(valor,'.',','));

   end
   else
     result := '0';


end;

function TFnotaFiscalEletronica.atualizaICMTot(aux1, aux2, op: string): string;
begin

  try

      //aux1 :=  troca (aux1,'.',',');
      //aux2 :=  troca (aux2,'.',',');

      //aux1 := formata_valor (troca(aux1,'.',','));
      //aux2 := formata_valor (troca(aux2,'.',','));

      aux1:= tira_ponto (formata_valor (aux1));
      aux2:= tira_ponto (formata_valor (aux2));

      if (StrToCurr (aux2) >= 0) then
      begin


        if (op = '-') then
            aux1 := CurrToStr (StrToCurr (aux1) - StrToCurr (aux2))

        else
            aux1 := CurrToStr (StrToCurr (aux1) + StrToCurr (aux2));

       // result := aux1;
        result :=  (formata_valor (aux1));

      end;

  except

    MensagemErro ('Erro fun��o: atualizaICMTot(aux1, aux2, op: string): string');
    Exit;

  end;

end;

procedure TFnotaFiscalEletronica.zeraCampos;
begin



end;

procedure TFnotaFiscalEletronica.cancela;
begin

  self.zeracampos ();


  self.esconde_mostra_Labels (false);

  self.limpaStringGrid ();

end;

procedure TFnotaFiscalEletronica.BitBtn2Click(Sender: TObject);
begin

  Self.cancela ();

end;

procedure TFnotaFiscalEletronica.limpaStringGrid;
begin



end;

function TFnotaFiscalEletronica.enviaDevolucao: Boolean;


begin




end;

function TFnotaFiscalEletronica.retorna_codigo_fornecedor(pCNPJ: string): string;
var
  query:TIBQuery;
  msg:string;
begin

  query:=TIBQuery.Create (nil);
  msg:='';
  Result := '';

  try

    query.Database := FDataModulo.IBDatabase;


      query.Close;
      query.sql.Clear;
      query.sql.Add ('select codigo,razaosocial,cgc');
      query.sql.Add ('from tabfornecedor');
      query.sql.Add ('where (cgc = '+#39+pCNPJ+#39+') '+'or (cgc = '+#39+formata_CNPJ (pCNPJ)+#39+')');

      //InputBox ('','',query.sql.Text);

      query.Open;
      query.Last;
      
      if (query.RecordCount > 1) then
      begin

        query.First;
        while not (query.Eof) do
        begin

          msg :=  msg + #13+query.fieldbyname ('codigo').AsString+', '+query.fieldbyname ('razaosocial').AsString+', '+query.fieldbyname ('cgc').AsString;
          query.Next;

        end;

        MensagemAviso ('Duplicidade de CNPJ'+msg);
        Exit;

      end else
        result := query.fieldbyname ('codigo').AsString;



  finally

    FreeAndNil (query);

  end;




end;

function TFnotaFiscalEletronica.formata_CNPJ(pCNPJ:string): string;
begin

   if (Length (RetornaSoNumeros (pCNPJ)) = 14) then
   begin

    Insert ('.',pCNPJ,3);  
    Insert ('.',pCNPJ,7);
    Insert ('/',pCNPJ,11);
    Insert ('-',pCNPJ,16);

    result := pCNPJ;

   end else
   begin

    MensagemAviso ('CNPJ inv�lido');
    Result := '';

   end;

end;

function TFnotaFiscalEletronica.pesquisa_produto: string;

begin


end;

procedure TFnotaFiscalEletronica.get_dadosProduto(var pIsento,pSubstituto:string;pCodigoProduto: string);
var
  query:TIBQuery;
begin


  query:=TIBQuery.Create (nil);

  try
    query.Database := FDataModulo.IBDatabase;

    query.Close;
    query.SQL.Clear;
    query.SQL.Add ('select icms_isento_consumidorfinal,icms_substituto');
    query.SQL.Add ('from tabproduto');
    query.SQL.Add ('where codigo = '+pCodigoProduto);

    query.Open;

    pIsento     := query.FieldByname ('icms_isento_consumidorfinal').asstring;
    pSubstituto := query.FieldByname ('icms_substituto').AsString;

  finally

    FreeAndNil (query);

  end;


end;

procedure TFnotaFiscalEletronica.BitBtn1Click(Sender: TObject);
begin

  if not (self.enviaDevolucao ()) then
    MensagemErro ('Processo n�o concluido')

  else
    MensagemSucesso ('Processo concluido');

end;

function TFnotaFiscalEletronica.pesquisa_cfop: string;
var
   FpesquisaLocal:Tfpesquisa;
   query:tibquery;
   pesquisa:string;
begin

   result := '';
   pesquisa := 'select * from tabcfop';

   try

      Fpesquisalocal:=Tfpesquisa.create(nil);

      if (FpesquisaLocal.PreparaPesquisa (pesquisa,'Pesquisa Produtos',nil) = True) then
      begin

          try

            if (FpesquisaLocal.showmodal = mrok) then
                result := FpesquisaLocal.QueryPesq.fieldbyname ('codigo').asstring;

          finally

            FpesquisaLocal.QueryPesq.close;

          end;

      end;

   finally

      FreeandNil (FPesquisaLocal);

   end;

end;

function TFnotaFiscalEletronica.Valida_dados_NF: Boolean;

begin



end;

function TFnotaFiscalEletronica.enviaDevolucao(Pnota:string;PmodoContigencia, PForcaProcessamento: boolean): Boolean;  {(nota,false,False)}
begin



end;

function TFnotaFiscalEletronica.get_orig (indiceProduto:integer): TpcnOrigemMercadoria;
var
  st:Integer;
begin

   st := StrToInt (objExtraiXML.produtos[indiceProduto].ICMS.orig);

   if (st = 0) then
       Result := oeNacional

   else if (st = 1) then
       Result := oeEstrangeiraImportacaoDireta

   else if (st = 2) then
       Result := oeEstrangeiraAdquiridaBrasil
   else
       Result := oeNacional;

end;

function TFnotaFiscalEletronica.get_cst_icms(indiceProduto: integer): TpcnCSTIcms;
var
  st:Integer;
begin

  st := StrToInt (objExtraiXML.produtos[indiceProduto].ICMS.CST);

  case st of

    0:  result := cst00;
    10: result := cst10;
    20: result := cst20;
    30: result := cst30;
    40: result := cst40;
    41: result := cst41;
    50: result := cst50;
    51: result := cst51;
    60: result := cst60;
    70: result := cst70;
    90: result := cst90;

  else

    begin

      MensagemErro('C�digo da Situa��o Tribut�ria do ICMS Inv�lida');
      exit;
    end

  end;

end;

function TFnotaFiscalEletronica.get_modalidade_bc(indiceProduto: integer): TpcnDeterminacaoBaseIcms;
var
  modalidade:integer;
begin

  modalidade := StrToInt (objExtraiXML.produtos[indiceProduto].ICMS.modBC);

  if (modalidade = 0) then
    result := dbiMargemValorAgregado
  else if (modalidade = 1) then
    result := dbiPauta
  else if (modalidade = 2) then
    result := dbiPrecoTabelado
  else if (modalidade = 3) then
    Result := dbiValorOperacao;

end;

function TFnotaFiscalEletronica.get_modalidade_bc_cst(indiceProduto: integer): TpcnDeterminacaoBaseIcmsST;
var
  modalidade:integer;
begin

  modalidade := StrToInt (objExtraiXML.produtos[indiceProduto].ICMS.modBCST);

  if (modalidade = 0) then
    result := dbisPrecoTabelado
  else if (modalidade = 1) then
    result := dbisListaNegativa
  else if (modalidade = 2) then
    result := dbisListaPositiva
  else if (modalidade = 3) then
    Result := dbisListaNeutra
  else if (modalidade = 4) then
    result := dbisMargemValorAgregado
  else if (modalidade = 5) then
    result := dbisPauta;


end;

function TFnotaFiscalEletronica.formata(valor: string): string;
begin

    if (Trim (valor) = '') then
      valor:='0.00';

    Result :=  formata_valor (troca(valor,'.',','));
    Result :=  tira_ponto (formata_valor (Result));

end;

function TFnotaFiscalEletronica.get_cst_pis(indiceProduto: integer): TpcnCstPis;
var
  st:Integer;
begin

  st := StrToInt (objExtraiXML.produtos[indiceProduto].PIS.CST);

  case st of

    01: result := pis01;
    02: result := pis02;
    03: result := pis03;
    04: result := pis04; {obs - atualizar para o pis 05}
    06: result := pis06;
    07: result := pis07;
    08: result := pis08;
    09: result := pis09;
    49: result := pis49;
    50: result := pis50;
    51: result := pis51;
    52: result := pis52;
    53: result := pis53;
    54: result := pis54;
    55: result := pis55;
    56: result := pis56;
    60: result := pis60;
    61: result := pis61;
    62: result := pis62;
    63: result := pis63;
    64: result := pis64;
    65: result := pis65;
    66: result := pis66;
    67: result := pis67;
    70: result := pis70;
    71: result := pis71;
    72: result := pis72;
    73: result := pis73;
    74: result := pis74;
    75: result := pis75;
    98: result := pis98;
    99: result := pis99;

  else

    begin

      MensagemErro('C�digo da Situa��o Tribut�ria do PIS Inv�lida');
      exit;
    end

  end;

  

end;

function TFnotaFiscalEletronica.get_cst_cofins(indiceProduto: integer): TpcnCstCofins;
var
  st:Integer;
begin

  st := StrToInt (objExtraiXML.produtos[indiceProduto].COFINS.CST);

  case st of

    01: result := cof01;
    02: result := cof02;
    03: result := cof03;
    04: result := cof04;
    06: result := cof06;
    07: result := cof07;
    08: result := cof08;
    09: result := cof09;
    49: result := cof49;
    50: result := cof50;
    51: result := cof51;
    52: result := cof52;
    53: result := cof53;
    54: result := cof54;
    55: result := cof55;
    56: result := cof56;
    60: result := cof60;
    61: result := cof61;
    62: result := cof62;
    63: result := cof63;
    64: result := cof64;
    65: result := cof65;
    66: result := cof66;
    67: result := cof67;
    70: result := cof70;
    71: result := cof71;
    72: result := cof72;
    73: result := cof73;
    74: result := cof74;
    75: result := cof75;
    98: result := cof98;
    99: result := cof99;

  else

    begin

      MensagemErro('C�digo da Situa��o Tribut�ria do COFINS Inv�lida');
      exit;
    end

  end;

end;

procedure TFnotaFiscalEletronica.retorna_nf(pnf: string);
begin


end;

{function TFnotaFiscalEletronica.existe_refnfe(var pObservacao: string): Boolean;
var
  query:TIBQuery;
begin

  result := true;

  query:=TIBQuery.Create (nil);

  try

    query.Database:=FDataModulo.IBDatabase;
    query.Close;
    query.SQL.Clear;
    query.SQL.Add ('select codigo,numero');
    query.SQL.Add ('from tabnotafiscal');
    query.SQL.Add ('where (observacoes = '+#39+pObservacao+#39+') and (situacao = ''I'')');

    //InputBox('','',query.SQL.Text);


    try
      query.Open;
      query.Last;

      if (query.RecordCount > 0) then
      begin
        result := true;
        pObservacao := query.fieldbyname('codigo').AsString;
      end else
        result := False;

    except

      MensagemErro ('Erro: fun��o existe_refnfe (UnotaFiscalEletronica)');
      Result := True;

    end;


  finally

    FreeAndNil (query);

  end;

end;  }

procedure TFnotaFiscalEletronica.btAjudaClick(Sender: TObject);
begin
      FAjuda.PassaAjuda('NOTA FISCAL ELETRONICA');
      FAjuda.ShowModal;
end;

procedure TFnotaFiscalEletronica.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   if (Key = VK_F2) then
   begin

         FAjuda.PassaAjuda('NOTA FISCAL ELETRONICA');
         FAjuda.ShowModal;
   end;
end;

procedure TFnotaFiscalEletronica.cancelaNFeAntigo;
var
  PArquivoCancela,PNFSistema,vAux : String;
begin

 (*if ( FileExists (edtArquivoCancelamento.Text) ) then
 begin
  MensagemAviso ('Esta NF-e ja possui um arquivo de cancelamento');
  Exit;
 end;

 if not(InputQuery('WebServices Cancelamento', 'Justificativa do cancelamento', vAux)) then
  exit;

  //if (Length(vAux) < 15) then
  //begin
    //MensagemErro ('A justificativa deve ser maior ou igual a 15 caracteres');
    //Exit;
  //end;


 try

   Screen.Cursor := crHourGlass;

   if not objTransmiteNFE.cancelaAntigo(edtArquivoXML.Text,vAux) then
   begin
    Exit;
   end
   else
   begin

    {Deu Certo}
    PArquivoCancela:=FComponentesNfe.ACBrNFe.NotasFiscais.Configuracoes.Arquivos.PathCan+StringReplace(FcomponentesNfe.ACBrNFe.WebServices.Cancelamento.NFeChave,'Nfe','',[rfIgnoreCase])+'-can.xml';


     Try

        {agora preciso localizar a NF e cancelar na tabnotafiscal. OBS: o codigo da NF-e � o numero da NF
        ent�o localizo tabnotafiscal aonde nf.numero = codigoNF-e e nf.modelo = 26 (NF-e)}

        CANCELA_NF_POR_NFE(lbCodigo.Caption);

        //Localizando na TABNFE
        if (Self.ObjNfObjetos.objgeranfe.NFE.LocalizaCodigo(lbCodigo.Caption)=True) then
        Begin
          Self.ObjNfObjetos.objgeranfe.NFE.TabelaparaObjeto;
          Self.ObjNfObjetos.objgeranfe.NFE.Status:= dsedit;
          Self.ObjNfObjetos.objgeranfe.NFE.Submit_StatusNota('C');
          Self.ObjNfObjetos.objgeranfe.NFE.Submit_ARQUIVOCANCELAMENTO(PArquivoCancela);
          Self.ObjNfObjetos.objgeranfe.NFE.Submit_PROTOCOLOCANCELAMENTO(FcomponentesNfe.ACBrNFe.WebServices.Cancelamento.Protocolo);
          Self.ObjNfObjetos.objgeranfe.NFE.Submit_DataCancelamento(datetostr(FcomponentesNfe.ACBrNFe.WebServices.Cancelamento.DhRecbto));
          Self.ObjNfObjetos.objgeranfe.NFE.Submit_MotivoCancelamento(vaux);

          if (Self.ObjNfObjetos.objgeranfe.NFE.Salvar(false)=False) then
            MensagemErro('Erro na tentativa de Alterar o Status da NFe para Cancelada e guardar o protocolo');

        End;

        FDataModulo.IBTransaction.CommitRetaining;

     Finally
      FDataModulo.IBTransaction.RollbackRetaining;
     End;

    if (self.ObjNFE.LocalizaCodigo (lbCodigo.Caption)) then
      Self.TabelaParaControles;

   end;

 finally
   Screen.Cursor := crDefault;
 end;  *)


end;



procedure TFnotaFiscalEletronica.Label47Click(Sender: TObject);
begin
  self.cancelaNFeAntigo;
end;

procedure TFnotaFiscalEletronica.btn3Click(Sender: TObject);
var
  conexaoSegura:string;
  vrSenha:string;
begin

  try

    ObjParametroGlobal.ValidaParametro('NFE SENHA DO EMAIL');
    vrSenha:=ObjParametroGlobal.Get_Valor;

    exec_sql('update TABPARAMETROS set valor = '+#39+edtServidorSMTP.Text+#39+' where nome = ''NFE SERVIDOR SMTP''');
    exec_sql('update TABPARAMETROS set valor = '+#39+edtPorta.Text+#39+' where nome = ''NFE PORTA''');
    exec_sql('update TABPARAMETROS set valor = '+#39+edtEmailAssunto.Text+#39+' where nome = ''NFE ASSUNTO DO EMAIL ENVIADO''');
    exec_sql('update TABPARAMETROS set valor = '+#39+mmoEmailMsg.Text+#39+' where nome = ''NFE ASSUNTO DO EMAIL ENVIADO''');
    exec_sql('update TABEMPRESA    set email = '+#39+edtSmtpUser.Text+#39);

    if (vrSenha <> edtSmtpPass.Text) then
      exec_sql('update TABPARAMETROS set valor = '+#39+embaralha_desembaralha(edtSmtpPass.Text)+#39+' where nome = '+#39+'NFE SENHA DO EMAIL'+#39);


    if (chkEmailSSL.Checked) then
      conexaoSegura:='SIM'
    else
      conexaoSegura:='NAO';

    exec_sql('update TABPARAMETROS set valor = '+#39+conexaoSegura+#39+' where nome = ''NFE SMTP CONEXAO SEGURA''');

  except

    MensagemErro('N�o foi possivel salvar altera��es');
    FDataModulo.IBTransaction.RollbackRetaining;

  end;

  FDataModulo.IBTransaction.CommitRetaining;
  MensagemSucesso('Configura��o atualizada');

end;

procedure TFnotaFiscalEletronica.btn2Click(Sender: TObject);
var
  ssPass:string;
begin

  ssPass:=embaralha_desembaralha (edtSmtpPass.Text);

  try

    Screen.Cursor:=crHourGlass;
    objTransmiteNFE.enviarEmail (edtAnexo.Text,edtServidorSMTP.Text,edtPorta.Text,edtSmtpUser.Text
                                                             ,ssPass,edtSmtpUser.Text,edtPara.Text
                                                             ,edtEmailAssunto.Text,mmoEmailMsg.Lines
                                                             ,chkEmailSSL.Checked // SSL - Conex�o Segura
                                                             ,True //Enviar PDF junto
                                                             ,nil //Lista com emails que ser�o enviado c�pias - TStrings
                                                             ,nil // Lista de anexos - TStrings
                                                             ,true  //Pede confirma��o de leitura do email
                                                             ,true  //Aguarda Envio do Email(n�o usa thread)
                                                             ,ObjEmpresaGlobal.Get_RAZAOSOCIAL // Nome do Rementente
                                                             ,chkEmailSSL.Checked); // Auto TLS

  finally

    Screen.Cursor:=crDefault;

  end;

end;


procedure TFnotaFiscalEletronica.btConfirmaStatusNotaClick(Sender: TObject);
var
  temp: TStream;
  qry:TIBQuery;
  xml: TStringList;
  codigoNF:string;
  cstat:Integer;
begin

  if Trim( lbCodigo.Caption ) = '' then
    Exit;
  try
    qry := TIBQuery.Create(nil);
    qry.Database := FDataModulo.IBDatabase;

    qry.Close;
    qry.SQL.Text := 'select arquivob from tabnfe where codigo=' + lbCodigo.Caption;
    qry.Open;

    temp := qry.CreateBlobStream( qry.Fields[0] ,bmRead);

    xml := TStringList.Create;
    xml.LoadFromStream( temp );

    cstat := objTransmiteNFE.consultaNotaProcessada( xml.Text );
    if (cstat=100) or (cstat=205) or (cstat=302) then
    begin
      ObjNFE.Status := dsEdit;
      if cstat=100 then
        ObjNFE.Submit_STATUSNOTA('G')
      else
        ObjNFE.Submit_STATUSNOTA('D');

      //ObjNFE.submit_arquivo_xml(); j� esta preenchido, no objeto para tabela carrega o xml do arquivo
      if not ObjNFE.Salvar( false ) then
      begin
        MensagemErro('N�o foi poss�vel alterar o status da nota para impresso. Contate o suporte');
        Exit;
      end;

      codigoNF := get_campoTabela('codigo','nfe','tabnotafiscal',lbCodigo.Caption);
      if cstat=100 then
        exec_sql(' update tabnotafiscal set situacao = ' + QuotedStr('I') + ' where codigo='+codigoNF )
      else
        exec_sql(' update tabnotafiscal set situacao = ' + QuotedStr('D') + ' where codigo='+codigoNF );

      FDataModulo.IBTransaction.Commit;
      Self.ObjNFE.LocalizaCodigo( lbCodigo.Caption );
      Self.ObjNFE.TabelaparaObjeto;
      ObjetoParaControles;
    end;

  finally
    if Assigned( temp ) then
      FreeAndNil( temp );
    if Assigned( qry ) then
      FreeAndNil( qry );
    if Assigned( xml ) then
      FreeAndNil( xml );
  end;
end;

procedure TFnotaFiscalEletronica.SpeedButton2Click(Sender: TObject);
begin
  if not (FileExists(edtArquivoXML.Text)) and Not (DirectoryExists(edtArquivoXML.Text)) then
    MensagemAviso('O arquivo ou diret�rio n�o existe')
  else
    ShellExecute(Application.Handle,PChar('open'),PChar('explorer.exe'),PChar(edtArquivoXML.Text),nil,SW_SHOWMAXIMIZED);
end;

end.



