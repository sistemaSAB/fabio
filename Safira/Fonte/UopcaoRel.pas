unit UopcaoRel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons;

type
  TFOpcaorel = class(TForm)
    RgOpcoes: TRadioGroup;
    pnl3: TPanel;
    btCancelar: TSpeedButton;
    pnlRodape: TPanel;
    Img1: TImage;
    pnl1: TPanel;
    btRelatorios: TSpeedButton;
    lb1: TLabel;
    lbdata: TLabel;
    procedure BtrelatoriosClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FOpcaorel: TFOpcaorel;

implementation

uses UessencialGlobal, UescolheImagemBotao;

{$R *.DFM}

procedure TFOpcaorel.BtrelatoriosClick(Sender: TObject);
begin
     tag:=1;
     close;
end;

procedure TFOpcaorel.BtCancelarClick(Sender: TObject);
begin
     tag:=0;
     close;
End;

procedure TFOpcaorel.FormActivate(Sender: TObject);
var
cont:Integer;
apoio:STring;
begin
     tag:=0;
     RgOpcoes.ItemIndex:=0;
    // PegaFiguraBotoes(nil,nil,btcancelar,nil,nil,btrelatorios,nil,nil);
     FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');
     lbdata.Caption:= DiaSemana(Now);
     lbdata.Caption:=lbdata.Caption+', '+FormatDateTime('dd "de" mmmm "de" yyyy', Now);    
     If(RgOpcoes.Items.Count=0)
     Then exit;

     //criando o atalho para as opcoes
     for cont:=0 to RgOpcoes.items.count -1 do
     Begin
          if Pos('&',RgOpcoes.items[cont])=0
          Then Begin
                    apoio:='';
                    apoio:='&'+Inttostr(cont+1)+' - '+RgOpcoes.items[cont];
                    RgOpcoes.items[cont]:='';
                    RgOpcoes.items[cont]:=apoio;
          End;
     End;

end;

end.
