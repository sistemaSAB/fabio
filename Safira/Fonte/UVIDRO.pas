unit UVIDRO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjVIDROCOR,
  UessencialGlobal, Tabs, Grids, DBGrids, Mask,uobjvidro_icms, ComCtrls,
  UFRImposto_ICMS,UFORNECEDOR,UGRUPOVIDRO,UCOR,UpesquisaMenu,UErrosDesativarMateriais,
  ImgList,ClipBrd;

type
  TFVIDRO = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb4: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    bt1: TSpeedButton;
    Guia: TPageControl;
    ts1: TTabSheet;
    panel2: TPanel;
    lb2: TLabel;
    edtcodigo2: TEdit;
    edtReferencia: TEdit;
    lbLbReferencia: TLabel;
    lbLbGrupoVidro: TLabel;
    edtGrupoVidro: TEdit;
    edtDescricao: TEdit;
    lbLbDescricao: TLabel;
    lbLbUnidade: TLabel;
    edtUnidade: TEdit;
    edtFornecedor: TEdit;
    lbLbFornecedor: TLabel;
    lbLbPeso: TLabel;
    lbLbPrecoCusto: TLabel;
    lb1: TLabel;
    lb3: TLabel;
    edtEspessura: TEdit;
    edtNCM: TEdit;
    edtPrecoCusto: TEdit;
    edtPeso: TEdit;
    lb5: TLabel;
    grpGroupMargem: TGroupBox;
    lbLbPorcentagemFornecido: TLabel;
    lbLbPorcentagemRetirado: TLabel;
    lbLbPorcentagemInstalado: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    lb8: TLabel;
    edtPorcentagemInstalado: TEdit;
    edtPorcentagemFornecido: TEdit;
    edtPorcentagemRetirado: TEdit;
    grpGroupPrecoVenda: TGroupBox;
    lbLbPrecoVendaInstalado: TLabel;
    lbLbPrecoVendaFornecido: TLabel;
    lbLbPrecoVendaRetirado: TLabel;
    lb9: TLabel;
    lb10: TLabel;
    lb11: TLabel;
    edtPrecoVendaInstalado: TEdit;
    edtPrecoVendaFornecido: TEdit;
    edtPrecoVendaRetirado: TEdit;
    lbLbAreaMinima: TLabel;
    lbLbArredondamento: TLabel;
    edtArredondamento: TEdit;
    edtAreaMinima: TEdit;
    chkAtivo: TCheckBox;
    lbAtivoInativo: TLabel;
    btUltimo: TSpeedButton;
    btProximo: TSpeedButton;
    btAnterior: TSpeedButton;
    btPrimeiro: TSpeedButton;
    ts2: TTabSheet;
    panel3: TPanel;
    lbLbClassificaoFiscal: TLabel;
    edtClassificacaoFiscal: TEdit;
    FRImposto_ICMS1: TFRImposto_ICMS;
    panelICMSEstado1: TPanel;
    lbLbAliquota_ICMS_Estado: TLabel;
    lbLbReducao_BC_ICMS_Estado: TLabel;
    lbLbAliquota_ICMS_Cupom_Estado: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    lb14: TLabel;
    lb15: TLabel;
    edtAliquota_ICMS_Estado: TEdit;
    edtReducao_BC_ICMS_Estado: TEdit;
    edtAliquota_ICMS_Cupom_Estado: TEdit;
    COMBOIsentoicms_estado: TComboBox;
    COMBOSUBSTITUICAOICMS_ESTADO: TComboBox;
    edtVALORPAUTA_SUB_TRIB_ESTADO: TEdit;
    panelICMSForaEstado1: TPanel;
    lb16: TLabel;
    lb17: TLabel;
    lb18: TLabel;
    lb19: TLabel;
    lb20: TLabel;
    lb21: TLabel;
    lb22: TLabel;
    edtAliquota_ICMS_ForaEstado: TEdit;
    edtReducao_BC_ICMS_ForaEstado: TEdit;
    edtAliquota_ICMS_Cupom_ForaEstado: TEdit;
    COMBOisentoicms_foraestado: TComboBox;
    COMBOSubstituicaoICMS_ForaEstado: TComboBox;
    edtVALORPAUTA_SUB_TRIB_FORAESTADO: TEdit;
    edtpercentualagregado: TEdit;
    lb23: TLabel;
    edtSituacaoTributaria_TabelaA: TEdit;
    lbLbSituacaoTributaria_TabelaA: TLabel;
    edtSituacaoTributaria_TabelaB: TEdit;
    ts3: TTabSheet;
    panelCor: TPanel;
    lbLbCor: TLabel;
    lbnomecor: TLabel;
    lbEstoque: TLabel;
    lb24: TLabel;
    lb25: TLabel;
    lb26: TLabel;
    lbCusto: TLabel;
    lb27: TLabel;
    lb28: TLabel;
    edtCorReferencia: TEdit;
    edtcodigoCOR: TEdit;
    grpVidros: TGroupBox;
    lbLbPorcentagemAcrescimo: TLabel;
    lbLbAcrescimoExtra: TLabel;
    lbLbPorcentagemAcrescimoFinal: TLabel;
    lb29: TLabel;
    lb30: TLabel;
    lb31: TLabel;
    edtPorcentagemAcrescimo: TEdit;
    edtAcrescimoExtra: TEdit;
    edtPorcentagemAcrescimoFinal: TEdit;
    edtValorExtra: TEdit;
    edtacrescimoicms: TEdit;
    edtporcentagemacrescimoicms: TEdit;
    edtCor: TEdit;
    edtvalorfinal: TEdit;
    edtValorCusto: TEdit;
    edtinstaladofinal: TEdit;
    edtfornecidofinal: TEdit;
    edtretiradofinal: TEdit;
    rg_forma_de_calculo_percentual: TRadioGroup;
    edtclassificacaofiscal_cor: TEdit;
    panelpnl2: TPanel;
    btgravarcor: TBitBtn;
    btexcluircor: TBitBtn;
    btcancelacor: TBitBtn;
    dbgridCOR: TDBGrid;
    lbNomeFornecedor: TLabel;
    lbNomeGrupoVidro: TLabel;
    il1: TImageList;
    Label1: TLabel;
    edtCest: TEdit;
    Label2: TLabel;
    edtEstoqueMinimo: TEdit;
    procedure EdtGrupoVidroKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure EdtGrupoVidroExit(Sender: TObject);
    procedure EdtFornecedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure EdtFornecedorExit(Sender: TObject);
    procedure edtSituacaoTributaria_TabelaAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtSituacaoTributaria_TabelaAExit(Sender: TObject);
    procedure edtSituacaoTributaria_TabelaBKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtSituacaoTributaria_TabelaBExit(Sender: TObject);
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtPorcentagemInstaladoExit(Sender: TObject);
    procedure EdtPorcentagemFornecidoExit(Sender: TObject);
    procedure EdtPorcentagemRetiradoExit(Sender: TObject);
    procedure btGravarCorClick(Sender: TObject);
    procedure BtExcluirCorClick(Sender: TObject);
    procedure BtCancelarCorClick(Sender: TObject);
    procedure edtCorReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCorReferenciaExit(Sender: TObject);
    procedure edtAcrescimoExtraExit(Sender: TObject);
    procedure dbgridCORDblClick(Sender: TObject);
    procedure edtValorExtraExit(Sender: TObject);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure EdtPrecoCustoExit(Sender: TObject);
    procedure EdtAreaMinimaExit(Sender: TObject);
    procedure edtacrescimoicmsExit(Sender: TObject);
    procedure edtporcentagemacrescimoicmsExit(Sender: TObject);
    procedure EdtPrecoVendaInstaladoExit(Sender: TObject);
    procedure EdtPrecoVendaFornecidoExit(Sender: TObject);
    procedure EdtPrecoVendaRetiradoExit(Sender: TObject);
    procedure FRImposto_ICMS1BtGravar_material_icmsClick(Sender: TObject);
    procedure dbgridCORKeyPress(Sender: TObject; var Key: Char);
    procedure dbgridCORDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure EdtGrupoVidroDblClick(Sender: TObject);
    procedure EdtFornecedorDblClick(Sender: TObject);
    procedure edtCorReferenciaDblClick(Sender: TObject);
    procedure edtNCMKeyPress(Sender: TObject; var Key: Char);
    procedure lbEstoqueMouseLeave(Sender: TObject);
    procedure lbEstoqueMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbEstoqueClick(Sender: TObject);
    procedure edtquant(Sender: TObject;
  var Key: Char);
    procedure edtEdtGrupoVidroKeyPress(Sender: TObject; var Key: Char);
    procedure bt1Click(Sender: TObject);
    procedure chkAtivoClick(Sender: TObject);
    procedure FRImposto_ICMS1btReplicarPedidoClick(Sender: TObject);
    procedure FRImposto_ICMS1Button1Click(Sender: TObject);
    procedure FRImposto_ICMS1edttipocliente_material_icmsKeyDown(
      Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FRImposto_ICMS1edttipocliente_material_icmsExit(
      Sender: TObject);
    procedure FRImposto_ICMS1edtoperacaoKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure FRImposto_ICMS1edtoperacaoExit(Sender: TObject);
    procedure GuiaChange(Sender: TObject);
    procedure lbNomeGrupoVidroClick(Sender: TObject);
    procedure lbNomeFornecedorClick(Sender: TObject);
    procedure lbNomeGrupoVidroMouseLeave(Sender: TObject);
    procedure lbNomeGrupoVidroMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure lbnomecorClick(Sender: TObject);
    procedure edtGrupoVidroKeyPress(Sender: TObject; var Key: Char);
    procedure edtFornecedorKeyPress(Sender: TObject; var Key: Char);
    procedure edtPrecoCustoKeyPress(Sender: TObject; var Key: Char);
    procedure edtEspessuraKeyPress(Sender: TObject; var Key: Char);
    procedure edtArredondamentoKeyPress(Sender: TObject; var Key: Char);
    procedure edtAreaMinimaKeyPress(Sender: TObject; var Key: Char);
    procedure edtPorcentagemInstaladoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemFornecidoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemRetiradoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemAcrescimoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtValorExtraKeyPress(Sender: TObject; var Key: Char);
    procedure edtAcrescimoExtraKeyPress(Sender: TObject; var Key: Char);
    procedure edtacrescimoicmsKeyPress(Sender: TObject; var Key: Char);
    procedure edtporcentagemacrescimoicmsKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemAcrescimoFinalKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtNCMKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtNCMExit(Sender: TObject);
    procedure edtCestKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
 private
    AlteraPrecoPeloCusto:Boolean;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    Procedure ResgataCores;
    Procedure PreparaCor;

 private
    EstadoObjeto:string;
    PAlteraCampoEstoque:boolean;

    { Private declarations }
  public


 end;

var
  FVIDRO: TFVIDRO;
  ObjVidroCor:TObjVidroCor;

implementation

uses Upesquisa, UessencialLocal, UDataModulo, UobjVIDRO, UobjGRUPOVIDRO,
  UobjFORNECEDOR, UescolheImagemBotao, Uprincipal, UFiltraImp, UAjuda,
  UobjCEST;

{$R *.dfm}


procedure TFVIDRO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjVidroCor.Vidro=Nil)
     Then exit;

     If (ObjVidroCor.Vidro.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;


    try
      FRImposto_ICMS1.ObjMaterial_ICMS.Free;
    Except
          on e:exception do
          Begin
               mensagemerro(e.message);
          End;
    End;


    Try
        ObjVidroCor.Vidro.free;
    Except
          on e:exception do
          Begin
               mensagemerro(e.message);
          End;
    End;

    Self.Tag:=0;
end;

procedure TFVIDRO.btNovoClick(Sender: TObject);
begin
     Self.limpaLabels;
     limpaedit(Self);

     habilita_campos(Self);
     desab_botoes(Self);
     chkAtivo.Checked:=True;
     lbAtivoInativo.Caption:='Ativo';

     lbCodigo.Caption:='0';
     edtARREDONDAMENTO.text:='0';
     edtAREAMINIMA.text:='0';


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjVidroCor.Vidro.status:=dsInsert;
     Guia.TabIndex:=0;

     if (AlteraPrecoPeloCusto=True)
     Then Begin
              EdtPrecoVendaInstalado.Enabled:=false;
              EdtPrecoVendaRetirado.Enabled:=false;
              EdtPrecoVendaFornecido.Enabled:=false;
     End;
     
     EdtReferencia.setfocus;
     Self.EstadoObjeto:='INSER��O';

     EdtSituacaoTributaria_TabelaA.Text:='1';
     EdtSituacaoTributaria_TabelaB.Text:='1';
     EdtClassificacaoFiscal.Text:='1';

     comboisentoicms_estado.Text:='N�O';
     comboSUBSTITUICAOICMS_ESTADO.Text:='SIM';
     edtVALORPAUTA_SUB_TRIB_ESTADO.Text:='0';
     EdtAliquota_ICMS_Estado.Text:='0';
     EdtReducao_BC_ICMS_Estado.Text:='0';
     EdtAliquota_ICMS_Cupom_Estado.Text:='0';

     comboisentoicms_foraestado.Text:='N�O';
     comboSUBSTITUICAOICMS_foraESTADO.Text:='SIM';
     edtVALORPAUTA_SUB_TRIB_foraESTADO.Text:='0';
     EdtAliquota_ICMS_foraEstado.Text:='0';
     EdtReducao_BC_ICMS_foraEstado.Text:='0';
     EdtAliquota_ICMS_Cupom_foraEstado.Text:='0';
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

end;

procedure TFVIDRO.btSalvarClick(Sender: TObject);
begin

     If ObjVidroCor.Vidro.Status=dsInactive
     Then exit;

     If (EdtReferencia.Text<>'')then
     If (ObjVidroCOR.Vidro.VerificaReferencia(ObjVidroCor.Vidro.Status, lbCodigo.Caption, EdtReferencia.Text)=true) then
     Begin
         MensagemErro('Refer�ncia j� existe.');
         exit;
     end;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjVidroCor.Vidro.salvar(true)=False)
     Then exit;

     lbCodigo.Caption:=ObjVidroCor.Vidro.Get_codigo;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

     if (Self.EstadoObjeto='INSER��O')then
     if (ObjVidroCor.CadastraVidroEmTodasAsCores(lbCodigo.Caption)=false)then
     Begin
         MensagemErro('Erro ao tentar cadastrar essa Vidro nas cores escolhidas.');
         FDataModulo.IBTransaction.RollbackRetaining;
         Self.EstadoObjeto:='';
         exit;
     end
     else MensagemAviso('Cores Cadastradas com Sucesso !');

     FDataModulo.IBTransaction.CommitRetaining;

     Self.EstadoObjeto:='';

     habilita_botoes(Self);
     desabilita_campos(Self);

     ObjVidroCor.Vidro.LocalizaCodigo(lbCodigo.Caption);
     ObjVidroCor.Vidro.TabelaparaObjeto;
     self.ObjetoParaControles;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
      btopcoes.Visible :=true;


end;

procedure TFVIDRO.btAlterarClick(Sender: TObject);
begin
    if(lbCodigo.Caption='') then exit;
    If (ObjVidroCor.Vidro.Status=dsinactive) and (lbCodigo.Caption<>'')  and (guia.TabIndex=0)
    Then Begin
                habilita_campos(Self);
                ObjVidroCor.Vidro.Status:=dsEdit;
                guia.TabIndex:=0;
                EdtReferencia.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                
                if (AlteraPrecoPeloCusto=True)
                Then Begin
                         EdtPrecoVendaInstalado.Enabled:=false;
                         EdtPrecoVendaRetirado.Enabled:=false;
                         EdtPrecoVendaFornecido.Enabled:=false;
                End;
                ObjVidroCor.Vidro.ReferenciaAnterior:=EdtReferencia.Text;
                Self.EstadoObjeto:='EDI��O';
          End;

                btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

end;

procedure TFVIDRO.btCancelarClick(Sender: TObject);
begin
     ObjVidroCor.Vidro.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     EdtPrecoVendaInstalado.Enabled:=false;
     EdtPrecoVendaRetirado.Enabled:=false;
     EdtPrecoVendaFornecido.Enabled:=false;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
      btopcoes.Visible :=true;
      lbCodigo.Caption:=''


     

end;

procedure TFVIDRO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
            FpesquisaLocal.NomeCadastroPersonalizacao:='TFVIDRO.btPesquisarClick';
            If (FpesquisaLocal.PreparaPesquisa(ObjVidroCor.Vidro.Get_pesquisa,ObjVidroCor.Vidro.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjVidroCor.Vidro.status<>dsinactive
                                  then exit;

                                  If (ObjVidroCor.Vidro.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjVidroCor.Vidro.ZERARTABELA;
                                  Guia.TabIndex:=0;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;

procedure TFVIDRO.btExcluirClick(Sender: TObject);
begin
     If (ObjVidroCor.Vidro.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (ObjVidroCor.Vidro.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjVidroCor.Vidro.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFVIDRO.btRelatorioClick(Sender: TObject);
begin
//    ObjVidroCor.Vidro.Imprime(lbCodigo.Caption);
end;

procedure TFVIDRO.btSairClick(Sender: TObject);
begin
    Self.close;
end;


procedure TFVIDRO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    If Key=VK_Escape
    Then Self.Close;

      if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('CADASTRO DE VIDROS');
         FAjuda.ShowModal;
    end;


end;

procedure TFVIDRO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFVIDRO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjVidroCor.Vidro do
    Begin
        Submit_Codigo(lbCodigo.Caption);
        Submit_Codigo2(edtCodigo2.text);

        Submit_Referencia(edtReferencia.text);
        GrupoVidro.Submit_codigo(edtGrupoVidro.text);
        Submit_Descricao(edtDescricao.text);
        Submit_Unidade(edtUnidade.text);
        Fornecedor.Submit_codigo(edtFornecedor.text);
        Submit_Peso(edtPeso.text);
        Submit_PrecoCusto(tira_ponto(edtPrecoCusto.text));

        Submit_PorcentagemInstalado(tira_ponto(edtPorcentagemInstalado.text));
        Submit_PorcentagemFornecido(tira_ponto(edtPorcentagemFornecido.text));
        Submit_PorcentagemRetirado(tira_ponto(edtPorcentagemRetirado.text));

        Submit_precovendaInstalado(tira_ponto(edtprecovendaInstalado.text));
        Submit_precovendaFornecido(tira_ponto(edtprecovendaFornecido.text));
        Submit_precovendaRetirado(tira_ponto(edtprecovendaRetirado.text));

        Submit_Arredondamento(edtArredondamento.text);
        Submit_AreaMinima(edtAreaMinima.text);
        Submit_ClassificacaoFiscal(edtClassificacaoFiscal.text);

        //Campos que nao sao mais usados
        Submit_IsentoICMS_Estado('N');
        Submit_SubstituicaoICMS_Estado('N');
        Submit_ValorPauta_Sub_Trib_Estado('0');
        Submit_Aliquota_ICMS_Estado('0');
        Submit_Reducao_BC_ICMS_Estado('0');
        Submit_Aliquota_ICMS_Cupom_Estado('0');
        Submit_IsentoICMS_ForaEstado('N');
        Submit_SubstituicaoICMS_ForaEstado('N');
        Submit_ValorPauta_Sub_Trib_ForaEstado('0');
        Submit_Aliquota_ICMS_ForaEstado('0');
        Submit_Reducao_BC_ICMS_ForaEstado('0');
        Submit_Aliquota_ICMS_Cupom_ForaEstado('0');
        SituacaoTributaria_TabelaA.Submit_codigo('0');
        SituacaoTributaria_TabelaB.Submit_codigo('0');
        Submit_percentualagregado('0');
        Submit_ipi('0');
        Submit_NCM(edtNCM.Text);
        Submit_cest(edtCest.Text);
        Submit_Espessura(edtEspessura.Text);

        if(chkAtivo.Checked=True)
        then Submit_Ativo('S')
        else Submit_Ativo('N');
        //CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFVIDRO.LimpaLabels;
begin
    lbCodigo.Caption:='';
    LbEstoque.Caption:='';
    lbNomeGrupoVidro.caption:='';
    lbNomeFornecedor.caption:='';
end;

function TFVIDRO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjVidroCor.Vidro do
     Begin
        lbCodigo.Caption:=Get_Codigo;
        EdtCodigo2.text:=Get_Codigo2;
        EdtReferencia.text:=Get_Referencia;
        EdtGrupoVidro.text:=GrupoVidro.Get_codigo;
        LbNomeGrupoVidro.Caption:=GrupoVidro.Get_Nome;
        EdtDescricao.text:=Get_Descricao;
        EdtUnidade.text:=Get_Unidade;
        EdtFornecedor.text:=Fornecedor.Get_codigo;
        LbNomeFornecedor.Caption:=Fornecedor.Get_RazaoSocial;
        EdtPeso.text:=Get_Peso;
        EdtPrecoCusto.text:=formata_valor(Get_PrecoCusto);
        EdtPorcentagemInstalado.text:=Get_PorcentagemInstalado;
        EdtPorcentagemFornecido.text:=Get_PorcentagemFornecido;
        EdtPorcentagemRetirado.text:=Get_PorcentagemRetirado;
        EdtArredondamento.text:=Get_Arredondamento;
        EdtAreaMinima.text:=Get_AreaMinima;
        ComboIsentoICMS_Estado.text:=Get_IsentoICMS_Estado;
        ComboSubstituicaoICMS_Estado.text:=Get_SubstituicaoICMS_Estado;
        EdtValorPauta_Sub_Trib_Estado.text:=Get_ValorPauta_Sub_Trib_Estado;
        EdtAliquota_ICMS_Estado.text:=Get_Aliquota_ICMS_Estado;
        EdtReducao_BC_ICMS_Estado.text:=Get_Reducao_BC_ICMS_Estado;
        EdtAliquota_ICMS_Cupom_Estado.text:=Get_Aliquota_ICMS_Cupom_Estado;
        ComboIsentoICMS_ForaEstado.text:=Get_IsentoICMS_ForaEstado;
        ComboSubstituicaoICMS_ForaEstado.text:=Get_SubstituicaoICMS_ForaEstado;
        EdtValorPauta_Sub_Trib_ForaEstado.text:=Get_ValorPauta_Sub_Trib_ForaEstado;
        EdtAliquota_ICMS_ForaEstado.text:=Get_Aliquota_ICMS_ForaEstado;
        EdtReducao_BC_ICMS_ForaEstado.text:=Get_Reducao_BC_ICMS_ForaEstado;
        EdtAliquota_ICMS_Cupom_ForaEstado.text:=Get_Aliquota_ICMS_Cupom_ForaEstado;
        EdtSituacaoTributaria_TabelaA.text:=SituacaoTributaria_TabelaA.Get_codigo;
        EdtSituacaoTributaria_TabelaB.text:=SituacaoTributaria_TabelaB.Get_codigo;
        EdtClassificacaoFiscal.text:=Get_ClassificacaoFiscal;
        
        EdtPrecoVendaInstalado.Text:=Get_PrecoVendaInstalado;
        EdtPrecoVendaFornecido.Text:=Get_PrecoVendaFornecido;
        EdtPrecoVendaRetirado.Text:=Get_PrecoVendaRetirado;
        edtNCM.text:=Get_NCM;
        edtCest.Text := Get_cest;
        edtEspessura.text:=Get_Espessura;

        if(Get_Ativo='S') then
        begin
            chkAtivo.Checked:=True;
            lbAtivoInativo.Caption:='Ativo';
        end
        else
        begin
            chkAtivo.Checked:=False;
            lbAtivoInativo.Caption:='Inativo';
        end;


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFVIDRO.TabelaParaControles: Boolean;
begin
     If (ObjVidroCor.Vidro.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFVIDRO.EdtFornecedorExit(Sender: TObject);
begin
    ObjVidroCor.Vidro.EdtFornecedorExit(Sender, LbNomeFornecedor);
end;

procedure TFVIDRO.EdtFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    ObjVidroCor.Vidro.EdtFornecedorKeyDown(Sender, Key, shift, LbNomeFornecedor);
end;

procedure TFVIDRO.EdtGrupoVidroExit(Sender: TObject);
begin
    ObjVidroCor.Vidro.EdtGrupoVidroExit(Sender, LbNomeGrupoVidro);
end;

procedure TFVIDRO.EdtGrupoVidroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    ObjVidroCor.Vidro.EdtGrupoVidroKeyDown(Sender, Key, Shift, LbNomeGrupoVidro);
end;

procedure TFVIDRO.edtSituacaoTributaria_TabelaAExit(Sender: TObject);
begin
    ObjVidroCor.Vidro.EdtSituacaoTributaria_TabelaAExit(Sender, Nil);
end;

procedure TFVIDRO.edtSituacaoTributaria_TabelaAKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjVidroCor.Vidro.EdtSituacaoTributaria_TabelaAKeyDown(Sender, Key, Shift, Nil);
end;

procedure TFVIDRO.edtSituacaoTributaria_TabelaBExit(Sender: TObject);
begin
    ObjVidroCor.Vidro.EdtSituacaoTributaria_TabelaBExit(Sender, Nil);
end;

procedure TFVIDRO.edtSituacaoTributaria_TabelaBKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjVidroCor.Vidro.EdtSituacaoTributaria_TabelaBKeyDown(Sender, key, Shift, Nil);
end;

procedure TFVIDRO.EdtPorcentagemInstaladoExit(Sender: TObject);
Var
    PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaInstalado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemInstalado.Text))/100)));
end;

procedure TFVIDRO.EdtPorcentagemFornecidoExit(Sender: TObject);
Var
    PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaFornecido.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemFornecido.Text))/100)));
end;

procedure TFVIDRO.EdtPorcentagemRetiradoExit(Sender: TObject);
Var
    PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaRetirado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemRetirado.Text))/100)));
end;


procedure TFVIDRO.PreparaCor;
begin
     //Resgatando as Cores para essa ferragem
     Self.ResgataCores;
     //habilitando os edits desse panel
     habilita_campos(PanelCor);
     EdtPorcentagemAcrescimo.Enabled:=False;
     EdtPorcentagemAcrescimoFinal.Enabled:=False;
     EdtValorCusto.Enabled:=false;
     {if(PAlteraCampoEstoque=True)
     then EdtEstoque.Enabled:=True
     else
     EdtEstoque.Enabled:=False;    }

     lbnomecor.caption:='';
     LbCusto.caption:='Custo'+#13+'R$ '+EdtPrecoCusto.text;
end;

procedure TFVIDRO.ResgataCores;
begin
     ObjVidroCor.ResgataCorVidro(lbCodigo.Caption);
     formatadbgrid(DBGRIDCOR);
     DBGRIDCOR.Ctl3D:=False;
end;

procedure TFVIDRO.btGravarCorClick(Sender: TObject);
begin
    With ObjVidroCOR do
    Begin
        ZerarTabela;

        if (edtcodigoCOR.text='')
        or (edtcodigoCOR.text='0')
        Then Begin
                Status:=dsInsert;
                edtcodigoCOR.Text:='0';
                
        End
        Else Status:=dsEdit;

        Submit_Codigo(edtcodigoCOR.Text);
        Vidro.Submit_codigo(lbCodigo.Caption);
        Cor.Submit_codigo(edtCor.text);

        {if (EdtEstoque.text='')
        Then EdtEstoque.Text:='0';   }
        
        //Submit_Estoque(edtEstoque.text);

        Submit_PorcentagemAcrescimo(EdtPorcentagemAcrescimo.Text);
        Submit_AcrescimoExtra(edtAcrescimoExtra.text);
        Submit_ValorExtra(EdtValorExtra.Text);
        Submit_ValorFinal(EdtValorFinal.Text);
        Submit_ValorExtra(EdtValorExtra.Text);
        Submit_AcrescimoIcms(edtacrescimoicms.Text);
        Submit_PorcentagemAcrescimoIcms(edtporcentagemacrescimoicms.text);
        Submit_ValorFinal(EdtValorFinal.Text);
        Submit_ClassificacaoFiscal(edtclassificacaofiscal_cor.Text);
       // Submit_Estoque(EdtEstoque.Text);
        Submit_EstoqueMinimo( edtEstoqueMinimo.Text );

        if (Salvar(True)=False)
        Then Begin
                  EdtCorReferencia.SetFocus;
                  exit;
        End;
        lbnomecor.caption:='';
        limpaedit(panelcor);
        BtCancelarCorClick(sender);

        
        EdtCorReferencia.SetFocus;
        Self.ResgataCores;
    End;

end;

procedure TFVIDRO.BtExcluirCorClick(Sender: TObject);
begin
     if (edtcodigoCOR.Text='')then
     exit;

     If (DBGRIDCOR.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGRIDCOR.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir essa cor do Vidro Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     ObjVidroCor.Exclui(DBGRIDCOR.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     limpaedit(panelcor);
     Self.ResgataCores;

end;

procedure TFVIDRO.BtCancelarCorClick(Sender: TObject);
begin
     limpaedit(panelcor);
     EdtValorCusto.Clear;
     EdtPorcentagemAcrescimo.Clear;
     EdtValorExtra.Clear;
     EdtAcrescimoExtra.Clear;
     EdtPorcentagemAcrescimoFinal.Clear;
     EdtValorFinal.Clear;;
     lbnomecor.caption:='';
     edtacrescimoicms.text:='';
     edtporcentagemacrescimoicms.text:='';
     EdtCorReferencia.SetFocus;
end;

procedure TFVIDRO.edtCorReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   ObjVidroCor.EdtCorKeyDown(Sender, EdtCor, key, Shift, lbnomecor);
end;

procedure TFVIDRO.edtCorReferenciaExit(Sender: TObject);
begin
    try
        ObjVidroCor.EdtCor_PorcentagemCor_Exit(Sender, EdtCor, lbnomecor , EdtPorcentagemAcrescimo, EdtValorCusto);
    Except
    End;

    EdtValorCusto.Enabled:=false;
    EdtValorCusto.Text:=EdtValorCusto.text;

    grpVidros.Enabled:=true;
    edtacrescimoExtra.enabled:=true;
    EdtValorExtra.Enabled:=true;
end;

procedure TFVIDRO.edtAcrescimoExtraExit(Sender: TObject);
Var PValorCusto, PAcrescimoExtra, PValorExtra :Currency;
begin
    if ((TEdit(Sender).Text=''))then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoCusto.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaInstalado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaFornecido.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaRetirado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtCor.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;

    PValorCusto:=0;
    PAcrescimoExtra:=0;
    PValorExtra:=0;

    try
          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));

          PAcrescimoExtra:=StrTOCurr(tira_ponto(EdtAcrescimoExtra.Text));
          PValorExtra:=((PValorCusto*PAcrescimoExtra)/100);
          EdtValorExtra.Text:=Formata_Valor(PValorExtra);
    except;
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais.');
    end;

    EdtPorcentagemAcrescimoFinal.Text:=CurrToStr(StrToCurr(tira_ponto(EdtPorcentagemAcrescimo.Text))+StrToCurr(tira_ponto(EdtAcrescimoExtra.Text))+
                                            StrToCurr(tira_ponto(edtporcentagemacrescimoicms.Text)));

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(tira_ponto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));

end;

procedure TFVIDRO.dbgridCORDblClick(Sender: TObject);
begin

     If (DBGRIDCOR.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGRIDCOR.DataSource.DataSet.RecordCount=0)
     Then exit;


     limpaedit(PanelCor);
     lbnomecor.caption:='';
     if (ObjVidroCor.LocalizaCodigo(DBGRIDCOR.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataCores;
               exit;
     End;

     With ObjVidroCor do
     Begin
          TabelaparaObjeto;
          edtcodigoCOR.text:=Get_Codigo;
          edtclassificacaofiscal_cor.text:=Get_ClassificacaoFiscal;

          EdtCorReferencia.Text:=Cor.Get_Referencia;
          EdtCor.text:=Cor.Get_codigo;
          lbnomecor.caption:=Cor.Get_Descricao;
          EdtPorcentagemAcrescimo.text:=Get_PorcentagemAcrescimo;
          //EdtEstoque.text:=RetornaEstoque;
          LbEstoque.Caption:=RetornaEstoque+' '+Vidro.Get_Unidade;
          edtEstoqueMinimo.Text := Get_EstoqueMinimo;
          
          EdtAcrescimoExtra.text:=Get_AcrescimoExtra;
          EdtPorcentagemAcrescimoFinal.text:=Get_PorcentagemAcrescimoFinal;
          GrpVidros.Enabled:=true;

          EdtValorCusto.Enabled:=false;
          EdtValorExtra.Enabled:=true;
          EdtAcrescimoExtra.Enabled:=true;
          EdtValorExtra.Text:=formata_valor(Get_ValorExtra);
          EdtValorFinal.Text:=formata_valor(Get_ValorFinal);
          EdtValorCusto.Text:=formata_valor(Vidro.Get_PrecoCusto);
          edtacrescimoicms.Text:=formata_valor(Get_AcrescimoIcms);
          edtporcentagemacrescimoicms.Text:=formata_valor(Get_PorcentagemAcrescimoIcms);
          edtacrescimoicms.Enabled:=true;
          edtporcentagemacrescimoicms.Enabled:=true;

          edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
          edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
          edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
          
          EdtCorReferencia.SetFocus;
     End;

end;

procedure TFVIDRO.edtValorExtraExit(Sender: TObject);
Var
PValorCusto, PPorcentagemExtra, PValorExtra : Currency;
begin

    if ((TEdit(Sender).Text=''))then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoCusto.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaInstalado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaFornecido.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtPrecoVendaRetirado.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;
    if (EdtCor.Text='')then
    begin
      TEdit(Sender).Text:='';
      exit;
    end;

    TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);

    PValorCusto:=0;
    PValorExtra:=0;
    PPorcentagemExtra:=0;

    try


          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));

          PValorExtra:=StrToCurr(Tira_Ponto(EdtValorExtra.Text));

          PPorcentagemExtra:=(PValorExtra*100)/PValorCusto;
          EdtAcrescimoExtra.Text:=FormatFloat('#,##0.00',PPorcentagemExtra);

          if (edtacrescimoicms.text='') and (edtporcentagemacrescimoicms.text='')
          then Begin
                    edtacrescimoicms.text:='0';
                    edtporcentagemacrescimoicms.text:='0';
          End;

    except
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais');
    end;



    
    //********** Atualiza Valor Final, s� pra mostra porque este campo � Computed By
    EdtPorcentagemAcrescimoFinal.Text :=formata_valor(StrToCurr(Tira_Ponto(EdtPorcentagemAcrescimo.Text))+
                                      StrToCurr(tira_ponto(EdtAcrescimoExtra.Text))+
                                      StrToCurr(tira_ponto(edtporcentagemacrescimoicms.Text))
                                      );

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(Tira_POnto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));

(*

Antes era calculado sobre o Custo.
Exemplo
O Vidro incolor custa 100,00, sendo assim
os demais ser�o calculados sobre ele
ent�o adiciona-se por exemplo
R$ 30,00 no custo para o vidro verde
sendo assim o calculo deveria ser
custo R$ 130 + %de instalado retirado ou fornecido

tomando com exemplo 10% de percentual

R$ 130 +104% = 265,20

Entao devo na cor devo calcular assim

R$ 30,00 de aumento representam 30% de aumento

sendo assim R$ (100*104%) = 204+30%+(61,20)=265,20 � o preco final do video verde





    if ((TEdit(Sender).Text=''))then
    exit;

    TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);

    PValorCusto:=0;
    PValorExtra:=0;
    PPorcentagemExtra:=0;

    try
          PValorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.Text));
          PValorExtra:=StrToCurr(Tira_Ponto(EdtValorExtra.Text));
          PPorcentagemExtra:=(PValorExtra*100)/PValorCusto;
          EdtAcrescimoExtra.Text:=FormatFloat('#,##0.00',PPorcentagemExtra);
    except
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais');
    end;
    //********** Atualiza Valor Final, s� pra mostra poruque este campo � Computed By
    EdtPorcentagemAcrescimoFinal.Text :=formata_valor(StrToCurr(Tira_Ponto(EdtPorcentagemAcrescimo.Text))+
                                      StrToCurr(tira_ponto(EdtAcrescimoExtra.Text))+
                                      StrToCurr(tira_ponto(edtporcentagemacrescimoicms.Text))
                                      );

    EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(Tira_POnto(EdtPorcentagemAcrescimoFinal.Text))/100));




*)

end;



procedure TFVIDRO.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjVidroCor.Vidro.PrimeiroRegistro = false)then
    exit;

    ObjVidroCor.Vidro.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFVIDRO.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigo.Caption='')then
    lbCodigo.Caption:='0';

    if  (ObjVidroCor.Vidro.RegistoAnterior(StrToInt(lbCodigo.Caption)) = false)then
    exit;

    ObjVidroCor.Vidro.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFVIDRO.btProximoClick(Sender: TObject);
begin
    if (lbCodigo.Caption='')then
    lbCodigo.Caption:='0';

    if  (ObjVidroCor.Vidro.ProximoRegisto(StrToInt(lbCodigo.Caption)) = false)then
    exit;

    ObjVidroCor.Vidro.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFVIDRO.btUltimoClick(Sender: TObject);
begin
    if  (ObjVidroCor.Vidro.UltimoRegistro = false)then
    exit;

    ObjVidroCor.Vidro.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFVIDRO.EdtPrecoCustoExit(Sender: TObject);
begin
      TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
      if(EdtPorcentagemInstalado.Text<>'')
      then EdtPorcentagemInstaladoExit(Sender);
      if(EdtPorcentagemFornecido.Text<>'')
      then EdtPorcentagemFornecidoExit(sender);
      if(EdtPorcentagemRetirado.Text<>'')
      then EdtPorcentagemRetiradoExit(Sender);
end;

procedure TFVIDRO.EdtAreaMinimaExit(Sender: TObject);
begin
//    Guia.TabIndex:=1;
end;


procedure TFVIDRO.edtacrescimoicmsExit(Sender: TObject);
Var
PValorCusto, PPorcentagemExtraICMS, PValorExtraICMS : Currency;
begin
    if ((TEdit(Sender).Text=''))
    then exit;

    TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);

    PValorCusto:=0;
    PValorExtraICMS:=0;
    PPorcentagemExtraICMS:=0;

    try

          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));

          PValorExtraICMS:=StrToCurr(Tira_Ponto(edtacrescimoicms.Text));
          
          PPorcentagemExtraICMS:=(PValorExtraICMS*100)/PValorCusto;
          edtporcentagemacrescimoicms.Text:=FormatFloat('#,##0.00',PPorcentagemExtraICMS);
    except
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais');
    end;
    //********** Atualiza Valor Final, s� pra mostra poruque este campo � Computed By
    EdtPorcentagemAcrescimoFinal.Text :=formata_valor(StrToCurr(Tira_Ponto(EdtPorcentagemAcrescimo.Text))+
                                      StrToCurr(tira_ponto(EdtAcrescimoExtra.Text))+
                                      StrToCurr(tira_ponto(edtporcentagemacrescimoicms.Text))
                                      );

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(Tira_POnto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));

end;
procedure TFVIDRO.edtporcentagemacrescimoicmsExit(Sender: TObject);
Var
PValorCusto, PAcrescimoICMS, PporcentagemICMS :Currency;
begin
    if ((TEdit(Sender).Text=''))then
    exit;

    PValorCusto:=0;
    PAcrescimoICMS:=0;
    PporcentagemICMS:=0;

    try
          if (Rg_forma_de_calculo_percentual.ItemIndex=0)
          Then PvalorCusto:=StrToCurr(tira_ponto(EdtPrecoCusto.text))
          Else PValorCusto:=StrToCurr(tira_ponto(EdtPrecoVendaInstalado.text));


          PporcentagemICMS:=StrTOCurr(tira_ponto(edtporcentagemacrescimoicms.Text));
          PAcrescimoICMS:=((PValorCusto*PporcentagemICMS)/100);
          edtacrescimoicms.Text:=Formata_Valor(PAcrescimoICMS);
    except;
          MensagemErro('Erro ao tentar transformar de Porcentagem para Reais.');
    end;

    EdtPorcentagemAcrescimoFinal.Text:=CurrToStr(StrToCurr(tira_ponto(EdtPorcentagemAcrescimo.Text))+StrToCurr(tira_ponto(EdtAcrescimoExtra.Text))+
                                       StrToCurr(tira_ponto(edtporcentagemacrescimoicms.Text)));

    //EdtValorFinal.Text :=formata_valor(PValorCusto+(PvalorCusto*StrToCurr(tira_ponto(EdtPorcentagemAcrescimoFinal.Text))/100));
    edtinstaladofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaInstalado.text)+((StrToFloat(EdtPrecoVendaInstalado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtfornecidofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaFornecido.text)+((StrToFloat(EdtPrecoVendaFornecido.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));
    edtretiradofinal.text:=formata_valor(StrToFloat(EdtPrecoVendaRetirado.text)+((StrToFloat(EdtPrecoVendaRetirado.text)*strtofloat(EdtPorcentagemAcrescimoFinal.Text ))/100));


    btGravarCor.SetFocus;
end;

procedure TFVIDRO.EdtPrecoVendaInstaladoExit(Sender: TObject);
Var
  PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemInstalado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaInstalado.Text))/precocusto));
end;

procedure TFVIDRO.EdtPrecoVendaFornecidoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemfornecido.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendafornecido.Text))/precocusto));

end;

procedure TFVIDRO.EdtPrecoVendaRetiradoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemretirado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaretirado.Text))/precocusto));
end;


procedure TFVIDRO.FRImposto_ICMS1BtGravar_material_icmsClick(
  Sender: TObject);
begin
  FRImposto_ICMS1.BtGravar_material_icmsClick(Sender);

end;

procedure TFVIDRO.dbgridCORKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Self.DBGRIDCORDblClick(sender);
end;

procedure TFVIDRO.dbgridCORDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGRIDCOR.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGRIDCOR.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGRIDCOR.DefaultDrawDataCell(Rect,DBGRIDCOR.Columns[DataCol].Field, State);
          End;
end;

procedure TFVIDRO.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.TabIndex:=0;
     ColocaUpperCaseEdit(Self);

     Try
        ObjVidroCor:=TObjVidroCor.create;
        DBGRIDCOR.DataSource:=ObjVidroCor.ObjDatasource;

        //Instanciando o Vidro na Material_ICMS
        FRImposto_ICMS1.ObjMaterial_ICMS:=TObjVIDRO_ICMS.Create(self);

     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

    FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
    FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
      FescolheImagemBotao.PegaFiguraBotaopequeno(Btgravarcor,'BOTAOINSERIR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btcancelacor,'BOTAOCANCELARPRODUTO.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btexcluircor,'BOTAORETIRAR.BMP');

     habilita_botoes(Self);

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE VIDRO')=False)
     Then desab_botoes(Self);


     PAlteraCampoEstoque:=False;
     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('PERMITE ALTERAR O CAMPO ESTOQUE NO CADASTRO DE VIDRO')=True)
     Then PAlteraCampoEstoque:=true;


     Rg_forma_de_calculo_percentual.itemindex:=0;
     if (ObjParametroGlobal.ValidaParametro('FORMA DE CALCULO DE PERCENTUAL DE ACRESCIMO NAS CORES DO VIDRO')=True)
     then Begin
               if (ObjParametroGlobal.get_valor='1')
               then Rg_forma_de_calculo_percentual.itemindex:=1;
     End;
     Rg_forma_de_calculo_percentual.enabled:=False;

     AlteraPrecoPeloCusto:=True;
     if (ObjParametroGlobal.ValidaParametro('CALCULA PRECO DE VENDA DO VIDRO PELO CUSTO E PERCENTUAIS?')=True)
     then Begin
               if (ObjParametroGlobal.get_valor<>'SIM')
               then AlteraPrecoPeloCusto:=false;
     End;

     if(Tag<>0)
     then begin
        if(ObjVidroCor.Vidro.LocalizaCodigo(IntToStr(Tag))=True)
        then
        begin
            ObjVidroCor.Vidro.TabelaparaObjeto;
            self.ObjetoParaControles;

        end;
     end;

end;



procedure TFVIDRO.EdtGrupoVidroDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FGrupovidro:TFGRUPOVIDRO;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fgrupovidro:=TFGRUPOVIDRO.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabgrupovidro','',Fgrupovidro)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtGrupoVidro.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 LbNomeGrupoVidro.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FGrupovidro);

     End;
end;


procedure TFVIDRO.EdtFornecedorDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Ffornecedor:TFFORNECEDOR;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Ffornecedor:=TFFORNECEDOR.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabfornecedor','',Ffornecedor)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtFornecedor.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 LbNomeFornecedor.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('razaosocial').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Ffornecedor);

     End;
end;


procedure TFVIDRO.edtCorReferenciaDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Fcor:TFCOR ;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fcor:=TFCOR.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabcor','',Fcor)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtCorReferencia.text:=FpesquisaLocal.QueryPesq.fieldbyname('referencia').asstring;
                                 lbnomecor.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fcor);

     End;
end;
procedure TFVIDRO.edtNCMKeyPress(Sender: TObject; var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFVIDRO.lbEstoqueMouseLeave(Sender: TObject);
begin
        TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFVIDRO.lbEstoqueMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFVIDRO.lbEstoqueClick(Sender: TObject);
begin

     if(ObjParametroGlobal.ValidaParametro('UTILIZA SPED FISCAL')=false) then
     begin
          MensagemAviso('O Parametro "UTILIZA SPED FISCAL" n�o foi encontrado!!!');
          Exit;
     end;
     if(ObjParametroGlobal.Get_Valor='SIM')
     then Exit;
     
     If(PAlteraCampoEstoque=False)then
     begin
          MensagemAviso('O Usu�rio '+ObjUsuarioGlobal.Get_nome+' n�o tem autoriza��o para alterar o estoque');
          Exit;
     end
     else
     begin
          with FfiltroImp do
          begin
              DesativaGrupos;
              Grupo01.Enabled:=True;
              edtgrupo01.Text:='';
              LbGrupo01.Caption:='QUANTIDADE';
              edtgrupo01.OnKeyPress:=edtquant;

              MensagemAviso('Acerte o estoque: Digite a quantidade a tirar ou acrescentar no estoque');

              ShowModal;

              if(Tag=0)
              then Exit;

              if(edtgrupo01.Text='') or(edtgrupo01.Text='0')
              then Exit;
              
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Status:=dsInsert;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Submit_DATA(FormatDateTime('dd/mm/yyyy',Now));
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('PELO MODO ACERTO DE ESTOQUE (VIDRO)');
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(edtgrupo01.Text);
              OBJESTOQUEGLOBAL.Submit_VIDROCOR(ObjVIDROCOR.Get_Codigo);

              if (OBJESTOQUEGLOBAL.Salvar(True)=False)
              Then Begin
                     MensagemErro('Erro na tentativa de salvar o registro de estoque do PERFILADO');
                     exit;
              End;

              lbestoque.Caption:=ObjVIDROCOR.RetornaEstoque+' '+ObjVidroCor.Vidro.Get_Unidade;


          end;
     end;
end;

procedure TFVIDRO.edtquant(Sender: TObject;
  var Key: Char);
begin
     if not (Key in['0'..'9',Chr(8),'-',',']) then
    begin
        Key:= #0;
    end;

end;

procedure TFVIDRO.edtEdtGrupoVidroKeyPress(Sender: TObject; var Key: Char);
begin
      if not (Key in['0'..'9',Chr(8),',']) then
    begin
        Key:= #0;
    end;
end;

procedure TFVIDRO.bt1Click(Sender: TObject);
begin
      FAjuda.PassaAjuda('CADASTRO DE VIDROS');
      FAjuda.ShowModal;
end;

procedure TFVIDRO.chkAtivoClick(Sender: TObject);
begin
      if(lbCodigo.Caption='') or (lbCodigo.Caption='0')
      then Exit;
      if(chkAtivo.Checked=True)
      then lbAtivoInativo.Caption:='Ativo';

      if(chkAtivo.Checked=False)
      then  lbAtivoInativo.Caption:='Inativo'

end;

procedure TFVIDRO.FRImposto_ICMS1btReplicarPedidoClick(Sender: TObject);
begin
     FRImposto_ICMS1.Button1Click(Sender);
end;

procedure TFVIDRO.FRImposto_ICMS1Button1Click(Sender: TObject);
begin
  FRImposto_ICMS1.Button1Click(Sender);

end;

procedure TFVIDRO.FRImposto_ICMS1edttipocliente_material_icmsKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  FRImposto_ICMS1.edttipocliente_material_icmsKeyDown(Sender, Key,
  Shift);

end;

procedure TFVIDRO.FRImposto_ICMS1edttipocliente_material_icmsExit(
  Sender: TObject);
begin
  FRImposto_ICMS1.edttipocliente_material_icmsExit(Sender);

end;

procedure TFVIDRO.FRImposto_ICMS1edtoperacaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  FRImposto_ICMS1.edtoperacaoKeyDown(Sender, Key, Shift);

end;

procedure TFVIDRO.FRImposto_ICMS1edtoperacaoExit(Sender: TObject);
begin
  FRImposto_ICMS1.edtoperacaoExit(Sender);

end;

procedure TFVIDRO.GuiaChange(Sender: TObject);
begin
      if  (Guia.TabIndex=2)//cor
     Then Begin
               LbEstoque.Caption:='';

               if (OBJVidroCOR.Vidro.Status=dsinsert)
               or (lbCodigo.Caption='')
               Then Begin
                         exit;
               End;
               habilita_campos(panelCor);
               Self.PreparaCor;
               EdtCorReferencia.SetFocus;
     End
     Else Begin
             if (Guia.TabIndex=1)//Impostos
             Then Begin
                      if (OBJVidroCOR.Vidro.Status=dsinsert)
                      or (lbCodigo.Caption='')
                      Then Begin
                                exit;
                      End;
                      FRImposto_ICMS1.Acessaframe(lbCodigo.Caption);
                     
             End
             Else Begin
                      Guia.TabIndex:=0;

                          
             End;
     end;
end;

procedure TFVIDRO.lbNomeGrupoVidroClick(Sender: TObject);
var
  FGrupoVidro:TFGRUPOVIDRO;
begin
    try
      FGrupoVidro:=TFGRUPOVIDRO.Create(nil);
    except
      Exit;
    end;

    try
      if(edtGrupoVidro.Text='')
      then Exit;

      FGrupoVidro.tag:=StrToInt(edtGrupoVidro.Text);
      FGrupoVidro.ShowModal;
    finally
      FreeAndNil(FGrupoVidro);
    end;

end;

procedure TFVIDRO.lbNomeFornecedorClick(Sender: TObject);
var
  FFornecedor:TFFORNECEDOR;
begin
  try
    FFornecedor:=TFFORNECEDOR.Create(nil);
  except
    Exit;
  end;

  try
    if(edtFornecedor.Text='')
    then Exit;

    FFornecedor.tag:=StrToInt(edtFornecedor.Text);
    FFornecedor.ShowModal;

  finally
    FreeAndNil(FFornecedor);
  end;
end;

procedure TFVIDRO.lbNomeGrupoVidroMouseLeave(Sender: TObject);
begin
TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFVIDRO.lbNomeGrupoVidroMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFVIDRO.lbnomecorClick(Sender: TObject);
var
  Fcor:TFCOR;
begin
   try
      Fcor:=TFCOR.Create(nil);
   except
      Exit;
   end;

   try
     if(EdtCor.Text='')
     then Exit;

     Fcor.Tag:=StrToInt(EdtCor.Text);
     fcor.ShowModal;
   finally
     FreeAndNil(Fcor);
   end; 

end;

procedure TFVIDRO.edtGrupoVidroKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFVIDRO.edtFornecedorKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFVIDRO.edtPrecoCustoKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFVIDRO.edtEspessuraKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFVIDRO.edtArredondamentoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFVIDRO.edtAreaMinimaKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFVIDRO.edtPorcentagemInstaladoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFVIDRO.edtPorcentagemFornecidoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFVIDRO.edtPorcentagemRetiradoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFVIDRO.edtPorcentagemAcrescimoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFVIDRO.edtValorExtraKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFVIDRO.edtAcrescimoExtraKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFVIDRO.edtacrescimoicmsKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFVIDRO.edtporcentagemacrescimoicmsKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFVIDRO.edtPorcentagemAcrescimoFinalKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFVIDRO.edtNCMKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  FDataModulo.edtNCMKeyDown(Sender,Key,Shift);
end;

procedure TFVIDRO.edtNCMExit(Sender: TObject);
begin
  //a l�gica no sab n�o � essa, para cada tipo de opera��o
  //existe um tipo de imposto.
  //FDataModulo.edtNCMExit(Sender, FRImposto_ICMS1.edtpercentualtributo);
end;

procedure TFVIDRO.edtCestKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  if Key=vk_f9 then
  begin
    Clipboard.AsText := EDTNCM.Text;
    With TObjCEST.Create do
    begin
      edtCESTkeydown(sender,Key,shift);
      Free;
    end;
  end;

end;

end.

