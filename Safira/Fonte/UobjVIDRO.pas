unit UobjVIDRO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJGRUPOVIDRO
,UOBJFORNECEDOR,UOBJTABELAA_ST,UOBJTABELAB_ST,UMostraBarraProgresso,Forms;

Type
   TObjVIDRO=class

          Public
                //ObjDatasource                               :TDataSource;
               Status                                      :TDataSetState;
               SqlInicial                                  :String[200];
               ReferenciaAnterior                          :string;
               GrupoVidro:TOBJGRUPOVIDRO;
               Fornecedor:TOBJFORNECEDOR;
               SituacaoTributaria_TabelaA:TOBJTABELAA_ST ;
               SituacaoTributaria_TabelaB:TOBJTABELAB_ST ;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaReferencia(Parametro:string) :boolean;
                Function    LocalizaCodigo2(Parametro:String) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Codigo2(parametro: string);
                Function Get_Codigo2: string;

                Procedure Submit_Referencia(parametro: string);
                Function Get_Referencia: string;
                Procedure Submit_Descricao(parametro: string);
                Function Get_Descricao: string;
                Procedure Submit_Unidade(parametro: string);
                Function Get_Unidade: string;
                Procedure Submit_Peso(parametro: string);
                Function Get_Peso: string;
                Procedure Submit_PrecoCusto(parametro: string);
                Function Get_PrecoCusto: string;
                Procedure Submit_PorcentagemInstalado(parametro: string);
                Function Get_PorcentagemInstalado: string;
                Procedure Submit_PorcentagemFornecido(parametro: string);
                Function Get_PorcentagemFornecido: string;
                Procedure Submit_PorcentagemRetirado(parametro: string);
                Function Get_PorcentagemRetirado: string;

                Function Get_PrecoVendaInstalado: string;
                Function Get_PrecoVendaFornecido: string;
                Function Get_PrecoVendaRetirado: string;

                Procedure Submit_PrecoVendaInstalado(parametro: string);
                Procedure Submit_PrecoVendaFornecido(parametro: string);
                Procedure Submit_PrecoVendaRetirado(parametro: string);

                Procedure Submit_Arredondamento(parametro: string);
                Function Get_Arredondamento: string;
                Procedure Submit_AreaMinima(parametro: string);
                Function Get_AreaMinima: string;
                Procedure Submit_IsentoICMS_Estado(parametro: string);
                Function Get_IsentoICMS_Estado: string;
                Procedure Submit_SubstituicaoICMS_Estado(parametro: string);
                Function Get_SubstituicaoICMS_Estado: string;
                Procedure Submit_ValorPauta_Sub_Trib_Estado(parametro: string);
                Function Get_ValorPauta_Sub_Trib_Estado: string;
                Procedure Submit_Aliquota_ICMS_Estado(parametro: string);
                Function Get_Aliquota_ICMS_Estado: string;
                Procedure Submit_Reducao_BC_ICMS_Estado(parametro: string);
                Function Get_Reducao_BC_ICMS_Estado: string;
                Procedure Submit_Aliquota_ICMS_Cupom_Estado(parametro: string);
                Function Get_Aliquota_ICMS_Cupom_Estado: string;
                Procedure Submit_IsentoICMS_ForaEstado(parametro: string);
                Function Get_IsentoICMS_ForaEstado: string;
                Procedure Submit_SubstituicaoICMS_ForaEstado(parametro: string);
                Function Get_SubstituicaoICMS_ForaEstado: string;
                Procedure Submit_ValorPauta_Sub_Trib_ForaEstado(parametro: string);
                Function Get_ValorPauta_Sub_Trib_ForaEstado: string;
                Procedure Submit_Aliquota_ICMS_ForaEstado(parametro: string);
                Function Get_Aliquota_ICMS_ForaEstado: string;
                Procedure Submit_Reducao_BC_ICMS_ForaEstado(parametro: string);
                Function Get_Reducao_BC_ICMS_ForaEstado: string;
                Procedure Submit_Aliquota_ICMS_Cupom_ForaEstado(parametro: string);
                Function Get_Aliquota_ICMS_Cupom_ForaEstado: string;

                Procedure Submit_percentualagregado(parametro: string);
                Function Get_percentualagregado: string;

                Procedure Submit_ipi(parametro: string);
                Function Get_ipi: string;

                Procedure Submit_ClassificacaoFiscal(parametro: string);
                Function Get_ClassificacaoFiscal: string;
                procedure EdtGrupoVidroExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtGrupoVidroKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtGrupoVidroKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
                procedure EdtVidroKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
                procedure EdtVidroKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;labelnome:TLabel);overload;

                procedure EdtFornecedorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFornecedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtSituacaoTributaria_TabelaAExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtSituacaoTributaria_TabelaAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtSituacaoTributaria_TabelaBExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtSituacaoTributaria_TabelaBKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                Function  VerificaReferencia(PStatus:TDataSetState; PCodigo,PReferencia:string):Boolean;  {Rodolfo}
                Procedure  AtualizaMargens(PGrupoVidro:string);

                function PrimeiroRegistro: Boolean;
                function ProximoRegisto(PCodigo: Integer): Boolean;
                function RegistoAnterior(PCodigo: Integer): Boolean;
                function UltimoRegistro: Boolean;
                procedure AtualizaPrecos(PGrupoVidro: string);
                procedure Reajusta(PIndice, Pgrupo: string);

                procedure Submit_NCM(parametro:string);
                function Get_NCM:String;

                procedure Submit_Espessura(parametro:string);
                function Get_Espessura:string;
                procedure Submit_Ativo(parametro:string);
                Function Get_Ativo:string;

                procedure Submit_cest(parametro:string);
                Function Get_cest:string;

                function AtualizaTributospeloNCM:Boolean;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Codigo2:string;
               Referencia:string;
               Descricao:string;
               Unidade:string;
               Peso:string;
               PrecoCusto:string;
               PorcentagemInstalado:string;
               PorcentagemFornecido:string;
               PorcentagemRetirado:string;

               PrecoVendaInstalado:string;
               PrecoVendaFornecido:string;
               PrecoVendaRetirado:string;
               
               Arredondamento:string;
               AreaMinima:string;
               IsentoICMS_Estado:string;
               SubstituicaoICMS_Estado:string;
               ValorPauta_Sub_Trib_Estado:string;
               Aliquota_ICMS_Estado:string;
               Reducao_BC_ICMS_Estado:string;
               Aliquota_ICMS_Cupom_Estado:string;
               IsentoICMS_ForaEstado:string;
               SubstituicaoICMS_ForaEstado:string;
               ValorPauta_Sub_Trib_ForaEstado:string;
               Aliquota_ICMS_ForaEstado:string;
               Reducao_BC_ICMS_ForaEstado:string;
               Aliquota_ICMS_Cupom_ForaEstado:string;
               percentualagregado:string;
               ipi:string;
               ClassificacaoFiscal:string;
               NCM:string;
               Espessura:string;
               Ativo:string;
               cest:string;



//CODIFICA VARIAVEIS PRIVADAS

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;


   End;


implementation
uses Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UFORNECEDOR, UTABELAA_ST, UTABELAB_ST;



{ TTabTitulo }


Function  TObjVIDRO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Codigo2:=fieldbyname('Codigo2').asstring;
        Self.Referencia:=fieldbyname('Referencia').asstring;
        If(FieldByName('GrupoVidro').asstring<>'')
        Then Begin
                 If (Self.GrupoVidro.LocalizaCodigo(FieldByName('GrupoVidro').asstring)=False)
                 Then Begin
                          Messagedlg('GrupoVidro N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.GrupoVidro.TabelaparaObjeto;
        End;
        Self.Descricao:=fieldbyname('Descricao').asstring;
        Self.Unidade:=fieldbyname('Unidade').asstring;
        If(FieldByName('Fornecedor').asstring<>'')
        Then Begin
                 If (Self.Fornecedor.LocalizaCodigo(FieldByName('Fornecedor').asstring)=False)
                 Then Begin
                          Messagedlg('Fornecedor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Fornecedor.TabelaparaObjeto;
        End;
        Self.Peso:=fieldbyname('Peso').asstring;
        Self.PrecoCusto:=fieldbyname('PrecoCusto').asstring;
        Self.PorcentagemInstalado:=fieldbyname('PorcentagemInstalado').asstring;
        Self.PorcentagemFornecido:=fieldbyname('PorcentagemFornecido').asstring;
        Self.PorcentagemRetirado:=fieldbyname('PorcentagemRetirado').asstring;
        Self.PrecoVendaInstalado:=fieldbyname('PrecoVendaInstalado').asstring;
        Self.PrecoVendaFornecido:=fieldbyname('PrecoVendaFornecido').asstring;
        Self.PrecoVendaRetirado:=fieldbyname('PrecoVendaRetirado').asstring;
        Self.Arredondamento:=fieldbyname('Arredondamento').asstring;
        Self.AreaMinima:=fieldbyname('AreaMinima').asstring;
        Self.IsentoICMS_Estado:=fieldbyname('IsentoICMS_Estado').asstring;
        Self.SubstituicaoICMS_Estado:=fieldbyname('SubstituicaoICMS_Estado').asstring;
        Self.ValorPauta_Sub_Trib_Estado:=fieldbyname('ValorPauta_Sub_Trib_Estado').asstring;
        Self.Aliquota_ICMS_Estado:=fieldbyname('Aliquota_ICMS_Estado').asstring;
        Self.Reducao_BC_ICMS_Estado:=fieldbyname('Reducao_BC_ICMS_Estado').asstring;
        Self.Aliquota_ICMS_Cupom_Estado:=fieldbyname('Aliquota_ICMS_Cupom_Estado').asstring;
        Self.IsentoICMS_ForaEstado:=fieldbyname('IsentoICMS_ForaEstado').asstring;
        Self.SubstituicaoICMS_ForaEstado:=fieldbyname('SubstituicaoICMS_ForaEstado').asstring;
        Self.ValorPauta_Sub_Trib_ForaEstado:=fieldbyname('ValorPauta_Sub_Trib_ForaEstado').asstring;
        Self.Aliquota_ICMS_ForaEstado:=fieldbyname('Aliquota_ICMS_ForaEstado').asstring;
        Self.Reducao_BC_ICMS_ForaEstado:=fieldbyname('Reducao_BC_ICMS_ForaEstado').asstring;
        Self.Aliquota_ICMS_Cupom_ForaEstado:=fieldbyname('Aliquota_ICMS_Cupom_ForaEstado').asstring;
        Self.percentualagregado:=fieldbyname('percentualagregado').asstring;
        Self.ipi:=fieldbyname('ipi').asstring;
        If(FieldByName('SituacaoTributaria_TabelaA').asstring<>'')
        Then Begin
                 If (Self.SituacaoTributaria_TabelaA.LocalizaCodigo(FieldByName('SituacaoTributaria_TabelaA').asstring)=False)
                 Then Begin
                          Messagedlg('SituacaoTributaria_TabelaA N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.SituacaoTributaria_TabelaA.TabelaparaObjeto;
        End;
        If(FieldByName('SituacaoTributaria_TabelaB').asstring<>'')
        Then Begin
                 If (Self.SituacaoTributaria_TabelaB.LocalizaCodigo(FieldByName('SituacaoTributaria_TabelaB').asstring)=False)
                 Then Begin
                          Messagedlg('SituacaoTributaria_TabelaB N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.SituacaoTributaria_TabelaB.TabelaparaObjeto;
        End;
        Self.ClassificacaoFiscal:=fieldbyname('ClassificacaoFiscal').asstring;
        self.NCM:=fieldbyname('NCM').AsString;
        self.Espessura:=fieldbyname('Espessura').asstring;
        self.Ativo:=fieldbyname('ativo').AsString;
        self.cest:=fieldbyname('cest').AsString;
//CODIFICA TABELAPARAOBJETO


        result:=True;
     End;
end;


Procedure TObjVIDRO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Codigo2').asstring:=Self.Codigo2;
        ParamByName('Referencia').asstring:=Self.Referencia;
        ParamByName('GrupoVidro').asstring:=Self.GrupoVidro.GET_CODIGO;
        ParamByName('Descricao').asstring:=Self.Descricao;
        ParamByName('Unidade').asstring:=Self.Unidade;
        ParamByName('Fornecedor').asstring:=Self.Fornecedor.GET_CODIGO;
        ParamByName('Peso').asstring:=virgulaparaponto(Self.Peso);
        ParamByName('PrecoCusto').asstring:=virgulaparaponto(Self.PrecoCusto);
        ParamByName('PorcentagemInstalado').asstring:=virgulaparaponto(Self.PorcentagemInstalado);
        ParamByName('PorcentagemFornecido').asstring:=virgulaparaponto(Self.PorcentagemFornecido);
        ParamByName('PorcentagemRetirado').asstring:=virgulaparaponto(Self.PorcentagemRetirado);

        ParamByName('PrecoVendaInstalado').asstring:=virgulaparaponto(Self.PrecoVendaInstalado);
        ParamByName('PrecoVendaFornecido').asstring:=virgulaparaponto(Self.PrecoVendaFornecido);
        ParamByName('precovendaretirado').asstring:=virgulaparaponto(Self.precovendaretirado);


        ParamByName('Arredondamento').asstring:=virgulaparaponto(Self.Arredondamento);
        ParamByName('AreaMinima').asstring:=virgulaparaponto(Self.AreaMinima);
        ParamByName('IsentoICMS_Estado').asstring:=Self.IsentoICMS_Estado;
        ParamByName('SubstituicaoICMS_Estado').asstring:=Self.SubstituicaoICMS_Estado;
        ParamByName('ValorPauta_Sub_Trib_Estado').asstring:=virgulaparaponto(Self.ValorPauta_Sub_Trib_Estado);
        ParamByName('Aliquota_ICMS_Estado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_Estado);
        ParamByName('Reducao_BC_ICMS_Estado').asstring:=virgulaparaponto(Self.Reducao_BC_ICMS_Estado);
        ParamByName('Aliquota_ICMS_Cupom_Estado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_Cupom_Estado);
        ParamByName('IsentoICMS_ForaEstado').asstring:=Self.IsentoICMS_ForaEstado;
        ParamByName('SubstituicaoICMS_ForaEstado').asstring:=Self.SubstituicaoICMS_ForaEstado;
        ParamByName('ValorPauta_Sub_Trib_ForaEstado').asstring:=virgulaparaponto(Self.ValorPauta_Sub_Trib_ForaEstado);
        ParamByName('Aliquota_ICMS_ForaEstado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_ForaEstado);
        ParamByName('Reducao_BC_ICMS_ForaEstado').asstring:=virgulaparaponto(Self.Reducao_BC_ICMS_ForaEstado);
        ParamByName('Aliquota_ICMS_Cupom_ForaEstado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_Cupom_ForaEstado);
        ParamByName('percentualagregado').asstring:=virgulaparaponto(Self.percentualagregado);
        ParamByName('ipi').asstring:=virgulaparaponto(Self.ipi);
        ParamByName('SituacaoTributaria_TabelaA').asstring:=Self.SituacaoTributaria_TabelaA.GET_CODIGO;
        ParamByName('SituacaoTributaria_TabelaB').asstring:=Self.SituacaoTributaria_TabelaB.GET_CODIGO;
        ParamByName('ClassificacaoFiscal').asstring:=Self.ClassificacaoFiscal;
        ParamByName('NCM').AsString:=self.NCM;
        ParamByName('Espessura').AsString:=self.Espessura;
        ParamByName('ativo').AsString:=Self.Ativo;
        ParamByName('cest').AsString:=Self.cest;
//CODIFICA OBJETOPARATABELA

  End;
End;

//***********************************************************************

function TObjVIDRO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjVIDRO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Codigo2:='';
        Referencia:='';
        GrupoVidro.ZerarTabela;
        Descricao:='';
        Unidade:='';
        Fornecedor.ZerarTabela;
        Peso:='';
        PrecoCusto:='';
        PorcentagemInstalado:='';
        PorcentagemFornecido:='';
        PorcentagemRetirado:='';

        PrecoVendaInstalado:='';
        PrecoVendaFornecido:='';
        PrecoVendaRetirado:='';

        Arredondamento:='';
        AreaMinima:='';
        IsentoICMS_Estado:='';
        SubstituicaoICMS_Estado:='';
        ValorPauta_Sub_Trib_Estado:='';
        Aliquota_ICMS_Estado:='';
        Reducao_BC_ICMS_Estado:='';
        Aliquota_ICMS_Cupom_Estado:='';
        IsentoICMS_ForaEstado:='';
        SubstituicaoICMS_ForaEstado:='';
        ValorPauta_Sub_Trib_ForaEstado:='';
        Aliquota_ICMS_ForaEstado:='';
        Reducao_BC_ICMS_ForaEstado:='';
        Aliquota_ICMS_Cupom_ForaEstado:='';
        percentualagregado:='';
        ipi:='';
        SituacaoTributaria_TabelaA.ZerarTabela;
        SituacaoTributaria_TabelaB.ZerarTabela;
        ClassificacaoFiscal:='';
        NCM:='';
        Espessura:='';
        Ativo:='';
        cest:='';

//CODIFICA ZERARTABELA

     End;
end;

Function TObjVIDRO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';

      if (PrecoVendaInstalado='')
      Then Mensagem:=mensagem+'/Pre�o de Venda Instalado';

      if (PrecoVendaFornecido='')
      Then Mensagem:=Mensagem+'/Pre�o de Venda Fornecido';


      if (PrecoVendaRetirado='')
      Then mensagem:=mensagem+'/Pre�o de Venda Retirado';

      if (percentualagregado='')
      Then percentualagregado:='0';

      if (IPI='')
      Then Ipi:='0';

      
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjVIDRO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.GrupoVidro.LocalizaCodigo(Self.GrupoVidro.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ GrupoVidro n�o Encontrado!';
      If (Self.Fornecedor.LocalizaCodigo(Self.Fornecedor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Fornecedor n�o Encontrado!';
      If (Self.SituacaoTributaria_TabelaA.LocalizaCodigo(Self.SituacaoTributaria_TabelaA.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ SituacaoTributaria_TabelaA n�o Encontrado!';
      If (Self.SituacaoTributaria_TabelaB.LocalizaCodigo(Self.SituacaoTributaria_TabelaB.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ SituacaoTributaria_TabelaB n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjVIDRO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.GrupoVidro.Get_Codigo<>'')
        Then Strtoint(Self.GrupoVidro.Get_Codigo);
     Except
           Mensagem:=mensagem+'/GrupoVidro';
     End;
     try
        If (Self.Fornecedor.Get_Codigo<>'')
        Then Strtoint(Self.Fornecedor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Fornecedor';
     End;
     try
        Strtofloat(Self.Peso);
     Except
           Mensagem:=mensagem+'/Peso';
     End;
     try
        Strtofloat(Self.PrecoCusto);
     Except
           Mensagem:=mensagem+'/PrecoCusto';
     End;
     try
        Strtofloat(Self.PorcentagemInstalado);
     Except
           Mensagem:=mensagem+'/PorcentagemInstalado';
     End;
     try
        Strtofloat(Self.PorcentagemFornecido);
     Except
           Mensagem:=mensagem+'/PorcentagemFornecido';
     End;
     try
        Strtofloat(Self.PorcentagemRetirado);
     Except
           Mensagem:=mensagem+'/PorcentagemRetirado';
     End;
     try
        Strtofloat(Self.Arredondamento);
     Except
           Mensagem:=mensagem+'/Arredondamento';
     End;
     try
        Strtofloat(Self.AreaMinima);
     Except
           Mensagem:=mensagem+'/AreaMinima';
     End;
     try
        Strtofloat(Self.ValorPauta_Sub_Trib_Estado);
     Except
           Mensagem:=mensagem+'/ValorPauta_Sub_Trib_Estado';
     End;
     try
        Strtofloat(Self.Aliquota_ICMS_Estado);
     Except
           Mensagem:=mensagem+'/Aliquota_ICMS_Estado';
     End;
     try
        Strtofloat(Self.Reducao_BC_ICMS_Estado);
     Except
           Mensagem:=mensagem+'/Reducao_BC_ICMS_Estado';
     End;
     try
        Strtofloat(Self.Aliquota_ICMS_Cupom_Estado);
     Except
           Mensagem:=mensagem+'/Aliquota_ICMS_Cupom_Estado';
     End;
     try
        Strtofloat(Self.ValorPauta_Sub_Trib_ForaEstado);
     Except
           Mensagem:=mensagem+'/ValorPauta_Sub_Trib_ForaEstado';
     End;
     try
        Strtofloat(Self.Aliquota_ICMS_ForaEstado);
     Except
           Mensagem:=mensagem+'/Aliquota_ICMS_ForaEstado';
     End;
     try
        Strtofloat(Self.Reducao_BC_ICMS_ForaEstado);
     Except
           Mensagem:=mensagem+'/Reducao_BC_ICMS_ForaEstado';
     End;

     try
        Strtofloat(Self.Aliquota_ICMS_Cupom_ForaEstado);
     Except
           Mensagem:=mensagem+'/Aliquota_ICMS_Cupom_ForaEstado';
     End;

     try
        Strtofloat(Self.ipi);
     Except
           Mensagem:=mensagem+'/IPI';
     End;

     try
        If (Self.SituacaoTributaria_TabelaA.Get_Codigo<>'')
        Then Strtoint(Self.SituacaoTributaria_TabelaA.Get_Codigo);
     Except
           Mensagem:=mensagem+'/SituacaoTributaria_TabelaA';
     End;

     try
        If (Self.SituacaoTributaria_TabelaB.Get_Codigo<>'')
        Then Strtoint(Self.SituacaoTributaria_TabelaB.Get_Codigo);
     Except
           Mensagem:=mensagem+'/SituacaoTributaria_TabelaB';
     End;

     try
        Strtofloat(Self.PrecoVendaInstalado);
     Except
           Mensagem:=mensagem+'/Pre�o de Venda Instalado';
     End;

     try
        Strtofloat(Self.PrecoVendaRetirado);
     Except
           Mensagem:=mensagem+'/Pre�o de Venda Retirado';
     End;

     try
        Strtofloat(Self.PrecoVendaFornecido);
     Except
           Mensagem:=mensagem+'/Pre�o de Venda Fornecido';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjVIDRO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjVIDRO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjVIDRO.LocalizaCodigo2(Parametro: String): boolean;
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro VIDRO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,codigo2,Referencia,GrupoVidro,Descricao,Unidade,Fornecedor');
           SQL.ADD(' ,Peso,PrecoCusto,PorcentagemInstalado,PorcentagemFornecido,PorcentagemRetirado');
           SQL.ADD(' ,PrecoVendaInstalado,PrecoVendaFornecido,PrecoVendaRetirado,Arredondamento');
           SQL.ADD(' ,AreaMinima,IsentoICMS_Estado,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado');
           SQL.ADD(' ,Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado');
           SQL.ADD(' ,IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado,ValorPauta_Sub_Trib_ForaEstado');
           SQL.ADD(' ,Aliquota_ICMS_ForaEstado,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado');
           SQL.ADD(' ,SituacaoTributaria_TabelaA,SituacaoTributaria_TabelaB,ClassificacaoFiscal,percentualagregado,ipi,ncm,Espessura,ativo,cest');
           SQL.ADD(' ');
           SQL.ADD(' from  TABVIDRO');
           SQL.ADD(' WHERE codigo2='+#39+parametro+#39);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;


function TObjVIDRO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro VIDRO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,codigo2,Referencia,GrupoVidro,Descricao,Unidade,Fornecedor');
           SQL.ADD(' ,Peso,PrecoCusto,PorcentagemInstalado,PorcentagemFornecido,PorcentagemRetirado');
           SQL.ADD(' ,PrecoVendaInstalado,PrecoVendaFornecido,PrecoVendaRetirado,Arredondamento');
           SQL.ADD(' ,AreaMinima,IsentoICMS_Estado,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado');
           SQL.ADD(' ,Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado');
           SQL.ADD(' ,IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado,ValorPauta_Sub_Trib_ForaEstado');
           SQL.ADD(' ,Aliquota_ICMS_ForaEstado,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado');
           SQL.ADD(' ,SituacaoTributaria_TabelaA,SituacaoTributaria_TabelaB,ClassificacaoFiscal,percentualagregado,ipi,ncm,Espessura,ativo,cest');
           SQL.ADD(' ');
           SQL.ADD(' from  TABVIDRO');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjVIDRO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjVIDRO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjVIDRO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.GrupoVidro:=TOBJGRUPOVIDRO.create;
        Self.Fornecedor:=TOBJFORNECEDOR.create;
        Self.SituacaoTributaria_TabelaA:=TOBJTABELAA_ST .create;
        Self.SituacaoTributaria_TabelaB:=TOBJTABELAB_ST .create;
        Self.ZerarTabela;

        With Self do
        Begin
             
                InsertSQL.clear;
                InsertSQL.add('Insert Into TABVIDRO(Codigo,codigo2,Referencia,GrupoVidro,Descricao');
                InsertSQL.add(' ,Unidade,Fornecedor,Peso,PrecoCusto,PorcentagemInstalado');
                InsertSQL.add(' ,PorcentagemFornecido,PorcentagemRetirado');
                InsertSQL.add(' ,Arredondamento');
                InsertSQL.add(' ,AreaMinima,IsentoICMS_Estado,SubstituicaoICMS_Estado');
                InsertSQL.add(' ,ValorPauta_Sub_Trib_Estado,Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado');
                InsertSQL.add(' ,Aliquota_ICMS_Cupom_Estado,IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado');
                InsertSQL.add(' ,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
                InsertSQL.add(' ,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado');
                InsertSQL.add(' ,SituacaoTributaria_TabelaA,SituacaoTributaria_TabelaB');
                InsertSQL.add(' ,ClassificacaoFiscal,PrecoVendaInstalado,PrecoVendaFornecido,PrecoVendaRetirado,percentualagregado,ipi,ncm,Espessura,ativo,cest)');
                InsertSQL.add('values (:Codigo,:codigo2,:Referencia,:GrupoVidro,:Descricao,:Unidade');
                InsertSQL.add(' ,:Fornecedor,:Peso,:PrecoCusto,:PorcentagemInstalado');
                InsertSQL.add(' ,:PorcentagemFornecido,:PorcentagemRetirado');
                InsertSQL.add(' ,:Arredondamento');
                InsertSQL.add(' ,:AreaMinima,:IsentoICMS_Estado,:SubstituicaoICMS_Estado');
                InsertSQL.add(' ,:ValorPauta_Sub_Trib_Estado,:Aliquota_ICMS_Estado');
                InsertSQL.add(' ,:Reducao_BC_ICMS_Estado,:Aliquota_ICMS_Cupom_Estado');
                InsertSQL.add(' ,:IsentoICMS_ForaEstado,:SubstituicaoICMS_ForaEstado');
                InsertSQL.add(' ,:ValorPauta_Sub_Trib_ForaEstado,:Aliquota_ICMS_ForaEstado');
                InsertSQL.add(' ,:Reducao_BC_ICMS_ForaEstado,:Aliquota_ICMS_Cupom_ForaEstado');
                InsertSQL.add(' ,:SituacaoTributaria_TabelaA,:SituacaoTributaria_TabelaB');
                InsertSQL.add(' ,:ClassificacaoFiscal,:PrecoVendaInstalado,:PrecoVendaFornecido,:PrecoVendaRetirado,:percentualagregado,:ipi,:ncm,:Espessura,:ativo,:cest)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABVIDRO set Codigo=:Codigo,codigo2=:codigo2,Referencia=:Referencia');
                ModifySQL.add(',GrupoVidro=:GrupoVidro,Descricao=:Descricao,Unidade=:Unidade');
                ModifySQL.add(',Fornecedor=:Fornecedor,Peso=:Peso,PrecoCusto=:PrecoCusto');
                ModifySQL.add(',PorcentagemInstalado=:PorcentagemInstalado,PorcentagemFornecido=:PorcentagemFornecido');
                ModifySQL.add(',PorcentagemRetirado=:PorcentagemRetirado');
                ModifySQL.add(',Arredondamento=:Arredondamento,AreaMinima=:AreaMinima');
                ModifySQL.add(',IsentoICMS_Estado=:IsentoICMS_Estado,SubstituicaoICMS_Estado=:SubstituicaoICMS_Estado');
                ModifySQL.add(',ValorPauta_Sub_Trib_Estado=:ValorPauta_Sub_Trib_Estado');
                ModifySQL.add(',Aliquota_ICMS_Estado=:Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado=:Reducao_BC_ICMS_Estado');
                ModifySQL.add(',Aliquota_ICMS_Cupom_Estado=:Aliquota_ICMS_Cupom_Estado');
                ModifySQL.add(',IsentoICMS_ForaEstado=:IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado=:SubstituicaoICMS_ForaEstado');
                ModifySQL.add(',ValorPauta_Sub_Trib_ForaEstado=:ValorPauta_Sub_Trib_ForaEstado');
                ModifySQL.add(',Aliquota_ICMS_ForaEstado=:Aliquota_ICMS_ForaEstado');
                ModifySQL.add(',Reducao_BC_ICMS_ForaEstado=:Reducao_BC_ICMS_ForaEstado');
                ModifySQL.add(',Aliquota_ICMS_Cupom_ForaEstado=:Aliquota_ICMS_Cupom_ForaEstado');
                ModifySQL.add(',SituacaoTributaria_TabelaA=:SituacaoTributaria_TabelaA');
                ModifySQL.add(',SituacaoTributaria_TabelaB=:SituacaoTributaria_TabelaB');
                ModifySQL.add(',ClassificacaoFiscal=:ClassificacaoFiscal,PrecoVendaInstalado=:PrecoVendaInstalado,PrecoVendaFornecido=:PrecoVendaFornecido,PrecoVendaRetirado=:PrecoVendaRetirado');
                ModifySQL.add(',percentualagregado=:percentualagregado,ipi=:ipi,ncm=:ncm,Espessura=:Espessura,ativo=:ativo,cest=:cest where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABVIDRO where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjVIDRO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjVIDRO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select Referencia,Descricao,GrupoVidro,Unidade');
     Self.ParametroPesquisa.add(',Peso,PrecoCusto,Fornecedor, PorcentagemInstalado,PorcentagemFornecido,PorcentagemRetirado');
     Self.ParametroPesquisa.add(',PrecoVendaInstalado,PrecoVendaFornecido,PrecoVendaRetirado,Arredondamento');
     Self.ParametroPesquisa.add(',AreaMinima,IsentoICMS_Estado,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado');
     Self.ParametroPesquisa.add(',Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado');
     Self.ParametroPesquisa.add(',IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado,ValorPauta_Sub_Trib_ForaEstado');
     Self.ParametroPesquisa.add(',Aliquota_ICMS_ForaEstado,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado');
     Self.ParametroPesquisa.add(',SituacaoTributaria_TabelaA,SituacaoTributaria_TabelaB,ClassificacaoFiscal,Codigo,codigo2,percentualagregado,ipi,ncm,Espessura,ativo,cest');
     Self.ParametroPesquisa.add(' from TabVIDRO');
     Result:=Self.ParametroPesquisa;
end;

function TObjVIDRO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Vidro ';
end;


function TObjVIDRO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENVIDRO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjVIDRO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.GrupoVidro.FREE;
    Self.Fornecedor.FREE;
    Self.SituacaoTributaria_TabelaA.FREE;
    Self.SituacaoTributaria_TabelaB.FREE;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjVIDRO.RetornaCampoCodigo: string;
begin
      result:='Codigo';
end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjVIDRO.RetornaCampoNome: string;
begin
      result:='DESCRICAO';
end;

procedure TObjVidro.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjVidro.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjVidro.Submit_Referencia(parametro: string);
begin
        Self.Referencia:=Parametro;
end;
function TObjVidro.Get_Referencia: string;
begin
        Result:=Self.Referencia;
end;
procedure TObjVidro.Submit_Descricao(parametro: string);
begin
        Self.Descricao:=Parametro;
end;
function TObjVidro.Get_Descricao: string;
begin
        Result:=Self.Descricao;
end;
procedure TObjVidro.Submit_Unidade(parametro: string);
begin
        Self.Unidade:=Parametro;
end;
function TObjVidro.Get_Unidade: string;
begin
        Result:=Self.Unidade;
end;
procedure TObjVidro.Submit_Peso(parametro: string);
begin
        Self.Peso:=Parametro;
end;
function TObjVidro.Get_Peso: string;
begin
        Result:=Self.Peso;
end;
procedure TObjVidro.Submit_PrecoCusto(parametro: string);
begin
        Self.PrecoCusto:=Parametro;
end;
function TObjVidro.Get_PrecoCusto: string;
begin
        Result:=Self.PrecoCusto;
end;
procedure TObjVidro.Submit_PorcentagemInstalado(parametro: string);
begin
        Self.PorcentagemInstalado:=Parametro;
end;
function TObjVidro.Get_PorcentagemInstalado: string;
begin
        Result:=Self.PorcentagemInstalado;
end;
procedure TObjVidro.Submit_PorcentagemFornecido(parametro: string);
begin
        Self.PorcentagemFornecido:=Parametro;
end;
function TObjVidro.Get_PorcentagemFornecido: string;
begin
        Result:=Self.PorcentagemFornecido;
end;
procedure TObjVidro.Submit_PorcentagemRetirado(parametro: string);
begin
        Self.PorcentagemRetirado:=Parametro;
end;
function TObjVidro.Get_PorcentagemRetirado: string;
begin
        Result:=Self.PorcentagemRetirado;
end;
function TObjVidro.Get_PrecoVendaInstalado: string;
begin
        Result:=Self.PrecoVendaInstalado;
end;
function TObjVidro.Get_PrecoVendaFornecido: string;
begin
        Result:=Self.PrecoVendaFornecido;
end;

function TObjVidro.Get_PrecoVendaRetirado: string;
begin
        Result:=Self.PrecoVendaRetirado;
end;
procedure TObjVidro.Submit_Arredondamento(parametro: string);
begin
        Self.Arredondamento:=Parametro;
end;
function TObjVidro.Get_Arredondamento: string;
begin
        Result:=Self.Arredondamento;
end;
procedure TObjVidro.Submit_AreaMinima(parametro: string);
begin
        Self.AreaMinima:=Parametro;
end;
function TObjVidro.Get_AreaMinima: string;
begin
        Result:=Self.AreaMinima;
end;
procedure TObjVidro.Submit_IsentoICMS_Estado(parametro: string);
begin
        Self.IsentoICMS_Estado:=Parametro;
end;
function TObjVidro.Get_IsentoICMS_Estado: string;
begin
        Result:=Self.IsentoICMS_Estado;
end;
procedure TObjVidro.Submit_SubstituicaoICMS_Estado(parametro: string);
begin
        Self.SubstituicaoICMS_Estado:=Parametro;
end;
function TObjVidro.Get_SubstituicaoICMS_Estado: string;
begin
        Result:=Self.SubstituicaoICMS_Estado;
end;
procedure TObjVidro.Submit_ValorPauta_Sub_Trib_Estado(parametro: string);
begin
        Self.ValorPauta_Sub_Trib_Estado:=Parametro;
end;
function TObjVidro.Get_ValorPauta_Sub_Trib_Estado: string;
begin
        Result:=Self.ValorPauta_Sub_Trib_Estado;
end;
procedure TObjVidro.Submit_Aliquota_ICMS_Estado(parametro: string);
begin
        Self.Aliquota_ICMS_Estado:=Parametro;
end;
function TObjVidro.Get_Aliquota_ICMS_Estado: string;
begin
        Result:=Self.Aliquota_ICMS_Estado;
end;
procedure TObjVidro.Submit_Reducao_BC_ICMS_Estado(parametro: string);
begin
        Self.Reducao_BC_ICMS_Estado:=Parametro;
end;
function TObjVidro.Get_Reducao_BC_ICMS_Estado: string;
begin
        Result:=Self.Reducao_BC_ICMS_Estado;
end;
procedure TObjVidro.Submit_Aliquota_ICMS_Cupom_Estado(parametro: string);
begin
        Self.Aliquota_ICMS_Cupom_Estado:=Parametro;
end;
function TObjVidro.Get_Aliquota_ICMS_Cupom_Estado: string;
begin
        Result:=Self.Aliquota_ICMS_Cupom_Estado;
end;
procedure TObjVidro.Submit_IsentoICMS_ForaEstado(parametro: string);
begin
        Self.IsentoICMS_ForaEstado:=Parametro;
end;
function TObjVidro.Get_IsentoICMS_ForaEstado: string;
begin
        Result:=Self.IsentoICMS_ForaEstado;
end;
procedure TObjVidro.Submit_SubstituicaoICMS_ForaEstado(parametro: string);
begin
        Self.SubstituicaoICMS_ForaEstado:=Parametro;
end;
function TObjVidro.Get_SubstituicaoICMS_ForaEstado: string;
begin
        Result:=Self.SubstituicaoICMS_ForaEstado;
end;
procedure TObjVidro.Submit_ValorPauta_Sub_Trib_ForaEstado(parametro: string);
begin
        Self.ValorPauta_Sub_Trib_ForaEstado:=Parametro;
end;
function TObjVidro.Get_ValorPauta_Sub_Trib_ForaEstado: string;
begin
        Result:=Self.ValorPauta_Sub_Trib_ForaEstado;
end;
procedure TObjVidro.Submit_Aliquota_ICMS_ForaEstado(parametro: string);
begin
        Self.Aliquota_ICMS_ForaEstado:=Parametro;
end;
function TObjVidro.Get_Aliquota_ICMS_ForaEstado: string;
begin
        Result:=Self.Aliquota_ICMS_ForaEstado;
end;
procedure TObjVidro.Submit_Reducao_BC_ICMS_ForaEstado(parametro: string);
begin
        Self.Reducao_BC_ICMS_ForaEstado:=Parametro;
end;
function TObjVidro.Get_Reducao_BC_ICMS_ForaEstado: string;
begin
        Result:=Self.Reducao_BC_ICMS_ForaEstado;
end;
procedure TObjVidro.Submit_Aliquota_ICMS_Cupom_ForaEstado(parametro: string);
begin
        Self.Aliquota_ICMS_Cupom_ForaEstado:=Parametro;
end;
function TObjVidro.Get_Aliquota_ICMS_Cupom_ForaEstado: string;
begin
        Result:=Self.Aliquota_ICMS_Cupom_ForaEstado;
end;

procedure TObjVidro.Submit_percentualagregado(parametro: string);
begin
        Self.percentualagregado:=Parametro;
end;
function TObjVidro.Get_percentualagregado: string;
begin
        Result:=Self.percentualagregado;
end;

procedure TObjVidro.Submit_ipi(parametro: string);
begin
        Self.ipi:=Parametro;
end;
function TObjVidro.Get_ipi: string;
begin
        Result:=Self.ipi;
end;


procedure TObjVidro.Submit_ClassificacaoFiscal(parametro: string);
begin
        Self.ClassificacaoFiscal:=Parametro;
end;
function TObjVidro.Get_ClassificacaoFiscal: string;
begin
        Result:=Self.ClassificacaoFiscal;
end;
//CODIFICA GETSESUBMITS


procedure TObjVIDRO.EdtGrupoVidroExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.GrupoVidro.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.GrupoVidro.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.GrupoVidro.GET_NOME;
End;
procedure TObjVIDRO.EdtGrupoVidroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.GrupoVidro.Get_Pesquisa,Self.GrupoVidro.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoVidro.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.GrupoVidro.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoVidro.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjVIDRO.EdtGrupoVidroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.GrupoVidro.Get_Pesquisa,Self.GrupoVidro.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoVidro.RETORNACAMPOCODIGO).asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjVIDRO.EdtVidroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabvidro','',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('CODIGO').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjVIDRO.EdtFornecedorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Fornecedor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Fornecedor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Fornecedor.Get_RazaoSocial;
End;
procedure TObjVIDRO.EdtFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FFORNECEDOR:TFFORNECEDOR;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FFORNECEDOR:=TFFORNECEDOR.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Fornecedor.Get_Pesquisa,Self.Fornecedor.Get_TituloPesquisa,FFornecedor)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Fornecedor.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Fornecedor.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Fornecedor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FFORNECEDOR);
     End;
end;
procedure TObjVIDRO.EdtSituacaoTributaria_TabelaAExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.SituacaoTributaria_TabelaA.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.SituacaoTributaria_TabelaA.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.SituacaoTributaria_TabelaA.Get_DESCRICAO;
End;
procedure TObjVIDRO.EdtSituacaoTributaria_TabelaAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FTABELAA_ST :TFTABELAA_ST ;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FTABELAA_ST :=TFTABELAA_ST .create(nil);

            If (FpesquisaLocal.PreparaPesquisa
(Self.SituacaoTributaria_TabelaA.Get_Pesquisa,Self.SituacaoTributaria_TabelaA.Get_TituloPesquisa,FTABELAA_ST)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SituacaoTributaria_TabelaA.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.SituacaoTributaria_TabelaA.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SituacaoTributaria_TabelaA.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FTABELAA_ST );
     End;
end;
procedure TObjVIDRO.EdtSituacaoTributaria_TabelaBExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.SituacaoTributaria_TabelaB.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.SituacaoTributaria_TabelaB.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.SituacaoTributaria_TabelaB.Get_DESCRICAO;
End;
procedure TObjVIDRO.EdtSituacaoTributaria_TabelaBKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FTABELAB_ST :TFTABELAB_ST ;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FTABELAB_ST :=TFTABELAB_ST .create(nil);

            If (FpesquisaLocal.PreparaPesquisa
           (Self.SituacaoTributaria_TabelaB.Get_Pesquisa,Self.SituacaoTributaria_TabelaB.Get_TituloPesquisa,FTABELAB_ST)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SituacaoTributaria_TabelaB.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.SituacaoTributaria_TabelaB.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SituacaoTributaria_TabelaB.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FTABELAB_ST );
     End;
end;
//CODIFICA EXITONKEYDOWN




procedure TObjVIDRO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJVIDRO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



function TObjVIDRO.VerificaReferencia(PStatus: TDataSetState; PCodigo,
  PReferencia: string): Boolean;
begin
     Result:=true;

     PReferencia := StrReplaceRef(PReferencia); {Rodolfo}

     With Self.Objquery do
     Begin

         if (PStatus = dsInsert) then  // quando for insercao
         Begin
             close;
             SQL.Clear;
             SQL.Add('Select Codigo from TabVidro Where Referencia = '+#39+UpperCase(PReferencia)+#39);
             Open;

             if (RecordCount > 0)then
             Begin
                 Result:=true;
                 exit;
             end else
             Result:=false;
             exit;
         end;


         if  (PStatus = dsEdit)then    // quando for edicao
         Begin
             if Self.ReferenciaAnterior = PReferencia then
             Begin                      // Se a Referencia  que esta chegando � a mesma da anteiror
                 Result:=false;         // eu nem preciso verificar nada
                 exit;
             end else
             Begin
                 close;
                 SQL.Clear;
                 SQL.Add('Select Codigo from TabVidro Where Referencia = '+#39+UpperCase(PReferencia)+#39);
                 Open;

                 if (RecordCount > 0)then
                 Begin
                     Result:=true;
                     exit;
                 end else
                 Result:=false;
                 exit;
             end;
         end;
     end;

end;

function TObjVIDRO.LocalizaReferencia(Parametro: string): boolean;
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro VIDRO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,codigo2,Referencia,GrupoVidro,Descricao,Unidade,Fornecedor');
           SQL.ADD(' ,Peso,PrecoCusto,PorcentagemInstalado,PorcentagemFornecido,PorcentagemRetirado');
           SQL.ADD(' ,PrecoVendaInstalado,PrecoVendaFornecido,PrecoVendaRetirado,Arredondamento');
           SQL.ADD(' ,AreaMinima,IsentoICMS_Estado,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado');
           SQL.ADD(' ,Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado');
           SQL.ADD(' ,IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado,ValorPauta_Sub_Trib_ForaEstado');
           SQL.ADD(' ,Aliquota_ICMS_ForaEstado,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado');
           SQL.ADD(' ,SituacaoTributaria_TabelaA,SituacaoTributaria_TabelaB,ClassificacaoFiscal,percentualagregado,ipi,ncm,Espessura,ativo,cest');
           SQL.ADD(' ');
           SQL.ADD(' from  TABVIDRO');
           SQL.ADD(' WHERE Referencia='+#39+parametro+#39);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

procedure TObjVIDRO.AtualizaMargens(PGrupoVidro: string);
Var QueryLOcal:TIBquery;
    PPorcentagemInstalado:string;
    PPorcentagemRetirado:string;
    PPorcentagemFornecido:string;
    NovoPreco:Currency;
begin
  if (PGrupoVidro='')then
  Begin
           MensagemErro('Escolha um Grupo de Vidro');
           exit;
  end;

  if (Self.GrupoVidro.LocalizaCodigo(PGrupoVidro)=false)then
  Begin
          MensagemErro('Grupo de Vidro n�o encontrado');
          exit;
  end;
  Self.GrupoVidro.TabelaparaObjeto;
  PPorcentagemInstalado:=Self.GrupoVidro.Get_PorcentagemInstalado;
  PPorcentagemRetirado:=Self.GrupoVidro.Get_PorcentagemRetirado;
  PPorcentagemFornecido:=Self.GrupoVidro.Get_PorcentagemFornecido;

  try
    try
      QueryLOcal:=TIBQuery.Create(nil);
      QueryLOcal.Database:=FDataModulo.IBDatabase;
    except;
      MensagemErro('Erro ao tenar criar a QueryLocal');
      exit;
    end;

    With QueryLOcal  do
    Begin
      Close;
      Sql.Clear;
      Sql.Add('Select TabVidro.Codigo from TabVidro');
      Sql.Add('Where GrupoVidro = '+PGrupoVidro);
      Open;
      First;

      // Para cada Vidro desse grupo eu Altero o Valor das margns
      While not (eof) do
      Begin
        Self.ZerarTabela;
        if (Self.LocalizaCodigo(fieldbyname('Codigo').AsString)=false)then
        Begin
          MensagemErro('Vidro n�o encontrada.');
          exit;
        end;

        Self.TabelaparaObjeto;
        Self.Status:=dsEdit;
        Self.Submit_PorcentagemInstalado(PPorcentagemInstalado);
        Self.Submit_PorcentagemFornecido(PPorcentagemFornecido);
        Self.Submit_PorcentagemRetirado(PPorcentagemRetirado);

        NovoPreco:=(StrToCurr(Get_PrecoCusto)+((StrToCurr(Get_PrecoCusto)*StrToCurr(PPorcentagemInstalado))/100));
        Self.Submit_PrecoVendaInstalado(CurrToStr(NovoPreco));
        NovoPreco:=(StrToCurr(Get_PrecoCusto)+((StrToCurr(Get_PrecoCusto)*StrToCurr(PorcentagemFornecido))/100));
        self.Submit_PrecoVendaFornecido(CurrToStr(NovoPreco));
        NovoPreco:=(StrToCurr(Get_PrecoCusto)+((StrToCurr(Get_PrecoCusto)*StrToCurr(PorcentagemRetirado))/100));
        self.Submit_PrecoVendaRetirado(CurrToStr(NovoPreco));
        
        if (Self.Salvar(false)=false)then
        begin
          MensagemErro('Erro ao tentar Atualiza as margens na Tabela de Vidros');
          exit;
        end;
        Next;
      end;
    end;
    FDataModulo.IBTransaction.CommitRetaining;
    MensagemAviso('Margem Atualiada com Sucesso !');

  finally
    FreeAndNil(QueryLOcal);
  end;
end;

function TObjVidro.PrimeiroRegistro: Boolean;
Var MenorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabVidro') ;
           Open;

           if (FieldByName('MenorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MenorCodigo:=fieldbyname('MenorCodigo').AsString;
           Self.LocalizaCodigo(MenorCodigo);

           Result:=true;
     end;
end;

function TObjVidro.UltimoRegistro: Boolean;
Var MaiorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabVidro') ;
           Open;

           if (FieldByName('MaiorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MaiorCodigo:=fieldbyname('MaiorCodigo').AsString;
           Self.LocalizaCodigo(MaiorCodigo);

           Result:=true;
     end;
end;

function TObjVidro.ProximoRegisto(PCodigo:Integer): Boolean;
Var MaiorValor:Integer;
begin
     Result:=false;

     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabVidro') ;
           Open;

           MaiorValor:=fieldbyname('MaiorCodigo').AsInteger;
     end;

     if (MaiorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Inc(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MaiorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Inc(PCodigo,1);
     end;

     Result:=true;
end;

function TObjVidro.RegistoAnterior(PCodigo: Integer): Boolean;
Var MenorValor:Integer;
begin
     Result:=false;
     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabVidro') ;
           Open;

           MenorValor:=fieldbyname('MenorCodigo').AsInteger;
     end;

     if (MenorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Dec(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MenorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Dec(PCodigo,1);
     end;

     Result:=true;
end;


procedure TObjVidro.AtualizaPrecos(PGrupoVidro: string);
Var QueryLOcal :TIBQuery; PVAlorCusto, PPorcentagemReajuste:Currency;
begin

      if (PGrupoVidro = '')then
      Begin
           MensagemErro('Pesquise um grupo de Vidro antes Atualizar os pre�os.');
           exit;
      end;

      if MessageDlg('Tem certeza que deseja alterar os valores dos PRE�OS DE CUSTO de '+#13+
                    'todos os Vidros pertencentes a este grupo?. Lembrando que esse processo � irrevers�vel.', mtConfirmation, [mbYes, mbNo], 0) = mrNo
      then Begin
           exit;
      end;

      With FfiltroImp do
      Begin
           DesativaGrupos;
           Grupo01.Enabled:=true;
           LbGrupo01.Caption:='Acr�scimo em (%)';
           edtgrupo01.Enabled:=True;

           ShowModal;

           if (Tag = 0)then
           Exit;

           try
                PPorcentagemReajuste:=StrToCurr(edtgrupo01.Text);
           except
                MensagemErro('Valor inv�lido.');
                exit;
           end;
      end;

      try
            try
                 QueryLocal:=TIBQuery.Create(nil);
                 QueryLOcal.Database:=FDataModulo.IBDatabase;
            except
                 MensagemErro('Erro ao tentar criar a Query Local');
                 exit;
            end;

            With  QueryLOcal do
            Begin
                 Close;
                 Sql.Clear;
                 Sql.Add('Select Codigo from TabVidro Where GrupoVidro = '+PGrupoVidro);
                 Open;
                 First;

                 if (RecordCount = 0)then
                 Begin
                      MensagemErro('Nenhuma Vidro encotrado para este grupo.');
                      exit;
                 end;


                 While Not (eof) do
                 Begin
                      PVAlorCusto:=0;
                      Self.ZerarTabela;
                      Self.LocalizaCodigo(fieldbyname('Codigo').AsString);
                      Self.TabelaparaObjeto;
                      PVAlorCusto:=StrToCurr(Self.Get_PrecoCusto);

                      Self.Status:=dsEdit;
                      Self.Submit_PrecoCusto(CurrToStr(PVAlorCusto+(PVAlorCusto*(PPorcentagemReajuste/100))));
                      try
                           Self.Salvar(false);
                      except
                           MensagemErro('Erro ao tentar Salvar o novo pre�o.');
                           FDataModulo.IBTransaction.RollbackRetaining;
                           exit;
                      end;
                      Next;
                 end;

                 FDataModulo.IBTransaction.CommitRetaining;
                 MensagemAviso('Pre�o de custo de vidro atualizado com Sucesso!');
            end;

      finally
            FreeAndNil(QueryLOcal);
      end;

end;

procedure TObjVidro.Reajusta(PIndice,Pgrupo: string);
var
   Psinal:string;
begin
  try
    strtofloat(Pindice);

    if (strtofloat(Pindice)>=0) then
      Psinal:='+'
    else
    begin
      Pindice:=floattostr(strtofloat(Pindice)*-1);
      Psinal:='-';
    End;
  except
    mensagemerro('Valor Inv�lido no �ndice');
    exit;
  End;

  if (Pgrupo<>'') then
  begin
    try
      strtoint(Pgrupo);
      if (Self.Grupovidro.LocalizaCodigo(pgrupo)=False) then
      begin
        MensagemErro('Grupo do vidro n�o encontrado');
        exit;
      end;
    Except
      mensagemerro('Grupo Inv�lido');
      exit;
    end;
  End;

  With Self.Objquery do
  Begin
    close;
    SQL.clear;
    SQL.add('Update Tabvidro Set PRECOCUSTO=PRECOCUSTO'+psinal+'((PRECOCUSTO*'+virgulaparaponto(Pindice)+')/100)');
    SQl.Add('precovendainstalado = precocusto + ( precocusto * porcentageminstalado/100),');
    SQl.Add('precovendafornecido = precocusto + ( precocusto * porcentagemfornecido/100),');
    SQl.Add('precovendaretirado = precocusto + ( precocusto * porcentagemretirado/100)');

    if (Pgrupo<>'') then
      SQL.add('Where Grupovidro='+Pgrupo);

    try
      execsql;
      FDataModulo.IBTransaction.CommitRetaining;
      mensagemaviso('Conclu�do');
    Except
      on e:exception do
      begin
        FDataModulo.IBTransaction.RollbackRetaining;
        mensagemerro('Erro na tentativa de Reajustar o valor do vidro'+#13+E.message);
        exit;
      End;
    End;
  End;
end;

function TObjVIDRO.Get_Codigo2: string;
begin
     Result:=Self.codigo2;
end;

procedure TObjVIDRO.Submit_Codigo2(parametro: string);
begin
     Self.codigo2:=parametro;
end;

procedure TObjVIDRO.Submit_PrecoVendaFornecido(parametro: string);
begin
     Self.PrecoVendaFornecido:=parametro;
end;

procedure TObjVIDRO.Submit_PrecoVendaInstalado(parametro: string);
begin
     Self.PrecoVendaInstalado:=parametro;
end;

procedure TObjVIDRO.Submit_PrecoVendaRetirado(parametro: string);
begin
     Self.PrecoVendaRetirado:=parametro;
end;

procedure TObjVIDRO.Submit_NCM(parametro:string);
begin
    Self.NCM:=parametro;
end;

function TObjVIDRO.Get_NCM:string;
begin
    result:=self.NCM;
end;

procedure TObjVIDRO.EdtVidroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;labelnome:TLabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabvidro','',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('CODIGO').asstring;
                                 labelnome.Caption:= FpesquisaLocal.querypesq.fieldbyname('descricao').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjVIDRO.Submit_Espessura(parametro:string);
begin
    Self.Espessura:=parametro;
end;

function TObjVIDRO.Get_Espessura:string;
begin
    Result:=Espessura;
end;

procedure TObjVIDRO.Submit_Ativo(parametro:string);
begin
    Self.Ativo:=parametro;
end;

function TObjVIDRO.Get_Ativo:string;
begin
    Result:=Self.Ativo;
end;

function TObjVIDRO.AtualizaTributospeloNCM:Boolean;
var
  qryprod, qryaux: TIBQuery;
  percentual,NCM:string;
begin
  {procuro por produtos onde o ncm estiver preenchido
  depois localizo no cadastro de ncm , resgato o valor do imposto
  e atualiza o produto
  }

  Result := False;
  qryprod := TIBQuery.Create(nil);
  qryaux := TIBQuery.Create(nil);

  qryprod.Database := FDataModulo.IBDatabase;
  qryaux.Database := FDataModulo.IBDatabase;
  try
    qryprod.DisableControls;
    qryprod.Close;
    qryprod.SQL.Clear;
    qryprod.SQL.Add('SELECT COUNT(CODIGO) FROM TABVIDRO WHERE NCM IS NOT NULL');
    qryprod.Open;

    FMostraBarraProgresso.ConfiguracoesIniciais(qryprod.Fields[0].AsInteger - 1,0);
    FMostraBarraProgresso.Lbmensagem.Caption := 'Atualizando percentuais de tributos (Vidro)';
    FMostraBarraProgresso.btcancelar.Visible := True;
    FMostraBarraProgresso.Show;
    Application.ProcessMessages;

    qryprod.Close;
    qryprod.SQL.Clear;
    qryprod.SQL.Add('select ficms.codigo,icms.sta,f.ncm');
    qryprod.SQL.Add('from tabimposto_icms icms');
    qryprod.SQL.Add('join tabvidro_icms ficms on ficms.imposto=icms.codigo');
    qryprod.SQL.Add('join tabvidro f on f.codigo=ficms.vidro');
    qryprod.SQL.Add('where f.ncm is not null');
    qryprod.SQL.Add('group by ficms.codigo, icms.sta,f.ncm');
    qryprod.Open;

    while not(qryprod.Eof) do
    begin
      NCM := qryprod.Fields[2].AsString;
      if(qryprod.Fields[1].AsString = '0') then
        percentual := get_campoTabela('PERCENTUALTRIBUTO','CODIGONCM','TABNCM',NCM)
      else
        if( (qryprod.Fields[1].AsString = '1') or (qryprod.Fields[1].AsString = '2')) then
          percentual := get_campoTabela('PERCENTUALTRIBUTOIMP','CODIGONCM','TABNCM',NCM)
        else
          percentual := '0';

      if(StrToCurrDef(percentual,0) > 0) then
      begin
        qryaux.DisableControls;
        qryaux.Close;
        qryaux.SQL.Clear;
        qryaux.SQL.Add('UPDATE tabvidro_icms SET PERCENTUALTRIBUTO=:PTRIBUTO WHERE CODIGO=:PPRODUTO');
        qryaux.Params[0].AsCurrency := StrToCurr(percentual);
        qryaux.Params[1].AsString := qryprod.Fields[0].AsString;

        try
          qryaux.ExecSQL;
        except
          raise Exception.Create('Erro ao atualizar percentual do tributo para o produto: '+qryprod.Fields[0].AsString);
        end;
      end;
      qryprod.Next;
      FMostraBarraProgresso.IncrementaBarra1(1);
      if(FMostraBarraProgresso.Cancelar) then
        Exit;      
      Application.ProcessMessages;
    end;
    qryaux.Transaction.CommitRetaining;
    //FDataModulo.IBTransaction.CommitRetaining;
    Result := True;
  finally
    FMostraBarraProgresso.Close;
    if(qryaux.Transaction.Active) then
      qryaux.Transaction.Rollback;
    if(qryprod <> nil) then
      FreeAndNil(qryprod);
    if(qryaux <> nil) then
      FreeAndNil(qryaux);
  end;

end;


function TObjVIDRO.Get_cest: string;
begin
  Result := Self.cest;
end;

procedure TObjVIDRO.Submit_cest(parametro: string);
begin
  self.cest := parametro;
end;

end.



