unit UobjCREDITO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UOBJCLIENTE,UOBJTITULO,sysutils,controls,forms, UReltxtRDPRINT  ,Rdprint, UObjLancamento;
//USES_INTERFACE

Type
   TObjCREDITO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               CLIENTE:TOBJCLIENTE;
               TITULO:TOBJTITULO;
               LANCAMENTO:TObjLancamento;

//CODIFICA VARIAVEIS PUBLICAS



                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    SalvarSemGerarTitulo(ComCommit:Boolean) :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Valor(parametro: string);
                Function Get_Valor: string;
                Procedure Submit_Historico(parametro: string);
                Function Get_Historico: string;
                Procedure Submit_Observacao(parametro: string);
                Function Get_Observacao: string;
               
                Procedure Opcoes(Pcodigo:string);

//CODIFICA DECLARA GETSESUBMITS

                procedure EdtCLIENTEExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtCLIENTEKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);Overload;
                procedure EdtCLIENTEKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);Overload;
                procedure EdtTITULOExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtTITULOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Function GeraeQuitatitulo:boolean;
                Function EstatisticaCredito(Pcliente:string):string;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Valor:string;
               Historico:string;
               Observacao:Tstringlist;
               Observacao_Stream:TmemoryStream;
//CODIFICA VARIAVEIS PRIVADAS

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                procedure ImprimeHistoricoCredito;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,Dialogs,UDatamodulo,
UmenuRelatorios, UCliente, UTitulo, UGeraTitulo, UObjGeraTitulo,
  ULancamentoFR, ExtCtrls;





Function  TObjCREDITO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
var
Temp:TStream;
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('CLIENTE').asstring<>'')
        Then Begin
                 If (Self.CLIENTE.LocalizaCodigo(FieldByName('CLIENTE').asstring)=False)
                 Then Begin
                          Messagedlg('Cliente N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CLIENTE.TabelaparaObjeto;
        End;
        If(FieldByName('TITULO').asstring<>'')
        Then Begin
                 If (Self.TITULO.LocalizaCodigo(FieldByName('TITULO').asstring)=False)
                 Then Begin
                          Messagedlg('T�tulo N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.TITULO.TabelaparaObjeto;
        End;
        Self.Valor:=formata_valor(fieldbyname('Valor').asstring);
        Self.Historico:=fieldbyname('Historico').asstring;

        //Self.Observacao:=fieldbyname('Observacao').asstring;
        try
           try
             Temp:=CreateBlobStream(FieldByName('Observacao'),bmRead);
             Self.Observacao.LoadFromStream(temp);
           Except
                 Messagedlg('Erro na Tentativa de Recuperar o valor do Campo descri��o!',mterror,[mbok],0);
                 exit;
           End;
        Finally
               freeandnil(temp);
        End;
        If(FieldByName('LANCAMENTO').asstring<>'')
        Then Begin
                 If (Self.LANCAMENTO.LocalizaCodigo(FieldByName('LANCAMENTO').asstring)=False)
                 Then Begin
                          Messagedlg('Lan�amento N�o encontrado!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.LANCAMENTO.TabelaparaObjeto;
        End;
        result:=True;
     End;

//CODIFICA TABELAPARAOBJETO

end;


Procedure TObjCREDITO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('CLIENTE').asstring:=Self.CLIENTE.GET_CODIGO;
        ParamByName('TITULO').asstring:=Self.TITULO.GET_CODIGO;
        ParamByName('Valor').asstring:=virgulaparaponto(come(Self.Valor,'.'));
        ParamByName('Historico').asstring:=Self.Historico;

        //ParamByName('Observacao').asstring:=Self.Observacao;
        Self.Observacao_Stream.Clear;
        Self.Observacao.SaveToStream(Self.Observacao_Stream);
        ParamByName('observacao').LoadFromStream(Self.Observacao_Stream,ftBlob);
        ParamByName('lancamento').AsString:=sELF.LANCAMENTO.Get_CODIGO;
//CODIFICA OBJETOPARATABELA

  End;
End;

//***********************************************************************

function TObjCREDITO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 if (Self.Status=dsinsert)
 Then Begin
           if (Self.GeraeQuitatitulo=False)
           Then Begin
                    result:=false;
                    exit;
           End;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCREDITO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        CLIENTE.ZerarTabela;
        TITULO.ZerarTabela;
        Valor:='';
        Historico:='';
        Observacao.clear;
        LANCAMENTO.ZerarTabela;
//CODIFICA ZERARTABELA
     End;
end;

Function TObjCREDITO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (CLIENTE.get_Codigo='')
      Then Mensagem:=mensagem+'/Cliente';
      //If (TITULO.get_Codigo='')
      //Then Mensagem:=mensagem+'/T�tulo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCREDITO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     If (Self.CLIENTE.LocalizaCodigo(Self.CLIENTE.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Cliente n�o Encontrado!'
     Else Self.CLIENTE.TabelaparaObjeto;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCREDITO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.CLIENTE.Get_Codigo<>'')
        Then Strtoint(Self.CLIENTE.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Cliente';
     End;
     try
        If (Self.TITULO.Get_Codigo<>'')
        Then Strtoint(Self.TITULO.Get_Codigo);
     Except
           Mensagem:=mensagem+'/T�tulo';
     End;
     try
        Strtofloat(come(Self.Valor,'.'));
     Except
           Mensagem:=mensagem+'/Valor';
     End;
     try
        If (Self.LANCAMENTO.Get_Codigo<>'')
        Then Strtoint(Self.LANCAMENTO.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Lan�amento';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCREDITO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCREDITO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCREDITO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CREDITO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,CLIENTE,TITULO,Valor,Historico,Observacao,lancamento');
           SQL.ADD(' from  tabcredito');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCREDITO.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjCREDITO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjCREDITO.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjCREDITO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.CLIENTE:=TOBJCLIENTE.create;
        Self.TITULO:=TOBJTITULO.create;

        Self.observacao:=TStringList.Create;
        Self.observacao_Stream:=TmemoryStream.Create;

        Self.LANCAMENTO:=TObjLancamento.Create;
        
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into tabcredito(CODIGO,CLIENTE,TITULO,Valor,Historico');
                InsertSQL.add(' ,Observacao,Lancamento)');
                InsertSQL.add('values (:CODIGO,:CLIENTE,:TITULO,:Valor,:Historico,:Observacao,:Lancamento');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update tabcredito set CODIGO=:CODIGO,CLIENTE=:CLIENTE');
                ModifySQL.add(',TITULO=:TITULO,Valor=:Valor,Historico=:Historico,Observacao=:Observacao,Lancamento=:Lancamento');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from tabcredito where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCREDITO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCREDITO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from VIEWCREDITO');
     Result:=Self.ParametroPesquisa;
end;

function TObjCREDITO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de CREDITO ';
end;


function TObjCREDITO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCREDITO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCREDITO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCREDITO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.CLIENTE.FREE;
    Self.TITULO.FREE;
    Freeandnil(observacao);
    Freeandnil(observacao_Stream);
    Self.LANCAMENTO.Free;
    //CODIFICA DESTRUICAO DE OBJETOS
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCREDITO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCREDITO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjcredito.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjcredito.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjcredito.Submit_Valor(parametro: string);
begin
        Self.Valor:=Parametro;
end;
function TObjcredito.Get_Valor: string;
begin
        Result:=Self.Valor;
end;
procedure TObjcredito.Submit_Historico(parametro: string);
begin
        Self.Historico:=Parametro;
end;
function TObjcredito.Get_Historico: string;
begin
        Result:=Self.Historico;
end;
procedure TObjcredito.Submit_Observacao(parametro: string);
begin
        Self.Observacao.Text:=Parametro;
end;
function TObjcredito.Get_Observacao: string;
begin
        Result:=Self.Observacao.Text;
end;
//CODIFICA GETSESUBMITS


procedure TObjCREDITO.EdtCLIENTEExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.CLIENTE.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.CLIENTE.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.CLIENTE.GET_NOME;
End;
procedure TObjCREDITO.EdtCLIENTEKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FCLIENTE:TFCLIENTE;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCLIENTE:=TFCLIENTE.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.CLIENTE.Get_Pesquisa,Self.CLIENTE.Get_TituloPesquisa,FCLIENTE)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CLIENTE.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CLIENTE.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CLIENTE.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCLIENTE);
     End;
end;
procedure TObjCREDITO.EdtTITULOExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.TITULO.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.TITULO.tabelaparaobjeto;
End;
procedure TObjCREDITO.EdtTITULOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FTITULO:TFTITULO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FTITULO:=TFTITULO.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.TITULO.Get_Pesquisa,Self.TITULO.Get_TituloPesquisa,FTITULO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TITULO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.TITULO.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TITULO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FTITULO);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjCREDITO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCREDITO';

          With RgOpcoes do
          Begin
                items.clear;
                Items.Add('Historico de Credito');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
              0:Self.ImprimeHistoricoCredito;
          End;

     end;

end;

Function TObjCREDITO.GeraeQuitatitulo:Boolean;
Var ObjGeraTitulo:TObjGeraTitulo;
    plancamento,ptitulo,ppendencia:string;
    phistorico:string;
begin

      result:=false;

      try
           ObjGeraTitulo:=TobjgeraTitulo.create;
      Except
            MensagemErro('N�o foi poss�vel criar o GERADOR');
            exit;
      End;

      //Vamos gerar um titulo a receber do cliente papa poder ter uma entrada no caixa
      //Em seguida mostra a tela de recebimento do titulo, onde o usuario vai escolher o portador e se
      //o recebimento vai ser em moeda ou cheque ou etc e tal

      Try

            Objgeratitulo.ZerarTabela;
            if (ObjGeraTitulo.LocalizaHistorico('LANCAMENTO DE CREDITO DE BONIFICACAO')=False)
            then Begin
                      Mensagemerro('Gerador de T�tulo n�o encontrado "LANCAMENTO DE CREDITO DE BONIFICACAO"');
                      exit;
            End;
            ObjGeraTitulo.TabelaparaObjeto;

            if (strtoint(ObjGeraTitulo.Prazo.Get_Parcelas)<>1)
            Then Begin
                      MensagemErro('O gerador de t�tulo "LANCAMENTO DE CREDITO DE BONIFICACAO" est� com prazo diferente de A VISTA');
                      exit;
            End;


            Try
                  //Gera o t�tulo a receber
                  ptitulo:='';
                  Self.TITULO.ZerarTabela;
                  Self.TITULO.Status:=dsinsert;
                  ptitulo:=Self.TITULO.Get_NovoCodigo;
                  Self.TITULO.Submit_CODIGO(ptitulo);
                  Self.TITULO.Submit_HISTORICO('Lan�amento de Cr�dito de Bonifica��o '+Self.CLIENTE.Get_Nome);
                  Self.TITULO.Submit_GERADOR(ObjGeraTitulo.Get_Gerador);
                  Self.TITULO.Submit_CODIGOGERADOR(ObjGeraTitulo.Get_CodigoGerador);
                  Self.TITULO.Submit_CREDORDEVEDOR(ObjGeraTitulo.get_credordevedor);
                  Self.TITULO.Submit_CODIGOCREDORDEVEDOR(Self.CLIENTE.get_codigo);
                  Self.TITULO.Submit_EMISSAO(datetostr(now));
                  Self.TITULO.Submit_PRAZO(ObjGeraTitulo.Get_Prazo);
                  Self.TITULO.Submit_PORTADOR(ObjGeraTitulo.Get_Portador);
                  Self.TITULO.Submit_VALOR(Self.Valor);
                  Self.TITULO.Submit_CONTAGERENCIAL(ObjGeraTitulo.Get_ContaGerencial);
                  Self.TITULO.Submit_NUMDCTO('');
                  Self.TITULO.Submit_NotaFiscal('');
                  Self.TITULO.Submit_GeradoPeloSistema('N');
                  Self.TITULO.Submit_ExportaLanctoContaGer('N');
                  Self.TITULO.Submit_ParcelasIguais(true);

                  if (Self.TITULO.Salvar(False)=False)
                  then Begin
                            mensagemerro('Erro na tentativa de salvar o t�tulo a pagar');
                            strtoint('a');
                            exit;
                  End;


                  if (Self.LANCAMENTO.Pendencia.LocalizaporTitulo(ptitulo)=False)
                  Then Begin
                            MensagemAviso('Pend�ncia para o t�tulo n�o localizada');
                            strtoint('a');
                            exit;
                  End;
                  Self.LANCAMENTO.Pendencia.TabelaparaObjeto;
                  ppendencia:=Self.LANCAMENTO.Pendencia.get_codigo;


                  Phistorico:='Recebimento de Cr�dito de Bonifica��o '+Self.CLIENTE.get_nome;
                  Plancamento:=Self.LANCAMENTO.Get_NovoCodigo;

                  //chamo essa versao do NovoLancamento pois o codigo retorna
                  if (Self.LANCAMENTO.NovoLancamento(ppendencia,'R',Self.valor,plancamento,phistorico,False,true,true,objgeratitulo.Get_CredorDevedor,Self.cliente.get_codigo,datetostr(now))=False)
                  Then Begin

                            Messagedlg('Erro na tentativa de Quitar',mterror,[mbok],0);
                            strtoint('a');
                            exit;
                  End;

                  self.Status:=dsEdit;
                  Self.LANCAMENTO.Submit_CODIGO(plancamento);
                  If(self.Salvar(false)=false)
                  Then Begin
                            strtoint('a');
                            Mensagemerro('Erro ao gravar titulo na tabcredito');
                            Exit;
                  End;

                  //aumentando o credito no cadastro de cliente


                  FDataModulo.IBTransaction.CommitRetaining;
                  result:=true;

            Except
                  FDataModulo.IBTransaction.RollbackRetaining;

                  if (ptitulo<>'')
                  Then Begin
                            if (self.LocalizaCodigo(Self.codigo)=True)
                            Then Begin
                                      Self.status:=dsedit;
                                      Self.TITULO.submit_codigo('');
                                      Self.salvar(true);
                            End;

                            if  (Self.TITULO.LocalizaCodigo(ptitulo)=True)
                            then Self.LANCAMENTO.ExcluiTitulo(PTITULO,TRUE);
                  End;

                  if (self.LocalizaCodigo(Self.codigo)=True)
                  Then Begin
                            Self.Exclui(Self.codigo,true);
                  End;
                  
            End;

      Finally
            ObjGeraTitulo.Free;
            //objlancamento.free;

      End;

end;

function TObjCREDITO.EstatisticaCredito(Pcliente: string): string;
begin
      with Objquery do
      Begin
            close;
            sql.clear;
            sql.add('select sum(valor) as soma from tabcredito where cliente='+pcliente);
            open;
            last;
            if(recordcount<>0)
            Then result:=fieldbyname('soma').asstring
            Else result:='';
      End;
end;

procedure TObjCREDITO.Opcoes(Pcodigo: string);
begin
     //Opcoes de Impress�o
     With FopcaoRel do
     Begin
          With RgOpcoes do
          Begin
                items.clear;
                items.add('Reimpress�o de comprovante de Recebimento');//0
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0:Begin
                      If(Self.CODIGO<>'')
                      Then Begin
                            If(Self.LANCAMENTO.Get_CODIGO<>'')
                            Then Begin
                                  Self.LANCAMENTO.ImprimeRecibo(Self.Lancamento.Get_CODIGO,'','');
                            End
                            Else Begin
                                  MensagemAviso('N�o Houve quita��o no lan�amento do cr�dito '+self.Get_CODIGO);
                                  Exit;
                            End;
                      End
                      Else Begin
                            MensagemAviso('Escolha um lan�amento de Cr�dito para Reimpress�o de Recibo!');
                            Exit;
                      End;
                End;
          End;
     end;
end;

procedure TObjCREDITO.ImprimeHistoricoCredito;
Var  PCliente, PDataInicial, PDataFinal, PHistorico:String;
     Linha:Integer;
     SubTotal, Total:Currency;

Begin
      With FfiltroImp do
      Begin
           DesativaGrupos;
           Grupo01.Enabled:=true;
           edtgrupo01.OnKeyDown:=Self.EdtCLIENTEKeyDown;
           LbGrupo01.Caption:='Cliente';
           edtgrupo01.EditMask:='';

           Grupo02.Enabled:=true;
           edtgrupo02.EditMask:=MascaraData;
           LbGrupo02.Caption:='Data Inicial';

           Grupo03.Enabled:=true;
           edtgrupo03.EditMask:=MascaraData;
           LbGrupo03.Caption:='Data Final';

           ShowModal;
           if (Tag = 0)
           then exit;

           PCliente:=edtgrupo01.Text;
           PDataInicial:=edtgrupo02.Text;
           PDataFinal:=edtgrupo03.Text;
      end;

      if (PCliente <> '')
      then Begin
              if (Self.CLIENTE.LocalizaCodigo(PCliente)=false)
              then Begin
                       MensagemErro('Cliente n�o encontrado');
                       exit;
              end;
      end;

      try
           if (PDataInicial <> '  /  /    ')
           then StrToDate(PDataInicial);
      except
           MensagemErro('Data Inicial Inv�lida');
           exit;
      end;

      try
           if (PDataFinal <> '  /  /    ')
           then StrToDate(PDataFinal);
      except
           MensagemErro('Data Final Inv�lida');
           exit;
      end;

      AbrirImpressao;
      Linha:=3;
      ImprimirNegrito(Linha,30, 'LAN�AMENTOS DE CR�DITO');
      inc(Linha,2);

      With Self.Objquery do
      Begin
           Close;
           SQL.Clear;
           SQL.Add('Select TabLancamentoCredito.Codigo, TabLancamentoCredito.Data, TabCliente.Codigo as Cliente, TabCliente.Nome as NOmeCliente,');
           SQL.Add('TabLancamento.Historico as HistoricoLancamento, TabCredito.Historico as HistoricoCredito,');
           SQL.Add('TabLancamentoCredito.Valor');{, TabLOTECREDITOPONTOS.Codigo as LOTECREDITOPONTOS');  }
           SQL.Add('from TabLancamentoCredito');
           SQL.Add('join TabCliente on TabCliente.Codigo = TabLancamentoCredito.Cliente');
           SQL.Add('left join TabValores on TabValores.Codigo = TabLancamentoCredito.Valores');
           SQL.Add('left join TabLancamento on TabLancamento.Codigo = TabValores.Lancamento');
           SQL.Add('left join TabCredito on TabCredito.Codigo = TabLancamentoCredito.Credito');
           {SQL.Add('left join TabLOTECREDITOPONTOS on TabLOTECREDITOPONTOS.Codigo = TabLancamentoCredito.LOTECREDITOPONTOS');     }
           SQL.Add('Where TabLancamentoCredito.Codigo > -100 ');

           if (PCliente <> '')
           then SQL.Add('And TabCliente.Codigo =  '+PCliente);

           if (PDataInicial <> '  /  /    ')
           then SQL.Add('And TabLancamentoCredito.Data >= '+#39+FormatDateTime('mm/dd/yyyy', StrtoDate(PDataInicial))+#39);

           if (PDataFinal <> '  /  /    ')
           then SQL.Add('And TabLancamentoCredito.Data <= '+#39+FormatDateTime('mm/dd/yyyy', StrtoDate(PDataFinal))+#39);

           SQL.Add('Order By TabCliente.Codigo, TabLancamento.data');

           //InputBox('a','a',sql.text);

           Open;

           SubTotal:=0;
           Total:=0;
           PCliente:=fieldbyname('Cliente').AsString;
           ImprimirNegrito(Linha,1, 'CLIENTE: '+fieldbyname('NomeCliente').asString);
           inc(linha,1);
           ImprimirNegrito(Linha,1,CompletaPalavra('CODIGO',10,' ')+' '+
                                    CompletaPalavra('DATA',10,' ')+' '+
                                    CompletaPalavra('HIST�RICO',50,' ')+' '+
                                    CompletaPalavra_a_Esquerda('VALOR',15,' '));
           inc(linha,1);
           While Not (eof) do
           Begin

                if (PCliente<>fieldbyname('Cliente').AsString)
                then Begin
                      ImprimirNegrito(Linha,1,CompletaPalavra(' ',10,' ')+' '+
                      CompletaPalavra(' ' ,10,' ')+' '+
                      CompletaPalavra(' ',50,' ')+' '+
                      CompletaPalavra_a_Esquerda(formata_valor(SubTotal),15,' '));
                      inc(linha,1);
                      ImprimirNegrito(Linha,1, 'CLIENTE: '+fieldbyname('NomeCliente').asString);
                      inc(linha,1);
                      ImprimirNegrito(Linha,1,CompletaPalavra('CODIGO',10,' ')+' '+
                                              CompletaPalavra('DATA',10,' ')+' '+
                                              CompletaPalavra('HIST�RICO',50,' ')+' '+
                                              CompletaPalavra_a_Esquerda('VALOR',15,' '));
                      SubTotal:=0;
                      inc(linha,1);
                      PCliente:=fieldbyname('Cliente').AsString;
                end;


                PHISTORICO:='';
                if (fieldbyname('HistoricoLancamento').AsString <> '')
                then PHistorico:='PAG. - '+fieldbyname('HistoricoLancamento').AsString
                else if (fieldbyname('HistoricoCredito').AsString <> '')
                     then PHistorico:=fieldbyname('HistoricoCredito').AsString ;
                    { else if (fieldbyname('LOTECREDITOPONTOS').AsString <> '')
                          then PHistorico:='LOTE '+fieldbyname('LOTECREDITOPONTOS').AsString+' de Cr�ditos por Pontos';  }


                ImprimirSimples(Linha,1,CompletaPalavra(fieldbyname('CODIGO').AsString ,10,' ')+' '+
                        CompletaPalavra(fieldbyname('DATA').AsString ,10,' ')+' '+
                        CompletaPalavra(PHISTORICO,50,' ')+' '+
                        CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR').AsString),15,' '));
                inc(linha,1);
                SubTotal:=SubTotal+fieldbyname('VALOR').AsCurrency;
                Total:=Total+fieldbyname('VALOR').AsCurrency;
                Next;
           end;

           ImprimirNegrito(Linha,1,CompletaPalavra(' ' ,10,' ')+' '+
           CompletaPalavra(' ' ,10,' ')+' '+
           CompletaPalavra(' ',50,' ')+' '+
           CompletaPalavra_a_Esquerda(formata_valor(SubTotal),15,' '));
           inc(linha,2);

           ImprimirNegrito(Linha,1,CompletaPalavra(' ' ,10,' ')+' '+
           CompletaPalavra(' ' ,10,' ')+' '+
           CompletaPalavra(' ',50,' ')+' '+
           CompletaPalavra_a_Esquerda(formata_valor(Total),15,' '));
      end;                                         

      FecharImpressao;
end;


procedure TObjCREDITO.EdtCLIENTEKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.CLIENTE.Get_Pesquisa,Self.CLIENTE.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CLIENTE.RETORNACAMPOCODIGO).asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjCREDITO.SalvarSemGerarTitulo(ComCommit:Boolean):Boolean;
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;


 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

end.




