unit UobjCOR;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal;

Type
   TObjCOR=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                ReferenciaAnterior                          :string;
                
                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaReferencia(Parametro:string) :boolean;

                Function    LocalizaReferenciaCorFerragem(Parametro:string) :boolean;
                Function    LocalizaReferenciaCorPerfilado(Parametro:string) :boolean;
                Function    LocalizaReferenciaCorVidro(Parametro:string) :boolean;
                Function    LocalizaReferenciaCorKitBox(Parametro:string) :boolean;
                Function    LocalizaReferenciaCorPersiana(Parametro:string) :boolean;
                Function    LocalizaReferenciaCorDiverso(Parametro:string) :boolean;


                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;

                Function    Get_PesquisaCorFerragem         :TStringList;overload;
                Function    Get_PesquisaCorFerragem(pprojeto:string):TStringList;overload;
                function    Get_PesquisaCorFerragem(PCodigo:Integer): TStringList;overload;

                Function    Get_PesquisaCorPErfilado        :TStringList;overload;
                Function    Get_PesquisaCorPErfilado        (pprojeto:string):TStringList;overload;
                function    Get_PesquisaCorPErfilado(PCodigo: Integer): TStringList;overload;

                Function    Get_PesquisaCorVidro            :TStringList;overload;
                Function    Get_PesquisaCorVidro(pprojeto:string): TStringList;overload;
                function    Get_PesquisaCorVidro(PCodigo: Integer): TStringList;overload;

                Function    Get_PesquisaCorKItBox           :TStringList;overload;
                Function    Get_PesquisaCorKItBox(pprojeto:string): TStringList;overload;
                function    Get_PesquisaCorKItBox(PCodigo: Integer): TStringList;overload;

                Function    Get_PesquisaCorDiverso          :TStringList;overload;
                function    Get_PesquisaCorDiverso(pprojeto:string): TStringList;overload;
                function    Get_PesquisaCorDiverso(pcodigo: integer): TStringList;overload;

                Function    Get_PesquisaCorPersianaGrupoDiametro :TStringList;overload;
                Function    Get_PesquisaCorPersianaGrupoDiametro (pcodigo: integer):TStringList;overload;



                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime;
                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Referencia(parametro: string);
                Function Get_Referencia: string;
                Procedure Submit_Descricao(parametro: string);
                Function Get_Descricao: string;
                Procedure Submit_PorcentagemAcrescimo(parametro: string);
                Function Get_PorcentagemAcrescimo: string;
                Procedure Submit_CodigoCorDelphi(parametro: string);
                Function Get_CodigoCorDelphi: string;
                Function  VerificaReferencia(PStatus:TDataSetState; PCodigo,PReferencia:string):Boolean; {Rodolfo}
                function PrimeiroRegistro: Boolean;
                function ProximoRegisto(PCodigo: Integer): Boolean;
                function RegistoAnterior(PCodigo: Integer): Boolean;
                function UltimoRegistro: Boolean;
                procedure EdtCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel; PcodigoMaterial: string;PMATERIAL: STring);


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Referencia:string;
               Descricao:string;
               PorcentagemAcrescimo:string;
               CodigoCorDelphi:string;
//CODIFICA VARIAVEIS PRIVADAS

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;






   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls, UMenuRelatorios
//USES IMPLEMENTATION
;


{ TTabTitulo }


Function  TObjCOR.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Referencia:=fieldbyname('Referencia').asstring;
        Self.Descricao:=fieldbyname('Descricao').asstring;
        Self.PorcentagemAcrescimo:=fieldbyname('PorcentagemAcrescimo').asstring;
        Self.CodigoCorDelphi:=fieldbyname('CodigoCorDelphi').asstring;
//CODIFICA TABELAPARAOBJETO





        result:=True;
     End;
end;


Procedure TObjCOR.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Referencia').asstring:=Self.Referencia;
        ParamByName('Descricao').asstring:=Self.Descricao;
        ParamByName('PorcentagemAcrescimo').asstring:=virgulaparaponto(Self.PorcentagemAcrescimo);
        ParamByName('CodigoCorDelphi').asstring:=Self.CodigoCorDelphi;
//CODIFICA OBJETOPARATABELA





  End;
End;

//***********************************************************************

function TObjCOR.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


  if Self.status=dsinsert
  Then Begin
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
  End;

   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCOR.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Referencia:='';
        Descricao:='';
        PorcentagemAcrescimo:='';
        CodigoCorDelphi:='';
//CODIFICA ZERARTABELA





     End;
end;

Function TObjCOR.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Descricao='')
      Then Mensagem:=mensagem+'/Descri��o da Cor';
      If (Referencia='')
      Then Mensagem:=mensagem+'/Referencia';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjCOR.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjCOR.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        Strtofloat(Self.PorcentagemAcrescimo);
     Except
           Mensagem:=mensagem+'/Porcentagem Acrescimo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjCOR.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjCOR.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';
//CODIFICA VERIFICAFAIXA


     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjCOR.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabCOR est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Referencia,Descricao,PorcentagemAcrescimo,CodigoCorDelphi');
           SQL.ADD(' ');
           SQL.ADD(' from  TabCor');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCOR.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCOR.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCOR.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabCor(Codigo,Referencia,Descricao,PorcentagemAcrescimo');
                InsertSQL.add(' ,CodigoCorDelphi)');
                InsertSQL.add('values (:Codigo,:Referencia,:Descricao,:PorcentagemAcrescimo');
                InsertSQL.add(' ,:CodigoCorDelphi)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabCor set Codigo=:Codigo,Referencia=:Referencia');
                ModifySQL.add(',Descricao=:Descricao,PorcentagemAcrescimo=:PorcentagemAcrescimo');
                ModifySQL.add(',CodigoCorDelphi=:CodigoCorDelphi');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabCor where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCOR.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCOR.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCOR');
     Result:=Self.ParametroPesquisa;
end;

function TObjCOR.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de COR ';
end;


function TObjCOR.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCOR,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCOR,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCOR.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCOR.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCOR.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjCor.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjCor.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjCor.Submit_Referencia(parametro: string);
begin
        Self.Referencia:=Parametro;
end;
function TObjCor.Get_Referencia: string;
begin
        Result:=Self.Referencia;
end;
procedure TObjCor.Submit_Descricao(parametro: string);
begin
        Self.Descricao:=Parametro;
end;
function TObjCor.Get_Descricao: string;
begin
        Result:=Self.Descricao;
end;
procedure TObjCor.Submit_PorcentagemAcrescimo(parametro: string);
begin
        Self.PorcentagemAcrescimo:=Parametro;
end;
function TObjCor.Get_PorcentagemAcrescimo: string;
begin
        Result:=Self.PorcentagemAcrescimo;
end;
procedure TObjCor.Submit_CodigoCorDelphi(parametro: string);
begin
        Self.CodigoCorDelphi:=Parametro;
end;
function TObjCor.Get_CodigoCorDelphi: string;
begin
        Result:=Self.CodigoCorDelphi;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjCOR.Imprime;
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCOR';
          With RgOpcoes do
          Begin
               items.clear;
                // Addicionar os Items.Add
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;       // Chamada de Func�es
          end;
     end;
end;

function TObjCOR.VerificaReferencia(PStatus: TDataSetState; PCodigo,
  PReferencia: string): Boolean;  {Rodolfo}
begin
     Result:=true;

     PReferencia := StrReplaceRef(PReferencia); {Rodolfo}

     With Self.Objquery do
     Begin

         if (PStatus = dsInsert) then  // quando for insercao
         Begin
             close;
             SQL.Clear;
             SQL.Add('Select Codigo from TabCor Where Referencia = '+#39+UpperCase(PReferencia)+#39);
             Open;

             if (RecordCount > 0)then
             Begin
                 Result:=true;
                 exit;
             end else
             Result:=false;
             exit;
         end;


         if  (PStatus = dsEdit)then    // quando for edicao
         Begin
             if Self.ReferenciaAnterior = PReferencia then
             Begin                      // Se a Referencia  que esta chegando � a mesma da anteiror
                 Result:=false;         // eu nem preciso verificar nada
                 exit;
             end else
             Begin
                 close;
                 SQL.Clear;
                 SQL.Add('Select Codigo from TabCor Where Referencia = '+#39+UpperCase(PReferencia)+#39);
                 Open;

                 if (RecordCount > 0)then
                 Begin
                     Result:=true;
                     exit;
                 end else
                 Result:=false;
                 exit;
             end;
         end;
     end;
end;

function TObjCOR.LocalizaReferencia(Parametro: string): boolean;
begin
       if (Parametro = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabCOR est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Referencia,Descricao,PorcentagemAcrescimo,CodigoCorDelphi');
           SQL.ADD(' ');
           SQL.ADD(' from  TabCor');
           SQL.ADD(' WHERE Referencia='+#39+parametro+#39);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;
function TObjCor.PrimeiroRegistro: Boolean;
Var MenorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabCor') ;
           Open;

           if (FieldByName('MenorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MenorCodigo:=fieldbyname('MenorCodigo').AsString;
           Self.LocalizaCodigo(MenorCodigo);

           Result:=true;
     end;
end;

function TObjCor.UltimoRegistro: Boolean;
Var MaiorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabCor') ;
           Open;

           if (FieldByName('MaiorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MaiorCodigo:=fieldbyname('MaiorCodigo').AsString;
           Self.LocalizaCodigo(MaiorCodigo);

           Result:=true;
     end;
end;

function TObjCor.ProximoRegisto(PCodigo:Integer): Boolean;
Var MaiorValor:Integer;
begin
     Result:=false;

     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabCor') ;
           Open;

           MaiorValor:=fieldbyname('MaiorCodigo').AsInteger;
     end;

     if (MaiorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Inc(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MaiorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Inc(PCodigo,1);
     end;

     Result:=true;
end;

function TObjCor.RegistoAnterior(PCodigo: Integer): Boolean;
Var MenorValor:Integer;
begin
     Result:=false;
     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabCor') ;
           Open;

           MenorValor:=fieldbyname('MenorCodigo').AsInteger;
     end;

     if (MenorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Dec(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MenorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Dec(PCodigo,1);
     end;

     Result:=true;
end;

function TObjCOR.Get_PesquisaCorFerragem: TStringList;
begin
     Result:=Self.Get_PesquisaCorFerragem(-1);
end;

function TObjCOR.Get_PesquisaCorFerragem(PCodigo:Integer): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select distinct(tabcor.codigo), TabCor.Referencia,TabCor.Descricao, TabCor.PorcentagemAcrescimo,');
     Self.ParametroPesquisa.add('TabCor.CodigoCorDelphi');
     Self.ParametroPesquisa.add('from tabferragemcor');
     Self.ParametroPesquisa.add('join tabcor on tabferragemcor.cor=tabcor.codigo');

     if (Pcodigo>=0)
     Then Self.ParametroPesquisa.add('where Tabferragemcor.ferragem='+inttostr(pcodigo));

     Result:=Self.ParametroPesquisa;
end;

function TObjCOR.Get_PesquisaCorFerragem(pprojeto:string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select tabcor.codigo,tabcor.referencia,tabcor.descricao,tabcor.porcentagemacrescimo');
     Self.ParametroPesquisa.add('from PROC_COR_PEDIDO_FERRAGEM('+pprojeto+') PROC');
     Self.ParametroPesquisa.add('join tabcor on tabcor.codigo=proc.cor');

     Result:=Self.ParametroPesquisa;
end;


function TObjCOR.Get_PesquisaCorDiverso: TStringList;
begin
     Result:=Self.Get_PesquisaCorDiverso(-1);
End;

function TObjCOR.Get_PesquisaCorDiverso(pcodigo:integer): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select distinct(tabcor.codigo), TabCor.Referencia,TabCor.Descricao, TabCor.PorcentagemAcrescimo,');
     Self.ParametroPesquisa.add('TabCor.CodigoCorDelphi');
     Self.ParametroPesquisa.add('from tabDiversocor');
     Self.ParametroPesquisa.add('join tabcor on tabDiversocor.cor=tabcor.codigo');

     if (Pcodigo>=0)
     Then Self.ParametroPesquisa.add('where Tabdiversocor.diverso='+inttostr(pcodigo));
     
     Result:=Self.ParametroPesquisa;

end;


function TObjCOR.Get_PesquisaCorDiverso(pprojeto:string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select tabcor.codigo,tabcor.referencia,tabcor.descricao,tabcor.porcentagemacrescimo');
     Self.ParametroPesquisa.add('from PROC_COR_PEDIDO_DIVERSO('+pprojeto+') PROC');
     Self.ParametroPesquisa.add('join tabcor on tabcor.codigo=proc.cor');

     Result:=Self.ParametroPesquisa;

end;
function TObjCOR.Get_PesquisaCorKItBox: TStringList;
begin
     Result:=Get_PesquisaCorKItBox(-1);
End;

function TObjCOR.Get_PesquisaCorKItBox(PCodigo: Integer): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select distinct(tabcor.codigo), TabCor.Referencia,TabCor.Descricao, TabCor.PorcentagemAcrescimo,');
     Self.ParametroPesquisa.add('TabCor.CodigoCorDelphi');
     Self.ParametroPesquisa.add('from tabKitBoxcor');
     Self.ParametroPesquisa.add('join tabcor on tabKitBoxcor.cor=tabcor.codigo');
     if (Pcodigo>=0)
     Then Self.ParametroPesquisa.add('where Tabkitboxcor.kitbox='+inttostr(pcodigo));

     Result:=Self.ParametroPesquisa;

end;


function TObjCOR.Get_PesquisaCorKItBox(pprojeto:string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select tabcor.codigo,tabcor.referencia,tabcor.descricao,tabcor.porcentagemacrescimo');
     Self.ParametroPesquisa.add('from PROC_COR_PEDIDO_KITBOX('+pprojeto+') PROC');
     Self.ParametroPesquisa.add('join tabcor on tabcor.codigo=proc.cor');
     Result:=Self.ParametroPesquisa;

end;
function TObjCOR.Get_PesquisaCorPErfilado: TStringList;
BEgin
     Result:=Self.Get_PesquisaCorPErfilado(-1);
End;

function TObjCOR.Get_PesquisaCorPErfilado(PCodigo:Integer): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select distinct(tabcor.codigo), TabCor.Referencia,TabCor.Descricao, TabCor.PorcentagemAcrescimo,');
     Self.ParametroPesquisa.add('TabCor.CodigoCorDelphi');
     Self.ParametroPesquisa.add('from tabPerfiladocor');
     Self.ParametroPesquisa.add('join tabcor on tabPerfiladocor.cor=tabcor.codigo');
     if (Pcodigo>=0)
     Then Self.ParametroPesquisa.add('where Tabperfiladocor.perfilado='+inttostr(pcodigo));

     Result:=Self.ParametroPesquisa;

end;

function TObjCOR.Get_PesquisaCorPErfilado(pprojeto:string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select tabcor.codigo,tabcor.referencia,tabcor.descricao,tabcor.porcentagemacrescimo');
     Self.ParametroPesquisa.add('from PROC_COR_PEDIDO_PERFILADO('+pprojeto+') PROC');
     Self.ParametroPesquisa.add('join tabcor on tabcor.codigo=proc.cor');
     Result:=Self.ParametroPesquisa;
end;
function TObjCOR.Get_PesquisaCorVidro: TStringList;
Begin
     Result:=Self.Get_PesquisaCorVidro(-1);
End;

function TObjCOR.Get_PesquisaCorVidro(PCodigo: Integer): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select distinct(tabcor.codigo), TabCor.Referencia,TabCor.Descricao, TabCor.PorcentagemAcrescimo,');
     Self.ParametroPesquisa.add('TabCor.CodigoCorDelphi');
     Self.ParametroPesquisa.add('from tabVidrocor');
     Self.ParametroPesquisa.add('join tabcor on tabVidrocor.cor=tabcor.codigo');
     if (Pcodigo>=0)
     Then Self.ParametroPesquisa.add('where Tabvidrocor.vidro='+inttostr(pcodigo));

     Result:=Self.ParametroPesquisa;

end;


function TObjCOR.Get_PesquisaCorVidro(pprojeto:string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select distinct(tabcor.codigo), TabCor.Referencia,TabCor.Descricao, TabCor.PorcentagemAcrescimo,');
     Self.ParametroPesquisa.add('TabCor.CodigoCorDelphi');
     Self.ParametroPesquisa.add('from tabVidrocor');
     Self.ParametroPesquisa.add('join tabcor on tabVidrocor.cor=tabcor.codigo');
     Self.ParametroPesquisa.add('join tabvidro on tabvidro.codigo=tabvidrocor.vidro');
     Self.ParametroPesquisa.add('join tabprojeto on tabprojeto.grupovidro=tabvidro.grupovidro');
     Self.ParametroPesquisa.add('Where tabprojeto.codigo='+pprojeto);
     Self.ParametroPesquisa.Add('and tabvidro.ativo=''S'' ');
     Result:=Self.ParametroPesquisa;

end;





function TObjCOR.LocalizaReferenciaCorFerragem(Parametro: string): boolean;
begin
       if (Parametro = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabCOR est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       Parametro := StrReplaceRef(parametro); {rodolfo}

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select distinct(tabcor.codigo), TabCor.Referencia,TabCor.Descricao, TabCor.PorcentagemAcrescimo,');
           SQL.ADD('TabCor.CodigoCorDelphi');
           SQL.ADD('from tabferragemcor');
           SQL.ADD('join tabcor on tabferragemcor.cor=tabcor.codigo');
           SQL.Add('Where TabCor.Referencia = '+#39+Parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjCOR.LocalizaReferenciaCorDiverso(Parametro: string): boolean;
begin
       if (Parametro = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabCOR est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       Parametro := StrReplaceRef(parametro); {rodolfo}

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select distinct(tabcor.codigo), TabCor.Referencia,TabCor.Descricao, TabCor.PorcentagemAcrescimo,');
           SQL.ADD('TabCor.CodigoCorDelphi');
           SQL.ADD('from tabDiversocor');
           SQL.ADD('join tabcor on tabDiversocor.cor=tabcor.codigo');
           SQL.Add('Where TabCor.Referencia = '+#39+Parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjCOR.LocalizaReferenciaCorKitBox(Parametro: string): boolean;
begin
       if (Parametro = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabCOR est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       Parametro := StrReplaceRef(parametro); {rodolfo}

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select distinct(tabcor.codigo), TabCor.Referencia,TabCor.Descricao, TabCor.PorcentagemAcrescimo,');
           SQL.ADD('TabCor.CodigoCorDelphi');
           SQL.ADD('from tabKitBoxcor');
           SQL.ADD('join tabcor on tabKitBoxcor.cor=tabcor.codigo');
           SQL.Add('Where TabCor.Referencia = '+#39+Parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjCOR.LocalizaReferenciaCorPerfilado(Parametro: string): boolean;
begin
       if (Parametro = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabCOR est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       parametro:= StrReplaceRef(parametro); {rodolfo}

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select distinct(tabcor.codigo), TabCor.Referencia,TabCor.Descricao, TabCor.PorcentagemAcrescimo,');
           SQL.ADD('TabCor.CodigoCorDelphi');
           SQL.ADD('from tabPErfiladocor');
           SQL.ADD('join tabcor on tabPErfiladocor.cor=tabcor.codigo');
           SQL.Add('Where TabCor.Referencia = '+#39+Parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjCOR.LocalizaReferenciaCorVidro(Parametro: string): boolean;
begin
       if (Parametro = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabCOR est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       Parametro := StrReplaceRef(parametro); {rodolfo}

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select distinct(tabcor.codigo), TabCor.Referencia,TabCor.Descricao, TabCor.PorcentagemAcrescimo,');
           SQL.ADD('TabCor.CodigoCorDelphi');
           SQL.ADD('from tabVidrocor');
           SQL.ADD('join tabcor on tabVidrocor.cor=tabcor.codigo');
           SQL.Add('Where TabCor.Referencia = '+#39+Parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;


function TObjCOR.Get_PesquisaCorPersianaGrupoDiametro: TStringList;
Begin
     result:=Self.Get_PesquisaCorPersianaGrupoDiametro(-1);
End;
function TObjCOR.Get_PesquisaCorPersianaGrupoDiametro(pcodigo: integer): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select distinct(tabcor.codigo), TabCor.Referencia,TabCor.Descricao, TabCor.PorcentagemAcrescimo,');
     Self.ParametroPesquisa.add('TabCor.CodigoCorDelphi');
     Self.ParametroPesquisa.add('from TabPersianaGrupoDiametroCor');
     Self.ParametroPesquisa.add('join tabcor on TabPersianaGrupoDiametroCor.cor=tabcor.codigo');

     if (Pcodigo>=0)
     Then Self.ParametroPesquisa.add('where TabPersianaGrupoDiametroCor.persiana='+inttostr(pcodigo));

     Result:=Self.ParametroPesquisa;

end;

function TObjCOR.LocalizaReferenciaCorPersiana(Parametro: string): boolean;
begin
       if (Parametro = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabCOR est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select distinct(tabcor.codigo), TabCor.Referencia,TabCor.Descricao, TabCor.PorcentagemAcrescimo,');
           SQL.ADD('TabCor.CodigoCorDelphi');
           SQL.ADD('from tabPersianaGrupoDiametroCor');
           SQL.ADD('join tabcor on tabPersianaGrupoDiametroCor.cor=tabcor.codigo');
           SQL.Add('Where TabCor.Referencia = '+#39+Parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;


procedure TObjCor.EdtCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel;PcodigoMaterial:string;PMATERIAL:STring);
var
   FpesquisaLocal:Tfpesquisa;
   Ptitulo:String;
   pcodigo:integer;
begin

     If (key <>vk_f9)
     Then exit;

     Try
        Pcodigo:=strtoint(pcodigomaterial);
     Except
        MensagemErro('Escolha um produto v�lido');
        exit;
     End;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            if (uppercase(pmaterial)='DIVERSOS')
            Then Self.Get_PesquisaCorDiverso(Pcodigo);

            if (uppercase(pmaterial)='FERRAGEM')
            Then Self.Get_PesquisaCorFerragem(Pcodigo);

            if (uppercase(pmaterial)='KITBOX')
            Then Self.Get_PesquisaCorKItBox(Pcodigo);

            if (uppercase(pmaterial)='PERFILADO')
            Then Self.Get_PesquisaCorPErfilado(Pcodigo);

            if (uppercase(pmaterial)='PERSIANA')
            Then Self.Get_PesquisaCorPersianaGrupoDiametro(Pcodigo);

            if (uppercase(pmaterial)='VIDRO')
            Then Self.Get_PesquisaCorVidro(Pcodigo);



            If (FpesquisaLocal.PreparaPesquisa(Self.ParametroPesquisa,'Pesquisa de Cores de '+uppercase(Pmaterial),Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


end.



