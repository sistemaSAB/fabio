object FAjuda: TFAjuda
  Left = 485
  Top = 244
  Width = 948
  Height = 644
  BorderIcons = [biSystemMenu]
  Caption = 'Ajuda'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 932
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clWhite
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    object lb1: TLabel
      Left = 446
      Top = 5
      Width = 89
      Height = 37
      Caption = 'Ajuda'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -32
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 567
    Width = 932
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    Color = clWhite
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 1
    DesignSize = (
      932
      39)
    object lb3: TLabel
      Left = 523
      Top = 11
      Width = 95
      Height = 18
      Caption = 'Procurar por:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object bt2: TSpeedButton
      Left = 894
      Top = 0
      Width = 38
      Height = 38
      Cursor = crHandPoint
      Anchors = [akLeft, akBottom]
      BiDiMode = bdLeftToRight
      Flat = True
      Glyph.Data = {
        660F0000424D660F000000000000360000002800000024000000240000000100
        180000000000300F0000130B0000130B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFCAB7BADCCBD6D0C9D0C1C5C0B3C2B4B1C3B2B4C6B5C2
        CAC3D8D2D7F3E2EFF0DDE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDACAD5D0CCD19BB89F59A0612B96360F
        932201971B019D1A01A51E04AC291EB43C48BE5F8FD29FDBE6DCFCF1F9FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4CED383AC8729
        872E007D06008305028D0E009617009D1B00A31F00AB2300B42501BB2500C121
        01C4200AC53360CA77D8E9DCF3E2EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9
        C7D4B0BFB1348937017400027E06008511008A1400901600961A019B1E00A220
        01AA2500B02700B72902BF2E04C73100CF3000D4260ACE348BD39EEFDCEBFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFD6C6D18DB29206770901770000810E008310018612008A14
        009016009519009A1D01A02000A82300AE2500B42902BC2C02C32D05C93309D1
        3604DA3503DA2553D070E6D8E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6C5D27DAE82027801007B05008010
        00810E008310028712018A1602911700931901991A009F1D00A62100AB2300B2
        2702B92B01BE2D05C32E06CB3305D23609D93700DD2742CF62E5D4E1FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2C1CA91B696
        02800200820500820D00820D00830E008510008812008D14008E170093170098
        19019E1E00A22100A92401AF2500B52702BA2A03C12E03C63005CC3406D03509
        D63901DA2554CE70E3D0DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFC2AEADB1BCB409880F01860700871100870D00820B00840F0087110082
        05028304008D14009418009718009B1B03A11F00A62101AB2300B12600B62800
        BC2B01C12E02C52F06CB3306CE3306D33600D32284CD95D8C1CFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFC2CA389943008A01018D10008A0F0089
        0F00870D00860E00860A3A9C424AA04C017C0600880A00961A009A1A009F1D00
        A31F00A72201AD2501B22500B72700BC2B00C02D03C42E04C93106CB3301D02E
        0DC732C3CEC6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCAB4B991B297018E
        03009110008F10028E11008C0F018A10008709088A0EC9D5C9F1E5F1A6C6A736
        953A018104009110029D1C00A01E00A42000A92102AC2401B22500B72600BA2A
        00BD2C01C12E04C43103C53203C62259C46ED2C1CAFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFCFC0C845A55E00961101940E01930F00920E008F10008E100089090B
        8C13C6D2C6DBD5DAE5DCE6E3DFE48CBD8F219026008902019B1800A31F01A61F
        00A92400AC2400B12601B42900B72900BA2A00BD2C01BF2C00C12B06BA2CBFC7
        C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABB9AD1CA03C17A23300980D00961101
        9513019211008F10008A0A0B8F13C5D4C6DED8DDD4D4D4D7D4D6ECDEEAD9DBDB
        78B57B138E18008D08039F1C00A72200AA2000AC2200AE2400B12600B42600B6
        2800B72901B82A00B61877BE85C8B0BCFFFFFFFFFFFFFFFFFFC9B1B37BB18614
        A43427A6430C9F2500990E00981100961100941200900C0A9113C8D7C9E2DCE1
        D8D8D8D6D8D9D6D6D6DBD7DCF0E1EFCDD8D061AD65038E0F00940B00A41D00A9
        2102AA2200AC2400AC2401AF2501B22703B12700B11C3AB14FCDBEC6FFFFFFFF
        FFFFFFFFFFCCBAC151AD6617A93925A74223A742099E1F00990E009912009712
        00940B0C9515CCDBCDE7E1E6DDDEDCDCDDDBDBDBDBDAD9DBD5D7D7E0D9E0F0E4
        F0BDD3C048A64F038D0900980F00A42000A82000A82000A92100AA2200AC2400
        AB1F0FA62BB9BCBAFFFFFFFFFFFFFFFFFFC2B9BC39AD541EAD4024AB4424AB44
        21A842099F1D009C0F009A1300960D0B9714CFDED0ECE6EBE2E3E1E1E1E1E0E0
        E0DFDFDFDCDCDCDDDDDDDADADAE9DEE6EDE5ECA9CBAC329F39008B0200991400
        A42000A31F00A42000A42000A420019E18A5B9A6FFFFFFFFFFFFFFFFFFB5B6B4
        2FB04D25B24526AF4724AD4524AB4420A7400CA121009B10009A0D0B9A15D3E2
        D4F0EAEFE6E6E6E5E5E5E4E4E4E3E3E3E2E2E2E0E0E0E0E0E0DEDEDEDCDCDCEE
        E3EDE8E4E994C3951D9428009313009F1D009E1C009F1D009E1C01991090B391
        FFFFFFFFFFFFFFFFFFB3B7B12CB34C27B74729B34828B24725AE4625AC4525AC
        4612A52B009D0D0A9D13D5E7D6F5EFF4EBEBEBE9E9E9E8E7E9E7E7E7E5E5E5E5
        E5E5E4E4E4E3E4E2E4E2E1E1E1E1DFDFDFFDEAFBDAE4DE0F901D01971501991A
        01991A01991A02920A8AB08CFFFFFFFFFFFFFFFFFFB2B6B02FB65029BB4B2BB7
        4C2BB54A29B34827B14626AD4624AB4715A63311A120D5E8D7FAF5F7F0F1EDEF
        EFEFECEFEDECECECEAEAEAE8EAEAE9E9E9E8E8E8E6E7E5E6E6E6F7ECF6EFE8EF
        8AC190089012009414009617019519009418008D0889AF8BFFFFFFFFFFFFFFFF
        FFB9B7B635B9542BBE4C2EBB4E2DBA4D2CB64B2BB44C28B14926AF4723AC4426
        AB44DCEFE0FBF9F9F1F3F3F3F3F3F2F2F2F1F1F1F4F0EFEFEFEFEDEDEDECECEC
        FAF1FBFBF2FCAED1AF369C36008701029313019315009214009214009016018A
        0A90AE91FFFFFFFFFFFFFFFFFFC3BABD44B7602BC24E2FBE5130BD502DBA4D2C
        B84D29B54A28B44925AF4329AD48DFF2E3FFFFFFFBF9F9F7F7F7F5F5F5F4F4F4
        F3F3F3F5F3F3FDF7FCFEFBFDC7DFC74CA94E008701008D050093130191130190
        12008F14008E13008E1301880EA5B2A2FFFFFFFFFFFFFFFFFFCBBBC25DB76E2B
        C54E31C35331C1512EBD502FBC4F2EBB4E2EB84C27B3482DB14CDFF2E3FFFFFF
        FCFBFDFCFCFCFBFBFBF9F9F9FEFBFDFFFFFFDFEEE063B562038A060088000192
        0D009311009110008F10008F11018D10008C0F008A0A148B1EB7B8B6FFFFFFFF
        FFFFFFFFFFD0B5B884B89029C94F36C55833C55533C25533C0532FBE512FBC4F
        29B84B2FB650E0F3E4FFFFFFFCFCFCFCFCFCFEFEFEFDFFFFF2F9F48ACD9A1C9C
        370392170A982109972006921C039217039116018F14018E15028D14048B1700
        850B42974DC7BAC2FFFFFFFFFFFFFFFFFFFFFFFFB4BBB43AC65B36CC5A36C858
        36C65635C55534C45432BF522DBC4F33BD52DFF5E3FEFEFEFDFDFDFFFFFFFBFD
        FDA3D9AE36AD520B9D2D1DA43E22A54421A2411FA03F1D9B3B1B993918973416
        9434159232128E30138F3100862083AE8DC1AEB1FFFFFFFFFFFFFFFFFFFFFFFF
        D5C5D068C17C32D2583ACC5C38CA5A38C75A36C65635C4572FC15136C055E6F7
        EAFFFFFFFEFEFEBCE5C64CBD6617A73620AD402AAF4825AC4524A94224A64123
        A54020A13E1E9F3C1E9D3A1C9A371A973718953514922F1D9138BBBCBAFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFD2BDBFAEBFB13FD16139D05C3CCE5E3CCB5E38C9
        5C38C75A34C65631C452B8E9C3D2EFD869CA8023B24521B3432FB94E2CB34C2A
        B14A2AB14A27AF4526AA4522A94326A54222A34020A23D209F3C1B9C391C9A37
        0791256CAA7CCDBAC3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCCDD57BC8
        8E36D65A3ED15F3DD05D3CCF5D3DCC5F3BCB5B35C75734C45431C15125BB4933
        C05334BE5332BC512FB94E2EB54E2BB24B2AB14B29B04A27AE4726AA4526A744
        24A54220A44022A13E189C372B9C44C0C1BFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFD4D4D45AD17635D75A41D4623FD2603DCF5F3CCE5E3C
        CC5C37C95938C55637C65937C45735C25534C15434BE5331BB5030BA4F2EB74F
        2DB44D2AB14A2AAE4928AC4729AB4627A84721A540109D3098B89FCBBBC2FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7E9EFD1E1D64AD66B37
        D75D44D46341D46241D1613DCF5F3ECE5E3CCB5E3DCA5D3AC75A36C55835C457
        35C15636C05533BD5232BC5130B9512FB64F2DB44E2DB24B29B04A2AB04614A6
        3678B686D1C0C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF9FEC7E1CF48D66B37DA5942D66442D56741D4623FD1613DCF5F
        3ECE5E3CCB5E3BCA5D39C85B37C65937C35836C35636C05535BF5433BA5331B8
        5230B7502CB54D17AE3A6DB880D1C2CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEF8FDCEE0D359D6763BDD5F
        41D96244D76542D36641D3633FD1613FCF5F3ECE5D3DCC5F3DCA5D3BC85B3AC7
        5A37C45736C25737C15535BE562BBA4D24B64683C193D0C2CDFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFF6E7EBDBD9D97FCF9447DD6C3DDF6141D96243D56542D56541D36340D2
        6240D0603ECD603DCC5F3ECB5E3DC75C3CC65B32C5532BBF4D49BE67ACC5B1CF
        BFCAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6C3BEDDCED6B5C6B878D08E4DD8
        7040DD653FDB5F3DD76040D7633ED4623FD3613AD05E36CD5932CC5537C9594F
        C76E91C69ECBC4C7C6B3B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFD3C1C0D7C7D2BDC4BF99C7A375CC8A5ED27953D26F4DD16C4BCE6D58
        CF7467CD7F84CA95ACC8B1CAC5C7CEB9C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCDBABDD1C1C8C9
        C0C3C1BFBEBCBDB9BDBEBCC6C1C3D0C1C9CDB7BCFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
      Layout = blGlyphBottom
      ParentBiDiMode = False
    end
    object edt1: TMaskEdit
      Left = 623
      Top = 10
      Width = 244
      Height = 20
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
    end
  end
  object pnlItensAjuda: TPanel
    Left = 0
    Top = 49
    Width = 932
    Height = 518
    Align = alClient
    Color = clWhite
    TabOrder = 2
    object tv1: TTreeView
      Left = 1
      Top = 1
      Width = 243
      Height = 516
      Cursor = crHandPoint
      Align = alLeft
      BevelInner = bvNone
      BevelOuter = bvNone
      Color = clWhite
      Ctl3D = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Indent = 19
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      OnClick = tv1Click
      OnGetSelectedIndex = tv1GetSelectedIndex
    end
    object mmoAjuda: TRichEdit
      Left = 244
      Top = 1
      Width = 687
      Height = 516
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = clWhite
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 1
    end
  end
  object pnl5: TPanel
    Left = 0
    Top = 24
    Width = 246
    Height = 25
    Align = alCustom
    BevelOuter = bvNone
    Color = 10643006
    TabOrder = 3
    object lb2: TLabel
      Left = 37
      Top = 0
      Width = 153
      Height = 23
      Align = alCustom
      Caption = 'T'#243'picos de Ajuda'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
end
