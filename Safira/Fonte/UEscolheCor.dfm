object FEscolheCor: TFEscolheCor
  Left = 209
  Top = 134
  Width = 425
  Height = 480
  BorderIcons = [biSystemMenu]
  Caption = 'Escolha a Cor'
  Color = clBtnFace
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object bitbtn1: TSpeedButton
    Left = 296
    Top = 14
    Width = 101
    Height = 32
    Caption = 'Salvar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    Glyph.Data = {
      46060000424D4606000000000000360400002800000016000000160000000100
      08000000000010020000120B0000120B0000000100004F000000099B4A00F0F0
      F0008CD0AA0050AC7900D7D7D7006AD0970023B16300CCCCCC003DD27F00FAFE
      FB000BB45600A6D5BB0099B5A600E6E6E60032AA6800BADCC9000EA853001AC4
      660087E9B3005FB084007AD1A1004AD58700C5E9D500B9E0CA00F7F7F700D5EF
      E0000AA550009EC9B10069DE9C00DEDEDE0022D873003AB872000BBB5A00E1FA
      EC004FC28300ADF3CB0029C86F00BAF5D4005DD7930019AD5B003FCF7F0064BC
      8B0022BF6800EFFCF500DDEEE4000AAD530080DDA90099E4BA008BBFA20010BC
      5D00EBF7F000CDEBDB009FD7B80039C0750041D3820072DBA10029B266000AA1
      4D007ED8A60034B56E0070D19C004AB47A000BB6570024C76C00D3F8E400FFFF
      FF008DE7B5001EB561000DAC5400F5FDF9003DAC6F0086CFA7004CD58900C5ED
      D700E7F6ED002BC6700044D1830040BA76000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000414141414141
      414141410909414141414141414141416E6741411801010D1D04040707070704
      041D0D0D011809416E6741411801010D0D1D04070C0704041D1D0D0101184141
      6E6741414141414141091B032930414141414141414141416E67414141414141
      2C130512123D0F4141414141414141416E6741414141410F3D1C1C1C1C1C0E2C
      41414141414141416E67414141410B1F48483608084C4C464541414141414141
      6E674141411706241131313131313F2A29414141414141416E6741413206313E
      4427272D2020203144344141414141416E674141472D27022C41093329440A0A
      0A271941414141416E6741414D1F4A41414141414116382D2D2D3B0941414141
      6E6741410532414141414141414132221A1A1A14414141416E67414141414141
      41414141414141093C1A1A10494141416E674141414141414141414141414141
      413A1A00350941416E67414141414141414141414141414141412E39392F4141
      6E6741414141414141414141414141414141413700432B416E67414141414141
      414141414141414141414141260042416E674141414141414141414141414141
      4141414145284B416E6741414141414141414141414141414141414141211E40
      6E67414141414141414141414141414141414141414123256E67414141414141
      414141414141414141414141414141416E674141414141414141414141414141
      41414141414141416E67}
    ParentFont = False
    OnClick = BitBtn1Click
  end
  object BitBtn2: TSpeedButton
    Left = 296
    Top = 49
    Width = 101
    Height = 32
    Caption = 'Cancelar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    Glyph.Data = {
      360C0000424D360C000000000000360000002800000020000000200000000100
      180000000000000C0000130B0000130B00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFEFEFFFEFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
      FFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFDFEFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDADBEF8C91DD9DA0D7E6E6EEFFFFFEFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFECEDFB8C94EA5366F8495ADE9B9FD4F5F4F7FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBED
      ECF3FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFCED0F66978F34C63ED3047E45A63D3D4D5ECFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFAFDB4B4E6A1
      A0DAFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFEFFFFFFFFFFFFFFCBCEF66573F24258E6263EDD4755D7C5C7E9FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDADBF56B6CDBA3
      A3E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFDADBF96C7AF13C54E7223BD83C4CD4B0B3E0FBFB
      FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F6FC999CEB5C62E0D3
      D6F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
      FFFFFFFFFFFFFFFFFFFEFFFFF4F2FC929BF43B52EB2039D7253AD2797FD0E5E5
      F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFCFDC2C7F34F5DE88389E9F3
      F3FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFEFEFFD0D4F95466EE213BDE1830D23E4CCCB0B1
      DCF8F7FAFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEDFE1F66F7CEE4154EBBFC3F1FB
      FCFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFF
      FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFF8F8FDA3AAF2364CE41934D91D33D26169
      CCD5D6E9FEFEFEFFFFFEFEFEFFFFFFFFEDEEF9939CEC374AEB7B87EBEAEBF9FF
      FFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEEBEBFA8690ED2A42E11531D82338
      CE8086CDEAEAF3FDFFFFFFFFFFF5F5FAA9AFE94054E94D5FEBC5CBF3FCFCFDFF
      FFFFFFFFFFFFFFFFFFFEFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFEFFFFFEFFFFFFFFFFFFFFFFFFFDFEFFDEE2F97281EB1D38DE132E
      D93443CD9EA1D0F5F7FBFAFAFCB5B8E54658E62E46ED949DEBF2F3FBFFFEFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFFDDDFFB717EEB1E37
      DC1C37DF4050D0A9ACD2ABAEE13F51E51D39F06A78E9E2E2F8FEFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFDFDFFD8DCFA7280
      E8283FDC1D33D43141C12F3FCF152FE44558E3C3C7F1FEFDFEFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFEFFFEFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFEFFD9DA
      F5626EDB1228CE0A22CC0821CE2539D0A2A8E6F7F7FDFFFFFFFFFFFFFFFFFFFE
      FFFFFFFFFFFEFFFEFDFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFEFEFFFFFFFFFFFFFFFFFFFFFFFBFBFCE3E5F3A9AD
      E54555D50821CD011AC6031BCD2A39C1A8AAD3F3F3F6FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFEFEFEFEFEFEFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
      FFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEF4F4F8CACDEB858DE23C4F
      E30F2BE1021FDA182FD52E42D51429CC434FC8A6A9D5EEEFF4FFFFFFFEFEFEFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFE2E3EFB2B4E57E88E85567EF3149
      EC1937F11D37EE7B86EAB5BAEE5C6DE71F37E93F4FDC999DD6EAE9F0FEFEFEFF
      FFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
      FFFFFFFFFFFFFFFFFFFFFFFFF4F5F7CED0E89CA0E58894F37C8CF76679F14C63
      F4455BF78894F5E5E7FBFAFAFED7DBF98390F7495EF8505FE28D92D2DEE0EAFD
      FDFDFFFFFFFFFFFFFDFDFFFDFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFEFFFFFEFFFFECEBF8ADB2E37F8BE78F9DFB8F9DF58393F2798AFA7686
      FAACB4F9ECEEFDFEFEFFFFFFFFFCFDFFEDEEFDB8C0F97C8BF96B78E98288D0C9
      CADAFFFFFEFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFE4E5FA9EA8F87284FC7383EC8693F3919DFEA0AAFBD2D6
      FAF8F9FDFFFFFFFFFFFFFEFEFEFFFFFFFFFFFEFCFCFEE5E5FBBAC0FC939DF875
      7DDA9F9FD2E3E2ECFAFAFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
      FFFFFFFFFFFEFFFFFFFDFDFFE7E9FCA9B3F88E9CF59DA9F7C3C8FBE9EBFDFDFD
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6FEE0E1FDB2
      B8FB7A81E08789D6CACAEAFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF3F5FEE3E6FEE9EAFEF8F8FEFEFEFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8
      F8FEDBDEFDB8BAF0D1D1F2FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFF
      FFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF
      FFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFEFEFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FEFEFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD
      FFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
      FEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    ParentFont = False
    OnClick = BitBtn2Click
  end
  object CheckListBoxCor: TCheckListBox
    Left = 6
    Top = 43
    Width = 284
    Height = 380
    Flat = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    ItemHeight = 15
    ParentFont = False
    TabOrder = 0
    OnClick = CheckListBoxCorClick
    OnKeyPress = CheckListBoxCorKeyPress
  end
  object CheckListBoxCodigoCor: TCheckListBox
    Left = 305
    Top = 97
    Width = 85
    Height = 262
    Flat = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    ItemHeight = 15
    ParentFont = False
    TabOrder = 1
    Visible = False
  end
  object CheckBoxMarcarDesmarcar: TCheckBox
    Left = 6
    Top = 1
    Width = 235
    Height = 17
    Caption = 'Marcar / Desmarcar Todas'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = CheckBoxMarcarDesmarcarClick
  end
  object EdtPesquisa: TEdit
    Left = 6
    Top = 21
    Width = 284
    Height = 19
    CharCase = ecUpperCase
    TabOrder = 3
    OnExit = EdtPesquisaExit
  end
end
