unit UobjVIDROCOR;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJVIDRO,UOBJCOR;

Type
   TObjVIDROCOR=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Vidro:TOBJVIDRO;
                Cor:TOBJCOR;


                Constructor Create;
                Destructor  Free;

                function RetornaEstoque: String;


                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaReferencia(Parametro:string) :boolean;
                Function    Localiza_Vidro_Cor(PVidro,Pcor:string):Boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;

                Procedure Submit_PorcentagemAcrescimo(parametro: string);
                Function Get_PorcentagemAcrescimo: string;


                Procedure Submit_AcrescimoExtra(parametro: string);
                Function Get_AcrescimoExtra: string;

                Procedure Submit_ValorExtra(parametro: string);
                Function Get_ValorExtra: string;

                Procedure Submit_ValorFinal(parametro: string);
                Function Get_ValorFinal: string;

                Procedure Submit_AcrescimoIcms(Parametro:string);
                Procedure Submit_PorcentagemAcrescimoIcms(Parametro:string);

                Function Get_AcrescimoIcms:string;
                Function Get_PorcentagemAcrescimoIcms:string;

                Function Get_PorcentagemAcrescimoFinal: string;

                Procedure Submit_ClassificacaoFiscal(parametro:String);
                Function Get_ClassificacaoFiscal:String;

                procedure EdtVidroExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtVidroKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtCorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtCor_PorcentagemCor_Exit(Sender: TObject;Var PEdtCodigo:TEdit; LABELNOME:TLABEL; EdtPorcentagemCor,EdtValorCusto:TEdit);

                procedure EdtCorKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Procedure ResgataCorVidro(PVidro:string);
                Function CadastraVidroEmTodasAsCores(PVidro:string):Boolean;

                procedure EdtCorDeVidroKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtVidroDisponivelNaCorKeyDown(Sender: TObject;Var PEdtCodigo:TEdit; PCor :string; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Function  Get_VidroNaCor(PCor : string) :TStringList;
                procedure Relatorioestoque;
                procedure EdtvidroCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);

                procedure Submit_Estoque(parametro:string);
                function Get_Estoque:String;

                procedure Submit_EstoqueMinimo(parametro:string);
                function Get_EstoqueMinimo:string;
                

                procedure DiminuiEstoque(quantidade,vidrocor:string);
                function AumentaEstoque(quantidade,vidrocor:string):Boolean;

          Private
               Objquery:Tibquery;
               ObjqueryEstoque:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               PorcentagemAcrescimo:string;

               AcrescimoExtra:string;
               PorcentagemAcrescimoFinal:string;
               ValorExtra :string;
               ValorFinal :string;
               AcrescimoIcms:string;
               PorcentagemAcrescimoIcms:string;
               ClassificacaoFiscal:String;
               Estoque:string;
               EstoqueMinimo:string;

               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;



   End;


implementation
uses Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UEscolheCor, UReltxtRDPRINT,rdprint, UVIDRO;

Function  TObjVIDROCOR.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.ClassificacaoFiscal:=fieldbyname('ClassificacaoFiscal').asstring;


        If(FieldByName('Vidro').asstring<>'')
        Then Begin
                 If (Self.Vidro.LocalizaCodigo(FieldByName('Vidro').asstring)=False)
                 Then Begin
                          Messagedlg('Vidro N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Vidro.TabelaparaObjeto;
        End;
        If(FieldByName('Cor').asstring<>'')
        Then Begin
                 If (Self.Cor.LocalizaCodigo(FieldByName('Cor').asstring)=False)
                 Then Begin
                          Messagedlg('Cor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Cor.TabelaparaObjeto;
        End;
        Self.PorcentagemAcrescimo:=fieldbyname('PorcentagemAcrescimo').asstring;

        Self.AcrescimoExtra:=fieldbyname('AcrescimoExtra').asstring;
        Self.PorcentagemAcrescimoFinal:=fieldbyname('PorcentagemAcrescimoFinal').asstring;
        Self.ValorExtra:=fieldbyname('ValorExtra').AsString;
        Self.ValorFinal:=fieldbyname('ValorFinal').AsString;
        Self.AcrescimoIcms:=Fieldbyname('acrescimoicms').asstring;
        Self.PorcentagemAcrescimoIcms:=Fieldbyname('porcentagemacrescimoicms').asstring;
        self.Estoque:=fieldbyname('Estoque').AsString;
        self.EstoqueMinimo := Objquery.fieldbyname('EstoqueMinimo').AsString;


        result:=True;
     End;
end;


Procedure TObjVIDROCOR.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('ClassificacaoFiscal').asstring:=Self.ClassificacaoFiscal;
        ParamByName('Vidro').asstring:=Self.Vidro.GET_CODIGO;
        ParamByName('Cor').asstring:=Self.Cor.GET_CODIGO;
        ParamByName('PorcentagemAcrescimo').asstring:=virgulaparaponto(Self.PorcentagemAcrescimo);

        ParamByName('AcrescimoExtra').asstring:=virgulaparaponto(Self.AcrescimoExtra);
        ParamByName('ValorExtra').AsString:=virgulaparaponto(Self.ValorExtra);
        ParamByName('ValorFinal').AsString:=virgulaparaponto(Self.ValorFinal);
        Parambyname('acrescimoicms').asstring:=virgulaparaponto(Self.AcrescimoIcms);
        Parambyname('porcentagemacrescimoicms').asstring:=virgulaparaponto(Self.PorcentagemAcrescimoIcms);
        //ParamByName('Estoque').AsString:=virgulaparaponto(Self.Estoque);
        ParamByName('estoqueminimo').AsString:=virgulaparaponto(Self.EstoqueMinimo);
  End;
End;

//***********************************************************************

function TObjVIDROCOR.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjVIDROCOR.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Vidro.ZerarTabela;
        Cor.ZerarTabela;
        PorcentagemAcrescimo:='';

        AcrescimoExtra:='';
        PorcentagemAcrescimoFinal:='';
        ValorExtra:='';
        ValorFinal:='';
        AcrescimoIcms:='';
        PorcentagemAcrescimoIcms:='';
        ClassificacaoFiscal:='';
        Estoque:='';
        EstoqueMinimo := '';
     End;
end;

Function TObjVIDROCOR.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';

      if (AcrescimoIcms='')
      Then AcrescimoIcms:='0';

      if (PorcentagemAcrescimoIcms='')
      Then PorcentagemAcrescimoIcms:='0';

      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjVIDROCOR.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     If (Self.Vidro.LocalizaCodigo(Self.Vidro.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Vidro n�o Encontrado!';


     If (Self.Cor.LocalizaCodigo(Self.Cor.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Cor n�o Encontrado!'
     Else Begin
               Self.Cor.TabelaparaObjeto;
               Self.PorcentagemAcrescimo:=Self.cor.Get_PorcentagemAcrescimo;
     End;

     If (mensagem<>'')
     Then Begin
              Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
              exit;
     End;
     result:=true;
End;

function TObjVIDROCOR.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.Vidro.Get_Codigo<>'')
        Then Strtoint(Self.Vidro.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Vidro';
     End;
     try
        If (Self.Cor.Get_Codigo<>'')
        Then Strtoint(Self.Cor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Cor';
     End;

     Try
        if (Self.AcrescimoIcms='')
        Then strtofloat(Self.AcrescimoIcms);
     Except
        mensagem:=Mensagem+'/Acr�scimo de Icms';
     End;

     Try
        if (Self.PorcentagemAcrescimoIcms='')
        Then strtofloat(Self.PorcentagemAcrescimoIcms);
     Except
        mensagem:=Mensagem+'/Porcentagem de Acr�scimo de Icms';
     End;




     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjVIDROCOR.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjVIDROCOR.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjVIDROCOR.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro VIDROCOR vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Vidro,Cor,PorcentagemAcrescimo,AcrescimoExtra');
           SQL.ADD(' ,PorcentagemAcrescimoFinal, AcrescimoIcms,PorcentagemAcrescimoIcms,ValorExtra, ValorFinal,ClassificacaoFiscal');
           SQL.ADD(' ,Estoque,EstoqueMinimo from  TabVidroCor');
           SQL.ADD(' WHERE Codigo='+parametro);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;

       End;
end;

procedure TObjVIDROCOR.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjVIDROCOR.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjVIDROCOR.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryEstoque:=TIBQuery.create(nil);
        Self.ObjqueryEstoque.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryPesquisa:=Tibquery.Create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjqueryPesquisa;

        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Vidro:=TOBJVIDRO.create;
        Self.Cor:=TOBJCOR.create;

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabVidroCor(Codigo,Vidro,Cor,PorcentagemAcrescimo,AcrescimoIcms,PorcentagemAcrescimoIcms');
                InsertSQL.add(' ,AcrescimoExtra,  ValorExtra,ValorFinal,ClassificacaoFiscal');//,Estoque');
                InsertSql.Add(',EstoqueMinimo');
                InsertSql.Add(' )');
                InsertSQL.add('values (:Codigo,:Vidro,:Cor,:PorcentagemAcrescimo,:AcrescimoIcms,:PorcentagemAcrescimoIcms');
                InsertSql.Add(',:AcrescimoExtra,:ValorExtra,:ValorFinal,:ClassificacaoFiscal');//,:Estoque');
                InsertSql.Add(',:EstoqueMinimo');
                InsertSql.Add(')');

                ModifySQL.clear;
                ModifySQL.add('Update TabVidroCor set Codigo=:Codigo,Vidro=:Vidro,Cor=:Cor');
                ModifySQL.add(',PorcentagemAcrescimo=:PorcentagemAcrescimo,AcrescimoIcms=:AcrescimoIcms,PorcentagemAcrescimoIcms=:PorcentagemAcrescimoICMS');
                ModifySQL.add(',AcrescimoExtra=:AcrescimoExtra,ValorExtra=:ValorExtra,ValorFinal=:ValorFinal,ClassificacaoFiscal=:ClassificacaoFiscal');
                //ModifySQL.add(',Estoque=:Estoque');
                ModifySQL.add(', EstoqueMinimo=:EstoqueMinimo');
                ModifySQl.Add(' where Codigo=:Codigo');


                DeleteSQL.clear;
                DeleteSql.add('Delete from TabVidroCor where Codigo=:Codigo ');


                Self.status          :=dsInactive;
        End;

end;
procedure TObjVIDROCOR.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjVIDROCOR.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewVIDROCOR');
     Result:=Self.ParametroPesquisa;
end;

function TObjVIDROCOR.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Vidro por Cor ';
end;


function TObjVIDROCOR.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENVIDROCOR,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjVIDROCOR.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ObjqueryEstoque);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    FreeAndNil(Self.ObjQueryPesquisa);
    FreeAndNil(Self.ObjDataSource);
    Self.Vidro.FREE;
    Self.Cor.FREE;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjVIDROCOR.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjVIDROCOR.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjVidroCor.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjVidroCor.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjVidroCor.Submit_PorcentagemAcrescimo(parametro: string);
begin
        Self.PorcentagemAcrescimo:=tira_ponto(Parametro);
end;
function TObjVidroCor.Get_PorcentagemAcrescimo: string;
begin
        Result:=Self.PorcentagemAcrescimo;
end;
procedure TObjVidroCor.Submit_AcrescimoExtra(parametro: string);
begin
        Self.AcrescimoExtra:=Tira_Ponto(Parametro);
end;
function TObjVidroCor.Get_AcrescimoExtra: string;
begin
        Result:=Self.AcrescimoExtra;
end;
function TObjVidroCor.Get_PorcentagemAcrescimoFinal: string;
begin
        Result:=Self.PorcentagemAcrescimoFinal;
end;
//CODIFICA GETSESUBMITS


procedure TObjVIDROCOR.EdtVidroExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Vidro.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Vidro.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Vidro.Get_Descricao;
End;
procedure TObjVIDROCOR.EdtVidroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FVidroLocal: TFVIDRO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FVidroLocal := TFVIDRO.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Vidro.Get_Pesquisa,Self.Vidro.Get_TituloPesquisa,FVidroLocal)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Vidro.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Vidro.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Vidro.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeandNil(FVidroLocal);
     End;
end;
procedure TObjVIDROCOR.EdtCorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Cor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Cor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Cor.Get_Descricao;
End;
procedure TObjVIDROCOR.EdtCorKeyDown(Sender: TObject;Var PEdtCodigo:TEdit; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cor.Get_Pesquisa,Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjVIDROCOR.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJVIDROCOR';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjVIDROCOR.ResgataCorVidro(PVidro: string);
begin
     With Self.ObjqueryPesquisa do
     Begin
          close;
          SQL.clear;
          SQL.ADD('Select TVC.Cor,TabCor.Descricao as NomeCor,TVC.PorcentagemAcrescimoFinal,TVC.PorcentagemAcrescimo,TVC.AcrescimoExtra,TVC.PorcentagemAcrescimoIcms');
          SQL.ADD(' ,TVC.codigo,TVC.AcrescimoIcms,TVC.ValorExtra,TVC.ClassificacaoFiscal,TVC.Estoque');
          SQL.ADD(' from  TabVidroCor TVC');
          SQL.ADD(' join tabCor on TVC.Cor=Tabcor.codigo');

          SQL.ADD(' where Vidro='+pVidro);

          if (PVidro='')
          Then exit;
          open;
     End;

end;

procedure TObjVIDROCOR.EdtCor_PorcentagemCor_Exit(Sender: TObject;Var PEdtCodigo:TEdit;
  LABELNOME: TLABEL; EdtPorcentagemCor, EdtValorCusto: TEdit);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then Begin
               EdtPorcentagemCor.Text:='0';
               exit;
     End;

     If (Self.Cor.LocalizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     Self.Cor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Cor.Get_Descricao;
     EdtPorcentagemCor.Text:=Self.Cor.Get_PorcentagemAcrescimo;
     EdtValorCusto.text:=Self.Vidro.Get_PrecoCusto;
     PEdtCodigo.Text:=Self.Cor.Get_Codigo;
end;

function TObjVIDROCOR.LocalizaReferencia(Parametro: string): boolean;
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro VIDROCOR vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Vidro,Cor,PorcentagemAcrescimo,AcrescimoExtra,acrescimoicms,porcentagemacrescimoicms');
           SQL.ADD(' ,PorcentagemAcrescimoFinal,ValorExtra, ValorFinal,ClassificacaoFiscal,Estoque,EstoqueMinimo');
           SQL.ADD(' from  TabVidroCor');
           SQL.ADD(' WHERE Referencia='+#39+parametro+#39);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;

       End;

end;

function TObjVIDROCOR.CadastraVidroEmTodasAsCores(PVidro: string): Boolean;
Var   Cont : Integer;
      PPOrcentagemAcrescimo:string;
begin
     Result:=false;
     if (Self.Vidro.LocalizaCodigo(PVidro)=false)then
     Begin
          MensagemErro('Vidro n�o encontrado.');
          Result:=false;
          exit;
     end;

     With  Self.ObjqueryPesquisa  do
     Begin
          Close;
          SQl.Clear;
          Sql.Add('Select Codigo,Referencia, Descricao from TabCor');
          Open;
          First;

          FEscolheCor.CheckListBoxCor.Clear;
          FEscolheCor.CheckListBoxCodigoCor.Clear;
          While not (Eof) do
          Begin
               FEscolheCor.CheckListBoxCor.Items.Add(fieldbyname('Referencia').AsString+' - '+fieldbyname('Descricao').AsString);
               FEscolheCor.CheckListBoxCodigoCor.Items.Add(fieldbyname('Codigo').AsString);
          Next;
          end;
          FEscolheCor.ShowModal;

          if FEscolheCor.Tag = 0 then
          Begin
               Result:=false;
               exit;
          end;

          // Cadastrando os cores
          for Cont:=0 to FEscolheCor.CheckListBoxCor.Items.Count-1 do
          Begin
               if FEscolheCor.CheckListBoxCodigoCor.Checked[Cont]=true then
               Begin
                    Self.Cor.LocalizaCodigo(FEscolheCor.CheckListBoxCodigoCor.Items.Strings[Cont]);
                    Self.Cor.TabelaparaObjeto;
                    PPOrcentagemAcrescimo:=Self.Cor.Get_PorcentagemAcrescimo;

                    Self.ZerarTabela;
                    Self.Status:=dsInsert;
                    Self.Submit_Codigo(Get_NovoCodigo);
                    Self.Vidro.Submit_Codigo(PVidro);
                    Self.Cor.Submit_Codigo(FEscolheCor.CheckListBoxCodigoCor.Items.Strings[Cont]);

                    Self.Submit_AcrescimoExtra('0');
                    Self.Submit_PorcentagemAcrescimo(PPOrcentagemAcrescimo);
                    Self.Submit_ClassificacaoFiscal('');



                    if (Self.Salvar(false)=false)then
                    Begin
                         MensagemErro('Erro ao tentar Salvar os  Vidros por Cor');
                         Result:=false;
                         exit;
                    end;
               end;
          end;

          Result:=true;
     end;
end;

function TObjVIDROCOR.Localiza_Vidro_Cor(PVidro, Pcor: string): Boolean;
begin
       if (Pvidro='')
       or (Pcor='')
       Then Begin
                 Messagedlg('Escolha um Vidro e uma Cor',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Vidro,Cor,PorcentagemAcrescimo,AcrescimoExtra,ValorExtra,AcrescimoICms,PorcentagemAcrescimoICms');
           SQL.ADD(' ,PorcentagemAcrescimoFinal,ValorFinal,ClassificacaoFiscal,Estoque,EstoqueMinimo');
           SQL.ADD(' from  TabVidroCor');
           SQL.ADD(' WHERE Vidro='+Pvidro);
           SQL.ADD(' and Cor='+Pcor);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjVIDROCOR.Get_ValorExtra: string;
begin
    Result:=Self.ValorExtra;
end;

function TObjVIDROCOR.Get_ValorFinal: string;
begin
    Result:=Self.ValorFinal;
end;

procedure TObjVIDROCOR.Submit_ValorExtra(parametro: string);
begin
    Self.ValorExtra:=tira_ponto(parametro);
end;

procedure TObjVIDROCOR.Submit_ValorFinal(parametro: string);
begin
    Self.ValorFinal:=tira_ponto(parametro);
end;

procedure TObjVIDROCOR.EdtCorDeVidroKeyDown(Sender: TObject; var PEdtCodigo: TEdit; var Key: Word; Shift: TShiftState;
  LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cor.Get_PesquisaCorVidro,Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjVIDROCOR.EdtVidroDisponivelNaCorKeyDown(Sender: TObject; var PEdtCodigo: TEdit; PCor: string; var Key: Word; Shift: TShiftState;
  LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_VidroNaCor(PCor),Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjVIDROCOR.Get_VidroNaCor(PCor: string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select Distinct(TabVidro.Referencia),TabVidro.DESCRICAO, TabVidro.Unidade,');
     Self.ParametroPesquisa.add('TabVidro.Peso, TabVidroCor.Codigo');
     Self.ParametroPesquisa.add('from TabVidroCor');
     Self.ParametroPesquisa.add('Join TabVidro on TabVidro.Codigo = TabVidroCor.Vidro');
     if (PCor <> '')then
     Self.ParametroPesquisa.add('Where TabVidroCor.Cor = '+PCor);

     Result:=Self.ParametroPesquisa;

end;


procedure TObjVIDROCOR.Relatorioestoque;
var
Pgrupo:string;
PsomaEstoque:integer;
PsomaValorTotal:Currency;
pdata:string;
vidro:string;
begin
     With FfiltroImp do
     begin
          DesativaGrupos;

          Grupo01.Enabled:=true;
          LbGrupo01.caption:='Grupo Vidro';
          edtgrupo01.OnKeyDown:=Self.VIDRO.EdtGrupoVIDROKeyDown;

          Grupo02.Enabled:=True;
          LbGrupo02.Caption:='Vidro';
          edtgrupo02.OnKeyDown:=Self.Vidro.EdtVidroKeyDown;

          Grupo03.Enabled:=True;
          edtgrupo03.EditMask:=MascaraData;
          LbGrupo03.caption:='Data Limite';



          Showmodal;

          if (tag=0)
          then exit;

          pGrupo:='';
          if (edtgrupo01.Text<>'')
          Then Begin
                    if (self.VIDRO.GrupoVIDRO.LocalizaCodigo(edtgrupo01.text)=False)
                    Then Begin
                              MensagemErro('Grupo n�o encontrado');
                              exit;
                    End;
                    self.VIDRO.GrupoVIDRO.TabelaparaObjeto;
                    pgrupo:=self.VIDRO.GrupoVIDRO.get_codigo;
          End;

          vidro:='';
          if(edtgrupo02.Text<>'')
          then vidro:=edtgrupo02.Text;

          pdata:='';

          if(edtgrupo03.Text<>'  /  /    ')
          then pdata:=edtgrupo03.Text;
          {

          if (comebarra(edtgrupo03.text)<>'')
          Then if (Validadata(2,pdata,False)=False)
               then exit;      }

     End;

     With Self.ObjQueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select');
          sql.add('TabVIDRO.codigo as VIDRO,');
          sql.add('TabVIDRO.REferencia as REFVIDRO,');
          sql.add('TabVIDRO.descricao as NOMEVIDRO,');
          sql.add('Tabcor.codigo as COR,');
          sql.add('Tabcor.Referencia as REFCOR,');
          sql.add('tabcor.descricao as NOMECOR,');
          sql.add('tabVIDRO.precocusto,');
          
          sql.add('coalesce(sum(TabEstoque.quantidade),0) as estoque,');//alterado aqui
          sql.add('(coalesce(sum(TabEstoque.quantidade),0)*tabvidro.precocusto) as VALORTOTAL');
          
          sql.add('from tabVIDROcor');
          sql.add('join tabVIDRO on tabVIDROcor.VIDRO=tabVIDRO.codigo');
          sql.add('join tabcor on TabVIDROcor.cor=tabcor.codigo');
          //sql.add('left join tabestoque on tabestoque.vidrocor=TabvidroCor.codigo');

          if (pdata<>'')
         then Begin
                    sql.add('left join tabestoque on (tabestoque.vidrocor=TabvidroCor.codigo');
                    sql.add('and tabestoque.data<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(pdata))+#39+')');
                    
                   // ParamByName('pdata').asdatetime:=strtodate(FormatDateTime('mm/dd/yyyy',strtodate(pdata)));
         End
         Else sql.add('left join tabestoque on tabestoque.vidrocor=TabvidroCor.codigo');



          sql.add('Where tabvidrocor.codigo<>-500');

         { if (pdata<>'')then
          Begin
              sql.add('and tabestoque.data<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(pdata))+#39);
          End;  }

          if (Pgrupo<>'')
          Then Sql.add('and TabVIDRO.GrupoVIDRO='+pgrupo);

          if(vidro<>'')
          then SQL.Add('and tabvidro.codigo='+vidro);

          sql.add('group by TabVIDRO.codigo,');
          sql.add('TabVIDRO.REferencia,');
          sql.add('TabVIDRO.descricao,');
          sql.add('Tabcor.codigo,');
          sql.add('Tabcor.Referencia,');
          sql.add('tabcor.descricao,');
          sql.add('tabVIDRO.precocusto');

          

          Sql.add('order by TabVIDRO.referencia,tabcor.referencia');
          
          open;

          if (Recordcount=0)
          then Begin
                    MensagemAviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;

          With FreltxtRDPRINT do
          begin
               ConfiguraImpressao;
               RDprint.FonteTamanhoPadrao:=S17cpp;
               RDprint.TamanhoQteColunas:=130;
               RDprint.Abrir;
               if (RDprint.Setup=False)
               then begin
                         RDprint.Fechar;
                         exit;
               end;
               LinhaLocal:=3;

               RDprint.ImpC(linhalocal,65,'RELAT�RIO DE ESTOQUE  - VIDRO',[negrito]);
               IncrementaLinha(2);

               if (Pgrupo<>'')
               then begin
                         RDprint.ImpF(linhalocal,1,'Grupo: '+Self.VIDRO.GrupoVIDRO.Get_Codigo+'-'+Self.VIDRO.GrupoVIDRO.Get_Nome,[negrito]);
                         IncrementaLinha(1);
               End;

               if (Pdata<>'')
               then Begin
                         VerificaLinha;
                         RDprint.Impf(linhalocal,1,'Data: '+pdata,[negrito]);
                         IncrementaLinha(1);
               end;

               IncrementaLinha(1);
               VerificaLinha;
               RDprint.Impf(linhalocal,1,CompletaPalavra('CODIGO',6,' ')+' '+
                                             CompletaPalavra('REF.',10,' ')+' '+
                                             CompletaPalavra('NOME VIDRO',42,' ')+' '+
                                             CompletaPalavra('REF.COR',10,' ')+' '+
                                             CompletaPalavra('NOME COR',19,' ')+' '+
                                             CompletaPalavra_a_Esquerda('ESTOQUE',12,' ')+' '+
                                             CompletaPalavra_a_Esquerda('CUSTO',12,' ')+' '+
                                             CompletaPalavra_a_Esquerda('TOTAL',12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               IncrementaLinha(1);

               PsomaEstoque:=0;
               PsomaValorTotal:=0;

               While not(self.ObjQueryPesquisa.eof) do
               Begin
                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('VIDRO').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('refVIDRO').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('nomeVIDRO').asstring,42,' ')+' '+
                                             CompletaPalavra(fieldbyname('refcor').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('nomecor').asstring,19,' ')+' '+
                                             CompletaPalavra_a_Esquerda(fieldbyname('estoque').asstring,12,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('precocusto').asstring),12,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valortotal').asstring),12,' '));
                    IncrementaLinha(1);

                    PsomaEstoque:=PsomaEstoque+Fieldbyname('estoque').asinteger;
                    PsomaValorTotal:=PsomaValorTotal+Fieldbyname('valortotal').asfloat;


                    self.ObjQueryPesquisa.next;
               End;
               DesenhaLinha;
               VerificaLinha;
               RDprint.Impf(linhalocal,93,CompletaPalavra_a_Esquerda(inttostr(pSomaEstoque),12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(formata_valor(psomavalortotal),12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               RDprint.fechar;
          End;
     End;


end;


function TObjVIDROCOR.Get_AcrescimoIcms: string;
begin
     Result:=Self.AcrescimoIcms;
end;

function TObjVIDROCOR.Get_PorcentagemAcrescimoIcms: string;
begin
     result:=Self.PorcentagemAcrescimoIcms;
end;

procedure TObjVIDROCOR.Submit_AcrescimoIcms(Parametro: string);
begin
     Self.AcrescimoIcms:=Parametro;
end;

procedure TObjVIDROCOR.Submit_PorcentagemAcrescimoIcms(Parametro: string);
begin
     Self.PorcentagemAcrescimoIcms:=Parametro;
end;


procedure TObjvidroCOR.EdtvidroCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,Self.Get_TituloPesquisa,Nil)=True)
            Then Begin                                                                         
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then begin
                                            if (Self.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring))
                                            Then Begin
                                                      Self.TabelaparaObjeto;
                                                      LABELNOME.caption:=completapalavra(Self.Vidro.Get_Descricao,50,' ')+' COR: ' +Self.Cor.Get_Descricao;
                                            End;
                                 End;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjVIDROCOR.Get_ClassificacaoFiscal: String;
begin
     Result:=Self.ClassificacaoFiscal;
end;

procedure TObjVIDROCOR.Submit_ClassificacaoFiscal(parametro: String);
begin
     Self.ClassificacaoFiscal:=Parametro;
end;

function TObjVIDROCOR.RetornaEstoque:String;
begin
        With Self.ObjQueryEstoque do
        begin
             close;
             sql.clear;
             sql.add('Select coalesce(sum(quantidade),0) as estoque from tabestoque where vidrocor='+Self.Codigo);
             //SQL.Add('Select estoque from tabvidrocor where codigo='+self.Codigo);
             open;
             result:=fieldbyname('estoque').asstring;
        End;
end;

procedure TObjVIDROCOR.Submit_Estoque(parametro:string);
begin
    Self.Estoque:=parametro;
end;

function TObjVIDROCOR.Get_Estoque:string;
begin
    Result:=self.Estoque;
end;

procedure TObjVIDROCOR.DiminuiEstoque(quantidade,vidrocor:string);
var
   ObjqueryEstoque:TIBQuery;
begin
   try
          ObjqueryEstoque:=TIBQuery.Create(nil);
          ObjqueryEstoque.Database:=FDataModulo.IBDatabase;
          with ObjqueryEstoque do
          begin
                Close;
                sql.Clear;
                sql.Add('update tabvidrocor set estoque=estoque+'+quantidade);
                SQL.Add('where codigo='+vidrocor);
                ExecSQL;
          end;
   finally
          FreeAndNil(ObjqueryEstoque);
   end;

end;

function TObjVIDROCOR.AumentaEstoque(quantidade,vidrocor:string):Boolean;
var
   ObjqueryEstoque:TIBQuery;
begin
   Result:=True;
   try
          ObjqueryEstoque:=TIBQuery.Create(nil);
          ObjqueryEstoque.Database:=FDataModulo.IBDatabase;
          with ObjqueryEstoque do
          begin
                try
                      Close;
                      sql.Clear;
                      sql.Add('update tabvidrocor set estoque=estoque+'+quantidade);
                      SQL.Add('where codigo='+vidrocor);
                      ExecSQL;
                except
                      Result:=False;
                end;

          end;
   finally
          FreeAndNil(ObjqueryEstoque);
          FDataModulo.IBTransaction.CommitRetaining;
   end;

end;



function TObjVIDROCOR.Get_EstoqueMinimo: string;
begin
  result := self.EstoqueMinimo;
end;

procedure TObjVIDROCOR.Submit_EstoqueMinimo(parametro: string);
begin
  self.estoqueminimo := parametro;
end;

end.



