object FConsultaRapida: TFConsultaRapida
  Left = 547
  Top = 254
  Width = 882
  Height = 549
  Caption = 'CONSULTA R'#193'PIDA'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    866
    511)
  PixelsPerInch = 96
  TextHeight = 13
  object lbnomeproduto: TLabel
    Left = 258
    Top = 255
    Width = 596
    Height = 19
    AutoSize = False
    Caption = 'Nome produto'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object label10: TLabel
    Left = 15
    Top = 328
    Width = 226
    Height = 28
    AutoSize = False
    Caption = 'Valor do Produto R$........ '
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbNomeCor: TLabel
    Left = 258
    Top = 292
    Width = 598
    Height = 20
    AutoSize = False
    Caption = 'Nome cor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 15
    Top = 365
    Width = 230
    Height = 29
    AutoSize = False
    Caption = 'Valor Total           R$..........  '
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbPreco: TLabel
    Left = 259
    Top = 327
    Width = 172
    Height = 20
    AutoSize = False
    BiDiMode = bdRightToLeft
    Caption = '0,00'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = []
    ParentBiDiMode = False
    ParentFont = False
  end
  object lbPRecoTotal: TLabel
    Left = 258
    Top = 367
    Width = 180
    Height = 19
    AutoSize = False
    BiDiMode = bdRightToLeft
    Caption = '0,00'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = []
    ParentBiDiMode = False
    ParentFont = False
  end
  object lb5: TLabel
    Left = 15
    Top = 258
    Width = 231
    Height = 18
    Caption = 'Poduto............................'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb6: TLabel
    Left = 15
    Top = 293
    Width = 229
    Height = 18
    Caption = 'Cor.................................'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb7: TLabel
    Left = 15
    Top = 182
    Width = 233
    Height = 18
    Caption = 'Material...........................'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbMaterial: TLabel
    Left = 258
    Top = 180
    Width = 593
    Height = 21
    AutoSize = False
    Caption = 'Nome material'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbTipo: TLabel
    Left = 258
    Top = 218
    Width = 594
    Height = 21
    AutoSize = False
    Caption = 'Nome tipo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb10: TLabel
    Left = 15
    Top = 221
    Width = 230
    Height = 18
    Caption = 'Tipo................................'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object bvl1: TBevel
    Left = 2
    Top = 156
    Width = 866
    Height = 2
    Anchors = [akLeft, akTop, akRight]
  end
  object EdtCodigoCor: TEdit
    Left = 7
    Top = 121
    Width = 30
    Height = 24
    Color = clSkyBlue
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = False
    OnKeyDown = edtReferenciaCorKeyDown
  end
  object EdtCodigoProduto: TEdit
    Left = 47
    Top = 128
    Width = 30
    Height = 24
    Color = clSkyBlue
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = False
    OnKeyDown = edtReferenciaCorKeyDown
  end
  object PanelQuantidade: TPanel
    Left = 0
    Top = 50
    Width = 866
    Height = 108
    Align = alTop
    Color = 14024703
    TabOrder = 2
    object lbAltura: TLabel
      Left = 482
      Top = 45
      Width = 94
      Height = 16
      Caption = 'Altura..........'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbLArgura: TLabel
      Left = 671
      Top = 46
      Width = 76
      Height = 16
      Caption = 'Largura....'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label56: TLabel
      Left = 481
      Top = 21
      Width = 94
      Height = 16
      Caption = 'Quantidade..'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb3: TLabel
      Left = 13
      Top = 21
      Width = 84
      Height = 16
      Caption = 'Material.....'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb4: TLabel
      Left = 15
      Top = 47
      Width = 80
      Height = 16
      Caption = 'Tipo..........'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbCor: TLabel
      Left = 274
      Top = 22
      Width = 65
      Height = 16
      Caption = 'Cor........'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb1: TLabel
      Left = 272
      Top = 46
      Width = 72
      Height = 16
      Caption = 'Produto...'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbCalcular: TLabel
      Left = 713
      Top = 74
      Width = 122
      Height = 32
      Cursor = crHandPoint
      Caption = 'C&alcular'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = btCalcularClick
      OnMouseMove = lbCalcularMouseMove
      OnMouseLeave = lbCalcularMouseLeave
    end
    object EdtAltura: TEdit
      Left = 579
      Top = 45
      Width = 84
      Height = 19
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnExit = EdtAlturaExit
    end
    object EdtLargura: TEdit
      Left = 751
      Top = 43
      Width = 84
      Height = 19
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnExit = EdtLarguraExit
    end
    object EdtQuantidade: TEdit
      Left = 578
      Top = 18
      Width = 84
      Height = 19
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnExit = EdtQuantidadeExit
    end
    object edtReferenciaCor: TEdit
      Left = 348
      Top = 19
      Width = 89
      Height = 19
      CharCase = ecUpperCase
      Color = 6073854
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnExit = edtReferenciaCorExit
      OnKeyDown = edtReferenciaCorKeyDown
    end
    object edtreferenciaproduto: TEdit
      Left = 348
      Top = 45
      Width = 89
      Height = 19
      CharCase = ecUpperCase
      Color = 6073854
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnExit = edtreferenciaprodutoExit
      OnKeyDown = edtreferenciaprodutoKeyDown
    end
    object ComboMateriais: TComboBox
      Left = 100
      Top = 20
      Width = 145
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
      OnExit = ComboMateriaisExit
      Items.Strings = (
        'VIDRO'
        'FERRAGEM'
        'PERFILADO'
        'KITBOX'
        'PERSIANA'
        'DIVERSOS'
        'SERVI'#199'O')
    end
    object ComboTipo: TComboBox
      Left = 100
      Top = 46
      Width = 145
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 1
      OnExit = ComboTipoExit
      Items.Strings = (
        'INSTALADO'
        'FORNECIDO'
        'RETIRADO')
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 466
    Width = 866
    Height = 45
    Align = alBottom
    Color = clMedGray
    TabOrder = 3
    DesignSize = (
      866
      45)
    object imgrodape: TImage
      Left = 1
      Top = 1
      Width = 864
      Height = 43
      Align = alBottom
    end
    object lb2: TLabel
      Left = 530
      Top = 11
      Width = 288
      Height = 20
      Anchors = [akTop]
      Caption = 'Existem X funcion'#225'rios cadastrados'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 866
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 4
    object lbnomeformulario: TLabel
      Left = 515
      Top = 0
      Width = 265
      Height = 35
      Align = alCustom
      Alignment = taCenter
      Caption = 'Pesquisa R'#225'pida'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -29
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
end
