object FSERVICO: TFSERVICO
  Left = 481
  Top = 281
  Width = 813
  Height = 493
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'SERVICO'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object imgrodape: TImage
    Left = 0
    Top = 410
    Width = 797
    Height = 45
    Align = alBottom
    Stretch = True
  end
  object lbLbReferencia: TLabel
    Left = 16
    Top = 73
    Width = 53
    Height = 14
    Caption = 'Refer'#234'ncia'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object lbLbGrupoServico: TLabel
    Left = 16
    Top = 101
    Width = 70
    Height = 14
    Caption = 'Grupo Servi'#231'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object lbLbDescricao: TLabel
    Left = 16
    Top = 130
    Width = 49
    Height = 14
    Caption = 'Descri'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object lbLbPrecoCusto: TLabel
    Left = 16
    Top = 158
    Width = 74
    Height = 14
    Caption = 'Pre'#231'o de Custo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object lbLbArredondamento: TLabel
    Left = 16
    Top = 186
    Width = 81
    Height = 14
    Caption = 'Arredondamento'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object lbNomeGrupoServico: TLabel
    Left = 194
    Top = 101
    Width = 408
    Height = 13
    Cursor = crHandPoint
    AutoSize = False
    Color = clSkyBlue
    Font.Charset = ANSI_CHARSET
    Font.Color = clAqua
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
    OnClick = lbNomeGrupoServicoClick
    OnMouseMove = lbNomeGrupoServicoMouseMove
    OnMouseLeave = lbNomeGrupoServicoMouseLeave
  end
  object lbAtivoInativo: TLabel
    Left = 675
    Top = 67
    Width = 80
    Height = 37
    Caption = 'Ativo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -32
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb11: TLabel
    Left = 16
    Top = 215
    Width = 27
    Height = 14
    Caption = 'CFOP'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object lb12: TLabel
    Left = 16
    Top = 243
    Width = 20
    Height = 14
    Caption = 'STB'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object lbNomeCfop: TLabel
    Left = 194
    Top = 214
    Width = 334
    Height = 13
    Cursor = crHandPoint
    AutoSize = False
    Color = clSkyBlue
    Font.Charset = ANSI_CHARSET
    Font.Color = clAqua
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
    OnClick = lbNomeGrupoServicoClick
    OnMouseMove = lbNomeGrupoServicoMouseMove
    OnMouseLeave = lbNomeGrupoServicoMouseLeave
  end
  object lbSTB: TLabel
    Left = 194
    Top = 239
    Width = 334
    Height = 13
    Cursor = crHandPoint
    AutoSize = False
    Color = clSkyBlue
    Font.Charset = ANSI_CHARSET
    Font.Color = clAqua
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = True
    OnClick = lbNomeGrupoServicoClick
    OnMouseMove = lbNomeGrupoServicoMouseMove
    OnMouseLeave = lbNomeGrupoServicoMouseLeave
  end
  object lb13: TLabel
    Left = 16
    Top = 269
    Width = 78
    Height = 14
    Caption = 'Tipo de Al'#237'quiota'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 797
    Height = 54
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    DesignSize = (
      797
      54)
    object lbnomeformulario: TLabel
      Left = 487
      Top = 3
      Width = 104
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Cadastro de'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigo: TLabel
      Left = 602
      Top = 8
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb1: TLabel
      Left = 488
      Top = 25
      Width = 64
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Servi'#231'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object btAjuda: TSpeedButton
      Left = 744
      Top = -17
      Width = 53
      Height = 80
      Cursor = crHandPoint
      Hint = 'Ajuda'
      Anchors = [akTop, akRight]
      Flat = True
      Glyph.Data = {
        4A0D0000424D4A0D0000000000003600000028000000240000001F0000000100
        180000000000140D0000D8030000D80300000000000000000000FEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFAF6FBF9F2FEFEF4FFFFFAFF
        FCFDFFF9FFFEFEFDFCFFFFFEFEFFFDFEFCFEFEF8FFFFFBFEFCFFFFFCFFFDF6FB
        FFF7F8FFF7FBFFFBFBFDF7FFFFFCFFFFFEF7FDFCFBFFFFFFFBFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFF9FD
        F8FDFFFFFDFBFFFFFBFFFFFDFEFFFCFCFFFEFFFEFDFFFCF9FBF0EFF1F0EBEDF9
        F1F2F6F8F9F1FFFFF4FFFFFEFCFFFFFDFFFFFAFFFBFCFFFEFDFFFFFAFDFAFCFD
        F7FFFFFFFCFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFFFCFFFFFCFEFFFDFCFFFFFFFFFDFFF2E4E6DEBCBCCF
        9F99EB8688E68081EA7E7DF68686FE9092FDA2A5F7C3C3FFF0EDFFFAFFF8FFFF
        FFFDFFFFF5FDFCFDFFF6FFFFFBF8FAFFFDFEFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFBFFFCFEFFFBFFFCE7
        E1DCC29E9EBF7579DC7677F07B76FF8783FF8F88FD9388FB8F84FF837BFF7977
        F77576F98084FBB6B9F2F2ECE8FFFAF8FDFCFFFEFFFAFCFDFFFCFEFCFEFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFEFDFFFAFCD7B8BBB17879C87772F08E88FE9893F58E8CE7797DD76F70
        D26867DC716EE88581F69A95F99591FE8686FF7173F28484FFD9D4FFFFFBFDF8
        F7FFFCFFFFFEFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFF9B99D9CA76B6CDA8786F99B96E17D79
        B6484C98222FAB1122AA0F1EA5131FA41D25AA2226C53938E6625BFF9085FD9B
        91FF7B7CF2777BF4C8C7F8FFFEF9FEFDFFFEFFFFFBFCFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFC0A0A1A7706D
        E6928CFC9691B3484A87192589112398182BA32E31DC6F71EB8082CC5050BB28
        26D32A22EB2F24FE3727F36D69FF9D97FB7D83F7707EFEC5CDFFFEFFFFFDFDED
        FFFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFCFBFDFFFDFFFFFBFFFAFFFFF2FAFA
        FFFEFEDCBDBEA66D6BDB9092ED909996303C770E1B8D1E2C9B1C2BA11425D96F
        75FDA8B0FFA2B0FFA0AAC83F43BC2F26D73927FF3929FF2B1CEC5650FF9996FE
        7D7AFD7F7EFADFDBFFFEFFF4FEFEFEFDFFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFF
        FDF9FEFFFDFFF7FAFEFAFFFFF4E6E7A67F81CF8B8CEC9A9F92323C7B121D9123
        2F921C27A5232EA71C27DE837EFFB7B5F9A9AEFDB0B3BF4343B42821C9372BDA
        3326E33626CB2728CA5556FF9893FB7777DE9A9BFFFEFEFFFCFDFEFDFFFEFDFF
        FEFDFFFEFDFFFEFEFEFEFDFFFFFBFFFFFBFFFAFBFFFFFFFFC3AEADA77678F8A4
        A9A9515776131B8D202898242BA3282CA72629A82627B55047EBADA2FFBCB5CD
        7974A92E2AA13026A5372BBD332DBF3528BD2B2DA51E26DF7372FF918FE27377
        F0CCCCFFFFFEFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFAF9FBFFFDFFFFFAFFFFFE
        FFF5F0EFA38384DB9FA0E78A91791D22852428992E309E2827A72C28AC3028AB
        3229A3271FA13E36B4413E9E332CB03129A73126A23127B0322EA93026AD3131
        A91925B63239F2948FFD7C7FE9989BFBFFF9FEFDFFFEFDFFFEFDFFFEFDFFFFFE
        FEFCFBFDFFFEFFFFFDFFFFFEFFDACECEAE8687F0ABAEB156597E24248B2C2992
        2A25A93730A93328AE3B2EA33222D35E59EA928BF28C87E27D74AC362BA93A2A
        A23729A5342AA3342CA62728B02931A71821D8716FFF8A8ADE7D7FEFECE7FEFD
        FFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFFFDFCFEFFFCFEFFFEFFC0B3B1BF9392F0
        A5A78F3B35842A2396352B9F372AA93C2EAC3D2DAC3C2AA1311FD16D69F8CABF
        FBC0B7FCABA3AA3A2EA23827A33928A9372AA6352BAE302BA72C2EAA1922C550
        51FF9692DC7475E0D2D3FFFEFFFEFDFFFEFDFFFEFDFFFFFDFEFEFDFFFEFDFFFF
        FDFFFFFEFEB6A9A7CB9D9CEC9E9F8335288E3527A0392AAD4130A83C2AA73B29
        A93927AE3827CE6C66FFCDC1FABAB5FFBFB6D46B62AD3126B43427B13A2BA933
        27A93327A02A25AC1F28AF373BFC9D94E7797BD1B8BCFFFEFFFFFEFFFEFDFFFE
        FDFFFFFDFEFCFCFCFDFCFEFFFEFFFDFBFBB5A8A6DBAEABE19192813522983C29
        A73B29AF3E2AA83B26AA3F2AAD3B2AAF3224B05A4EFFE0D0FFCEC6F7BEB5FFBE
        B3DE8C7BA44434A33628AD3C2CAD3726A8342DA7262DAE2E33FA9B92E27A7BD4
        ABB3FFFEFFFFFEFFFEFDFFFEFDFFFBFCFFF9FEFFFFFCFDFBFFFFFCF9FBC9AAAB
        DEB1ADDF9A91813628B03C2BAB3D25A7412AA6362AB3362EA13726AE3C25A833
        24D79987FFF4E4F9D3CEFEBEBEFFC9C3EC9E91AD4436B137299C3B27AA3526B1
        2F28A4302FF7969AE67F86C7AAA5FFFEFFFFFEFFFEFDFFFEFDFFFAFBFFFCFEFF
        FFFEFFFCFEFFFCFEFFCEB4B4E6B6B2E6A99F98392AA13426B2382C9E3E2EAA3E
        33AD4338A74539A24536AB443B9B3D38CF8A87FFEBE6FEDED9F1C0B8FFC9BDEC
        8B81AA372AA73B2AA83F2CA52B1FA6403BF9A1A1E57E86C9B3AEFFFEFFFFFEFF
        FEFDFFFEFDFFFAFDFFFDFCFEFFFEFFFDFCFEFAFFFFD3BEC0ECB5B2F5C2B8AB40
        329F3D31B84D49A24F47AD554EAE574DAE5D55B1655FAD5654B34E50A13B40B9
        7B7BFFE1DBFED7CFF9CAC2FFBCB5BD5146AD3627A3402AA83526AD544AFEACAB
        D5787FD4C7C5FFFEFFFEFDFFFEFDFFFEFDFFFBFFFFFFFBFDFFFDFDFFFDFFF9FE
        FFDFD1D2E7B1B0FFDACEC26B61A74C45A55C54A85A54D3958FF2B8B3F9C2BFF4
        B4B3AD5352AB5C59AC5553A54343F7C0BBFDE0D9FFCCCAFBC9C9CC6A60B03429
        9F3923A63624C9776BFFADACC67E84E4E0DFFEFDFFFEFDFFFEFDFFFEFDFFFAFE
        FFFFFCFFFCFCFCFFFEFFFBFEFFF4ECEDE2B7B4FFDBD2DAB0ABA6504AA86153AB
        514AD59594FFF8F7FEF1EFFAD4D2C469649C5148A85950A14B45ECBBB3FBE1DA
        FFCED0FFCFD6D06E66AB352AAD3B2A9F3726F7A89DFBA4A7BB8E91F2F7F6FEFD
        FFFEFDFFFEFDFFFEFDFFFCFBFDFFFEFFFCFEFEFEFDFFFDFCFFFFFEFFE2C7C3F3
        CAC1EDDEDCBD7672AD5144CE6459D7797AF6F5F1F4FFFEF9F3EEE9B4AAB16860
        B45E58C99087FAD7CDFFDCD6F8D5D2FECBCFBE5A55A23127A12F22CB7264FFC3
        BBD28B8ECFB7B9FAFFFFFEFDFFFEFDFFFEFDFFFEFDFFFEFBFDFFFFFFFCFEFEFB
        FDFEFFFAFFFFFEFFEBE5DEF2C4BDFFE5E8EFC8C0AE6053E17164F47773F0BBB8
        FDFEFAFDFFFFFFF8F1FCD6D4FFCDCCFFDDD8FFDED8FFE0DAFFDDD7E7A1A2A339
        32A7352EAC4A3EF8B8ADECB2ADB68E90FAECEEFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFDFFFCFCFCFAFFFEFAFDFFFFFBFFFFFDFDFBFFF9FFCFC9FFC7CCFFF9EF
        E8BFB0D66D5FFF7C70F9807EEACECDFFFBFFFDFCFFF9FEFFFDFFFFFFE4E5FFE2
        DFF6E7DEEDB6AFB34648A1312BB03E37FCAA9FF7CDC1BC8F8CD5C7C8FFFDFFFC
        FEFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFFFEFFFFFEFF
        FDFCFEFCFBFDFCC8C8F6D7D6FFFCF6F0C9C1E57F7AFF7673F98481ECA5A1E6D6
        D7FCF1F3F9F1F1ECD7D9F0C1C3D78A8DA84646A03232A54B4BECABAAFFD1CFC9
        A09EC9ADACFEF9F8FBFFFFFCFCFCFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFF
        FDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFDFCFEFFF4EEF8CDCAFDD8D4FFFBF4FDE6
        E4FBA1A6FB7375FD6F68EA6E5CCE6855B76958AE6458A5514B993E3AA04D4BC8
        7F7BFCC3C1FFCBCCDAA4A4BFAEABF3F9F4FFFFFEFFFCFDFCFEFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFEFDFFFFFEFFF4FF
        FFFBF2EFF6D6D1FDD9D3FFEFE8FCFBF1FFE6DCF6BBB2EE9285D07C70B76F65B2
        6F66C0807BDDA39EF3C1BBFFD6CFF6C6C5D4ACADCEBBB8F9FAF6F7FFFDFAFCFC
        FFFEFFFCF9FBFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFC
        FEFDFCFEFEFDFFFFFEFFFFFAFCFBFFFFF9F9F9FCE0DFFFD3CCFEDDD4F8ECE6F8
        F6F5F5F8F6F4EFEEF6E4E5FADBDCFFDBDCFFD9D8FBCAC8F0C0BCD7B5B6E1D8D5
        FAFFFEFFFFFFFEFCFCFAFFFFF8FDFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFFFFFBFFFBFCFDFDFFFD
        FBFFFFEEF1FFE2E1FFD3D2FFD0D2FFD2CFFFD1CDFFD2CFFFCFCFFFC9C9F2BFC3
        E9C4C6EED6D8F5F7F7FFFBFCFFFEFFFFFCFEFDFFFFF9FCFFFCFBFFFFFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFFFEFDFFFEFDFFFDFCFEFDFCFEFD
        FCFEFFFBFFF0FDFBF1FFFBFFFEFFFFFDFFF9FEFFF9FCFAFEF2EEF9EAE1F4E5DC
        F3E3DCF3E3DDF6E7E5FCF1F3FEFAFFF9FCFFF4FEFEFFFEFFFFF7FAFFFDFFFCF9
        FBFFFDFFFFFCFFF5F8FCFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFF7F9FFFDFEFFFFFBF8FFFDF9FDFBFAF1FBFB
        F6FFFFFBFDFEFBFEFFFFFCFFFFFDFFFFFEFFFBFFFFF8FFFFFDFEFFFFFAFFFFFD
        FFFAFEFFF8FFFFF9FBFCFFFDFFFFF9FCFBFAFCFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFCFFFEFF
        F7FDFFF5FEFFFBFFFFFFFFFEFFFCFDFFFCFFFFFAFFFFFCFFF9FEFDF9FFFDFBFF
        FEF9FCFAFBFBFBFFFFFFFFFCFEFEFDFFFBFAFCFBFFFFF5FDFDF7FFFFFDFFFFFF
        FCFFFEFDFFFEFDFFFEFDFFFEFDFF}
      OnClick = btAjudaClick
    end
    object btrelatorio: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btRelatorioClick
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btPesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btExcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btCancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btSalvarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btAlterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btNovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btSairClick
      Spacing = 0
    end
  end
  object edtReferencia: TEdit
    Left = 116
    Top = 69
    Width = 72
    Height = 19
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    MaxLength = 10
    ParentFont = False
    TabOrder = 1
  end
  object edtGrupoServico: TEdit
    Left = 116
    Top = 97
    Width = 72
    Height = 19
    Color = 6073854
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    MaxLength = 9
    ParentFont = False
    TabOrder = 2
    OnDblClick = edtGrupoServicoDblClick
    OnExit = edtGrupoServicoExit
    OnKeyDown = edtGrupoServicoKeyDown
    OnKeyPress = edtGrupoServicoKeyPress
  end
  object edtDescricao: TEdit
    Left = 116
    Top = 126
    Width = 499
    Height = 19
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    MaxLength = 100
    ParentFont = False
    TabOrder = 3
  end
  object edtPrecoCusto: TEdit
    Left = 116
    Top = 154
    Width = 72
    Height = 19
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    MaxLength = 5
    ParentFont = False
    TabOrder = 4
    OnExit = edtPrecoCustoExit
    OnKeyPress = edtPrecoCustoKeyPress
  end
  object edtArredondamento: TEdit
    Left = 116
    Top = 182
    Width = 72
    Height = 19
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    MaxLength = 6
    ParentFont = False
    TabOrder = 5
    OnKeyPress = edtArredondamentoKeyPress
  end
  object grp1: TGroupBox
    Left = 16
    Top = 298
    Width = 210
    Height = 105
    Caption = 'Margem de Lucro'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    object lbLbPorcentagemInstalado: TLabel
      Left = 11
      Top = 20
      Width = 43
      Height = 14
      Caption = 'Instalado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbPorcentagemFornecido: TLabel
      Left = 11
      Top = 76
      Width = 48
      Height = 14
      Caption = 'Fornecido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbLbPorcentagemRetirado: TLabel
      Left = 11
      Top = 48
      Width = 40
      Height = 14
      Caption = 'Retirado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb2: TLabel
      Left = 180
      Top = 23
      Width = 10
      Height = 14
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb3: TLabel
      Left = 180
      Top = 52
      Width = 10
      Height = 14
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb4: TLabel
      Left = 180
      Top = 79
      Width = 10
      Height = 14
      Caption = '%'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object edtPorcentagemRetirado: TEdit
      Left = 106
      Top = 48
      Width = 72
      Height = 19
      Color = 15663069
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 1
      OnExit = edtPorcentagemRetiradoExit
      OnKeyPress = edtPorcentagemRetiradoKeyPress
    end
    object edtPorcentagemFornecido: TEdit
      Left = 106
      Top = 76
      Width = 72
      Height = 19
      Color = 15663069
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 2
      OnExit = edtPorcentagemFornecidoExit
      OnKeyPress = edtPorcentagemFornecidoKeyPress
    end
    object edtPorcentagemInstalado: TEdit
      Left = 106
      Top = 20
      Width = 72
      Height = 19
      Color = 15663069
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 0
      OnExit = edtPorcentagemInstaladoExit
      OnKeyPress = edtPorcentagemInstaladoKeyPress
    end
  end
  object grp2: TGroupBox
    Left = 264
    Top = 299
    Width = 210
    Height = 105
    Caption = 'Pre'#231'o de Venda'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    object lb5: TLabel
      Left = 11
      Top = 20
      Width = 43
      Height = 14
      Caption = 'Instalado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb6: TLabel
      Left = 11
      Top = 76
      Width = 48
      Height = 14
      Caption = 'Fornecido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb7: TLabel
      Left = 11
      Top = 48
      Width = 40
      Height = 14
      Caption = 'Retirado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb8: TLabel
      Left = 108
      Top = 23
      Width = 13
      Height = 14
      Caption = 'R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb9: TLabel
      Left = 108
      Top = 50
      Width = 13
      Height = 14
      Caption = 'R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb10: TLabel
      Left = 108
      Top = 78
      Width = 13
      Height = 14
      Caption = 'R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object edtPrecoVendaRetirado: TEdit
      Left = 125
      Top = 48
      Width = 72
      Height = 19
      Color = 15663069
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 0
      OnExit = edtPrecoVendaRetiradoExit
    end
    object edtPrecoVendaFornecido: TEdit
      Left = 125
      Top = 76
      Width = 72
      Height = 19
      Color = 15663069
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 1
      OnExit = edtPrecoVendaFornecidoExit
    end
    object edtPrecoVendaInstalado: TEdit
      Left = 125
      Top = 20
      Width = 72
      Height = 19
      Color = 15663069
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 2
      OnExit = edtPrecoVendaInstaladoExit
    end
  end
  object chkAtivo: TCheckBox
    Left = 639
    Top = 74
    Width = 25
    Height = 25
    Alignment = taLeftJustify
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clLime
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    OnClick = chkAtivoClick
  end
  object EdtCFOP: TEdit
    Left = 116
    Top = 211
    Width = 72
    Height = 19
    Color = 6073854
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    MaxLength = 6
    ParentFont = False
    TabOrder = 9
    OnDblClick = EdtCFOPDblClick
    OnKeyDown = EdtCFOPKeyDown
    OnKeyPress = edtArredondamentoKeyPress
  end
  object edtSTB: TEdit
    Left = 116
    Top = 239
    Width = 72
    Height = 19
    Color = 6073854
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    MaxLength = 6
    ParentFont = False
    TabOrder = 10
    OnDblClick = edtSTBDblClick
    OnKeyDown = edtSTBKeyDown
    OnKeyPress = edtArredondamentoKeyPress
  end
  object ComboTipoAliquota: TComboBox
    Left = 116
    Top = 265
    Width = 133
    Height = 22
    Color = 10210815
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ItemHeight = 14
    ParentFont = False
    TabOrder = 11
    Text = 'Tributado   |T'
    OnKeyPress = ComboTipoAliquotaKeyPress
    Items.Strings = (
      ''
      'Tributado      |T'
      'Isento         |I'
      'Substitui'#231#227'o   |F'
      'N'#227'o Tributado  |N ')
  end
end
