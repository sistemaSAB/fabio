unit UGRUPOFERRAGEM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjFERRAGEM,
  UessencialGlobal, Tabs,UPLanodeContas;

type
  TFGRUPOFERRAGEM = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigoGrupoFerragens: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    lbLbNome: TLabel;
    edtNome: TEdit;
    lb2: TLabel;
    edtPlanodeContas: TEdit;
    grp1: TGroupBox;
    lbLbPorcentagemFornecido: TLabel;
    lbLbPorcentagemRetirado: TLabel;
    lbLbPorcentagemInstalado: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    lb5: TLabel;
    edtPorcentagemInstalado: TEdit;
    edtPorcentagemFornecido: TEdit;
    edtPorcentagemRetirado: TEdit;
    lbNomePlanoDeContas: TLabel;
    btPrimeiro: TSpeedButton;
    btAnterior: TSpeedButton;
    btProximo: TSpeedButton;
    btUltimo: TSpeedButton;
//DECLARA COMPONENTES

    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);

    procedure btOpcoesClick(Sender: TObject);
    procedure edtPlanodeContasExit(Sender: TObject);
    procedure edtPlanodeContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure edtPlanodeContasDblClick(Sender: TObject);
    procedure edtPlanodeContasKeyPress(Sender: TObject; var Key: Char);
    procedure edtPorcentagemInstaladoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemFornecidoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemRetiradoKeyPress(Sender: TObject;
      var Key: Char);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FGRUPOFERRAGEM: TFGRUPOFERRAGEM;
  ObjFERRAGEM:TObjFERRAGEM;

implementation

uses Upesquisa, UessencialLocal, UobjGRUPOFERRAGEM, UMenuRelatorios,
  UDataModulo, UescolheImagemBotao;

{$R *.dfm}




procedure TFGRUPOFERRAGEM.FormShow(Sender: TObject);
begin
     Self.limpaLabels;
     ColocaUpperCaseEdit(Self);
     desabilita_campos(Self);
     ColocaUpperCaseEdit(Self);

     Try
        ObjFERRAGEM:=TObjFERRAGEM.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     habilita_botoes(Self);
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE GRUPO DE FERRAGEM')=False)
     Then desab_botoes(Self);


    FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
    FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
    lbCodigoGrupoFerragens.caption:='';
    if(Tag<>0)
    then begin
         if(ObjFERRAGEM.GrupoFerragem.LocalizaCodigo(IntToStr(Tag))=True)then
         begin
              ObjFERRAGEM.GrupoFerragem.TabelaparaObjeto;
              self.ObjetoParaControles;
         end;

    end;

end;

procedure TFGRUPOFERRAGEM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjFERRAGEM=Nil)
     Then exit;

     If (ObjFERRAGEM.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjFERRAGEM.free;
   
end;

procedure TFGRUPOFERRAGEM.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

      lbCodigoGrupoFerragens.Caption:='0';
     //edtcodigo.text:=ObjFerragem.GrupoFerragem.Get_novocodigo;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjFerragem.GrupoFerragem.status:=dsInsert;
     EdtNome.setfocus;
      btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;
end;

procedure TFGRUPOFERRAGEM.btSalvarClick(Sender: TObject);
begin

     If ObjFerragem.GrupoFerragem.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjFerragem.GrupoFerragem.salvar(true)=False)
     Then exit;

     lbCodigoGrupoFerragens.Caption:=ObjFerragem.GrupoFerragem.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFGRUPOFERRAGEM.btAlterarClick(Sender: TObject);
begin
    If (ObjFerragem.GrupoFerragem.Status=dsinactive) and (lbCodigoGrupoFerragens.Caption<>'')
    Then Begin
                habilita_campos(Self);

                ObjFerragem.GrupoFerragem.Status:=dsEdit;
                edtNome.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                 btnovo.Visible :=false;
               btalterar.Visible:=false;
               btpesquisar.Visible:=false;
               btrelatorio.Visible:=false;
               btexcluir.Visible:=false;
               btsair.Visible:=false;
               btopcoes.Visible :=false;
          End;

end;

procedure TFGRUPOFERRAGEM.btCancelarClick(Sender: TObject);
begin
     ObjFerragem.GrupoFerragem.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
      lbCodigoGrupoFerragens.caption:='';

end;

procedure TFGRUPOFERRAGEM.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjFerragem.GrupoFerragem.Get_pesquisa,ObjFerragem.GrupoFerragem.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjFerragem.GrupoFerragem.status<>dsinactive
                                  then exit;

                                  If (ObjFerragem.GrupoFerragem.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjFerragem.GrupoFerragem.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFGRUPOFERRAGEM.btExcluirClick(Sender: TObject);
begin
     If (ObjFerragem.GrupoFerragem.status<>dsinactive) or (lbCodigoGrupoFerragens.Caption='')
     Then exit;

     If (ObjFerragem.GrupoFerragem.LocalizaCodigo(lbCodigoGrupoFerragens.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjFerragem.GrupoFerragem.exclui(lbCodigoGrupoFerragens.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;

     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFGRUPOFERRAGEM.btRelatorioClick(Sender: TObject);
begin
//    ObjFerragem.GrupoFerragem.Imprime;
end;

procedure TFGRUPOFERRAGEM.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFGRUPOFERRAGEM.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFGRUPOFERRAGEM.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFGRUPOFERRAGEM.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;

end;

procedure TFGRUPOFERRAGEM.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFGRUPOFERRAGEM.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjFerragem.GrupoFerragem do
    Begin
        Submit_Codigo(lbCodigoGrupoFerragens.Caption);
        Submit_Nome(edtNome.text);
        Submit_PorcentagemInstalado(EdtPorcentagemInstalado.Text);
        Submit_PorcentagemFornecido(EdtPorcentagemFornecido.Text);
        Submit_PorcentagemRetirado(EdtPorcentagemRetirado.Text);
        PlanoDeContas.Submit_CODIGO(EdtPlanodeContas.Text);

        result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFGRUPOFERRAGEM.LimpaLabels;
begin
   lbNomePlanoDeContas.Caption:='';
end;

function TFGRUPOFERRAGEM.ObjetoParaControles: Boolean;
begin
  Try
     With ObjFerragem.GrupoFerragem do
     Begin
     
        lbCodigoGrupoFerragens.Caption:=Get_Codigo;
        EdtNome.text:=Get_Nome;
        EdtPorcentagemInstalado.Text:=Get_PorcentagemInstalado;
        EdtPorcentagemFornecido.Text:=Get_PorcentagemFornecido;
        EdtPorcentagemRetirado.Text:=Get_PorcentagemRetirado;
        EdtPlanodeContas.Text:=PlanoDeContas.Get_CODIGO;
        lbNomePlanoDeContas.Caption:='TIPO = '+ObjFERRAGEM.GrupoFerragem.PlanoDeContas.Get_Tipo+' | ';
        lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' CLASSIF. = '+ObjFERRAGEM.GrupoFerragem.PlanoDeContas.Get_Classificacao+' | ';
        lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' NOME = '+ObjFERRAGEM.GrupoFerragem.PlanoDeContas.Get_Nome;
        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFGRUPOFERRAGEM.TabelaParaControles: Boolean;
begin
     If (ObjFerragem.GrupoFerragem.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;


procedure TFGRUPOFERRAGEM.btOpcoesClick(Sender: TObject);
begin
     With FmenuRelatorios do
     Begin
          //era no titulo antes
          NomeObjeto:='UOBJGRUPOFERRAGEM';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Atualiza Margens');
                Items.Add('Atualiza Pre�o de Custo');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0 :  ObjFERRAGEM.AtualizaMargens(lbCodigoGrupoFerragens.Caption);
                1 :  ObjFERRAGEM.AtualizaPrecos(lbCodigoGrupoFerragens.Caption);
          End;
     end;

end;

procedure TFGRUPOFERRAGEM.edtPlanodeContasExit(Sender: TObject);
begin
    ObjFERRAGEM.GrupoFerragem.EdtGrupoPlanoDeContasExit(Sender, lbNomePlanoDeContas);
    lbNomePlanoDeContas.Caption:='TIPO = '+ObjFERRAGEM.GrupoFerragem.PlanoDeContas.Get_Tipo+' | ';
    lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' CLASSIF. = '+ObjFERRAGEM.GrupoFerragem.PlanoDeContas.Get_Classificacao+' | ';
    lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' NOME = '+ObjFERRAGEM.GrupoFerragem.PlanoDeContas.Get_Nome;
end;

procedure TFGRUPOFERRAGEM.edtPlanodeContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjFERRAGEM.GrupoFerragem.EdtGrupoPlanoDeContasKeyDown(Sender, Key, Shift, lbNomePlanoDeContas);
end;

procedure TFGRUPOFERRAGEM.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjFerragem.GrupoFerragem.PrimeiroRegistro = false)then
    exit;

    ObjFerragem.GrupoFerragem.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOFERRAGEM.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigoGrupoFerragens.Caption='')then
    lbCodigoGrupoFerragens.Caption:='0';

    if  (ObjFerragem.GrupoFerragem.RegistoAnterior(StrToInt(lbCodigoGrupoFerragens.Caption)) = false)then
    exit;

    ObjFerragem.GrupoFerragem.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOFERRAGEM.btProximoClick(Sender: TObject);
begin
    if (lbCodigoGrupoFerragens.Caption='')then
    lbCodigoGrupoFerragens.Caption:='0';

    if  (ObjFerragem.GrupoFerragem.ProximoRegisto(StrToInt(lbCodigoGrupoFerragens.Caption)) = false)then
    exit;

    ObjFerragem.GrupoFerragem.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOFERRAGEM.btUltimoClick(Sender: TObject);
begin
    if  (ObjFerragem.GrupoFerragem.UltimoRegistro = false)then
    exit;

    ObjFerragem.GrupoFerragem.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;



procedure TFGRUPOFERRAGEM.edtPlanodeContasDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FplanoContas:TFplanodeContas;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FplanoContas:=TFplanodeContas.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabplanodecontas','',FplanoContas)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtPlanodeContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbNomePlanodeContas.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FplanoContas);

     End;
end;
procedure TFGRUPOFERRAGEM.edtPlanodeContasKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFGRUPOFERRAGEM.edtPorcentagemInstaladoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFGRUPOFERRAGEM.edtPorcentagemFornecidoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFGRUPOFERRAGEM.edtPorcentagemRetiradoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

end.
