object FtrocaMaterialpedido: TFtrocaMaterialpedido
  Left = 463
  Top = 147
  Width = 856
  Height = 580
  Caption = 'Troca de Materiais de Pedido Conclu'#237'do'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelPedido: TPanel
    Left = 0
    Top = 43
    Width = 840
    Height = 203
    Align = alTop
    BevelOuter = bvNone
    Color = 10643006
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    DesignSize = (
      840
      203)
    object lb1: TLabel
      Left = 672
      Top = 203
      Width = 14
      Height = 13
      Caption = 'lb1'
    end
    object lb2: TLabel
      Left = 10
      Top = 13
      Width = 38
      Height = 14
      Cursor = crHandPoint
      Caption = 'Pedido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = btgravarClick
    end
    object lbConcluir: TLabel
      Left = 714
      Top = 42
      Width = 110
      Height = 33
      Cursor = crHandPoint
      Hint = 'Concluir Trocas ou Devolu'#231#245'es'
      Anchors = [akTop, akRight]
      Caption = 'Concluir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -24
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = btgravarClick
      OnMouseMove = lbConcluirMouseMove
      OnMouseLeave = lbConcluirMouseLeave
    end
    object lbCancelar: TLabel
      Left = 713
      Top = 76
      Width = 118
      Height = 33
      Cursor = crHandPoint
      Hint = 'Cancelar Trocas ou devolu'#231#245'es'
      Anchors = [akTop, akRight]
      Caption = 'Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -24
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = btcancelarClick
      OnMouseMove = lbConcluirMouseMove
      OnMouseLeave = lbConcluirMouseLeave
    end
    object lbDescricaoPedido: TLabel
      Left = 124
      Top = 13
      Width = 429
      Height = 22
      Cursor = crHandPoint
      AutoSize = False
      Caption = 'Descri'#231#227'o do Pedido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clAqua
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = lbDescricaoPedidoClick
      OnMouseMove = lbDescricaoPedidoMouseMove
      OnMouseLeave = lbDescricaoPedidoMouseLeave
    end
    object edtPedido: TEdit
      Left = 55
      Top = 9
      Width = 64
      Height = 19
      Color = 6073854
      TabOrder = 0
      OnChange = edtPedidoChange
      OnDblClick = edtPedidoDblClick
      OnExit = edtPedidoExit
      OnKeyDown = edtPedidoKeyDown
      OnKeyPress = edtPedidoKeyPress
    end
    object MemoPedido: TMemo
      Left = 8
      Top = 40
      Width = 428
      Height = 160
      BevelEdges = []
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = 10643006
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      Lines.Strings = (
        'Cliente:123456789x123456789x123456789x123456789'
        'G')
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      OnClick = MemoPedidoClick
    end
    object MemoNovosValores: TMemo
      Left = 443
      Top = 43
      Width = 258
      Height = 149
      BorderStyle = bsNone
      Color = 10643006
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      Lines.Strings = (
        'MemoNovosValores')
      ParentFont = False
      TabOrder = 2
      OnClick = MemoPedidoClick
    end
  end
  object PanelMateriais: TPanel
    Left = 0
    Top = 246
    Width = 840
    Height = 296
    Align = alClient
    TabOrder = 1
    object edtNovaCor: TEdit
      Left = 680
      Top = 342
      Width = 121
      Height = 21
      TabOrder = 0
      Visible = False
    end
    object pnl1: TPanel
      Left = 1
      Top = 250
      Width = 838
      Height = 45
      Align = alBottom
      TabOrder = 1
      object Image1: TImage
        Left = 1
        Top = 0
        Width = 836
        Height = 44
        Align = alBottom
        Stretch = True
      end
    end
    object edtCodigoMaterial: TEdit
      Left = 632
      Top = 334
      Width = 121
      Height = 21
      TabOrder = 2
      Visible = False
    end
    object pgc1: TPageControl
      Left = 1
      Top = 1
      Width = 838
      Height = 249
      ActivePage = ts1
      Align = alClient
      TabOrder = 3
      OnChange = pgc1Change
      object ts1: TTabSheet
        Caption = 'Materiais do Pedido'
        object pnl4: TPanel
          Left = 0
          Top = 0
          Width = 830
          Height = 30
          Align = alTop
          BevelOuter = bvNone
          Color = clCream
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
          object btBtExcluirProjeto: TButton
            Left = 0
            Top = 1
            Width = 138
            Height = 29
            Hint = 'Devolve um projeto inteiro...'#13#10'Ex: Devolver uma Janela de Correr'
            Caption = 'Devolver &Projeto'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btBtExcluirProjetoClick
          end
          object btexcluir: TButton
            Left = 138
            Top = 1
            Width = 138
            Height = 29
            Hint = 'Devolve material selecionado'
            Caption = '&Devolver Material'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = btexcluirClick
          end
          object btAlterar: TButton
            Left = 276
            Top = 1
            Width = 138
            Height = 29
            Caption = 'Alterar &Cor do Material'
            TabOrder = 2
            OnClick = btAlterarClick
          end
          object btTrocarMaterial: TButton
            Left = 414
            Top = 1
            Width = 136
            Height = 29
            Caption = 'Trocar &Material'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            OnClick = btTrocarMaterialClick
          end
        end
        object dbgrid: TDBGrid
          Left = 0
          Top = 30
          Width = 830
          Height = 161
          TabStop = False
          Align = alClient
          Ctl3D = False
          DataSource = Dspesquisa
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = dbgridDrawColumnCell
          OnKeyPress = dbgridKeyPress
        end
        object pnl2: TPanel
          Left = 0
          Top = 191
          Width = 830
          Height = 30
          Align = alBottom
          TabOrder = 2
          Visible = False
          object lbInformacaoProcura: TLabel
            Left = 496
            Top = 5
            Width = 181
            Height = 16
            Caption = 'Voc'#234' esta procurando por'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object edtPesquisa: TEdit
            Left = 16
            Top = 4
            Width = 417
            Height = 19
            CharCase = ecUpperCase
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 0
            OnExit = edtPesquisaExit
            OnKeyPress = edtPesquisaKeyPress
          end
        end
      end
      object ts2: TTabSheet
        Caption = 'Materiais Trocados'
        ImageIndex = 1
        object StrGridMateriaisTrocados: TStringGrid
          Left = 0
          Top = 0
          Width = 830
          Height = 221
          Align = alClient
          Ctl3D = False
          FixedCols = 0
          Options = [goHorzLine]
          ParentCtl3D = False
          TabOrder = 0
        end
      end
      object ts3: TTabSheet
        Caption = 'Materiais Devolvidos'
        ImageIndex = 2
        object StrGridMateriaisDevolvidos: TStringGrid
          Left = 0
          Top = 0
          Width = 830
          Height = 221
          Align = alClient
          Ctl3D = False
          FixedCols = 0
          Options = [goHorzLine]
          ParentCtl3D = False
          TabOrder = 0
        end
      end
    end
  end
  object pnl3: TPanel
    Left = 0
    Top = 0
    Width = 840
    Height = 43
    Align = alTop
    Color = clCream
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object lbnomeformulario: TLabel
      Left = 221
      Top = 8
      Width = 396
      Height = 24
      Align = alCustom
      Alignment = taCenter
      Anchors = [akTop]
      Caption = 'Troca de Materiais de Pedidos Conclu'#237'dos'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -21
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
  end
  object Dspesquisa: TDataSource
    DataSet = QueryPesquisa
    Left = 624
    Top = 27
  end
  object QueryPesquisa: TIBQuery
    Left = 592
    Top = 28
  end
  object QueryCalculos: TIBQuery
    Left = 560
    Top = 36
  end
end
