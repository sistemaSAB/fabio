unit UErrosDesativarMateriais;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,IBQuery,UDataModulo,UpesquisaMenu;

type
  TFErrosDesativarMaterial = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    pnl3: TPanel;
    lbErro: TLabel;
    mmoErro: TMemo;
    ImgRodape: TImage;
    lbCancelar: TLabel;
    lbExcluir: TLabel;
    procedure lbExcluirMouseLeave(Sender: TObject);
    procedure lbExcluirMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbCancelarClick(Sender: TObject);
    procedure lbExcluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure mmoErroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
      CodigoMaterialGlobal:string;
      NomeMaterialGlobal:string;
      TipoMaterialGlobal:string;
  public
        Procedure PassaMaterial(CodigoMaterial:string;NomeMaterial:string;TipoMaterial:string);
  end;

var
  FErrosDesativarMaterial: TFErrosDesativarMaterial;

implementation

uses DateUtils, UescolheImagemBotao, UAjuda;

{$R *.dfm}


procedure TFErrosDesativarMaterial.PassaMaterial(codigoMaterial:string;NomeMaterial:string;TipoMaterial:string);
var
   Query:TIBQuery;
begin
    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;
    CodigoMaterialGlobal:=CodigoMaterial;
    NomeMaterialGlobal:=NomeMaterial;
    TipoMaterialGlobal:=TipoMaterial;
    try
        with query do
        begin
            lbErro.Caption:='Projetos Usando a(o) '+ TipoMaterial+' '+NomeMaterial;
           
            Close;
            SQL.Clear;
            SQL.Add('select Tabprojeto.descricao from tab'+TipoMaterial+'_proj');
            SQL.Add('join tabprojeto on tabprojeto.codigo= tab'+TipoMaterial+'_proj.projeto');
            sql.Add('where '+TipoMaterial+'='+codigoMaterial);
            Open;
            mmoErro.Lines.Clear;
            while not Eof do
            begin

                 mmoErro.Lines.Add(fieldbyname('descricao').AsString);
                 Next;
            end;


        end;
    finally
        FreeAndNil(Query);
    end;


end;

procedure TFErrosDesativarMaterial.lbExcluirMouseLeave(Sender: TObject);
begin
       TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFErrosDesativarMaterial.lbExcluirMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFErrosDesativarMaterial.lbCancelarClick(Sender: TObject);
begin
     self.Close;
end;

procedure TFErrosDesativarMaterial.lbExcluirClick(Sender: TObject);
var
  QueryDeleta:TIBQuery;
begin
      QueryDeleta:=TIBQuery.Create(nil);
      QueryDeleta.Database:=FDataModulo.IBDatabase;

      if (messagedlg('Certeza que deseja excluir o material dos projetos?',mtconfirmation,[mbyes,mbno],0)=mrno)
      then exit ;

      try
            With QueryDeleta do
            begin
                  close;
                  SQL.clear;
                  sql.add('delete from tab'+TipoMaterialGlobal+'_proj where '+TipoMaterialGlobal+'='+CodigoMaterialGlobal);
                  ExecSQL;
                  FDataModulo.IBTransaction.CommitRetaining;

            end;
      finally
            FreeAndNil(QueryDeleta);
            self.Close;

      end;

end;

procedure TFErrosDesativarMaterial.FormShow(Sender: TObject);
begin
      FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
end;

procedure TFErrosDesativarMaterial.mmoErroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
     if (Key = VK_F1) then
    begin

      try
        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(self);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('');
         FAjuda.ShowModal;
    end;
end;

end.
