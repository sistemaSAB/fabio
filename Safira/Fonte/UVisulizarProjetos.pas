unit UVisulizarProjetos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, StdCtrls,CodigoFigura, Menus,jpeg;

type
  TFVisulizarProjetos = class(TForm)
    panelFundo: TPanel;
    panelpnl2: TPanel;
    lb36: TLabel;
    ListBox1: TListBox;
    PaintBoxTela: TPaintBox;
    PopUpMenu: TPopupMenu;
    GerarDesenho1: TMenuItem;
    MostrarReferncias1: TMenuItem;
    NoMostrarReferncias1: TMenuItem;
    VisualizarMedidas1: TMenuItem;
    NoMostrarMostrarObjetosDesenho1: TMenuItem;
    img2: TImage;
    tmr1: TTimer;
    panel2: TPanel;
    lb1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure VisualizarMedidas1Click(Sender: TObject);
    procedure NoMostrarReferncias1Click(Sender: TObject);
    procedure MostrarReferncias1Click(Sender: TObject);
    procedure NoMostrarMostrarObjetosDesenho1Click(Sender: TObject);
    procedure GerarDesenho1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tmr1Timer(Sender: TObject);
   
  private
     //***Variaveis para desenhar os projetos
    ListaComponentes:TStringList;
    ListaItens:TStringList;
    pInic : TPoint; //indica o ponto onde o botao do mouse foi pressionado
    pFim : TPoint;   //indica o ponto onde o botao do mouse foi solto
    pCurr : TPoint;  //indica o ponto corrente
    botao: Boolean;    //indica que o botao do mouse foi pressionado no paintboxtela
    botaoArras : Boolean;  // Variavel que controla se o botao esta sendo arrastado
    objDesenha: TDesenho; //Objeto do tipo TDesenho
    listaFiguras: TList; //lista de figuras que estao no paintboxtela
    listaFerragens : array[0..200] of integer; //marca na posi��o da figura se � uma ferragem ou n�o
    figCorrente : TRect;  //Variavel auxiliar usada para criar o rascunho da nova fig.
    cor : TColor;    //seleciona a cor
    op : Integer;   //tipo da figura: retangulo, circulo, linha
    cont : Integer; //Conta quantas figuras estao inseridas na lista
    hab : Boolean; //Variavel que controla se as figuras estao habilitadas
    idFig : Integer; //Id da figura no banco de dados
    idCarr : Boolean; // Controla se o id da figura ja foi carregado
    ferrag : Boolean;    //indica se o form ferragens esta aberto
    largura : Integer;  // largura do objeto
    altura : Integer; //altura do objeto
    ctrlTag : Integer;  //guarda o self.tag de uma figura quando uma tecla de atalho � pressionado
    tmpColor : Tcolor; //variavel temporaria que guarda a cor (usada no TListBox)
    listBoxContr : Boolean; // varivel que controla a entrada no TlistBox
    polyclick : Boolean;   // marca que a op��o do polyline sera desenhado
    points:  array[0..200] of Tpoint;  //array de pontos que guardara as informa��es dos clicks
    indCurr : Integer; //indice que determina quantos pontos ja foram selecionados para criar o poligon
    Drawing : Boolean; //indica que o botao foi clicado e que o desenho selecionado foi o poligon
    entrad : Boolean; //indica que � a primeira entrada dos pontos do poligon
    menorx : integer; //menor x encontrado nos pontos dos poligonos
    menory : integer; // meno y encontrado nos pontos dos poligonos
    pux_banc : boolean; //verifica se uma figura foi carregada do banco
    CodigoProjeto:string;
    CodigoPedido:string;
    CodigoPedidoProjeto:string;

    procedure CarregaDesenhoProjeto(ComReferencias:Boolean;MostraLinhas:Boolean);
    procedure LimpaTudo();
    procedure CriarFigura(NomeFigura:string ; TipoMaterial:string ; CodigoMaterial:string);
    procedure DefineTamanhoPoligon();
    procedure Habilita(i : Integer);
    procedure DefineTamanhoFerragem();
    procedure AdicionaLista(NomeFigura:string; TipoMaterial:string ; CodigoMaterial:string);
    function  LocalizaTagCompAoCarregar(parametro:string):Integer;
    procedure DesabilitaHabilita(acao:Boolean);
    procedure Arruma ();
    procedure Preenchemedidas;
    Function  PedidoProjetoCodigo:string;
    function  RetornaOperadorMatematico(str:string):string;
  public
    procedure PassaObjetos(Projeto:string;Pedido:string;pedidoprojeto:string);
  end;

var
  FVisulizarProjetos: TFVisulizarProjetos;

implementation

uses UDataModulo,IBQuery,UessencialGlobal, UobjParametros;

{$R *.dfm}

procedure Split (const Delimiter: Char; Input: string; const Strings: TStrings);
begin
   Assert(Assigned(Strings));
   Strings.Clear;
   Strings.Delimiter := Delimiter;
   Strings.DelimitedText := Input;
end;

procedure TFVisulizarProjetos.PassaObjetos(Projeto:string;Pedido:string;pedidoprojeto:string);
begin
    CodigoProjeto:=projeto;
    CodigoPedido:=pedido;
    CodigoPedidoProjeto:=pedidoprojeto;
end;


procedure TFVisulizarProjetos.CarregaDesenhoProjeto(ComReferencias:Boolean;MostraLinhas:Boolean);
var query: TIBQuery;  //declara��o da query
    enviar : string;
    busc : string;
    str : string;
    retorno : TstringList;
    k : integer;
    Indice:Integer;
begin

    LimpaTudo();  //limpa todos os objetos e toda a listbox

    query := TIBQuery.Create(nil);
    query.Database :=FDataModulo.IBDatabase;

    ListaComponentes.Clear;
    ListaItens.Clear;
    listaFiguras.Clear;
    //busca o id da figura pelo nome clicado no StringGrid
    with query do
    begin
          close;
          sql.Clear;
          //enviar := 'select id from tabprojetos_id where projeto = '+lbCodigoProjeto.Caption;

          sql.Add('select id from tabprojetos_id where projeto = '+CodigoProjeto);
          open;
          Last;
          if(recordcount=0)then
          begin
              idCarr:=False;
              Exit;
          end;
          busc    := fieldbyname('id').AsString;
          idCarr  := True;  //Indica que uma figura ja foi carregada
          idFig   := strtoint(busc);    //Indica o numero da figura que foi carregada
          close;
    end;

    //Busca todos os objetos da figura
    with query do
    begin
        close;
        sql.Clear;
        sql.Add('select * from tabprojetospontos_id where projeto_id = '#39+busc+#39 );
        open;
        First;
        cont := 0; //Digo que na tela nao tem nenhum figura
        while not (query.eof) do
        begin
             if(MostraLinhas=True) then
             begin
                 op := FieldByName('tipo').AsInteger;  //tipo do objeto
                 cor := StringToColor (FieldByName('cor').AsString);
                 pInic.X := FieldByName('xsup').AsInteger;  //xsuperior  do objeto
                 pInic.Y := FieldByName('ysup').AsInteger;  //ysuperior do objeto

                 if(op <> 3)then
                 begin
                   pFim.X := FieldByName('xinf').AsInteger;   //xinferior do objeto
                   pFim.Y := FieldByName('yinf').AsInteger;   //yinferior do objeto
                 end
                 else
                 begin
                      retorno := TStringList.Create;
                      str := FieldByName('pts').AsString;
                      Split(';', str, retorno);
                      k := 0;
                      while (k < (retorno.count-1)/2) do
                      begin
                          points[k].X := strtoint(retorno[k*2]);
                          points[k].Y := strtoint(retorno[(k*2)+1]);
                          k := k+1;
                      end;
                      indCurr := round((retorno.count-1)/2);
                      pux_banc := true;
                 end;
                 criarFigura(fieldbyname('nome').asstring,fieldbyname('tipomaterial').asstring,fieldbyname('codigomaterial').asstring);
                 pux_banc := false;
                 cor := clbtnface;
             end
             else
             begin
                if(FieldByName('tipo').AsInteger<>21)then
                begin
                   op := FieldByName('tipo').AsInteger;  //tipo do objeto
                   cor := StringToColor (FieldByName('cor').AsString);
                   pInic.X := FieldByName('xsup').AsInteger;  //xsuperior  do objeto
                   pInic.Y := FieldByName('ysup').AsInteger;  //ysuperior do objeto

                   if(op <> 3)then
                   begin
                     pFim.X := FieldByName('xinf').AsInteger;   //xinferior do objeto
                     pFim.Y := FieldByName('yinf').AsInteger;   //yinferior do objeto
                   end
                   else
                   begin
                        retorno := TStringList.Create;
                        str := FieldByName('pts').AsString;
                        Split(';', str, retorno);
                        k := 0;
                        while (k < (retorno.count-1)/2) do
                        begin
                            points[k].X := strtoint(retorno[k*2]);
                            points[k].Y := strtoint(retorno[(k*2)+1]);
                            k := k+1;
                        end;
                        indCurr := round((retorno.count-1)/2);
                        pux_banc := true;
                   end;
                   criarFigura(fieldbyname('nome').asstring,fieldbyname('tipomaterial').asstring,fieldbyname('codigomaterial').asstring);
                   pux_banc := false;
                   cor := clbtnface;
                end;
             end;
             if(ComReferencias=true)then
             begin
                 //Se � um componente preciso apenas passar sua referencia pra listanomesmedidas do obj
                 //preciso inseri-lo na lista de componentes (listacomponentes)
                 if(FieldByName('tipomaterial').AsString='TABCOMPONENTE') then
                 begin
                    ListaComponentes.Add(IntToStr(objDesenha.ContFig)+';'+fieldbyname('nome').asstring);
                    objDesenha.ListaNomesMedidas.Add(FieldByName('referencia').AsString)
                 end;
                 //Se for uma ferragem preciso passar para a lista listanomemedidas do componente a referencia da ferragem
                 //posi��o (x,y) da mesma dentro do componente
                 //Preciso inseri-la na lista de itens (listaitens)
                 if(FieldByName('tipomaterial').AsString='TABFERRAGEM') then
                 begin
                     //Localizo em qual componente essa ferragem foi inserida
                     if(fieldbyname('ligadoaocomponente').asstring<>'')then
                     begin
                         indice:=LocalizaTagCompAoCarregar(fieldbyname('ligadoaocomponente').asstring);
                         //Caso o indice seja -1 ent�o a mesma n�o esta ligada em nenhum componente
                         if(Indice<>-1) then
                         begin
                            //Somente se o valor em referencia, q para ferragens � (text;x;y) estiver com algo
                            if(FieldByName('referencia').AsString<>'') then
                            begin
                              TDesenho(listaFiguras[Indice]).ListaNomesMedidas.Add(FieldByName('referencia').AsString+';'+inttostr(objDesenha.ContFig));
                              ListaItens.Add(IntToStr(objDesenha.ContFig)+';'+IntToStr(Indice));
                            end;
                         end;
                     end;
                 end;
             end;
             query.Next;
        end;

        close;

        DesabilitaHabilita(False);
    end;

end;

procedure TFVisulizarProjetos.DesabilitaHabilita(acao:Boolean);
var
  i:Integer;
  NomeRestante2,nome2:string;
begin
  //HABILITA
  if(acao=True) then
  begin
    for i:=0 to ListBox1.Count-1 do
    begin
      NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox1.Items[i],' ');
      nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
      TDesenho(listaFiguras[StrToInt(nome2)]).Enabled:=true;
      TDesenho(listaFiguras[StrToInt(nome2)]).Botao := true;
    end;
    PaintBoxTela.Enabled:=True;
    ListBox1.Enabled:=True;
  end
  else
  begin  //DESABILITA
    for i:=0 to ListBox1.Count-1 do
    begin
      NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox1.Items[i],' ');
      nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
      TDesenho(listaFiguras[StrToInt(nome2)]).Enabled:=False;
      TDesenho(listaFiguras[StrToInt(nome2)]).Botao := False ;
    end;
    PaintBoxTela.Enabled:=False;
    ListBox1.Enabled:=False;
  end;
end;


function TFVisulizarProjetos.LocalizaTagCompAoCarregar(parametro:string):Integer;
var
  Query:TIBQuery;
  i:Integer;
  Nome,NomeRestante:string;
  Nome2,NomeRestante2:string;
begin
    //caso n�o encontre retorna -1
    Result:=-1;

    query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;

    try
      with Query do
      begin
          Close;
          SQL.Clear;
          SQL.Add('select nome from tabprojetospontos_id where id='+parametro);
          Open;
          //percorro a lista de obj procurando pelo nome do componente
          for i:=0 to ListBox1.Count-1 do
          begin
              //Pego o nome do componente
              NomeRestante:=retornaPalavrasDepoisSimbolo(ListBox1.Items[i],'-');
              nome:=retornaPalavrasAntesSimbolo(NomeRestante,'-');
              //Procuro pelo componente na lista de componentes
              if(Nome=FieldByName('nome').AsString) then
              begin
                   //Pego a tag do obj, por exemplo (obj 1), tag=1
                   NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox1.Items[i],' ');
                   nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');

                   Result:=StrToInt(Nome2);
              end;
          end;
      end;
    finally
      FreeAndNil(Query);
    end;

end;

function TFVisulizarProjetos.PedidoProjetoCodigo:string;
var
  Query:TIBQuery;
begin
    { query:=TIBQuery.Create(nil);
     Query.Database:=FDataModulo.IBDatabase;
     result:='-1';
     try
       with Query do
       begin
         Close;
         SQL.Clear;
         SQL.Add('select * from tabpedido_proj where pedido='+CodigoPedido);
         SQL.Add('and projeto='+CodigoProjeto);
         Open;

         Result:=fieldbyname('codigo').asstring;

       end;
     finally
       FreeAndNil(Query);
     end;   }

end;


procedure TFVisulizarProjetos.FormShow(Sender: TObject);
begin
    op := 0;
    cont := 0;
    cor := clBtnFace;
    idCarr := False; // indica que nenhuma figura foi carregada na tela
    hab := True; // Nesta inicializa��o decidi-se se a figura ira come�ar marcada ou nao
    Self.Tag := -1;
    ctrlTag := -1;
    polyclick := false;
    indCurr := 0;
    Entrad := true;
    listaFiguras := TList.Create;
    ListaComponentes:=TStringList.Create;
    ListaItens:=TStringList.Create;
    CarregaDesenhoProjeto(true,True);

    
end;

procedure TFVisulizarProjetos.LimpaTudo();
var i : integer;
    str : string;
    parte : TStringList;
begin
  try
     // exclui todos os objetos da tela
     for i := cont-1 downto 0 do
     begin
      parte := TStringList.Create;
      str := ListBox1.Items[i];

      try
        Split(' ', str, parte);
        TDesenho (listaFiguras[strtoint(parte[1])]).Free;
      finally
        parte.Free;
      end;
     end;
     listaFiguras.Clear;
     //Limpa o listbox onde mostra cada figura
     for i:= ListBox1.Count - 1 downto 0 do
      ListBox1.Items.Delete(i);
     ListBox1.Clear;
     cont := 0;
     Refresh;
  finally

  end;
end;

procedure TFVisulizarProjetos.Arruma ();
var
  tmp: Integer;
begin
  //Verifico se o ponto y final � menor que ponto y inicial
  if (PFim.Y < PInic.Y) then
  begin
        tmp := PFim.Y;
        PFim.Y := PInic.Y;
        PInic.Y := tmp;
  end;
  //Verifico se o ponto x final � menor que ponto x inicial
  if (PFim.X < PInic.X) then
  begin
        tmp := PFim.X;
        PFim.X := PInic.X;
        PInic.X := tmp;
  end;
end;


procedure TFVisulizarProjetos.CriarFigura(NomeFigura:string ; TipoMaterial:string; CodigoMaterial:string);
begin
  if(op <> 3) then
  begin
        Arruma();
        //To mandando vazio o nome do componente, pq na hora de salvar novamente zua tudo
        objDesenha := TDesenho.Create(PaintBoxTela,cor, op, listaFiguras.Count,pinic,pfim, points, indCurr-1,NomeFigura);

  end
  else
  objDesenha := TDesenho.Create(PaintBoxTela,cor, op, listaFiguras.Count,pinic,pfim, points, indCurr-1,'');

  objDesenha.PassaForm(Self);
  objDesenha.Parent := panelFundo;

  //SetBounds (X da tela, Y da tela, Largura do obj, Altura do Obj)
  //objDesenha.SetBounds (arraypoints[0].x, arraypoints[0].y, pfim.x, pfim.y);
  if (op < 3) then
    objDesenha.SetBounds (pInic.X, pInic.Y, pfim.x-pinic.x, pfim.y-pinic.y)
  else
  if(op = 3) then
  begin
       DefineTamanhoPoligon();
       if(pux_banc = false)then
       objDesenha.SetBounds (menorX, menorY, largura, altura)
       else   objDesenha.SetBounds (pinic.X, pinic.Y, largura, altura) ;
  end
  else
  begin
      if (op=4) or (op>=22) then
          objDesenha.SetBounds (pInic.X, pInic.Y, pfim.x-pinic.x, pfim.y-pinic.y)
      else
      if(op=21)
      then objDesenha.SetBounds (pInic.X, pInic.Y, pfim.x-pinic.x, pfim.y-pinic.y)
      else
      begin
          begin
             DefineTamanhoFerragem();
             objDesenha.SetBounds (pInic.X, pInic.Y, largura*2, altura*2);
          end;
      end;
  end;

  listaFiguras.Add(objDesenha);

  Habilita(objDesenha.ContFig);

  cont := cont + 1;
  AdicionaLista(NomeFigura,TipoMaterial,CodigoMaterial);

end;

procedure TFVisulizarProjetos.AdicionaLista(NomeFigura:string; TipoMaterial:string ; CodigoMaterial:string);
begin
    if(NomeFigura<>'')
    then ListBox1.Items.Add('Obj '+inttostr(objDesenha.ContFig)+' - '+NomeFigura+' - '+TipoMaterial+' - '+CodigoMaterial)
    else ListBox1.Items.Add('Obj '+inttostr(objDesenha.ContFig)+' -');



end;

procedure TFVisulizarProjetos.Habilita(i : Integer);
begin
  if(hab = True)then
  begin
    TDesenho(listaFiguras[i]).Botao := True;  //indica que a figura esta habilitada para edi��o
    //TDesenho(listaFiguras[i]).BringToFront;
  end
  else
  begin
    TDesenho(listaFiguras[i]).Botao := False;  //indica que a figura esta desabilitada para edi��o
    //TDesenho(listaFiguras[i]).SendToBack;
  end;
end;

procedure TFVisulizarProjetos.DefineTamanhoPoligon();
var
    i: integer;
    maiorx : integer;
    maiory : integer;
begin
    menorx := points[0].x;
    menory := points[0].y;
    maiorx := points[0].x;
    maiory := points[0].y;
    for i := 1 to indCurr-1 do
    begin
      if(points[i].x < menorx)then
          menorx := points[i].x;
      if(points[i].y < menory)then
          menory := points[i].y;
      if(points[i].x > maiorx)then
          maiorx := points[i].x;
      if(points[i].y > maiory)then
          maiory := points[i].y;
    end;
    //a largura do poligono sera definida pela diferen�a entre o maior e o menor ponto x
    largura := maiorx-menorx+6;
    //a altura do poligono sera definida pela diferen�a entre o maior e o menor ponto y
    altura := maiory-menory+6;
end;

procedure TFVisulizarProjetos.DefineTamanhoFerragem;
begin
  case op of
    4:
    begin
      largura := 8;
      altura := 5;
    end;

    5: begin
      largura := 10;
      altura := 7;
    end;

    6:
    begin
      largura := 12;
      altura := 9;
    end;

    7:
    begin
      largura := 12;
      altura := 9;
    end;

    8: begin
      largura := 8;
      altura := 11;
    end;

    9:
    begin
      largura := 8;
      altura := 11;
    end;

    10:
    begin
      largura := 8;
      altura := 11;
    end;

    11:
    begin
      largura := 8;
      altura := 11;
    end;

    12:
    begin
      largura := 8;
      altura := 9;
    end;

    13:
    begin
      largura := 8;
      altura := 5;
    end;

    14:
    begin
      largura := 4;
      altura := 10;
    end;

    15:
    begin
      largura := 10;
      altura := 10;
    end;

  end;
end;


procedure TFVisulizarProjetos.Preenchemedidas;
var
  i:Integer;
  NomeRestante2,nome2,CodigoMaterial,tipo:string;
  query:TIBQuery;
  alturalargura,texto,operador,texto2:string;
  Resultado:Currency;
  lf : TLogFont;
  tf : TFont;
  folgaLargura,FolgaAltura:Currency;
begin

{TODO:Preenchemedidas}

    PaintBoxTela.Font.Color:=clBlack;
    PaintBoxTela.Font.Size:=12; //6

    query:=TIBQuery.Create(nil);
    query.Database:=FDataModulo.IBDatabase;

    try

        for i:=0 to ListBox1.Count-1 do
        begin

           NomeRestante2  := retornaPalavrasDepoisSimbolo2(ListBox1.Items[i],' ');
           nome2          := retornaPalavrasAntesSimbolo(NomeRestante2,' ');

           if(TDesenho(listaFiguras[StrToInt(nome2)]).TipoFigura=21)then
           begin

              if((TDesenho(listaFiguras[StrToInt(nome2)]).pt2.x-TDesenho(listaFiguras[StrToInt(nome2)]).pt1.x)>1)then
              begin

                 NomeRestante2  := retornaPalavrasDepoisSimbolo(ListBox1.Items[i],'-');
                 tipo           := retornaPalavrasAntesSimbolo(NomeRestante2,'-');
                 NomeRestante2  := retornaPalavrasDepoisSimbolo(NomeRestante2,'-');
                 NomeRestante2  := retornaPalavrasDepoisSimbolo(NomeRestante2,'-');
                 CodigoMaterial := retornaPalavrasAntesSimbolo(NomeRestante2,'-');

                 if(tipo='LinhaMedida')then
                 begin
                     folgaLargura:=0;
                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select cp.folgalargura from tabcomponente_proj cp where projeto='+CodigoProjeto);
                     query.SQL.Add('and componente='+CodigoMaterial);
                     query.Open;
                     folgaLargura:=query.fieldbyname('folgalargura').AsCurrency;


                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select largura from tabcomponente_pp where componente='+CodigoMaterial);
                     query.SQL.Add('and pedidoprojeto='+CodigoPedidoProjeto);
                     query.Open;

                     if(query.fieldbyname('largura').AsString='')then
                     begin
                       query.Close;
                       query.SQL.Clear;
                       query.SQL.Add('select largura from tabcomponente_pp where componente='+CodigoMaterial);
                       query.SQL.Add('and pedidoprojeto='+get_campoTabela('PEDIDOPROJETOPRINCIPAL','codigo','tabpedido_proj',CodigoPedidoProjeto));
                       query.Open;
                     end;

                     PaintBoxTela.Canvas.Font.Size:=12;//7
                     GetObject(PaintBoxTela.Canvas.Font.Handle, SizeOf(TLogFont), @lf);
                     lf.lfHeight := PaintBoxTela.Canvas.Font.Height;
                     lf.lfEscapement:=0*10;
                     lf.lfQuality := ANTIALIASED_QUALITY;//PROOF_QUALITY;
                     PaintBoxTela.Canvas.Font.Handle := CreateFontIndirect(lf);

                     PaintBoxTela.Canvas.TextOut(TDesenho(listaFiguras[StrToInt(nome2)]).pt1.x,TDesenho(listaFiguras[StrToInt(nome2)]).pt2.y+10,CurrToStr(query.fieldbyname('largura').AsCurrency-folgaLargura)+' mm.');
                 end;

                 if(tipo='LinhaMedidaPuxador')then
                 begin
                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select largura from tabcomponente_pp where componente='+CodigoMaterial);
                     query.SQL.Add('and pedidoprojeto='+CodigoPedidoProjeto);
                     query.Open;
                     alturalargura:=query.fieldbyname('largura').asstring;

                     if(alturalargura='') then
                     begin
                       query.Close;
                       query.SQL.Clear;
                       query.SQL.Add('select largura from tabcomponente_pp where componente='+CodigoMaterial);
                       query.SQL.Add('and pedidoprojeto='+get_campoTabela('PEDIDOPROJETOPRINCIPAL','codigo','tabpedido_proj',CodigoPedidoProjeto));
                       query.Open;
                       alturalargura:=query.fieldbyname('largura').asstring;
                     end;

                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select tabcortecomponente_proj.* from tabcortecomponente_proj');
                     query.SQL.Add('join tabcomponente_proj on tabcomponente_proj.codigo=tabcortecomponente_proj.componente_proj');
                     query.SQL.Add('where tabcomponente_proj.componente='+CodigoMaterial);
                     query.SQL.Add('and tabcomponente_proj.projeto='+CodigoProjeto);
                     query.SQL.Add('and tabcortecomponente_proj.puxador=''S'' ');
                     query.Open;

                     PaintBoxTela.Canvas.Font.Size:=12;//7
                     GetObject(PaintBoxTela.Canvas.Font.Handle, SizeOf(TLogFont), @lf);
                     lf.lfHeight := PaintBoxTela.Canvas.Font.Height;
                     lf.lfEscapement:=0*10;
                     lf.lfQuality := ANTIALIASED_QUALITY;//PROOF_QUALITY;
                     PaintBoxTela.Canvas.Font.Handle := CreateFontIndirect(lf);

                     texto:= StrReplace(query.fieldbyname('formulaposicaocorte').AsString,'LARGURA',alturalargura);

                     PaintBoxTela.Canvas.TextOut(TDesenho(listaFiguras[StrToInt(nome2)]).pt1.x,TDesenho(listaFiguras[StrToInt(nome2)]).pt2.y+10,texto+'mm');
                 end;

              end;

              if((TDesenho(listaFiguras[StrToInt(nome2)]).pt2.y-TDesenho(listaFiguras[StrToInt(nome2)]).pt1.y)>1)then
              begin
                 NomeRestante2:=retornaPalavrasDepoisSimbolo(ListBox1.Items[i],'-');
                 tipo:=retornaPalavrasAntesSimbolo(NomeRestante2,'-');
                 NomeRestante2:=retornaPalavrasDepoisSimbolo(NomeRestante2,'-');
                 NomeRestante2:=retornaPalavrasDepoisSimbolo(NomeRestante2,'-');
                 CodigoMaterial:= retornaPalavrasAntesSimbolo(NomeRestante2,'-');
                 if(tipo='LinhaMedida')then
                 begin
                     FolgaAltura:=0;
                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select cp.folgaaltura from tabcomponente_proj cp where projeto='+CodigoProjeto);
                     query.SQL.Add('and componente='+CodigoMaterial);
                     query.Open;

                     FolgaAltura:=query.fieldbyname('folgaaltura').AsCurrency;

                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select altura from tabcomponente_pp where componente='+CodigoMaterial);
                     query.SQL.Add('and pedidoprojeto='+CodigoPedidoProjeto);
                     query.Open;

                     if(query.fieldbyname('altura').AsString='')then
                     begin
                       query.Close;
                       query.SQL.Clear;
                       query.SQL.Add('select altura from tabcomponente_pp where componente='+CodigoMaterial);
                       query.SQL.Add('and pedidoprojeto='+get_campoTabela('PEDIDOPROJETOPRINCIPAL','codigo','tabpedido_proj',CodigoPedidoProjeto));
                       query.Open;
                     end;

                     PaintBoxTela.Canvas.Font.Size:=12; //8
                     GetObject(PaintBoxTela.Canvas.Font.Handle, SizeOf(TLogFont), @lf);
                     lf.lfHeight := PaintBoxTela.Canvas.Font.Height;
                     lf.lfEscapement:=90*10;
                     lf.lfQuality := ANTIALIASED_QUALITY;//PROOF_QUALITY;
                     PaintBoxTela.Canvas.Font.Handle := CreateFontIndirect(lf);

                     PaintBoxTela.Canvas.TextOut(TDesenho(listaFiguras[StrToInt(nome2)]).pt1.x,TDesenho(listaFiguras[StrToInt(nome2)]).pt2.y-50,CurrToStr(query.fieldbyname('altura').AsCurrency-FolgaAltura)+' mm.');
                 end;
                 if(tipo='LinhaMedidaPuxador')then
                 begin
                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select altura from tabcomponente_pp where componente='+CodigoMaterial);
                     query.SQL.Add('and pedidoprojeto='+CodigoPedidoProjeto);
                     query.Open;

                     alturalargura:=query.fieldbyname('altura').asstring;

                     if(alturalargura='') then
                     begin
                       query.Close;
                       query.SQL.Clear;
                       query.SQL.Add('select altura from tabcomponente_pp where componente='+CodigoMaterial);
                       query.SQL.Add('and pedidoprojeto='+get_campoTabela('PEDIDOPROJETOPRINCIPAL','codigo','tabpedido_proj',CodigoPedidoProjeto));
                       query.Open;
                       alturalargura:=query.fieldbyname('altura').asstring;
                     end;

                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select tabcortecomponente_proj.* from tabcortecomponente_proj');
                     query.SQL.Add('join tabcomponente_proj on tabcomponente_proj.codigo=tabcortecomponente_proj.componente_proj');
                     query.SQL.Add('where tabcomponente_proj.componente='+CodigoMaterial);
                     query.SQL.Add('and tabcomponente_proj.projeto='+CodigoProjeto);
                     query.SQL.Add('and tabcortecomponente_proj.puxador=''S'' ');
                     query.Open;


                     texto:= StrReplace(query.fieldbyname('formulaalturacorte').AsString,'ALTURA',alturalargura);

                     operador:=RetornaOperadorMatematico(query.fieldbyname('formulaalturacorte').AsString);
                     texto2:=retornaPalavrasAntesSimbolo(texto,operador);
                     texto:=retornaPalavrasDepoisSimbolo2(texto,operador);

                     if(operador='/')
                     then Resultado:=StrToCurr(texto2)/StrToCurr(texto);

                     PaintBoxTela.Canvas.Font.Size:=12;//8
                     GetObject(PaintBoxTela.Canvas.Font.Handle, SizeOf(TLogFont), @lf);
                     lf.lfHeight := PaintBoxTela.Canvas.Font.Height;
                     lf.lfEscapement:=90*10;
                     lf.lfQuality := ANTIALIASED_QUALITY;//PROOF_QUALITY;
                     PaintBoxTela.Canvas.Font.Handle := CreateFontIndirect(lf);

                     PaintBoxTela.Canvas.TextOut(TDesenho(listaFiguras[StrToInt(nome2)]).pt1.x,TDesenho(listaFiguras[StrToInt(nome2)]).pt2.y-50,CurrToStr(Resultado)+' mm.');

                 end;
              end;
           end;
        end;
    finally
        FreeAndNil(query);
    end;


   { PaintBoxTela.Font.Color:=clBlack;
    PaintBoxTela.Font.Size:=6;
    for i:=0 to ListaComponentes.Count-1 do
    begin

    end;    }


end;

procedure TFVisulizarProjetos.VisualizarMedidas1Click(Sender: TObject);
begin
   Preenchemedidas;
   DesabilitaHabilita(False);
   Repaint;
end;

procedure TFVisulizarProjetos.NoMostrarReferncias1Click(Sender: TObject);
begin
   CarregaDesenhoProjeto(False,False);
   PaintBoxTela.Repaint
end;

procedure TFVisulizarProjetos.MostrarReferncias1Click(Sender: TObject);
begin
   CarregaDesenhoProjeto(True,True);
   PaintBoxTela.Repaint;
end;

procedure TFVisulizarProjetos.NoMostrarMostrarObjetosDesenho1Click(
  Sender: TObject);
begin
     if(panelpnl2.Visible=false)
     then panelpnl2.Visible:=True
     else panelpnl2.Visible:=False;
end;

function TFVisulizarProjetos.RetornaOperadorMatematico(str:string):string;
var
  i,j:Integer;
  aux:string ;
begin
      Result:='';
      aux:='';
      i:=0;
      if(Str='')
      then Exit;
      while (i <= Length (str)) do
      begin
          if(Str[i]='/') or (Str[i]='+') or (Str[i]='-') or (Str[i]='*')
          then aux:=Str[i];

          Inc(i,1);
      end;
      Result:=aux;

end;

procedure TFVisulizarProjetos.GerarDesenho1Click(Sender: TObject);
var
  IMGJpg:TJPEGImage;
begin
  try
    IMGJpg:=TJPEGImage.Create;
  except
    Exit;
  end;
  try
    if (ObjParametroGlobal.ValidaParametro('CAMINHO DOS DESENHOS PARA PEDIDO DE COMPRA')=false)then
    Begin
        MensagemErro('O par�metro "CAMINHO DOS DESENHOS PARA PEDIDO DE COMPRA" n�o foi encontrado.');
        exit;
    end;
    if DirectoryExists(ObjParametroGlobal.Get_Valor) then
    begin
      img2.Canvas.CopyRect(img2.Canvas.ClipRect,PaintBoxTela.Canvas,PaintBoxTela.Canvas.ClipRect);
      IMGJpg.Assign(img2.Picture.Graphic);
      IMGJpg.SaveToFile(ObjParametroGlobal.Get_Valor+CodigoPedidoProjeto+'_compra.jpg');
    end
    else
    begin
      tmr1.Enabled := false;
      MensagemErro('O caminho dos desenhos no par�metro "CAMINHO DOS DESENHOS PARA PEDIDO DE COMPRA" � inv�lido. Configure e retorne novamente.');
      Exit;
    end;
  finally
    FreeAndNil(IMGJpg);
  end;

end;

procedure TFVisulizarProjetos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    LimpaTudo;
    ListaComponentes.Free;
    ListaItens.Free;

end;

procedure TFVisulizarProjetos.tmr1Timer(Sender: TObject);
begin
  VisualizarMedidas1Click(Sender);
  VisualizarMedidas1Click(Sender);
  GerarDesenho1Click(Sender);
  self.Close;
end;

end.
