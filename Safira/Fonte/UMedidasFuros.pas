unit UMedidasFuros;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask,UessencialGlobal,UDataModulo, IBQuery, ExtCtrls,
  jpeg, Buttons,UobjCORTECOMPONENTE_PROJ,Upesquisa,DB, Grids, DBGrids;

type
  TFmedidasFuros = class(TForm)
    pnl1: TPanel;
    lb5: TLabel;
    pnl2: TPanel;
    pnlAjuda: TPanel;
    mmo1: TMemo;
    pnl3: TPanel;
    lb1: TLabel;
    lb2: TLabel;
    lb6: TLabel;
    edtOperadoraAltura: TMaskEdit;
    edtValorAltura: TMaskEdit;
    edtOperadorLargura: TMaskEdit;
    edtValorLargura: TMaskEdit;
    edtTamanhoFuro: TMaskEdit;
    lbNomeFerragem: TLabel;
    bt1: TSpeedButton;
    edtAlturaOP: TMaskEdit;
    edtLarguraOP: TMaskEdit;
    lbNomeComponente: TLabel;
    edtFerragemProj: TMaskEdit;
    lb3: TLabel;
    dbgrid2: TDBGrid;
    pnl4: TPanel;
    lb13: TLabel;
    lb15: TLabel;
    lb10: TLabel;
    btGrava: TBitBtn;
    btExclui: TBitBtn;
    btCancela: TBitBtn;
    edtdesconto_reais_pedido: TEdit;
    edtdesconto_porcento_pedido: TEdit;
    COMBOMateriais: TComboBox;
    lb4: TLabel;
    lb7: TLabel;
    lb8: TLabel;
    chkPuxador: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure edtOperadoraAlturaKeyPress(Sender: TObject; var Key: Char);
    procedure edtValorAlturaKeyPress(Sender: TObject; var Key: Char);
    procedure edtValorLarguraKeyPress(Sender: TObject; var Key: Char);
    procedure edtUnidadeMedidaAlturaExit(Sender: TObject);
    procedure edtOperadoraAlturaExit(Sender: TObject);
    procedure edtOperadorLarguraExit(Sender: TObject);
    procedure lb7Click(Sender: TObject);
    procedure bt1Click(Sender: TObject);
    procedure edtUnidadeMedidaLarguraExit(Sender: TObject);
    procedure lb7MouseLeave(Sender: TObject);
    procedure lb7MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtFerragemProjKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtFerragemProjExit(Sender: TObject);
    procedure edtAlturaOPExit(Sender: TObject);
    procedure edtLarguraOPExit(Sender: TObject);
    procedure edtAlturaOPKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure dbgrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btGravaClick(Sender: TObject);
    procedure btCancelaClick(Sender: TObject);
    procedure dbgrid2DblClick(Sender: TObject);
    procedure btExcluiClick(Sender: TObject);
    procedure chkPuxadorClick(Sender: TObject);

  private
        TabelaMaterialGlobal:string;
        CodigoMaterialGlobal:string;
        ObjCorteComponente_Proj:TObjCORTECOMPONENTE_PROJ;
        Projeto:string;
        StatusInsercao:Boolean;
        procedure CarregaMedidas;
        function  RetornaOperadorMatematico(Str:string):string;
        function  RetornaValor(Str:string):string;
        function  RetornaUnidadeMedida(Str:string):string;
        function  RetornaPalavrasAntesSimboloLocal(str: string): string;



  public
        AlturaCorte:string;
        PosicaoCorte:string;
        TamanhoCorte:String;
        procedure PassaMaterial(Tabelamaterial:string;CodigoMaterial:string;NomeMaterial:string;CPROJETO:string);


  end;

var
  FmedidasFuros: TFmedidasFuros;

implementation

uses Math, UescolheImagemBotao;

{$R *.dfm}

procedure TFmedidasFuros.FormShow(Sender: TObject);
begin

    ObjCorteComponente_Proj:=TObjCORTECOMPONENTE_PROJ.Create;
    lbNomeFerragem.Caption:='';

    Self.Tag:=0;

    edtFerragemProj.Text:='';
    edtOperadoraAltura.text:='';
    edtOperadorLargura.Text:='';
    edtValorLargura.Text:='';
    edtValorAltura.text:='';
    edtTamanhoFuro.text:='';
    pnlAjuda.Visible:=False;
    edtAlturaOP.Text:='';
    edtLarguraOP.Text:='';


    {edtOperadoraAltura.Enabled:=False;
    edtOperadorLargura.Enabled:=False;
    edtValorLargura.Enabled:=False;
    edtValorAltura.Enabled:=False;
    edtUnidadeMedidaAltura.Enabled:=False;
    edtUnidadeMedidaLargura.Enabled:=False;
    edtTamanhoFuro.Enabled:=False;
    edtUnidadeMedidaAltura.Enabled:=False;
    edtUnidadeMedidaLargura.Enabled:=False;
    edtAlturaOP.Enabled:=False;
    edtLarguraOP.Enabled:=False; }


    ObjCorteComponente_Proj.ResgataCortes(CodigoMaterialGlobal);
    dbgrid2.DataSource:=Self.ObjCorteComponente_Proj.ObjDatasource;

    FescolheImagemBotao.PegaFiguraBotaopequeno(btGrava,'BOTAOINSERIR.BMP');
    FescolheImagemBotao.PegaFiguraBotaopequeno(BtCancela,'BOTAOCANCELARPRODUTO.BMP');
    FescolheImagemBotao.PegaFiguraBotaopequeno(BtExclui,'BOTAORETIRAR.BMP');


end;

procedure TFmedidasFuros.btCancelarClick(Sender: TObject);
begin
      lbNomeFerragem.Caption:='';
      edtFerragemProj.Text:='';
      edtOperadoraAltura.text:='';
      edtOperadorLargura.Text:='';
      edtValorLargura.Text:='';
      edtValorAltura.text:='';
      edtTamanhoFuro.text:='';
      pnlAjuda.Visible:=False;
      edtAlturaOP.Text:='';
      edtLarguraOP.Text:='';
end;

procedure TFmedidasFuros.edtOperadoraAlturaKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key in['/','*','-','+',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFmedidasFuros.edtValorAlturaKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (Key in['0'..'9',Chr(8)]) then
     begin
            Key:= #0;
     end;
end;

procedure TFmedidasFuros.edtValorLarguraKeyPress(Sender: TObject;
  var Key: Char);
begin
         if not (Key in['0'..'9',Chr(8)]) then
     begin
            Key:= #0;
     end;
end;

procedure TFmedidasFuros.edtUnidadeMedidaAlturaExit(Sender: TObject);
begin
        {if(edtUnidadeMedidaAltura.Text<>'mm') and(edtUnidadeMedidaAltura.Text<>'cm') and (edtUnidadeMedidaAltura.Text<>'m') then
        begin
              MensagemErro('Unidade de medida invalida / Informar em milimetros(mm) centimetros(cm) metros(m) ');
              edtUnidadeMedidaAltura.Text:='';

        end;}
end;

procedure TFmedidasFuros.PassaMaterial(Tabelamaterial:string;CodigoMaterial:string;NomeMaterial:string;CProjeto:string);
begin
    TabelaMaterialGlobal:=Tabelamaterial;
    CodigoMaterialGlobal:=CodigoMaterial;
    lbNomeComponente.Caption:='Informa��es de corte no vidro do Componente "'+NomeMaterial+'"';
    Projeto:=CPROJETO;
end;

procedure TFmedidasFuros.edtOperadoraAlturaExit(Sender: TObject);
begin
        if(edtAlturaOP.Text='')
        then edtOperadoraAltura.Text:='';
        
        {if(edtOperadoraAltura.Text<>'/') and (edtOperadoraAltura.Text<>'')
        then edtUnidadeMedidaAltura.Visible:=True
        else edtUnidadeMedidaAltura.Visible:=False; }
end;

procedure TFmedidasFuros.edtOperadorLarguraExit(Sender: TObject);
begin
      if(edtLarguraOP.Text='')
      then edtOperadorLargura.Text:='';

      {if(edtOperadorLargura.Text<>'/') and (edtOperadorLargura.Text<>'')
      then edtUnidadeMedidaLargura.Visible:=True
      else edtUnidadeMedidaLargura.Visible:=False; }
end;

procedure TFmedidasFuros.lb7Click(Sender: TObject);
Var
  ObjQuery:TIBQuery;
begin
         {Self.Tag:=1;

         ObjQuery:=TIBQuery.Create(nil);
         ObjQuery.Database:=FDataModulo.IBDatabase;

         with ObjCorteComponente_Proj do
         begin
              try
                    ZerarTabela;
                    ObjQuery.Close;
                    ObjQuery.sql.Clear;
                    ObjQuery.sql.Add('select * from tabcortecomponente_proj');
                    ObjQuery.sql.Add('where COMPONENTE_PROJ='+CodigoMaterialGlobal);
                    ObjQuery.SQL.Add('and ferragem='+edtFerragemProj.Text);
                    ObjQuery.Open;
                    ObjQuery.Last;
                    if(ObjQuery.recordcount>0) then
                    begin
                         Status:=dsEdit;
                         Submit_CODIGO(ObjQuery.fieldbyname('codigo').AsString);
                    end
                    else
                    begin
                          Status:=dsInsert;
                          Submit_CODIGO('0');
                    end;
                    Submit_FORMULAALTURACORTE(edtAlturaOP.Text+edtOperadoraAltura.text+edtValorAltura.text{+edtUnidadeMedidaAltura.text);
                    //Submit_FORMULAPOSICAOCORTE(edtLarguraOP.Text+edtOperadorLargura.text+edtValorLargura.text{+edtUnidadeMedidaLargura.text}
                    {ubmit_TAMANHOFURO(edtTamanhoFuro.Text);
                    COMPONENTE_PROJ.Submit_Codigo(CodigoMaterialGlobal);
                    FERRAGEM.Submit_Codigo(edtFerragemProj.Text);
                    If(Salvar(true)=False) then
                    begin
                          MensagemErro('Erro ao salvar as formulas de recorte no vidro');
                          Exit;
                    end;
              finally
                    FreeAndNil(ObjQuery);
              end;


         end;
         limpaedit(FmedidasFuros);
         lbNomeFerragem.Caption:='';   }
         //self.Close;
end;

procedure TFmedidasFuros.bt1Click(Sender: TObject);
begin
     if(pnlAjuda.Visible=False)
     then pnlAjuda.Visible:=True
     else pnlAjuda.Visible:=False;
end;

procedure TFmedidasFuros.edtUnidadeMedidaLarguraExit(Sender: TObject);
begin
      { if(edtUnidadeMedidaLargura.Text<>'mm') and (edtUnidadeMedidaLargura.Text<>'cm') and (edtUnidadeMedidaLargura.Text<>'m') then
        begin
              MensagemErro('Unidade de medida invalida / Informar em milimetros(mm) centimetros(cm) metros(m) ');
              edtUnidadeMedidaLargura.Text:='';

        end;   }
end;

procedure TFmedidasFuros.lb7MouseLeave(Sender: TObject);
begin
        TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFmedidasFuros.lb7MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFmedidasFuros.CarregaMedidas;
var
    Query:TIBQuery;
    AUX:string;
begin
        query:=TIBQuery.Create(nil);
        Query.Database:=FDataModulo.IBDatabase;

        {if(edtFerragemProj.Text='') then
        begin

              Exit;
        end;     }

        try
              with Query do
              begin
                 { Close;
                  sql.Clear;
                  sql.Add('select tabcortecomponente_proj.*,tabferragem.descricao from tabcortecomponente_proj');
                  sql.Add('join tabferragem on tabferragem.codigo=tabcortecomponente_proj.ferragem');
                  sql.Add('where COMPONENTE_PROJ='+CodigoMaterialGlobal);
                  SQL.Add('and ferragem='+dbgrid2.DataSource.DataSet.fieldbyname('ferragem').asstring);
                  Open;    }


                  //NO CASO DA FORMULA SE UTILIZAR DO VALOR DA ALTURA OU DA LARGURA PRA SE CALCULAR A POSI��O DO
                  //CORTE NO VIDRO PRA SE INSTALAR A FERRAGEM
                  lbNomeFerragem.Caption:=dbgrid2.DataSource.DataSet.fieldbyname('descricao').AsString;
                  edtFerragemProj.Text:=dbgrid2.DataSource.DataSet.fieldbyname('ferragem').AsString ;
                  edtAlturaOP.Text:=retornaPalavrasAntesSimboloLocal(dbgrid2.DataSource.DataSet.fieldbyname('formulaalturacorte').AsString);
                  if(edtAlturaOP.Text<>'ALTURA') and (edtAlturaOP.Text<>'LARGURA')
                  then edtAlturaOP.Text:='';

                  edtLarguraOP.Text:=retornaPalavrasAntesSimboloLocal(dbgrid2.DataSource.DataSet.fieldbyname('formulaposicaocorte').AsString);
                  if(edtLarguraOP.Text<>'ALTURA') and (edtLarguraOP.Text<>'LARGURA')
                  then edtLarguraOP.Text:='';

                  edtOperadoraAltura.Text:=RetornaOperadorMatematico(dbgrid2.DataSource.DataSet.fieldbyname('formulaalturacorte').AsString);
                  edtOperadorLargura.Text:=RetornaOperadorMatematico(dbgrid2.DataSource.DataSet.fieldbyname('formulaposicaocorte').AsString);
                  edtTamanhoFuro.Text:=dbgrid2.DataSource.DataSet.fieldbyname('tamanhofuro').asstring;
                  edtValorAltura.Text:=RetornaValor(dbgrid2.DataSource.DataSet.fieldbyname('formulaalturacorte').AsString);
                  edtValorLargura.Text:=RetornaValor(dbgrid2.DataSource.DataSet.fieldbyname('formulaposicaocorte').AsString);

                  if(dbgrid2.DataSource.DataSet.FieldByName('puxador').AsString='S')
                  then chkPuxador.Checked:=True
                  else chkPuxador.Checked:=False;

                  {if(edtOperadoraAltura.Text<>'/') then
                  begin
                          AUX:=retornaPalavrasDepoisSimbolo(fieldbyname('formulaalturacorte').AsString,edtOperadoraAltura.Text);
                          edtUnidadeMedidaAltura.Text:=RetornaUnidadeMedida(AUX);
                          edtUnidadeMedidaAltura.Visible:=true;
                  end;
                  if(edtOperadorLargura.Text<>'/') then
                  begin
                        AUX:=retornaPalavrasDepoisSimbolo(fieldbyname('formulaposicaocorte').AsString,edtOperadorLargura.Text);
                        edtUnidadeMedidaLargura.Text:=RetornaUnidadeMedida(AUX);
                        edtUnidadeMedidaLargura.Visible:=True;

                  end;   }


              end;
        finally
              FreeAndNil(Query);
        end;
end;

function TFmedidasFuros.RetornaOperadorMatematico(str:string):string;
var
  i,j:Integer;
  aux:string ;
begin
      Result:='';
      aux:='';
      i:=0;
      if(Str='')
      then Exit;
      while (i <= Length (str)) do
      begin
          if(Str[i]='/') or (Str[i]='+') or (Str[i]='-') or (Str[i]='*')
          then aux:=Str[i];

          Inc(i,1);
      end;
      Result:=aux;

end;

function TFmedidasFuros.RetornaValor(Str:string):string;
var
  i,j:Integer;
  aux:string ;
begin
       Result:='';
       i:=0;
       aux:='';
       if(Str='')
       then Exit;
       while (i <= Length (str)) do
       begin
            if(Str[i] in['0'..'9'])
            then aux:=aux+Str[i];

            Inc(i,1);
       end;

       result:=aux;
end;


function TFmedidasFuros.RetornaUnidadeMedida(str:string):string;
var
    i:Integer;
    Aux:string;
begin
      result:='';
      i:=0;
      aux:='';

      if(Str='')
      then Exit;

      while (i<Length(str)+1) do
      begin
              if (Str[i]='c') or(Str[i]='m')
              then aux:=aux+Str[i];
              Inc(i,1);
      end;

      result:=aux;

end;


function TFmedidasFuros.retornaPalavrasAntesSimboloLocal(str: string): string;
var
  i,j:Integer;
  aux:string;
begin

   result:='';

  if (str = '') then
    Exit;

  aux:='';
  
  i:=1;
  while ((str[i] <> '/') and(str[i] <> '-') and (str[i] <> '+') and (str[i] <> '*') and (i <= Length (str))) do
  begin

    aux:=aux+str[i];
    i:=i+1;

  end;
  //aux:=aux+Simbolo;
  i:=Length(aux);
  result:='';

  while ( (aux[i] = ' ') and (i >= 1 ) ) do I:=I-1;


  for j := 1 to i  do
    result:=result+aux[j];


end;

procedure TFmedidasFuros.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
        ObjCorteComponente_Proj.Free;
        Tag:=0;
end;

procedure TFmedidasFuros.edtFerragemProjKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   sql:string;
begin
       If (key <>vk_f9)
       Then exit;

       Try
              Fpesquisalocal:=Tfpesquisa.create(Nil);
              sql:='select ferragem.* from tabferragem ferragem join tabferragem_proj fp on fp.ferragem=ferragem.codigo where fp.projeto='+Projeto;
              If (FpesquisaLocal.PreparaPesquisa(sql,'',nil)=True)
              Then Begin
                        Try
                          If (FpesquisaLocal.showmodal=mrok) Then
                          Begin
                                   edtFerragemProj.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                   lbNomeFerragem.Caption:=FpesquisaLocal.querypesq.fieldbyname('descricao').AsString;
                                   edtOperadoraAltura.text:='';
                                   edtOperadorLargura.Text:='';
                                   edtValorLargura.Text:='';
                                   edtValorAltura.text:='';
                                   edtTamanhoFuro.text:='';
                                   pnlAjuda.Visible:=False;
                                   edtAlturaOP.Text:='';
                                   edtLarguraOP.Text:='';

                          End;
                        Finally
                               FpesquisaLocal.QueryPesq.close;
                        End;
              End;

       Finally
             FreeandNil(FPesquisaLocal);

       End;
end;

procedure TFmedidasFuros.edtFerragemProjExit(Sender: TObject);
begin
      //
end;

procedure TFmedidasFuros.edtAlturaOPExit(Sender: TObject);
begin
        if(edtAlturaOP.Text<>'ALTURA') and (edtAlturaOP.Text<>'') and (edtLarguraOP.Text<>'LARGURA') then
        begin
              MensagemAviso('Valor inv�lido');
              edtAlturaOP.text:='';
        end;
end;

procedure TFmedidasFuros.edtLarguraOPExit(Sender: TObject);
begin
       if(edtLarguraOP.Text<>'LARGURA') and (edtLarguraOP.Text<>'') and (edtLarguraOP.Text<>'ALTURA')  then
       begin
              MensagemAviso('Valor inv�lido');
              edtLarguraOP.Text:='';
       end;
end;

procedure TFmedidasFuros.edtAlturaOPKeyPress(Sender: TObject;
  var Key: Char);
begin
           if (Key in['0'..'9']) then
           begin
                  Key:= #0;
           end;
end;

procedure TFmedidasFuros.FormKeyPress(Sender: TObject; var Key: Char);
begin
       if key=#13
        Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFmedidasFuros.dbgrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With dbgrid2.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(dbgrid2.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                dbgrid2.DefaultDrawDataCell(Rect, dbgrid2.Columns[DataCol].Field, State);
          End;
end;

procedure TFmedidasFuros.btGravaClick(Sender: TObject);
Var
  ObjQuery:TIBQuery;
begin
         if(edtFerragemProj.Text='')and(chkPuxador.Checked=False)
         then Exit;

         Self.Tag:=1;

         ObjQuery:=TIBQuery.Create(nil);
         ObjQuery.Database:=FDataModulo.IBDatabase;

         with ObjCorteComponente_Proj do
         begin
              try
                    ZerarTabela;
                    ObjQuery.Close;
                    ObjQuery.sql.Clear;
                    ObjQuery.sql.Add('select * from tabcortecomponente_proj');
                    ObjQuery.sql.Add('where COMPONENTE_PROJ='+CodigoMaterialGlobal);
                    if(edtFerragemProj.Text<>'')
                    then ObjQuery.SQL.Add('and ferragem='+edtFerragemProj.Text)
                    else ObjQuery.SQL.Add('and puxador=''S'' ');
                    ObjQuery.Open;
                    ObjQuery.Last;
                    if(ObjQuery.recordcount>0) then
                    begin
                         Status:=dsEdit;
                         Submit_CODIGO(ObjQuery.fieldbyname('codigo').AsString);
                    end
                    else
                    begin
                          Status:=dsInsert;
                          Submit_CODIGO('0');
                    end;
                    Submit_FORMULAALTURACORTE(edtAlturaOP.Text+edtOperadoraAltura.text+edtValorAltura.text{+edtUnidadeMedidaAltura.text});
                    Submit_FORMULAPOSICAOCORTE(edtLarguraOP.Text+edtOperadorLargura.text+edtValorLargura.text{+edtUnidadeMedidaLargura.text});
                    Submit_TAMANHOFURO(edtTamanhoFuro.Text);
                    COMPONENTE_PROJ.Submit_Codigo(CodigoMaterialGlobal);

                    if(chkPuxador.Checked= True)
                    then Submit_Puxador('S')
                    else Submit_Puxador('N');
                    
                    If(edtFerragemProj.Text<>'')then
                    begin
                        FERRAGEM.Submit_Codigo(edtFerragemProj.Text);
                    end;
                    If(Salvar(true)=False) then
                    begin
                          MensagemErro('Erro ao salvar as formulas de recorte no vidro');
                          Exit;
                    end;
              finally
                    FreeAndNil(ObjQuery);
              end;


         end;
         lbNomeFerragem.Caption:='';
         ObjCorteComponente_Proj.ResgataCortes(CodigoMaterialGlobal);
         lbNomeFerragem.Caption:='';
         edtFerragemProj.Text:='';
         edtOperadoraAltura.text:='';
         edtOperadorLargura.Text:='';
         edtValorLargura.Text:='';
         edtValorAltura.text:='';
         edtTamanhoFuro.text:='';
         pnlAjuda.Visible:=False;
         edtAlturaOP.Text:='';
         edtLarguraOP.Text:='';

end;

procedure TFmedidasFuros.btCancelaClick(Sender: TObject);
begin
       lbNomeFerragem.Caption:='';
       edtFerragemProj.Text:='';
       edtOperadoraAltura.text:='';
       edtOperadorLargura.Text:='';
       edtValorLargura.Text:='';
       edtValorAltura.text:='';
       edtTamanhoFuro.text:='';
       pnlAjuda.Visible:=False;
       edtAlturaOP.Text:='';
       edtLarguraOP.Text:='';
end;

procedure TFmedidasFuros.dbgrid2DblClick(Sender: TObject);
begin
       CarregaMedidas;
end;

procedure TFmedidasFuros.btExcluiClick(Sender: TObject);
begin
       with ObjCorteComponente_Proj do
       begin
              Status:=dsInactive;
              if(Exclui(dbgrid2.DataSource.DataSet.fieldbyname('codigo').asstring,true)=False) then
              begin
                   MensagemErro('Erro ao tentar Excluir rela��o');
                   Exit;
              end;
              ResgataCortes(CodigoMaterialGlobal);
              lbNomeFerragem.Caption:='';
              edtFerragemProj.Text:='';
              edtOperadoraAltura.text:='';
              edtOperadorLargura.Text:='';
              edtValorLargura.Text:='';
              edtValorAltura.text:='';
              edtTamanhoFuro.text:='';
              pnlAjuda.Visible:=False;
              edtAlturaOP.Text:='';
              edtLarguraOP.Text:='';

       end;
end;

procedure TFmedidasFuros.chkPuxadorClick(Sender: TObject);
begin
{  if()then
  begin

  end }
end;

end.
