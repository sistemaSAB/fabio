unit UreprocessaChequeDevolvidoComissao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, StdCtrls, Mask, DB, IBCustomDataSet, IBQuery;

type
  TFreprocessaChequeDevolvidoComissao = class(TForm)
    Grid: TStringGrid;
    Panel1: TPanel;
    EdtDataInicial: TMaskEdit;
    EdtDataFinal: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    btpesquisar: TButton;
    IBQuery1: TIBQuery;
    BtSelecionaTodos: TButton;
    BtRetiraSelecao: TButton;
    btprocessar: TButton;
    procedure GridDblClick(Sender: TObject);
    procedure GridKeyPress(Sender: TObject; var Key: Char);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtSelecionaTodosClick(Sender: TObject);
    procedure BtRetiraSelecaoClick(Sender: TObject);
    procedure btprocessarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FreprocessaChequeDevolvidoComissao: TFreprocessaChequeDevolvidoComissao;

implementation

uses UessencialGlobal, UDataModulo, UMostraBarraProgresso;

{$R *.dfm}

procedure TFreprocessaChequeDevolvidoComissao.GridDblClick(
  Sender: TObject);
begin
     if ( grid.Cells[0,Grid.row]='X' )
     then grid.Cells[0,Grid.row]:=''
     Else grid.Cells[0,Grid.row]:='X';
end;

procedure TFreprocessaChequeDevolvidoComissao.GridKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     then self.GridDblClick(sender);
end;

procedure TFreprocessaChequeDevolvidoComissao.btpesquisarClick(
  Sender: TObject);
var
PdataInicial,PdataFinal:String;  
begin

      Grid.RowCount:=1;
      Grid.ColCount:=1;

      Grid.Cols[0].clear;

      Pdatainicial:='';
      PdataFinal:='';

      if (comebarra(EdtDataInicial.Text)<>'')
      then Begin
                Try
                   strtodate(EdtDataInicial.Text);
                   PdataInicial:=EdtDataInicial.Text;
                except
                      mensagemErro('Digite uma Data Inicial v�lida ou deixe-a em branco');
                      exit;
                End;
      End;

      if (comebarra(EdtDataFinal.Text)<>'')
      then Begin
                Try
                   strtodate(EdtDataFinal.Text);
                   PdataFinal:=EdtDataFinal.Text;
                except
                      mensagemErro('Digite uma Data Final v�lida ou deixe-a em branco');
                      exit;
                End;
      End;



     With Self.IBQuery1 do
     begin
          database:=Fdatamodulo.IBDatabase;
          close;
          sql.clear;
          sql.add('Select tabchequedevolvido.codigo as OCORRENCIA,tabtransferenciaportador.data, ');
          sql.add('tabtitulo.historico as historicotitulo,tabchequesportador.valor,tabchequedevolvido.tituloareceber,');
          sql.add('tabtitulo.credordevedor,tabtitulo.codigocredordevedor,count(tabcomissaovendedores.codigo) as CONTACOMISSAO');
          sql.add('from tabchequedevolvido ');
          sql.add('join tabtransferenciaportador on tabchequedevolvido.transferencia=tabtransferenciaportador.codigo');
          sql.add('join tabchequesportador on tabchequedevolvido.cheque=tabchequesportador.codigo');
          sql.add('join tabtitulo on tabchequedevolvido.tituloareceber=tabtitulo.codigo');
          sql.add('left join tabcomissaovendedores on tabcomissaovendedores.chequedevolvido=tabchequedevolvido.codigo');
          sql.add('where tabchequedevolvido.codigo<>-10');

          if (Pdatainicial<>'')
          then Begin
                    sql.add('and TabTransferenciaportador.data>=:pdatainicial');
                    ParamByName('pdatainicial').asdatetime:=strtodate(pdatainicial);
          End;

          if (Pdatafinal<>'')
          then begin
                    sql.add('and TabTransferenciaPortador.data<=:pdatafinal');
                    ParamByName('pdatafinal').asdatetime:=strtodate(pdatafinal);
          End;

          sql.add('group by tabchequedevolvido.codigo,tabtransferenciaportador.data, ');
          sql.add('tabtitulo.historico,tabchequesportador.valor,tabchequedevolvido.tituloareceber,');
          sql.add('tabtitulo.credordevedor,tabtitulo.codigocredordevedor');
          sql.add('order by tabtransferenciaportador.data');


          open;
          last;
          if (recordcount=0)
          then Begin
                    mensagemaviso('Nenhuma informa��o foi selecionada');
                    exit;
          End;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.Lbmensagem.caption:='Processando pesquisa';
          first;


          Grid.ColCount:=5;
          grid.Cells[1,0]:='CLIENTE';
          grid.Cells[2,0]:='VALOR';
          grid.Cells[3,0]:='DATA';
          grid.Cells[4,0]:='OCORRENCIA';
          //grid.Cells[5,0]:='LANCADO COMISSAO';


          while not(eof) do
          begin
              FMostraBarraProgresso.IncrementaBarra1(1);
              FMostraBarraProgresso.Show;
              Application.ProcessMessages;

              if (fieldbyname('contacomissao').asinteger=0)//s� traz os que n�o tem comiss�o
              Then Begin
                        grid.rowcount:=grid.RowCount+1;
                        grid.Cells[1,Grid.RowCount-1]:=ObjLanctoPortadorGlobal.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring);
                        grid.Cells[2,Grid.RowCount-1]:=formata_valor(Fieldbyname('valor').asstring);
                        grid.Cells[3,Grid.RowCount-1]:=Fieldbyname('data').asstring;
                        grid.Cells[4,Grid.RowCount-1]:=Fieldbyname('ocorrencia').asstring;
              End;

              next;
          End;
          FMostraBarraProgresso.Close;
          AjustaLArguraColunaGrid(grid);
          Grid.FixedCols:=1;
          Grid.FixedRows:=1;

          Showmessage('Concluido');





     End;
end;

procedure TFreprocessaChequeDevolvidoComissao.FormShow(Sender: TObject);
begin
     Grid.RowCount:=1;
     Grid.ColCount:=1;
     grid.cells[0,0]:='';

end;

procedure TFreprocessaChequeDevolvidoComissao.BtSelecionaTodosClick(
  Sender: TObject);
var
cont:integer;
begin
     for cont:=1 to grid.RowCount-1 do
     Begin
          grid.cells[0,cont]:='X';
          grid.row:=cont;
     End;
     Showmessage('Conclu�do');
end;

procedure TFreprocessaChequeDevolvidoComissao.BtRetiraSelecaoClick(
  Sender: TObject);
var
cont:integer;
begin
     for cont:=1 to grid.RowCount-1 do
     Begin
          grid.cells[0,cont]:='';
          grid.row:=cont;
     End;
     Showmessage('Conclu�do');
end;

procedure TFreprocessaChequeDevolvidoComissao.btprocessarClick(
  Sender: TObject);
var
cont:integer;
begin
     Try
         for cont:=1 to Self.Grid.RowCount-1 do
         Begin
              With Self.IBQuery1 do
              Begin
                  if (Self.Grid.Cells[0,cont]='X')
                  Then Begin 
                          close;
                          sql.clear;
                          sql.add('Select count(codigo) from tabcomissaovendedores where chequedevolvido='+Self.Grid.Cells[4,cont]);
                          open;

                          if (Fields[0].AsInteger=0)
                          Then Begin
                                    if (ObjIntegracaoglobal.LancaComissaoNegativa(Self.Grid.Cells[4,cont])=False)
                                    Then Begin
                                              MensagemErro('Abortado');
                                              exit;
                                    End;
                          End;
                  End;

              End;
              Grid.Row:=cont;
         End;
         FDataModulo.IBTransaction.CommitRetaining;
         Showmessage('Conclu�do');
         btpesquisarClick(sender);

     Finally
            FDataModulo.IBTransaction.RollbackRetaining;

     End;
end;

end.
