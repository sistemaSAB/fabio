unit UobjIMPOSTO_ICMS;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UOBJTABELAB_ST,UOBJTABELAA_ST,UOBJCFOP,UobjCSOSN
;
//USES_INTERFACE



Type
   TObjIMPOSTO_ICMS=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                STB:TOBJTABELAB_ST ;
                STA:TOBJTABELAA_ST ;
                CFOP:TOBJCFOP ;
                CSOSN:TObjCSOSN;


                Constructor Create(Owner:TComponent);
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:STRING) :boolean;
                Function    Exclui(Pcodigo:STRING;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :STRING;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:STRING;
                Function  RetornaCampoCodigo:STRING;
                Function  RetornaCampoNome:STRING;
                Procedure Imprime(Pcodigo:STRING);

                Procedure Submit_CODIGO(parametro: STRING);
                Function Get_CODIGO: STRING;
                Procedure Submit_MODALIDADE(parametro: STRING);
                Function Get_MODALIDADE: STRING;
                Procedure Submit_PERC_REDUCAO_BC(parametro: STRING);
                Function Get_PERC_REDUCAO_BC: STRING;
                Procedure Submit_ALIQUOTA(parametro: STRING);
                Function Get_ALIQUOTA: STRING;
                Procedure Submit_IVA(parametro: STRING);
                Function Get_IVA: STRING;
                Procedure Submit_PAUTA(parametro: STRING);
                Function Get_PAUTA: STRING;
                Procedure Submit_MODALIDADE_ST(parametro: STRING);
                Function Get_MODALIDADE_ST: STRING;
                Procedure Submit_PERC_REDUCAO_BC_ST(parametro: STRING);
                Function Get_PERC_REDUCAO_BC_ST: STRING;
                Procedure Submit_ALIQUOTA_ST(parametro: STRING);
                Function Get_ALIQUOTA_ST: STRING;
                Procedure Submit_IVA_ST(parametro: STRING);
                Function Get_IVA_ST: STRING;
                Procedure Submit_PAUTA_ST(parametro: STRING);
                Function Get_PAUTA_ST: STRING;

                Function Get_FORMULA_BC:STRING;
                Function Get_FORMULA_BC_ST:STRING;

                Procedure Submit_FORMULA_BC(parametro:STRING);
                Procedure Submit_FORMULA_BC_ST(parametro:STRING);

                function RetornaNomeCFOPFORA:string;

                Function Get_formula_valor_imposto:STRING;
                Function Get_formula_valor_imposto_ST:STRING;

                Procedure Submit_formula_valor_imposto(parametro:STRING);
                Procedure Submit_formula_valor_imposto_ST(parametro:STRING);

                procedure Submit_CFOP_FORAESTADO(parametro:string);
                Function Get_CFOP_FORAESTADO:string;

                //25/03 - celio removido foi realocado para vidro_icms, ferragem_icms
                //procedure Submit_PERCENTUALTRIBUTO(parametro:string);
                //function Get_PERCENTUALTRIBUTO:string;


                procedure EdtSTBExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtSTBKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                procedure EdtSTAExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtSTAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                procedure EdtCFOPExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtCFOPKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

//CODIFICA DECLARA GETSESUBMITS


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               Owner:TComponent;

               CODIGO:STRING;
               MODALIDADE:STRING;
               PERC_REDUCAO_BC:STRING;
               ALIQUOTA:STRING;
               IVA:STRING;
               PAUTA:STRING;

               MODALIDADE_ST:STRING;
               PERC_REDUCAO_BC_ST:STRING;
               ALIQUOTA_ST:STRING;
               IVA_ST:STRING;
               PAUTA_ST:STRING;
               FORMULA_BC:STRING;
               FORMULA_BC_ST:STRING;

               formula_valor_imposto:STRING;
               formula_valor_imposto_ST:STRING;
               CFOP_FORAESTADO:string;
               //PERCENTUALTRIBUTO:string;

//CODIFICA VARIAVEIS PRIVADAS

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UmenuRelatorios;





Function  TObjIMPOSTO_ICMS.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;


        If(FieldByName('STA').asstring<>'')
        Then Begin
                 If (Self.STA.LocalizaCodigo(FieldByName('STA').asstring)=False)
                 Then Begin
                          Messagedlg('Sit. Trib. Tabela A N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.STA.TabelaparaObjeto;
        End;
        If(FieldByName('STB').asstring<>'')
        Then Begin
                 If (Self.STB.LocalizaCodigo(FieldByName('STB').asstring)=False)
                 Then Begin
                          Messagedlg('Sit. Trib. Tabela B N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.STB.TabelaparaObjeto;
        End;

        If(FieldByName('CFOP').asstring<>'')
        Then Begin
                 If (Self.CFOP.LocalizaCodigo(FieldByName('CFOP').asstring)=False)
                 Then Begin
                          Messagedlg('CFOP N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CFOP.TabelaparaObjeto;
        End;

        if (FieldByName('CSOSN').AsString <> '') then
        begin

          if not (self.CSOSN.LocalizaCodigo(fieldbyname('CSOSN').AsString)) then
          begin

            MensagemErro('CSOSN N�o encontrado');
            self.ZerarTabela;
            result := False;
            Exit;

          end else
            self.CSOSN.TabelaparaObjeto;

        end;

        Self.MODALIDADE:=fieldbyname('MODALIDADE').asstring;
        Self.PERC_REDUCAO_BC:=fieldbyname('PERC_REDUCAO_BC').asstring;
        Self.ALIQUOTA:=fieldbyname('ALIQUOTA').asstring;
        Self.IVA:=fieldbyname('IVA').asstring;
        Self.PAUTA:=fieldbyname('PAUTA').asstring;
        Self.MODALIDADE_ST:=fieldbyname('MODALIDADE_ST').asstring;
        Self.PERC_REDUCAO_BC_ST:=fieldbyname('PERC_REDUCAO_BC_ST').asstring;
        Self.ALIQUOTA_ST:=fieldbyname('ALIQUOTA_ST').asstring;
        Self.IVA_ST:=fieldbyname('IVA_ST').asstring;
        Self.PAUTA_ST:=fieldbyname('PAUTA_ST').asstring;

        Self.FORMULA_BC:=fieldbyname('FORMULA_BC').asstring;
        Self.FORMULA_BC_ST:=fieldbyname('FORMULA_BC_ST').asstring;

        //Self.PERCENTUALTRIBUTO:=fieldbyname('PERCENTUALTRIBUTO').AsString;
        Self.formula_valor_imposto:=fieldbyname('formula_valor_imposto').asstring;
        Self.formula_valor_imposto_ST:=fieldbyname('formula_valor_imposto_ST').asstring;
        CFOP_FORAESTADO:=fieldbyname('CFOP_FORAESTADO').AsString;


        result:=True;
     End;
end;


Procedure TObjIMPOSTO_ICMS.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('STB').asstring:=Self.STB.GET_CODIGO;
        ParamByName('STA').asstring:=Self.STA.GET_CODIGO;
        ParamByName('CFOP').asstring:=Self.CFOP.GET_CODIGO;
        ParamByName('CSOSN').AsString:=self.CSOSN.Get_codigo;
        ParamByName('MODALIDADE').asstring:=Self.MODALIDADE;
        ParamByName('PERC_REDUCAO_BC').asstring:=virgulaparaponto(Self.PERC_REDUCAO_BC);
        ParamByName('ALIQUOTA').asstring:=virgulaparaponto(Self.ALIQUOTA);
        ParamByName('IVA').asstring:=virgulaparaponto(Self.IVA);
        ParamByName('PAUTA').asstring:=virgulaparaponto(Self.PAUTA);
        ParamByName('MODALIDADE_ST').asstring:=Self.MODALIDADE_ST;
        ParamByName('PERC_REDUCAO_BC_ST').asstring:=virgulaparaponto(Self.PERC_REDUCAO_BC_ST);
        ParamByName('ALIQUOTA_ST').asstring:=virgulaparaponto(Self.ALIQUOTA_ST);
        ParamByName('IVA_ST').asstring:=virgulaparaponto(Self.IVA_ST);
        ParamByName('PAUTA_ST').asstring:=virgulaparaponto(Self.PAUTA_ST);
        ParamByName('FORMULA_BC').asstring:=Self.FORMULA_BC;
        ParamByName('FORMULA_BC_ST').asstring:=Self.FORMULA_BC_ST;


        ParamByName('formula_valor_imposto').asstring:=Self.formula_valor_imposto;
        ParamByName('formula_valor_imposto_ST').asstring:=Self.formula_valor_imposto_ST;
        ParamByName('CFOP_FORAESTADO').AsString:=Self.CFOP_FORAESTADO;
        //ParamByName('PERCENTUALTRIBUTO').AsString:=virgulaparaponto(PERCENTUALTRIBUTO);
//CODIFICA OBJETOPARATABELA












  End;
End;

//***********************************************************************

function TObjIMPOSTO_ICMS.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;



  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
   //InputBox('','',Objquery.SQL.Text);
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjIMPOSTO_ICMS.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        STB.ZerarTabela;
        STA.ZerarTabela;
        CFOP.ZerarTabela;
        CSOSN.ZerarTabela;
        MODALIDADE:='';
        PERC_REDUCAO_BC:='';
        ALIQUOTA:='';
        IVA:='';
        PAUTA:='';
        MODALIDADE_ST:='';
        PERC_REDUCAO_BC_ST:='';
        ALIQUOTA_ST:='';
        IVA_ST:='';
        PAUTA_ST:='';
        FORMULA_BC:='';
        FORMULA_BC_ST:='';

        formula_valor_imposto:='';
        formula_valor_imposto_ST:='';
        CFOP_FORAESTADO:='';
        //PERCENTUALTRIBUTO:='';
//CODIFICA ZERARTABELA












     End;
end;

Function TObjIMPOSTO_ICMS.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      //CODIFICA VERIFICABRANCOS

      if (Self.FORMULA_BC='')
      Then Self.FORMULA_BC:='0';

      if (Self.FORMULA_BC_ST='')
      Then Self.FORMULA_BC_ST:='0';


      if (Self.formula_valor_imposto='')
      Then Self.formula_valor_imposto:='0';

      if (Self.formula_valor_imposto_ST='')
      Then Self.formula_valor_imposto_ST:='0';





  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjIMPOSTO_ICMS.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.STB.LocalizaCodigo(Self.STB.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Sit. Trib. Tabela B n�o Encontrado!';

      If (Self.STA.LocalizaCodigo(Self.STA.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Sit. Trib. Tabela A n�o Encontrado!';

      If (Self.CFOP.LocalizaCodigo(Self.CFOP.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ CFOP n�o Encontrado!';

     { if not (self.CSOSN.LocalizaCodigo(self.CSOSN.Get_codigo)) then
        mensagem:=mensagem+'/ CSOSN n�o encontrado';
                                                          }

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjIMPOSTO_ICMS.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        If (Self.STB.Get_Codigo<>'')
        Then Strtoint(Self.STB.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Sit. Trib. Tabela B';
     End;

     try
        If (Self.STA.Get_Codigo<>'')
        Then Strtoint(Self.STA.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Sit. Trib. Tabela A';
     End;
     
     try
        If (Self.CFOP.Get_Codigo<>'')
        Then Strtoint(Self.CFOP.Get_Codigo);
     Except
           Mensagem:=mensagem+'/CFOP';
     End;

     try
        Strtofloat(Self.PERC_REDUCAO_BC);
     Except
           Mensagem:=mensagem+'/Percentual de Redu��o da Base de C�lculo';
     End;
     try
        Strtofloat(Self.ALIQUOTA);
     Except
           Mensagem:=mensagem+'/Al�quota';
     End;
     try
        Strtofloat(Self.IVA);
     Except
           Mensagem:=mensagem+'/�ndice de valor agregado (IVA)';
     End;
     try
        Strtofloat(Self.PAUTA);
     Except
           Mensagem:=mensagem+'/Pauta';
     End;
     try
        Strtofloat(Self.PERC_REDUCAO_BC_ST);
     Except
           Mensagem:=mensagem+'/Percentual de Redu��o da Base de C�lculo (ST)';
     End;
     try
        Strtofloat(Self.ALIQUOTA_ST);
     Except
           Mensagem:=mensagem+'/Al�quota (ST)';
     End;
     try
        Strtofloat(Self.IVA_ST);
     Except
           Mensagem:=mensagem+'/�ndice de valor agregado - IVA (ST(';
     End;
     
     try
        Strtofloat(Self.PAUTA_ST);
     Except
           Mensagem:=mensagem+'/Pauta (ST)';
     End;
     
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjIMPOSTO_ICMS.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
              Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjIMPOSTO_ICMS.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjIMPOSTO_ICMS.LocalizaCodigo(parametro: STRING): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro IMPOSTO_ICMS vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,STA,STB,CFOP,MODALIDADE,PERC_REDUCAO_BC,ALIQUOTA,IVA,PAUTA');
           SQL.ADD(' ,MODALIDADE_ST,PERC_REDUCAO_BC_ST,ALIQUOTA_ST,IVA_ST,PAUTA_ST,FORMULA_BC,FORMULA_BC_ST,formula_valor_imposto,formula_valor_imposto_ST,CFOP_FORAESTADO,CSOSN');
           //SQL.ADD(' ,PERCENTUALTRIBUTO');
           SQL.ADD(' from  TABIMPOSTO_ICMS');
           SQL.ADD(' WHERE codigo='+parametro);


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjIMPOSTO_ICMS.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjIMPOSTO_ICMS.Exclui(Pcodigo: STRING;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjIMPOSTO_ICMS.create(Owner:TComponent);
begin

  self.Owner := Owner;
  Self.Objquery:=TIBQuery.create(nil);
  Self.Objquery.Database:=FDataModulo.IbDatabase;
  Self.ParametroPesquisa:=TStringList.create;

  InsertSql:=TStringList.create;
  DeleteSql:=TStringList.create;
  ModifySQl:=TStringList.create;
  Self.STB:=TOBJTABELAB_ST .create;
  Self.STA:=TOBJTABELAA_ST .create;
  Self.CFOP:=TOBJCFOP.create;
  self.CSOSN:=TObjCSOSN.Create(Self.Owner);


  Self.ZerarTabela;

  With Self do
  Begin

    InsertSQL.clear;
    InsertSQL.add('Insert Into TABIMPOSTO_ICMS(CODIGO,STA,STB,CFOP,MODALIDADE,PERC_REDUCAO_BC');
    InsertSQL.add(' ,ALIQUOTA,IVA,PAUTA,MODALIDADE_ST,PERC_REDUCAO_BC_ST');
    InsertSQL.add(' ,ALIQUOTA_ST,IVA_ST,PAUTA_ST,FORMULA_BC,FORMULA_BC_ST,formula_valor_imposto,formula_valor_imposto_ST,CFOP_FORAESTADO,CSOSN)');//,PERCENTUALTRIBUTO)');
    InsertSQL.add('values (:CODIGO,:STA,:STB,:CFOP,:MODALIDADE,:PERC_REDUCAO_BC,:ALIQUOTA');
    InsertSQL.add(' ,:IVA,:PAUTA,:MODALIDADE_ST,:PERC_REDUCAO_BC_ST,:ALIQUOTA_ST');
    InsertSQL.add(' ,:IVA_ST,:PAUTA_ST,:FORMULA_BC,:FORMULA_BC_ST,:formula_valor_imposto,:formula_valor_imposto_ST,:CFOP_FORAESTADO,:CSOSN)');//,:PERCENTUALTRIBUTO)');

    ModifySQL.clear;
    ModifySQL.add('Update TABIMPOSTO_ICMS set CODIGO=:CODIGO,STA=:STA,STB=:STB,CFOP=:CFOP,MODALIDADE=:MODALIDADE');
    ModifySQL.add(',PERC_REDUCAO_BC=:PERC_REDUCAO_BC,ALIQUOTA=:ALIQUOTA');
    ModifySQL.add(',IVA=:IVA,PAUTA=:PAUTA,MODALIDADE_ST=:MODALIDADE_ST');
    ModifySQL.add(',PERC_REDUCAO_BC_ST=:PERC_REDUCAO_BC_ST,ALIQUOTA_ST=:ALIQUOTA_ST');
    ModifySQL.add(',IVA_ST=:IVA_ST,PAUTA_ST=:PAUTA_ST,FORMULA_BC=:FORMULA_BC,FORMULA_BC_ST=:FORMULA_BC_ST,formula_valor_imposto=:formula_valor_imposto,formula_valor_imposto_ST=:formula_valor_imposto_ST,CFOP_FORAESTADO=:CFOP_FORAESTADO');
    ModifySQl.Add(',CSOSN=:CSOSN');//,PERCENTUALTRIBUTO=:PERCENTUALTRIBUTO');
    ModifySQL.add('where codigo=:codigo');

    DeleteSQL.clear;
    DeleteSql.add('Delete from TABIMPOSTO_ICMS where codigo=:codigo ');

    Self.status := dsInactive;

  End;

end;
procedure TObjIMPOSTO_ICMS.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjIMPOSTO_ICMS.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabIMPOSTO_ICMS');
     Result:=Self.ParametroPesquisa;
end;

function TObjIMPOSTO_ICMS.Get_TituloPesquisa: STRING;
begin
     Result:=' Pesquisa de Imposto de ICMS ' ;
end;


function TObjIMPOSTO_ICMS.Get_NovoCodigo: STRING;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENIMPOSTO_ICMS,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjIMPOSTO_ICMS.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.STA.FREE;
    Self.STB.FREE;
    Self.CFOP.FREE;
    self.CSOSN.Free;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjIMPOSTO_ICMS.RetornaCampoCodigo: STRING;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjIMPOSTO_ICMS.RetornaCampoNome: STRING;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjIMPOSTO_ICMS.Submit_CODIGO(parametro: STRING);
begin
        Self.CODIGO:=Parametro;
end;
function TObjIMPOSTO_ICMS.Get_CODIGO: STRING;
begin
        Result:=Self.CODIGO;
end;
procedure TObjIMPOSTO_ICMS.Submit_MODALIDADE(parametro: STRING);
begin
        Self.MODALIDADE:=Parametro;
end;
function TObjIMPOSTO_ICMS.Get_MODALIDADE: STRING;
begin
        Result:=Self.MODALIDADE;
end;
procedure TObjIMPOSTO_ICMS.Submit_PERC_REDUCAO_BC(parametro: STRING);
begin
        Self.PERC_REDUCAO_BC:=Parametro;
end;
function TObjIMPOSTO_ICMS.Get_PERC_REDUCAO_BC: STRING;
begin
        Result:=Self.PERC_REDUCAO_BC;
end;
procedure TObjIMPOSTO_ICMS.Submit_ALIQUOTA(parametro: STRING);
begin
        Self.ALIQUOTA:=Parametro;
end;
function TObjIMPOSTO_ICMS.Get_ALIQUOTA: STRING;
begin
        Result:=Self.ALIQUOTA;
end;
procedure TObjIMPOSTO_ICMS.Submit_IVA(parametro: STRING);
begin
        Self.IVA:=Parametro;
end;
function TObjIMPOSTO_ICMS.Get_IVA: STRING;
begin
        Result:=Self.IVA;
end;
procedure TObjIMPOSTO_ICMS.Submit_PAUTA(parametro: STRING);
begin
        Self.PAUTA:=Parametro;
end;
function TObjIMPOSTO_ICMS.Get_PAUTA: STRING;
begin
        Result:=Self.PAUTA;
end;
procedure TObjIMPOSTO_ICMS.Submit_MODALIDADE_ST(parametro: STRING);
begin
        Self.MODALIDADE_ST:=Parametro;
end;
function TObjIMPOSTO_ICMS.Get_MODALIDADE_ST: STRING;
begin
        Result:=Self.MODALIDADE_ST;
end;
procedure TObjIMPOSTO_ICMS.Submit_PERC_REDUCAO_BC_ST(parametro: STRING);
begin
        Self.PERC_REDUCAO_BC_ST:=Parametro;
end;
function TObjIMPOSTO_ICMS.Get_PERC_REDUCAO_BC_ST: STRING;
begin
        Result:=Self.PERC_REDUCAO_BC_ST;
end;
procedure TObjIMPOSTO_ICMS.Submit_ALIQUOTA_ST(parametro: STRING);
begin
        Self.ALIQUOTA_ST:=Parametro;
end;
function TObjIMPOSTO_ICMS.Get_ALIQUOTA_ST: STRING;
begin
        Result:=Self.ALIQUOTA_ST;
end;
procedure TObjIMPOSTO_ICMS.Submit_IVA_ST(parametro: STRING);
begin
        Self.IVA_ST:=Parametro;
end;
function TObjIMPOSTO_ICMS.Get_IVA_ST: STRING;
begin
        Result:=Self.IVA_ST;
end;
procedure TObjIMPOSTO_ICMS.Submit_PAUTA_ST(parametro: STRING);
begin
        Self.PAUTA_ST:=Parametro;
end;
function TObjIMPOSTO_ICMS.Get_PAUTA_ST: STRING;
begin
        Result:=Self.PAUTA_ST;
end;
//CODIFICA GETSESUBMITS


procedure TObjIMPOSTO_ICMS.EdtSTBExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.STB.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.STB.tabelaparaobjeto;
     labelnome.caption:=Self.Stb.Get_DESCRICAO;
End;
procedure TObjIMPOSTO_ICMS.EdtSTBKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.STB.Get_Pesquisa,Self.STB.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.STB.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.STB.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.STB.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
////
procedure TObjIMPOSTO_ICMS.EdtSTAExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.STA.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.STA.tabelaparaobjeto;
     labelnome.caption:=Self.STA.Get_DESCRICAO;
End;
procedure TObjIMPOSTO_ICMS.EdtSTAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.STA.Get_Pesquisa,Self.STA.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.STA.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.STA.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.STA.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjIMPOSTO_ICMS.EdtCFOPExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.CFOP.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.CFOP.tabelaparaobjeto;
     labelnome.caption:=Self.CFOP.Get_NOME;
End;

procedure TObjIMPOSTO_ICMS.EdtCFOPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.CFOP.Get_Pesquisa,Self.CFOP.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CFOP.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CFOP.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CFOP.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

//CODIFICA EXITONKEYDOWN

procedure TObjIMPOSTO_ICMS.Imprime(Pcodigo: STRING);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJIMPOSTO_ICMS';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



function TObjIMPOSTO_ICMS.Get_FORMULA_BC: STRING;
begin
     Result:=Self.formula_bc;
end;

function TObjIMPOSTO_ICMS.Get_FORMULA_BC_ST: STRING;
begin
     Result:=Self.formula_bc_st;
end;

procedure TObjIMPOSTO_ICMS.Submit_FORMULA_BC(parametro: STRING);
begin
     Self.FORMULA_BC:=parametro;
end;

procedure TObjIMPOSTO_ICMS.Submit_FORMULA_BC_ST(parametro: STRING);
begin
     Self.FORMULA_BC_ST:=parametro;
end;



function TObjIMPOSTO_ICMS.Get_formula_valor_imposto: STRING;
begin
     Result:=Self.formula_valor_imposto;
end;

function TObjIMPOSTO_ICMS.Get_formula_valor_imposto_ST: STRING;
begin
     Result:=Self.formula_valor_imposto_st;
end;

procedure TObjIMPOSTO_ICMS.Submit_formula_valor_imposto(parametro: STRING);
begin
     Self.formula_valor_imposto:=parametro;
end;

procedure TObjIMPOSTO_ICMS.Submit_formula_valor_imposto_ST(parametro: STRING);
begin
     Self.formula_valor_imposto_ST:=parametro;
end;

procedure TObjIMPOSTO_ICMS.Submit_CFOP_FORAESTADO(parametro:string);
begin
    self.CFOP_FORAESTADO:=parametro;
end;

function TObjIMPOSTO_ICMS.Get_CFOP_FORAESTADO:string;
begin
    Result:=Self.CFOP_FORAESTADO;
end;

function TObjIMPOSTO_ICMS.RetornaNomeCFOPFORA:string ;
var
    Query:TIBQuery;
begin

    Result := '';

    if Trim(CFOP_FORAESTADO) = '' then
      Exit;

    try
      Query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
        with Query do
        begin
              close;
              sql.Clear;
              sql.add('select nome from tabcfop where codigo='+CFOP_FORAESTADO);
              Open;
              Result:=fieldbyname('nome').AsString;
        end;
    finally
          FreeAndNil(Query);
    end;


end;
{
procedure TObjIMPOSTO_ICMS.Submit_PERCENTUALTRIBUTO(parametro:string);
begin
  Self.PERCENTUALTRIBUTO:=parametro;
end;

function TObjIMPOSTO_ICMS.Get_PERCENTUALTRIBUTO:string;
begin
  Result:=PERCENTUALTRIBUTO;
end;
}
end.



