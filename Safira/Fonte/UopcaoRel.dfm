object FOpcaorel: TFOpcaorel
  Left = 459
  Top = 245
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Op'#231#245'es - EXCLAIM TECNOLOGIA'
  ClientHeight = 505
  ClientWidth = 761
  Color = 10643006
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object RgOpcoes: TRadioGroup
    Left = 0
    Top = 50
    Width = 667
    Height = 405
    Align = alLeft
    BiDiMode = bdRightToLeftReadingOnly
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentBiDiMode = False
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
  end
  object pnl3: TPanel
    Left = 0
    Top = 0
    Width = 761
    Height = 50
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -21
    Font.Name = 'Arial Black'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object btCancelar: TSpeedButton
      Left = 711
      Top = 2
      Width = 53
      Height = 52
      Cursor = crHandPoint
      Hint = 'Ajuda'
      Flat = True
      Glyph.Data = {
        660F0000424D660F000000000000360000002800000024000000240000000100
        180000000000300F0000130B0000130B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFFFFFFFFFFFF
        FFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFEFFFDFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFCFEFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFC9CBE96C72D56B70C5CFCFDFFFFFFEFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E5F97881E85368FF384EE9666DC2ECEB
        EFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFF9F8FAE9E8F1FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1C3F35E70
        F35167EE314BEB303DCFB5B6DCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFCFEAAAAE0A1A0D8FBFBFBFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
        FFFFFFFFFFFFFFBABEF35A6AF2475CE72942E01F32D59A9DDAFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD8D9F35453D3
        A2A1E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
        FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFC4C6F6596AEF4058E6253EDA1E
        33D58D90D4FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF8D8EE6454BDAD5D9F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFE9
        E4F96C7AF23E56EE233BD51A34DC545BC2E6E6ECFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCBCDF0313DE17B80E6FDFDFDFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFBFDFEA5ACF53D53EF2039D71833D71D2DC69B9DCD
        FEFEFEFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFEFDFFFFFFFEEDF0F55765E7243B
        EBC4C6EEFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8EAFC717FF0
        2741E9142DCF1B32DC4049BED7D4E3FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFF
        FFFDFDFD8F96E61630F46673E7F8F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFF
        FFFFFFFFFFFFFEFEFECFD0F64A5CE51F39E1142FD3182ED46D71C0F4F4F4FFFF
        FFFFFFFEFEFDFFFFFFFFFFFFFFB0B4E42E43EC2A40E6C8CAEDFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDB5B9F33A4FE61732
        DA132FDA1E31C89394C7FCFCFCFDFFFFFFFFFFFFFFFEC3C5E74153E01E39F582
        8CE4FCFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFEFFFFFFFEFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFDFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFAFCFDA2AAF02D46E40B28D71832DD3743C7B0B0CEFDFFFFFFFFFFC9
        CAE44C5ADC1D3AF94355DEE4E4F6FFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F7FF9BA2F12238DE0E2CD923
        3EE24D59C9C4C4D6C6C7E3505DDF1938F91E36E4B4B8EDFFFFFFFDFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF
        FFFFF8F8FE98A2F0223ADA1430DB2336D33E4BB93D4ACA1531EA0B26E27C87E3
        FDFAFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFF
        FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFFB1B5F03144D50C23CD0E27D1
        0C26D10721D35D6ADEEAEAFAFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFFFFFFFF
        FEFCFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFF
        D0CDE63C4ACC011BCD0118C10017CA1728BFB4B6D8FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFEFFFEFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFEFEFEE6E8F0959AD93143D60520D5001ACC011CCB031ED3041AC04F54
        AFD0CEDAFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FEFEFEFFFFFFFFFFFFFFFFFFF7F7F7C1C3E2737ADB2F41E21835F40524E50322
        E32F41DC5764D8172ED8051EDA3B45BBBCBDD2FFFFFFFFFFFFFEFEFEFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFDADBE9A0A2DC6E78E46074
        FB485EF32C45E71A39FA2F45F1B3B7F1F2F1FA9AA3ED2B44F21330FD3A46CEA5
        A6C8FCFAFAFFFFFFFFFFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFEDEEF2BABC
        DF8D91E28391F78798FF7686F05D71EE4A61FD5265F6BABFF6FFFFFFFFFFFFFE
        FEFEBFC3F66073FA445AFF4755D78E90C6E9EBECFFFFFEFFFFFFFFFFFFFEFDFF
        FCFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF
        FFFFFFFFDFDDF3939CDC7481E98E9EFF94A1F78494ED8291F87386FF818EF8D4
        D6F9FFFFFFFFFFFFFFFFFFFDFFFFFFFFFFE6E7FB9CA7F76F81FE6772E47D81C8
        CCCCD8FFFFFEFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2D4F68593F96B7EFD7080EA8290F08E
        9AFF8897FDABB1F8E8EBFAFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFEFFFFFF
        F9F8FCCFD1F9A5AFFD848FF76E75D29E9ECEEAE9EBFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFEFEFEFEFFFFFFFAFBFFD9
        DCFB8E9BF77989F38393F4A6AFF9D3D6FCF8F8FEFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEFFDD1D3FC99A1FA6169DA7477
        CCB7B8DEFEFDFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFEFEFEEBEEFDD4DAFDD9DBFEF0F0FEFEFDFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFF3F4FED1D5FE9298EB9998E2FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFDFFFFFFFFFFFFFFFF
        FFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFEFFFEFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFEFEFEFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFDFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFDFFFFFFFF
        FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FDFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFF
        FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
      OnClick = BtCancelarClick
    end
    object lb1: TLabel
      Left = 298
      Top = 9
      Width = 100
      Height = 32
      Caption = 'Op'#231#245'es'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object pnlRodape: TPanel
    Left = 0
    Top = 455
    Width = 761
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindowFrame
    TabOrder = 2
    object Img1: TImage
      Left = 0
      Top = 0
      Width = 761
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbdata: TLabel
      Left = 10
      Top = 9
      Width = 341
      Height = 32
      Caption = 'Segunda Feira, 10/11/2047'
      Color = clWindow
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
  end
  object pnl1: TPanel
    Left = 667
    Top = 50
    Width = 94
    Height = 405
    Align = alClient
    BevelOuter = bvNone
    Color = 10643006
    TabOrder = 3
    object btRelatorios: TSpeedButton
      Left = 44
      Top = 3
      Width = 49
      Height = 41
      Cursor = crHandPoint
      Hint = 'Ajuda'
      Flat = True
      Glyph.Data = {
        660F0000424D660F000000000000360000002800000024000000240000000100
        180000000000300F0000130B0000130B00000000000000000000A1663EA3653F
        A3663EA3663EA2643EA3663EA3663EA1663EA3663EA2673FA3663EA4673F9965
        3C996D4EA4816DB19486BB9F98BB9F98BBA19BB7988FA78673966E5593603FA3
        663EA3663EA3663EA3663EA3663EA2653DA3663EA3663EA3663EA3663EA3663E
        A3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA4663EA366
        3EA3663E9B704FAE9484CAB7BADCCBD6D0C9D0C1C5C0B3C2B4B1C3B2B4C6B5C2
        CAC3D8D2D7F3E2EFF0DDE6C3B0A999745A9B613DA3663EA3663EA4673FA3663E
        A3663EA3673DA3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA366
        3EA3663EA3663EA1663E9C6C4AB59D91DACAD5D0CCD19BB89F59A0612B96360F
        932201971B019D1A01A51E04AC291EB43C48BE5F8FD29FDBE6DCFCF1F9CBBBB5
        976F56A3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA366
        3EA3663EA1663EA3673DA2673FA4663DA3663EA88165CFBFC6D4CED383AC8729
        872E007D06008305028D0E009617009D1B00A31F00AB2300B42501BB2500C121
        01C4200AC53360CA77D8E9DCF3E2EFA38C7D925D38A3663EA1663EA3663EA366
        3EA3663EA3663EA3663EA3653FA3663EA3663EA3663EA3663EA3663EAA8C79D9
        C7D4B0BFB1348937017400027E06008511008A1400901600961A019B1E00A220
        01AA2500B02700B72902BF2E04C73100CF3000D4260ACE348BD39EEFDCEBB3A0
        9B8E5E3AA4663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA3
        663EA3663EAA9080D6C6D18DB29206770901770000810E008310018612008A14
        009016009519009A1D01A02000A82300AE2500B42902BC2C02C32D05C93309D1
        3604DA3503DA2553D070E6D8E4B8A6A58D5D3BA3663EA4663EA3663EA3663EA3
        663EA3653FA4663EA3663EA3663EA78672D6C5D27DAE82027801007B05008010
        00810E008310028712018A1602911700931901991A009F1D00A62100AB2300B2
        2702B92B01BE2D05C32E06CB3305D23609D93700DD2742CF62E5D4E1AD9A9595
        5E39A3663EA2653DA2653DA4673FA3663EA3663EA3663EA37758D2C1CA91B696
        02800200820500820D00820D00830E008510008812008D14008E170093170098
        19019E1E00A22100A92401AF2500B52702BA2A03C12E03C63005CC3406D03509
        D63901DA2554CE70E3D0DF997F71A2653DA3653FA2653DA3663EA3663EA3663E
        A26942C2AEADB1BCB409880F01860700871100870D00820B00840F0087110082
        05028304008D14009418009718009B1B03A11F00A62101AB2300B12600B62800
        BC2B01C12E02C52F06CB3306CE3306D33600D32284CD95D8C1CF8B6448A3663E
        A3663EA3663EA3663EA3673DAC8876CFC2CA389943008A01018D10008A0F0089
        0F00870D00860E00860A3A9C424AA04C017C0600880A00961A009A1A009F1D00
        A31F00A72201AD2501B22500B72700BC2B00C02D03C42E04C93106CB3301D02E
        0DC732C3CEC6AD969499623BA3663EA3663EA3663EA16A43CAB4B991B297018E
        03009110008F10028E11008C0F018A10008709088A0EC9D5C9F1E5F1A6C6A736
        953A018104009110029D1C00A01E00A42000A92102AC2401B22500B72600BA2A
        00BD2C01C12E04C43103C53203C62259C46ED2C1CA90674EA3663EA4663EA366
        3EAA8069CFC0C845A55E00961101940E01930F00920E008F10008E100089090B
        8C13C6D2C6DBD5DAE5DCE6E3DFE48CBD8F219026008902019B1800A31F01A61F
        00A92400AC2400B12601B42900B72900BA2A00BD2C01BF2C00C12B06BA2CBFC7
        C0A78B84A1663EA3663EA3663EBBA096ABB9AD1CA03C17A23300980D00961101
        9513019211008F10008A0A0B8F13C5D4C6DED8DDD4D4D4D7D4D6ECDEEAD9DBDB
        78B57B138E18008D08039F1C00A72200AA2000AC2200AE2400B12600B42600B6
        2800B72901B82A00B61877BE85C8B0BC93603FA3663EA46D48C9B1B37BB18614
        A43427A6430C9F2500990E00981100961100941200900C0A9113C8D7C9E2DCE1
        D8D8D8D6D8D9D6D6D6DBD7DCF0E1EFCDD8D061AD65038E0F00940B00A41D00A9
        2102AA2200AC2400AC2401AF2501B22703B12700B11C3AB14FCDBEC6926952A3
        653FA87C64CCBAC151AD6617A93925A74223A742099E1F00990E009912009712
        00940B0C9515CCDBCDE7E1E6DDDEDCDCDDDBDBDBDBDAD9DBD5D7D7E0D9E0F0E4
        F0BDD3C048A64F038D0900980F00A42000A82000A82000A92100AA2200AC2400
        AB1F0FA62BB9BCBAA18071A3663EB29789C2B9BC39AD541EAD4024AB4424AB44
        21A842099F1D009C0F009A1300960D0B9714CFDED0ECE6EBE2E3E1E1E1E1E0E0
        E0DFDFDFDCDCDCDDDDDDDADADAE9DEE6EDE5ECA9CBAC329F39008B0200991400
        A42000A31F00A42000A42000A420019E18A5B9A6AB8D88A3663EC3B0ADB5B6B4
        2FB04D25B24526AF4724AD4524AB4420A7400CA121009B10009A0D0B9A15D3E2
        D4F0EAEFE6E6E6E5E5E5E4E4E4E3E3E3E2E2E2E0E0E0E0E0E0DEDEDEDCDCDCEE
        E3EDE8E4E994C3951D9428009313009F1D009E1C009F1D009E1C01991090B391
        B49897A2643EC2B4B6B3B7B12CB34C27B74729B34828B24725AE4625AC4525AC
        4612A52B009D0D0A9D13D5E7D6F5EFF4EBEBEBE9E9E9E8E7E9E7E7E7E5E5E5E5
        E5E5E4E4E4E3E4E2E4E2E1E1E1E1DFDFDFFDEAFBDAE4DE0F901D01971501991A
        01991A01991A02920A8AB08CB6999C9E633CC1B4B6B2B6B02FB65029BB4B2BB7
        4C2BB54A29B34827B14626AD4624AB4715A63311A120D5E8D7FAF5F7F0F1EDEF
        EFEFECEFEDECECECEAEAEAE8EAEAE9E9E9E8E8E8E6E7E5E6E6E6F7ECF6EFE8EF
        8AC190089012009414009617019519009418008D0889AF8BB599999E633CC2B1
        AEB9B7B635B9542BBE4C2EBB4E2DBA4D2CB64B2BB44C28B14926AF4723AC4426
        AB44DCEFE0FBF9F9F1F3F3F3F3F3F2F2F2F1F1F1F4F0EFEFEFEFEDEDEDECECEC
        FAF1FBFBF2FCAED1AF369C36008701029313019315009214009214009016018A
        0A90AE91B49695A3673DB69788C3BABD44B7602BC24E2FBE5130BD502DBA4D2C
        B84D29B54A28B44925AF4329AD48DFF2E3FFFFFFFBF9F9F7F7F7F5F5F5F4F4F4
        F3F3F3F5F3F3FDF7FCFEFBFDC7DFC74CA94E008701008D050093130191130190
        12008F14008E13008E1301880EA5B2A2AC8D84A3663EAB7C60CBBBC25DB76E2B
        C54E31C35331C1512EBD502FBC4F2EBB4E2EB84C27B3482DB14CDFF2E3FFFFFF
        FCFBFDFCFCFCFBFBFBF9F9F9FEFBFDFFFFFFDFEEE063B562038A060088000192
        0D009311009110008F10008F11018D10008C0F008A0A148B1EB7B8B6A17D6BA3
        663EA56C45D0B5B884B89029C94F36C55833C55533C25533C0532FBE512FBC4F
        29B84B2FB650E0F3E4FFFFFFFCFCFCFCFCFCFEFEFEFDFFFFF2F9F48ACD9A1C9C
        370392170A982109972006921C039217039116018F14018E15028D14048B1700
        850B42974DC7BAC29B6F52A3663EA3663EC4A69BB4BBB43AC65B36CC5A36C858
        36C65635C55534C45432BF522DBC4F33BD52DFF5E3FEFEFEFDFDFDFFFFFFFBFD
        FDA3D9AE36AD520B9D2D1DA43E22A54421A2411FA03F1D9B3B1B993918973416
        9434159232128E30138F3100862083AE8DC1AEB19D6641A3673DA4663EAF8366
        D5C5D068C17C32D2583ACC5C38CA5A38C75A36C65635C4572FC15136C055E6F7
        EAFFFFFFFEFEFEBCE5C64CBD6617A73620AD402AAF4825AC4524A94224A64123
        A54020A13E1E9F3C1E9D3A1C9A371A973718953514922F1D9138BBBCBAA98D82
        A3663EA3663EA3663EA36841D2BDBFAEBFB13FD16139D05C3CCE5E3CCB5E38C9
        5C38C75A34C65631C452B8E9C3D2EFD869CA8023B24521B3432FB94E2CB34C2A
        B14A2AB14A27AF4526AA4522A94326A54222A34020A23D209F3C1B9C391C9A37
        0791256CAA7CCDBAC39E6F50A1663EA3663EA3663EA3663EB28D73DCCDD57BC8
        8E36D65A3ED15F3DD05D3CCF5D3DCC5F3BCB5B35C75734C45431C15125BB4933
        C05334BE5332BC512FB94E2EB54E2BB24B2AB14B29B04A27AE4726AA4526A744
        24A54220A44022A13E189C372B9C44C0C1BFB29891A3663EA3663EA3663EA366
        3EA3663EA3663ECFBCB5D4D4D45AD17635D75A41D4623FD2603DCF5F3CCE5E3C
        CC5C37C95938C55637C65937C45735C25534C15434BE5331BB5030BA4F2EB74F
        2DB44D2AB14A2AAE4928AC4729AB4627A84721A540109D3098B89FCBBBC2A170
        50A3663EA3663EA3653FA3663EA3653FA3663EA97656F7E9EFD1E1D64AD66B37
        D75D44D46341D46241D1613DCF5F3ECE5E3CCB5E3DCA5D3AC75A36C55835C457
        35C15636C05533BD5232BC5130B9512FB64F2DB44E2DB24B29B04A2AB04614A6
        3678B686D1C0C9A9846EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA4
        673FB88F78FFF9FEC7E1CF48D66B37DA5942D66442D56741D4623FD1613DCF5F
        3ECE5E3CCB5E3BCA5D39C85B37C65937C35836C35636C05535BF5433BA5331B8
        5230B7502CB54D17AE3A6DB880D1C2CAB09787A3663EA3663EA3663EA3663EA3
        663EA3663EA3663EA3673DA3663EA4673FC09D83FEF8FDCEE0D359D6763BDD5F
        41D96244D76542D36641D3633FD1613FCF5F3ECE5D3DCC5F3DCA5D3BC85B3AC7
        5A37C45736C25737C15535BE562BBA4D24B64683C193D0C2CDB39D92A06740A3
        663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663E
        B9947AF6E7EBDBD9D97FCF9447DD6C3DDF6141D96243D56542D56541D36340D2
        6240D0603ECD603DCC5F3ECB5E3DC75C3CC65B32C5532BBF4D49BE67ACC5B1CF
        BFCAAD9382A1663FA4673FA3663EA3663EA3663EA3663EA3663EA1663EA3653F
        A2653DA3663EA4673FA3663EA3663EAB7C5DD6C3BEDDCED6B5C6B878D08E4DD8
        7040DD653FDB5F3DD76040D7633ED4623FD3613AD05E36CD5932CC5537C9594F
        C76E91C69ECBC4C7C6B3B6A68167A2653DA3663EA3663EA2653DA3663EA3663E
        A3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA166
        3FB6947DD3C1C0D7C7D2BDC4BF99C7A375CC8A5ED27953D26F4DD16C4BCE6D58
        CF7467CD7F84CA95ACC8B1CAC5C7CEB9C2B093849F6B47A3663EA3663EA4663E
        A3673DA3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA366
        3EA3663EA4663EA3663EA3663EA3663EA26B44B18970C7ACA2CDBABDD1C1C8C9
        C0C3C1BFBEBCBDB9BDBEBCC6C1C3D0C1C9CDB7BCC1A4A0AE8A78A06C48A3663E
        A3663EA1663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA3663EA366
        3EA3673DA1663EA3663EA4673FA3663EA3663EA3663EA46640A3663EA46640A3
        663EA3663EA4704CA98168B59D91C1B2B0BDB2B5BFB1B2B5A098A7826CA67352
        A4673FA4673FA3663EA1663FA3663EA3663EA3663EA3663EA3663EA3663EA366
        3EA3663EA3663EA3663E}
      OnClick = BtrelatoriosClick
    end
  end
end
