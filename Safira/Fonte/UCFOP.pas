unit UCFOP;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjCFOP;

type
  TFCFOP = class(TForm)
    imgrodape: TImage;
    edtCodigo: TEdit;
    lb2: TLabel;
    edtNome: TEdit;
    lb3: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lb1: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btgravar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjCFOP:TObjCFOP;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCFOP: TFCFOP;


implementation

uses UessencialGlobal, Upesquisa, UDataModulo, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCFOP.ControlesParaObjeto: Boolean;
Begin
       Try
        With Self.ObjCFOP do
        Begin
             Submit_CODIGO(edtCODIGO.text);
             Submit_NOME(edtNOME.text);
             result:=true;
        End;
        Except
          result:=False;
        End;

End;

function TFCFOP.ObjetoParaControles: Boolean;
Begin
       Try
        With Self.ObjCFOP do
        Begin
             edtCODIGO.text:=Get_CODIGO;
             edtNOME.text:=Get_NOME;
             result:=true;
        End;
        Except
              result:=False;
        End;

End;

function TFCFOP.TabelaParaControles: Boolean;
begin
     If (Self.ObjCFOP.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFCFOP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCFOP=Nil)
     Then exit;

If (Self.ObjCFOP.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjCFOP.free;
end;

procedure TFCFOP.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFCFOP.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     desab_botoes(Self);

     //edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjCFOP.Get_novocodigo;
     edtcodigo.enabled:=True;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     btalterar.visible:=false;
     btrelatorios.visible:=false;
     btsair.Visible:=false;
     btopcoes.visible:=false;
     btexcluir.Visible:=false;
     btnovo.Visible:=false;
     btopcoes.visible:=false;

     Self.ObjCFOP.status:=dsInsert;
     EdtCODIGO.setfocus;

end;


procedure TFCFOP.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCFOP.Status=dsinactive) and (EdtCodigo.text<>'')
    Then
    Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjCFOP.Status:=dsEdit;
                edtNome.setfocus;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btalterar.visible:=false;
                btrelatorios.visible:=false;
                btsair.Visible:=false;
                btopcoes.visible:=false;
                btexcluir.Visible:=false;
                btnovo.Visible:=false;
                btopcoes.visible:=false;
    End;


end;

procedure TFCFOP.btgravarClick(Sender: TObject);
begin

     If Self.ObjCFOP.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjCFOP.salvar(true)=False)
     Then exit;

     habilita_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
      btopcoes.visible:=true;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCFOP.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjCFOP.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCFOP.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (Self.ObjCFOP.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFCFOP.btcancelarClick(Sender: TObject);
begin
     Self.ObjCFOP.cancelar;
     btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
      btopcoes.visible:=true;

     limpaedit(Self);
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFCFOP.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFCFOP.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCFOP.Get_pesquisa,Self.ObjCFOP.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCFOP.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCFOP.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjCFOP.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFCFOP.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     desabilita_campos(Self);

     Try
        Self.ObjCFOP:=TObjCFOP.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');


     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE CFOP')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);
    if (self.Tag <> 0) then
    begin

      if (ObjCFOP.LocalizaCodigo (IntToStr (Self.Tag))) then
      begin

        ObjCFOP.TabelaparaObjeto;
        Self.ObjetoParaControles;

      end;

    end;
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCFOP.OBJETO.Get_Pesquisa,Self.ObjCFOP.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjCFOP.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjCFOP.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjCFOP.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
