unit UobjCONTAGERENCIALFLUXOCAIXA;
Interface
Uses forms,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal
,UOBJCONTAGER
,UOBJFLUXOCAIXA
;
//USES_INTERFACE




Type
   TObjCONTAGERENCIALFLUXOCAIXA=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               Contagerencial:TOBJCONTAGER;
               FluxoCaixa:TOBJFLUXOCAIXA;
//CODIFICA VARIAVEIS PUBLICAS



                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                procedure EdtContagerencialExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtContagerencialKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtFluxoCaixaExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFluxoCaixaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Procedure RetornaContas(Pfluxo:string);
                Procedure GeraPlanilha;
                Procedure RastrearContas;
//CODIFICA DECLARA GETSESUBMITS



         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
//CODIFICA VARIAVEIS PRIVADAS



               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UContaGer, UinterageExcel, DateUtils, UmostraStringList;





Function  TObjCONTAGERENCIALFLUXOCAIXA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('Contagerencial').asstring<>'')
        Then Begin
                 If (Self.Contagerencial.LocalizaCodigo(FieldByName('Contagerencial').asstring)=False)
                 Then Begin
                          Messagedlg('Conta Gerencial N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Contagerencial.TabelaparaObjeto;
        End;
        If(FieldByName('FluxoCaixa').asstring<>'')
        Then Begin
                 If (Self.FluxoCaixa.LocalizaCodigo(FieldByName('FluxoCaixa').asstring)=False)
                 Then Begin
                          Messagedlg('Fluxo de Caixa N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.FluxoCaixa.TabelaparaObjeto;
        End;
//CODIFICA TABELAPARAOBJETO



        result:=True;
     End;
end;


Procedure TObjCONTAGERENCIALFLUXOCAIXA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Contagerencial').asstring:=Self.Contagerencial.GET_CODIGO;
        ParamByName('FluxoCaixa').asstring:=Self.FluxoCaixa.GET_CODIGO;
//CODIFICA OBJETOPARATABELA



  End;
End;

//***********************************************************************

function TObjCONTAGERENCIALFLUXOCAIXA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCONTAGERENCIALFLUXOCAIXA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Contagerencial.ZerarTabela;
        FluxoCaixa.ZerarTabela;
//CODIFICA ZERARTABELA



     End;
end;

Function TObjCONTAGERENCIALFLUXOCAIXA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Contagerencial.get_Codigo='')
      Then Mensagem:=mensagem+'/Conta Gerencial';
      If (FluxoCaixa.get_Codigo='')
      Then Mensagem:=mensagem+'/Fluxo de Caixa';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjCONTAGERENCIALFLUXOCAIXA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Contagerencial.LocalizaCodigo(Self.Contagerencial.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Conta Gerencial n�o Encontrado!'
      Else Begin
                Self.Contagerencial.TabelaparaObjeto;

                if (Self.Contagerencial.Get_Tipo='C')
                Then Mensagem:=Mensagem+'/Conta Gerencial tipo inv�lido';
                

      End;



      If (Self.FluxoCaixa.LocalizaCodigo(Self.FluxoCaixa.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Fluxo de Caixa n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCONTAGERENCIALFLUXOCAIXA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.Contagerencial.Get_Codigo<>'')
        Then Strtoint(Self.Contagerencial.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Conta Gerencial';
     End;
     try
        If (Self.FluxoCaixa.Get_Codigo<>'')
        Then Strtoint(Self.FluxoCaixa.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Fluxo de Caixa';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCONTAGERENCIALFLUXOCAIXA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCONTAGERENCIALFLUXOCAIXA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCONTAGERENCIALFLUXOCAIXA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CONTAGERENCIALFLUXOCAIXA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Contagerencial,FluxoCaixa');
           SQL.ADD(' from  TabContaGerencialFluxoCaixa');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCONTAGERENCIALFLUXOCAIXA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCONTAGERENCIALFLUXOCAIXA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCONTAGERENCIALFLUXOCAIXA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ObjDatasource:=TDataSource.create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjqueryPesquisa;



        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Contagerencial:=TOBJCONTAGER.create;
        Self.FluxoCaixa:=TOBJFLUXOCAIXA.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabContaGerencialFluxoCaixa(CODIGO,Contagerencial');
                InsertSQL.add(' ,FluxoCaixa)');
                InsertSQL.add('values (:CODIGO,:Contagerencial,:FluxoCaixa)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabContaGerencialFluxoCaixa set CODIGO=:CODIGO');
                ModifySQL.add(',Contagerencial=:Contagerencial,FluxoCaixa=:FluxoCaixa');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabContaGerencialFluxoCaixa where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCONTAGERENCIALFLUXOCAIXA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCONTAGERENCIALFLUXOCAIXA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCONTAGERENCIALFLUXOCAIXA');
     Result:=Self.ParametroPesquisa;
end;

function TObjCONTAGERENCIALFLUXOCAIXA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de CONTAGERENCIALFLUXOCAIXA ';
end;


function TObjCONTAGERENCIALFLUXOCAIXA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCONTAGERENCIALFLUXOCAIXA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCONTAGERENCIALFLUXOCAIXA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCONTAGERENCIALFLUXOCAIXA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Contagerencial.FREE;
    Self.FluxoCaixa.FREE;
    freeandnil(Self.ObjDatasource);
    FreeAndNil(self.objquerypesquisa);

//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCONTAGERENCIALFLUXOCAIXA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCONTAGERENCIALFLUXOCAIXA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjContaGerencialFluxoCaixa.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjContaGerencialFluxoCaixa.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
//CODIFICA GETSESUBMITS


procedure TObjCONTAGERENCIALFLUXOCAIXA.EdtContagerencialExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Contagerencial.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Contagerencial.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Contagerencial.GET_NOME;
End;
procedure TObjCONTAGERENCIALFLUXOCAIXA.EdtContagerencialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FCONTAGER:TFCONTAGER;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCONTAGER:=TFCONTAGER.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Contagerencial.Get_Pesquisa,Self.Contagerencial.Get_TituloPesquisa,FContager)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Contagerencial.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Contagerencial.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Contagerencial.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCONTAGER);
     End;
end;
procedure TObjCONTAGERENCIALFLUXOCAIXA.EdtFluxoCaixaExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.FluxoCaixa.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.FluxoCaixa.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.FluxoCaixa.GET_NOME;
End;
procedure TObjCONTAGERENCIALFLUXOCAIXA.EdtFluxoCaixaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.FluxoCaixa.Get_Pesquisa,Self.FluxoCaixa.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FluxoCaixa.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.FluxoCaixa.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FluxoCaixa.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjCONTAGERENCIALFLUXOCAIXA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCONTAGERENCIALFLUXOCAIXA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



procedure TObjCONTAGERENCIALFLUXOCAIXA.RetornaContas(Pfluxo: string);
begin
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select tabCONTAGERENCIALFLUXOCAIXA.codigo,tabCONTAGERENCIALFLUXOCAIXA.contagerencial,tabcontager.nome');
          sql.add('from tabCONTAGERENCIALFLUXOCAIXA join tabcontager on tabCONTAGERENCIALFLUXOCAIXA.contagerencial=tabcontager.codigo');
          sql.add('where FluxoCaixa='+Pfluxo);
          open;
     End;

end;

procedure TObjCONTAGERENCIALFLUXOCAIXA.GeraPlanilha;
var
path:string;
datainicial,datafinal:string;
pdata:tdate;
linha,cont,diasuteis:integer;
pprevisaovenda:currency;
pportador:string;
LinhaCR,LInhaCheques,LinhaPrevisaoCR,LinhadaData,LinhaCP:String;
psaldocaixasebancos:currency;
ObjqueryLocal:tibquery;
begin
     LinhadaData:='2';
     LinhaPrevisaoCR:='4';
     LInhaCheques:='5';
     LinhaCR:='6';
     LInhaCP:='10';

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;
          Grupo04.Enabled:=true;

          
          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;
          edtgrupo03.EditMask:='';
          edtgrupo04.EditMask:='';

          LbGrupo01.caption:='Data Inicial';
          LbGrupo02.caption:='Data Final';
          LbGrupo03.caption:='Previs�o de Venda';
          LbGrupo04.caption:='Portador de Cheques Pr�';

          showmodal;

          if (tag=0)
          then exit;

          if (Validadata(1,datainicial,false)=false)
          then exit;

          if (Validadata(2,datafinal,false)=false)
          then exit;

          Try
            pprevisaovenda:=strtocurr(edtgrupo03.text);
          Except
                mensagemerro('Valor Inv�lido para previs�o de venda');
                exit;
          end;

          pportador:=edtgrupo04.text;

          if (pportador<>'')
          Then Begin
                    if (ObjLanctoPortadorGlobal.Portador.LocalizaCodigo(pportador)=false)
                    then Begin
                              MensagemErro('Portador de cheques n�o localizado');
                              exit;
                   End;
          End;

     End;

     Try
        ObjqueryLocal:=tibquery.Create(nil);
        ObjqueryLocal.database:=FdataModulo.IBDatabase;
     Except
           MensagemErro('Erro na tentativa de Criar a Query Local');
           exit;
     End;

Try     


     diasuteis:=0;



     pdata:=strtodate(datainicial);
     while (pdata<=strtodate(datafinal)) do
     Begin

          if (DayOfWeek(pdata)<>1)and(DayOfWeek(pdata)<>7)
          Then inc(diasuteis,1);

          pdata:=IncDay(pdata,1);

     End;

     
     path:=ExtractFilePath(application.ExeName);

     if (path[length(path)]<>'\')
     Then path:=path+'\';

     path:=path+'fluxodecaixa.xls';

     if (FileExists(path)=false)
     then Begin
               mensagemErro('Arquivo n�o encontrado: '+path);
               exit;
     End;

     if (FInterageExcel.AbreArquivoExcel(path)=false)
     then exit;

     //primeiro escrevendo a previs�o

     FInterageExcel.Escreve('C1',floattostr(pprevisaovenda));

     //****************Contas a receber**************************


     //Somando o Saldo dos Caixas e Bancos
     psaldocaixasebancos:=0;
     With Self.ObjqueryPesquisa do
     begin
             close;
             SQL.clear;
             SQL.add('Select codigo from tabportador where classificacao=''B'' or classificacao=''C''');
             open;
             while not(eof) do
             Begin
                  psaldocaixasebancos:=psaldocaixasebancos+strtocurr(ObjLanctoPortadorGlobal.portador.proc_SaldoAnterior(fieldbyname('codigo').asstring,datainicial));
                  next;
             End;
      End;
     FInterageExcel.Escreve('C1',formatfloat('#####0.00',pprevisaovenda));
     FInterageExcel.Escreve('F1',formatfloat('#####0.00',psaldocaixasebancos));

     FInterageExcel.Escreve('B'+LinhaPrevisaoCR,'Vendas a Vista');
     FInterageExcel.Escreve('B'+LInhaCheques,'Cheques P. a Receber');
     FInterageExcel.Escreve('B'+LinhaCR,'Duplicatas a Receber');
     pdata:=strtodate(datainicial);
     cont:=3;//coluna C
     while (pdata<=strtodate(datafinal)) do
     Begin
          FInterageExcel.Escreve(FInterageExcel.RetornaColuna(cont)+LinhadaData,formatdatetime('mm/dd/yyyy',pdata));

          //Valor da Previsao por dia uteis
          if (DayOfWeek(pdata)<>1)and(DayOfWeek(pdata)<>7)//s� escreve se for de segunda a sexta
          Then FInterageExcel.Escreve(FInterageExcel.RetornaColuna(cont)+LinhaPrevisaoCR,formatfloat('#####0.00',(pprevisaovenda/diasuteis)));//previs�o de venda

          With Self.ObjqueryPesquisa do
          begin

             //soma dos cheques do portador de cheques no dia
             close;
             SQL.clear;
             SQL.add('Select sum(tabchequesportador.valor) as SOMA');
             SQL.add('from tabchequesportador');
             SQL.add('where not(ChequedoPortador=''S'')');
             SQL.add('and TabChequesPortador.vencimento='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
             SQL.add('and tabchequesportador.portador='+PPortador);
             open;

             if (fieldbyname('soma').ascurrency>0)
             Then FInterageExcel.Escreve(FInterageExcel.RetornaColuna(cont)+LInhaCheques,formatfloat('#####0.00',fieldbyname('soma').ascurrency));//Cheques do dia

             //contas a receber do dia
             close;
             SQL.clear;
             SQL.add('Select sum(tabpendencia.saldo) as SOMA');
             SQL.add('from tabpendencia');
             SQL.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
             sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
             SQL.add('where tabcontager.tipo=''C'' ');
             SQL.add('and tabpendencia.vencimento='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
             open;

             if (fieldbyname('soma').ascurrency>0)
             Then FInterageExcel.Escreve(FInterageExcel.RetornaColuna(cont)+LinhaCR,formatfloat('#####0.00',fieldbyname('soma').ascurrency));

          End;

          pdata:=IncDay(pdata,1);
          cont:=cont+1;
     End;

     //********************CONTAS A PAGAR**************************************
     //Selecionando as contas gerenciais por ordem

     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select codigo,nome,detalhacredordevedor from tabfluxocaixa order by ordem');
          open;

          Linha:=strtoint(Linhacp);

          while not(eof) do
          Begin
               //para cada linha do fluxo seleciono as contas gerenciais
               FInterageExcel.Escreve('B'+inttostr(linha),fieldbyname('nome').asstring);

               if (fieldbyname('detalhacredordevedor').asstring='N')
               Then Begin 
                         //Se nao for detalhar toda a soma vai numa unica linha

                         pdata:=strtodate(datainicial);
                         cont:=3;//coluna C
                         while (pdata<=strtodate(datafinal)) do
                         Begin
                              //selecionando a soma do Saldo das COntas que vencem nesse dia
                              //e pertencem as contas gerenciais dessa linha
                              Self.Objquery.close;
                              Self.Objquery.sql.clear;
                              Self.Objquery.sql.add('Select sum(tabpendencia.saldo) as SOMA from tabpendencia');
                              Self.Objquery.sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                              Self.Objquery.sql.add('where tabpendencia.vencimento='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
                              Self.Objquery.sql.add('and Tabtitulo.contagerencial in (');
                              Self.Objquery.sql.add('Select contagerencial from tabcontagerencialfluxocaixa where fluxocaixa='+fieldbyname('codigo').asstring+')');
                              Self.Objquery.open;

                              if (Self.Objquery.fieldbyname('soma').ascurrency>0)
                              Then FInterageExcel.Escreve(FInterageExcel.RetornaColuna(cont)+inttostr(linha),formatfloat('#####0.00',Self.Objquery.fieldbyname('soma').ascurrency));
                              
                              pdata:=IncDay(pdata,1);
                              cont:=cont+1;
                         End;
               End
               Else Begin
                         //Primeiro seleciono os fornecedores que tem saldo
                         //e estao ligados a estas contagerenciais
                         Self.Objquery.close;
                         Self.Objquery.sql.clear;
                         Self.Objquery.sql.add('Select tabtitulo.credordevedor,tabtitulo.codigocredordevedor,');
                         Self.Objquery.sql.add('sum(tabpendencia.saldo) as SOMA');
                         Self.Objquery.sql.add('from tabpendencia');
                         Self.Objquery.sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                         Self.Objquery.sql.add('where tabpendencia.vencimento>='+#39+formatdatetime('mm/dd/yyyy',strtodate(datainicial))+#39);
                         Self.Objquery.sql.add('and   tabpendencia.vencimento<='+#39+formatdatetime('mm/dd/yyyy',strtodate(datafinal))+#39);
                         Self.Objquery.sql.add('and Tabtitulo.contagerencial in');
                         Self.Objquery.sql.add('(');
                         Self.Objquery.sql.add('   Select contagerencial from tabcontagerencialfluxocaixa where fluxocaixa='+fieldbyname('codigo').asstring);
                         Self.Objquery.sql.add(')');
                         Self.Objquery.sql.add('group by tabtitulo.credordevedor,tabtitulo.codigocredordevedor');
                         Self.Objquery.sql.add('having sum(tabpendencia.saldo)>0');
                         Self.Objquery.sql.add('order by credordevedor,codigocredordevedor');
                         Self.Objquery.open;

                         //percorro um a um para cada um percorro as datas
                         while not(Self.Objquery.eof) do
                         Begin
                              linha:=linha+1;
                              //escrevendo o nome do Fornecedor(credor/devedor)
                              FInterageExcel.Escreve('B'+inttostr(linha),ObjLanctoPortadorGlobal.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.Objquery.fieldbyname('credordevedor').asstring,Self.Objquery.fieldbyname('codigocredordevedor').asstring));

                              pdata:=strtodate(datainicial);
                              cont:=3;//coluna C
                              while (pdata<=strtodate(datafinal)) do
                              Begin
                                   //selecionando a soma do Saldo das COntas que vencem nesse dia
                                   //e pertencem as contas gerenciais dessa linha
                                   //e sao do credor devedor atual

                                   objquerylocal.close;
                                   objquerylocal.sql.clear;
                                   objquerylocal.sql.add('Select sum(tabpendencia.saldo) as SOMA from tabpendencia');
                                   objquerylocal.sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                                   objquerylocal.sql.add('where tabpendencia.vencimento='+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
                                   objquerylocal.sql.add('and Tabtitulo.contagerencial in (');
                                   objquerylocal.sql.add('Select contagerencial from tabcontagerencialfluxocaixa where fluxocaixa='+fieldbyname('codigo').asstring+')');
                                   objquerylocal.sql.add('and tabtitulo.credordevedor='+Self.Objquery.fieldbyname('credordevedor').asstring);
                                   objquerylocal.sql.add('and tabtitulo.codigocredordevedor='+Self.Objquery.fieldbyname('codigocredordevedor').asstring);
                                   objquerylocal.open;

                                   if (objquerylocal.fieldbyname('soma').ascurrency>0)
                                   Then FInterageExcel.Escreve(FInterageExcel.RetornaColuna(cont)+inttostr(linha),formatfloat('#####0.00',objquerylocal.fieldbyname('soma').ascurrency));

                                   pdata:=IncDay(pdata,1);
                                   cont:=cont+1;
                              End;//while das datas
                              Self.Objquery.next;
                         End;//while dos fornecedores
               End;//else detalha por fornecedor

               linha:=linha+1;


              next;
          End;//WHILE DAS LINHAS DA PARTE DE A PAGAR

          //previsao de compras
          CLOSE;
          SQL.clear;
          sql.add('Select * from tabprevisaofinanceira');
          sql.add('where data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(datainicial))+#39);
          sql.add('and   data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(datafinal))+#39);
          sql.add('AND DEBITO_CREDITO=''D'' ');
          open;

          FInterageExcel.Escreve('B'+inttostr(linha),'Previs�o de Compras');
          while not(eof) do
          Begin
               linha:=linha+1;
               FInterageExcel.Escreve('B'+inttostr(linha),fieldbyname('historico').asstring);

               pdata:=strtodate(datainicial);
               cont:=3;//coluna C
               while (pdata<=strtodate(datafinal)) do
               Begin
                    if (datetostr(pdata)=fieldbyname('data').asstring)
                    Then Begin
                              FInterageExcel.Escreve(FInterageExcel.RetornaColuna(cont)+inttostr(linha),formatfloat('#####0.00',fieldbyname('valor').ascurrency));
                              break;//ja encontrou a data sai desse while
                    End;

                   pdata:=IncDay(pdata,1);
                   cont:=cont+1;
               End;//while ddata

          
               next;
          End;//while da previsao


     End;


     MensagemAviso('Concluido');

Finally
       freeandnil(Objquerylocal);
End;

end;

procedure TObjCONTAGERENCIALFLUXOCAIXA.RastrearContas;
var
datainicial,datafinal:string;
begin
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;

          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;

          LbGrupo01.caption:='Data Inicial';
          LbGrupo02.caption:='Data Final';

          showmodal;

          if (tag=0)
          then exit;

          if (Validadata(1,datainicial,false)=false)
          then exit;

          if (Validadata(2,datafinal,false)=false)
          then exit;
     End;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select tabtitulo.contagerencial,tabcontager.nome');
          sql.add('from tabpendencia');
          sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          sql.add('where tabpendencia.vencimento>='+#39+formatdatetime('mm/dd/yyyy',strtodate(datainicial))+#39);
          sql.add('and   tabpendencia.vencimento<='+#39+formatdatetime('mm/dd/yyyy',strtodate(datafinal))+#39);
          sql.add('and Tabcontager.tipo=''D'' ');//as a receber estao todas agrupadas
          sql.add('and tabtitulo.contagerencial not in');
          sql.add('(select contagerencial from tabcontagerencialfluxocaixa)');
          sql.add('group by tabtitulo.contagerencial,tabcontager.nome');
          open;
          last;
          if (recordcount=0)
          then Begin
                    MensagemAviso('N�o foi encontrado nenhuma Conta Gerencial que vence no per�odo que n�o esteja configurada');
                    exit;
          End;
          first;
          FmostraStringList.Memo.Lines.clear;
          FmostraStringList.Memo.Lines.add('CONTAS GERENCIAIS A PAGAR QUE N�O EST�O NO FLUXO DE CAIXA');
          while not(eof) do
          Begin
               FmostraStringList.Memo.Lines.add(fieldbyname('contagerencial').asstring+' - '+fieldbyname('nome').asstring);
               next;
          End;
          FmostraStringList.ShowModal;
     End;
end;

end.



