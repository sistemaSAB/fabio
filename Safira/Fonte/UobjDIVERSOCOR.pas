unit UobjDIVERSOCOR;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,
UOBJDIVERSO,UOBJCOR;

Type
   TObjDIVERSOCOR=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Diverso:TOBJDIVERSO;
                Cor:TOBJCOR;

                Constructor Create;
                Destructor  Free;
                function RetornaEstoque: String;
                
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_PorcentagemAcrescimo(parametro: string);
                Function Get_PorcentagemAcrescimo: string;
                Procedure Submit_AcrescimoExtra(parametro: string);
                Function Get_AcrescimoExtra: string;

                Procedure Submit_classificacaofiscal(parametro: string);
                Function Get_classificacaofiscal: string;

                Function Get_PorcentagemAcrescimoFinal: string;

                procedure Submit_Estoque(parametro:string);
                function Get_Estoque:String;

                procedure Submit_EstoqueMinimo(parametro:string);
                function Get_EstoqueMinimo:string;



                procedure EdtDiversoExit(Sender: TObject;LABELNOME:TLABEL);

                procedure EdtDiversoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);


                procedure EdtCorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;

                procedure EdtCor_PorcentagemCor_Exit(Sender: TObject;Var PEdtCodigo:TEdit;  LABELNOME:TLABEL; EdtPorcentagemCor:TEdit);
                procedure EdtCorKeyDown(Sender: TObject;var EdtCodigo : TEdit;  var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);Overload;

                Procedure ResgataCorDiverso(PDiverso:string);
                Function CadastraDiversoEmTodasAsCores(PDiverso:string):Boolean;

                Function Localiza_Diverso_Cor(Pdiverso,Pcor:string):boolean;

                procedure EdtCorDeDiversoKeyDown(Sender: TObject;  var PEdtCodigo: TEdit; var Key: Word; Shift: TShiftState;LABELNOME: Tlabel);
                procedure EdtDiversoDisponivelNaCorKeyDown(Sender: TObject; var PEdtCodigo: TEdit; PCor: string; var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
                function Get_DiversoNaCor(PCor: string): TStringList;
                Procedure Relatorioestoque;

                procedure EdtDiversoCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);

                procedure DiminuiEstoque(quantidade,diversocor:string);
                function AumentaEstoque(quantidade,diversocor:string):Boolean;
  
         Private
               Objquery:Tibquery;
               ObjqueryEstoque:Tibquery;
               ObjQueryPesquisa:TIBQuery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               PorcentagemAcrescimo:string;
               AcrescimoExtra:string;
               classificacaofiscal:string;
               PorcentagemAcrescimoFinal:string;
               Estoque:string;
               EstoqueMinimo:string;
               
               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;


   End;


implementation
uses Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UEscolheCor, Forms, UReltxtRDPRINT,rdprint, UDIVERSO;

{ TTabTitulo }


Function  TObjDIVERSOCOR.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If(FieldByName('Diverso').asstring<>'')
        Then Begin
                 If (Self.Diverso.LocalizaCodigo(FieldByName('Diverso').asstring)=False)
                 Then Begin
                          Messagedlg('Diverso N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Diverso.TabelaparaObjeto;
        End;
        If(FieldByName('Cor').asstring<>'')
        Then Begin
                 If (Self.Cor.LocalizaCodigo(FieldByName('Cor').asstring)=False)
                 Then Begin
                          Messagedlg('Cor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Cor.TabelaparaObjeto;
        End;
        Self.PorcentagemAcrescimo:=fieldbyname('PorcentagemAcrescimo').asstring;

        Self.AcrescimoExtra:=fieldbyname('AcrescimoExtra').asstring;
        Self.classificacaofiscal:=fieldbyname('classificacaofiscal').asstring;
        Self.PorcentagemAcrescimoFinal:=fieldbyname('PorcentagemAcrescimoFinal').asstring;
        self.Estoque:=fieldbyname('Estoque').AsString;
        self.EstoqueMinimo := Objquery.fieldbyname('EstoqueMinimo').AsString;
        result:=True;
     End;
end;


Procedure TObjDIVERSOCOR.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Diverso').asstring:=Self.Diverso.GET_CODIGO;
        ParamByName('Cor').asstring:=Self.Cor.GET_CODIGO;
        ParamByName('PorcentagemAcrescimo').asstring:=virgulaparaponto(Self.PorcentagemAcrescimo);

        ParamByName('AcrescimoExtra').asstring:=virgulaparaponto(Self.AcrescimoExtra);
        ParamByName('classificacaofiscal').asstring:=Self.classificacaofiscal;
        //ParamByName('Estoque').AsString:= virgulaparaponto(Self.Estoque);
        ParamByName('estoqueminimo').AsString:=virgulaparaponto(Self.EstoqueMinimo);
  End;
End;

//***********************************************************************

function TObjDIVERSOCOR.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjDIVERSOCOR.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Diverso.ZerarTabela;
        Cor.ZerarTabela;
        PorcentagemAcrescimo:='';

        AcrescimoExtra:='';
        classificacaofiscal:='';
        PorcentagemAcrescimoFinal:='';
        Estoque:='';
        EstoqueMinimo := '';

     End;
end;

Function TObjDIVERSOCOR.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjDIVERSOCOR.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Diverso.LocalizaCodigo(Self.Diverso.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Diverso n�o Encontrado!';
      If (Self.Cor.LocalizaCodigo(Self.Cor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Cor n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjDIVERSOCOR.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.Diverso.Get_Codigo<>'')
        Then Strtoint(Self.Diverso.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Diverso';
     End;
     try
        If (Self.Cor.Get_Codigo<>'')
        Then Strtoint(Self.Cor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Cor';
     End;
     try
        Strtofloat(Self.PorcentagemAcrescimo);
     Except
           Mensagem:=mensagem+'/PorcentagemAcrescimo';
     End;
     try
        Strtofloat(Self.AcrescimoExtra);
     Except
           Mensagem:=mensagem+'/AcrescimoExtra';
     End;
     
     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjDIVERSOCOR.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjDIVERSOCOR.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjDIVERSOCOR.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=False;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro DIVERSOCOR vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Diverso,Cor,PorcentagemAcrescimo,AcrescimoExtra');
           SQL.ADD(' ,PorcentagemAcrescimoFinal,classificacaofiscal,Estoque,EstoqueMinimo');
           SQL.ADD(' from  TABDIVERSOCOR');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjDIVERSOCOR.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjDIVERSOCOR.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjDIVERSOCOR.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryEstoque:=TIBQuery.create(nil);
        Self.ObjqueryEstoque.Database:=FDataModulo.IbDatabase;


        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjQueryPesquisa;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Diverso:=TOBJDIVERSO.create;
        Self.Cor:=TOBJCOR.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABDIVERSOCOR(Codigo,Diverso,Cor,PorcentagemAcrescimo');
                InsertSQL.add(' ,AcrescimoExtra,classificacaofiscal');//,Estoque');
                InsertSql.Add(',EstoqueMinimo');
                InsertSql.Add(' )');
                InsertSQL.add('values (:Codigo,:Diverso,:Cor,:PorcentagemAcrescimo');
                InsertSQL.add(' ,:AcrescimoExtra,:classificacaofiscal');//,:Estoque');
                InsertSql.Add(',:EstoqueMinimo');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABDIVERSOCOR set Codigo=:Codigo,Diverso=:Diverso');
                ModifySQL.add(',Cor=:Cor,PorcentagemAcrescimo=:PorcentagemAcrescimo');
                ModifySQL.add(',AcrescimoExtra=:AcrescimoExtra');
                ModifySQL.add(',classificacaofiscal=:classificacaofiscal');//,Estoque=:Estoque');
                ModifySQl.Add(',EstoqueMinimo=:EstoqueMinimo');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABDIVERSOCOR where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjDIVERSOCOR.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjDIVERSOCOR.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewDIVERSOCOR');
     Result:=Self.ParametroPesquisa;
end;

function TObjDIVERSOCOR.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Diverso por Cor ';
end;


function TObjDIVERSOCOR.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENDIVERSOCOR,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENDIVERSOCOR,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjDIVERSOCOR.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ObjqueryEstoque);
    Freeandnil(Self.ObjQueryPesquisa);
    Freeandnil(Self.ObjDataSource);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Diverso.FREE;
    Self.Cor.FREE;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjDIVERSOCOR.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjDIVERSOCOR.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjDiversoCor.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjDiversoCor.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjDiversoCor.Submit_PorcentagemAcrescimo(parametro: string);
begin
        Self.PorcentagemAcrescimo:=Parametro;
end;
function TObjDiversoCor.Get_PorcentagemAcrescimo: string;
begin
        Result:=Self.PorcentagemAcrescimo;
end;
procedure TObjDiversoCor.Submit_AcrescimoExtra(parametro: string);
begin
        Self.AcrescimoExtra:=Parametro;
end;
function TObjDiversoCor.Get_AcrescimoExtra: string;
begin
        Result:=Self.AcrescimoExtra;
end;

procedure TObjDiversoCor.Submit_classificacaofiscal(parametro: string);
begin
        Self.classificacaofiscal:=Parametro;
end;
function TObjDiversoCor.Get_classificacaofiscal: string;
begin
        Result:=Self.classificacaofiscal;
end;



function TObjDiversoCor.Get_PorcentagemAcrescimoFinal: string;
begin
        Result:=Self.PorcentagemAcrescimoFinal;
end;
procedure TObjDIVERSOCOR.EdtDiversoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Diverso.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Diverso.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Diverso.Get_Descricao ;
End;
procedure TObjDIVERSOCOR.EdtDiversoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FDiversoLocal: TFDIVERSO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FDiversoLocal := TFDIVERSO.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('select * from tabdiverso where ativo=''S'' ',Self.Diverso.Get_TituloPesquisa,FDiversoLocal)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Diverso.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Diverso.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Diverso.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeandNil(FDiversoLocal);
     End;
end;





procedure TObjDIVERSOCOR.EdtDiversoCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,Self.Get_TituloPesquisa,Nil)=True)
            Then Begin                                                                         
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;


                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then begin
                                            if (Self.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring))
                                            Then Begin
                                                      Self.TabelaparaObjeto;
                                                      LABELNOME.caption:=completapalavra(Self.Diverso.Get_Descricao,50,' ')+' COR: ' +Self.Cor.Get_Descricao;
                                            End;
                                 End;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;





procedure TObjDIVERSOCOR.EdtCorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Cor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Cor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Cor.Get_Descricao;
End;

procedure TObjDIVERSOCOR.EdtCorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cor.Get_Pesquisa,Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjDIVERSOCOR.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJDIVERSOCOR';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjDIVERSOCOR.ResgataCorDiverso(PDiverso: string);
begin
     With Self.ObjqueryPesquisa do
     Begin
          close;
          SQL.clear;
          SQL.ADD('Select TDC.Cor,TabCor.Descricao as NomeCor,TDC.PorcentagemAcrescimo,TDC.AcrescimoExtra');
          SQL.ADD(' ,TDC.PorcentagemAcrescimoFinal,TDC.codigo,TDC.classificacaofiscal,TDC.Estoque');
          SQL.ADD(' from  TabDiversoCor TDC');
          SQL.ADD(' join tabCor on TDC.Cor=Tabcor.codigo');

          SQL.ADD(' where Diverso='+pDiverso);

          if (PDiverso='')
          Then exit;
          open;
     End;

end;

procedure TObjDIVERSOCOR.EdtCor_PorcentagemCor_Exit(Sender: TObject;
  var PEdtCodigo: TEdit; LABELNOME: TLABEL; EdtPorcentagemCor: TEdit);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Cor.LocalizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.text:='';
               exit;
     End;
     Self.Cor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Cor.Get_Descricao;
     EdtPorcentagemCor.Text:=Self.Cor.Get_PorcentagemAcrescimo;
     PEdtCodigo.text:=Self.Cor.Get_Codigo;

end;

procedure TObjDIVERSOCOR.EdtCorKeyDown(Sender: TObject;
  var EdtCodigo: TEdit; var Key: Word; Shift: TShiftState;
  LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cor.Get_Pesquisa,Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 EdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjDIVERSOCOR.CadastraDiversoEmTodasAsCores(
  PDiverso: string): Boolean;
Var   Cont : Integer;
      PPOrcentagemAcrescimo:string;
begin
     Result:=false;
     if (Self.Diverso.LocalizaCodigo(PDiverso)=false)then
     Begin
          MensagemErro('Diverso n�o encontrado.');
          Result:=false;
          exit;
     end;

     With  Self.ObjqueryPesquisa  do
     Begin
          Close;
          SQl.Clear;
          Sql.Add('Select Codigo,Referencia, Descricao from TabCor');
          Open;
          First;

          FEscolheCor.CheckListBoxCor.Clear;
          FEscolheCor.CheckListBoxCodigoCor.Clear;
          While not (Eof) do
          Begin
               FEscolheCor.CheckListBoxCor.Items.Add(fieldbyname('Referencia').AsString+' - '+fieldbyname('Descricao').AsString);
               FEscolheCor.CheckListBoxCodigoCor.Items.Add(fieldbyname('Codigo').AsString);
          Next;
          end;
          FEscolheCor.ShowModal;

          if FEscolheCor.Tag = 0 then
          Begin
               Result:=false;
               exit;
          end;

          // Cadastrando os cores
          for Cont:=0 to FEscolheCor.CheckListBoxCor.Items.Count-1 do
          Begin
               if FEscolheCor.CheckListBoxCodigoCor.Checked[Cont]=true then
               Begin
                    Self.Cor.LocalizaCodigo(FEscolheCor.CheckListBoxCodigoCor.Items.Strings[Cont]);
                    Self.Cor.TabelaparaObjeto;
                    PPOrcentagemAcrescimo:=Self.Cor.Get_PorcentagemAcrescimo;

                    Self.ZerarTabela;
                    Self.Status:=dsInsert;
                    Self.Submit_Codigo(Get_NovoCodigo);
                    Self.Diverso.Submit_Codigo(PDiverso);
                    Self.Cor.Submit_Codigo(FEscolheCor.CheckListBoxCodigoCor.Items.Strings[Cont]);
                    Self.Submit_AcrescimoExtra('0');
                    Self.Submit_classificacaofiscal('');
                    Self.Submit_PorcentagemAcrescimo(PPOrcentagemAcrescimo);
                    if (Self.Salvar(false)=false)then
                    Begin
                         MensagemErro('Erro ao tentar Salvar a FerragmeCor');
                         Result:=false;
                         exit;
                    end;
               end;
          end;

          Result:=true;
     end;
end;
function TObjDIVERSOCOR.Localiza_Diverso_Cor(Pdiverso,
  Pcor: string): boolean;
begin
       if (Pdiverso='')
       or (Pcor='')
       Then Begin
                 Messagedlg('Escolha o Diverso e a Cor',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Diverso,Cor,PorcentagemAcrescimo,AcrescimoExtra');
           SQL.ADD(' ,PorcentagemAcrescimoFinal,classificacaofiscal,Estoque,EstoqueMinimo');
           SQL.ADD(' from  TabDiversoCor');
           SQL.ADD(' WHERE Diverso='+Pdiverso+'  and Cor='+Pcor);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;
procedure TObjDiversoCOR.EdtCorDeDiversoKeyDown(Sender: TObject; var PEdtCodigo: TEdit; var Key: Word; Shift: TShiftState;
  LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cor.Get_PesquisaCorDiverso,Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjDiversoCOR.EdtDiversoDisponivelNaCorKeyDown(
  Sender: TObject; var PEdtCodigo: TEdit; PCor: string; var Key: Word;
  Shift: TShiftState; LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_DiversoNaCor(PCor),Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjDiversoCOR.Get_DiversoNaCor(PCor: string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select Distinct(TabDiverso.Referencia),TabDiverso.DESCRICAO, TabDiverso.Unidade,');
     Self.ParametroPesquisa.add('TabDiversoCor.Codigo');
     Self.ParametroPesquisa.add('from TabDiversoCor');
     Self.ParametroPesquisa.add('Join TabDiverso on TabDiverso.Codigo = TabDiversoCor.Diverso');
     if (PCor <> '')then
     Self.ParametroPesquisa.add('Where TabDiversoCor.Cor = '+PCor);

     Result:=Self.ParametroPesquisa;

end;



procedure TObjDIVERSOCOR.Relatorioestoque;
var
Pgrupo:string;
PsomaEstoque:integer;
PsomaValorTotal:Currency;
pdata:string;
Produto:string;
begin
     With FfiltroImp do
     begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.caption:='Grupo Diverso';
          edtgrupo01.OnKeyDown:=Self.Diverso.EdtGrupoDiversoKeyDown;
          edtgrupo01.Color:=$005CADFE;

          Grupo02.Enabled:=True;
          lbgrupo02.Caption:='Diverso';
          edtgrupo02.OnKeyDown:=self.Diverso.EdtDiversoKeyDown;
          edtgrupo02.Color:=$005CADFE;

          Grupo03.Enabled:=True;
          edtgrupo03.EditMask:=MascaraData;
          LbGrupo03.caption:='Data Limite';

          Showmodal;

          if (tag=0)
          then exit;

          pGrupo:='';
          if (edtgrupo01.Text<>'')
          Then Begin
                    if (self.Diverso.GrupoDiverso.LocalizaCodigo(edtgrupo01.text)=False)
                    Then Begin
                              MensagemErro('Grupo n�o encontrado');
                              exit;
                    End;
                    self.Diverso.GrupoDiverso.TabelaparaObjeto;
                    pgrupo:=self.Diverso.GrupoDiverso.get_codigo;
          End;
          produto:='';
          if(edtgrupo02.Text<>'')
          then Produto:=edtgrupo02.Text;

          pdata:='';
          if (comebarra(edtgrupo03.text)<>'')
          Then if (Validadata(3,pdata,False)=False)
               then exit;


     End;

     With Self.ObjQueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select');
          sql.add('TabDiverso.codigo as DIVERSO,');
          sql.add('TabDiverso.REferencia as REFDIVERSO,');
          sql.add('TabDiverso.descricao as NOMEDIVERSO,');
          sql.add('Tabcor.codigo as COR,');
          sql.add('Tabcor.Referencia as REFCOR,');
          sql.add('tabcor.descricao as NOMECOR,');
          sql.add('tabdiverso.precocusto,');

          sql.add('coalesce(sum(TabEstoque.quantidade),0) as estoque,');//alterado aqui
          sql.add('coalesce(sum(TabEstoque.quantidade),0)*tabdiverso.precocusto as VALORTOTAL');


          sql.add('from tabdiversocor');
          sql.add('join tabdiverso on tabdiversocor.diverso=tabdiverso.codigo');
          sql.add('join tabcor on Tabdiversocor.cor=tabcor.codigo');

          if (pdata<>'')
          then Begin
                    sql.add('left join tabestoque on (tabestoque.diversocor=TabdiversoCor.codigo');
                    sql.add('and tabestoque.data<=:pdata)');

                    ParamByName('pdata').asdatetime:=strtodate(pdata);
          End
          Else sql.add('left join tabestoque on tabestoque.diversocor=TabdiversoCor.codigo');


          Sql.add('Where Tabdiverso.codigo<>-500');

          if (Pgrupo<>'')
          Then Sql.add('and TabDiverso.GrupoDiverso='+pgrupo);

          if(Produto<> '')
          then sql.Add('and tabdiverso.codigo='+produto);

          Sql.add('group by Tabdiverso.codigo,');
          sql.add('Tabdiverso.REferencia,');
          sql.add('Tabdiverso.descricao,');
          sql.add('Tabcor.codigo,');
          sql.add('Tabcor.Referencia,');
          sql.add('tabcor.descricao,');
          sql.add('tabdiverso.precocusto');

          Sql.add('order by TabDiverso.referencia,tabcor.referencia');

          
          open;

          if (Recordcount=0)
          then Begin
                    MensagemAviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;

          With FreltxtRDPRINT do
          begin
               ConfiguraImpressao;
               RDprint.FonteTamanhoPadrao:=S17cpp;
               RDprint.TamanhoQteColunas:=130;
               RDprint.Abrir;
               if (RDprint.Setup=False)
               then begin
                         RDprint.Fechar;
                         exit;
               end;
               LinhaLocal:=3;

               RDprint.ImpC(linhalocal,65,'RELAT�RIO DE ESTOQUE  - DIVERSOS',[negrito]);
               IncrementaLinha(2);

               if (Pgrupo<>'')
               then begin
                         RDprint.ImpF(linhalocal,1,'Grupo: '+Self.Diverso.GrupoDiverso.Get_Codigo+'-'+Self.Diverso.GrupoDiverso.Get_Nome,[negrito]);
                         IncrementaLinha(1);
               End;

               if (Pdata<>'')
               then Begin
                         VerificaLinha;
                         RDprint.Impf(linhalocal,1,'Data: '+pdata,[negrito]);
                         IncrementaLinha(1);
               end;


               IncrementaLinha(1);
               VerificaLinha;
               RDprint.Impf(linhalocal,1,CompletaPalavra('CODIGO',6,' ')+' '+
                                             CompletaPalavra('REF.',10,' ')+' '+
                                             CompletaPalavra('NOME DIVERSO',42,' ')+' '+
                                             CompletaPalavra('REF.COR',10,' ')+' '+
                                             CompletaPalavra('NOME COR',19,' ')+' '+
                                             CompletaPalavra_a_Esquerda('ESTOQUE',12,' ')+' '+
                                             CompletaPalavra_a_Esquerda('CUSTO',12,' ')+' '+
                                             CompletaPalavra_a_Esquerda('TOTAL',12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               IncrementaLinha(1);

               PsomaEstoque:=0;
               PsomaValorTotal:=0;

               While not(self.ObjQueryPesquisa.eof) do
               Begin
                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('diverso').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('refdiverso').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('nomediverso').asstring,42,' ')+' '+
                                             CompletaPalavra(fieldbyname('refcor').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('nomecor').asstring,19,' ')+' '+
                                             CompletaPalavra_a_Esquerda(fieldbyname('estoque').asstring,12,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('precocusto').asstring),12,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valortotal').asstring),12,' '));
                    IncrementaLinha(1);

                    PsomaEstoque:=PsomaEstoque+Fieldbyname('estoque').asinteger;
                    PsomaValorTotal:=PsomaValorTotal+Fieldbyname('valortotal').asfloat;


                    self.ObjQueryPesquisa.next;
               End;
               DesenhaLinha;
               VerificaLinha;
               RDprint.Impf(linhalocal,93,CompletaPalavra_a_Esquerda(inttostr(pSomaEstoque),12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(formata_valor(psomavalortotal),12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               RDprint.fechar;
          End;
     End;


end;


function TObjDIVERSOCOR.RetornaEstoque:String;
begin
        With Self.ObjQueryEstoque do
        begin
             close;
             sql.clear;
             sql.add('Select coalesce(sum(quantidade),0) as soma from tabestoque where diversocor='+Self.Codigo);
             open;
             result:=fieldbyname('soma').asstring;
        End;
end;

procedure TObjDIVERSOCOR.Submit_Estoque(parametro:string);
begin
    Self.Estoque:=parametro;
end;

function TObjDIVERSOCOR.Get_Estoque:string;
begin
    Result:=self.Estoque;
end;

procedure TObjDIVERSOCOR.DiminuiEstoque(quantidade,diversocor:string);
var
   ObjqueryEstoque:TIBQuery;
begin
   try
          ObjqueryEstoque:=TIBQuery.Create(nil);
          ObjqueryEstoque.Database:=FDataModulo.IBDatabase;
          with ObjqueryEstoque do
          begin
                Close;
                sql.Clear;
                sql.Add('update tabdiversocor set estoque=estoque+'+quantidade);
                SQL.Add('where codigo='+diversocor);
                ExecSQL;
          end;
   finally
          FreeAndNil(ObjqueryEstoque);
   end;

end;

function TObjDIVERSOCOR.AumentaEstoque(quantidade,diversocor:string):Boolean;
var
   ObjqueryEstoque:TIBQuery;
begin
   result:=True;
   try
          ObjqueryEstoque:=TIBQuery.Create(nil);
          ObjqueryEstoque.Database:=FDataModulo.IBDatabase;
          with ObjqueryEstoque do
          begin
                try
                    Close;
                    sql.Clear;
                    sql.Add('update tabdiversocor set estoque=estoque+'+quantidade);
                    SQL.Add('where codigo='+diversocor);
                    ExecSQL;
                except
                    Result:=False;
                end;
          end;
   finally
          FDataModulo.IBTransaction.CommitRetaining;
          FreeAndNil(ObjqueryEstoque);
   end;

end;

function TObjDIVERSOCOR.Get_EstoqueMinimo: string;
begin
  result := self.estoqueminimo;
end;

procedure TObjDIVERSOCOR.Submit_EstoqueMinimo(parametro: string);
begin
  self.estoqueminimo := parametro;
end;

end.



