unit USERVICO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, db, UObjSERVICO,
  UessencialGlobal, Tabs, UGRUPOSERVICO, IBQuery, UErrosDesativarMateriais, UpesquisaMenu;

type
  TFSERVICO = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    lbLbReferencia: TLabel;
    edtReferencia: TEdit;
    edtGrupoServico: TEdit;
    lbLbGrupoServico: TLabel;
    lbLbDescricao: TLabel;
    edtDescricao: TEdit;
    edtPrecoCusto: TEdit;
    lbLbPrecoCusto: TLabel;
    lbLbArredondamento: TLabel;
    edtArredondamento: TEdit;
    lbNomeGrupoServico: TLabel;
    grp1: TGroupBox;
    lbLbPorcentagemInstalado: TLabel;
    lbLbPorcentagemFornecido: TLabel;
    lbLbPorcentagemRetirado: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    edtPorcentagemRetirado: TEdit;
    edtPorcentagemFornecido: TEdit;
    edtPorcentagemInstalado: TEdit;
    grp2: TGroupBox;
    lb5: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    lb8: TLabel;
    lb9: TLabel;
    lb10: TLabel;
    edtPrecoVendaRetirado: TEdit;
    edtPrecoVendaFornecido: TEdit;
    edtPrecoVendaInstalado: TEdit;
    chkAtivo: TCheckBox;
    lbAtivoInativo: TLabel;
    btAjuda: TSpeedButton;
    lb11: TLabel;
    EdtCFOP: TEdit;
    lb12: TLabel;
    edtSTB: TEdit;
    lbNomeCfop: TLabel;
    lbSTB: TLabel;
    ComboTipoAliquota: TComboBox;
    lb13: TLabel;
    procedure edtPorcentagemInstaladoExit(Sender: TObject);
    procedure edtPorcentagemRetiradoExit(Sender: TObject);
    procedure edtPorcentagemFornecidoExit(Sender: TObject);
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtGrupoServicoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoServicoExit(Sender: TObject);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure edtPrecoCustoExit(Sender: TObject);
    procedure edtPrecoVendaInstaladoExit(Sender: TObject);
    procedure edtPrecoVendaFornecidoExit(Sender: TObject);
    procedure edtPrecoVendaRetiradoExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtGrupoServicoDblClick(Sender: TObject);
    procedure chkAtivoClick(Sender: TObject);
    procedure btAjudaClick(Sender: TObject);
    procedure edtGrupoServicoKeyPress(Sender: TObject; var Key: Char);
    procedure lbNomeGrupoServicoClick(Sender: TObject);
    procedure lbNomeGrupoServicoMouseLeave(Sender: TObject);
    procedure lbNomeGrupoServicoMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure edtPorcentagemInstaladoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemRetiradoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemFornecidoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPrecoCustoKeyPress(Sender: TObject; var Key: Char);
    procedure edtArredondamentoKeyPress(Sender: TObject; var Key: Char);
    procedure EdtCFOPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSTBKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSTBDblClick(Sender: TObject);
    procedure EdtCFOPDblClick(Sender: TObject);
    procedure ComboTipoAliquotaKeyPress(Sender: TObject; var Key: Char);
  private
         AlteraPrecoPeloCusto:boolean;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    procedure AbreComCodigo(parametro:string);
  end;

var
  FSERVICO: TFSERVICO;
  ObjSERVICO:TObjSERVICO;

implementation

uses Upesquisa, UessencialLocal, UDataModulo, UescolheImagemBotao,
  UGRUPOPERFILADO, Uprincipal, UAjuda;

{$R *.dfm}


procedure TFSERVICO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjSERVICO=Nil)
     Then exit;

     If (ObjSERVICO.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjSERVICO.free;
    self.Tag:=0;
end;

procedure TFSERVICO.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbCodigo.Caption:='0';
     chkAtivo.Checked:=True;
     lbAtivoInativo.Caption:='Ativo';
     //edtcodigo.text:=ObjSERVICO.Get_novocodigo;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjSERVICO.status:=dsInsert;
     EdtReferencia.setfocus;
     
     if (AlteraPrecoPeloCusto=True)
     Then Begin
              EdtPrecoVendaInstalado.Enabled:=false;
              EdtPrecoVendaRetirado.Enabled:=false;
              EdtPrecoVendaFornecido.Enabled:=false;
     End;

     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

end;

procedure TFSERVICO.btSalvarClick(Sender: TObject);
begin

     If ObjSERVICO.Status=dsInactive
     Then exit;

     If (EdtReferencia.Text<>'')then begin
         If (ObjServico.VerificaReferencia(ObjServico.Status, lbCodigo.Caption, EdtReferencia.Text)=true) then  
         Begin
             MensagemErro('Refer�ncia j� existe.');
             exit;
         end;
     end;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjSERVICO.salvar(true)=False)
     Then exit;

     lbCodigo.Caption:=ObjSERVICO.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     EdtPrecoVendaFornecido.Enabled:=false;
     EdtPrecoVendaInstalado.Enabled:=false;
     EdtPrecoVendaRetirado.Enabled:=false;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;


end;

procedure TFSERVICO.btAlterarClick(Sender: TObject);
begin
    If (ObjSERVICO.Status=dsinactive) and ( lbCodigo.Caption<>'')
    Then Begin
                habilita_campos(Self);
                ObjSERVICO.Status:=dsEdit;
                edtReferencia.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;

                if (AlteraPrecoPeloCusto=True)
                Then Begin
                         EdtPrecoVendaInstalado.Enabled:=false;
                         EdtPrecoVendaRetirado.Enabled:=false;
                         EdtPrecoVendaFornecido.Enabled:=false;
                End;
                
                ObjSERVICO.ReferenciaAnterior:=EdtReferencia.Text;
                btnovo.Visible :=false;
                 btalterar.Visible:=false;
                 btpesquisar.Visible:=false;
                 btrelatorio.Visible:=false;
                 btexcluir.Visible:=false;
                 btsair.Visible:=false;
                 btopcoes.Visible :=false;
          End;

end;

procedure TFSERVICO.btCancelarClick(Sender: TObject);
begin
     ObjSERVICO.cancelar;
     Self.limpaLabels;
     limpaedit(Self);

     desabilita_campos(Self);
     habilita_botoes(Self);
     EdtPrecoVendaFornecido.Enabled:=false;
     EdtPrecoVendaInstalado.Enabled:=false;
     EdtPrecoVendaRetirado.Enabled:=false;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;


end;

procedure TFSERVICO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjSERVICO.Get_pesquisa,ObjSERVICO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjSERVICO.status<>dsinactive
                                  then exit;

                                  If (ObjSERVICO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjSERVICO.ZERARTABELA;

                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;


                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
        EdtPrecoVendaFornecido.Enabled:=false;
        EdtPrecoVendaInstalado.Enabled:=false;
        EdtPrecoVendaRetirado.Enabled:=false;


end;

procedure TFSERVICO.btExcluirClick(Sender: TObject);
begin
     If (ObjSERVICO.status<>dsinactive) or ( lbCodigo.Caption='')
     Then exit;

     If (ObjSERVICO.LocalizaCodigo( lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjSERVICO.exclui( lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     EdtPrecoVendaFornecido.Enabled:=false;
     EdtPrecoVendaInstalado.Enabled:=false;
     EdtPrecoVendaRetirado.Enabled:=false;

     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFSERVICO.btRelatorioClick(Sender: TObject);
begin
//    ObjSERVICO.Imprime( lbCodigo.Caption);
end;

procedure TFSERVICO.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFSERVICO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFSERVICO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFSERVICO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
   if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('CADASTRO DE SERVI�OS');
         FAjuda.ShowModal;
    end;


    If Key=VK_Escape
    Then Self.Close;

end;

procedure TFSERVICO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFSERVICO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjSERVICO do
    Begin
        Submit_Codigo( lbCodigo.Caption);
        Submit_Referencia(edtReferencia.text);
        GrupoServico.Submit_codigo(edtGrupoServico.text);
        CFOP.Submit_CODIGO(EdtCFOP.Text);
        STB.Submit_CODIGO(edtSTB.Text);
        Submit_Descricao(edtDescricao.text);
        Submit_PrecoCusto(tira_ponto(edtPrecoCusto.text));
        Submit_PorcentagemInstalado(tira_ponto(edtPorcentagemInstalado.text));
        Submit_PorcentagemFornecido(tira_ponto(edtPorcentagemFornecido.text));
        Submit_PorcentagemRetirado(tira_ponto(edtPorcentagemRetirado.text));
        Submit_Arredondamento(tira_ponto(edtArredondamento.text));

        if(ComboTipoAliquota.ItemIndex=1)
        then Submit_Tipoaliquota('T');
        if(ComboTipoAliquota.ItemIndex=2)
        then Submit_Tipoaliquota('I');
        if(ComboTipoAliquota.ItemIndex=3)
        then Submit_Tipoaliquota('F');
        if(ComboTipoAliquota.ItemIndex=4)
        then Submit_Tipoaliquota('N');

         if(chkAtivo.Checked=True)
        then Submit_Ativo('S')
        else Submit_Ativo('N');

         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFSERVICO.LimpaLabels;
begin
    LbNomeGrupoServico.Caption:='';
    LbCodigo.Caption:='';
    lbNomeCfop.Caption:='';
    lbSTB.Caption:='';
end;

function TFSERVICO.ObjetoParaControles: Boolean;
begin
  Try
       With ObjSERVICO do
       Begin
            lbCodigo.Caption:=Get_Codigo;
            EdtReferencia.text:=Get_Referencia;
            EdtGrupoServico.text:=GrupoServico.Get_codigo;
            LbNomeGrupoServico.Caption:=GrupoServico.Get_Nome;
            EdtDescricao.text:=Get_Descricao;
            EdtPrecoCusto.text:=formata_valor(Get_PrecoCusto);
            EdtPorcentagemInstalado.text:=Get_PorcentagemInstalado;
            EdtPorcentagemFornecido.text:=Get_PorcentagemFornecido;
            EdtPorcentagemRetirado.text:=Get_PorcentagemRetirado;
            EdtArredondamento.text:=Get_Arredondamento;
            EdtPrecoVendaRetirado.Text:=Get_PrecoVendaRetirado;
            EdtPrecoVendaFornecido.Text:=Get_PrecoVendaFornecido;
            EdtPrecoVendaInstalado.Text:=Get_PrecoVendaInstalado;
            EdtCFOP.Text:=CFOP.Get_CODIGO;
            edtSTB.Text:=stb.Get_CODIGO;
            lbNomeCfop.Caption:= CFOP.Get_NOME;
            lbSTB.Caption:=stb.Get_DESCRICAO;
            if Get_Tipoaliquota=''
            then ComboTipoAliquota.ItemIndex:=0;
            if Get_Tipoaliquota='T'
            then ComboTipoAliquota.ItemIndex:=1;
            if Get_Tipoaliquota='I'
            then ComboTipoAliquota.ItemIndex:=2;
            if Get_Tipoaliquota='F'
            then ComboTipoAliquota.ItemIndex:=3;
            if Get_Tipoaliquota='N'
            then ComboTipoAliquota.ItemIndex:=4;

            if(Get_Ativo='S') then
            begin
                  chkAtivo.Checked:=True;
                  lbAtivoInativo.Caption:='Ativo';
            end
            else
            begin
                  chkAtivo.Checked:=False;
                  lbAtivoInativo.Caption:='Inativo';
            end;
            lbNomeGrupoServico.Caption:=GrupoServico.Get_Nome;
            result:=True;
       End;
  Except
        Result:=False;
  End;
end;

function TFSERVICO.TabelaParaControles: Boolean;
begin
     If (ObjSERVICO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;


procedure TFSERVICO.edtPorcentagemInstaladoExit(Sender: TObject);
Var
  PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaInstalado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemInstalado.Text))/100)));
end;

procedure TFSERVICO.edtPorcentagemRetiradoExit(Sender: TObject);
Var
  PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));

    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaRetirado.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemRetirado.Text))/100)));
end;

procedure TFSERVICO.edtPorcentagemFornecidoExit(Sender: TObject);
Var
  PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=True)
    Then EdtPrecoVendaFornecido.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemFornecido.Text))/100)));
end;

procedure TFSERVICO.edtGrupoServicoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   ObjSERVICO.EdtGrupoServicoKeyDown(Sender, Key, Shift, LbNomeGrupoServico);
end;

procedure TFSERVICO.edtGrupoServicoExit(Sender: TObject);
begin
  ObjSERVICO.EdtGrupoServicoExit(Sender, LbNomeGrupoServico);
end;

procedure TFSERVICO.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjServico.PrimeiroRegistro = false)then
    exit;

    ObjServico.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFSERVICO.btAnteriorClick(Sender: TObject);
begin
    if ( lbCodigo.Caption='')then
    lbCodigo.Caption:='0';

    if  (ObjServico.RegistoAnterior(StrToInt( lbCodigo.Caption)) = false)then
    exit;

    ObjServico.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFSERVICO.btProximoClick(Sender: TObject);
begin
    if ( lbCodigo.Caption='')then
     lbCodigo.Caption:='0';

    if  (ObjServico.ProximoRegisto(StrToInt( lbCodigo.Caption)) = false)then
    exit;

    ObjServico.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFSERVICO.btUltimoClick(Sender: TObject);
begin
    if  (ObjServico.UltimoRegistro = false)then
    exit;

    ObjServico.TabelaparaObjeto;
    Self.ObjetoParaControles;
end;

procedure TFSERVICO.edtPrecoCustoExit(Sender: TObject);
begin
   TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);
end;

procedure TFSERVICO.edtPrecoVendaInstaladoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemInstalado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaInstalado.Text))/precocusto));
end;

procedure TFSERVICO.edtPrecoVendaFornecidoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemfornecido.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendafornecido.Text))/precocusto));
end;

procedure TFSERVICO.edtPrecoVendaRetiradoExit(Sender: TObject);
Var
PrecoCusto:Currency;
begin
    PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCusto.text));
    if (AlteraPrecoPeloCusto=False)
    Then EdtPorcentagemretirado.Text:=CurrToStr(-100+((100*StrToCurr(EdtPrecoVendaretirado.Text))/precocusto));
end;


procedure TFSERVICO.AbreComCodigo(parametro:string);
begin
    if(ObjSERVICO.LocalizaReferencia(parametro)=True)
    then
    begin
        ObjSERVICO.TabelaparaObjeto;
        self.ObjetoParaControles;

    end;
end;


procedure TFSERVICO.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     ColocaUpperCaseEdit(Self);

     Try
         ObjSERVICO:=TObjSERVICO.create;
     Except
         Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
         Self.close;
     End;

    FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
    FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');

    habilita_botoes(Self);
    if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE SERVICO')=False)
    Then desab_botoes(Self);

    AlteraPrecoPeloCusto:=True;
    if (ObjParametroGlobal.ValidaParametro('CALCULA PRECO DE VENDA DO SERVICO PELO CUSTO E PERCENTUAIS?')=True)
    then Begin
       if (ObjParametroGlobal.get_valor<>'SIM')
       then AlteraPrecoPeloCusto:=false;
    End;

    if(Tag<>0)
    then begin
        if(ObjSERVICO.LocalizaCodigo(IntToStr(tag))=True)
        then begin
              ObjSERVICO.TabelaparaObjeto;
              Self.ObjetoParaControles;

        end;
    end;

end;

procedure TFSERVICO.edtGrupoServicoDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FgrupoServico:TFGRUPOSERVICO;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            try
                FgrupoServico:=TFGRUPOSERVICO.Create(nil);
            except

            end;


            If (FpesquisaLocal.PreparaPesquisa('Select * from tabgruposervico','',FgrupoServico)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 edtGrupoServico.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                lbNomeGrupoServico.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FgrupoServico);

     End;
end;

procedure TFSERVICO.chkAtivoClick(Sender: TObject);
var
  QueryPesq:TIBQuery;
  FERRODESATIVARMATERIAL:TFErrosDesativarMaterial;
begin
      if(lbCodigo.Caption='') or (lbCodigo.Caption='0')
      then Exit;

      QueryPesq:=TIBQuery.Create(nil);
      QueryPesq.Database:=FDataModulo.IBDatabase;
      FERRODESATIVARMATERIAL:=TFErrosDesativarMaterial.Create(nil);

      try
          if(chkAtivo.Checked=True)
          then lbAtivoInativo.Caption:='Ativo';

          if(chkAtivo.Checked=False) then
          begin
              with QueryPesq do
              begin
                  Close;
                  SQL.Clear;
                  SQL.Add('select * from tabservico_proj where servico='+lbCodigo.Caption);
                  Open;

                  if(recordcount=0)
                  then lbAtivoInativo.Caption:='Inativo'
                  else
                  begin
                       chkAtivo.Checked:=True;
                       FERRODESATIVARMATERIAL.PassaMaterial(lbCodigo.Caption,EdtDescricao.Text,'servico');
                       FERRODESATIVARMATERIAL.ShowModal;
                  end;

              end;


          end;
      finally
           FreeAndNil(QueryPesq);
           FERRODESATIVARMATERIAL.Free;
      end;
end;

procedure TFSERVICO.btAjudaClick(Sender: TObject);
begin
         FAjuda.PassaAjuda('CADASTRO DE SERVI�OS');
         FAjuda.ShowModal;
end;

procedure TFSERVICO.edtGrupoServicoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFSERVICO.lbNomeGrupoServicoClick(Sender: TObject);
var
  FGrupoServico:TFGRUPOSERVICO;
begin
    try
       FGrupoServico:=TFGRUPOSERVICO.Create(nil);
    except
       Exit;
    end;

    try
        if(edtGrupoServico.Text='')
        then Exit;

        FGrupoServico.Tag:=StrToInt(edtGrupoServico.Text);
        FGrupoServico.ShowModal;

    finally
        FreeAndNil(FGrupoServico);
    end;


end;

procedure TFSERVICO.lbNomeGrupoServicoMouseLeave(Sender: TObject);
begin
TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFSERVICO.lbNomeGrupoServicoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFSERVICO.edtPorcentagemInstaladoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFSERVICO.edtPorcentagemRetiradoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFSERVICO.edtPorcentagemFornecidoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFSERVICO.edtPrecoCustoKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFSERVICO.edtArredondamentoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFSERVICO.EdtCFOPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ObjSERVICO.EdtCFOPKeyDown(Sender,Key,Shift,lbNomeCfop);
end;

procedure TFSERVICO.edtSTBKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ObjSERVICO.EdtSTBKeyDown(Sender,Key,Shift,lbSTB);
end;

procedure TFSERVICO.edtSTBDblClick(Sender: TObject);
var
  key:Word;
  shift:TShiftState;
begin
   key:=VK_F9;
   ObjSERVICO.EdtSTBKeyDown(Sender,key,shift,lbSTB);
end;

procedure TFSERVICO.EdtCFOPDblClick(Sender: TObject);
var
  Key:Word;
  shift:TShiftState;
begin
     key:=VK_F9;
     ObjSERVICO.EdtCFOPKeyDown(Sender,Key,shift,lbNomeCfop);
end;

procedure TFSERVICO.ComboTipoAliquotaKeyPress(Sender: TObject;
  var Key: Char);
begin
  key:=#0;
end;

end.
