inherited fAcertaImpostoSafira: TfAcertaImpostoSafira
  Left = 618
  Top = 326
  Caption = 'fAcertaImpostoSafira'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    inherited TabSheet2: TTabSheet
      inherited PageControl2: TPageControl
        inherited TabSheet3: TTabSheet
          inherited btOk_ICMS: TSpeedButton
            OnClick = btOk_ICMSClick
          end
          inherited edt_ICMS_BC: TEdit
            Hint = 'VALORBASECALCULO'
          end
          inherited edt_ICMS_ALIQUOTA: TEdit
            Hint = 'PERCENTUALICMS'
          end
          inherited edt_ICMS_VALOR: TEdit
            Hint = 'VALORICMS'
          end
          inherited edt_ICMS_REDUCAO: TEdit
            Hint = 'PERCENTUALREDUCAOBC'
          end
          inherited edt_ICMS_ValorFrete: TEdit
            Hint = 'VALORFRETE'
          end
          inherited edt_ICMS_CSOSN: TEdit
            Hint = 'CSOSN'
          end
          inherited edt_ICMS_ValorOutros: TEdit
            Hint = 'VALOROUTROS'
          end
          inherited edt_ICMS_CST: TEdit
            Hint = 'CST'
          end
        end
        inherited TabSheet4: TTabSheet
          inherited btOK_ICMS_ST: TSpeedButton
            OnClick = btOK_ICMS_STClick
          end
        end
        inherited TabSheet5: TTabSheet
          inherited btOk_PIS: TSpeedButton
            OnClick = btOk_PISClick
          end
        end
        inherited TabSheet6: TTabSheet
          inherited btOK_COFINS: TSpeedButton
            OnClick = btOK_COFINSClick
          end
        end
        inherited TabSheet7: TTabSheet
          inherited btOK_IPI: TSpeedButton
            OnClick = btOK_IPIClick
          end
        end
        inherited TabSheet15: TTabSheet
          inherited SpeedButton2: TSpeedButton
            OnClick = SpeedButton2Click
          end
        end
      end
    end
    inherited TabSheet1: TTabSheet
      inherited pageControlTotais: TPageControl
        ActivePage = TabSheet9
        OnChange = pageControlTotaisChange
        inherited TabSheet14: TTabSheet
          inherited SpeedButton1: TSpeedButton
            OnClick = SpeedButton1Click
          end
        end
      end
    end
  end
end
