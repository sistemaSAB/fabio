unit UIMPOSTO_COFINS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjIMPOSTO_COFINS,
  jpeg,IBQuery,UDataModulo;

type
  TFIMPOSTO_COFINS = class(TForm)
    pnl1: TPanel;
    imgrodape: TImage;
    lb9: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbcodigo: TLabel;
    lb7: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    lbLbST: TLabel;
    edtST_COFINS: TEdit;
    lbNomeST_COFINS: TLabel;
    lbLbTIPOCALCULO: TLabel;
    cbbComboTipoCalculo_COFINS: TComboBox;
    lbLbALIQUOTAPERCENTUAL: TLabel;
    edtALIQUOTAPERCENTUAL_COFINS: TEdit;
    edtALIQUOTAVALOR_COFINS: TEdit;
    lbLbALIQUOTAVALOR: TLabel;
    lb2: TLabel;
    edtFORMULABASECALCULO_COFINS: TEdit;
    edtFORMULA_VALOR_IMPOSTO_COFINS: TEdit;
    lb3: TLabel;
    lbLbTIPOCALCULO_ST: TLabel;
    cbbTIPOCALCULO_ST_COFINS: TComboBox;
    edtALIQUOTAVALOR_ST_COFINS: TEdit;
    lbLbALIQUOTAVALOR_ST: TLabel;
    edtALIQUOTAPERCENTUAL_ST_COFINS: TEdit;
    lbLbALIQUOTAPERCENTUAL_ST: TLabel;
    lb4: TLabel;
    edtFORMULABASECALCULO_ST_COFINS: TEdit;
    lb5: TLabel;
    edtFORMULA_VALOR_IMPOSTO_ST_COFINS: TEdit;
//DECLARA COMPONENTES
    procedure edtST_COFINSKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtST_COFINSExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MostraQuantidadeCadastrada;
    procedure edtST_COFINSDblClick(Sender: TObject);
  private
         ObjIMPOSTO_COFINS:TObjIMPOSTO_COFINS;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FIMPOSTO_COFINS: TFIMPOSTO_COFINS;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFIMPOSTO_COFINS.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjIMPOSTO_COFINS do
    Begin
        Submit_CODIGO(lbcodigo.Caption);
        ST.Submit_codigo(edtST_COFINS.text);
        Submit_TIPOCALCULO(cbbcomboTIPOCALCULO_COFINS.text);
        Submit_ALIQUOTAPERCENTUAL(edtALIQUOTAPERCENTUAL_COFINS.text);
        Submit_ALIQUOTAVALOR(edtALIQUOTAVALOR_COFINS.text);
        Submit_TIPOCALCULO_ST(cbbTIPOCALCULO_ST_COFINS.text);
        Submit_ALIQUOTAPERCENTUAL_ST(edtALIQUOTAPERCENTUAL_ST_COFINS.text);
        Submit_ALIQUOTAVALOR_ST(edtALIQUOTAVALOR_ST_COFINS.text);
        Submit_FORMULABASECALCULO(EdtFORMULABASECALCULO_COFINS.text);
        Submit_FORMULABASECALCULO_ST(EdtFORMULABASECALCULO_ST_COFINS.text);

        Submit_formula_valor_imposto(Edtformula_valor_imposto_COFINS.text);
        Submit_formula_valor_imposto_ST(Edtformula_valor_imposto_ST_COFINS.text);
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFIMPOSTO_COFINS.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjIMPOSTO_COFINS do
     Begin
        lbcodigo.Caption:=Get_CODIGO;
        EdtST_COFINS.text:=ST.Get_codigo;
        cbbComboTIPOCALCULO_COFINS.text:=Get_TIPOCALCULO;
        EdtALIQUOTAPERCENTUAL_COFINS.text:=Get_ALIQUOTAPERCENTUAL;
        EdtALIQUOTAVALOR_COFINS.text:=Get_ALIQUOTAVALOR;
        CbbTIPOCALCULO_ST_COFINS.text:=Get_TIPOCALCULO_ST;
        EdtALIQUOTAPERCENTUAL_ST_COFINS.text:=Get_ALIQUOTAPERCENTUAL_ST;
        EdtALIQUOTAVALOR_ST_COFINS.text:=Get_ALIQUOTAVALOR_ST;
        edtFORMULABASECALCULO_COFINS.text:=Get_FORMULABASECALCULO;
        edtFORMULABASECALCULO_ST_COFINS.text:=Get_FORMULABASECALCULO_ST;

        edtformula_valor_imposto_COFINS.text:=Get_formula_valor_imposto;
        edtformula_valor_imposto_ST_COFINS.text:=Get_formula_valor_imposto_ST;

//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFIMPOSTO_COFINS.TabelaParaControles: Boolean;
begin
     If (Self.ObjIMPOSTO_COFINS.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFIMPOSTO_COFINS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjIMPOSTO_COFINS=Nil)
     Then exit;

If (Self.ObjIMPOSTO_COFINS.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjIMPOSTO_COFINS.free;
end;

procedure TFIMPOSTO_COFINS.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFIMPOSTO_COFINS.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     

     lbcodigo.Caption:='0';
     //edtcodigo_COFINS.text:=Self.ObjIMPOSTO_COFINS.Get_novocodigo;

     btnovo.Visible:=false;
     btalterar.Visible:=False;
     btexcluir.Visible:=false;
     btrelatorios.Visible:=false;
     btopcoes.Visible:=false;
     btsair.Visible:=False;
     btpesquisar.visible:=False;

     


     Self.ObjIMPOSTO_COFINS.status:=dsInsert;

     edtST_COFINS.setfocus;

end;


procedure TFIMPOSTO_COFINS.btalterarClick(Sender: TObject);
begin
    If (Self.ObjIMPOSTO_COFINS.Status=dsinactive) and (lbcodigo.Caption<>'')
    Then Begin
                MensagemAviso('N�o � permitido a altera��o de Impostos');
                (*habilita_campos(Self);
                EdtCodigo_COFINS.enabled:=False;
                Self.ObjIMPOSTO_COFINS.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtST_COFINS.setfocus;*)
                btnovo.Visible:=false;
                 btalterar.Visible:=False;
                 btexcluir.Visible:=false;
                 btrelatorios.Visible:=false;
                 btopcoes.Visible:=false;
                 btsair.Visible:=False;
                 btpesquisar.visible:=False;

    End;


end;

procedure TFIMPOSTO_COFINS.btgravarClick(Sender: TObject);
begin

     If Self.ObjIMPOSTO_COFINS.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjIMPOSTO_COFINS.salvar(true)=False)
     Then exit;

    lbcodigo.Caption:=Self.ObjIMPOSTO_COFINS.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorios.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     MostraQuantidadeCadastrada;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFIMPOSTO_COFINS.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjIMPOSTO_COFINS.status<>dsinactive) or (lbcodigo.Caption='')
     Then exit;

     If (Self.ObjIMPOSTO_COFINS.LocalizaCodigo(lbcodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjIMPOSTO_COFINS.exclui(lbcodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFIMPOSTO_COFINS.btcancelarClick(Sender: TObject);
begin
     Self.ObjIMPOSTO_COFINS.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorios.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
      btopcoes.Visible :=true;

end;

procedure TFIMPOSTO_COFINS.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFIMPOSTO_COFINS.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjIMPOSTO_COFINS.Get_pesquisa,Self.ObjIMPOSTO_COFINS.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjIMPOSTO_COFINS.status<>dsinactive
                                  then exit;

                                  If (Self.ObjIMPOSTO_COFINS.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjIMPOSTO_COFINS.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFIMPOSTO_COFINS.LimpaLabels;
begin
//LIMPA LABELS
     LbNomeST_COFINS.Caption:='';
     lbcodigo.Caption:='';
end;

procedure TFIMPOSTO_COFINS.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);


     Try
        Self.ObjIMPOSTO_COFINS:=TObjIMPOSTO_COFINS.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     MostraQuantidadeCadastrada;

end;
procedure TFIMPOSTO_COFINS.edtST_COFINSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjIMPOSTO_COFINS.edtSTkeydown(sender,key,shift,lbnomeST_COFINS);
end;
 
procedure TFIMPOSTO_COFINS.edtST_COFINSExit(Sender: TObject);
begin
    ObjIMPOSTO_COFINS.edtSTExit(sender,lbnomeST_COFINS);
end;
//CODIFICA ONKEYDOWN E ONEXIT

procedure TFIMPOSTO_COFINS.MostraQuantidadeCadastrada;
var
  Query:TIBQuery;
  cont:Integer;
begin
    try
      Query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;
     cont:=0;
    try
      with Query do
      begin
          Close;
          sql.Clear;
          sql.Add('select * from tabimposto_cofins');
          Open;
          while not Eof do
          begin
             inc(cont,1);
             Next;

          end;
          if(cont>1) then
                lb9.Caption:='Existem '+IntToStr(cont)+' cofins cadastrados'
          else
          begin
                lb9.Caption:='Existe '+IntToStr(cont)+' cofins cadastrado';
          end;

      end;
    finally
       FreeAndNil(Query);
    end;


end;

{r4mr}
procedure TFIMPOSTO_COFINS.edtST_COFINSDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtST_COFINSKeyDown(Sender, Key, Shift);
end;

end.

