unit UPROJETO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjPROJETO,
  UessencialGlobal, Tabs, Grids, DBGrids,UobjFERRAGEM_PROJ, UobjPERFILADO_PROJ,
  UobjCOMPONENTE_PROJ,UobjSERVICO_PROJ, UobjKITBOX_PROJ,UobjDiverso_Proj, DBCtrls,IBQuery,UCOMPONENTE,UDIVERSO
  ,UFERRAGEM,UKITBOX,UPERFILADO,USERVICO,UpesquisaMenu, ComCtrls,
  UFRImposto_ICMS,UobjPROJETO_ICMS, Mask,CodigoFigura, Menus,UFiltraImp,UMedidasFuros,UAjuda,
  jpeg, ImgList,ClipBrd;

type
  TFPROJETO = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigoProjeto: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    imgrodape: TImage;
    lb14: TLabel;
    tsPrincipal: TTabSheet;
    tsFerragens: TTabSheet;
    Guia: TPageControl;
    tsPerfis: TTabSheet;
    tsComponentes: TTabSheet;
    tsKitBox: TTabSheet;
    tsServicos: TTabSheet;
    tsDiversos: TTabSheet;
    OpenDialog: TOpenDialog;
    PanelDiverso_Proj: TPanel;
    lbNomeDiverso_Proj: TLabel;
    lb18: TLabel;
    lb19: TLabel;
    lbAlturaDiverso_Proj: TLabel;
    lbLarguraDiverso_Proj: TLabel;
    lbcancelar1: TLabel;
    lbGravar1: TLabel;
    lbExcluir1: TLabel;
    edtDiverso_ProjReferencia: TEdit;
    edtQuantidadeDiverso_proj: TEdit;
    edtDiverso_Proj: TEdit;
    edtCodigoDiverso_Proj: TEdit;
    edtAlturaDiverso_Proj: TEdit;
    edtLarguraDiverso_proj: TEdit;
    dbgridDiverso_Proj: TDBGrid;
    PanelComponente_Proj: TPanel;
    lbNomeComponente_Proj: TLabel;
    lb20: TLabel;
    lb21: TLabel;
    lb22: TLabel;
    lb23: TLabel;
    lb24: TLabel;
    lb25: TLabel;
    lbGravar: TLabel;
    lbExcluir: TLabel;
    lbCancelar: TLabel;
    edtCodigoComponente_Proj: TEdit;
    edtComponente_ProjReferencia: TEdit;
    edtFormulaAlturaComponente_Proj: TEdit;
    edtFolgaAlturaComponente_Proj: TEdit;
    edtFormulaLarguraComponente_Proj: TEdit;
    edtFolgaLarguraComponente_Proj: TEdit;
    COMBOCorteComponente_Proj: TComboBox;
    edtComponente_Proj: TEdit;
    dbgridComponente_Proj: TDBGrid;
    PanelFerragem_Proj: TPanel;
    lbNomeFerragem_Proj: TLabel;
    lb26: TLabel;
    lb27: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    edtCodigoFerragem_Proj: TEdit;
    edtFerragem_ProjReferencia: TEdit;
    edtQtdeFerragem_Proj: TEdit;
    edtFerragem_Proj: TEdit;
    dbgridFerragem_Proj: TDBGrid;
    dbgridPerfilado_Proj: TDBGrid;
    PanelPerfilado_Proj: TPanel;
    lbNomePerfilado_Proj: TLabel;
    lb28: TLabel;
    lb29: TLabel;
    lb8: TLabel;
    lb9: TLabel;
    lb10: TLabel;
    edtCodigoPerfilado_Proj: TEdit;
    edtPerfilado_ProjReferencia: TEdit;
    edtFormulaLArguraPerfilado_Proj: TEdit;
    edtFormulaAlturaPerfilado_Proj: TEdit;
    edtPerfilado_Proj: TEdit;
    PanelServico_Proj: TPanel;
    lbNomeServico_proj: TLabel;
    lb31: TLabel;
    lb32: TLabel;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    edtCodigoServico_Proj: TEdit;
    edtServico_ProjReferencia: TEdit;
    edtServico_Proj: TEdit;
    edtQuantidadeServicoProj: TEdit;
    dbgridServico_Proj: TDBGrid;
    PanelKitBox_Proj: TPanel;
    lbNomeKitBox_Proj: TLabel;
    lb33: TLabel;
    lb34: TLabel;
    lb5: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    edtCodigoKitBox_proj: TEdit;
    edtKitBox_ProjReferencia: TEdit;
    edtQuantidadeKitBox_Proj: TEdit;
    edtKitBox_Proj: TEdit;
    dbgridKitBox_Proj: TDBGrid;
    Panel1: TPanel;
    ImageDesenho: TImage;
    lbLbReferencia: TLabel;
    edtReferencia: TEdit;
    lbLbDescricao: TLabel;
    edtDescricao: TEdit;
    lbLbComplemento: TLabel;
    MemoComplemento: TMemo;
    lbLbGrupoVidro: TLabel;
    edtGrupoVidro: TEdit;
    lbNomeGrupoVidro: TLabel;
    lb15: TLabel;
    COMBOProjetoBranco: TComboBox;
    grp1: TGroupBox;
    Image1: TImage;
    Image3: TImage;
    Image4: TImage;
    RadioAlturaPorLargura: TRadioButton;
    RadioAlturaPorLargura_AlturaPorLargura: TRadioButton;
    RadioVarios: TRadioButton;
    grp3: TGroupBox;
    lbLbMaoDeObra: TLabel;
    lbLbAreaMinima: TLabel;
    edtAreaMinima: TEdit;
    edtMaoDeObra: TEdit;
    grp2: TGroupBox;
    lb16: TLabel;
    lb17: TLabel;
    edtLarguraMinima: TEdit;
    edtLarguraMaxima: TEdit;
    grp4: TGroupBox;
    lbLbAlturaMinima: TLabel;
    lbLbAlturaMaxima: TLabel;
    edtAlturaMinima: TEdit;
    edtAlturaMaxima: TEdit;
    tsImpostos: TTabSheet;
    dlgSave1: TSaveDialog;
    FRImposto_ICMS1: TFRImposto_ICMS;
    edtNCM: TMaskEdit;
    lb35: TLabel;
    ts1: TTabSheet;
    pnlBotesModelagem: TPanel;
    pnl2: TPanel;
    lb36: TLabel;
    ListBox1: TListBox;
    pnl3: TPanel;
    PaintBoxTela: TPaintBox;
    edtAlt: TEdit;
    edtLarg: TEdit;
    edtX: TEdit;
    edtY: TEdit;
    lbY: TLabel;
    lbX: TLabel;
    lbLarg: TLabel;
    lbAlt: TLabel;
    PopUpMenu: TPopupMenu;
    Copiar1: TMenuItem;
    Colar1: TMenuItem;
    Colar2: TMenuItem;
    Excluir1: TMenuItem;
    ColorDialog1: TColorDialog;
    PopUpListaObjetos: TPopupMenu;
    MenuItem4: TMenuItem;
    Medidas1: TMenuItem;
    Image2: TImage;
    Image5: TImage;
    lbPosicaoFuros: TLabel;
    bt1: TSpeedButton;
    lblAltServ: TLabel;
    lblLargServ: TLabel;
    gboxControla: TGroupBox;
    rbControlaSim: TRadioButton;
    rbControlaNao: TRadioButton;
    lb37: TLabel;
    COMBOUtilizaKitPerfilado: TComboBox;
    edtquantidade: TEdit;
    lb30: TLabel;
    lb38: TLabel;
    btTroca: TBitBtn;
    GerarDesenho1: TMenuItem;
    MostrarReferncias1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    NoMostrarReferncias1: TMenuItem;
    Puxador1: TMenuItem;
    btDesenhaQuadrado: TSpeedButton;
    btDesenhaEsfera: TSpeedButton;
    btDesenhaLinha: TSpeedButton;
    btEditarImagem: TSpeedButton;
    btCancela: TSpeedButton;
    btSalvarModelagem: TSpeedButton;
    btVisualizar: TSpeedButton;
    btLimpaDesenho: TSpeedButton;
    btNovoModelo: TSpeedButton;
    btMostraFerragens: TSpeedButton;
    MeioCirculo1: TMenuItem;
    C1: TMenuItem;
    Retangulo1: TMenuItem;
    N3: TMenuItem;
    DesenharLinha1: TMenuItem;
    ModelosdeFerragens1: TMenuItem;
    LigarMaterial1: TMenuItem;
    edtAltServ: TEdit;
    edtLargServ: TEdit;
    N14deumcrculoesquerda1: TMenuItem;
    N14deumcrculodireita1: TMenuItem;
    btAjudaVideo: TSpeedButton;
    tsProducao: TTabSheet;
    Panel2: TPanel;
    img1: TImage;
    PanelFundoProducao: TPanel;
    PaintBox1: TPaintBox;
    ListBox2: TListBox;
    STRGLinhaServicos: TStringGrid;
    il1: TImageList;
    btADDServicos: TSpeedButton;
    btSalvarServicosProducao: TSpeedButton;
    lbNomedoservicoprod: TLabel;
    btCancelarModServ: TSpeedButton;
    il2: TImageList;
    Puxadores1: TMenuItem;
    il3: TImageList;
    btMenuDesenhosAdd: TSpeedButton;
    panel4: TPanel;
    btPuxadores: TSpeedButton;
    btMeioCiruclo: TSpeedButton;
    bt1por4Esquerda: TSpeedButton;
    bt1po4direito: TSpeedButton;
    lb39: TLabel;
    ComboEsquadria: TComboBox;
    chkesquadriaperfilado: TCheckBox;
    chkesquadriadiverso: TCheckBox;
    chkComponenteEsquadria: TCheckBox;
    grp5: TGroupBox;
    rbsimm2: TRadioButton;
    rbnaom2: TRadioButton;
    Splitter1: TSplitter;
    Label1: TLabel;
    edtCest: TEdit;

    Procedure PosicionaForm;

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtGrupoVidroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoVidroExit(Sender: TObject);
    procedure btGravarFerragem_ProjClick(Sender: TObject);
    procedure edtFerragem_ProjReferenciaExit(Sender: TObject);
    procedure edtFerragem_ProjReferenciaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure dbgridFerragem_ProjDblClick(Sender: TObject);
    procedure btExcluirFerragem_ProjClick(Sender: TObject);
    procedure btCancelarFerragem_ProjClick(Sender: TObject);
    procedure btGravarPerfialdo_ProjClick(Sender: TObject);
    procedure GuiaChange2(Sender: TObject; NewTab: Integer;var AllowChange: Boolean);
    procedure edtPerfilado_ProjReferenciaExit(Sender: TObject);
    procedure edtPerfilado_ProjReferenciaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure dbgridPerfilado_ProjDblClick(Sender: TObject);
    procedure btExcluirPerfialdo_ProjClick(Sender: TObject);
    procedure btCancelarPerfilado_ProjClick(Sender: TObject);
    procedure brGravarComponente_ProjClick(Sender: TObject);
    procedure edtComponente_ProjReferenciaExit(Sender: TObject);
    procedure edtComponente_ProjReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgridComponente_ProjDblClick(Sender: TObject);
    procedure brExcuirComponente_ProjClick(Sender: TObject);
    procedure brCancelarComponente_ProjClick(Sender: TObject);
    procedure btGravarKitBox_ProjClick(Sender: TObject);
    procedure btExcluirKitBox_ProjClick(Sender: TObject);
    procedure btCancelarKitBox_ProjClick(Sender: TObject);
    procedure dbgridKitBox_ProjDblClick(Sender: TObject);
    procedure edtKitBox_ProjReferenciaExit(Sender: TObject);
    procedure edtKitBox_ProjReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure brGravarServico_ProjClick(Sender: TObject);
    procedure brExcluirServico_ProjClick(Sender: TObject);
    procedure brCancelarServico_ProjClick(Sender: TObject);
    procedure NotebookDblClick(Sender: TObject);
    procedure edtServico_ProjReferenciaExit(Sender: TObject);
    procedure edtServico_ProjReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgridServico_ProjDblClick(Sender: TObject);
    procedure edtDiverso_ProjReferenciaExit(Sender: TObject);
    procedure edtDiverso_ProjReferenciaKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure dbgridDiverso_ProjDblClick(Sender: TObject);
    procedure edtAlturaDiverso_ProjExit(Sender: TObject);
    procedure edtLarguraDiverso_projExit(Sender: TObject);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure edtFormulaAlturaPerfilado_ProjExit(Sender: TObject);
    procedure edtFormulaLArguraPerfilado_ProjExit(Sender: TObject);
    procedure edtFormulaAlturaComponente_ProjExit(Sender: TObject);
    procedure edtFormulaLarguraComponente_ProjExit(Sender: TObject);
    procedure BtOpcoesClick(Sender: TObject);
    procedure COMBOCorteComponente_ProjKeyPress(Sender: TObject;
      var Key: Char);
    procedure dbgridComponente_ProjDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dbgridDiverso_ProjDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dbgridFerragem_ProjDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dbgridKitBox_ProjDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dbgridPerfilado_ProjDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dbgridServico_ProjDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure lbGravarMouseLeave(Sender: TObject);
    procedure lbGravarMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PanelComponente_ProjMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure edtComponente_ProjReferenciaKeyPress(Sender: TObject;
      var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure edtGrupoVidroKeyPress(Sender: TObject; var Key: Char);
    procedure edtAlturaMaximaKeyPress(Sender: TObject; var Key: Char);
    procedure edtAlturaMinimaKeyPress(Sender: TObject; var Key: Char);
    procedure edtLarguraMaximaKeyPress(Sender: TObject; var Key: Char);
    procedure edtLarguraMinimaKeyPress(Sender: TObject; var Key: Char);
    procedure edtAreaMinimaKeyPress(Sender: TObject; var Key: Char);
    procedure edtMaoDeObraKeyPress(Sender: TObject; var Key: Char);
    procedure lbNomeComponente_ProjClick(Sender: TObject);
    procedure lbNomeDiverso_ProjClick(Sender: TObject);
    procedure lbNomeFerragem_ProjClick(Sender: TObject);
    procedure lbNomeKitBox_ProjClick(Sender: TObject);
    procedure lbNomePerfilado_ProjClick(Sender: TObject);
    procedure lbNomeServico_projClick(Sender: TObject);
    procedure edtGrupoVidroDblClick(Sender: TObject);
    procedure edtComponente_ProjReferenciaDblClick(Sender: TObject);
    procedure edtDiverso_ProjReferenciaDblClick(Sender: TObject);
    procedure edtFerragem_ProjReferenciaDblClick(Sender: TObject);
    procedure edtKitBox_ProjReferenciaDblClick(Sender: TObject);
    procedure edtPerfilado_ProjReferenciaDblClick(Sender: TObject);
    procedure edtServico_ProjReferenciaDblClick(Sender: TObject);
    procedure ImageDesenhoClick(Sender: TObject);
    procedure GuiaChange(Sender: TObject);
    procedure FRImposto_ICMS1BtGravar_material_icmsClick(Sender: TObject);
    procedure FRImposto_ICMS1btcancelar_material_ICMSClick(
      Sender: TObject);
    procedure FRImposto_ICMS1btexcluir_material_ICMSClick(Sender: TObject);
    procedure FRImposto_ICMS1Button1Click(Sender: TObject);
    procedure PaintBoxTelaDblClick(Sender: TObject);
    procedure PaintBoxTelaMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PaintBoxTelaMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PaintBoxTelaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btDesenhaQuadradoClick(Sender: TObject);
    procedure btDesenhaLinhaClick(Sender: TObject);
    procedure btLimpaDesenhoClick(Sender: TObject);
    procedure btMostraFerragensClick(Sender: TObject);
    procedure btEditarImagemClick(Sender: TObject);
    procedure ColarCtrlV1Click(Sender: TObject);
    procedure CopiarCtrlC1Click(Sender: TObject);
    procedure Copiar1Click(Sender: TObject);
    procedure Colar1Click(Sender: TObject);
    procedure btDesenhaEsferaClick(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ListBox1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Colar2Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure btSalvarModelagemClick(Sender: TObject);
    procedure CarregaDesenhoProjeto(ComReferencias:Boolean;MostraLinhas:Boolean);
    procedure CarregaDesenhoProjetoProducao(ComReferencias:Boolean;MostraLinhas:Boolean);
    procedure btNovoModeloClick(Sender: TObject);
    procedure PaintBoxTelaClick(Sender: TObject);
    procedure btPoligonoClick(Sender: TObject);
    procedure STRGProdutosClick(Sender: TObject);
    procedure STRGProdutosMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Medidas1Click(Sender: TObject);
    procedure lbPosicaoCorteFerragemClick(Sender: TObject);
    procedure lbPosicaoCortePerfiladoClick(Sender: TObject);
    procedure lbPosicaoCorteDiversoClick(Sender: TObject);
    //procedure chk1Click(Sender: TObject);
    procedure lbPosicaoFurosClick(Sender: TObject);
    procedure bt1Click(Sender: TObject);
{    procedure edtComponenteFerragemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);  }

    procedure edtLargServExit(Sender: TObject); {Rodolfo}
    procedure edtAltServExit(Sender: TObject);
    procedure rbControlaSimClick(Sender: TObject);
    procedure rbControlaNaoClick(Sender: TObject);
    procedure edtServico_ProjReferenciaClick(Sender: TObject);
    procedure edtAltServKeyPress(Sender: TObject; var Key: Char);
    procedure FRImposto_ICMS1btReplicarImpostosClick(Sender: TObject);
    procedure Excluir1Click(Sender: TObject);
    procedure btTrocaClick(Sender: TObject);
    procedure pnl2MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure btVisualizarClick(Sender: TObject);
    procedure GerarDesenho1Click(Sender: TObject);
    procedure btcancelaClick(Sender: TObject);
    procedure MostrarReferncias1Click(Sender: TObject);
    procedure NoMostrarReferncias1Click(Sender: TObject);
    procedure Puxador1Click(Sender: TObject);{Rodolfo}
    procedure Preenchemedidas;
    procedure MeioCirculo1Click(Sender: TObject);
    procedure ModelosdeFerragens1Click(Sender: TObject);
    procedure C1Click(Sender: TObject);
    procedure Retangulo1Click(Sender: TObject);
    procedure DesenharLinha1Click(Sender: TObject);
    procedure LigarMaterial1Click(Sender: TObject);
    procedure N14deumcrculoesquerda1Click(Sender: TObject);
    procedure N14deumcrculodireita1Click(Sender: TObject);
    procedure btAjudaVideoClick(Sender: TObject);
    procedure edtServicoProducaoDblClick(Sender: TObject);
    procedure PaintBox1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PaintBox1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure STRGLinhaServicosDrawCell(Sender: TObject; ACol,
      ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure STRGLinhaServicosDblClick(Sender: TObject);
    procedure STRGLinhaServicosClick(Sender: TObject);
    procedure PaintBox1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btADDServicosClick(Sender: TObject);
    procedure STRGLinhaServicosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure STRGLinhaServicosMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btSalvarServicosProducaoClick(Sender: TObject);
    procedure btCancelarModServClick(Sender: TObject);
    procedure STRGLinhaServicosMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Puxadores1Click(Sender: TObject);
    procedure btMenuDesenhosAddClick(Sender: TObject);
    procedure btPuxadoresClick(Sender: TObject);
    procedure bt1por4EsquerdaClick(Sender: TObject);
    procedure bt1po4direitoClick(Sender: TObject);
    procedure btMeioCirucloClick(Sender: TObject);
    procedure btLigarMaterialClick(Sender: TObject);
    procedure ComboEsquadriaKeyPress(Sender: TObject; var Key: Char);
    procedure chkesquadriadiversoClick(Sender: TObject);
    procedure chkesquadriaperfiladoClick(Sender: TObject);
    procedure rbsimm2Click(Sender: TObject);
    procedure rbnaom2Click(Sender: TObject);
    procedure edtNCMKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCestKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);


  private
    ObjPROJETO:TObjPROJETO;
    ObjFERRAGEM_PROJ:TObjFERRAGEM_PROJ;
    ObjPERFILADO_PROJ:TObjPERFILADO_PROJ;
    ObjCOMPONENTE_PROJ:TObjCOMPONENTE_PROJ;
    ObjSERVICO_PROJ:TObjSERVICO_PROJ;
    ObjKITBOX_PROJ:TObjKITBOX_PROJ;
    ObjDiverso_Proj:TOBjDiverso_Proj;
    ListaComponentes:TStringList;
    ListaItens:TStringList;
    ListaServicosProducao:TStringList;

    FIsButtonDown: array[TMouseButton] of Boolean;
    FP1, FP2: TPoint;
    FP1new: TPoint;
    //***Variaveis para desenhar os projetos
    pInic : TPoint; //indica o ponto onde o botao do mouse foi pressionado
    pFim : TPoint;   //indica o ponto onde o botao do mouse foi solto
    pCurr : TPoint;  //indica o ponto corrente
    botao: Boolean;    //indica que o botao do mouse foi pressionado no paintboxtela
    botaoArras : Boolean;  // Variavel que controla se o botao esta sendo arrastado
    objDesenha: TDesenho; //Objeto do tipo TDesenho
    listaFiguras: TList; //lista de figuras que estao no paintboxtela
    listaFigurasParaProducao: TList; //lista de figuras que estao no paintboxtela
    listaFerragens : array[0..200] of integer; //marca na posi��o da figura se � uma ferragem ou n�o
    figCorrente : TRect;  //Variavel auxiliar usada para criar o rascunho da nova fig.
    cor : TColor;    //seleciona a cor
    op : Integer;   //tipo da figura: retangulo, circulo, linha
    cont : Integer; //Conta quantas figuras estao inseridas na lista
    contProducao : Integer;
    hab : Boolean; //Variavel que controla se as figuras estao habilitadas
    idFig : Integer; //Id da figura no banco de dados
    idCarr : Boolean; // Controla se o id da figura ja foi carregado
    ferrag : Boolean;    //indica se o form ferragens esta aberto
    largura : Integer;  // largura do objeto
    altura : Integer; //altura do objeto
    ctrlTag : Integer;  //guarda o self.tag de uma figura quando uma tecla de atalho � pressionado
    tmpColor : Tcolor; //variavel temporaria que guarda a cor (usada no TListBox)
    listBoxContr : Boolean; // varivel que controla a entrada no TlistBox
    polyclick : Boolean;   // marca que a op��o do polyline sera desenhado
    points:  array[0..200] of Tpoint;  //array de pontos que guardara as informa��es dos clicks
    indCurr : Integer; //indice que determina quantos pontos ja foram selecionados para criar o poligon
    Drawing : Boolean; //indica que o botao foi clicado e que o desenho selecionado foi o poligon
    entrad : Boolean; //indica que � a primeira entrada dos pontos do poligon
    menorx : integer; //menor x encontrado nos pontos dos poligonos
    menory : integer; // meno y encontrado nos pontos dos poligonos
    pux_banc : boolean; //verifica se uma figura foi carregada do banco

    auxStr : string; {Grava o nome do servico anterior armazenado no label antes de ser apagado}

    procedure Importar();
    procedure Arruma ();
    procedure AdicionaLista(NomeFigura:string; TipoMaterial:string ; CodigoMaterial:string);
    procedure AdicionaListaIMGProducao(NomeFigura:string; TipoMaterial:string ; CodigoMaterial:string);
    procedure RetiraLista(pos : integer);
    procedure RetiraListaProducao( pos : integer);
    procedure Habilita(i : Integer);
    procedure DefineTamanhoFerragem();
    procedure DefineTamanhoPoligon();
    procedure CriarFigura(NomeFigura:string ; TipoMaterial:string ; CodigoMaterial:string);
    procedure CriarFiguraIMGProducao(NomeFigura:string ; TipoMaterial:string; CodigoMaterial:string);
    Procedure GerarLinhaMedidas(Tipo:string;direcao:string;tagcomp:integer;tag_aux:Integer);
    Function  VerificaEspacoVazioDesenhaLinha(direcao:string):Boolean;

    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    Procedure ResgataFerragens_Proj;
    Procedure PreparaFerragem_Proj;
    Procedure ResgataPerfilado_Proj;
    Procedure PreparaPerfilado_Proj;
    Procedure ResgataComponente_Proj;
    Procedure PreparaComponente_Proj;
    Procedure ResgataKitBox_Proj;
    Procedure PreparaKitBox_Proj;
    Procedure ResgataServico_Proj;
    Procedure PreparaServico_Proj;
    Procedure ResgataDiverso_Proj;
    Procedure PreparaDiverso_Proj;
    procedure Replicar;
    procedure MostraQuatidadeProjeto;
    procedure LimpaTudo();
    procedure LimpaTudoIMGProducao();
    procedure ConfiguraStringGrid;
    function LocalizaPosicaonoComponente:Integer;
    function LocalizaPosicaonoComponenteProducao:Integer;
    function FinalizarProjeto:Boolean;
    function VerificaFerragemInserida(var comp:integer):Integer;
    function LocalizaPosicaoListaComponente(parametro:Integer):Integer;
    function LocalizaTagCompAoCarregar(parametro:string):Integer;
    function LocalizaTagListaXYREFDentroComponente(parametro:integer;parametro2:integer):Integer;
    procedure DesabilitaHabilita(acao:Boolean);
    function RetornaLinhaReferentetTagOBj:Integer;
    procedure MostraServicosProducao();
    procedure GravaServicoProducao;
    procedure __MarcaServicoPorComponente;
    procedure __RetiraServicoProducaodaLista;
    Function ___VerificaDuplicidadePorComponente_Servicos(TagComponente:string;var paramaetroVerificaIgual:Boolean):Boolean;
    procedure __GravaServicosPorComponentes;
    procedure __CarregarMarcadoresServicos;
    procedure __LimpaStringGrid;

  public
      procedure AbreComCodigo(parametro:string);
  end;


var
  FPROJETO: TFPROJETO;

implementation

uses UessencialLocal, UDataModulo, UpesquisaProjeto, UopcaoRel,
  UescolheImagemBotao,Upesquisa,UGRUPOVIDRO, Uprincipal, Ferragens,
  UobjGRUPOVIDRO, DateUtils, Math, UvideosAjuda, IBCustomDataSet, UobjCEST;

{$R *.dfm}


procedure TFPROJETO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjPROJETO=Nil)
     Then exit;

     If (Self.ObjPROJETO.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjPROJETO.free;
    Self.ObjFERRAGEM_PROJ.Free;
    Self.ObjPERFILADO_PROJ.Free;
    Self.ObjCOMPONENTE_PROJ.Free;
    Self.ObjSERVICO_PROJ.Free;
    Self.ObjKITBOX_PROJ.Free;
    Self.ObjDiverso_Proj.Free;
    ListaComponentes.Free;
    listaFiguras.Free;
    listaFigurasParaProducao.Free;
    ListaServicosProducao.Free;

    Action := caFree;
    Self := nil;
end;


procedure TFPROJETO.btNovoClick(Sender: TObject);
begin
     if (ObjPermissoesUsoGlobal.ValidaPermissao('CRIAR UM NOVO PROJETO')=False)
     Then EXIT;


     limpaedit(Self);
     //ImageDesenho.Visible:=false;
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);
     lbCodigoProjeto.caption:='0';

     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjPROJETO.status:=dsInsert;
     Guia.TabIndex:=0;
     RadioAlturaPorLargura.Checked:=true;
     ComboProjetoBranco.Text:='N';
     EdtAlturaMaxima.text:='0';
     EdtAlturaMinima.text:='0';
     EdtLarguraMaxima.text:='0';
     EdtLarguraMinima.text:='0';
     EdtAreaMinima.text:='0';
     EdtMaoDeObra.text:='0';

     if (ObjPROJETO.VerificaSeJaTemProjetoBranco=true)then
     Begin
          ComboProjetoBranco.Enabled:=false;
     end else ComboProjetoBranco.Enabled:=true;

     EdtReferencia.setfocus;
     
end;

procedure TFPROJETO.btSalvarClick(Sender: TObject);
var
  strRef: string; {rodolfo}
begin

    if(lbCodigoProjeto.Caption = '')
    then exit;

     If ObjPROJETO.Status=dsInactive
     Then exit;

     If (EdtReferencia.Text<>'')then    begin
         strRef := StrReplaceRef(edtReferencia.Text); {rodolfo}
         If (ObjPROJETO.VerificaReferencia(ObjPROJETO.Status, lbCodigoProjeto.caption, strRef)=true) then
         Begin
             MensagemErro('Refer�ncia j� existe.');
             exit;
         end;
     end;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjPROJETO.salvar(true)=False)
     Then exit;

     lbCodigoProjeto.caption:=ObjPROJETO.Get_codigo;
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     desabilita_campos(Self);
     MostraQuatidadeProjeto;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFPROJETO.btAlterarClick(Sender: TObject);
begin


    if(lbCodigoProjeto.Caption = '')
    then exit;
    btnovo.Visible :=false;
    btalterar.Visible:=false;
    btpesquisar.Visible:=false;
    btrelatorio.Visible:=false;
    btexcluir.Visible:=false;
    btsair.Visible:=false;
    btopcoes.Visible :=false;
    If (ObjPROJETO.Status=dsinactive) and (lbCodigoProjeto.caption<>'')
    Then
    Begin
                if (ObjPermissoesUsoGlobal.ValidaPermissao('ALTERAR UM PROJETO')=False)
                Then EXIT;

                habilita_campos(Self);
                ObjPROJETO.Status:=dsEdit;
                guia.TabIndex:=0;
                EdtReferencia.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                ObjPROJETO.ReferenciaAnterior:=EdtReferencia.Text;
                ComboProjetoBranco.Enabled:=false;
    End;

end;

procedure TFPROJETO.btCancelarClick(Sender: TObject);
begin
     ObjPROJETO.cancelar;

     limpaedit(Self);
     ImageDesenho.Visible:=false;
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     lbCodigoProjeto.caption:='';
     ImageDesenho.Stretch:=False;
     FescolheImagemBotao.PegaFiguraImagem(ImageDesenho,'FOTO');
     ImageDesenho.Visible:=True;

end;

procedure TFPROJETO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisaProjeto;
   CaminhoDesenho:string;
begin

        Try
           Fpesquisalocal:=TFpesquisaProjeto.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjPROJETO.Get_pesquisa,ObjPROJETO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjPROJETO.status<>dsinactive
                                  then exit;

                                  If (ObjPROJETO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjPROJETO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            ImageDesenho.Visible:=false;
                                            Self.limpaLabels;
                                            exit;
                                  End;
                                  CarregaDesenhoProjeto(true,True);
                                  try
                                     ImageDesenho.Stretch:=True;
                                     ImageDesenho.Visible:=true;
                                     CaminhoDesenho:=ObjPROJETO.ResgataDesenho(EdtReferencia.Text);

                                     if (CaminhoDesenho<>'')
                                     then Begin
                                                if (FileExists(CaminhoDesenho))Then
                                                Begin
                                                          ImageDesenho.Visible:=True;

                                                          ImageDesenho.Picture.LoadFromFile(CaminhoDesenho);
                                                End
                                                Else ImageDesenho.Visible:=False;
                                     End
                                     else
                                    
                                  except
                                      MensagemErro('Erro ao tentar transferir o desenho.');
                                  end;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
        Guia.TabIndex:=0;
       // Notebook.PageIndex:=0;
end;

procedure TFPROJETO.btExcluirClick(Sender: TObject);
begin


     If (ObjPROJETO.status<>dsinactive) or ( lbCodigoProjeto.caption='')
     Then exit;

     If (ObjPROJETO.LocalizaCodigo( lbCodigoProjeto.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('EXCLUIR UM PROJETO')=False)
     Then EXIT;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     if (ObjPROJETO.ExcluiRelacionamento( lbCodigoProjeto.caption)=false)then
     Begin
          MensagemErro('N�o foi poss�vel excluir este projeto.');
          exit;
     end;

     // Deleto o arquivo do desenho para naum dar duplicidade nos proximos cadasros
     if (ObjPROJETO.ExcluiDesenho(ObjPROJETO.Get_Referencia+'.bmp')=false)then
     Begin
          MensagemErro('Erro ao tentar deletar o desenho da pasta do servidor.');
          exit;
     end;

     if (ObjPROJETO.exclui( lbCodigoProjeto.caption,True)=False) then
     begin
      Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
      exit;
     end;

     limpaedit(Self);
     ImageDesenho.Visible:=false;
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFPROJETO.btRelatorioClick(Sender: TObject);
begin
//    ObjPROJETO.Imprime( lbCodigoProjeto.caption);
end;

procedure TFPROJETO.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFPROJETO.PosicionaForm;
begin
    Self.Caption:='      '+Self.Caption;
    Self.Left:=-18;
    Self.Top:=0;
end;
procedure TFPROJETO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFPROJETO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFPROJETO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    If Key=VK_Escape
    Then Self.Close;

    If (Key = VK_F2) then
    begin

           FAjuda.PassaAjuda('CADASTRO DE PROJETOS');
           FAjuda.ShowModal;
    end;

    if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;
    if(Guia.TabIndex=7)then
    begin
      //Ctrl+C   - Copia o self.tag da Figura
      if (ssCtrl in Shift) and (key = 67) then
      begin
            if(Self.Tag <> -1)then
              ctrlTag := Self.Tag
            else ShowMessage('Nenhuma figura selecionada');
      end;

      //Ctrl+V - Cria uma nova figura com base nas informa��es da figura guardada
      if (ssCtrl in Shift) and (key = 86) then
      begin //Ctrl+V
            ColarCtrlV1Click(sender);
      end;

      //Ctrl+S  -  Sava o projeto da figura
      if (ssCtrl in Shift) and (key = 83) then
      begin
            btSalvarModelagemClick(Sender);
      end;

      //DEL   -     Exclui figura
      if (key = VK_DELETE) then
      begin
              btLimpaDesenhoClick(Sender);
      end;

      //Aumenta a largura
      if(ssShift in Shift) and (Key = 39)then
      begin
              if Self.Tag <> - 1 then
              begin
                TDesenho(listaFiguras[Self.Tag]).pt2.x := TDesenho(listaFiguras[Self.Tag]).pt2.x+1;
                TDesenho(listaFiguras[Self.Tag]).setBounds(TDesenho(listaFiguras[Self.Tag]).pt1.x, TDesenho(listaFiguras[Self.Tag]).pt1.y, TDesenho(listaFiguras[Self.Tag]).Width+1, TDesenho(listaFiguras[Self.Tag]).Height);
                EdtX.Text := inttostr(TDesenho(listaFiguras[Self.Tag]).pt1.x);
              end;

      end;

      //diminui a largura
      if(ssShift in Shift) and ( key = 37 ) then
      begin
              if Self.Tag <> - 1 then
              begin
                TDesenho(listaFiguras[Self.Tag]).pt2.x := TDesenho(listaFiguras[Self.Tag]).pt2.x+1;
                TDesenho(listaFiguras[Self.Tag]).setBounds(TDesenho(listaFiguras[Self.Tag]).pt1.x, TDesenho(listaFiguras[Self.Tag]).pt1.y, TDesenho(listaFiguras[Self.Tag]).Width-1, TDesenho(listaFiguras[Self.Tag]).Height);
                EdtX.Text := inttostr(TDesenho(listaFiguras[Self.Tag]).pt1.x);

              end;
             
      end;
      //Aumenta o tamanho
      if(ssShift in Shift) and ( key = 38 ) then
      begin
              if Self.Tag <> - 1 then
              begin
                TDesenho(listaFiguras[Self.Tag]).pt2.y := TDesenho(listaFiguras[Self.Tag]).pt2.Y-1;
                TDesenho(listaFiguras[Self.Tag]).setBounds(TDesenho(listaFiguras[Self.Tag]).pt1.x, TDesenho(listaFiguras[Self.Tag]).pt1.y, TDesenho(listaFiguras[Self.Tag]).Width, TDesenho(listaFiguras[Self.Tag]).Height-1);
                EdtY.Text := inttostr(TDesenho(listaFiguras[Self.Tag]).pt1.y);
              end;
              
      end;

      //dimunui o tamanho
      if(ssShift in Shift) and ( key = 40 ) then
      begin
              if Self.Tag <> - 1 then
              begin
                TDesenho(listaFiguras[Self.Tag]).pt2.y := TDesenho(listaFiguras[Self.Tag]).pt2.y+1;
                TDesenho(listaFiguras[Self.Tag]).setBounds(TDesenho(listaFiguras[Self.Tag]).pt1.x, TDesenho(listaFiguras[Self.Tag]).pt1.y, TDesenho(listaFiguras[Self.Tag]).Width, TDesenho(listaFiguras[Self.Tag]).Height+1);
                EdtY.Text := inttostr(TDesenho(listaFiguras[Self.Tag]).pt1.y);
              end;

      end;

      //Movimenta o objeto para direita
      if(ssCtrl in Shift) and (Key = 39)then
      begin
              if Self.Tag <> - 1 then
              begin
                TDesenho(listaFiguras[Self.Tag]).pt1.x := TDesenho(listaFiguras[Self.Tag]).pt1.x+1;
                TDesenho(listaFiguras[Self.Tag]).setBounds(TDesenho(listaFiguras[Self.Tag]).pt1.x, TDesenho(listaFiguras[Self.Tag]).pt1.y, TDesenho(listaFiguras[Self.Tag]).Width, TDesenho(listaFiguras[Self.Tag]).Height);
                EdtX.Text := inttostr(TDesenho(listaFiguras[Self.Tag]).pt1.x);
              end;
      end;

      //Movimenta o objeto para esquerda
      if(ssCtrl in Shift) and ( key = 37 ) then
      begin
              if Self.Tag <> - 1 then
              begin
                TDesenho(listaFiguras[Self.Tag]).pt1.x := TDesenho(listaFiguras[Self.Tag]).pt1.x-1;
                TDesenho(listaFiguras[Self.Tag]).setBounds(TDesenho(listaFiguras[Self.Tag]).pt1.x, TDesenho(listaFiguras[Self.Tag]).pt1.y, TDesenho(listaFiguras[Self.Tag]).Width, TDesenho(listaFiguras[Self.Tag]).Height);
                EdtX.Text := inttostr(TDesenho(listaFiguras[Self.Tag]).pt1.x);
              end;
      end;

      //Movimenta o objeto para cima
      if(ssCtrl in Shift) and ( key = 38 ) then
      begin
              if Self.Tag <> - 1 then
              begin
                TDesenho(listaFiguras[Self.Tag]).pt1.y := TDesenho(listaFiguras[Self.Tag]).pt1.y-1;
                TDesenho(listaFiguras[Self.Tag]).setBounds(TDesenho(listaFiguras[Self.Tag]).pt1.x, TDesenho(listaFiguras[Self.Tag]).pt1.y, TDesenho(listaFiguras[Self.Tag]).Width, TDesenho(listaFiguras[Self.Tag]).Height);
                EdtY.Text := inttostr(TDesenho(listaFiguras[Self.Tag]).pt1.y);
              end;
      end;

      //Movimenta o objeto para baixo
      if(ssCtrl in Shift) and ( key = 40 ) then
      begin
              if Self.Tag <> - 1 then
              begin
                TDesenho(listaFiguras[Self.Tag]).pt1.y := TDesenho(listaFiguras[Self.Tag]).pt1.y+1;
                TDesenho(listaFiguras[Self.Tag]).setBounds(TDesenho(listaFiguras[Self.Tag]).pt1.x, TDesenho(listaFiguras[Self.Tag]).pt1.y, TDesenho(listaFiguras[Self.Tag]).Width, TDesenho(listaFiguras[Self.Tag]).Height);
                EdtY.Text := inttostr(TDesenho(listaFiguras[Self.Tag]).pt1.y);
              end;
      end;
    end;

end;
//Procedimento que copia os dados de uma figura para posteriormente poder ser recriada
procedure TFPROJETO.CopiarCtrlC1Click(Sender: TObject);
begin
 if(Self.Tag <> -1)then
    ctrlTag := Self.Tag
 else ShowMessage('Nenhuma figura selecionada');
end;

//Cria uma nova figura e coloca-a na lista de figuras
procedure TFPROJETO.ColarCtrlV1Click(Sender: TObject);
var i:integer;
    totPts: integer;
begin
   if (ctrlTag <> -1) then
   begin
       op := TDesenho(listaFiguras[ctrlTag]).TipoFigura;  //tipo do objeto
       cor := TDesenho(listaFiguras[ctrlTag]).tmpColor;

       if(op <> 3)then
       begin
            pFim.X :=  pInic.X + TDesenho(listaFiguras[ctrlTag]).width ;   //xinferior do objeto
            pFim.Y := pInic.Y + TDesenho(listaFiguras[ctrlTag]).height;   //yinferior do objeto
       end
       else
       begin
          totPts:= TDesenho(listaFiguras[ctrlTag]).tam;
          for i := 0 to totPts do
              points[i] := TDesenho(listaFiguras[ctrlTag]).pontos[i];
          indCurr := totPts+1;
       end;

       criarFigura('','','');
       cor := clBtnFace;
   end
   else showmessage('Nenhuma figura selecionada');
end;


procedure TFPROJETO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFPROJETO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjPROJETO do
    Begin
        Submit_Codigo( lbCodigoProjeto.caption);
        Submit_Referencia(UpperCase(edtReferencia.text));
        Submit_Descricao(edtDescricao.text);
        Submit_Complemento(MemoComplemento.text);
        Submit_AlturaMinima(edtAlturaMinima.text);
        Submit_LarguraMinima(edtLarguraMinima.text);
        Submit_AlturaMaxima(edtAlturaMaxima.text);
        Submit_LarguraMaxima(edtLarguraMaxima.text);
        Submit_AreaMinima(edtAreaMinima.text);
        Submit_ProjetoBranco(ComboProjetoBranco.Text);
        Submit_NCM(edtNCM.Text);
        Submit_cest(edtCest.Text);


        if (RadioAlturaPorLargura.Checked = true)then
            Submit_Eixo('AxL')
        else
        if (RadioAlturaPorLargura_AlturaPorLargura.Checked=true)then
            Submit_Eixo('AxL AxL')
        else
        if (RadioVarios.Checked=true)then
            Submit_Eixo('VARIOS');

        Submit_MaoDeObra(edtMaoDeObra.text);
        GrupoVidro.Submit_codigo(edtGrupoVidro.text);//CODIFICA SUBMITS

        if(COMBOUtilizaKitPerfilado.Text='S')
        then Submit_UtilizaKitPerfilado('S')
        else Submit_UtilizaKitPerfilado('N');

        Submit_esquadria(ComboEsquadria.Text);

         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFPROJETO.LimpaLabels;
begin
//LIMPA LABELS
     LbNomeGrupoVidro.caption:='';
end;

function TFPROJETO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjPROJETO do
     Begin
         lbCodigoProjeto.caption:=Get_Codigo;
        EdtReferencia.text:=Get_Referencia;
        EdtDescricao.text:=Get_Descricao;
        MemoComplemento.text:=Get_Complemento;
        
        EdtAlturaMinima.text:=Get_AlturaMinima;
        EdtLarguraMinima.text:=Get_LarguraMinima;
        EdtAlturaMaxima.text:=Get_AlturaMaxima;
        EdtLarguraMaxima.text:=Get_LarguraMaxima;
        EdtAreaMinima.text:=Get_AreaMinima;
        ComboProjetoBranco.Text:=Get_ProjetoBranco;
        edtNCM.text:=Get_NCM;
        edtCest.Text := Get_cest;

        if (Get_eixo='AXL')
        Then RadioAlturaPorLargura.Checked:=True
        Else
           if (Get_eixo='AXL AXL')
           Then RadioAlturaPorLargura_AlturaPorLargura.Checked:=True
           Else
              if (Get_Eixo='VARIOS')
              then RadioVarios.Checked:=True;

        EdtMaoDeObra.text:=Get_MaoDeObra;
        EdtGrupoVidro.text:=GrupoVidro.Get_codigo;
        LbNomeGrupoVidro.Caption:=GrupoVidro.Get_Nome;
        if(Get_UtilizaKitPerfilado='S')
        then COMBOUtilizaKitPerfilado.Text:='S'
        else COMBOUtilizaKitPerfilado.text:='N';

        if(Get_esquadria='S')
        then ComboEsquadria.Text:='S'
        else ComboEsquadria.Text:='N';

     

        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFPROJETO.TabelaParaControles: Boolean;
begin
     If (ObjPROJETO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFPROJETO.edtGrupoVidroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FgrupoVidro:TFGRUPOVIDRO;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FgrupoVidro:=TFGRUPOVIDRO.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('select * from tabgrupovidro','Pesquisa de Grupo de vidros',FgrupoVidro)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtGrupoVidro.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 LbNomeGrupoVidro.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FgrupoVidro);

     End;
end;


procedure TFPROJETO.edtGrupoVidroExit(Sender: TObject);
begin
    ObjPROJETO.EdtGrupoVidroExit(Sender, LbNomeGrupoVidro);
end;

procedure TFPROJETO.btGravarFerragem_ProjClick(Sender: TObject);
begin

    With ObjFERRAGEM_PROJ do
    Begin

         ZerarTabela;
         if (edtCodigoFerragem_Proj.Text='')
         or (edtCodigoFerragem_Proj.Text='0')
         then Begin
              Status:=dsInsert;
              edtCodigoFerragem_Proj.Text:='0';
              if (VerificaSeJaExisteEstaFerragemNesteProjeto( lbCodigoProjeto.caption, edtFerragem_Proj.Text)=true)then
              Begin
                   MensagemErro('Esta Ferragem j� foi cadastrada para este projeto');
                   exit;
              end;
         end
         else Status:=dsEdit;

         Submit_Codigo(edtCodigoFerragem_Proj.Text);
         Ferragem.Submit_Codigo(edtFerragem_Proj.Text);
         Projeto.Submit_Codigo( lbCodigoProjeto.caption);
         Submit_Quantidade(EdtQtdeFerragem_Proj.Text);

       {  if(edtComponenteFerragem.Visible=True) then
         begin
                Submit_Ligadacomponente('S');
                Submit_ComponenteLigacao(edtComponenteFerragem.Text);
         end; }

         if (Salvar(True)=False)
         Then Begin
                  edtFerragem_ProjReferencia.SetFocus;
                  exit;
         End;
         lbNomeFerragem_Proj.caption:='';
         limpaedit(PanelFerragem_Proj);
         edtFerragem_ProjReferencia.SetFocus;
         Self.ResgataFerragens_Proj;
        { edtComponenteFerragem.Visible:=False;
         chk1.Checked:=False;  }
    end;
end;

procedure TFPROJETO.ResgataFerragens_Proj;
begin
   ObjFERRAGEM_PROJ.Resgata_Ferragem_Proj( lbCodigoProjeto.caption);
end;

procedure TFPROJETO.PreparaFerragem_Proj;
begin
     //Resgatando as Cores para essa ferragem
     Self.ResgataFerragens_Proj;
     //habilitando os edits desse panel
     limpaedit(PanelFerragem_Proj);
     habilita_campos(PanelFerragem_Proj);
end;

procedure TFPROJETO.edtFerragem_ProjReferenciaExit(Sender: TObject);
begin
   ObjFERRAGEM_PROJ.EdtFerragemExit(Sender, edtFerragem_Proj, lbNomeFerragem_Proj);
end;

procedure TFPROJETO.edtFerragem_ProjReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   ObjFERRAGEM_PROJ.EdtFerragemKeyDown(Sender, edtFerragem_Proj, Key, Shift, lbNomeFerragem_Proj);
end;

procedure TFPROJETO.dbgridFerragem_ProjDblClick(Sender: TObject);
begin

     If (DBGridFerragem_Proj.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridFerragem_Proj.DataSource.DataSet.RecordCount=0)
     Then exit;             

     limpaedit(PanelFerragem_Proj);
     lbNomeFerragem_Proj.caption:='';
     if (ObjFERRAGEM_PROJ.LocalizaCodigo(DBGridFerragem_Proj.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataFerragens_Proj;
               exit;
     End;

     With ObjFERRAGEM_PROJ do
     Begin
          TabelaparaObjeto;
          edtCodigoFerragem_Proj.Text:=Get_Codigo;
          edtFerragem_ProjReferencia.Text:=Ferragem.Get_Referencia;
          edtFerragem_Proj.Text:=Ferragem.Get_Codigo;
          lbNomeFerragem_Proj.Caption:=Ferragem.Get_Descricao;
          EdtQtdeFerragem_Proj.Text:=Get_Quantidade;
          edtFerragem_ProjReferencia.SetFocus;

        {  if(Get_Ligadacomponente='S') then
          begin
              chk1.Checked:=True;
              edtComponenteFerragem.Visible:=True;
              edtComponenteFerragem.Text:=Get_ComponenteLigacao;
          end
          else
          begin
              edtComponenteFerragem.Visible:=False;
              chk1.Checked:=False;
          end;  }
     End;
     

end;

procedure TFPROJETO.btExcluirFerragem_ProjClick(Sender: TObject);
begin
     if (edtCodigoFerragem_Proj.Text='')then
     exit;

     If (DBGridFerragem_Proj.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridFerragem_Proj.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir essa Ferragem Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     ObjFERRAGEM_PROJ.Exclui(DBGridFerragem_Proj.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     limpaedit(PanelFerragem_Proj);
     lbNomeFerragem_Proj.Caption:='';
     Self.ResgataFerragens_Proj;

    // edtComponenteFerragem.Visible:=False;
    // chk1.Checked:=False;

end;

procedure TFPROJETO.btCancelarFerragem_ProjClick(Sender: TObject);
begin
   limpaedit(PanelFerragem_Proj);
   lbNomeFerragem_Proj.Caption:='';
   edtFerragem_ProjReferencia.SetFocus;
  // edtComponenteFerragem.Visible:=False;
   //chk1.Checked:=False;
end;

procedure TFPROJETO.btGravarPerfialdo_ProjClick(Sender: TObject);
begin
    if (edtFormulaLArguraPerfilado_Proj.Text='')then
    Begin
          MensagemErro('Campo Largura n�o pode ficar vazio.');
          exit;
    end;

    if (edtFormulaAlturaPerfilado_Proj.Text='')then
    Begin
          MensagemErro('Campo Altura n�o pode ficar vazio.');
          exit;
    end;

    if( get_campoTabela('kitperfilado','codigo','tabperfilado',EdtPerfilado_Proj.Text) = 'S' )then
      if(StrToCurrDef(edtquantidade.Text,0) = 0) then
      begin
        MensagemErro('Preencha a quantidade');
        edtquantidade.SetFocus;
        exit;
      end;

    With ObjPERFILADO_PROJ do
    Begin
         ZerarTabela;
         if (EdtCodigoPerfilado_Proj.Text='')
         or (edtCodigoPerfilado_Proj.Text='0')
         then Begin
              Status:=dsInsert;
              edtCodigoPerfilado_Proj.Text:='0';
              if (VerificaSeJaExisteEstaPerfiladoNesteProjeto( lbCodigoProjeto.caption, edtPerfilado_Proj.Text)=true)then
              Begin
                   MensagemErro('Este Perfilado j� foi cadastrada para este projeto');
                   exit;
              end;
         end
         else Status:=dsEdit;

         Submit_Codigo(EdtCodigoPerfilado_Proj.Text);
         Perfilado.Submit_Codigo(EdtPerfilado_Proj.Text);
         Projeto.Submit_Codigo( lbCodigoProjeto.caption);
         Submit_FormulaAltura(edtFormulaAlturaPerfilado_Proj.Text);
         Submit_FormulaLargura(edtFormulaLArguraPerfilado_Proj.Text);
         if(chkesquadriaperfilado.Checked=True)
         then begin
            Submit_Formulaquantidade(edtquantidade.Text);
            Submit_Esquadria('S');
         end
         else begin
            Submit_quantidade(edtquantidade.Text);
            Submit_Esquadria('N');
         end;

         if (Salvar(True)=False)
         Then Begin
                  EdtPerfilado_ProjReferencia.SetFocus;
                  exit;
         End;
         lbNomePerfilado_Proj.caption:='';
         limpaedit(PanelPerfilado_Proj);
         EdtPerfilado_ProjReferencia.SetFocus;
         Self.ResgataPerfilado_Proj;

         

    end;

end;

procedure TFPROJETO.PreparaPerfilado_Proj;
begin
     Self.ResgataPerfilado_Proj;
     limpaedit(PanelPerfilado_Proj);
     habilita_campos(PanelPerfilado_Proj);
     if(ComboEsquadria.Text='S')
     then chkesquadriaperfilado.Visible:=True
     else chkesquadriaperfilado.Visible:=False;
end;

procedure TFPROJETO.ResgataPerfilado_Proj;
begin
ObjPerfilado_PROJ.Resgata_Perfilado_Proj( lbCodigoProjeto.caption);
end;


procedure TFPROJETO.GuiaChange2(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin

     if (newtab=0)//Projeto - Principal
     Then Begin
               if ( lbCodigoProjeto.caption<>'') and (ObjPROJETO.Status=dsinactive)
               Then Begin
                         if (ObjPROJETO.LocalizaCodigo( lbCodigoProjeto.caption)=False)
                         Then exit;
                         ObjPROJETO.TabelaparaObjeto;
                         Self.ObjetoParaControles;
               End;
               //Notebook.PageIndex:=NewTab;

     End;

     //  Ferragem
     if (newtab=1)
     Then Begin
               if (ObjPROJETO.Status=dsinsert)
               or ( lbCodigoProjeto.caption='')
               Then Begin
                         AllowChange:=False;
                         exit;
               End;

               Self.PreparaFerragem_Proj;
              // Notebook.PageIndex:=NewTab;
               edtFerragem_ProjReferencia.SetFocus;

     End;


     // Perfilado
     if (newtab=2)
     Then Begin
               if (ObjPROJETO.Status=dsinsert)
               or ( lbCodigoProjeto.caption='')
               Then Begin
                         AllowChange:=False;
                         exit;
               End;

               Self.PreparaPerfilado_Proj;
               //Notebook.PageIndex:=NewTab;
               EdtPerfilado_ProjReferencia.SetFocus;

     End;


     // Componente
     if (newtab=3)
     Then Begin
               if (ObjPROJETO.Status=dsinsert)
               or ( lbCodigoProjeto.caption='')
               Then Begin
                         AllowChange:=False;
                         exit;
               End;

               Self.PreparaComponente_Proj;
              // Notebook.PageIndex:=NewTab;
               edtComponente_ProjReferencia.SetFocus;

     End;


     // KitBox
     if (newtab=4)
     Then Begin
               if (ObjPROJETO.Status=dsinsert)
               or ( lbCodigoProjeto.caption='')
               Then Begin
                         AllowChange:=False;
                         exit;
               End;

               Self.PreparaKitBox_Proj;
               //Notebook.PageIndex:=NewTab;
               PanelKitBox_Proj.Visible:=True;
               edtKitBox_ProjReferencia.SetFocus;

     End;


     // Servico
     if (newtab=5)
     Then Begin
               if (ObjPROJETO.Status=dsinsert)
               or ( lbCodigoProjeto.caption='')
               Then Begin
                         AllowChange:=False;
                         exit;
               End;

               Self.PreparaServico_Proj;
               //Notebook.PageIndex:=NewTab;
               edtServico_ProjReferencia.SetFocus;

     End;

     // Diverso
     if (newtab=6)
     Then Begin
               if (ObjPROJETO.Status=dsinsert)
               or ( lbCodigoProjeto.caption='')
               Then Begin
                         AllowChange:=False;
                         exit;
               End;

               Self.PreparaDiverso_Proj;
               //Notebook.PageIndex:=NewTab;
               edtDiverso_ProjReferencia.SetFocus;
              
     End;

end;

procedure TFPROJETO.edtPerfilado_ProjReferenciaExit(Sender: TObject);
var
  Kitperfilado:Boolean;
begin
     ObjPERFILADO_PROJ.EdtPerfiladoExit(Sender, EdtPerfilado_Proj, lbNomePerfilado_Proj,kitperfilado);
     if(kitperfilado=True) then
     begin
          lb38.Visible:=True;
          edtquantidade.Visible:=True;
          edtFormulaLArguraPerfilado_Proj.Visible:=False;
          edtFormulaAlturaPerfilado_Proj.Visible:=False;
          lb30.Visible:=False;
          lb29.Visible:=False;
          edtFormulaLArguraPerfilado_Proj.Text:='L';
          edtFormulaAlturaPerfilado_Proj.Text:='A';
     end
     else
     begin
          lb38.Visible:=false;
          edtquantidade.Visible:=false;
          edtFormulaLArguraPerfilado_Proj.Visible:=True;
          edtFormulaAlturaPerfilado_Proj.Visible:=True;
          lb30.Visible:=True;
          lb29.Visible:=True;
     end;
end;

procedure TFPROJETO.edtPerfilado_ProjReferenciaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjPERFILADO_PROJ.EdtPerfiladoKeyDown(Sender, EdtPerfilado_Proj, Key, Shift, lbNomePerfilado_Proj);
end;

procedure TFPROJETO.dbgridPerfilado_ProjDblClick(Sender: TObject);
begin
     If (DBGridPerfilado_Proj.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridPerfilado_Proj.DataSource.DataSet.RecordCount=0)
     Then exit;

     limpaedit(PanelPerfilado_Proj);
     lbNomePerfilado_Proj.caption:='';
     if (ObjPerfilado_PROJ.LocalizaCodigo(DBGridPerfilado_Proj.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataFerragens_Proj;
               exit;
     End;

     With ObjPerfilado_PROJ do
     Begin
          TabelaparaObjeto;
          edtCodigoPerfilado_Proj.Text:=Get_Codigo;
          EdtPerfilado_ProjReferencia.Text:=Perfilado.Get_Referencia;
          edtPerfilado_Proj.Text:=Perfilado.Get_Codigo;
          lbNomePerfilado_Proj.Caption:=Perfilado.Get_Descricao;
          edtFormulaLArguraPerfilado_Proj.Text:=Get_FormulaLargura;
          edtFormulaAlturaPerfilado_Proj.Text:=Get_FormulaAltura;
          if(Perfilado.Get_KitPerfilado='S') then
          begin
               edtquantidade.Visible:=True;
               edtquantidade.Text:=Get_Quantidade;
               edtFormulaLArguraPerfilado_Proj.Visible:=False;
               edtFormulaAlturaPerfilado_Proj.visible:=False;
               lb30.Visible:=False;
               lb29.Visible:=False;
               lb38.Visible:=True;
               chkesquadriaperfilado.Checked:=false;
          end
          else
          begin
               if(Get_Esquadria='S')then
               begin
                  //edtquantidade.Visible:=True;
                  //edtquantidade.Text:=Get_Formulaquantidade;
                  chkesquadriaperfilado.Checked:=True;
               end
               else
               begin
                  edtquantidade.Visible:=False;
                  edtFormulaLArguraPerfilado_Proj.Visible:=True;
                  edtFormulaAlturaPerfilado_Proj.visible:=True;
                  lb30.Visible:=True;
                  lb29.Visible:=True;
                  lb38.Visible:=false;
                  chkesquadriaperfilado.Checked:=false;
               end;
          end;
          EdtPerfilado_ProjReferencia.SetFocus;
     End;

end;

procedure TFPROJETO.btExcluirPerfialdo_ProjClick(Sender: TObject);
begin
     if (EdtCodigoPerfilado_Proj.Text='')then
     exit;

     If (DBGridPerfilado_Proj.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridPerfilado_Proj.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir esse Perfilado Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     ObjPerfilado_PROJ.Exclui(DBGridPerfilado_Proj.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     limpaedit(PanelPerfilado_Proj);
     lbNomePerfilado_Proj.Caption:='';
     Self.ResgataPerfilado_Proj;

end;

procedure TFPROJETO.btCancelarPerfilado_ProjClick(Sender: TObject);
begin
   limpaedit(PanelPerfilado_Proj);
   lbNomePerfilado_Proj.Caption:='';
   EdtPerfilado_ProjReferencia.SetFocus;
end;

procedure TFPROJETO.PreparaComponente_Proj;
begin
     Self.ResgataComponente_Proj;
     limpaedit(PanelComponente_Proj);
     ComboCorteComponente_Proj.Text:='';
     habilita_campos(PanelComponente_Proj);
     if(ComboEsquadria.Text='S')
     then chkComponenteEsquadria.Visible:=True
     else chkComponenteEsquadria.Visible:=False;
end;

procedure TFPROJETO.ResgataComponente_Proj;
begin
ObjCOMPONENTE_PROJ.Resgata_Componente_Proj( lbCodigoProjeto.caption);
end;

procedure TFPROJETO.brGravarComponente_ProjClick(Sender: TObject);
begin
    if (edtFormulaAlturaComponente_Proj.Text='')then
    Begin
         MensagemErro('Campo Altura n�o pode ficar vazio.');
         edtFormulaAlturaComponente_Proj.SetFocus;
         exit;
    end;

    if (edtFolgaAlturaComponente_Proj.Text='')then
    Begin
         MensagemErro('Campo Folga n�o pode ficar vazio.');
         edtFolgaAlturaComponente_Proj.SetFocus;
         exit;
    end;

    if (edtFormulaLarguraComponente_Proj.Text='')then
    Begin
         MensagemErro('Campo Largura n�o pode ficar vazio.');
         edtFormulaLarguraComponente_Proj.SetFocus;
         exit;
    end;

    if (edtFolgaLarguraComponente_Proj.Text='')then
    Begin
         MensagemErro('Campo Folga n�o pode ficar vazio.');
         edtFolgaLarguraComponente_Proj.SetFocus;
         exit;
    end;

    With ObjComponente_PROJ do
    Begin
         ZerarTabela;
         if (EdtCodigoComponente_Proj.Text='')
         or (edtCodigoComponente_Proj.Text='0')
         then Begin
              Status:=dsInsert;
              edtCodigoComponente_Proj.Text:='0';
              if (VerificaSeJaExisteEsteComponenteNesteProjeto( lbCodigoProjeto.caption, edtComponente_Proj.Text)=true)then
              Begin
                   MensagemErro('Este Perfilado j� foi cadastrada para este projeto');
                   exit;
              end;
         end
         else Status:=dsEdit;

         Submit_Codigo(EdtCodigoComponente_Proj.Text);
         Componente.Submit_Codigo(EdtComponente_Proj.Text);
         Projeto.Submit_Codigo( lbCodigoProjeto.caption);
         Submit_FormulaAltura(edtFormulaAlturaComponente_Proj.Text);
         Submit_FormulaLargura(edtFormulaLarguraComponente_Proj.Text);
         Submit_FolgaAltura(edtFolgaAlturaComponente_Proj.Text);
         Submit_FolgaLargura(edtFolgaLarguraComponente_Proj.Text);
         Submit_Corte(ComboCorteComponente_Proj.Text);

         if(chkComponenteEsquadria.Checked=True)
         then Submit_Esquadria('S')
         else Submit_Esquadria('N');

         
         if (Salvar(True)=False)
         Then Begin
                  edtComponente_ProjReferencia.SetFocus;
                  exit;
         End;
         lbNomeComponente_Proj.caption:='';
         limpaedit(PanelComponente_Proj);
         ComboCorteComponente_Proj.Text:='';
         edtComponente_ProjReferencia.SetFocus;
         Self.ResgataComponente_Proj;
    end;

end;

procedure TFPROJETO.edtComponente_ProjReferenciaExit(Sender: TObject);
begin
  ObjCOMPONENTE_PROJ.EdtComponenteExit(Sender,edtComponente_Proj,lbNomeComponente_Proj);
end;

procedure TFPROJETO.edtComponente_ProjReferenciaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   ObjCOMPONENTE_PROJ.EdtComponenteKeyDown(Sender,edtComponente_Proj, Key,Shift, lbNomeComponente_Proj);
end;

procedure TFPROJETO.dbgridComponente_ProjDblClick(Sender: TObject);
begin
     If (DBGridComponente_Proj.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridComponente_Proj.DataSource.DataSet.RecordCount=0)
     Then exit;

     limpaedit(PanelComponente_Proj);
     lbNomeComponente_Proj.caption:='';
     if (ObjComponente_PROJ.LocalizaCodigo(DBGridComponente_Proj.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataFerragens_Proj;
               exit;
     End;

     With ObjComponente_PROJ do
     Begin
          TabelaparaObjeto;
          edtCodigoComponente_Proj.Text:=Get_Codigo;
          edtComponente_Proj.Text:=Componente.Get_Codigo;
          edtComponente_ProjReferencia.Text:=Componente.Get_Referencia;
          lbNomeComponente_Proj.Caption:=Componente.Get_Descricao;
          edtFormulaLArguraComponente_Proj.Text:=Get_FormulaLargura;
          edtFormulaAlturaComponente_Proj.Text:=Get_FormulaAltura;
          edtFolgaAlturaComponente_Proj.Text:=Get_FolgaAltura;
          edtFolgaLarguraComponente_Proj.Text:=Get_FolgaLargura;
          ComboCorteComponente_Proj.Text:=Get_Corte;
          edtComponente_ProjReferencia.SetFocus;
          if(Get_Esquadria='S')
          then chkComponenteEsquadria.Checked:=True
          else chkComponenteEsquadria.Checked:=False;
     End;

end;

procedure TFPROJETO.brExcuirComponente_ProjClick(Sender: TObject);
begin
     if (edtCodigoComponente_Proj.Text='')then
     exit;

     If (DBGridComponente_Proj.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridComponente_Proj.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir esse Componente Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     ObjComponente_PROJ.Exclui(DBGridComponente_Proj.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     limpaedit(PanelComponente_Proj);
     ComboCorteComponente_Proj.Text:='';
     lbNomeComponente_Proj.Caption:='';
     Self.ResgataComponente_Proj;

end;

procedure TFPROJETO.brCancelarComponente_ProjClick(Sender: TObject);
begin
   limpaedit(PanelComponente_Proj);
   lbNomeComponente_Proj.Caption:='';
   edtComponente_ProjReferencia.SetFocus;
end;

procedure TFPROJETO.PreparaKitBox_Proj;
begin
     Self.ResgataKitBox_Proj;
     limpaedit(PanelKitBox_Proj);
     habilita_campos(PanelKitBox_Proj);
end;

procedure TFPROJETO.ResgataKitBox_Proj;
begin
ObjKitBox_PROJ.Resgata_KitBox_Proj( lbCodigoProjeto.caption);
end;

procedure TFPROJETO.btGravarKitBox_ProjClick(Sender: TObject);
begin
    With ObjKitBox_PROJ do
    Begin
         ZerarTabela;
         if (EdtCodigoKitBox_Proj.Text='')
         or (edtCodigoKitBox_Proj.Text='0')
         then Begin
              Status:=dsInsert;
              edtCodigoKitBox_Proj.Text:='0';
              if (VerificaSeJaExisteEstaKitBoxNesteProjeto( lbCodigoProjeto.caption, edtKitBox_Proj.Text)=true)then
              Begin
                   MensagemErro('Este KitBox j� foi cadastrada para este projeto');
                   exit;
              end;
         end
         else Status:=dsEdit;

         Submit_Codigo(EdtCodigoKitBox_Proj.Text);
         KitBox.Submit_Codigo(EdtKitBox_Proj.Text);
         Projeto.Submit_Codigo( lbCodigoProjeto.caption);
         Submit_Quantidade(edtQuantidadeKitBox_Proj.Text);

         if (Salvar(True)=False)
         Then Begin
                  edtKitBox_ProjReferencia.SetFocus;
                  exit;
         End;
         lbNomeKitBox_Proj.caption:='';
         limpaedit(PanelKitBox_Proj);
         edtKitBox_ProjReferencia.SetFocus;
         Self.ResgataKitBox_Proj;
    end;

end;

procedure TFPROJETO.btExcluirKitBox_ProjClick(Sender: TObject);
begin
     if (edtCodigoKitBox_proj.Text='')then
     exit;

     If (DBGridKitBox_Proj.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridKitBox_Proj.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir esse KitBox Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     ObjKitBox_PROJ.Exclui(DBGridKitBox_Proj.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     limpaedit(PanelKitBox_Proj);
     lbNomeKitBox_Proj.Caption:='';
     Self.ResgataKitBox_Proj;

end;

procedure TFPROJETO.btCancelarKitBox_ProjClick(Sender: TObject);
begin
   limpaedit(PanelKitBox_Proj);
   lbNomeKitBox_Proj.Caption:='';
   edtKitBox_ProjReferencia.SetFocus;

end;

procedure TFPROJETO.dbgridKitBox_ProjDblClick(Sender: TObject);
begin
     If (DBGridKitBox_Proj.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridKitBox_Proj.DataSource.DataSet.RecordCount=0)
     Then exit;

     limpaedit(PanelKitBox_Proj);
     lbNomeKitBox_Proj.caption:='';
     if (ObjKitBox_PROJ.LocalizaCodigo(DBGridKitBox_Proj.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataFerragens_Proj;
               exit;
     End;

     With ObjKitBox_PROJ do
     Begin
          TabelaparaObjeto;
          edtCodigoKitBox_Proj.Text:=Get_Codigo;
          edtKitBox_ProjReferencia.Text:=KitBox.Get_Referencia;
          edtKitBox_Proj.Text:=KitBox.Get_Codigo;
          lbNomeKitBox_Proj.Caption:=KitBox.Get_Descricao;
          edtQuantidadeKitBox_Proj.Text:=Get_Quantidade;
          edtKitBox_ProjReferencia.SetFocus;
     End;

end;

procedure TFPROJETO.edtKitBox_ProjReferenciaExit(Sender: TObject);
begin
  ObjKITBOX_PROJ.EdtKitBoxExit(Sender, edtKitBox_Proj, lbNomeKitBox_Proj);
end;

procedure TFPROJETO.edtKitBox_ProjReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   ObjKITBOX_PROJ.EdtKitBoxKeyDown(Sender, edtKitBox_Proj, Key, Shift, lbNomeKitBox_Proj);
end;

procedure TFPROJETO.PreparaServico_Proj;
begin
     Self.ResgataServico_Proj;
     limpaedit(PanelServico_Proj);
     habilita_campos(PanelServico_Proj);
     rbControlaSim.Enabled := True; {rodolfo}
     rbControlaNao.Enabled := True; {Rodolfo}
     rbsimm2.Enabled := True;
     rbnaom2.Enabled := True;
end;

procedure TFPROJETO.ResgataServico_Proj;
begin
ObjServico_PROJ.Resgata_Servico_Proj( lbCodigoProjeto.caption);
end;

procedure TFPROJETO.brGravarServico_ProjClick(Sender: TObject);
begin
    With ObjServico_PROJ do
    Begin
         ZerarTabela;
         if (EdtCodigoServico_Proj.Text='')
         or (edtCodigoServico_Proj.Text='0')
         then Begin
              Status:=dsInsert;
              edtCodigoServico_Proj.Text:='0';
              if (VerificaSeJaExisteEstaServicoNesteProjeto( lbCodigoProjeto.caption, edtServico_Proj.Text)=true)then
              Begin
                   MensagemErro('Este Servi�o j� foi cadastrada para este projeto');
                   exit;
              end;
         end
         else Status:=dsEdit;

         Submit_Codigo(EdtCodigoServico_Proj.Text);
         Servico.Submit_Codigo(EdtServico_Proj.Text);
         Submit_Quantidade(EdtQuantidadeServicoProj.Text);
         Projeto.Submit_Codigo(lbCodigoProjeto.caption);

         if( rbControlaSim.Checked = True ) then
         begin    {Rodolfo}
             Submit_ALTURA(edtAltServ.text);
             Submit_LARGURA(edtLargServ.text);
             Submit_ControlaPorMilimetro('SIM');
             Submit_Quantidade('0');
         end
         else
         begin  //se nao controla por milimetro
             Submit_ControlaPorMilimetro('NAO');
         end;

         if( rbsimm2.Checked = True ) then
         begin
             Submit_ALTURA(edtAltServ.text);
             Submit_LARGURA(edtLargServ.text);
             submit_controlaM2('S');
             Submit_Quantidade('0');
         end
         else
         begin 
             submit_controlaM2('N');
         end;

         if (Salvar(True)=False) Then
         Begin
                  edtServico_ProjReferencia.SetFocus;
                  exit;
         End;
         
         lbNomeServico_Proj.caption:='';
         limpaedit(PanelServico_Proj);
         edtServico_ProjReferencia.SetFocus;
         Self.ResgataServico_Proj;
    end;

end;

procedure TFPROJETO.brExcluirServico_ProjClick(Sender: TObject);
begin
     if (EdtCodigoServico_Proj.Text='')then
     exit;

     If (DBGridServico_Proj.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridServico_Proj.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir esse Servico Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;

     ObjServico_PROJ.Exclui(DBGridServico_Proj.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     limpaedit(PanelServico_Proj);
     lbNomeServico_proj.Caption:='';
     Self.ResgataServico_Proj;
     rbControlaNao.Checked:=True; {Rodolfo};

end;

procedure TFPROJETO.brCancelarServico_ProjClick(Sender: TObject);
begin
   limpaedit(PanelServico_Proj);
   lbNomeServico_Proj.Caption:='';
   edtServico_ProjReferencia.SetFocus;
   {gboxControla.Enabled:=True;
   rbControlaSim.Enabled:=True;
   rbControlaNao.Enabled:=True; }
   rbControlaNao.Checked:=True; {Rodolfo}
end;

procedure TFPROJETO.NotebookDblClick(Sender: TObject);
begin
     If (DBGridServico_Proj.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridServico_Proj.DataSource.DataSet.RecordCount=0)
     Then exit;

     limpaedit(PanelServico_Proj);
     lbNomeServico_Proj.caption:='';
     if (ObjServico_PROJ.LocalizaCodigo(DBGridServico_Proj.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataFerragens_Proj;
               exit;
     End;

     With ObjServico_PROJ do
     Begin
          TabelaparaObjeto;
          edtCodigoServico_Proj.Text:=Get_Codigo;
          edtServico_Proj.Text:=Servico.Get_Codigo;
          
          lbNomeServico_Proj.Caption:=Servico.Get_Descricao;
          edtServico_ProjReferencia.SetFocus;
     End;

end;

{Rodolfo}
procedure TFPROJETO.edtServico_ProjReferenciaExit(Sender: TObject);
begin
  ObjSERVICO_PROJ.EdtServicoExit(Sender, edtServico_Proj, lbNomeServico_proj);

  //Verifica se o campo de identificacao do edit foi alterado ou se foi apenas clicado  - Rodolfo
  if(auxStr <> lbNomeServico_proj.Caption)then
      rbControlaNao.Checked := True;

end;

procedure TFPROJETO.edtServico_ProjReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ObjSERVICO_PROJ.EdtServicoKeyDown(Sender, edtServico_Proj, key, Shift, lbNomeServico_proj);
end;


procedure TFPROJETO.dbgridServico_ProjDblClick(Sender: TObject);
begin
     If (DBGridServico_Proj.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridServico_Proj.DataSource.DataSet.RecordCount=0)
     Then exit;
     {gboxControla.Enabled:=False;
     rbControlaSim.Enabled:=False;
     rbControlaNao.Enabled:=False; }
     limpaedit(PanelServico_Proj);
     lbNomeServico_Proj.caption:='';
     if (ObjServico_PROJ.LocalizaCodigo(DBGridServico_Proj.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataFerragens_Proj;
               exit;
     End;

     With ObjServico_PROJ do
     Begin
          TabelaparaObjeto;
          edtCodigoServico_Proj.Text:=Get_Codigo;
          edtServico_ProjReferencia.Text:=Servico.Get_Referencia;
          edtServico_Proj.Text:=Servico.Get_Codigo;
          EdtQuantidadeServicoProj.Text:=Get_Quantidade;
          lbNomeServico_Proj.Caption:=Servico.Get_Descricao;
          edtServico_ProjReferencia.SetFocus;
     End;

     {Rodolfo ==}
     if(ObjServico_PROJ.Get_ControlaPorMilimetro = 'SIM') then begin
          rbControlaSim.Checked:= True;
          edtAltServ.text := ObjServico_PROJ.Get_Altura;
          edtLargServ.text := ObjServico_PROJ.Get_Largura;
          //rbControlaSimClick(Sender);
     end
     else begin
          edtAltServ.text := '';
          edtLargServ.text := '';
          rbControlaNao.Checked := True;
          //rbControlaNaoClick(sender);
     end;

     if(ObjServico_PROJ.Get_ControlaM2 = 'S') then begin
          rbsimm2.Checked:= True;
          edtAltServ.text := ObjServico_PROJ.Get_Altura;
          edtLargServ.text := ObjServico_PROJ.Get_Largura;
          //rbControlaSimClick(Sender);
     end
     else begin
          edtAltServ.text := '';
          edtLargServ.text := '';
          rbnaom2.Checked := True;
          //rbControlaNaoClick(sender);
     end;


     {== Rodolfo}

end;

procedure TFPROJETO.PreparaDiverso_Proj;
begin
     Self.ResgataDiverso_Proj;
     limpaedit(PanelDiverso_Proj);
     habilita_campos(PanelDiverso_Proj);

     //if(ComboEsquadria.Text='S')
     //then chkesquadriadiverso.Visible:=True
     //else chkesquadriadiverso.Visible:=False;

end;

procedure TFPROJETO.ResgataDiverso_Proj;
begin
ObjDiverso_PROJ.Resgata_Diverso_Proj(lbCodigoProjeto.caption);
end;

procedure TFPROJETO.edtDiverso_ProjReferenciaExit(Sender: TObject);
begin
  ObjDiverso_Proj.EdtDiversoExit(Sender,edtDiverso_proj, lbNomeDiverso_Proj);

  if (ObjDiverso_Proj.Diverso.Get_ContralaPorMetroQuadrado='S')then
  begin
       EdtLarguraDiverso_proj.Visible:=true;
       edtAlturaDiverso_Proj.Visible:=true;
       LBAlturaDiverso_Proj.Visible:=true;
       LBLarguraDiverso_Proj.Visible:=true;
       edtAlturaDiverso_Proj.SetFocus;
       EdtQuantidadeDiverso_proj.Enabled:=false;
  end else
  Begin
       EdtLarguraDiverso_proj.Visible:=false;
       edtAlturaDiverso_Proj.Visible:=false;
       LBAlturaDiverso_Proj.Visible:=false;
       LBLarguraDiverso_Proj.Visible:=false;
       EdtQuantidadeDiverso_proj.Enabled:=true;
  end;
  
end;

procedure TFPROJETO.edtDiverso_ProjReferenciaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjDiverso_Proj.EdtDiversoKeyDown(Sender, EdtDiverso_Proj, key, Shift,lbNomeDiverso_Proj);
end;

procedure TFPROJETO.BitBtn1Click(Sender: TObject);
begin
    With ObjDiverso_PROJ do
    Begin
         ZerarTabela;
         if (edtCodigoDiverso_Proj.Text='')
         or (edtCodigoDiverso_Proj.Text='0')
         then Begin
              Status:=dsInsert;
              edtCodigoDiverso_Proj.Text:='0';
              if (VerificaSeJaExisteEstaDiversoNesteProjeto(lbCodigoProjeto.caption, edtDiverso_Proj.Text)=true)then
              Begin
                   MensagemErro('Este Diverso j� foi cadastrada para este projeto');
                   exit;
              end;
         end
         else Status:=dsEdit;

         Submit_Codigo(edtCodigoDiverso_Proj.Text);
         Diverso.Submit_Codigo(edtDiverso_Proj.Text);
         Projeto.Submit_Codigo(lbCodigoProjeto.caption);
         Submit_Quantidade(EdtQuantidadeDiverso_Proj.Text);
         Submit_Altura(edtAlturaDiverso_Proj.Text);
         Submit_Largura(edtLarguraDiverso_Proj.Text);
         if (Salvar(True)=False)
         Then Begin
                  edtDiverso_ProjReferencia.SetFocus;
                  exit;
         End;
         lbNomeDiverso_Proj.caption:='';
         limpaedit(PanelDiverso_Proj);
         edtDiverso_ProjReferencia.SetFocus;
         Self.ResgataDiverso_Proj;
    end;

end;

procedure TFPROJETO.BitBtn2Click(Sender: TObject);
begin
     if (edtCodigoDiverso_Proj.Text='')then
     exit;

     If (DBGridDiverso_Proj.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridDiverso_Proj.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir esse Diverso Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     ObjDiverso_PROJ.Exclui(DBGridDiverso_Proj.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     limpaedit(PanelDiverso_Proj);
     lbNomeDiverso_Proj.Caption:='';
     Self.ResgataFerragens_Proj;

end;

procedure TFPROJETO.BitBtn3Click(Sender: TObject);
begin
   limpaedit(PanelDiverso_Proj);
   lbNomeDiverso_Proj.Caption:='';
   EdtLarguraDiverso_proj.Visible:=false;
   edtAlturaDiverso_Proj.Visible:=false;
   LBAlturaDiverso_Proj.Visible:=false;
   LBLarguraDiverso_Proj.Visible:=false;
   edtDiverso_ProjReferencia.SetFocus;
end;

procedure TFPROJETO.dbgridDiverso_ProjDblClick(Sender: TObject);
begin
     If (DBGridDiverso_Proj.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridDiverso_Proj.DataSource.DataSet.RecordCount=0)
     Then exit;

     limpaedit(PanelDiverso_Proj);
     lbNomeDiverso_Proj.caption:='';
     if (ObjDiverso_PROJ.LocalizaCodigo(DBGridDiverso_Proj.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataFerragens_Proj;
               exit;
     End;

     With ObjDiverso_PROJ do
     Begin
          TabelaparaObjeto;
          edtCodigoDiverso_Proj.Text:=Get_Codigo;
          edtDiverso_ProjReferencia.Text:=Diverso.Get_Referencia;
          edtDiverso_Proj.Text:=Diverso.Get_Codigo;
          lbNomeDiverso_Proj.Caption:=Diverso.Get_Descricao;
          EdtQuantidadeDiverso_Proj.Text:=Get_Quantidade;
          EdtLarguraDiverso_proj.Text:=Get_Largura;
          edtAlturaDiverso_Proj.Text:=Get_Altura;
          edtDiverso_ProjReferencia.SetFocus;

          if (Diverso.Get_ContralaPorMetroQuadrado='S')then
          begin
               EdtLarguraDiverso_proj.Visible:=true;
               edtAlturaDiverso_Proj.Visible:=true;
               LBAlturaDiverso_Proj.Visible:=true;
               LBLarguraDiverso_Proj.Visible:=true;
               EdtQuantidadeDiverso_proj.Enabled:=false;
          end else
          Begin
               EdtLarguraDiverso_proj.Visible:=false;
               edtAlturaDiverso_Proj.Visible:=false;
               LBAlturaDiverso_Proj.Visible:=false;
               LBLarguraDiverso_Proj.Visible:=false;
               EdtQuantidadeDiverso_proj.Enabled:=true;
          end;





     End;
end;

procedure TFPROJETO.edtAlturaDiverso_ProjExit(Sender: TObject);
begin
     try
        if (edtAlturaDiverso_Proj.Text<>'')and (EdtLarguraDiverso_proj.Text<>'')then
        EdtQuantidadeDiverso_proj.Text:=CurrToStr(StrToCurr(edtAlturaDiverso_Proj.Text)* StrToCurr(edtLarguraDiverso_Proj.Text));
     except
        MensagemErro('Erro ao tentar calcular os Metros Quadrados.');
     end;
end;

procedure TFPROJETO.edtLarguraDiverso_projExit(Sender: TObject);
begin
     try
        if (edtAlturaDiverso_Proj.Text<>'')and (EdtLarguraDiverso_proj.Text<>'')then
        EdtQuantidadeDiverso_proj.Text:=CurrToStr(StrToCurr(edtAlturaDiverso_Proj.Text)* StrToCurr(edtLarguraDiverso_Proj.Text));
     except
        MensagemErro('Erro ao tentar calcular os Metros Quadrados.');
     end;

end;

procedure TFPROJETO.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjPROJETO.PrimeiroRegistro = false)then
    exit;

    ObjPROJETO.TabelaparaObjeto;
    Self.ObjetoParaControles;

    try
       ImageDesenho.Visible:=true;
       if (ObjPROJETO.ResgataDesenho(EdtReferencia.Text)<>'')
       then ImageDesenho.Picture.LoadFromFile(ObjPROJETO.ResgataDesenho(EdtReferencia.Text))
       else ImageDesenho.Visible:=false;

    except
        MensagemErro('Erro ao tentar transferir o desenho.');
    end;

end;

procedure TFPROJETO.btUltimoClick(Sender: TObject);
begin
    if  (ObjPROJETO.UltimoRegistro = false)then
    exit;

    ObjPROJETO.TabelaparaObjeto;
    Self.ObjetoParaControles;

    try
       ImageDesenho.Visible:=true;
       if (ObjPROJETO.ResgataDesenho(EdtReferencia.Text)<>'')
       then Begin
                if (FileExists(ObjPROJETO.ResgataDesenho(EdtReferencia.Text)))
                then ImageDesenho.Picture.LoadFromFile(ObjPROJETO.ResgataDesenho(EdtReferencia.Text));
       End
       else ImageDesenho.Visible:=false;

    except
        MensagemErro('Erro ao tentar transferir o desenho.');
    end;

end;    
procedure TFPROJETO.btProximoClick(Sender: TObject);
begin
    if (lbCodigoProjeto.caption='')then
    lbCodigoProjeto.caption:='0';

    if  (ObjPROJETO.ProximoRegisto(StrToInt(lbCodigoProjeto.caption)) = false)then
    exit;

    ObjPROJETO.TabelaparaObjeto;
    Self.ObjetoParaControles;

    try
       ImageDesenho.Visible:=true;
       if (ObjPROJETO.ResgataDesenho(EdtReferencia.Text)<>'')
       then Begin
                 if (Fileexists(ObjPROJETO.ResgataDesenho(EdtReferencia.Text)))
                 Then ImageDesenho.Picture.LoadFromFile(ObjPROJETO.ResgataDesenho(EdtReferencia.Text));
       End
       else
       ImageDesenho.Visible:=false;

    except
        MensagemErro('Erro ao tentar transferir o desenho.');
    end;
    

end;

procedure TFPROJETO.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigoProjeto.caption='')then
   lbCodigoProjeto.caption:='0';
    
    if  (ObjPROJETO.RegistoAnterior(StrToInt(lbCodigoProjeto.caption)) = false)then
    exit;

    ObjPROJETO.TabelaparaObjeto;
    Self.ObjetoParaControles;

    try
       ImageDesenho.Visible:=true;
       if (ObjPROJETO.ResgataDesenho(EdtReferencia.Text)<>'')then
       ImageDesenho.Picture.LoadFromFile(ObjPROJETO.ResgataDesenho(EdtReferencia.Text))
       else
       ImageDesenho.Visible:=false;

    except
        MensagemErro('Erro ao tentar transferir o desenho.');
    end;
    

end;



procedure TFPROJETO.edtFormulaAlturaPerfilado_ProjExit(Sender: TObject);
begin
    Tedit(Sender).Text:=virgulaparaponto(Tedit(Sender).Text);
end;

procedure TFPROJETO.edtFormulaLArguraPerfilado_ProjExit(Sender: TObject);
begin
Tedit(Sender).Text:=virgulaparaponto(Tedit(Sender).Text);
end;

procedure TFPROJETO.edtFormulaAlturaComponente_ProjExit(Sender: TObject);
begin
Tedit(Sender).Text:=virgulaparaponto(Tedit(Sender).Text);
end;

procedure TFPROJETO.edtFormulaLarguraComponente_ProjExit(Sender: TObject);
begin
Tedit(Sender).Text:=virgulaparaponto(Tedit(Sender).Text);
end;

procedure TFPROJETO.BtOpcoesClick(Sender: TObject);
begin
     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Replicar Projeto Atual');
          RgOpcoes.Items.add('Outras Op��es');

          showmodal;
          if (tag=0)
          Then exit;
          Case RgOpcoes.ItemIndex of
            0:Self.Replicar;
            1:self.ObjPROJETO.Opcoes(lbCodigoProjeto.caption);
          End;
     End;
     
End;

procedure TFPROJETO.COMBOCorteComponente_ProjKeyPress(Sender: TObject;
  var Key: Char);
begin
      Abort;
end;


procedure TFPROJETO.Replicar;
var
PnovoCodigo,PCodigoantigo:string;
begin
     if (lbCodigoProjeto.caption='')
     Then Begin
               MensagemAviso('Escolha primeiramente o projeto que deseja replicar');
               exit;
     End;

     PCodigoantigo:= Self.ObjPROJETO.get_codigo;
     if (Self.ObjPROJETO.LocalizaCodigo(lbCodigoProjeto.caption)=False)
     then begin
               MensagemAviso('O projeto escolhido n�o foi encontrado no Banco de Dados. Se estiver criando um novo projeto salve primeiramente antes de tentar replic�-lo!');
               exit;
     End;

     Self.ObjPROJETO.TabelaparaObjeto;

     Try
         Self.ObjPROJETO.Status:=dsInsert;
         Self.ObjPROJETO.Submit_Codigo('0');
         Self.ObjPROJETO.Submit_Referencia(Self.ObjPROJETO.Get_Referencia+'(2)');
         
         if (Self.ObjPROJETO.Salvar(False)=False)
         Then Begin
                   MensagemErro('Erro na tentativa de gravar um novo projeto');
                   exit; 
         End;

         if (Self.ObjPROJETO.LocalizaCodigo(Self.ObjPROJETO.get_codigo)=false)
         then Begin
                   MensagemErro('Projeto '+Self.ObjPROJETO.Get_Codigo+' n�o localizado');
                   exit;
         End;
         
         Self.ObjPROJETO.TabelaparaObjeto;


         Pnovocodigo:=Self.ObjPROJETO.Get_Codigo;

         //replicando os itens agora
         if (Self.ObjFERRAGEM_PROJ.Replica(lbCodigoProjeto.caption,PnovoCodigo)=false)
         then Begin
                   MensagemErro('Erro na tentativa de replicar as ferragens');
                   exit;
         End;


         if (Self.ObjPERFILADO_PROJ.Replica(lbCodigoProjeto.caption,PnovoCodigo)=false)
         then Begin
                   MensagemErro('Erro na tentativa de replicar os Perfilados');
                   exit;
         End;


         if (Self.ObjComponente_PROJ.Replica(lbCodigoProjeto.caption,PnovoCodigo)=false)
         then Begin
                   MensagemErro('Erro na tentativa de replicar os Componentes');
                   exit;
         End;

         if (Self.ObjKitBox_PROJ.Replica(lbCodigoProjeto.caption,PnovoCodigo)=false)
         then Begin
                   MensagemErro('Erro na tentativa de replicar os Kit-Box');
                   exit;
         End;


         if (Self.Objservico_PROJ.Replica(lbCodigoProjeto.caption,PnovoCodigo)=false)
         then Begin
                   MensagemErro('Erro na tentativa de replicar os Servi�os');
                   exit;
         End;

         if (Self.ObjDiverso_Proj.Replica(lbCodigoProjeto.caption,PnovoCodigo)=false)
         then Begin
                   MensagemErro('Erro na tentativa de replicar os Diversos');
                   exit;
         End;
         if (Self.ObjPROJETO.replicadesenho(PnovoCodigo,PCodigoantigo)=false)
         then Begin
                   MensagemErro('Erro na tentativa de replicar os Diversos');
                   exit;
         End;

         FDataModulo.IBTransaction.CommitRetaining;

         MensagemAviso('Projeto Replicado com Sucesso. C�digo: '+PnovoCodigo);



         Self.ObjPROJETO.LocalizaCodigo(PnovoCodigo);
         Self.ObjPROJETO.TabelaparaObjeto;
         Self.TabelaParaControles;



         Self.Guia.TabIndex:=0;




     Finally
            FDataModulo.IBTransaction.RollbackRetaining;
     End;

end;



procedure TFPROJETO.dbgridComponente_ProjDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridComponente_Proj.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridComponente_Proj.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridComponente_Proj.DefaultDrawDataCell(Rect, DBGridComponente_Proj.Columns[DataCol].Field, State);
          End;
end;

procedure TFPROJETO.dbgridDiverso_ProjDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridDiverso_Proj.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridDiverso_Proj.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridDiverso_Proj.DefaultDrawDataCell(Rect,DBGridDiverso_Proj.Columns[DataCol].Field, State);
          End;
end;

procedure TFPROJETO.dbgridFerragem_ProjDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridFerragem_Proj.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridFerragem_Proj.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridFerragem_Proj.DefaultDrawDataCell(Rect,DBGridFerragem_Proj.Columns[DataCol].Field, State);
          End;
end;

procedure TFPROJETO.dbgridKitBox_ProjDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridKitBox_Proj.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridKitBox_Proj.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridKitBox_Proj.DefaultDrawDataCell(Rect,DBGridKitBox_Proj.Columns[DataCol].Field, State);
          End;
end;

procedure TFPROJETO.dbgridPerfilado_ProjDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridPerfilado_Proj.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridPerfilado_Proj.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridPerfilado_Proj.DefaultDrawDataCell(Rect,DBGridPerfilado_Proj.Columns[DataCol].Field, State);
          End;
end;

procedure TFPROJETO.dbgridServico_ProjDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridServico_Proj.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridServico_Proj.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridServico_Proj.DefaultDrawDataCell(Rect,DBGridServico_Proj.Columns[DataCol].Field, State);
          End;
end;

procedure TFPROJETO.lbGravarMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFPROJETO.lbGravarMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
      ScreenCursorProc(crHandPoint);

end;

procedure TFPROJETO.PanelComponente_ProjMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      ScreenCursorProc(crDefault);
end;

procedure TFPROJETO.edtComponente_ProjReferenciaKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFPROJETO.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     ColocaUpperCaseEdit(Self);
     ImageDesenho.Visible:=false;
     Self.limpaLabels;
     desabilita_campos(Self);
     RadioAlturaPorLargura.Enabled:=True;
     RadioAlturaPorLargura_AlturaPorLargura.Enabled:=True;
     RadioVarios.Enabled:=True;
     Guia.TabIndex:=0;

    // edtComponenteFerragem.Visible:=False;
    // chk1.Checked:=False;

     Try
        Self.ObjPROJETO:=TObjPROJETO.Create;
        Self.ObjFERRAGEM_PROJ:=TObjFERRAGEM_PROJ.Create;
        Self.ObjPERFILADO_PROJ:=TObjPERFILADO_PROJ.Create;
        Self.ObjCOMPONENTE_PROJ:=TObjCOMPONENTE_PROJ.Create;
        Self.ObjSERVICO_PROJ:=TObjSERVICO_PROJ.Create;
        Self.ObjKITBOX_PROJ:=TObjKITBOX_PROJ.Create;
        Self.ObjDiverso_Proj:=TOBjDiverso_Proj.Create;

        FRImposto_ICMS1.ObjMaterial_ICMS:=TobjPROJETO_ICMS.Create(self);

        DBGridFerragem_Proj.DataSource:=ObjFERRAGEM_PROJ.ObjDatasource;
        DBGridPerfilado_Proj.DataSource:=ObjPERFILADO_PROJ.ObjDatasource;
        DBGridComponente_Proj.DataSource:=ObjCOMPONENTE_PROJ.ObjDatasource;
        DBGridKitBox_Proj.DataSource:=ObjKITBOX_PROJ.ObjDatasource;
        DBGridServico_Proj.DataSource:=ObjSERVICO_PROJ.ObjDatasource;
        DBGridDiverso_Proj.DataSource:=ObjDiverso_Proj.ObjDatasource;

     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     ImageDesenho.Stretch:=False;
     FescolheImagemBotao.PegaFiguraImagem(ImageDesenho,'FOTO');
     ImageDesenho.Visible:=True;
     lbCodigoProjeto.caption:='';
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE PROJETO')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);

     if(ObjParametroGlobal.ValidaParametro('VENDER PROJETOS COMO PRODUTOS')=false)
     then MensagemErro('Paramentro "VENDER PROJETOS COMO PRODUTOS" n�o encontrado');

     if(ObjParametroGlobal.Get_Valor='NAO') or (ObjParametroGlobal.Get_Valor='N')
     then Guia.Pages[9].TabVisible:=False;

     if(Tag<>0)
     then begin

          if(ObjPROJETO.Localizacodigo(IntToStr(tag))=True)
          then begin
                ObjPROJETO.TabelaparaObjeto;
                self.ObjetoParaControles;

          end;

     end;

      MostraQuatidadeProjeto;
      op := 0;
      cont := 0;
      cor := clBtnFace;
      idCarr := False; // indica que nenhuma figura foi carregada na tela
      hab := True; // Nesta inicializa��o decidi-se se a figura ira come�ar marcada ou nao
      Self.Tag := -1;
      ctrlTag := -1;
      polyclick := false;
      indCurr := 0;
      Entrad := true;
      listaFiguras := TList.Create;
      listaFigurasParaProducao:= TList.Create;
      ListaComponentes:=TStringList.Create;
      ListaItens:=TStringList.Create;
      ListaServicosProducao:=TStringList.Create;
      Importar();


end;

procedure TFPROJETO.MostraQuatidadeProjeto;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;

    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabprojeto');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb14.Caption:='Existem '+IntToStr(contador)+' projetos cadastrados'
       else
       begin
            lb14.Caption:='Existe '+IntToStr(contador)+' projeto cadastrado';
       end;

    finally

    end;


end;


procedure TFPROJETO.edtGrupoVidroKeyPress(Sender: TObject; var Key: Char);
begin
     if not (Key in['0'..'9',Chr(8)]) then
     begin
            Key:= #0;
     end;
end;

procedure TFPROJETO.edtAlturaMaximaKeyPress(Sender: TObject;
  var Key: Char);
begin
        if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFPROJETO.edtAlturaMinimaKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFPROJETO.edtLarguraMaximaKeyPress(Sender: TObject;
  var Key: Char);
begin
        if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFPROJETO.edtLarguraMinimaKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFPROJETO.edtAreaMinimaKeyPress(Sender: TObject; var Key: Char);
begin
        if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFPROJETO.edtMaoDeObraKeyPress(Sender: TObject; var Key: Char);
begin
         if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFPROJETO.AbreComCodigo(parametro:string);
begin

    if(ObjPROJETO.LocalizaReferencia(parametro)=True) then
    begin
               ObjPROJETO.TabelaparaObjeto;
               self.ObjetoParaControles;
    end

end;

procedure TFPROJETO.lbNomeComponente_ProjClick(Sender: TObject);
var
  Fcomponente:TFCOMPONENTE;
  Query:TIBQuery;
  parametro : string; {rodolfo}
begin
  if(edtComponente_ProjReferencia.Text='')
  then Exit;

  parametro:= StrReplaceRef(edtComponente_ProjReferencia.Text);  {rodolfo}

  try
    Fcomponente:=TFCOMPONENTE.Create(nil);
    Query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;
  except

  end;

  try
     with Query do
     begin
         Close;
         SQL.Clear;
         //SQL.Add('select codigo from tabcomponente where referencia=' +#39+edtComponente_ProjReferencia.Text+#39);
         SQL.Add('select codigo from tabcomponente where referencia=' +#39+parametro+#39);   {rodolfo}
         Open;

         Fcomponente.Tag:=StrToInt(fieldbyname('codigo').AsString);
         Fcomponente.ShowModal;
      end;
  finally
      FreeAndNil(Fcomponente);
      FreeAndNil(Query);
  end;



end;

procedure TFPROJETO.lbNomeDiverso_ProjClick(Sender: TObject);
var
  Fdiverso:TFDIVERSO;
  query:TIBQuery;
  parametro : string;  {rodolfo}
begin
    if(edtDiverso_ProjReferencia.Text='')
    then Exit;

    parametro := StrReplaceRef(edtDiverso_ProjReferencia.Text); {rodolfo}

    try
      Fdiverso:=TFDIVERSO.Create(nil);
      query:=TIBQuery.Create(nil);
      query.Database:=FDataModulo.IBDatabase;

    except

    end;

    try
      with query do
      begin
          Close;
          sql.Clear;
          //sql.Add('select codigo from tabdiverso where referencia='+#39+edtDiverso_ProjReferencia.Text+#39);
          sql.Add('select codigo from tabdiverso where referencia='+#39+parametro+#39);  {rodolfo}
          Open;
          Fdiverso.Tag:=StrToInt(fieldbyname('codigo').AsString);
          Fdiverso.ShowModal;
      end;
    finally
      FreeAndNil(Fdiverso);
      FreeAndNil(query);
    end;



end;

procedure TFPROJETO.lbNomeFerragem_ProjClick(Sender: TObject);
var
  FFERRAGEM:TFFERRAGEM;
  query:TIBQuery;
  parametro : string; {Rodolfo}
begin
    if(edtFerragem_ProjReferencia.Text='')
    then Exit;

    parametro:= StrReplaceRef(edtFerragem_ProjReferencia.Text); {Rodolfo}

    try
      FFERRAGEM:=TFFERRAGEM.Create(nil);
      query:=TIBQuery.Create(nil);
      query.Database:=FDataModulo.IBDatabase;

    except

    end;

    try
      with query do
      begin
          Close;
          sql.Clear;
          //sql.Add('select codigo from tabferragem where referencia='+#39+edtFerragem_ProjReferencia.Text+#39);
          sql.Add('select codigo from tabferragem where referencia='+#39+parametro+#39); {Rodolfo}
          Open;
          FFERRAGEM.Tag:=StrToInt(fieldbyname('codigo').AsString);
          FFERRAGEM.ShowModal;
      end;
    finally
       FreeAndNil(FFERRAGEM);
       FreeAndNil(query);
    end;


end;



procedure TFPROJETO.lbNomeKitBox_ProjClick(Sender: TObject);
var
    FkitBox: TFKITBOX;
    query:TIBQuery;
    parametro: string; {rodolfo}
begin
    if(edtKitBox_ProjReferencia.Text ='')
    then Exit;

    parametro := StrReplaceRef(edtKitBox_ProjReferencia.Text);   {rodolfo}

    try
      FkitBox:=TFKITBOX.Create(nil);
      query:=TIBQuery.Create(nil);
      query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with query do
      begin
          Close;
          sql.Clear;
          //sql.Add('select codigo from tabkitbox where referencia ='+#39+edtKitBox_ProjReferencia.Text+#39);
          sql.Add('select codigo from tabkitbox where referencia ='+#39+parametro+#39);  {rodolfo}
          Open;
          FkitBox.Tag:=StrToInt(fieldbyname('codigo').AsString);
          FkitBox.ShowModal;
      end;
    finally
      FreeAndNil(FkitBox);
      FreeAndNil(query);
    end;

end;

procedure TFPROJETO.lbNomePerfilado_ProjClick(Sender: TObject);
var
  Fperfilado:TFPERFILADO;
  query:TIBQuery;
  parametro : string; {Rodolfo}
begin
      if(EdtPerfilado_ProjReferencia.Text='')
      then Exit;

      parametro := StrReplaceRef(edtPerfilado_ProjReferencia.text); {Rodolfo}

      try
        Fperfilado:=TFPERFILADO.Create(nil);
        query:=TIBQuery.Create(nil);
        query.Database:=FDataModulo.IBDatabase;
      except

      end;

      try
         with query do
         begin
            Close;
            sql.Clear;
            //sql.Add('select codigo from tabperfilado where referencia = '#39+EdtPerfilado_ProjReferencia.Text+#39);
            sql.Add('select codigo from tabperfilado where referencia = '#39+parametro+#39);   {rodolfo}
            Open;
            Fperfilado.Tag:=StrToInt(fieldbyname('codigo').AsString);
            Fperfilado.ShowModal;
         end;
      finally
         FreeAndNil(Fperfilado);
         FreeAndNil(query);
      end;


end;

procedure TFPROJETO.lbNomeServico_projClick(Sender: TObject);
var
  Fservico:TFSERVICO;
  query:TIBQuery;
  parametro:string; {rodolfo}
begin
    if(edtServico_ProjReferencia.Text='')
    then Exit;

    parametro:= StrReplaceRef(edtServico_ProjReferencia.Text);     {rodolfo}

    try
      Fservico:=TFSERVICO.Create(nil);
      query:=TIBQuery.Create(nil);
      query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with query do
      begin
          Close;
          sql.Clear;
          //sql.Add('select codigo from tabservico where referencia='+#39+edtServico_ProjReferencia.Text+#39);
          sql.Add('select codigo from tabservico where referencia='+#39+parametro+#39);    {rodolfo}
          Open;
          Fservico.Tag:=StrToInt(fieldbyname('codigo').AsString);
          Fservico.ShowModal;
      end;
    finally
      FreeAndNil(Fservico);
      FreeAndNil(query);
    end;


end;

procedure TFPROJETO.edtGrupoVidroDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FgrupoVidro:TFGRUPOVIDRO;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FgrupoVidro:=TFGRUPOVIDRO.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('select * from tabgrupovidro','Pesquisa de Grupo de vidros',FgrupoVidro)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtGrupoVidro.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 LbNomeGrupoVidro.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FgrupoVidro);

     End;
end;

procedure TFPROJETO.edtComponente_ProjReferenciaDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Fcomponente:TFCOMPONENTE;
   key : Word; {rodolfo}
   shift : TShiftState; {rodolfo}
begin
    key := VK_F9;   {rodolfo}
    edtComponente_ProjReferenciaKeyDown(Sender, key, shift); {rodolfo}
     {Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fcomponente:=TFCOMPONENTE.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from TabCOMPONENTE','',Fcomponente)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 edtComponente_ProjReferencia.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbNomeComponente_Proj.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fcomponente);

     End;   }
end;


procedure TFPROJETO.edtDiverso_ProjReferenciaDblClick(Sender: TObject);
var
     Key:Word;
     Shift:TShiftState;
begin
      Key:=VK_F9;
      ObjDiverso_Proj.EdtDiversoKeyDown(Sender, EdtDiverso_Proj, key, Shift,lbNomeDiverso_Proj);
end;


procedure TFPROJETO.edtFerragem_ProjReferenciaDblClick(Sender: TObject);
var
    Key:Word;
    Shift:TShiftState;
begin
     Key:=VK_F9;
     ObjFERRAGEM_PROJ.EdtFerragemKeyDown(Sender, edtFerragem_Proj, Key, Shift, lbNomeFerragem_Proj);
     {Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fferragem:=TFFERRAGEM.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabferragem','',Fferragem)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 edtFerragem_ProjReferencia.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbNomeFerragem_Proj.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fferragem);

     End;}
end;

procedure TFPROJETO.edtKitBox_ProjReferenciaDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FKitBox:TFKITBOX;
   key : Word;        {rodolfo}
   shift: TShiftState;         {rodolfo}
begin
    Key := VK_F9;  {rodolfo}
    edtKitBox_ProjReferenciaKeyDown(Sender, key, shift);     {rodolfo}
     {Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FKitBox:=TFKITBOX.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabkitbox where ativo=''S'' ','',FKitBox)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 edtKitBox_ProjReferencia.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbNomeKitBox_Proj.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FKitBox);

     End; }
end;


procedure TFPROJETO.edtPerfilado_ProjReferenciaDblClick(Sender: TObject);
var
    Key:Word;
    Shift:TShiftState;
begin
     kEY:=VK_F9;
     ObjPERFILADO_PROJ.EdtPerfiladoKeyDown(Sender, EdtPerfilado_Proj, Key, Shift, lbNomePerfilado_Proj);

     {Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fperfilado:=TFPERFILADO.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabperfilado','',Fperfilado)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtPerfilado_ProjReferencia.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbNomePerfilado_Proj.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fperfilado);

     End; }
end;

procedure TFPROJETO.edtServico_ProjReferenciaDblClick(Sender: TObject);
var
   Key:Word;
   Shift:TShiftState;
begin
     Key:=VK_F9;
     ObjSERVICO_PROJ.EdtServicoKeyDown(Sender, edtServico_Proj, key, Shift, lbNomeServico_proj);
end;

procedure TFPROJETO.ImageDesenhoClick(Sender: TObject);
begin
            if ( lbCodigoProjeto.caption='')then
            Begin
                MensagemErro('Localize o projeto ao qual deseja gravar o desenho.');
                exit;
            end;

            if (OpenDialog.Execute)then
            Begin
                ImageDesenho.Visible:=true;
                ImageDesenho.Picture.LoadFromFile(OpenDialog.FileName);
                ImageDesenho.Stretch:=true;
            end;
            try
                ImageDesenho.Picture.SaveToFile(ObjPROJETO.ResgataLocalDesenhos+EdtReferencia.Text+'.jpg');
            except
                MensagemErro('Erro ao tentar copiar o desenho para o Servidor.');
                exit;
            end;

            if (ObjPROJETO.status<>dsInactive)then
            exit;

            With ObjPROJETO do
            Begin
                LocalizaCodigo( lbCodigoProjeto.caption);
                TabelaparaObjeto;
                Status:=dsEdit;
                if (Salvar(true)=false)then
                Begin
                    MensagemErro('Erro ao tentar Salvar o Desenho.');
                    exit;
                end;
            end;
            //Winexec('D:\SAB BLINDEX\Vidros-Exclaim\Debug\Win32\DesenhoExclaim.exe',SW_SHOWNOACTIVATE);

end;

procedure TFPROJETO.GuiaChange(Sender: TObject);
begin
     if (Guia.TabIndex=0)//Projeto - Principal
     Then Begin
               if ( lbCodigoProjeto.caption<>'') and (ObjPROJETO.Status=dsinactive)
               Then Begin
                         if (ObjPROJETO.LocalizaCodigo( lbCodigoProjeto.caption)=False)
                         Then exit;
                         ObjPROJETO.TabelaparaObjeto;
                         Self.ObjetoParaControles;
               End;
               //Notebook.PageIndex:=NewTab;

     End;

     //  Ferragem
     if (Guia.TabIndex=1)
     Then Begin
               if (ObjPROJETO.Status=dsinsert)
               or ( lbCodigoProjeto.caption='')
               Then Begin
                         exit;
                         Guia.TabIndex:=0;
               End;

               Self.PreparaFerragem_Proj;
              // Notebook.PageIndex:=NewTab;
               edtFerragem_ProjReferencia.SetFocus;
     End;


     // Perfilado
     if (Guia.TabIndex=2)
     Then Begin
               if (ObjPROJETO.Status=dsinsert)
               or ( lbCodigoProjeto.caption='')
               Then Begin
                         exit;
                         Guia.TabIndex:=0;
               End;

               Self.PreparaPerfilado_Proj;
               //Notebook.PageIndex:=NewTab;
               EdtPerfilado_ProjReferencia.SetFocus;

     End;


     // Componente
     if (Guia.TabIndex=3)
     Then Begin
               if (ObjPROJETO.Status=dsinsert)
               or ( lbCodigoProjeto.caption='')
               Then Begin
                         exit;
                         Guia.TabIndex:=0;
               End;

               Self.PreparaComponente_Proj;
              // Notebook.PageIndex:=NewTab;
               edtComponente_ProjReferencia.SetFocus;

     End;


     // KitBox
     if (Guia.TabIndex=4)
     Then Begin
               if (ObjPROJETO.Status=dsinsert)
               or ( lbCodigoProjeto.caption='')
               Then Begin
                         exit;
                         Guia.TabIndex:=0;
               End;

               Self.PreparaKitBox_Proj;
               //Notebook.PageIndex:=NewTab;
               PanelKitBox_Proj.Visible:=True;
               edtKitBox_ProjReferencia.SetFocus;

     End;


     // Servico
     if (Guia.TabIndex=5)
     Then Begin
               if (ObjPROJETO.Status=dsinsert)
               or ( lbCodigoProjeto.caption='')
               Then Begin
                         exit;
                         Guia.TabIndex:=0;
               End;

               Self.PreparaServico_Proj;
               //Notebook.PageIndex:=NewTab;
               edtServico_ProjReferencia.SetFocus;
               //rbControlaNao.Checked := True;

     End;

     // Diverso
     if (Guia.TabIndex=6)
     Then Begin
               if (ObjPROJETO.Status=dsinsert)
               or ( lbCodigoProjeto.caption='')
               Then Begin
                         exit;
                         Guia.TabIndex:=0;
               End;

               Self.PreparaDiverso_Proj;
               //Notebook.PageIndex:=NewTab;
               edtDiverso_ProjReferencia.SetFocus;

     End;
     //Produ��o
     if(Guia.TabIndex=8)
     then begin
             if (ObjPROJETO.Status=dsinsert)
             or ( lbCodigoProjeto.caption='')
             or ( lbCodigoProjeto.caption='0')
             Then Begin
                         exit;
                         Guia.TabIndex:=0;
             End;
             ListaServicosProducao.Clear;
             LimpaTudoIMGProducao;
             contProducao:=0;
             __LimpaStringGrid;
             MostraServicosProducao;
             CarregaDesenhoProjetoProducao(True,False);
             __CarregarMarcadoresServicos;
             STRGLinhaServicos.SetFocus;
             lbNomedoservicoprod.Caption:='';
             Self.Tag:=-1;

     end;
     //Impostos
     if(Guia.TabIndex=9)
     then begin
             if (ObjPROJETO.Status=dsinsert)
             or ( lbCodigoProjeto.caption='')
             or ( lbCodigoProjeto.caption='0')
             Then Begin
                         exit;
                         Guia.TabIndex:=0;
             End;
             FRImposto_ICMS1.Acessaframe(lbCodigoProjeto.Caption);

     end;
     //Aba de modelagen de projetos
     if(Guia.TabIndex=7)
     then begin
             if (ObjPROJETO.Status=dsinsert)
             or ( lbCodigoProjeto.caption='')
             or ( lbCodigoProjeto.caption='0')
             Then Begin
                         exit;
                         Guia.TabIndex:=0;
                         
             End;
             CarregaDesenhoProjeto(True,True);
     end;

end;

procedure TFPROJETO.FRImposto_ICMS1BtGravar_material_icmsClick(
  Sender: TObject);
begin
    FRImposto_ICMS1.BtGravar_material_icmsClick(Sender);
end;

procedure TFPROJETO.FRImposto_ICMS1btcancelar_material_ICMSClick(
  Sender: TObject);
begin
      FRImposto_ICMS1.btcancelar_material_ICMSClick(Sender);
end;

procedure TFPROJETO.FRImposto_ICMS1btexcluir_material_ICMSClick(
  Sender: TObject);
begin
     FRImposto_ICMS1.btexcluir_material_ICMSClick(Sender);
end;

procedure TFPROJETO.FRImposto_ICMS1Button1Click(Sender: TObject);
begin
     FRImposto_ICMS1.Button1Click(Sender);
end;

procedure TFPROJETO.PaintBoxTelaDblClick(Sender: TObject);
var
  Shift: TShiftState;
begin
        if(polyclick = true)and(Drawing=true)then
        begin

              Drawing := false;
              polyclick := false;
              PaintBoxTela.Canvas.Polygon(Slice(points, indCurr));
              //PaintBoxTela.Canvas.LineTo(Slice(points, indCurr));
              op := 3;
              CriarFigura('','','');
              indCurr := 0;
              entrad := true;
              repaint;

        end;

        if(polyclick = true)and(Drawing=true) and (op=20)then
        begin

              Drawing := false;
              polyclick := false;
              //PaintBoxTela.Canvas.Polygon(Slice(points, indCurr));
              //PaintBoxTela.Canvas.LineTo(Slice(points, indCurr));
              op := 3;
              CriarFigura('','','');
              indCurr := 0;
              entrad := true;
              repaint;

        end;

end;

//Faz a inversao do pontos inicial e final da figura desenhada
procedure TFPROJETO.Arruma ();
var
  tmp: Integer;
begin
  //Verifico se o ponto y final � menor que ponto y inicial
  if (PFim.Y < PInic.Y) then
  begin
        tmp := PFim.Y;
        PFim.Y := PInic.Y;
        PInic.Y := tmp;
  end;
  //Verifico se o ponto x final � menor que ponto x inicial
  if (PFim.X < PInic.X) then
  begin
        tmp := PFim.X;
        PFim.X := PInic.X;
        PInic.X := tmp;
  end;
end;

//Adiciona no listbox o indice da nova figura
procedure TFPROJETO.AdicionaLista(NomeFigura:string; TipoMaterial:string ; CodigoMaterial:string);
begin
    if(NomeFigura<>'')
    then ListBox1.Items.Add('Obj '+inttostr(objDesenha.ContFig)+' - '+NomeFigura+' - '+TipoMaterial+' - '+CodigoMaterial)
    else ListBox1.Items.Add('Obj '+inttostr(objDesenha.ContFig)+' -');
   { with STRGProdutos do
    begin
          RowCount:=RowCount+1;
          Cells[0,RowCount-1]:='Obj '+inttostr(objDesenha.ContFig)+' -';
          Cells[1,RowCount-1]:=NomeFigura;
          Cells[2,RowCount-1]:=TipoMaterial;
          Cells[3,RowCount-1]:=CodigoMaterial;
    end;    }


end;

//Adiciona no listbox o indice da nova figura
procedure TFPROJETO.AdicionaListaIMGProducao(NomeFigura:string; TipoMaterial:string ; CodigoMaterial:string);
begin
    if(NomeFigura<>'')
    then ListBox2.Items.Add('Obj '+inttostr(objDesenha.ContFig)+' - '+NomeFigura+' - '+TipoMaterial+' - '+CodigoMaterial)
    else ListBox2.Items.Add('Obj '+inttostr(objDesenha.ContFig)+' -');
   { with STRGProdutos do
    begin
          RowCount:=RowCount+1;
          Cells[0,RowCount-1]:='Obj '+inttostr(objDesenha.ContFig)+' -';
          Cells[1,RowCount-1]:=NomeFigura;
          Cells[2,RowCount-1]:=TipoMaterial;
          Cells[3,RowCount-1]:=CodigoMaterial;
    end;    }


end;

procedure TFPROJETO.RetiraLista( pos : integer);
var
  str : string;
  I   : Integer;
begin
    str := 'Obj '+ inttostr(pos)+' -';
    //Procura no lista a identifica��o dessa ferragem, se acordo com a tag do desenho
    for i:=0 to ListBox1.Count-1 do
    begin
          //se encontrar a cadeia de caracteres dentro de alguma
          //palavra, ent�o retorna a palavra
          if(str=retornaPalavrasAntesSimbolo(ListBox1.Items[i],'-')+' -')
          then str:=ListBox1.Items[i];
    end;
    ListBox1.Items.Delete(ListBox1.items.IndexOf(str));

end;

procedure TFPROJETO.RetiraListaProducao( pos : integer);
var
  str : string;
  I   : Integer;
begin
    str := 'Obj '+ inttostr(pos)+' -';
    //Procura no lista a identifica��o dessa ferragem, se acordo com a tag do desenho
    for i:=0 to ListBox2.Count-1 do
    begin
          //se encontrar a cadeia de caracteres dentro de alguma
          //palavra, ent�o retorna a palavra
          if(str=retornaPalavrasAntesSimbolo(ListBox2.Items[i],'-')+' -')
          then str:=ListBox2.Items[i];
    end;
    ListBox2.Items.Delete(ListBox2.items.IndexOf(str));

end;

//Define a largura e altura que o poligono deve ter
procedure TFPROJETO.DefineTamanhoPoligon();
var
    i: integer;
    maiorx : integer;
    maiory : integer;
begin
    menorx := points[0].x;
    menory := points[0].y;
    maiorx := points[0].x;
    maiory := points[0].y;
    for i := 1 to indCurr-1 do
    begin
      if(points[i].x < menorx)then
          menorx := points[i].x;
      if(points[i].y < menory)then
          menory := points[i].y;
      if(points[i].x > maiorx)then
          maiorx := points[i].x;
      if(points[i].y > maiory)then
          maiory := points[i].y;
    end;
    //a largura do poligono sera definida pela diferen�a entre o maior e o menor ponto x
    largura := maiorx-menorx+6;
    //a altura do poligono sera definida pela diferen�a entre o maior e o menor ponto y
    altura := maiory-menory+6;
end;

//Defini��o dos tamanhos das ferragens estaticas
procedure TFPROJETO.DefineTamanhoFerragem;
begin
  case op of
    4:
    begin
      largura := 8;
      altura := 5;
    end;

    5: begin
      largura := 10;
      altura := 7;
    end;

    6:
    begin
      largura := 12;
      altura := 9;
    end;

    7:
    begin
      largura := 12;
      altura := 9;
    end;

    8: begin
      largura := 8;
      altura := 11;
    end;

    9:
    begin
      largura := 8;
      altura := 11;
    end;

    10:
    begin
      largura := 8;
      altura := 11;
    end;

    11:
    begin
      largura := 8;
      altura := 11;
    end;

    12:
    begin
      largura := 8;
      altura := 9;
    end;

    13:
    begin
      largura := 8;
      altura := 5;
    end;

    14:
    begin
      largura := 4;
      altura := 10;
    end;

    15:
    begin
      largura := 10;
      altura := 10;
    end;

  end;

end;

//Procedimento que passa os dados de cria��o para unit de cria��o de figuras
procedure TFPROJETO.CriarFigura(NomeFigura:string ; TipoMaterial:string; CodigoMaterial:string);
begin
  if(op <> 3) then
  begin
        Arruma();
        //To mandando vazio o nome do componente, pq na hora de salvar novamente zua tudo
        objDesenha := TDesenho.Create(PaintBoxTela,cor, op, listaFiguras.Count,pinic,pfim, points, indCurr-1,NomeFigura);

  end
  else
  objDesenha := TDesenho.Create(PaintBoxTela,cor, op, listaFiguras.Count,pinic,pfim, points, indCurr-1,'');

  objDesenha.PassaForm(Self);
  objDesenha.Parent := pnl3;

  //SetBounds (X da tela, Y da tela, Largura do obj, Altura do Obj)
  //objDesenha.SetBounds (arraypoints[0].x, arraypoints[0].y, pfim.x, pfim.y);
  if (op < 3) then
    objDesenha.SetBounds (pInic.X, pInic.Y, pfim.x-pinic.x, pfim.y-pinic.y)
  else
  if(op = 3) then
  begin
       DefineTamanhoPoligon();
       if(pux_banc = false)then
       objDesenha.SetBounds (menorX, menorY, largura, altura)
       else   objDesenha.SetBounds (pinic.X, pinic.Y, largura, altura) ;
  end
  else
  begin
      if (op=4) or (op>=22) then
          objDesenha.SetBounds (pInic.X, pInic.Y, pfim.x-pinic.x, pfim.y-pinic.y)
      else
      if(op=21)
      then objDesenha.SetBounds (pInic.X, pInic.Y, pfim.x-pinic.x, pfim.y-pinic.y)
      else
      begin
          begin
             DefineTamanhoFerragem();
             objDesenha.SetBounds (pInic.X, pInic.Y, largura*2, altura*2);
          end;
      end;
  end;

  listaFiguras.Add(objDesenha);

  Habilita(objDesenha.ContFig);

  cont := cont + 1;
  AdicionaLista(NomeFigura,TipoMaterial,CodigoMaterial);

end;

//Procedimento que passa os dados de cria��o para unit de cria��o de figuras
procedure TFPROJETO.CriarFiguraIMGProducao(NomeFigura:string ; TipoMaterial:string; CodigoMaterial:string);
begin
  if(op <> 3) then
  begin
        Arruma();
        //To mandando vazio o nome do componente, pq na hora de salvar novamente zua tudo
        objDesenha := TDesenho.Create(PaintBox1,cor, op, listaFigurasParaProducao.Count,pinic,pfim, points, indCurr-1,NomeFigura);

  end
  else
  objDesenha := TDesenho.Create(PaintBox1,cor, op, listaFigurasParaProducao.Count,pinic,pfim, points, indCurr-1,'');

  objDesenha.PassaForm(Self);
  objDesenha.Parent := PanelFundoProducao;

  if (op < 3) then
    objDesenha.SetBounds (pInic.X, pInic.Y, pfim.x-pinic.x, pfim.y-pinic.y)
  else
  if(op = 3) then
  begin
       DefineTamanhoPoligon();
       if(pux_banc = false)then
       objDesenha.SetBounds (menorX, menorY, largura, altura)
       else   objDesenha.SetBounds (pinic.X, pinic.Y, largura, altura) ;
  end
  else
  begin
      if (op=4) or (op>=22) then
          objDesenha.SetBounds (pInic.X, pInic.Y, pfim.x-pinic.x, pfim.y-pinic.y)
      else
      if(op=21)
      then objDesenha.SetBounds (pInic.X, pInic.Y, pfim.x-pinic.x, pfim.y-pinic.y)
      else
      begin
          begin
             DefineTamanhoFerragem();
             objDesenha.SetBounds (pInic.X, pInic.Y, largura*2, altura*2);
          end;
      end;
  end;

  listaFigurasParaProducao.Add(objDesenha);

  //Habilita(objDesenha.ContFig);

  contProducao := contProducao + 1;
  AdicionaListaIMGProducao(NomeFigura,TipoMaterial,CodigoMaterial);

end;

//Procedimento que habilita a figura para movimenta��o e altera��o de tamanho
procedure TFPROJETO.Habilita(i : Integer);
begin
  if(hab = True)then
  begin
    TDesenho(listaFiguras[i]).Botao := True;  //indica que a figura esta habilitada para edi��o
    //TDesenho(listaFiguras[i]).BringToFront;
  end
  else
  begin
    TDesenho(listaFiguras[i]).Botao := False;  //indica que a figura esta desabilitada para edi��o
    //TDesenho(listaFiguras[i]).SendToBack;
  end;
end;

procedure TFPROJETO.PaintBoxTelaMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
          pInic := Point(X,Y);
          botao := true;

//          ListBox1.ItemIndex:=RetornaLinhaReferentetTagOBj;

          if(polyclick = true)then
          begin
              pCurr := Point(X,Y);

              if(Entrad = true)then
              begin
                points[indCurr]:= pInic;
                indCurr := indCurr+1;
              end;
              Drawing := true;
          end;

          if (op > 0) and (op < 4)  then
          begin
              figCorrente.Left := X-1;
              figCorrente.Top := Y-1;
              figCorrente.Right := X;
              figCorrente.Bottom := Y;
          end;

          if(op>=22) then
          begin
              figCorrente.Left := X-1;
              figCorrente.Top := Y-1;
              figCorrente.Right := X;
              figCorrente.Bottom := Y;
          end;

          if(op=20) and (polyclick = true) then //PaintBoxTela.Canvas.LineTo(X,Y);
          begin
                pCurr := Point(X,Y);

                if(Entrad = true)then
                begin
                  points[indCurr]:= pInic;
                  indCurr := indCurr+1;
                end;
                Drawing := true;
          end;

          botaoArras := False;
end;

procedure TFPROJETO.PaintBoxTelaMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  ARect : TRect;
begin
   lb36.Caption := Format ('Coordenadas  (x=%d, y=%d)', [X, Y]);
   ListBox1.SetFocus;
//   ListBox1.ItemIndex:=RetornaLinhaReferentetTagOBj;
  if(polyclick = true)and(Drawing = true)then
  begin
       with PaintBoxTela do
       begin
             Canvas.Pen.Mode := pmNotXor;
             Canvas.MoveTo(Pinic.X,Pinic.Y);
             Canvas.LineTo(pCurr.X,pCurr.Y);
             Canvas.MoveTo(pinic.X,pInic.Y);
             Canvas.LineTo(x,y);
             pCurr := Point(x, y);
             Canvas.Pen.Mode := pmCopy;
       end;
  end;

  if(polyclick = true)and(Drawing = true) and (op=20)then
  begin
       with PaintBoxTela do
       begin
             Canvas.Pen.Mode := pmNotXor;
             Canvas.MoveTo(Pinic.X,Pinic.Y);
             Canvas.LineTo(pCurr.X,pCurr.Y);
             Canvas.MoveTo(pinic.X,pInic.Y);
             Canvas.LineTo(x,y);
             pCurr := Point(x, y);
             Canvas.Pen.Mode := pmCopy;
            
       end;
  end;

  if(botao = true) and ( op > 0) and (op < 4) then
    botaoArras := true;

  if(botao = true) and ( op>=22)
  then botaoArras := true;

  if botaoArras then
  begin
      ARect := objDesenha.NormalizeRect (figCorrente);
      PaintBoxTela.Canvas.DrawFocusRect (ARect);
      figCorrente.Right := X;
      figCorrente.Bottom := Y;
      ARect := objDesenha.NormalizeRect (figCorrente);
      PaintBoxTela.Canvas.DrawFocusRect(ARect);
  end;

  screen.cursor := crDefault;
end;

procedure TFPROJETO.PaintBoxTelaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Arect : TRect;
begin
  pFim := Point(X,Y);
///  ListBox1.ItemIndex:=RetornaLinhaReferentetTagOBj;
  if(polyclick = true)and(Drawing=true)then
  begin
       PaintBoxTela.Canvas.MoveTo(pInic.X, pInic.Y);
       PaintBoxTela.Canvas.LineTo(x,y);

        if entrad = false then
        begin
              points[indCurr].x := pFim.X;
              points[indCurr].y := pFim.Y;
              indCurr := indCurr+1;
        end
        else entrad := false;
  end;

  if(polyclick = true)and(Drawing=true) and (op=20)then
  begin
        PaintBoxTela.Canvas.MoveTo(pInic.X, pInic.Y);
        PaintBoxTela.Canvas.LineTo(x,y);
        if entrad = false then
        begin
              points[indCurr].x := pFim.X;
              points[indCurr].y := pFim.Y;
              indCurr := indCurr+1;
        end
        else entrad := false;
  end;


  if (op>0) and (op<4) then
  begin
        ValidateRect(Handle, nil);
        ARect := objDesenha.NormalizeRect (figCorrente);
        Canvas.DrawFocusRect (ARect);
        figCorrente.Right := X;
        figCorrente.Bottom := Y;
        ARect := objDesenha.NormalizeRect (figCorrente);
        InvalidateRect (Handle , @ARect, FALSE);
        Paintboxtela.Repaint;

        if botaoArras = true then
           if (abs (figCorrente.Right - figCorrente.Left) >= 1) and (abs (figCorrente.Bottom - figCorrente.Top) >=  1)
           then CriarFigura('','','');
  end;
  
  if (ferrag)  then
  begin
    if(op=0)then
    begin
          op := form9.tipoFerragem;
          if(form9.tipoFerragem <> 0) then
          begin
                form9.tipoFerragem := 0;
                CriarFigura('','','');
          end;
    end;
  end;

  if(op>=22)then
  begin
        ValidateRect(Handle, nil);
        ARect := objDesenha.NormalizeRect (figCorrente);
        Canvas.DrawFocusRect (ARect);
        figCorrente.Right := X;
        figCorrente.Bottom := Y;
        ARect := objDesenha.NormalizeRect (figCorrente);
        InvalidateRect (Handle , @ARect, FALSE);
        Paintboxtela.Repaint;

        if botaoArras = true then
           if (abs (figCorrente.Right - figCorrente.Left) >=  6) and (abs (figCorrente.Bottom - figCorrente.Top) >=  6)
           then CriarFigura('','','');
  end;

  op := 0;
  botao := False;
  botaoArras:= False;

end;

//Carrega todas as figuras do banco de dados
procedure TFPROJETO.Importar();
var query: TIBQuery;  //declara��o da query
    enviar : string;
begin
 { query := TIBQuery.Create(nil);

  query.Database := FDataModulo.IBDatabase;
  self.DataSource1.DataSet := query;

  with query do
  begin
      close;
      sql.Clear;
      enviar := 'select nome from figuras';
      sql.Add( enviar );
      open;
      First;
  end;     }

end;

procedure TFPROJETO.btDesenhaQuadradoClick(Sender: TObject);
begin
      op := 1;
      Self.Tag := -1;
      botao := false;
end;

procedure TFPROJETO.btDesenhaLinhaClick(Sender: TObject);
begin
     if (polyclick = true) then
      begin
            polyclick := false;
      end
     else
     begin
            polyclick := true;
            op := 20;
     end;

     indCurr:=0;
end;

//Procedimento respons�vel por apagar uma figura do paintbox
procedure TFPROJETO.btLimpaDesenhoClick(Sender: TObject);
var
  pos : integer;
  i,tagcomp,aux,tipofigura,tagiten  : Integer;
begin
    tagcomp:=-1;
    tagiten:=-1;
    aux:=-1;
    if (cont > 0) then
    begin
        if Self.Tag <> -1 then
        begin
              tipofigura := TDesenho (listaFiguras [listaFiguras.IndexOf(listaFiguras [Self.Tag])]).TipoFigura;
              pos := TDesenho (listaFiguras [listaFiguras.IndexOf(listaFiguras [Self.Tag])]).ContFig;
              TDesenho (listaFiguras [listaFiguras.IndexOf(listaFiguras [Self.Tag])]).Free;
              RetiraLista(pos); //retira objeto do listbox

              if(tipofigura>3) then
              begin
                  //Preciso encontrar em qual componente esta a figura
                  for i:=0 to ListaItens.Count-1 do
                  begin
                     if(StrToInt(retornaPalavrasAntesSimbolo(ListaItens[i],';'))=Self.Tag) then
                     begin
                        tagcomp:=StrToInt(retornaPalavrasDepoisSimbolo2(ListaItens[i],';'));
                        tagiten:=i;
                     end;
                  end;
                  //Caso a figura que esta sendo excluida seja uma ferragem ou acessorio
                  if(tagcomp<>-1) then
                  begin
                      //preciso encontrar a referencia dessa ferragem no componente e excluir essa informa��o
                      for i:=1 to TDesenho (listaFiguras [tagcomp]).ListaNomesMedidas.Count-1 do
                      begin
                           if(StrToInt(retornaPalavrasDepoisSimbolo2(retornaPalavrasDepoisSimbolo2(retornaPalavrasDepoisSimbolo2(TDesenho (listaFiguras [tagcomp]).ListaNomesMedidas[i],';'),';'),';'))=Self.Tag)then
                           begin
                                aux:=i;
                           end;
                      end;
                      //Excluo a linha da stringGrid responsavel por escrever a referencia no componente
                      if(aux<>-1)
                      then TDesenho (listaFiguras [tagcomp]).ListaNomesMedidas.Delete(aux);
                  end;

                  //se n�o encontrar ent�o significa que n�o esta como componente ou que � um objeto n�o marcado
                  if(tagiten<>-1)
                  then ListaItens.Delete(tagiten);
              end
              else
              begin
                   //preciso excluir da lista de componentes o componente excluido
                   for i:=0 to ListaComponentes.Count-1 do
                   begin
                     if(StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))=Self.Tag)
                     then aux:=i;
                   end;
                   //se n�o encontrar ent�o significa que n�o esta como componente ou que � um objeto n�o marcado
                   if(aux<>-1)
                   then ListaComponentes.Delete(aux);
                   
              end;
              cont := cont - 1;
              Self.Tag := -1;
              PaintBoxTela.Repaint;
        end
        else showmessage ('Nenhuma figura selecionada');
    end
    else showmessage ('Nenhuma figura na tela');
    if(cont = 0)then
    listaFiguras.Count := 0
end;

//Mostra Form Com as ferragens
procedure TFPROJETO.btMostraFerragensClick(Sender: TObject);
begin
      Form9.Top := Self.Top  + 565;
      Form9.Left := Self.Left + 325;
      Form9.Show;
      ferrag := true;

end;
//Procedimento do botao de habilita��o de edi��o de uma imagem
procedure TFPROJETO.btEditarImagemClick(Sender: TObject);
var i:integer;
begin
 CarregaDesenhoProjeto(True,False);
 PaintBoxTela.Repaint;
 DesabilitaHabilita(True);
{ hab := not hab;
 if (cont > 0) then
 begin
    for i  := 0 to listaFiguras.Count-1 do
    begin
          Habilita(i);
    end;
    PaintBoxTela.Repaint;
 end;  }
end;

//Split Quebra a string conforme um delimitador que � passado
procedure Split (const Delimiter: Char; Input: string; const Strings: TStrings);
begin
   Assert(Assigned(Strings));
   Strings.Clear;
   Strings.Delimiter := Delimiter;
   Strings.DelimitedText := Input;
end;


procedure TFPROJETO.Copiar1Click(Sender: TObject);
begin
     if(Self.Tag <> -1)then
        ctrlTag := Self.Tag
     else ShowMessage('Nenhuma figura selecionada');
end;

function TFPROJETO.VerificaEspacoVazioDesenhaLinha(direcao:string):Boolean;
var
  i:Integer;
begin
    result:=True;
    for i:=0 to ListaComponentes.Count -1 do
    begin
      { if(TDesenho(listaFiguras[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt1.X <= pInic.X) and
         (TDesenho(listaFiguras[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt2.X >= pInic.X) and
         (TDesenho(listaFiguras[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt1.y <= pInic.y) and
         (TDesenho(listaFiguras[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt2.y >= pInic.y)
       then Result:=False;     }

      //se for em y, preciso somente ver o x, se existe algo no intervalo
      if(direcao='y') then
      begin
        if(TDesenho(listaFiguras[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt1.X<= pInic.X) and
        (TDesenho(listaFiguras[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt2.X>= pInic.X)
        then Result:=False;
      end
      else       //se for em x, preciso somente ver o y, se existe algo no intervalo
      begin
        if(TDesenho(listaFiguras[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt1.Y<= pInic.Y) and
        (TDesenho(listaFiguras[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt2.Y>= pInic.Y)
        then Result:=False;
      end;
    end;
end;

procedure TFPROJETO.GerarLinhaMedidas(Tipo:string;Direcao:string;tagcomp:integer;Tag_aux:Integer);
var
    i:integer;
    totPts: integer;
    NomeRestanteAux,CodigoMaterial:string;
begin
  if(Tipo='componente') then
  begin
     if (tagcomp <> -1) then
     begin
         op := 21;  //tipo do objeto
         cor := TDesenho(listaFiguras[tagcomp]).tmpColor;
          PaintBoxTela.Canvas.Pen.Mode := pmCopy;
         if(op <> 3)then
         begin
              if(direcao='y') then
              begin
                pInic.X := TDesenho(listaFiguras[tagcomp]).pt2.X+10;
                pFim.X  := pInic.X + 1 ;
                pInic.Y := TDesenho(listaFiguras[tagcomp]).pt1.y +10;
                pFim.Y  := TDesenho(listaFiguras[tagcomp]).pt2.y -10;
              end ;
              if(direcao='x') then
              begin
                pInic.X := TDesenho(listaFiguras[tagcomp]).pt1.x;
                pFim.X  := TDesenho(listaFiguras[tagcomp]).pt2.x;
                pInic.Y := TDesenho(listaFiguras[tagcomp]).pt2.y+10;
                pFim.Y  := pInic.y + 1 ;
              end;
         end
         else
         begin
            totPts:= TDesenho(listaFiguras[tagcomp]).tam;
            for i := 0 to totPts do
                points[i] := TDesenho(listaFiguras[tagcomp]).pontos[i];
            indCurr := totPts+1;
         end;
         //comentei aqui pra poder liberar para escrever dentro dos componetes as medidas
        { if(VerificaEspacoVazioDesenhaLinha(direcao)=false)
         then Exit;  }

         NomeRestanteAux:=retornaPalavrasDepoisSimbolo(ListBox1.Items[tagcomp],'-');
         NomeRestanteAux:=retornaPalavrasDepoisSimbolo(NomeRestanteAux,'-');
         NomeRestanteAux:=retornaPalavrasDepoisSimbolo(NomeRestanteAux,'-');
         CodigoMaterial:= retornaPalavrasAntesSimbolo(NomeRestanteAux,'-');

         criarFigura('LinhaMedida','tabcomponente',CodigoMaterial);
         cor := clBtnFace;
     end
     else showmessage('Nenhuma figura selecionada');
  end;
  if(tipo='puxador')then
  begin
     if(tagcomp<>-1) and (tag_aux<>-1) then
     begin
         op  := 21;  //tipo do objeto
         cor := TDesenho(listaFiguras[tag_aux]).tmpColor;
         if(direcao='y') then
         begin
            pInic.X := TDesenho(listaFiguras[tag_aux]).pt2.X-5;
            pFim.X  := pInic.X + 1 ;
            pInic.Y := TDesenho(listaFiguras[tag_aux]).pt2.y;
            pFim.Y  := TDesenho(listaFiguras[tagcomp]).pt2.y;
         end ;
         if(direcao='x') then
         begin
            pInic.X := TDesenho(listaFiguras[tag_aux]).pt1.x;
            pFim.X  := TDesenho(listaFiguras[tagcomp]).pt2.x;
            if((pFim.X-pInic.x)>50)then
            begin
                pInic.X := TDesenho(listaFiguras[tagcomp]).pt1.x;
                pFim.X  := TDesenho(listaFiguras[tag_aux]).pt2.x-5;
            end;
            pInic.Y := TDesenho(listaFiguras[tag_aux]).pt2.y+10;
            pFim.Y  := pInic.y + 1 ;
         end;
         NomeRestanteAux:=retornaPalavrasDepoisSimbolo(ListBox1.Items[tagcomp],'-');
         NomeRestanteAux:=retornaPalavrasDepoisSimbolo(NomeRestanteAux,'-');
         NomeRestanteAux:=retornaPalavrasDepoisSimbolo(NomeRestanteAux,'-');
         CodigoMaterial:= retornaPalavrasAntesSimbolo(NomeRestanteAux,'-');

         criarFigura('LinhaMedidaPuxador','tabcomponente',CodigoMaterial);


     end;
  end;
end;

procedure TFPROJETO.Colar1Click(Sender: TObject);
var
    i:integer;
    totPts: integer;
begin
   if (ctrlTag <> -1) then
   begin
       op := TDesenho(listaFiguras[ctrlTag]).TipoFigura;  //tipo do objeto
       cor := TDesenho(listaFiguras[ctrlTag]).tmpColor;

       if(op <> 3)then
       begin
            pFim.X :=  pInic.X + TDesenho(listaFiguras[ctrlTag]).width ;   //xinferior do objeto
            pFim.Y := pInic.Y + TDesenho(listaFiguras[ctrlTag]).height;   //yinferior do objeto
       end
       else
       begin
          totPts:= TDesenho(listaFiguras[ctrlTag]).tam;
          for i := 0 to totPts do
              points[i] := TDesenho(listaFiguras[ctrlTag]).pontos[i];
          indCurr := totPts+1;
       end;

       criarFigura('','','');
       cor := clBtnFace;
   end
   else showmessage('Nenhuma figura selecionada');
end;

procedure TFPROJETO.btDesenhaEsferaClick(Sender: TObject);
begin
    op := 2;
    Self.Tag := -1;
    botao := false;
end;

procedure TFPROJETO.ListBox1Click(Sender: TObject);
var tmp: integer;
    str : string;
    parte : TStringList;
begin
    if tbKeyIsDown(VK_CONTROL)
    then Exit;

    if tbKeyIsDown(VK_SHIFT)
    then Exit;

    if(cont > 0) then
    begin
            tmp := Self.Tag;
            Self.Tag :=  listbox1.ItemIndex;
            parte := TStringList.Create;
            str := ListBox1.Items[listbox1.ItemIndex];

            try
              Split(' ', str, parte);
              self.Tag := strtoint( parte[1] );
            finally
              parte.Free;
            end;

            TDesenho(listaFiguras[self.Tag]).move := true;

            //indica que estou tinha clicado em uma figura mas agora estou clicando em outra
            //e devo voltar a cor antiga da outra cor
            if((tmp <> -1) and (tmp <> Self.Tag) and (listBoxContr = true))then
            begin
                 TDesenho(listaFiguras[tmp]).move := false;
                 TDesenho(listaFiguras[tmp]).Color := tmpColor;
                 repaint;
                 
            end;

            listBoxContr := true; //diz para o programa que o mouse foi clicado dentro do listbox

            tmpColor := TDesenho(listaFiguras[self.tag]).Color;
            TDesenho(listaFiguras[self.tag]).Color := clAqua;

            repaint;

            //Trecho que habilita e desabilita a figura
            hab := not TDesenho(listaFiguras[self.tag]).botao;
            Habilita(Self.Tag);
            PaintBoxTela.Repaint;

            EdtLarg.Text := inttostr(TDesenho(listaFiguras[self.tag]).Width);
            EdtAlt.Text := inttostr(TDesenho(listaFiguras[self.tag]).Height);
            EdtX.Text := inttostr(TDesenho(listaFiguras[self.tag]).pt1.X);
            EdtY.Text := inttostr(TDesenho(listaFiguras[self.tag]).pt1.Y);
    end
    else showmessage('erro');
end;



procedure TFPROJETO.ListBox1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
        //Verifica se alguma figura esta selecionada
      if(Self.Tag <> -1)and (self.Tag<=cont)then
      begin
            TDesenho(listaFiguras[self.Tag]).move := false;
            TDesenho(listaFiguras[self.tag]).Color := tmpColor; // figura recebe a cor original dela
            repaint;
      end;
      listBoxContr := false; //mostra pro programa que o o mouse deixou o listBox
end;

procedure TFPROJETO.Colar2Click(Sender: TObject);
begin
      if(Self.Tag <> -1)then
      begin //verifica se uma figura foi selecionada
            ColorDialog1.Execute();  //abre a op��o das cores
            TDesenho(listaFiguras[Self.Tag]).Color := ColorDialog1.Color;
            TDesenho(listaFiguras[Self.Tag]).tmpColor := ColorDialog1.Color;
            PaintBoxTela.Repaint;
            Self.Tag := -1;
      end
      else showmessage('Nenhuma Figura Selecionada');
end;

function TFPROJETO.LocalizaPosicaonoComponente:Integer;
var
  i:Integer;
begin
  Result:=-1;
  for i:=0 to ListaComponentes.Count-1 do
  begin
       if  ((TDesenho(listaFiguras[Self.Tag]).pt1.X >= TDesenho(listaFiguras[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt1.X))
       and ((TDesenho(listaFiguras[Self.Tag]).pt2.X <= TDesenho(listaFiguras[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt2.X))
       and ((TDesenho(listaFiguras[Self.Tag]).pt1.Y >= TDesenho(listaFiguras[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt1.Y))
       and ((TDesenho(listaFiguras[Self.Tag]).pt2.Y <= TDesenho(listaFiguras[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt2.y))
       then
       begin
            result:=StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'));
       end;
  end;


end;

function TFPROJETO.LocalizaPosicaonoComponenteProducao:Integer;
var
  i:Integer;
begin
  Result:=-1;
  for i:=0 to ListaComponentes.Count-1 do
  begin
    if  ((TDesenho(listaFigurasParaProducao[Self.Tag]).pt1.X >= TDesenho(listaFigurasParaProducao[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt1.X))
    and ((TDesenho(listaFigurasParaProducao[Self.Tag]).pt2.X <= TDesenho(listaFigurasParaProducao[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt2.X))
    and ((TDesenho(listaFigurasParaProducao[Self.Tag]).pt1.Y >= TDesenho(listaFigurasParaProducao[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt1.Y))
    and ((TDesenho(listaFigurasParaProducao[Self.Tag]).pt2.Y <= TDesenho(listaFigurasParaProducao[StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'))]).pt2.y))
    then
    begin
            result:=StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';'));
    end;
  end;


end;

procedure TFPROJETO.MenuItem4Click(Sender: TObject);
var
  Key:Word;
  Shift:TShiftState;
  Codigo,NomeMaterial,tipomaterial,referencia:string;
  NomeRestante2,Nome2:string;
  X,Y,Xaux,TagComponente,i,PosXi,PosYi,Xaux2:Integer;
begin
      if(ListBox1.ItemIndex<0) and (Self.Tag=-1) and (RetornaLinhaReferentetTagOBj>0) then
      begin
           MensagemErro('Selecione um material');
           Exit;
      end;

      ListBox1.ItemIndex:=RetornaLinhaReferentetTagOBj;

      if(lbCodigoProjeto.Caption='') or (lbCodigoProjeto.Caption='0') then
      begin
          MensagemErro('Escolha um projeto');
          Exit;
      end;

      with FfiltroImp do
      begin
           DesativaGrupos;
           Grupo06.Enabled:=True;
           ComboGrupo06.Items.Clear;
           ComboGrupo06.Items.Add('COMPONENTE');
           ComboGrupo06.Items.Add('FERRAGENS');
           ComboGrupo06.Items.Add('KITBOX');
           ComboGrupo06.Items.Add('PERFILADOS');
           ComboGrupo06.Items.Add('DIVERSOS');
           LbGrupo06.Caption:='Tipo de Material';

           ShowModal;

           Key:=VK_F9;
           if(ComboGrupo06.Text='FERRAGENS') then
           begin
                 ObjFERRAGEM_PROJ.EdtFerragemKeyDown2(Sender,Codigo,Key,Shift,NomeMaterial,lbCodigoProjeto.Caption,referencia);
                 tipomaterial:='TABFERRAGEM';


           end;
           if(ComboGrupo06.Text='COMPONENTE')then
           begin
                //no caso dos vidros em projeto, temos os seguinte...
                //na hora de cadastrar um projeto ja se escolhe o a que classe de vidro
                //o projeto pertence, por exemplo, pertence a classe de vidros temperados etc
                //ent�o se indicar que � um vidro, ent�o basta apenas mudar o nome do objeto pra vidro
                //NomeMaterial:='GRUPO DE VIDROS '+ObjPROJETO.GrupoVidro.Get_Nome;
                //tipomaterial:='TABGRUPOVIDRO';
                //Codigo:=ObjPROJETO.GrupoVidro.Get_Codigo;
                ObjCOMPONENTE_PROJ.EdtComponenteKeyDown2(Sender,Key,Shift,NomeMaterial,Codigo,lbCodigoProjeto.Caption,referencia);
                tipomaterial:='TABCOMPONENTE';
           end;
           if(ComboGrupo06.Text='KITBOX') then
           begin
                ObjKitBoxCor.EdtKitBoxKeyDown2(sender,key,Shift,codigo,NomeMaterial,lbCodigoProjeto.Caption);
                tipomaterial:='TABKITBOX';
           end;
           if(ComboGrupo06.Text='PERFILADOS')then
           begin
                ObjPERFILADO_PROJ.EdtPerfiladoKeyDown2(Sender,key,shift,NomeMaterial,codigo,lbCodigoProjeto.Caption);
                tipomaterial:='TABPERFILADO';
           end;
           if(ComboGrupo06.Text='DIVERSOS') then
           begin
                ObjDiverso_Proj.EdtDiversoKeyDown2(Sender,key,Shift,NomeMaterial,Codigo,lbCodigoProjeto.Caption);
                tipomaterial:='TABDIVERSO';
           end;
           if(NomeMaterial<>'') then
           begin
                 ListBox1.Items[listbox1.ItemIndex]:=retornaPalavrasAntesSimbolo(ListBox1.Items[listbox1.ItemIndex],'-');
                 ListBox1.Items[listbox1.ItemIndex]:=ListBox1.Items[listbox1.ItemIndex]+' - '+NomeMaterial+' - '+tipomaterial+' - '+Codigo;

                 //Marcando um componente
                 if(tipomaterial='TABCOMPONENTE') then
                 begin
                      ctrlTag := Self.Tag;
                      if(TDesenho(listaFiguras[Self.Tag]).RetornaQuantidadeStringList>0)
                      then TDesenho(listaFiguras[Self.Tag]).ListaNomesMedidas[0]:=referencia
                      else
                      begin
                         TDesenho(listaFiguras[Self.Tag]).ListaNomesMedidas.Add(referencia);
                         //listo como componente
                         ListaComponentes.Add(IntToStr(Self.Tag)+';'+NomeMaterial);
                      end;

                 end;

                 //Marcando uma ferragem
                 if(tipomaterial='TABFERRAGEM') then
                 begin
                      //Preciso localizar em qual componente a ferragem esta.
                      TagComponente:=LocalizaPosicaonoComponente;

                      if(TagComponente=-1) then
                      begin
                         MensagemAviso('Ferragem n�o esta em nenhum componente!');
                         Exit;
                      end;
                      ctrlTag := Self.Tag;
                      //Altero o tipo da figura para ferragem
                      if(TDesenho(listaFiguras[Self.Tag]).TipoFigura<=3)
                      then TDesenho(listaFiguras[Self.Tag]).TipoFigura:=4;   

                      //X vai ser o X superior da ferragem menos o X inferior da ferragem
                      X:=TDesenho(listaFiguras[Self.Tag]).pt2.X - TDesenho(listaFiguras[Self.Tag]).pt1.X;
                      //Y vai ser o Y inicial da figura menos o y inicial do componente onde vai escrever (onde a ferragem esta)
                      Y:=TDesenho(listaFiguras[Self.Tag]).pt1.Y - TDesenho(listaFiguras[TagComponente]).pt1.Y;

                      //Preciso achar a posi��o correta para escrever no componente
                      //Pego o valor de X inicial da ferragem e subtraio X inicial do componente
                      //Caso seja 0, ent�o a ferragem ta no inicio componente, ent�o basta escrever a partir do valor
                      //encontrado pra X
                      //Caso seja maior que 0, ent�o a ferragem n�o esta no incio, logo ela esta na posi��o (Valor encontrado na subtra��o)
                      //Preciso dar um espa�o de 40 para escrever a referencia da ferragem
                      Xaux := TDesenho(listaFiguras[Self.Tag]).pt1.X - TDesenho(listaFiguras[TagComponente]).pt1.X;
                      Xaux2:= TDesenho(listaFiguras[TagComponente]).pt2.X - TDesenho(listaFiguras[TagComponente]).pt1.X;

                      //Se tiver al�m da metade do componente em que esta
                      if(Xaux >(Xaux2/2)) then
                      begin
                           // Tenho que escrever antes do desenho do item(ferragem)
                           X:=Xaux - 40;
                      end
                      else
                      begin
                          //se estiver antes da metade, ent�o preciso multiplicar o valor por 2
                          //pois o valor � o tamanho do desenho, ent�o reservo duas vezes o tamanho do desenho pra poder escrever sua referencia
                          if(Xaux>0) and (X<Xaux)
                          then X:=Xaux*2;
                      end;

                      //Adiciono na lista de nomes do componente.
                      //Xaux - tag da ferragem
                      //Xaux2 - tag do componente

                      Xaux:=VerificaFerragemInserida(Xaux2);
                      //se a ferragem ainda n�o estiver ligada a nenhum componente
                      //basta fazer a liga��o adicionando nas listas
                      if(Xaux=-1) then
                      begin

                          //Verfico se n�o vou escrever aonde j� foi escrito =)
                          for i:=1 to (TDesenho(listaFiguras[TagComponente]).RetornaQuantidadeStringList -1) do
                          begin
                              //Pega a posi��o X,Y onde tem algo escrito
                              PosXi:=StrToInt(retornaPalavrasAntesSimbolo(retornaPalavrasDepoisSimbolo2(TDesenho(listaFiguras[TagComponente]).ListaNomesMedidas[i],';'),';'));
                              PosYi:=StrToInt(RetornaPalavrasAntesSimbolo(retornaPalavrasDepoisSimbolo2(retornaPalavrasDepoisSimbolo2(TDesenho(listaFiguras[TagComponente]).ListaNomesMedidas[i],';'),';'),';'));
                              if  (X >= PosXi-10)  and
                                  (X <= PosXi+10)  and
                                  (Y >= PosYi-10)  and
                                  (Y <= PosYi+10)  then
                              begin
                                  if((Y+50)>TDesenho(listaFiguras[TagComponente]).pt2.Y)
                                  then Y:=Y-50
                                  else Y:=Y+50;
                              end;
                          end;

                          TDesenho(listaFiguras[TagComponente]).ListaNomesMedidas.Add(referencia+';'+IntToStr(X)+';'+IntToStr(Y)+';'+inttostr(Self.Tag));
                          ListaItens.Add(IntToStr(Self.Tag)+';'+inttostr(TagComponente));
                      end
                      else
                      begin
                          //caso esteja significa que quero editar
                          //trocar a ferragem
                          //ent�o edito os valores nas listas

                          //edito na listaitens
                          ListaItens[Xaux]:=(IntToStr(Self.Tag)+';'+inttostr(TagComponente));

                          //procuro em que posi��o da lista esta a informa��o com o nome desse material
                          for i:=1 to TDesenho (listaFiguras [Xaux2]).ListaNomesMedidas.Count-1 do
                          begin
                              if(StrToInt(retornaPalavrasDepoisSimbolo2(retornaPalavrasDepoisSimbolo2(retornaPalavrasDepoisSimbolo2(TDesenho (listaFiguras [Xaux2]).ListaNomesMedidas[i],';'),';'),';'))=Self.Tag)then
                              begin
                                 Xaux:=i;
                              end;
                          end;
                          //exclou a linha da lista referente a este item
                          TDesenho (listaFiguras [Xaux2]).ListaNomesMedidas.Delete(Xaux);
                          //insiro novamente, caso mude de componente muda tambem

                          //Verfico se n�o vou escrever aonde j� foi escrito =)
                          for i:=1 to (TDesenho(listaFiguras[TagComponente]).RetornaQuantidadeStringList -1) do
                          begin
                              //Pega a posi��o X,Y onde tem algo escrito
                              PosXi:=StrToInt(retornaPalavrasAntesSimbolo(retornaPalavrasDepoisSimbolo2(TDesenho(listaFiguras[TagComponente]).ListaNomesMedidas[i],';'),';'));
                              PosYi:=StrToInt(RetornaPalavrasAntesSimbolo(retornaPalavrasDepoisSimbolo2(retornaPalavrasDepoisSimbolo2(TDesenho(listaFiguras[TagComponente]).ListaNomesMedidas[i],';'),';'),';'));
                              if  (X >= PosXi-10)  and
                                  (X <= PosXi+10)  and
                                  (Y >= PosYi-10)  and
                                  (Y <= PosYi+10)  then
                              begin
                                  if((Y+50)>TDesenho(listaFiguras[TagComponente]).pt2.Y)
                                  then Y:=Y-50
                                  else Y:=Y+50;
                              end;
                          end;                          
                          TDesenho(listaFiguras[TagComponente]).ListaNomesMedidas.Add(referencia+';'+IntToStr(X)+';'+IntToStr(Y)+';'+inttostr(Self.Tag));
                      end;

                 end;
                 PaintBoxTela.Repaint;
                 Self.Tag := -1;
           end;
      end;
end;

function TFPROJETO.VerificaFerragemInserida(var comp:integer):Integer;
var
  i:Integer;
begin
   Result:=-1;
   comp:=-1;
   for i:=0 to ListaItens.count-1 do
   begin
      if(StrToInt(retornaPalavrasAntesSimbolo(ListaItens[i],';'))=Self.Tag) then
      begin
          comp:=StrToInt(retornaPalavrasDepoisSimbolo2(ListaItens[i],';'));
          Result:=i;
      end;
   end;

end;

//Salvando a imagem no banco de dados
procedure TFPROJETO.btSalvarModelagemClick(Sender: TObject);
var i: Integer;
    j : integer;
    k : integer;
    str : string;
    parte : TStringList;
    query : TIBQuery;  //declara��o da query
    query2 : TIBQuery;
    tipo : string;
    pts : string;
    x1, y1, x2, y2: string;
    cor : string;
    enviar : string;
    Nome :string;
    CodigoMaterial:string;
    TipoMaterial:string;
    contador:Integer;
    NomeRestanteAux,Texto,PosX,PosY,referenciaItem:string;
    Indice,Indice2,PosicaoListaMedidasComp:Integer;
begin
  query := TIBQuery.Create(nil);
  query.Database := FDataModulo.IBDatabase;

  query2 := TIBQuery.Create(nil);
  query2.Database := FDataModulo.IBDatabase;

  //Gera as linhas de medida
  FinalizarProjeto;

  //Verifico se estou inserindo ou atualizando
  with query do
  begin
     close;
     sql.Clear;
     sql.Add('select * from tabprojetos_id where projeto='+lbCodigoProjeto.Caption);
     Open;

     Last;
     if(recordcount>0)
     then idCarr:=True; //Ja tem, preciso apenas atualizar...
  end;


  if (lbCodigoProjeto.Caption <> '') and (lbCodigoProjeto.Caption<>'0') then
  begin
     //Se esta criando uma nova figura entao cadastro no banco uma nova figura
     if(idCarr = False)then
     begin
       with query do
       begin
         close;
         sql.Clear;
         //Adiciona no banco de dados figuras os dados
         sql.Add('insert into tabprojetos_id(nome, qtd,projeto) values ('#39+edtDescricao.Text+#39+','+#39+inttostr(cont)+#39+','+lbcodigoprojeto.caption+')');
         ExecSQL;   //executa a query
         FDataModulo.IBTransaction.CommitRetaining;   //comita pro banco
         close;
       end;
     end
     else
     begin  //senao deleto os pontos dela e coloco os novos pontos
       with query do
       begin
              close;
              sql.Clear;
              //remove os pontos da figura
              sql.Add('delete from tabprojetospontos_id where projeto_id = '#39+inttostr(idFig)+#39 +' and ligadoaocomponente is not null');
              ExecSQL;   //executa a query
              FDataModulo.IBTransaction.CommitRetaining;   //comita pro banco

              close;
              sql.Clear;
              sql.Add('delete from tabprojetospontos_id where projeto_id = '#39+inttostr(idFig)+#39);
              ExecSQL;   //executa a query
              FDataModulo.IBTransaction.CommitRetaining;   //comita pro banco

              close;
              sql.Clear;
              //atualiza a quantidade de pontos no banco
              sql.Add('update tabprojetos_id set nome='#39+edtDescricao.Text+#39+',qtd='#39+inttostr(cont)+#39+' where id = '#39+inttostr(idFig)+#39);
              ExecSQL;   //executa a query
              FDataModulo.IBTransaction.CommitRetaining;   //comita pro banco
              close;
       end;
     end;

     //Pega o codigo atual da figura  caso
     if(idCarr = False)then
     begin
       with query do
       begin
          close;
          sql.Clear;
          sql.Add('select MAX(ID) from tabprojetos_id');
          open;
          idFig := fields[0].asinteger;
          close;
       end;
       idCarr := true;
     end;

     for i := 0 to cont-1 do
     begin
       parte := TStringList.Create;
       str := ListBox1.Items[i];
       NomeRestanteAux:=retornaPalavrasDepoisSimbolo(ListBox1.Items[i],'-');
       nome:=retornaPalavrasAntesSimbolo(NomeRestanteAux,'-');
       NomeRestanteAux:=retornaPalavrasDepoisSimbolo(NomeRestanteAux,'-');
       TipoMaterial:= retornaPalavrasAntesSimbolo(NomeRestanteAux,'-');
       NomeRestanteAux:=retornaPalavrasDepoisSimbolo(NomeRestanteAux,'-');
       CodigoMaterial:= retornaPalavrasAntesSimbolo(NomeRestanteAux,'-');


       try
         Split(' ', str, parte);
         j := strtoint(parte[1]);
         pts := '';

         tipo:= inttostr(TDesenho (listaFiguras [j]).TipoFigura);
         cor := ColorToString(TDesenho (listaFiguras [j]).color);

         x1 :=  inttostr(TDesenho (listaFiguras [j]).pt1.x);
         y1 :=  inttostr(TDesenho (listaFiguras [j]).pt1.Y);

         if(strtoint(tipo) <> 3)then
         begin
              x2 :=  inttostr(TDesenho (listaFiguras [j]).pt2.X);
              y2 :=  inttostr(TDesenho (listaFiguras [j]).pt2.Y);
         end
         else
         begin
            for k := 0 to TDesenho (listaFiguras [j]).tam do
            begin
                  pts:= pts + inttostr(TDesenho (listaFiguras [j]).pontos[k].x) + ';';
                  pts:= pts + inttostr(TDesenho (listaFiguras [j]).pontos[k].y) + ';';
            end;
         end;

         with query do
         begin
           close;
           sql.Clear;
           //Adiciona no banco de dados figuras os dados
           //se n�o for um poligono

           if(CodigoMaterial='')
           then CodigoMaterial:='0';

           if(strtoint(tipo) <> 3)then
           begin
                //Se for um componente apenas grava
                if(TipoMaterial='TABCOMPONENTE')
                then enviar :=  'insert into tabprojetospontos_id(tipo, cor, xsup, ysup, xinf, yinf, projeto_id,nome,tipomaterial,codigomaterial,referencia) values ('#39+tipo+#39+','+#39+cor+#39+','+#39+x1+#39+','+#39+y1+#39+','+#39+x2+#39+','+#39+y2+#39+','+#39+inttostr(idFig)+#39+','+#39+nome+#39+','+#39+TipoMaterial+#39+','+#39+CodigoMaterial+#39+','+#39+TDesenho (listaFiguras [j]).ListaNomesMedidas[0]+#39+')'
                else
                begin
                  if(TipoMaterial='TABFERRAGEM')  then
                  begin
                     //se for um item, ent�o preciso gravar em qual componente esta
                     //procuro a linha na lista de itens (listaitens) que corresponde ao item
                     //sabendo a linha que ferragem esta, sei em qual componente a ferragem esta
                     //localizo a linha na lista de componentes que o componente esta (listacomponentes)
                     //INDICE = LINHA DA STRINGLIST (LISTACOMPONENTE) EM QUE ESTA O COMPONENTE QUE A FERRAGEM ESTA LIGADA
                     Indice:= LocalizaPosicaoListaComponente(j);

                     //INDICE2 = VALOR COM A TAG DO OBJ CORRESPONDENTE (COMPONENTE)

                     //Se vier com -1, significa que o item n�o foi ligado a nenhum material e n�o esta em nenhum componente
                     if(Indice<>-1) then
                     begin
                       //localizo em qual componente este item(ferragem) esta.
                       query2.Close;
                       query2.SQL.Clear;
                       query2.SQL.Add('select id from tabprojetospontos_id where nome='+#39+retornaPalavrasDepoisSimbolo2(ListaComponentes[indice],';')+#39);
                       query2.SQL.Add('and projeto_id='+inttostr(idFig));
                       query2.Open;

                       //tenho a tag de id do componente
                       Indice2:=StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[indice],';'));
                       //localizo dentro da lista de medidas do componente qual linha esta a ferragens
                       PosicaoListaMedidasComp :=LocalizaTagListaXYREFDentroComponente(Indice2,j);

                       //desmembro a informa��o nessa linha, pra montar a referencia desse item
                       //Referencia+posi��o x,y dentro componente
                       referenciaItem:='';
                       Texto:=retornaPalavrasAntesSimbolo(TDesenho(listaFiguras[Indice2]).ListaNomesMedidas[PosicaoListaMedidasComp],';');
                       PosX:=retornaPalavrasAntesSimbolo(retornaPalavrasDepoisSimbolo2(TDesenho(listaFiguras[Indice2]).ListaNomesMedidas[PosicaoListaMedidasComp],';'),';');
                       PosY:=RetornaPalavrasAntesSimbolo(retornaPalavrasDepoisSimbolo2(retornaPalavrasDepoisSimbolo2(TDesenho(listaFiguras[Indice2]).ListaNomesMedidas[PosicaoListaMedidasComp],';'),';'),';');
                       referenciaItem:=Texto+';'+PosX+';'+PosY;

                       enviar :=  'insert into tabprojetospontos_id(tipo, cor, xsup, ysup, xinf, yinf, projeto_id,nome,tipomaterial,codigomaterial,ligadoaocomponente,referencia) values ('#39+tipo+#39+','+#39+cor+#39+','+#39+x1+#39+','+#39+y1+#39+','+#39+x2+#39+','+#39+y2+#39+','+#39+inttostr(idFig)+#39+','+#39+nome+#39+','+#39+TipoMaterial+#39+','+#39+CodigoMaterial+#39+','+#39+query2.fieldbyname('id').AsString+#39+','+#39+referenciaItem+#39+ ')'
                     end
                     else enviar :=  'insert into tabprojetospontos_id(tipo, cor, xsup, ysup, xinf, yinf, projeto_id,nome,tipomaterial,codigomaterial) values ('#39+tipo+#39+','+#39+cor+#39+','+#39+x1+#39+','+#39+y1+#39+','+#39+x2+#39+','+#39+y2+#39+','+#39+inttostr(idFig)+#39+','+#39+nome+#39+','+#39+TipoMaterial+#39+','+#39+CodigoMaterial+#39+')';
                  end
                  else enviar :=  'insert into tabprojetospontos_id(tipo, cor, xsup, ysup, xinf, yinf, projeto_id,nome,tipomaterial,codigomaterial) values ('#39+tipo+#39+','+#39+cor+#39+','+#39+x1+#39+','+#39+y1+#39+','+#39+x2+#39+','+#39+y2+#39+','+#39+inttostr(idFig)+#39+','+#39+nome+#39+','+#39+TipoMaterial+#39+','+#39+CodigoMaterial+#39+')';
                end;
           end
           else
           begin
             if(TDesenho (listaFiguras [j]).ListaNomesMedidas.Count>0)
             then enviar :=  'insert into tabprojetospontos_id(tipo, cor, xsup, ysup, pts, projeto_id,nome,tipomaterial,codigomaterial,referencia) values ('#39+tipo+#39+','+#39+cor+#39+','+#39+x1+#39+','+#39+y1+#39+','+#39+pts+#39+','+#39+inttostr(idFig)+#39+','+#39+nome+#39+','+#39+TipoMaterial+#39+','+#39+CodigoMaterial+#39+','+#39+TDesenho (listaFiguras [j]).ListaNomesMedidas[0]+#39+')'
             else enviar :=  'insert into tabprojetospontos_id(tipo, cor, xsup, ysup, pts, projeto_id,nome,tipomaterial,codigomaterial) values ('#39+tipo+#39+','+#39+cor+#39+','+#39+x1+#39+','+#39+y1+#39+','+#39+pts+#39+','+#39+inttostr(idFig)+#39+','+#39+nome+#39+','+#39+TipoMaterial+#39+','+#39+CodigoMaterial+#39+')'
           end;

           sql.Add( enviar );

           try
            ExecSQL;              //executa a query
           except

            on e:Exception do
            begin
              ShowMessage(e.Message);
              InputBox('erro','erro no sql:',enviar);
            end

           end;

           close;

         end;

       finally
         parte.Free;
         FDataModulo.IBTransaction.CommitRetaining;   //comita pro banco
       end;
     end;
     {CarregaDesenhoProjeto(False);
     image5.Canvas.CopyRect(Image5.Canvas.ClipRect,PaintBoxTela.Canvas,PaintBoxTela.Canvas.ClipRect);
     Image5.Picture.SaveToFile(ObjPROJETO.ResgataLocalDesenhos+EdtReferencia.Text+'.bmp');  }

     DesabilitaHabilita(False);
     MensagemAviso('Figura Salva');
  end
  else
  begin
      MensagemAviso('Escolha um projeto!');
  end;
  FreeAndNil(query); //libera a query

end;

//Procedimento que limpa todas as figuras existentes no paintbox
//Chama o procedimento de retirada de figura do listbox
procedure TFPROJETO.LimpaTudo();
var i : integer;
    str : string;
    parte : TStringList;
begin
  try
            // exclui todos os objetos da tela
            for i := cont-1 downto 0 do
            begin
                  parte := TStringList.Create;
                  str := ListBox1.Items[i];

                  try
                    Split(' ', str, parte);
                    TDesenho (listaFiguras[strtoint(parte[1])]).Free;
                  finally
                    parte.Free;
                  end;
            end;
            listaFiguras.Clear;
            //Limpa o listbox onde mostra cada figura
            for i:= ListBox1.Count - 1 downto 0 do
                ListBox1.Items.Delete(i);
            ListBox1.Clear;
            cont := 0;

            Refresh;

  finally

  end;


end;

//Procedimento que limpa todas as figuras existentes no paintbox
//Chama o procedimento de retirada de figura do listbox
procedure TFPROJETO.LimpaTudoIMGProducao();
var i : integer;
    str : string;
    parte : TStringList;
begin
  try
      // exclui todos os objetos da tela
      for i := contProducao-1 downto 0 do
      begin
          parte := TStringList.Create;
          str := ListBox2.Items[i];

          try
            Split(' ', str, parte);
            TDesenho (listaFigurasParaProducao[strtoint(parte[1])]).Free;
          finally
            parte.Free;
          end;
      end;
      listaFigurasParaProducao.Clear;
      //Limpa o listbox onde mostra cada figura
      for i:= ListBox2.Count - 1 downto 0 do
          ListBox2.Items.Delete(i);
      ListBox2.Clear;
      contProducao := 0;

      Refresh;

  finally

  end;


end;

//Carregar os desenhos do projeto
procedure TFPROJETO.CarregaDesenhoProjeto(ComReferencias:Boolean;MostraLinhas:Boolean);
var query: TIBQuery;  //declara��o da query
    enviar : string;
    busc : string;
    str : string;
    retorno : TstringList;
    k : integer;
    Indice:Integer;
begin

    LimpaTudo();  //limpa todos os objetos e toda a listbox

    ConfiguraStringGrid;
    query := TIBQuery.Create(nil);
    query.Database :=FDataModulo.IBDatabase;

    ListaComponentes.Clear;
    ListaItens.Clear;
    listaFiguras.Clear;
    //busca o id da figura pelo nome clicado no StringGrid
    with query do
    begin
          close;
          sql.Clear;
          //enviar := 'select id from tabprojetos_id where projeto = '+lbCodigoProjeto.Caption;

          sql.Add('select id from tabprojetos_id where projeto = '+lbCodigoProjeto.Caption);
          open;
          Last;
          if(recordcount=0)then
          begin
              idCarr:=False;
              Exit;
          end;
          busc    := fieldbyname('id').AsString;
          idCarr  := True;  //Indica que uma figura ja foi carregada
          idFig   := strtoint(busc);    //Indica o numero da figura que foi carregada
          close;
    end;

    //Busca todos os objetos da figura
    with query do
    begin
        close;
        sql.Clear;
        sql.Add('select * from tabprojetospontos_id where projeto_id = '#39+busc+#39 );
        open;
        First;
        cont := 0; //Digo que na tela nao tem nenhum figura
        while not (query.eof) do
        begin
             if(MostraLinhas=True) then
             begin
                 op := FieldByName('tipo').AsInteger;  //tipo do objeto
                 cor := StringToColor (FieldByName('cor').AsString);
                 pInic.X := FieldByName('xsup').AsInteger;  //xsuperior  do objeto
                 pInic.Y := FieldByName('ysup').AsInteger;  //ysuperior do objeto

                 if(op <> 3)then
                 begin
                   pFim.X := FieldByName('xinf').AsInteger;   //xinferior do objeto
                   pFim.Y := FieldByName('yinf').AsInteger;   //yinferior do objeto
                 end
                 else
                 begin
                      retorno := TStringList.Create;
                      str := FieldByName('pts').AsString;
                      Split(';', str, retorno);
                      k := 0;
                      while (k < (retorno.count-1)/2) do
                      begin
                          points[k].X := strtoint(retorno[k*2]);
                          points[k].Y := strtoint(retorno[(k*2)+1]);
                          k := k+1;
                      end;
                      indCurr := round((retorno.count-1)/2);
                      pux_banc := true;
                 end;
                 criarFigura(fieldbyname('nome').asstring,fieldbyname('tipomaterial').asstring,fieldbyname('codigomaterial').asstring);
                 pux_banc := false;
                 cor := clbtnface;
             end
             else
             begin
                if(FieldByName('tipo').AsInteger<>21)then
                begin
                   op := FieldByName('tipo').AsInteger;  //tipo do objeto
                   cor := StringToColor (FieldByName('cor').AsString);
                   pInic.X := FieldByName('xsup').AsInteger;  //xsuperior  do objeto
                   pInic.Y := FieldByName('ysup').AsInteger;  //ysuperior do objeto

                   if(op <> 3)then
                   begin
                     pFim.X := FieldByName('xinf').AsInteger;   //xinferior do objeto
                     pFim.Y := FieldByName('yinf').AsInteger;   //yinferior do objeto
                   end
                   else
                   begin
                        retorno := TStringList.Create;
                        str := FieldByName('pts').AsString;
                        Split(';', str, retorno);
                        k := 0;
                        while (k < (retorno.count-1)/2) do
                        begin
                            points[k].X := strtoint(retorno[k*2]);
                            points[k].Y := strtoint(retorno[(k*2)+1]);
                            k := k+1;
                        end;
                        indCurr := round((retorno.count-1)/2);
                        pux_banc := true;
                   end;
                   criarFigura(fieldbyname('nome').asstring,fieldbyname('tipomaterial').asstring,fieldbyname('codigomaterial').asstring);
                   pux_banc := false;
                   cor := clbtnface;
                end;
             end;
             if(ComReferencias=true)then
             begin
                 //Se � um componente preciso apenas passar sua referencia pra listanomesmedidas do obj
                 //preciso inseri-lo na lista de componentes (listacomponentes)
                 if(FieldByName('tipomaterial').AsString='TABCOMPONENTE') then
                 begin
                    ListaComponentes.Add(IntToStr(objDesenha.ContFig)+';'+fieldbyname('nome').asstring);
                    objDesenha.ListaNomesMedidas.Add(FieldByName('referencia').AsString)
                 end;
                 //Se for uma ferragem preciso passar para a lista listanomemedidas do componente a referencia da ferragem
                 //posi��o (x,y) da mesma dentro do componente
                 //Preciso inseri-la na lista de itens (listaitens)
                 if(FieldByName('tipomaterial').AsString='TABFERRAGEM') then
                 begin
                     //Localizo em qual componente essa ferragem foi inserida
                     if(fieldbyname('ligadoaocomponente').asstring<>'')then
                     begin
                         indice:=LocalizaTagCompAoCarregar(fieldbyname('ligadoaocomponente').asstring);
                         //Caso o indice seja -1 ent�o a mesma n�o esta ligada em nenhum componente
                         if(Indice<>-1) then
                         begin
                            //Somente se o valor em referencia, q para ferragens � (text;x;y) estiver com algo
                            if(FieldByName('referencia').AsString<>'') then
                            begin
                              TDesenho(listaFiguras[Indice]).ListaNomesMedidas.Add(FieldByName('referencia').AsString+';'+inttostr(objDesenha.ContFig));
                              ListaItens.Add(IntToStr(objDesenha.ContFig)+';'+IntToStr(Indice));
                            end;
                         end;
                     end;
                 end;
             end;
             query.Next;
        end;

        close;
        DesabilitaHabilita(False);
        //Preenchemedidas;
    end;

end;

//Carregar os desenhos do projeto
procedure TFPROJETO.CarregaDesenhoProjetoProducao(ComReferencias:Boolean;MostraLinhas:Boolean);
var query: TIBQuery;  //declara��o da query
    enviar : string;
    busc : string;
    str : string;
    retorno : TstringList;
    k : integer;
    Indice:Integer;
begin
  LimpaTudoIMGProducao();  //limpa todos os objetos e toda a listbox

  query := TIBQuery.Create(nil);
  query.Database :=FDataModulo.IBDatabase;
  try
      ListaComponentes.Clear;
      ListaItens.Clear;
      listaFigurasParaProducao.Clear;
      //busca o id da figura pelo nome clicado no StringGrid
      with query do
      begin
          close;
          sql.Clear;
          sql.Add('select id from tabprojetos_id where projeto = '+lbCodigoProjeto.Caption);

          open;
          Last;
          if(recordcount=0)then
          begin
              idCarr:=False;
              Exit;
          end;
          busc    := fieldbyname('id').AsString;
          idCarr  := True;  //Indica que uma figura ja foi carregada
          idFig   := strtoint(busc);    //Indica o numero da figura que foi carregada
          close;
      end;

      //Busca todos os objetos da figura
      with query do
      begin
        close;
        sql.Clear;
        sql.Add('select * from tabprojetospontos_id where projeto_id = '#39+busc+#39 );
        SQL.Add('and tipomaterial<>''TABFERRAGEM''');
        open;
        First;
        contProducao := 0; //Digo que na tela nao tem nenhum figura
        while not (query.eof) do
        begin
             if(MostraLinhas=True) then
             begin
                 op := FieldByName('tipo').AsInteger;  //tipo do objeto
                 cor := StringToColor (FieldByName('cor').AsString);
                 pInic.X := FieldByName('xsup').AsInteger;  //xsuperior  do objeto
                 pInic.Y := FieldByName('ysup').AsInteger;  //ysuperior do objeto

                 if(op <> 3)then
                 begin
                   pFim.X := FieldByName('xinf').AsInteger;   //xinferior do objeto
                   pFim.Y := FieldByName('yinf').AsInteger;   //yinferior do objeto
                 end
                 else
                 begin
                      retorno := TStringList.Create;
                      str := FieldByName('pts').AsString;
                      Split(';', str, retorno);
                      k := 0;
                      while (k < (retorno.count-1)/2) do
                      begin
                          points[k].X := strtoint(retorno[k*2]);
                          points[k].Y := strtoint(retorno[(k*2)+1]);
                          k := k+1;
                      end;
                      indCurr := round((retorno.count-1)/2);
                      pux_banc := true;
                 end;
                 CriarFiguraIMGProducao(fieldbyname('nome').asstring,fieldbyname('tipomaterial').asstring,fieldbyname('codigomaterial').asstring);
                 pux_banc := false;
                 cor := $00A2663E;
             end
             else
             begin
                if(FieldByName('tipo').AsInteger<>21)then
                begin
                   op := FieldByName('tipo').AsInteger;  //tipo do objeto
                   cor := StringToColor (FieldByName('cor').AsString);
                   pInic.X := FieldByName('xsup').AsInteger;  //xsuperior  do objeto
                   pInic.Y := FieldByName('ysup').AsInteger;  //ysuperior do objeto

                   if(op <> 3)then
                   begin
                     pFim.X := FieldByName('xinf').AsInteger;   //xinferior do objeto
                     pFim.Y := FieldByName('yinf').AsInteger;   //yinferior do objeto
                   end
                   else
                   begin
                        retorno := TStringList.Create;
                        str := FieldByName('pts').AsString;
                        Split(';', str, retorno);
                        k := 0;
                        while (k < (retorno.count-1)/2) do
                        begin
                            points[k].X := strtoint(retorno[k*2]);
                            points[k].Y := strtoint(retorno[(k*2)+1]);
                            k := k+1;
                        end;
                        indCurr := round((retorno.count-1)/2);
                        pux_banc := true;
                   end;
                   CriarFiguraIMGProducao(fieldbyname('nome').asstring,fieldbyname('tipomaterial').asstring,fieldbyname('codigomaterial').asstring);
                   pux_banc := false;
                   cor := $00A2663E;
                end;
             end;
             if(ComReferencias=true)then
             begin
                 //Se � um componente preciso apenas passar sua referencia pra listanomesmedidas do obj
                 //preciso inseri-lo na lista de componentes (listacomponentes)
                 if(FieldByName('tipomaterial').AsString='TABCOMPONENTE') then
                 begin
                    ListaComponentes.Add(IntToStr(objDesenha.ContFig)+';'+fieldbyname('nome').asstring);
                    objDesenha.ListaNomesMedidas.Add(FieldByName('referencia').AsString)
                 end;

             end;
             query.Next;
        end;
        close;
      end;
  finally
      FreeAndNil(query);
  end;
end;

procedure TFPROJETO.btNovoModeloClick(Sender: TObject);
begin
     LimpaTudo;
     ListaItens.Clear;
     ListaComponentes.Clear;
     DesabilitaHabilita(True);
end;

procedure TFPROJETO.PaintBoxTelaClick(Sender: TObject);
var tmp: integer;
    str : string;
    parte : TStringList;
begin
//    ListBox1.ItemIndex:=RetornaLinhaReferentetTagOBj;

    if listbox1.ItemIndex<0
    then exit;

    if(cont > 0) then
    begin
       tmp := Self.Tag;
       Self.Tag :=  listbox1.ItemIndex;
       parte := TStringList.Create;
       str := ListBox1.Items[listbox1.ItemIndex];

       try
         Split(' ', str, parte);
         self.Tag := strtoint( parte[1] );
       finally
         parte.Free;
       end;

       TDesenho(listaFiguras[self.Tag]).move := true;

       //indica que estou tinha clicado em uma figura mas agora estou clicando em outra
       //e devo voltar a cor antiga da outra cor
       if((tmp <> -1) and (tmp <> Self.Tag) and (listBoxContr = true))then
       begin
          TDesenho(listaFiguras[tmp]).move := false;
          TDesenho(listaFiguras[tmp]).Color := tmpColor;
          repaint;
       end;

       listBoxContr := true; //diz para o programa que o mouse foi clicado dentro do listbox

       tmpColor := TDesenho(listaFiguras[self.tag]).Color;
       TDesenho(listaFiguras[self.tag]).Color := clAqua;

       repaint;

       //Trecho que habilita e desabilita a figura
       //tmp := hab;
       hab := not TDesenho(listaFiguras[self.tag]).botao;
       Habilita(Self.Tag);
       PaintBoxTela.Repaint;
       //hab := tmpt;

       EdtLarg.Text := inttostr(TDesenho(listaFiguras[self.tag]).Width);
       EdtAlt.Text := inttostr(TDesenho(listaFiguras[self.tag]).Height);
       EdtX.Text := inttostr(TDesenho(listaFiguras[self.tag]).pt1.X);
       EdtY.Text := inttostr(TDesenho(listaFiguras[self.tag]).pt1.Y);
    end
    else showmessage('erro');
end;

procedure TFPROJETO.btPoligonoClick(Sender: TObject);
begin
   if (polyclick = true) then
   begin
      polyclick := false;
   end
   else
   begin
      polyclick := true;
      op := 0;
   end;
   indCurr:=0;
end;

procedure TFPROJETO.ConfiguraStringGrid;
begin
    {with STRGProdutos do
    begin
          RowCount:=1;
          ColCount:=4;
          ColWidths[0] := 50;
          ColWidths[1] := 250;
          ColWidths[2] := 250;
          ColWidths[3] := 50;
          Cells[0,0] := 'OBJ';
          Cells[1,0] := 'MATERIAL';
          Cells[2,0] := 'TIPO MATERIAL';
          Cells[3,0] := 'CODIGO';

    end;    }
end;

procedure TFPROJETO.STRGProdutosClick(Sender: TObject);
var tmp: integer;
    str : string;
    parte : TStringList;
begin

end;

procedure TFPROJETO.STRGProdutosMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
       if(Self.Tag <> -1)and (self.Tag<=cont)then
      begin
            TDesenho(listaFiguras[self.Tag]).move := false;
            TDesenho(listaFiguras[self.tag]).Color := tmpColor; // figura recebe a cor original dela
            repaint;
      end;
      listBoxContr  := false; //mostra pro programa que o o mouse deixou o listBox
end;

procedure TFPROJETO.Medidas1Click(Sender: TObject);
var
  Nome:string;
  NomeRestanteAux:string;
  CodigoMaterial:string;
  TipoMaterial:string;
  Query:TIBQuery;
begin
       //Aqui marcar a posi��o no vidro que deve ser cortada pra colocar
       //os projetos, fechadura etc
       //Salvar na TabFerragem_Proj
        Query:=TIBQuery.Create(nil);
        Query.Database:=FDataModulo.IBDatabase;
        try
                NomeRestanteAux:=retornaPalavrasDepoisSimbolo(ListBox1.Items[listbox1.ItemIndex],'-');
                nome:=retornaPalavrasAntesSimbolo(NomeRestanteAux,'-');
                NomeRestanteAux:=retornaPalavrasDepoisSimbolo(NomeRestanteAux,'-');
                TipoMaterial:= retornaPalavrasAntesSimbolo(NomeRestanteAux,'-');
                NomeRestanteAux:=retornaPalavrasDepoisSimbolo(NomeRestanteAux,'-');
                CodigoMaterial:= retornaPalavrasAntesSimbolo(NomeRestanteAux,'-');

                with Query do
                begin
                        Close;
                        sql.Clear;
                        sql.Add('select codigo from '+TipoMaterial+'_PROJ');
                        if(TipoMaterial='TABFERRAGEM') then
                        begin
                             sql.Add('where FERRAGEM='+CodigoMaterial);
                             sql.Add('and projeto='+lbCodigoProjeto.Caption);
                             Open;
                             FmedidasFuros.PassaMaterial(TipoMaterial+'_PROJ',fieldbyname('codigo').AsString,nome,lbCodigoProjeto.Caption);
                             FmedidasFuros.ShowModal;
                             if(FmedidasFuros.Tag=1) then
                             begin
                                  ObjFERRAGEM_PROJ.LocalizaCodigo(fieldbyname('codigo').AsString);
                                  ObjFERRAGEM_PROJ.TabelaparaObjeto;

                                  ObjFERRAGEM_PROJ.Status:=dsEdit;

                                  ObjFERRAGEM_PROJ.Submit_AlturaCorte(FmedidasFuros.AlturaCorte);
                                  ObjFERRAGEM_PROJ.Submit_PosicaoCorte(FmedidasFuros.PosicaoCorte);
                                  ObjFERRAGEM_PROJ.Submit_TamanhoCorte(FmedidasFuros.TamanhoCorte);

                                  ObjFERRAGEM_PROJ.Salvar(True);
                                  
                             end;
                        end
                        else
                        begin
                              if(TipoMaterial='TABPERFILADO') then
                              begin
                                   sql.Add('where PERFILADO='+CodigoMaterial);
                                   sql.Add('and projeto='+lbCodigoProjeto.Caption);
                                   Open;
                                   FmedidasFuros.PassaMaterial(TipoMaterial+'_PROJ',fieldbyname('codigo').AsString,nome,lbCodigoProjeto.Caption);
                                   FmedidasFuros.ShowModal;
                                   if(FmedidasFuros.Tag=1) then
                                   begin
                                        ObjPERFILADO_PROJ.LocalizaCodigo(fieldbyname('codigo').AsString);
                                        ObjPERFILADO_PROJ.TabelaparaObjeto;
                                        ObjPERFILADO_PROJ.Status:=dsEdit;

                                        ObjPERFILADO_PROJ.Submit_AlturaCorte(FmedidasFuros.AlturaCorte);
                                        ObjPERFILADO_PROJ.Submit_PosicaoCorte(FmedidasFuros.PosicaoCorte);
                                        ObjPERFILADO_PROJ.Submit_TamanhoCorte(FmedidasFuros.TamanhoCorte);

                                        ObjPERFILADO_PROJ.Salvar(True);

                                   end;
                              end
                              else
                              begin
                                    if(TipoMaterial='TABDIVERSO') then
                                    begin
                                         sql.Add('where DIVERSO='+CodigoMaterial);
                                         sql.Add('and projeto='+lbCodigoProjeto.Caption);
                                         Open;
                                         FmedidasFuros.PassaMaterial(TipoMaterial+'_PROJ',fieldbyname('codigo').AsString,nome,lbCodigoProjeto.Caption);
                                         FmedidasFuros.ShowModal;
                                         if(FmedidasFuros.Tag=1) then
                                         begin
                                              ObjDIVERSO_PROJ.LocalizaCodigo(fieldbyname('codigo').AsString);
                                              ObjDIVERSO_PROJ.TabelaparaObjeto;

                                              ObjDIVERSO_PROJ.Status:=dsEdit;

                                              ObjDIVERSO_PROJ.Submit_AlturaCorte(FmedidasFuros.AlturaCorte);
                                              ObjDIVERSO_PROJ.Submit_PosicaoCorte(FmedidasFuros.PosicaoCorte);
                                              ObjDIVERSO_PROJ.Submit_TamanhoCorte(FmedidasFuros.TamanhoCorte);

                                              ObjDIVERSO_PROJ.Salvar(True);

                                         end;
                                    end
                                    else exit;
                              end

                        end;

                end;


        finally
                FreeAndNil(Query);
        end;


end;

procedure TFPROJETO.lbPosicaoCorteFerragemClick(Sender: TObject);
begin
     {if(edtCodigoFerragem_Proj.Text='')
     then Exit;

     if(chk1.Checked=False) then
     begin
           MensagemErro('� preciso ligar a Ferragem em um componente antes');
           Exit;
     end;

     FmedidasFuros.PassaMaterial('TABFERRAGEM_PROJ',edtCodigoFerragem_Proj.Text,lbNomeFerragem_Proj.Caption);
     FmedidasFuros.ShowModal;
     if(FmedidasFuros.Tag=1) then
     begin
          ObjFERRAGEM_PROJ.Status:=dsEdit;
          ObjFERRAGEM_PROJ.Submit_AlturaCorte(FmedidasFuros.AlturaCorte);
          ObjFERRAGEM_PROJ.Submit_PosicaoCorte(FmedidasFuros.PosicaoCorte);
          ObjFERRAGEM_PROJ.Submit_TamanhoCorte(FmedidasFuros.TamanhoCorte);

          ObjFERRAGEM_PROJ.Salvar(True);


     end; }
end;

procedure TFPROJETO.lbPosicaoCortePerfiladoClick(Sender: TObject);
begin
    { if(edtCodigoPerfilado_Proj.Text='')
     then Exit;
     FmedidasFuros.PassaMaterial('TABPerfilado_PROJ',edtCodigoPerfilado_Proj.Text,lbNomePerfilado_Proj.Caption);
     FmedidasFuros.ShowModal;
     if(FmedidasFuros.Tag=1) then
     begin
          ObjPerfilado_PROJ.Status:=dsEdit;
          ObjPerfilado_PROJ.Submit_AlturaCorte(FmedidasFuros.AlturaCorte);
          ObjPerfilado_PROJ.Submit_PosicaoCorte(FmedidasFuros.PosicaoCorte);
          ObjPerfilado_PROJ.Submit_TamanhoCorte(FmedidasFuros.TamanhoCorte);

          ObjPerfilado_PROJ.Salvar(True);


     end;  }
end;

procedure TFPROJETO.lbPosicaoCorteDiversoClick(Sender: TObject);
begin
     { if(edtCodigoDiverso_Proj.Text='')
     then Exit;
     FmedidasFuros.PassaMaterial('TABDiverso_PROJ',edtCodigoDiverso_Proj.Text,lbNomeDiverso_Proj.Caption);
     FmedidasFuros.ShowModal;
     if(FmedidasFuros.Tag=1) then
     begin
          ObjDiverso_PROJ.Status:=dsEdit;
          ObjDiverso_PROJ.Submit_AlturaCorte(FmedidasFuros.AlturaCorte);
          ObjDiverso_PROJ.Submit_PosicaoCorte(FmedidasFuros.PosicaoCorte);
          ObjDiverso_PROJ.Submit_TamanhoCorte(FmedidasFuros.TamanhoCorte);

          ObjDiverso_PROJ.Salvar(True);


     end;   }
end;

procedure TFPROJETO.lbPosicaoFurosClick(Sender: TObject);
begin
     if(edtCodigoComponente_Proj.Text='')
     then Exit;

     FmedidasFuros.PassaMaterial('TABCOMPONENTE_PROJ',edtCodigoComponente_Proj.Text,lbNomeComponente_Proj.Caption,lbCodigoProjeto.Caption);
     FmedidasFuros.ShowModal;
     if(FmedidasFuros.Tag=1) then
     begin
         { ObjFERRAGEM_PROJ.Status:=dsEdit;
          ObjFERRAGEM_PROJ.Submit_AlturaCorte(FmedidasFuros.AlturaCorte);
          ObjFERRAGEM_PROJ.Submit_PosicaoCorte(FmedidasFuros.PosicaoCorte);
          ObjFERRAGEM_PROJ.Submit_TamanhoCorte(FmedidasFuros.TamanhoCorte);

          ObjFERRAGEM_PROJ.Salvar(True); }


     end;
end;

procedure TFPROJETO.bt1Click(Sender: TObject);
begin
      FAjuda.PassaAjuda('CADASTRO DE PROJETOS');
      FAjuda.ShowModal;
end;

{ == Rodolfo == }
procedure TFPROJETO.edtLargServExit(Sender: TObject);
var
pquantidade,paltura,plargura,plarguraarredondamento,palturaarredondamento:Currency;
begin
  { if (edtAltServ.Text <> '') and (edtLargServ.Text <> '') then
   Begin
        Try
           paltura:=strtocurr(tira_ponto(edtAltServ.Text));
        Except
              paltura:=0;
        End;

        Try
           plargura:=strtocurr(tira_ponto(edtLargServ.Text));
        Except
            plargura:=0;
        End;

        FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
        FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);

        pquantidade:=((plargura+plarguraarredondamento)*(paltura+palturaarredondamento))/1000000;
        edtquantidadeservicoproj.Text:=formata_valor(CurrToStr(pquantidade));
   end
   Else edtquantidadeservicoproj.Text:='0';  }

end;
{ === }

{Rodolfo}
procedure TFPROJETO.edtAltServExit(Sender: TObject);
begin
//     edtLargServExit(Sender);
end;

{Rodolfo}
procedure TFPROJETO.rbControlaSimClick(Sender: TObject);
begin
   rbsimm2.Checked:=False;
   rbnaom2.Checked:=True;

   edtQuantidadeServicoProj.Enabled:=False;
   edtQuantidadeServicoProj.Color:=cl3DLight;
   edtLargServ.Visible:=True;
   edtAltServ.Visible:=True;

   lblAltServ.Visible := True;
   lblLargServ.Visible := True;

   edtQuantidadeServicoProj.Left:= 172;
   lb32.Left := 172;
   edtQuantidadeServicoProj.Visible:=False;
   lb32.Visible:=False;


end;

{Rodolfo}
procedure TFPROJETO.rbControlaNaoClick(Sender: TObject);
begin
    //edtLargServ.Enabled:=False;
    //edtAltServ.Enabled:=False;
    //edtQuantidadeServicoProj.Enabled:=True;
    //edtAltServ.Color:=cl3DLight;
    //edtLargServ.Color:=cl3DLight;
    //edtQuantidadeServicoProj.Color:=clWhite;

    edtQuantidadeServicoProj.Visible:=True;
    lb32.Visible:=True;

   // edtAltServ.text := '';
   // edtLargServ.text := '';
    edtquantidadeservicoproj.Text := '';

    edtQuantidadeServicoProj.Enabled:=True;
    edtQuantidadeServicoProj.Color:=clWhite;
    edtLargServ.Visible:=False;
    edtAltServ.Visible:=False;

    lblAltServ.Visible := False;
    lblLargServ.Visible := False;

    edtQuantidadeServicoProj.Left:= 7;
    lb32.Left := 7;
end;

{Rodolfo}
procedure TFPROJETO.edtServico_ProjReferenciaClick(Sender: TObject);
begin
  auxStr := lbNomeServico_proj.Caption;
end;

{Rodolfo}
procedure TFPROJETO.edtAltServKeyPress(Sender: TObject; var Key: Char);
begin
end;

{Rodolfo}
procedure TFPROJETO.FRImposto_ICMS1btReplicarImpostosClick(
  Sender: TObject);
begin
      FRImposto_ICMS1.Button1Click(Sender);
end;

procedure TFPROJETO.Excluir1Click(Sender: TObject);
begin
    //
end;

function TFPROJETO.FinalizarProjeto:Boolean;
var
  i:Integer;
begin
   //Primeiro eu gero as linhas de medidas no projeto
   for i:=0 to ListaComponentes.Count-1 do
   begin
      GerarLinhaMedidas('componente','y',StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';')),-1);
      GerarLinhaMedidas('componente','x',StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';')),-1);
   end;
end;


procedure TFPROJETO.btTrocaClick(Sender: TObject);
begin
    if(self.Tag <> -1) then begin
        if((EdtX.Text <> '')and(EdtY.Text <> '')and(EdtAlt.Text <> '')and(EdtLarg.Text <> '')) then begin  //Se os campos dos edits nao estao vazios
            if((strtoint(EdtLarg.Text) >= 0) and (strtoint(EdtAlt.Text) >= 0))then begin   //se a largura e a altura fornecida � maior do que 6
                if((strtoint(EdtX.Text) >= 0)and(strtoint(EdtY.Text)>=0))then  begin    //se a figura esta dentro do campo de visao da tela (1 condi��o)
                    if((strtoint(EdtX.Text) <= 634)and(strtoint(EdtY.Text)<=564))then begin   //se a figura esta dentro do campo de visao da tela (2 condi��o)
                        if(TDesenho(listaFiguras[self.tag]).TipoFigura <4)then begin  //Quando n�o � uma ferragem eu posso alterar a altura e a largura
                          TDesenho(listaFiguras[self.tag]).Width := strtoint(EdtLarg.Text);
                          TDesenho(listaFiguras[self.tag]).Height := strtoint(EdtAlt.Text);
                          TDesenho(listaFiguras[self.tag]).pt1.X := strtoint(EdtX.Text);
                          TDesenho(listaFiguras[self.tag]).pt1.Y := strtoint(EdtY.Text);
                          TDesenho(listaFiguras[Self.Tag]).setBounds(TDesenho(listaFiguras[Self.Tag]).pt1.x, TDesenho(listaFiguras[Self.Tag]).pt1.y, TDesenho(listaFiguras[Self.Tag]).Width, TDesenho(listaFiguras[Self.Tag]).Height);
                          repaint;
                        end
                        else begin  //senao posso mexer apenas na ponto inicial X e inicial Y
                          TDesenho(listaFiguras[self.tag]).pt1.X := strtoint(EdtX.Text);
                          TDesenho(listaFiguras[self.tag]).pt1.Y := strtoint(EdtY.Text);
                          TDesenho(listaFiguras[Self.Tag]).setBounds(TDesenho(listaFiguras[Self.Tag]).pt1.x, TDesenho(listaFiguras[Self.Tag]).pt1.y, TDesenho(listaFiguras[Self.Tag]).Width, TDesenho(listaFiguras[Self.Tag]).Height);
                          repaint;
                        end;
                    end;
                end;
            end;
        end;
    end;
end;

procedure TFPROJETO.pnl2MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
    habilita_campos(pnl2);    
end;

procedure TFPROJETO.btVisualizarClick(Sender: TObject);
begin
    //FinalizarProjeto;
    Preenchemedidas;
    DesabilitaHabilita(False);

end;


function TFPROJETO.LocalizaPosicaoListaComponente(parametro:integer):Integer;
var
   i:Integer;
   aux:Integer;
begin
     //caso n�o encontre nada retorno -1
     result:=-1 ;

     //percorro a lista de ferragens
     for i:=0 to ListaItens.Count -1 do
     begin
         //encontro qual linha � responsavel pela ferragem em quest�o (listaitens)
         //pego a tag do componente em que ela esta (obj x / tag =x)
         if(StrToInt(retornaPalavrasAntesSimbolo(ListaItens[i],';')) = parametro)
         then  aux:=StrToInt(retornaPalavrasDepoisSimbolo2(ListaItens[i],';'));
     end;

     //Localizei a tag do componente em que a ferragem esta
     //agora preciso saber em qual linha da lista de componetes (listacomponentes) o mesmo esta
     for i:=0 to ListaComponentes.Count-1 do
     begin
         if(StrToInt(retornaPalavrasAntesSimbolo(ListaComponentes[i],';')) = aux)
         then  result:=i;
     end;
end;

function TFPROJETO.LocalizaTagCompAoCarregar(parametro:string):Integer;
var
  Query:TIBQuery;
  i:Integer;
  Nome,NomeRestante:string;
  Nome2,NomeRestante2:string;
begin
    //caso n�o encontre retorna -1
    Result:=-1;

    query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;

    try
      with Query do
      begin
          Close;
          SQL.Clear;
          SQL.Add('select nome from tabprojetospontos_id where id='+parametro);
          Open;
          //percorro a lista de obj procurando pelo nome do componente
          for i:=0 to ListBox1.Count-1 do
          begin
              //Pego o nome do componente
              NomeRestante:=retornaPalavrasDepoisSimbolo(ListBox1.Items[i],'-');
              nome:=retornaPalavrasAntesSimbolo(NomeRestante,'-');
              //Procuro pelo componente na lista de componentes
              if(Nome=FieldByName('nome').AsString) then
              begin
                   //Pego a tag do obj, por exemplo (obj 1), tag=1
                   NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox1.Items[i],' ');
                   nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');

                   Result:=StrToInt(Nome2);
              end;
          end;
      end;
    finally
      FreeAndNil(Query);
    end;

end;

function TFPROJETO.LocalizaTagListaXYREFDentroComponente(parametro:Integer;parametro2:integer):Integer;
var
  i:Integer;
  indice:string;

begin
    //encontro a linha na lista (listanomesmedidas) do componente referente a ferragem em questa�o
    for i:=1 to TDesenho(listaFiguras[parametro]).ListaNomesMedidas.Count-1 do
    begin
       indice:=RetornaPalavrasDepoisSimbolo2(retornaPalavrasDepoisSimbolo2(retornaPalavrasDepoisSimbolo2(TDesenho(listaFiguras[parametro]).ListaNomesMedidas[i],';'),';'),';');
       if(StrToInt(indice)=parametro2)
       then Result:=i;
    end;
end;

procedure TFPROJETO.GerarDesenho1Click(Sender: TObject);
var
  IMGJpg:TJPEGImage;
begin
  try
    IMGJpg:=TJPEGImage.Create;
  except
     Exit;
  end;

  try
    CarregaDesenhoProjeto(False,False);
    PaintBoxTela.Repaint;
    image5.Canvas.CopyRect(Image5.Canvas.ClipRect,PaintBoxTela.Canvas,PaintBoxTela.Canvas.ClipRect);
    IMGJpg.Assign(Image5.Picture.Graphic);
    IMGJpg.SaveToFile(ObjPROJETO.ResgataLocalDesenhos+EdtReferencia.Text+'.jpg');
    //Image5.Picture.SaveToFile(ObjPROJETO.ResgataLocalDesenhos+EdtReferencia.Text+'.bmp');
    DesabilitaHabilita(False);
  finally
    FreeAndNil(IMGJpg);
  end;

    // CarregaDesenhoProjeto(False);
end;

procedure TFPROJETO.DesabilitaHabilita(acao:Boolean);
var
  i:Integer;
  NomeRestante2,nome2:string;
begin
  //HABILITA
  if(acao=True) then
  begin
    for i:=0 to ListBox1.Count-1 do
    begin
      NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox1.Items[i],' ');
      nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
      TDesenho(listaFiguras[StrToInt(nome2)]).Enabled:=true;
      TDesenho(listaFiguras[StrToInt(nome2)]).Botao := true;
    end;
    PaintBoxTela.Enabled:=True;
    btSalvarModelagem.Enabled:=True;
    ListBox1.Enabled:=True;
  end
  else
  begin  //DESABILITA
    for i:=0 to ListBox1.Count-1 do
    begin
      NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox1.Items[i],' ');
      nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
      TDesenho(listaFiguras[StrToInt(nome2)]).Enabled:=False;
      TDesenho(listaFiguras[StrToInt(nome2)]).Botao := False ;
    end;
    PaintBoxTela.Enabled:=False;
    btSalvarModelagem.Enabled:=False;
    ListBox1.Enabled:=False;
  end;
end;

procedure TFPROJETO.btcancelaClick(Sender: TObject);
begin
   CarregaDesenhoProjeto(True,True);
   DesabilitaHabilita(False);
end;

procedure TFPROJETO.MostrarReferncias1Click(Sender: TObject);
begin
   CarregaDesenhoProjeto(True,True);
   PaintBoxTela.Repaint;
end;

procedure TFPROJETO.NoMostrarReferncias1Click(Sender: TObject);
begin
   CarregaDesenhoProjeto(False,False);
   PaintBoxTela.Repaint;
end;

procedure TFPROJETO.Puxador1Click(Sender: TObject);
var
  Nome:string;
  NomeRestanteAux:string;
  CodigoMaterial:string;
  TipoMaterial:string;
  Query:TIBQuery;
  NomeRestante2,nome2:string;
  tagcomp:integer;
begin
  //Marca ou desmarcar os puxadores
  if(ListBox1.ItemIndex<0) then
  begin
    MensagemErro('Selecione um material');
    Exit;
  end;

  if(Tag=-1)
  then Exit;

  if(lbCodigoProjeto.Caption='') or (lbCodigoProjeto.Caption='0') then
  begin
    MensagemErro('Escolha um projeto');
    Exit;
  end;
  //Pego a tag do obj, por exemplo (obj 1), tag=1
  NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox1.Items[listbox1.ItemIndex],' ');
  nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');

  NomeRestanteAux:=retornaPalavrasDepoisSimbolo(ListBox1.Items[listbox1.ItemIndex],'-');
  nome:=retornaPalavrasAntesSimbolo(NomeRestanteAux,'-');
  NomeRestanteAux:=retornaPalavrasDepoisSimbolo(NomeRestanteAux,'-');
  TipoMaterial:= retornaPalavrasAntesSimbolo(NomeRestanteAux,'-');
  NomeRestanteAux:=retornaPalavrasDepoisSimbolo(NomeRestanteAux,'-');
  CodigoMaterial:= retornaPalavrasAntesSimbolo(NomeRestanteAux,'-');

  tagcomp:=LocalizaPosicaonoComponente;

  if(TipoMaterial<>'TABCOMPONENTE')then
  begin
     GerarLinhaMedidas('puxador','y',tagcomp,StrToInt(nome2));
     GerarLinhaMedidas('puxador','x',tagcomp,StrToInt(nome2))
  end
  else MensagemAviso('Voc� esta tentando marcar um componente como puxador!');

end;

procedure TFPROJETO.Preenchemedidas;
var
  i:Integer;
  NomeRestante2,nome2:string;
  AlongFont:TLogFont;
  Hfonte:THandle;
begin
  PaintBoxTela.Font.Color:=clBlack;
  PaintBoxTela.Font.Size:=6;
  for i:=0 to ListBox1.Count-1 do
  begin
    NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox1.Items[i],' ');
    nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
    if(TDesenho(listaFiguras[StrToInt(nome2)]).TipoFigura=21)then
    begin
      if((TDesenho(listaFiguras[StrToInt(nome2)]).pt2.x-TDesenho(listaFiguras[StrToInt(nome2)]).pt1.x)>1)then
      begin
        PaintBoxTela.Canvas.TextOut(TDesenho(listaFiguras[StrToInt(nome2)]).pt1.x,TDesenho(listaFiguras[StrToInt(nome2)]).pt2.y+10,'Largura');
      end;

      if((TDesenho(listaFiguras[StrToInt(nome2)]).pt2.y-TDesenho(listaFiguras[StrToInt(nome2)]).pt1.y)>1)then
      begin
        {AlongFont.lfHeight:=PaintBoxTela.Font.Height;
        AlongFont.lfWidth:=0;
        AlongFont.lfEscapement:=-450;
        AlongFont.lfOrientation:=-450;
        AlongFont.lfWeight:=FW_DEMIBOLD;
        AlongFont.lfCharSet:=ANSI_CHARSET;
        AlongFont.lfOutPrecision:=OUT_DEFAULT_PRECIS;
        AlongFont.lfClipPrecision:=OUT_DEFAULT_PRECIS;
        AlongFont.lfQuality:=DEFAULT_QUALITY;
        AlongFont.lfPitchAndFamily:=DEFAULT_PITCH;
        StrCopy(AlongFont.lfFaceName,pchar(PaintBoxTela.Font.Name));
        hFont:=CreateFontIndirect(AlongFont);
        PaintBoxTela.Font.Handle := hFont ;}
        PaintBoxTela.Canvas.TextOut(TDesenho(listaFiguras[StrToInt(nome2)]).pt1.x,TDesenho(listaFiguras[StrToInt(nome2)]).pt2.y-50,'Altura');
      end;
    end;
  end;
end;

procedure TFPROJETO.MeioCirculo1Click(Sender: TObject);
begin
   //trecho que desenha meia esfera
   op := 22;
   Self.Tag := -1;
   botao := false;
end;

procedure TFPROJETO.ModelosdeFerragens1Click(Sender: TObject);
begin
   Form9.Top := Self.Top  + 565;
   Form9.Left := Self.Left + 325;
   Form9.Show;
   ferrag := true;
end;

procedure TFPROJETO.C1Click(Sender: TObject);
begin
   op := 2;
   Self.Tag := -1;
   botao := false;
end;

procedure TFPROJETO.Retangulo1Click(Sender: TObject);
begin
   op := 1;
   Self.Tag := -1;
   botao := false;
end;

procedure TFPROJETO.DesenharLinha1Click(Sender: TObject);
begin
   if (polyclick = true) then
   begin
      polyclick := false;
   end
   else
   begin
      polyclick := true;
      op := 20;
   end;
   indCurr:=0;
end;

procedure TFPROJETO.LigarMaterial1Click(Sender: TObject);
begin
   MenuItem4Click(Sender);
end;

function TFPROJETO.RetornaLinhaReferentetTagOBj:Integer;
var
  i:Integer;
  NomeRestante2,nome2:string;
begin
  Result:=-1; 
  for i:=0 to ListBox1.Count-1 do
  begin
      NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox1.Items[i],' ');
      nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
      if(Nome2=IntToStr(Self.Tag))
      then result:=i;
  end;

end;

procedure TFPROJETO.N14deumcrculoesquerda1Click(Sender: TObject);
begin
   //trecho que desenha 1/4 da esfera
   op := 23;
   Self.Tag := -1;
   botao := false;
end;

procedure TFPROJETO.N14deumcrculodireita1Click(Sender: TObject);
begin
   //trecho que desenha 1/4 da esfera
   op := 24;
   Self.Tag := -1;
   botao := false;
end;

procedure TFPROJETO.btAjudaVideoClick(Sender: TObject);
var
   FVideosAjuda:TFvideos ;
begin
    FvideosAjuda:= TFvideos.Create(nil);
    try
      FVideosAjuda.PegaLink('','CADASTRO DE PROJETOS');
      FVideosAjuda.ShowModal;
    finally
      FreeAndNil(FVideosAjuda);
    end;

end;

procedure TFPROJETO.MostraServicosProducao;
var
  SelectServicosProducao_QUERY:TIBQuery;
begin
  SelectServicosProducao_QUERY:=TIBQuery.Create(nil);
  SelectServicosProducao_QUERY.Database:=FDataModulo.IBDatabase;

  try
    with STRGLinhaServicos do
    begin
      ColCount:=1;
      RowCount:=3;
      ColWidths[0] := 128;

      RowHeights[1]:=40;
      RowHeights[2]:=0;

      Cells[0,0] := '';


      SelectServicosProducao_QUERY.Close;
      SelectServicosProducao_QUERY.SQL.Clear;
      SelectServicosProducao_QUERY.SQL.Text:=
      'select tabservicoproducao_proj.codigo,projeto,servico,ordem,referencia,tabservico.descricao,tabservicoproducao_proj.listadecomponentes from tabservicoproducao_proj'+
      ' join  tabservico on tabservico.codigo=tabservicoproducao_proj.servico'+
      ' where tabservicoproducao_proj.projeto='+lbCodigoProjeto.Caption+
      ' order by tabservicoproducao_proj.ordem';

      SelectServicosProducao_QUERY.Open;

      while not SelectServicosProducao_QUERY.Eof do
      begin
          STRGLinhaServicos.Cells[SelectServicosProducao_QUERY.RecordCount-1,0]:= SelectServicosProducao_QUERY.Fields[4].AsString;
          STRGLinhaServicos.Cells[SelectServicosProducao_QUERY.RecordCount-1,2]:= SelectServicosProducao_QUERY.Fields[6].AsString;
          SelectServicosProducao_QUERY.Next;

          ColCount:=ColCount+1;
          ColWidths[ColCount-1] := 128;
      end;
      ColCount:=ColCount-1;

    end;
  finally
    FreeAndNil(SelectServicosProducao_QUERY);
  end;
end;

procedure TFPROJETO.edtServicoProducaoDblClick(Sender: TObject);
var
   Key:Word;
   Shift:TShiftState;
begin

end;

procedure TFPROJETO.GravaServicoProducao;
begin

end;

procedure TFPROJETO.PaintBox1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
  var
  ARect : TRect;
begin
  __MarcaServicoPorComponente;
end;

procedure TFPROJETO.PaintBox1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  var
  ARect : TRect;  
begin
  if(op<>25)
  then Exit;
  pInic := Point(X,Y+20);
  pFim := Point(X+30,y+50);
  CriarFiguraIMGProducao(IntToStr(STRGLinhaServicos.col+1),'','');
  PaintBox1.Repaint;
  op:=0;
end;

procedure TFPROJETO.STRGLinhaServicosDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
  if((ARow = 1))
  then begin
    if(STRGLinhaServicos.Cells[ACol,0] <> '' ) then
    begin
      Il1.Draw(STRGLinhaServicos.Canvas,Rect.Left ,Rect.Top,ACol+1);
    end
    else
    begin
      Il1.Draw(STRGLinhaServicos.Canvas, Rect.Left , Rect.Top+1, 0); //check box sem marcar    5
    end;
  end;
end;

procedure TFPROJETO.STRGLinhaServicosDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Fservico:TFSERVICO;

begin
  if(STRGLinhaServicos.Col<>0)then
  begin
        if( STRGLinhaServicos.Cells[STRGLinhaServicos.Col-1,0]='')
        then Exit
  end;
  op := 0;
  if(STRGLinhaServicos.Row=0)
  then Exit;
  Try
    Fpesquisalocal:=Tfpesquisa.create(Nil);
    Fservico:=TFSERVICO.Create(nil);
    If (FpesquisaLocal.PreparaPesquisa('select referencia,descricao,codigo from tabservico where ativo =''S'' ','',Fservico)=True)
    Then Begin
      Try
        If (FpesquisaLocal.showmodal=mrok) Then
        Begin
          STRGLinhaServicos.Cells[STRGLinhaServicos.Col,0] :=FpesquisaLocal.QueryPesq.Fields[0].AsString;
          if(STRGLinhaServicos.ColCount=(STRGLinhaServicos.Col+1)) then
          begin
            STRGLinhaServicos.ColCount:=STRGLinhaServicos.ColCount+1;
            STRGLinhaServicos.ColWidths[STRGLinhaServicos.ColCount-1] := 128;
          end;
        End;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;
  Finally
    FreeandNil(FPesquisaLocal);
    FreeAndNil(Fservico);
  End;
end;


procedure TFPROJETO.STRGLinhaServicosClick(Sender: TObject);
begin
  op := 25;
  Self.Tag := -1;
  botao := false;
  if(STRGLinhaServicos.Cells[STRGLinhaServicos.Col,0]<>'') then
  begin
      lbNomedoservicoprod.Caption:=get_campoTabela('descricao','referencia','tabservico',STRGLinhaServicos.Cells[STRGLinhaServicos.Col,0]);
  end
  else lbNomedoservicoprod.Caption:='';
end;

procedure TFPROJETO.PaintBox1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FIsButtonDown[Button] := False;
end;

procedure TFPROJETO.btADDServicosClick(Sender: TObject);
begin
  STRGLinhaServicos.ColCount:=STRGLinhaServicos.ColCount+1;
  STRGLinhaServicos.ColWidths[STRGLinhaServicos.ColCount-1]:=128;
end;

procedure TFPROJETO.STRGLinhaServicosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Button: TMouseButton;
begin
  if(Key=46)then
  begin
     if(STRGLinhaServicos.Col< STRGLinhaServicos.ColCount) then
     begin
        if (STRGLinhaServicos.Cells[STRGLinhaServicos.Col+1,0]<>'')
        then Exit;

        STRGLinhaServicos.Cells[STRGLinhaServicos.Col,0] :='';
        if(STRGLinhaServicos.Col<>0) and (STRGLinhaServicos.ColCount>1)
        then STRGLinhaServicos.ColCount:=STRGLinhaServicos.ColCount-1;
        STRGLinhaServicos.Refresh;
     end;
  end;
  if(Key=13) then
  begin
    STRGLinhaServicosDblClick(Sender);
    STRGLinhaServicos.SetFocus;
  end;
  if(key=37) or (key=38) or (key=39) or (key=40)
  then FIsButtonDown[Button] := False;

end;

procedure TFPROJETO.STRGLinhaServicosMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
   FIsButtonDown[Button] := True;
end;

procedure TFPROJETO.btSalvarServicosProducaoClick(Sender: TObject);
var
  QryServicoproducaoproj_QUERY:TIBQuery;
  i:Integer;
begin
  QryServicoproducaoproj_QUERY:=TIBQuery.Create(nil);
  QryServicoproducaoproj_QUERY.Database:=FDataModulo.IBDatabase;
  try
    QryServicoproducaoproj_QUERY.Close;
    QryServicoproducaoproj_QUERY.SQL.Clear;
    QryServicoproducaoproj_QUERY.SQL.Text :=
    'delete from tabservicoproducao_proj where projeto='+lbCodigoProjeto.Caption;

    try
       QryServicoproducaoproj_QUERY.ExecSQL;
    except
       FDataModulo.IBTransaction.RollbackRetaining;
       exit;
    end;
    __GravaServicosPorComponentes;
    for i:=0 to STRGLinhaServicos.ColCount-1 do
    begin
      if(STRGLinhaServicos.cells[i,0] <>'') then
      begin
        QryServicoproducaoproj_QUERY.Close;
        QryServicoproducaoproj_QUERY.SQL.Clear;
        QryServicoproducaoproj_QUERY.SQL.Text:=
        'INSERT INTO TABSERVICOPRODUCAO_PROJ (CODIGO,PROJETO,SERVICO,ORDEM,LISTADECOMPONENTES) '+
        'VALUES ('+'0,'+lbCodigoProjeto.Caption+','+get_campoTabela('codigo','referencia','tabservico',STRGLinhaServicos.cells[i,0])+','+IntToStr(i)+','+#39+STRGLinhaServicos.cells[i,2]+#39+')';
        try
           QryServicoproducaoproj_QUERY.ExecSQL;
        except
           FDataModulo.IBTransaction.RollbackRetaining;
           Exit;
        end;
      end;
    end;
    FDataModulo.IBTransaction.CommitRetaining;
  finally
    FreeAndNil(QryServicoproducaoproj_QUERY);
    MostraServicosProducao;
    lbNomedoservicoprod.Caption:='';
  end;
end;

procedure TFPROJETO.btCancelarModServClick(Sender: TObject);
begin
  lbNomedoservicoprod.Caption:='';
  MostraServicosProducao;
end;

procedure TFPROJETO.__MarcaServicoPorComponente;
var
  tmp: integer;
  str : string;
  parte : TStringList;
  TagComponente:Integer;
  pos:Integer;
  VerificaIgual:Boolean;
begin
  if(Tag=-1)
  then Exit;
  if(TDesenho(listaFigurasParaProducao[Self.Tag]).TipoFigura=25) then
  begin
    TagComponente:=LocalizaPosicaonoComponenteProducao;
    if(TagComponente=-1) then
    begin
      pos := TDesenho (listaFigurasParaProducao [listaFigurasParaProducao.IndexOf(listaFigurasParaProducao [Self.Tag])]).ContFig;
      TDesenho (listaFigurasParaProducao [listaFigurasParaProducao.IndexOf(listaFigurasParaProducao [Self.Tag])]).Free;
      RetiraListaProducao(pos);
      contProducao:=contProducao-1;
      __RetiraServicoProducaodaLista;
    end
    else
    begin
      if(___VerificaDuplicidadePorComponente_Servicos(IntToStr(TagComponente),VerificaIgual)=False)  then
      begin
        if(VerificaIgual=False)
        then ListaServicosProducao.Add(IntToStr(Tag)+';'+TDesenho(listaFigurasParaProducao[Self.Tag]).NomeFigura+';'+IntToStr(TagComponente))
      end
      else
      begin
        pos := TDesenho (listaFigurasParaProducao [listaFigurasParaProducao.IndexOf(listaFigurasParaProducao [Self.Tag])]).ContFig;
        TDesenho (listaFigurasParaProducao [listaFigurasParaProducao.IndexOf(listaFigurasParaProducao [Self.Tag])]).Free;
        RetiraListaProducao(pos);
        contProducao:=contProducao-1;
        __RetiraServicoProducaodaLista;
      end;
    end;
    Tag:=-1;
  end; 
end;

procedure TFPROJETO.__RetiraServicoProducaodaLista;
var
  i:integer;
begin
  for i:=0 to ListaServicosProducao.Count-1 do
  begin
    if(retornaPalavrasAntesSimbolo(ListaServicosProducao[i],';')=IntToStr(Tag)) then
    begin
      ListaServicosProducao.Delete(i);
      Exit;
    end;
  end;
end;

function TFPROJETO.___VerificaDuplicidadePorComponente_Servicos(TagComponente:string;var paramaetroVerificaIgual:Boolean):Boolean;
var
  i:Integer;
  TagComp:string;
  TagServico:string;
begin
  Result:=False;
  paramaetroVerificaIgual:=False;
  for i:=0 to ListaServicosProducao.Count-1 do
  begin
    TagServico:=retornaPalavrasAntesSimbolo(retornaPalavrasDepoisSimbolo2(ListaServicosProducao[i],';'),';');
    TagComp:=retornaPalavrasDepoisSimbolo2(retornaPalavrasDepoisSimbolo2(ListaServicosProducao[i],';'),';');

    //Verifico se estou marcando um servi�o no componente que j� esta marcado
    if(TagComp=TagComponente) And (TDesenho(listaFigurasParaProducao[Self.Tag]).NomeFigura=TagServico) then
    begin
      //se � o mesmo marcador ent�o n�o estou criando um novo marcador de servi�o e sim movendo dentro do componente
      if(TDesenho(listaFigurasParaProducao[Self.Tag]).ContFig=TDesenho(listaFigurasParaProducao[StrToInt(retornaPalavrasAntesSimbolo(ListaServicosProducao[i],';'))]).ContFig)
      then paramaetroVerificaIgual:=True
      else
      begin
        Result:=True;
        Exit;
      end;
    end;
  end;
end;

procedure TFPROJETO.__GravaServicosPorComponentes;
var
  i:integer;
  TagServico:integer;
  QryInsert_QUERY : TIBQuery;  //declara��o da query
  QryDeleteSelect_QUERY : TIBQuery;
begin
  for i:=0 to STRGLinhaServicos.ColCount do
  begin
     STRGLinhaServicos.Cells[i,2]:='';
  end;

  for i:=0 to ListaServicosProducao.Count-1 do
  begin
       TagServico:=StrToInt(retornaPalavrasAntesSimbolo(retornaPalavrasDepoisSimbolo2(ListaServicosProducao[i],';'),';'));
       STRGLinhaServicos.Cells[TagServico-1,2]:= STRGLinhaServicos.Cells[TagServico-1,2]+ TDesenho(listaFigurasParaProducao[StrToInt(retornaPalavrasDepoisSimbolo2(retornaPalavrasDepoisSimbolo2(ListaServicosProducao[i],';'),';'))]).ListaNomesMedidas[0]+';';
  end;
  try
    QryInsert_QUERY := TIBQuery.Create(nil);
    QryInsert_QUERY.Database := FDataModulo.IBDatabase;
    QryDeleteSelect_QUERY := TIBQuery.Create(nil);
    QryDeleteSelect_QUERY.Database := FDataModulo.IBDatabase;
  except
    Exit;
  end;

  try
    QryInsert_QUERY.Close;
    QryInsert_QUERY.SQL.Clear;
    QryInsert_QUERY.SQL.Text:=
    'INSERT INTO TABSERVICOSPRODUCAO_DESENHO (TIPO,COR,XSUP,YSUP,XINF,YINF,NOME,PROJETO_ID,REFERENCIA) '+
    'VALUES (:TIPO,:COR,:XSUP,:YSUP,:XINF,:YINF,:NOME,:PROJETO_ID,:REFERENCIA) ';

    QryDeleteSelect_QUERY.Close;
    QryDeleteSelect_QUERY.SQL.Clear;
    QryDeleteSelect_QUERY.SQL.Text:=
    'DELETE FROM TABSERVICOSPRODUCAO_DESENHO WHERE PROJETO_ID= '+lbCodigoProjeto.Caption;
    try
      QryDeleteSelect_QUERY.ExecSQL;
    except
      FDataModulo.IBTransaction.RollbackRetaining;
      Exit;
    end;

    for i:=0 to ListaServicosProducao.count-1 do
    begin
      QryInsert_QUERY.Params[0].AsString := IntToStr(TDesenho(listaFigurasParaProducao[StrToInt(retornaPalavrasAntesSimbolo(ListaServicosProducao[i],';'))]).TipoFigura);
      QryInsert_QUERY.Params[1].AsString := ColorToString(TDesenho(listaFigurasParaProducao[StrToInt(retornaPalavrasAntesSimbolo(ListaServicosProducao[i],';'))]).Color);
      QryInsert_QUERY.Params[2].AsString := IntToStr(TDesenho(listaFigurasParaProducao[StrToInt(retornaPalavrasAntesSimbolo(ListaServicosProducao[i],';'))]).pt1.X);
      QryInsert_QUERY.Params[3].AsString := IntToStr(TDesenho(listaFigurasParaProducao[StrToInt(retornaPalavrasAntesSimbolo(ListaServicosProducao[i],';'))]).pt1.Y);
      QryInsert_QUERY.Params[4].AsString := IntToStr(TDesenho(listaFigurasParaProducao[StrToInt(retornaPalavrasAntesSimbolo(ListaServicosProducao[i],';'))]).pt2.X);
      QryInsert_QUERY.Params[5].AsString := IntToStr(TDesenho(listaFigurasParaProducao[StrToInt(retornaPalavrasAntesSimbolo(ListaServicosProducao[i],';'))]).pt2.Y);
      QryInsert_QUERY.Params[6].AsString := retornaPalavrasDepoisSimbolo2(retornaPalavrasDepoisSimbolo2(ListaServicosProducao[i],';'),';');
      QryInsert_QUERY.Params[7].AsString := lbCodigoProjeto.Caption;
      QryInsert_QUERY.Params[8].AsString := TDesenho(listaFigurasParaProducao[StrToInt(retornaPalavrasAntesSimbolo(ListaServicosProducao[i],';'))]).NomeFigura;
      try
        QryInsert_QUERY.ExecSQL;
      except
        FDataModulo.IBTransaction.RollbackRetaining;
        Exit;
      end;
    end;

    FDataModulo.IBTransaction.CommitRetaining;

  finally
    FreeAndNil(QryInsert_QUERY);
    FreeAndNil(QryDeleteSelect_QUERY);
  end;
end;

procedure TFPROJETO.__CarregarMarcadoresServicos;
var
  QryCarregaMarcadores_QUERY:TIBQuery;
  k:Integer;
begin
  try
    QryCarregaMarcadores_QUERY:=TIBQuery.Create(nil);
    QryCarregaMarcadores_QUERY.Database:=FDataModulo.IBDatabase;
  except
    Exit;
  end;
  ListaServicosProducao.Clear;
  try
    QryCarregaMarcadores_QUERY.Close;
    QryCarregaMarcadores_QUERY.SQL.Clear;
    QryCarregaMarcadores_QUERY.SQL.Text:=
    'SELECT TIPO,COR,XSUP,YSUP,XINF,YINF,NOME,PROJETO_ID,REFERENCIA FROM TABSERVICOSPRODUCAO_DESENHO '+
    'WHERE PROJETO_ID='+lbCodigoProjeto.Caption;
    
    QryCarregaMarcadores_QUERY.Open;
    while not QryCarregaMarcadores_QUERY.Eof do
    begin
      op      := QryCarregaMarcadores_QUERY.Fields[0].AsInteger;  //tipo do objeto
      cor     := QryCarregaMarcadores_QUERY.Fields[1].AsInteger;
      pInic.X := QryCarregaMarcadores_QUERY.Fields[2].AsInteger;  //xsuperior  do objeto
      pInic.Y := QryCarregaMarcadores_QUERY.Fields[3].AsInteger;  //ysuperior do objeto

      if(op <> 3)then
      begin
        pFim.X := QryCarregaMarcadores_QUERY.Fields[4].AsInteger;   //xinferior do objeto
        pFim.Y := QryCarregaMarcadores_QUERY.Fields[5].AsInteger;   //yinferior do objeto
      end;
      CriarFiguraIMGProducao(QryCarregaMarcadores_QUERY.Fields[8].AsString,'','');
      pux_banc := false;

      ListaServicosProducao.Add(IntToStr(TDesenho(listaFigurasParaProducao[listaFigurasParaProducao.Count-1]).ContFig)+';'+QryCarregaMarcadores_QUERY.Fields[8].AsString+';'+QryCarregaMarcadores_QUERY.Fields[6].AsString);
      
      QryCarregaMarcadores_QUERY.Next;
    end;
  finally
    FreeAndNil(QryCarregaMarcadores_QUERY);
  end;
end;

procedure TFPROJETO.__LimpaStringGrid;
var
  i:Integer;
begin
  for i:=0 to STRGLinhaServicos.ColCount-1 do
  begin
    STRGLinhaServicos.Cells[i,0]:='';
    STRGLinhaServicos.Cells[i,1]:='';
    STRGLinhaServicos.Cells[i,2]:='';
  end;
end;

procedure TFPROJETO.STRGLinhaServicosMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FIsButtonDown[Button] := True;
end;

procedure TFPROJETO.Puxadores1Click(Sender: TObject);
begin
  op := 26;
  Self.Tag := -1;
  botao := false;
end;

procedure TFPROJETO.btMenuDesenhosAddClick(Sender: TObject);
begin
  panel4.Visible:= not panel4.Visible;
  if(btMenuDesenhosAdd.Caption='g')
  then btMenuDesenhosAdd.Caption:='f'
  else btMenuDesenhosAdd.Caption:='g';
end;

procedure TFPROJETO.btPuxadoresClick(Sender: TObject);
begin
  op := 26;
  Self.Tag := -1;
  botao := false;
end;

procedure TFPROJETO.bt1por4EsquerdaClick(Sender: TObject);
begin
   //trecho que desenha 1/4 da esfera
   op := 23;
   Self.Tag := -1;
   botao := false;
end;

procedure TFPROJETO.bt1po4direitoClick(Sender: TObject);
begin
   //trecho que desenha 1/4 da esfera
   op := 24;
   Self.Tag := -1;
   botao := false;
end;

procedure TFPROJETO.btMeioCirucloClick(Sender: TObject);
begin
   //trecho que desenha meia esfera
   op := 22;
   Self.Tag := -1;
   botao := false;
end;

procedure TFPROJETO.btLigarMaterialClick(Sender: TObject);
begin
   MenuItem4Click(Sender);
end;

procedure TFPROJETO.ComboEsquadriaKeyPress(Sender: TObject; var Key: Char);
begin
  Key:=#0;
end;

procedure TFPROJETO.chkesquadriadiversoClick(Sender: TObject);
begin
    if(chkesquadriadiverso.Checked=True)then
    begin
      edtQuantidadeDiverso_proj.Visible:=True;
      edtLarguraDiverso_proj.Visible:=True;
      edtAlturaDiverso_Proj.Visible:=True;

      lbLarguraDiverso_Proj.Visible:=True;
      lbAlturaDiverso_Proj.Visible:=True;
      lb19.Visible:=True;
    end
    else
    begin
      edtQuantidadeDiverso_proj.Visible:=False;
      edtLarguraDiverso_proj.Visible:=False;
      edtAlturaDiverso_Proj.Visible:=False;

      lbLarguraDiverso_Proj.Visible:=False;
      lbAlturaDiverso_Proj.Visible:=False;
      lb19.Visible:=False;
    end;
end;

procedure TFPROJETO.chkesquadriaperfiladoClick(Sender: TObject);
begin
    {if(chkesquadriaperfilado.Checked=True)then
    begin
      edtquantidade.Visible:=True;
      lb38.Visible:=True;
    end
    else
    begin
      edtquantidade.Visible:=False;
      lb38.Visible:=False;
    end;}
end;

procedure TFPROJETO.rbsimm2Click(Sender: TObject);
begin
    rbControlaSim.Checked:=False;
    rbControlaNao.Checked:=True;

    edtQuantidadeServicoProj.Enabled:=False;
    edtQuantidadeServicoProj.Color:=cl3DLight;
    edtLargServ.Visible:=True;
    edtAltServ.Visible:=True;

    lblAltServ.Visible := True;
    lblLargServ.Visible := True;

    edtQuantidadeServicoProj.Left:= 172;
    lb32.Left := 172;
    edtQuantidadeServicoProj.Visible:=False;
    lb32.Visible:=False;


end;

procedure TFPROJETO.rbnaom2Click(Sender: TObject);
begin
    edtQuantidadeServicoProj.Visible:=True;
    lb32.Visible:=True;
    edtquantidadeservicoproj.Text := '';

    edtQuantidadeServicoProj.Enabled:=True;
    edtQuantidadeServicoProj.Color:=clWhite;
    edtLargServ.Visible:=False;
    edtAltServ.Visible:=False;

    lblAltServ.Visible := False;
    lblLargServ.Visible := False;

    edtQuantidadeServicoProj.Left:= 7;
    lb32.Left := 7;
end;

procedure TFPROJETO.edtNCMKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  FDataModulo.edtNCMKeyDown(Sender,Key,Shift);
end;

procedure TFPROJETO.edtCestKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if Key=vk_f9 then
  begin
    Clipboard.AsText := EDTNCM.Text;
    With TObjCEST.Create do
    begin
      edtCESTkeydown(sender,Key,shift);
      Free;
    end;
  end;
end;

end.



