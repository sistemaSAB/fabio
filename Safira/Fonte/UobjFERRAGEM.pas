unit UobjFERRAGEM;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJGRUPOFERRAGEM
,UOBJFORNECEDOR,UOBJTABELAA_ST,UOBJTABELAB_ST,UMostraBarraProgresso;

Type
   TObjFERRAGEM=class

          Public
                Status                                      :TDataSetState;
                ObjDataSource                               :TDataSource;
                SqlInicial                                  :String[200];
                ReferenciaAnterior                          :string;

                GrupoFerragem:TOBJGRUPOFERRAGEM;
                Fornecedor:TOBJFORNECEDOR;
                SituacaoTributaria_TabelaA:TOBJTABELAA_ST ;
                SituacaoTributaria_TabelaB:TOBJTABELAB_ST ;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaReferencia(Parametro:string) :boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_Pesquisa2                   :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure  ZerarTabela;
                Procedure  Cancelar;
                Procedure  Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Referencia(parametro: string);
                Function Get_Referencia: string;
                Procedure Submit_Descricao(parametro: string);
                Function Get_Descricao: string;
                Procedure Submit_Unidade(parametro: string);
                Function Get_Unidade: string;
                Procedure Submit_Peso(parametro: string);
                Function Get_Peso: string;
                Procedure Submit_PrecoCusto(parametro: string);
                Function Get_PrecoCusto: string;
                Procedure Submit_PorcentagemInstalado(parametro: string);
                Function Get_PorcentagemInstalado: string;
                Procedure Submit_PorcentagemFornecido(parametro: string);
                Function Get_PorcentagemFornecido: string;
                Procedure Submit_PorcentagemRetirado(parametro: string);
                Function Get_PorcentagemRetirado: string;

                Procedure Submit_percentualagregado(parametro: string);
                Function Get_percentualagregado: string;

                Procedure Submit_ipi(parametro: string);
                Function Get_ipi: string;


                Function Get_PrecoVendaInstalado: string;
                Function Get_PrecoVendaFornecido: string;
                Function Get_PrecoVendaRetirado: string;

                Procedure Submit_PrecoVendaInstalado(parametro: string);
                Procedure Submit_PrecoVendaFornecido(parametro: string);
                Procedure Submit_PrecoVendaRetirado(parametro: string);

                Procedure Submit_IsentoICMS_Estado(parametro: string);
                Function Get_IsentoICMS_Estado: string;
                Procedure Submit_SubstituicaoICMS_Estado(parametro: string);
                Function Get_SubstituicaoICMS_Estado: string;
                Procedure Submit_ValorPauta_Sub_Trib_Estado(parametro: string);
                Function Get_ValorPauta_Sub_Trib_Estado: string;
                Procedure Submit_Aliquota_ICMS_Estado(parametro: string);
                Function Get_Aliquota_ICMS_Estado: string;
                Procedure Submit_Reducao_BC_ICMS_Estado(parametro: string);
                Function Get_Reducao_BC_ICMS_Estado: string;
                Procedure Submit_Aliquota_ICMS_Cupom_Estado(parametro: string);
                Function Get_Aliquota_ICMS_Cupom_Estado: string;
                Procedure Submit_IsentoICMS_ForaEstado(parametro: string);
                Function Get_IsentoICMS_ForaEstado: string;
                Procedure Submit_SubstituicaoICMS_ForaEstado(parametro: string);
                Function Get_SubstituicaoICMS_ForaEstado: string;
                Procedure Submit_ValorPauta_Sub_Trib_ForaEstado(parametro: string);
                Function Get_ValorPauta_Sub_Trib_ForaEstado: string;
                Procedure Submit_Aliquota_ICMS_ForaEstado(parametro: string);
                Function Get_Aliquota_ICMS_ForaEstado: string;
                Procedure Submit_Reducao_BC_ICMS_ForaEstado(parametro: string);
                Function Get_Reducao_BC_ICMS_ForaEstado: string;
                Procedure Submit_Aliquota_ICMS_Cupom_ForaEstado(parametro: string);
                Function Get_Aliquota_ICMS_Cupom_ForaEstado: string;
                Procedure Submit_ClassificacaoFiscal(parametro: string);
                Function Get_ClassificacaoFiscal: string;



                procedure EdtGrupoFerragemExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtGrupoFerragemKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtGrupoFerragemKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;

                procedure EdtFerragemKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
                procedure EdtFerragemKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;



                procedure EdtFornecedorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFornecedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                procedure EdtSituacaoTributaria_TabelaAExit(Sender: TObject);
                procedure EdtSituacaoTributaria_TabelaAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure EdtSituacaoTributaria_TabelaBExit(Sender: TObject);
                procedure EdtSituacaoTributaria_TabelaBKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);

                Function  VerificaReferencia(PStatus:TDataSetState; PCodigo,PReferencia:string):Boolean; {Rodolfo}
                Procedure  AtualizaMargens(PGrupoFerragem:string);
                Procedure AtualizaPrecos(PGrupoFerrragem:string);

                function PrimeiroRegistro: Boolean;
                function ProximoRegisto(PCodigo: Integer): Boolean;
                function RegistoAnterior(PCodigo: Integer): Boolean;
                function UltimoRegistro: Boolean;
                procedure Reajusta(PIndice, Pgrupo: string);

                procedure Submit_NCM(parametro:string);
                function Get_NCM:String;

                procedure Submit_cest(parametro:string);
                function Get_cest:String;


                procedure Submit_Ativo(parametro:string);
                Function Get_Ativo:string;

                function AtualizaTributospeloNCM:Boolean;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Referencia:string;
               Descricao:string;
               Unidade:string;
               Peso:string;
               PrecoCusto:string;
               PorcentagemInstalado:string;
               PorcentagemFornecido:string;
               PorcentagemRetirado:string;
               PrecoVendaInstalado:string;
               PrecoVendaFornecido:string;
               PrecoVendaRetirado:string;

               percentualagregado:string;
               ipi:string;

               IsentoICMS_Estado:string;
               SubstituicaoICMS_Estado:string;
               ValorPauta_Sub_Trib_Estado:string;
               Aliquota_ICMS_Estado:string;
               Reducao_BC_ICMS_Estado:string;
               Aliquota_ICMS_Cupom_Estado:string;
               IsentoICMS_ForaEstado:string;
               SubstituicaoICMS_ForaEstado:string;
               ValorPauta_Sub_Trib_ForaEstado:string;
               Aliquota_ICMS_ForaEstado:string;
               Reducao_BC_ICMS_ForaEstado:string;
               Aliquota_ICMS_Cupom_ForaEstado:string;
               ClassificacaoFiscal:string;
               NCM:string;
               ATIVO:string;
               CEST:string;


               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
  UFORNECEDOR, UMenuRelatorios,
  Forms;
{ TTabTitulo }


Function  TObjFERRAGEM.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Referencia:=fieldbyname('Referencia').asstring;
        If(FieldByName('GrupoFerragem').asstring<>'')
        Then Begin
                 If (Self.GrupoFerragem.LocalizaCodigo(FieldByName('GrupoFerragem').asstring)=False)
                 Then Begin
                          Messagedlg('Grupo Ferragem N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.GrupoFerragem.TabelaparaObjeto;
        End;
        Self.Descricao:=fieldbyname('Descricao').asstring;
        Self.Unidade:=fieldbyname('Unidade').asstring;
        If(FieldByName('Fornecedor').asstring<>'')
        Then Begin
                 If (Self.Fornecedor.LocalizaCodigo(FieldByName('Fornecedor').asstring)=False)
                 Then Begin
                          Messagedlg('Fornecedor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Fornecedor.TabelaparaObjeto;
        End;
        Self.Peso:=fieldbyname('Peso').asstring;
        Self.PrecoCusto:=fieldbyname('PrecoCusto').asstring;
        Self.PorcentagemInstalado:=fieldbyname('PorcentagemInstalado').asstring;
        Self.PorcentagemFornecido:=fieldbyname('PorcentagemFornecido').asstring;
        Self.PorcentagemRetirado:=fieldbyname('PorcentagemRetirado').asstring;
        Self.PrecoVendaInstalado:=fieldbyname('PrecoVendaInstalado').asstring;
        Self.PrecoVendaFornecido:=fieldbyname('PrecoVendaFornecido').asstring;
        Self.PrecoVendaRetirado:=fieldbyname('PrecoVendaRetirado').asstring;

        Self.IsentoICMS_Estado:=fieldbyname('IsentoICMS_Estado').asstring;
        Self.SubstituicaoICMS_Estado:=fieldbyname('SubstituicaoICMS_Estado').asstring;
        Self.ValorPauta_Sub_Trib_Estado:=fieldbyname('ValorPauta_Sub_Trib_Estado').asstring;
        Self.percentualagregado:=fieldbyname('percentualagregado').asstring;
        Self.ipi:=fieldbyname('ipi').asstring;
        Self.Aliquota_ICMS_Estado:=fieldbyname('Aliquota_ICMS_Estado').asstring;
        Self.Reducao_BC_ICMS_Estado:=fieldbyname('Reducao_BC_ICMS_Estado').asstring;
        Self.Aliquota_ICMS_Cupom_Estado:=fieldbyname('Aliquota_ICMS_Cupom_Estado').asstring;
        Self.IsentoICMS_ForaEstado:=fieldbyname('IsentoICMS_ForaEstado').asstring;
        Self.SubstituicaoICMS_ForaEstado:=fieldbyname('SubstituicaoICMS_ForaEstado').asstring;
        Self.ValorPauta_Sub_Trib_ForaEstado:=fieldbyname('ValorPauta_Sub_Trib_ForaEstado').asstring;
        Self.Aliquota_ICMS_ForaEstado:=fieldbyname('Aliquota_ICMS_ForaEstado').asstring;
        Self.Reducao_BC_ICMS_ForaEstado:=fieldbyname('Reducao_BC_ICMS_ForaEstado').asstring;
        Self.Aliquota_ICMS_Cupom_ForaEstado:=fieldbyname('Aliquota_ICMS_Cupom_ForaEstado').asstring;
        If(FieldByName('SituacaoTributaria_TabelaA').asstring<>'')
        Then Begin
                 If (Self.SituacaoTributaria_TabelaA.LocalizaCodigo(FieldByName('SituacaoTributaria_TabelaA').asstring)=False)
                 Then Begin
                          Messagedlg('SituacaoTributaria_TabelaA N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.SituacaoTributaria_TabelaA.TabelaparaObjeto;
        End;
        If(FieldByName('SituacaoTributaria_TabelaB').asstring<>'')
        Then Begin
                 If (Self.SituacaoTributaria_TabelaB.LocalizaCodigo(FieldByName('SituacaoTributaria_TabelaB').asstring)=False)
                 Then Begin
                          Messagedlg('SituacaoTributaria_TabelaB N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.SituacaoTributaria_TabelaB.TabelaparaObjeto;
        End;
        Self.ClassificacaoFiscal:=fieldbyname('ClassificacaoFiscal').asstring;
        self.NCM:=fieldbyname('NCM').AsString;
        self.cest:=fieldbyname('cest').AsString;
        Self.ATIVO:=fieldbyname('ativo').AsString;
//CODIFICA TABELAPARAOBJETO

        result:=True;
     End;
end;


Procedure TObjFERRAGEM.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Referencia').asstring:=Self.Referencia;
        ParamByName('GrupoFerragem').asstring:=Self.GrupoFerragem.GET_CODIGO;
        ParamByName('Descricao').asstring:=Self.Descricao;
        ParamByName('Unidade').asstring:=Self.Unidade;
        ParamByName('Fornecedor').asstring:=Self.Fornecedor.GET_CODIGO;
        ParamByName('Peso').asstring:=virgulaparaponto(Self.Peso);
        ParamByName('PrecoCusto').asstring:=virgulaparaponto(Self.PrecoCusto);

        ParamByName('PorcentagemInstalado').asstring:=virgulaparaponto(Self.PorcentagemInstalado);
        ParamByName('PorcentagemFornecido').asstring:=virgulaparaponto(Self.PorcentagemFornecido);
        ParamByName('PorcentagemRetirado').asstring:=virgulaparaponto(Self.PorcentagemRetirado);

        ParamByName('precovendaInstalado').asstring:=virgulaparaponto(Self.precovendaInstalado);
        ParamByName('precovendaFornecido').asstring:=virgulaparaponto(Self.precovendaFornecido);
        ParamByName('precovendaRetirado').asstring:=virgulaparaponto(Self.precovendaRetirado);

        ParamByName('IsentoICMS_Estado').asstring:=Self.IsentoICMS_Estado;
        ParamByName('SubstituicaoICMS_Estado').asstring:=Self.SubstituicaoICMS_Estado;
        ParamByName('ValorPauta_Sub_Trib_Estado').asstring:=virgulaparaponto(Self.ValorPauta_Sub_Trib_Estado);
        ParamByName('Aliquota_ICMS_Estado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_Estado);
        ParamByName('percentualagregado').asstring:=virgulaparaponto(Self.percentualagregado);
        ParamByName('ipi').asstring:=virgulaparaponto(Self.ipi);
        ParamByName('Reducao_BC_ICMS_Estado').asstring:=virgulaparaponto(Self.Reducao_BC_ICMS_Estado);
        ParamByName('Aliquota_ICMS_Cupom_Estado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_Cupom_Estado);
        ParamByName('IsentoICMS_ForaEstado').asstring:=Self.IsentoICMS_ForaEstado;
        ParamByName('SubstituicaoICMS_ForaEstado').asstring:=Self.SubstituicaoICMS_ForaEstado;
        ParamByName('ValorPauta_Sub_Trib_ForaEstado').asstring:=virgulaparaponto(Self.ValorPauta_Sub_Trib_ForaEstado);
        ParamByName('Aliquota_ICMS_ForaEstado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_ForaEstado);
        ParamByName('Reducao_BC_ICMS_ForaEstado').asstring:=virgulaparaponto(Self.Reducao_BC_ICMS_ForaEstado);
        ParamByName('Aliquota_ICMS_Cupom_ForaEstado').asstring:=virgulaparaponto(Self.Aliquota_ICMS_Cupom_ForaEstado);
        ParamByName('SituacaoTributaria_TabelaA').asstring:=Self.SituacaoTributaria_TabelaA.GET_CODIGO;
        ParamByName('SituacaoTributaria_TabelaB').asstring:=Self.SituacaoTributaria_TabelaB.GET_CODIGO;
        ParamByName('ClassificacaoFiscal').asstring:=Self.ClassificacaoFiscal;
        ParamByName('NCM').AsString:=self.NCM;
        ParamByName('cest').AsString:=self.cest;
        ParamByName('ativo').AsString:=Self.ATIVO;

  End;
End;

//***********************************************************************

function TObjFERRAGEM.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;




 { if (Self.VerificaNumericos=False)
  Then Exit;    }

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjFERRAGEM.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Referencia:='';
        GrupoFerragem.ZerarTabela;
        Descricao:='';
        Unidade:='';
        Fornecedor.ZerarTabela;
        Peso:='';
        PrecoCusto:='';
        PorcentagemInstalado:='';
        PorcentagemFornecido:='';
        PorcentagemRetirado:='';
        PrecoVendaInstalado:='';
        PrecoVendaFornecido:='';
        PrecoVendaRetirado:='';

        IsentoICMS_Estado:='';
        SubstituicaoICMS_Estado:='';
        ValorPauta_Sub_Trib_Estado:='';
        Aliquota_ICMS_Estado:='';
        percentualagregado:='';
        ipi:='';
        Reducao_BC_ICMS_Estado:='';
        Aliquota_ICMS_Cupom_Estado:='';
        IsentoICMS_ForaEstado:='';
        SubstituicaoICMS_ForaEstado:='';
        ValorPauta_Sub_Trib_ForaEstado:='';
        Aliquota_ICMS_ForaEstado:='';
        Reducao_BC_ICMS_ForaEstado:='';
        Aliquota_ICMS_Cupom_ForaEstado:='';
        SituacaoTributaria_TabelaA.ZerarTabela;
        SituacaoTributaria_TabelaB.ZerarTabela;
        ClassificacaoFiscal:='';
        NCM:='';
        cest:='';
        ativo:='';


     End;
end;

Function TObjFERRAGEM.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Referencia='')
      Then Mensagem:=mensagem+'/Referencia';
      If (Descricao='')
      Then Mensagem:=mensagem+'/Descri��o';
      If (Unidade='')
      Then Mensagem:=mensagem+'/Unidade';
     
      if (percentualagregado='')
      then percentualagregado:='0';

      if (ipi='')
      then ipi:='0';


      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjFERRAGEM.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.GrupoFerragem.Get_CODIGO<>'')then
      If (Self.GrupoFerragem.LocalizaCodigo(Self.GrupoFerragem.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Grupo Ferragem n�o Encontrado!';
      If (Self.Fornecedor.Get_CODIGO<>'')then
      If (Self.Fornecedor.LocalizaCodigo(Self.Fornecedor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Fornecedor n�o Encontrado!';

      If (Self.SituacaoTributaria_TabelaA.Get_CODIGO<>'')then
      If (Self.SituacaoTributaria_TabelaA.LocalizaCodigo(Self.SituacaoTributaria_TabelaA.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ SituacaoTributaria_TabelaA n�o Encontrado!';
      if (Self.SituacaoTributaria_TabelaB.Get_CODIGO<>'')then
      If (Self.SituacaoTributaria_TabelaB.LocalizaCodigo(Self.SituacaoTributaria_TabelaB.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ SituacaoTributaria_TabelaB n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjFERRAGEM.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.GrupoFerragem.Get_Codigo<>'')
        Then Strtoint(Self.GrupoFerragem.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Grupo Ferragem';
     End;
     try
        If (Self.Fornecedor.Get_Codigo<>'')
        Then Strtoint(Self.Fornecedor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Fornecedor';
     End;
     try
        Strtofloat(Self.Peso);
     Except
           Mensagem:=mensagem+'/Peso';
     End;
     try
        Strtofloat(Self.PrecoCusto);
     Except
           Mensagem:=mensagem+'/PrecoCusto';
     End;

     try
        Strtofloat(Self.PorcentagemInstalado);
     Except
           Mensagem:=mensagem+'/Porcentagem Instalado';
     End;
     try
        Strtofloat(Self.PorcentagemFornecido);
     Except
           Mensagem:=mensagem+'/Porcentagem Fornecido';
     End;
     try
        Strtofloat(Self.PorcentagemRetirado);
     Except
           Mensagem:=mensagem+'/PorcentagemRetirado';
     End;

     try
        Strtofloat(Self.precovendaInstalado);
     Except
           Mensagem:=mensagem+'/Pre�o de Venda Instalado';
     End;
     try
        Strtofloat(Self.precovendaFornecido);
     Except
           Mensagem:=mensagem+'/Pre�o de Venda Fornecido';
     End;
     try
        Strtofloat(Self.precovendaRetirado);
     Except
           Mensagem:=mensagem+'/Pre�o de Venda Retirado';
     End;

     try
        Strtofloat(Self.ValorPauta_Sub_Trib_Estado);
     Except
           Mensagem:=mensagem+'/ValorPauta_Sub_Trib_Estado';
     End;
     try
        Strtofloat(Self.Aliquota_ICMS_Estado);
     Except
           Mensagem:=mensagem+'/Aliquota_ICMS_Estado';
     End;


     try
        Strtofloat(Self.percentualagregado);
     Except
           Mensagem:=mensagem+'/percentual agregado';
     End;


     try
        Strtofloat(Self.ipi);
     Except
           Mensagem:=mensagem+'/percentual agregado';
     End;

     try
        Strtofloat(Self.Reducao_BC_ICMS_Estado);
     Except
           Mensagem:=mensagem+'/Reducao_BC_ICMS_Estado';
     End;
     try
        Strtofloat(Self.Aliquota_ICMS_Cupom_Estado);
     Except
           Mensagem:=mensagem+'/Aliquota_ICMS_Cupom_Estado';
     End;
     try
        Strtofloat(Self.ValorPauta_Sub_Trib_ForaEstado);
     Except
           Mensagem:=mensagem+'/ValorPauta_Sub_Trib_ForaEstado';
     End;
     try
        Strtofloat(Self.Aliquota_ICMS_ForaEstado);
     Except
           Mensagem:=mensagem+'/Aliquota_ICMS_ForaEstado';
     End;
     try
        Strtofloat(Self.Reducao_BC_ICMS_ForaEstado);
     Except
           Mensagem:=mensagem+'/Reducao_BC_ICMS_ForaEstado';
     End;
     try
        Strtofloat(Self.Aliquota_ICMS_Cupom_ForaEstado);
     Except
           Mensagem:=mensagem+'/Aliquota_ICMS_Cupom_ForaEstado';
     End;
     try
        If (Self.SituacaoTributaria_TabelaA.Get_Codigo<>'')
        Then Strtoint(Self.SituacaoTributaria_TabelaA.Get_Codigo);
     Except
           Mensagem:=mensagem+'/SituacaoTributaria_TabelaA';
     End;
     try
        If (Self.SituacaoTributaria_TabelaB.Get_Codigo<>'')
        Then Strtoint(Self.SituacaoTributaria_TabelaB.Get_Codigo);
     Except
           Mensagem:=mensagem+'/SituacaoTributaria_TabelaB';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjFERRAGEM.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjFERRAGEM.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjFERRAGEM.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro FERRAGEM vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Referencia,GrupoFerragem,Descricao,Unidade,Fornecedor');
           SQL.ADD(' ,Peso,PrecoCusto,PorcentagemInstalado,PorcentagemFornecido,PorcentagemRetirado');
           SQL.ADD(' ,PrecoVendaInstalado,PrecoVendaFornecido,PrecoVendaRetirado,IsentoICMS_Estado');
           SQL.ADD(' ,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado,Aliquota_ICMS_Estado');
           SQL.ADD(' ,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado,IsentoICMS_ForaEstado');
           SQL.ADD(' ,SubstituicaoICMS_ForaEstado,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
           SQL.ADD(' ,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado,SituacaoTributaria_TabelaA');
           SQL.ADD(' ,SituacaoTributaria_TabelaB,ClassificacaoFiscal,percentualagregado,ipi,NCM,ativo,cest');
           SQL.ADD(' from  TabFerragem');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjFERRAGEM.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjFERRAGEM.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjFERRAGEM.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ObjDataSource:=TDataSource.Create(nil);
        Self.ObjDataSource.DataSet:=Self.Objquery;

        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.GrupoFerragem:=TOBJGRUPOFERRAGEM.create;
        Self.Fornecedor:=TOBJFORNECEDOR.create;
        Self.SituacaoTributaria_TabelaA:=TOBJTABELAA_ST .create;
        Self.SituacaoTributaria_TabelaB:=TOBJTABELAB_ST .create;
//CODIFICA CRIACAO DE OBJETOS
        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear; 
                InsertSQL.add('Insert Into TabFerragem(Codigo,Referencia,GrupoFerragem');
                InsertSQL.add(' ,Descricao,Unidade,Fornecedor,Peso,PrecoCusto,PorcentagemInstalado');
                InsertSQL.add(' ,PorcentagemFornecido,PorcentagemRetirado');
                InsertSQL.add(' ,IsentoICMS_Estado');
                InsertSQL.add(' ,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado');
                InsertSQL.add(' ,Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado');
                InsertSQL.add(' ,IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado');
                InsertSQL.add(' ,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
                InsertSQL.add(' ,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado');
                InsertSQL.add(' ,SituacaoTributaria_TabelaA,SituacaoTributaria_TabelaB');
                InsertSQL.add(' ,ClassificacaoFiscal,PrecoVendaInstalado,PrecoVendaFornecido,PrecoVendaRetirado,percentualagregado,ipi,NCM,ativo,cest)');
                InsertSQL.add('values (:Codigo,:Referencia,:GrupoFerragem,:Descricao');
                InsertSQL.add(' ,:Unidade,:Fornecedor,:Peso,:PrecoCusto,:PorcentagemInstalado');
                InsertSQL.add(' ,:PorcentagemFornecido,:PorcentagemRetirado');
                InsertSQL.add(' ,:IsentoICMS_Estado');
                InsertSQL.add(' ,:SubstituicaoICMS_Estado,:ValorPauta_Sub_Trib_Estado');
                InsertSQL.add(' ,:Aliquota_ICMS_Estado,:Reducao_BC_ICMS_Estado,:Aliquota_ICMS_Cupom_Estado');
                InsertSQL.add(' ,:IsentoICMS_ForaEstado,:SubstituicaoICMS_ForaEstado');
                InsertSQL.add(' ,:ValorPauta_Sub_Trib_ForaEstado,:Aliquota_ICMS_ForaEstado');
                InsertSQL.add(' ,:Reducao_BC_ICMS_ForaEstado,:Aliquota_ICMS_Cupom_ForaEstado');
                InsertSQL.add(' ,:SituacaoTributaria_TabelaA,:SituacaoTributaria_TabelaB');
                InsertSQL.add(' ,:ClassificacaoFiscal,:PrecoVendaInstalado,:PrecoVendaFornecido,:PrecoVendaRetirado,:percentualagregado,:ipi,:NCM,:ativo,:cest)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabFerragem set Codigo=:Codigo,Referencia=:Referencia');
                ModifySQL.add(',GrupoFerragem=:GrupoFerragem,Descricao=:Descricao,Unidade=:Unidade');
                ModifySQL.add(',Fornecedor=:Fornecedor,Peso=:Peso,PrecoCusto=:PrecoCusto');
                ModifySQL.add(',PorcentagemInstalado=:PorcentagemInstalado,PorcentagemFornecido=:PorcentagemFornecido');
                ModifySQL.add(',PorcentagemRetirado=:PorcentagemRetirado');
                ModifySQL.add(',IsentoICMS_Estado=:IsentoICMS_Estado,SubstituicaoICMS_Estado=:SubstituicaoICMS_Estado');
                ModifySQL.add(',ValorPauta_Sub_Trib_Estado=:ValorPauta_Sub_Trib_Estado');
                ModifySQL.add(',Aliquota_ICMS_Estado=:Aliquota_ICMS_Estado,Reducao_BC_ICMS_Estado=:Reducao_BC_ICMS_Estado');
                ModifySQL.add(',Aliquota_ICMS_Cupom_Estado=:Aliquota_ICMS_Cupom_Estado');
                ModifySQL.add(',IsentoICMS_ForaEstado=:IsentoICMS_ForaEstado,SubstituicaoICMS_ForaEstado=:SubstituicaoICMS_ForaEstado');
                ModifySQL.add(',ValorPauta_Sub_Trib_ForaEstado=:ValorPauta_Sub_Trib_ForaEstado');
                ModifySQL.add(',Aliquota_ICMS_ForaEstado=:Aliquota_ICMS_ForaEstado');
                ModifySQL.add(',Reducao_BC_ICMS_ForaEstado=:Reducao_BC_ICMS_ForaEstado');
                ModifySQL.add(',Aliquota_ICMS_Cupom_ForaEstado=:Aliquota_ICMS_Cupom_ForaEstado');
                ModifySQL.add(',SituacaoTributaria_TabelaA=:SituacaoTributaria_TabelaA');
                ModifySQL.add(',SituacaoTributaria_TabelaB=:SituacaoTributaria_TabelaB');
                ModifySQL.add(',ClassificacaoFiscal=:ClassificacaoFiscal,PrecoVendaInstalado=:PrecoVendaInstalado,PrecoVendaFornecido=:PrecoVendaFornecido,PrecoVendaRetirado=:PrecoVendaRetirado');
                ModifySQL.add(',percentualagregado=:percentualagregado,ipi=:ipi,NCM=:NCM,ativo=:ativo,cest=:cest where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabFerragem where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjFERRAGEM.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjFERRAGEM.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select Referencia,Descricao,Unidade,Fornecedor');
     Self.ParametroPesquisa.add(',Peso,PrecoCusto,PorcentagemInstalado,PorcentagemFornecido,PorcentagemRetirado');
     Self.ParametroPesquisa.add(',PrecoVendaInstalado,PrecoVendaFornecido,PrecoVendaRetirado,IsentoICMS_Estado');
     Self.ParametroPesquisa.add(',SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado,Aliquota_ICMS_Estado');
     Self.ParametroPesquisa.add(',Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado,IsentoICMS_ForaEstado');
     Self.ParametroPesquisa.add(',SubstituicaoICMS_ForaEstado,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
     Self.ParametroPesquisa.add(',Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado,SituacaoTributaria_TabelaA');
     Self.ParametroPesquisa.add(',SituacaoTributaria_TabelaB,ClassificacaoFiscal,GrupoFerragem,Codigo,percentualagregado,ipi,NCM,ativo,cest');
     Self.ParametroPesquisa.add(' from TabFERRAGEM');
     Result:=Self.ParametroPesquisa;
end;

function TObjFERRAGEM.Get_Pesquisa2: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select Referencia,Descricao,Unidade,Fornecedor');
     Self.ParametroPesquisa.add(',Peso,PrecoCusto,PorcentagemInstalado,PorcentagemFornecido,PorcentagemRetirado');
     Self.ParametroPesquisa.add(',PrecoVendaInstalado,PrecoVendaFornecido,PrecoVendaRetirado,IsentoICMS_Estado');
     Self.ParametroPesquisa.add(',SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado,Aliquota_ICMS_Estado');
     Self.ParametroPesquisa.add(',Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado,IsentoICMS_ForaEstado');
     Self.ParametroPesquisa.add(',SubstituicaoICMS_ForaEstado,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
     Self.ParametroPesquisa.add(',Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado,SituacaoTributaria_TabelaA');
     Self.ParametroPesquisa.add(',SituacaoTributaria_TabelaB,ClassificacaoFiscal,GrupoFerragem,Codigo,percentualagregado,ipi,NCM,ativo,cest');
     Self.ParametroPesquisa.add(' from TabFERRAGEM');
     Self.ParametroPesquisa.Add(' where ativo=''S'' ');
     Result:=Self.ParametroPesquisa;
end;

function TObjFERRAGEM.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de FERRAGEM ';
end;


function TObjFERRAGEM.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENFERRAGEM,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENFERRAGEM,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjFERRAGEM.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    FreeAndNil(ObjDataSource);
    Self.GrupoFerragem.FREE;
    Self.Fornecedor.FREE;
    Self.SituacaoTributaria_TabelaA.FREE;
    Self.SituacaoTributaria_TabelaB.FREE;   
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjFERRAGEM.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjFERRAGEM.RetornaCampoNome: string;
begin
      result:='descricao';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjFerragem.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjFerragem.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjFerragem.Submit_Referencia(parametro: string);
begin
        Self.Referencia:=Parametro;
end;
function TObjFerragem.Get_Referencia: string;
begin
        Result:=Self.Referencia;
end;
procedure TObjFerragem.Submit_Descricao(parametro: string);
begin
        Self.Descricao:=Parametro;
end;
function TObjFerragem.Get_Descricao: string;
begin
        Result:=Self.Descricao;
end;
procedure TObjFerragem.Submit_Unidade(parametro: string);
begin
        Self.Unidade:=Parametro;
end;
function TObjFerragem.Get_Unidade: string;
begin
        Result:=Self.Unidade;
end;
procedure TObjFerragem.Submit_Peso(parametro: string);
begin
        Self.Peso:=Parametro;
end;
function TObjFerragem.Get_Peso: string;
begin
        Result:=Self.Peso;
end;
procedure TObjFerragem.Submit_PrecoCusto(parametro: string);
begin
        Self.PrecoCusto:=Parametro;
end;
function TObjFerragem.Get_PrecoCusto: string;
begin
        Result:=Self.PrecoCusto;
end;
procedure TObjFerragem.Submit_PorcentagemInstalado(parametro: string);
begin
        Self.PorcentagemInstalado:=Parametro;
end;
function TObjFerragem.Get_PorcentagemInstalado: string;
begin
        Result:=Self.PorcentagemInstalado;
end;
procedure TObjFerragem.Submit_PorcentagemFornecido(parametro: string);
begin
        Self.PorcentagemFornecido:=Parametro;
end;
function TObjFerragem.Get_PorcentagemFornecido: string;
begin
        Result:=Self.PorcentagemFornecido;
end;
procedure TObjFerragem.Submit_PorcentagemRetirado(parametro: string);
begin
        Self.PorcentagemRetirado:=Parametro;
end;
function TObjFerragem.Get_PorcentagemRetirado: string;
begin
        Result:=Self.PorcentagemRetirado;
end;
function TObjFerragem.Get_PrecoVendaInstalado: string;
begin
        Result:=Self.PrecoVendaInstalado;
end;
function TObjFerragem.Get_PrecoVendaFornecido: string;
begin
        Result:=Self.PrecoVendaFornecido;
end;
function TObjFerragem.Get_PrecoVendaRetirado: string;
begin
        Result:=Self.PrecoVendaRetirado;
end;
procedure TObjFerragem.Submit_IsentoICMS_Estado(parametro: string);
begin
        Self.IsentoICMS_Estado:=Parametro;
end;
function TObjFerragem.Get_IsentoICMS_Estado: string;
begin
        Result:=Self.IsentoICMS_Estado;
end;
procedure TObjFerragem.Submit_SubstituicaoICMS_Estado(parametro: string);
begin
        Self.SubstituicaoICMS_Estado:=Parametro;
end;
function TObjFerragem.Get_SubstituicaoICMS_Estado: string;
begin
        Result:=Self.SubstituicaoICMS_Estado;
end;
procedure TObjFerragem.Submit_ValorPauta_Sub_Trib_Estado(parametro: string);
begin
        Self.ValorPauta_Sub_Trib_Estado:=Parametro;
end;
function TObjFerragem.Get_ValorPauta_Sub_Trib_Estado: string;
begin
        Result:=Self.ValorPauta_Sub_Trib_Estado;
end;
procedure TObjFerragem.Submit_Aliquota_ICMS_Estado(parametro: string);
begin
        Self.Aliquota_ICMS_Estado:=Parametro;
end;
function TObjFerragem.Get_Aliquota_ICMS_Estado: string;
begin
        Result:=Self.Aliquota_ICMS_Estado;
end;

procedure TObjFerragem.Submit_percentualagregado(parametro: string);
begin
        Self.percentualagregado:=Parametro;
end;
function TObjFerragem.Get_percentualagregado: string;
begin
        Result:=Self.percentualagregado;
end;


procedure TObjFerragem.Submit_ipi(parametro: string);
begin
        Self.ipi:=Parametro;
end;
function TObjFerragem.Get_ipi: string;
begin
        Result:=Self.ipi;
end;


procedure TObjFerragem.Submit_Reducao_BC_ICMS_Estado(parametro: string);
begin
        Self.Reducao_BC_ICMS_Estado:=Parametro;
end;
function TObjFerragem.Get_Reducao_BC_ICMS_Estado: string;
begin
        Result:=Self.Reducao_BC_ICMS_Estado;
end;
procedure TObjFerragem.Submit_Aliquota_ICMS_Cupom_Estado(parametro: string);
begin
        Self.Aliquota_ICMS_Cupom_Estado:=Parametro;
end;
function TObjFerragem.Get_Aliquota_ICMS_Cupom_Estado: string;
begin
        Result:=Self.Aliquota_ICMS_Cupom_Estado;
end;
procedure TObjFerragem.Submit_IsentoICMS_ForaEstado(parametro: string);
begin
        Self.IsentoICMS_ForaEstado:=Parametro;
end;
function TObjFerragem.Get_IsentoICMS_ForaEstado: string;
begin
        Result:=Self.IsentoICMS_ForaEstado;
end;
procedure TObjFerragem.Submit_SubstituicaoICMS_ForaEstado(parametro: string);
begin
        Self.SubstituicaoICMS_ForaEstado:=Parametro;
end;
function TObjFerragem.Get_SubstituicaoICMS_ForaEstado: string;
begin
        Result:=Self.SubstituicaoICMS_ForaEstado;
end;
procedure TObjFerragem.Submit_ValorPauta_Sub_Trib_ForaEstado(parametro: string);
begin
        Self.ValorPauta_Sub_Trib_ForaEstado:=Parametro;
end;
function TObjFerragem.Get_ValorPauta_Sub_Trib_ForaEstado: string;
begin
        Result:=Self.ValorPauta_Sub_Trib_ForaEstado;
end;
procedure TObjFerragem.Submit_Aliquota_ICMS_ForaEstado(parametro: string);
begin
        Self.Aliquota_ICMS_ForaEstado:=Parametro;
end;
function TObjFerragem.Get_Aliquota_ICMS_ForaEstado: string;
begin
        Result:=Self.Aliquota_ICMS_ForaEstado;
end;
procedure TObjFerragem.Submit_Reducao_BC_ICMS_ForaEstado(parametro: string);
begin
        Self.Reducao_BC_ICMS_ForaEstado:=Parametro;
end;
function TObjFerragem.Get_Reducao_BC_ICMS_ForaEstado: string;
begin
        Result:=Self.Reducao_BC_ICMS_ForaEstado;
end;
procedure TObjFerragem.Submit_Aliquota_ICMS_Cupom_ForaEstado(parametro: string);
begin
        Self.Aliquota_ICMS_Cupom_ForaEstado:=Parametro;
end;
function TObjFerragem.Get_Aliquota_ICMS_Cupom_ForaEstado: string;
begin
        Result:=Self.Aliquota_ICMS_Cupom_ForaEstado;
end;
procedure TObjFerragem.Submit_ClassificacaoFiscal(parametro: string);
begin
        Self.ClassificacaoFiscal:=Parametro;
end;
function TObjFerragem.Get_ClassificacaoFiscal: string;
begin
        Result:=Self.ClassificacaoFiscal;
end;
//CODIFICA GETSESUBMITS


procedure TObjFERRAGEM.EdtGrupoFerragemExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.GrupoFerragem.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.GrupoFerragem.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.GrupoFerragem.GET_NOME;
End;

procedure TObjFERRAGEM.EdtGrupoFerragemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.GrupoFerragem.Get_Pesquisa,Self.GrupoFerragem.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoFerragem.RETORNACAMPOCODIGO).asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjFERRAGEM.EdtFerragemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabferragem','',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjFERRAGEM.EdtGrupoFerragemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.GrupoFerragem.Get_Pesquisa,Self.GrupoFerragem.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoFerragem.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.GrupoFerragem.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoFerragem.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjFERRAGEM.EdtFerragemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabferragem','',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.GrupoFerragem.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjFERRAGEM.EdtFornecedorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Fornecedor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Fornecedor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Fornecedor.Get_RazaoSocial;
End;
procedure TObjFERRAGEM.EdtFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FFORNECEDOR:TFFORNECEDOR;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FFORNECEDOR:=TFFORNECEDOR.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Fornecedor.Get_Pesquisa,Self.Fornecedor.Get_TituloPesquisa,FFornecedor)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Fornecedor.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Fornecedor.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Fornecedor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FFORNECEDOR);
     End;
end;

procedure TObjFERRAGEM.EdtSituacaoTributaria_TabelaAExit(Sender: TObject);
Begin
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.SituacaoTributaria_TabelaA.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.SituacaoTributaria_TabelaA.tabelaparaobjeto;

End;
procedure TObjFERRAGEM.EdtSituacaoTributaria_TabelaAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa
            (Self.SituacaoTributaria_TabelaA.Get_Pesquisa,Self.SituacaoTributaria_TabelaA.Get_TituloPesquisa,
            nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SituacaoTributaria_TabelaA.RETORNACAMPOCODIGO).asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjFERRAGEM.EdtSituacaoTributaria_TabelaBExit(Sender: TObject);
Begin
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.SituacaoTributaria_TabelaB.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.SituacaoTributaria_TabelaB.tabelaparaobjeto;
End;
procedure TObjFERRAGEM.EdtSituacaoTributaria_TabelaBKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa
               (Self.SituacaoTributaria_TabelaB.Get_Pesquisa,Self.SituacaoTributaria_TabelaB.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SituacaoTributaria_TabelaB.RETORNACAMPOCODIGO).asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN




procedure TObjFERRAGEM.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJFERRAGEM';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
             -1 : Exit;
          End;
     end;

end;



function TObjFERRAGEM.VerificaReferencia(PStatus: TDataSetState; PCodigo,
  PReferencia: string): Boolean;         {rodolfo}
begin
     Result:=true;

     pReferencia := StrReplaceRef(PReferencia); {Rodolfo}

     With Self.Objquery do
     Begin

         if (PStatus = dsInsert) then  // quando for insercao
         Begin
             close;
             SQL.Clear;
             SQL.Add('Select Codigo from TabFerragem Where Referencia = '+#39+UpperCase(PReferencia)+#39);
             Open;

             if (RecordCount > 0)then
             Begin
                 Result:=true;
                 exit;
             end else
             Result:=false;
             exit;
         end;


         if  (PStatus = dsEdit)then    // quando for edicao
         Begin
             if Self.ReferenciaAnterior = PReferencia then
             Begin                      // Se a Referencia  que esta chegando � a mesma da anteiror
                 Result:=false;         // eu nem preciso verificar nada
                 exit;
             end else
             Begin
                 close;
                 SQL.Clear;
                 SQL.Add('Select Codigo from TabFerragem Where Referencia = '+#39+UpperCase(PReferencia)+#39);
                 Open;

                 if (RecordCount > 0)then
                 Begin
                     Result:=true;
                     exit;
                 end else
                 Result:=false;
                 exit;
             end;
         end;
     end;
end;
function TObjFERRAGEM.LocalizaReferencia(Parametro: string): boolean;
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro FERRAGEM vazio',mterror,[mbok],0);
                 exit;
       End;

       Parametro := strReplaceRef(Parametro); {Rodolfo}

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Referencia,GrupoFerragem,Descricao,Unidade,Fornecedor');
           SQL.ADD(' ,Peso,PrecoCusto,PorcentagemInstalado,PorcentagemFornecido,PorcentagemRetirado');
           SQL.ADD(' ,PrecoVendaInstalado,PrecoVendaFornecido,PrecoVendaRetirado,IsentoICMS_Estado');
           SQL.ADD(' ,SubstituicaoICMS_Estado,ValorPauta_Sub_Trib_Estado,Aliquota_ICMS_Estado');
           SQL.ADD(' ,Reducao_BC_ICMS_Estado,Aliquota_ICMS_Cupom_Estado,IsentoICMS_ForaEstado');
           SQL.ADD(' ,SubstituicaoICMS_ForaEstado,ValorPauta_Sub_Trib_ForaEstado,Aliquota_ICMS_ForaEstado');
           SQL.ADD(' ,Reducao_BC_ICMS_ForaEstado,Aliquota_ICMS_Cupom_ForaEstado,SituacaoTributaria_TabelaA');
           SQL.ADD(' ,SituacaoTributaria_TabelaB,ClassificacaoFiscal,percentualagregado,ipi,NCM,ativo,cest');
           SQL.ADD(' from  TabFerragem');
           SQL.ADD(' WHERE Referencia='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

Procedure TObjFERRAGEM.AtualizaMargens(PGrupoFerragem: string);
Var QueryLOcal:TIBquery;
    PPorcentagemInstalado:string;
    PPorcentagemRetirado:string;
    PPorcentagemFornecido:string;
    NovoPreco:currency;
begin
      if (PGrupoFerragem='')then
      Begin
           MensagemErro('Escolha um Grupo de Ferragem');
           exit;
      end;

      if (Self.GrupoFerragem.LocalizaCodigo(PGrupoFerragem)=false)then
      Begin
          MensagemErro('Grupo de Ferragem n�o encontrado');
          exit;
      end;
      Self.GrupoFerragem.TabelaparaObjeto;
      PPorcentagemInstalado:=Self.GrupoFerragem.Get_PorcentagemInstalado;
      PPorcentagemRetirado:=Self.GrupoFerragem.Get_PorcentagemRetirado;
      PPorcentagemFornecido:=Self.GrupoFerragem.Get_PorcentagemFornecido;

      try
            try
                  QueryLOcal:=TIBQuery.Create(nil);
                  QueryLOcal.Database:=FDataModulo.IBDatabase;
            except;
                 MensagemErro('Erro ao tenar criar a QueryLocal');
                 exit;
            end;

            With QueryLOcal  do
            Begin
                 Close;
                 Sql.Clear;
                 Sql.Add('Select TabFerragem.Codigo from TabFerragem');
                 Sql.Add('Where GrupoFerragem = '+PGrupoFerragem);
                 Open;
                 First;

                 // Para cada ferragem desse grupo eu Altero o Valor das margns
                 While not (eof) do
                 Begin
                      Self.ZerarTabela;
                      if (Self.LocalizaCodigo(fieldbyname('Codigo').AsString)=false)then
                      Begin
                           MensagemErro('Ferragem n�o encontrada.');
                           exit;
                      end;

                      Self.TabelaparaObjeto;
                      Self.Status:=dsEdit;
                      Self.Submit_PorcentagemInstalado(PPorcentagemInstalado);
                      Self.Submit_PorcentagemFornecido(PPorcentagemFornecido);
                      Self.Submit_PorcentagemRetirado(PPorcentagemRetirado);

                      NovoPreco:=(StrToCurr(Get_PrecoCusto)+((StrToCurr(Get_PrecoCusto)*StrToCurr(PPorcentagemInstalado))/100));
                      Self.Submit_PrecoVendaInstalado(CurrToStr(NovoPreco));
                      NovoPreco:=(StrToCurr(Get_PrecoCusto)+((StrToCurr(Get_PrecoCusto)*StrToCurr(PorcentagemFornecido))/100));
                      self.Submit_PrecoVendaFornecido(CurrToStr(NovoPreco));
                      NovoPreco:=(StrToCurr(Get_PrecoCusto)+((StrToCurr(Get_PrecoCusto)*StrToCurr(PorcentagemRetirado))/100));
                      self.Submit_PrecoVendaRetirado(CurrToStr(NovoPreco));

                      if (Self.Salvar(false)=false)then
                      begin
                          MensagemErro('Erro ao tentar Atualiza as margens na Tabela de Ferragens');
                          exit;
                      end;
                      Next;
                 end;
            end;
            FDataModulo.IBTransaction.CommitRetaining;
            MensagemAviso('Margem Atualiada com Sucesso !');
      finally
        FreeAndNil(QueryLOcal);
      end;
end;

function TObjFerragem.PrimeiroRegistro: Boolean;
Var MenorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabFerragem') ;
           Open;

           if (FieldByName('MenorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MenorCodigo:=fieldbyname('MenorCodigo').AsString;
           Self.LocalizaCodigo(MenorCodigo);

           Result:=true;
     end;
end;

function TObjFerragem.UltimoRegistro: Boolean;
Var MaiorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabFerragem') ;
           Open;

           if (FieldByName('MaiorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MaiorCodigo:=fieldbyname('MaiorCodigo').AsString;
           Self.LocalizaCodigo(MaiorCodigo);

           Result:=true;
     end;
end;

function TObjFerragem.ProximoRegisto(PCodigo:Integer): Boolean;
Var MaiorValor:Integer;
begin
     Result:=false;

     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from TabFerragem') ;
           Open;

           MaiorValor:=fieldbyname('MaiorCodigo').AsInteger;
     end;

     if (MaiorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Inc(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MaiorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Inc(PCodigo,1);
     end;

     Result:=true;
end;

function TObjFerragem.RegistoAnterior(PCodigo: Integer): Boolean;
Var MenorValor:Integer;
begin
     Result:=false;
     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from TabFerragem') ;
           Open;

           MenorValor:=fieldbyname('MenorCodigo').AsInteger;
     end;

     if (MenorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Dec(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MenorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Dec(PCodigo,1);
     end;

     Result:=true;
end;

procedure TObjFERRAGEM.AtualizaPrecos(PGrupoFerrragem: string);
VAr QueryLOcal :TIBQuery; PVAlorCusto, PPorcentagemReajuste:Currency;
begin

      if (PGrupoFerrragem = '')then
      Begin
           MensagemErro('Pesquise o um grupo de ferragem entes Atualzar os pre�os.');
           exit;
      end;

      if MessageDlg('Tem certeza que deseja alterar os valores dos PRE�OS DE CUSTO de '+#13+
                    'todas as ferragens pertencentes a este grupo?. Lembrando que esse processo � irrevers�vel.', mtConfirmation, [mbYes, mbNo], 0) = mrNo
      then Begin
           exit;
      end;

      With FfiltroImp do
      Begin
           DesativaGrupos;
           Grupo01.Enabled:=true;
           LbGrupo01.Caption:='Acr�scimo em (%)';
           edtgrupo01.Enabled:=True;

           ShowModal;

           if (Tag = 0)then
           Exit;

           try
                PPorcentagemReajuste:=StrToCurr(edtgrupo01.Text);
           except
                MensagemErro('Valor inv�lido.');
                exit;
           end;
      end;

      try
            try
                 QueryLocal:=TIBQuery.Create(nil);
                 QueryLOcal.Database:=FDataModulo.IBDatabase;
            except
                 MensagemErro('Erro ao tentar criar a Query Local');
                 exit;
            end;

            With  QueryLOcal do
            Begin
                 Close;
                 Sql.Clear;
                 Sql.Add('Select Codigo from TabFerragem Where GrupoFerragem = '+PGrupoFerrragem);
                 Open;
                 First;

                 if (RecordCount = 0)then
                 Begin
                      MensagemErro('Nenhuma ferragem encotrada para este grupo.');
                      exit;
                 end;


                 While Not (eof) do
                 Begin
                      PVAlorCusto:=0;
                      Self.ZerarTabela;
                      Self.LocalizaCodigo(fieldbyname('Codigo').AsString);
                      Self.TabelaparaObjeto;
                      PVAlorCusto:=StrToCurr(Self.Get_PrecoCusto);

                      Self.Status:=dsEdit;
                      Self.Submit_PrecoCusto(CurrToStr(PVAlorCusto+(PVAlorCusto*(PPorcentagemReajuste/100))));
                      try
                           Self.Salvar(false);
                      except
                           MensagemErro('Erro ao tentar Salvar o novo pre�o.');
                           FDataModulo.IBTransaction.RollbackRetaining;
                           exit;
                      end;
                 Next;
                 end;

                 FDataModulo.IBTransaction.CommitRetaining;
                 MensagemAviso('Pre�o de Custo de Ferragens  Atualizado com Sucesso!');
            end;

      finally
            FreeAndNil(QueryLOcal);
      end;

end;


procedure TObjFerragem.Reajusta(PIndice,Pgrupo: string);
var
   Psinal:string;
begin

  try

    strtofloat(Pindice);

    if (strtofloat(Pindice)>=0) Then
      Psinal:='+'
    Else
    begin
      Pindice:=floattostr(strtofloat(Pindice)*-1);
      Psinal:='-';
    End;

  except
    mensagemerro('Valor Inv�lido no �ndice');
    exit;
  End;

  if (Pgrupo<>'') Then
  Begin

    try

      strtoint(Pgrupo);

      if (Self.GrupoFerragem.LocalizaCodigo(pgrupo)=False) Then
      begin
        MensagemErro('Grupo de Ferragem n�o encontrado');
        exit;
      End;

    Except

      mensagemerro('Grupo Inv�lido');
      exit;

    end;

  End;

  With Self.Objquery do
  Begin

    close;
    SQL.clear;
    SQL.add('Update Tabferragem Set PRECOCUSTO=PRECOCUSTO'+psinal+'((PRECOCUSTO*'+virgulaparaponto(Pindice)+')/100),');
    SQl.Add('precovendainstalado = precocusto + ( precocusto * porcentageminstalado/100),');
    SQl.Add('precovendafornecido = precocusto + ( precocusto * porcentagemfornecido/100),');
    SQl.Add('precovendaretirado = precocusto + ( precocusto * porcentagemretirado/100)');


    if (Pgrupo<>'') Then
      SQL.add('Where Grupoferragem='+Pgrupo);

    try
    
      execsql;
      FDataModulo.IBTransaction.CommitRetaining;
      mensagemaviso('Conclu�do');

    Except

      on e:exception do
      begin

        FDataModulo.IBTransaction.RollbackRetaining;
        mensagemerro('Erro na tentativa de Reajustar o valor da Ferragem'+#13+E.message);
        exit;
        
      End;

    End;

  End;

end;

procedure TObjFERRAGEM.Submit_PrecoVendaFornecido(parametro: string);
begin
     Self.PrecoVendaFornecido:=parametro;
end;

procedure TObjFERRAGEM.Submit_PrecoVendaInstalado(parametro: string);
begin
     Self.PrecoVendaInstalado:=parametro;
end;

procedure TObjFERRAGEM.Submit_PrecoVendaRetirado(parametro: string);
begin
    Self.PrecoVendaRetirado:=parametro;
end;

procedure TObjFERRAGEM.Submit_NCM(parametro:string);
begin
     SELF.NCM:=parametro;
end;

function TObjFERRAGEM.Get_NCM:string;
begin
    result:=self.NCM;
end;

procedure TObjFERRAGEM.Submit_Ativo(parametro:string);
begin
    self.ATIVO:=parametro;
end;

function TObjFERRAGEM.Get_Ativo:string;
begin
    Result:=ATIVO;
end;

function TObjFERRAGEM.AtualizaTributospeloNCM:Boolean;
var
  qryprod, qryaux: TIBQuery;
  percentual,NCM:string;
begin
  {procuro por produtos onde o ncm estiver preenchido
  depois localizo no cadastro de ncm , resgato o valor do imposto
  e atualiza o produto
  }

  Result := False;
  qryprod := TIBQuery.Create(nil);
  qryaux := TIBQuery.Create(nil);

  qryprod.Database := FDataModulo.IBDatabase;
  qryaux.Database := FDataModulo.IBDatabase;

  try

    qryprod.DisableControls;
    qryprod.Close;
    qryprod.SQL.Clear;
    qryprod.SQL.Add('SELECT COUNT(CODIGO) FROM TABFERRAGEM WHERE NCM IS NOT NULL');
    qryprod.Open;

    FMostraBarraProgresso.ConfiguracoesIniciais(qryprod.Fields[0].AsInteger - 1,0);
    FMostraBarraProgresso.Lbmensagem.Caption := 'Atualizando percentuais de tributos (Ferragens)';
    FMostraBarraProgresso.btcancelar.Visible := True;
    FMostraBarraProgresso.Show;
    Application.ProcessMessages;

    qryprod.Close;
    qryprod.SQL.Clear;
    qryprod.SQL.Add('select ficms.codigo,icms.sta,f.ncm');
    qryprod.SQL.Add('from tabimposto_icms icms');
    qryprod.SQL.Add('join tabferragem_icms ficms on ficms.imposto=icms.codigo');
    qryprod.SQL.Add('join tabferragem f on f.codigo=ficms.ferragem');
    qryprod.SQL.Add('where f.ncm is not null');
    qryprod.SQL.Add('group by ficms.codigo, icms.sta,f.ncm');

    qryprod.Open;

    while not(qryprod.Eof) do
    begin
      NCM := qryprod.Fields[2].AsString;
      if(qryprod.Fields[1].AsString = '0') then
        percentual := get_campoTabela('PERCENTUALTRIBUTO','CODIGONCM','TABNCM',NCM)
      else
      if( (qryprod.Fields[1].AsString = '1') or (qryprod.Fields[1].AsString = '2')) then
        percentual := get_campoTabela('PERCENTUALTRIBUTOIMP','CODIGONCM','TABNCM',NCM)
      else
        percentual := '0';

      if(StrToCurrDef(percentual,0) > 0) then
      begin

        qryaux.DisableControls;
        qryaux.Close;
        qryaux.SQL.Clear;
        qryaux.SQL.Add('UPDATE tabferragem_icms SET PERCENTUALTRIBUTO=:PTRIBUTO WHERE CODIGO=:PPRODUTO');
        qryaux.Params[0].AsCurrency := StrToCurr(percentual);
        qryaux.Params[1].AsString := qryprod.Fields[0].AsString;

        try
          qryaux.ExecSQL;
        except
          raise Exception.Create('Erro ao atualizar percentual do tributo para o produto: '+qryprod.Fields[0].AsString);
        end;
        
      end;

      qryprod.Next;
      FMostraBarraProgresso.IncrementaBarra1(1);

      if(FMostraBarraProgresso.Cancelar) then
        Exit;

      Application.ProcessMessages;

    end;

    qryaux.Transaction.CommitRetaining;
    //FDataModulo.IBTransaction.CommitRetaining;
    Result := True;

  finally

    FMostraBarraProgresso.Close;

    if(qryaux.Transaction.Active) then
      qryaux.Transaction.Rollback;

    if(qryprod <> nil) then
      FreeAndNil(qryprod);

    if(qryaux <> nil) then
      FreeAndNil(qryaux);

  end;

end;


function TObjFERRAGEM.Get_cest: String;
begin
  result := Self.CEST;
end;

procedure TObjFERRAGEM.Submit_cest(parametro: string);
begin
  self.CEST := parametro;
end;

end.



