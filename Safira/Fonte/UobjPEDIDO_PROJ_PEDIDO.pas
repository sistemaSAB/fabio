unit UobjPEDIDO_PROJ_PEDIDO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJPEDIDO_PEDIDO,
UOBJPROJETO,UOBJCOR,Graphics;

Type
   TObjPEDIDO_PROJ_PEDIDO=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Pedido:TOBJPEDIDO_PEDIDO;
                Projeto:TOBJPROJETO;
                CorPerfilado:TOBJCOR;
                CorFerragem:TOBJCOR;
                CorVidro:TOBJCOR;
                CorKitBox:TOBJCOR;
                CorDiverso:TOBJCOR;


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_PesquisaViewProjetoNoRomaneio    :TStringList;
                function    Get_Pesquisaview: TStringList;overload;
                Function    Get_Pesquisaview(ppedido:string):TStringList;overload;

                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure ResgataPEDIDO_PROJ(PPedido:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Local(parametro: string);
                Function Get_Local: string;
                Procedure Submit_TipoPedido(parametro: string);
                Function Get_TipoPedido: string;
                Procedure Submit_PedidoProjetoPrincipal(parametro: string);
                Function Get_PedidoProjetoPrincipal: string;

                Procedure Submit_ValorTotal     (parametro:string);
                Procedure Submit_ValorAcrescimo (parametro:string);
                Procedure Submit_ValorDesconto  (parametro:string);
                Procedure Submit_observacao     (parametro:string);

                Function Get_ValorTotal     :string;
                Function Get_ValorAcrescimo :string;
                Function Get_ValorDesconto  :string;
                Function Get_ValorFinal     :string;
                Function Get_observacao     :String;

                procedure EdtPedidoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtPedidoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtPedidoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;


                procedure EdtPedido_ConCluido_KeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtProjetoExit(Sender: TObject;Var PEdtCodigo:TEdit;LABELNOME:TLABEL);
                procedure EdtProjetoKeyDown(Sender: TObject; Var PEdtCodigo :TEdit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtCorPerfiladoExit(Sender: TObject;Var PEdtCodigo :TEdit; LABELNOME:TLABEL);
                procedure EdtCorPerfiladoKeyDown(Sender: TObject;Var PEdtCodigo:Tedit; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtCorPerfiladoKeyDown(Sender: TObject;Var PEdtCodigo:Tedit; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel;PcodigoProjeto:string);overload;
                procedure EdtCorFerragemExit(Sender: TObject;Var PEdtCodigo:TEdit;LABELNOME:TLABEL);
                procedure EdtCorFerragemKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtCorFerragemKeyDown(Sender: TObject; Var PEdtCodigo:TEdit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel;PcodigoProjeto:string);overload;
                procedure EdtCorVidroExit(Sender: TObject; Var PedtCodigo:TEdit; LABELNOME:TLABEL);
                procedure EdtCorVidroKeyDown(Sender: TObject; Var PEdtCodigo:TEdit; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtCorVidroKeyDown(Sender: TObject; Var PEdtCodigo:TEdit; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel;Pprojeto:string);overload;

                procedure EdtCorKitBoxExit(Sender: TObject;Var PEdtCodigo :TEdit;LABELNOME:TLABEL);
                procedure EdtCorKitBoxKeyDown(Sender: TObject; Var PEdtCodigo:TEdit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtCorKitBoxKeyDown(Sender: TObject; Var PEdtCodigo:TEdit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel;pprojeto:string);overload;

                procedure EdtCorDiversoExit(Sender: TObject;Var PEdtCodigo :TEdit;LABELNOME:TLABEL);
                procedure EdtCorDiversoKeyDown(Sender: TObject; Var PEdtCodigo:TEdit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtCorDiversoKeyDown(Sender: TObject; var PEdtCodigo: TEdit; var Key: Word; Shift: TShiftState;LABELNOME: Tlabel;PcodigoProjeto:string);overload;

                Function VerificaSePedidoProjetoPrincipal(PPedidoProjeto:string):Boolean;

                Function Excluir_sem_produtos(Ppedido:string):boolean;

                procedure EdtCorFerragemPraTrocaKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;Ferragem:string);
                procedure EdtCorPerfiladoPraTrocaKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;Perfilado:string);
                procedure EdtCorKitBoxPraTrocaKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;KitBox:string);
                procedure EdtCorDiversoPraTrocaKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;Diverso:string);
                procedure EdtCorVidroPraTrocaKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;Vidro:string);

         Private
               Objquery:Tibquery;
               ObjQueryPesquisa:TIBquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Local:string;
               TipoPedido:string;
               ValorTotal     :string;
               ValorAcrescimo :string;
               ValorDesconto  :string;
               ValorFinal     :string;
               PedidoProjetoPrincipal:string;
               ParametroPesquisa:TStringList;
               observacao:String;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UpesquisaProjeto;

Function  TObjPEDIDO_PROJ_PEDIDO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If(FieldByName('Pedido').asstring<>'')
        Then Begin
                 If (Self.Pedido.LocalizaCodigo(FieldByName('Pedido').asstring)=False)
                 Then Begin
                          Messagedlg('Pedido N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Pedido.TabelaparaObjeto;
        End;
        If(FieldByName('Projeto').asstring<>'')
        Then Begin
                 If (Self.Projeto.LocalizaCodigo(FieldByName('Projeto').asstring)=False)
                 Then Begin
                          Messagedlg('Projeto N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Projeto.TabelaparaObjeto;
        End;
        If(FieldByName('CorPerfilado').asstring<>'')
        Then Begin
                 If (Self.CorPerfilado.LocalizaCodigo(FieldByName('CorPerfilado').asstring)=False)
                 Then Begin
                          Messagedlg('CorPerfilado N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CorPerfilado.TabelaparaObjeto;
        End;
        If(FieldByName('CorFerragem').asstring<>'')
        Then Begin
                 If (Self.CorFerragem.LocalizaCodigo(FieldByName('CorFerragem').asstring)=False)
                 Then Begin
                          Messagedlg('CorFerragem N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CorFerragem.TabelaparaObjeto;
        End;
        If(FieldByName('CorVidro').asstring<>'')
        Then Begin
                 If (Self.CorVidro.LocalizaCodigo(FieldByName('CorVidro').asstring)=False)
                 Then Begin
                          Messagedlg('CorVidro N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CorVidro.TabelaparaObjeto;
        End;

        If(FieldByName('CorKitBox').asstring<>'')
        Then Begin
                 If (Self.CorKitBox.LocalizaCodigo(FieldByName('CorKitBox').asstring)=False)
                 Then Begin
                          Messagedlg('CorKitBox N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CorKitBox.TabelaparaObjeto;
        End;

        If(FieldByName('CorDiverso').asstring<>'')
        Then Begin
                 If (Self.CorDiverso.LocalizaCodigo(FieldByName('CorDiverso').asstring)=False)
                 Then Begin
                          Messagedlg('CorDiverso N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CorDiverso.TabelaparaObjeto;
        End;



        Self.Local:=fieldbyname('Local').asstring;
        Self.TipoPedido:=fieldbyname('TipoPedido').asstring;

        Self.ValorTotal    :=fieldbyname('ValorTotal').asstring;
        Self.ValorAcrescimo:=fieldbyname('ValorAcrescimo').asstring;
        Self.ValorDesconto :=fieldbyname('ValorDesconto').asstring;
        Self.ValorFinal    :=fieldbyname('ValorFinal').asstring;
        Self.PedidoProjetoPrincipal:=fieldbyname('PedidoProjetoPrincipal').AsString;
        Self.observacao:=fieldbyname('observacao').asstring;

        result:=True;
     End;
end;


Procedure TObjPEDIDO_PROJ_PEDIDO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Pedido').asstring:=Self.Pedido.GET_CODIGO;
        ParamByName('Projeto').asstring:=Self.Projeto.GET_CODIGO;
        ParamByName('CorPerfilado').asstring:=Self.CorPerfilado.GET_CODIGO;
        ParamByName('CorFerragem').asstring:=Self.CorFerragem.GET_CODIGO;
        ParamByName('CorVidro').asstring:=Self.CorVidro.GET_CODIGO;
        ParamByName('Local').asstring:=Self.Local;
        ParamByName('TipoPedido').asstring:=Self.TipoPedido;
        ParamByName('CorKitBox').AsString:=Self.CorKitBox.Get_Codigo;
        ParamByName('ValorTotal').asstring     := virgulaparaponto(Self.ValorTotal)     ;
        ParamByName('ValorAcrescimo').asstring :=  virgulaparaponto(Self.ValorAcrescimo) ;
        ParamByName('ValorDesconto').asstring  :=  virgulaparaponto(Self.ValorDesconto)  ;
        ParamByName('CorDiverso').AsString:=CorDiverso.Get_Codigo;
        ParamByName('PedidoProjetoPrincipal').AsString:=Self.PedidoProjetoPrincipal;
        ParamByName('observacao').asstring:=Self.observacao;
  End;
End;

//***********************************************************************

function TObjPEDIDO_PROJ_PEDIDO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin

    result:=False;

    if (Self.VerificaBrancos=True)
    Then exit;

    if (Self.VerificaNumericos=False)
    Then Exit;

    if (Self.VerificaData=False)
    Then Exit;

    if (Self.VerificaFaixa=False)
    Then Exit;

    if (Self.VerificaRelacionamentos=False)
    then Exit;


    If Self.LocalizaCodigo(Self.CODIGO)=False
    Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
    End
    Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
    End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
    Self.ObjetoParaTabela;

    Try
      Self.Objquery.ExecSQL;
    Except
        if (Self.Status=dsInsert)
        Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
        Else Messagedlg('Erro na  tentativa de Editar ',mterror,[mbok],0);
        exit;
    End;

    if ComCommit=True
    Then FDataModulo.IBTransaction.CommitRetaining;

    Self.status          :=dsInactive;
    result:=True;
    
end;

procedure TObjPEDIDO_PROJ_PEDIDO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Pedido.ZerarTabela;
        Projeto.ZerarTabela;
        CorPerfilado.ZerarTabela;
        CorFerragem.ZerarTabela;
        CorVidro.ZerarTabela;
        CorKitBox.ZerarTabela;
        Local:='';
        TipoPedido:='';
        Self.ValorTotal:='';
        Self.ValorAcrescimo:='';
        Self.ValorDesconto:='';
        Self.ValorFinal:='';
        Self.CorDiverso.ZerarTabela;
        PedidoProjetoPrincipal:='';
        observacao:='';
        
     End;
end;

Function TObjPEDIDO_PROJ_PEDIDO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';

      if (Self.ValorTotal='')
      Then Mensagem:=Mensagem+'/ Valor Total';

      if (Self.ValorDesconto='')
      Then Mensagem:=Mensagem+'/ Valor de Desconto';

      if (Self.ValorAcrescimo='')
      Then Mensagem:=Mensagem+'/ Valor de Acr�scimo';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPEDIDO_PROJ_PEDIDO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Pedido.LocalizaCodigo(Self.Pedido.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Pedido n�o Encontrado!';

      If (Self.Projeto.LocalizaCodigo(Self.Projeto.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Projeto n�o Encontrado!';

      If (Self.CorPerfilado.LocalizaCodigo(Self.CorPerfilado.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ CorPerfilado n�o Encontrado!';

      If (Self.CorFerragem.LocalizaCodigo(Self.CorFerragem.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ CorFerragem n�o Encontrado!';

      If (Self.CorVidro.LocalizaCodigo(Self.CorVidro.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ CorVidro n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;

     result:=true;
End;

function TObjPEDIDO_PROJ_PEDIDO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.Pedido.Get_Codigo<>'')
        Then Strtoint(Self.Pedido.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Pedido';
     End;
     try
        If (Self.Projeto.Get_Codigo<>'')
        Then Strtoint(Self.Projeto.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Projeto';
     End;
     try
        If (Self.CorPerfilado.Get_Codigo<>'')
        Then Strtoint(Self.CorPerfilado.Get_Codigo);
     Except
           Mensagem:=mensagem+'/CorPerfilado';
     End;
     try
        If (Self.CorFerragem.Get_Codigo<>'')
        Then Strtoint(Self.CorFerragem.Get_Codigo);
     Except
           Mensagem:=mensagem+'/CorFerragem';
     End;
     try
        If (Self.CorVidro.Get_Codigo<>'')
        Then Strtoint(Self.CorVidro.Get_Codigo);
     Except
           Mensagem:=mensagem+'/CorVidro';
     End;

     Try
        strtofloat(Self.ValorTotal);
     Except
           Mensagem:=Mensagem+'Valor Total';
     End;

     Try
        strtofloat(Self.ValorDesconto);
     Except
           Mensagem:=Mensagem+'Valor de Desconto';
     End;

     Try
        strtofloat(Self.ValorAcrescimo);
     Except
           Mensagem:=Mensagem+'Valor Acr�scimo';
     End;


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPEDIDO_PROJ_PEDIDO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPEDIDO_PROJ_PEDIDO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPEDIDO_PROJ_PEDIDO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PEDIDO_PROJ vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;  
           SQL.ADD('Select Codigo,Pedido,Projeto,CorPerfilado,CorFerragem,CorVidro');
           SQL.ADD(',Local,TipoPedido,CorKitBox,ValorTotal,ValorDesconto,ValorAcrescimo,ValorFinal, CorDiverso,PedidoProjetoPrincipal,observacao');
           SQL.ADD('from  TABPEDIDO_PROJ');
           SQL.ADD('WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPEDIDO_PROJ_PEDIDO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
        End
        Else result:=false;
     Except
           on e:exception do
           Begin
               mensagemerro(e.message);
               result:=false;
           End;
     End;
end;


constructor TObjPEDIDO_PROJ_PEDIDO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDataSource.DataSet:=Self.ObjQueryPesquisa;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Pedido:=TOBJPEDIDO_PEDIDO.create;
        Self.Projeto:=TOBJPROJETO.create;
        Self.CorPerfilado:=TOBJCOR.create;
        Self.CorFerragem:=TOBJCOR.create;
        Self.CorVidro:=TOBJCOR.create;
        Self.CorKitBox:=TOBJCor.Create;
        Self.CorDiverso:=TObjCOR.Create;

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABPEDIDO_PROJ(Codigo,Pedido,Projeto,CorPerfilado');
                InsertSQL.add(' ,CorFerragem,CorVidro,Local');
                InsertSQL.add(' ,TipoPedido,CorKitBox,ValorTotal,ValorDesconto,ValorAcrescimo, CorDiverso, PedidoProjetoPrincipal,observacao)');
                InsertSQL.add('values (:Codigo,:Pedido,:Projeto,:CorPerfilado,:CorFerragem');
                InsertSQL.add(' ,:CorVidro,:Local,:TipoPedido');
                InsertSQL.add(' ,:CorKitBox,:ValorTotal,:ValorDesconto,:ValorAcrescimo,:CorDiverso,:PedidoProjetoPrincipal,:observacao)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABPEDIDO_PROJ set Codigo=:Codigo,Pedido=:Pedido');
                ModifySQL.add(',Projeto=:Projeto,CorPerfilado=:CorPerfilado,CorFerragem=:CorFerragem');
                ModifySQL.add(',CorVidro=:CorVidro');
                ModifySQL.add(',Local=:Local,TipoPedido=:TipoPedido');
                ModifySQL.add(',CorKitBox=:CorKitBox,ValorTotal=:ValorTotal,ValorDesconto=:ValorDesconto,ValorAcrescimo=:ValorAcrescimo');
                ModifySQl.Add(',CorDiverso=:CorDiverso,PedidoProjetoPrincipal=:PedidoProjetoPrincipal');
                ModifySQL.add(',observacao=:observacao where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABPEDIDO_PROJ where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPEDIDO_PROJ_PEDIDO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPEDIDO_PROJ_PEDIDO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPEDIDO_PROJ');
     Result:=Self.ParametroPesquisa;
end;

function TObjPEDIDO_PROJ_PEDIDO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Projetos do Pedido ';
end;


function TObjPEDIDO_PROJ_PEDIDO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENPEDIDO_PROJ,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPEDIDO_PROJ_PEDIDO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(Self.ObjQueryPesquisa);
    Freeandnil(Self.ObjDataSource);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Pedido.FREE;
    Self.Projeto.FREE;
    Self.CorPerfilado.FREE;
    Self.CorFerragem.FREE;
    Self.CorVidro.FREE;
    Self.CorKitBox.Free;
    Self.CorDiverso.Free;

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPEDIDO_PROJ_PEDIDO.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPEDIDO_PROJ_PEDIDO.RetornaCampoNome: string;
begin
      result:='Descricao';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPEDIDO_PROJ_PEDIDO.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjPEDIDO_PROJ_PEDIDO.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjPEDIDO_PROJ_PEDIDO.Submit_Local(parametro: string);
begin
        Self.Local:=Parametro;
end;
function TObjPEDIDO_PROJ_PEDIDO.Get_Local: string;
begin
        Result:=Self.Local;
end;
procedure TObjPEDIDO_PROJ_PEDIDO.Submit_TipoPedido(parametro: string);
begin
        Self.TipoPedido:=Parametro;
end;
function TObjPEDIDO_PROJ_PEDIDO.Get_TipoPedido: string;
begin
        Result:=Self.TipoPedido;
end;
//CODIFICA GETSESUBMITS


procedure TObjPEDIDO_PROJ_PEDIDO.EdtPedidoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Pedido.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Pedido.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Pedido.Get_Descricao;
End;


procedure TObjPEDIDO_PROJ_PEDIDO.EdtPedidoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Begin
     Self.edtpedidokeydown(sender,key,shift,nil);
end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtPedidoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Pedido.Get_Pesquisa,Self.Pedido.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Pedido.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Pedido.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Pedido.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtPedido_ConCluido_KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FTROCAMATERIALPEDIDOCONCLUIDO.PEDIDO';
            If (FpesquisaLocal.PreparaPesquisa(Self.Pedido.Get_PesquisaConcluido,Self.Pedido.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Pedido.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Pedido.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Pedido.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjPEDIDO_PROJ_PEDIDO.EdtProjetoExit(Sender: TObject;Var PEdtCodigo:TEdit;   LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Projeto.localizaReferencia (TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     Self.Projeto.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Projeto.Get_Descricao;
     PEdtCodigo.Text:=Self.Projeto.Get_Codigo;

End;


procedure TObjPEDIDO_PROJ_PEDIDO.EdtProjetoKeyDown(Sender: TObject;Var PEdtCodigo :TEdit;  var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal :TFpesquisaProjeto;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=TFpesquisaProjeto.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Projeto.Get_Pesquisa,Self.Projeto.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Projeto.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Projeto.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorPerfiladoExit(Sender: TObject;Var PEdtCodigo :TEdit;  LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.CorPerfilado.LocalizaReferenciaCorPerfilado(TEdit(Sender).text)=False)
     Then Begin
               MensagemErro('Cor n�o encontrada em nenhum Perfilado cadastrado.');
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               TEdit(Sender).SetFocus;
               exit;
     End;
     Self.CorPerfilado.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.CorPerfilado.Get_Descricao;
     PEdtCodigo.Text:=Self.CorPerfilado.Get_Codigo;

End;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorPerfiladoKeyDown(Sender: TObject; Var PEdtCodigo:Tedit;var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CorPerfilado.Get_PesquisaCorPErfilado,Self.CorPerfilado.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CorPerfilado.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CorPerfilado.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorPerfiladoKeyDown(Sender: TObject; Var PEdtCodigo:Tedit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel;PcodigoProjeto:string);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <> vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CorPerfilado.Get_PesquisaCorPErfilado(PcodigoProjeto),Self.CorPerfilado.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CorPerfilado.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CorPerfilado.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorFerragemExit(Sender: TObject;Var PEdtCodigo:TEdit; LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.CorFerragem.LocalizaReferenciaCorFerragem(TEdit(Sender).text)=False)
     Then Begin
               MensagemErro('Cor n�o encontrada em nenhuma Ferragem cadastrada.');
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               TEdit(Sender).SetFocus;
               exit;
     End;
     Self.CorFerragem.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.CorFerragem.Get_Descricao;
     PEdtCodigo.Text:=Self.CorFerragem.Get_Codigo;
//     TEdit(Sender).Color:=StringToColor(Self.CorFerragem.Get_CodigoCorDelphi);
End;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorFerragemKeyDown(Sender: TObject; Var PEdtCodigo:TEdit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel;PcodigoProjeto:string);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CorFerragem.Get_PesquisaCorFerragem(pcodigoprojeto),Self.CorFerragem.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CorFerragem.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CorFerragem.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorFerragemKeyDown(Sender: TObject; Var PEdtCodigo:TEdit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CorFerragem.Get_PesquisaCorFerragem,Self.CorFerragem.Get_TituloPesquisa,Nil)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CorFerragem.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CorFerragem.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorFerragemPraTrocaKeyDown(Sender: TObject; Var PEdtCodigo:TEdit;var Key: Word;Shift: TShiftState;Ferragem:string);
var
   FpesquisaLocal:Tfpesquisa;
   sqldepesquisa:string;
begin

     If (key <>vk_f9)
     Then exit;
     sqldepesquisa:='select tabcor.codigo as codigocor,tabcor.descricao,tabcor.referencia,tabferragemcor.* from tabferragemcor join tabcor on tabcor.codigo=tabferragemcor.cor  where ferragem='+Ferragem;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FTROCAMATERIALPEDIDOCONCLUIDO.BTALTERAR';
            If (FpesquisaLocal.PreparaPesquisa(sqldepesquisa,Self.CorFerragem.Get_TituloPesquisa,Nil)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigocor').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;



procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorVidroExit(Sender: TObject;Var PedtCodigo:TEdit; LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.CorVidro.LocalizaReferenciaCorVidro(TEdit(Sender).text)=False)
     Then Begin
               MensagemErro('Cor n�o encontrada em nenhum Vidro cadastrado.');
               TEdit(Sender).text:='';
               PedtCodigo.Text:='';
               TEdit(Sender).SetFocus;
               exit;
     End;
     Self.CorVidro.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.CorVidro.Get_Descricao;
     PedtCodigo.Text:=Self.CorVidro.Get_Codigo;



End;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorVidroKeyDown(Sender: TObject; Var PEdtCodigo:TEdit; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel;Pprojeto:string);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CorVidro.Get_PesquisaCorVidro(pprojeto) ,Self.CorVidro.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CorVidro.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CorVidro.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorVidroKeyDown(Sender: TObject; Var PEdtCodigo:TEdit; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CorVidro.Get_PesquisaCorVidro ,Self.CorVidro.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CorVidro.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CorVidro.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN





procedure TObjPEDIDO_PROJ_PEDIDO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPEDIDO_PROJ';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjPEDIDO_PROJ_PEDIDO.ResgataPEDIDO_PROJ(PPedido: string);
begin
       With Self.ObjQueryPesquisa do
       Begin
           Close;
           SQL.Clear;
           Sql.add('Select TabProjeto.Referencia,');
           Sql.add('TabPedido_Proj.LOcal,');
           Sql.add('TabPedido_Proj.ValorTotal,');
           Sql.add('TCORPERFILADO.DESCRICAO as NOMECORPERFILADO,');
           Sql.add('TCORFERRAGEM.DESCRICAO as NOMECORFERRAGEM,');
           Sql.add('TCORVIDRO.DESCRICAO as NOMECORVIDRO,');
           SQl.Add('TCORKITBOX.DESCRICAO as NOMECORKITBOX,');
           SQl.Add('TCORDIVERSO.DESCRICAO as NOMECORDIVERSO,');
           Sql.add('TabPedido_Proj.TipoPedido,');
           Sql.add('TabPedido_Proj.ValorDesconto,');
           Sql.add('TabPedido_Proj.ValorAcrescimo,');
           Sql.add('TabPedido_Proj.ValorFinal,');
           Sql.add('TabPedido_Proj.Codigo as PedidoProjeto,');
           Sql.add('TabPedido_Proj.PedidoProjetoPrincipal as Projeto_Principal,TabPedido_Proj.observacao');
           Sql.add('from tabpedido_proj');
           Sql.add('left join Tabcor TCORPERFILADO on tabpedido_proj.corperfilado=TCORPERFILADO.codigo');
           Sql.add('left join Tabcor TCORFERRAGEM on tabpedido_proj.corferragem=TCORFERRAGEM.codigo');
           Sql.add('left join Tabcor TCORVIDRO on tabpedido_proj.corvidro=TCORVIDRO.codigo');
           Sql.add('left join Tabcor TCORKITBOX on tabpedido_proj.corkitbox=TCORKITBOX.codigo');
           Sql.add('left join Tabcor TCORDIVERSO on tabpedido_proj.cordiverso=TCORDIVERSO.codigo');
                                               

           Sql.add('Join TabProjeto on TabProjeto.Codigo = TabPedido_Proj.Projeto');
           Sql.add('Where TabPedido_Proj.Pedido = '+PPEDIDO);



           if (PPedido = '')then
           exit;

           Open;

       end;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorKitBoxExit(Sender: TObject;Var PEdtCodigo :TEdit; LABELNOME: TLABEL);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.CorKitBox.LocalizaReferenciaCorKitBox(TEdit(Sender).text)=False)
     Then Begin
               MensagemErro('Cor n�o encontrada em nenhum KitBox Cadastrado.');
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               TEdit(Sender).SetFocus;
               exit;
     End;
     Self.CorKitBox.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.CorKitBox.Get_Descricao;
     PEdtCodigo.Text:=Self.CorKitBox.Get_Codigo;

end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorKitBoxKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;
  var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CorKitBox.Get_PesquisaCorKItBox,Self.CorKitBox.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CorKitBox.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CorKitBox.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorKitBoxKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;
  var Key: Word; Shift: TShiftState; LABELNOME: Tlabel;pprojeto:string);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CorKitBox.Get_PesquisaCorKItBox(pprojeto),Self.CorKitBox.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CorKitBox.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CorKitBox.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjPEDIDO_PROJ_PEDIDO.Get_ValorAcrescimo: string;
begin
     Result:=Self.ValorAcrescimo;
end;

function TObjPEDIDO_PROJ_PEDIDO.Get_ValorDesconto: string;
begin
     Result:=Self.ValorDesconto;
end;

function TObjPEDIDO_PROJ_PEDIDO.Get_ValorFinal: string;
begin
     Result:=Self.ValorFinal;
end;

function TObjPEDIDO_PROJ_PEDIDO.Get_ValorTotal: string;
begin
     Result:=Self.ValorTotal;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.Submit_ValorAcrescimo(parametro: string);
begin
     Self.ValorAcrescimo:=parametro;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.Submit_ValorDesconto(parametro: string);
begin
     Self.ValorDesconto:=parametro;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.Submit_ValorTotal(parametro: string);
begin
     if (parametro='')
     Then Parametro:='0';
     
     Self.ValorTotal:=parametro;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorDiversoExit(Sender: TObject;
  var PEdtCodigo: TEdit; LABELNOME: TLABEL);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.CorDiverso.LocalizaReferenciaCorDiverso(TEdit(Sender).text)=False)
     Then Begin
               MensagemErro('Cor n�o encontrada em nenhum Diverso cadastrado.');
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               TEdit(Sender).SetFocus;
               exit;
     End;
     Self.CorDiverso.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.CorDiverso.Get_Descricao;
     PEdtCodigo.Text:=Self.CorDiverso.Get_Codigo;

end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorDiversoKeyDown(Sender: TObject; var PEdtCodigo: TEdit; var Key: Word; Shift: TShiftState;
  LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CorDiverso.Get_PesquisaCorDiverso,Self.CorDiverso.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CorDiverso.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CorDiverso.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorDiversoKeyDown(Sender: TObject; var PEdtCodigo: TEdit; var Key: Word; Shift: TShiftState;LABELNOME: Tlabel;PcodigoProjeto:string);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CorDiverso.Get_PesquisaCorDiverso(PcodigoProjeto),Self.CorDiverso.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CorDiverso.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CorDiverso.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjPEDIDO_PROJ_PEDIDO.Get_PesquisaViewProjetoNoRomaneio: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewProjetoNoRomaneio');
     Result:=Self.ParametroPesquisa;

end;

function TObjPEDIDO_PROJ_PEDIDO.Get_Pesquisaview: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewPedidoProjeto');
     Result:=Self.ParametroPesquisa;
end;

function TObjPEDIDO_PROJ_PEDIDO.Get_Pesquisaview(ppedido: string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewPedidoProjeto ');

     if (Ppedido<>'')
     Then Self.ParametroPesquisa.add('where Pedido='+ppedido);
     
     Result:=Self.ParametroPesquisa;
end;


function TObjPEDIDO_PROJ_PEDIDO.Get_PedidoProjetoPrincipal: string;
begin
    Result:=Self.PedidoProjetoPrincipal;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.Submit_PedidoProjetoPrincipal(parametro: string);
begin
    Self.PedidoProjetoPrincipal:=parametro;
end;


Function TObjPEDIDO_PROJ_PEDIDO.VerificaSePedidoProjetoPrincipal(PPedidoProjeto: string): Boolean;
Var QueryLocal:TIBQuery;
begin
     try
           Result:=false;
           try
                 QueryLocal:=TIBQuery.Create(nil);
                 QueryLocal.Database:=FDataModulo.IBDatabase;
           except
                MensagemErro('Erro ao tentar criar a Query Local');
                exit;
           end;

           With  QueryLocal do
           Begin
                Close;
                Sql.Clear;
                Sql.Add('Select Codigo from TabPedido_proj');
                Sql.Add('Where PedidoProjetoPrincipal = '+PPedidoProjeto);
                Open;

                if (RecordCount > 0)
                then Result:=true
                else Result:=false;

           end;          

     finally
           FreeAndNil(QueryLocal);

     end;

end;


function TObjPEDIDO_PROJ_PEDIDO.Excluir_sem_produtos(
  Ppedido: string): boolean;
var
QTemp:Tibquery;
conta:integer;
begin
     Result:=False;
     //Esse procedimento exclui os pedidos projetos em branco ou seja
     //que n�o possuem nenhum produto ligado a ele

     Try
        QTemp:=Tibquery.create(nil);
        QTemp.database:=Fdatamodulo.IBDatabase;
     Except
        MensagemErro('Erro na tentativa de criar a Query Tempor�ria');
        exit;
     End;

     Try
       With Self.Objquery do
       Begin
            close;
            sql.clear;
            sql.add('Select codigo from tabpedido_proj where pedido='+ppedido);
            open;


            while not(eof) do
            Begin
                 conta:=0;

                 Qtemp.close;
                 Qtemp.sql.clear;
                 Qtemp.sql.add('Select Contagem from ProccontaprodutosPP('+Fieldbyname('codigo').asstring+')');
                 Qtemp.open;
                 conta:=qTEMP.fieldbyname('contaGEM').asinteger;

                 if (Conta=0)
                 then begin
                            Qtemp.close;
                            Qtemp.sql.clear;
                            Qtemp.sql.add('Delete from TabcomissaoColocador where pedidoprojeto='+Fieldbyname('codigo').asstring);
                            try
                                Qtemp.execsql;
                            Except
                                Mensagemerro('Erro na tentativa de excluir a comiss�o do colocador do  pedido projeto='+Fieldbyname('codigo').asstring+' em branco');
                            End;

                            Qtemp.close;
                            Qtemp.sql.clear;
                            Qtemp.sql.add('Delete from tabpedido_proj where codigo='+Fieldbyname('codigo').asstring);
                            try
                                Qtemp.execsql;
                            Except
                                Mensagemerro('Erro na tentativa de excluir o pedido projeto='+Fieldbyname('codigo').asstring);
                            End;
                 End;
                 
                 next;
            End;

            result:=true;
       End;

     Finally
            Freeandnil(qtemp);
     end;


end;

function TObjPEDIDO_PROJ_PEDIDO.Get_observacao: String;
begin
     Result:=Self.Observacao;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.Submit_observacao(parametro: string);
begin
     Self.Observacao:=parametro;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorPerfiladoPraTrocaKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;Perfilado:string);
var
   FpesquisaLocal:Tfpesquisa;
   sqldepesquisa:string;
begin

     If (key <>vk_f9)
     Then exit;
     sqldepesquisa:='select tabcor.codigo as codigocor,tabcor.descricao,tabcor.referencia,tabperfiladocor.* from tabperfiladocor join tabcor on tabcor.codigo=tabperfiladocor.cor  where perfilado='+Perfilado;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FTROCAMATERIALPEDIDOCONCLUIDO.BTALTERAR';
            If (FpesquisaLocal.PreparaPesquisa(sqldepesquisa,Self.CorFerragem.Get_TituloPesquisa,Nil)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigocor').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorKitBoxPraTrocaKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;KitBox:string);
var
   FpesquisaLocal:Tfpesquisa;
   sqldepesquisa:string;
begin

     If (key <>vk_f9)
     Then exit;
     sqldepesquisa:='select tabcor.codigo as codigocor,tabcor.descricao,tabcor.referencia,tabKitBoxcor.* from tabKitBoxcor join tabcor on tabcor.codigo=tabKitBoxcor.cor  where KitBox='+KitBox;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FTROCAMATERIALPEDIDOCONCLUIDO.BTALTERAR';
            If (FpesquisaLocal.PreparaPesquisa(sqldepesquisa,Self.CorFerragem.Get_TituloPesquisa,Nil)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigocor').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorDiversoPraTrocaKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;Diverso:string);
var
   FpesquisaLocal:Tfpesquisa;
   sqldepesquisa:string;
begin

     If (key <>vk_f9)
     Then exit;
     sqldepesquisa:='select tabcor.codigo as codigocor,tabcor.descricao,tabcor.referencia,tabDiversocor.* from tabDiversocor join tabcor on tabcor.codigo=tabDiversocor.cor  where Diverso='+Diverso;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FTROCAMATERIALPEDIDOCONCLUIDO.BTALTERAR';
            If (FpesquisaLocal.PreparaPesquisa(sqldepesquisa,Self.CorFerragem.Get_TituloPesquisa,Nil)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigocor').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPEDIDO_PROJ_PEDIDO.EdtCorVidroPraTrocaKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;Shift: TShiftState;Vidro:string);
var
   FpesquisaLocal:Tfpesquisa;
   sqldepesquisa:string;
begin

     If (key <>vk_f9)
     Then exit;
     sqldepesquisa:='select tabcor.codigo as codigocor,tabcor.descricao,tabcor.referencia,tabVidrocor.* from tabVidrocor join tabcor on tabcor.codigo=tabVidrocor.cor  where Vidro='+Vidro;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FTROCAMATERIALPEDIDOCONCLUIDO.BTALTERAR';
            If (FpesquisaLocal.PreparaPesquisa(sqldepesquisa,Self.CorFerragem.Get_TituloPesquisa,Nil)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigocor').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

end.



