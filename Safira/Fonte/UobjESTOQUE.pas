unit UobjESTOQUE;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
;
//USES_INTERFACE


Type
  TMateriaisEstoque = class
  public
    MaterialTipo: string;
    MaterialCodigo: integer;
  end;


   TObjESTOQUE=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:STRING) :boolean;

                Function    LocalizaporCampo(Pcampo,Pvalor:String):boolean;


                Function    Exclui(Pcodigo:STRING;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :STRING;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:STRING;
                Function  RetornaCampoCodigo:STRING;
                Function  RetornaCampoNome:STRING;
                Procedure Imprime(Pcodigo:STRING);

                Procedure Submit_CODIGO(parametro: STRING);
                Function Get_CODIGO: STRING;
                Procedure Submit_QUANTIDADE(parametro: STRING);
                Function Get_QUANTIDADE: STRING;
                Procedure Submit_DATA(parametro:string );
                Function Get_DATA:string ;
                Procedure Submit_FERRAGEMCOR(parametro: STRING);
                Function Get_FERRAGEMCOR: STRING;
                Procedure Submit_VIDROCOR(parametro: STRING);
                Function Get_VIDROCOR: STRING;
                Procedure Submit_PERFILADOCOR(parametro: STRING);
                Function Get_PERFILADOCOR: STRING;
                Procedure Submit_KITBOXCOR(parametro: STRING);
                Function Get_KITBOXCOR: STRING;
                Procedure Submit_DIVERSOCOR(parametro: STRING);
                Function Get_DIVERSOCOR: STRING;
                Procedure Submit_PERSIANAGRUPODIAMETROCOR(parametro: STRING);
                Function Get_PERSIANAGRUPODIAMETROCOR: STRING;
                Procedure Submit_PEDIDOPROJETOROMANEIO(parametro: STRING);
                Function Get_PEDIDOPROJETOROMANEIO: STRING;
                Procedure Submit_FERRAGEM_PP(parametro: STRING);
                Function Get_FERRAGEM_PP: STRING;
                Procedure Submit_VIDRO_pp(parametro: STRING);
                Function Get_VIDRO_pp: STRING;
                Procedure Submit_PERFILADO_PP(parametro: STRING);
                Function Get_PERFILADO_PP: STRING;
                Procedure Submit_KITBOX_PP(parametro: STRING);
                Function Get_KITBOX_PP: STRING;
                Procedure Submit_DIVERSO_PP(parametro: STRING);
                Function Get_DIVERSO_PP: STRING;
                Procedure Submit_PERSIANA_PP(parametro: STRING);
                Function Get_PERSIANA_PP: STRING;
                Procedure Submit_ENTRADAPRODUTOS(parametro: STRING);
                Function Get_ENTRADAPRODUTOS: STRING;
                Procedure Submit_FERRAGEM_EP(parametro: STRING);
                Function Get_FERRAGEM_EP: STRING;
                Procedure Submit_VIDRO_EP(parametro: STRING);
                Function Get_VIDRO_EP: STRING;
                Procedure Submit_PERFILADO_EP(parametro: STRING);
                Function Get_PERFILADO_EP: STRING;
                Procedure Submit_KITBOX_EP(parametro: STRING);
                Function Get_KITBOX_EP: STRING;
                Procedure Submit_DIVERSO_EP(parametro: STRING);
                Function Get_DIVERSO_EP: STRING;
                Procedure Submit_PERSIANA_EP(parametro: STRING);
                Function Get_PERSIANA_EP: STRING;
                Procedure Submit_OBSERVACAO(parametro: STRING);
                Function Get_OBSERVACAO: STRING;
                procedure Submit_NotaFiscal(parametro:string);
                function Get_NotaFiscal:string;
                //CODIFICA DECLARA GETSESUBMITS
                function ExcluiPedidoProjetoRomaneio(PCodigo: String;PComCommit:boolean): boolean;
                function AtualizaEstoqueCorMateriais:boolean;


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:STRING;
               QUANTIDADE:STRING;
               DATA:string;
               //Chaves Estrangeiras dos Produtos
               FERRAGEMCOR:STRING;
               VIDROCOR:STRING;
               PERFILADOCOR:STRING;
               KITBOXCOR:STRING;
               DIVERSOCOR:STRING;
               PERSIANAGRUPODIAMETROCOR:STRING;

               //Baixa de Estoque pelo Romaneio
               PEDIDOPROJETOROMANEIO:STRING;
               FERRAGEM_PP:STRING;
               VIDRO_pp:STRING;
               PERFILADO_PP:STRING;
               KITBOX_PP:STRING;
               DIVERSO_PP:STRING;
               PERSIANA_PP:STRING;
               //Entrada de Estoque pela Compra
               ENTRADAPRODUTOS:STRING;
               FERRAGEM_EP:STRING;
               VIDRO_EP:STRING;
               PERFILADO_EP:STRING;
               KITBOX_EP:STRING;
               DIVERSO_EP:STRING;
               PERSIANA_EP:STRING;
               //*******************************
               OBSERVACAO:STRING;
               NOTAFISCAL:string;

               NOMETABELA : string;
               NOMECAMPO  : string;
               VALORCAMPO : string;
//CODIFICA VARIAVEIS PRIVADAS

               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;

               procedure AtualizaEstoqueCor;
               function retornaEstoque:Currency;
               procedure retornaMateriaisEstoque( pedidoprojetoromaneio:string; var lista: TList);
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;

Function  TObjESTOQUE.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.QUANTIDADE:=fieldbyname('QUANTIDADE').asstring;
        Self.DATA:=fieldbyname('DATA').asstring;
        Self.FERRAGEMCOR:=fieldbyname('FERRAGEMCOR').asstring;
        Self.VIDROCOR:=fieldbyname('VIDROCOR').asstring;
        Self.PERFILADOCOR:=fieldbyname('PERFILADOCOR').asstring;
        Self.KITBOXCOR:=fieldbyname('KITBOXCOR').asstring;
        Self.DIVERSOCOR:=fieldbyname('DIVERSOCOR').asstring;
        Self.PERSIANAGRUPODIAMETROCOR:=fieldbyname('PERSIANAGRUPODIAMETROCOR').asstring;
        Self.PEDIDOPROJETOROMANEIO:=fieldbyname('PEDIDOPROJETOROMANEIO').asstring;
        Self.FERRAGEM_PP:=fieldbyname('FERRAGEM_PP').asstring;
        Self.VIDRO_pp:=fieldbyname('VIDRO_pp').asstring;
        Self.PERFILADO_PP:=fieldbyname('PERFILADO_PP').asstring;
        Self.KITBOX_PP:=fieldbyname('KITBOX_PP').asstring;
        Self.DIVERSO_PP:=fieldbyname('DIVERSO_PP').asstring;
        Self.PERSIANA_PP:=fieldbyname('PERSIANA_PP').asstring;
        Self.ENTRADAPRODUTOS:=fieldbyname('ENTRADAPRODUTOS').asstring;
        Self.FERRAGEM_EP:=fieldbyname('FERRAGEM_EP').asstring;
        Self.VIDRO_EP:=fieldbyname('VIDRO_EP').asstring;
        Self.PERFILADO_EP:=fieldbyname('PERFILADO_EP').asstring;
        Self.KITBOX_EP:=fieldbyname('KITBOX_EP').asstring;
        Self.DIVERSO_EP:=fieldbyname('DIVERSO_EP').asstring;
        Self.PERSIANA_EP:=fieldbyname('PERSIANA_EP').asstring;
        Self.OBSERVACAO:=fieldbyname('OBSERVACAO').asstring;
        Self.NOTAFISCAL:=fieldbyname('NOTAFISCAL').AsString;
        result:=True;
     End;
end;


Procedure TObjESTOQUE.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('QUANTIDADE').asstring:=virgulaparaponto(Self.QUANTIDADE);
        ParamByName('DATA').asstring:=Self.DATA;
        ParamByName('FERRAGEMCOR').asstring:=Self.FERRAGEMCOR;
        ParamByName('VIDROCOR').asstring:=Self.VIDROCOR;
        ParamByName('PERFILADOCOR').asstring:=Self.PERFILADOCOR;
        ParamByName('KITBOXCOR').asstring:=Self.KITBOXCOR;
        ParamByName('DIVERSOCOR').asstring:=Self.DIVERSOCOR;
        ParamByName('PERSIANAGRUPODIAMETROCOR').asstring:=Self.PERSIANAGRUPODIAMETROCOR;
        ParamByName('PEDIDOPROJETOROMANEIO').asstring:=Self.PEDIDOPROJETOROMANEIO;
        ParamByName('FERRAGEM_PP').asstring:=Self.FERRAGEM_PP;
        ParamByName('VIDRO_pp').asstring:=Self.VIDRO_pp;
        ParamByName('PERFILADO_PP').asstring:=Self.PERFILADO_PP;
        ParamByName('KITBOX_PP').asstring:=Self.KITBOX_PP;
        ParamByName('DIVERSO_PP').asstring:=Self.DIVERSO_PP;
        ParamByName('PERSIANA_PP').asstring:=Self.PERSIANA_PP;
        ParamByName('ENTRADAPRODUTOS').asstring:=Self.ENTRADAPRODUTOS;
        ParamByName('FERRAGEM_EP').asstring:=Self.FERRAGEM_EP;
        ParamByName('VIDRO_EP').asstring:=Self.VIDRO_EP;
        ParamByName('PERFILADO_EP').asstring:=Self.PERFILADO_EP;
        ParamByName('KITBOX_EP').asstring:=Self.KITBOX_EP;
        ParamByName('DIVERSO_EP').asstring:=Self.DIVERSO_EP;
        ParamByName('PERSIANA_EP').asstring:=Self.PERSIANA_EP;
        ParamByName('OBSERVACAO').asstring:=Self.OBSERVACAO;
        ParamByName('NOTAFISCAL').AsString:=self.NOTAFISCAL;
//CODIFICA OBJETOPARATABELA


  End;
End;

//***********************************************************************

function TObjESTOQUE.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


  If Self.LocalizaCodigo(Self.CODIGO)=False
  Then Begin
           if(Self.Status=dsedit)
           Then Begin
                     Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                     exit;
           End;
  End
  Else Begin
           if(Self.Status=dsinsert)
           Then Begin
                     Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                     exit;
           End;
  End;

  if Self.status=dsinsert
  Then Begin
            Self.Objquery.SQL.Clear;
            Self.Objquery.SQL.text:=Self.InsertSql.Text;
            if (Self.Codigo='0')
            Then Self.codigo:=Self.Get_NovoCodigo;
  End
  Else Begin
            if (Self.Status=dsedit)
            Then Begin
                      Self.Objquery.SQL.Clear;
                      Self.Objquery.SQL.text:=Self.ModifySQl.Text;
            End
            Else Begin
                      Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                      exit;
            End;
  End;
  Self.ObjetoParaTabela;

  Try
    Self.Objquery.ExecSQL;
  Except
       on e:exception do
       begin
             if (Self.Status=dsInsert)
             Then Messagedlg('Erro na  tentativa de Inserir o estoque '+#13+e.message,mterror,[mbok],0)
             Else Messagedlg('Erro na  tentativa de Editar o estoque '+#13+e.message,mterror,[mbok],0);
             exit;
       End;
  End;

  AtualizaEstoqueCor;

  If ComCommit=True Then
    FDataModulo.IBTransaction.CommitRetaining;



  Self.status          :=dsInactive;
  result:=True;
end;

procedure TObjESTOQUE.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        QUANTIDADE:='';
        DATA:='';
        FERRAGEMCOR:='';
        VIDROCOR:='';
        PERFILADOCOR:='';
        KITBOXCOR:='';
        DIVERSOCOR:='';
        PERSIANAGRUPODIAMETROCOR:='';
        PEDIDOPROJETOROMANEIO:='';
        FERRAGEM_PP:='';
        VIDRO_pp:='';
        PERFILADO_PP:='';
        KITBOX_PP:='';
        DIVERSO_PP:='';
        PERSIANA_PP:='';
        ENTRADAPRODUTOS:='';
        FERRAGEM_EP:='';
        VIDRO_EP:='';
        PERFILADO_EP:='';
        KITBOX_EP:='';
        DIVERSO_EP:='';
        PERSIANA_EP:='';
        OBSERVACAO:='';
        NOTAFISCAL:='';

        //campos de controle
        NOMETABELA := '';
        NOMECAMPO := '';
        VALORCAMPO := '';
     End;
end;

Function TObjESTOQUE.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS


      if ((FERRAGEMCOR='') and (VIDROCOR='') and (PERFILADOCOR='')
      and (KITBOXCOR='') and (DIVERSOCOR='') and (PERSIANAGRUPODIAMETROCOR=''))
      Then Mensagem:=mensagem+'/Pelo menos um material deve ser escolhido para alterar o estoque';


     if (PEDIDOPROJETOROMANEIO<>'')
     then Begin
               if ((FERRAGEM_PP='') and (VIDRO_PP='') and (PERFILADO_PP='')
                  and (KITBOX_PP='') and (DIVERSO_PP='') and (PERSIANA_PP=''))
               Then Mensagem:=mensagem+'/Pelo menos um material deve ser preenchido no Romaneio para alterar o estoque';
     End;

     if (ENTRADAPRODUTOS<>'')
     then Begin
               if ((FERRAGEM_EP='') and (VIDRO_EP='') and (PERFILADO_EP='')
                  and (KITBOX_EP='') and (DIVERSO_EP='') and (PERSIANA_EP=''))
               Then Mensagem:=mensagem+'/Pelo menos um material deve ser preenchido na Entrada de Produtos para alterar o estoque';
     End;

     
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjESTOQUE.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
soma:integer;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS

     soma:=0;

     if (FERRAGEMCOR<>'')
     then inc(soma,1);

     if (VIDROCOR<>'')
     then inc(soma,1);

     if (PERFILADOCOR<>'')
     then inc(soma,1);


     if(KITBOXCOR<>'')
     then inc(soma,1);

     if(DIVERSOCOR<>'')
     then inc(soma,1);

     if (PERSIANAGRUPODIAMETROCOR<>'')
     then inc(soma,1);

     if (Soma>1)
     Then Mensagem:=Mensagem+'/Somente pode ser escolhido um material para alterar o estoque ';


     if (PEDIDOPROJETOROMANEIO<>'') and (self.ENTRADAPRODUTOS<>'')
     Then mensagem:=mensagem+'/N�o � poss�vel lan�ar altera��o de estoque para Romaneio e Entrada ao mesmo tempo';


     if (PEDIDOPROJETOROMANEIO<>'')
     then Begin
               soma:=0;

               if (FERRAGEM_PP<>'')
               then inc(soma,1);

               if (VIDRO_pp<>'')
               then inc(soma,1);

               if (PERFILADO_PP<>'')
               then inc(soma,1);

               if (KITBOX_PP<>'')
               then inc(soma,1);

               if (DIVERSO_PP<>'')
               then inc(soma,1);

               if (PERSIANA_PP<>'')
               then inc(soma,1);

                 if (Soma>1)
                 Then Mensagem:=Mensagem+'/Somente pode ser escolhido um material do Romaneio para alterar o estoque ';
     End;

     if (ENTRADAPRODUTOS<>'')
     then Begin
               soma:=0;

               if (FERRAGEM_EP<>'')
               then inc(soma,1);

               if (VIDRO_EP<>'')
               then inc(soma,1);

               if (PERFILADO_EP<>'')
               then inc(soma,1);

               if (KITBOX_EP<>'')
               then inc(soma,1);

               if (DIVERSO_EP<>'')
               then inc(soma,1);

               if (PERSIANA_EP<>'')
               then inc(soma,1);

               if (Soma>1)
               Then Mensagem:=Mensagem+'/Somente pode ser escolhido um material da Entrada para alterar o estoque ';
     End;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
     End;

     result:=true;
End;

function TObjESTOQUE.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
     try
        Strtofloat(Self.QUANTIDADE);
     Except
           Mensagem:=mensagem+'/QUANTIDADE';
     End;
     try
        if (Self.FERRAGEMCOR<>'')
        Then Strtoint(Self.FERRAGEMCOR);
     Except
           Mensagem:=mensagem+'/FERRAGEMCOR';
     End;
     try
        if (Self.VIDROCOR<>'')
        Then Strtoint(Self.VIDROCOR);
     Except
           Mensagem:=mensagem+'/VIDROCOR';
     End;
     try
        if (Self.PERFILADOCOR<>'')
        Then Strtoint(Self.PERFILADOCOR);
     Except
           Mensagem:=mensagem+'/PERFILADOCOR';
     End;
     try
        if (Self.KITBOXCOR<>'')
        Then Strtoint(Self.KITBOXCOR);
     Except
           Mensagem:=mensagem+'/KITBOXCOR';
     End;
     try
        if (Self.DIVERSOCOR<>'')
        Then Strtoint(Self.DIVERSOCOR);
     Except
           Mensagem:=mensagem+'/DIVERSOCOR';
     End;
     try
        if (Self.PERSIANAGRUPODIAMETROCOR<>'')
        Then Strtoint(Self.PERSIANAGRUPODIAMETROCOR);
     Except
           Mensagem:=mensagem+'/PERSIANAGRUPODIAMETROCOR';
     End;
     try
        if (Self.PEDIDOPROJETOROMANEIO<>'')
        Then Strtoint(Self.PEDIDOPROJETOROMANEIO);
     Except
           Mensagem:=mensagem+'/PEDIDOPROJETOROMANEIO';
     End;
     try
        if (Self.FERRAGEM_PP<>'')
        Then Strtoint(Self.FERRAGEM_PP);
     Except
           Mensagem:=mensagem+'/FERRAGEM_PP';
     End;
     try
        if (Self.VIDRO_pp<>'')
        Then Strtoint(Self.VIDRO_pp);
     Except
           Mensagem:=mensagem+'/VIDRO_pp';
     End;
     try
        if (Self.PERFILADO_PP<>'')
        Then Strtoint(Self.PERFILADO_PP);
     Except
           Mensagem:=mensagem+'/PERFILADO_PP';
     End;
     try
        if (Self.KITBOX_PP<>'')
        then Strtoint(Self.KITBOX_PP);
     Except
           Mensagem:=mensagem+'/KITBOX_PP';
     End;
     try
        if (Self.DIVERSO_PP<>'')
        Then Strtoint(Self.DIVERSO_PP);
     Except
           Mensagem:=mensagem+'/DIVERSO_PP';
     End;
     try
        if (Self.PERSIANA_PP<>'')
        Then Strtoint(Self.PERSIANA_PP);
     Except
           Mensagem:=mensagem+'/PERSIANA_PP';
     End;
     try
        if (Self.ENTRADAPRODUTOS<>'')
        Then Strtoint(Self.ENTRADAPRODUTOS);
     Except
           Mensagem:=mensagem+'/ENTRADAPRODUTOS';
     End;
     try
        if (Self.FERRAGEM_EP<>'')
        Then Strtoint(Self.FERRAGEM_EP);
     Except
           Mensagem:=mensagem+'/FERRAGEM_EP';
     End;
     try
        if (Self.VIDRO_EP<>'')
        Then Strtoint(Self.VIDRO_EP);
     Except
           Mensagem:=mensagem+'/VIDRO_EP';
     End;
     try
        if (Self.PERFILADO_EP<>'')
        then Strtoint(Self.PERFILADO_EP);
     Except
           Mensagem:=mensagem+'/PERFILADO_EP';
     End;
     try
        if (Self.KITBOX_EP<>'')
        Then Strtoint(Self.KITBOX_EP);
     Except
           Mensagem:=mensagem+'/KITBOX_EP';
     End;
     try
        if (Self.DIVERSO_EP<>'')
        Then Strtoint(Self.DIVERSO_EP);
     Except
           Mensagem:=mensagem+'/DIVERSO_EP';
     End;
     try
        if (Self.PERSIANA_EP<>'')
        Then Strtoint(Self.PERSIANA_EP);
     Except
           Mensagem:=mensagem+'/PERSIANA_EP';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjESTOQUE.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.DATA);
     Except
           Mensagem:=mensagem+'/DATA';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjESTOQUE.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;



function TObjESTOQUE.LocalizaporCampo(Pcampo, Pvalor: String): boolean;
begin
     if (Pcampo='')
     then Begin
               MensagemErro('Escolha o campo que deseja localizar');
               exit;
     End;

     With Self.Objquery do
     Begin
         close;
         Sql.Clear;
         SQL.ADD('Select CODIGO,QUANTIDADE,DATA,FERRAGEMCOR,VIDROCOR,PERFILADOCOR');
         SQL.ADD(' ,KITBOXCOR,DIVERSOCOR,PERSIANAGRUPODIAMETROCOR,PEDIDOPROJETOROMANEIO');
         SQL.ADD(' ,FERRAGEM_PP,VIDRO_pp,PERFILADO_PP,KITBOX_PP,DIVERSO_PP,PERSIANA_PP');
         SQL.ADD(' ,ENTRADAPRODUTOS,FERRAGEM_EP,VIDRO_EP,PERFILADO_EP,KITBOX_EP,DIVERSO_EP');
         SQL.ADD(' ,PERSIANA_EP,OBSERVACAO,NOTAFISCAL');
         SQL.ADD(' from  TABESTOQUE');
         SQL.ADD(' WHERE '+Pcampo+'=:pvalor');
         ParamByName('pvalor').asstring:=pvalor;
         Open;
         If (recordcount>0)
         Then Result:=True
         Else Result:=False;
     End;
end;



function TObjESTOQUE.LocalizaCodigo(parametro: STRING): boolean;//ok
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro ESTOQUE vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,QUANTIDADE,DATA,FERRAGEMCOR,VIDROCOR,PERFILADOCOR');
           SQL.ADD(' ,KITBOXCOR,DIVERSOCOR,PERSIANAGRUPODIAMETROCOR,PEDIDOPROJETOROMANEIO');
           SQL.ADD(' ,FERRAGEM_PP,VIDRO_pp,PERFILADO_PP,KITBOX_PP,DIVERSO_PP,PERSIANA_PP');
           SQL.ADD(' ,ENTRADAPRODUTOS,FERRAGEM_EP,VIDRO_EP,PERFILADO_EP,KITBOX_EP,DIVERSO_EP');
           SQL.ADD(' ,PERSIANA_EP,OBSERVACAO,NOTAFISCAL');
           SQL.ADD(' from  TABESTOQUE');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjESTOQUE.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjESTOQUE.Exclui(Pcodigo: STRING;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjESTOQUE.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABESTOQUE(CODIGO,QUANTIDADE,DATA,FERRAGEMCOR');
                InsertSQL.add(' ,VIDROCOR,PERFILADOCOR,KITBOXCOR,DIVERSOCOR,PERSIANAGRUPODIAMETROCOR');
                InsertSQL.add(' ,PEDIDOPROJETOROMANEIO,FERRAGEM_PP,VIDRO_pp,PERFILADO_PP');
                InsertSQL.add(' ,KITBOX_PP,DIVERSO_PP,PERSIANA_PP,ENTRADAPRODUTOS,FERRAGEM_EP');
                InsertSQL.add(' ,VIDRO_EP,PERFILADO_EP,KITBOX_EP,DIVERSO_EP,PERSIANA_EP');
                InsertSQL.add(' ,OBSERVACAO,NOTAFISCAL)');
                InsertSQL.add('values (:CODIGO,:QUANTIDADE,:DATA,:FERRAGEMCOR,:VIDROCOR');
                InsertSQL.add(' ,:PERFILADOCOR,:KITBOXCOR,:DIVERSOCOR,:PERSIANAGRUPODIAMETROCOR');
                InsertSQL.add(' ,:PEDIDOPROJETOROMANEIO,:FERRAGEM_PP,:VIDRO_pp,:PERFILADO_PP');
                InsertSQL.add(' ,:KITBOX_PP,:DIVERSO_PP,:PERSIANA_PP,:ENTRADAPRODUTOS');
                InsertSQL.add(' ,:FERRAGEM_EP,:VIDRO_EP,:PERFILADO_EP,:KITBOX_EP,:DIVERSO_EP');
                InsertSQL.add(' ,:PERSIANA_EP,:OBSERVACAO,:NOTAFISCAL)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABESTOQUE set CODIGO=:CODIGO,QUANTIDADE=:QUANTIDADE');
                ModifySQL.add(',DATA=:DATA,FERRAGEMCOR=:FERRAGEMCOR,VIDROCOR=:VIDROCOR');
                ModifySQL.add(',PERFILADOCOR=:PERFILADOCOR,KITBOXCOR=:KITBOXCOR,DIVERSOCOR=:DIVERSOCOR');
                ModifySQL.add(',PERSIANAGRUPODIAMETROCOR=:PERSIANAGRUPODIAMETROCOR');
                ModifySQL.add(',PEDIDOPROJETOROMANEIO=:PEDIDOPROJETOROMANEIO,FERRAGEM_PP=:FERRAGEM_PP');
                ModifySQL.add(',VIDRO_pp=:VIDRO_pp,PERFILADO_PP=:PERFILADO_PP,KITBOX_PP=:KITBOX_PP');
                ModifySQL.add(',DIVERSO_PP=:DIVERSO_PP,PERSIANA_PP=:PERSIANA_PP,ENTRADAPRODUTOS=:ENTRADAPRODUTOS');
                ModifySQL.add(',FERRAGEM_EP=:FERRAGEM_EP,VIDRO_EP=:VIDRO_EP,PERFILADO_EP=:PERFILADO_EP');
                ModifySQL.add(',KITBOX_EP=:KITBOX_EP,DIVERSO_EP=:DIVERSO_EP,PERSIANA_EP=:PERSIANA_EP');
                ModifySQL.add(',OBSERVACAO=:OBSERVACAO,NOTAFISCAL=:NOTAFISCAL');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABESTOQUE where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjESTOQUE.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjESTOQUE.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabESTOQUE');
     Result:=Self.ParametroPesquisa;
end;

function TObjESTOQUE.Get_TituloPesquisa: STRING;
begin
     Result:=' Pesquisa de ESTOQUE ';
end;


function TObjESTOQUE.Get_NovoCodigo: STRING;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENESTOQUE,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENESTOQUE,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjESTOQUE.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjESTOQUE.RetornaCampoCodigo: STRING;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjESTOQUE.RetornaCampoNome: STRING;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjESTOQUE.Submit_CODIGO(parametro: STRING);
begin
        Self.CODIGO:=Parametro;
end;
function TObjESTOQUE.Get_CODIGO: STRING;
begin
        Result:=Self.CODIGO;
end;
procedure TObjESTOQUE.Submit_QUANTIDADE(parametro: STRING);
begin
        Self.QUANTIDADE:=Parametro;
end;
function TObjESTOQUE.Get_QUANTIDADE: STRING;
begin
        Result:=Self.QUANTIDADE;
end;
procedure TObjESTOQUE.Submit_DATA(parametro:string );
begin
        Self.DATA:=Parametro;
end;
function TObjESTOQUE.Get_DATA:string ;
begin
        Result:=Self.DATA;
end;
procedure TObjESTOQUE.Submit_FERRAGEMCOR(parametro: STRING);
begin
  Self.FERRAGEMCOR:=Parametro;
  Self.NOMETABELA := 'TABFERRAGEMCOR';
  Self.NOMECAMPO := 'FERRAGEMCOR';
  self.VALORCAMPO := parametro;
end;
function TObjESTOQUE.Get_FERRAGEMCOR: STRING;
begin
        Result:=Self.FERRAGEMCOR;
end;
procedure TObjESTOQUE.Submit_VIDROCOR(parametro: STRING);
begin
  Self.VIDROCOR:=Parametro;
  Self.NOMETABELA := 'TABVIDROCOR';
  Self.NOMECAMPO := 'VIDROCOR';
  self.VALORCAMPO := parametro;
end;
function TObjESTOQUE.Get_VIDROCOR: STRING;
begin
        Result:=Self.VIDROCOR;
end;
procedure TObjESTOQUE.Submit_PERFILADOCOR(parametro: STRING);
begin
  Self.PERFILADOCOR:=Parametro;
  Self.NOMETABELA := 'TABPERFILADOCOR';
  Self.NOMECAMPO :=  'PERFILADOCOR';
  self.VALORCAMPO := parametro;
end;
function TObjESTOQUE.Get_PERFILADOCOR: STRING;
begin
        Result:=Self.PERFILADOCOR;
end;
procedure TObjESTOQUE.Submit_KITBOXCOR(parametro: STRING);
begin
  Self.KITBOXCOR:=Parametro;
  Self.NOMETABELA := 'TABKITBOXCOR';
  Self.NOMECAMPO := 'KITBOXCOR';
  self.VALORCAMPO := parametro;
end;
function TObjESTOQUE.Get_KITBOXCOR: STRING;
begin
        Result:=Self.KITBOXCOR;
end;
procedure TObjESTOQUE.Submit_DIVERSOCOR(parametro: STRING);
begin
  Self.DIVERSOCOR:=Parametro;
  Self.NOMETABELA := 'TABDIVERSOCOR';
  Self.NOMECAMPO := 'DIVERSOCOR';
  self.VALORCAMPO := parametro;
end;
function TObjESTOQUE.Get_DIVERSOCOR: STRING;
begin
        Result:=Self.DIVERSOCOR;
end;
procedure TObjESTOQUE.Submit_PERSIANAGRUPODIAMETROCOR(parametro: STRING);
begin
  Self.PERSIANAGRUPODIAMETROCOR:=Parametro;
  Self.NOMETABELA := 'TABPERSIANAGRUPODIAMETROCOR';
  Self.NOMECAMPO := 'PERSIANAGRUPODIAMETROCOR';
  self.VALORCAMPO := parametro;
end;
function TObjESTOQUE.Get_PERSIANAGRUPODIAMETROCOR: STRING;
begin
        Result:=Self.PERSIANAGRUPODIAMETROCOR;
end;
procedure TObjESTOQUE.Submit_PEDIDOPROJETOROMANEIO(parametro: STRING);
begin
        Self.PEDIDOPROJETOROMANEIO:=Parametro;
end;
function TObjESTOQUE.Get_PEDIDOPROJETOROMANEIO: STRING;
begin
        Result:=Self.PEDIDOPROJETOROMANEIO;
end;
procedure TObjESTOQUE.Submit_FERRAGEM_PP(parametro: STRING);
begin
        Self.FERRAGEM_PP:=Parametro;
end;
function TObjESTOQUE.Get_FERRAGEM_PP: STRING;
begin
        Result:=Self.FERRAGEM_PP;
end;
procedure TObjESTOQUE.Submit_VIDRO_pp(parametro: STRING);
begin
        Self.VIDRO_pp:=Parametro;
end;
function TObjESTOQUE.Get_VIDRO_pp: STRING;
begin
        Result:=Self.VIDRO_pp;
end;
procedure TObjESTOQUE.Submit_PERFILADO_PP(parametro: STRING);
begin
        Self.PERFILADO_PP:=Parametro;
end;
function TObjESTOQUE.Get_PERFILADO_PP: STRING;
begin
        Result:=Self.PERFILADO_PP;
end;
procedure TObjESTOQUE.Submit_KITBOX_PP(parametro: STRING);
begin
        Self.KITBOX_PP:=Parametro;
end;
function TObjESTOQUE.Get_KITBOX_PP: STRING;
begin
        Result:=Self.KITBOX_PP;
end;
procedure TObjESTOQUE.Submit_DIVERSO_PP(parametro: STRING);
begin
        Self.DIVERSO_PP:=Parametro;
end;
function TObjESTOQUE.Get_DIVERSO_PP: STRING;
begin
        Result:=Self.DIVERSO_PP;
end;
procedure TObjESTOQUE.Submit_PERSIANA_PP(parametro: STRING);
begin
        Self.PERSIANA_PP:=Parametro;
end;
function TObjESTOQUE.Get_PERSIANA_PP: STRING;
begin
        Result:=Self.PERSIANA_PP;
end;
procedure TObjESTOQUE.Submit_ENTRADAPRODUTOS(parametro: STRING);
begin
        Self.ENTRADAPRODUTOS:=Parametro;
end;
function TObjESTOQUE.Get_ENTRADAPRODUTOS: STRING;
begin
        Result:=Self.ENTRADAPRODUTOS;
end;
procedure TObjESTOQUE.Submit_FERRAGEM_EP(parametro: STRING);
begin
        Self.FERRAGEM_EP:=Parametro;
end;
function TObjESTOQUE.Get_FERRAGEM_EP: STRING;
begin
        Result:=Self.FERRAGEM_EP;
end;
procedure TObjESTOQUE.Submit_VIDRO_EP(parametro: STRING);
begin
        Self.VIDRO_EP:=Parametro;
end;
function TObjESTOQUE.Get_VIDRO_EP: STRING;
begin
        Result:=Self.VIDRO_EP;
end;
procedure TObjESTOQUE.Submit_PERFILADO_EP(parametro: STRING);
begin
        Self.PERFILADO_EP:=Parametro;
end;
function TObjESTOQUE.Get_PERFILADO_EP: STRING;
begin
        Result:=Self.PERFILADO_EP;
end;
procedure TObjESTOQUE.Submit_KITBOX_EP(parametro: STRING);
begin
        Self.KITBOX_EP:=Parametro;
end;
function TObjESTOQUE.Get_KITBOX_EP: STRING;
begin
        Result:=Self.KITBOX_EP;
end;
procedure TObjESTOQUE.Submit_DIVERSO_EP(parametro: STRING);
begin
        Self.DIVERSO_EP:=Parametro;
end;
function TObjESTOQUE.Get_DIVERSO_EP: STRING;
begin
        Result:=Self.DIVERSO_EP;
end;
procedure TObjESTOQUE.Submit_PERSIANA_EP(parametro: STRING);
begin
        Self.PERSIANA_EP:=Parametro;
end;
function TObjESTOQUE.Get_PERSIANA_EP: STRING;
begin
        Result:=Self.PERSIANA_EP;
end;
procedure TObjESTOQUE.Submit_OBSERVACAO(parametro: STRING);
begin
        Self.OBSERVACAO:=Parametro;
end;
function TObjESTOQUE.Get_OBSERVACAO: STRING;
begin
        Result:=Self.OBSERVACAO;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjESTOQUE.Imprime(Pcodigo: STRING);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJESTOQUE';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



function TObjESTOQUE.ExcluiPedidoProjetoRomaneio(PCodigo: String;PComCommit:boolean): boolean;
var
  listaMateriais: TList;
  cont: Integer;
begin
  //Esta funcao � chamada no momento do retorno de um Romaneio
  //onde � necess�rio excluir a baixa do estoque
  //que foi dada na conclus�o, ele exclui a baixa de todos
  // os itens do pedido/projeto/romaneio selecionado
  //30/06/16-celio-para manter a igualdade da tabestoque com o estoque do materialCOR (tabvidrocor...)
  //precisa antes guardar quais s�o e depois da exclusao atualizar  estoque de todos os materiais deste romaneio
  Result := false;
  listaMateriais := TList.Create;
  try
    retornaMateriaisEstoque( Pcodigo, listaMateriais );
    if( listaMateriais.Count = 0 ) then
    begin
      MensagemErro('N�o foi poss�vel localizar materiais utilizados na baixa do estoque');
      Exit;
    end;
    With Self.Objquery do
    Begin
      close;
      sql.clear;
      sql.add('Delete from tabestoque where PEDIDOPROJETOROMANEIO='+Pcodigo);
      Try
        execsql;

        //agora preciso recalcular o estoque dos itens
        for cont := 0 to listaMateriais.Count -1 do
        begin
          self.ZerarTabela;
          Self.NOMETABELA := 'TAB'+TMateriaisEstoque( listaMateriais[cont] ).MaterialTipo+'COR';
          self.NOMECAMPO := TMateriaisEstoque( listaMateriais[cont] ).MaterialTipo+'COR';
          self.VALORCAMPO := IntToStr( TMateriaisEstoque( listaMateriais[cont] ).MaterialCodigo );
          Self.AtualizaEstoqueCor;
        end;

        if (PComCommit) then
          FDataModulo.IBTransaction.CommitRetaining;
             
        result:=true;
      Except
        on e:exception do
        Begin
          if (PComCommit) then
            FDataModulo.IBTransaction.RollbackRetaining;

          Mensagemerro(e.message);
          exit;
        End;
      End;
    End;

  finally
    for cont := 0 to listaMateriais.Count -1 do
      TMateriaisEstoque(listaMateriais[cont]).Free;
    FreeAndNil( listaMateriais );
  end;
end;

procedure TObjESTOQUE.Submit_NotaFiscal(parametro:string);
begin
    Self.NOTAFISCAL:=parametro;
end;

function TObjESTOQUE.Get_NotaFiscal:string;
begin
    Result:=Self.NOTAFISCAL;
end; 

{TObjESTOQUE.AtualizaEstoqueCor
  vai gravar na tabela materialCOR o total do estoque atual
  que dever� sempre bater com o valor da soma da tabestoque
}
procedure TObjESTOQUE.AtualizaEstoqueCor;
var
  estoque: Currency;
begin
  //soma o estoque do material
  estoque := retornaEstoque;

  //Atualiza Estoque no materialcor
  With Self.Objquery do
  begin
    close;
    sql.clear;
    sql.add('update '+NOMETABELA+' set estoque=:pestoque where CODIGO='+Self.VALORCAMPO);
    ParamByName('pestoque').AsCurrency := estoque; 
    ExecSQL;
  End;
end;

function TObjESTOQUE.retornaEstoque: currency;
begin
  With Self.Objquery do
  begin
    close;
    sql.clear;
    sql.add('Select coalesce(sum(quantidade),0) as estoque from TABESTOQUE where '+NOMECAMPO+'='+Self.VALORCAMPO);
    open;
    result:=fieldbyname('estoque').AsCurrency;
  End;
end;

procedure Tobjestoque.retornaMateriaisEstoque( pedidoprojetoromaneio:string; var lista: TList );
var
  material: TMateriaisEstoque;
begin
  lista.Clear;
  Self.Objquery.close;
  Self.Objquery.sql.Clear;
  Self.Objquery.SQL.Add('select e.ferragemcor,e.vidrocor,e.perfiladocor,e.kitboxcor,e.diversocor, e.persianagrupodiametrocor');
  Self.Objquery.SQL.Add('from tabestoque e where PEDIDOPROJETOROMANEIO='+pedidoprojetoromaneio);
  Self.Objquery.Open;
  while not Self.Objquery.Eof do
  begin
    material := TMateriaisEstoque.Create;
    if( Objquery.FieldByName('ferragemcor').AsString <> '' ) then
    begin
      material.MaterialTipo := 'ferragem';
      material.MaterialCodigo := Objquery.FieldByName('ferragemcor').AsInteger;
    end
    else
    if( Objquery.FieldByName('vidrocor').AsString <> '' ) then
    begin
      material.MaterialTipo := 'vidro';
      material.MaterialCodigo := Objquery.FieldByName('vidrocor').AsInteger;
    end
    else
    if( Objquery.FieldByName('perfiladocor').AsString <> '' ) then
    begin
      material.MaterialTipo := 'perfilado';
      material.MaterialCodigo := Objquery.FieldByName('perfiladocor').AsInteger;
    end
    else
    if( Objquery.FieldByName('kitboxcor').AsString <> '' ) then
    begin
      material.MaterialTipo := 'kitbox';
      material.MaterialCodigo := Objquery.FieldByName('kitboxcor').AsInteger;
    end
    else
    if( Objquery.FieldByName('diversocor').AsString <> '' ) then
    begin
      material.MaterialTipo := 'diverso';
      material.MaterialCodigo := Objquery.FieldByName('diversocor').AsInteger;
    end
    else
    if( Objquery.FieldByName('persianagrupodiametrocor').AsString <> '' ) then
    begin
      material.MaterialTipo := 'persianagrupodiametro';
      material.MaterialCodigo := Objquery.FieldByName('persianagrupodiametrocor').AsInteger;
    end;
    lista.Add( material );

    Self.Objquery.Next;
  end;

end;

{
  ATEN��O n�o executar este m�todo no meio de uma transa��o existente
}
function TObjESTOQUE.AtualizaEstoqueCorMateriais:boolean;
var
  listaTabelas : TStringList;
  qry: TIBQuery;
  cont : Integer;
begin
  Result := false;
  listaTabelas := TStringList.Create;
  qry := TIBQuery.Create(nil);
  qry.Database := FDataModulo.IBDatabase;
  if( FDataModulo.IBTransaction.InTransaction ) then
  begin
    //MensagemErro('Existe uma transa��o em andamento, n�o ser� poss�vel continuar');
    //exit;
    FDataModulo.IBTransaction.Rollback;
  end;
  FDataModulo.IBTransaction.StartTransaction;
  try
    try
      listaTabelas.Clear;
      listaTabelas.Add('FERRAGEMCOR');
      listaTabelas.Add('VIDROCOR');
      listaTabelas.Add('PERFILADOCOR');
      listaTabelas.Add('KITBOXCOR');
      listaTabelas.Add('DIVERSOCOR');
      listaTabelas.Add('PERSIANAGRUPODIAMETROCOR');
      for cont := 0 to listaTabelas.Count - 1 do
      begin
        qry.Close;
        qry.SQL.Clear;
        qry.SQL.Add('SELECT CODIGO FROM TAB'+listatabelas[cont]);
        //qry.SQL.Add('where ((estoque is null) or (estoque = 0))');
        qry.Open;
        qry.Last;
        InicializaBarradeProgressoRelatorio( qry.RecordCount, 'Processando ' + listatabelas[cont] );
        qry.First;
        while not qry.Eof do
        begin
          IncrementaBarraProgressoRelatorio;
          self.ZerarTabela;
          Self.NOMETABELA := 'TAB'+listatabelas[cont];
          self.NOMECAMPO := listatabelas[cont];
          self.VALORCAMPO := qry.Fields[0].AsString;
          self.AtualizaEstoqueCor;
          qry.Next;
        end;
      end;
      FechaBarraProgressoRelatorio;
    except
      on e:exception do
      begin
        MensagemErro('Erro ao atualizar o estoque dos materiais.'+#13+'Erro: '+e.Message);
        FDataModulo.IBTransaction.Rollback;
        Exit;
      end;
    end;
  finally
    if( FDataModulo.IBTransaction.InTransaction ) then
      FDataModulo.IBTransaction.Commit;
    FreeAndNil( listaTabelas );
    FreeAndNil( qry );
  end;
  Result := True;
end;

end.



