unit UobjENTRADAPRODUTOS;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJFORNECEDOR,UOBJTITULO,UobjModeloNF,UObjCFOP;

Type
   TObjENTRADAPRODUTOS=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                FORNECEDOR:TOBJFORNECEDOR ;
                TITULO:TOBJTITULO ;
                CFOP:TObjCFOP;
                MODELONF:TObjMODELONF;

//CODIFICA VARIAVEIS PUBLICAS



                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_DATA(parametro: string);
                Function Get_DATA: string;
                Procedure Submit_VALORPRODUTOS(parametro: string);
                Function Get_VALORPRODUTOS: string;
                Procedure Submit_DESCONTOS(parametro: string);
                Function Get_DESCONTOS: string;
                Function Get_VALORFINAL: string;
                Procedure Submit_OBSERVACOES(parametro: String);
                Function Get_OBSERVACOES: String;
                Procedure Submit_NF(parametro: string);
                Function Get_NF: string;
                Procedure Submit_EMISSAONF(parametro: string);
                Function Get_EMISSAONF: string;
                Procedure Submit_CONCLUIDO(parametro: string);
                Function Get_CONCLUIDO: string;
                Procedure Submit_VALORFRETE(parametro: string);
                Function Get_VALORFRETE: string;

                Procedure Submit_OUTROSGASTOS(parametro: string);
                Function Get_OUTROSGASTOS: string;

                Procedure Submit_IcmsSubstituto(parametro: string);
                Function Get_IcmsSubstituto: string;
                Procedure Submit_ChaveNfe(parametro:String);
                Function Get_ChaveNfe:string;

                Procedure Submit_serienf(parametro:String);
                Function Get_serienf:string;

                procedure submit_spedvalido(parametro:string);
                function get_spedvalido:string;


                Function Get_valoripi:string;
                Function Get_CreditoICMS:string;
                Function Get_CreditoPIS:string;
                Function Get_CreditoCOFINS:string;
                Function Get_CreditoIPI:string;

                Procedure Submit_valoripi(parametro:string);
                Procedure Submit_CreditoICMS(parametro:string);
                Procedure Submit_CreditoPIS(parametro:string);
                Procedure Submit_CreditoCOFINS(parametro:string);
                Procedure Submit_CreditoIPI(parametro:string);

                procedure Submit_BASECALCULOICMS(parametro:string);
                procedure Submit_VALORICMS(parametro:string);
                procedure Submit_BASECALCULOICMSSUBSTITUICAO(parametro:string);
                procedure Submit_VALORSEGURO(parametro:string);

                Function Get_BASECALCULOICMS:string;
                function Get_VALORICMS:string;
                function Get_BASECALCULOICMSSUBSTITUICAO:string;
                Function Get_VALORSEGURO:string;

                procedure Submit_IcmsSobreFrete(parametro:string);
                procedure Submit_FreteFornecedor(parametro:string);
                Function Get_icmssobrefrete:string;
                function Get_FreteFornecedor:string;


                procedure EdtFORNECEDORExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFORNECEDORKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtTITULOExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtTITULOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Procedure opcoes(Pcodigo:string);
                Procedure RefazContabilidade(PdataInicial,PdataFinal:string);

//CODIFICA DECLARA GETSESUBMITS



         Private
               Objquery:Tibquery;
               Objquerytemp:tibquery;
               
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               DATA:string;
               VALORPRODUTOS:string;
               DESCONTOS:string;
               VALORFINAL:string;
               OBSERVACOES:String;
               NF:string;
               EMISSAONF:string;
               CONCLUIDO:string;
               VALORFRETE:string;
               OUTROSGASTOS:string;
               IcmsSubstituto:string;
                valoripi:string;
                CreditoICMS:string;
                CreditoPIS:string;
                CreditoCOFINS:string;
                CreditoIPI:string;

                icmssobrefrete:string;
                FreteFornecedor:string;

                BASECALCULOICMS:string;
                VALORICMS:string;
                BASECALCULOICMSSUBSTITUICAO:string;
                VALORSEGURO:string;
                ChaveNfe:string;
                serienf:string;
                spedvalido:string;


               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Function  VerificaNf_Fornecedor:Boolean;
                Procedure ObjetoparaTabela;
                Procedure GerarFinanceiro(pcodigo:string);


                procedure RetornarFinanceiro(pcodigo: string);

                Function  GeraContabilidade2(Pcodigo:string):Boolean;

                Function  RetornarContabilidade(Pcodigo:string):Boolean;


   End;


implementation
uses UopcaoRel,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
  UObjGeraTitulo, UObjLancamento,
  UMostraBarraProgresso, UFORNECEDOR;


{ TTabTitulo }


Function  TObjENTRADAPRODUTOS.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('FORNECEDOR').asstring<>'')
        Then Begin
                 If (Self.FORNECEDOR.LocalizaCodigo(FieldByName('FORNECEDOR').asstring)=False)
                 Then Begin
                          Messagedlg('Fornecedor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.FORNECEDOR.TabelaparaObjeto;
        End;
        Self.DATA:=fieldbyname('DATA').asstring;
        If(FieldByName('TITULO').asstring<>'')
        Then Begin
                 If (Self.TITULO.LocalizaCodigo(FieldByName('TITULO').asstring)=False)
                 Then Begin
                          Messagedlg('T�tulo N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.TITULO.TabelaparaObjeto;
        End;

        if(FieldByName('modelonf').AsString<>'') then
        begin
                if(self.MODELONF.LocalizaCodigo(fieldbyname('modelonf').asstring)=False) then
                begin
                          MessageDlg('Modelo de NF n�o encontrado',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          Exit;
                end
                else Self.MODELONF.TabelaparaObjeto;
        end;

        if(FieldByName('CFOP').AsString<>'') then
        begin
                if(self.CFOP.LocalizaCodigo(fieldbyname('CFOP').asstring)=False) then
                begin
                          MessageDlg('Modelo de NF n�o encontrado',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          Exit;
                end
                else Self.CFOP.TabelaparaObjeto;
        end;

        Self.VALORPRODUTOS:=fieldbyname('VALORPRODUTOS').asstring;
        Self.DESCONTOS:=fieldbyname('DESCONTOS').asstring;
        Self.VALORFINAL:=fieldbyname('VALORFINAL').asstring;
        Self.OBSERVACOES:=fieldbyname('OBSERVACOES').asstring;
        Self.NF:=fieldbyname('NF').asstring;
        Self.EMISSAONF:=fieldbyname('EMISSAONF').asstring;
        Self.CONCLUIDO:=fieldbyname('CONCLUIDO').asstring;
        Self.VALORFRETE:=fieldbyname('VALORFRETE').asstring;
        Self.OUTROSGASTOS:=fieldbyname('OUTROSGASTOS').asstring;
        Self.IcmsSubstituto:=fieldbyname('IcmsSubstituto').asstring;

        Self.valoripi:=fieldbyname('valoripi').asstring;
        Self.CreditoICMS:=fieldbyname('creditoicms').asstring;
        Self.CreditoPIS:=fieldbyname('creditopis').asstring;
        Self.CreditoCOFINS:=fieldbyname('creditocofins').asstring;
        Self.CreditoIPI:=fieldbyname('creditoipi').asstring;

        Self.BASECALCULOICMS:=fieldbyname('basecalculoicms').AsString;
        Self.VALORICMS:=fieldbyname('valoricms').AsString;
        self.BASECALCULOICMSSUBSTITUICAO:=fieldbyname('BASECALCULOICMSSUBSTITUICAO').AsString;
        self.VALORSEGURO:=fieldbyname('valorseguro').AsString;
        self.icmssobrefrete :=fieldbyname('freteparafornecedor').AsString;
        Self.FreteFornecedor :=fieldbyname('icmssobrefrete').AsString;
        Self.ChaveNfe:=fieldbyname('chavenfe').AsString;
        Self.serienf:=fieldbyname('serienf').AsString;
        Self.spedvalido:=fieldbyname('spedvalido').AsString;

        result:=True;
     End;
end;


Procedure TObjENTRADAPRODUTOS.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('FORNECEDOR').asstring:=Self.FORNECEDOR.GET_CODIGO;
        ParamByName('DATA').asstring:=Self.DATA;
        ParamByName('TITULO').asstring:=Self.TITULO.GET_CODIGO;
        ParamByName('VALORPRODUTOS').asstring:=virgulaparaponto(Self.VALORPRODUTOS);
        ParamByName('DESCONTOS').asstring:=virgulaparaponto(Self.DESCONTOS);
        ParamByName('OBSERVACOES').asstring:=Self.OBSERVACOES;
        ParamByName('NF').asstring:=Self.NF;
        ParamByName('EMISSAONF').asstring:=Self.EMISSAONF;
        ParamByName('CONCLUIDO').asstring:=Self.CONCLUIDO;
        ParamByName('VALORFRETE').asstring:=virgulaparaponto(Self.VALORFRETE);
        ParamByName('OUTROSGASTOS').asstring:=virgulaparaponto(Self.OUTROSGASTOS);
        ParamByName('IcmsSubstituto').asstring:=virgulaparaponto(Self.IcmsSubstituto);

        parambyname('valoripi').asstring:=virgulaparaponto(Self.valoripi);
        parambyname('creditoicms').asstring:=virgulaparaponto(Self.CreditoICMS);
        parambyname('creditopis').asstring:=virgulaparaponto(Self.CreditoPIS);
        parambyname('creditocofins').asstring:=virgulaparaponto(Self.CreditoCOFINS);
        parambyname('creditoipi').asstring:=virgulaparaponto(Self.CreditoIPI);
        ParamByName('modelonf').AsString:=MODELONF.Get_CODIGO;

        ParamByName('basecalculoicms').AsString:=virgulaparaponto(Self.BASECALCULOICMS);
        ParamByName('valoricms').AsString:=virgulaparaponto(Self.VALORICMS);
        ParamByName('BASECALCULOICMSSUBSTITUICAO').AsString:=virgulaparaponto(self.BASECALCULOICMSSUBSTITUICAO);
        ParamByName('valorseguro').AsString:=virgulaparaponto(Self.VALORSEGURO);

        ParamByName('freteparafornecedor').AsString:=self.icmssobrefrete;
        ParamByName('icmssobrefrete').AsString:=Self.FreteFornecedor ;
        ParamByName('chavenfe').AsString:=self.ChaveNfe;
        ParamByName('serienf').AsString:=self.serienf;
        ParamByName('spedvalido').AsString:=self.spedvalido;
        ParamByName('CFOP').AsString:=self.CFOP.Get_CODIGO;
  End;
End;

//***********************************************************************

function TObjENTRADAPRODUTOS.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;

  if (Self.VerificaNf_Fornecedor=False)
  Then exit;

  If Self.LocalizaCodigo(Self.CODIGO)=False
  Then Begin
            if(Self.Status=dsedit)
            Then Begin
                      Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                      exit;
            End;
  End
  Else Begin
            if(Self.Status=dsinsert)
            Then Begin
                      Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                      exit;
            End;
  End;

  if Self.status=dsinsert
  Then Begin
            Self.Objquery.SQL.Clear;
            Self.Objquery.SQL.text:=Self.InsertSql.Text;
            if (Self.Codigo='0')
            Then Self.codigo:=Self.Get_NovoCodigo;
  End
  Else Begin
            if (Self.Status=dsedit)
            Then Begin
                      Self.Objquery.SQL.Clear;
                      Self.Objquery.SQL.text:=Self.ModifySQl.Text;
            End
            Else Begin
                      Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                      exit;
            End;
  End;
  Self.ObjetoParaTabela;
  Try
    Self.Objquery.ExecSQL;
  Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0);
       exit;
  End;

  If ComCommit=True
  Then FDataModulo.IBTransaction.CommitRetaining;

  Self.status          :=dsInactive;
  result:=True;
end;

procedure TObjENTRADAPRODUTOS.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        FORNECEDOR.ZerarTabela;
        DATA:='';
        TITULO.ZerarTabela;
        VALORPRODUTOS:='';
        DESCONTOS:='';
        VALORFINAL:='';
        OBSERVACOES:='';
        NF:='';
        EMISSAONF:='';
        CONCLUIDO:='';
        VALORFRETE:='';
        OUTROSGASTOS:='';
        IcmsSubstituto:='';

        valoripi:='';
        CreditoICMS:='';
        CreditoPIS:='';
        CreditoCOFINS:='';
        CreditoIPI:='';
        MODELONF.ZerarTabela;
        CFOP.ZerarTabela;

        Self.BASECALCULOICMS:='';
        Self.BASECALCULOICMSSUBSTITUICAO:='';
        self.VALORICMS:='';
        Self.VALORSEGURO:='';
        FreteFornecedor:='';
        icmssobrefrete:='';
        chavenfe:='';
        serienf:='';
        spedvalido:='';

//CODIFICA ZERARTABELA

     End;
end;

Function TObjENTRADAPRODUTOS.VerificaBrancos:boolean;
var
   Mensagem:string;
begin

  Result:=True;
  mensagem:='';

  With Self do
  Begin

    If (CODIGO='') Then
      Mensagem:=mensagem+'/C�digo';

    If (serienf='') Then
      Mensagem:=mensagem+'/Serie';

    if (spedvalido='') then
      spedvalido := 'N';

    if (OUTROSGASTOS='')
    Then OUTROSGASTOS:='0';

    if (DESCONTOS='')
    then DESCONTOS:='0';

    if (VALORFRETE='')
    then VALORFRETE:='0';

    if (valoripi='')
    Then valoripi:='0';

    if (IcmsSubstituto='')
    Then IcmsSubstituto:='0';


  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjENTRADAPRODUTOS.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.FORNECEDOR.LocalizaCodigo(Self.FORNECEDOR.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Fornecedor n�o Encontrado!';

      if (Self.TITULO.Get_CODIGO<>'')
      Then Begin
              If (Self.TITULO.LocalizaCodigo(Self.TITULO.Get_CODIGO)=False)
              Then Mensagem:=mensagem+'/ T�tulo n�o Encontrado!';
      End;
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjENTRADAPRODUTOS.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.FORNECEDOR.Get_Codigo<>'')
        Then Strtoint(Self.FORNECEDOR.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Fornecedor';
     End;
     try
        If (Self.TITULO.Get_Codigo<>'')
        Then Strtoint(Self.TITULO.Get_Codigo);
     Except
           Mensagem:=mensagem+'/T�tulo';
     End;
     try
        Strtofloat(Self.VALORPRODUTOS);
     Except
           Mensagem:=mensagem+'/Valor dos Produtos';
     End;
     try
        Strtofloat(Self.DESCONTOS);
     Except
           Mensagem:=mensagem+'/Descontos';
     End;
     try
        Strtofloat(Self.VALORFRETE);
     Except
           Mensagem:=mensagem+'/Valor do Frete';
     End;

     try
        Strtofloat(Self.OUTROSGASTOS);
     Except
           Mensagem:=mensagem+'/Outros Gastos';
     End;

     try
        Strtofloat(Self.IcmsSubstituto);
     Except
           Mensagem:=mensagem+'/Icms Substituto';
     End;


     try
        Strtofloat(Self.valoripi);
     Except
           Mensagem:=mensagem+'/Valor do IPI';
     End;

     try
        Strtofloat(Self.CreditoICMS);
     Except
           Mensagem:=mensagem+'/Cr�dito de ICMS';
     End;

     try
        Strtofloat(Self.CreditoPIS);
     Except
           Mensagem:=mensagem+'/Cr�dito de PIS';
     End;

     try
        Strtofloat(Self.CreditoCOFINS);
     Except
           Mensagem:=mensagem+'/Cr�dito de COFINS';
     End;

     try
        Strtofloat(Self.CreditoIPI);
     Except
           Mensagem:=mensagem+'/Cr�dito de IPI';
     End;



//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjENTRADAPRODUTOS.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.DATA);
     Except
        Mensagem:=mensagem+'/Data';
     End;

     try
        if(Self.EMISSAONF<>'')
        Then Strtodate(Self.EMISSAONF);
     Except
           Mensagem:=mensagem+'/Emiss�o NF';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End
     Else Begin
               if (ObjParametroGlobal.ValidaParametro('DATA DO ULTIMO FECHAMENTO DA CONTABILIDADE')=False)
               Then exit;

               if (trim(ObjParametroGlobal.Get_Valor)<>'')
               Then Begin
                         Try
                            Strtodate(ObjParametroGlobal.get_valor);
                         Except
                               mensagemerro('Data Inv�lida no par�metro de fechamento da contabilidade');
                               exit;
                         End;

                         if (strtodate(Self.DATA)<=Strtodate(ObjParametroGlobal.get_valor))
                         then Begin
                                   mensagemerro('N�o � poss�vel emitir uma entrada anterior ao fechamento cont�bil'+#13+'Fechamento: '+ObjParametroGlobal.Get_Valor);
                                   exit;
                         End;
               End;
     End;
     result:=true;

end;

function TObjENTRADAPRODUTOS.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
        If not((CONCLUIDO='N') OR (CONCLUIDO='S'))
        Then Mensagem:=mensagem+'/Valor Inv�lido para o campo Conclu�do';

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjENTRADAPRODUTOS.VerificaNf_Fornecedor: Boolean;
begin
       result:=true;
       
       if (Self.Nf='')
       Then exit;

       result:=False;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,FORNECEDOR,DATA,TITULO,VALORPRODUTOS,DESCONTOS,VALORFINAL');
           SQL.ADD(' ,OBSERVACOES,NF,EMISSAONF,CONCLUIDO,VALORFRETE,OUTROSGASTOS,valoripi,IcmsSubstituto,CreditoICMS,CreditoPIS,CreditoCOFINS,CreditoIPI,modelonf');
           SQL.ADD(' ,basecalculoicms,valoricms,BASECALCULOICMSSUBSTITUICAO,valorseguro,freteparafornecedor,icmssobrefrete,chavenfe,serienf,spedvalido from  TABENTRADAPRODUTOS');
           SQL.ADD(' WHERE Fornecedor='+Self.Fornecedor.Get_codigo);
           SQL.ADD(' and  Nf='+#39+Self.NF+#39);
           Open;
           If (recordcount>0)
           Then Begin
                     if (Self.CODIGO<>Fieldbyname('codigo').asstring)
                     Then Begin
                               if (Messagedlg('Esta Nota Fiscal j� foi cadastrada para este fornecedor na Entrada N� '+Fieldbyname('codigo').asstring+'. Tem certeza que deseja cadastrar Novamente?',mtconfirmation,[mbyes,mbno],0)=mrno)
                               Then exit;
                     End;
           End;

           Result:=true;
       End;
end;


function TObjENTRADAPRODUTOS.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro ENTRADAPRODUTOS vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,FORNECEDOR,DATA,TITULO,VALORPRODUTOS,DESCONTOS,VALORFINAL');
           SQL.ADD(' ,OBSERVACOES,NF,EMISSAONF,CONCLUIDO,VALORFRETE,OUTROSGASTOS,valoripi,IcmsSubstituto,CreditoICMS,CreditoPIS,CreditoCOFINS,CreditoIPI,modelonf');
           SQL.ADD(',basecalculoicms,valoricms,BASECALCULOICMSSUBSTITUICAO,valorseguro,freteparafornecedor,icmssobrefrete,chavenfe,serienf,spedvalido,CFOP from  TABENTRADAPRODUTOS');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjENTRADAPRODUTOS.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjENTRADAPRODUTOS.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin

  Try

    result:=true;

    if (Self.LocalizaCodigo(Pcodigo)) then
    begin

      Self.Objquery.close;
      Self.Objquery.SQL.clear;
      Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
      Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
      Self.Objquery.ExecSQL;

      if (ComCommit) then
        FDataModulo.IBTransaction.CommitRetaining;

    end
    else
      result := false;
      
  Except

     result := false;
     
  End;

end;


constructor TObjENTRADAPRODUTOS.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.FORNECEDOR:=TOBJFORNECEDOR .create;
        Self.TITULO:=TOBJTITULO .create;
        Self.CFOP:=TObjCFOP.Create;
        self.MODELONF:=TObjMODELONF.Create;
        Self.Objquerytemp:=tibquery.Create(nil);
        Self.Objquerytemp.Database:=FDataModulo.IBDatabase;

//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABENTRADAPRODUTOS(CODIGO,FORNECEDOR,DATA');
                InsertSQL.add(' ,TITULO,VALORPRODUTOS,DESCONTOS,OBSERVACOES');
                InsertSQL.add(' ,NF,EMISSAONF,CONCLUIDO,VALORFRETE,OUTROSGASTOS,IcmsSubstituto,valoripi,CreditoICMS,CreditoPIS,CreditoCOFINS,CreditoIPI,modelonf');
                InsertSql.Add(',basecalculoicms,valoricms,BASECALCULOICMSSUBSTITUICAO,valorseguro,freteparafornecedor,icmssobrefrete,chavenfe,serienf,spedvalido,CFOP)') ;
                InsertSQL.add('values (:CODIGO,:FORNECEDOR,:DATA,:TITULO,:VALORPRODUTOS');
                InsertSQL.add(' ,:DESCONTOS,:OBSERVACOES,:NF,:EMISSAONF');
                InsertSQL.add(' ,:CONCLUIDO,:VALORFRETE,:OUTROSGASTOS,:IcmsSubstituto,:valoripi,:CreditoICMS,:CreditoPIS,:CreditoCOFINS,:CreditoIPI,:modelonf');
                InsertSql.Add(',:basecalculoicms,:valoricms,:BASECALCULOICMSSUBSTITUICAO,:valorseguro,:freteparafornecedor,:icmssobrefrete,:chavenfe,:serienf,:spedvalido,:CFOP)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABENTRADAPRODUTOS set CODIGO=:CODIGO,FORNECEDOR=:FORNECEDOR');
                ModifySQL.add(',DATA=:DATA,TITULO=:TITULO,VALORPRODUTOS=:VALORPRODUTOS');
                ModifySQL.add(',DESCONTOS=:DESCONTOS,OBSERVACOES=:OBSERVACOES');
                ModifySQL.add(',NF=:NF,EMISSAONF=:EMISSAONF,CONCLUIDO=:CONCLUIDO,VALORFRETE=:VALORFRETE');
                ModifySQL.add(',OUTROSGASTOS=:OUTROSGASTOS,IcmsSubstituto=:IcmsSubstituto');
                ModifySQL.add(',valoripi=:valoripi,CreditoICMS=:CreditoICMS,CreditoPIS=:CreditoPIS,CreditoCOFINS=:CreditoCOFINS,CreditoIPI=:CreditoIPI,modelonf=:modelonf');
                ModifySQl.Add(',basecalculoicms=:basecalculoicms,valoricms=:valoricms,BASECALCULOICMSSUBSTITUICAO=:BASECALCULOICMSSUBSTITUICAO,valorseguro=:valorseguro') ;
                ModifySQL.add(',freteparafornecedor=:freteparafornecedor,icmssobrefrete=:icmssobrefrete,chavenfe=:chavenfe,serienf=:serienf,spedvalido=:spedvalido,CFOP=:CFOP where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABENTRADAPRODUTOS where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjENTRADAPRODUTOS.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjENTRADAPRODUTOS.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabENTRADAPRODUTOS');
     Result:=Self.ParametroPesquisa;
end;

function TObjENTRADAPRODUTOS.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de ENTRADAPRODUTOS ';
end;


function TObjENTRADAPRODUTOS.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENENTRADAPRODUTOS,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENENTRADAPRODUTOS,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;
destructor TObjENTRADAPRODUTOS.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.FORNECEDOR.FREE;
    Self.TITULO.FREE;
    FreeAndNil(Self.Objquerytemp);
    self.CFOP.Free;
    Self.MODELONF.Free;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjENTRADAPRODUTOS.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;
//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjENTRADAPRODUTOS.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;
procedure TObjENTRADAPRODUTOS.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjENTRADAPRODUTOS.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjENTRADAPRODUTOS.Submit_DATA(parametro: string);
begin
        Self.DATA:=Parametro;
end;
function TObjENTRADAPRODUTOS.Get_DATA: string;
begin
        Result:=Self.DATA;
end;
procedure TObjENTRADAPRODUTOS.Submit_VALORPRODUTOS(parametro: string);
begin
        Self.VALORPRODUTOS:=Parametro;
end;
function TObjENTRADAPRODUTOS.Get_VALORPRODUTOS: string;
begin
        Result:=Self.VALORPRODUTOS;
end;
procedure TObjENTRADAPRODUTOS.Submit_DESCONTOS(parametro: string);
begin
        Self.DESCONTOS:=Parametro;
end;
function TObjENTRADAPRODUTOS.Get_DESCONTOS: string;
begin
        Result:=Self.DESCONTOS;
end;
function TObjENTRADAPRODUTOS.Get_VALORFINAL: string;
begin
        Result:=Self.VALORFINAL;
end;
procedure TObjENTRADAPRODUTOS.Submit_OBSERVACOES(parametro: String);
begin
        Self.OBSERVACOES:=Parametro;
end;
function TObjENTRADAPRODUTOS.Get_OBSERVACOES: String;
begin
        Result:=Self.OBSERVACOES;
end;
procedure TObjENTRADAPRODUTOS.Submit_NF(parametro: string);
begin
        Self.NF:=Parametro;
end;
function TObjENTRADAPRODUTOS.Get_NF: string;
begin
        Result:=Self.NF;
end;
procedure TObjENTRADAPRODUTOS.Submit_EMISSAONF(parametro: string);
begin
        if (trim(comebarra(Parametro))<>'')
        Then Self.EMISSAONF:=Parametro
        Else Self.EMISSAONF:='';
end;
function TObjENTRADAPRODUTOS.Get_EMISSAONF: string;
begin
        Result:=Self.EMISSAONF;
end;
procedure TObjENTRADAPRODUTOS.Submit_CONCLUIDO(parametro: string);
begin
        Self.CONCLUIDO:=Parametro;
end;
function TObjENTRADAPRODUTOS.Get_CONCLUIDO: string;
begin
        Result:=Self.CONCLUIDO;
end;
procedure TObjENTRADAPRODUTOS.Submit_VALORFRETE(parametro: string);
begin
        Self.VALORFRETE:=Parametro;
end;
function TObjENTRADAPRODUTOS.Get_VALORFRETE: string;
begin
        Result:=Self.VALORFRETE;
end;
procedure TObjENTRADAPRODUTOS.Submit_OUTROSGASTOS(parametro: string);
begin
        Self.OUTROSGASTOS:=Parametro;
end;
function TObjENTRADAPRODUTOS.Get_OUTROSGASTOS: string;
begin
        Result:=Self.OUTROSGASTOS;
end;
procedure TObjENTRADAPRODUTOS.Submit_IcmsSubstituto(parametro: string);
begin
        Self.IcmsSubstituto:=Parametro;
end;
function TObjENTRADAPRODUTOS.Get_IcmsSubstituto: string;
begin
        Result:=Self.IcmsSubstituto;
end;
//CODIFICA GETSESUBMITS
procedure TObjENTRADAPRODUTOS.EdtFORNECEDORExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.FORNECEDOR.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.FORNECEDOR.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.FORNECEDOR.Get_Fantasia;
End;
procedure TObjENTRADAPRODUTOS.EdtFORNECEDORKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Ffornecedor:TFFORNECEDOR;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Ffornecedor:=TFFORNECEDOR.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.FORNECEDOR.Get_Pesquisa,Self.FORNECEDOR.Get_TituloPesquisa,Ffornecedor)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FORNECEDOR.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.FORNECEDOR.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FORNECEDOR.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Ffornecedor);
     End;
end;
procedure TObjENTRADAPRODUTOS.EdtTITULOExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.TITULO.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.TITULO.tabelaparaobjeto;
End;
procedure TObjENTRADAPRODUTOS.EdtTITULOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.TITULO.Get_Pesquisa,Self.TITULO.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TITULO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.TITULO.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TITULO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN






procedure TObjENTRADAPRODUTOS.opcoes(Pcodigo: string);
begin

     With FOpcaorel do
     Begin
          RgOpcoes.items.clear;
          RgOpcoes.items.add('Gerar    Financeiro');
          RgOpcoes.items.add('Retornar Financeiro');
          showmodal;
          if (tag=0)
          Then exit;
          Case RgOpcoes.ItemIndex of
            0:Begin
                   Self.GerarFinanceiro(pcodigo);
            End;
            1:Begin
                   Self.RetornarFinanceiro(pcodigo);
            End;
          End;
     End;
end;

procedure TObjENTRADAPRODUTOS.GerarFinanceiro(pcodigo: string);
var
  Objgeratitulo:Tobjgeratitulo;
  PCodigoTitulo:string;
begin
     if (Pcodigo='')
     then Begin
               Messagedlg('Escolha a Entrada que deseja gerar o Financeiro',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.LocalizaCodigo(pcodigo)=False)
     Then Begin
               Messagedlg('Entrada n�o localizada',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;

     if (Self.Get_Concluido='S')
     or (Self.Titulo.Get_CODIGO<>'')
     Then Begin
               Messagedlg('J� Foi Gerado o Financeiro para esta Entrada, n�o � poss�vel gerar o financeiro novamente',mtinformation,[mbok],0);
               exit;
     End;

     Try
        Objgeratitulo:=TObjGeraTitulo.create;
     Except
           messagedlg('Erro na tentativa de Cria��o do Objeto de Gera��o de T�tulo!',mterror,[mbok],0);
           exit;
     End;

     Try

        If (Objgeratitulo.LocalizaHistorico('TITULO DE ENTRADA DE PRODUTOS')=False)
        Then Begin
                  Messagedlg('O GERATITULO de hist�rico="TITULO DE ENTRADA DE PRODUTOS" n�o foi encontrado!',mterror,[mbok],0);
                  exit;
        End;
        Objgeratitulo.TabelaparaObjeto;
        
        Self.Titulo.ZerarTabela;
        Self.Titulo.Status:=dsinsert;
        Pcodigotitulo:=Self.titulo.get_novocodigo;
        Self.Titulo.Submit_CODIGO(Pcodigotitulo);
        Self.Titulo.Submit_HISTORICO('COMPRA NF'+Self.NF+' - '+Self.Fornecedor.Get_Fantasia);
        Self.Titulo.Submit_GERADOR(Objgeratitulo.Get_Gerador);
        Self.Titulo.Submit_CODIGOGERADOR(Objgeratitulo.Get_CodigoGerador);
        Self.Titulo.Submit_CREDORDEVEDOR(Objgeratitulo.Get_CredorDevedor);
        Self.Titulo.Submit_CODIGOCREDORDEVEDOR(Self.FORNECEDOR.get_codigo);
        Self.Titulo.Submit_EMISSAO(Self.Data);
        Self.Titulo.Submit_PRAZO(Objgeratitulo.Get_Prazo);
        Self.Titulo.Submit_PORTADOR(Objgeratitulo.get_portador);
        Self.Titulo.Submit_VALOR(Self.ValorFinal);
        Self.Titulo.Submit_CONTAGERENCIAL(Objgeratitulo.Get_ContaGerencial);
        Self.Titulo.Submit_NUMDCTO(Self.get_codigo);//vai o numero da entrada
        Self.Titulo.Submit_GeradoPeloSistema('N');
        Self.Titulo.Submit_ParcelasIguais(True);
        Self.TITULO.Submit_NotaFiscal(Self.NF);

        if (Self.titulo.salvar(False,False)=False)//nao lanco a Contabilidade, sera lancada uma contabilidade especial por causa do Custo estoque no Gerencial
        Then Begin
                  FDataModulo.IBTransaction.RollbackRetaining;
                  MEssagedlg('Erro na tentativa de Gerar o T�tulo',mterror,[mbok],0);
                  exit;
        End;

     Finally
           Objgeratitulo.Free;
     end;
     
     Self.Status:=dsEdit;
     Self.Concluido:='S';

     if (Self.Salvar(False)=False)
     Then Begin
               FDataModulo.IBTransaction.RollbackRetaining;
               MEssagedlg('Erro na tentativa de Gravar N�mero do T�tulo na Entrada',mterror,[mbok],0);
               exit;
     End;

     if (Self.GeraContabilidade2(Pcodigo)=False)
     Then Begin
               FDataModulo.IBTransaction.RollbackRetaining;
               MEssagedlg('Erro na tentativa de Gerar a Contabilidade ',mterror,[mbok],0);
               exit;
     End;

     FDataModulo.IBTransaction.Commit;
     Messagedlg('Financeiro gerado com Sucesso!',mtinformation,[mbok],0);
end;


procedure TObjEntradaProdutos.RetornarFinanceiro(pcodigo: string);
var
ObjLancamento:TobjLancamento;
PcodigoTitulo:string;
Begin
     if (Pcodigo='')
     Then Begin
               Messagedlg('Escolha a entrada que deseja retornar o Financeiro',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.LocalizaCodigo(pcodigo)=False)
     Then Begin
               Messagedlg('Entrada n�o localizada',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;

     if (Self.Get_Concluido<>'S')
     or (Self.Titulo.Get_CODIGO='')
     Then Begin
               Messagedlg('Esta entrada n�o foi conclu�do, n�o � poss�vel retornar o financeiro',mtinformation,[mbok],0);
               exit;
     End;

     PCodigoTitulo:=Self.Titulo.get_codigo;
     Self.Status:=dsedit;
     Self.Titulo.ZerarTabela;
     Self.Submit_Concluido('N');
     if (self.Salvar(False)=False)
     Then Begin
               FDataModulo.IBTransaction.RollbackRetaining;
               Messagedlg('Erro na Tentativa de Salvar a Entrada!',mterror,[mbok],0);
               exit;
     End;

     Try
        ObjLancamento:=TObjLancamento.Create;
     Except
           Messagedlg('Erro na tentativa de Criar o Objeto de Lan�amento',mterror,[mbok],0);
           exit;
     End;

     Try
         if (ObjLancamento.ExcluiTitulo(PcodigoTitulo,False)=False)
         Then Begin
                   FDataModulo.IBTransaction.RollbackRetaining;
                   Messagedlg('Erro na tentativa de Excluir o T�tulo!',mterror,[mbok],0);
                   exit;
         End;

         if (Self.RetornarContabilidade(pcodigo)=False)
         then Begin
                   FDataModulo.IBTransaction.RollbackRetaining;
                   MensagemErro('N�o foi poss�vel retornar a contabilidade da entrada');
                   exit;
         End;

         FDataModulo.IBTransaction.CommitRetaining;
         Messagedlg('Financeiro retornado com Sucesso!',mtInformation,[mbok],0);

    Finally
            ObjLancamento.Free;
    End;




End;





function TObjENTRADAPRODUTOS.GeraContabilidade2(Pcodigo: string): Boolean;
var
pvalor,Psomaprodutos,PvalorTotal,Pdesconto,Pfrete,PoutrosGastos,PIcmsSubstituto,PvalorFinal:currency;
pcontafrete,pcontaoutrosgastos,pcontaIcmsSubstituto,Pcontafornecedor,pcontadesconto:string;
PcontaIPI,PcontaCreditoICMS,PcontaCreditoPIS,PcontaCreditoCOFINS,PcontaCreditoIPI:string;
begin
{
    A camila pediu altera��o na gera��o da Contabilidade dia 14/05/2009
    Fiz um backup da fun��o (geracontabilidadeX) caso precise depois da anterior

}
                       


     Result:=False;

     PcontaDesconto:='';
     if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL DESCONTO RECEBIDO NA ENTRADA DE PRODUTOS')=true)
     Then pcontadesconto:=ObjParametroGlobal.Get_Valor;

     PcontaFrete:='';
     if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL FRETE PAGO NA ENTRADA DE PRODUTOS')=true)
     Then PcontaFrete:=ObjParametroGlobal.Get_Valor;

     PcontaOutrosGastos:='';
     if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL OUTROS GASTOS NA ENTRADA DE PRODUTOS')=true)
     Then PcontaOutrosGastos:=ObjParametroGlobal.Get_Valor;

     PcontaIcmsSubstituto:='';
     if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL ICMS SUBSTITUTO NA ENTRADA DE PRODUTOS')=true)
     Then PcontaIcmsSubstituto:=ObjParametroGlobal.Get_Valor;


     PcontaIPI:='';
     if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL IPI NA ENTRADA DE PRODUTOS')=true)
     Then PcontaIPI:=ObjParametroGlobal.Get_Valor;

     PcontaCreditoICMS:='';
     if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL CR�DITO ICMS NA ENTRADA DE PRODUTOS')=true)
     Then PcontaCreditoICMS:=ObjParametroGlobal.Get_Valor;

     PcontaCreditoPIS:='';
     if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL CR�DITO PIS NA ENTRADA DE PRODUTOS')=true)
     Then PcontaCreditoPIS:=ObjParametroGlobal.Get_Valor;

     PcontaCreditoCOFINS:='';
     if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL CR�DITO COFINS NA ENTRADA DE PRODUTOS')=true)
     Then PcontaCreditoCOFINS:=ObjParametroGlobal.Get_Valor;

     PcontaCreditoIPI:='';
     if (ObjParametroGlobal.ValidaParametro('CONTA CONT�BIL CR�DITO IPI NA ENTRADA DE PRODUTOS')=true)
     Then PcontaCreditoIPI:=ObjParametroGlobal.Get_Valor;


{
Na Compra

Ao concluir - n�o gerar a contabilidade do financeiro (fornecedor/contagerencial)

Cada Cadastro (ferragem, perfilado....) possui grupos e cada grupo possui uma conta na contabilidade,
essa conta ser� usada em lan�amento D/C com o fornecedor.

//*************************EMAIL 13/05/09**********************

Preciso fazer uma altera��o nos lan�amentos cont�beis da Entrada de produtos:
Preciso que tenha um campo no m�dulo de entrada para informar se a mercadoria � de mercado interno ou externo.

Exemplo:

Para NF XXX
Valor dos produtos 100,00
Valor do IPI              5,00
Valor Total da NF   105,00

Se for de mercado interno fazemos o seguinte lan�amento:

D Estoque de Mercadorias  83,75
D Valor do IPI                    5,00
D Valor do Icms                 7,00
D Valor do PIS                   1,65
D Valor do Cofins               7,60
C Fornecedor                 105,00

O valor do produto para ser calculada a m�dia para custo ser� :
V.produto unit *aliquota Icms = X          -->    100*7% =    7,00
V.produto unit *aliquota IPI = Y             -->    100*5% =    5,00
V.produto unit *aliquota PIS = Z            -->    100*1,65%= 1,65
V.produto unit *aliquota COFINS = W      -->    100*7,6% = 7,60

O valor a ser considerado ser� = (V.NF)-(X+Y+Z+W) --> (105,00)-(21,25)= 83,75

Se for no Mercado Externo:
NF no valor de 100,00
IPI                    5,00
Total da NF     105,00

D-Estoque        105,00
C-Fornecedor    105,00

O valor do produto para ser calculada a m�dia para custo ser� o valor unit�rio informado na entrada de produtos.
Estou mandando c�pia para a Genice.

Qualquer d�vida entre em contato.

Camila


****RONNEI 22/06/2009****

Seguindo o da Tabela do Excel  ExemploImpostosEntrada na pasta Docs


Para cada produto fazer um lan�amento de estoque, considerando os campos
de cr�dito de PIS, COFINS, ICMS, IPI


                                                                                                Cr�ditos digitados                 |               Lan�amentos Cont�beis
                                                                                                                                   |                       D�bitos                            |    Cr�ditos
Produtos	  Valor Produto	Quantidade 	Valor Total	    Ipi Pago	Valor Final por Produto		Pis		    Cofins		Icms		    Ipi			 |	    Estoque		    Icms		Pis		    Cofins		  IPI		  |  Fornecedor
XXX	 	      R$ 100,00 		1	          R$ 100,00 	    R$ 5,00 	 R$ 105,00 	 		          R$ 1,65 	 R$ 7,60 	 R$ 7,00 	  R$ -   	 | 	    R$ 88,75 	 R$ 7,00 	 R$ 1,65 	 R$ 7,60 	 R$ -   	  |  R$ 105,00
XXXX	 	    R$ 100,00 		1	          R$ 100,00 	    R$ 5,00 	 R$ 105,00 	 		          R$ 1,65 	 R$ 7,60 	 R$ 7,00 	  R$ 5,00  | 		 R$ 83,75 	 R$ 7,00 	 R$ 1,65 	 R$ 7,60 	 R$ 5,00 	  |  R$ 105,00
}

    if (Self.LocalizaCodigo(Pcodigo)=False)
    Then begin
              MensagemErro('Entrada '+pcodigo+' n�o localizada para gerar a contabilidade');
              exit;
    End;
    Self.TabelaparaObjeto;

    try
       PvalorTotal:=strtofloat(Self.VALORPRODUTOS);
    Except
       MensagemErro('Erro no valor dos produtos');
       exit;
    End;

    try
       Pfrete:=strtofloat(Self.VALORFRETE);
    Except
       MensagemErro('Erro no valor do frete');
       exit;
    End;

    try
       PoutrosGastos:=strtofloat(Self.OUTROSGASTOS);
    Except
       MensagemErro('Erro no valor dos outros gastos');
       exit;
    End;

    try
       PIcmsSubstituto:=strtofloat(Self.IcmsSubstituto);
    Except
       MensagemErro('Erro no valor do Icms Substituto');
       exit;
    End;


    try
       Pdesconto:=strtofloat(Self.DESCONTOS);
    Except
       MensagemErro('Erro no valor dos descontos');
       exit;
    End;

    try
       PvalorFinal:=strtofloat(Self.VALORFINAL);
    Except
       MensagemErro('Erro no valor Final');
       exit;
    End;


    Pcontafornecedor:=Self.FORNECEDOR.Get_CodigoPlanoContas;

    With Self.Objquery do
    begin
         (*

          A Stored Procedure Proc_plano_contas_ep_3 retorna produto a produto
          com o c�digo do plano de contas e os dados
          de pagamento e impostos

          Portando, fazer um loop em todos os produtos
          Debitando
            Estoque  ((ValorFinal+Ipipago)-CreditoPis-CreditoCofins-CreditoIcms-CreditoIpi
            Cr�dito ICMS
            Cr�dito IPI
            Cr�dito Pis
            Cr�dito Cofins

          Creditando
              ValorFinal+Ipipago
              
          *****campos****
          
          PPLANODECONTAS INTEGER,
  pvalorfinal decimal(6,4),
  pipipago    decimal(6,4),
  pcreditoicms             decimal(6,4),
  pcreditoipi decimal(6,4),
  pcreditopis decimal(6,4),
  pcreditocofins decimal(6,4),
  PCADASTRO VARCHAR(20)


          *)
         close;
         sql.clear;
         sql.add('Select * from PROC_PLANOCONTAS_EP_3('+Pcodigo+')');
         open;
         last;
         if (recordcount=0)
         Then Begin
                   MensagemErro('Nenhum produto foi encontrado nessa entrada');
                   exit;
         end;



         //partida simples


         first;
         PsomaProdutos:=0;
         While not(eof) do
         begin
               PsomaProdutos:=PsomaProdutos+fieldbyname('pvalorfinal').asfloat;

               //Creditando o Fornecedor do Produto Atual (Valor Final + Ipi Pago)
               Self.TITULO.ObjExportaContabilidade.ZerarTabela;
               Self.TITULO.ObjExportaContabilidade.status:=dsinsert;
               Self.TITULO.ObjExportaContabilidade.Submit_CODIGO(Self.TITULO.ObjExportaContabilidade.Get_NovoCodigo);
               Self.TITULO.ObjExportaContabilidade.Submit_Data(Self.DATA);
               Self.TITULO.ObjExportaContabilidade.Submit_ContaCredite(Pcontafornecedor);
               Self.TITULO.ObjExportaContabilidade.Submit_Valor(floattostr(fieldbyname('pvalorfinal').ascurrency+fieldbyname('pipipago').ascurrency));
               Self.TITULO.ObjExportaContabilidade.Submit_Historico('ENTRADA DE PRODUTOS '+Self.codigo+'- Forn. '+Self.FORNECEDOR.Get_Fantasia);
               Self.TITULO.ObjExportaContabilidade.Submit_ObjGerador('OBJENTRADAPRODUTOS');
               Self.TITULO.ObjExportaContabilidade.Submit_CodGerador(Pcodigo);
               Self.TITULO.ObjExportaContabilidade.Submit_Exportado('N');
               Self.TITULO.ObjExportaContabilidade.Submit_Exporta('S');
               
               if (Self.TITULO.ObjExportaContabilidade.Salvar(False,True)=False)
               Then Begin
                        MensagemErro('N�o foi poss�vel salvar o lan�amento cont�bil de cr�dito para o fornecedor. Cadastro '+fieldbyname('pcadastro').asstring+' C�digo PEP: '+fieldbyname('pcodigo').asstring);
                        exit;
               End;

               //Debitando a Conta de Estoque
               //ValorFinal do Produto + Ipi Pago - Cr�ditos (icms+ipi+pis+cofins)
               Pvalor:=(fieldbyname('pvalorfinal').asfloat+fieldbyname('pipipago').asfloat)-
               fieldbyname('pcreditoicms').asfloat -
               fieldbyname('pcreditoipi').asfloat -
               fieldbyname('pcreditopis').asfloat -
               fieldbyname('pcreditocofins').asfloat;
               
               Self.TITULO.ObjExportaContabilidade.ZerarTabela;
               Self.TITULO.ObjExportaContabilidade.status:=dsinsert;
               Self.TITULO.ObjExportaContabilidade.Submit_CODIGO(Self.TITULO.ObjExportaContabilidade.Get_NovoCodigo);
               Self.TITULO.ObjExportaContabilidade.Submit_Data(Self.DATA);
               Self.TITULO.ObjExportaContabilidade.Submit_ContaDebite(fieldbyname('pplanodecontas').asstring);
               Self.TITULO.ObjExportaContabilidade.Submit_Valor(floattostr(pvalor));
               Self.TITULO.ObjExportaContabilidade.Submit_Historico('ENTRADA DE PRODUTOS '+Self.codigo+'- Forn. '+completapalavra(Self.FORNECEDOR.Get_Fantasia,30,' ')+' - Estoque Cad: '+fieldbyname('pcadastro').asstring+' CodPep: '+fieldbyname('pcodigo').asstring);
               Self.TITULO.ObjExportaContabilidade.Submit_ObjGerador('OBJENTRADAPRODUTOS');
               Self.TITULO.ObjExportaContabilidade.Submit_CodGerador(Pcodigo);
               Self.TITULO.ObjExportaContabilidade.Submit_Exportado('N');
               Self.TITULO.ObjExportaContabilidade.Submit_Exporta('S');

               if (Self.TITULO.ObjExportaContabilidade.Salvar(False,True)=False)
               Then Begin
                        MensagemErro('N�o foi poss�vel salvar o lan�amento cont�bil no valor do(a) '+fieldbyname('Pcadastro').asstring);
                        exit;
               End;

               //Debitando a Conta Cr�dito de Ipi

                Pvalor:=fieldbyname('pcreditoipi').asfloat;

               Self.TITULO.ObjExportaContabilidade.ZerarTabela;
               Self.TITULO.ObjExportaContabilidade.status:=dsinsert;
               Self.TITULO.ObjExportaContabilidade.Submit_CODIGO(Self.TITULO.ObjExportaContabilidade.Get_NovoCodigo);
               Self.TITULO.ObjExportaContabilidade.Submit_Data(Self.DATA);
               Self.TITULO.ObjExportaContabilidade.Submit_ContaDebite(PcontaCreditoIPI);
               Self.TITULO.ObjExportaContabilidade.Submit_Valor(floattostr(pvalor));
               Self.TITULO.ObjExportaContabilidade.Submit_Historico('CR�DITO DE IPI ENTRADA DE PRODUTOS '+Self.codigo+'- Forn. '+completapalavra(Self.FORNECEDOR.Get_Fantasia,30,' ')+' - Cad: '+fieldbyname('pcadastro').asstring+' CodPep: '+fieldbyname('pcodigo').asstring);
               Self.TITULO.ObjExportaContabilidade.Submit_ObjGerador('OBJENTRADAPRODUTOS');
               Self.TITULO.ObjExportaContabilidade.Submit_CodGerador(Pcodigo);
               Self.TITULO.ObjExportaContabilidade.Submit_Exportado('N');
               Self.TITULO.ObjExportaContabilidade.Submit_Exporta('S');

               if (Self.TITULO.ObjExportaContabilidade.Salvar(False,True)=False)
               Then Begin
                        MensagemErro('N�o foi poss�vel salvar o lan�amento cont�bil no valor do(a) '+fieldbyname('Pcadastro').asstring);
                        exit;
               End;

               //Cr�dito de Icms
               Pvalor:=fieldbyname('pcreditoicms').asfloat;

               Self.TITULO.ObjExportaContabilidade.ZerarTabela;
               Self.TITULO.ObjExportaContabilidade.status:=dsinsert;
               Self.TITULO.ObjExportaContabilidade.Submit_CODIGO(Self.TITULO.ObjExportaContabilidade.Get_NovoCodigo);
               Self.TITULO.ObjExportaContabilidade.Submit_Data(Self.DATA);
               Self.TITULO.ObjExportaContabilidade.Submit_ContaDebite(PcontaCreditoICMS);
               Self.TITULO.ObjExportaContabilidade.Submit_Valor(floattostr(pvalor));
               Self.TITULO.ObjExportaContabilidade.Submit_Historico('CR�DITO DE ICMS ENTRADA DE PRODUTOS '+Self.codigo+'- Forn. '+completapalavra(Self.FORNECEDOR.Get_Fantasia,30,' ')+' - Cad: '+fieldbyname('pcadastro').asstring+' CodPep: '+fieldbyname('pcodigo').asstring);
               Self.TITULO.ObjExportaContabilidade.Submit_ObjGerador('OBJENTRADAPRODUTOS');
               Self.TITULO.ObjExportaContabilidade.Submit_CodGerador(Pcodigo);
               Self.TITULO.ObjExportaContabilidade.Submit_Exportado('N');
               Self.TITULO.ObjExportaContabilidade.Submit_Exporta('S');

               if (Self.TITULO.ObjExportaContabilidade.Salvar(False,True)=False)
               Then Begin
                        MensagemErro('N�o foi poss�vel salvar o lan�amento cont�bil no valor do(a) '+fieldbyname('Pcadastro').asstring);
                        exit;
               End;

               //Cr�dito de Pis
               Pvalor:=fieldbyname('pcreditopis').asfloat;

               Self.TITULO.ObjExportaContabilidade.ZerarTabela;
               Self.TITULO.ObjExportaContabilidade.status:=dsinsert;
               Self.TITULO.ObjExportaContabilidade.Submit_CODIGO(Self.TITULO.ObjExportaContabilidade.Get_NovoCodigo);
               Self.TITULO.ObjExportaContabilidade.Submit_Data(Self.DATA);
               Self.TITULO.ObjExportaContabilidade.Submit_ContaDebite(PcontaCreditoPIS);
               Self.TITULO.ObjExportaContabilidade.Submit_Valor(floattostr(pvalor));
               Self.TITULO.ObjExportaContabilidade.Submit_Historico('CR�DITO DE PIS ENTRADA DE PRODUTOS '+Self.codigo+'- Forn. '+completapalavra(Self.FORNECEDOR.Get_Fantasia,30,' ')+' - Cad: '+fieldbyname('pcadastro').asstring+' CodPep: '+fieldbyname('pcodigo').asstring);
               Self.TITULO.ObjExportaContabilidade.Submit_ObjGerador('OBJENTRADAPRODUTOS');
               Self.TITULO.ObjExportaContabilidade.Submit_CodGerador(Pcodigo);
               Self.TITULO.ObjExportaContabilidade.Submit_Exportado('N');
               Self.TITULO.ObjExportaContabilidade.Submit_Exporta('S');

               if (Self.TITULO.ObjExportaContabilidade.Salvar(False,True)=False)
               Then Begin
                        MensagemErro('N�o foi poss�vel salvar o lan�amento cont�bil no valor do(a) '+fieldbyname('Pcadastro').asstring);
                        exit;
               End;

               //Cr�dito de Cofins
               Pvalor:=fieldbyname('pcreditocofins').asfloat;

               Self.TITULO.ObjExportaContabilidade.ZerarTabela;
               Self.TITULO.ObjExportaContabilidade.status:=dsinsert;
               Self.TITULO.ObjExportaContabilidade.Submit_CODIGO(Self.TITULO.ObjExportaContabilidade.Get_NovoCodigo);
               Self.TITULO.ObjExportaContabilidade.Submit_Data(Self.DATA);
               Self.TITULO.ObjExportaContabilidade.Submit_ContaDebite(PcontaCreditoCOFINS);
               Self.TITULO.ObjExportaContabilidade.Submit_Valor(floattostr(pvalor));
               Self.TITULO.ObjExportaContabilidade.Submit_Historico('CR�DITO DE COFINS ENTRADA DE PRODUTOS '+Self.codigo+'- Forn. '+completapalavra(Self.FORNECEDOR.Get_Fantasia,30,' ')+' - Cad: '+fieldbyname('pcadastro').asstring+' CodPep: '+fieldbyname('pcodigo').asstring);
               Self.TITULO.ObjExportaContabilidade.Submit_ObjGerador('OBJENTRADAPRODUTOS');
               Self.TITULO.ObjExportaContabilidade.Submit_CodGerador(Pcodigo);
               Self.TITULO.ObjExportaContabilidade.Submit_Exportado('N');
               Self.TITULO.ObjExportaContabilidade.Submit_Exporta('S');

               if (Self.TITULO.ObjExportaContabilidade.Salvar(False,True)=False)
               Then Begin
                        MensagemErro('N�o foi poss�vel salvar o lan�amento cont�bil no valor do(a) '+fieldbyname('Pcadastro').asstring);
                        exit;
               End;

              next;
         End;//While

         if (Psomaprodutos<>PvalorTotal)
         then begin
                   if (Messagedlg('O valor dos produtos n�o fecha com o valor total da NF'+#13+'Produtos '+currtostr(Psomaprodutos)+' Valor Total '+floattostr(Pvalortotal)+' Entrada N� '+Pcodigo+'. Deseja continuar?',mtconfirmation,[mbyes,mbno],0)=mrno)
                   Then exit;
         End;

         if (Pfrete<>0)
         then begin
                   Self.TITULO.ObjExportaContabilidade.ZerarTabela;
                   Self.TITULO.ObjExportaContabilidade.status:=dsinsert;
                   Self.TITULO.ObjExportaContabilidade.Submit_CODIGO(Self.TITULO.ObjExportaContabilidade.Get_NovoCodigo);
                   Self.TITULO.ObjExportaContabilidade.Submit_Data(Self.DATA);
                   Self.TITULO.ObjExportaContabilidade.Submit_ContaDebite(pcontafrete);
                   Self.TITULO.ObjExportaContabilidade.Submit_ContaCredite(Pcontafornecedor);
                   Self.TITULO.ObjExportaContabilidade.Submit_Valor(FloatToStr(Pfrete));
                   Self.TITULO.ObjExportaContabilidade.Submit_Historico('FRETE - ENTRADA DE PRODUTOS '+Self.codigo+'- Forn. '+Self.FORNECEDOR.Get_Fantasia);
                   Self.TITULO.ObjExportaContabilidade.Submit_ObjGerador('OBJENTRADAPRODUTOS');
                   Self.TITULO.ObjExportaContabilidade.Submit_CodGerador(Pcodigo);
                   Self.TITULO.ObjExportaContabilidade.Submit_Exportado('N');
                   Self.TITULO.ObjExportaContabilidade.Submit_Exporta('S');

                   if (Self.TITULO.ObjExportaContabilidade.Salvar(False,True)=False)
                   Then Begin
                            MensagemErro('N�o foi poss�vel salvar o lan�amento cont�bil no valor do frete da compra para o fornecedor');
                            exit;
                   End;
         End;//frete

         if (PoutrosGastos<>0)
         then begin
                   Self.TITULO.ObjExportaContabilidade.ZerarTabela;
                   Self.TITULO.ObjExportaContabilidade.status:=dsinsert;
                   Self.TITULO.ObjExportaContabilidade.Submit_CODIGO(Self.TITULO.ObjExportaContabilidade.Get_NovoCodigo);
                   Self.TITULO.ObjExportaContabilidade.Submit_Data(Self.DATA);
                   Self.TITULO.ObjExportaContabilidade.Submit_ContaDebite(pcontaoutrosgastos);
                   Self.TITULO.ObjExportaContabilidade.Submit_ContaCredite(Pcontafornecedor);
                   Self.TITULO.ObjExportaContabilidade.Submit_Valor(FloatToStr(PoutrosGastos));
                   Self.TITULO.ObjExportaContabilidade.Submit_Historico('OUTROS GASTOS - ENTRADA DE PRODUTOS '+Self.codigo+'- Forn. '+Self.FORNECEDOR.Get_Fantasia);
                   Self.TITULO.ObjExportaContabilidade.Submit_ObjGerador('OBJENTRADAPRODUTOS');
                   Self.TITULO.ObjExportaContabilidade.Submit_CodGerador(Pcodigo);
                   Self.TITULO.ObjExportaContabilidade.Submit_Exportado('N');
                   Self.TITULO.ObjExportaContabilidade.Submit_Exporta('S');

                   if (Self.TITULO.ObjExportaContabilidade.Salvar(False,True)=False)
                   Then Begin
                            MensagemErro('N�o foi poss�vel salvar o lan�amento cont�bil no valor do frete da compra para o fornecedor');
                            exit;
                   End;
         End;//frete

         if (PIcmsSubstituto<>0)
         then begin
                   Self.TITULO.ObjExportaContabilidade.ZerarTabela;
                   Self.TITULO.ObjExportaContabilidade.status:=dsinsert;
                   Self.TITULO.ObjExportaContabilidade.Submit_CODIGO(Self.TITULO.ObjExportaContabilidade.Get_NovoCodigo);
                   Self.TITULO.ObjExportaContabilidade.Submit_Data(Self.DATA);
                   Self.TITULO.ObjExportaContabilidade.Submit_ContaDebite(pcontaIcmsSubstituto);
                   Self.TITULO.ObjExportaContabilidade.Submit_ContaCredite(Pcontafornecedor);
                   Self.TITULO.ObjExportaContabilidade.Submit_Valor(FloatToStr(PIcmsSubstituto));
                   Self.TITULO.ObjExportaContabilidade.Submit_Historico('ICMS SUBSTITUTO - ENTRADA DE PRODUTOS '+Self.codigo+'- Forn. '+Self.FORNECEDOR.Get_Fantasia);
                   Self.TITULO.ObjExportaContabilidade.Submit_ObjGerador('OBJENTRADAPRODUTOS');
                   Self.TITULO.ObjExportaContabilidade.Submit_CodGerador(Pcodigo);
                   Self.TITULO.ObjExportaContabilidade.Submit_Exportado('N');
                   Self.TITULO.ObjExportaContabilidade.Submit_Exporta('S');

                   if (Self.TITULO.ObjExportaContabilidade.Salvar(False,True)=False)
                   Then Begin
                            MensagemErro('N�o foi poss�vel salvar o lan�amento cont�bil no valor do frete da compra para o fornecedor');
                            exit;
                   End;
         End;//frete

         if (Pdesconto>0)
         Then begin
                  //se tiver desconto
                  Self.TITULO.ObjExportaContabilidade.ZerarTabela;
                  Self.TITULO.ObjExportaContabilidade.status:=dsinsert;
                  Self.TITULO.ObjExportaContabilidade.Submit_CODIGO(Self.TITULO.ObjExportaContabilidade.Get_NovoCodigo);
                  Self.TITULO.ObjExportaContabilidade.Submit_Data(Self.DATA);
                  Self.TITULO.ObjExportaContabilidade.Submit_ContaCredite(pcontadesconto);
                  Self.TITULO.ObjExportaContabilidade.Submit_ContaDebite(Pcontafornecedor);
                  Self.TITULO.ObjExportaContabilidade.Submit_Valor(floattostr(Pdesconto));
                  Self.TITULO.ObjExportaContabilidade.Submit_Historico('DESCONTO - ENTRADA DE PRODUTOS '+Self.codigo+'- Forn. '+Self.FORNECEDOR.Get_Fantasia);
                  Self.TITULO.ObjExportaContabilidade.Submit_ObjGerador('OBJENTRADAPRODUTOS');
                  Self.TITULO.ObjExportaContabilidade.Submit_CodGerador(Pcodigo);
                  Self.TITULO.ObjExportaContabilidade.Submit_Exportado('N');
                  Self.TITULO.ObjExportaContabilidade.Submit_Exporta('S');

                  if (Self.TITULO.ObjExportaContabilidade.Salvar(False,true)=False)
                  Then Begin
                           MensagemErro('N�o foi poss�vel salvar o lan�amento cont�bil no valor do desconto para o fornecedor');
                           exit;
                  End;
         End;




    End;

    result:=true;
end;

function TObjENTRADAPRODUTOS.RetornarContabilidade(
  Pcodigo: string): Boolean;
begin
     Result:=False;
     With Self.Objquery do
     Begin
          close;
          SQL.clear;
          sql.add('Select * from TabExportaContabilidade where ObjGerador='+#39+'OBJENTRADAPRODUTOS'+#39+' and codgerador='+#39+Pcodigo+#39);
          sql.add('and Exportado=''S'' ');
          open;

          if (recordcount>0)
          then begin
                    if (messagedlg('Os dados cont�beis dessa entrada ja foram exportados, certeza que deseja que excluir a contabilidade ?',mtConfirmation,[mbyes,mbno],0)=mrno)
                    then exit;

                    if (Self.TITULO.ObjExportaContabilidade.LancaInverso('OBJENTRADAPRODUTOS',Pcodigo,datetostr(now))=False)
                    then begin
                                mensagemerro('N�o foi poss�vel lan�ar o inverso na contabilidade');
                                exit;
                    End;
          End
          Else begin
                    if (Self.TITULO.ObjExportaContabilidade.excluiporgerador('OBJENTRADAPRODUTOS',Pcodigo)=False)
                    then Begin
                              MensagemErro('Erro na tentativa de excluir a contabilidade da entrada');
                              exit;
                    end;
          End;
     End;

     Result:=True;
end;

procedure TObjENTRADAPRODUTOS.RefazContabilidade(PdataInicial,
  PdataFinal: string);
begin
     //primeiro seleciono as entradas desse periodo
     //depois excluo a contabilidade do titulo a pagar
     //logo em seguida contabilidade da propria entrada

     Try
        StrToDate(PdataInicial);
        StrToDate(PdataFinal);
     Except
           MensagemErro('Data Inv�lida');
           exit;
     End;

     With Self.Objquerytemp do
     Begin
          close;
          SQL.clear;                                             
          SQL.add('Select * from TabEntradaProdutos where Data>='+#39+formatdatetime('mm/dd/yyyy', strtodate(pdatainicial))+#39);
          sql.add('and Data<='+#39+formatdatetime('mm/dd/yyyy', strtodate(pdatafinal))+#39);
          sql.add('order by data');
          open;
          last;
          Try
             FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
             FMostraBarraProgresso.Lbmensagem.Caption:='Apagando Contabilidade';
             first;
             While not(eof) do
             Begin
                  FMostraBarraProgresso.IncrementaBarra1(1);
                  FMostraBarraProgresso.Show;

                  if (fieldbyname('Titulo').asstring<>'')
                  Then Begin
                          If (Self.TITULO.ObjExportaContabilidade.excluiporgerador('OBJTITULO',fieldbyname('Titulo').asstring)=False)
                          Then Begin
                                    mensagemerro('Erro na tentativa de apagar a contabilidade do t�tulo '+fieldbyname('Titulo').asstring);
                                    exit;
                          End;
                  End;

                  If (Self.titulo.ObjExportaContabilidade.excluiporgerador('OBJENTRADAPRODUTOS',fieldbyname('codigo').asstring)=False)
                  Then Begin
                            mensagemerro('Erro na tentativa de apagar a contabilidade da entrada '+fieldbyname('codigo').asstring);
                            exit;
                  End;

                  //gerando a nova contabilidade
                  if (Self.GeraContabilidade2(fieldbyname('codigo').asstring)=False)
                  Then Begin
                            MensagemErro('Erro na tentativa de gerar a contabilidade da Entrada '+fieldbyname('codigo').asstring);
                            exit;
                  End;

                  next;
             End;
             MensagemAviso('Conclu�do');
             FDataModulo.IBTransaction.CommitRetaining;

          Finally
                 FDataModulo.IBTransaction.RollbackRetaining;
                 FMostraBarraProgresso.Close;
          End;
          


     End;
end;

function TObjENTRADAPRODUTOS.Get_CreditoCOFINS: string;
begin
     Result:=Self.CreditoCOFINS;
end;

function TObjENTRADAPRODUTOS.Get_CreditoICMS: string;
begin
     Result:=Self.CreditoICMS;
end;

function TObjENTRADAPRODUTOS.Get_CreditoIPI: string;
begin
     Result:=Self.CreditoIPI;
end;

function TObjENTRADAPRODUTOS.Get_CreditoPIS: string;
begin
      Result:=Self.CreditoPIS;
end;

function TObjENTRADAPRODUTOS.Get_valoripi: string;
begin
    Result:=Self.ValorIPI;
end;

procedure TObjENTRADAPRODUTOS.Submit_CreditoCOFINS(parametro: string);
begin
     Self.CreditoCOFINS:=parametro;
end;

procedure TObjENTRADAPRODUTOS.Submit_CreditoICMS(parametro: string);
begin
     Self.CreditoICMS:=parametro;
end;

procedure TObjENTRADAPRODUTOS.Submit_CreditoIPI(parametro: string);
begin
     Self.CreditoIPI:=parametro;
end;

procedure TObjENTRADAPRODUTOS.Submit_CreditoPIS(parametro: string);
begin
     Self.CreditoPIS:=parametro;
end;

procedure TObjENTRADAPRODUTOS.Submit_valoripi(parametro: string);
begin
     Self.valoripi:=parametro;
end;

procedure TObjENTRADAPRODUTOS.Submit_BASECALCULOICMS(parametro:string);
begin
    Self.BASECALCULOICMS:=parametro;
end;

procedure TObjENTRADAPRODUTOS.Submit_VALORICMS(parametro:string);
begin
    Self.VALORICMS:=parametro;
end;

procedure TObjENTRADAPRODUTOS.Submit_BASECALCULOICMSSUBSTITUICAO(parametro:string);
begin
    Self.BASECALCULOICMSSUBSTITUICAO:=parametro;
end;

procedure TObjENTRADAPRODUTOS.Submit_VALORSEGURO(parametro:string);
begin
    Self.VALORSEGURO:=parametro;
end;

function TObjENTRADAPRODUTOS.Get_BASECALCULOICMS:string;
begin
    Result:=Self.BASECALCULOICMS;
end;

function TObjENTRADAPRODUTOS.Get_VALORICMS:string;
begin
    Result:=self.VALORICMS;
end;

function TObjENTRADAPRODUTOS.Get_BASECALCULOICMSSUBSTITUICAO:string;
begin
    Result:=Self.BASECALCULOICMSSUBSTITUICAO;
end;

function TObjENTRADAPRODUTOS.Get_VALORSEGURO:string;
begin
    Result:=Self.VALORSEGURO;
end;

function TObjENTRADAPRODUTOS.Get_icmssobrefrete:string;
begin
    Result:=icmssobrefrete;
end;

function TObjENTRADAPRODUTOS.Get_FreteFornecedor:string;
begin
    Result:=FreteFornecedor;
end;

procedure TObjENTRADAPRODUTOS.Submit_IcmsSobreFrete(parametro:string);
begin
    Self.icmssobrefrete:=parametro;
end;

procedure TObjENTRADAPRODUTOS.Submit_FreteFornecedor(parametro:string);
begin
    Self.FreteFornecedor:=parametro;
end;

procedure TObjENTRADAPRODUTOS.Submit_ChaveNfe(parametro:string);
begin
    self.ChaveNfe:=parametro;
end;

function TObjENTRADAPRODUTOS.Get_ChaveNfe:string;
begin
    Result:=Self.ChaveNfe;
end;

function TObjENTRADAPRODUTOS.Get_serienf: string;
begin
  Result := self.serienf;
end;

procedure TObjENTRADAPRODUTOS.Submit_serienf(parametro: String);
begin
  self.serienf := parametro;
end;

function TObjENTRADAPRODUTOS.get_spedvalido: string;
begin
  result := self.spedvalido;
end;

procedure TObjENTRADAPRODUTOS.submit_spedvalido(parametro: string);
begin
  self.spedvalido := parametro;
end;

end.

