unit UvideosAjuda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw, ExtCtrls, StdCtrls, ComCtrls,ActiveX,
  IdAntiFreezeBase, IdAntiFreeze, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP, jpeg,IBQuery,UDataModulo, Grids,ShellAPI;

type
  TFvideos = class(TForm)
    pnl1: TPanel;
    lbnomeformulario: TLabel;
    lbNomemodulo: TLabel;
    pnl2: TPanel;
    Img1: TImage;
    Pesquisa: TPanel;
    panel2: TPanel;
    pnl6: TPanel;
    wb1: TWebBrowser;
    panel3: TPanel;
    lb2: TLabel;
    img2: TImage;
    lbVernoYoutube: TLabel;
    STRGListaVideos: TStringGrid;
    procedure FormShow(Sender: TObject);
    procedure lbVernoYoutubeMouseLeave(Sender: TObject);
    procedure lbVernoYoutubeMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure STRGListaVideosClick(Sender: TObject);
    procedure lbVernoYoutubeClick(Sender: TObject);
    procedure img2Click(Sender: TObject);
  private
    procedure WebLoadHTML(WebBrowser: TWebBrowser; HTMLCode: string) ;
  public
    Link:String;
    LinkPagina:String;
    procedure PegaLink(parametro:string;NomeModulo:string);
    procedure Buscalinks;
    procedure MontaStringGrid;
  end;

var
  Fvideos: TFvideos;

implementation

uses UescolheImagemBotao;

{$R *.dfm}

procedure TFvideos.FormShow(Sender: TObject);
var
  S,HTML: String;
begin
  FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');
  MontaStringGrid;
  if(Link='')then
  begin
     Buscalinks;
  end;
  HTML :=Link ;
  WebLoadHTML(Wb1,HTML);
end;

procedure TFvideos.Buscalinks;
var
  Query:TibQuery;
begin
  Query:=TIBQuery.Create(nil);
  Query.Database:=FDataModulo.IBDatabase;
  try
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('select tablinksvideos.* from tablinksvideos');
    Query.sql.Add('join tabajuda on tabajuda.codigo=tablinksvideos.ajuda');
    Query.SQL.Add('where nomemenu='+#39+lbNomemodulo.Caption+#39);
    Query.Open;
    STRGListaVideos.Cells[0,0]:='V�DEOS';
    link:=Query.fieldbyname('linkvideo').AsString;
    LinkPagina:=Query.fieldbyname('linkpagina').AsString;
    while not Query.Eof do
    begin
        STRGListaVideos.Cells[0,STRGListaVideos.RowCount-1]:=Query.fieldbyname('nomevideo').AsString;
        STRGListaVideos.Cells[1,STRGListaVideos.RowCount-1]:=Query.fieldbyname('linkpagina').AsString;
        STRGListaVideos.Cells[2,STRGListaVideos.RowCount-1]:=Query.fieldbyname('linkvideo').AsString;
        STRGListaVideos.RowCount:=STRGListaVideos.RowCount+1;
        Query.Next;
    end;
    if(STRGListaVideos.RowCount>2)
    then STRGListaVideos.RowCount:=STRGListaVideos.RowCount-1;
  finally
    FreeAndNil(Query);
  end;

end;

procedure TFvideos.MontaStringGrid;
begin
  with STRGListaVideos do
  begin
     RowCount:=2;
     ColCount:=3;
     ColWidths[0]:=200;
     ColWidths[1]:=0;
     ColWidths[2]:=0;

     Cells[0,0]:='V�DEOS';

  end;
end;

procedure TFvideos.PegaLink(parametro:string;NomeModulo:string);
begin
    Link:=parametro;
    lbNomemodulo.Caption:=NomeModulo;
end;

procedure TFvideos.WebLoadHTML(WebBrowser: TWebBrowser; HTMLCode: string) ;
var
  SL: TStringList;
  MS: TMemoryStream;
begin
  if not Assigned(WebBrowser.Document) then begin
    WebBrowser.Navigate('about:blank');
    while WebBrowser.ReadyState <> READYSTATE_COMPLETE do
      Application.ProcessMessages;
  end;
  SL := TStringList.Create;
  MS := TMemoryStream.Create;
  try
    SL.Text := HTMLCode;
    SL.SaveToStream(MS);
    MS.Seek(0,0);
    (WebBrowser.Document as IPersistStreamInit).Load(TStreamAdapter.Create(ms)) ;
  finally
    SL.Free;
    MS.Free;
  end;
end;

procedure TFvideos.lbVernoYoutubeMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFvideos.lbVernoYoutubeMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFvideos.STRGListaVideosClick(Sender: TObject);
begin
  LinkPagina:=STRGListaVideos.Cells[1,STRGListaVideos.Row];
  WebLoadHTML(Wb1,STRGListaVideos.Cells[2,STRGListaVideos.Row]);
end;

procedure TFvideos.lbVernoYoutubeClick(Sender: TObject);
begin
   ShellExecute(Application.Handle, nil, PChar(LinkPagina), nil, nil, sw_hide);
end;

procedure TFvideos.img2Click(Sender: TObject);
begin
   lbVernoYoutubeClick(Sender);
end;

end.
