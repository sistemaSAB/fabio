unit UNOTAFISCALCFOP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjNOTAFISCALCFOP;

type
  TFNOTAFISCALCFOP = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbNOTAFISCAL: TLabel;
    EdtNOTAFISCAL: TEdit;
    LbCFOP: TLabel;
    EdtCFOP: TEdit;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdtNOTAFISCALKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtNOTAFISCALExit(Sender: TObject);
    procedure EdtCFOPExit(Sender: TObject);
    procedure EdtCFOPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FNOTAFISCALCFOP: TFNOTAFISCALCFOP;
  ObjNOTAFISCALCFOP:TObjNOTAFISCALCFOP;

implementation

uses UessencialGlobal, Upesquisa;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFNOTAFISCALCFOP.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjNOTAFISCALCFOP do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        NOTAFISCAL.Submit_Codigo(edtNOTAFISCAL.text);
        CFOP.Submit_Codigo(edtCFOP.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFNOTAFISCALCFOP.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjNOTAFISCALCFOP do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNOTAFISCAL.text:=NOTAFISCAL.Get_codigo;
        EdtCFOP.text:=CFOP.Get_CODIGO;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFNOTAFISCALCFOP.TabelaParaControles: Boolean;
begin
     If (ObjNOTAFISCALCFOP.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFNOTAFISCALCFOP.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        ObjNOTAFISCALCFOP:=TObjNOTAFISCALCFOP.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

procedure TFNOTAFISCALCFOP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjNOTAFISCALCFOP=Nil)
     Then exit;

If (ObjNOTAFISCALCFOP.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

ObjNOTAFISCALCFOP.free;
end;

procedure TFNOTAFISCALCFOP.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFNOTAFISCALCFOP.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjNOTAFISCALCFOP.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjNOTAFISCALCFOP.status:=dsInsert;
     Guia.pageindex:=0;
     EdtNOTAFISCAL.setfocus;

end;


procedure TFNOTAFISCALCFOP.btalterarClick(Sender: TObject);
begin
    If (ObjNOTAFISCALCFOP.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjNOTAFISCALCFOP.Status:=dsEdit;
                guia.pageindex:=0;
                EdtNOTAFISCAL.setfocus;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;


end;

procedure TFNOTAFISCALCFOP.btgravarClick(Sender: TObject);
begin

     If ObjNOTAFISCALCFOP.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjNOTAFISCALCFOP.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjNOTAFISCALCFOP.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFNOTAFISCALCFOP.btexcluirClick(Sender: TObject);
begin
     If (ObjNOTAFISCALCFOP.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjNOTAFISCALCFOP.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjNOTAFISCALCFOP.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFNOTAFISCALCFOP.btcancelarClick(Sender: TObject);
begin
     ObjNOTAFISCALCFOP.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFNOTAFISCALCFOP.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFNOTAFISCALCFOP.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjNOTAFISCALCFOP.Get_pesquisa,ObjNOTAFISCALCFOP.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjNOTAFISCALCFOP.status<>dsinactive
                                  then exit;

                                  If (ObjNOTAFISCALCFOP.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjNOTAFISCALCFOP.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFNOTAFISCALCFOP.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFNOTAFISCALCFOP.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;

procedure TFNOTAFISCALCFOP.EdtNOTAFISCALKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjNOTAFISCALCFOP.EdtNOTAFISCALKeyDown(sender,key,shift,nil);
end;

procedure TFNOTAFISCALCFOP.EdtNOTAFISCALExit(Sender: TObject);
begin
     ObjNOTAFISCALCFOP.EdtNOTAFISCALexit(sender,nil);
end;

procedure TFNOTAFISCALCFOP.EdtCFOPExit(Sender: TObject);
begin
     ObjNOTAFISCALCFOP.EdtCFOPExit(sender,nil);
end;

procedure TFNOTAFISCALCFOP.EdtCFOPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjNOTAFISCALCFOP.EdtCFOPKeyDown(sender,key,shift,nil);
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(ObjNOTAFISCALCFOP.OBJETO.Get_Pesquisa,ObjNOTAFISCALCFOP.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(OBJNOTAFISCALCFOP.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If OBJNOTAFISCALCFOP.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(OBJNOTAFISCALCFOP.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
