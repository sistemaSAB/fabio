unit UescolheTransportadora;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons,UObjNotaFiscal;

type
  TFEscolheTransportadora = class(TForm)
    PanelTransportadora: TPanel;
    LbNomeTransportadora: TLabel;
    LbEnderecoTransportadora: TLabel;
    LbMunicipioTransportadora: TLabel;
    LbIETransportadora: TLabel;
    LbUFTransportadora: TLabel;
    LbUFVeiculoTransportadora: TLabel;
    LbCNPJTransportadora: TLabel;
    LbPlacaVeiculoTransportadora: TLabel;
    EdtNomeTransportadora: TEdit;
    EdtEnderecoTransportadora: TEdit;
    EdtMunicipioTransportadora: TEdit;
    EdtIETransportadora: TEdit;
    EdtUFTransportadora: TEdit;
    EdtUFVeiculoTransportadora: TEdit;
    EdtCNPJTransportadora: TEdit;
    EdtPlacaVeiculoTransportadora: TEdit;
    Label9: TLabel;
    Panel1: TPanel;
    Label13: TLabel;
    lbtransportadora: TLabel;
    LbFreteporContaTransportadora: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    edttransportadora: TEdit;
    EdtFreteporContaTransportadora: TEdit;
    Panel2: TPanel;
    Label1: TLabel;
    EdtQuantidade: TEdit;
    Label2: TLabel;
    edtEspecie: TEdit;
    lbMarca: TLabel;
    edtMarca: TEdit;
    lbNumeracao: TLabel;
    edtNumeracao: TEdit;
    Label3: TLabel;
    edtPesoBruto: TEdit;
    Label4: TLabel;
    edtPesoLiquido: TEdit;
    btOK: TBitBtn;
    btCancelar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edttransportadoraExit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btOKClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure edtPesoBrutoKeyPress(Sender: TObject; var Key: Char);
    procedure edtPesoLiquidoKeyPress(Sender: TObject; var Key: Char);
    procedure EdtQuantidadeKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumeracaoKeyPress(Sender: TObject; var Key: Char);
    procedure edtPesoBrutoExit(Sender: TObject);
    procedure edtPesoLiquidoExit(Sender: TObject);
    procedure EdtQuantidadeExit(Sender: TObject);
    procedure edttransportadoraKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edttransportadoraKeyPress(Sender: TObject; var Key: Char);
    procedure EdtFreteporContaTransportadoraKeyPress(Sender: TObject;
      var Key: Char);
    procedure EdtPlacaVeiculoTransportadoraKeyPress(Sender: TObject;
      var Key: Char);
    procedure EdtCNPJTransportadoraKeyPress(Sender: TObject;
      var Key: Char);
    procedure EdtIETransportadoraKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private

  public
    objNotaFiscal:TObjNotaFiscal;
    procedure get_dados (pCodigoTransportadora:string);
  end;

var
  FEscolheTransportadora: TFEscolheTransportadora;

implementation

uses UessencialGlobal, UobjTRANSPORTADORA;

{$R *.dfm}

procedure TFEscolheTransportadora.FormCreate(Sender: TObject);
begin
      Self.Tag:=0;
       self.objNotaFiscal:=TObjNotaFiscal.Create;
end;

procedure TFEscolheTransportadora.FormKeyPress(Sender: TObject;
  var Key: Char);
begin

      if (key = #13) then
        Perform(Wm_NextDlgCtl,0,0)
      else
      begin

        if (Key=#27) then
          Self.Close;

      end;

end;

procedure TFEscolheTransportadora.edttransportadoraExit(Sender: TObject);
begin

  self.objNotaFiscal.EdtTransportadoraExit (Sender,lbtransportadora);

end;

procedure TFEscolheTransportadora.BitBtn1Click(Sender: TObject);
begin
      self.Tag:=1;
      Self.Close;
end;

procedure TFEscolheTransportadora.btOKClick(Sender: TObject);
begin

    if (Trim (EdtFreteporContaTransportadora.Text) = '') then
    begin

      MensagemAviso ('Preencha o campo ''Frete por conta''');
      EdtFreteporContaTransportadora.SetFocus;
      Exit;

    end;

    if (Trim(EdtQuantidade.Text) = '') then
    begin
      MensagemAviso ('Preencha o campo ''quantidade''');
      EdtQuantidade.SetFocus;
      Exit;
    end;

  self.Tag:=1;
  Self.Close;

end;

procedure TFEscolheTransportadora.btCancelarClick(Sender: TObject);
begin

  self.Tag:=0;
  Self.Close;


end;

procedure TFEscolheTransportadora.edtPesoBrutoKeyPress(Sender: TObject;
  var Key: Char);
begin

  ValidaNumeros(tedit(Sender),Key,'float');

end;

procedure TFEscolheTransportadora.edtPesoLiquidoKeyPress(Sender: TObject;
  var Key: Char);
begin

  ValidaNumeros(tedit(Sender),Key,'float');

end;

procedure TFEscolheTransportadora.EdtQuantidadeKeyPress(Sender: TObject;var Key: Char);
begin

    if not (Key in['0'..'9',Chr(8)]) then
      Key:= #0;

end;

procedure TFEscolheTransportadora.edtNumeracaoKeyPress(Sender: TObject;
  var Key: Char);
begin
ValidaNumeros(tedit(Sender),Key,'integer');
end;

procedure TFEscolheTransportadora.edtPesoBrutoExit(Sender: TObject);
begin

   if (TEdit (Sender).Text <> '') then
   begin

      try

        StrToCurr (TEdit (Sender).text);

      except

        MensagemErro ('Peso inv�lido');
        TEdit (Sender).SetFocus;

      end;

   end;


end;

procedure TFEscolheTransportadora.edtPesoLiquidoExit(Sender: TObject);
begin

   if (TEdit (Sender).Text <> '') then
   begin

      try

        StrToCurr (TEdit (Sender).text);

      except

        MensagemErro ('Peso inv�lido');
        TEdit (Sender).SetFocus;

      end;

   end;

end;

procedure TFEscolheTransportadora.EdtQuantidadeExit(Sender: TObject);
begin

   if (TEdit (Sender).Text <> '') then
   begin

      try

        StrToInt (TEdit (Sender).text);

      except

        MensagemErro ('Quantidade inv�lida');
        TEdit (Sender).SetFocus;

      end;

   end;

end;

procedure TFEscolheTransportadora.edttransportadoraKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin

  self.objNotaFiscal.EdtTransportadoraKeyDown (Sender,Key,Shift,lbtransportadora);
  self.get_dados (self.edttransportadora.Text);

end;

procedure TFEscolheTransportadora.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

  self.objNotaFiscal.Free;

end;

procedure TFEscolheTransportadora.get_dados (pCodigoTransportadora:string);
begin

  if (pCodigoTransportadora = '') then Exit;

  with (self.objNotaFiscal.Transportadora) do
  begin

    LocalizaCodigo (pCodigoTransportadora);
    TabelaparaObjeto;

    self.EdtNomeTransportadora.Text         := Get_NOME      ();
    Self.EdtPlacaVeiculoTransportadora.Text := Get_PLACA     ();
    self.EdtUFVeiculoTransportadora.Text    := get_UFPlaca   ();
    self.EdtCNPJTransportadora.Text         := get_CPFCNPJ   ();
    self.EdtEnderecoTransportadora.Text     := get_Endereco  ();
    self.EdtMunicipioTransportadora.Text    := get_Municipio ();
    self.EdtUFTransportadora.Text           := get_UF        ();
    self.EdtIETransportadora.Text           := get_IE        ();

  end;


end;

procedure TFEscolheTransportadora.edttransportadoraKeyPress(
  Sender: TObject; var Key: Char);
begin

   if not (Key in['0'..'9',Chr(8)]) then
      Key:= #0;

end;

procedure TFEscolheTransportadora.EdtFreteporContaTransportadoraKeyPress(
  Sender: TObject; var Key: Char);
begin

  if not (Key in['0'..'9',Chr(8)]) then
      Key:= #0;

end;

procedure TFEscolheTransportadora.EdtPlacaVeiculoTransportadoraKeyPress(
  Sender: TObject; var Key: Char);
begin

  if not (Key in['0'..'9','a'..'z','A'..'Z',Chr(8)]) then
    Key:= #0

end;

procedure TFEscolheTransportadora.EdtCNPJTransportadoraKeyPress(Sender: TObject; var Key: Char);
begin

  if not (Key in['0'..'9',Chr(8)]) then
    Key:= #0

end;

procedure TFEscolheTransportadora.EdtIETransportadoraKeyPress(
  Sender: TObject; var Key: Char);
begin

   if not (Key in['0'..'9',Chr(8)]) then
    Key:= #0

end;

procedure TFEscolheTransportadora.FormShow(Sender: TObject);
begin

  self.EdtNomeTransportadora.SetFocus;

end;

end.
