unit UobjDIVERSO_PP;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJPEDIDO_PROJ
,UOBJDiversoCor;

Type
   TObjDiverso_PP=class

          Public
                ObjDataSource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                PedidoProjeto:TOBJPEDIDO_PROJ;
                DiversoCor:TOBJDIVERSOCor;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure ResgataDIVERSO_PP(PPedido, PPEdidoProjeto: string);
                Function ResgataValorFinal(PPedido:string;Var PValor:Currency):Boolean;

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Quantidade(parametro: string);
                Function Get_Quantidade: string;
                Procedure Submit_Altura(parametro: string);
                Function Get_Altura: string;
                Procedure Submit_Largura(parametro: string);
                Function Get_Largura: string;
                Procedure Submit_Valor(parametro: string);
                Function Get_Valor: string;
                Function Get_ValorFinal:string;

                procedure EdtPedidoProjetoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtDiversoExit(Sender: TObject;Var PEdtCodigo:TEdit;LABELNOME:TLABEL);
                procedure EdtDiversoKeyDown(Sender: TObject; Var PEdtCodigo:TEdit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Function DeletaDiverso_PP(PPedidoProjeto:string):Boolean;

                Function Soma_por_PP(PpedidoProjeto:string):Currency;
                Procedure RetornaDadosRel(PpedidoProjeto:string;STrDados:TStrings);
                Procedure RetornaDadosRelProjetoBranco(PpedidoProjeto:string;STrDados:TStrings; Var PTotal:Currency);

                Function PreencheDiversoPP(PPedidoProjeto:string;var PValor:Currency;PprojetoemBRanco:Boolean):Boolean;

                function DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string; PControlaNegativo: Boolean;Pdata: String): boolean;


         Private
               Objquery:Tibquery;
               ObjQueryPesquisa:TIBquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Quantidade:string;
               Altura:string;
               Largura:string;
               Valor:string;
               ValorFinal:string;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UmostraStringList;



Function  TObjDiverso_PP.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If(FieldByName('PedidoProjeto').asstring<>'')
        Then Begin
                 If (Self.PedidoProjeto.LocalizaCodigo(FieldByName('PedidoProjeto').asstring)=False)
                 Then Begin
                          Messagedlg('PedidoProjeto N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PedidoProjeto.TabelaparaObjeto;
        End;
        If(FieldByName('DiversoCor').asstring<>'')
        Then Begin
                 If (Self.DiversoCor.LocalizaCodigo(FieldByName('DiversoCor').asstring)=False)
                 Then Begin
                          Messagedlg('DiversoCor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.DiversoCor.TabelaparaObjeto;
        End;
        Self.Quantidade:=fieldbyname('Quantidade').asstring;
        Self.Altura:=fieldbyname('Altura').asstring;
        Self.Largura:=fieldbyname('Largura').asstring;
        Self.Valor:=fieldbyname('Valor').asstring;
        Self.ValorFinal:=fieldbyname('ValorFinal').AsString;
        result:=True;
     End;
end;


Procedure TObjDiverso_PP.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('PedidoProjeto').asstring:=Self.PedidoProjeto.GET_CODIGO;
        ParamByName('DiversoCor').asstring:=Self.DiversoCor.GET_CODIGO;
        ParamByName('Quantidade').asstring:=virgulaparaponto(Self.Quantidade);
        ParamByName('Altura').asstring:=virgulaparaponto(Self.Altura);
        ParamByName('Largura').asstring:=virgulaparaponto(Self.Largura);
        ParamByName('Valor').asstring:=virgulaparaponto(Self.Valor);
//CODIFICA OBJETOPARATABELA

  End;
End;

//***********************************************************************

function TObjDiverso_PP.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjDiverso_PP.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        PedidoProjeto.ZerarTabela;
        DiversoCor.ZerarTabela;
        Quantidade:='';
        Altura:='';
        Largura:='';
        Valor:='';
        ValorFinal:='';








     End;
end;

Function TObjDiverso_PP.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjDiverso_PP.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.PedidoProjeto.LocalizaCodigo(Self.PedidoProjeto.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ PedidoProjeto n�o Encontrado!';
      If (Self.DiversoCor.LocalizaCodigo(Self.DiversoCor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ DiversoCor n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjDiverso_PP.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.PedidoProjeto.Get_Codigo<>'')
        Then Strtoint(Self.PedidoProjeto.Get_Codigo);
     Except
           Mensagem:=mensagem+'/PedidoProjeto';
     End;
     try
        If (Self.DiversoCor.Get_Codigo<>'')
        Then Strtoint(Self.DiversoCor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/DiversoCor';
     End;
     try
        Strtofloat(Self.Quantidade);
     Except
           Mensagem:=mensagem+'/Quantidade';
     End;
     try
        if (Self.Altura<>'')then
        Strtofloat(Self.Altura);
     Except
           Mensagem:=mensagem+'/Altura';
     End;
     try
        if (Self.Largura<>'')then
        Strtofloat(Self.Largura);
     Except
           Mensagem:=mensagem+'/Largura';
     End;
     try
        Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjDiverso_PP.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjDiverso_PP.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjDiverso_PP.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro Diverso_PP vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,PedidoProjeto,DiversoCor,Quantidade,Altura,Largura,Valor,ValorFinal');
           SQL.ADD(' ');
           SQL.ADD(' from  TABDiverso_PP');
           SQL.ADD(' WHERE Codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjDiverso_PP.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjDiverso_PP.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjDiverso_PP.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDataSource.DataSet:=Self.ObjQueryPesquisa;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.PedidoProjeto:=TOBJPEDIDO_PROJ.create;
        Self.DiversoCor:=TOBJDiversoCor.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABDiverso_PP(Codigo,PedidoProjeto,DiversoCor');
                InsertSQL.add(' ,Quantidade,Altura,Largura,Valor)');
                InsertSQL.add('values (:Codigo,:PedidoProjeto,:DiversoCor,:Quantidade');
                InsertSQL.add(' ,:Altura,:Largura,:Valor)');

                ModifySQL.clear;
                ModifySQL.add('Update TABDiverso_PP set Codigo=:Codigo,PedidoProjeto=:PedidoProjeto');
                ModifySQL.add(',DiversoCor=:DiversoCor,Quantidade=:Quantidade,Altura=:Altura');
                ModifySQL.add(',Largura=:Largura,Valor=:Valor');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABDiverso_PP where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjDiverso_PP.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjDiverso_PP.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabDiverso_PP');
     Result:=Self.ParametroPesquisa;
end;

function TObjDiverso_PP.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Diverso_PP ';
end;


function TObjDiverso_PP.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENDiverso_PP,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENDiverso_PP,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjDiverso_PP.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(Self.ObjQueryPesquisa);
    Freeandnil(Self.ObjDataSource);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.PedidoProjeto.FREE;
Self.DiversoCor.FREE;
//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjDiverso_PP.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjDiverso_PP.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjDiverso_PP.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjDiverso_PP.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjDiverso_PP.Submit_Quantidade(parametro: string);
begin
        Self.Quantidade:=Parametro;
end;
function TObjDiverso_PP.Get_Quantidade: string;
begin
        Result:=Self.Quantidade;
end;
procedure TObjDiverso_PP.Submit_Altura(parametro: string);
begin
        Self.Altura:=Parametro;
end;
function TObjDiverso_PP.Get_Altura: string;
begin
        Result:=Self.Altura;
end;
procedure TObjDiverso_PP.Submit_Largura(parametro: string);
begin
        Self.Largura:=Parametro;
end;
function TObjDiverso_PP.Get_Largura: string;
begin
        Result:=Self.Largura;
end;
procedure TObjDiverso_PP.Submit_Valor(parametro: string);
begin
        Self.Valor:=Parametro;
end;
function TObjDiverso_PP.Get_Valor: string;
begin
        Result:=Self.Valor;
end;
//CODIFICA GETSESUBMITS


procedure TObjDiverso_PP.EdtPedidoProjetoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PedidoProjeto.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PedidoProjeto.tabelaparaobjeto;
End;
procedure TObjDiverso_PP.EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.PedidoProjeto.Get_Pesquisa,Self.PedidoProjeto.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoProjeto.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.PedidoProjeto.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoProjeto.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjDiverso_PP.EdtDiversoExit(Sender: TObject;Var PEdtCodigo:TEdit;  LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.DiversoCor.Diverso.localizaReferencia (TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     Self.DiversoCor.Diverso.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.DiversoCor.Diverso.Get_Descricao;
     PEdtCodigo.Text:=Self.DiversoCor.Get_Codigo;

End;
procedure TObjDiverso_PP.EdtDiversoKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin


     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from  VIEWDIVERSOCOR',Self.DiversoCor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('REF_DIVERSO').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 LABELNOME.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('Descricao').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjDiverso_PP.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJDiverso_PP';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjDiverso_PP.ResgataDiverso_PP(PPedido, PPEdidoProjeto: string);
begin
       With Self.ObjQueryPesquisa do
       Begin
           Close;
           SQL.Clear;
           Sql.add('Select TabDiverso.Referencia, TabDiverso.Descricao as Diverso,');
           Sql.add('TabCor.Descricao as Cor,');
           Sql.add('TabDiverso_PP.Quantidade,');
           Sql.add('TabDiverso_PP.Altura,');
           Sql.add('TabDiverso_PP.Largura,');
           Sql.add('TabDiverso_PP.Valor,');
           Sql.add('TabDiverso_PP.ValorFinal,TabPedido_Proj.Codigo as PedidoProjeto,');
           Sql.add('TabDiverso_PP.Codigo');
           Sql.add('from TabDiverso_PP');
           Sql.add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabDiverso_PP.PedidoProjeto');
           Sql.add('Join TabPedido on TabPedido.Codigo = TabPedido_Proj.Pedido');
           Sql.add('Join TabDiversoCor on TabDiversoCor.Codigo = TabDiverso_PP.DiversoCor');
           Sql.add('join TabDiverso  on TabDiverso.Codigo = TabDiversoCor.Diverso');
           Sql.add('Join TabCor on TabCor.Codigo = TabDiversoCor.Cor');

           Sql.add('Where TabPedido_Proj.Pedido = '+PPedido);
           if (PpedidoProjeto <> '' )
           then Sql.add('and   TabPedido_Proj.Codigo ='+PPedidoProjeto);

           if (PPedido = '')then
           exit;

           Open;                    
       end;

end;

function TObjDiverso_PP.DeletaDiverso_PP(PPedidoProjeto: string): Boolean;
begin
     Result:=False;
     With Objquery  do
     Begin
        Close;
        SQL.Clear;
        Sql.Add('Delete from TabDiverso_PP where PedidoProjeto='+ppedidoprojeto);
        Try
            execsql;
        Except
              On E:Exception do
              Begin
                   Messagedlg('Erro na tentativa de Excluir os DiversoCors do Projeto '+#13+E.message,mterror,[mbok],0);
                   exit;
              End;
        End;
     end;
     Result:=true;
end;

function TObjDiverso_PP.Get_ValorFinal: string;
begin
    Result := Self.ValorFinal;
end;

function TObjDiverso_PP.ResgataValorFinal(PPedido: string;
  var PValor: Currency): Boolean;
begin
      Result:=False;
      With Self.Objquery  do
      Begin
           Close;
           Sql.Clear;
           Sql.Add('Select SUM(ValorFinal) as ValorFinal');
           Sql.Add('from TabDiverso_PP');
           Sql.Add('Where TabDiverso_PP.PedidoProjeto = '+PPedido);

           if (PPedido='')then
           exit;

           Open;

           PValor:=fieldbyname('ValorFinal').AsCurrency;

           Result:=true;
      end;

end;

function TObjDiverso_PP.Soma_por_PP(PpedidoProjeto: string): Currency;
begin
     Self.Objquery.close;
     Self.Objquery.sql.clear;
     Self.Objquery.sql.add('Select sum(valorfinal) as SOMA from TabDiverso_PP where PedidoProjeto='+PpedidoProjeto);
     Try
         Self.Objquery.open;
         Result:=Self.Objquery.fieldbyname('soma').asfloat;
     Except
           Result:=0;
     End;

end;

procedure TObjDiverso_PP.RetornaDadosRel(PpedidoProjeto: string;  STrDados: TStrings);
begin
     if (PpedidoProjeto='')
     Then exit;

     With Self.ObjQueryPesquisa do
     Begin
          close;
          SQL.clear;
          sql.add('Select codigo from TabDiverso_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          first;

          if (RecordCount = 0)then
          exit;

          While not(eof) do
          Begin
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;
               STrDados.Add(CompletaPalavra(Self.DiversoCor.Diverso.Get_Referencia,10,' ')+' '+
                            CompletaPalavra(Self.DiversoCor.Cor.Get_Referencia+'-'+Self.DiversoCor.Cor.Get_Descricao,16,' ')+' '+
                            CompletaPalavra(Self.DiversoCor.Diverso.Get_Descricao,45,' ')+' '+
                            CompletaPalavra(Self.Get_Quantidade+' '+Self.DiversoCor.Diverso.Get_Unidade,6,' ')+' '+
                            CompletaPalavra_a_Esquerda(formata_valor(Self.Get_Valor),10,' ')+' '+
                            CompletaPalavra_a_Esquerda(formata_valor(StrToCurr(Self.Get_Quantidade)*StrToCurr(Self.Get_Valor)),8,' '));
              next;
          end;
          close;
          sql.clear;
          sql.add('Select sum(ValorFinal) as SOMA FROM TabDiverso_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          STrDados.Add('?'+CompletaPalavra_a_Esquerda('Total DIVERSOS: '+formata_valor(fieldbyname('soma').asstring),100,' '));
          STrDados.Add(' ');
     End;
end;
function TObjDiverso_PP.PreencheDiversoPP(PPedidoProjeto: string; var PValor: Currency; PprojetoemBRanco: Boolean): Boolean;
Begin

    Result:=false;
    Pvalor:=0;

    //Localizo o Pedido Projeto
    if (Self.PedidoProjeto.LocalizaCodigo(PPedidoProjeto)=false)
    then exit;
    Self.PedidoProjeto.TabelaparaObjeto;
    //**********************************

    With ObjQueryPesquisa do
    Begin
        //Apaga Apenas os Diversos ligadas a este Pedido Projeto
        Self.DeletaDiverso_PP(PPedidoProjeto);

        if (PprojetoemBRanco=True)
        Then Begin
                  result:=True;
                  exit;
        End;

        //esse sqls faz um relacionamento entre os diversos de um projeto
        //e a tabela DiversoCor, para saber quais dos Diversos de um determinado
        //projeto nao esto cadastar na COR X, esse sql abaixo retorna
        //as que estaum nesse caso, para testa-lo no ibconsole
        //retire o distinct e acrescente TabDiversoCor.Cor para visualizar
        //e retire a clausula where

        close;
        sql.clear;
        sql.add('Select distinct(TabDiverso_Proj.Diverso)');
        sql.add('from TabDiverso_Proj');
        sql.add('left join TabDiversoCor on');
        sql.add('(TabDiverso_Proj.Diverso=TabDiversoCor.Diverso and TabDiversoCor.Cor='+Self.PedidoProjeto.CorDiverso.Get_Codigo+')');
        sql.add('Where TabDiverso_Proj.Projeto='+Self.PedidoProjeto.Projeto.Get_Codigo);
        sql.add('and TabDiversoCor.Cor is null');
        open;
        last;
        if (recordcount>0)
        Then Begin
                  if (Messagedlg('Alguns Diversos cadastrados para este projeto n�o existem na Cor Escolhida, n�o ser� poss�vel continuar.'+#13+'Deseja visualiza-las?',mtconfirmation,[mbyes,mbno],0)=mrno)
                  Then exit;
                  FmostraStringList.Memo.clear;
                  FmostraStringList.Memo.Lines.add('DIVERSOS N�O CADASTRADOS PARA A COR='+Self.PedidoProjeto.CorDiverso.Get_Codigo+'-'+Self.PedidoProjeto.CorDiverso.Get_Descricao);
                  FmostraStringList.Memo.Lines.add(' ');
                  first;
                  While not(eof) do
                  Begin
                       Self.DiversoCor.Diverso.LocalizaCodigo(fieldbyname('Diverso').asstring);
                       Self.DiversoCor.Diverso.TabelaparaObjeto;
                       FmostraStringList.Memo.Lines.add(Self.DiversoCor.Diverso.Get_Codigo+' - '+Self.DiversoCor.Diverso.Get_Descricao);
                       next;
                  End;
                  FmostraStringList.ShowModal;
                  exit;
        End;


        //Esse SQL Retorna o Codigo da DiversoCor,o PReco, e a quantidade (cadastrada no projeto)

        Close;
        SQL.Clear;
        SQL.Add('Select  TabDiversoCor.Codigo as DiversoCor, TabDiverso_Proj.Quantidade,');

        if (Self.PedidoProjeto.Get_TipoPedido='INSTALADO')
        Then SQL.Add('(tabDiverso.PRecoVendaInstalado+(TabDiverso.PrecoVendaInstalado*TabDiversoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        if (Self.PedidoProjeto.Get_TipoPedido='RETIRADO')
        Then SQL.Add('(tabDiverso.PRecoVendaretirado+(TabDiverso.PrecoVendaretirado*TabDiversoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        if (Self.PedidoProjeto.Get_TipoPedido='FORNECIDO')
        Then SQL.Add('(tabDiverso.PRecoVendafornecido+(TabDiverso.PrecoVendafornecido*TabDiversoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

        SQL.Add('from TabDiverso_Proj');
        SQL.Add('Join TabDiversoCor on TabDiversoCOr.Diverso = TabDiverso_Proj.Diverso');
        SQL.Add('join TabDiverso on TabDiverso.Codigo = TabDiversoCor.Diverso');
        SQL.Add('Where TabDiverso_Proj.Projeto = '+Self.PedidoProjeto.Projeto.Get_Codigo);
        SQL.Add('and   TabDiversoCor.Cor = '+Self.PedidoProjeto.CorDiverso.Get_Codigo);
        Open;

        // Preenchendo a TabDiverso_PP
        While not (eof) do
        Begin
            Self.Status:=dsInsert;
            Self.Submit_Codigo(Self.Get_NovoCodigo);
            //Self.PedidoProjeto.Submit_Codigo(Self);
            Self.DiversoCor.Submit_Codigo(fieldbyname('DiversoCor').AsString);
            Self.Submit_Quantidade(fieldbyname('Quantidade').AsString);
            //Self.Submit_Valor(fieldbyname('Preco').AsString);
            Self.Submit_Valor(formata_valor(fieldbyname('Preco').AsString,4,True));
            if Self.Salvar(false)=false then
            Begin
                 Result:=false;
                 exit;
            end;
            Next;
        end;

        PValor:=0;
        Pvalor:=Self.Soma_por_PP(Self.PedidoProjeto.Get_Codigo);
        result:=true;
    end;

end;


function TobjDiverso_pp.DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;PControlaNegativo: Boolean;Pdata:String): boolean;var PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          Sql.add('Select codigo,Diversocor,(quantidade*-1) as quantidade from TabDiverso_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;

          first;
          
          While not(eof) do
          Begin
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Status:=dsinsert;
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(fieldbyname('quantidade').asstring);
              OBJESTOQUEGLOBAL.Submit_DATA(Pdata);
              OBJESTOQUEGLOBAL.Submit_diversoCOR(fieldbyname('diversocor').asstring);
              OBJESTOQUEGLOBAL.Submit_PEDIDOPROJETOROMANEIO(PpedidoProjetoRomaneio);
              OBJESTOQUEGLOBAL.Submit_diverso_PP(fieldbyname('codigo').asstring);
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('ROMANEIO');



              Self.DiversoCor.LocalizaCodigo(fieldbyname('Diversocor').asstring);
              Self.DiversoCor.TabelaparaObjeto;

              try
                 PestoqueAtual:=strtofloat(Self.DiversoCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual do Diverso '+Self.DiversoCor.Diverso.Get_Descricao+' da cor '+Self.DiversoCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              Try
                PnovoEstoque:=PestoqueAtual+fieldbyname('quantidade').asfloat;
              Except
                    Messagedlg('Erro na gera��o do novo Estoque do Diverso '+Self.DiversoCor.Diverso.Get_Descricao+' da cor '+Self.DiversoCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              if (Pnovoestoque<0)
              then Begin
                        if (PControlaNegativo=True)
                        Then begin
                                  Messagedlg('O Diverso '+Self.DiversoCor.Diverso.Get_Descricao+' da cor '+Self.DiversoCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                  exit;
                        End;
              End;

              if (OBJESTOQUEGLOBAL.Salvar(False)=False)
              then exit;

              next;
          End;
          result:=True;
          exit;
     End;
end;


procedure TObjDiverso_PP.RetornaDadosRelProjetoBranco(PpedidoProjeto: string; STrDados: TStrings; var PTotal: Currency);
begin
     if (PpedidoProjeto='')
     Then exit;

     With Self.ObjQueryPesquisa do
     Begin
          {close;
          SQL.clear;
          sql.add('Select codigo from TabDiverso_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          first;

          if (RecordCount = 0)then
          exit;

          While not(eof) do
          Begin
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;
               STrDados.Add(CompletaPalavra(Self.DiversoCor.Diverso.Get_Referencia,10,' ')+' '+
                            CompletaPalavra(Self.DiversoCor.Cor.Get_Referencia+'-'+Self.DiversoCor.Cor.Get_Descricao,16,' ')+' '+
                            CompletaPalavra(Self.DiversoCor.Diverso.Get_Descricao,45,' ')+' '+
                            CompletaPalavra(Self.Get_Quantidade+' '+Self.DiversoCor.Diverso.Get_Unidade,6,' ')+' '+
                            CompletaPalavra_a_Esquerda(formata_valor(Self.Get_Valor),10,' ')+' '+
                            CompletaPalavra_a_Esquerda(formata_valor(StrToCurr(Self.Get_Quantidade)*StrToCurr(Self.Get_Valor)),8,' '));
              next;
          end;}

          close;
          sql.clear;
          sql.add('Select TabDiverso_PP.codigo,');
          sql.add('TabDiverso.Referencia,');
          sql.add('(TabCor.referencia||''-''||TabCor.Descricao) as COR,');
          sql.add('TabDiverso.Descricao,');
          sql.add('TabDiverso_PP.quantidade,');
          sql.add('TabDiverso.unidade,');
          sql.add('TabDiverso_PP.valor,');
          sql.add('(TabDiverso_PP.valor*TabDiverso_PP.quantidade)as mult');
          sql.add('from tabdiverso_pp');
          sql.add('join TabDiversoCor on TabDiverso_PP.DiversoCor=TabDiversoCor.codigo');
          sql.add('join tabcor on tabdiversocor.cor=tabcor.codigo');
          sql.add('join tabdiverso on Tabdiversocor.diverso=tabdiverso.codigo');
          sql.add('where TabDiverso_PP.PedidoProjeto='+PpedidoProjeto);
          open;
          first;

          if (RecordCount = 0)
          then exit;

          While not(eof) do
          Begin
               //Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               //Self.TabelaparaObjeto;
               STrDados.Add(CompletaPalavra(fieldbyname('referencia').asstring,10,' ')+' '+
                            CompletaPalavra(fieldbyname('cor').asstring,16,' ')+' '+
                            CompletaPalavra(fieldbyname('descricao').asstring,45,' ')+' '+
                            CompletaPalavra(fieldbyname('quantidade').asstring+' '+fieldbyname('unidade').asstring,6,' ')+' '+
                            CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),10,' ')+' '+
                            CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('mult').asstring),8,' '));
              next;
          end;

          close;
          sql.clear;
          sql.add('Select sum(ValorFinal) as SOMA FROM TabDiverso_PP where PedidoProjeto='+PpedidoProjeto);
          open;

          PTotal:=PTotal+fieldbyname('soma').AsCurrency;
     End;

end;

end.




