unit UGRUPOVIDRO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjVIDRO,
  UessencialGlobal, Tabs,IBQuery,UPLanodeContas;

type
  TFGRUPOVIDRO = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigoGrupoVidro: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    lbLbNome: TLabel;
    EdtNome: TEdit;
    lb5: TLabel;
    EdtPlanodeContas: TEdit;
    grp1: TGroupBox;
    lbLbPorcentagemRetirado: TLabel;
    lbLbPorcentagemFornecido: TLabel;
    lbLbPorcentagemInstalado: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    lb2: TLabel;
    EdtPorcentagemRetirado: TEdit;
    EdtPorcentagemInstalado: TEdit;
    EdtPorcentagemFornecido: TEdit;
    lbNomePlanoDeContas: TLabel;
    pnl1: TPanel;
    imgrodape: TImage;
    lb14: TLabel;
    btPrimeiro: TSpeedButton;
    btAnterior: TSpeedButton;
    btProximo: TSpeedButton;
    btUltimo: TSpeedButton;
//DECLARA COMPONENTES

    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdtPlanodeContasExit(Sender: TObject);
    procedure EdtPlanodeContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure EdtPlanodeContasDblClick(Sender: TObject);
    procedure EdtPorcentagemInstaladoKeyPress(Sender: TObject;
      var Key: Char);
    procedure EdtPorcentagemFornecidoKeyPress(Sender: TObject;
      var Key: Char);
    procedure EdtPorcentagemRetiradoKeyPress(Sender: TObject;
      var Key: Char);
    procedure EdtPlanodeContasKeyPress(Sender: TObject; var Key: Char);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    procedure MostraQuantidadeCadastrada;
  public
    { Public declarations }
  end;

var
  FGRUPOVIDRO: TFGRUPOVIDRO;
  ObjVIDRO:TObjVIDRO;

implementation

uses Upesquisa, UessencialLocal, UMenuRelatorios, UobjGRUPOVIDRO,
  UDataModulo, UescolheImagemBotao;

{$R *.dfm}



procedure TFGRUPOVIDRO.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     ColocaUpperCaseEdit(Self);

     Try
        ObjVIDRO:=TObjVIDRO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     habilita_botoes(Self);
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE GRUPO DE VIDRO')=False)
     Then desab_botoes(Self);
    FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
    FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
    lbCodigoGrupoVidro.caption:='';
    MostraQuantidadeCadastrada;
    if(Tag<>0)
    then begin
         if(ObjVIDRO.GrupoVidro.LocalizaCodigo(IntToStr(Tag))=True)then
         begin
              ObjVIDRO.GrupoVidro.TabelaparaObjeto;
              self.ObjetoParaControles;
         end;

    end;

end;

procedure TFGRUPOVIDRO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjVIDRO.GrupoVidro=Nil)
     Then exit;

     If (ObjVIDRO.GrupoVidro.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjVIDRO.free;

end;

procedure TFGRUPOVIDRO.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbCodigoGrupoVidro.Caption:='0';
     //edtcodigo.text:=ObjVidro.GrupoVidro.Get_novocodigo;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjVidro.GrupoVidro.status:=dsInsert;
     EdtNome.setfocus;

     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

end;

procedure TFGRUPOVIDRO.btSalvarClick(Sender: TObject);
begin

     If ObjVidro.GrupoVidro.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjVidro.GrupoVidro.salvar(true)=False)
     Then exit;

     lbCodigoGrupoVidro.Caption:=ObjVidro.GrupoVidro.Get_codigo;
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFGRUPOVIDRO.btAlterarClick(Sender: TObject);
begin
    If (ObjVidro.GrupoVidro.Status=dsinactive) and (lbCodigoGrupoVidro.Caption<>'')
    Then Begin
                habilita_campos(Self);
                ObjVidro.GrupoVidro.Status:=dsEdit;
                edtNome.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btnovo.Visible :=false;
               btalterar.Visible:=false;
               btpesquisar.Visible:=false;
               btrelatorio.Visible:=false;
               btexcluir.Visible:=false;
               btsair.Visible:=false;
               btopcoes.Visible :=false;
          End;

end;

procedure TFGRUPOVIDRO.btCancelarClick(Sender: TObject);
begin
     ObjVidro.GrupoVidro.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     lbCodigoGrupoVidro.Caption:='';
    


end;

procedure TFGRUPOVIDRO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjVidro.GrupoVidro.Get_pesquisa,ObjVidro.GrupoVidro.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjVidro.GrupoVidro.status<>dsinactive
                                  then exit;

                                  If (ObjVidro.GrupoVidro.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjVidro.GrupoVidro.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFGRUPOVIDRO.btExcluirClick(Sender: TObject);
begin
     If (ObjVidro.GrupoVidro.status<>dsinactive) or (lbCodigoGrupoVidro.Caption='')
     Then exit;

     If (ObjVidro.GrupoVidro.LocalizaCodigo(lbCodigoGrupoVidro.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjVidro.GrupoVidro.exclui(lbCodigoGrupoVidro.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
     lbCodigoGrupoVidro.Caption:='';

end;

procedure TFGRUPOVIDRO.btRelatorioClick(Sender: TObject);
begin
//    ObjVidro.GrupoVidro.Imprime;
end;

procedure TFGRUPOVIDRO.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFGRUPOVIDRO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFGRUPOVIDRO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFGRUPOVIDRO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;

end;

procedure TFGRUPOVIDRO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFGRUPOVIDRO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjVidro.GrupoVidro do
    Begin
        Submit_Codigo(lbCodigoGrupoVidro.Caption);
        Submit_Nome(edtNome.text);
        Submit_PorcentagemInstalado(EdtPorcentagemInstalado.Text);
        Submit_PorcentagemFornecido(EdtPorcentagemFornecido.Text);
        Submit_PorcentagemRetirado(EdtPorcentagemRetirado.Text);
        PlanodeContas.Submit_CODIGO(EdtPlanodeContas.Text);
        result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFGRUPOVIDRO.LimpaLabels;
begin
     lbNomePlanoDeContas.Caption:='';
end;

function TFGRUPOVIDRO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjVidro.GrupoVidro do
     Begin
        lbCodigoGrupoVidro.Caption:=Get_Codigo;
        EdtNome.text:=Get_Nome;
        EdtPorcentagemInstalado.Text:=Get_PorcentagemInstalado;
        EdtPorcentagemFornecido.Text:=Get_PorcentagemFornecido;
        EdtPorcentagemRetirado.Text:=Get_PorcentagemRetirado;
        EdtPlanodeContas.Text:=PlanoDeContas.Get_CODIGO;
        lbNomePlanoDeContas.Caption:='TIPO = '+ObjVidro.GrupoVidro.PlanoDeContas.Get_Tipo+' | ';
        lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' CLASSIF. = '+ObjVidro.GrupoVidro.PlanoDeContas.Get_Classificacao+' | ';
        lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' NOME = '+ObjVidro.GrupoVidro.PlanoDeContas.Get_Nome;

        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFGRUPOVIDRO.TabelaParaControles: Boolean;
begin
     If (ObjVidro.GrupoVidro.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;


procedure TFGRUPOVIDRO.SpeedButton1Click(Sender: TObject);
begin
     With FmenuRelatorios do
     Begin
          //era no titulo antes
          NomeObjeto:='UObjGrupoVidro';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Atualiza Margens');
                Items.Add('Atualiza Pre�os');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0 :  ObjVidro.AtualizaMargens(lbCodigoGrupoVidro.Caption);
                1 :  ObjVIDRO.AtualizaPrecos(lbCodigoGrupoVidro.Caption);

          End;
     end;

end;

procedure TFGRUPOVIDRO.EdtPlanodeContasExit(Sender: TObject);
begin
    ObjVidro.GrupoVidro.EdtGrupoPlanoDeContasExit(Sender, lbNomePlanoDeContas);
    lbNomePlanoDeContas.Caption:='TIPO = '+ObjVidro.GrupoVidro.PlanoDeContas.Get_Tipo+' | ';
    lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' CLASSIF. = '+ObjVidro.GrupoVidro.PlanoDeContas.Get_Classificacao+' | ';
    lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' NOME = '+ObjVidro.GrupoVidro.PlanoDeContas.Get_Nome;

end;

procedure TFGRUPOVIDRO.EdtPlanodeContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjVIDRO.GrupoVidro.EdtGrupoPlanoDeContasKeyDown(Sender, Key, Shift, lbNomePlanoDeContas);
end;

procedure TFGRUPOVIDRO.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjVidro.GrupoVidro.PrimeiroRegistro = false)then
    exit;

    ObjVidro.GrupoVidro.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOVIDRO.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigoGrupoVidro.Caption='')then
    lbCodigoGrupoVidro.Caption:='0';

    if  (ObjVidro.GrupoVidro.RegistoAnterior(StrToInt(lbCodigoGrupoVidro.Caption)) = false)then
    exit;

    ObjVidro.GrupoVidro.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOVIDRO.btProximoClick(Sender: TObject);
begin
    if (lbCodigoGrupoVidro.Caption='')then
    lbCodigoGrupoVidro.Caption:='0';

    if  (ObjVidro.GrupoVidro.ProximoRegisto(StrToInt(lbCodigoGrupoVidro.Caption)) = false)then
    exit;

    ObjVidro.GrupoVidro.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOVIDRO.btUltimoClick(Sender: TObject);
begin
    if  (ObjVidro.GrupoVidro.UltimoRegistro = false)then
    exit;

    ObjVidro.GrupoVidro.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOVIDRO.MostraQuantidadeCadastrada;
var
  Query:TIBQuery;
  contador:Integer;
begin
   contador:=0;
   try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
   except

   end;

   try
         with Query do
         begin
              Close;
              SQL.Clear;
              SQL.Add('select codigo from tabgrupovidro');
              Open;
              while not Eof
              do begin
                Inc(contador,1);
                Next;
              end;
              if(contador>1) then
                    lb14.Caption:='Existem '+IntToStr(contador)+' grupos cadastrados'
              else
              begin
                    lb14.Caption:='Existe '+IntToStr(contador)+' grupo cadastrado';
              end;
         end;
   finally
         FreeAndNil(FGRUPOVIDRO);
   end;


end;

procedure TFGRUPOVIDRO.EdtPlanodeContasDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FplanoContas:TFplanodeContas;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FplanoContas:=TFplanodeContas.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabplanodecontas','',FplanoContas)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtPlanodeContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbNomePlanodeContas.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FplanoContas);

     End;
end;

procedure TFGRUPOVIDRO.EdtPorcentagemInstaladoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFGRUPOVIDRO.EdtPorcentagemFornecidoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFGRUPOVIDRO.EdtPorcentagemRetiradoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFGRUPOVIDRO.EdtPlanodeContasKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

end.
