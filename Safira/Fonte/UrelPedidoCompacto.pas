unit UrelPedidoCompacto;

interface

uses dialogs,Windows, SysUtils, Messages, Classes, Graphics, Controls,
  ExtCtrls, Forms, QuickRpt, QRCtrls;

type
  TQRpedidoCompacto = class(TQuickRep)
    SB: TQRStringsBand;
    QRBand4: TQRBand;
    lbnomesuperior: TQRLabel;
    LbNumPag: TQRSysData;
    BandaDetail: TQRBand;
    QrImagemProjeto: TQRImage;
    QRSysData1: TQRSysData;
    QRShape3: TQRShape;
    LBDADOS: TQRLabel;
    BANDATITULO: TQRBand;
    LbNome: TQRLabel;
    LBEndereco: TQRLabel;
    LbContato: TQRLabel;
    LbObra: TQRLabel;
    QrlinhaSuperiorProposta: TQRShape;
    QrLbProposta: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel18: TQRLabel;
    LBTelefone: TQRLabel;
    lbCNPJCPF: TQRLabel;
    lbRGIE: TQRLabel;
    QRLabel21: TQRLabel;
    QRImageLogotipo: TQRImage;
    LBCabecalhoLinha1: TQRLabel;
    LBCabecalhoLinha2: TQRLabel;
    LBCabecalhoLinha3: TQRLabel;
    LBCabecalhoLinha4: TQRLabel;
    LBProposta: TQRLabel;
    lbTitulo: TQRLabel;
    LbReferenciaProjeto: TQRLabel;
    lbDescricao: TQRLabel;
    LbAltura: TQRLabel;
    lbLargura: TQRLabel;
    LbLocal: TQRLabel;
    BandSumario: TQRBand;
    LbValorTotal: TQRLabel;
    LBValorAcrescimo: TQRLabel;
    LBValorDesconto: TQRLabel;
    LBValorFinal: TQRLabel;
    lbValorTotalRotulo: TQRLabel;
    lbValorAcrescimoRotulo: TQRLabel;
    lbValorDescontoRotulo: TQRLabel;
    lbValorFinalRotulo: TQRLabel;
    ShapeTracoVendedor: TQRShape;
    lbNomeVendedorAssinatura: TQRLabel;
    lbCondicoesPagamento: TQRLabel;
    QrMemoPendencias: TQRMemo;
    QrMemoObservacao: TQRMemo;
    QRShape2: TQRShape;
    QRShape4: TQRShape;
    Label20: TQRLabel;
    LbVendedor: TQRLabel;
    LbValorBonificacao: TQRLabel;
    QRLabel2: TQRLabel;
    QRShape5: TQRShape;
    LbNomeClienteAssinatura: TQRLabel;
    QrDbLabelCEP: TQRLabel;
    QrLabelCep: TQRLabel;
    QrlBEmail1: TQRLabel;
    lBEmail2: TQRLabel;
    lB1: TQRLabel;
    lB2: TQRLabel;
    lBMedidalocal: TQRLabel;
    lBmedidalocal1: TQRLabel;
    lB3: TQRLabel;
    lBdata: TQRLabel;
    lB6: TQRLabel;
    lBFoneVendedor: TQRLabel;
    lBDataEntrega: TQRLabel;
    lBDataE: TQRLabel;
    lb4: TQRLabel;
    lbDataOrcamento: TQRLabel;
    procedure LBDADOSPrint(sender: TObject; var Value: String);
    procedure BANDATITULOBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private

  public

    destructor Destroy;override;

  end;

var
  QRpedidoCompacto: TQRpedidoCompacto;

implementation

{$R *.DFM}


procedure TQRpedidoCompacto.LBDADOSPrint(sender: TObject; var Value: String);
var
apoio:string;
posicao:integer;
begin
  lbdados.font.Color:=clBLACK;
  LBDADOS.Font.Style:=[];
  LBDADOS.Font.size:=9;

  if (sb.Item='')
  Then Begin
    Value:=sb.item;
    exit;
  End;

  case sb.item[1] of
     '?':Begin//negrito
              LBDADOS.Font.Style:=[fsbold];
              value:=copy(sb.Item,2,length(sb.item));
         end;
     '�':Begin//cor

              apoio:=copy(sb.Item,2,length(sb.item));
              posicao:=pos('�',apoio);
              If (posicao=0)//tem que ter o fecho e entre eles a cor
              Then value:=sb.item
              Else Begin//s� 3 cores, vermelho,verde e azul
                        apoio:=copy(apoio,1,posicao-1);
                        if UPPERCASE(apoio)='VERMELHO'
                        tHEN lbdados.font.Color:=CLRED
                        eLSE
                                IF UPPERCASE(APOIO)='VERDE'
                                THEN lbdados.font.Color:=clGreen
                                ELSE
                                        IF UPPERCASE(APOIO)='AZUL'
                                        THEN lbdados.font.Color:=clBlue
                                        ELSE lbdados.font.Color:=clBLACK;


                        //pegando os dados fora a cor
                        value:=copy(sb.Item,posicao+2,length(sb.item));
              End;
         End;
  Else
    Begin
      value:=Sb.item;
    End;
  end;
end;

procedure TQRpedidoCompacto.BANDATITULOBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
BANDATITULO.Height:=235+(QrMemoPendencias.Lines.count*12);
QrlinhaSuperiorProposta.Top:=205+(QrMemoPendencias.Lines.count*19);
QrLbProposta.Top:=215+(QrMemoPendencias.Lines.count*19);
end;

destructor TQRpedidoCompacto.Destroy;
begin
  inherited;
end;

end.
