unit UEscolheMateriaisReplicar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, StdCtrls;

type
  TFEscolheMateriaisReplicar = class(TForm)
    Panel: TPanel;
    GRID: TStringGrid;
    pnl1: TPanel;
    Image1: TImage;
    lb2: TLabel;
    pnl6: TPanel;
    lbselecionartodos: TLabel;
    lb1: TLabel;
    lb3: TLabel;
    procedure GRIDDblClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure GRIDKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure lbselecionartodosMouseLeave(Sender: TObject);
    procedure lbselecionartodosMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEscolheMateriaisReplicar: TFEscolheMateriaisReplicar;

implementation

uses UescolheImagemBotao;

{$R *.dfm}

procedure TFEscolheMateriaisReplicar.GRIDDblClick(Sender: TObject);
begin
     if (Self.grid.row=0)
     then exit;
     if (Self.grid.cells[0,Self.grid.row]='X')
     then Self.grid.cells[0,Self.grid.row]:=''
     Else Self.grid.cells[0,Self.grid.row]:='X';
end;

procedure TFEscolheMateriaisReplicar.Button1Click(Sender: TObject);
var
cont:integer;
begin
     for cont:=1 to Self.GRID.RowCount-1 do
     Begin
          Self.grid.cells[0,cont]:='X';
     End;
end;

procedure TFEscolheMateriaisReplicar.Button2Click(Sender: TObject);
var
cont:integer;
begin
     for cont:=1 to Self.GRID.RowCount-1 do
     Begin
          Self.grid.cells[0,cont]:='';
     End;
end;

procedure TFEscolheMateriaisReplicar.Button3Click(Sender: TObject);
var
cont:integer;
begin
     for cont:=1 to Self.GRID.Row do
     Begin
          Self.grid.cells[0,cont]:='X';
     End;
end;

procedure TFEscolheMateriaisReplicar.GRIDKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then Self.GRIDDblClick(sender);
end;

procedure TFEscolheMateriaisReplicar.FormShow(Sender: TObject);
begin
       FescolheImagemBotao.PegaFiguraImagem(Image1,'RODAPE');
end;

procedure TFEscolheMateriaisReplicar.lbselecionartodosMouseLeave(
  Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFEscolheMateriaisReplicar.lbselecionartodosMouseMove(
  Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFEscolheMateriaisReplicar.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      if(Key=VK_F5)
      then lbselecionartodos.OnClick(Sender);
      if(Key=VK_F6)
      then lb1.OnClick(Sender);
      if(Key=VK_F7)
      then lb3.OnClick(Sender);
end;

end.
