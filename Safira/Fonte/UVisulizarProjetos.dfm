object FVisulizarProjetos: TFVisulizarProjetos
  Left = 682
  Top = 232
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'FVisulizarProjetos'
  ClientHeight = 551
  ClientWidth = 868
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelFundo: TPanel
    Left = 0
    Top = 61
    Width = 689
    Height = 490
    Align = alClient
    Color = clWhite
    PopupMenu = PopUpMenu
    TabOrder = 0
    object img2: TImage
      Left = 1
      Top = 1
      Width = 687
      Height = 488
      Align = alClient
      Visible = False
    end
    object PaintBoxTela: TPaintBox
      Left = 1
      Top = 1
      Width = 687
      Height = 488
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      PopupMenu = PopUpMenu
    end
  end
  object panelpnl2: TPanel
    Left = 689
    Top = 61
    Width = 179
    Height = 490
    Align = alRight
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object lb36: TLabel
      Left = 51
      Top = 9
      Width = 68
      Height = 23
      Caption = 'Objetos'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6073854
      Font.Height = -16
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentFont = False
      PopupMenu = PopUpMenu
    end
    object ListBox1: TListBox
      Left = 1
      Top = 39
      Width = 177
      Height = 450
      Align = alBottom
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ItemHeight = 14
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
    end
  end
  object panel2: TPanel
    Left = 0
    Top = 0
    Width = 868
    Height = 61
    Align = alTop
    BevelOuter = bvNone
    Color = clWhite
    TabOrder = 2
    object lb1: TLabel
      Left = 274
      Top = 16
      Width = 341
      Height = 24
      Caption = 'AGUARDE, renderizando imagens'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object PopUpMenu: TPopupMenu
    Left = 565
    Top = 81
    object GerarDesenho1: TMenuItem
      Caption = 'Gerar Desenho'
      OnClick = GerarDesenho1Click
    end
    object MostrarReferncias1: TMenuItem
      Caption = 'Mostrar Refer'#234'ncias'
      OnClick = MostrarReferncias1Click
    end
    object NoMostrarReferncias1: TMenuItem
      Caption = 'N'#227'o Mostrar Refer'#234'ncias'
      OnClick = NoMostrarReferncias1Click
    end
    object VisualizarMedidas1: TMenuItem
      Caption = 'Visualizar Medidas'
      OnClick = VisualizarMedidas1Click
    end
    object NoMostrarMostrarObjetosDesenho1: TMenuItem
      Caption = 'N'#227'o Mostrar/Mostrar Objetos (Desenho)'
      OnClick = NoMostrarMostrarObjetosDesenho1Click
    end
  end
  object tmr1: TTimer
    Interval = 2000
    OnTimer = tmr1Timer
    Left = 768
    Top = 24
  end
end
