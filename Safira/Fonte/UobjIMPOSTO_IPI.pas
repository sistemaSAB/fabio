unit UobjIMPOSTO_IPI;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UOBJSITUACAOTRIBUTARIA_IPI
;
//USES_INTERFACE



Type
   TObjIMPOSTO_IPI=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String;
                ST:TOBJSITUACAOTRIBUTARIA_IPI;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :String;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:String;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_ClasseEnq(parametro: String);
                Function Get_ClasseEnq: String;
                Procedure Submit_CodigoEnquadramento(parametro: String);
                Function Get_CodigoEnquadramento: String;
                Procedure Submit_CnpjProdutor(parametro: String);
                Function Get_CnpjProdutor: String;
                Procedure Submit_CodigoSelo(parametro: String);
                Function Get_CodigoSelo: String;
                Procedure Submit_TipoCalculo(parametro: String);
                Function Get_TipoCalculo: String;
                Procedure Submit_Aliquota(parametro: String);
                Function Get_Aliquota: String;
                Procedure Submit_QuantidadeTotalUP(parametro: String);
                Function Get_QuantidadeTotalUP: String;
                Procedure Submit_ValorUnidade(parametro: string);
                Function Get_ValorUnidade: string;
                Procedure Submit_FORMULABASECALCULO(parametro: String);
                Function Get_FORMULABASECALCULO: String;

                Procedure Submit_formula_valor_imposto(parametro: String);
                Function Get_formula_valor_imposto: String;
                procedure EdtSTExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtSTKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               ClasseEnq:String;
               CodigoEnquadramento:String;
               CnpjProdutor:String;
               CodigoSelo:String;
               TipoCalculo:String;
               Aliquota:String;
               QuantidadeTotalUP:String;
               ValorUnidade:string;
               FORMULABASECALCULO:String;
               formula_valor_imposto:String;
//CODIFICA VARIAVEIS PRIVADAS












               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, USITUACAOTRIBUTARIA_IPI;





Function  TObjIMPOSTO_IPI.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('ST').asstring<>'')
        Then Begin
                 If (Self.ST.LocalizaCodigo(FieldByName('ST').asstring)=False)
                 Then Begin
                          Messagedlg('Situa��o Tribut�ria N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.ST.TabelaparaObjeto;
        End;
        Self.ClasseEnq:=fieldbyname('ClasseEnq').asstring;
        Self.CodigoEnquadramento:=fieldbyname('CodigoEnquadramento').asstring;
        Self.CnpjProdutor:=fieldbyname('CnpjProdutor').asstring;
        Self.CodigoSelo:=fieldbyname('CodigoSelo').asstring;
        Self.TipoCalculo:=fieldbyname('TipoCalculo').asstring;
        Self.Aliquota:=fieldbyname('Aliquota').asstring;
        Self.QuantidadeTotalUP:=fieldbyname('QuantidadeTotalUP').asstring;
        Self.ValorUnidade:=fieldbyname('ValorUnidade').asstring;
        Self.FORMULABASECALCULO:=fieldbyname('FORMULABASECALCULO').asstring;
        Self.formula_valor_imposto:=fieldbyname('formula_valor_imposto').asstring;
//CODIFICA TABELAPARAOBJETO











        result:=True;
     End;
end;


Procedure TObjIMPOSTO_IPI.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('ST').asstring:=Self.ST.GET_CODIGO;
        ParamByName('ClasseEnq').asstring:=Self.ClasseEnq;
        ParamByName('CodigoEnquadramento').asstring:=Self.CodigoEnquadramento;
        ParamByName('CnpjProdutor').asstring:=Self.CnpjProdutor;
        ParamByName('CodigoSelo').asstring:=Self.CodigoSelo;
        ParamByName('TipoCalculo').asstring:=Self.TipoCalculo;
        ParamByName('Aliquota').asstring:=virgulaparaponto(Self.Aliquota);
        ParamByName('QuantidadeTotalUP').asstring:=virgulaparaponto(Self.QuantidadeTotalUP);
        ParamByName('ValorUnidade').asstring:=virgulaparaponto(Self.ValorUnidade);
        ParamByName('FORMULABASECALCULO').asstring:=Self.FORMULABASECALCULO;
        ParamByName('formula_valor_imposto').asstring:=Self.formula_valor_imposto;
//CODIFICA OBJETOPARATABELA











  End;
End;

//***********************************************************************

function TObjIMPOSTO_IPI.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjIMPOSTO_IPI.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        ST.ZerarTabela;
        ClasseEnq:='';
        CodigoEnquadramento:='';
        CnpjProdutor:='';
        CodigoSelo:='';
        TipoCalculo:='';
        Aliquota:='';
        QuantidadeTotalUP:='';
        ValorUnidade:='';
        FORMULABASECALCULO:='';
        formula_valor_imposto:='';
//CODIFICA ZERARTABELA











     End;
end;

Function TObjIMPOSTO_IPI.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (ST.get_Codigo='')
      Then Mensagem:=mensagem+'/Situa��o Tribut�ria';
      If (TipoCalculo='')
      Then Mensagem:=mensagem+'/Tipo de C�lculo';

      If (FORMULABASECALCULO='')
      Then FORMULABASECALCULO:='0';

      If (formula_valor_imposto='')
      Then formula_valor_imposto:='0';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjIMPOSTO_IPI.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.ST.LocalizaCodigo(Self.ST.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Situa��o Tribut�ria n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjIMPOSTO_IPI.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.ST.Get_Codigo<>'')
        Then Strtoint(Self.ST.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Situa��o Tribut�ria';
     End;
     try
        Strtofloat(Self.Aliquota);
     Except
           Mensagem:=mensagem+'/Al�quota';
     End;
     try
        Strtofloat(Self.QuantidadeTotalUP);
     Except
           Mensagem:=mensagem+'/Quantidade  de unidade padr�o para tributa��o';
     End;
     try
        Strtofloat(Self.ValorUnidade);
     Except
           Mensagem:=mensagem+'/Valor por Unidade';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjIMPOSTO_IPI.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjIMPOSTO_IPI.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjIMPOSTO_IPI.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro IMPOSTO_IPI vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,ST,ClasseEnq,CodigoEnquadramento,CnpjProdutor,CodigoSelo');
           SQL.ADD(' ,TipoCalculo,Aliquota,QuantidadeTotalUP,ValorUnidade,FORMULABASECALCULO,formula_valor_imposto');
           SQL.ADD(' ');
           SQL.ADD(' from  TABImposto_IPI');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjIMPOSTO_IPI.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjIMPOSTO_IPI.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjIMPOSTO_IPI.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.ST:=TOBJSITUACAOTRIBUTARIA_IPI.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABImposto_IPI(CODIGO,ST,ClasseEnq,CodigoEnquadramento');
                InsertSQL.add(' ,CnpjProdutor,CodigoSelo,TipoCalculo,Aliquota,QuantidadeTotalUP');
                InsertSQL.add(' ,ValorUnidade,FORMULABASECALCULO,formula_valor_imposto)');
                InsertSQL.add('values (:CODIGO,:ST,:ClasseEnq,:CodigoEnquadramento');
                InsertSQL.add(' ,:CnpjProdutor,:CodigoSelo,:TipoCalculo,:Aliquota,:QuantidadeTotalUP');
                InsertSQL.add(' ,:ValorUnidade,:FORMULABASECALCULO,:formula_valor_imposto)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABImposto_IPI set CODIGO=:CODIGO,ST=:ST,ClasseEnq=:ClasseEnq');
                ModifySQL.add(',CodigoEnquadramento=:CodigoEnquadramento,CnpjProdutor=:CnpjProdutor');
                ModifySQL.add(',CodigoSelo=:CodigoSelo,TipoCalculo=:TipoCalculo,Aliquota=:Aliquota');
                ModifySQL.add(',QuantidadeTotalUP=:QuantidadeTotalUP,ValorUnidade=:ValorUnidade');
                ModifySQL.add(',FORMULABASECALCULO=:FORMULABASECALCULO,formula_valor_imposto=:formula_valor_imposto');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABImposto_IPI where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjIMPOSTO_IPI.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjIMPOSTO_IPI.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabIMPOSTO_IPI');
     Result:=Self.ParametroPesquisa;
end;

function TObjIMPOSTO_IPI.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de IMPOSTO_IPI ';
end;


function TObjIMPOSTO_IPI.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENIMPOSTO_IPI,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENIMPOSTO_IPI,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjIMPOSTO_IPI.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.ST.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjIMPOSTO_IPI.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjIMPOSTO_IPI.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjImposto_IPI.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjImposto_IPI.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjImposto_IPI.Submit_ClasseEnq(parametro: String);
begin
        Self.ClasseEnq:=Parametro;
end;
function TObjImposto_IPI.Get_ClasseEnq: String;
begin
        Result:=Self.ClasseEnq;
end;
procedure TObjImposto_IPI.Submit_CodigoEnquadramento(parametro: String);
begin
        Self.CodigoEnquadramento:=Parametro;
end;
function TObjImposto_IPI.Get_CodigoEnquadramento: String;
begin
        Result:=Self.CodigoEnquadramento;
end;
procedure TObjImposto_IPI.Submit_CnpjProdutor(parametro: String);
begin
        Self.CnpjProdutor:=Parametro;
end;
function TObjImposto_IPI.Get_CnpjProdutor: String;
begin
        Result:=Self.CnpjProdutor;
end;
procedure TObjImposto_IPI.Submit_CodigoSelo(parametro: String);
begin
        Self.CodigoSelo:=Parametro;
end;
function TObjImposto_IPI.Get_CodigoSelo: String;
begin
        Result:=Self.CodigoSelo;
end;
procedure TObjImposto_IPI.Submit_TipoCalculo(parametro: String);
begin
        Self.TipoCalculo:=Parametro;
end;
function TObjImposto_IPI.Get_TipoCalculo: String;
begin
        Result:=Self.TipoCalculo;
end;
procedure TObjImposto_IPI.Submit_Aliquota(parametro: String);
begin
        Self.Aliquota:=Parametro;
end;
function TObjImposto_IPI.Get_Aliquota: String;
begin
        Result:=Self.Aliquota;
end;
procedure TObjImposto_IPI.Submit_QuantidadeTotalUP(parametro: String);
begin
        Self.QuantidadeTotalUP:=Parametro;
end;
function TObjImposto_IPI.Get_QuantidadeTotalUP: String;
begin
        Result:=Self.QuantidadeTotalUP;
end;
procedure TObjImposto_IPI.Submit_ValorUnidade(parametro: string);
begin
        Self.ValorUnidade:=Parametro;
end;
function TObjImposto_IPI.Get_ValorUnidade: string;
begin
        Result:=Self.ValorUnidade;
end;
procedure TObjImposto_IPI.Submit_FORMULABASECALCULO(parametro: String);
begin
        Self.FORMULABASECALCULO:=Parametro;
end;
function TObjImposto_IPI.Get_FORMULABASECALCULO: String;
begin
        Result:=Self.FORMULABASECALCULO;
end;


procedure TObjImposto_IPI.Submit_formula_valor_imposto(parametro: String);
begin
        Self.formula_valor_imposto:=Parametro;
end;
function TObjImposto_IPI.Get_formula_valor_imposto: String;
begin
        Result:=Self.formula_valor_imposto;
end;

//CODIFICA GETSESUBMITS


procedure TObjIMPOSTO_IPI.EdtSTExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.ST.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.ST.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.ST.GET_NOME;
End;
procedure TObjIMPOSTO_IPI.EdtSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FST:TFSITUACAOTRIBUTARIA_IPI;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FST:=TFSITUACAOTRIBUTARIA_IPI.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.ST.Get_Pesquisa,Self.ST.Get_TituloPesquisa,FST)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ST.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.ST.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ST.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FST);
     End;
end;
//CODIFICA EXITONKEYDOWN

procedure TObjIMPOSTO_IPI.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJIMPOSTO_IPI';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



end.



