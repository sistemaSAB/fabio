unit USITUACAOTRIBUTARIA_COFINS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjSITUACAOTRIBUTARIA_COFINS,
  jpeg;

type
  TFSITUACAOTRIBUTARIA_COFINS = class(TForm)
    edtNOME: TEdit;
    lbLbNOME: TLabel;
    edtCODIGO: TEdit;
    lbLbCODIGO: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    btNovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btCancelar: TBitBtn;
    btexcluir: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btOpcoes: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    imgrodape: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjSITUACAOTRIBUTARIA_COFINS:TObjSITUACAOTRIBUTARIA_COFINS;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSITUACAOTRIBUTARIA_COFINS: TFSITUACAOTRIBUTARIA_COFINS;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFSITUACAOTRIBUTARIA_COFINS.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjSITUACAOTRIBUTARIA_COFINS do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_NOME(edtNOME.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFSITUACAOTRIBUTARIA_COFINS.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjSITUACAOTRIBUTARIA_COFINS do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNOME.text:=Get_NOME;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFSITUACAOTRIBUTARIA_COFINS.TabelaParaControles: Boolean;
begin
     If (Self.ObjSITUACAOTRIBUTARIA_COFINS.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFSITUACAOTRIBUTARIA_COFINS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjSITUACAOTRIBUTARIA_COFINS=Nil)
     Then exit;

If (Self.ObjSITUACAOTRIBUTARIA_COFINS.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjSITUACAOTRIBUTARIA_COFINS.free;
end;

procedure TFSITUACAOTRIBUTARIA_COFINS.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFSITUACAOTRIBUTARIA_COFINS.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     
     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjSITUACAOTRIBUTARIA_COFINS.Get_novocodigo;
     //edtcodigo.enabled:=False;

     
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorios.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

     Self.ObjSITUACAOTRIBUTARIA_COFINS.status:=dsInsert;
     edtcodigo.setfocus;

end;


procedure TFSITUACAOTRIBUTARIA_COFINS.btalterarClick(Sender: TObject);
begin
    If (Self.ObjSITUACAOTRIBUTARIA_COFINS.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjSITUACAOTRIBUTARIA_COFINS.Status:=dsEdit;
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtNOME.setfocus;
                btnovo.Visible :=false;
                 btalterar.Visible:=false;
                 btpesquisar.Visible:=false;
                 btrelatorios.Visible:=false;
                 btexcluir.Visible:=false;
                 btsair.Visible:=false;
                 btopcoes.Visible :=false;
                
    End;


end;

procedure TFSITUACAOTRIBUTARIA_COFINS.btgravarClick(Sender: TObject);
begin

     If Self.ObjSITUACAOTRIBUTARIA_COFINS.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjSITUACAOTRIBUTARIA_COFINS.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjSITUACAOTRIBUTARIA_COFINS.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorios.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;

end;

procedure TFSITUACAOTRIBUTARIA_COFINS.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjSITUACAOTRIBUTARIA_COFINS.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjSITUACAOTRIBUTARIA_COFINS.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjSITUACAOTRIBUTARIA_COFINS.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFSITUACAOTRIBUTARIA_COFINS.btcancelarClick(Sender: TObject);
begin
     Self.ObjSITUACAOTRIBUTARIA_COFINS.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorios.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;

end;

procedure TFSITUACAOTRIBUTARIA_COFINS.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFSITUACAOTRIBUTARIA_COFINS.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjSITUACAOTRIBUTARIA_COFINS.Get_pesquisa,Self.ObjSITUACAOTRIBUTARIA_COFINS.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjSITUACAOTRIBUTARIA_COFINS.status<>dsinactive
                                  then exit;

                                  If (Self.ObjSITUACAOTRIBUTARIA_COFINS.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjSITUACAOTRIBUTARIA_COFINS.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFSITUACAOTRIBUTARIA_COFINS.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFSITUACAOTRIBUTARIA_COFINS.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     
     Try
        Self.ObjSITUACAOTRIBUTARIA_COFINS:=TObjSITUACAOTRIBUTARIA_COFINS.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.

