unit UimportaPedido;

interface

uses
  db,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,UObjPedidoObjetos;

type
  TFimportaPedido = class(TForm)
    OpenDialog1: TOpenDialog;
    MemoDados: TMemo;
    Panel1: TPanel;
    Label1: TLabel;
    lbcaminho: TLabel;
    btcaminhoarquivo: TButton;
    BtInicia: TBitBtn;
    edterro: TEdit;
    procedure btcaminhoarquivoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtIniciaClick(Sender: TObject);
  private
    { Private declarations }
         ObjPedidoObjetos:TObjPedidoObjetos;
  public
    { Public declarations }
  end;

var
  FimportaPedido: TFimportaPedido;

implementation

uses UessencialGlobal, UDataModulo, UobjPEDIDO_PEDIDO,
  UMostraBarraProgresso, UobjPEDIDO_PROJ_PEDIDO;

{$R *.dfm}

procedure TFimportaPedido.btcaminhoarquivoClick(Sender: TObject);
begin
     if (OpenDialog1.execute=False)
     Then exit;

     lbcaminho.caption:=OpenDialog1.filename;
     MemoDados.lines.LoadFromFile(lbcaminho.caption);
end;

procedure TFimportaPedido.FormCreate(Sender: TObject);
begin
     lbcaminho.caption:='';
     memodados.text:='';
     desab_botoes(Self);

     Try
        Self.ObjPedidoObjetos:=TObjPedidoObjetos.Create(self);

        if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSAR M�DULO DE IMPORTA��O DE PEDIDOS')=False)
        Then exit;

        habilita_botoes(Self);
     Except
           mensagemerro('Erro na tentativa de criar o objeto de pedidos');
           exit;
     End;

end;

procedure TFimportaPedido.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    If (Self.ObjPedidoObjetos<>Nil)
    Then Self.ObjPedidoObjetos.free;


    Action := caFree;
    Self := nil;

end;

procedure TFimportaPedido.BtIniciaClick(Sender: TObject);
var
cont:integer;
PStr:TStringList;
pCodigovidrocoratual,PpedidoprojetoAtual,ppedidoatual,PVENDEDORPADRAO:string;
temp:string;
pvalorvidro,pquantidade:currency;
begin
{
------- Lay-out do arquivo de exporta��o Vers�o 2 ------------------------------------------------------------

C STATUS;CODPEDIDO;CODCLI;NOMECLI;NUMCLI;DATAPEDIDO;PREVISAOENTREGA;CODMAT;DESCMAT;
O OBSPEDIDO;
I ITEM;CODPECA;LARGURA;ALTURA,AREA;AMOLADURA;TIPO;OBSPECA;CODACABAMENTO;QTDEACABAMENTO;CODCANTO;QTDECANTO;CODFORMA;CODSERVI�O;
I ITEM;CODPECA;LARGURA;ALTURA,AREA;AMOLADURA;TIPO;OBSPECA;
I ITEM;CODPECA;LARGURA;ALTURA,AREA;AMOLADURA;TIPO;OBSPECA;
I ITEM;CODPECA;LARGURA;ALTURA,AREA;AMOLADURA;TIPO;OBSPECA;
F AREAPEDIDO;
Q CODPE�AQ;LARGURAQ;ALTURAQ;AREAQ;AMOLADURAQ;TIPOQ;REPOSICAO;
K ITEMKIT;CODKIT;QTDEKIT;OBSKIT;STATUS;
  
  (A linha iniciada com Q refere-se aos dados das pe�as quebradas, esta linha somente existir� se existirem pe�as quebradas para este pedido)
  (A linha iniciada com K refere-se aos dados dos kits deste pedido, esta linha somente existir� se existirem kits neste pedidos pedido)

--------------------------------------------------------------------------------------------------------------

Tipos de dados
STATUS: INT 1
CODPEDIDO: CHAR 6
CODCLI : CHAR 4
NOMECLI : CHAR 50
NUMCLI : CHAR 10
DATAPEDIDO : DATE
PREVISAOENTREGA : DATE
CODMAT : CHAR 3
DESCMAT : CHAR 30
OBSPEDIDO : MEMO/TEXT
ITEM : INT
CODPECA : CHAR 6
LARGURA : DECIMAL
ALTURA : DECIMAL
AREA : DECIMAL
AMOLADURA : INT
TIPO : F ou M (Fixo ou M�vel)
OBSPECA : CHAR 50

CODACABAMENTO: CHAR 3
QTDEACABAMENTO: DECIMAL
CODCANTO: CHAR 3
QTDECANTO: DECIMAL
CODFORMA: CHAR 3
CODSERVI�O: CHAR 3
AREAPEDIDO : DECIMAL
Abaixo pe�as relativa a quebra com os mesmos tipos do respectivo campo
Descrito acima:
CODPE�AQ,LARGURAQ;ALTURAQ;AREAQ;AMOLADURAQ;TIPOQ;REPOSICAO (char 6);
}

     if (lbcaminho.caption='')
     Then begin
              Mensagemerro('Escolha o arquivo que deseja importar');
              exit;
     End;


     if (ObjParametroGlobal.ValidaParametro('CODIGO VENDEDOR PADR�O')=false)then
      Begin
           MensagemErro('O par�metro "CODIGO VENDEDOR PADR�O" n�o foi encontrado.');
           exit;
      end;
      PVENDEDORPADRAO:=ObjParametroGlobal.Get_Valor;


     edterro.text:='';

     Try
        PStr:=Tstringlist.create;
     Except
           Mensagemerro('Erro na tentativa de criar a String list');
           exit;
     End;

Try

     ppedidoatual:='';
     PpedidoprojetoAtual:='';
     pCodigovidrocoratual:='';
     
     FMostraBarraProgresso.ConfiguracoesIniciais(MemoDados.lines.count,0);
     FMostraBarraProgresso.lbmensagem.caption:='Importando pedidos';
     FMostraBarraProgresso.Lbmensagem2.visible:=true;
     
     for cont:=0 to MemoDados.lines.count-1 do
     begin
          FMostraBarraProgresso.IncrementaBarra1(1);
          FMostraBarraProgresso.show;
          Application.ProcessMessages;

          edterro.text:=MemoDados.lines[cont];

          temp:=MemoDados.lines[cont];

          While (Pos(';;',temp)>0) do
          Begin
               //quando tenho ;; o explode nao funciona direito, entao coloco um ponto no meio pra acertar
               temp:=strreplace(temp,';;',';.;');
          End;

          if (ExplodeStr(temp,pstr,';','STRING')=False)
          Then Begin
                    mensagemerro('N�o foi poss�vel decodificar a linha '+inttostr(cont+1)+#13+MemoDados.lines[cont]);
                    exit;
          End;

          if (pstr[0]='C')
          Then Begin
                    //cabe�alho, crio um novo pedido e um projeto em branco pra ele
                    //C STATUS;CODPEDIDO;CODCLI;NOMECLI;NUMCLI;DATAPEDIDO;PREVISAOENTREGA;CODMAT;DESCMAT;
                    //0   1      2         3       4       5       6           7             8       9

                    if (Self.ObjPedidoObjetos.ObjVidro_PP.VidroCor.Vidro.LocalizaCodigo2(pstr[8])=False)
                    Then Begin
                              Mensagemerro('Vidro '+Pstr[8]+' n�o encontrado');
                              exit;
                    End;
                    
                    Self.ObjPedidoObjetos.ObjVidro_PP.VidroCor.Vidro.TabelaparaObjeto;
                    Self.ObjPedidoObjetos.ObjVidro_PP.VidroCor.Cor.LocalizaReferencia('0');
                    Self.ObjPedidoObjetos.ObjVidro_PP.VidroCor.Cor.TabelaparaObjeto;


                    if (ObjPedidoObjetos.ObjVidro_PP.VidroCor.Localiza_Vidro_Cor(Self.ObjPedidoObjetos.ObjVidro_PP.VidroCor.Vidro.get_codigo,Self.ObjPedidoObjetos.ObjVidro_PP.VidroCor.Cor.GET_CODIGO)=False)
                    then Begin
                              mensagemerro('N�o foi localizado o vidro '+Self.ObjPedidoObjetos.ObjVidro_PP.VidroCor.Vidro.Get_Descricao+' na cor 0(zero)');
                              exit;
                    End;
                    ObjPedidoObjetos.ObjVidro_PP.VidroCor.TabelaparaObjeto;
                    pCodigovidrocoratual:=ObjPedidoObjetos.ObjVidro_PP.VidroCor.get_codigo;

                    Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.ZerarTabela;
                    With Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido do
                    Begin
                          status:=dsinsert;
                          Submit_Codigo('0');
                          Submit_Descricao(PStr[4]+' '+PStr[5]);//nomecli e NUMCLI
                          Submit_Complemento('');
                          Submit_Obra('PED '+pstr[2]);

                          if (cliente.LocalizaCodigo2(pstr[3])=False)
                          Then Begin
                                    Mensagemerro('Cliente '+pstr[3]+' n�o encontrado');
                                    exit;
                          End;
                          cliente.TabelaparaObjeto;

                          Arquiteto.Submit_CODIGO('');
                          Vendedor.Submit_codigo(pvendedorpadrao);
                          
                          Submit_Titulo('');
                          Submit_Data(pstr[6]);
                          Submit_Concluido('N');
                          Submit_Observacao('');
                          Submit_ValorTotal('0');
                          Submit_ValorTitulo('0');
                          Submit_Valoracrescimo('0');
                          Submit_Valordesconto('0');
                          Submit_ValorComissaoArquiteto('0');
                          Submit_ValorBonificacaoCliente('0');
                          Submit_BonificacaoGerada('0');
                          Submit_AlteradoporTroca('N');

                          if (Salvar(false)=False)
                          then Begin
                                    Mensagemerro('Erro na tentativa de Gravar o pedido');
                                    exit;
                          End;
                          Ppedidoatual:=get_codigo;
                    End;

                    //criando um projeto em branco
                    With   Self.ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto do
                    Begin
                         ZerarTabela;
                         Status:=dsInsert;
                         Submit_Codigo('0');
                         Pedido.Submit_Codigo(ppedidoatual);
                         Projeto.Submit_Codigo(FDataModulo.CodigoProjetoBrancoGlobal);

                         CorFerragem.LocalizaReferencia('0');
                         CorFerragem.TabelaparaObjeto;
                         CorPerfilado.LocalizaReferencia('0');
                         CorPerfilado.TabelaparaObjeto;
                         CorVidro.LocalizaReferencia('0');
                         CorVidro.TabelaparaObjeto;
                         CorKitBox.LocalizaReferencia('0');
                         CorKitBox.TabelaparaObjeto;
                         CorDiverso.LocalizaReferencia('0');
                         CorDiverso.TabelaparaObjeto;
                         
                         Submit_Local('');
                         Submit_TipoPedido('RETIRADO');
                         Submit_ValorTotal    ('0');
                         Submit_ValorDesconto ('0');
                         Submit_ValorAcrescimo('0');

                         if (Salvar(false)=False)
                         Then Begin
                               Mensagemerro('Erro na tentativa de gravar um pedido/projeto em branco');
                               exit;
                         End;
                         PpedidoprojetoAtual:=Get_Codigo;
                    End;

                    FMostraBarraProgresso.Lbmensagem2.caption:=ppedidoatual;
                    FMostraBarraProgresso.show;
                    Application.ProcessMessages;
          End;//Cabecalho

          if (pstr[0]='F')//totalizacao
          then Begin
                    //F AREAPEDIDO;
                    {

                     F;3,08;12840;16;
                     0  1   2     3

                     O primeiro: 3,08 � a somatoria da area do pedido do cliente (somatoria do 5o campo das linhas iniciadas por I, que representa cada pe�a)
                     O segundo: 12840 � a somatoria da quantidade de acabamento (10o campo das linhas iniciadas por I, que representa cada pe�a)
                     O terceiro: 16 � somatoria da quantidade de cantos especiais utilizados nas pe�as (11o campo das linhas iniciadas por I, que representa cada pe�a)
                     Se voce vai usar apenas o total, utilize apenas o primeiro campo. Ele tem a somatoria das areas das pe�as arredondadas de 5 em 5 mm, como � de uso na industria do vidro.
                    }


                    //para se totalizar algo � necess�rio ter um pedido criado
                    if (ppedidoatual='')
                    then Begin
                              Mensagemerro('Existe um erro no arquivo de importa��o, existe um registro de Totaliza��o "F" sem que haja um registro de cria��o de pedido "C"');
                              exit;
                    End;

                    With Self.ObjPedidoObjetos.ObjVidro_PP do
                    Begin
                        ZerarTabela;

                        Status:=dsInsert;
                        Submit_Codigo('0');
                        Submit_PedidoProjeto(PpedidoprojetoAtual);
                        VidroCor.Submit_Codigo(pCodigovidrocoratual);

                        //a quantidade tenho em metros quadrados
                        //preciso converrter em milimetros
                        //pra isso preferi manter a altura sempre 1000 mm (1 metro)
                        //e calcular apenas a largura 

                        Pquantidade:=strtofloat(PStr[1]);

                        try
                            pvalorvidro:=Strtofloat(tira_ponto(Formata_valor(Self.ObjPedidoObjetos.RetornaValorvidro(PpedidoprojetoAtual,pCodigovidrocoratual))));
                        Except
                                pvalorvidro:=0;
                        End;

                        Submit_Quantidade(floattostr(pquantidade));
                        Submit_Valor(floattostr(pvalorvidro));
                        Submit_Largura(floattostr(pquantidade*1000));
                        Submit_Altura('1000');
                        Submit_Complemento('');

                        if (Salvar(True)=False)
                        Then Begin
                                  Mensagemerro('Erro na tentativa de gravar o vidro');
                                  exit;
                        End;
                        Self.ObjPedidoObjetos.AtualizavalorProjeto(PpedidoprojetoAtual,false);
                    End;

                    PpedidoprojetoAtual:='';
                    ppedidoatual:='';
                    pCodigovidrocoratual:='';
          End;

          

     End;//for

     MensagemAviso('Conclu�do');
     FdataModulo.IBTransaction.CommitRetaining;

Finally
       FMostraBarraProgresso. close;
       FdataModulo.IBTransaction.RollbackRetaining;
       freeandnil(Pstr);
End;

end;

end.
