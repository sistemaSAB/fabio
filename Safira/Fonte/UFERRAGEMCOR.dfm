object FFERRAGEMCOR: TFFERRAGEMCOR
  Left = -20
  Top = 103
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsNone
  Caption = 'CADASTRO DE FERRAGEMCOR'
  ClientHeight = 409
  ClientWidth = 792
  Color = 13421772
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object ImageGuiaPrincipal: TImage
    Left = 25
    Top = -1
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaPrincipalClick
  end
  object ImageGuiaExtra: TImage
    Left = 25
    Top = 76
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaExtraClick
  end
  object btFechar: TSpeedButton
    Left = 754
    Top = 6
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btFecharClick
  end
  object btMinimizar: TSpeedButton
    Left = 754
    Top = 32
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btMinimizarClick
  end
  object lbAjuda: TLabel
    Left = 649
    Top = 351
    Width = 50
    Height = 13
    Caption = 'Ajuda...'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btAjuda: TSpeedButton
    Left = 719
    Top = 336
    Width = 28
    Height = 38
    Flat = True
  end
  object PainelExtra: TPanel
    Left = 40
    Top = 27
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 0
    object ImagePainelExtra: TImage
      Left = 0
      Top = 14
      Width = 98
      Height = 352
    end
  end
  object PainelPrincipal: TPanel
    Left = 40
    Top = -13
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 1
    object ImagePainelPrincipal: TImage
      Left = 1
      Top = 14
      Width = 98
      Height = 352
    end
    object btNovo: TSpeedButton
      Left = 15
      Top = 25
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btNovoClick
    end
    object btSalvar: TSpeedButton
      Left = 15
      Top = 66
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSalvarClick
    end
    object btAlterar: TSpeedButton
      Left = 15
      Top = 107
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btAlterarClick
    end
    object btCancelar: TSpeedButton
      Left = 15
      Top = 148
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btCancelarClick
    end
    object btSair: TSpeedButton
      Left = 15
      Top = 311
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSairClick
    end
    object btRelatorio: TSpeedButton
      Left = 15
      Top = 270
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btRelatorioClick
    end
    object btExcluir: TSpeedButton
      Left = 15
      Top = 229
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btExcluirClick
    end
    object btPesquisar: TSpeedButton
      Left = 15
      Top = 188
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      Layout = blGlyphRight
      OnClick = btPesquisarClick
    end
  end
  object Notebook: TNotebook
    Left = 139
    Top = 0
    Width = 612
    Height = 331
    TabOrder = 3
    object TPage
      Left = 0
      Top = 0
      Caption = 'Default'
      object Bevel1: TBevel
        Left = 0
        Top = 0
        Width = 612
        Height = 331
        Align = alClient
        Shape = bsFrame
        Style = bsRaised
      end
      object LbCodigo: TLabel
        Left = 3
        Top = 32
        Width = 40
        Height = 13
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbFerragem: TLabel
        Left = 3
        Top = 56
        Width = 55
        Height = 13
        Caption = 'Ferragem'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbCor: TLabel
        Left = 3
        Top = 80
        Width = 21
        Height = 13
        Caption = 'Cor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbPorcentagemAcrescimo: TLabel
        Left = 3
        Top = 104
        Width = 138
        Height = 13
        Caption = 'Porcentagem Acrescimo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbEstoque: TLabel
        Left = 3
        Top = 128
        Width = 45
        Height = 13
        Caption = 'Estoque'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbAcrescimoExtra: TLabel
        Left = 3
        Top = 152
        Width = 93
        Height = 13
        Caption = 'Acr'#233'scimo Extra'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbPorcentagemAcrescimoFinal: TLabel
        Left = 3
        Top = 176
        Width = 168
        Height = 13
        Caption = 'Porcentagem Acr'#233'scimo Final'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeFerragem: TLabel
        Left = 256
        Top = 56
        Width = 345
        Height = 13
        AutoSize = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeCor: TLabel
        Left = 256
        Top = 83
        Width = 345
        Height = 13
        AutoSize = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object EdtPorcentagemAcrescimoFinal: TEdit
        Left = 177
        Top = 174
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object EdtAcrescimoExtra: TEdit
        Left = 177
        Top = 150
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 1
      end
      object EdtEstoque: TEdit
        Left = 177
        Top = 126
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 2
      end
      object EdtPorcentagemAcrescimo: TEdit
        Left = 177
        Top = 102
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 3
      end
      object EdtCor: TEdit
        Left = 177
        Top = 78
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 4
        OnExit = edtCorExit
        OnKeyDown = edtCorKeyDown
      end
      object EdtFerragem: TEdit
        Left = 177
        Top = 54
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 5
        OnExit = edtFerragemExit
        OnKeyDown = edtFerragemKeyDown
      end
      object EdtCodigo: TEdit
        Left = 177
        Top = 30
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 6
      end
    end
  end
  object Guia: TTabSet
    Left = 141
    Top = 1
    Width = 607
    Height = 22
    BackgroundColor = 6710886
    DitherBackground = False
    EndMargin = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentBackground = True
    StartMargin = -3
    SelectedColor = 15921906
    Tabs.Strings = (
      '&1 - Principal')
    TabIndex = 0
    UnselectedColor = 13421772
    OnClick = GuiaClick
  end
end
