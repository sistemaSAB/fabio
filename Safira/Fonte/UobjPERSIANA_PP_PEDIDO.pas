unit UobjPERSIANA_PP_PEDIDO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJPERSIANAGRUPODIAMETROCOR;

Type
   TObjPERSIANA_PP_PEDIDO=class

          Public
                ObjDataSource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                PersianaGrupoDiametroCor:TOBJPERSIANAGRUPODIAMETROCOR;
                //PedidoProjeto:TOBJPEDIDO_PROJ;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure ResgataPERSIANA_PP(PPedido, PPEdidoProjeto: string);
                Function ResgataValorFinal(PPedido:string;Var PValor:Currency):Boolean;

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Quantidade(parametro: string);
                Function Get_Quantidade: string;
                Procedure Submit_Valor(parametro: string);

                Function Get_Complemento: String;
                Procedure Submit_Complemento(parametro: String);

                Function Get_Altura: string;
                Procedure Submit_Altura(parametro: string);

                Function Get_Largura: string;
                Procedure Submit_Largura(parametro: string);

                Function Get_Local: string;
                Procedure Submit_Local(parametro: string);
                Function Get_Instalacao: string;
                Procedure Submit_Instalacao(parametro: string);
                Function Get_Comando: string;
                Procedure Submit_Comando(parametro: string);

                Function Get_Valor: string;
                Function Get_ValorFinal:string;

                Function get_Pedidoprojeto:string;
                Procedure Submit_PedidoProjeto(Parametro:string);

                procedure EdtPersianaGrupoDiametroCorExit(Sender: TObject;VAr PEdtCodigo :TEdit;LABELNOME:TLABEL);
                procedure EdtPersianaGrupoDiametroCorKeyDown(Sender: TObject; Var PEdtCodigo:TEdit;var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Function DeletaPersiana_PP(PPedidoProjeto:string): Boolean;
                Function Soma_por_PP(PpedidoProjeto:string):Currency;
                Procedure RetornaDadosRel(PpedidoProjeto:string;STrDados:TStrings;ComValor:Boolean);
                Procedure RetornaDadosRel2(PpedidoProjeto:string;STrDados:TStrings;ComValor:Boolean);
                Procedure RetornaDadosRelProjetoBranco(PpedidoProjeto:string;STrDados:TStrings; pquantidade:integer;Var PTotal:Currency);
                procedure EdtpersianaGrupoDiametroCorExit_codigo(Sender: TObject;LABELNOME: TLABEL);

                function DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string; PControlaNegativo: Boolean;Pdata: String): boolean;
                Function DiminuiAumentaEstoquePedido(NotaFiscal,PpedidoProjeto: String;PControlaNegativo: Boolean;Pdata:String;aumenta:String): boolean;                
                function AumentaEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;Pdata:string):Boolean;

         Private
               Objquery:Tibquery;
               ObjQueryPesquisa:TIBquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Quantidade:string;
               Valor:string;
               ValorFinal:string;
               Altura : string;
               Largura :string;
               Complemento:String;
               Local:string;
               Instalacao:string;
               Comando:string;
               PedidoProjeto:string;
               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;




   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UobjCOR, UessencialLocal;


Function  TObjPERSIANA_PP_PEDIDO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If(FieldByName('PersianaGrupoDiametroCor').asstring<>'')
        Then Begin
                 If (Self.PersianaGrupoDiametroCor.LocalizaCodigo(FieldByName('PersianaGrupoDiametroCor').asstring)=False)
                 Then Begin
                          Messagedlg('PersianaGrupoDiametroCor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PersianaGrupoDiametroCor.TabelaparaObjeto;
        End;
        Self.PedidoProjeto:=fieldbyname('PedidoProjeto').asstring;

        Self.Quantidade:=fieldbyname('Quantidade').asstring;
        Self.Valor:=fieldbyname('Valor').asstring;
        Self.ValorFinal:=fieldbyname('ValorFinal').AsString;
        Self.Complemento:=fieldbyname('Complemento').AsString;
        Self.Altura:=fieldbyname('Altura').AsString;
        Self.Largura:=fieldbyname('Largura').AsString;
        Self.Local:=fieldbyname('LOcal').AsString;
        Self.Instalacao:=fieldbyname('Instalacao').AsString;
        Self.Comando:=fieldbyname('Comando').AsString;
        result:=True;
     End;
end;


Procedure TObjPERSIANA_PP_PEDIDO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('PersianaGrupoDiametroCor').asstring:=Self.PersianaGrupoDiametroCor.GET_CODIGO;
        ParamByName('PedidoProjeto').asstring:=Self.PedidoProjeto;
        ParamByName('Quantidade').asstring:=virgulaparaponto(Self.Quantidade);
        ParamByName('Valor').asstring:=virgulaparaponto(Self.Valor);
        Parambyname('Complemento').AsString:=Self.Complemento;
        ParamByName('Altura').AsString:=virgulaparaponto(Self.Altura);
        ParamByName('Largura').AsString:=virgulaparaponto(Self.Largura);
        ParamByName('Local').AsString:=Self.Local;
        ParamByName('Instalacao').AsString:=Self.Instalacao;
        ParamByName('Comando').AsString:=Self.Comando;
  End;
End;

//***********************************************************************

function TObjPERSIANA_PP_PEDIDO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPERSIANA_PP_PEDIDO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        PersianaGrupoDiametroCor.ZerarTabela;
        PedidoProjeto:='';
        Quantidade:='';
        Valor:='';
        ValorFinal:='';
        Complemento:='';
        Altura:='';
        Largura:='';
        Local:='';
        Instalacao:='';
        Comando:='';
     End;
end;

Function TObjPERSIANA_PP_PEDIDO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPERSIANA_PP_PEDIDO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.PersianaGrupoDiametroCor.LocalizaCodigo(Self.PersianaGrupoDiametroCor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ PersianaGrupoDiametroCor n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjPERSIANA_PP_PEDIDO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.PersianaGrupoDiametroCor.Get_Codigo<>'')
        Then Strtoint(Self.PersianaGrupoDiametroCor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/PersianaGrupoDiametroCor';
     End;
     try
        If (Self.PedidoProjeto<>'')
        Then Strtoint(Self.PedidoProjeto);
     Except
           Mensagem:=mensagem+'/PedidoProjeto';
     End;
     try
        Strtofloat(Self.Quantidade);
     Except
           Mensagem:=mensagem+'/Quantidade';
     End;
     try
        Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPERSIANA_PP_PEDIDO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPERSIANA_PP_PEDIDO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPERSIANA_PP_PEDIDO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PERSIANA_PP vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,PersianaGrupoDiametroCor,PedidoProjeto,Quantidade');
           SQL.ADD(' ,Valor, ValorFinal,Complemento,Altura, Largura,Local, Instalacao, Comando');
           SQL.ADD(' from  TABPERSIANA_PP');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPERSIANA_PP_PEDIDO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPERSIANA_PP_PEDIDO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPERSIANA_PP_PEDIDO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDataSource.DataSet:=Self.ObjQueryPesquisa;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.PersianaGrupoDiametroCor:=TOBJPERSIANAGRUPODIAMETROCOR.create;
        //Self.PedidoProjeto:=TOBJPEDIDO_PROJ.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABPERSIANA_PP(Codigo,PersianaGrupoDiametroCor');
                InsertSQL.add(' ,PedidoProjeto,Quantidade,Valor, Complemento, Altura, Largura, Local, Instalacao, Comando)');
                InsertSQL.add('values (:Codigo,:PersianaGrupoDiametroCor,:PedidoProjeto');
                InsertSQL.add(' ,:Quantidade,:Valor,:Complemento,:Altura,:Largura,:Local,:Instalacao,:Comando)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABPERSIANA_PP set Codigo=:Codigo,PersianaGrupoDiametroCor=:PersianaGrupoDiametroCor');
                ModifySQL.add(',PedidoProjeto=:PedidoProjeto,Quantidade=:Quantidade');
                ModifySQL.add(',Valor=:Valor,Complemento=:Complemento, Altura=:Altura, Largura=:Largura');
                ModifySQl.Add(',Local=:Local, Instalacao=:Instalacao, Comando=:Comando');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABPERSIANA_PP where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPERSIANA_PP_PEDIDO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPERSIANA_PP_PEDIDO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPERSIANA_PP');
     Result:=Self.ParametroPesquisa;
end;

function TObjPERSIANA_PP_PEDIDO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de PERSIANA_PP ';
end;


function TObjPERSIANA_PP_PEDIDO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPERSIANA_PP,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENPERSIANA_PP,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPERSIANA_PP_PEDIDO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(Self.ObjQueryPesquisa);
    Freeandnil(Self.ObjDataSource);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.PersianaGrupoDiametroCor.FREE;
    //Self.PedidoProjeto.FREE;
//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPERSIANA_PP_PEDIDO.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPERSIANA_PP_PEDIDO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPERSIANA_PP_PEDIDO.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjPERSIANA_PP_PEDIDO.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjPERSIANA_PP_PEDIDO.Submit_Quantidade(parametro: string);
begin
        Self.Quantidade:=Parametro;
end;
function TObjPERSIANA_PP_PEDIDO.Get_Quantidade: string;
begin
        Result:=Self.Quantidade;
end;
procedure TObjPERSIANA_PP_PEDIDO.Submit_Valor(parametro: string);
begin
        Self.Valor:=Parametro;
end;
function TObjPERSIANA_PP_PEDIDO.Get_Valor: string;
begin
        Result:=Self.Valor;
end;
//CODIFICA GETSESUBMITS


procedure TObjPERSIANA_PP_PEDIDO.EdtPersianaGrupoDiametroCorExit(Sender: TObject;VAr PEdtCodigo :TEdit; LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PersianaGrupoDiametroCor.localizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               PEdtCodigo.Text:='';
               exit;
     End;
     Self.PersianaGrupoDiametroCor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.PersianaGrupoDiametroCor.Persiana.Get_Nome+' - '+Self.PersianaGrupoDiametroCor.Cor.Get_Descricao;
     PEdtCodigo.Text:=Self.PersianaGrupoDiametroCor.Get_Codigo;

End;
procedure TObjPERSIANA_PP_PEDIDO.EdtPersianaGrupoDiametroCorKeyDown(Sender: TObject;Var PEdtCodigo:TEdit;  var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FpesquisaLocal.PesquisaEmEstoque := True;
            If (FpesquisaLocal.PreparaPesquisa(self.PersianaGrupoDiametroCor.Get_Pesquisa,Self.PersianaGrupoDiametroCor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('REF_PERSIANA').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 LABELNOME.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('Persiana').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjPERSIANA_PP_PEDIDO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPERSIANA_PP';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjPERSIANA_PP_PEDIDO.ResgataPERSIANA_PP(PPedido, PPEdidoProjeto: string);
begin
       With Self.ObjQueryPesquisa do
       Begin
           Close;
           SQL.Clear;
           Sql.add('Select  TabPersiana.Referencia,TabPersiana.Nome as Persiana,');
           Sql.add('TabPersiana_PP.Quantidade,');
           Sql.add('TabPersiana_PP.Valor,');
           Sql.add('TabPersiana_PP.ValorFinal,');
           Sql.Add('TabPersiana_PP.Complemento,TabPedido_Proj.Codigo as PedidoProjeto,');
           Sql.add('TabPersiana_PP.Codigo');
           Sql.add('from TabPersiana_PP');
           Sql.add('Join TabPedido_Proj on TabPedido_Proj.Codigo = TabPersiana_PP.PedidoProjeto');
           Sql.add('Join TabPedido on TabPedido.Codigo = TabPedido_Proj.Pedido');
           Sql.add('Join TabPersianaGrupoDiametroCor on TabPersianaGrupoDiametroCor.Codigo = TabPersiana_PP.PersianaGrupoDiametroCor');
           Sql.add('Join TabPersiana on TabPersiana.Codigo = TabPersianaGrupoDiametroCor.Persiana');

           Sql.add('Where TabPedido_Proj.Pedido = '+PPedido);
           if (PpedidoProjeto <> '' )
           then Sql.add('and   TabPedido_Proj.Codigo ='+PPedidoProjeto);

           if (PPedido = '')then
           exit;

           Open;

       end;
end;

function TObjPERSIANA_PP_PEDIDO.DeletaPersiana_PP(PPedidoProjeto: string): Boolean;
begin
     Result:=False;
     With Objquery  do
     Begin
        Close;
        SQL.Clear;
        Sql.Add('Delete from TabPersiana_PP where PedidoProjeto='+ppedidoprojeto);
        Try
            execsql;
        Except
              On E:Exception do
              Begin
                   Messagedlg('Erro na tentativa de Excluir as Persianas do Projeto '+#13+E.message,mterror,[mbok],0);
                   exit;
              End;
        End;
     end;
     Result:=true;
end;

function TObjPERSIANA_PP_PEDIDO.Get_ValorFinal: string;
begin
     Result:=Self.ValorFinal;
end;

function TObjPERSIANA_PP_PEDIDO.ResgataValorFinal(PPedido: string; Var PValor:Currency): Boolean;
begin
      Result:=False;
      With Self.Objquery  do
      Begin
           Close;
           Sql.Clear;
           Sql.Add('Select SUM(ValorFinal) as ValorFinal');
           Sql.Add('from TabPersiana_PP');
           Sql.Add('Where TabPersiana_PP.PedidoProjeto = '+PPedido);

           if (PPedido='')then
           exit;

           Open;

           PValor:=fieldbyname('ValorFinal').AsCurrency;

           Result:=true;
      end;
end;

function TObjPERSIANA_PP_PEDIDO.Soma_por_PP(PpedidoProjeto: string): Currency;
begin

     Self.Objquery.close;
     Self.Objquery.sql.clear;
     Self.Objquery.sql.add('Select sum(valorfinal) as SOMA from TabPersiana_PP where PedidoProjeto='+PpedidoProjeto);
     Try
         Self.Objquery.open;
         Result:=Self.Objquery.fieldbyname('soma').asfloat;
     Except
           Result:=0;
     End;

end;

procedure TObjPERSIANA_PP_PEDIDO.RetornaDadosRel(PpedidoProjeto: string;STrDados: TStrings;ComValor:Boolean);
Var QtdeLinhasMemo, Cont:Integer;
    PStrList:TStringList;
    PAlturaLargura:string;
begin
     if (PpedidoProjeto='')
     Then exit;

try
     try
         PStrList:=TStringList.Create;
     except
         MensagemErro('Erro ao tentar criar o PStrList');
         exit;
     end;

     With Self.ObjQueryPesquisa do
     Begin
          close;
          SQL.clear;
          sql.add('Select codigo from TabPersiana_pp where PedidoProjeto='+PpedidoProjeto);
          open;
          first;

          if (RecordCount = 0)then
          exit;

          While not(eof) do
          Begin
               if(ComValor=True) then
               begin

                     Self.LocalizaCodigo(fieldbyname('codigo').asstring);
                     Self.TabelaparaObjeto;

                     PreencheStringList(PStrList, Self.Get_Complemento);

                     STrDados.Add(CompletaPalavra(Self.PersianaGrupoDiametroCor.Persiana.Get_Referencia,10,' ')+' '+
                                                         CompletaPalavra(Self.PersianaGrupoDiametroCor.Cor.Get_Referencia+'-'+
                                                         Self.PersianaGrupoDiametroCor.Cor.Get_Descricao,16,' ')+' '+
                                                         CompletaPalavra(Self.PersianaGrupoDiametroCor.Persiana.Get_Nome,45,' ')+' '+
                                                         CompletaPalavra(Self.Get_Quantidade+' m�',6,' ')+' '+
                                                         CompletaPalavra_a_Esquerda(formata_valor(Self.Get_Valor),10,' ')+' '+
                                                         CompletaPalavra_a_Esquerda(formata_valor(StrToCurr(Self.Get_Quantidade)*StrToCurr(Self.Get_Valor)),8,' '));

                     PAlturaLargura:='';
                     if (Get_Altura<>'')then
                     PAlturaLargura:=PAlturaLargura+'ALTURA: '+Get_Altura;
                     if (Get_Largura<>'')then
                     PAlturaLargura:=PAlturaLargura+'    LARGURA: '+Get_Largura;

                     if (Self.Get_Local <>'')then
                     STrDados.Add('                            '+'LOCAL: '+CompletaPalavra(Get_Local,30,' '));

                     if (Self.Get_Instalacao <>'')then
                     STrDados.Add('                            '+'INSTALA��O: '+CompletaPalavra(Get_Instalacao,30,' '));

                     if (Self.Get_Comando <>'')then
                     STrDados.Add('                            '+'COMANDO: '+CompletaPalavra(Get_Comando,30,' '));

                     For Cont:=0 to PStrList.Count-1 do
                     Begin
                         STrDados.Add('                            '+ CompletaPalavra(PStrList.Strings[Cont],30,' '));
                    end;
               end
               else
               begin
                     Self.LocalizaCodigo(fieldbyname('codigo').asstring);
                     Self.TabelaparaObjeto;

                     PreencheStringList(PStrList, Self.Get_Complemento);

                     STrDados.Add(CompletaPalavra(Self.PersianaGrupoDiametroCor.Persiana.Get_Referencia,10,' ')+' '+
                                                         CompletaPalavra(Self.PersianaGrupoDiametroCor.Cor.Get_Referencia+'-'+
                                                         Self.PersianaGrupoDiametroCor.Cor.Get_Descricao,16,' ')+' '+
                                                         CompletaPalavra(Self.PersianaGrupoDiametroCor.Persiana.Get_Nome,45,' ')+' '+
                                                         CompletaPalavra(Self.Get_Quantidade+' m�',6,' '));

                     PAlturaLargura:='';
                     if (Get_Altura<>'')then
                     PAlturaLargura:=PAlturaLargura+'ALTURA: '+Get_Altura;
                     if (Get_Largura<>'')then
                     PAlturaLargura:=PAlturaLargura+'    LARGURA: '+Get_Largura;

                     if (Get_Largura<>'')
                     then STrDados.Add(CompletaPalavra(PAlturaLargura,30,' '));

                     if (Self.Get_Local <>'')then
                     STrDados.Add('                            '+'LOCAL: '+CompletaPalavra(Get_Local,30,' '));

                     if (Self.Get_Instalacao <>'')then
                     STrDados.Add('                            '+'INSTALA��O: '+CompletaPalavra(Get_Instalacao,30,' '));

                     if (Self.Get_Comando <>'')then
                     STrDados.Add('                            '+'COMANDO: '+CompletaPalavra(Get_Comando,30,' '));

                     For Cont:=0 to PStrList.Count-1 do
                     Begin
                         STrDados.Add('                            '+ CompletaPalavra(PStrList.Strings[Cont],30,' '));
                     end;

               end;

              next;
          end;
          if(ComValor=True) then
          begin
              close;
              sql.clear;
              sql.add('Select sum(ValorFinal) as SOMA FROM TabPersiana_PP where PedidoProjeto='+PpedidoProjeto);
              open;
              STrDados.Add('?'+CompletaPalavra_a_Esquerda('Total PERSIANAS: '+formata_valor(fieldbyname('soma').asstring),100,' '));
              STrDados.Add(' ');
          end;
     End;
finally
     FreeAndNil(PStrList);
end;
end;

function TObjPERSIANA_PP_PEDIDO.AumentaEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;Pdata:String): boolean;
var
PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          Sql.add('Select codigo,PersianaGrupoDiametroCor,quantidade from TabPersiana_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;
          first;
          While not(eof) do
          Begin

              Self.PersianaGrupoDiametroCor.LocalizaCodigo(fieldbyname('PersianaGrupoDiametroCor').asstring);
              Self.PersianaGrupoDiametroCor.TabelaparaObjeto;

              try
                 PestoqueAtual:=strtofloat(Self.PersianaGrupoDiametroCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual do Persiana '+Self.PersianaGrupoDiametroCor.Persiana.Get_Nome+' da cor '+Self.PersianaGrupoDiametroCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              Try
                PnovoEstoque:=PestoqueAtual+fieldbyname('quantidade').asfloat;
              Except
                    Messagedlg('Erro na gera��o do novo Estoque do Persiana '+Self.PersianaGrupoDiametroCor.Persiana.Get_Nome+' da cor '+Self.PersianaGrupoDiametroCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              PersianaGrupoDiametroCor.AumentaEstoque(fieldbyname('quantidade').asstring,fieldbyname('PersianaGrupoDiametroCor').asstring);

              Next;
          End;
          result:=True;
          exit;
     End;
end;

function TObjPERSIANA_PP_PEDIDO.DiminuiAumentaEstoquePedido(NotaFiscal,PpedidoProjeto: String;PControlaNegativo: Boolean;Pdata:String;aumenta:String): boolean;
var
PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          if(aumenta='N')
          then Sql.add('Select codigo,PersianaGrupoDiametroCor,(quantidade*-1) as quantidade from TabPersiana_PP where PedidoProjeto='+PpedidoProjeto)
          else Sql.add('Select codigo,PersianaGrupoDiametroCor,quantidade from TabPersiana_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;
          first;
          While not(eof) do
          Begin
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Status:=dsinsert;
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(fieldbyname('quantidade').asstring);
              OBJESTOQUEGLOBAL.Submit_DATA(Pdata);
              OBJESTOQUEGLOBAL.Submit_PERSIANAGRUPODIAMETROCOR(fieldbyname('PERSIANAGRUPODIAMETROCOR').asstring);
              OBJESTOQUEGLOBAL.Submit_NotaFiscal(NotaFiscal);
              OBJESTOQUEGLOBAL.Submit_PERSIANA_PP(fieldbyname('codigo').asstring);
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('PEDIDO (NOTA FISCAL)');


              Self.PersianaGrupoDiametroCor.LocalizaCodigo(fieldbyname('PersianaGrupoDiametroCor').asstring);
              Self.PersianaGrupoDiametroCor.TabelaparaObjeto;

              try
                 PestoqueAtual:=strtofloat(Self.PersianaGrupoDiametroCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual do Persiana '+Self.PersianaGrupoDiametroCor.Persiana.Get_Nome+' da cor '+Self.PersianaGrupoDiametroCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              Try
                PnovoEstoque:=PestoqueAtual+fieldbyname('quantidade').asfloat;
              Except
                    Messagedlg('Erro na gera��o do novo Estoque do Persiana '+Self.PersianaGrupoDiametroCor.Persiana.Get_Nome+' da cor '+Self.PersianaGrupoDiametroCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              if (Pnovoestoque<0)
              then Begin
                        if (PControlaNegativo=True)
                        Then begin
                                  Messagedlg('O Persiana '+Self.PersianaGrupoDiametroCor.Persiana.Get_Nome+' da cor '+Self.PersianaGrupoDiametroCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                  exit;
                        End;
              End;
              
              if (OBJESTOQUEGLOBAL.Salvar(False)=False)
              then exit;

              next;
          End;
          result:=True;
          exit;
     End;
end;


function TObjPERSIANA_PP_PEDIDO.DiminuiEstoqueRomaneio(PpedidoProjetoRomaneio,PpedidoProjeto: string;PControlaNegativo: Boolean;Pdata:String): boolean;
var
PestoqueAtual,PnovoEstoque:Currency;
begin
     Result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          Sql.add('Select codigo,PersianaGrupoDiametroCor,(quantidade*-1) as quantidade from TabPersiana_PP where PedidoProjeto='+PpedidoProjeto);
          open;
          if (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          End;
          first;
          While not(eof) do
          Begin
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Status:=dsinsert;
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(fieldbyname('quantidade').asstring);
              OBJESTOQUEGLOBAL.Submit_DATA(Pdata);
              OBJESTOQUEGLOBAL.Submit_PERSIANAGRUPODIAMETROCOR(fieldbyname('PERSIANAGRUPODIAMETROCOR').asstring);
              OBJESTOQUEGLOBAL.Submit_PEDIDOPROJETOROMANEIO(PpedidoProjetoRomaneio);
              OBJESTOQUEGLOBAL.Submit_PERSIANA_PP(fieldbyname('codigo').asstring);
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('ROMANEIO');


              Self.PersianaGrupoDiametroCor.LocalizaCodigo(fieldbyname('PersianaGrupoDiametroCor').asstring);
              Self.PersianaGrupoDiametroCor.TabelaparaObjeto;

              try
                 PestoqueAtual:=strtofloat(Self.PersianaGrupoDiametroCor.RetornaEstoque);
              Except
                    Messagedlg('Erro no Estoque Atual do Persiana '+Self.PersianaGrupoDiametroCor.Persiana.Get_Nome+' da cor '+Self.PersianaGrupoDiametroCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              Try
                PnovoEstoque:=PestoqueAtual+fieldbyname('quantidade').asfloat;
              Except
                    Messagedlg('Erro na gera��o do novo Estoque do Persiana '+Self.PersianaGrupoDiametroCor.Persiana.Get_Nome+' da cor '+Self.PersianaGrupoDiametroCor.Cor.Get_Descricao,mterror,[mbok],0);
                    exit;
              End;

              if (Pnovoestoque<0)
              then Begin
                        if (PControlaNegativo=True)
                        Then begin
                                  Messagedlg('O Persiana '+Self.PersianaGrupoDiametroCor.Persiana.Get_Nome+' da cor '+Self.PersianaGrupoDiametroCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
                                  exit;
                        End;
              End;


              //PersianaGrupoDiametroCor.DiminuiEstoque(fieldbyname('quantidade').asstring,fieldbyname('PersianaGrupoDiametroCor').asstring);
              if (OBJESTOQUEGLOBAL.Salvar(False)=False)
              then exit;

              next;
          End;
          result:=True;
          exit;
     End;
end;

function TObjPERSIANA_PP_PEDIDO.Get_Complemento: String;
begin
    Result:=Self.Complemento;
end;

procedure TObjPERSIANA_PP_PEDIDO.Submit_Complemento(parametro: String);
begin
    Self.Complemento:=parametro;
end;

function TObjPERSIANA_PP_PEDIDO.Get_Altura: string;
begin
    Result:=Self.Altura;
end;

function TObjPERSIANA_PP_PEDIDO.Get_Largura: string;
begin
    Result:=Self.Largura;
end;

procedure TObjPERSIANA_PP_PEDIDO.Submit_Altura(parametro: string);
begin
    Self.Altura:=parametro;
end;

procedure TObjPERSIANA_PP_PEDIDO.Submit_Largura(parametro: string);
begin
    Self.Largura:=parametro;
end;

procedure TObjPERSIANA_PP_PEDIDO.RetornaDadosRelProjetoBranco(PpedidoProjeto: string; STrDados: TStrings; pquantidade:integer;var PTotal: Currency);
var
  QtdeLinhasMemo, Cont:Integer;
  PStrList:TStringList;
  PAlturaLargura:string;
begin
  if (PpedidoProjeto='') then
    Exit;

  try
    try
      PStrList:=TStringList.Create;
    except
      MensagemErro('Erro ao tentar criar o PStrList');
      exit;
    end;

    with Self.ObjQueryPesquisa do
    begin
      Close;
      SQL.Clear;
      SQL.Text := ' select p.Referencia, c.Referencia|| ' + QuotedStr('-') + '|| c.Descricao as COR, '+
                  ' p.Nome, gp.Nome as Grupo, pc.Diametro, sp.complemento, sp.altura, sp.largura, '+
                  ' sp.local, sp.instalacao, sp.comando, sp.valor, '+
                  ' sum(sp.quantidade) * cast(:qtde as numeric(9,2)) as quantidade, '+
                  ' sum(sp.quantidade) * cast(:qtde as numeric(9,2)) * sp.valor as mult '+
                  ' from TabPersiana_pp sp '+
                  ' inner join TabpersianaGrupoDiametroCor pc on (sp.PersianaGrupoDiametroCor = pc.codigo)  '+
                  ' inner join TabCor c on (pc.Cor = c.codigo) '+
                  ' inner join tabpersiana p on (pc.persiana = p.codigo) '+
                  ' inner join TabGrupoPersiana gp on (gp.Codigo = pc.GrupoPersiana) '+
                  ' where sp.PedidoProjeto= :pedidoprojeto '+
                  ' group by 1,2,3,4,5,6,7,8,9,10,11,12 ';

      ParamByName('qtde').AsInteger := pquantidade;
      ParamByName('pedidoprojeto').AsString := PpedidoProjeto;
      Open;
      First;
      if (RecordCount = 0)then
        Exit;

      STrDados.Add(CompletaPalavra('?DESCRI��O',28,' ')+' '+
                   CompletaPalavra('GRUPO',20,' ')+' '+
                   CompletaPalavra('COR',24,' ')+' '+
                   CompletaPalavra('DIAMETRO',9,' ')+' '+
                   CompletaPalavra_a_Esquerda('QTDE',5,' ')+' '+
                   CompletaPalavra_a_Esquerda('VALOR',10,' '));


      while not(eof) do
      begin
        //Self.LocalizaCodigo(fieldbyname('codigo').asstring);
        //Self.TabelaparaObjeto;

        PreencheStringList(PStrList, Fieldbyname('complemento').asstring);

        STrDados.Add(CompletaPalavra(Fieldbyname('referencia').asstring+'-'+fieldbyname('Nome').AsString,27,' ')+' '+
                                           CompletaPalavra(Fieldbyname('Grupo').asstring,20,' ')+' '+
                                           CompletaPalavra(Fieldbyname('Cor').asstring,24,' ')+' '+
                                           CompletaPalavra(Fieldbyname('Diametro').asstring+' mm.',9,' ')+' '+
                                           CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('Quantidade').asstring),5,' ')+' '+
                                           CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('mult').asstring),10,' '));

        PAlturaLargura:='';

        if (Fieldbyname('altura').asstring<>'') then
          PAlturaLargura:=PAlturaLargura+'ALTURA: '+Fieldbyname('altura').asstring;

        if (Fieldbyname('largura').asstring<>'') then
          PAlturaLargura:=PAlturaLargura+'    LARGURA: '+Fieldbyname('largura').asstring;

        if (PAlturaLargura<>'') then
          StrDados.Add('                            '+PAlturaLargura);

        if (Fieldbyname('local').asstring <>'') then
          STrDados.Add('                            '+'LOCAL: '+CompletaPalavra(Fieldbyname('local').asstring,30,' '));

        if (Fieldbyname('instalacao').asstring<>'') then
          STrDados.Add('                            '+'INSTALA��O: '+CompletaPalavra(Fieldbyname('instalacao').asstring,30,' '));

        if (Fieldbyname('comando').asstring<>'') then
          STrDados.Add('                            '+'COMANDO: '+CompletaPalavra(Fieldbyname('comando').asstring,30,' '));

        for Cont:=0 to PStrList.Count-1 do
        begin
          STrDados.Add('                            '+ CompletaPalavra(PStrList.Strings[Cont],30,' '));
        end;

        Next;
      end;
      STrDados.Add('');
      Close;
      sql.clear;
      sql.add('Select sum(ValorFinal) as SOMA FROM TabPersiana_PP where PedidoProjeto='+PpedidoProjeto);
      open;

      PTotal:=PTotal+(fieldbyname('soma').AsCurrency*pquantidade);
    end;
  finally
    FreeAndNil(PStrList);
  end;
end;

function TObjPERSIANA_PP_PEDIDO.Get_Comando: string;
begin
    Result:=Self.Comando;
end;

function TObjPERSIANA_PP_PEDIDO.Get_Instalacao: string;
begin
    Result:=Self.Instalacao;
end;

function TObjPERSIANA_PP_PEDIDO.Get_Local: string;
begin
    REsult:=Self.Local;
end;

procedure TObjPERSIANA_PP_PEDIDO.Submit_Comando(parametro: string);
begin
    Self.Comando:=parametro;
end;

procedure TObjPERSIANA_PP_PEDIDO.Submit_Instalacao(parametro: string);
begin
    Self.Instalacao:=parametro;
end;

procedure TObjPERSIANA_PP_PEDIDO.Submit_Local(parametro: string);
begin
    Self.Local:=parametro;
end;

function TObjPERSIANA_PP_PEDIDO.get_Pedidoprojeto: string;
begin
     Result:=Self.pedidoprojeto;
end;

procedure TObjPERSIANA_PP_PEDIDO.Submit_PedidoProjeto(Parametro: string);
begin
     Self.pedidoprojeto:=parametro;
end;


procedure TObjpersiana_PP_PEDIDO.EdtpersianaGrupoDiametroCorExit_codigo(Sender: TObject;
LABELNOME: TLABEL);
begin
     labelnome.caption:='';
     If (Tedit(Sender).Text='')
     Then exit;

     If (Self.PersianaGrupoDiametroCor.localizacodigo (Tedit(Sender).Text)=False)
     Then Begin
               Tedit(Sender).Text:='';
               exit;
     End;
     Self.PersianaGrupoDiametroCor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.PersianaGrupoDiametroCor.persiana.get_Nome+'-'+Self.PersianaGrupoDiametroCor.Diametro.Get_Codigo+'-'+Self.PersianaGrupoDiametroCor.Cor.Get_Descricao;
     Tedit(Sender).Text:=Self.PersianaGrupoDiametroCor.Get_Codigo;
end;


procedure TObjPERSIANA_PP_PEDIDO.RetornaDadosRel2(PpedidoProjeto: string;
  STrDados: TStrings; ComValor: Boolean);
Var QtdeLinhasMemo, Cont:Integer;
    PStrList:TStringList;
    PAlturaLargura:string;
begin
  if (PpedidoProjeto='') then
    Exit;

  try
     PStrList:=TStringList.Create;
  except
     MensagemErro('Erro ao tentar criar o PStrList');
     exit;
  end;

  try
    with Self.ObjQueryPesquisa do
    begin
      Close;
      SQL.Clear;
      SQL.Text := ' select p.Referencia, c.Referencia|| ' + QuotedStr('-') + '|| c.Descricao as COR, '+
                  ' p.Nome, gp.Nome as Grupo, pc.Diametro, sp.complemento, sp.altura, sp.largura, '+
                  ' sp.local, sp.instalacao, sp.comando, sp.valor, '+
                  ' sum(sp.quantidade)  as quantidade, '+
                  ' sum(sp.quantidade) * sp.valor as mult '+
                  ' from TabPersiana_pp sp '+
                  ' inner join TabpersianaGrupoDiametroCor pc on (sp.PersianaGrupoDiametroCor = pc.codigo)  '+
                  ' inner join TabCor c on (pc.Cor = c.codigo) '+
                  ' inner join tabpersiana p on (pc.persiana = p.codigo) '+
                  ' inner join TabGrupoPersiana gp on (gp.Codigo = pc.GrupoPersiana) '+
                  ' where sp.PedidoProjeto= :pedidoprojeto '+
                  ' group by 1,2,3,4,5,6,7,8,9,10,11,12 ';

      ParamByName('pedidoprojeto').AsString := PpedidoProjeto;
      Open;
      First;
      if (RecordCount = 0)then
        Exit;

      while not(eof) do
      begin
        PreencheStringList(PStrList, Fieldbyname('complemento').asstring);

        if ComValor then
        begin
          STrDados.Add(CompletaPalavra(Fieldbyname('referencia').asstring, 10,' ')+' '+
                       CompletaPalavra(Fieldbyname('Cor').asstring, 15,' ')+' '+
                       CompletaPalavra(Fieldbyname('nome').asstring, 44,' ')+' '+
                       CompletaPalavra(Fieldbyname('quantidade').asstring+' m�',6,' ')+' '+
                       CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valor').asstring), 10,' ')+' '+
                       CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('mult').asstring), 10,' '));
        end
        else begin
          STrDados.Add(CompletaPalavra(Fieldbyname('referencia').asstring, 10,' ')+' '+
                       CompletaPalavra(Fieldbyname('Cor').asstring, 16,' ')+' '+
                       CompletaPalavra(Fieldbyname('nome').asstring, 45,' ')+' '+
                       CompletaPalavra(Fieldbyname('quantidade').asstring+' m�',6,' '));
        end;

        PAlturaLargura:='';

        if (Fieldbyname('altura').asstring<>'') then
          PAlturaLargura:=PAlturaLargura+'ALTURA: '+Fieldbyname('altura').asstring;

        if (Fieldbyname('largura').asstring<>'') then
          PAlturaLargura:=PAlturaLargura+'    LARGURA: '+Fieldbyname('largura').asstring;

        if (PAlturaLargura<>'') then
          StrDados.Add('                            '+PAlturaLargura);

        if (Fieldbyname('local').asstring <>'') then
          STrDados.Add('                            '+'LOCAL: '+CompletaPalavra(Fieldbyname('local').asstring,30,' '));

        if (Fieldbyname('instalacao').asstring<>'') then
          STrDados.Add('                            '+'INSTALA��O: '+CompletaPalavra(Fieldbyname('instalacao').asstring,30,' '));

        if (Fieldbyname('comando').asstring<>'') then
          STrDados.Add('                            '+'COMANDO: '+CompletaPalavra(Fieldbyname('comando').asstring,30,' '));

        for Cont:=0 to PStrList.Count-1 do
        begin
          STrDados.Add('                            '+ CompletaPalavra(PStrList.Strings[Cont],30,' '));
        end;

        Next;
      end;

      if ComValor then
      begin
        close;
        sql.clear;
        sql.add('Select sum(ValorFinal) as SOMA FROM TabPersiana_PP where PedidoProjeto='+PpedidoProjeto);
        open;
        STrDados.Add('?'+CompletaPalavra_a_Esquerda('Total PERSIANAS: '+formata_valor(fieldbyname('soma').asstring),100,' '));
        STrDados.Add(' ');
      end;
    end;
  finally
    FreeAndNil(PStrList);
  end;
end;

end.






