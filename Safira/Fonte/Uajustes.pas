unit Uajustes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,ibquery;

type
  TFajustes = class(TForm)
    Button1: TButton;
    SaveDialog1: TSaveDialog;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fajustes: TFajustes;

implementation

uses UDataModulo, UessencialGlobal, UMostraBarraProgresso,
  UobjPedidoObjetos;

{$R *.dfm}

procedure TFajustes.Button1Click(Sender: TObject);
var
ibquery1,ibquery2:tibquery;
pnome,vetor,vetorcerto:String;
cont1,cont2:integer;
begin
     vetor     :='������������������';
     vetorcerto:='aaaaeeeiiioooouuu�';


     Try
        ibquery1:=TIBQuery.create(nil);
        ibquery2:=TIBQuery.create(nil);

        ibquery1.Database:=FDataModulo.IBDatabase;
        ibquery2.Database:=FDataModulo.IBDatabase;

     Except
           mensagemErro('Erro na tentativa de criar as querys');
     End;
     Try
        With ibquery1 do
        Begin
             close;
             sql.clear;
             sql.add('Select codigo,nome from tabcidade order by codigo');
             open;
             last;
             if (recordcount=0)
             Then Begin
                       mensagemaviso('Nenhuma informa��o foi selecionada');
                       exit;
             End;
             FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
             FMostraBarraProgresso.Lbmensagem.caption:='Acertando nomes das cidades....';
             first;
             While not(eof) do
             Begin
                  FMostraBarraProgresso.IncrementaBarra1(1);
                  FMostraBarraProgresso.show;
                  Application.ProcessMessages;


                  pnome:=fieldbyname('nome').asstring;
                  
                  for cont1:=1 to length(pnome) do
                  Begin
                       for cont2:=1 to length(vetor) do
                       Begin
                              if (vetor[cont2]=pnome[cont1])
                              then pnome[cont1]:=vetorcerto[cont2];
                       End;
                  End;

                  if (pnome<>fieldbyname('nome').asstring)
                  Then Begin
                            ibquery2.close;
                            ibquery2.sql.clear;
                            ibquery2.sql.add('update tabcidade set nome='+#39+uppercase(pnome)+#39+' where codigo='+ibquery1.fieldbyname('codigo').asstring);
                            ibquery2.ExecSQL;
                  End;
                  
                  next;
             End;
             
             FMostraBarraProgresso.close;
             FDataModulo.IBTransaction.CommitRetaining;
             mensagemaviso('concluido');



        End;



     Finally
            FMostraBarraProgresso.close;
            freeandnil(ibquery1);
            freeandnil(ibquery2);
     End;


end;


procedure TFajustes.Button2Click(Sender: TObject);
var
   objpedido:TObjPedidoObjetos;
begin
     Try
        objpedido:=TObjPedidoObjetos.Create(self);
     Except
           on E:exception do
           Begin
                mensagemerro(e.message);
                exit;
           End;
     End;

     Try
        objpedido.refaz_base_calculo_comissao_colocador;
     Finally
            objpedido.free;
     End;
end;

end.
