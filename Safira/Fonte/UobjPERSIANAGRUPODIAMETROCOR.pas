unit UobjPERSIANAGRUPODIAMETROCOR;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJPERSIANA 
,UOBJGRUPOPERSIANA,UOBJCOR,UOBJDIAMETRO;

Type
   TObjPERSIANAGRUPODIAMETROCOR=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Persiana:TOBJPERSIANA ;
                GrupoPersiana:TOBJGRUPOPERSIANA;
                Cor:TOBJCOR;
                Diametro:TOBJDIAMETRO;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaReferencia(Parametro:string) :boolean;
                Function    Localiza_persiana_grupo_diametro_Cor(Ppersiana,Pgrupo,Pdiametro,Pcor:string):boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;

                Procedure Submit_PrecoCusto(parametro: string);
                Function Get_PrecoCusto: string;
                Procedure Submit_PorcentagemInstalado(parametro: string);
                Function Get_PorcentagemInstalado: string;
                Procedure Submit_PorcentagemFornecido(parametro: string);
                Function Get_PorcentagemFornecido: string;

                Procedure Submit_PorcentagemRetirado(parametro: string);
                Function Get_PorcentagemRetirado: string;

                Procedure Submit_classificacaofiscal(parametro: string);
                Function Get_classificacaofiscal: string;

                Function Get_PrecoVendaInstalado: string;

                Function Get_PrecoVendaFornecido: string;

                Function Get_PrecoVendaRetirado: string;

                procedure Submit_Estoque(parametro:String);
                function Get_Estoque:string;

                procedure Submit_EstoqueMinimo(parametro:string);
                function Get_EstoqueMinimo:string;


                procedure EdtPersianaExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtPersianaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtPersianaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;

                procedure EdtGrupoPersianaExit(Sender: TObject;LABELNOME:TLABEL);overload;
                procedure EdtGrupoPersianaExit(Sender: TObject;var EdtCodigo :Tedit;LABELNOME:TLABEL);overload;

                procedure EdtGrupoPersianaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtGrupoPersianaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
                procedure EdtGrupoPersianaKeyDown(Sender: TObject;var EdtCodigo:TEdit; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtGrupoPersianaKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState;LABELNOME: Tlabel; Ppersiana, PCorPersiana: string);overload;


                procedure EdtCorExit(Sender: TObject;LABELNOME:TLABEL);overload;
                procedure EdtCorExit(Sender: TObject;Var EdtCodigo :Tedit ;LABELNOME:TLABEL);overload;
                procedure EdtCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtCorKeyDown(Sender: TObject;Var EdtCodigo:Tedit; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;

                procedure EdtDiametroExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtDiametroKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtDiametroKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel; Ppersiana, Pcor,Pgrupo: string);overload;

                Procedure ResgataPersianaGDC(PPerciana:string);
                Function  CadastraPersianaEmTodasAsCores(PPersiana, PGrupoPersiana, PDiametro:string):Boolean;

                Procedure  AtualizaMargens(PGrupoPersiana:string);
                procedure AtualizaPrecos(PGrupoPersianaGrupoDiametroCor: string);

                procedure EdtCorDePersianaGrupoDiametroKeyDown(Sender: TObject; var PEdtCodigo: TEdit; var Key: Word; Shift: TShiftState;LABELNOME: Tlabel);
                procedure EdtPersianaGrupoDiametroDisponivelNaCorKeyDown(Sender: TObject; var PEdtCodigo: TEdit; PCor: string; var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
                function Get_PersianaGrupoDiametroNaCor(PCor: string): TStringList;
                procedure Relatorioestoque;
                procedure Reajusta(PIndice, Pgrupo: string);
                procedure EdtpersianagrupodiametroCorKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
                function RetornaEstoque: String;

                procedure DiminuiEstoque(quantidade,persianacor:string);
                function AumentaEstoque(quantidade,persianacor:string):Boolean;

         Private
               Objquery:Tibquery;
               ObjqueryEstoque:Tibquery;
               ObjQueryPesquisa:TIBQuery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               PrecoCusto:string;
               PorcentagemInstalado:string;
               PorcentagemFornecido:string;
               PorcentagemRetirado:string;
               classificacaofiscal:string;
               PrecoVendaInstalado:string;
               PrecoVendaFornecido:string;
               PrecoVendaRetirado:string;
               estoque:string;
               EstoqueMinimo:string;
               
               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                function Get_PesquisaGrupoPersiana(ppersiana,Pcorpersiana: string): TStringList;
                function Get_PesquisaDiametroPersiana(ppersiana, Pcorpersiana,Pgrupo: string): TStringList;





   End;


implementation
uses Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  UEscolheCor, UReltxtRDPRINT,rdprint, UPERSIANA;

{ TTabTitulo }


Function  TObjPERSIANAGRUPODIAMETROCOR.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If(FieldByName('Persiana').asstring<>'')
        Then Begin
                 If (Self.Persiana.LocalizaCodigo(FieldByName('Persiana').asstring)=False)
                 Then Begin
                          Messagedlg('Persiana N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Persiana.TabelaparaObjeto;
        End;
        If(FieldByName('GrupoPersiana').asstring<>'')
        Then Begin
                 If (Self.GrupoPersiana.LocalizaCodigo(FieldByName('GrupoPersiana').asstring)=False)
                 Then Begin
                          Messagedlg('GrupoPersiana N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.GrupoPersiana.TabelaparaObjeto;
        End;
        If(FieldByName('Cor').asstring<>'')
        Then Begin
                 If (Self.Cor.LocalizaCodigo(FieldByName('Cor').asstring)=False)
                 Then Begin
                          Messagedlg('Cor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Cor.TabelaparaObjeto;
        End;
        If(FieldByName('Diametro').asstring<>'')
        Then Begin
                 If (Self.Diametro.LocalizaCodigo(FieldByName('Diametro').asstring)=False)
                 Then Begin
                          Messagedlg('Di�metro N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Diametro.TabelaparaObjeto;
        End;

        Self.PrecoCusto:=fieldbyname('PrecoCusto').asstring;
        Self.PorcentagemInstalado:=fieldbyname('PorcentagemInstalado').asstring;
        Self.PorcentagemFornecido:=fieldbyname('PorcentagemFornecido').asstring;
        Self.PorcentagemRetirado:=fieldbyname('PorcentagemRetirado').asstring;
        Self.classificacaofiscal:=fieldbyname('classificacaofiscal').asstring;
        Self.PrecoVendaInstalado:=fieldbyname('PrecoVendaInstalado').asstring;
        Self.PrecoVendaFornecido:=fieldbyname('PrecoVendaFornecido').asstring;
        Self.PrecoVendaRetirado:=fieldbyname('PrecoVendaRetirado').asstring;
        Self.estoque:=fieldbyname('estoque').asstring;
        self.EstoqueMinimo := Objquery.fieldbyname('EstoqueMinimo').AsString;
        result:=True;
     End;
end;


Procedure TObjPERSIANAGRUPODIAMETROCOR.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Persiana').asstring:=Self.Persiana.GET_CODIGO;
        ParamByName('GrupoPersiana').asstring:=Self.GrupoPersiana.GET_CODIGO;
        ParamByName('Cor').asstring:=Self.Cor.GET_CODIGO;
        ParamByName('Diametro').asstring:=Self.Diametro.GET_CODIGO;

        ParamByName('PrecoCusto').asstring:=virgulaparaponto(Self.PrecoCusto);
        ParamByName('PorcentagemInstalado').asstring:=virgulaparaponto(Self.PorcentagemInstalado);
        ParamByName('PorcentagemFornecido').asstring:=virgulaparaponto(Self.PorcentagemFornecido);
        ParamByName('PorcentagemRetirado').asstring:=virgulaparaponto(Self.PorcentagemRetirado);
        ParamByName('classificacaofiscal').asstring:=Self.classificacaofiscal;
        //ParamByName('estoque').AsString:=virgulaparaponto(Self.estoque);
        ParamByName('estoqueminimo').AsString:=virgulaparaponto(Self.EstoqueMinimo);

  End;
End;

//***********************************************************************

function TObjPERSIANAGRUPODIAMETROCOR.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;


  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPERSIANAGRUPODIAMETROCOR.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Persiana.ZerarTabela;
        GrupoPersiana.ZerarTabela;
        Cor.ZerarTabela;
        Diametro.ZerarTabela;

        PrecoCusto:='';
        PorcentagemInstalado:='';
        PorcentagemFornecido:='';
        PorcentagemRetirado:='';
        classificacaofiscal:='';
        PrecoVendaInstalado:='';
        PrecoVendaFornecido:='';
        PrecoVendaRetirado:='';
        Estoque:='';
        EstoqueMinimo := '';
     End;
end;

Function TObjPERSIANAGRUPODIAMETROCOR.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPERSIANAGRUPODIAMETROCOR.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Persiana.LocalizaCodigo(Self.Persiana.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Persiana n�o Encontrado!';
      If (Self.GrupoPersiana.LocalizaCodigo(Self.GrupoPersiana.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ GrupoPersiana n�o Encontrado!';
      If (Self.Cor.LocalizaCodigo(Self.Cor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Cor n�o Encontrado!';
      If (Self.Diametro.LocalizaCodigo(Self.Diametro.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Di�metro n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjPERSIANAGRUPODIAMETROCOR.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.Persiana.Get_Codigo<>'')
        Then Strtoint(Self.Persiana.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Persiana';
     End;
     try
        If (Self.GrupoPersiana.Get_Codigo<>'')
        Then Strtoint(Self.GrupoPersiana.Get_Codigo);
     Except
           Mensagem:=mensagem+'/GrupoPersiana';
     End;
     try
        If (Self.Cor.Get_Codigo<>'')
        Then Strtoint(Self.Cor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Cor';
     End;
     try
        If (Self.Diametro.Get_Codigo<>'')
        Then Strtoint(Self.Diametro.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Di�metro';
     End;
     try
        Strtofloat(Self.PrecoCusto);
     Except
           Mensagem:=mensagem+'/PrecoCusto';
     End;
     try
        Strtofloat(Self.PorcentagemInstalado);
     Except
           Mensagem:=mensagem+'/PorcentagemInstalado';
     End;
     try
        Strtofloat(Self.PorcentagemFornecido);
     Except
           Mensagem:=mensagem+'/PorcentagemFornecido';
     End;
     try
        Strtofloat(Self.PorcentagemRetirado);
     Except
           Mensagem:=mensagem+'/PorcentagemRetirado';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPERSIANAGRUPODIAMETROCOR.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPERSIANAGRUPODIAMETROCOR.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPERSIANAGRUPODIAMETROCOR.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PERSIANAGRUPODIAMETROCOR vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Persiana,GrupoPersiana,Cor,Diametro,PrecoCusto');
           SQL.ADD(' ,PorcentagemInstalado,PorcentagemFornecido,PorcentagemRetirado');
           SQL.ADD(' ,PrecoVendaInstalado,PrecoVendaFornecido,PrecoVendaRetirado');
           sql.Add(',classificacaofiscal,estoque,EstoqueMinimo');
           SQL.ADD(' from  TABPERSIANAGRUPODIAMETROCOR');
           SQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPERSIANAGRUPODIAMETROCOR.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPERSIANAGRUPODIAMETROCOR.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPERSIANAGRUPODIAMETROCOR.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryEstoque:=TIBQuery.create(nil);
        Self.ObjqueryEstoque.Database:=FDataModulo.IbDatabase;

        Self.ParametroPesquisa:=TStringList.create;


        Self.ObjQueryPesquisa:=TIBQuery.Create(nil);
        Self.ObjQueryPesquisa.Database:=FDataModulo.IBDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjQueryPesquisa;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Persiana:=TOBJPERSIANA .create;
        Self.GrupoPersiana:=TOBJGRUPOPERSIANA.create;
        Self.Cor:=TOBJCOR.create;
        Self.Diametro:=TOBJDIAMETRO.create;
//CODIFICA CRIACAO DE OBJETOS





        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABPERSIANAGRUPODIAMETROCOR(Codigo,Persiana');
                InsertSQL.add(' ,GrupoPersiana,Cor,Diametro,PrecoCusto,PorcentagemInstalado');
                InsertSQL.add(' ,PorcentagemFornecido,PorcentagemRetirado,classificacaofiscal');//,estoque');
                InsertSql.Add(',EstoqueMinimo');
                InsertSql.Add(' )');
                InsertSQL.add('values (:Codigo,:Persiana,:GrupoPersiana,:Cor,:Diametro');
                InsertSQL.add(' ,:PrecoCusto,:PorcentagemInstalado,:PorcentagemFornecido');
                InsertSQL.add(' ,:PorcentagemRetirado,:classificacaofiscal');//,:estoque');
                InsertSql.Add(',:EstoqueMinimo');
                InsertSql.Add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABPERSIANAGRUPODIAMETROCOR set Codigo=:Codigo');
                ModifySQL.add(',Persiana=:Persiana,GrupoPersiana=:GrupoPersiana,Cor=:Cor');
                ModifySQL.add(',Diametro=:Diametro,PrecoCusto=:PrecoCusto');
                ModifySQL.add(',PorcentagemInstalado=:PorcentagemInstalado,PorcentagemFornecido=:PorcentagemFornecido');
                ModifySQL.add(',PorcentagemRetirado=:PorcentagemRetirado,classificacaofiscal=:classificacaofiscal');
                //ModifySQL.add(',estoque=:estoque');
                ModifySQL.add(', EstoqueMinimo=:EstoqueMinimo');
                ModifySQl.Add(' where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABPERSIANAGRUPODIAMETROCOR where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPERSIANAGRUPODIAMETROCOR.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPERSIANAGRUPODIAMETROCOR.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewPERSIANAGRUPODIAMETROCOR');
     Result:=Self.ParametroPesquisa;
end;

function TObjPERSIANAGRUPODIAMETROCOR.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de PERSIANAGRUPODIAMETROCOR ';
end;


function TObjPERSIANAGRUPODIAMETROCOR.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPERSIANAGRUPODIAMETROCOR,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENPERSIANAGRUPODIAMETROCOR,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPERSIANAGRUPODIAMETROCOR.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ObjqueryEstoque);
    Freeandnil(Self.ObjqueryPesquisa);
    Freeandnil(Self.ObjDataSource);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Persiana.FREE;
    Self.GrupoPersiana.FREE;
    Self.Cor.FREE;
    Self.Diametro.FREE;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPERSIANAGRUPODIAMETROCOR.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPERSIANAGRUPODIAMETROCOR.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPersianaGrupoDiametroCor.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjPersianaGrupoDiametroCor.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjPersianaGrupoDiametroCor.Submit_PrecoCusto(parametro: string);
begin
        Self.PrecoCusto:=Parametro;
end;
function TObjPersianaGrupoDiametroCor.Get_PrecoCusto: string;
begin
        Result:=Self.PrecoCusto;
end;
procedure TObjPersianaGrupoDiametroCor.Submit_PorcentagemInstalado(parametro: string);
begin
        Self.PorcentagemInstalado:=Parametro;
end;
function TObjPersianaGrupoDiametroCor.Get_PorcentagemInstalado: string;
begin
        Result:=Self.PorcentagemInstalado;
end;
procedure TObjPersianaGrupoDiametroCor.Submit_PorcentagemFornecido(parametro: string);
begin
        Self.PorcentagemFornecido:=Parametro;
end;
function TObjPersianaGrupoDiametroCor.Get_PorcentagemFornecido: string;
begin
        Result:=Self.PorcentagemFornecido;
end;
procedure TObjPersianaGrupoDiametroCor.Submit_PorcentagemRetirado(parametro: string);
begin
        Self.PorcentagemRetirado:=Parametro;
end;
function TObjPersianaGrupoDiametroCor.Get_PorcentagemRetirado: string;
begin
        Result:=Self.PorcentagemRetirado;
end;



procedure TObjPersianaGrupoDiametroCor.Submit_classificacaofiscal(parametro: string);
begin
        Self.classificacaofiscal:=Parametro;
end;
function TObjPersianaGrupoDiametroCor.Get_classificacaofiscal: string;
begin
        Result:=Self.classificacaofiscal;
end;

function TObjPersianaGrupoDiametroCor.Get_PrecoVendaInstalado: string;
begin
        Result:=Self.PrecoVendaInstalado;
end;
function TObjPersianaGrupoDiametroCor.Get_PrecoVendaFornecido: string;
begin
        Result:=Self.PrecoVendaFornecido;
end;
function TObjPersianaGrupoDiametroCor.Get_PrecoVendaRetirado: string;
begin
        Result:=Self.PrecoVendaRetirado;
end;
//CODIFICA GETSESUBMITS


procedure TObjPERSIANAGRUPODIAMETROCOR.EdtPersianaExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Persiana.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Persiana.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Persiana.GET_NOME;
End;
procedure TObjPERSIANAGRUPODIAMETROCOR.EdtPersianaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
  FpesquisaLocal:Tfpesquisa;
  FPersianaLocal: TFPERSIANA;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPersianaLocal := TFPERSIANA.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabpersiana where ativo = ''S'' ',Self.Persiana.Get_TituloPesquisa,FPersianaLocal)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Persiana.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Persiana.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Persiana.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeandNil(FPersianaLocal);
     End;
end;

procedure TObjPERSIANAGRUPODIAMETROCOR.EdtPersianaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Persiana.Get_Pesquisa,Self.Persiana.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Persiana.RETORNACAMPOCODIGO).asstring;

                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPERSIANAGRUPODIAMETROCOR.EdtGrupoPersianaExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.GrupoPersiana.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.GrupoPersiana.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.GrupoPersiana.GET_NOME;
End;
procedure TObjPERSIANAGRUPODIAMETROCOR.EdtGrupoPersianaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
  FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.GrupoPersiana.Get_Pesquisa,Self.GrupoPersiana.Get_TituloPesquisa,NIl)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoPersiana.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.GrupoPersiana.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoPersiana.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPERSIANAGRUPODIAMETROCOR.EdtGrupoPersianaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.GrupoPersiana.Get_Pesquisa,Self.GrupoPersiana.Get_TituloPesquisa,NIl)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoPersiana.RETORNACAMPOCODIGO).asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPERSIANAGRUPODIAMETROCOR.EdtCorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Cor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Cor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Cor.Get_Descricao;
End;
procedure TObjPERSIANAGRUPODIAMETROCOR.EdtCorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cor.Get_Pesquisa,Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjPERSIANAGRUPODIAMETROCOR.EdtDiametroExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Diametro.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Diametro.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Diametro.Get_Codigo;
End;
procedure TObjPERSIANAGRUPODIAMETROCOR.EdtDiametroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Diametro.Get_Pesquisa,Self.Diametro.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Diametro').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Diametro.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Diametro.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjPERSIANAGRUPODIAMETROCOR.EdtDiametroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel;Ppersiana,Pcor,Pgrupo:string);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisadiametropersiana(Ppersiana,Pcor,Pgrupo),Self.Diametro.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Diametro.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Diametro.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

//CODIFICA EXITONKEYDOWN




procedure TObjPERSIANAGRUPODIAMETROCOR.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPERSIANAGRUPODIAMETROCOR';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;
          End;
     end;

end;



procedure TObjPERSIANAGRUPODIAMETROCOR.ResgataPersianaGDC(
  PPerciana: string);
begin
     With  ObjQueryPesquisa   do
     Begin
          Close;
          SQL.Clear;
          Sql.Add('Select  TabPersiana.Referencia, TabPersiana.Nome,');
          Sql.Add('TabGrupoPersiana.NOme as Grupo,');
          Sql.Add('TabCor.Descricao as Cor,');
          Sql.Add('TabDiametro.Codigo as Diametro,');
          Sql.Add('TPGDC.PrecoCusto,');
          Sql.Add('TPGDC.PorcentagemInstalado,');
          Sql.Add('TPGDC.PorcentagemFornecido,');
          Sql.Add('TPGDC.PorcentagemRetirado,');
          Sql.Add('TPGDC.PrecoVendaInstalado,');
          Sql.Add('TPGDC.PrecoVendaFornecido,');
          Sql.Add('TPGDC.PrecoVendaRetirado,');
          Sql.Add('TPGDC.COdigo,');
          Sql.Add('TPGDC.classificacaofiscal,TPGDC.estoque');
          Sql.Add('from TabPersianaGrupoDiametroCor TPGDC');
          Sql.Add('Join TabPersiana on TabPersiana.Codigo = TPGDC.Persiana');
          Sql.Add('Join TabGrupoPersiana on TabGrupoPersiana.Codigo = TPGDC.GrupoPersiana');
          Sql.Add('Join TabCor on TabCor.Codigo = TPGDC.Cor');
          Sql.Add('Join TabDiametro on TabDiametro.Codigo = TPGDC.Diametro');
          Sql.Add('Where TPGDC.Persiana = '+PPerciana);
          if (PPerciana = '')then
          exit;

          Open;
     end;
end;
procedure TObjPERSIANAGRUPODIAMETROCOR.EdtGrupoPersianaKeyDown(Sender: TObject; var EdtCodigo: TEdit; var Key: Word; Shift: TShiftState;
  LABELNOME: Tlabel);
var
  FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.GrupoPersiana.Get_Pesquisa,Self.GrupoPersiana.Get_TituloPesquisa,NIl)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 EdtCodigo.Text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.GrupoPersiana.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoPersiana.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjPERSIANAGRUPODIAMETROCOR.EdtGrupoPersianaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState;LABELNOME: Tlabel;Ppersiana:string;PCorPersiana:string);
var
  FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaGrupoPersiana(Ppersiana,PCorPersiana),Self.GrupoPersiana.Get_TituloPesquisa,NIl)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.GrupoPersiana.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.GrupoPersiana.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPERSIANAGRUPODIAMETROCOR.EdtGrupoPersianaExit(
  Sender: TObject; var EdtCodigo: Tedit; LABELNOME: TLABEL);
begin
     labelnome.caption:='';
     If (TEdit(Sender).text='')
     Then exit;

     If (Self.GrupoPersiana.LocalizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               EdtCodigo.Text:='';
               exit;
     End;
     Self.GrupoPersiana.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.GrupoPersiana.GET_NOME;
     TEdit(Sender).Text:=Self.GrupoPersiana.Get_Referencia;
     EdtCodigo.Text:=Self.GrupoPersiana.Get_Codigo;
end;

procedure TObjPERSIANAGRUPODIAMETROCOR.EdtCorExit(Sender: TObject;
  var EdtCodigo: Tedit; LABELNOME: TLABEL);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Cor.LocalizaReferencia(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               EdtCodigo.Text:='';
               exit;
     End;
     Self.Cor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Cor.Get_Descricao;
     EdtCodigo.Text:=Self.Cor.Get_Codigo;
     TEdit(Sender).Text:=Self.Cor.Get_Referencia;

end;

procedure TObjPERSIANAGRUPODIAMETROCOR.EdtCorKeyDown(Sender: TObject;
  var EdtCodigo: Tedit; var Key: Word; Shift: TShiftState;
  LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cor.Get_Pesquisa,Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 EdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;                                 
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjPERSIANAGRUPODIAMETROCOR.LocalizaReferencia(
  Parametro: string): boolean;
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PERSIANAGRUPODIAMETROCOR vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Persiana,GrupoPersiana,Cor,Diametro,PrecoCusto');
           SQL.ADD(' ,PorcentagemInstalado,PorcentagemFornecido,PorcentagemRetirado');
           SQL.ADD(' ,PrecoVendaInstalado,PrecoVendaFornecido,PrecoVendaRetirado');
           sql.add(',classificacaofiscal,estoque,EstoqueMinimo');
           SQL.ADD(' from  TABPERSIANAGRUPODIAMETROCOR');
           SQL.ADD(' WHERE Referencia='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjPERSIANAGRUPODIAMETROCOR.Localiza_persiana_grupo_diametro_Cor(
  Ppersiana, Pgrupo, Pdiametro, Pcor: string): boolean;
begin
       if (ppersiana='')
       or (pgrupo='')
       or (pdiametro='')
       or (pcor='')
       Then Begin
                 Messagedlg('Escolha todos os dados da persiana',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Persiana,GrupoPersiana,Cor,Diametro,PrecoCusto');
           SQL.ADD(' ,PorcentagemInstalado,PorcentagemFornecido,PorcentagemRetirado');
           SQL.ADD(' ,PrecoVendaInstalado,PrecoVendaFornecido,PrecoVendaRetirado');
           sql.Add(',classificacaofiscal,estoque,EstoqueMinimo');
           SQL.ADD(' from  TABPERSIANAGRUPODIAMETROCOR');
           SQL.ADD(' WHERE Persiana='+ppersiana);
           SQL.ADD(' and GrupoPersiana='+Pgrupo);
           SQL.ADD(' and Cor='+Pcor);
           SQL.ADD(' and Diametro='+Pdiametro);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjPERSIANAGRUPODIAMETROCOR.CadastraPersianaEmTodasAsCores(
  PPersiana, PGrupoPersiana, PDiametro: string): Boolean;
Var   Cont : Integer;
      PPOrcentagemAcrescimo:string;
begin
     Result:=false;
     if (Self.Persiana.LocalizaCodigo(PPersiana)=false)then
     Begin
          MensagemErro('Persiana n�o encontrado.');
          Result:=false;
          exit;
     end;

     With  Self.ObjqueryPesquisa  do
     Begin
          Close;
          SQl.Clear;
          Sql.Add('Select Codigo,Referencia, Descricao from TabCor');
          Open;
          First;

          FEscolheCor.CheckListBoxCor.Clear;
          FEscolheCor.CheckListBoxCodigoCor.Clear;
          While not (Eof) do
          Begin
               FEscolheCor.CheckListBoxCor.Items.Add(fieldbyname('Referencia').AsString+' - '+fieldbyname('Descricao').AsString);
               FEscolheCor.CheckListBoxCodigoCor.Items.Add(fieldbyname('Codigo').AsString);
          Next;
          end;
          FEscolheCor.ShowModal;

          if FEscolheCor.Tag = 0 then
          Begin
               Result:=false;
               exit;
          end;

          // Cadastrando os cores
          for Cont:=0 to FEscolheCor.CheckListBoxCor.Items.Count-1 do
          Begin
               if FEscolheCor.CheckListBoxCodigoCor.Checked[Cont]=true then
               Begin
                    Self.Cor.LocalizaCodigo(FEscolheCor.CheckListBoxCodigoCor.Items.Strings[Cont]);
                    Self.Cor.TabelaparaObjeto;
                    PPOrcentagemAcrescimo:=Self.Cor.Get_PorcentagemAcrescimo;

                    Self.ZerarTabela;
                    Self.Status:=dsInsert;
                    Self.Submit_Codigo(Get_NovoCodigo);
                    Self.Persiana.Submit_Codigo(PPersiana);
                    Self.GrupoPersiana.Submit_Codigo(PGrupoPersiana);
                    Self.Diametro.Submit_Codigo(PDiametro);
                    Self.Cor.Submit_Codigo(FEscolheCor.CheckListBoxCodigoCor.Items.Strings[Cont]);
                    Self.Submit_PrecoCusto('0');
                    Self.Submit_PorcentagemInstalado('0');
                    Self.Submit_PorcentagemFornecido('0');
                    Self.Submit_PorcentagemRetirado('0');
                    Self.Submit_classificacaofiscal('');


                    if (Self.Salvar(false)=false)then
                    Begin
                         MensagemErro('Erro ao tentar Salvar as Persianas nas cores escolhidas');
                         Result:=false;
                         exit;
                    end;
               end;
          end;

          Result:=true;
     end;
end;

procedure TObjPERSIANAGRUPODIAMETROCOR.AtualizaMargens(
  PGrupoPersiana: string);
Var QueryLOcal:TIBquery;
    PPorcentagemInstalado:string;
    PPorcentagemRetirado:string;
    PPorcentagemFornecido:string;

begin

      if (PGrupoPersiana='')then
      Begin
           MensagemErro('Escolha um Grupo de Persiana');
           exit;
      end;

      if (Self.GrupoPersiana.LocalizaCodigo(PGrupoPersiana)=false)then
      Begin
          MensagemErro('Grupo de Persiana n�o encontrado');
          exit;
      end;
      Self.GrupoPersiana.TabelaparaObjeto;
      PPorcentagemInstalado:=Self.GrupoPersiana.Get_PorcentagemInstalado;
      PPorcentagemRetirado:=Self.GrupoPersiana.Get_PorcentagemRetirado;
      PPorcentagemFornecido:=Self.GrupoPersiana.Get_PorcentagemFornecido;

      try
            try
                  QueryLOcal:=TIBQuery.Create(nil);
                  QueryLOcal.Database:=FDataModulo.IBDatabase;
            except;
                 MensagemErro('Erro ao tenar criar a QueryLocal');
                 exit;
            end;

            With QueryLOcal  do
            Begin
                 Close;
                 Sql.Clear;
                 Sql.Add('Select TabPersianaGrupoDiametroCor.Codigo from TabPersianaGrupoDiametroCor');
                 Sql.Add('Where GrupoPersiana = '+PGrupoPersiana);
                 Open;
                 First;

                 // Para cada PersianaGrupoDiametroCor desse grupo eu Altero o Valor das margns
                 While not (eof) do
                 Begin
                      Self.ZerarTabela;
                      if (Self.LocalizaCodigo(fieldbyname('Codigo').AsString)=false)then
                      Begin
                           MensagemErro('Persiana n�o encontrada.');
                           exit;
                      end;

                      Self.TabelaparaObjeto;
                      Self.Status:=dsEdit;
                      Self.Submit_PorcentagemInstalado(PPorcentagemInstalado);
                      Self.Submit_PorcentagemFornecido(PPorcentagemFornecido);
                      Self.Submit_PorcentagemRetirado(PPorcentagemRetirado);
                      if (Self.Salvar(false)=false)then
                      begin
                          MensagemErro('Erro ao tentar Atualiza as margens na Tabela de Ferragens');
                          exit;
                      end;
                      Next;
                 end;
            end;


            FDataModulo.IBTransaction.CommitRetaining;
            MensagemAviso('Margem Atualiada com Sucesso !');


      finally
        FreeAndNil(QueryLOcal);
      end;
end;


procedure TObjPersianaGrupoDiametroCor.AtualizaPrecos(PGrupoPersianaGrupoDiametroCor: string);
Var QueryLOcal :TIBQuery; PVAlorCusto, PPorcentagemReajuste:Currency;
begin

      if (PGrupoPersianaGrupoDiametroCor = '')then
      Begin
           MensagemErro('Pesquise o um grupo de PersianaGrupoDiametroCor entes Atualizar os pre�os.');
           exit;
      end;

      if MessageDlg('Tem certea que deseja alterar os valores dos PRE�OS DE CUSTO de '+#13+
                    'todas as Persianas pertencentes e este grupo?. Lembrando que esse processo � irrevers�vel.', mtConfirmation, [mbYes, mbNo], 0) = mrNo
      then Begin
           exit;
      end;

      With FfiltroImp do
      Begin
           DesativaGrupos;
           Grupo01.Enabled:=true;
           LbGrupo01.Caption:='Acr�scimo em (%)';
           edtgrupo01.Enabled:=True;

           ShowModal;

           if (Tag = 0)then
           Exit;

           try
                PPorcentagemReajuste:=StrToCurr(edtgrupo01.Text);
           except
                MensagemErro('Valor inv�lido.');
                exit;
           end;
      end;

      try
            try
                 QueryLocal:=TIBQuery.Create(nil);
                 QueryLOcal.Database:=FDataModulo.IBDatabase;
            except
                 MensagemErro('Erro ao tentar criar a Query Local');
                 exit;
            end;

            With  QueryLOcal do
            Begin
                 Close;
                 Sql.Clear;
                 Sql.Add('Select Codigo from TabPersianaGrupoDiametroCor Where GrupoPersiana = '+PGrupoPersianaGrupoDiametroCor);
                 Open;
                 First;

                 if (RecordCount = 0)then
                 Begin
                      MensagemErro('Nenhuma Persiana encotrada para este grupo.');
                      exit;
                 end;


                 While Not (eof) do
                 Begin
                      PVAlorCusto:=0;
                      Self.ZerarTabela;
                      Self.LocalizaCodigo(fieldbyname('Codigo').AsString);
                      Self.TabelaparaObjeto;
                      PVAlorCusto:=StrToCurr(Self.Get_PrecoCusto);

                      Self.Status:=dsEdit;
                      Self.Submit_PrecoCusto(CurrToStr(PVAlorCusto+(PVAlorCusto*(PPorcentagemReajuste/100))));
                      try
                           Self.Salvar(false);
                      except
                           MensagemErro('Erro ao tentar Salvar o novo pre�o.');
                           FDataModulo.IBTransaction.RollbackRetaining;
                           exit;
                      end;
                 Next;
                 end;

                 FDataModulo.IBTransaction.CommitRetaining;
                 MensagemAviso('Pre�o de custo de persianas atualizado com Sucesso!');
            end;

      finally
            FreeAndNil(QueryLOcal);
      end;

end;

procedure TObjPersianaGrupoDiametroCOR.EdtCorDePersianaGrupoDiametroKeyDown(Sender: TObject; var PEdtCodigo: TEdit; var Key: Word; Shift: TShiftState;
  LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Cor.Get_PesquisaCorPersianaGrupoDiametro,Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPersianaGrupoDiametroCOR.EdtPersianaGrupoDiametroDisponivelNaCorKeyDown(
  Sender: TObject; var PEdtCodigo: TEdit; PCor: string; var Key: Word;
  Shift: TShiftState; LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PersianaGrupoDiametroNaCor(PCor),Self.Cor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Referencia').asstring;
                                 PEdtCodigo.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Cor.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjPersianaGrupoDiametroCOR.Get_PersianaGrupoDiametroNaCor(PCor: string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select Distinct(TabPersiana.Referencia),TabPersiana.NOme, TabGrupoPersiana.Referencia as Ref_Grupo,');
     Self.ParametroPesquisa.add('TabGrupoPersiana.NOme as Grupo, TabPersianaGrupoDiametroCor.Codigo,TabPersianaGrupoDiametroCor.estoque');
     Self.ParametroPesquisa.add('from TabPersianaGrupoDiametroCor');
     Self.ParametroPesquisa.add('Join TabPersiana on TabPersiana.Codigo = TabPersianaGrupoDiametroCor.Persiana');
     Self.ParametroPesquisa.add('Join TabGrupoPersiana on TabGrupoPersiana.Codigo = TabPersianaGrupoDiametroCor.GrupoPersiana');

     if (PCor <> '')then
     Self.ParametroPesquisa.add('Where TabPersianaGrupoDiametroCor.Cor = '+PCor);

     Result:=Self.ParametroPesquisa;

end;



procedure TObjpersianagrupoDiametroCOR.Relatorioestoque;
var
Pgrupo:string;
PsomaEstoque:integer;
PsomaValorTotal:Currency;
persiana:string;
pData:string;
begin
     With FfiltroImp do
     begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.caption:='Grupo Persiana';
          edtgrupo01.OnKeyDown:=Self.EdtGrupopersianaKeyDown;
          edtgrupo01.Color:=$005CADFE;

          Grupo02.Enabled:=True;
          LbGrupo02.Caption:='Persiana';
          edtgrupo02.OnKeyDown:=Self.EdtPersianaKeyDown;
          edtgrupo02.Color:=$005CADFE;

          Grupo03.Enabled:=True;
          edtgrupo03.EditMask:=MascaraData;
          LbGrupo03.caption:='Data Limite';

          Showmodal;

          if (tag=0)
          then exit;

          pGrupo:='';
          if (edtgrupo01.Text<>'')
          Then Begin
                    if (self.Grupopersiana.LocalizaCodigo(edtgrupo01.text)=False)
                    Then Begin
                              MensagemErro('Grupo n�o encontrado');
                              exit;
                    End;
                    self.Grupopersiana.TabelaparaObjeto;
                    pgrupo:=self.Grupopersiana.get_codigo;
          End;
          persiana:='';
          if(edtgrupo02.Text<>'')
          then persiana:=edtgrupo02.Text;

          pdata:='';
          if(edtgrupo03.Text<>'  /  /    ')
          then pdata:=edtgrupo03.Text;
     End;

     With Self.ObjQueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select');
          sql.add('Tabpersiana.codigo as persiana,');
          sql.add('Tabpersiana.REferencia as REFpersiana,');
          sql.add('Tabpersiana.nome as NOMEpersiana,');
          sql.add('Tabcor.codigo as COR,');
          sql.add('Tabcor.Referencia as REFCOR,');
          sql.add('tabcor.descricao as NOMECOR,');
          sql.add('tabpersianagrupodiametrocor.precocusto,');
          sql.add('coalesce(sum(TabEstoque.quantidade),0) as estoque,');//alterado aqui
          sql.add('(coalesce(sum(TabEstoque.quantidade),0)*tabpersianagrupodiametrocor.precocusto) as VALORTOTAL');
          sql.add('from tabpersianagrupodiametrocor');
          sql.add('join tabpersiana on tabpersianagrupodiametrocor.persiana=tabpersiana.codigo');
          sql.add('join tabcor on Tabpersianagrupodiametrocor.cor=tabcor.codigo');
          if (pdata<>'')
          then Begin
                    sql.add('left join tabestoque on (tabestoque.persianagrupodiametrocor=tabpersianagrupodiametrocor.codigo');
                    sql.add('and tabestoque.data<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(pdata))+#39+')');

          End
          Else sql.add('left join tabestoque on tabestoque.persianagrupodiametrocor=tabpersianagrupodiametrocor.codigo');

          sql.Add('where tabpersiana.codigo<>-50');

          if (Pgrupo<>'')
          Then Sql.add('and Tabpersianagrupodiametrocor.Grupopersiana='+pgrupo);

          if(persiana<>'')
          then SQL.Add('and tabpersiana.codigo='+persiana);

          sql.add('group by tabpersiana.codigo,');
          sql.add('tabpersiana.REferencia,');
          sql.add('tabpersiana.nome,');
          sql.add('Tabcor.codigo,');
          sql.add('Tabcor.Referencia,');
          sql.add('tabcor.descricao,');
          sql.add('tabpersianagrupodiametrocor.precocusto');

          Sql.add('order by Tabpersiana.referencia,tabcor.referencia');

          open;

          if (Recordcount=0)
          then Begin
                    MensagemAviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;

          With FreltxtRDPRINT do
          begin
               ConfiguraImpressao;
               RDprint.FonteTamanhoPadrao:=S17cpp;
               RDprint.TamanhoQteColunas:=130;
               RDprint.Abrir;
               if (RDprint.Setup=False)
               then begin
                         RDprint.Fechar;
                         exit;
               end;
               LinhaLocal:=3;

               RDprint.ImpC(linhalocal,65,'RELAT�RIO DE ESTOQUE  - PERSIANA',[negrito]);
               IncrementaLinha(2);

               if (Pgrupo<>'')
               then begin
                         RDprint.ImpF(linhalocal,1,'Grupo: '+Self.Grupopersiana.Get_Codigo+'-'+Self.Grupopersiana.Get_Nome,[negrito]);
                         IncrementaLinha(2);
               End;

               VerificaLinha;
               RDprint.Impf(linhalocal,1,CompletaPalavra('CODIGO',6,' ')+' '+
                                             CompletaPalavra('REF.',10,' ')+' '+
                                             CompletaPalavra('NOME PERSIANA',42,' ')+' '+
                                             CompletaPalavra('REF.COR',10,' ')+' '+
                                             CompletaPalavra('NOME COR',19,' ')+' '+
                                             CompletaPalavra_a_Esquerda('ESTOQUE',12,' ')+' '+
                                             CompletaPalavra_a_Esquerda('CUSTO',12,' ')+' '+
                                             CompletaPalavra_a_Esquerda('TOTAL',12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               IncrementaLinha(1);

               PsomaEstoque:=0;
               PsomaValorTotal:=0;

               While not(self.ObjQueryPesquisa.eof) do
               Begin
                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('persiana').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('refpersiana').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('nomepersiana').asstring,42,' ')+' '+
                                             CompletaPalavra(fieldbyname('refcor').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('nomecor').asstring,19,' ')+' '+
                                             CompletaPalavra_a_Esquerda(fieldbyname('estoque').asstring,12,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('precocusto').asstring),12,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valortotal').asstring),12,' '));
                    IncrementaLinha(1);

                    PsomaEstoque:=PsomaEstoque+Fieldbyname('estoque').asinteger;
                    PsomaValorTotal:=PsomaValorTotal+Fieldbyname('valortotal').asfloat;


                    self.ObjQueryPesquisa.next;
               End;
               DesenhaLinha;
               VerificaLinha;
               RDprint.Impf(linhalocal,93,CompletaPalavra_a_Esquerda(inttostr(pSomaEstoque),12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(formata_valor(psomavalortotal),12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               RDprint.fechar;
          End;
     End;


end;



procedure TObjPERSIANAGRUPODIAMETROCOR.Reajusta(PIndice, Pgrupo: string);
var
   Psinal:string;
begin
  try
    strtofloat(Pindice);

    if (strtofloat(Pindice)>=0) then
      Psinal:='+'
    else
    begin
      Pindice:=floattostr(strtofloat(Pindice)*-1);
      Psinal:='-';
    End;
  except
    mensagemerro('Valor Inv�lido no �ndice');
    exit;
  End;

  if (Pgrupo<>'') then
  begin
    try
      strtoint(Pgrupo);
      if (Self.GrupoPersiana.LocalizaCodigo(pgrupo)=False) then
      begin
        MensagemErro('Grupo de Persiana n�o encontrado');
        exit;
      End;
    Except
      mensagemerro('Grupo Inv�lido');
      exit;
    end;
  End;

  With Self.Objquery do
  Begin
    close;
    SQL.clear;
    SQL.add('Update Tabpersianagrupodiametrocor Set PRECOCUSTO=PRECOCUSTO'+psinal+'((PRECOCUSTO*'+virgulaparaponto(Pindice)+')/100)');
    SQl.Add('precovendainstalado = precocusto + ( precocusto * porcentageminstalado/100),');
    SQl.Add('precovendafornecido = precocusto + ( precocusto * porcentagemfornecido/100),');
    SQl.Add('precovendaretirado = precocusto + ( precocusto * porcentagemretirado/100)');

    if (Pgrupo<>'') then
      SQL.add('Where Grupopersiana='+Pgrupo);

    try
      execsql;
      FDataModulo.IBTransaction.CommitRetaining;
      mensagemaviso('Conclu�do');
    Except
      on e:exception do
      begin
        FDataModulo.IBTransaction.RollbackRetaining;
        mensagemerro('Erro na tentativa de Reajustar o valor da Persiana'+#13+E.message);
        exit;
      End;
    End;
  End;
end;

function TObjPERSIANAGRUPODIAMETROCOR.Get_PesquisaGrupoPersiana(ppersiana,Pcorpersiana:string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select TabGRUPOPERSIANA.* from TabGRUPOPERSIANA');
     Self.ParametroPesquisa.add('join tabpersianagrupodiametrocor on TabpersianaGrupoDiametroCor.Grupopersiana=tabgrupopersiana.codigo');
     Self.ParametroPesquisa.add('where tabpersianagrupodiametrocor.persiana='+ppersiana);
     Self.ParametroPesquisa.add('and tabpersianagrupodiametrocor.cor='+pcorpersiana);
     Result:=Self.ParametroPesquisa;
end;


function TObjPERSIANAGRUPODIAMETROCOR.Get_PesquisaDiametroPersiana(ppersiana,Pcorpersiana,Pgrupo:string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select TABDIAMETRO.* from TABDIAMETRO');
     Self.ParametroPesquisa.add('join tabpersianagrupodiametrocor on TabpersianaGrupoDiametroCor.diametro=TABDIAMETRO.codigo');
     Self.ParametroPesquisa.add('where tabpersianagrupodiametrocor.persiana='+ppersiana);
     Self.ParametroPesquisa.add('and tabpersianagrupodiametrocor.cor='+pcorpersiana);
     Self.ParametroPesquisa.add('and tabpersianagrupodiametrocor.grupopersiana='+Pgrupo);
     Result:=Self.ParametroPesquisa;
end;



procedure TObjpersianagrupodiametroCOR.EdtpersianagrupodiametroCorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,Self.Get_TituloPesquisa,Nil)=True)
            Then Begin                                                                         
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                            if (Self.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring))
                                            Then Begin
                                                      Self.TabelaparaObjeto;
                                                      LABELNOME.caption:=completapalavra(Self.Persiana.Get_Nome,50,' ')+' GRUPO: ' +Self.GrupoPersiana.Get_Nome+' COR: '+Self.Cor.Get_Descricao;
                                            End;

                                 End;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;



function TObjpersianagrupodiametroCOR.RetornaEstoque:String;
begin
        With Self.ObjQueryEstoque do
        begin
             close;
             sql.clear;
             sql.add('Select coalesce(sum(quantidade),0) as estoque from tabestoque where persianagrupodiametrocor='+Self.Codigo);
             //SQL.Add('Select estoque from tabpersianagrupodiametrocor where codigo='+self.Codigo);
             open;
             result:=fieldbyname('estoque').asstring;
        End;
end;

procedure TObjPERSIANAGRUPODIAMETROCOR.Submit_Estoque(parametro:string);
begin
    self.estoque:=parametro;
end;

function TObjPERSIANAGRUPODIAMETROCOR.Get_Estoque:string;
begin
    Result:=self.estoque;
end;

procedure TObjPERSIANAGRUPODIAMETROCOR.DiminuiEstoque(quantidade,persianacor:string);
var
   ObjqueryEstoque:TIBQuery;
begin
   try
          ObjqueryEstoque:=TIBQuery.Create(nil);
          ObjqueryEstoque.Database:=FDataModulo.IBDatabase;
          with ObjqueryEstoque do
          begin
                Close;
                sql.Clear;
                sql.Add('update TabPersianaGrupoDiametroCor set estoque=estoque+'+quantidade);
                SQL.Add('where codigo='+persianacor);
                ExecSQL;
          end;
   finally
          FreeAndNil(ObjqueryEstoque);
   end;

end;

function TObjPERSIANAGRUPODIAMETROCOR.AumentaEstoque(quantidade,persianacor:string):Boolean;
var
   ObjqueryEstoque:TIBQuery;
begin
   Result:=True;
   try
          ObjqueryEstoque:=TIBQuery.Create(nil);
          ObjqueryEstoque.Database:=FDataModulo.IBDatabase;
          with ObjqueryEstoque do
          begin
                try
                      Close;
                      sql.Clear;
                      sql.Add('update TabPersianaGrupoDiametroCor set estoque=estoque+'+quantidade);
                      SQL.Add('where codigo='+persianacor);
                      ExecSQL;
                except
                     Result:=False;
                end;



          end;
   finally
          FreeAndNil(ObjqueryEstoque);
          FDataModulo.IBTransaction.CommitRetaining;
   end;

end;



function TObjPERSIANAGRUPODIAMETROCOR.Get_EstoqueMinimo: string;
begin
  result := self.EstoqueMinimo;
end;

procedure TObjPERSIANAGRUPODIAMETROCOR.Submit_EstoqueMinimo(
  parametro: string);
begin
  self.EstoqueMinimo := parametro;
end;

end.



