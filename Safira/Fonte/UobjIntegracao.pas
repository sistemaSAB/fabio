unit UobjIntegracao;

(*Existe um Objeto igual a este mais Vazio usado apenas para interface das fun��es que ser�o usadas em outros aplicativos,
Exemplo
no Amanda usaremos este que n�o tem nada e n�o instaciaremos a variavel global,
por�m no safira instanciaremos e a unit n�o ser� essa ser� uma unit local ao
fonte do Safira por�m com o mesmo nome, usarei em casos onde preciso
integrar o financeiro a m�dulos especificos de cada softwares
*)

interface
uses ibquery,dialogs,controls;

type

   TObjIntegracao=class

          Public


                constructor create;
                destructor free;
                Function ExcluiporChequeDevolvido(Pcodigo:String):boolean;
                Function LancaComissaoNegativa(Pcodigo:string):boolean;


          Private
                ObjQuery,Objquery2,Objquery3:Tibquery;

                
   End;


implementation

uses UDataModulo,sysutils, UessencialGlobal;

{ TObjIntegracao }

constructor TObjIntegracao.create;
begin
     Try
        Self.ObjQuery:=Tibquery.create(nil);
        Self.ObjQuery.database:=Fdatamodulo.ibdatabase;
     Except
           on e:exception do
           Begin
                 raise exception.create(e.message);
           End;
     End;

     Try
        Self.ObjQuery2:=Tibquery.create(nil);
        Self.ObjQuery2.database:=Fdatamodulo.ibdatabase;
     Except
           on e:exception do
           Begin
                 raise exception.create(e.message);
           End;
     End;

     Try
        Self.ObjQuery3:=Tibquery.create(nil);
        Self.ObjQuery3.database:=Fdatamodulo.ibdatabase;
     Except
           on e:exception do
           Begin
                 raise exception.create(e.message);
           End;
     End;


end;

function TObjIntegracao.ExcluiporChequeDevolvido(Pcodigo: String): boolean;
begin
     Result:=False;
     //O Pcodigo � o C�digo do Registro Cheque Devolvido

     
     //Para Cada Cheque Devolvido lan�ado, foi lan�ado uma comiss�o negativa na Tabela de Comiss�o
     //E uma comissao positiva ligada ao titulo a receber do cheque devolvivo
     
     //Sendo Assim em casos de exclus�o do cheque devolvido (registro que controla)
     //devo excluir essa comiss�o negativa

     With Self.ObjQuery do
     Begin
          close;
          sql.clear;
          sql.add('Select count(codigo) as CONTA from TABCOMISSAOVENDEDORES WHERE lotepagamento is not null and ChequeDevolvido='+Pcodigo);
          open;

          if (fieldbyname('conta').asinteger>0)
          Then Begin
                    if (Messagedlg('Existem comiss�es que est�o inclu�das em lotes de pagamento, certeza que deseja exclu�-las?',mtconfirmation,[mbyes,mbno],0)=mrno)
                    then exit;
          End;

          
          close;
          sql.Clear;
          sql.add('Delete from TabComissaoVendedores where ChequeDevolvido='+Pcodigo);
          Try
             ExecSQL;
          Except
                on e:exception do
                Begin
                     MessageDlg('Erro ao tentar Excluir a Comiss�o ligada ao cheque devolvido'+#13+e.message,mterror,[mbok],0);
                     exit;
                End;
          End;
     End;
     Result:=True;
end;

destructor TObjIntegracao.free;
begin
     freeandnil(Self.objquery);
     freeandnil(Self.objquery2);
     freeandnil(Self.objquery3);
end;

function TObjIntegracao.LancaComissaoNegativa(Pcodigo: string): boolean;
var
ptituloareceberchequedevolvido,
ppendenciaareceberchequedevolvido,pvalorcomissao,ppedido,pvendedor,ppendencia,plancamento,
pvalorpendencia,pvalorlancamento,pvalorcheque:string;
PComissaoDevolver,pvalortituloareceberchequedevolvido,PpercentualCheque,PpercentualLancamento:currency;
begin
     result:=False;
     ppendencia:='';
     plancamento:='';
     pvalorpendencia:='';

     //O Pcodigo � o C�digo do Registro Cheque Devolvido

     With Self.ObjQuery do
     Begin
          


          close;
          sql.clear;
          sql.add('Select tabvalores.codigo as CODIGOVALORES,tabvalores.lancamento,tabvalores.valor as ValorCheque,');
          sql.add('tablancamento.pendencia,tabchequedevolvido.tituloareceber,tabtitulo.valor as valortitulo,tablancamento.valor as VALORLANCAMENTO,');
          sql.add('tabpendencia.valor as valorpendencia from tabchequedevolvido');
          sql.add('join tabvalores on tabchequedevolvido.cheque=tabvalores.cheque');
          sql.add('join tablancamento on tabvalores.lancamento=tablancamento.codigo');
          sql.add('join tabtitulo on tabchequedevolvido.tituloareceber=tabtitulo.codigo');
          sql.add('left join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
          sql.add('where tabchequedevolvido.codigo='+pcodigo);
          open;

          if (fieldbyname('codigovalores').asstring='')
          then Begin
                    Messagedlg('N�o foi encontrado o Lan�amento de entrada deste cheque',mterror,[mbok],0);
                    exit;
          End;


          Plancamento:=Fieldbyname('lancamento').asstring;
          PvalorCheque:=Fieldbyname('valorcheque').asstring;
          PvalorLancamento:=Fieldbyname('valorlancamento').asstring;

          ptituloareceberchequedevolvido:=Fieldbyname('tituloareceber').asstring;
          pvalortituloareceberchequedevolvido:=Fieldbyname('valortitulo').ascurrency;


          //Calculando o percentual do cheque em rela��o ao lan�amento
          //Lancamento      100
          //Cheque           X
          //X =  (Cheque*100)/Pendencia
          PpercentualCheque:=(strtocurr(pvalorcheque)*100)/strtocurr(pvalorlancamento);

          //Se o Cheque foi em um valor maior que o lancamento(gerando troco) trabalho apenas com 100%
          if (ppercentualCheque>100)
          Then PpercentualCheque:=100;

          if (fieldbyname('pendencia').asstring='')//Foi em Lote
          Then Begin
                    Self.Objquery2.Close;
                    Self.Objquery2.sql.clear;
                    Self.Objquery2.sql.add('Select tabpendencia.valor as valorpendencia,tablancamento.valor AS VALORLANCAMENTO,');
                    Self.Objquery2.sql.add('tablancamento.pendencia from tablancamento');
                    Self.Objquery2.sql.add('JOIN TABPENDENCIA ON TABLANCAMENTO.PENDENCIA=TABPENDENCIA.CODIGO');
                    Self.Objquery2.sql.add('where lancamentopai='+Plancamento);
                    try
                        Self.Objquery2.open;
                    Except
                          on e:exception do
                          Begin
                               Messagedlg('Erro ao tentar selecionar as pend�ncias do Lote '+plancamento+#13+E.message,mterror,[mbok],0);
                               exit;
                          End;
                    End;

          End
          Else Begin
                    //normal
                    Self.Objquery2.Close;
                    Self.Objquery2.sql.clear;
                    Self.Objquery2.sql.add('Select tabpendencia.valor as valorpendencia,tablancamento.valor AS VALORLANCAMENTO,');
                    Self.Objquery2.sql.add('tablancamento.pendencia from tablancamento ');
                    Self.Objquery2.sql.add('JOIN TABPENDENCIA ON TABLANCAMENTO.PENDENCIA=TABPENDENCIA.CODIGO');
                    Self.Objquery2.sql.add('where tablancamento.codigo='+Plancamento);
                    try
                        Self.Objquery2.open;
                    Except
                          on e:exception do
                          Begin
                               MessageDlg('Erro ao tentar selecionar os dados da pend�ncia do lan�amento '+plancamento+#13+E.message,mterror,[mbok],0);
                               exit;
                          End;
                    End;
          End;

          While not(Self.Objquery2.eof) do
          begin
               //Guardando o C�digo da pend�ncia atual e o valor inicial da mesma
               Ppendencia:=Self.Objquery2.Fieldbyname('pendencia').asstring;
               PvalorPendencia:=Self.Objquery2.Fieldbyname('valorpendencia').asstring;
               pvalorlancamento:=Self.Objquery2.Fieldbyname('valorlancamento').asstring;

               //Procurando se esta pendencia tem comiss�o ligada a ela
               close;
               sql.clear;
               sql.add('Select TCV.*,tabpendencia.Valor as VALORPENDENCIA from tabcomissaovendedores TCV');
               sql.add('join tabpendencia on TCV.pendencia=tabpendencia.codigo');
               sql.add('where TCV.pendencia='+ppendencia);
               open;

               if (recordcount>0)//n�o � uma pend�ncia de cheque devolvido
               Then Begin

                         //� proveniente de uma comiss�o
                         Pvendedor:=fieldbyname('vendedor').asstring;
                         ppedido:=fieldbyname('pedido').asstring;
                         PvalorComissao:=fieldbyname('valorcomissao').asstring;

                         //Calculando o percentual do lan�amento em rela��o a pend�ncia
                         //Pendencia    100
                         //Lancamento    X

                         PpercentualLancamento:=(strtocurr(pvalorlancamento)*100)/strtocurr(pvalorpendencia);

                         if (PpercentualLancamento>100)
                         Then PpercentualLancamento:=100;


               
                         PComissaoDevolver:=(((strtocurr(PvalorComissao)*PpercentualLancamento)/100)*ppercentualcheque)/100;


                         (*Lan�ando a Comissao negativa

                         (Comissao = 100%  do Valor que ser� definido por ->  (Valor da COmissao (inicial) X   %Lancamento X % Cheque representa quitacao em rela��o ao valor do lancamento)

                         Exemplo

                         Pedido gera 5 parcelas de R$ 100,00 com 3% de COmiss�o cada

                         Isso significa que cada parcela ter� R$ 3,00 de comiss�o


                         Vou receber R$ 50,00 de uma parcela de R$ 100,00
                         com um cheque de R$ 25,00

                         Ao Calcular percebo ent�o que o cheque representa 50% do Valor do Lan�amento

                         E o lan�amento representa 50% do Valor da Parcela

                         Encontro a Comiss�o inicial que � R$ 3,00

                         Lan�o uma comiss�o negativa no valor de -1 X 3,00x50%(perc.lancto)  X  50% (percentual do cheque)

                         ou seja R$ 1,5*50% = R$ 0,75

                         e uma positiva no mesmo valor

                         A negativa ligo a mesma pendencia inicial, assim no pr�ximo lote

                         ela ser� usada no c�lculo da comiss�o, pois a pend�ncia estar� com Saldo Zerado

                         A positiva lan�o na pend�ncia do T�tulo a receber gerado pelo Cheque Devolvido

                         Assim no pr�ximo reprocessamento se ele tiver sido recebido a comiss�o � devolvida
                         ao vendedor

                         As duas ter�o q estar ligadas a chave estrangeira  do Cheque Devolvido
                         para em caso de exclus�o do registro do cheque devolvido, as duas sejam exclu�das

                         *)


                         //Comiss�o Negativa


                         close;
                         sql.clear;
                         sql.clear;
                         sql.add('Insert Into TabComissaoVendedores(Codigo,Vendedor,Pedido,Pendencia,Valor,Comissao,ChequeDevolvido,observacao) values (-1,:pvendedor,:ppedido,:ppendencia,:Pvalor,:ppercentual,:pchequedevolvido,:pobservacao)');
                         ParamByName('pvendedor').asstring:=pvendedor;
                         ParamByName('ppedido').asstring:=ppedido;
                         ParamByName('ppendencia').asstring:=ppendencia;
                         ParamByName('pvalor').ascurrency:=PcomissaoDevolver*-1;//negativa
                         ParamByName('ppercentual').ascurrency:=100;
                         ParamByName('pchequedevolvido').asstring:=Pcodigo;
                         ParamByName('pobservacao').asstring:='CHEQUE DEVOLVIVO. R$ COM '+formata_valor(pvalorcomissao)+'/ %LAN '+currtostr(PpercentualLancamento)+'/ %CH '+CurrToStr(PpercentualCheque);

                         Try
                             ExecSQL;
                         Except
                               on e:exception do
                               Begin
                                    Messagedlg('Erro na tentativa de lan�ar a comiss�o negativa '+#13+e.message,mterror,[mbok],0);
                                    exit;
                               end;
                         end;


                         //Positiva

                         //Tenho que dividir a devolu��o da comiss�o ao vendedor de acordo com o n�mero de parcelas
                         //e o valor de cada parcela do T�tulo a Receber do Cheque Devolvido

                         close;
                         sql.clear;
                         sql.add('Select * from tabpendencia where titulo='+ptituloareceberchequedevolvido);
                         open;
                         first;

                         While not(eof) do
                         Begin

                               //pvalortituloareceberchequedevolvido   100%
                               //valorpendencia                           X

                               Self.Objquery3.close;
                               Self.Objquery3.sql.clear;
                               Self.Objquery3.sql.clear;
                               Self.Objquery3.sql.add('Insert Into TabComissaoVendedores(Codigo,Vendedor,Pedido,Pendencia,Valor,Comissao,ChequeDevolvido,observacao) values (-1,:pvendedor,:ppedido,:ppendencia,:Pvalor,:ppercentual,:pchequedevolvido,:pobservacao)');
                               Self.Objquery3.ParamByName('pvendedor').asstring:=pvendedor;
                               Self.Objquery3.ParamByName('ppedido').asstring:=ppedido;
                               Self.Objquery3.ParamByName('ppendencia').asstring:=fieldbyname('codigo').AsString;//pendencia do titulo a receber
                               Self.Objquery3.ParamByName('pvalor').ascurrency:=PComissaoDevolver;
                               //O Percentual depende do percentual da pendencia em relacao ao valor do titulo a receber do cheque devolvido
                               Self.Objquery3.ParamByName('ppercentual').ascurrency:=(fieldbyname('valor').ascurrency*100)/pvalortituloareceberchequedevolvido;
                               Self.Objquery3.ParamByName('pchequedevolvido').asstring:=Pcodigo;
                               Self.ObjQuery3.ParamByname('pobservacao').asstring:='';
                               Try
                                   Self.Objquery3.ExecSQL;
                               Except
                                     on e:exception do
                                     Begin
                                          Messagedlg('Erro na tentativa de lan�ar a comiss�o positiva '+#13+e.message,mterror,[mbok],0);
                                          exit;
                                     end;
                               end;

                               Next;//pr�xima pendencia do titulo a receber do cheque devolvido
                         End;

               End;//RecordCount de comissao ligada a pendencia

               Self.Objquery2.Next;//pr�xima pend�ncia
          End;//While


     End;//with

     result:=True;
end;

end.
