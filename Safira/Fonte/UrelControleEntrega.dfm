�
 TQRCONTROLEENTREGA 0g  TPF0TQrControleEntregaQrControleEntregaLeft Top WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLEQRSTRBANDSB Functions.DATA00'''' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinAutoPrintIfEmpty	
SnapToGrid	UnitsMMZoomd TQRBandBandacLeft&Top&Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values      ��@������v�	@ BandTyperbPageHeader TQRLabellBlbnomesuperiorLeft TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@                 �@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption SAB BLINDEX - EXCLAIM TECNOLOGIAColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize	  
TQRSysDataqrsysdtLbNumPagLeftmTopWidthZHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@     b�	@UUUUUUU�@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	ColorclWhiteDataqrsPageNumberTextPagina TransparentFontSize
  
TQRSysDataqrsysdt1Left�TopWidthDHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������$�	@UUUUUUU�@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	ColorclWhiteDataqrsDateTimeTransparentFontSize
  TQRShapeqrshp1Left�TopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values       �@UUUUUUU��      ��@UUUUUU��	@ 	Pen.WidthShape
qrsHorLine   TQRBandBandaTITULOLeft&TopAWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values�������@������v�	@ BandTyperbTitle TQRShapeqrshp2Left�TopVWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values       �@UUUUUUU����������@UUUUUU��	@ Shape
qrsHorLine  TQRLabellBNomeLeftVTop[WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������@TUUUUU��@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBNomeColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabel
lBEnderecoLeftVToppWidthsHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������@������*�@      ^�	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption
lBEnderecoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabel	lBContatoLeftVTop� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������@�������@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption	lBContatoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellBObraLeftVTop� WidthsHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������@VUUUUU��@      ^�	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBObraColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRShapeqrshpQrlinhaSuperiorPropostaLeft�Top� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values       �@UUUUUUU��UUUUUUu�@UUUUUU��	@ 	Pen.WidthShape
qrsHorLine  TQRLabellBQrLbPropostaLeft� Top� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values       �@������:�@UUUUUUe�@UUUUUUQ�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionCONTROLE DE ENTREGAColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellB1LeftTop\WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@������j�@      P�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
Nome     :ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabellB2LeftTopqWidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@UUUUUU}�@      P�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   Endereço :ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabellB3LeftTop� WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@      (�@      P�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
Obra     :ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabellB4LeftTop� WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@UUUUUUE�@      P�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
Contato  :ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabellB5LeftrTop\WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��@������j�@      P�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
Telefone :ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabellB6LeftrTop� WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��@UUUUUUE�@      P�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
CNPJ/CPF :ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabel	lBLabel20LeftsTop� WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������f�@VUUUUU�@      P�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
Vendedor :ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabel
lBTelefoneLeft�Top\WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������(�	@������j�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption
lBTelefoneColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabel	lBCNPJCPFLeft�Top� Width{HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������(�	@UUUUUUE�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption	lBCNPJCPFColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabel
lBVendedorLeft�Top� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU}�	@VUUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption
lBVendedorColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellBRGIELeftsTop� WidthVHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ^�	@UUUUUUE�@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBRGIEColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellB7LeftCTop� Width9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ~�	@UUUUUUE�@      Ж@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionRG/IE :ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRImageimgLogotipoLeftTopWidth� HeightQFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      P�@ XUUUUU�@ XUUUUU�@      ��@   TQRLabellBCabecalhoLinha1Left� TopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      p�@       �@     ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBCabecalhoLinha1ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameVerdana
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellBCabecalhoLinha2Left� TopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@ XUUUU�@       �@     ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption~CabecalhoLinha2ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameVerdana
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellBCabecalhoLinha3Left� Top-Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@ XUUUU�@�������@     ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBCabecalhoLinha3ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameVerdana
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellBCabecalhoLinha4Left� Top;Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU�@�������@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBCabecalhoLinha4ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameVerdana
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabel
lBPropostaLeftTopWidth[HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUC�	@       �@TUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
lBPropostaColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellBTituloLeftTopWidthRHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUC�	@UUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption	lbtitluloColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellB10LeftTop� Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@      ��@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionComplemento    :ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabellBcomplemento1Left� Top� WidthDHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@�������@������R�@������ҿ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionComplementoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabelLBEmailLeftYTop� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������z�@      `�@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBEmail@lbemail.com.brColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel1LeftTop� WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@      `�@      P�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
Email    :ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel2LeftTop� Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@UUUUUU��@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionComplemento Ped:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabellbComplementoPedidoLeft� Top� WidthDHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@�������@      �@������ҿ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionComplemento do pedidoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
   TQRBandBandaDetailLeft&TopQWidth�Height� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesUUUUUU�@������v�	@ BandTyperbDetail TQRImageimgmProjetoLeftTopWidthiHeightMFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@UUUUUUU�@������*�@      �@ Stretch	  TQRLabellBReferenciaProjetoLeftpTopWidthZHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������*�@UUUUUUU�@UUUUUU�	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBReferenciaProjetoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellBDescricaoLeftpTopWidthZHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������*�@VUUUUU��@UUUUUU�	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBDescricaoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellBComplementoLeftpTopPWidthZHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������*�@��������@UUUUUU�	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBComplementoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellBAlturaLeftpTop0Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������*�@       �@       �@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBAlturaColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellBLocalLeftpTop"WidthZHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������*�@�������@UUUUUU�	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBLocalColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabel	lBLarguraLeft:Top/Width	HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@TUUUUU��@UUUUUUI�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption	lBLarguraColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRShapeqrshp3Left�TopbWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUUU��UUUUUU��@UUUUUU��	@ Shape
qrsHorLine  TQRShapeqrshp4Left�Top�Width�Height
Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@UUUUUUU��UUUUUUU��UUUUUU��	@ 	Pen.WidthShape
qrsHorLine  TQRLabellBITEMQTDEValorLeftTopsWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@       �@������"�@TUUUUU��	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBITEMQTDEValorColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabellB8LeftpTop@Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU�@������*�@UUUUUUU�@������R�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionLbAlturaColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellB9Left:Top@Width
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@UUUUUUU�@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption	lbLarguraColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellBLadoLeftPTop0Width1HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������	@       �@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionlBLadoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellBlado1LeftPTopCWidth1HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������	@UUUUUUE�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionlBLadoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabellBObservacaoProjetoLeft%Top^WidthEHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@TUUUUU��@      ��@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption   ObservaçãoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameVerdana
Font.Style 
ParentFontTransparentWordWrap	FontSize   TQRStringsBandQrStrBandSBLeft&Top�Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style ForceNewColumnForceNewPage
ParentFontSize.ValuesUUUUUU�@������v�	@ MasterOwnerPrintBefore TQRLabellBDADOSLeft TopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@          UUUUUUU� @TUUUUU��	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlBDADOSColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style OnPrintLBDADOSPrint
ParentFontTransparentWordWrap	FontSize	   TQRBandBandaBandSumarioLeft&Top�Width�Height� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values      �@������v�	@ BandType	rbSummary TQRLabellBNomeVendedorAssinatura1LeftTop8WidthEHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������*�@������*�@      ��@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption   ObservaçãoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameVerdana
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRMemoqrmObservacaoLeftTopUWidthqHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU�@UUUUUUU�@UUUUUU��@     �@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretch	ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRShapeqrshp5Left�TopWidth�Height
Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@ XUUUUU�� XUUUUU� @UUUUUU��	@ 	Pen.WidthShape
qrsHorLine  TQRLabellB12Left�TopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��	@       �@UUUUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionData da Venda    ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellBdataLeftZTopWidthpHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU�	@       �@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionlBdataColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellBDataELeft�TopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��	@������
�@      `�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionData de Entrega   ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellBDataEntregaLeftZTopWidthpHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU�	@UUUUUUu�@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionlBDataEntregaColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize    