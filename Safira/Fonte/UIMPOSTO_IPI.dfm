object FIMPOSTO_IPI: TFIMPOSTO_IPI
  Left = 416
  Top = 189
  Width = 847
  Height = 509
  Caption = 'Cadastro de Imposto (IPI) - EXCLAIM TECNOLOGIA'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbLbST: TLabel
    Left = 5
    Top = 59
    Width = 108
    Height = 13
    Caption = 'Situa'#231#227'o Tribut'#225'ria'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbNomeST: TLabel
    Left = 125
    Top = 76
    Width = 108
    Height = 13
    Caption = 'Situa'#231#227'o Tribut'#225'ria'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbCodigoEnquadramento: TLabel
    Left = 319
    Top = 98
    Width = 186
    Height = 13
    Caption = 'C'#243'digo de Enquadramento Legal'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbClasseEnq: TLabel
    Left = 5
    Top = 98
    Width = 272
    Height = 13
    Caption = 'Classe de Enquadramento (Cigarros e bebidas)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbCnpjProdutor: TLabel
    Left = 5
    Top = 138
    Width = 100
    Height = 13
    Caption = 'CNPJ do Produtor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbCodigoSelo: TLabel
    Left = 319
    Top = 138
    Width = 158
    Height = 13
    Caption = 'C'#243'digo do Selo de Controle'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbTipoCalculo: TLabel
    Left = 5
    Top = 177
    Width = 88
    Height = 13
    Caption = 'Tipo de C'#225'lculo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbAliquota: TLabel
    Left = 5
    Top = 217
    Width = 46
    Height = 13
    Caption = 'Al'#237'quota'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbQuantidadeTotalUP: TLabel
    Left = 5
    Top = 256
    Width = 240
    Height = 13
    Caption = 'Quantidade  de unidade padr'#227'o para Trib.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbFORMULABASECALCULO: TLabel
    Left = 5
    Top = 296
    Width = 160
    Height = 13
    Caption = 'F'#243'rmula da Base de C'#225'lculo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lb1: TLabel
    Left = 5
    Top = 337
    Width = 149
    Height = 13
    Caption = 'F'#243'rmula Valor do Imposto'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object lbLbValorUnidade: TLabel
    Left = 7
    Top = 379
    Width = 103
    Height = 13
    Caption = 'Valor por Unidade'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object imgrodape: TImage
    Left = 0
    Top = 434
    Width = 831
    Height = 37
    Align = alBottom
    Stretch = True
  end
  object edtST_IPI: TEdit
    Left = 5
    Top = 75
    Width = 100
    Height = 19
    Color = 6073854
    MaxLength = 9
    TabOrder = 0
    OnDblClick = edtST_IPIDblClick
    OnKeyDown = EdtST_IPIKeyDown
  end
  object edtCodigoEnquadramento_IPI: TEdit
    Left = 319
    Top = 114
    Width = 100
    Height = 19
    MaxLength = 10
    TabOrder = 1
  end
  object edtClasseEnq_IPI: TEdit
    Left = 5
    Top = 114
    Width = 100
    Height = 19
    MaxLength = 10
    TabOrder = 2
  end
  object edtCnpjProdutor_IPI: TEdit
    Left = 5
    Top = 154
    Width = 200
    Height = 19
    MaxLength = 20
    TabOrder = 3
  end
  object edtCodigoSelo_IPI: TEdit
    Left = 319
    Top = 154
    Width = 100
    Height = 19
    MaxLength = 10
    TabOrder = 4
  end
  object cbbComboTipoCalculo_IPI: TComboBox
    Left = 5
    Top = 193
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 5
    Text = '  '
    Items.Strings = (
      'Percentual'
      'Valor')
  end
  object edtAliquota_IPI: TEdit
    Left = 5
    Top = 233
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 6
  end
  object edtQuantidadeTotalUP_IPI: TEdit
    Left = 5
    Top = 272
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 7
  end
  object edtFORMULABASECALCULO_IPI: TEdit
    Left = 5
    Top = 312
    Width = 549
    Height = 19
    MaxLength = 500
    TabOrder = 8
  end
  object edtFORMULA_VALOR_IMPOSTO_IPI: TEdit
    Left = 5
    Top = 352
    Width = 548
    Height = 19
    TabOrder = 9
  end
  object edtValorUnidade_IPI: TEdit
    Left = 7
    Top = 395
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 10
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 831
    Height = 54
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 11
    DesignSize = (
      831
      54)
    object lbnomeformulario: TLabel
      Left = 553
      Top = 3
      Width = 104
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Cadastro de'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigo: TLabel
      Left = 692
      Top = 9
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb5: TLabel
      Left = 585
      Top = 25
      Width = 25
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'IPI'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
      Spacing = 0
    end
  end
end
