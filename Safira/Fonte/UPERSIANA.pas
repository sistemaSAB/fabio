// No codigo fonte GDC significa "Grupo Diametro Cor"

unit UPERSIANA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UobjPERSIANAGRUPODIAMETROCOR,
  Tabs,UessencialGlobal, CheckLst, Grids, DBGrids, DBCtrls, ComCtrls,
  UFRImposto_ICMS,uobjpersiana_icms,UFORNECEDOR,UGRUPOPERSIANA,UCOR,UDIAMETRO,UpesquisaMenu,ClipBrd;

type
  TFPERSIANA = class(TForm)
    Guia: TTabSet;
    Notebook: TNotebook;
    EdtReferencia: TEdit;
    EdtNome: TEdit;
    LbNome: TLabel;
    LbReferencia: TLabel;
    PanelPersianaGDC: TPanel;
    EdtCodigoPersianaGDC: TEdit;
    LbGrupoPersiana: TLabel;
    LbCor: TLabel;
    LbDiametro: TLabel;
    LbEstoque: TLabel;
    EdtCorGDCReferencia: TEdit;
    EdtGrupoPersianaGDCReferencia: TEdit;
    LbNomeGrupoPersianaGDC: TLabel;
    LbNomeCorGDC: TLabel;
    LbNomeDiametroGDC: TLabel;
    EdtDiametroGDC: TEdit;
    GroupBoxMargem: TGroupBox;
    LbPorcentagemFornecido: TLabel;
    LbPorcentagemRetirado: TLabel;
    LbPorcentagemInstalado: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdtPorcentagemInstaladoGDC: TEdit;
    EdtPorcentagemFornecidoGDC: TEdit;
    EdtPorcentagemRetiradoGDC: TEdit;
    GroupBoxPrecoVenda: TGroupBox;
    LbPrecoVendaInstalado: TLabel;
    LbPrecoVendaFornecido: TLabel;
    LbPrecoVendaRetirado: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdtPrecoVendaInstaladoGDC: TEdit;
    EdtPrecoVendaFornecidoGDC: TEdit;
    EdtPrecoVendaRetiradoGDC: TEdit;
    DBGridPersianaGDC: TDBGrid;
    EdtGrupoPersianaGDC: TEdit;
    EdtCorGDC: TEdit;
    LbSituacaoTributaria_TabelaA: TLabel;
    EdtSituacaoTributaria_TabelaA: TEdit;
    EdtSituacaoTributaria_TabelaB: TEdit;
    PanelICMSForaEstado: TPanel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    EdtAliquota_ICMS_ForaEstado: TEdit;
    EdtReducao_BC_ICMS_ForaEstado: TEdit;
    EdtAliquota_ICMS_Cupom_ForaEstado: TEdit;
    comboisentoicms_foraestado: TComboBox;
    comboSUBSTITUICAOICMS_FORAESTADO: TComboBox;
    edtVALORPAUTA_SUB_TRIB_FORAESTADO: TEdit;
    PanelICMSEstado: TPanel;
    LbAliquota_ICMS_Estado: TLabel;
    LbReducao_BC_ICMS_Estado: TLabel;
    LbAliquota_ICMS_Cupom_Estado: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    EdtAliquota_ICMS_Estado: TEdit;
    EdtReducao_BC_ICMS_Estado: TEdit;
    EdtAliquota_ICMS_Cupom_Estado: TEdit;
    comboisentoicms_estado: TComboBox;
    comboSUBSTITUICAOICMS_ESTADO: TComboBox;
    edtVALORPAUTA_SUB_TRIB_ESTADO: TEdit;
    lb2: TLabel;
    EdtFornecedor: TEdit;
    LbNomeFornecedor: TLabel;
    Label16: TLabel;
    edtpercentualagregado: TEdit;
    Panel1: TPanel;
    EdtClassificacaoFiscal: TEdit;
    LbClassificaoFiscal: TLabel;
    FRImposto_ICMS1: TFRImposto_ICMS;
    Label15: TLabel;
    edtclassificacaofiscal_cor: TEdit;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    SpeedButtonPrimeiro: TSpeedButton;
    SpeedButtonAnterior: TSpeedButton;
    SpeedButtonProximo: TSpeedButton;
    SpeedButtonUltimo: TSpeedButton;
    pnl2: TPanel;
    btgravarGDC: TBitBtn;
    btExcluirGDC: TBitBtn;
    btCancelarGDC: TBitBtn;
    edtNCM: TEdit;
    lb3: TLabel;
    lb4: TLabel;
    lb5: TLabel;
    edtPrecoCustoGDC: TEdit;
    chkAtivo: TCheckBox;
    lbAtivoInativo: TLabel;
    btAjuda: TSpeedButton;
    Label1: TLabel;
    edtCest: TEdit;
    Label8: TLabel;
    edtEstoqueMinimo: TEdit;
//DECLARA COMPONENTES

    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btGravarGDCClick(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure EdtGrupoPersianaGDCReferenciaExit(Sender: TObject);
    procedure EdtGrupoPersianaGDCReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtCorGDCReferenciaExit(Sender: TObject);
    procedure EdtCorGDCReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtDiametroGDCExit(Sender: TObject);
    procedure EdtDiametroGDCKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtPrecoCustoGDCExit(Sender: TObject);
    procedure EdtPorcentagemInstaladoGDCExit(Sender: TObject);
    procedure EdtPorcentagemFornecidoGDCExit(Sender: TObject);
    procedure EdtPorcentagemRetiradoGDCExit(Sender: TObject);
    procedure DBGridPersianaGDCDblClick(Sender: TObject);
    procedure BtExcluirGDCClick(Sender: TObject);
    procedure btCancelarGDCClick(Sender: TObject);
    procedure EdtFornecedorExit(Sender: TObject);
    procedure EdtFornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
    procedure DBGridPersianaGDCDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure EdtFornecedorDblClick(Sender: TObject);
    procedure EdtGrupoPersianaGDCReferenciaDblClick(Sender: TObject);
    procedure EdtCorGDCReferenciaDblClick(Sender: TObject);
    procedure EdtDiametroGDCDblClick(Sender: TObject);
    procedure edtNCMKeyPress(Sender: TObject; var Key: Char);
    procedure LbEstoqueMouseLeave(Sender: TObject);
    procedure LbEstoqueMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure LbEstoqueClick(Sender: TObject);
    procedure edtquant(Sender: TObject;
  var Key: Char);
    procedure chkAtivoClick(Sender: TObject);
    procedure btAjudaClick(Sender: TObject);
    procedure LbNomeFornecedorClick(Sender: TObject);
    procedure LbNomeFornecedorMouseLeave(Sender: TObject);
    procedure LbNomeGrupoPersianaGDCClick(Sender: TObject);
    procedure LbNomeCorGDCClick(Sender: TObject);
    procedure LbNomeFornecedorMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure edtNCMKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCestKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    PAlteraCampoEstoque:boolean;
    objPERSIANAGRUPODIAMETROCOR:tobjPERSIANAGRUPODIAMETROCOR;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    Procedure ResgataPersianaGDC;
    Procedure PreparaPersianaGDC;
    Procedure PraparaGroupBox;
    Procedure LimpaeditGrupoBox;

  public
        procedure AbreComCodigo(parametro:string);
  end;

var
  FPERSIANA: TFPERSIANA;

implementation

uses Upesquisa, UessencialLocal, UDataModulo, UobjPERSIANA,
  UescolheImagemBotao, Uprincipal, UFiltraImp, UAjuda, UobjCEST;

{$R *.dfm}



procedure TFPERSIANA.FormCreate(Sender: TObject);
begin

(*Points[0].X :=305;      Points[0].Y := 22;
Points[1].X :=286;      Points[1].Y := 3;
Points[2].X :=41;       Points[2].Y := 3;
Points[3].X :=41;       Points[3].Y := 22;
Points[4].X :=28;       Points[4].Y := 35;
Points[5].X :=28;       Points[5].Y := 164;
Points[6].X :=41;       Points[6].Y := 177;
Points[7].X :=41;       Points[7].Y := 360;
Points[8].X :=62;       Points[8].Y := 381;
Points[9].X :=632;      Points[9].Y := 381;
Points[10].X :=647;     Points[10].Y := 396;
Points[11].X :=764;     Points[11].Y := 396;
Points[12].X :=764;     Points[12].Y := 95;
Points[13].X :=780;     Points[13].Y := 79;
Points[14].X :=780;     Points[14].Y := 22;
Points[15].X :=305;     Points[15].Y := 22;

Regiao1:=CreatePolygonRgn(Points, 16, WINDING);
SetWindowRgn(Self.Handle, regiao1, False);*)

     
end;



procedure TFPERSIANA.FormClose(Sender: TObject; var Action: TCloseAction);
begin

     If (Self.objPERSIANAGRUPODIAMETROCOR=Nil)
     Then exit;

     If (Self.objPERSIANAGRUPODIAMETROCOR.Persiana.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    try
          Self.objPERSIANAGRUPODIAMETROCOR.free;
    Except
          on e:exception do
          Begin
               mensagemerro(e.message);
          End;
    End;
    try
      FRImposto_ICMS1.ObjMaterial_ICMS.Free;
    Except
          on e:exception do
          Begin
               mensagemerro(e.message);
          End;
    End;
    self.Tag:=0;
end;

procedure TFPERSIANA.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);
     chkAtivo.Checked:=True;
     lbAtivoInativo.Caption:='Ativo';

     lbCodigo.caption:='0';

     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     objPERSIANAGRUPODIAMETROCOR.Persiana.status:=dsInsert;
     Guia.TabIndex:=0;
     Notebook.PageIndex:=0;

     EdtReferencia.setfocus;
     EdtCorGDCReferencia.Enabled:=false;


     comboisentoicms_estado.Text:='N�O';
     comboSUBSTITUICAOICMS_ESTADO.Text:='SIM';
     edtVALORPAUTA_SUB_TRIB_ESTADO.Text:='0';
     EdtAliquota_ICMS_Estado.Text:='0';
     EdtReducao_BC_ICMS_Estado.Text:='0';
     EdtAliquota_ICMS_Cupom_Estado.Text:='0';

     comboisentoicms_foraestado.Text:='N�O';
     comboSUBSTITUICAOICMS_foraESTADO.Text:='SIM';
     edtVALORPAUTA_SUB_TRIB_foraESTADO.Text:='0';
     EdtAliquota_ICMS_foraEstado.Text:='0';
     EdtReducao_BC_ICMS_foraEstado.Text:='0';
     EdtAliquota_ICMS_Cupom_foraEstado.Text:='0';
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;
end;

procedure TFPERSIANA.btSalvarClick(Sender: TObject);
begin

     If objPERSIANAGRUPODIAMETROCOR.Persiana.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (objPERSIANAGRUPODIAMETROCOR.Persiana.salvar(true)=False)
     Then exit;

     lbCodigo.caption:=objPERSIANAGRUPODIAMETROCOR.Persiana.Get_codigo;
     habilita_botoes(Self);
     desabilita_campos(Self);
      btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
      btopcoes.Visible :=true;
end;

procedure TFPERSIANA.btAlterarClick(Sender: TObject);
begin
    If (objPERSIANAGRUPODIAMETROCOR.Persiana.Status=dsinactive) and (lbCodigo.caption<>'')
    Then Begin
                habilita_campos(Self);

                objPERSIANAGRUPODIAMETROCOR.Persiana.Status:=dsEdit;
                guia.TabIndex:=0;
                EdtReferencia.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btnovo.Visible :=false;
                 btalterar.Visible:=false;
                 btpesquisar.Visible:=false;
                 btrelatorio.Visible:=false;
                 btexcluir.Visible:=false;
                 btsair.Visible:=false;
                 btopcoes.Visible :=false;
          End;

end;

procedure TFPERSIANA.btCancelarClick(Sender: TObject);
begin
     objPERSIANAGRUPODIAMETROCOR.Persiana.cancelar;
     LbNomeFornecedor.Caption:='';

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
      btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
      btopcoes.Visible :=true;
      lbCodigo.Caption:='';

end;

procedure TFPERSIANA.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(objPERSIANAGRUPODIAMETROCOR.Persiana.Get_pesquisa,objPERSIANAGRUPODIAMETROCOR.Persiana.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If objPERSIANAGRUPODIAMETROCOR.Persiana.status<>dsinactive
                                  then exit;

                                  If (objPERSIANAGRUPODIAMETROCOR.Persiana.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  objPERSIANAGRUPODIAMETROCOR.Persiana.ZERARTABELA;
                                  Guia.TabIndex:=0;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
     Guia.TabIndex:=0;
     Notebook.PageIndex:=0; 
end;

procedure TFPERSIANA.btExcluirClick(Sender: TObject);
begin
     If (objPERSIANAGRUPODIAMETROCOR.Persiana.status<>dsinactive) or (lbCodigo.caption='')
     Then exit;

     If (objPERSIANAGRUPODIAMETROCOR.Persiana.LocalizaCodigo(lbCodigo.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (objPERSIANAGRUPODIAMETROCOR.Persiana.exclui(lbCodigo.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFPERSIANA.btRelatorioClick(Sender: TObject);
begin
//    objPERSIANAGRUPODIAMETROCOR.Persiana.Imprime(lbCodigo.caption);
end;

procedure TFPERSIANA.btSairClick(Sender: TObject);
begin
    Self.close;
end;


procedure TFPERSIANA.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFPERSIANA.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFPERSIANA.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    If Key=VK_Escape
    Then Self.Close;

      if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;
    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('CADASTRO DE PERSIANAS');
         FAjuda.ShowModal;
    end;


end;

procedure TFPERSIANA.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFPERSIANA.ControlesParaObjeto: Boolean;
begin
  Try
    With objPERSIANAGRUPODIAMETROCOR.Persiana do
    Begin
        Submit_Codigo(lbCodigo.caption);
        Submit_Referencia(edtReferencia.text);
        Submit_Nome(edtNome.text);
        Fornecedor.Submit_Codigo(EdtFornecedor.Text);

        Submit_ClassificacaoFiscal(edtClassificacaoFiscal.text);

//Campos que nao sao mais usados
        Submit_IsentoICMS_Estado('N');
        Submit_SubstituicaoICMS_Estado('N');
        Submit_ValorPauta_Sub_Trib_Estado('0');
        Submit_Aliquota_ICMS_Estado('0');
        Submit_Reducao_BC_ICMS_Estado('0');
        Submit_Aliquota_ICMS_Cupom_Estado('0');
        Submit_IsentoICMS_ForaEstado('N');
        Submit_SubstituicaoICMS_ForaEstado('N');
        Submit_ValorPauta_Sub_Trib_ForaEstado('0');
        Submit_Aliquota_ICMS_ForaEstado('0');
        Submit_Reducao_BC_ICMS_ForaEstado('0');
        Submit_Aliquota_ICMS_Cupom_ForaEstado('0');
        SituacaoTributaria_TabelaA.Submit_codigo('0');
        SituacaoTributaria_TabelaB.Submit_codigo('0');
        Submit_percentualagregado('0');
        Submit_ipi('0');
        Submit_NCM(edtNCM.Text);
        Submit_cest(edtCest.Text);

        if(chkAtivo.Checked=True)
        then Submit_Ativo('S')
        else Submit_Ativo('N');


        Result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFPERSIANA.LimpaLabels;
begin
    LbNomeGrupoPersianaGDC.Caption:='';
    LbNomeCorGDC.Caption:='';
    LbNomeDiametroGDC.Caption:='';
end;

function TFPERSIANA.ObjetoParaControles: Boolean;
begin
  Try
     With objPERSIANAGRUPODIAMETROCOR.Persiana do
     Begin
        lbCodigo.caption:=Get_Codigo;
        EdtReferencia.text:=Get_Referencia;
        EdtNome.text:=Get_Nome;
        EdtFornecedor.Text:=Fornecedor.Get_Codigo;
        LbNomeFornecedor.Caption:=Fornecedor.Get_RazaoSocial;
        ComboIsentoICMS_Estado.text:=Get_IsentoICMS_Estado;
        ComboSubstituicaoICMS_Estado.text:=Get_SubstituicaoICMS_Estado;
        EdtValorPauta_Sub_Trib_Estado.text:=Get_ValorPauta_Sub_Trib_Estado;
        EdtAliquota_ICMS_Estado.text:=Get_Aliquota_ICMS_Estado;
        EdtReducao_BC_ICMS_Estado.text:=Get_Reducao_BC_ICMS_Estado;
        EdtAliquota_ICMS_Cupom_Estado.text:=Get_Aliquota_ICMS_Cupom_Estado;
        ComboIsentoICMS_ForaEstado.text:=Get_IsentoICMS_ForaEstado;
        ComboSubstituicaoICMS_ForaEstado.text:=Get_SubstituicaoICMS_ForaEstado;
        EdtValorPauta_Sub_Trib_ForaEstado.text:=Get_ValorPauta_Sub_Trib_ForaEstado;
        EdtAliquota_ICMS_ForaEstado.text:=Get_Aliquota_ICMS_ForaEstado;
        EdtReducao_BC_ICMS_ForaEstado.text:=Get_Reducao_BC_ICMS_ForaEstado;
        EdtAliquota_ICMS_Cupom_ForaEstado.text:=Get_Aliquota_ICMS_Cupom_ForaEstado;
        EdtSituacaoTributaria_TabelaA.text:=SituacaoTributaria_TabelaA.Get_codigo;
        EdtSituacaoTributaria_TabelaB.text:=SituacaoTributaria_TabelaB.Get_codigo;
        EdtClassificacaoFiscal.text:=Get_ClassificacaoFiscal;
        edtpercentualagregado.Text:=Get_percentualagregado;
        edtNCM.Text:=Get_NCM;
        edtCest.Text := Get_cest;
        if(Get_Ativo='S') then
        begin
            chkAtivo.Checked:=True;
            lbAtivoInativo.Caption:='Ativo';
        end
        else
        begin
            chkAtivo.Checked:=False;
            lbAtivoInativo.Caption:='Inativo';
        end;

        result:=True;                                        
     End;
  Except
        Result:=False;
  End;
end;

function TFPERSIANA.TabelaParaControles: Boolean;
begin
     If (objPERSIANAGRUPODIAMETROCOR.Persiana.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFPERSIANA.btGravarGDCClick(Sender: TObject);
begin
    With objPERSIANAGRUPODIAMETROCOR do
    Begin
        ZerarTabela;

        if (EdtCodigoPersianaGDC.text='')
        or (edtcodigoPersianaGDC.text='0')
        Then Begin
                Status:=dsInsert;
                edtcodigoPersianaGDC.Text:='0';

                //PReciso cadastrar as persianas em todas as cores, mas eu tenho que ter
                //o codigo da persiana, grupo e diametro, estoque e preco custo Zero
                //Segundo a Joana e Camila a persiana naum tem preco de custo e somente preco de venda
                // ao qual � colcocado na hora do pedido

                if (objPERSIANAGRUPODIAMETROCOR.CadastraPersianaEmTodasAsCores(lbCodigo.caption,EdtGrupoPersianaGDC.Text,EdtDiametroGDC.Text)=false)then
                Begin
                    MensagemErro('Erro ao tentar cadastrar essa Persiana nas cores escolhidas.');
                    FDataModulo.IBTransaction.RollbackRetaining;
                    exit;
                end;

                FDataModulo.IBTransaction.CommitRetaining;
                MensagemAviso('Cores Cadastradas com Sucesso !');
        End
        Else Begin
              Status:=dsEdit;
              Submit_Codigo(EdtCodigoPersianaGDC.Text);

              Submit_classificacaofiscal(edtclassificacaofiscal_cor.Text);


              Persiana.Submit_Codigo(lbCodigo.caption);
              GrupoPersiana.Submit_Codigo(EdtGrupoPersianaGDC.Text);
              Cor.Submit_Codigo(EdtCorGDC.Text);
              Diametro.Submit_Codigo(EdtDiametroGDC.Text);
              //if (EdtEstoqueGDC.text='')
              //Then EdtEstoqueGDC.Text:='0';
              //Submit_Estoque(EdtEstoqueGDC.Text);
              Submit_PrecoCusto(tira_ponto(edtPrecoCustoGDC.text));
              Submit_PorcentagemInstalado(EdtPorcentagemInstaladoGDC.Text);
              Submit_PorcentagemRetirado(EdtPorcentagemRetiradoGDC.Text);
              Submit_PorcentagemFornecido(EdtPorcentagemFornecidoGDC.Text);
              //Submit_Estoque(EdtEstoqueGDC.Text);
              Submit_EstoqueMinimo( edtEstoqueMinimo.Text );
              if (Salvar(True)=False)
              Then Begin
                        EdtGrupoPersianaGDC.SetFocus;
                        exit;
              End;
        end;

        LimpaLabels;
        limpaedit(PanelPersianaGDC);
        Self.LimpaeditGrupoBox;
        EdtGrupoPersianaGDCReferencia.SetFocus;
        Self.ResgataPersianaGDC;

        EdtCorGDCReferencia.Enabled:=true;
        EdtCorGDCReferencia.Enabled:=False;
    End;

end;

procedure TFPERSIANA.PreparaPersianaGDC;
begin
     Self.ResgataPersianaGDC;
     limpaedit(PanelPersianaGDC);
     habilita_campos(PanelPersianaGDC);
     EdtPrecoVendaInstaladoGDC.Enabled:=false;
     EdtPrecoVendaRetiradoGDC.Enabled:=false;
     EdtPrecoVendaFornecidoGDC.Enabled:=false;
    { if(PAlteraCampoEstoque=True)
     then EdtEstoqueGDC.Enabled:=True
     else
     EdtEstoqueGDC.Enabled:=False;    }
end;

procedure TFPERSIANA.ResgataPersianaGDC;
begin
     objPERSIANAGRUPODIAMETROCOR.ResgataPersianaGDC(lbCodigo.caption);
     formatadbgrid(DBGridPersianaGDC);
end;

procedure TFPERSIANA.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     if (newtab=2)//cor
     Then Begin
               if (objPERSIANAGRUPODIAMETROCOR.Persiana.Status=dsinsert)
               or (lbCodigo.caption='')
               Then Begin
                         AllowChange:=False;
                         exit;
               End;

               Self.PreparaPersianaGDC;
               Self.PraparaGroupBox;
               Notebook.PageIndex:=NewTab;
               EdtGrupoPersianaGDCReferencia.SetFocus;
               EdtCorGDCReferencia.Enabled:=false;

     End
     Else Begin
             if (newtab=1)//Impostos
             Then Begin
                      if (objPERSIANAGRUPODIAMETROCOR.persiana.Status=dsinsert)
                      or (lbCodigo.caption='')
                      Then Begin
                                AllowChange:=False;
                                exit;
                      End;
                      Notebook.PageIndex:=NewTab;
                      FRImposto_ICMS1.Acessaframe(lbCodigo.caption);


             End
             Else Begin
                     Notebook.PageIndex:=NewTab;
                      
             End;
     end;
end;

procedure TFPERSIANA.EdtGrupoPersianaGDCReferenciaExit(Sender: TObject);
begin
    ObjPERSIANAGRUPODIAMETROCOR.EdtGrupoPersianaExit(Sender,EdtGrupoPersianaGDC, LbNomeGrupoPersianaGDC);
end;

procedure TFPERSIANA.EdtGrupoPersianaGDCReferenciaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    objPERSIANAGRUPODIAMETROCOR.EdtGrupoPersianaKeyDown(Sender,EdtGrupoPersianaGDC, Key, Shift,LbNomeGrupoPersianaGDC );
end;

procedure TFPERSIANA.EdtCorGDCReferenciaExit(Sender: TObject);
begin
    objPERSIANAGRUPODIAMETROCOR.EdtCorExit(Sender,EdtCorGDC, LbNomeCorGDC);
end;

procedure TFPERSIANA.EdtCorGDCReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    objPERSIANAGRUPODIAMETROCOR.EdtCorKeyDown(Sender,EdtCorGDC,Key, Shift, LbNomeCorGDC);
end;

procedure TFPERSIANA.EdtDiametroGDCExit(Sender: TObject);
begin
    LbNomeDiametroGDC.Caption:='mm.';
end;

procedure TFPERSIANA.EdtDiametroGDCKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    objPERSIANAGRUPODIAMETROCOR.EdtDiametroKeyDown(Sender, Key, Shift, LbNomeDiametroGDC);
end;

procedure TFPERSIANA.PraparaGroupBox;
begin
     GroupBoxMargem.Enabled:=true;
     GroupBoxPrecoVenda.Enabled:=true;
     EdtPorcentagemInstaladoGDC.Enabled:=true;
     EdtPorcentagemFornecidoGDC.Enabled:=true;
     EdtPorcentagemRetiradoGDC.Enabled:=true;
     EdtPrecoVendaInstaladoGDC.Enabled:=false;
     EdtPrecoVendaFornecidoGDC.Enabled:=false;
     EdtPrecoVendaRetiradoGDC.Enabled:=false;          
end;

procedure TFPERSIANA.EdtPrecoCustoGDCExit(Sender: TObject);
begin
      TEdit(Sender).Text:=formata_valor(TEdit(Sender).Text);

Self.PraparaGroupBox;
EdtPorcentagemInstaladoGDC.SetFocus;
end;

procedure TFPERSIANA.EdtPorcentagemInstaladoGDCExit(Sender: TObject);
Var
  PrecoCusto:Currency;
begin
  PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCustoGDC.text));
  EdtPrecoVendaInstaladoGDC.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemInstaladoGDC.Text))/100)));
end;

procedure TFPERSIANA.EdtPorcentagemFornecidoGDCExit(Sender: TObject);
Var
  PrecoCusto:Currency;
begin
  PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCustoGDC.text));
  EdtPrecoVendaFornecidoGDC.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemFornecidoGDC.Text))/100)));
end;

procedure TFPERSIANA.EdtPorcentagemRetiradoGDCExit(Sender: TObject);
Var
  PrecoCusto:Currency;
begin
  PrecoCusto:=StrToCurr(tira_ponto(edtPrecoCustoGDC.text));
  EdtPrecoVendaRetiradoGDC.Text:=CurrToStr((PrecoCusto+((PrecoCusto*StrToCurr(EdtPorcentagemRetiradoGDC.Text))/100)));
end;

procedure TFPERSIANA.LimpaeditGrupoBox;
begin
      EdtPorcentagemInstaladoGDC.Clear;
      EdtPorcentagemFornecidoGDC.Clear;
      EdtPorcentagemRetiradoGDC.Clear;

      EdtPrecoVendaInstaladoGDC.Clear;
      EdtPrecoVendaFornecidoGDC.Clear;
      EdtPrecoVendaRetiradoGDC.Clear;
end;

procedure TFPERSIANA.DBGridPersianaGDCDblClick(Sender: TObject);
begin
     If (DBGridPersianaGDC.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridPersianaGDC.DataSource.DataSet.RecordCount=0)
     Then exit;

     limpaedit(PanelPersianaGDC);
     LimpaLabels;
     if (objPERSIANAGRUPODIAMETROCOR.LocalizaCodigo(DBGridPersianaGDC.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataPersianaGDC; 
               exit;
     End;

     With objPERSIANAGRUPODIAMETROCOR do
     Begin
          TabelaparaObjeto;
          EdtCodigoPersianaGDC.Text:=Get_Codigo;
          lbCodigo.caption:=Persiana.Get_Codigo;
          Edtclassificacaofiscal_cor.Text:=Get_classificacaofiscal;

          EdtGrupoPersianaGDC.Text:=GrupoPersiana.Get_Codigo;
          EdtGrupoPersianaGDCReferencia.Text:=GrupoPersiana.Get_Referencia;
          LbNomeGrupoPersianaGDC.Caption:=GrupoPersiana.Get_Nome;
          EdtCorGDC.Text:=Cor.Get_Codigo;
          EdtCorGDCReferencia.Text:=Cor.Get_Referencia;
          LbNomeCorGDC.Caption:=Cor.Get_Descricao;
          EdtDiametroGDC.Text:=Diametro.Get_Codigo;
          LbNomeDiametroGDC.Caption:='mm.';
          //EdtEstoqueGDC.Text:=RetornaEstoque;
          LbEstoque.Caption:=RetornaEstoque;
          edtEstoqueMinimo.Text := Get_EstoqueMinimo;
          
          EdtPrecoCustoGDC.Text:=Get_PrecoCusto;
          EdtPorcentagemInstaladoGDC.Text:=Get_PorcentagemInstalado;
          EdtPorcentagemRetiradoGDC.Text:=Get_PorcentagemRetirado;
          EdtPorcentagemFornecidoGDC.Text:=Get_PorcentagemFornecido;
          EdtPrecoVendaInstaladoGDC.Text:=Get_PrecoVendaInstalado;
          EdtPrecoVendaFornecidoGDC.Text:=Get_PrecoVendaFornecido;
          EdtPrecoVendaRetiradoGDC.Text:=Get_PrecoVendaRetirado;

          EdtCorGDCReferencia.Enabled:=true;
          EdtCorGDCReferencia.Enabled:=true;
     End;

end;

procedure TFPERSIANA.BtExcluirGDCClick(Sender: TObject);
begin
     if (EdtCodigoPersianaGDC.Text='')then
     exit;

     If (DBGridPersianaGDC.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridPersianaGDC.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (Messagedlg('Tem Certeza que deseja excluir essa de Persiana Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     objPERSIANAGRUPODIAMETROCOR.Exclui(DBGridPersianaGDC.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     limpaedit(PanelPersianaGDC);
     Self.LimpaeditGrupoBox;
     Self.ResgataPersianaGDC;

     EdtCorGDCReferencia.Enabled:=false;

end;

procedure TFPERSIANA.btCancelarGDCClick(Sender: TObject);
begin
     limpaedit(PanelPersianaGDC);
     Self.LimpaeditGrupoBox;
     EdtGrupoPersianaGDCReferencia.SetFocus;
     EdtCorGDCReferencia.Enabled:=false;
     LbNomeGrupoPersianaGDC.Caption:='';
     LbNomeCorGDC.Caption:='';
     LbNomeDiametroGDC.Caption:='';
end;

procedure TFPERSIANA.EdtFornecedorExit(Sender: TObject);
begin
  objPERSIANAGRUPODIAMETROCOR.Persiana.EdtFornecedorExit(Sender, LbNomeFornecedor);

  Guia.TabIndex:=1;
end;

procedure TFPERSIANA.EdtFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   objPERSIANAGRUPODIAMETROCOR.Persiana.EdtFornecedorKeyDown(Sender, key,Shift, LbNomeFornecedor);
end;

procedure TFPERSIANA.btPrimeiroClick(Sender: TObject);
begin
    if  (objPERSIANAGRUPODIAMETROCOR.Persiana.PrimeiroRegistro = false)then
    exit;

    ObjPersianaGrupoDiametroCor.Persiana.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFPERSIANA.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigo.caption='')then
    lbCodigo.caption:='0';

    if  (ObjPersianaGrupoDiametroCor.Persiana.RegistoAnterior(StrToInt(lbCodigo.caption)) = false)then
    exit;

    ObjPersianaGrupoDiametroCor.Persiana.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFPERSIANA.btProximoClick(Sender: TObject);
begin
    if (lbCodigo.caption='')then
    lbCodigo.caption:='0';

    if  (ObjPersianaGrupoDiametroCor.Persiana.ProximoRegisto(StrToInt(lbCodigo.caption)) = false)then
    exit;

    ObjPersianaGrupoDiametroCor.Persiana.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFPERSIANA.btUltimoClick(Sender: TObject);
begin
    if  (ObjPersianaGrupoDiametroCor.Persiana.UltimoRegistro = false)then
    exit;

    ObjPersianaGrupoDiametroCor.Persiana.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;



procedure TFPERSIANA.DBNavigatorClick(Sender: TObject;
  Button: TNavigateBtn);
begin
     If (DBGridPersianaGDC.DataSource.DataSet.Active=False)
     Then exit;

     If (DBGridPersianaGDC.DataSource.DataSet.RecordCount=0)
     Then exit;

     limpaedit(PanelPersianaGDC);
     LimpaLabels;
     if (objPERSIANAGRUPODIAMETROCOR.LocalizaCodigo(DBGridPersianaGDC.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then Begin
               Messagedlg('C�digo n�o localizado',mterror,[mbok],0);
               Self.ResgataPersianaGDC;
               exit;
     End;

     With objPERSIANAGRUPODIAMETROCOR do
     Begin
          TabelaparaObjeto;
          EdtCodigoPersianaGDC.Text:=Get_Codigo;
          lbCodigo.caption:=Persiana.Get_Codigo;
          EdtGrupoPersianaGDC.Text:=GrupoPersiana.Get_Codigo;
          EdtGrupoPersianaGDCReferencia.Text:=GrupoPersiana.Get_Referencia;
          LbNomeGrupoPersianaGDC.Caption:=GrupoPersiana.Get_Nome;
          EdtCorGDC.Text:=Cor.Get_Codigo;
          EdtCorGDCReferencia.Text:=Cor.Get_Referencia;
          LbNomeCorGDC.Caption:=Cor.Get_Descricao;
          EdtDiametroGDC.Text:=Diametro.Get_Codigo;
          LbNomeDiametroGDC.Caption:='mm.';
          LbEstoque.Caption:=RetornaEstoque;
          EdtPrecoCustoGDC.Text:=Get_PrecoCusto;
          EdtPorcentagemInstaladoGDC.Text:=Get_PorcentagemInstalado;
          EdtPorcentagemRetiradoGDC.Text:=Get_PorcentagemRetirado;
          EdtPorcentagemFornecidoGDC.Text:=Get_PorcentagemFornecido;
          EdtPrecoVendaInstaladoGDC.Text:=Get_PrecoVendaInstalado;
          EdtPrecoVendaFornecidoGDC.Text:=Get_PrecoVendaFornecido;
          EdtPrecoVendaRetiradoGDC.Text:=Get_PrecoVendaRetirado;

          EdtCorGDCReferencia.Enabled:=true;
          EdtCorGDCReferencia.Enabled:=true;
     End;

end;

procedure TFPERSIANA.DBGridPersianaGDCDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridPersianaGDC.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridPersianaGDC.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridPersianaGDC.DefaultDrawDataCell(Rect,DBGridPersianaGDC.Columns[DataCol].Field, State);
          End;
end;

procedure TFPERSIANA.AbreComCodigo(parametro:string);
begin
    if(objPERSIANAGRUPODIAMETROCOR.Persiana.LocalizaReferencia(parametro)=True)
    then
    begin
        objPERSIANAGRUPODIAMETROCOR.Persiana.TabelaparaObjeto;
        Self.ObjetoParaControles;
    end;
end;

procedure TFPERSIANA.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.TabIndex:=0;
     Notebook.PageIndex:=0;
     ColocaUpperCaseEdit(Self);

     Try
        Self.ObjPERSIANAGRUPODIAMETROCOR:=TobjPERSIANAGRUPODIAMETROCOR.create;
        DBGridPersianaGDC.DataSource:=objPERSIANAGRUPODIAMETROCOR.ObjDatasource;
        FRImposto_ICMS1.ObjMaterial_ICMS:=TObjpersiana_ICMS.Create(self);

     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     lbCodigo.Caption:='';
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btgravarGDC,'BOTAOINSERIR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btcancelarGDC,'BOTAOCANCELARPRODUTO.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btexcluirGDC,'BOTAORETIRAR.BMP');


     habilita_botoes(Self);
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE PERSIANA')=False)
     Then desab_botoes(Self);

     PAlteraCampoEstoque:=False;
     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('PERMITE ALTERAR O CAMPO ESTOQUE NO CADASTRO DE PERSIANA')=True)
     Then PAlteraCampoEstoque:=true;

     if(Tag<>0)
     then
     begin
        if(objPERSIANAGRUPODIAMETROCOR.Persiana.LocalizaCodigo(IntToStr(tag))=True)
        then
        begin
              objPERSIANAGRUPODIAMETROCOR.Persiana.TabelaparaObjeto;
              self.ObjetoParaControles;

        end;

     end;
end;

procedure TFPERSIANA.EdtFornecedorDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Ffornecedor:TFFORNECEDOR;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Ffornecedor:=TFFORNECEDOR.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabfornecedor','',Ffornecedor)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtFornecedor.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 LbNomeFornecedor.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('razaosocial').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Ffornecedor);

     End;
end;

procedure TFPERSIANA.EdtGrupoPersianaGDCReferenciaDblClick(
  Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FgrupoPersiana:TFGRUPOPERSIANA;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            try
                FgrupoPersiana:=TFGRUPOPERSIANA.Create(nil);
            except

            end;


            If (FpesquisaLocal.PreparaPesquisa('Select * from tabgrupopersiana','',FgrupoPersiana)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtGrupoPersianaGDCReferencia.text:=FpesquisaLocal.QueryPesq.fieldbyname('referencia').asstring;
                                 LbNomeGrupoPersianaGDC.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FgrupoPersiana);

     End;
end;

procedure TFPERSIANA.EdtCorGDCReferenciaDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Fcor:TFCOR ;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fcor:=TFCOR.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabcor','',Fcor)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtCorGDCReferencia.text:=FpesquisaLocal.QueryPesq.fieldbyname('referencia').asstring;
                                 LbNomeCorGDC.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fcor);

     End;
end;

procedure TFPERSIANA.EdtDiametroGDCDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   Fdiametro:TFDIAMETRO;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fdiametro:=TFDIAMETRO.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select Codigo as Diametro from TabDIAMETRO','',Fdiametro)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtDiametroGDC.text:=FpesquisaLocal.QueryPesq.fieldbyname('Diametro').asstring;
                               //  LbNomeDiametroGDC.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(Fdiametro);

     End;
end;


procedure TFPERSIANA.edtNCMKeyPress(Sender: TObject; var Key: Char);
begin
        if not (Key in['0'..'9',Chr(8)]) then
    begin
        Key:= #0;
    end;
end;

procedure TFPERSIANA.LbEstoqueMouseLeave(Sender: TObject);
begin
       TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFPERSIANA.LbEstoqueMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFPERSIANA.LbEstoqueClick(Sender: TObject);
begin
     if(ObjParametroGlobal.ValidaParametro('UTILIZA SPED FISCAL')=false) then
     begin
          MensagemAviso('O Parametro "UTILIZA SPED FISCAL" n�o foi encontrado!!!');
          Exit;
     end;
     if(ObjParametroGlobal.Get_Valor='SIM')
     then Exit;

      If(PAlteraCampoEstoque=False)then
     begin
          MensagemAviso('O Usu�rio '+ObjUsuarioGlobal.Get_nome+' n�o tem autoriza��o para alterar o estoque');
          Exit;
     end
     else
     begin
          with FfiltroImp do
          begin
              DesativaGrupos;
              Grupo01.Enabled:=True;
              edtgrupo01.Text:='';
              LbGrupo01.Caption:='QUANTIDADE';
              edtgrupo01.OnKeyPress:=edtquant;

              MensagemAviso('Acerte o estoque: Digite a quantidade a tirar ou acrescentar no estoque');

              ShowModal;

              if(Tag=0)
              then Exit;

              if(edtgrupo01.Text='') or(edtgrupo01.Text='0')
              then Exit;
              
              OBJESTOQUEGLOBAL.ZerarTabela;
              OBJESTOQUEGLOBAL.Status:=dsInsert;
              OBJESTOQUEGLOBAL.Submit_CODIGO('0');
              OBJESTOQUEGLOBAL.Submit_DATA(FormatDateTime('dd/mm/yyyy',Now));
              OBJESTOQUEGLOBAL.Submit_OBSERVACAO('PELO MODO ACERTO DE ESTOQUE (PERSIANA)');
              OBJESTOQUEGLOBAL.Submit_QUANTIDADE(edtgrupo01.Text);
              OBJESTOQUEGLOBAL.Submit_PERSIANAGRUPODIAMETROCOR(ObjPERSIANAGRUPODIAMETROCOR.Get_Codigo);

              if (OBJESTOQUEGLOBAL.Salvar(True)=False)
              Then Begin
                     MensagemErro('Erro na tentativa de salvar o registro de estoque do PERFILADO');
                     exit;
              End;

              lbestoque.Caption:=ObjPERSIANAGRUPODIAMETROCOR.RetornaEstoque;


          end;
     end;
end;

procedure TFPERSIANA.edtquant(Sender: TObject;
  var Key: Char);
begin
     if not (Key in['0'..'9',Chr(8),'-']) then
    begin
        Key:= #0;
    end;

end;

procedure TFPERSIANA.chkAtivoClick(Sender: TObject);
begin
      if(lbCodigo.Caption='') or (lbCodigo.Caption='0')
      then Exit;
      if(chkAtivo.Checked=True)
      then lbAtivoInativo.Caption:='Ativo';

      if(chkAtivo.Checked=False)
      then  lbAtivoInativo.Caption:='Inativo'

end;

procedure TFPERSIANA.btAjudaClick(Sender: TObject);
begin
     FAjuda.PassaAjuda('CADASTRO DE PERSIANAS');
     FAjuda.ShowModal;
end;

procedure TFPERSIANA.LbNomeFornecedorClick(Sender: TObject);
var
  FFornecedor:TFFORNECEDOR;
begin
  try
    FFornecedor:=TFFORNECEDOR.Create(nil);
  except
    Exit;
  end;

  try
    if(edtFornecedor.Text='')
    then Exit;

    FFornecedor.tag:=StrToInt(edtFornecedor.Text);
    FFornecedor.ShowModal;

  finally
    FreeAndNil(FFornecedor);
  end;
end;

procedure TFPERSIANA.LbNomeFornecedorMouseLeave(Sender: TObject);
begin
    TEdit(sender).Font.Style:=[fsBold];
  
end;

procedure TFPERSIANA.LbNomeGrupoPersianaGDCClick(Sender: TObject);
var
  FgrupoPersiana:TFGRUPOPERSIANA;
begin
  try
    FgrupoPersiana:=TFGRUPOPERSIANA.Create(nil);
  except
    Exit;
  end;

  try
    if(EdtGrupoPersianaGDC.Text='')
    then Exit;

    FgrupoPersiana.Tag:=StrToInt(EdtGrupoPersianaGDC.Text);
    FgrupoPersiana.ShowModal;

  finally
    FreeAndNil(FgrupoPersiana);
  end;


end;

procedure TFPERSIANA.LbNomeCorGDCClick(Sender: TObject);
var
  Fcor:TFCOR;
begin
   try
      Fcor:=TFCOR.Create(nil);
   except
      Exit;
   end;

   try
     if(EdtCorGDC.Text='')
     then Exit;

     Fcor.Tag:=StrToInt(EdtCorGDC.Text);
     fcor.ShowModal;
   finally
     FreeAndNil(Fcor);
   end; 

end;

procedure TFPERSIANA.LbNomeFornecedorMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
 TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFPERSIANA.edtNCMKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  FDataModulo.edtNCMKeyDown(Sender,Key,Shift);

end;

procedure TFPERSIANA.edtCestKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if Key=vk_f9 then
  begin
    Clipboard.AsText := EDTNCM.Text;
    With TObjCEST.Create do
    begin
      edtCESTkeydown(sender,Key,shift);
      Free;
    end;
  end;
end;

end.
