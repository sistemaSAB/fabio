object FEscolheTransportadora: TFEscolheTransportadora
  Left = 619
  Top = 279
  Width = 727
  Height = 351
  Caption = 'Escolha a Transportadora'
  Color = clBtnFace
  Constraints.MinHeight = 351
  Constraints.MinWidth = 727
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelTransportadora: TPanel
    Left = 0
    Top = 110
    Width = 711
    Height = 162
    Align = alClient
    TabOrder = 1
    object LbNomeTransportadora: TLabel
      Left = 2
      Top = 25
      Width = 98
      Height = 11
      Caption = 'NOME/RAZ'#195'O SOCIAL'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbEnderecoTransportadora: TLabel
      Left = 2
      Top = 70
      Width = 52
      Height = 11
      Caption = 'ENDERE'#199'O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbMunicipioTransportadora: TLabel
      Left = 242
      Top = 70
      Width = 50
      Height = 11
      Caption = 'MUNIC'#205'PIO'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbIETransportadora: TLabel
      Left = 580
      Top = 70
      Width = 101
      Height = 11
      Caption = 'INSCRI'#199#195'O ESTADUAL'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbUFTransportadora: TLabel
      Left = 486
      Top = 70
      Width = 13
      Height = 11
      Caption = 'UF'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbUFVeiculoTransportadora: TLabel
      Left = 486
      Top = 25
      Width = 13
      Height = 11
      Caption = 'UF'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbCNPJTransportadora: TLabel
      Left = 580
      Top = 25
      Width = 47
      Height = 11
      Caption = 'CNPJ/CPF'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbPlacaVeiculoTransportadora: TLabel
      Left = 362
      Top = 25
      Width = 91
      Height = 11
      Caption = 'PLACA DO VE'#205'CULO'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 3
      Top = 3
      Width = 275
      Height = 15
      Caption = 'TRANSPORTADOR / VOLUMES TRANSPORTADOS'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 2
      Top = 111
      Width = 59
      Height = 11
      Caption = 'QUANTIDADE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
    end
    object Label2: TLabel
      Left = 104
      Top = 111
      Width = 40
      Height = 11
      Caption = 'ESP'#201'CIE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
    end
    object lbMarca: TLabel
      Left = 242
      Top = 111
      Width = 31
      Height = 11
      Caption = 'MARCA'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
    end
    object lbNumeracao: TLabel
      Left = 362
      Top = 111
      Width = 58
      Height = 11
      Caption = 'NUMERA'#199#195'O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
    end
    object Label3: TLabel
      Left = 486
      Top = 111
      Width = 61
      Height = 11
      Caption = 'PESO BRUTO'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
    end
    object Label4: TLabel
      Left = 580
      Top = 111
      Width = 68
      Height = 11
      Caption = 'PESO L'#205'QUIDO'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
    end
    object EdtNomeTransportadora: TEdit
      Left = 2
      Top = 38
      Width = 343
      Height = 19
      Ctl3D = False
      MaxLength = 100
      ParentCtl3D = False
      TabOrder = 0
    end
    object EdtEnderecoTransportadora: TEdit
      Left = 2
      Top = 82
      Width = 219
      Height = 19
      Ctl3D = False
      MaxLength = 100
      ParentCtl3D = False
      TabOrder = 4
    end
    object EdtMunicipioTransportadora: TEdit
      Left = 242
      Top = 82
      Width = 224
      Height = 19
      Ctl3D = False
      MaxLength = 100
      ParentCtl3D = False
      TabOrder = 5
    end
    object EdtIETransportadora: TEdit
      Left = 580
      Top = 82
      Width = 122
      Height = 19
      Ctl3D = False
      MaxLength = 100
      ParentCtl3D = False
      TabOrder = 7
      OnKeyPress = EdtIETransportadoraKeyPress
    end
    object EdtUFTransportadora: TEdit
      Left = 486
      Top = 82
      Width = 81
      Height = 19
      Ctl3D = False
      MaxLength = 100
      ParentCtl3D = False
      TabOrder = 6
    end
    object EdtUFVeiculoTransportadora: TEdit
      Left = 486
      Top = 38
      Width = 81
      Height = 19
      Ctl3D = False
      MaxLength = 100
      ParentCtl3D = False
      TabOrder = 2
    end
    object EdtCNPJTransportadora: TEdit
      Left = 580
      Top = 38
      Width = 122
      Height = 19
      Ctl3D = False
      MaxLength = 100
      ParentCtl3D = False
      TabOrder = 3
      OnKeyPress = EdtCNPJTransportadoraKeyPress
    end
    object EdtPlacaVeiculoTransportadora: TEdit
      Left = 362
      Top = 38
      Width = 104
      Height = 19
      Ctl3D = False
      MaxLength = 100
      ParentCtl3D = False
      TabOrder = 1
      OnKeyPress = EdtPlacaVeiculoTransportadoraKeyPress
    end
    object EdtQuantidade: TEdit
      Left = 2
      Top = 125
      Width = 95
      Height = 19
      Hint = 'Quantidade de volumes transportados'
      Color = clGradientInactiveCaption
      Ctl3D = False
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      OnExit = EdtQuantidadeExit
      OnKeyPress = EdtQuantidadeKeyPress
    end
    object edtEspecie: TEdit
      Left = 104
      Top = 125
      Width = 117
      Height = 19
      Hint = 'Esp'#233'cie dos volumes transportados'
      Color = clGradientInactiveCaption
      Ctl3D = False
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
    end
    object edtMarca: TEdit
      Left = 242
      Top = 125
      Width = 115
      Height = 19
      Hint = 'Marca dos volumes transportados'
      Color = clGradientInactiveCaption
      Ctl3D = False
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 10
    end
    object edtNumeracao: TEdit
      Left = 362
      Top = 125
      Width = 102
      Height = 19
      Hint = 'Numera'#231#227'o dos volumes transportados'
      Color = clGradientInactiveCaption
      Ctl3D = False
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 11
      OnKeyPress = edtNumeracaoKeyPress
    end
    object edtPesoBruto: TEdit
      Left = 486
      Top = 125
      Width = 81
      Height = 19
      Hint = 'Peso bruto em (Kg)'
      Color = clGradientInactiveCaption
      Ctl3D = False
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 12
      OnExit = edtPesoBrutoExit
      OnKeyPress = edtPesoBrutoKeyPress
    end
    object edtPesoLiquido: TEdit
      Left = 580
      Top = 125
      Width = 122
      Height = 19
      Hint = 'Peso L'#237'quido (em Kg)'
      Color = clGradientInactiveCaption
      Ctl3D = False
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 13
      OnExit = edtPesoLiquidoExit
      OnKeyPress = edtPesoLiquidoKeyPress
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 711
    Height = 110
    Align = alTop
    TabOrder = 0
    object Label13: TLabel
      Left = 10
      Top = 5
      Width = 85
      Height = 14
      Caption = 'Transportadora'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbtransportadora: TLabel
      Left = 122
      Top = 24
      Width = 85
      Height = 14
      Caption = 'Transportadora'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbFreteporContaTransportadora: TLabel
      Left = 13
      Top = 52
      Width = 86
      Height = 11
      Caption = 'FRETE POR CONTA'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 10
      Top = 71
      Width = 70
      Height = 12
      Caption = '1. EMITENTE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 10
      Top = 90
      Width = 99
      Height = 12
      Caption = '2. DESTINAT'#193'RIO'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object edttransportadora: TEdit
      Left = 10
      Top = 21
      Width = 104
      Height = 19
      Ctl3D = False
      MaxLength = 100
      ParentCtl3D = False
      TabOrder = 0
      OnExit = edttransportadoraExit
      OnKeyDown = edttransportadoraKeyDown
      OnKeyPress = edttransportadoraKeyPress
    end
    object EdtFreteporContaTransportadora: TEdit
      Left = 82
      Top = 70
      Width = 32
      Height = 19
      Ctl3D = False
      MaxLength = 2
      ParentCtl3D = False
      TabOrder = 1
      OnKeyPress = EdtFreteporContaTransportadoraKeyPress
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 272
    Width = 711
    Height = 41
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      711
      41)
    object btOK: TBitBtn
      Left = 616
      Top = 8
      Width = 88
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'OK'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btOKClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object btCancelar: TBitBtn
      Left = 518
      Top = 8
      Width = 88
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = 'Cancelar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btCancelarClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
end
