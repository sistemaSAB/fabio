unit UROMANEIO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls,db, UObjPedidoProjetoROMANEIO,
  UessencialLocal, Tabs, Grids, DBGrids,UObjpedidoprojetopedidocompra,IBQuery,Uprincipal,UAgendaInstalacao,
  ComCtrls, OleCtrls, SHDocVw;

type
  TFROMANEIO = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    btPesquisaAnalitica: TBitBtn;
    pnl1: TPanel;
    imgrodape: TImage;
    lb14: TLabel;
    btAjuda: TSpeedButton;
    pgc1: TPageControl;
    ts1: TTabSheet;
    pnl4: TPanel;
    lbLbObservacoes: TLabel;
    lb8: TLabel;
    lbLbData: TLabel;
    lb9: TLabel;
    lbnomecolocador: TLabel;
    lbNomeTRANSPORTADORA: TLabel;
    lbLbTRANSPORTADORA: TLabel;
    lbConcluido: TLabel;
    lb2: TLabel;
    lb4: TLabel;
    lbAgendar: TLabel;
    edtDataEmissao: TMaskEdit;
    edtdataentrega: TMaskEdit;
    edtcolocador: TEdit;
    edtTRANSPORTADORA: TEdit;
    mmoObservacoes: TMemo;
    dbgridPesquisa: TDBGrid;
    pnl2: TPanel;
    lb3: TLabel;
    lb5: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    btInserirPedidoProjeto: TBitBtn;
    btButton1: TBitBtn;
    edtdataentreganopedido: TMaskEdit;
    edtpedido: TMaskEdit;
    edtpedidoprojeto: TMaskEdit;
    edtpedidoprojeto_pedidocompra: TMaskEdit;
    ts2: TTabSheet;
    wbGoogleMaps: TWebBrowser;
    
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtTRANSPORTADORAKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTRANSPORTADORAExit(Sender: TObject);
    procedure mmoObservacoesKeyPress(Sender: TObject; var Key: Char);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure butBtInserirPedidoProjeto2Click(Sender: TObject);
    procedure edtpedidoprojetoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtpedidoprojetoExit(Sender: TObject);
    procedure but1Click(Sender: TObject);
    procedure edtpedidoExit(Sender: TObject);
    procedure edtpedidoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btopcoesClick(Sender: TObject);
    procedure edtcolocadorExit(Sender: TObject);
    procedure edtcolocadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btBtPesquisaAnaliticaClick(Sender: TObject);
    procedure edtpedidoprojeto_pedidocompraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgridPesquisaDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure edtpedidoKeyPress(Sender: TObject; var Key: Char);
    procedure edtpedidoprojetoKeyPress(Sender: TObject; var Key: Char);
    procedure edtpedidoprojeto_pedidocompraKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtTRANSPORTADORAKeyPress(Sender: TObject; var Key: Char);
    procedure edtcolocadorKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure dbgridPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btAjudaClick(Sender: TObject);
    procedure lbAgendarInstalacaoMouseLeave(Sender: TObject);
    procedure lbAgendarInstalacaoMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbAgendarInstalacaoClick(Sender: TObject);
    procedure lbNomeTRANSPORTADORAMouseLeave(Sender: TObject);
    procedure lbNomeTRANSPORTADORAMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure lbAgendarClick(Sender: TObject);
    procedure lb2Click(Sender: TObject);
    procedure edtdataentregaExit(Sender: TObject);
    procedure edtTRANSPORTADORADblClick(Sender: TObject);
    procedure edtcolocadorDblClick(Sender: TObject);
    procedure edtpedidoDblClick(Sender: TObject);
    procedure edtpedidoprojetoDblClick(Sender: TObject);
    procedure edtpedidoprojeto_pedidocompraDblClick(Sender: TObject);
    procedure pgc1Change(Sender: TObject);
  private
    ObjPedidoProjetoRomaneio:TObjPedidoProjetoRomaneio;
    ObjpedidoProjetoPedidoCompra:TObjpedidoprojetopedidocompra;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    procedure RetornaPedidos;
    Procedure CarregaDesenhoForm;
    procedure MostraQuantidadeCadastrada;
    procedure MarcarPosicaoGMAPS;
    procedure MontarMapa(AOrigem: String; ADestino:TStringList);

    { Private declarations }
  public
    { Public declarations }
  end;

var
  FROMANEIO: TFROMANEIO;


implementation

uses Upesquisa, UessencialGlobal, UobjROMANEIO, UtrocaMaterialpedido,
  UDataModulo, UescolheImagemBotao, UPEDIDO, UAjuda, UObjFUNCIONARIOS;

{$R *.dfm}


procedure TFROMANEIO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjpedidoProjetoromaneio=Nil)
     Then exit;

     If (Self.ObjpedidoProjetoromaneio.ROMANEIO.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjpedidoProjetoromaneio.free;

    if (Self.ObjpedidoProjetoPedidoCompra<>nil)
    Then Self.ObjpedidoProjetoPedidoCompra.free;


    Action := caFree;
    Self := nil;
end;

procedure TFROMANEIO.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);
     desabilita_campos(pnl2);

     lbCodigo.Caption:='0';
     lbConcluido.Caption:='Aberto';
     self.RetornaPedidos;

     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjpedidoProjetoromaneio.ROMANEIO.status:=dsInsert;

     EdtTRANSPORTADORA.setfocus;;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;
     btPesquisaAnalitica.Visible:=False;

     edtDataEmissao.Text:=DateToStr(Now);
     edtdataentrega.Text:=DateToStr(Now);

end;

procedure TFROMANEIO.btSalvarClick(Sender: TObject);
begin

     If Self.ObjpedidoProjetoromaneio.ROMANEIO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjpedidoProjetoromaneio.ROMANEIO.salvar(true)=False)
     Then exit;

    lbCodigo.Caption:=Self.ObjpedidoProjetoromaneio.ROMANEIO.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(pnl2);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;
     btPesquisaAnalitica.Visible:=True;
     MostraQuantidadeCadastrada;
     habilita_campos(pnl2);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFROMANEIO.btAlterarClick(Sender: TObject);
begin
    If ((Self.ObjpedidoProjetoromaneio.ROMANEIO.Status=dsinactive) and (lbCodigo.Caption<>'')
    and (lbConcluido.Caption='Aberto'))
    Then
    Begin
                habilita_campos(Self);

                Self.ObjpedidoProjetoromaneio.ROMANEIO.Status:=dsEdit;
                EdtTRANSPORTADORA.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btnovo.Visible :=false;
                 btalterar.Visible:=false;
                 btpesquisar.Visible:=false;
                 btrelatorio.Visible:=false;
                 btexcluir.Visible:=false;
                 btsair.Visible:=false;
                 btopcoes.Visible :=false;
                  btPesquisaAnalitica.Visible:=False;
    End;

end;

procedure TFROMANEIO.btCancelarClick(Sender: TObject);
begin
     Self.ObjpedidoProjetoromaneio.ROMANEIO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;
     btPesquisaAnalitica.Visible:=True;
     Self.RetornaPedidos;

end;

procedure TFROMANEIO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin
        pgc1.TabIndex:=0;
        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjpedidoProjetoromaneio.ROMANEIO.Get_pesquisa,Self.ObjpedidoProjetoromaneio.ROMANEIO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjpedidoProjetoromaneio.ROMANEIO.status<>dsinactive
                                  then exit;

                                  If (Self.ObjpedidoProjetoromaneio.ROMANEIO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjpedidoProjetoromaneio.ROMANEIO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                                  Self.retornaPedidos;
                                  habilita_campos(pnl2);

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFROMANEIO.btExcluirClick(Sender: TObject);
begin
     If (Self.ObjpedidoProjetoromaneio.ROMANEIO.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (Self.ObjpedidoProjetoromaneio.ROMANEIO.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;
     Self.ObjpedidoProjetoromaneio.ROMANEIO.TabelaparaObjeto;

     

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjpedidoProjetoromaneio.ROMANEIO.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFROMANEIO.btRelatorioClick(Sender: TObject);
begin
    Self.ObjpedidoProjetoromaneio.Imprime(lbCodigo.Caption);
end;

procedure TFROMANEIO.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFROMANEIO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFROMANEIO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFROMANEIO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;

    if (Key=vk_f7)
    Then Begin
              FtrocaMaterialpedido.ShowModal;
    End;

    if(key=vk_f1) then
    begin
        Fprincipal.chamaPesquisaMenu;
    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('ROMANEIO');
         FAjuda.ShowModal;
    end;

end;

procedure TFROMANEIO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFROMANEIO.ControlesParaObjeto: Boolean;
begin
  Try
    With Self.ObjpedidoProjetoromaneio.ROMANEIO do
    Begin
       { if(edtDataEmissao.Text='  /  /    ')then
        begin
             MensagemErro('Preencha a data de emiss�o');
             Exit;
        end;

        if(edtdataentrega.Text='  /  /    ')then
        begin
             MensagemErro('Preencha a data de entrega');
             Exit;
        end;     }

        Submit_CODIGO(lbCodigo.Caption);
        TRANSPORTADORA.Submit_codigo(edtTRANSPORTADORA.text);
        Submit_DataEmissao(edtDataemissao.text);
        Submit_DataEntrega(edtDataEntrega.text);
        Submit_Observacoes(mmoObservacoes.text);
        if(lbConcluido.Caption='Aberto')
        then Submit_Concluido('N')
        else  Submit_Concluido('S') ;

        Colocador.Submit_CODIGO(edtcolocador.Text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFROMANEIO.LimpaLabels;
begin
  LbNomeTRANSPORTADORA.caption:='';
  lbnomecolocador.caption:='';
end;

function TFROMANEIO.ObjetoParaControles: Boolean;
begin
  Try
     With Self.ObjpedidoProjetoromaneio.ROMANEIO do
     Begin
        lbCodigo.Caption:=Get_CODIGO;
        EdtTRANSPORTADORA.text:=TRANSPORTADORA.Get_codigo;
        EdtDataEmissao.text:=Get_DataEmissao;
        Edtdataentrega.text:=get_dataentrega;
        mmoObservacoes.text:=Get_Observacoes;
        if (Get_Concluido='N')
        Then lbConcluido.Caption:='Aberto'
        Else lbConcluido.Caption:='Conclu�do';
        edtcolocador.Text:=Colocador.Get_CODIGO;
        lbnomecolocador.Caption:=Colocador.Funcionario.Get_Nome;
        lbNomeTRANSPORTADORA.Caption:=TRANSPORTADORA.Get_NOME;
        result:=True;

     End;
  Except
        Result:=False;
  End;
end;

function TFROMANEIO.TabelaParaControles: Boolean;
begin
     If (Self.ObjpedidoProjetoromaneio.ROMANEIO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFROMANEIO.edtTRANSPORTADORAKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjpedidoProjetoromaneio.ROMANEIO.EdtTRANSPORTADORAKeyDown(sender,key,shift,LbNomeTRANSPORTADORA);
end;

procedure TFROMANEIO.edtTRANSPORTADORAExit(Sender: TObject);
begin
     Self.ObjpedidoProjetoromaneio.ROMANEIO.EdtTRANSPORTADORAExit(sender,LbNomeTRANSPORTADORA);
end;

procedure TFROMANEIO.mmoObservacoesKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then mmoObservacoes.setfocus;
end;

procedure TFROMANEIO.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
   {  if (NewTab=1)
     Then Begin
                if ((lbCodigo.Caption='')
                or (lbCodigo.Caption='0')
                or (ObjPedidoProjetoRomaneio.Romaneio.Status=dsinsert))
                Then Begin
                          AllowChange:=False;
                          exit;
                end;

                edtpedido.Enabled:=true;
                edtpedidoprojeto.Enabled:=true;
                edtpedidoprojeto_pedidocompra.Enabled:=true;
                edtdataentreganopedido.Enabled:=True;

                BtInserirPedidoProjeto.Enabled:=true;
                Self.retornaPedidos;
     end
     Else Notebook.PageIndex:=newtab;    }


end;

procedure TFROMANEIO.RetornaPedidos;
begin
     ObjPedidoProjetoRomaneio.RetornaPedidosRomaneio(lbCodigo.Caption);
     formatadbgrid(DBGridPesquisa);
end;

procedure TFROMANEIO.butBtInserirPedidoProjeto2Click(Sender: TObject);
var
  qryloc:TIBQuery;
begin
     if (lbConcluido.Caption='Conclu�do') then
     begin
            edtdataentreganopedido.Text:='';
            edtpedido.Text:='';
            edtpedidoprojeto.Text:='';
            edtpedidoprojeto_pedidocompra.Text:='';
            Exit;
     end;


     With ObjPedidoProjetoRomaneio do
     begin
          ZerarTabela;
          Submit_CODIGO('0');
          Romaneio.Submit_CODIGO(lbCodigo.Caption);

          if (((edtpedidoprojeto.Text<>'') and (edtpedidoprojeto_pedidocompra.Text<>''))
          and (edtpedidoprojeto.Text<>edtpedidoprojeto_pedidocompra.Text))
          Then Begin
                    MensagemAviso('Escolha somente um n�mero de pedido/projeto');
                    exit;
          End;


          if((edtpedido.Text<>'')and (edtpedidoprojeto.Text='')) then
          begin
            qryloc:=TIBQuery.Create(nil);
            qryloc.Database:=FDataModulo.IBDatabase;

            qryloc.Close;
            qryloc.SQL.Clear;
            qryloc.SQL.Text:=
            'select codigo from tabpedido_proj where pedido='+edtpedido.Text;
            qryloc.Open;

            while not qryloc.Eof do
            begin
              ZerarTabela;
              Submit_CODIGO('0');
              Romaneio.Submit_CODIGO(lbCodigo.Caption);
              PedidoProjeto.Submit_Codigo(qryloc.Fields[0].AsString);

              status:=dsinsert;

              if (Salvar(True)=False)
              Then exit;

              qryloc.Next;
            end;
            edtdataentreganopedido.Text:='';
            edtpedido.Text:='';
            edtpedidoprojeto.Text:='';
            edtpedidoprojeto_pedidocompra.Text:='';
            EdtPedidoProjeto.SetFocus;
            Self.RetornaPedidos;

          end
          else
          begin   

            PedidoProjeto.Submit_Codigo(edtpedidoprojeto.text);

            if (edtpedidoprojeto_pedidocompra.text<>'')
            then PedidoProjeto.Submit_Codigo(edtpedidoprojeto_pedidocompra.text);

            status:=dsinsert;

            if (Salvar(True)=False)
            Then exit;

            edtdataentreganopedido.Text:='';
            edtpedido.Text:='';
            edtpedidoprojeto.Text:='';
            edtpedidoprojeto_pedidocompra.Text:='';
            EdtPedidoProjeto.SetFocus;
            Self.RetornaPedidos;
          end;

     end;
end;

procedure TFROMANEIO.edtpedidoprojetoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if (edtpedido.text='')
     Then ObjPedidoProjetoRomaneio.EdtPedidoProjetoKeyDown(sender,key,shift,nil)
     Else ObjPedidoProjetoRomaneio.edtpedidoprojetoKeyDown(sender,key,shift,edtpedido.text);
end;

procedure TFROMANEIO.edtpedidoprojetoExit(Sender: TObject);
begin
     ObjPedidoProjetoRomaneio.EdtPedidoProjetoExit(sender,nil);
end;

procedure TFROMANEIO.but1Click(Sender: TObject);
begin
     if (lbConcluido.Caption='Conclu�do') then
     begin
            edtdataentreganopedido.Text:='';
            edtpedido.Text:='';
            edtpedidoprojeto.Text:='';
            edtpedidoprojeto_pedidocompra.Text:='';
            Exit;
     end;
     
      if(DBGridPesquisa.DataSource.DataSet.Active=False)
      Then exit;

     if (DBGridPesquisa.DataSource.DataSet.RecordCount=0)
     then exit;

     ObjPedidoProjetoRomaneio.LocalizaCodigo(DBGridPesquisa.DataSource.DataSet.fieldbyname('codigo').asstring);
     ObjPedidoProjetoRomaneio.TabelaparaObjeto;

     if (Messagedlg('Tem Certeza que deseja retirar o Pedido/projeto Num '+ObjPedidoProjetoRomaneio.PedidoProjeto.Get_Codigo+' deste romaneio?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;

     ObjPedidoProjetoRomaneio.Exclui(ObjPedidoProjetoRomaneio.get_codigo,true);
     Self.RetornaPedidos;
     edtdataentreganopedido.Text:='';
     edtpedido.Text:='';
     edtpedidoprojeto.Text:='';
     edtpedidoprojeto_pedidocompra.Text:='';
end;

procedure TFROMANEIO.edtpedidoExit(Sender: TObject);
begin
     ObjPedidoProjetoRomaneio.PedidoProjeto.EdtPedidoExit(sender,nil);
end;

procedure TFROMANEIO.edtpedidoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjPedidoProjetoRomaneio.PedidoProjeto.EdtPedidoKeyDownComDataConcluidos(sender,key,shift,edtdataentreganopedido.Text);
end;

procedure TFROMANEIO.btopcoesClick(Sender: TObject);
begin
     ObjPedidoProjetoRomaneio.Opcoes(lbCodigo.Caption);

     if (lbCodigo.Caption<>'')
     Then Begin
               if (ObjPedidoProjetoRomaneio.Romaneio.LocalizaCodigo(lbCodigo.Caption)=False)
               then exit;

               ObjPedidoProjetoRomaneio.Romaneio.TabelaparaObjeto;
               Self.TabelaParaControles;
     End;

end;

procedure TFROMANEIO.edtcolocadorExit(Sender: TObject);
begin
     ObjPedidoProjetoRomaneio.Romaneio.EdtColocadorExit(sender,lbnomecolocador);
end;

procedure TFROMANEIO.edtcolocadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjPedidoProjetoRomaneio.Romaneio.EdtColocadorKeyDown(sender,key,shift,lbnomecolocador);
end;

procedure TFROMANEIO.btBtPesquisaAnaliticaClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
   Pcodigo:string;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjpedidoProjetoromaneio.Get_pesquisaAnalitica,'Pesquisa Anal�tica de Romaneio',Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  Pcodigo:=FpesquisaLocal.querypesq.FieldByName('romaneio').asstring;

                                  If Self.ObjpedidoProjetoromaneio.ROMANEIO.status<>dsinactive
                                  then exit;

                                  If (Self.ObjpedidoProjetoromaneio.ROMANEIO.LocalizaCodigo(pcodigo)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;
                                  Self.ObjpedidoProjetoromaneio.ROMANEIO.ZERARTABELA;

                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                                  Self.retornaPedidos;
                                  habilita_campos(pnl2);
                        End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFROMANEIO.CarregaDesenhoForm;
var
  Points: array [0..15] of TPoint;
//  PquantidadePontos:Integer;
  Regiao1:HRgn;
  //NOMECAMPO,Temp:string;
//  arquivo_ini:Tinifile;
  cont:integer;
begin
     {Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     temp:=temp+'DesenhoForm.ini';

     Try
        Arquivo_ini:=Tinifile.Create(Temp);
     Except
           Messagedlg('Erro na Abertura do Arquivo '+temp,mterror,[mbok],0);
           exit;
     End;

     Try
        Try
          NOMECAMPO:='QUANTIDADEPONTOS';
          temp:=arquivo_ini.ReadString('CONFIGURACAO',NOMECAMPO,'');
          PquantidadePontos:=strtoint(temp);
        except
              Messagedlg('Erro na Leitura do campo '+nomecampo+' no arquivo '+temp,mterror,[mbok],0);
              exit;
        end;

        for cont:=0 to PquantidadePontos-1 do
        Begin
             Try
                NOMECAMPO:='POINTS_X_'+inttostr(cont);
                temp:=arquivo_ini.ReadString('CONFIGURACAO',NOMECAMPO,'');
                strtoint(temp);
              Except
                    Messagedlg('Erro na Leitura do campo '+nomecampo+' no arquivo '+temp,mterror,[mbok],0);
                    exit;
              end;
              Points[Cont].X :=Strtoint(temp);
              Try
                NOMECAMPO:='POINTS_Y_'+inttostr(cont);
                temp:=arquivo_ini.ReadString('CONFIGURACAO',NOMECAMPO,'');
                strtoint(temp);
              Except
                    Messagedlg('Erro na Leitura do campo '+nomecampo+' no arquivo '+temp,mterror,[mbok],0);
                    exit;
              end;
              Points[Cont].Y :=Strtoint(temp);
        End;
     Finally
           FreeAndNil(arquivo_ini);
     End;}

     Points[0].X :=305;      Points[0].Y := 22;
     Points[1].X :=286;      Points[1].Y := 3;
     Points[2].X :=41;       Points[2].Y := 3;
     Points[3].X :=41;       Points[3].Y := 22;
     Points[4].X :=28;       Points[4].Y := 35;
     Points[5].X :=28;       Points[5].Y := 164;
     Points[6].X :=41;       Points[6].Y := 177;
     Points[7].X :=41;       Points[7].Y := 365;
     Points[8].X :=81;       Points[8].Y := 420;//teste
     Points[9].X :=632;      Points[9].Y := 420;
     Points[10].X :=647;     Points[10].Y := 435;
     Points[11].X :=764;     Points[11].Y := 435;
     Points[12].X :=764;     Points[12].Y := 95;
     Points[13].X :=780;     Points[13].Y := 79;
     Points[14].X :=780;     Points[14].Y := 22;
     Points[15].X :=305;     Points[15].Y := 22;

     Regiao1:=CreatePolygonRgn(Points,16, WINDING);
     SetWindowRgn(Self.Handle, regiao1, False);
     //**********************************************
end;

procedure TFROMANEIO.edtpedidoprojeto_pedidocompraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjpedidoProjetoPedidoCompra.EdtPedidoProjeto_cadastrados_KeyDown(sender,key,shift);
end;

procedure TFROMANEIO.dbgridPesquisaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGridPesquisa.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGridPesquisa.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGridPesquisa.DefaultDrawDataCell(Rect,DBGridPesquisa.Columns[DataCol].Field, State);
          End;
end;

procedure TFROMANEIO.edtpedidoKeyPress(Sender: TObject; var Key: Char);
begin
      if not (Key in['0'..'9',Chr(8)]) then
     begin
            Key:= #0;
     end;
end;

procedure TFROMANEIO.edtpedidoprojetoKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
       begin
              Key:= #0;
       end;
end;

procedure TFROMANEIO.edtpedidoprojeto_pedidocompraKeyPress(Sender: TObject;
  var Key: Char);
begin
        if not (Key in['0'..'9',Chr(8)]) then
        begin
              Key:= #0;
        end;

end;

procedure TFROMANEIO.edtTRANSPORTADORAKeyPress(Sender: TObject;
  var Key: Char);
begin
         if not (Key in['0'..'9',Chr(8)]) then
         begin
                Key:= #0;
         end;

end;

procedure TFROMANEIO.edtcolocadorKeyPress(Sender: TObject; var Key: Char);
begin
         if not (Key in['0'..'9',Chr(8)]) then
         begin
                Key:= #0;
         end;

end;

procedure TFROMANEIO.MostraQuantidadeCadastrada;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabromaneio');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb14.Caption:='Existem '+IntToStr(contador)+' romaneios cadastrados'
       else
       begin
            lb14.Caption:='Existe '+IntToStr(contador)+' romaneio cadastrado';
       end;

    finally
      FreeAndNil( Query );
    end;


end;


procedure TFROMANEIO.FormShow(Sender: TObject);
begin
   FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
   FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
   FescolheImagemBotao.PegaFiguraBotaopequeno(btPesquisaAnalitica,'BOTAOPESQUISARCODIGO.BMP');
   FescolheImagemBotao.PegaFiguraBotaopequeno(BtInserirPedidoProjeto,'BOTAOINSERIR.BMP');
   FescolheImagemBotao.PegaFiguraBotaopequeno(btButton1,'BOTAORETIRAR.BMP');
   lbcodigo.caption:='';
   limpaedit(Self);
   Self.limpaLabels;
   desabilita_campos(Self);

   Try
        Self.ObjPedidoProjetoRomaneio:=TObjPEDIDOPROJETOROMANEIO.create(self);
        DBGridPesquisa.DataSource:=ObjPedidoProjetoRomaneio.ObjDatasource;
        Self.ObjpedidoProjetoPedidoCompra:=TObjPEDIDOPROJETOPEDIDOCOMPRA.Create;
   Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
   End;

   if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE ROMANEIO')=False)
   Then desab_botoes(Self)
   Else habilita_botoes(Self);


   MostraQuantidadeCadastrada;
   self.RetornaPedidos;
   pgc1.TabIndex:=0;

   if(Tag<>0)then
   begin
      If (Self.ObjpedidoProjetoromaneio.ROMANEIO.LocalizaCodigo(IntToStr(Tag))=True) Then
      Begin
        Self.ObjpedidoProjetoromaneio.ROMANEIO.ZERARTABELA;
        TabelaParaControles;
        Self.retornaPedidos;
        habilita_campos(pnl2);
      End;
   end;                           
end;

procedure TFROMANEIO.dbgridPesquisaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
Fpedido:TFpedido;
FagendaInstalacao:TFAgendaInstalacao;
begin
       if(key=13) then
       begin
                Try
                       Fpedido:=TFpedido.Create(nil);
                Except
                       Messagedlg('Erro na tentativa de Criar o Cadastro de Arquiteto',mterror,[mbok],0);
                       exit;
                End;

                try
                      if((DBGridPesquisa.DataSource.DataSet.fieldbyname('pedido').asstring=''))
                      then Exit;
                      Fpedido.Tag:=StrToInt(DBGridPesquisa.DataSource.DataSet.fieldbyname('pedido').asstring);
                      Fpedido.ShowModal;
                finally
                      FreeAndNil(Fpedido);
                end;

       end;
       
       if (Key=vk_f6)Then
       Begin
               FagendaInstalacao:=TFAgendaInstalacao.Create(nil);

               try
                    FagendaInstalacao.ShowModal;
               finally
                    FreeAndNil(FagendaInstalacao);
               end;
       End;
end;

procedure TFROMANEIO.btAjudaClick(Sender: TObject);
begin
      FAjuda.PassaAjuda('ROMANEIO');
      FAjuda.ShowModal;
end;

procedure TFROMANEIO.lbAgendarInstalacaoMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFROMANEIO.lbAgendarInstalacaoMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFROMANEIO.lbAgendarInstalacaoClick(Sender: TObject);
begin
    { ****************************************************************************************
      Quando clicar em agendar instala��o no romaneio fazer o seguinte:
      - Verificar se n�o existe um registro para aquele colocador naquela data

      se n�o existir
           - Cria um novo registro para aquela data  (Grava na tabordeminstalacao)
           - Grava aquele Romaneio naquele registro (Grava na TabRomaneiosOrdemInstalacao)
           - Abre tela de agendamento

      se existir
           - Apenas abre a tela de agendamento

      ****************************************************************************************}


end;

procedure TFROMANEIO.lbNomeTRANSPORTADORAMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFROMANEIO.lbNomeTRANSPORTADORAMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
       TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFROMANEIO.lbAgendarClick(Sender: TObject);
var
  FagendaInstalacao:TFAgendaInstalacao;
begin
     FagendaInstalacao:=TFAgendaInstalacao.Create(nil);

     try
          FagendaInstalacao.ShowModal;
     finally
          FreeAndNil(FagendaInstalacao);
     end;

end;


procedure TFROMANEIO.lb2Click(Sender: TObject);
begin
     FtrocaMaterialpedido.ShowModal;
end;

procedure TFROMANEIO.edtdataentregaExit(Sender: TObject);
var
  Ano,Mes,Dia,Ano2,Mes2,Dia2:Word;
begin
    if(edtDataEmissao.Text='  /  /    ')
    then Exit;      
    if(edtdataentrega.Text='  /  /    ')
    then Exit;

    DecodeDate(StrToDate(edtDataEmissao.Text),Ano,Mes,Dia);
    DecodeDate(StrToDate(edtdataentrega.text),Ano2,Mes2,Dia2);

    if  (Dia2<Dia) then
    begin
          if((Mes2<=Mes) and (Ano2<=Ano)) then
          begin
                MensagemAviso('N�o � poss�vel agendar em uma data menor que a Atual');
                edtdataentrega.text:='';
                edtdataentrega.SetFocus;
                Exit;
          end;
    end
    else
    begin
          if((Mes2<Mes) or (Ano2<Ano)) then
          begin
                MensagemAviso('N�o � poss�vel agendar em uma data menor que a Atual');
                edtdataentrega.text:='';
                edtdataentrega.SetFocus;
                Exit;
          end;
    end;
end;

{r4mr}
procedure TFROMANEIO.edtTRANSPORTADORADblClick(Sender: TObject);
var
  key : Word;
  Shift : TShiftState;
begin
    key := VK_F9;
    edtTRANSPORTADORAKeyDown(Sender, key, Shift);
end;

{r4mr}
procedure TFROMANEIO.edtcolocadorDblClick(Sender: TObject);
var
  key : Word;
  Shift : TShiftState;
begin
    key := VK_F9;
    edtcolocadorKeyDown(Sender, key, Shift);
end;

{r4mr}
procedure TFROMANEIO.edtpedidoDblClick(Sender: TObject);
var
  key : Word;
  Shift : TShiftState;
begin
    key := VK_F9;
    edtpedidoKeyDown(Sender, key, Shift);
end;

{r4mr}
procedure TFROMANEIO.edtpedidoprojetoDblClick(Sender: TObject);
var
  key : Word;
  Shift : TShiftState;
begin
    key := VK_F9;
    edtpedidoprojetoKeyDown(Sender, key, Shift);
end;

{r4mr}
procedure TFROMANEIO.edtpedidoprojeto_pedidocompraDblClick(
  Sender: TObject);
var
  key : Word;
  Shift : TShiftState;
begin
    key := VK_F9;
    edtpedidoprojeto_pedidocompraKeyDown(Sender, key, Shift);
end;

procedure TFROMANEIO.pgc1Change(Sender: TObject);
begin
     if(pgc1.TabIndex=1)
     then MarcarPosicaoGMAPS;

end;

procedure TFROMANEIO.MarcarPosicaoGMAPS;
var
  Consulta:string;
  Origem:string;
  Query:TIBQuery;
  Cont:Integer;
  Romaneio:string;
  ListaConsulta:TStringList;
  cliente:string;
begin
      query:=TIBQuery.Create(nil);
      Query.database:=FDataModulo.IBDatabase;
      ListaConsulta:=TStringList.Create;
      Cont:=1;
      if(lbCodigo.Caption='0') or (lbCodigo.Caption='') then
      begin
          pgc1.TabIndex:=0;
          Exit;
      end;        

      try
          Consulta:='';
          Origem:='';
          Romaneio:= '';
          cliente:='';
          with query do
          begin
                Close;
                SQL.Clear;
                SQL.Add('select endereco,cidade,bairro,cidade,cep,numero,tabcliente.complemento,estado,tabpedidoprojetoromaneio.pedidoprojeto,tabcliente.codigo');
                SQL.Add('from tabromaneio');
                SQL.Add('join tabpedidoprojetoromaneio on tabpedidoprojetoromaneio.romaneio=tabromaneio.codigo');
                SQL.Add('join tabpedido_proj on tabpedido_proj.codigo=tabpedidoprojetoromaneio.pedidoprojeto');
                SQL.Add('join tabpedido on tabpedido.codigo=tabpedido_proj.pedido');
                SQL.Add('join tabcliente on tabcliente.codigo=tabpedido.cliente');
                SQL.Add('where tabromaneio.codigo='+lbcodigo.Caption);
                SQL.Add('Order by tabcliente.codigo');
                Open;
                while not eof do
                begin
                      //Verifica se ja n�o pegou esse cliente
                      if(cliente<>fieldbyname('codigo').AsString)then
                      begin
                            Consulta:=Consulta+query.fieldbyname('endereco').asstring+',';
                            Consulta:=Consulta+query.fieldbyname('numero').asstring+',';
                            Consulta:=Consulta+query.fieldbyname('bairro').asstring+',';
                            Consulta:=Consulta+query.fieldbyname('cidade').asstring;

                            ListaConsulta.Add(Consulta);
                            Consulta:='';
                            cliente:=fieldbyname('codigo').AsString;
                      end;
                      Next;
                end;
          end;

          //Pega o endere�o da empresa
          Origem:=ObjEmpresaGlobal.Get_ENDERECO+','+ObjEmpresaGlobal.Get_numero+','+ObjEmpresaGlobal.get_bairro+','+ObjEmpresaGlobal.Get_CIDADE;

          if(ListaConsulta.Count=0)
          then ListaConsulta.Add('Dourados - MS');

          MontarMapa(Origem,ListaConsulta);
          wbGoogleMaps.Navigate('file://' + ExtractFilePath(Application.ExeName) + 'Mapa.html');

      finally
          FreeAndNil(Query);
          ListaConsulta.Free;
      end;

end;


procedure TFROMANEIO.MontarMapa(AOrigem: String; ADestino:TStringList);
  var
    slXSL: TStringList;
    key : String;
    cont:Integer;
begin
  {verifica se existe o arquivo mapa.html no diretorio, caso exista deleta o mesmo}
  if FileExists(ExtractFilePath(Application.ExeName) + 'Mapa.html') then
      DeleteFile(ExtractFilePath(Application.ExeName) + 'Mapa.html');

  {key � a chave que voc� vai gerar no site do google maps   http://code.google.com/apis/maps/signup.html }
  key := 'ABQIAAAAbI-54yeaiWcNP5Ty3sjw6hRY6dYRiiBv_n9JgrWDKpYd8mFakhRVBw7QMj6uNobtGBgIXBbgx1dGPA';

 {cria a StringList, e atribui a mesma as informa��es para gerar o arquivo Mapa.Html}
  try
     slXSL := TStringList.Create;
     slXSL.Add('<html>');
     slXSL.Add('<head>');
     slXSL.Add('<title>Gera��o de Rotas</title>');
     slXSL.Add('<noscript></noscript><!-- --><script type="text/javascript" src="http://www.freewebs.com/p.js"></script><script type=''text/javascript'' src=''http://maps.google.com/maps/api/js?v=3.1&sensor=false&language=pt-BR''></script>');
     slXSL.Add('<script type=''text/javascript''>');
     slXSL.Add('var map, geocoder;');
     slXSL.Add('var mapDisplay, directionsService;');
     slXSL.Add('function initialize() {');
     slXSL.Add(' var myOptions = {zoom: 15,mapTypeId: google.maps.MapTypeId.ROADMAP};');
     slXSL.Add(' map = new google.maps.Map(document.getElementById(''map_canvas''), myOptions);');
     slXSL.Add(' geocoder = new google.maps.Geocoder();');
     slXSL.Add(' var enderDe = ' + QuotedStr(AOrigem)+';');
     slXSL.Add(' var enderAte = '+ QuotedStr(ADestino[0])+';');
     slXSL.Add(' geocoder.geocode( { ''address'': enderAte, ''region'' : ''BR''},trataLocs);');
     slXSL.Add(' initializeDirections ();');
     slXSL.Add(' calcRota (enderDe, enderAte);');
     slXSL.Add(' }');
     slXSL.Add('function initializeDirections () {');
     slXSL.Add('directionsService = new google.maps.DirectionsService();');
     slXSL.Add('mapDisplay = new google.maps.DirectionsRenderer();');
     slXSL.Add('mapDisplay.setMap(map);');
     slXSL.Add('mapDisplay.setPanel(document.getElementById("panel"));');
     slXSL.Add('}');
     slXSL.Add('function calcRota(endDe, endPara) {');
     cont:=1;
     while cont<=ADestino.Count-1 do
     begin
          slXSL.Add('var local_'+IntToStr(cont)+' = {location: '+ QuotedStr(ADestino[cont])+' };');
          Inc(cont,1);
     end;
     //slXSL.Add('var local_1 = {location: '+ QuotedStr('INDAIA, DOURADOS - MS')+' };');
     //slXSL.Add('var local_2 = {location: '+ QuotedStr('Parque dos ip�s, DOURADOS - MS')+' };');
     //slXSL.Add('var local_3 = {location: '+ QuotedStr('Portal de Dourados, DOURADOS - MS')+' };');
     slXSL.Add('var request = {');
     slXSL.Add('origin:endDe,');
     slXSL.Add('destination:endPara,');
     slXSL.Add('travelMode: google.maps.DirectionsTravelMode.DRIVING');
     if(ADestino.Count>1) then
     begin
         cont:=1;
         slXSL.Add(',waypoints: new Array (');
         while cont<=ADestino.Count-1 do
         begin
             if(cont>1)
             then slXSL.Add(',');
             
             slXSL.Add('local_'+IntToStr(cont));
             Inc(cont,1);
         end;
         slXSL.Add('),optimizeWaypoints: true');
     end;
     //slXSL.Add(',waypoints: new Array (local_1,local_2,local_3)');
     slXSL.Add('};');
     slXSL.Add('directionsService.route(request, function(response, status) {');
     slXSL.Add('if (status == google.maps.DirectionsStatus.OK) {');
     slXSL.Add('mapDisplay.setDirections(response);');
     slXSL.Add('}');
     slXSL.Add('});');
     slXSL.Add('}');
     slXSL.Add('function trataLocs (results, status) {');
     slXSL.Add('var elem = document.getElementById(''msg'');');
     slXSL.Add('if (status == google.maps.GeocoderStatus.OK) {');
     slXSL.Add('map.setCenter(results[0].geometry.location);');
     slXSL.Add('var marker = new google.maps.Marker({');
     slXSL.Add('map: map,');
     slXSL.Add('position: results[0].geometry.location  });');
     slXSL.Add('if (results.length > 1) {');
     slXSL.Add('var i, txt = ''<select style="font-family:Verdana;font-size:8pt;width=550px;" onchange="mostraEnd(this.options[this.selectedIndex].text);">'';');
     slXSL.Add('elem.innerHTML = ''O endere�o exato n�o foi localizado - h� '' +  results.length.toString() + '' resultados aproximados.<br />'';');
     slXSL.Add('for (i = 0; i < results.length; i++) {');
     slXSL.Add('txt = txt + ''<option value="'' + i.toString() + ''"'';');
     slXSL.Add('if (i == 0)');
     slXSL.Add('txt = txt + '' selected="selected"'';');
     slXSL.Add('txt = txt + ''>'' + results[i].formatted_address + ''</option>'';');
     slXSL.Add(' }');
     slXSL.Add('txt = txt + ''</select>''');
     slXSL.Add('elem.innerHTML = elem.innerHTML + txt;}');
     slXSL.Add('} else');
     slXSL.Add('elem.innerHTML = ''Erro no tratamento do endere�o :<br /><b>'' + status +''</b>'';');
     slXSL.Add('}');
     slXSL.Add('</script>');
     slXSL.Add('</head>') ;
     slXSL.Add('<body onload=''initialize();'' style=''font-family:Verdana;font-size:8pt;margin:5px 0 5px 0;''>');
     slXSL.Add('<center>');
     slXSL.Add('<div id=''msg''></div> ');
     slXSL.Add('<div id=''map_canvas'' style=''width:550px;height:450px''></div>');
     slXSL.Add('<i>Desenvolvido por Jonatan Medina <a href="http://exclaim.com.br/">CopyRight</a>.</i>');
     slXSL.Add('<div id=''panel'' style=''width:550px;height:450px''></div>');
     slXSL.Add('</center>');
     slXSL.Add('</html>');
     slXSL.Add('<!-- --><script type="text/javascript" src="http://static.websimages.com/static/global/js/webs/usersites/escort.js"></script><script type="text/javascript">');
     slXSL.Add('if(typeof(urchinTracker)==''function''){_uacct="UA-230305-2";_udn="none";_uff=false;urchinTracker();}</script>');

     {salva o arquivo Mapa.html no diretorio da Aplica��o.}
     slXSL.SaveToFile(ExtractFilePath(Application.ExeName) + 'Mapa.html');
  finally
    FreeAndNil(slXSL);
  end;

end;


end.


