unit UessencialLocal;

interface
uses Dialogs, Buttons, Forms, STDCTRLS, Classes, UessencialGlobal, Controls,Windows,TLHelp32
,SysUtils,PsAPI;


Procedure Coloca_Atalho_Botoes(Botao : TSpeedButton; Letra: string);
procedure ColocaUpperCaseEdit(FORMULARIO:Tform);
Procedure PreencheStringList(Var PStrList:TStringList; PComplemento: String);
Function MascaraData:string;
Function MascaraCNPJ:string;
Function MascaraCPF:string;
Function MascaraCEP:string;
Function SomenteNumero(Texto:string):string;
Function FormataCPF(Numero: string): string;
Function FormataCNPJ(Numero: String): String;


implementation

Procedure Coloca_Atalho_Botoes(Botao : TSpeedButton; Letra: string);
//Coloca o alt na letra que vem como parametro
//usado para speedbotom que o caption � na propria imagem
begin
  Botao.Font.Name:='Agency FB';
  Botao.Font.Size:=3;
  Botao.Font.Color:=$00999999;
  Botao.Caption:='&'+Letra;
end;

procedure ColocaUpperCaseEdit(FORMULARIO:Tform);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ComponentCount -1
   do Begin
        if FORMULARIO.Components [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Components [int_habilita]).Charcase:=ecUppercase;
   end;
end;

Procedure PreencheStringList(Var PStrList:TStringList; PComplemento: String);
Var COnt:Integer;
    Texto:string;
begin
     PStrList.Clear;
     Texto:='';
     for Cont:= 1 to Length(PComplemento) do
     Begin
          if (PComplemento[Cont]<> #13)
          then begin
              Texto:=Texto+PComplemento[Cont];
          end else
          begin
              PStrList.Add(Texto);
              Texto:='';
          end;
     end;
     PStrList.Add(Texto);
end;


Function MascaraData:string;
Begin
   Result:='!99/99/9999;1;';
end;

Function MascaraCNPJ:string;
Begin
   Result:='!99.999.999/9999-99;1;';
end;

Function MascaraCPF:string;
Begin
   Result:='!999.999.999-99;1;';
end;

Function MascaraCEP:string;
Begin
    Result:='!99.999-999;1;';
end;

Function SomenteNumero(Texto:string):string;
Var Cont : Integer;
Begin
      Result:='';
      for  Cont:=1 to Length(Texto) do
      Begin
          if (texto[cont] in ['0'..'9'])then
          Result:=Result+Texto[cont];
      end;

end;


Function FormataCNPJ(Numero: String): String;
var tmp,resultado: String;
    indx, indx1: integer;
begin
      for indx := 1 to Length(Numero) do
      begin
         if Numero[indx] in ['0'..'9'] Then
         resultado := resultado + Numero[indx];
      end;

      if Length(Resultado) <= 14 Then
      Begin
          resultado := StringOfChar('0', 14 - Length(Resultado)) + resultado;
          tmp := Copy(resultado,1,2) + '.';
          tmp := tmp + Copy(resultado,3,3) + '.';
          tmp := tmp + Copy(resultado,6,3) + '/';
          tmp := tmp + Copy(resultado,9,4) + '-' + Copy(resultado,13,2);
      end;
      Result := tmp;
end;


Function FormataCPF(Numero: string): string;
var
    tmp,resultado: string;
    indx, indx1: integer;
Begin
    for indx := 1 to Length(Numero) do
    begin
        if Numero[indx] in ['0'..'9'] Then
        resultado := resultado + Numero[indx];
    end;

    if Length(Resultado) = 11 Then
    Begin
        resultado := StringOfChar('0', 11 - Length(Resultado)) + resultado;
        tmp := Copy(resultado,1,3) + '.';
        tmp := tmp + Copy(resultado,4,3) + '.';
        tmp := tmp + Copy(resultado,7,3) + '-';
        tmp := tmp + Copy(resultado,10,2);
        Result := tmp;
    end else
    Begin
         MensagemErro('O CPF ou CNPJ '+resultado+' n�o � v�lido');
    end;
end;


end.
