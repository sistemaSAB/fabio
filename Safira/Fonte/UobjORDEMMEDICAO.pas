unit UobjORDEMMEDICAO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UobjCLIENTE,UobjPEDIDO,UobjVENDEDOR,UobjFuncionarios,UobjRELPERSREPORTBUILDER,UFiltraImp,UReltxtRDPRINT,rdprint
;
//USES_INTERFACE


Type
   TObjORDEMMEDICAO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                PEDIDO                                      :TObjPEDIDO;
                VENDEDOR                                    :TObjVENDEDOR;
                MEDIDOR                                     :TObjFUNCIONARIOS;
                CLIENTE                                     :TObjCLIENTE;
                                                             //CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);
                procedure BotaoOpcoes(pcodigo: string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_OBSERVACAO(parametro: string);
                Function Get_OBSERVACAO: string;
                Procedure Submit_DESCRICAO(parametro: string);
                Function Get_DESCRICAO: string;
                Procedure Submit_CONCLUIDO(parametro: string);
                Function Get_CONCLUIDO: string;

                procedure Submit_Datapedidoemissao(parametro:string);
                function Get_DataPedidoEmissao:string;
                procedure Submit_DataMedicao(parametro:string);
                Function Get_DataMedicao:string;
                procedure Submit_Endereco(parametro:string);
                Function Get_Endereco:string;
                procedure Submit_Cidade(parametro:string);
                function Get_Cidade:string;
                procedure Submit_UF(parametro:string);
                Function Get_UF:string;
                procedure Submit_Bairro(parametro:String);
                Function Get_Bairro:string;
                Procedure Submit_numero(parametro:string);
                function Get_Numero:string;
                procedure Submit_Duracao(parametro:String);
                procedure Submit_HorarioTermino(parametro:string);
                procedure Submit_HorarioMedicao(parametro:string);
                function Get_Duracao:string;
                Function Get_HorarioTermino:string;
                function Get_HorarioMedicao:String;
                procedure Submit_Complemento(parametro:String);
                Function Get_Complemento:string;


                Procedure EmitirOrdemMedicao(parametro:string);
                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               OBSERVACAO:string;
               DESCRICAO:string;
               CONCLUIDO:string;
               DATAPEDIDOEMISSAO:string;
               datamedicao:string;
               ENDERECO:string;
               BAIRRO:string;
               UF:string;
               CIDADE:string;
               NUMERO:string;
               Duracao:string;
               HorarioTermino:string;
               HorarioMedicao:string;
               complemento:String;


//CODIFICA VARIAVEIS PRIVADAS


               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                procedure ImprimeOrdensMedicao;
   End;


implementation
uses UopcaoRel,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, Mask;





Function  TObjORDEMMEDICAO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;

        If(fieldbyname('PEDIDO').asstring<>'')
        Then Begin
                 If (Self.PEDIDO.LocalizaCodigo(fieldbyname('PEDIDO').asstring)=False)
                 Then Begin
                          Messagedlg('PEDIDO N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PEDIDO.TabelaparaObjeto;
        End;
        If(fieldbyname('VENDEDOR').asstring<>'')
        Then Begin
                 If (Self.VENDEDOR.LocalizaCodigo(fieldbyname('VENDEDOR').asstring)=False)
                 Then Begin
                          Messagedlg('PEDIDO N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.VENDEDOR.TabelaparaObjeto;
        End;
        If(fieldbyname('MEDIDOR').asstring<>'')
        Then Begin
                 If (Self.MEDIDOR.LocalizaCodigo(fieldbyname('MEDIDOR').asstring)=False)
                 Then Begin
                          Messagedlg('PEDIDO N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.MEDIDOR.TabelaparaObjeto;
        End;
         If(fieldbyname('CLIENTE').asstring<>'')
        Then Begin
                 If (Self.CLIENTE.LocalizaCodigo(fieldbyname('CLIENTE').asstring)=False)
                 Then Begin
                          Messagedlg('PEDIDO N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CLIENTE.TabelaparaObjeto;
        End;


        Self.OBSERVACAO:=fieldbyname('OBSERVACAO').asstring;
        Self.DESCRICAO:=fieldbyname('DESCRICAO').asstring;
        Self.CONCLUIDO:=fieldbyname('CONCLUIDO').asstring;
        self.DATAPEDIDOEMISSAO:=fieldbyname('datapedidoemissao').AsString;
        self.datamedicao:=fieldbyname('datamedicao').AsString;
        self.NUMERO:=fieldbyname('numero').AsString;
        self.ENDERECO:=fieldbyname('endereco').AsString;
        self.UF:=fieldbyname('uf').AsString;
        self.BAIRRO:=fieldbyname('bairro').AsString;
        self.CIDADE:=fieldbyname('cidade').AsString;
        self.HorarioTermino:=fieldbyname('horariotermino').AsString;
        self.HorarioMedicao:=fieldbyname('horariomedicao').AsString;
        self.Duracao:=fieldbyname('duracao').AsString;
        self.complemento:=fieldbyname('complemento').AsString;
//CODIFICA TABELAPARAOBJETO


        result:=True;
     End;
end;


Procedure TObjORDEMMEDICAO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('PEDIDO').asstring:=Self.PEDIDO.Get_Codigo;
        ParamByName('VENDEDOR').asstring:=Self.VENDEDOR.Get_Codigo;
        ParamByName('MEDIDOR').asstring:=Self.MEDIDOR.Get_Codigo;
        ParamByName('CLIENTE').asstring:=Self.CLIENTE.Get_Codigo;
        ParamByName('OBSERVACAO').asstring:=Self.OBSERVACAO;
        ParamByName('DESCRICAO').asstring:=Self.DESCRICAO;
        ParamByName('CONCLUIDO').asstring:=Self.CONCLUIDO;
        ParamByName('datapedidoemissao').AsString:=Self.DATAPEDIDOEMISSAO;
        parambyname('datamedicao').asstring:=self.datamedicao;
        ParamByName('numero').AsString:=Self.NUMERO;
        ParamByName('endereco').AsString:=Self.ENDERECO;
        ParamByName('cidade').AsString:=self.CIDADE;
        ParamByName('bairro').AsString:=self.BAIRRO;
        ParamByName('uf').AsString:=self.UF;
        ParamByName('horariotermino').AsString:=Self.HorarioTermino;
        ParamByName('horariomedicao').AsString:=self.HorarioMedicao;
        ParamByName('duracao').AsString:=Self.Duracao;
        ParamByName('complemento').AsString:=Self.complemento;

//CODIFICA OBJETOPARATABELA

  End;
End;

//***********************************************************************

function TObjORDEMMEDICAO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjORDEMMEDICAO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        PEDIDO.ZerarTabela;
        VENDEDOR.ZerarTabela;
        MEDIDOR.ZerarTabela;
        CLIENTE.ZerarTabela;
        OBSERVACAO:='';
        DESCRICAO:='';
        CONCLUIDO:='';
        DATAPEDIDOEMISSAO:='';
        datamedicao:='';
        NUMERO:='';
        CIDADE:='';
        BAIRRO:='';
        UF:='';
        ENDERECO:='';
        HorarioTermino:='';
        HorarioMedicao:='';
        Duracao:='';
        complemento := '';

//CODIFICA ZERARTABELA


     End;
end;

Function TObjORDEMMEDICAO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjORDEMMEDICAO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjORDEMMEDICAO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;

//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjORDEMMEDICAO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
    
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjORDEMMEDICAO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjORDEMMEDICAO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro ORDEMMEDICAO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,PEDIDO,VENDEDOR,MEDIDOR,CLIENTE,OBSERVACAO,DESCRICAO');
           SQL.ADD(' ,CONCLUIDO,DATAC,USERC,DATAM,USERM,datapedidoemissao,datamedicao');
           SQL.Add(',endereco,cidade,bairro,uf,numero,horariomedicao,horariotermino,duracao,complemento') ;
           SQL.ADD(' from  TABORDEMMEDICAO');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjORDEMMEDICAO.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjORDEMMEDICAO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjORDEMMEDICAO.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjORDEMMEDICAO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        PEDIDO:=TObjPEDIDO.Create;
        VENDEDOR:=TObjVENDEDOR.Create;
        MEDIDOR:=TObjFUNCIONARIOS.Create;
        CLIENTE:=TObjCLIENTE.Create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABORDEMMEDICAO(CODIGO,PEDIDO,VENDEDOR,MEDIDOR');
                InsertSQL.add(' ,CLIENTE,OBSERVACAO,DESCRICAO,CONCLUIDO,DATAC,USERC');
                InsertSQL.add(' ,DATAM,USERM,datapedidoemissao,datamedicao,endereco,cidade,bairro,uf,numero');
                InsertSql.Add(' ,horariotermino,horariomedicao,duracao,complemento)');
                InsertSQL.add('values (:CODIGO,:PEDIDO,:VENDEDOR,:MEDIDOR,:CLIENTE');
                InsertSQL.add(' ,:OBSERVACAO,:DESCRICAO,:CONCLUIDO,:DATAC,:USERC,:DATAM');
                InsertSQL.add(' ,:USERM,:datapedidoemissao,:datamedicao,:endereco,:cidade,:bairro,:uf,:numero');
                InsertSql.Add(' ,:horariotermino,:horariomedicao,:duracao,:complemento)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABORDEMMEDICAO set CODIGO=:CODIGO,PEDIDO=:PEDIDO');
                ModifySQL.add(',VENDEDOR=:VENDEDOR,MEDIDOR=:MEDIDOR,CLIENTE=:CLIENTE');
                ModifySQL.add(',OBSERVACAO=:OBSERVACAO,DESCRICAO=:DESCRICAO,CONCLUIDO=:CONCLUIDO');
                ModifySQL.add(',DATAC=:DATAC,USERC=:USERC,DATAM=:DATAM,USERM=:USERM,datapedidoemissao=:datapedidoemissao,datamedicao=:datamedicao');
                ModifySQl.Add(',endereco=:endereco,cidade=:cidade,bairro=:bairro,uf=:uf,numero=:numero');
                ModifySQL.add(',horariotermino=:horariotermino,horariomedicao=:horariomedicao,duracao=:duracao');
                ModifySQL.add(',complemento=:complemento');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABORDEMMEDICAO where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjORDEMMEDICAO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjORDEMMEDICAO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     {
     self.ParametroPesquisa.Add('select TabORDEMMEDICAO.codigo,tabordemmedicao.DESCRICAO,tabcliente.nome as nomecliente ,tabvendedor.nome as nomevendedor,tabfuncionarios.nome as nomemedidor');
     self.ParametroPesquisa.Add(',TabORDEMMEDICAO.cliente,tabordemmedicao.vendedor,tabordemmedicao.medidor,tabordemmedicao.datapedidoemissao,tabordemmedicao.datamedicao');
     self.ParametroPesquisa.Add(',horariomedicao,horariotermino,duracao');
     Self.ParametroPesquisa.add('from TabORDEMMEDICAO');
     self.ParametroPesquisa.Add('join tabcliente on tabcliente.codigo=tabordemmedicao.cliente');
     self.ParametroPesquisa.Add('join tabfuncionarios on tabfuncionarios.codigo=tabordemmedicao.medidor');
     Self.ParametroPesquisa.Add('join tabvendedor on tabvendedor.codigo=tabordemmedicao.vendedor');
     }
     Self.ParametroPesquisa.Add('SELECT * FROM VIEW_ORDEMMEDICAO');

     Result:=Self.ParametroPesquisa;
end;

function TObjORDEMMEDICAO.Get_TituloPesquisa: string;
begin
     Result:='Pesquisa de ORDEMMEDICAO';
end;


function TObjORDEMMEDICAO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENORDEMMEDICAO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENORDEMMEDICAO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjORDEMMEDICAO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    PEDIDO.Free;
    VENDEDOR.Free;
    MEDIDOR.Free;
    CLIENTE.Free;
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjORDEMMEDICAO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjORDEMMEDICAO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjORDEMMEDICAO.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjORDEMMEDICAO.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;

procedure TObjORDEMMEDICAO.Submit_OBSERVACAO(parametro: string);
begin
        Self.OBSERVACAO:=Parametro;
end;
function TObjORDEMMEDICAO.Get_OBSERVACAO: string;
begin
        Result:=Self.OBSERVACAO;
end;
procedure TObjORDEMMEDICAO.Submit_DESCRICAO(parametro: string);
begin
        Self.DESCRICAO:=Parametro;
end;
function TObjORDEMMEDICAO.Get_DESCRICAO: string;
begin
        Result:=Self.DESCRICAO;
end;
procedure TObjORDEMMEDICAO.Submit_CONCLUIDO(parametro: string);
begin
        Self.CONCLUIDO:=Parametro;
end;
function TObjORDEMMEDICAO.Get_CONCLUIDO: string;
begin
        Result:=Self.CONCLUIDO;
end;

//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjORDEMMEDICAO.Imprime(Pcodigo: string);
begin

     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJORDEMMEDICAO';

          With RgOpcoes do
          Begin
                items.clear;
                Items.Add('Ordens de medi��o');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:ImprimeOrdensMedicao;
          End;
     end;

end;

procedure TObjORDEMMEDICAO.BotaoOpcoes(pcodigo: string);
begin
     if(pcodigo='')
     then Exit;

     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Emitir Ordem de Medi��o');

          showmodal;
          if (tag=0)
          Then exit;
          Case RgOpcoes.ItemIndex of
               0:Self.EmitirOrdemMedicao(pcodigo);
          End;
     End;
end;

procedure TObjORDEMMEDICAO.Submit_Datapedidoemissao(parametro:string);
begin
    Self.DATAPEDIDOEMISSAO:=parametro;
end;

procedure TObjORDEMMEDICAO.Submit_DataMedicao(parametro:string);
begin
    self.datamedicao:=parametro;
end;

function TObjORDEMMEDICAO.Get_DataPedidoEmissao:string;
begin
    Result:=Self.DATAPEDIDOEMISSAO;
end;

function TObjORDEMMEDICAO.Get_DataMedicao:string;
begin
    Result:=self.datamedicao;
end;

procedure TObjORDEMMEDICAO.EmitirOrdemMedicao(parametro:string);
var
  ObjRelPersReportBuilder:TObjRELPERSREPORTBUILDER;
begin
      if(parametro='')
      then Exit;
      try

            try
                   ObjRelPersreportBuilder:=TObjRELPERSREPORTBUILDER.Create;
            except
                   MensagemErro('Erro ao tentar criar o Objeto ObjRelPersreportBuilder');
                   exit;
            end;

            if (ObjRelPersReportBuilder.LocalizaNOme('ORDEM DE MEDICAO') = false)
            then Begin
                       MensagemErro('O Relatorio de ReporteBuilder "ORDEM DE MEDICAO" n�o foi econtrado');
                       exit;
            end;

            ObjRelPersReportBuilder.TabelaparaObjeto;
            ObjRelPersReportBuilder.SQLCabecalhoPreenchido:=StrReplace(ObjRelPersReportBuilder.Get_SQLCabecalho,':PCODIGO',parametro);
            ObjRelPersReportBuilder.SQLRepeticaoPreenchido:=StrReplace(ObjRelPersReportBuilder.get_SQLRepeticao,':PCODIGO',parametro);
            ObjRelPersReportBuilder.ChamaRelatorio(false);


      finally
            ObjRelPersreportBuilder.Free;
      end;
end;

procedure TObjORDEMMEDICAO.Submit_Endereco(parametro:string);
begin
    ENDERECO:=parametro;
end;

procedure TObjORDEMMEDICAO.Submit_Cidade(parametro:string);
begin
   CIDADE:=parametro;
end;

procedure TObjORDEMMEDICAO.Submit_UF(parametro:string);
begin
    uf:=parametro;
end;

procedure TObjORDEMMEDICAO.Submit_Bairro(parametro:string);
begin
    BAIRRO:=parametro;
end;

procedure TObjORDEMMEDICAO.Submit_numero(parametro:String);
begin
    NUMERO:=parametro;
end;

function TObjORDEMMEDICAO.Get_Endereco:string;
begin
    Result:=ENDERECO;
end;

function TObjORDEMMEDICAO.Get_Cidade:string;
begin
    Result:=CIDADE;
end;

function TObjORDEMMEDICAO.Get_UF:string;
begin
    Result:=UF;
end;

function TObjORDEMMEDICAO.Get_Bairro:string;
begin
    Result:=BAIRRO;
end;

function TObjORDEMMEDICAO.Get_Numero:string;
begin
    Result:=NUMERO;
end;

procedure TObjORDEMMEDICAO.ImprimeOrdensMedicao;
var
  ClientePesq,vendedorPesq,medidorPesq:String;
  DataPedidoEmissaoPesq,DataPedidoEmissaoPesqFim,DataMedicaoPesq,DataMedicaoPesqFim:string;
  medicaoConcluida,comOrcamento:Boolean;
  Query:TIBQuery;
  Linha:Integer;
begin
   Query:=TIBQuery.Create(nil);
   Query.Database:=FDataModulo.IBDatabase;

   try
        with FfiltroImp do
        begin
              DesativaGrupos;
              Grupo01.Enabled:=True;
              LbGrupo01.Caption:='Cliente';
              edtgrupo01.OnKeyDown:=PEDIDO.EdtClienteKeyDown;
              edtgrupo01.Color:=$005CADFE;
              Grupo02.Enabled:=True;
              LbGrupo02.Caption:='Data Emiss�o Inicio';
              edtgrupo02.EditMask:=MascaraData ;
              Grupo03.Enabled:=True;
              LbGrupo03.Caption:='Data Emiss�o Fim';
              edtgrupo03.EditMask:=MascaraData;
              Grupo04.Enabled:=True;
              LbGrupo04.Caption:='Data Medi��o Inicial';
              edtgrupo04.EditMask:=MascaraData;
              Grupo05.Enabled:=True;
              LbGrupo05.Caption:='Data da Medi��o Final';
              edtgrupo05.EditMask:=MascaraData;
              Grupo08.Enabled:=True;
              lbgrupo08.Caption:='Vendedor';
              edtgrupo08.OnKeyDown:=PEDIDO.EdtVendedorKeyDown;
              edtgrupo08.Color:=$005CADFE;

              grupo09.Enabled:=True;
              lbgrupo09.Caption:='Medidor';
              edtgrupo09.OnKeyDown:=MEDIDOR.EdtFuncionariosKeyDown;
              edtgrupo09.Color:=$005CADFE;

              medicaoConcluida := false;
              Grupo06.Enabled := True;
              LbGrupo06.Caption := 'Conclu�dos';
              Combo2Grupo06.Visible := false;
              ComboGrupo06.Items.Clear;
              ComboGrupo06.Items.Add('N�o');
              ComboGrupo06.Items.Add('Sim');
              ComboGrupo06.ItemIndex := 0;

              comOrcamento := false;
              Grupo07.Enabled := True;
              LbGrupo07.Caption := 'c/ Or�amento';
              edtgrupo07.Visible := false;
              ComboGrupo07.Items.Clear;
              ComboGrupo07.Items.Add('N�o');
              ComboGrupo07.Items.Add('Sim');
              ComboGrupo07.ItemIndex := 0;

              ShowModal;

              if(Tag=0)
              then Exit;

              if(edtgrupo01.Text<>'')
              then ClientePesq:=edtgrupo01.Text
              else ClientePesq:='';

              if(edtgrupo02.Text<>'')
              then DataPedidoEmissaoPesq:=edtgrupo02.Text
              else DataPedidoEmissaoPesq:='';

              if(edtgrupo03.Text<>'')
              then DataPedidoEmissaoPesqfim:=edtgrupo03.Text
              else DataPedidoEmissaoPesqfim:='';

              if(edtgrupo04.Text<>'')
              then DataMedicaoPesq:=edtgrupo04.Text
              else DataMedicaoPesq:='';

              if(edtgrupo05.Text<>'')
              then DataMedicaoPesqFim:=edtgrupo05.Text
              else DataMedicaoPesqFim:='';

              if(edtgrupo08.Text<>'')
              then vendedorPesq:=edtgrupo08.Text
              else vendedorPesq:='';

              if(edtgrupo09.Text<>'')
              then medidorPesq:=edtgrupo09.Text
              else medidorPesq:='';

              if(ComboGrupo06.ItemIndex = 1) then
                medicaoConcluida := True;

              if(ComboGrupo07.ItemIndex = 1) then
                comOrcamento := True;


        end;

        with Query do
        begin
              Close;
              SQL.Clear;
              SQL.Add('select TabORDEMMEDICAO.codigo,tabordemmedicao.DESCRICAO,tabcliente.nome as nomecliente ,tabvendedor.nome as nomevendedor,tabfuncionarios.nome as nomemedidor');
              SQL.Add(',TabORDEMMEDICAO.cliente,tabordemmedicao.vendedor,tabordemmedicao.medidor,tabordemmedicao.datapedidoemissao,tabordemmedicao.datamedicao');
              SQL.Add(',TabORDEMMEDICAO.endereco,TabORDEMMEDICAO.cidade,TabORDEMMEDICAO.uf,TabORDEMMEDICAO.numero,TabORDEMMEDICAO.bairro,TabORDEMMEDICAO.complemento');
              SQL.Add('from TabORDEMMEDICAO');
              SQL.Add('join tabcliente on tabcliente.codigo=tabordemmedicao.cliente');
              SQL.Add('join tabfuncionarios on tabfuncionarios.codigo=tabordemmedicao.medidor');
              SQL.Add('join tabvendedor on tabvendedor.codigo=tabordemmedicao.vendedor');
              SQL.Add('where TabORDEMMEDICAO.codigo>-1');
              if(ClientePesq<>'')
              then SQL.Add('and tabordemmedicao.cliente='+ClientePesq);
              if(vendedorPesq<>'')
              then SQL.Add('and tabordemmedicao.vendedor='+vendedorPesq);
              if(medidorPesq<>'')
              then SQL.Add('and tabordemmedicao.medidor='+medidorPesq);
              if(DataMedicaoPesq<>'  /  /    ')
              then SQL.Add('and tabordemmedicao.datamedicao>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(DataMedicaoPesq))+#39);
              if(DataMedicaoPesqFim<>'  /  /    ')
              then SQL.Add('and tabordemmedicao.datamedicao<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(DataMedicaoPesqFim))+#39);
              if(DataPedidoEmissaoPesq<>'  /  /    ')
              then SQL.Add('and tabordemmedicao.datapedidoemissao>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(DataPedidoEmissaoPesq))+#39);
              if(DataPedidoEmissaoPesqFim<>'  /  /    ' )
              then SQL.Add('and tabordemmedicao.datapedidoemissao<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(DataPedidoEmissaoPesqFim))+#39);

              if(comOrcamento) then
                sql.Add(' and (tabordemmedicao.pedido is not null or tabordemmedicao.pedido <> '+QuotedStr('')+')')
              else
                if(medicaoConcluida) then
                  sql.Add(' and tabordemmedicao.concluido='+QuotedStr('S'))
                else
                begin
                  sql.Add(' and tabordemmedicao.concluido='+QuotedStr('N'));
                  sql.Add(' and (tabordemmedicao.pedido is null)');
                end;

              sql.Add('order by TabORDEMMEDICAO.codigo');

              InputBox('','',Query.SQL.Text);

              Open;


             // FreltxtRDPRINT.ConfiguraImpressao;
              if (AbrirImpressaoPaisagem = false)
              then exit;
              Linha:=3;


              ImprimirNegrito(linha,30, 'Relat�rio de ORDENS DE MEDI��O ');
              Inc(Linha,1);

              if(medicaoConcluida) then
              begin
                ImprimirNegrito(linha,25, 'Medi��es Conclu�das');
                Inc(Linha,1);
              end;
              if(comOrcamento) then
              begin
                ImprimirNegrito(linha,25, 'Medi��es com Or�amento');
                Inc(Linha,1);
              end;

              ImprimirNegrito(Linha,1,CompletaPalavra('CODIGO',7,' ')+' '+
                                         CompletaPalavra('DESCRI��O',33,' ')+' '+
                                         CompletaPalavra('CLIENTE',20,' ')+' '+
                                         CompletaPalavra('VENDEDOR',20,' ')+' '+
                                         CompletaPalavra('MEDIDOR',20,' ')+' '+
                                         CompletaPalavra('DATA EMISS�O',12,' ')+' '+
                                         CompletaPalavra('DATA MEDI��O',12,' '));
              Inc(Linha,2);
              while not eof do
              begin
                   ImprimirSimples(Linha,1,CompletaPalavra_a_Esquerda(fieldbyname('codigo').AsString,7,' ')+' '+
                                         CompletaPalavra(fieldbyname('descricao').AsString,33,' ')+' '+
                                         CompletaPalavra(fieldbyname('nomecliente').AsString,20,' ')+' '+
                                         CompletaPalavra(fieldbyname('nomevendedor').AsString,20,' ')+' '+
                                         CompletaPalavra(fieldbyname('nomemedidor').AsString,20,' ')+' '+
                                         CompletaPalavra(fieldbyname('datapedidoemissao').AsString,12,' ')+' '+
                                         CompletaPalavra(fieldbyname('datamedicao').AsString,12,' '));
                   Inc(Linha,1);
                   Next;
              end;


              FreltxtRDPRINT.RDprint.Fechar;

        end;
   finally
        FreeAndNil(Query);
   end;


end;

procedure TObjORDEMMEDICAO.Submit_Duracao(parametro:String);
begin
      Duracao:=parametro;
end;

procedure TObjORDEMMEDICAO.Submit_HorarioTermino(parametro:string);
begin
      HorarioTermino:=parametro;
end;

procedure TObjORDEMMEDICAO.Submit_HorarioMedicao(parametro:string);
begin
      HorarioMedicao:=parametro;
end;

function TObjORDEMMEDICAO.Get_Duracao:string;
begin
    Result:=Duracao;
end;

function TObjORDEMMEDICAO.Get_HorarioTermino:string;
begin
  Result:=Self.HorarioTermino;
end;

function TObjORDEMMEDICAO.Get_HorarioMedicao:string;
begin
  Result:=HorarioMedicao;
end;


function TObjORDEMMEDICAO.Get_Complemento: string;
begin
  Result := self.complemento;
end;

procedure TObjORDEMMEDICAO.Submit_Complemento(parametro: String);
begin
  self.complemento := parametro;
end;

end.



