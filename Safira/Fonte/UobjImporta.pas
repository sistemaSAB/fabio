unit UobjImporta;
Interface
Uses Ibquery,windows,Classes,Db,UessencialGlobal,uobjcomponente,
Uobjperfilado_proj,UobjCOMPONENTE_PROJ,uobjprojeto,uobjcliente,UobjFornecedor;


Type
   TObjimporta=class

          Public
                Constructor Create(owner:TComponent);
                Destructor  Free;
                Function    InstanciaObjeto(pnometabela:STRING):boolean;
                function    DestroiObjeto: boolean;

                Function    RetornaTabelasDisponiveis(Ptabelas:TStrings):boolean;
                Function    RetornaCamposTabela(Ptabela:STRING;PCampos:TStrings):boolean;

                Function    ZerarTabela:boolean;
                procedure   Fieldbyname(PCampo:String;PValor:String);overload;
                function    Fieldbyname(PCampo:String):String;overload;
                function    Salvar(ComCommit: boolean): boolean;


         Private
               Owner:TComponent;
               tabelaatual:string;
               Objquery:Tibquery;

               ObjComponente:TObjCOMPONENTE;
               ObjComponente_PROJ:TObjCOMPONENTE_PROJ;
               ObjPerfilado_proj:tobjperfilado_proj;
               Objprojeto:Tobjprojeto;
               ObjCliente:TobjCliente;
               ObjFornecedor:TobjFornecedor;
   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls,
ibtable;


constructor TObjimporta.create(owner:TComponent);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin
        self.Owner := owner;
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.tabelaatual:='';
end;

function TObjimporta.RetornaTabelasDisponiveis(
  Ptabelas: TStrings): boolean;
begin
     Ptabelas.clear;
     Ptabelas.Add('TABCOMPONENTE');
     Ptabelas.Add('TABPERFILADO_PROJ');
     Ptabelas.Add('TABCOMPONENTE_PROJ');
     Ptabelas.Add('TABPROJETO');
     Ptabelas.Add('TABCLIENTE');
     Ptabelas.Add('TABFORNECEDOR');

end;



Function TObjimporta.Salvar(ComCommit:boolean):boolean;
begin
     if (Self.tabelaatual='TABCOMPONENTE')
     then Begin
              Self.ObjComponente.status:=dsinsert;
              result:=Self.ObjComponente.Salvar(ComCommit);
              exit;
     End;


     if (Self.tabelaatual='TABPERFILADO_PROJ')
     then Begin
              Self.ObjPerfilado_proj.status:=dsinsert;
              result:=Self.ObjPerfilado_proj.Salvar(ComCommit);
              exit;
     End;

     if (Self.tabelaatual='TABCOMPONENTE_PROJ')
     then Begin
              Self.ObjComponente_PROJ.status:=dsinsert;
              result:=Self.ObjComponente_PROJ.Salvar(ComCommit);
              exit;
     End;
     
     if (Self.tabelaatual='TABFORNECEDOR')
     then Begin
              Self.OBJFORNECEDOR.status:=dsinsert;
              result:=Self.OBJFORNECEDOR.Salvar(ComCommit);
              exit;
     End;

     if (Self.tabelaatual='TABPROJETO')
     then Begin
              Self.Objprojeto.status:=dsinsert;
              result:=Self.Objprojeto.Salvar(ComCommit);
              exit;
     End;

     if (Self.tabelaatual='TABCLIENTE')
     then Begin
              Self.ObjCliente.status:=dsinsert;
              result:=Self.ObjCliente.Salvar(ComCommit);
              exit;
     End;

     Mensagemerro('Tabela Inv�lida '+Self.tabelaatual);
     Exit;
end;

procedure TObjimporta.fieldbyname(PCampo, PValor: String);
begin
    if (Self.tabelaatual='TABCOMPONENTE')
    then Begin
             Self.ObjComponente.fieldbyname(Pcampo,Pvalor);
             exit;
    End;

    if (Self.tabelaatual='TABCOMPONENTE_PROJ')
    then Begin
             Self.ObjComponente_PROJ.fieldbyname(Pcampo,Pvalor);
             exit;
    End;

    if (Self.tabelaatual='TABPERFILADO_PROJ')
    then Begin
             Self.ObjPerfilado_proj.fieldbyname(Pcampo,Pvalor);
             exit;
    End;

    if (Self.tabelaatual='TABPROJETO')
    then Begin
             Self.Objprojeto.fieldbyname(Pcampo,Pvalor);
             exit;
    End;

    if (Self.tabelaatual='TABCLIENTE')
    then Begin
             Self.ObjCliente.fieldbyname(Pcampo,Pvalor);
             exit;
    End;


    if (Self.tabelaatual='TABFORNECEDOR')
    then Begin
             Self.ObjFORNECEDOR.fieldbyname(Pcampo,Pvalor);
             exit;
    End;


//SUBMIT
    

    Mensagemerro('Tabela Inv�lida '+Self.tabelaatual);
    exit;

end;

function TObjimporta.fieldbyname(PCampo: String): String;
begin
     if (Self.tabelaatual='TABCOMPONENTE')
     then Begin
               result:=Self.ObjComponente.fieldbyname(Pcampo);
               exit;
     End;

     if (Self.tabelaatual='TABCOMPONENTE_PROJ')
     then Begin
               result:=Self.ObjComponente_proj.fieldbyname(Pcampo);
               exit;
     End;

     if (Self.tabelaatual='TABPERFILADO_PROJ')
     then Begin
               result:=Self.ObjPerfilado_proj.fieldbyname(Pcampo);
               exit;
     End;

     if (Self.tabelaatual='TABPROJETO')
     then Begin
               result:=Self.Objprojeto.fieldbyname(Pcampo);
               exit;
     End;

     if (Self.tabelaatual='TABCLIENTE')
     then Begin
               result:=Self.objcliente.fieldbyname(Pcampo);
               exit;
     End;

     if (Self.tabelaatual='TABFORNECEDOR')
     then Begin
               result:=Self.objFORNECEDOR.fieldbyname(Pcampo);
               exit;
     End;


//GET     
     Mensagemerro('Tabela Inv�lida '+Self.tabelaatual);
     exit;

end;

destructor TObjimporta.Free;
begin
    Freeandnil(Self.Objquery);
end;

function TObjimporta.InstanciaObjeto(pnometabela: STRING): boolean;
begin
     result:=False;
     Pnometabela:=uppercase(Pnometabela);

     if (Pnometabela='TABPROJETO')
     then begin
               Self.TabelaAtual:=Pnometabela;
               Self.Objprojeto:=TObjPROJETO.Create;
               result:=true;
               exit;
     End;

     if (Pnometabela='TABCOMPONENTE')
     then begin
               Self.TabelaAtual:=Pnometabela;
               Self.ObjComponente:=TObjCOMPONENTE.Create;
               result:=true;
               exit;
     End;

     if (Pnometabela='TABCOMPONENTE_PROJ')
     then begin
               Self.TabelaAtual:=Pnometabela;
               Self.ObjComponente_PROJ:=TObjCOMPONENTE_PROJ.Create;
               result:=true;
               exit;
     End;

     if (Pnometabela='TABPERFILADO_PROJ')
     then begin
               Self.TabelaAtual:=Pnometabela;
               Self.ObjPerfilado_proj:=TObjPERFILADO_PROJ.Create;
               result:=true;
               exit;
     End;

     if (Pnometabela='TABCLIENTE')
     then begin
               Self.TabelaAtual:=Pnometabela;
               Self.ObjCliente:=TObjCLIENTE.Create;
               result:=true;
               exit;
     End;


     if (Pnometabela='TABFORNECEDOR')
     then begin
               Self.TabelaAtual:=Pnometabela;
               Self.ObjFORNECEDOR:=TObjFORNECEDOR.Create;
               result:=true;
               exit;
     End;


//INSTANCIAOBJETO

     mensagemerro('Objeto n�o configurado para a tabela '+Pnometabela);
     exit;
end;

function TObjimporta.DestroiObjeto: boolean;
begin
     result:=False;

     if (Self.tabelaatual='TABPROJETO')
     then begin
               Self.Objprojeto.free;
               result:=true;
               exit;
     End;

     if (Self.tabelaatual='TABCOMPONENTE')
     then begin
               Self.ObjComponente.free;
               result:=true;
               exit;
     End;

     if (Self.tabelaatual='TABCOMPONENTE_PROJ')
     then begin
               Self.ObjComponente_PROJ.free;
               result:=true;
               exit;
     End;

     if (Self.tabelaatual='TABPERFILADO_PROJ')
     then begin
               Self.ObjPerfilado_proj.free;
               result:=true;
               exit;
     End;


     if (Self.tabelaatual='TABCLIENTE')
     then begin
               Self.ObjCliente.free;
               result:=true;
               exit;
     End;

     if (Self.tabelaatual='TABFORNECEDOR')
     then begin
               Self.ObjFORNECEDOR.free;
               result:=true;
               exit;
     End;


     mensagemerro('Objeto n�o configurado para a tabela '+Self.tabelaatual);
     exit;
end;


function TObjimporta.RetornaCamposTabela(Ptabela: STRING;
  PCampos: TStrings): boolean;
var
  Ttabela:TIBTable;
  cont:integer;
begin

     Try
        Ttabela:=TIBTable.Create(nil);
        Ttabela.Database:=FDataModulo.IBDatabase;
     Except
           MensagemErro('Erro na tentativa de Criar a Table para resgatar o nome dos campos');
           exit;
     End;

     Try
        Try
            Ttabela.close;
            Ttabela.TableName:=Ptabela;
            Ttabela.open;
            Pcampos.clear;
        except
            mensagemerro('Erro na tentativa de abrir a tabela para resgatar os nomes dos campos da tabela');
            exit;
        End;

        for cont:=0 to Ttabela.Fields.Count-1 do
        Begin
             Pcampos.add(Ttabela.Fields[cont].FieldName);
        End;

     Finally
        ttabela.close;
        freeandnil(ttabela);
     End;
end;


function TObjimporta.ZerarTabela: boolean;
begin
     if (Self.tabelaatual='TABPROJETO')
     Then begin
              Objprojeto.ZerarTabela;
              exit;
     End;

     if (Self.tabelaatual='TABCOMPONENTE')
     Then Begin
              ObjComponente.ZerarTabela;
              exit;
     End;

     if (Self.tabelaatual='TABCOMPONENTE_PROJ')
     Then Begin
              ObjComponente_PROJ.ZerarTabela;
              exit;
     End;

     if (Self.tabelaatual='TABPERFILADO_PROJ')
     Then Begin
              ObjPerfilado_proj.ZerarTabela;
              exit;
     End;

     if (Self.tabelaatual='TABCLIENTE')
     Then Begin
              ObjCliente.ZerarTabela;
              exit;
     End;

     if (Self.tabelaatual='TABFORNECEDOR')
     Then Begin
              ObjFORNECEDOR.ZerarTabela;
              exit;
     End;


     Mensagemerro('Tabela Inv�lida '+Self.tabelaatual);
     exit;
end;


end.
