unit Upesquisa;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DB, DBGrids, ComCtrls, StdCtrls,IBCustomDataSet, IBQuery, IBDatabase,
  Mask, Buttons, UessencialGlobal, ExtCtrls;

type
  TFpesquisa = class(TForm)
    DBGrid: TDBGrid;
    StatusBar1: TStatusBar;
    DataSource1: TDataSource;
    querypesq: TIBQuery;
    Panel1: TPanel;
    BTSAIR: TBitBtn;
    Btcadastra: TBitBtn;
    edtbusca: TMaskEdit;
    procedure DBGridKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure edtbuscaKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtcadastraClick(Sender: TObject);
    procedure edtbuscaExit(Sender: TObject);
    procedure DBGridDblClick(Sender: TObject);
    procedure BTSAIRClick(Sender: TObject);

  private
      FormCadastro:Tform;
      str_pegacampo:string;
      comandosql:string;
      ComplementoPorcentagem:Str09;//usado para por % na pesquisa de nome automaticamente de acordo com um parametro
    { Private declarations }
  public
        Invisiveis:TStringList;
        NomeCadastroPersonalizacao:String;
        Function PreparaPesquisa(comando:TStringList;TitulodoForm:String;FormCad:Tform):Boolean;overload;
        Function PreparaPesquisa(comando:TStringList;TitulodoForm:String;FormCad:Tform;BasedeDados:TIBDatabase):Boolean;overload;
        
        Function PreparaPesquisaULTIMAPESQUISA(comando:TStringList;TitulodoForm:String;FormCad:Tform):Boolean;overload;
        Function PreparaPesquisa(comando:string;TitulodoForm:String;FormCad:Tform):Boolean;overload;
        Function PreparaPesquisa(comando:string;TitulodoForm:String;FormCad:Tform;BasedeDados:TIBDatabase):Boolean;overload;
        
        Function PreparaPesquisaN(comando:Str255;TitulodoForm:String;NomeForm:Str50):Boolean;

  end;

var
     Fpesquisa: TFpesquisa;



implementation

uses UDataModulo;



{$R *.DFM}

procedure TFpesquisa.DBGridKeyPress(Sender: TObject; var Key: Char);
begin

  Case Key of

  #32: Begin
               str_pegacampo:=dbgrid.SelectedField.FieldName;

               Case dbgrid.SelectedField.datatype of

                 ftdatetime :  Begin
                                 edtBusca.EditMask:='00/00/0000 00:00:00';
                                 EdtBusca.width:=70;
                              End;
                 ftdate    :  Begin
                                 edtBusca.EditMask:='00/00/0000;1;_';
                                 EdtBusca.width:=70;
                              End;
                 ftTime    :  Begin
                                 edtBusca.EditMask:='00:00';
                                 EdtBusca.width:=70;
                              End;
                 ftInteger  :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=250;
                                EdtBusca.maxlength:=8
                             End;

                 ftbcd      :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=250;
                                EdtBusca.maxlength:=8
                             End;
                 ftfloat    :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=250;
                                EdtBusca.maxlength:=8
                             End;
                 ftString   :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=400;
                                EdtBusca.maxlength:=255;
                             End;
                 Else Begin
                           Messagedlg('Est� tipo de dados n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
                           DBGrid.setfocus;
                           exit;
                      End;

               End;
               EdtBusca.Text :='';
               EdtBusca.Visible :=true;
               EdtBusca.SetFocus;
       End;
  #13: Begin
            if  Self.QueryPesq.recordcount<>0
            Then Self.modalresult:=mrok
            Else Self.modalresult:=mrCancel;
       End;
  #27:Begin
           Self.modalresult:=mrcancel;
      End;
  End;

end;

procedure TFpesquisa.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
cont:integer;
begin
    //O Tag � usado para indicar a formatacao do Dbgrid, zero ele
    //para nao influenciar em outras pesquisas
    Self.tag:=0;
    
    if edtbusca.Visible = true
    then  edtbusca.Visible:= false;


    if (Invisiveis<>nil)
    Then Begin
              Invisiveis.clear;
              for cont:=0 to dbgrid.Columns.Count-1 do
              Begin
                  if (dbgrid.Columns.Items[cont].Visible=False)
                  Then Invisiveis.add(inttostr(cont));
              End;
    End;

end;

procedure TFpesquisa.FormActivate(Sender: TObject);
var
cont:integer;
begin
     PegaCorForm(Self);
     Self.Tag:=0;
     PegaFiguraBotoes(nil,nil,nil,btcadastra,nil,nil,nil,btsair);
     
     Case Self.QueryPesq.state of

        dsInactive: Begin
                        Messagedlg('A tabela n�o foi preparada antes de chamar o form!', mterror,[mbok],0);
                        abort;
                    End;
        Else Begin
                ComplementoPorcentagem:='';
                comandosql:='';
                comandosql:= Self.QueryPesq.SQL.Text;
                if (Self.Tag=3)
                Then Formatadbgrid_3_casas(dbgrid)
                Else formatadbgrid(dbgrid);
                
                dbgrid.setfocus;
                If (ObjParametroGlobal.ValidaParametro('% PADRAO NA PESQUISA EM CAMPOS NOME')=True)
                Then Begin
                          If (ObjParametroGlobal.Get_Valor='SIM')
                          Then ComplementoPorcentagem:='%'
                          Else ComplementoPorcentagem:='';
                End;

                if (Invisiveis<>nil)
                Then Begin
                          for cont:=0 to Invisiveis.Count-1 do
                          Begin
                               try
                                  strtoint(Invisiveis[cont]);
                                  if (DBGrid.Columns.Count>Cont)
                                  Then dbgrid.Columns.Items[strtoint(Invisiveis[cont])].Visible :=False;
                               except
                               end;

                          End;
                End;


             End;
     End;
end;

procedure TFpesquisa.edtbuscaKeyPress(Sender: TObject; var Key: Char);
var
 int_busca:integer;
 str_busca:string;
 flt_busca:Currency;
 data_busca:Tdate;
 hora_busca:Ttime;
 datahora_busca:tdatetime;
 indice_grid:integer;
begin

   if key =#27//ESC
   then Begin
           EdtBusca.Visible := false;
           dbgrid.SetFocus;
           exit;
        End;
   If Key=#13//procura
   Then Begin
          //ordenando pela coluna antes de procurar
          If ( Self.QueryPesq.recordcount>0)
          Then Begin
            try
             indice_grid:=self.DBGrid.SelectedIndex;
             Self.QueryPesq.close;
             Self.QueryPesq.sql.clear;
             Self.QueryPesq.sql.add(comandosql+' order by '+str_pegacampo);
             Self.QueryPesq.open;
             formatadbgrid(dbgrid,querypesq);
             self.DBGrid.SelectedIndex:=indice_grid;
           except
           end;
          end;

          Case dbgrid.SelectedField.DataType of


             ftstring    :Begin
                               //utilizar a pesquisa com like "testar"S
                               try
                                        indice_grid:=self.DBGrid.SelectedIndex;
                                        Self.QueryPesq.close;
                                        Self.QueryPesq.sql.clear;
                                        If(Pos('WHERE',UpperCase(comandosql))<>0)
                                        Then Self.QueryPesq.sql.add(comandosql+' and UPPER('+str_pegacampo+') like '+#39
+ComplementoPorcentagem+edtbusca.text+ComplementoPorcentagem+#39)
                                        Else Self.QueryPesq.sql.add(comandosql+' where UPPER('+str_pegacampo+') like '+#39
+ComplementoPorcentagem+edtbusca.text+ComplementoPorcentagem+#39);

                                        Self.QueryPesq.sql.add(' order by '+str_pegacampo);
                                        Self.QueryPesq.open;
                                        //self.querypesq.SQL.SaveToFile('c:\ronnei.txt');
                                        formatadbgrid(dbgrid,querypesq);
                                        self.DBGrid.SelectedIndex:=indice_grid;
                                except
                                end;
                                //Self.QueryPesq.Locate(str_pegacampo,edtbusca.text,[Lopartialkey]);
                          End;
                          
             ftinteger   :Begin
                              try int_busca:=Strtoint(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,int_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftfloat     :Begin
                              try flt_busca:=Strtofloat(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,flt_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftBcd      :Begin
                              try flt_busca:=Strtofloat(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,flt_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftDate     :Begin
                              try data_busca:=Strtodate(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,data_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             fttime     :Begin
                              try hora_busca:=Strtodate(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,hora_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;

             ftDateTime :Begin
                              try datahora_busca:=Strtodatetime(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,datahora_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             Else Begin
                       Messagedlg('Este dado n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
                  End;
             End;


             Dbgrid.SetFocus;
             exit;
        End;

//Aqui defino as regras que podem ser digitadas
//como estou utilizando maskEdit Para Datas e Horas
//N�o preciso pois defini uma mascara, mas para
//float e Integer preciso

        Case dbgrid.SelectedField.DataType OF

                ftInteger:  Begin
                                 if not (Key in ['0'..'9',#8])
                                 Then key:=#0;
                            End;
                ftFloat  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftCurrency  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftBCD  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;


        End;
End;


procedure TFpesquisa.DBGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
indice_grid:integer;
cont,marca_registro:integer ;
comandoordena:String;
ColunaAtual:integer;
begin
     if (key=VK_Delete)
     then begin
               dbgrid.Columns.Items[dbgrid.SelectedIndex].Visible :=false;
               dbgrid.SelectedIndex :=dbgrid.SelectedIndex +1;
     end;



     if (key=VK_f12)
     Then Begin


                     try
                         ColunaAtual:=DbGrid.SelectedIndex;
                         str_pegacampo:=DbGrid.SelectedField.FieldName;
                         if (Length(str_pegacampo)>4)
                         Then Begin
                                   if (uppercase(copy(str_pegacampo,1,2))='XX')
                                   and (uppercase(copy(str_pegacampo,length(str_pegacampo)-1,2))='XX')
                                   Then exit;
                         End;



                         indice_grid:=self.DBGrid.SelectedIndex;
                         marca_registro:= self.DBGrid.DataSource.DataSet.fieldbyname('codigo').AsInteger;
                         ComandoOrdena:=comandosql+' order by ';

                         If (ssCtrl in Shift)
                         Then Begin  //Control pressionado, sinal de ordem por mais de uma coluna
                                   //procuro a palavra order se encontrar significa que posso ordenar pelo proximo campo
                                   If(Pos('order ',Self.QueryPesq.sql.text)<>0)
                                   Then ComandoOrdena:=Self.QueryPesq.sql.text+','
                                   Else ComandoOrdena:=comandosql+' order by ';
                              End;

                         Self.QueryPesq.close;
                         Self.QueryPesq.sql.clear;
                         Self.QueryPesq.sql.add(Comandoordena+str_pegacampo);
                         Self.QueryPesq.open;
                         DbGrid.SelectedIndex:=ColunaAtual;
                         formatadbgrid(dbgrid,querypesq);
                        
                        //self.DBGrid.DataSource.DataSet.Locate( 'codigo',marca_registro,[loCaseInsensitive]);
                        //self.DBGrid.SelectedIndex:=indice_grid;
                        //self.DBGrid.DataSource.DataSet.GotoBookmark(marca_registro);

                     except

                     end;
                //End
                //Else Begin
                 //       If (Self.QueryPesq.recordcount=0)
                 //       Then messagedlg('N�o Existem Registros para Serem Ordenados!',mterror,[mbok],0)
                  //      Else messagedlg('Selecione um Campo para Ordena��o e Pressione Ordenar!',mtwarning,[mbok],0);
                   //  End;
                DbGrid.setfocus;
          End
     Else Begin
               if key=VK_F11
               Then Begin
                         for cont:=0 to dbgrid.Columns.Count-1 do
                         dbgrid.Columns.Items[cont].Visible :=true;
               End;
     End;


end;

function TFpesquisa.PreparaPesquisa(comando: string;TitulodoForm:String;FormCad:Tform): Boolean;
begin
    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql.add(comando); 
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.Btcadastra.visible:=True;
                 End
            Else Self.Btcadastra.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;


function TFpesquisa.PreparaPesquisa(comando: string;TitulodoForm:String;FormCad:Tform;BasedeDados:TIBDatabase): Boolean;
begin
    querypesq.Database:=basededados;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql.add(comando); 
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.Btcadastra.visible:=True;
                 End
            Else Self.Btcadastra.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

procedure TFpesquisa.BtcadastraClick(Sender: TObject);
begin
     If (Self.FormCadastro<>nil)
     Then Begin
               IF Self.FormCadastro.Visible=False
               Then Self.FormCadastro.showmodal
               Else Self.FormCadastro.show;
               
               Self.QueryPesq.close;
               Self.QueryPesq.open;
               Self.DBGrid.setfocus;
               
     End;
end;

procedure TFpesquisa.edtbuscaExit(Sender: TObject);
begin
     edtbusca.Visible:=False;
end;


function TFpesquisa.PreparaPesquisa(comando: TStringList;
  TitulodoForm: String; FormCad: Tform): Boolean;
begin
    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql:=comando;
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.Btcadastra.visible:=True;
                 End
            Else Self.Btcadastra.visible:=False;
        End;
     Except
           ON e:exception do
           Begin
                 Messagedlg('Erro na Tentativa de Pesquisa'+#13+e.message,mterror,[mbok],0);
                 result:=false;
                 exit;
           End;
     End;
     result:=true;


end;


function TFpesquisa.PreparaPesquisa(comando: TStringList;
  TitulodoForm: String; FormCad: Tform;BasedeDados:TIBDatabase): Boolean;
begin
    querypesq.Database:=BasedeDados;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql:=comando;
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.Btcadastra.visible:=True;
                 End
            Else Self.Btcadastra.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;
end;



procedure TFpesquisa.DBGridDblClick(Sender: TObject);
begin
    if  Self.QueryPesq.recordcount<>0
    Then Self.modalresult:=mrok
    Else Self.modalresult:=mrCancel;
        
end;

function TFpesquisa.PreparaPesquisaN(comando: Str255; TitulodoForm:String;NomeForm: Str50): Boolean;
begin
    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql.add(comando);
            open;
            If (NomeForm<>'')
            Then Begin
                      //verificar se � possicel chamar um form pelo nome
                      FormCadastro:=Tform(Application.FindComponent(Nomeform));
                      Self.Btcadastra.visible:=True;
                 End
            Else Self.Btcadastra.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;
end;

procedure TFpesquisa.BTSAIRClick(Sender: TObject);
begin
     Self.Close;
end;

function TFpesquisa.PreparaPesquisaULTIMAPESQUISA(comando: TStringList;
  TitulodoForm: String; FormCad: Tform): Boolean;
begin

    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql:=comando;
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.Btcadastra.visible:=True;
                 End
            Else Self.Btcadastra.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;


end.


