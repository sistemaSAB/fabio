unit UobjIMPOSTO_COFINS;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UOBJSITUACAOTRIBUTARIA_COFINS
;
//USES_INTERFACE



Type
   TObjIMPOSTO_COFINS=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               ST:TOBJSITUACAOTRIBUTARIA_COFINS;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_TIPOCALCULO(parametro: string);
                Function Get_TIPOCALCULO: string;
                Procedure Submit_ALIQUOTAPERCENTUAL(parametro: string);
                Function Get_ALIQUOTAPERCENTUAL: string;
                Procedure Submit_ALIQUOTAVALOR(parametro: string);
                Function Get_ALIQUOTAVALOR: string;
                Procedure Submit_TIPOCALCULO_ST(parametro: string);
                Function Get_TIPOCALCULO_ST: string;
                Procedure Submit_ALIQUOTAPERCENTUAL_ST(parametro: string);
                Function Get_ALIQUOTAPERCENTUAL_ST: string;
                Procedure Submit_ALIQUOTAVALOR_ST(parametro: string);
                Function Get_ALIQUOTAVALOR_ST: string;

                Function Get_FORMULABASECALCULO:String;
                Function Get_FORMULABASECALCULO_ST:String;

                Procedure Submit_FORMULABASECALCULO(parametro:String);
                Procedure Submit_FORMULABASECALCULO_ST(parametro:String);


                Function Get_formula_valor_imposto:String;
                Function Get_formula_valor_imposto_ST:String;

                Procedure Submit_formula_valor_imposto(parametro:String);
                Procedure Submit_formula_valor_imposto_ST(parametro:String);

                procedure EdtSTExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtSTKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
//CODIFICA DECLARA GETSESUBMITS


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               TIPOCALCULO:string;
               ALIQUOTAPERCENTUAL:string;
               ALIQUOTAVALOR:string;
               TIPOCALCULO_ST:string;
               ALIQUOTAPERCENTUAL_ST:string;
               ALIQUOTAVALOR_ST:string;
               FORMULABASECALCULO:String;
               FORMULABASECALCULO_ST:String;


               formula_valor_imposto:String;
               formula_valor_imposto_ST:String;
//CODIFICA VARIAVEIS PRIVADAS









               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, USITUACAOTRIBUTARIA_COFINS;





Function  TObjIMPOSTO_COFINS.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('ST').asstring<>'')
        Then Begin
                 If (Self.ST.LocalizaCodigo(FieldByName('ST').asstring)=False)
                 Then Begin
                          Messagedlg('Situa��o Tribut�ria N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.ST.TabelaparaObjeto;
        End;
        Self.TIPOCALCULO:=fieldbyname('TIPOCALCULO').asstring;
        Self.ALIQUOTAPERCENTUAL:=fieldbyname('ALIQUOTAPERCENTUAL').asstring;
        Self.ALIQUOTAVALOR:=fieldbyname('ALIQUOTAVALOR').asstring;
        Self.TIPOCALCULO_ST:=fieldbyname('TIPOCALCULO_ST').asstring;
        Self.ALIQUOTAPERCENTUAL_ST:=fieldbyname('ALIQUOTAPERCENTUAL_ST').asstring;
        Self.ALIQUOTAVALOR_ST:=fieldbyname('ALIQUOTAVALOR_ST').asstring;

        Self.FORMULABASECALCULO:=fieldbyname('FORMULABASECALCULO').asstring;
        Self.FORMULABASECALCULO_ST:=fieldbyname('FORMULABASECALCULO_ST').asstring;

        Self.formula_valor_imposto:=fieldbyname('formula_valor_imposto').asstring;
        Self.formula_valor_imposto_ST:=fieldbyname('formula_valor_imposto_ST').asstring;

        result:=True;
     End;
end;


Procedure TObjIMPOSTO_COFINS.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('ST').asstring:=Self.ST.GET_CODIGO;
        ParamByName('TIPOCALCULO').asstring:=Self.TIPOCALCULO;
        ParamByName('ALIQUOTAPERCENTUAL').asstring:=virgulaparaponto(Self.ALIQUOTAPERCENTUAL);
        ParamByName('ALIQUOTAVALOR').asstring:=virgulaparaponto(Self.ALIQUOTAVALOR);
        ParamByName('TIPOCALCULO_ST').asstring:=Self.TIPOCALCULO_ST;
        ParamByName('ALIQUOTAPERCENTUAL_ST').asstring:=virgulaparaponto(Self.ALIQUOTAPERCENTUAL_ST);
        ParamByName('ALIQUOTAVALOR_ST').asstring:=virgulaparaponto(Self.ALIQUOTAVALOR_ST);
        ParamByName('FORMULABASECALCULO').asstring:=Self.FORMULABASECALCULO;
        ParamByName('FORMULABASECALCULO_ST').asstring:=Self.FORMULABASECALCULO_ST;

        ParamByName('formula_valor_imposto').asstring:=Self.formula_valor_imposto;
        ParamByName('formula_valor_imposto_ST').asstring:=Self.formula_valor_imposto_ST;
  End;
End;

//***********************************************************************

function TObjIMPOSTO_COFINS.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjIMPOSTO_COFINS.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        ST.ZerarTabela;
        TIPOCALCULO:='';
        ALIQUOTAPERCENTUAL:='';
        ALIQUOTAVALOR:='';
        TIPOCALCULO_ST:='';
        ALIQUOTAPERCENTUAL_ST:='';
        ALIQUOTAVALOR_ST:='';
        FORMULABASECALCULO:='';
        FORMULABASECALCULO_ST:='';

        formula_valor_imposto:='';
        formula_valor_imposto_ST:='';
     End;
end;

Function TObjIMPOSTO_COFINS.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      If (ST.get_Codigo='')
      Then Mensagem:=mensagem+'/Situa��o Tribut�ria';

      if (FORMULABASECALCULO='')
      then FORMULABASECALCULO:='0';

      if (FORMULABASECALCULO_ST='')
      then FORMULABASECALCULO_ST:='0';



      if (formula_valor_imposto='')
      then formula_valor_imposto:='0';

      if (formula_valor_imposto_ST='')
      then formula_valor_imposto_ST:='0';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjIMPOSTO_COFINS.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     If (Self.ST.LocalizaCodigo(Self.ST.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Situa��o Tribut�ria n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjIMPOSTO_COFINS.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.ST.Get_Codigo<>'')
        Then Strtoint(Self.ST.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Situa��o Tribut�ria';
     End;
     try
        Strtofloat(Self.ALIQUOTAPERCENTUAL);
     Except
           Mensagem:=mensagem+'/Al�quota Percentual';
     End;
     try
        Strtofloat(Self.ALIQUOTAVALOR);
     Except
           Mensagem:=mensagem+'/Al�quota em Valor';
     End;
     try
        Strtofloat(Self.ALIQUOTAPERCENTUAL_ST);
     Except
           Mensagem:=mensagem+'/Al�quota Percentual (ST)';
     End;
     try
        Strtofloat(Self.ALIQUOTAVALOR_ST);
     Except
           Mensagem:=mensagem+'/Al�quota em Valor (ST)';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjIMPOSTO_COFINS.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjIMPOSTO_COFINS.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjIMPOSTO_COFINS.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro IMPOSTO_COFINS vazio',mterror,[mbok],0);
                 exit;
       End;

       
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,ST,TIPOCALCULO,ALIQUOTAPERCENTUAL,ALIQUOTAVALOR,TIPOCALCULO_ST');
           SQL.ADD(' ,ALIQUOTAPERCENTUAL_ST,ALIQUOTAVALOR_ST,FORMULABASECALCULO,FORMULABASECALCULO_ST,formula_valor_imposto,formula_valor_imposto_ST');
           SQL.ADD(' from  TABIMPOSTO_COFINS');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjIMPOSTO_COFINS.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjIMPOSTO_COFINS.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjIMPOSTO_COFINS.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.ST:=TOBJSITUACAOTRIBUTARIA_COFINS.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABIMPOSTO_COFINS(CODIGO,ST,TIPOCALCULO,ALIQUOTAPERCENTUAL');
                InsertSQL.add(' ,ALIQUOTAVALOR,TIPOCALCULO_ST,ALIQUOTAPERCENTUAL_ST');
                InsertSQL.add(' ,ALIQUOTAVALOR_ST,FORMULABASECALCULO,FORMULABASECALCULO_ST,formula_valor_imposto,formula_valor_imposto_ST)');
                InsertSQL.add('values (:CODIGO,:ST,:TIPOCALCULO,:ALIQUOTAPERCENTUAL');
                InsertSQL.add(' ,:ALIQUOTAVALOR,:TIPOCALCULO_ST,:ALIQUOTAPERCENTUAL_ST');
                InsertSQL.add(' ,:ALIQUOTAVALOR_ST,:FORMULABASECALCULO,:FORMULABASECALCULO_ST,:formula_valor_imposto,:formula_valor_imposto_ST)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABIMPOSTO_COFINS set CODIGO=:CODIGO,ST=:ST,TIPOCALCULO=:TIPOCALCULO');
                ModifySQL.add(',ALIQUOTAPERCENTUAL=:ALIQUOTAPERCENTUAL,ALIQUOTAVALOR=:ALIQUOTAVALOR');
                ModifySQL.add(',TIPOCALCULO_ST=:TIPOCALCULO_ST,ALIQUOTAPERCENTUAL_ST=:ALIQUOTAPERCENTUAL_ST');
                ModifySQL.add(',ALIQUOTAVALOR_ST=:ALIQUOTAVALOR_ST,FORMULABASECALCULO=:FORMULABASECALCULO,FORMULABASECALCULO_ST=:FORMULABASECALCULO_ST');
                ModifySQL.add(',formula_valor_imposto=:formula_valor_imposto,formula_valor_imposto_ST=:formula_valor_imposto_ST where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABIMPOSTO_COFINS where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjIMPOSTO_COFINS.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjIMPOSTO_COFINS.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabIMPOSTO_COFINS');
     Result:=Self.ParametroPesquisa;
end;

function TObjIMPOSTO_COFINS.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Impostos - COFINS ';
end;


function TObjIMPOSTO_COFINS.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENIMPOSTO_COFINS,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENIMPOSTO_COFINS,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjIMPOSTO_COFINS.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.ST.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjIMPOSTO_COFINS.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjIMPOSTO_COFINS.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjIMPOSTO_COFINS.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjIMPOSTO_COFINS.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjIMPOSTO_COFINS.Submit_TIPOCALCULO(parametro: string);
begin
        Self.TIPOCALCULO:=Parametro;
end;
function TObjIMPOSTO_COFINS.Get_TIPOCALCULO: string;
begin
        Result:=Self.TIPOCALCULO;
end;
procedure TObjIMPOSTO_COFINS.Submit_ALIQUOTAPERCENTUAL(parametro: string);
begin
        Self.ALIQUOTAPERCENTUAL:=Parametro;
end;
function TObjIMPOSTO_COFINS.Get_ALIQUOTAPERCENTUAL: string;
begin
        Result:=Self.ALIQUOTAPERCENTUAL;
end;
procedure TObjIMPOSTO_COFINS.Submit_ALIQUOTAVALOR(parametro: string);
begin
        Self.ALIQUOTAVALOR:=Parametro;
end;
function TObjIMPOSTO_COFINS.Get_ALIQUOTAVALOR: string;
begin
        Result:=Self.ALIQUOTAVALOR;
end;
procedure TObjIMPOSTO_COFINS.Submit_TIPOCALCULO_ST(parametro: string);
begin
        Self.TIPOCALCULO_ST:=Parametro;
end;
function TObjIMPOSTO_COFINS.Get_TIPOCALCULO_ST: string;
begin
        Result:=Self.TIPOCALCULO_ST;
end;
procedure TObjIMPOSTO_COFINS.Submit_ALIQUOTAPERCENTUAL_ST(parametro: string);
begin
        Self.ALIQUOTAPERCENTUAL_ST:=Parametro;
end;
function TObjIMPOSTO_COFINS.Get_ALIQUOTAPERCENTUAL_ST: string;
begin
        Result:=Self.ALIQUOTAPERCENTUAL_ST;
end;
procedure TObjIMPOSTO_COFINS.Submit_ALIQUOTAVALOR_ST(parametro: string);
begin
        Self.ALIQUOTAVALOR_ST:=Parametro;
end;
function TObjIMPOSTO_COFINS.Get_ALIQUOTAVALOR_ST: string;
begin
        Result:=Self.ALIQUOTAVALOR_ST;
end;
//CODIFICA GETSESUBMITS


procedure TObjIMPOSTO_COFINS.EdtSTExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.ST.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.ST.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.ST.GET_NOME;
End;
procedure TObjIMPOSTO_COFINS.EdtSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FST:TFSITUACAOTRIBUTARIA_COFINS;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FST:=TFSITUACAOTRIBUTARIA_COFINS.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.ST.Get_Pesquisa,Self.ST.Get_TituloPesquisa,FST)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ST.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.ST.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ST.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FST);
     End;
end;
//CODIFICA EXITONKEYDOWN

procedure TObjIMPOSTO_COFINS.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJIMPOSTO_COFINS';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



function TObjIMPOSTO_COFINS.Get_FORMULABASECALCULO: String;
begin
     Result:=Self.FORMULABASECALCULO;
end;

function TObjIMPOSTO_COFINS.Get_FORMULABASECALCULO_ST: String;
begin
     Result:=Self.FORMULABASECALCULO_ST;
end;

procedure TObjIMPOSTO_COFINS.Submit_FORMULABASECALCULO(parametro: String);
begin
     Self.FORMULABASECALCULO:=parametro;
end;

procedure TObjIMPOSTO_COFINS.Submit_FORMULABASECALCULO_ST(parametro: String);
begin
     Self.FORMULABASECALCULO_ST:=parametro;
end;


function TObjIMPOSTO_COFINS.Get_formula_valor_imposto: String;
begin
     Result:=Self.formula_valor_imposto;
end;

function TObjIMPOSTO_COFINS.Get_formula_valor_imposto_ST: String;
begin
     Result:=Self.formula_valor_imposto_ST;
end;

procedure TObjIMPOSTO_COFINS.Submit_formula_valor_imposto(parametro: String);
begin
     Self.formula_valor_imposto:=parametro;
end;

procedure TObjIMPOSTO_COFINS.Submit_formula_valor_imposto_ST(parametro: String);
begin
     Self.formula_valor_imposto_ST:=parametro;
end;

end.



