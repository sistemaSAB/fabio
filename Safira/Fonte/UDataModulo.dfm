object FDataModulo: TFDataModulo
  OldCreateOrder = False
  Left = 565
  Top = 238
  Height = 375
  Width = 544
  object IBDatabase: TIBDatabase
    Params.Strings = (
      'user_name=SYSDBA'
      'password=3,')
    LoginPrompt = False
    DefaultTransaction = IBTransaction
    Left = 32
    Top = 16
  end
  object IBTransaction: TIBTransaction
    DefaultAction = TARollback
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 120
    Top = 16
  end
  object IbUser: TIBSecurityService
    TraceFlags = []
    SecurityAction = ActionAddUser
    UserID = 0
    GroupID = 0
    Left = 208
    Top = 24
  end
  object QueryImporta: TQuery
    Left = 24
    Top = 88
  end
  object ComponenteRdPrint: TRDprint
    ImpressoraPersonalizada.NomeImpressora = 'Modelo Personalizado - (Epson)'
    ImpressoraPersonalizada.AvancaOitavos = '27 48'
    ImpressoraPersonalizada.AvancaSextos = '27 50'
    ImpressoraPersonalizada.SaltoPagina = '12'
    ImpressoraPersonalizada.TamanhoPagina = '27 67 66'
    ImpressoraPersonalizada.Negrito = '27 69'
    ImpressoraPersonalizada.Italico = '27 52'
    ImpressoraPersonalizada.Sublinhado = '27 45 49'
    ImpressoraPersonalizada.Expandido = '27 14'
    ImpressoraPersonalizada.Normal10 = '18 27 80'
    ImpressoraPersonalizada.Comprimir12 = '18 27 77'
    ImpressoraPersonalizada.Comprimir17 = '27 80 27 15'
    ImpressoraPersonalizada.Comprimir20 = '27 77 27 15'
    ImpressoraPersonalizada.Reset = '27 80 18 20 27 53 27 70 27 45 48'
    ImpressoraPersonalizada.Inicializar = '27 64'
    OpcoesPreview.PaginaZebrada = False
    OpcoesPreview.Remalina = False
    OpcoesPreview.CaptionPreview = 'Rdprint Preview'
    OpcoesPreview.PreviewZoom = 100
    OpcoesPreview.CorPapelPreview = clWhite
    OpcoesPreview.CorLetraPreview = clBlack
    OpcoesPreview.Preview = False
    OpcoesPreview.BotaoSetup = Ativo
    OpcoesPreview.BotaoImprimir = Ativo
    OpcoesPreview.BotaoGravar = Ativo
    OpcoesPreview.BotaoLer = Ativo
    OpcoesPreview.BotaoProcurar = Ativo
    Margens.Left = 10
    Margens.Right = 10
    Margens.Top = 10
    Margens.Bottom = 10
    Autor = Deltress
    RegistroUsuario.NomeRegistro = 'Exclaim Tecnologia'
    RegistroUsuario.SerieProduto = 'SINGLE-0706/01006'
    RegistroUsuario.AutorizacaoKey = 'BTXV-5778-EZCC-2224-OESY'
    About = 'RDprint 4.0c - Registrado'
    Acentuacao = Transliterate
    CaptionSetup = 'Rdprint Setup'
    TitulodoRelatorio = 'Gerado por RDprint'
    UsaGerenciadorImpr = False
    CorForm = clBtnFace
    CorFonte = clBlack
    Impressora = Epson
    Mapeamento.Strings = (
      '//--- Grafico Compativel com Windows/USB ---//'
      '//'
      'GRAFICO=GRAFICO'
      'HP=GRAFICO'
      'DESKJET=GRAFICO'
      'LASERJET=GRAFICO'
      'INKJET=GRAFICO'
      'STYLUS=GRAFICO'
      'EPL=GRAFICO'
      'USB=GRAFICO'
      '//'
      '//--- Linha Epson Matricial 9 e 24 agulhas ---//'
      '//'
      'EPSON=EPSON'
      'GENERICO=EPSON'
      'LX-300=EPSON'
      'LX-810=EPSON'
      'FX-2170=EPSON'
      'FX-1170=EPSON'
      'LQ-1170=EPSON'
      'LQ-2170=EPSON'
      'OKIDATA=EPSON'
      '//'
      '//--- Rima e Emilia ---//'
      '//'
      'RIMA=RIMA'
      'EMILIA=RIMA'
      '//'
      '//--- Linha HP/Xerox padr'#227'o PCL ---//'
      '//'
      'PCL=HP'
      '//'
      '//--- Impressoras 40 Colunas ---//'
      '//'
      'DARUMA=BOBINA'
      'SIGTRON=BOBINA'
      'SWEDA=BOBINA'
      'BEMATECH=BOBINA')
    PortaComunicacao = 'LPT1'
    MostrarProgresso = True
    TamanhoQteLinhas = 66
    TamanhoQteColunas = 80
    TamanhoQteLPP = Seis
    NumerodeCopias = 1
    FonteTamanhoPadrao = S10cpp
    FonteEstiloPadrao = []
    Orientacao = poPortrait
    Left = 120
    Top = 88
  end
  object IBDatabaseBackup: TIBDatabase
    Params.Strings = (
      'user_name=sysdba')
    LoginPrompt = False
    DefaultTransaction = IBTransactionBackup
    Left = 440
    Top = 24
  end
  object IBTransactionBackup: TIBTransaction
    DefaultAction = TARollback
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 432
    Top = 80
  end
  object IBQueryPesquisa: TIBQuery
    Database = IBDatabase
    Transaction = IBTransaction
    Left = 32
    Top = 152
  end
  object IBDataPEDIDOPROJ_ORDEMPRODUCAO: TIBDataSet
    Database = IBDatabase
    Transaction = IBTransaction
    DeleteSQL.Strings = (
      'delete from TABSERVICOPRODUCAO_PROJ'
      'where'
      '  CODIGO = :OLD_CODIGO')
    InsertSQL.Strings = (
      'INSERT INTO TABPEDIDOPROJ_ORDEMPRODUCAO ('
      '    CODIGO,'
      '    PEDIDOPROJETO,'
      '    SERVICO,'
      '    ESTADOPRODUCAO,'
      '    HORARIOENTRADA,'
      '    HORARIOSAIDA,'
      '    DATAENTRADA,'
      '    DATASAIDA,'
      '    LISTADECOMPONENTES  )'
      'VALUES ('
      '    :CODIGO,'
      '    :PEDIDOPROJETO,'
      '    :SERVICO,'
      '    :ESTADOPRODUCAO,'
      '    :HORARIOENTRADA,'
      '    :HORARIOSAIDA,'
      '    :DATAENTRADA,'
      '    :DATASAIDA,'
      '    :LISTADECOMPONENTES'
      ')')
    SelectSQL.Strings = (
      'SELECT CODIGO,PROJETO,SERVICO,DATAC,DATAM,USERC,USERM'
      'FROM TABSERVICOPRODUCAO_PROJ'
      'WHERE CODIGO=:CODIGO')
    ModifySQL.Strings = (
      'update TABSERVICOPRODUCAO_PROJ'
      'set'
      '  CODIGO   =: CODIGO,'
      '  PROJETO =: PROJETO,'
      '  SERVICO  =: SERVICO  '
      'where'
      '  CODIGO = :OLD_CODIGO')
    Left = 432
    Top = 144
  end
  object unicQuery: TIBQuery
    Database = IBDatabase
    Transaction = unicTransaction
    Left = 40
    Top = 256
  end
  object unicTransaction: TIBTransaction
    DefaultDatabase = IBDatabase
    Left = 120
    Top = 256
  end
end
