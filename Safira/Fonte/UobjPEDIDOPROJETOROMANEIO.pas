unit UobjPEDIDOPROJETOROMANEIO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,Uobjromaneio,UObjPedido_proj;


Type
   TObjPEDIDOPROJETOROMANEIO=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Romaneio:TOBJROMANEIO;
                PedidoProjeto:TOBJPEDIDO_PROJ;
//CODIFICA VARIAVEIS PUBLICAS



                Constructor Create(Owner:TComponent);
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    Localiza_PedidoProjeto_Romaneio(PPedidoProjeto,Promaneio:string):Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_pesquisaAnalitica           :TStringList;
                Function    Get_Pesquisa_PedidoProjeto_nao_usado(Ppedido:string):TStringList;

                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                procedure EdtRomaneioExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtRomaneioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtPedidoProjetoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;Ppedido:string);overload;

                Procedure RetornaPedidosRomaneio(Promaneio:string);
                Procedure Opcoes(Promaneio:string);
                //Function  VerificaporPedido(ppedido:string):boolean;
//CODIFICA DECLARA GETSESUBMITS



         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               Owner:TComponent;
               
               CODIGO:string;
//CODIFICA VARIAVEIS PRIVADAS
               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
               Procedure ConcluirRomaneio(Promaneio:string);
               Procedure RetornarRomaneio(Promaneio:string);
               Procedure ImprimeRomaneio(PRomaneio:string);
               Procedure ImprimeRomaneio_analitico(PRomaneio:string);
               Procedure imprimeDadosCliente(Pcliente,Promaneio:string;var linha:integer;Pprimeiro:boolean);
               Procedure ImprimeCaixaTransportadora(var linha:integer);
               procedure ImprimeRomaneio_Sem_separarCliente(PRomaneio: string);
               Function CalculaPrevisaoComissao(Promaneio:string):currency;
               procedure ImprimeSomenteVidros(Pcodigo:string);
   End;

implementation

uses UopcaoRel,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios, UobjPedidoObjetos,
     UReltxtRDPRINT,rdprint,UobjCOMISSAO_ADIANT_FUNCFOLHA,uobjcomissaocolocador,
  UmostraStringList, UFiltraImp, UobjRELPERSREPORTBUILDER;

Function  TObjPEDIDOPROJETOROMANEIO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('Romaneio').asstring<>'')
        Then Begin
                 If (Self.Romaneio.LocalizaCodigo(FieldByName('Romaneio').asstring)=False)
                 Then Begin
                          Messagedlg('Romaneio N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Romaneio.TabelaparaObjeto;
        End;
        If(FieldByName('PedidoProjeto').asstring<>'')
        Then Begin
                 If (Self.PedidoProjeto.LocalizaCodigo(FieldByName('PedidoProjeto').asstring)=False)
                 Then Begin
                          Messagedlg('Pedido/Projeto N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PedidoProjeto.TabelaparaObjeto;
        End;
//CODIFICA TABELAPARAOBJETO



        result:=True;
     End;
end;


Procedure TObjPEDIDOPROJETOROMANEIO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Romaneio').asstring:=Self.Romaneio.GET_CODIGO;
        ParamByName('PedidoProjeto').asstring:=Self.PedidoProjeto.GET_CODIGO;
//CODIFICA OBJETOPARATABELA



  End;
End;

//***********************************************************************

function TObjPEDIDOPROJETOROMANEIO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;

  if (Self.Localiza_PedidoProjeto_Romaneio(Self.PedidoProjeto.Get_Codigo,Self.Romaneio.get_codigo)=true)
  Then Begin
           if (Self.Status=dsinsert)
           Then begin
                     Messagedlg('Este Pedido projeto j� se encontra ligado a este romaneio',mterror,[mbok],0);
                     exit;
           end;

           if (Self.Status=dsedit)
           and (self.Codigo<>Self.Objquery.FieldByName('codigo').asstring)
           Then begin
                     Messagedlg('Este Pedido projeto j� se encontra ligado a este romaneio',mterror,[mbok],0);
                     exit;
           end;

  end;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;

              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
    
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPEDIDOPROJETOROMANEIO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Romaneio.ZerarTabela;
        PedidoProjeto.ZerarTabela;
//CODIFICA ZERARTABELA



     End;
end;

Function TObjPEDIDOPROJETOROMANEIO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Romaneio.Get_CODIGO='')
      Then Mensagem:=mensagem+'/Romaneio';
      If (PedidoProjeto.Get_Codigo='')
      Then Mensagem:=mensagem+'/Pedido/Projeto';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPEDIDOPROJETOROMANEIO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Romaneio.LocalizaCodigo(Self.Romaneio.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Romaneio n�o Encontrado!';

      If (Self.PedidoProjeto.LocalizaCodigo(Self.PedidoProjeto.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Pedido/Projeto n�o Encontrado!'
      Else Begin
                Self.PedidoProjeto.TabelaparaObjeto;

                if (Self.PedidoProjeto.Pedido.Get_Concluido='N')
                Then Mensagem:=Mensagem+'\O Pedido N� '+Self.PedidoProjeto.Pedido.Get_Codigo+' n�o foi conclu�do, n�o � poss�vel utiliza-lo no romaneio';
      End;
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjPEDIDOPROJETOROMANEIO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.Romaneio.Get_Codigo<>'')
        Then Strtoint(Self.Romaneio.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Romaneio';
     End;
     try
        If (Self.PedidoProjeto.Get_Codigo<>'')
        Then Strtoint(Self.PedidoProjeto.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Pedido/Projeto';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPEDIDOPROJETOROMANEIO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPEDIDOPROJETOROMANEIO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

Function TObjPedidoProjetoromaneio.Localiza_PedidoProjeto_Romaneio(PPedidoProjeto,Promaneio:string):Boolean;
Begin
       if (PpedidoProjeto='')
       or (Promaneio='')
       Then Begin
                 Messagedlg('Par�metro inv�lidos',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Romaneio,PedidoProjeto');
           SQL.ADD(' from  TabPedidoProjetoRomaneio');
           SQL.ADD(' WHERE PedidoProjeto='+PPedidoProjeto);
           SQL.ADD(' and Romaneio='+Promaneio);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


function TObjPEDIDOPROJETOROMANEIO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PEDIDOPROJETOROMANEIO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Romaneio,PedidoProjeto');
           SQL.ADD(' from  TabPedidoProjetoRomaneio');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPEDIDOPROJETOROMANEIO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPEDIDOPROJETOROMANEIO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPEDIDOPROJETOROMANEIO.create(Owner:TComponent);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin

        Self.Owner := Owner;
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;
        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjqueryPesquisa;
        Self.ParametroPesquisa:=TStringList.create;
        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Romaneio:=TOBJROMANEIO.create;
        Self.PedidoProjeto:=TOBJPEDIDO_PROJ.create;
//CODIFICA CRIACAO DE OBJETOS
        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabPedidoProjetoRomaneio(CODIGO,Romaneio');
                InsertSQL.add(' ,PedidoProjeto)');
                InsertSQL.add('values (:CODIGO,:Romaneio,:PedidoProjeto)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabPedidoProjetoRomaneio set CODIGO=:CODIGO,Romaneio=:Romaneio');
                ModifySQL.add(',PedidoProjeto=:PedidoProjeto');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabPedidoProjetoRomaneio where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPEDIDOPROJETOROMANEIO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPEDIDOPROJETOROMANEIO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPEDIDOPROJETOROMANEIO');
     Result:=Self.ParametroPesquisa;
end;

function TObjPEDIDOPROJETOROMANEIO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de PEDIDOPROJETOROMANEIO ';
end;


function TObjPEDIDOPROJETOROMANEIO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPEDIDOPROJETOROMANEIO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENPEDIDOPROJETOROMANEIO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPEDIDOPROJETOROMANEIO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ObjqueryPesquisa);
    Freeandnil(Self.ObjDatasource);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Romaneio.FREE;
    Self.PedidoProjeto.FREE;
//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPEDIDOPROJETOROMANEIO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPEDIDOPROJETOROMANEIO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPedidoProjetoRomaneio.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjPedidoProjetoRomaneio.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
//CODIFICA GETSESUBMITS


procedure TObjPEDIDOPROJETOROMANEIO.EdtRomaneioExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Romaneio.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Romaneio.tabelaparaobjeto;
End;
procedure TObjPEDIDOPROJETOROMANEIO.EdtRomaneioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Romaneio.Get_Pesquisa,Self.Romaneio.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Romaneio.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Romaneio.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Romaneio.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjPEDIDOPROJETOROMANEIO.EdtPedidoProjetoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PedidoProjeto.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PedidoProjeto.tabelaparaobjeto;
End;
procedure TObjPEDIDOPROJETOROMANEIO.EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PedidoProjeto.Get_PesquisaView,Self.PedidoProjeto.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoProjeto.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.PedidoProjeto.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoProjeto.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjPEDIDOPROJETOROMANEIO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPEDIDOPROJETOROMANEIO';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Romaneio');
                items.add('Itens da Entrega (Sem separar por Cliente)');
                items.add('Romaneio Anal�tico');
                Items.Add('Imprimir somente os vidros do romaneio')
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:Self.ImprimeRomaneio(pcodigo);
            1:Self.ImprimeRomaneio_Sem_separarCliente(pcodigo);
            2:Self.ImprimeRomaneio_analitico(pcodigo);
            3:Self.ImprimeSomenteVidros(Pcodigo);
          End;
     end;

end;



procedure TObjPEDIDOPROJETOROMANEIO.RetornaPedidosRomaneio(
  Promaneio: string);
begin
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select TabPedido_proj.pedido,');
          sql.add('TabPedidoProjetoromaneio.PedidoProjeto,');
          sql.add('TabPedido_Proj.projeto,');
          sql.add('Tabprojeto.descricao,');
          sql.add('TabPedido.Cliente,TabCliente.nome,');
          sql.add('TabPedidoProjetoromaneio.codigo');
          sql.add('from TabPedidoProjetoromaneio');
          sql.add('join tabpedido_proj on tabPedidoProjetoromaneio.PedidoProjeto=tabPedido_Proj.codigo');
          sql.add('join Tabpedido on TabPedido_proj.pedido=TabPedido.codigo');
          sql.add('join tabCliente on TabPedido.Cliente=tabCliente.codigo');
          sql.add('join tabProjeto on TabPedido_Proj.projeto=tabprojeto.codigo');
          sql.add('where TabPedidoProjetoromaneio.Romaneio='+Promaneio);
          if (Promaneio='')
          then exit;
          open;
     End;
end;

procedure TObjPEDIDOPROJETOROMANEIO.EdtPedidoProjetoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState; Ppedido: string);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa_PedidoProjeto_nao_usado(Ppedido),Self.PedidoProjeto.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PedidoProjeto.RETORNACAMPOCODIGO).asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;

end;

function TObjPEDIDOPROJETOROMANEIO.Get_Pesquisa_PedidoProjeto_nao_usado(
  Ppedido: string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select viewpedidoprojeto.* from viewpedidoprojeto');
     Self.ParametroPesquisa.add('left join tabpedidoprojetoromaneio on tabpedidoprojetoromaneio.pedidoprojeto=viewpedidoprojeto.codigo');
     Self.ParametroPesquisa.add('where viewpedidoprojeto.pedido='+ppedido);
     Self.ParametroPesquisa.add('and TabPedidoProjetoromaneio.Codigo is null');

     Result:=Self.ParametroPesquisa;
end;

procedure TObjPEDIDOPROJETOROMANEIO.Opcoes(Promaneio: string);
begin
     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Concluir Romaneio');
          RgOpcoes.Items.add('Retornar Romaneio');
          Showmodal;
          if (Tag=0)
          Then exit;

          Case RgOpcoes.itemindex of
            0:Begin
                   Self.ConcluirRomaneio(Promaneio);
            End;

            1:Begin
                   Self.RetornarRomaneio(Promaneio);
            end;
          end;
     End;
end;

procedure TObjPEDIDOPROJETOROMANEIO.ConcluirRomaneio(Promaneio: string);
var
temp:string;
ObjPedidoObjetos:TObjPedidoObjetos;
PcomissaoColocador:string;
ObjComissaoColocador:TobjComissaoColocador;
begin
     //************************************************************************

     if (ObjPermissoesUsoGlobal.ValidaPermissao('CONCLUIR ROMANEIO')=False)
     Then exit;
     
     if (PRomaneio='')
     then Begin
               Messagedlg('Escolha um romaneio',mtinformation,[mbok],0);
               exit;
     End;

     If (Self.Romaneio.LocalizaCodigo(promaneio)=False)
     Then Begin
               Messagedlg('Romaneio n�o localizado!',mterror,[mbok],0);
               exit;
     End;
     Self.Romaneio.TabelaparaObjeto;

     if (Self.Romaneio.Get_Concluido='S')
     Then Begin
               Messagedlg('O Romaneio j� foi Conclu�do!',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.Romaneio.Colocador.Get_CODIGO='')
     Then Begin
               Messagedlg('� necess�rio possuir um Colocador para este Romaneio',mterror,[mbok],0);
               exit;
     End;

     Try
        Strtofloat(Self.Romaneio.Colocador.Get_Comissao);
        PComissaoColocador:=Self.Romaneio.Colocador.Get_Comissao;
     Except
           Messagedlg('Comiss�o inv�lida do Colocador do Romaneio',mterror,[mbok],0);
           exit;
     End;

     //No Processo de Conclusao todos os pedidosprojetos ligado
     //a este romaneio sofrem a baixa do estoque

     With Self.Objquery do
     Begin
          //verificando se algum pedido desses projetos esta com concluido='N'
          Close;
          sql.clear;
          sql.add('Select distinct(tabPedido.Codigo) as PEDIDO from tabpedidoprojetoromaneio');
          sql.add('join tabpedido_proj on tabpedidoprojetoromaneio.pedidoprojeto=tabpedido_proj.codigo');
          sql.add('join tabpedido on tabPedido_Proj.Pedido=Tabpedido.codigo');
          sql.add('where Tabpedido.Concluido=''N'' ');
          sql.add('and tabPedidoprojetoromaneio.romaneio='+Promaneio);
          open;
          if (Recordcount>0)
          Then Begin
                    first;
                    temp:='';
                    temp:=Fieldbyname('pedido').asstring;
                    next;
                    While not (eof) do
                    Begin
                         temp:=Temp+','+Fieldbyname('pedido').asstring;
                          next;
                    End;
                    Messagedlg('O(s) pedido(s) '+temp+' n�o foram conclu�dos',mterror,[mbok],0);
                    exit;
          End;
          close;
     End;
     
     Try
        objPedidoObjetos:=TObjPedidoObjetos.create(self.Owner);
     Except
           Messagedlg('Erro na tentativa de Criar o objeto de Pedidos',mterror,[mbok],0);
           exit;
     End;

     Try
        ObjComissaoColocador:=TobjComissaoColocador.create(Self.Owner);
     Except
           ObjPedidoObjetos.Free;
           Messagedlg('Erro na tentativa de Criar o Objeto de Comiss�o de Colocadores',mterror,[mbok],0);
           exit;
     End;


     Try
        With Self.Objquery do
        Begin
             //*****************************************************************
             close;
             sql.clear;
             sql.add('Select TabPedidoProjetoromaneio.codigo as PEDIDOPROJETOROMANEIO,TabPedidoProjetoromaneio.pedidoprojeto');
             sql.add('from TabPedidoProjetoromaneio');
             sql.add('where TabPedidoProjetoRomaneio.Romaneio='+Promaneio);
             open;
             if (recordcount=0)
             Then Begin
                       Messagedlg('N�o existe nenhum Pedido Projeto ligados a este Romaneio',mtinformation,[mbok],0);
                       exit;
             End;
             first;

             While not(eof) do
             Begin
                  //Aqui da baixa no estoque
                  //Lembrando que se o cliente � obrigado a emitir sped, o estoque s� baixado na emiss�o de notas fiscais
                  if (ObjPedidoObjetos.DiminuiEstoque(Fieldbyname('pedidoprojetoromaneio').asstring,Fieldbyname('pedidoprojeto').asstring,Self.Romaneio.Get_DataEntrega)=False)
                  Then Begin
                            FDataModulo.IBTransaction.RollbackRetaining;
                            exit;
                  End;

                  //exportando a contabilidade do Financeiro
                  if (ObjPedidoObjetos.ExportaContabilidade_financeiro_Romaneio(Fieldbyname('pedidoprojeto').asstring,promaneio,Self.Romaneio.Get_DataEntrega)=False)
                  Then Begin
                            FDataModulo.IBTransaction.RollbackRetaining;
                            exit;
                  End;

                  //Exportando a Contabilidade do ESTOQUE do PEDIDOPROJETO atual (CMV)
                  if (ObjPedidoObjetos.ExportaContabilidade_Estoque_Romaneio(Fieldbyname('pedidoprojeto').asstring,promaneio,Self.Romaneio.Get_DataEntrega)=False)
                  Then Begin
                            FDataModulo.IBTransaction.RollbackRetaining;
                            exit;
                  End;

                  next;
             End;
             //****************************************************************

             //acertando a comissao dos colocadores na Tabela TabComissaoColocadors
             if (ObjComissaoColocador.AcertaComissaoporRomaneio(Promaneio)=False)
             Then Begin
                        FDataModulo.IBTransaction.RollbackRetaining;
                        Messagedlg('Erro na tentativa de Acertar a Comiss�o do Colocador',mterror,[mbok],0);
                        exit;
             End;


              Self.Romaneio.LocalizaCodigo(Promaneio);
              Self.Romaneio.Status:=dsedit;
              Self.Romaneio.Submit_Concluido('S');
              //Self.Romaneio.Submit_dataEntrega(datetostr(now));
              if (Self.Romaneio.Salvar(True)=False)//passo com TRUE assim commita tudo se der certo
              Then begin
                        FDataModulo.IBTransaction.RollbackRetaining;
                        Messagedlg('Erro na tentativa de Salvar o Romaneio como Concluido',mterror,[mbok],0);
                        exit;
              End;
              Messagedlg('Romaneio Conclu�do com Sucesso!',mtinformation,[mbok],0);
              exit;
        End;

     Finally
            ObjPedidoObjetos.Free;
            ObjComissaoColocador.Free;
     End;
end;

procedure TObjPEDIDOPROJETOROMANEIO.RetornarRomaneio(Promaneio: string);
var
ObjPedidoObjetos:TObjPedidoObjetos;
ObjComissao_Adiant_FuncFolha:TobjComissao_Adiant_FuncFolha;
begin

     if (ObjPermissoesUsoGlobal.ValidaPermissao('RETORNAR ROMANEIO')=False)
     Then exit;


     if (PRomaneio='')
     then Begin
               Messagedlg('Escolha um romaneio',mtinformation,[mbok],0);
               exit;
     End;

     If (Self.Romaneio.LocalizaCodigo(promaneio)=False)
     Then Begin
               Messagedlg('Romaneio n�o localizado!',mterror,[mbok],0);
               exit;
     End;
     Self.Romaneio.TabelaparaObjeto;

     if(self.Romaneio.Get_Concluido='N') then
     begin
            MensagemErro('Este Romaneio n�o foi conclu�do');
            Exit;
     end;

     Try
        objPedidoObjetos:=TObjPedidoObjetos.create(Self.Owner);
     Except
           Messagedlg('Erro na tentativa de Criar o objeto de Pedidos',mterror,[mbok],0);
           exit;
     End;

     Try
        ObjComissao_Adiant_FuncFolha:=TobjComissao_Adiant_FuncFolha.create(self.Owner);
     Except
           ObjPedidoObjetos.Free;
           Messagedlg('Erro na tentativa de Criar o Objeto de Comiss�o de Colocadores',mterror,[mbok],0);
           exit;
     End;

     Try
        With Self.Objquery do
        Begin
             //verifica se existe um ordem de instala��o para esse romaneio
             Close;
             sql.Clear;
             sql.Add('select  troi.*,toi.data,toi.colocador from tabromaneiosordeminstalacao troi join tabordeminstalacao toi on troi.ordeminstalacao=toi.codigo where romaneio='+promaneio);

             Open;
             if(recordcount>0) then
             begin
                  MensagemAviso('J� foi agendada a instala��o de itens desse romaneio!!!');
                  FmostraStringList.Memo.clear;
                  FmostraStringList.Memo.Lines.add('*** ITENS COM A INSTALA��O AGENDADA ***');
                  FmostraStringList.Memo.Lines.add(' ');
                  FmostraStringList.Memo.Lines.add('*** Cancele a instala��o dos itens  ***');
                  FmostraStringList.Memo.Lines.add('DATA         COLOCADOR  PEDIDOPROJETO  ROMANEIO');
                  FmostraStringList.Memo.Lines.add(' ');
                  while not eof do
                  begin
                      FmostraStringList.Memo.Lines.add(fieldbyname('data').AsString+'    '+fieldbyname('colocador').asstring+'  '+fieldbyname('pedidoprojeto').asstring+'  '+fieldbyname('romaneio').asstring);
                      Next;
                  end;
                  FmostraStringList.ShowModal;
                  Exit;
             end;


             close;
             sql.clear;
             sql.add('Select *');
             sql.add('from TabPedidoProjetoromaneio');
             sql.add('where Romaneio='+Promaneio);
             open;
             if (recordcount=0)
             Then Begin
                       Messagedlg('N�o existe nenhum Pedido Projeto ligados a este Romaneio',mtinformation,[mbok],0);
                       exit;
             End;
             first;

             While not(eof) do
             Begin
                  if (OBJESTOQUEGLOBAL.ExcluiPedidoProjetoRomaneio(fieldbyname('codigo').asstring,False)=False)
                  Then Begin
                            MensagemErro('Ao tentar excluir a baixa de estoque do Romaneio, no pedido/projeto/romaneio '+Fieldbyname('codigo').asstring);
                            FDataModulo.IBTransaction.RollbackRetaining;
                            exit;
                  End;
                  next;
              End;
             //Aumentando o estoque
             //ObjPedidoObjetos.AumentaEstoque(fieldbyname('codigo').asstring,Fieldbyname('pedidoprojeto').asstring,Self.Romaneio.Get_DataEntrega);

              if (ObjComissao_Adiant_FuncFolha.ExcluiComissaoColocadorporRomaneio(Promaneio)=False)
              Then begin
                        FDataModulo.IBTransaction.RollbackRetaining;
                        Messagedlg('Erro na tentativa de Acertar as Comiss�es de Colocadores do Romaneio',mterror,[mbok],0);
                        exit;
              End;

              //Retornando a Contabilidade

              With Self.Objquery do
              begin
                   //verificando se a contabilidade foi exportada
                   close;
                   sql.clear;
                   sql.add('Select count(codigo) as CONTA from tabExportaContabilidade');
                   sql.add('where Objgerador='+#39+'OBJROMANEIO'+#39);
                   sql.add('and Codgerador='+Promaneio);
                   sql.add('and Exportado=''S'' ');
              
                   Try
                      open;
                      if (fieldbyname('conta').asinteger>0)
                      then begin
                                if (MessageDlg('Alguns registros da contabilidade foram exportados, certeza que deseja retornar o pedido?',mtConfirmation,[mbyes,mbno],0)=mrno)
                                then Begin
                                          FDataModulo.IBTransaction.RollbackRetaining;
                                          exit;
                                End;                                           
              
                                if (Self.PedidoProjeto.Pedido.Titulo.ObjExportaContabilidade.LancaInverso('OBJROMANEIO',Promaneio,datetostr(now))=False)
                                then begin
                                          FDataModulo.IBTransaction.RollbackRetaining;
                                          mensagemerro('N�o foi poss�vel lan�ar o inverso na contabilidade');
                                          exit;
                                End;
                      End
                      Else Begin
                                //apagando a contabilidade do estoque e do financeiro
                                close;
                                sql.clear;
                                sql.add('Delete from tabExportaContabilidade');
                                sql.add('where Objgerador='+#39+'OBJROMANEIO'+#39);
                                sql.add('and Codgerador='+Promaneio);
                                execsql;
                      End;
                   except
                      FDataModulo.IBTransaction.RollbackRetaining;
                      MensagemErro('N�o foi poss�vel excluir a contabilidade do Pedido');
                      exit;
                   End;
              
              
              End;

              Self.Romaneio.LocalizaCodigo(Promaneio);
              Self.Romaneio.Status:=dsedit;
              Self.Romaneio.Submit_Concluido('N');
              if (Self.Romaneio.Salvar(True)=False)//passo com TRUE assim commita tudo se der certo
              Then begin
                        FDataModulo.IBTransaction.RollbackRetaining;
                        Messagedlg('Erro na tentativa de Salvar o Romaneio como n�o Concluido',mterror,[mbok],0);
                        exit;
              End;
              Messagedlg('Romaneio Retornado com Sucesso!',mtinformation,[mbok],0);
              exit;

        End;
     Finally
            ObjPedidoObjetos.Free;
            ObjComissao_Adiant_FuncFolha.Free;
     End;
end;

procedure TObjPEDIDOPROJETOROMANEIO.ImprimeRomaneio(PRomaneio: string);
var
templinha,linha:integer;
pcliente:string;
ObjqueryServico:tibquery;
Psomacliente,
Psomadesconto,psomafinal,Pprevisaocomissao:Currency;
ppedido,TEMP,temp1,temp2:string;
begin



     if (Promaneio='')
     then begin
               Messagedlg('Escolha o Romaneio',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.Romaneio.LocalizaCodigo(PRomaneio)=false)
     Then Begin
               Messagedlg('Romaneio n�o encontrado',mterror,[mbok],0);
               exit;
     end;
     self.Romaneio.TabelaparaObjeto;

     Pprevisaocomissao:=Self.CalculaPrevisaoComissao(promaneio);

     Try
        ObjqueryServico:=tibquery.Create(nil);
        ObjqueryServico.DataBase:=FDataModulo.IBDatabase;
     Except
           Messagedlg('Erro na Tentativa de Gerar a Query de Servi�o',mterror,[mbok],0);
           exit;
     End;

Try


     With Self.Objquery do
     begin
          close;
          SQL.clear;

          sql.add('Select tabpedido.cliente,PMPP.material,');
          sql.add('PMPP.materialcor,');
          sql.add('PMPP.codigomaterial,');
          sql.add('PMPP.ReferenciaMaterial,');
          sql.add('PMPP.nomematerial,');
          sql.add('PMPP.nomecor,');
          sql.add('PMPP.valor,');
          sql.add('PMPP.unidade,PMPP.referenciamaterial,');
          sql.add('sum(PMPP.quantidade) as SOMAQUANTIDADE,');
          sql.add('sum(PMPP.valorfinal) as SOMAVALORFINAL');
          sql.add('from tabpedidoprojetoromaneio TPPR');
          sql.add('join proc_materiais_romaneio('+promaneio+') PMPP on TPPR.pedidoprojeto=PMPP.pedidoprojeto');
          sql.add('join tabpedido_proj on PMPP.pedidoprojeto=tabpedido_proj.codigo');
          sql.add('join tabpedido on tabpedido_proj.pedido=tabpedido.codigo');
          sql.add('group by tabpedido.cliente,PMPP.material,');
          sql.add('PMPP.materialcor,PMPP.codigomaterial,PMPP.referenciamaterial,');
          sql.add('PMPP.nomematerial,');
          sql.add('PMPP.nomecor,');
          sql.add('PMPP.valor,');
          sql.add('PMPP.unidade');
          sql.add('order by Tabpedido.cliente,PMPP.material,pmpp.nomematerial');
          open;
          

     Try
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S17cpp;
          FreltxtRDPRINT.RDprint.TamanhoQteColunas:=130;

          FreltxtRDPRINT.RDprint.Abrir;

          if (FreltxtRDPRINT.RDprint.Setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          end;

          linha:=3;

          //esta ordenado pelo cliente,depois pelo material(ferragem,vidro...) em seguinda pelo nome do material
          Pcliente:=fieldbyname('cliente').asstring;
          Self.imprimeDadosCliente(Pcliente,PRomaneio,linha,true);
          Psomacliente:=0;

          while not(eof) do
          begin
              if (Pcliente<>fieldbyname('cliente').asstring)
              Then Begin
                        //***********servicos**************
                        ObjqueryServico.close;
                        ObjqueryServico.sql.clear;
                        ObjqueryServico.sql.add('Select tabpedido.cliente,tabservico.codigo,tabservico.referencia,sum(tabservico_pp.quantidade) as SOMAQUANTIDADE,tabservico.descricao,tabservico_pp.valor,');
                        ObjqueryServico.sql.add('sum(tabservico_pp.valorfinal) as SOMAVALORFINAL');
                        ObjqueryServico.sql.add('from tabpedidoprojetoromaneio');
                        ObjqueryServico.sql.add('join tabpedido_proj on tabpedidoprojetoromaneio.pedidoprojeto=tabpedido_proj.codigo');
                        ObjqueryServico.sql.add('join tabpedido on tabpedido.codigo=tabpedido_proj.pedido');
                        ObjqueryServico.sql.add('join TabServico_pp on TabPedido_proj.codigo=Tabservico_pp.pedidoprojeto');
                        ObjqueryServico.sql.add('join tabservico on TabServico_pp.servico=tabservico.codigo');
                        ObjqueryServico.sql.add('where TabPedido.Cliente='+pcliente);
                        ObjqueryServico.sql.add('and TabPedidoProjetoRomaneio.romaneio='+promaneio);
                        ObjqueryServico.sql.add('group by tabpedido.cliente,tabservico.codigo,tabservico.referencia,tabservico.descricao,tabservico_pp.valor');
                        ObjqueryServico.open;
                        ObjqueryServico.first;
                        While not (ObjqueryServico.eof) do
                        Begin
                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                             FreltxtRDPRINT.RDprint.Imp (linha,1,
                                                  CompletaPalavra(ObjqueryServico.fieldbyname('referencia').asstring,6,' ')+' '+
                                                  CompletaPalavra('',2,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(ObjqueryServico.fieldbyname('somaquantidade').asstring,5,' ')+' '+
                                                  CompletaPalavra(ObjqueryServico.fieldbyname('descricao').asstring,47,' '));

                             inc(linha,1);
                             Psomacliente:=Psomacliente+ObjqueryServico.fieldbyname('somavalorfinal').asfloat;
                             ObjqueryServico.next;
                        End;

                        //Resgatando os C�digos dos Pedidos Projetos e Pedidos desse cliente

                        ObjqueryServico.close;
                        ObjqueryServico.sql.clear;
                        ObjqueryServico.sql.add('Select tabpedido_proj.pedido,tabpedidoprojetoromaneio.pedidoprojeto');
                        ObjqueryServico.sql.add('from tabpedidoprojetoromaneio');
                        ObjqueryServico.sql.add('join tabpedido_proj on tabpedidoprojetoromaneio.pedidoprojeto=tabpedido_proj.codigo');
                        ObjqueryServico.sql.add('join tabpedido on tabpedido.codigo=tabpedido_proj.pedido');
                        ObjqueryServico.sql.add('where TabPedido.Cliente='+pcliente);
                        ObjqueryServico.sql.add('and TabPedidoProjetoRomaneio.romaneio='+promaneio);
                        ObjqueryServico.sql.add('order by Tabpedido_proj.pedido');
                        ObjqueryServico.open;
                        ObjqueryServico.first;
                        ppedido:=ObjqueryServico.fieldbyname('pedido').asstring;
                        temp:='PEDIDO: '+ppedido+' PPJ:';
                        While not (ObjqueryServico.eof) do
                        Begin
                            //ver como vai imprimir esses dados
                            if (ppedido<>ObjqueryServico.fieldbyname('pedido').asstring)
                            then Begin
                                      ppedido:=ObjqueryServico.fieldbyname('pedido').asstring;
                                      temp:=temp+' PEDIDO: '+ppedido+' PPJ:';

                            End;

                            temp:=temp+' '+ObjqueryServico.fieldbyname('pedidoprojeto').asstring;
                            ObjqueryServico.next;
                        End;

                        inc(linha,1);
                        //tenho a variavel temp com os dados que deverao ser impressos
                        while (temp<>'') do
                        Begin
                             DividirValorCOMSEGUNDASEMTAMANHO(temp,90,temp1,temp2);
                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                             FreltxtRDPRINT.RDprint.Imp (linha,1,temp1);
                             inc(linha,1);
                             temp:=temp2;
                             temp2:='';
                        End;





                        Psomacliente:=0;
                        Pcliente:=fieldbyname('cliente').asstring;
                        Self.imprimeDadosCliente(pcliente,promaneio,linha,False);
              End;
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
              FreltxtRDPRINT.RDprint.Imp (linha,1,CompletaPalavra(fieldbyname('referenciamaterial').asstring,20,' ')+' '+
                                                  CompletaPalavra(fieldbyname('unidade').asstring,2,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(fieldbyname('somaquantidade').asstring,5,' ')+' '+
                                                  CompletaPalavra(fieldbyname('nomematerial').asstring,36,' ')+' '+
                                                  CompletaPalavra(fieldbyname('nomecor').asstring,10,' '));
              inc(linha,1);
              Psomacliente:=Psomacliente+fieldbyname('somavalorfinal').asfloat;
              next;
          end;

          //***********servicos**************
          ObjqueryServico.close;
          ObjqueryServico.sql.clear;
          ObjqueryServico.sql.add('Select tabpedido.cliente,tabservico.codigo,tabservico.referencia,sum(tabservico_pp.quantidade) as SOMAQUANTIDADE,tabservico.descricao,tabservico_pp.valor,');
          ObjqueryServico.sql.add('sum(tabservico_pp.valorfinal) as SOMAVALORFINAL');
          ObjqueryServico.sql.add('from tabpedidoprojetoromaneio');
          ObjqueryServico.sql.add('join tabpedido_proj on tabpedidoprojetoromaneio.pedidoprojeto=tabpedido_proj.codigo');
          ObjqueryServico.sql.add('join tabpedido on tabpedido.codigo=tabpedido_proj.pedido');
          ObjqueryServico.sql.add('join TabServico_pp on TabPedido_proj.codigo=Tabservico_pp.pedidoprojeto');
          ObjqueryServico.sql.add('join tabservico on TabServico_pp.servico=tabservico.codigo');
          ObjqueryServico.sql.add('where TabPedido.Cliente='+pcliente);
          ObjqueryServico.sql.add('and TabPedidoProjetoRomaneio.romaneio='+promaneio);

          ObjqueryServico.sql.add('group by tabpedido.cliente,tabservico.codigo,tabservico.referencia,tabservico.descricao,tabservico_pp.valor');
          ObjqueryServico.open;
          ObjqueryServico.first;
          While not (ObjqueryServico.eof) do
          Begin
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
               FreltxtRDPRINT.RDprint.Imp (linha,1,
                                    CompletaPalavra(ObjqueryServico.fieldbyname('codigo').asstring,6,' ')+' '+
                                    CompletaPalavra('',2,' ')+' '+
                                    CompletaPalavra_a_Esquerda(ObjqueryServico.fieldbyname('somaquantidade').asstring,5,' ')+' '+
                                    CompletaPalavra(ObjqueryServico.fieldbyname('descricao').asstring,47,' '));
               inc(linha,1);
               Psomacliente:=Psomacliente+ObjqueryServico.fieldbyname('somavalorfinal').asfloat;
               ObjqueryServico.next;
          End;


           //Resgatando os C�digos dos Pedidos Projetos e Pedidos desse cliente

           ObjqueryServico.close;
           ObjqueryServico.sql.clear;
           ObjqueryServico.sql.add('Select tabpedido_proj.pedido,tabpedidoprojetoromaneio.pedidoprojeto');
           ObjqueryServico.sql.add('from tabpedidoprojetoromaneio');
           ObjqueryServico.sql.add('join tabpedido_proj on tabpedidoprojetoromaneio.pedidoprojeto=tabpedido_proj.codigo');
           ObjqueryServico.sql.add('join tabpedido on tabpedido.codigo=tabpedido_proj.pedido');
           ObjqueryServico.sql.add('where TabPedido.Cliente='+pcliente);
           ObjqueryServico.sql.add('and TabPedidoProjetoRomaneio.romaneio='+promaneio);
           ObjqueryServico.sql.add('order by Tabpedido_proj.pedido');
           ObjqueryServico.open;
           ObjqueryServico.first;
           ppedido:=ObjqueryServico.fieldbyname('pedido').asstring;
           temp:='PEDIDO: '+ppedido+' PPJ:';
           While not (ObjqueryServico.eof) do
           Begin
               //ver como vai imprimir esses dados
               if (ppedido<>ObjqueryServico.fieldbyname('pedido').asstring)
               then Begin
                         ppedido:=ObjqueryServico.fieldbyname('pedido').asstring;
                         temp:=temp+' PEDIDO: '+ppedido+' PPJ:';
               End;

               temp:=temp+' '+ObjqueryServico.fieldbyname('pedidoprojeto').asstring;
               ObjqueryServico.next;
           End;

           inc(linha,1);
           //tenho a variavel temp com os dados que deverao ser impressos
           while (temp<>'') do
           Begin
                DividirValorCOMSEGUNDASEMTAMANHO(temp,90,temp1,temp2);
                FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                FreltxtRDPRINT.RDprint.Imp (linha,1,temp1);
                inc(linha,1);
                temp:=temp2;
                temp2:='';
           End;

           templinha:=linha;
           Self.ImprimeCaixaTransportadora(linha);

           if ((linha-templinha)<10)
           then linha:=templinha+13;

           FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
           FreltxtRDPRINT.RDprint.Impf(linha,1,'Previs�o de Comiss�o: R$ '+formata_valor(Pprevisaocomissao),[negrito]);
           inc(linha,1);

     Finally
          freltxtrdprint.RDprint.Fechar;
     end;



     end;
Finally
       Freeandnil(ObjqueryServico);
end;

end;

procedure TObjPEDIDOPROJETOROMANEIO.ImprimeRomaneio_analitico(PRomaneio: string);
var
linha:integer;
pcliente:string;
ObjqueryServico:tibquery;
Psomacliente,
Psomadesconto,psomafinal:Currency;
Plarguraxaltura,ppedido,TEMP,temp1,temp2:string;
PimprimeAlturaXLargura,agrupa:boolean;
begin

     PimprimeAlturaXLargura:=False;
     if (ObjParametroGlobal.ValidaParametro('IMPRIMIR ALTURAXLARGURA NO ROMANEIO')=TRUE)
     Then Begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               Then PimprimeAlturaXLargura:=True;
     End;


     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Agrupar itens iguais');
          RgOpcoes.Items.add('N�o Agrupar');

          showmodal;

          if (tag=0)
          then exit;

          if (RgOpcoes.ItemIndex=0)
          then Agrupa:=True
          Else Agrupa:=False;
     End;

     if (Promaneio='')
     then begin
               Messagedlg('Escolha o Romaneio',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.Romaneio.LocalizaCodigo(PRomaneio)=false)
     Then Begin
               Messagedlg('Romaneio n�o encontrado',mterror,[mbok],0);
               exit;
     end;
     self.Romaneio.TabelaparaObjeto;

     Try
        ObjqueryServico:=tibquery.Create(nil);
        ObjqueryServico.DataBase:=FDataModulo.IBDatabase;
     Except
           Messagedlg('Erro na Tentativa de Gerar a Query de Servi�o',mterror,[mbok],0);
           exit;
     End;

Try

     With Self.Objquery do
     begin
          close;
          SQL.clear;

          sql.add('Select tabpedido.cliente,PMPP.material,');
          sql.add('PMPP.materialcor,');
          sql.add('PMPP.codigomaterial,');
          sql.add('PMPP.ReferenciaMaterial,');
          sql.add('PMPP.nomematerial,');
          sql.add('PMPP.nomecor,');
          sql.add('PMPP.valor,');
          sql.add('PMPP.unidade,PMPP.referenciamaterial,PMPP.altura,PMPP.largura,');

          if (Agrupa)
          then Begin
                    sql.add('sum(PMPP.quantidade) as quantidade,');
                    sql.add('sum(PMPP.valorfinal) as valorfinal');
          End
          Else BEgin
                  sql.add('PMPP.quantidade,');
                  sql.add('PMPP.valorfinal');
          End;
          
          sql.add('from tabpedidoprojetoromaneio TPPR');
          sql.add('join proc_materiais_romaneio('+promaneio+') PMPP on TPPR.pedidoprojeto=PMPP.pedidoprojeto');
          sql.add('join tabpedido_proj on PMPP.pedidoprojeto=tabpedido_proj.codigo');
          sql.add('join tabpedido on tabpedido_proj.pedido=tabpedido.codigo');

          if (Agrupa)
          then Begin
                    sql.add('group by tabpedido.cliente,');
                    sql.add('PMPP.material,');
                    sql.add('PMPP.materialcor,');
                    sql.add('PMPP.codigomaterial,');
                    sql.add('PMPP.ReferenciaMaterial,');
                    sql.add('PMPP.nomematerial,');
                    sql.add('PMPP.nomecor,');
                    sql.add('PMPP.valor, ');
                    sql.add('PMPP.unidade,');
                    sql.add('PMPP.referenciamaterial,');
                    sql.add('PMPP.altura,');
                    sql.add('PMPP.largura');


          End;

          sql.add('order by Tabpedido.cliente,PMPP.material,pmpp.nomematerial');
          FmostraStringList.Memo.Lines.clear;
          FmostraStringList.Memo.Lines.text:=Sql.text;
          //FmostraStringList.showmodal;
          open;
          

     Try
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S17cpp;
          FreltxtRDPRINT.RDprint.TamanhoQteColunas:=130;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.Setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          end;

          linha:=3;

          //esta ordenado pelo cliente,depois pelo material(ferragem,vidro...) em seguinda pelo nome do material
          Pcliente:=fieldbyname('cliente').asstring;
          Self.imprimeDadosCliente(Pcliente,PRomaneio,linha,true);
          Psomacliente:=0;

          while not(eof) do
          begin
              if (Pcliente<>fieldbyname('cliente').asstring)
              Then Begin
                        //***********servicos**************
                        ObjqueryServico.close;
                        ObjqueryServico.sql.clear;
                        ObjqueryServico.sql.add('Select tabpedido.cliente,tabservico.codigo,tabservico.referencia,tabservico_pp.quantidade,tabservico.descricao,tabservico_pp.valor,');
                        ObjqueryServico.sql.add('tabservico_pp.valorfinal');
                        ObjqueryServico.sql.add('from tabpedidoprojetoromaneio');
                        ObjqueryServico.sql.add('join tabpedido_proj on tabpedidoprojetoromaneio.pedidoprojeto=tabpedido_proj.codigo');
                        ObjqueryServico.sql.add('join tabpedido on tabpedido.codigo=tabpedido_proj.pedido');
                        ObjqueryServico.sql.add('join TabServico_pp on TabPedido_proj.codigo=Tabservico_pp.pedidoprojeto');
                        ObjqueryServico.sql.add('join tabservico on TabServico_pp.servico=tabservico.codigo');
                        ObjqueryServico.sql.add('where TabPedido.Cliente='+pcliente);
                        ObjqueryServico.sql.add('and TabPedidoProjetoRomaneio.romaneio='+promaneio);
                        ObjqueryServico.open;
                        ObjqueryServico.first;
                        While not (ObjqueryServico.eof) do
                        Begin
                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                             FreltxtRDPRINT.RDprint.Imp (linha,1,
                                                  CompletaPalavra(ObjqueryServico.fieldbyname('referencia').asstring,20,' ')+' '+
                                                  CompletaPalavra(ObjqueryServico.fieldbyname('descricao').asstring,47,' ')+' '+
                                                  CompletaPalavra('',2,' ')+' '+
                                                  CompletaPalavra('',12,' ')+' '+
                                                  CompletaPalavra('',12,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(ObjqueryServico.fieldbyname('quantidade').asstring,12,' ')
                                                  );

                             inc(linha,1);
                             Psomacliente:=Psomacliente+ObjqueryServico.fieldbyname('valorfinal').asfloat;
                             ObjqueryServico.next;
                        End;

                        //Resgatando os C�digos dos Pedidos Projetos e Pedidos desse cliente

                        ObjqueryServico.close;
                        ObjqueryServico.sql.clear;
                        ObjqueryServico.sql.add('Select tabpedido_proj.pedido,tabpedidoprojetoromaneio.pedidoprojeto');
                        ObjqueryServico.sql.add('from tabpedidoprojetoromaneio');
                        ObjqueryServico.sql.add('join tabpedido_proj on tabpedidoprojetoromaneio.pedidoprojeto=tabpedido_proj.codigo');
                        ObjqueryServico.sql.add('join tabpedido on tabpedido.codigo=tabpedido_proj.pedido');
                        ObjqueryServico.sql.add('where TabPedido.Cliente='+pcliente);
                        ObjqueryServico.sql.add('and TabPedidoProjetoRomaneio.romaneio='+promaneio);
                        ObjqueryServico.sql.add('order by Tabpedido_proj.pedido');
                        ObjqueryServico.open;
                        ObjqueryServico.first;
                        ppedido:=ObjqueryServico.fieldbyname('pedido').asstring;
                        temp:='PEDIDO: '+ppedido+' PPJ:';
                        While not (ObjqueryServico.eof) do
                        Begin
                            //ver como vai imprimir esses dados
                            if (ppedido<>ObjqueryServico.fieldbyname('pedido').asstring)
                            then Begin
                                      ppedido:=ObjqueryServico.fieldbyname('pedido').asstring;
                                      temp:=temp+' PEDIDO: '+ppedido+' PPJ:';

                            End;

                            temp:=temp+' '+ObjqueryServico.fieldbyname('pedidoprojeto').asstring;
                            ObjqueryServico.next;
                        End;

                        inc(linha,1);
                        //tenho a variavel temp com os dados que deverao ser impressos
                        while (temp<>'') do
                        Begin
                             DividirValorCOMSEGUNDASEMTAMANHO(temp,90,temp1,temp2);
                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                             FreltxtRDPRINT.RDprint.Imp (linha,1,temp1);
                             inc(linha,1);
                             temp:=temp2;
                             temp2:='';
                        End;





                        Psomacliente:=0;
                        Pcliente:=fieldbyname('cliente').asstring;
                        Self.imprimeDadosCliente(pcliente,promaneio,linha,False);
              End;


              if (PimprimeAlturaXLargura=False)
              Then Begin
                       Plarguraxaltura:=CompletaPalavra_a_Esquerda(fieldbyname('largura').asstring,12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(fieldbyname('altura').asstring,12,' ')+' ';
              End
              Else Begin
                       Plarguraxaltura:=CompletaPalavra_a_Esquerda(fieldbyname('altura').asstring,12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(fieldbyname('largura').asstring,12,' ')+' ';
              End;


              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
              FreltxtRDPRINT.RDprint.Imp (linha,1,CompletaPalavra(fieldbyname('referenciamaterial').asstring,20,' ')+' '+
                                                  CompletaPalavra(fieldbyname('nomematerial').asstring,36,' ')+' '+
                                                  CompletaPalavra(fieldbyname('nomecor').asstring,10,' ')+' '+
                                                  CompletaPalavra(fieldbyname('unidade').asstring,2,' ')+' '+
                                                  PlarguraxAltura+
                                                  CompletaPalavra_a_Esquerda(fieldbyname('quantidade').asstring,12,' ')
                                                  );
              inc(linha,1);
              Psomacliente:=Psomacliente+fieldbyname('valorfinal').asfloat;
              next;
          end;

          //***********servicos**************
          ObjqueryServico.close;
          ObjqueryServico.sql.clear;
          ObjqueryServico.sql.add('Select tabpedido.cliente,tabservico.codigo,tabservico.referencia,tabservico_pp.quantidade,tabservico.descricao,tabservico_pp.valor,');
          ObjqueryServico.sql.add('tabservico_pp.valorfinal');
          ObjqueryServico.sql.add('from tabpedidoprojetoromaneio');
          ObjqueryServico.sql.add('join tabpedido_proj on tabpedidoprojetoromaneio.pedidoprojeto=tabpedido_proj.codigo');
          ObjqueryServico.sql.add('join tabpedido on tabpedido.codigo=tabpedido_proj.pedido');
          ObjqueryServico.sql.add('join TabServico_pp on TabPedido_proj.codigo=Tabservico_pp.pedidoprojeto');
          ObjqueryServico.sql.add('join tabservico on TabServico_pp.servico=tabservico.codigo');
          ObjqueryServico.sql.add('where TabPedido.Cliente='+pcliente);
          ObjqueryServico.sql.add('and TabPedidoProjetoRomaneio.romaneio='+promaneio);
          ObjqueryServico.open;
          ObjqueryServico.first;
          
          While not (ObjqueryServico.eof) do
          Begin
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
               FreltxtRDPRINT.RDprint.Imp (linha,1,
                                                  CompletaPalavra(ObjqueryServico.fieldbyname('referencia').asstring,20,' ')+' '+
                                                  CompletaPalavra(ObjqueryServico.fieldbyname('descricao').asstring,47,' ')+' '+
                                                  CompletaPalavra('',2,' ')+' '+
                                                  CompletaPalavra('',12,' ')+' '+
                                                  CompletaPalavra('',12,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(ObjqueryServico.fieldbyname('quantidade').asstring,12,' ')
                                                  );
               inc(linha,1);
               Psomacliente:=Psomacliente+ObjqueryServico.fieldbyname('valorfinal').asfloat;
               ObjqueryServico.next;
          End;


           //Resgatando os C�digos dos Pedidos Projetos e Pedidos desse cliente

           ObjqueryServico.close;
           ObjqueryServico.sql.clear;
           ObjqueryServico.sql.add('Select tabpedido_proj.pedido,tabpedidoprojetoromaneio.pedidoprojeto');
           ObjqueryServico.sql.add('from tabpedidoprojetoromaneio');
           ObjqueryServico.sql.add('join tabpedido_proj on tabpedidoprojetoromaneio.pedidoprojeto=tabpedido_proj.codigo');
           ObjqueryServico.sql.add('join tabpedido on tabpedido.codigo=tabpedido_proj.pedido');
           ObjqueryServico.sql.add('where TabPedido.Cliente='+pcliente);
           ObjqueryServico.sql.add('and TabPedidoProjetoRomaneio.romaneio='+promaneio);
           ObjqueryServico.sql.add('order by Tabpedido_proj.pedido');
           ObjqueryServico.open;
           ObjqueryServico.first;
           ppedido:=ObjqueryServico.fieldbyname('pedido').asstring;
           temp:='PEDIDO: '+ppedido+' PPJ:';
           While not (ObjqueryServico.eof) do
           Begin
               //ver como vai imprimir esses dados
               if (ppedido<>ObjqueryServico.fieldbyname('pedido').asstring)
               then Begin
                         ppedido:=ObjqueryServico.fieldbyname('pedido').asstring;
                         temp:=temp+' PEDIDO: '+ppedido+' PPJ:';
               End;

               temp:=temp+' '+ObjqueryServico.fieldbyname('pedidoprojeto').asstring;
               ObjqueryServico.next;
           End;

           inc(linha,1);
           //tenho a variavel temp com os dados que deverao ser impressos
           while (temp<>'') do
           Begin
                DividirValorCOMSEGUNDASEMTAMANHO(temp,90,temp1,temp2);
                FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                FreltxtRDPRINT.RDprint.Imp (linha,1,temp1);
                inc(linha,1);
                temp:=temp2;
                temp2:='';
           End;

          Self.ImprimeCaixaTransportadora(linha);
     Finally
          freltxtrdprint.RDprint.Fechar;
     end;



     end;
Finally
       Freeandnil(ObjqueryServico);
end;

end;



procedure TObjPEDIDOPROJETOROMANEIO.imprimeDadosCliente(Pcliente,Promaneio: string;
  var linha: integer;Pprimeiro:boolean);
var
PLARGURAALTURA:string;  
begin
     //************************************************************************************
     if (self.PedidoProjeto.Pedido.Cliente.LocalizaCodigo(pcliente)=False)
     Then begin
               Messagedlg('Cliente N� '+pcliente+' n�o foi encontrado',mterror,[mbok],0);
               exit;
     End;
     self.PedidoProjeto.Pedido.Cliente.TabelaparaObjeto;
     //************************************************************************************
     if (Pprimeiro=False)
     Then Begin
             self.ImprimeCaixaTransportadora(linha);
             linha:=3;
             FreltxtRDPRINT.RDprint.Novapagina;
     End;
     //************************************************************************************
     FreltxtRDPRINT.RDprint.ImpC(linha,65,'ROMANEIO N� '+PRomaneio,[negrito]);
     inc(linha,2);

     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
     FreltxtRDPRINT.RDprint.Imp(linha,1,completapalavra('_',130,'_'));
     inc(linha,1);
     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
     FreltxtRDPRINT.RDprint.ImpC(linha,65,'DADOS DO CLIENTE',[negrito]);
     inc(linha,1);
     //FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
     //FreltxtRDPRINT.RDprint.Imp(linha,1,completapalavra('_',90,'_'));
     //inc(linha,2);

     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
     FreltxtRDPRINT.RDprint.Imp (linha,1,CompletaPalavra('Nome    : '+Self.PedidoProjeto.Pedido.Cliente.Get_Nome,50,' ')+completapalavra(' C�digo: '+Self.PedidoProjeto.Pedido.Cliente.Get_codigo,40,' '));
     inc(linha,1);
     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
     FreltxtRDPRINT.RDprint.Imp (linha,1,CompletaPalavra('Endere�o: '+Self.PedidoProjeto.Pedido.Cliente.Get_Endereco,70,' ')+completapalavra(' Bairro: '+Self.PedidoProjeto.Pedido.Cliente.Get_Bairro,30,' '));
     inc(linha,1);
     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
     FreltxtRDPRINT.RDprint.Imp (linha,1,CompletaPalavra('Cidade  : '+Self.PedidoProjeto.Pedido.Cliente.Get_Cidade,30,' ')+completapalavra(' UF: '+Self.PedidoProjeto.Pedido.Cliente.Get_Estado,10,' ')+completapalavra(' CEP: '+Self.PedidoProjeto.Pedido.Cliente.Get_Estado,30,' '));
     inc(linha,1);
     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
     FreltxtRDPRINT.RDprint.Imp (linha,1,CompletaPalavra('Complemento  : '+Self.PedidoProjeto.Pedido.Cliente.Get_Complemento,30,' '));
     inc(linha,1);
     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
     FreltxtRDPRINT.RDprint.Imp (linha,1,CompletaPalavra('CNPJ/CPF: '+Self.PedidoProjeto.Pedido.Cliente.Get_CPF_CGC,30,' ')+completapalavra(' Insc.Est.: '+Self.PedidoProjeto.Pedido.Cliente.Get_RG_IE,30,' ')+completapalavra(' TEL/FAX: '+Self.PedidoProjeto.Pedido.Cliente.Get_Fone+' '+Self.PedidoProjeto.Pedido.Cliente.Get_Fax,30,' '));
     inc(linha,1);
     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
     FreltxtRDPRINT.RDprint.Imp (linha,1,CompletaPalavra('Celular: '+Self.PedidoProjeto.Pedido.Cliente.Get_Celular,30,' ')+completapalavra(' Contato: '+Self.PedidoProjeto.Pedido.Cliente.Get_Contato,30,' '));
     inc(linha,1);


     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
     FreltxtRDPRINT.RDprint.Imp(linha,1,completapalavra('_',130,'_'));
     inc(linha,1);

     PLARGURAALTURA:=CompletaPalavra_a_Esquerda('LARGURA',12,' ')+' '+
                     CompletaPalavra_a_Esquerda('ALTURA',12,' ')+' ';

     if (ObjParametroGlobal.ValidaParametro('IMPRIMIR ALTURAXLARGURA NO ROMANEIO')=TRUE)
     Then Begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               Then PLARGURAALTURA:=CompletaPalavra_a_Esquerda('ALTURA',12,' ')+' '+
                                    CompletaPalavra_a_Esquerda('LARGURA',12,' ')+' ';
     End;


     FreltxtRDPRINT.RDprint.Impf (linha,1,CompletaPalavra('REFER�NCIA',20,' ')+' '+
                                                  CompletaPalavra('DESCRICAO',36,' ')+' '+
                                                  CompletaPalavra('COR',10,' ')+' '+
                                                  CompletaPalavra('UN',2,' ')+' '+
                                                  PLARGURAALTURA+
                                                  CompletaPalavra_a_Esquerda('QUANTIDADE',12,' '),[negrito]
                                                  );
     inc(linha,1);
     //************************************************************************************
end;

procedure TObjPEDIDOPROJETOROMANEIO.ImprimeCaixaTransportadora(
  var linha: integer);
var
strimprime1,strimprime2,temp,temp2:string;
cont:integer;
begin

    //o box usa 12 linhas com as que espa�am, sendo assim
    //tenho q ver se vai dar pra imprimir 
    linha:=linha+12;
    FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
    if (linha>3)
    then linha:=linha-12;


    inc(linha,2);
    FreltxtRDPRINT.RDprint.Box(linha,1,Linha+10,90,false);
    inc(linha,1);

    FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
    FreltxtRDPRINT.RDprint.Imp(linha,2,'Emiss�o do Romaneio: '+Self.Romaneio.Get_DataEmissao);
    inc(linha,1);
    FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
    FreltxtRDPRINT.RDprint.Imp(linha,2,'Data de Entrega: '+Self.Romaneio.Get_DataEntrega);
    inc(linha,1);
    FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
    FreltxtRDPRINT.RDprint.Imp(linha,2,completapalavra('Transportadora: '+Self.Romaneio.transportadora.Get_CODIGO+'-'+Self.Romaneio.TRANSPORTADORA.Get_NOME,88,' '));
    inc(linha,1);
    FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
    FreltxtRDPRINT.RDprint.Imp(linha,2,completapalavra('Colocador: '+Self.Romaneio.Colocador.Get_CODIGO+'-'+Self.Romaneio.Colocador.Funcionario.Get_Nome,88,' '));
    inc(linha,1);
    FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
    FreltxtRDPRINT.RDprint.Imp(linha,2,'Placa: '+Self.Romaneio.transportadora.get_placa);
    inc(linha,1);
    FreltxtRDPRINT.RDprint.Impf(linha,2,'Observa��o: ',[negrito]);
    inc(linha,1);
    //imprimindo a observacao digitada no romaneio



    temp:=Self.romaneio.Get_Observacoes;
    temp2:='';
    for cont:=1 to length(temp) do
    begin
         if (length(temp2)=88)
         Then Begin
                   //Separando as palavras
                   temp2:=temp2+temp[cont];
                   strimprime1:=temp2;
                   DividirValorCOMSEGUNDASEMTAMANHO(strimprime1,88,strimprime2,temp2);
                   temp2:=trim(temp2);
                   //***********************************************************
                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                   FreltxtRDPRINT.RDprint.Imp(linha,2,strimprime2);
                   inc(linha,1);
         end
         Else temp2:=temp2+temp[cont];
    end;

    if (temp2<>'')
    Then Begin
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
              FreltxtRDPRINT.RDprint.Imp(linha,2,temp2);
              inc(linha,1);
    End;
end;

{function TObjPEDIDOPROJETOROMANEIO.VerificaporPedido(
  ppedido: string): boolean;
begin
     Result:=False;
     //verificando se tem algum pedidoprojeto pertence ao pedido em parametro
     //ligado a algum romaneio
     //este procedimento � chamando antes de retornar a venda de um pedido
     //senao ele volta pra concluido='N' e o pedidoprojeto fica ligado ao romaneio,
     //mas isso nao pode porque nao pode entregar pedido/projeto de pedidos que nao
     //foram concluidos
     With Self.Objquery do
     Begin
          close;
          SQL.clear;
          sql.add('Select count(TPPR.codigo) as conta from TabPedidoProjetoRomaneio TPPR');
          sql.add('join TabPedido_proj on TPPR.PedidoProjeto=TabPedido_proj.codigo');
          sql.add('join TabPedido on TabPedido_proj.pedido=tabpedido.codigo');
          sql.add('where TabPedido.codigo='+ppedido);
          open;
          if (fieldbyname('conta').asinteger=0)
          Then result:=True;
     End;
end;}


procedure TObjPEDIDOPROJETOROMANEIO.ImprimeRomaneio_Sem_separarCliente(PRomaneio: string);
var
linha:integer;
ObjqueryServico:tibquery;
Psomaquantidade,Psomadesconto,psomafinal:Currency;
begin
     if (Promaneio='')
     then begin
               Messagedlg('Escolha o Romaneio',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.Romaneio.LocalizaCodigo(PRomaneio)=false)
     Then Begin
               Messagedlg('Romaneio n�o encontrado',mterror,[mbok],0);
               exit;
     end;
     self.Romaneio.TabelaparaObjeto;

     Try
        ObjqueryServico:=tibquery.Create(nil);
        ObjqueryServico.DataBase:=FDataModulo.IBDatabase;
     Except
           Messagedlg('Erro na Tentativa de Gerar a Query de Servi�o',mterror,[mbok],0);
           exit;
     End;

Try


     With Self.Objquery do
     begin
          close;
          SQL.clear;
          sql.add('Select proc_materiais_romaneio.material,');
          sql.add('proc_materiais_romaneio.materialcor,');
          sql.add('proc_materiais_romaneio.codigomaterial,');
          sql.add('proc_materiais_romaneio.ReferenciaMaterial,');
          sql.add('proc_materiais_romaneio.nomematerial,');
          sql.add('proc_materiais_romaneio.nomecor,');
          sql.add('proc_materiais_romaneio.valor,');
          sql.add('proc_materiais_romaneio.unidade,proc_materiais_romaneio.referenciamaterial,');
          sql.add('sum(proc_materiais_romaneio.quantidade) as SOMAQUANTIDADE,');
          sql.add('sum(proc_materiais_romaneio.valorfinal) as SOMAVALORFINAL');
          sql.add('from proc_materiais_romaneio('+promaneio+')');
          sql.add('group by proc_materiais_romaneio.material,');
          sql.add('proc_materiais_romaneio.materialcor,proc_materiais_romaneio.codigomaterial,proc_materiais_romaneio.referenciamaterial,');
          sql.add('proc_materiais_romaneio.nomematerial,');
          sql.add('proc_materiais_romaneio.nomecor,');
          sql.add('proc_materiais_romaneio.valor,');
          sql.add('proc_materiais_romaneio.unidade');
          sql.add('order by proc_materiais_romaneio.material,proc_materiais_romaneio.nomematerial');
          open;

     Try
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S17cpp;
          FreltxtRDPRINT.RDprint.TamanhoQteColunas:=130;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.Setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          end;

          linha:=3;
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impc(linha,65,'ITENS DO ROMANEIO '+Promaneio,[negrito]);
          inc(linha,2);
          
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('REFER�NCIA',15,' ')+' '+
                                              CompletaPalavra('UN',2,' ')+' '+
                                              CompletaPalavra_a_Esquerda('QUANT',12,' ')+' '+
                                              CompletaPalavra('DESCRICAO',98,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Imp (linha,1,CompletaPalavra('_',130,'_'));
          inc(linha,1);
          
          Psomaquantidade:=0;
          while not(eof) do
          begin
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
              FreltxtRDPRINT.RDprint.Imp (linha,1,CompletaPalavra(fieldbyname('referenciamaterial').asstring,15,' ')+' '+
                                                  CompletaPalavra(fieldbyname('unidade').asstring,2,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(fieldbyname('somaquantidade').asstring,12,' ')+' '+
                                                  CompletaPalavra(fieldbyname('nomematerial').asstring,47,' ')+' '+
                                                  CompletaPalavra(fieldbyname('nomecor').asstring,50,' '));
              inc(linha,1);
              Psomaquantidade:=PsomaQuantidade+fieldbyname('somaquantidade').asfloat;
              next;
          end;
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Imp (linha,1,CompletaPalavra('_',130,'_'));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf (linha,1,'SOMA DAS QUANTIDADES: '+floattostr(psomaquantidade),[negrito]);
          inc(linha,1);
          

     Finally
          freltxtrdprint.RDprint.Fechar;
     end;



     end;
Finally
       Freeandnil(ObjqueryServico);
end;

end;

function TObjPEDIDOPROJETOROMANEIO.Get_pesquisaAnalitica: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('select * from VIEWPEDIDOPROJETONOROMANEIO');
     Result:=Self.ParametroPesquisa;
end;

function TObjPEDIDOPROJETOROMANEIO.CalculaPrevisaoComissao(
  Promaneio: string): currency;
begin
     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select sum((tabcolocador.comissao*(tabcomissaocolocador.basedecalculo*tabcomissaocolocador.porcentagempp)/100)/100) as VALORCOMISSAO');
          sql.add('from tabpedidoprojetoromaneio');
          sql.add('join tabcomissaocolocador on tabpedidoprojetoromaneio.pedidoprojeto=tabcomissaocolocador.pedidoprojeto');
          sql.add('join tabromaneio on tabpedidoprojetoromaneio.romaneio=tabromaneio.codigo');
          sql.add('join tabcolocador on tabromaneio.colocador=tabcolocador.codigo');
          sql.add('where tabromaneio.codigo='+promaneio);
          open;
          result:=fieldbyname('valorcomissao').ascurrency;
     End;
end;

procedure TObjPEDIDOPROJETOROMANEIO.ImprimeSomenteVidros(Pcodigo:string);
var
  ObjRelPersReportBuilder:TObjRELPERSREPORTBUILDER;
begin
      if(Pcodigo='')
      then Exit;
      try

            try
                   ObjRelPersreportBuilder:=TObjRELPERSREPORTBUILDER.Create;
            except
                   MensagemErro('Erro ao tentar criar o Objeto ObjRelPersreportBuilder');
                   exit;
            end;

            if (ObjRelPersReportBuilder.LocalizaNOme('IMPRIME VIDROS NO ROMANEIO') = false)
            then Begin
                       MensagemErro('O Relatorio de ReporteBuilder "IMPRIME VIDROS NO ROMANEIO" n�o foi econtrado');
                       exit;
            end;

            ObjRelPersReportBuilder.TabelaparaObjeto;
            ObjRelPersReportBuilder.SQLCabecalhoPreenchido:=StrReplace(ObjRelPersReportBuilder.Get_SQLCabecalho,':PCODIGO',Pcodigo);
            ObjRelPersReportBuilder.SQLRepeticaoPreenchido:=StrReplace(ObjRelPersReportBuilder.get_SQLRepeticao,':PCODIGO',Pcodigo);
            ObjRelPersReportBuilder.ChamaRelatorio(false);


      finally
            ObjRelPersreportBuilder.Free;
      end;
end;

end.





