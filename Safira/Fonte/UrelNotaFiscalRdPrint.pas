unit UrelNotaFiscalRdPrint;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  RDprint, StdCtrls, Buttons,inifiles, Menus, ExtCtrls;

type
  TFrelNotaFiscalRdPrint = class(TForm)
    ComponenteRdPrint: TRDprint;
    MainMenu1: TMainMenu;
    ini1: TMenuItem;
    Opes1: TMenuItem;
    IMPRIMEFOLHADEGUIA1: TMenuItem;
    SalvaPosies1: TMenuItem;
    ImprimeTeste1: TMenuItem;
    lbNaturezaOperacao: TLabel;
    lbcfop: TLabel;
    LbInscrEstSubstTribut: TLabel;
    LbDataEmissao: TLabel;
    LbFonefaxDestRemete: TLabel;
    LbNomeDestinatRemete: TLabel;
    LbCnpjDestRemete: TLabel;
    LbUfDestRemete: TLabel;
    LbEnderecoDestRemete: TLabel;
    LbBairroDestRemete: TLabel;
    LbCepDestRemete: TLabel;
    LbMunicipioDestRemete: TLabel;
    LbIeDestRemete: TLabel;
    LbDataSaida: TLabel;
    LbHorasaida: TLabel;
    lbcontrolesup: TLabel;
    lbsaidaentrada: TLabel;
    lbbasecalculo: TLabel;
    LbValoricms: TLabel;
    LbBaseCalculoICMSSubstituicao: TLabel;
    LBValorICMSSubstituicao: TLabel;
    LbValorFrete: TLabel;
    LbValorSeguro: TLabel;
    LbOutrasDespesas: TLabel;
    LbValorTotalIPI: TLabel;
    LbValorTotalNota: TLabel;
    LbNomeTransportadora: TLabel;
    LbFreteporcontaTransportadora: TLabel;
    LbPlacaVeiculoTransportadora: TLabel;
    LbUFTransportadora: TLabel;
    LbCnpjTransportadora: TLabel;
    LbEnderecoTransportadora: TLabel;
    LbMunicipioTransportadora: TLabel;
    LbUFEnderecoTransportadora: TLabel;
    LBIETransportadora: TLabel;
    LbQuantidadeTransportadora: TLabel;
    LbEspecieTransportadora: TLabel;
    LbMarcaTransportadora: TLabel;
    LbNumeroTransportadora: TLabel;
    lBPesoBrutoTransportadora: TLabel;
    LbPesoLiquidoTransportadora: TLabel;
    ValorTotalProdutos: TLabel;
    lbcontroleinf: TLabel;
    MEMOPRODUTOS: TLabel;
    MEMOINFORMACOESCOMPLEMENTARES: TLabel;
    PanelPOsicoes: TPanel;
    edtleft: TEdit;
    edttop: TEdit;
    LbColuna: TLabel;
    lblinha: TLabel;
    LbQtdCaracteres: TLabel;
    edtquantidade: TEdit;
    lbvencimentofatura1: TLabel;
    lbvalorfatura1: TLabel;
    lbvencimentofatura2: TLabel;
    lbvalorfatura2: TLabel;
    lbvencimentofatura3: TLabel;
    lbvalorfatura3: TLabel;
    lbvencimentofatura4: TLabel;
    lbvalorfatura4: TLabel;
    lbvencimentofatura5: TLabel;
    lbvalorfatura5: TLabel;
    procedure ini1Click(Sender: TObject);
    procedure IMPRIMEFOLHADEGUIA1Click(Sender: TObject);
    procedure PanelPOsicoesDockOver(Sender: TObject;
      Source: TDragDockObject; X, Y: Integer; State: TDragState;
      var Accept: Boolean);
    procedure FormDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure FormDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure lbcontrolesupClick(Sender: TObject);
    procedure edtleftKeyPress(Sender: TObject; var Key: Char);
    procedure edttopKeyPress(Sender: TObject; var Key: Char);
    procedure lbcontrolesupMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure edtquantidadeKeyPress(Sender: TObject; var Key: Char);
    procedure SalvaPosies1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure PassaLABELPRETO;
    Procedure AbrePosicoesINI;

    { Private declarations }
  public
    { Public declarations }
    Procedure CarregaConfiguracoesGerais;
    function PegaCampoIni(nomecampo: string; out VALOR: STRING): Boolean;
  end;

var
  FrelNotaFiscalRdPrint: TFrelNotaFiscalRdPrint;

implementation

uses UoBJConfiguraFolhaRdprint;

{$R *.DFM}


procedure TFrelNotaFiscalRdPrint.ini1Click(Sender: TObject);
begin
     Self.CarregaConfiguracoesGerais;
     Messagedlg('Processamento Conclu�do!',mtinformation,[mbok],0);
end;

procedure TFrelNotaFiscalRdPrint.IMPRIMEFOLHADEGUIA1Click(Sender: TObject);
var
cont:integer;
cont2:integer;
begin
     ComponenteRdPrint.Abrir;
     for cont:=1 to ComponenteRdPrint.TamanhoQteColunas do
     Begin
         ComponenteRdPrint.Imp(1,cont,inttostr(cont)[length(inttostr(cont))]);
     End;

     for cont:=2 to ComponenteRdPrint.TamanhoQteLinhas do
     Begin
         ComponenteRdPrint.Imp(cont,1,inttostr(cont));
     End;
     ComponenteRdPrint.Novapagina;
     ComponenteRdPrint.Imp(1,1,'NOVA FOLHA');
     ComponenteRdPrint.fechar;
end;

procedure TFrelNotaFiscalRdPrint.PanelPOsicoesDockOver(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
begin
     Accept:=True;
     PanelPOsicoes.Left:=X;
     PanelPOsicoes.top:=y;

end;

procedure TFrelNotaFiscalRdPrint.FormDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
     accept:=true;
end;

procedure TFrelNotaFiscalRdPrint.FormDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
     PanelPOsicoes.Left:=x;
     PanelPOsicoes.top:=y;
end;

procedure TFrelNotaFiscalRdPrint.lbcontrolesupClick(Sender: TObject);
begin
     PassaLABELPRETO;
     PanelPOsicoes.Caption:=TLabel(Sender).Name;
     TLabel(Sender).Font.Color:=clBlue;
     edtleft.text:=floattostr(int(TLabel(Sender).left/5));
     edttop.text:=floattostr(int((TLabel(Sender).Top+self.VertScrollBar.Position)/5));
     edtquantidade.text:=inttostr(TLabel(Sender).tag);
     edtleft.setfocus;
     VertScrollBar.Position:=0;


end;

procedure TFrelNotaFiscalRdPrint.PassaLABELPRETO;//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to Self.ComponentCount -1
   do Begin
        if uppercase(Self.Components [int_habilita].ClassName) = 'TLABEL'
        then Tedit(Self.Components [int_habilita]).font.color:=clblack;
   End;

end;


procedure TFrelNotaFiscalRdPrint.edtleftKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then
        if key=#13
        Then Begin
               if (edtleft.Text='0')
               or (edttop.text='0')
               Then Begin
                         MessageDlg('Nem a linha ou coluna pode ter valor Zero',mterror,[mbok],0);
                         exit;
               End;

               TLabel(Self.FindComponent(PanelPOsicoes.caption)).left:=5*strtoint(edtleft.text);
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).top:=5*(strtoint(edttop.text));
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).tag:=strtoint(edtquantidade.text);
               edttop.setfocus;
        End
        Else key:=#0;
end;

procedure TFrelNotaFiscalRdPrint.edttopKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then
        if key=#13
        Then Begin
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).left:=5*strtoint(edtleft.text);
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).top:=(5*strtoint(edttop.text))-VertScrollBar.position;
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).tag:=strtoint(edtquantidade.text);
               edtquantidade.setfocus;
        End
        Else key:=#0;
end;

procedure TFrelNotaFiscalRdPrint.lbcontrolesupMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
     Tlabel(Sender).hint:=Tlabel(Sender).name;
End;

procedure TFrelNotaFiscalRdPrint.FormShow(Sender: TObject);
begin
     CarregaConfiguracoesGerais;
     lbcontrolesup.onclick(lbcontrolesup);
end;

procedure TFrelNotaFiscalRdPrint.edtquantidadeKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then
        if key=#13
        Then Begin
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).left:=5*strtoint(edtleft.text);
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).top:=5*(strtoint(edttop.text));
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).tag:=strtoint(edtquantidade.text);
               edtleft.setfocus;
        End
        Else key:=#0;
end;

procedure TFrelNotaFiscalRdPrint.SalvaPosies1Click(Sender: TObject);
var
int_habilita:integer;
NOMECAMPO,Temp:string;
arquivo_ini:Tinifile;
cont:integer;
begin
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     temp:=Temp+'CONFRELS\CAMPOSNOTAFISCAL.ini';

     if (FileExists(temp)=False)
     then Begin
                  Messagedlg('O Arquivo '+temp+' n�o foi encontrado',mterror,[mbok],0);
                  exit;
     End;
     
     Try
        Arquivo_ini:=Tinifile.Create(temp);
     Except
           Messagedlg('Erro na Abertura do Arquivo '+temp,mterror,[mbok],0);
           exit;
     End;
Try
     Try

        for int_habilita:=0 to Self.ComponentCount -1
        do Begin
                if uppercase(Self.Components [int_habilita].ClassName) = 'TLABEL'
                then BEGIN
                          arquivo_ini.WriteString('CONFIGURACOESLABELS',UPPERCASE(Self.Components [int_habilita].Name)+'_LINHA',FLOATTOSTR(INT((TLabel(Self.FindComponent(Self.Components [int_habilita].NAME)).TOP/5))));
                          arquivo_ini.WriteString('CONFIGURACOESLABELS',UPPERCASE(Self.Components [int_habilita].Name)+'_COLUNA',FLOATTOSTR(INT((TLabel(Self.FindComponent(Self.Components [int_habilita].NAME)).left/5))));
                          arquivo_ini.WriteString('CONFIGURACOESLABELS',UPPERCASE(Self.Components [int_habilita].Name)+'_QUANTIDADE',FLOATTOSTR(INT((TLabel(Self.FindComponent(Self.Components [int_habilita].NAME)).TAG))));
                END;
        End;
        Messagedlg('Processamento Conclu�do!',mtinformation,[mbok],0);
     Except
           Messagedlg('Erro na escrita da CHAVE '+NOMECAMPO,mterror,[mbok],0);
           exit;
     End;
Finally
     FreeAndNil(arquivo_ini);
End;
end;



procedure TFrelNotaFiscalRdPrint.AbrePosicoesINI;
var
int_habilita:integer;
NOMECAMPO,Temp:string;
arquivo_ini:Tinifile;
cont:integer;
begin
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        temp:=Temp+'CONFRELS\CAMPOSNOTAFISCAL.ini';

        if (FileExists(temp)=False)
        then Begin
                  Messagedlg('O Arquivo '+temp+' n�o foi encontrado',mterror,[mbok],0);
                  exit;
        End;

        Arquivo_ini:=Tinifile.Create(temp);
     Except
           Messagedlg('Erro na Abertura do Arquivo '+temp,mterror,[mbok],0);
           exit;
     End;
Try
     Try

        for int_habilita:=0 to Self.ComponentCount -1
        do Begin

                if uppercase(Self.Components [int_habilita].ClassName) = 'TLABEL'
                then BEGIN
                          temp:=arquivo_ini.ReadString('CONFIGURACOESLABELS',UPPERCASE(Self.Components [int_habilita].Name)+'_LINHA','');
                          if(Temp<>'')
                          then begin
                              try
                                  strtoint(temp);
                                  TLabel(Self.FindComponent(Self.Components [int_habilita].NAME)).top:=(5*strtoint(temp));
                              except

                              end;
                          end;
                          temp:=arquivo_ini.ReadString('CONFIGURACOESLABELS',UPPERCASE(Self.Components [int_habilita].Name)+'_COLUNA','');
                          if(Temp<>'')
                          then begin
                              try
                                  strtoint(temp);
                                  TLabel(Self.FindComponent(Self.Components [int_habilita].NAME)).left:=(5*strtoint(temp));
                              except

                              end;
                          end;
                          temp:=arquivo_ini.ReadString('CONFIGURACOESLABELS',UPPERCASE(Self.Components [int_habilita].Name)+'_QUANTIDADE','');
                          if(Temp<>'')
                          then begin
                              try
                                   strtoint(temp);
                                   TLabel(Self.FindComponent(Self.Components [int_habilita].NAME)).tag:=(strtoint(temp));
                              except

                              end;
                          end;

                END;
        End;

    Except
          Messagedlg('Erro na escrita da CHAVE '+NOMECAMPO,mterror,[mbok],0);
          exit;
    End;
Finally
     FreeAndNil(arquivo_ini);
End;
end;

procedure TFrelNotaFiscalRdPrint.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
keyc:char;
begin
     keyc:=#13;
     if key=vk_left
     Then begin
                if (strtoint(edtleft.text)>=1)
                Then  edtleft.text:=inttostr(strtoint(edtleft.text)-1);
                self.edtleftKeyPress(Sender,keyc);
     End
     Else
        if key=vk_right
        Then Begin
                edtleft.text:=inttostr(strtoint(edtleft.text)+1);
                self.edtleftKeyPress(Sender,keyc);
        End
        Else
            if key=vk_up
            Then Begin
                if (strtoint(edttop.text)>=1)
                Then edttop.text:=inttostr(strtoint(edttop.text)-1);
                self.edtleftKeyPress(Sender,keyc);
            End
            Else
                if key=vk_down
                Then Begin
                        edttop.text:=inttostr(strtoint(edttop.text)+1);
                        self.edtleftKeyPress(Sender,keyc);
                End;
end;

procedure TFrelNotaFiscalRdPrint.CarregaConfiguracoesGerais;
var
  ObjConfiguraRdPrint:TObjConfiguraFolhaRdPrint;
begin
     Try
        ObjConfiguraRdPrint:=TObjConfiguraFolhaRdPrint.create;

        if (ObjConfiguraRdPrint.ResgataConfiguracoesFolha(ComponenteRdPrint,'RELNOTAFISCAL.INI')=False)
        Then Begin
                  freeandnil(ObjConfiguraRdPrint);
                  strtoint('s');
        End;

         freeandnil(ObjConfiguraRdPrint);



         Self.AbrePosicoesINI;
     Except
           Messagedlg('Erro',mterror,[mbok],0);
     End;
End;

Function TFrelNotaFiscalRdPrint.PegaCampoIni(nomecampo:string;OUT VALOR:STRING): Boolean;
var
Temp:string;
arquivo_ini:Tinifile;
begin
     Result:=False;
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        temp:=Temp+'CONFRELS\CAMPOSNOTAFISCAL.Ini';

        if (FileExists(temp)=False)
        then Begin
                  Messagedlg('O Arquivo '+temp+' n�o foi encontrado',mterror,[mbok],0);
                  exit;
        End;

        Arquivo_ini:=Tinifile.Create(temp);
     Except
           Messagedlg('Erro na Abertura do Arquivo '+temp,mterror,[mbok],0);
           exit;
     End;

     Try
        Try
            Temp:='';
            Temp:=arquivo_ini.ReadString('SISTEMA',NOMECAMPO,'');
            valor:=temp;
            result:=true;
        Except
              Messagedlg('Erro na Leitura da chave '+NOMECAMPO+' do arquivo INI!',mterror,[mbok],0);
              result:=False;
              exit;
        End;
     finally
        freeandnil(arquivo_ini);
     End;

End;


end.
