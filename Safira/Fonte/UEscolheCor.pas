unit UEscolheCor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, CheckLst;

type
  TFEscolheCor = class(TForm)
    CheckListBoxCor: TCheckListBox;
    CheckListBoxCodigoCor: TCheckListBox;
    CheckBoxMarcarDesmarcar: TCheckBox;
    EdtPesquisa: TEdit;
    bitbtn1: TSpeedButton;
    BitBtn2: TSpeedButton;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CheckListBoxCorClick(Sender: TObject);
    procedure CheckListBoxCorKeyPress(Sender: TObject; var Key: Char);
    procedure CheckBoxMarcarDesmarcarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtPesquisaExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEscolheCor: TFEscolheCor;

implementation

{$R *.dfm}

procedure TFEscolheCor.BitBtn2Click(Sender: TObject);
begin
    Self.Tag:=0;
    Close;
end;

procedure TFEscolheCor.BitBtn1Click(Sender: TObject);
Var Cont : Integer;
begin
     //Todos os Checados do 1� para o 2� Checkbox
     for Cont:=0 to CheckListBoxCor.Items.Count-1 do
     Begin
          CheckListBoxCodigoCor.Checked[cont]:=CheckListBoxCor.Checked[Cont];
     end;

     Self.Tag:=1;
     Close;
end;

procedure TFEscolheCor.CheckListBoxCorClick(Sender: TObject);
begin
     CheckListBoxCodigoCor.Checked[CheckListBoxCor.ItemIndex]:=CheckListBoxCor.Checked[CheckListBoxCor.ItemIndex];
end;

procedure TFEscolheCor.CheckListBoxCorKeyPress(Sender: TObject;
  var Key: Char);
begin
    CheckListBoxCodigoCor.Checked[CheckListBoxCor.ItemIndex]:=CheckListBoxCor.Checked[CheckListBoxCor.ItemIndex];
end;

procedure TFEscolheCor.CheckBoxMarcarDesmarcarClick(Sender: TObject);
VAr Cont : Integer;
begin
     for Cont := 0 to CheckListBoxCor.Items.Count-1 do
     Begin
          if (CheckBoxMarcarDesmarcar.Checked=true)then
          Begin
              CheckListBoxCor.Checked[Cont]:=true;
              CheckListBoxCodigoCor.Checked[cont]:=true;
          end else if (CheckBoxMarcarDesmarcar.Checked=false)then
          Begin
              CheckListBoxCor.Checked[Cont]:=false;
              CheckListBoxCodigoCor.Checked[Cont]:=false;
          end;
     end;

end;

procedure TFEscolheCor.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);

end;

procedure TFEscolheCor.EdtPesquisaExit(Sender: TObject);
Var Cont, X : Integer;

begin
   try
      For Cont:=0 to CheckListBoxCor.Items.Count-1 do
      Begin
           if Pos(EdtPesquisa.Text, CheckListBoxCor.Items[Cont])>0 then
           begin
                CheckListBoxCor.Selected[Cont]:=true;
                CheckListBoxCor.SetFocus;
                exit;
           end;
      end;

   except;

   end;
end;

procedure TFEscolheCor.FormShow(Sender: TObject);
begin
  Self.Color:=clBtnFace;
end;

end.
