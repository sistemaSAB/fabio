unit UProducao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, ExtCtrls, jpeg, Grids, ImgList,CodigoFigura,
  Mask, pngimage, Menus, ComCtrls, ShellAPI, ShellCtrls,IBQuery,UessencialGlobal;

type
  TFProducao = class(TForm)
    pnl1: TPanel;
    lbnomeformulario: TLabel;
    bt3: TSpeedButton;
    img1: TImage;
    panelpnl2: TPanel;
    img2: TImage;
    lbdiasemanarodape: TLabel;
    lbdatarodape: TLabel;
    lb6: TLabel;
    il1: TImageList;
    panelFundoProducao: TPanel;
    PaintBox1: TPaintBox;
    panel1: TPanel;
    img3: TImage;
    ListBox2: TListBox;
    STRGLinhaServicos: TStringGrid;
    panel3: TPanel;
    lbNomedoservicoprod: TLabel;
    lbCodigoPedido: TLabel;
    lbNomeProjeto: TLabel;
    img4: TImage;
    lb4: TLabel;
    lb5: TLabel;
    img5: TImage;
    imgAberto: TImage;
    lb3: TLabel;
    bvl2: TBevel;
    bvl3: TBevel;
    btConfiguracoes: TSpeedButton;
    PopUpConfiguracoes: TPopupMenu;
    N1Desfazer1: TMenuItem;
    ilImgPopMenu: TImageList;
    ImprimirCodigodeBarras1: TMenuItem;
    btPesquisar: TSpeedButton;
    PopUpPesquisar: TPopupMenu;
    MenuItem1: TMenuItem;
    PesquisarporPedido1: TMenuItem;
    PesquisarporCliente1: TMenuItem;
    tmrTempoExecucao: TTimer;
    btProcessar: TSpeedButton;
    btExecutar: TSpeedButton;
    btCancelar: TSpeedButton;
    btAnterior: TSpeedButton;
    btProximo: TSpeedButton;
    lbPedidoProjeto: TLabel;
    lbCliente: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Bevel1: TBevel;
    ExcluirPedidoProjeto1: TMenuItem;
    edtCodigoBarras: TEdit;
    shp1: TShape;
    DesfazerUltimoServionapeaSelecionada1: TMenuItem;
    ImprimirEtiquetasCodigodeBarras1: TMenuItem;
    lbMesa: TLabel;
    imgSucesso: TImage;
    imgFalse: TImage;
    lbNomeServicoMesa: TLabel;
    PesquisarporCodigodeBarras1: TMenuItem;
    LbInformacoes: TLabel;
    lbCordovidro: TLabel;
    procedure FormShow(Sender: TObject);
    procedure STRGLinhaServicosDrawCell(Sender: TObject; ACol,
      ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure STRGLinhaServicosDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btConfiguracoesClick(Sender: TObject);
    procedure edtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btPesquisarClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure btExecutarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure lbNomeProjetoMouseLeave(Sender: TObject);
    procedure panel3MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PaintBox1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btProximoClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure PesquisarporPedido1Click(Sender: TObject);
    procedure PesquisarporCliente1Click(Sender: TObject);
    procedure lbNomedoservicoprodMouseLeave(Sender: TObject);
    procedure N1Desfazer1Click(Sender: TObject);
    procedure ImprimirCodigodeBarras1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ExcluirPedidoProjeto1Click(Sender: TObject);
    procedure edtCodigoBarrasChange(Sender: TObject);
    procedure ImprimirEtiquetasCodigodeBarras1Click(Sender: TObject);
    procedure DesfazerUltimoServionapeaSelecionada1Click(Sender: TObject);
    procedure PesquisarporCodigodeBarras1Click(Sender: TObject);
    procedure edtCodigoBarrasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    ListaComponentes:TStringList;

    largura:Integer;
    altura:Integer;
    pInic : TPoint; //indica o ponto onde o botao do mouse foi pressionado
    pFim : TPoint;   //indica o ponto onde o botao do mouse foi solto
    pCurr : TPoint;  //indica o ponto corrente
    botao: Boolean;    //indica que o botao do mouse foi pressionado no paintboxtela
    botaoArras : Boolean;  // Variavel que controla se o botao esta sendo arrastado
    objDesenha: TDesenho; //Objeto do tipo TDesenho
    listaFigurasParaProducao: TList; //lista de figuras que estao no paintboxtela
    figCorrente : TRect;  //Variavel auxiliar usada para criar o rascunho da nova fig.
    cor : TColor;    //seleciona a cor
    op : Integer;   //tipo da figura: retangulo, circulo, linha
    contProducao : Integer;
    hab : Boolean; //Variavel que controla se as figuras estao habilitadas
    idFig : Integer; //Id da figura no banco de dados
    idCarr : Boolean; // Controla se o id da figura ja foi carregado
    ctrlTag : Integer;  //guarda o self.tag de uma figura quando uma tecla de atalho � pressionado
    tmpColor : Tcolor; //variavel temporaria que guarda a cor (usada no TListBox)
    listBoxContr : Boolean; // varivel que controla a entrada no TlistBox
    polyclick : Boolean;   // marca que a op��o do polyline sera desenhado
    points:  array[0..200] of Tpoint;  //array de pontos que guardara as informa��es dos clicks
    indCurr : Integer; //indice que determina quantos pontos ja foram selecionados para criar o poligon
    Drawing : Boolean; //indica que o botao foi clicado e que o desenho selecionado foi o poligon
    entrad : Boolean; //indica que � a primeira entrada dos pontos do poligon
    menorx : integer; //menor x encontrado nos pontos dos poligonos
    menory : integer; // meno y encontrado nos pontos dos poligonos
    pux_banc : boolean; //verifica se uma figura foi carregada do banco
    _CODIGOSERVICO:string;

    _ParametroPesquisaPedido:String;
    _ParametroPesquisaCliente:string;

    Function  ___MostraServicos(PedidoProjeto:string):Boolean;
    function  RetornaOperadorMatematico(str:string):string;
    Function  ___MostraServicos_PorComponente(RefComponente:string;PedidoProjeto:string):Boolean;
    function  LocalizaTagCompAoCarregar(parametro:string):Integer;
    procedure __LimpaTudoIMGProducao();
    procedure __CarregaDesenhoProjetoProducao(ComReferencias:Boolean;MostraLinhas:Boolean;Projeto:string);
    procedure __CriarFiguraIMGProducao(NomeFigura:string ; TipoMaterial:string; CodigoMaterial:string);
    procedure __Arruma ();
    procedure __DefineTamanhoPoligon();
    procedure __AdicionaListaIMGProducao(NomeFigura:string; TipoMaterial:string ; CodigoMaterial:string);
    procedure __LimpaTudo;
    procedure __MostraCorVidro(PedidoprojetoL:string);
    procedure __Preenchemedidas(CodigoPedidoProjeto,CodigoProjeto:string);

    procedure MarcaExecucaoServico();
    function __MarcaExecucaoServico_Componente(ReferenciaComponente,ESTADOPRODUCAO:string):boolean;
    procedure DesmarcaExecucaoServico;

    procedure __MarcaAguardandoServico;

    procedure DesabilitaHabilita(acao:Boolean);
    procedure __PosicionaServico;
    procedure __VerificaClickComponente(X,Y:Integer);
    procedure __KeyDownPedido(Sender: TObject;var Key: Word; Shift: TShiftState);
    procedure __KeyDownCliente(Sender: TObject;var Key: Word; Shift: TShiftState);
    procedure __ImprimeEtiquetas;
    procedure __ImprimeCodigosBarras;
    procedure __KeyDownComponentes(Sender: TObject;var Key: Word; Shift: TShiftState);
    procedure __LocalizaPecaCODIGOBARRAS;
    function ___ExecutarServico_Componente(codigo:string;Status,servico:string):Boolean;
    function ___VerificaConclusaoServicos(SERVICO,PEDIDOPROJETO:string):Boolean;
    function ___LiberaParaAlteracaoStatus(SERVICO,PEDIDOPROJETO,CODIGO,REFCOMPONENTE:string):boolean;
    procedure __LerConfiguracaoTipoMesa;

  public
    procedure __LocalizaProjetosPorPedido(ParametroPedido:string);
  end;

var
  FProducao: TFProducao;

implementation

uses UescolheImagemBotao,UDataModulo, Upesquisa, DB, DateUtils, UFiltraImp,
  Math, UobjRELPERSREPORTBUILDER, UpesquisaMenu, UAjuda, Uprincipal;

{$R *.dfm}

procedure TFProducao.FormShow(Sender: TObject);
begin
    FescolheImagemBotao.PegaFiguraImagem(Img2,'RODAPE');

    op := 0;
    contProducao := 0;
    cor := clBtnFace;
    idCarr := False; // indica que nenhuma figura foi carregada na tela
    hab := True; // Nesta inicializa��o decidi-se se a figura ira come�ar marcada ou nao
    Self.Tag := -1;
    ctrlTag := -1;
    polyclick := false;
    indCurr := 0;
    Entrad := true;

    listaFigurasParaProducao:= TList.Create;
    ListaComponentes:=TStringList.Create;
    __LerConfiguracaoTipoMesa;
    __LimpaTudo;

    if(_ParametroPesquisaPedido<>'')
    then btProximoClick(Sender);

    ___MostraServicos('');

end;

procedure TFProducao.__LerConfiguracaoTipoMesa;
var 
   Arquivo:TextFile;
   Valor:String;
begin
   AssignFile(arquivo,'ConfiguracaoLinhaProducao.txt');
   Reset(Arquivo); //abre o arquivo para leitura;

   While not eof(Arquivo) do
   begin
     Readln(arquivo,valor); //le do arquivo e desce uma linha. O conte�do lido � transferido para a vari�vel linha
   End;

   Closefile(Arquivo);

   lbMesa.Caption:=Valor;
   _CODIGOSERVICO:=get_campoTabela('codigo','referencia','tabservico',lbMesa.Caption);
   lbNomeServicoMesa.Caption:= get_campoTabela('descricao','codigo','tabservico',_CODIGOSERVICO);

end;

function TFProducao.___MostraServicos_PorComponente(RefComponente:string;PedidoProjeto:string):Boolean;
var
  QryMostraServicos_QUERY:Tibquery;
  Executando:Boolean;
begin
  if(RefComponente='')
  then Exit;

  Result:=False;

  QryMostraServicos_QUERY:=TIBQuery.Create(nil);
  QryMostraServicos_QUERY.Database:=FDataModulo.IBDatabase;
  Executando:=false;
  try
     QryMostraServicos_QUERY.Close;
     QryMostraServicos_QUERY.SQL.Clear;
     QryMostraServicos_QUERY.SQL.Text:=
     'select '+
     'tabpedidoproj_ordemproducao.estadoproducao, '+
     'tabservico.descricao as servico, '+
     'tabservico.referencia as referenciaservico, '+
     'tabprojeto.codigo, '+
     'tabpedido_proj.codigo as pedidoprojeto, '+
     'tabpedido.codigo, '+
     'tabprojeto.descricao as nomeprojeto, '+
     'tabpedidoproj_ordemproducao.codigo, '+
     'tabpedidoproj_ordemproducao.listadecomponentes '+
     'from tabpedidoproj_ordemproducao '+
     'join tabpedido_proj on tabpedido_proj.codigo=tabpedidoproj_ordemproducao.pedidoprojeto '+
     'join tabservico on tabpedidoproj_ordemproducao.servico=tabservico.codigo '+
     'join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto '+
     'join tabpedido on tabpedido.codigo=tabpedido_proj.pedido '+
     'join tabcomponentes_ordemproducao tcop on tcop.pp_ordemproducao=tabpedidoproj_ordemproducao.codigo '+
     'where tabpedidoproj_ordemproducao.pedidoprojeto='+PedidoProjeto+' '+
     'and tcop.refcomponente='+#39+RefComponente+#39+' '+
     'order by tabpedidoproj_ordemproducao.codigo';

     InputBox('','',QryMostraServicos_QUERY.SQL.Text);
  finally
     FreeAndNil(QryMostraServicos_QUERY);
  end;
end;

procedure TFProducao.__MostraCorVidro(PedidoprojetoL:string);
var
  qry_pesquisaL:TIBQuery;
begin
   qry_pesquisaL:=TIBQuery.Create(nil);
   qry_pesquisaL.Database:=FDataModulo.IBDatabase;
   try
     qry_pesquisaL.Close;
     qry_pesquisaL.SQL.Clear;
     qry_pesquisaL.SQL.Text:=
     'select descricao from tabvidro_pp    vp '+
     'join tabvidrocor vdc on vdc.codigo=vp.vidrocor '+
     'join tabcor cor on cor.codigo=vdc.cor '+
     'where pedidoprojeto='+PedidoprojetoL;

     qry_pesquisaL.Open;

     lbCordovidro.Caption:='Cor do Vidro: '+qry_pesquisaL.Fields[0].AsString;

   finally
     FreeAndNil(qry_pesquisaL);
   end;
end;

function TFProducao.___MostraServicos(PedidoProjeto:string):Boolean;
var
  QryMostraServicos_QUERY:Tibquery;
  Executando:Boolean;
begin
   if(PedidoProjeto='')
   then Exit;

   Result:=False;

   QryMostraServicos_QUERY:=TIBQuery.Create(nil);
   QryMostraServicos_QUERY.Database:=FDataModulo.IBDatabase;
   Executando:=false;
   try
     QryMostraServicos_QUERY.Close;
     QryMostraServicos_QUERY.SQL.Clear;
     QryMostraServicos_QUERY.SQL.Text:=
     'select '+
     'tabpedidoproj_ordemproducao.estadoproducao, '+
     'tabservico.descricao as servico, '+
     'tabservico.referencia as referenciaservico, '+
     'tabprojeto.codigo, '+
     'tabpedido_proj.codigo as pedidoprojeto, '+
     'tabpedido.codigo, '+
     'tabprojeto.descricao as nomeprojeto, '+
     'tabpedidoproj_ordemproducao.codigo, '+
     'tabpedidoproj_ordemproducao.listadecomponentes '+
     'from tabpedidoproj_ordemproducao '+
     'join tabpedido_proj on tabpedido_proj.codigo=tabpedidoproj_ordemproducao.pedidoprojeto '+
     'join tabservico on tabpedidoproj_ordemproducao.servico=tabservico.codigo '+
     'join tabprojeto on tabprojeto.codigo=tabpedido_proj.projeto '+
     'join tabpedido on tabpedido.codigo=tabpedido_proj.pedido '+
     'where tabpedidoproj_ordemproducao.pedidoprojeto='+PedidoProjeto+' '+
     'order by tabpedidoproj_ordemproducao.codigo';

     STRGLinhaServicos.ColCount:=1;
     STRGLinhaServicos.RowCount:=6;
     STRGLinhaServicos.ColWidths[0] := 128;
     STRGLinhaServicos.RowHeights[1]:=40;
     STRGLinhaServicos.RowHeights[2]:=0;
     STRGLinhaServicos.RowHeights[3]:=0;
     STRGLinhaServicos.RowHeights[4]:=0;
     //STRGLinhaServicos.RowHeights[5]:=50;
     STRGLinhaServicos.Cells[0,0] := '';
     STRGLinhaServicos.Cells[0,1] := '';
     STRGLinhaServicos.Cells[0,2] := '';

     QryMostraServicos_QUERY.Open;

     lbNomeProjeto.Caption:=QryMostraServicos_QUERY.Fields[6].AsString;
     lbCodigoPedido.Caption:='Pedido :'+QryMostraServicos_QUERY.Fields[5].AsString;
     lbPedidoProjeto.Caption:='Pedido/Projeto :'+QryMostraServicos_QUERY.Fields[4].AsString;

     //Mostrando a cor do vidro
    __MostraCorVidro(QryMostraServicos_QUERY.Fields[4].AsString);

     while not QryMostraServicos_QUERY.Eof do
     begin
        STRGLinhaServicos.Cells[QryMostraServicos_QUERY.RecordCount-1,0]:= QryMostraServicos_QUERY.Fields[2].AsString;
        STRGLinhaServicos.Cells[QryMostraServicos_QUERY.RecordCount-1,1] := '';
        STRGLinhaServicos.Cells[QryMostraServicos_QUERY.RecordCount-1,2] := QryMostraServicos_QUERY.Fields[0].AsString;
        STRGLinhaServicos.Cells[QryMostraServicos_QUERY.RecordCount-1,3] := QryMostraServicos_QUERY.Fields[7].AsString;
        STRGLinhaServicos.Cells[QryMostraServicos_QUERY.RecordCount-1,4] := QryMostraServicos_QUERY.Fields[8].AsString;
        //STRGLinhaServicos.Cells[QryMostraServicos_QUERY.RecordCount-1,5] := QryMostraServicos_QUERY.Fields[9].AsString;
        if(QryMostraServicos_QUERY.Fields[0].AsString='C')then
        begin
          imgFalse.Visible:=False;
          imgSucesso.Visible:=True;
          lbNomedoservicoprod.Caption:='EXECUTADO AT� : '+ get_campoTabela('descricao','referencia','tabservico',STRGLinhaServicos.Cells[QryMostraServicos_QUERY.RecordCount-1,0]);
          Executando:=True;
        end;

        QryMostraServicos_QUERY.Next;

        STRGLinhaServicos.ColCount:=STRGLinhaServicos.ColCount+1;
        STRGLinhaServicos.ColWidths[STRGLinhaServicos.ColCount-1] := 128;
     end;
     STRGLinhaServicos.ColCount:=STRGLinhaServicos.ColCount-1;

     if(QryMostraServicos_QUERY.RecordCount>0) then
     begin
         Result:=True;
         __CarregaDesenhoProjetoProducao(True,True,QryMostraServicos_QUERY.Fields[3].AsString);

         //Por alguma merda de motivo tem q chamar duas vezes essa porra de fun��o sen�o preenche as medidas automaticamente
         //Verificar essa buceta :@
         __Preenchemedidas(QryMostraServicos_QUERY.Fields[4].AsString,QryMostraServicos_QUERY.Fields[3].AsString);
         __Preenchemedidas(QryMostraServicos_QUERY.Fields[4].AsString,QryMostraServicos_QUERY.Fields[3].AsString);

     end;

     {if(Executando=True)
     then MarcaExecucaoServico;

     if(STRGLinhaServicos.Cells[0,2]='A')
     then __MarcaAguardandoServico;

     if(STRGLinhaServicos.Cells[STRGLinhaServicos.ColCount-1,2]='C')
     then DesmarcaExecucaoServico;   }

     __PosicionaServico;
   finally
     FreeAndNil(QryMostraServicos_QUERY);
   end;
end;

procedure TFProducao.__Preenchemedidas(CodigoPedidoProjeto,CodigoProjeto:string);
var
  i:Integer;
  NomeRestante2,nome2,CodigoMaterial,tipo:string;
  query:TIBQuery;
  alturalargura,texto,operador,texto2:string;
  Resultado:Currency;
  lf : TLogFont;
  tf : TFont;
  folgaLargura,FolgaAltura:Currency;
begin
    PaintBox1.Font.Color:=clBlack;
    PaintBox1.Font.Size:=5;

    query:=TIBQuery.Create(nil);
    query.Database:=FDataModulo.IBDatabase;
    try
        for i:=0 to ListBox2.Count-1 do
        begin
           NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox2.Items[i],' ');
           nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
           if(TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).TipoFigura=21)then
           begin
              if((TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).pt2.x-TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).pt1.x)>1)then
              begin
                 NomeRestante2:=retornaPalavrasDepoisSimbolo(ListBox2.Items[i],'-');
                 tipo:=retornaPalavrasAntesSimbolo(NomeRestante2,'-');
                 NomeRestante2:=retornaPalavrasDepoisSimbolo(NomeRestante2,'-');
                 NomeRestante2:=retornaPalavrasDepoisSimbolo(NomeRestante2,'-');
                 CodigoMaterial:= retornaPalavrasAntesSimbolo(NomeRestante2,'-');

                 if(tipo='LinhaMedida')then
                 begin
                     folgaLargura:=0;
                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select cp.folgalargura from tabcomponente_proj cp where projeto='+CodigoProjeto);
                     query.SQL.Add('and componente='+CodigoMaterial);
                     query.Open;

                     folgaLargura:=query.fieldbyname('folgalargura').AsCurrency;

                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select largura from tabcomponente_pp where componente='+CodigoMaterial);
                     query.SQL.Add('and pedidoprojeto='+CodigoPedidoProjeto);
                     query.Open;

                     PaintBox1.Canvas.Font.Size:=5;
                     GetObject(PaintBox1.Canvas.Font.Handle, SizeOf(TLogFont), @lf);
                     lf.lfHeight := PaintBox1.Canvas.Font.Height;
                     lf.lfEscapement:=0*10;
                     lf.lfQuality := PROOF_QUALITY;
                     PaintBox1.Canvas.Font.Handle := CreateFontIndirect(lf);

                     PaintBox1.Canvas.TextOut(TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).pt1.x,TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).pt2.y+10,CurrToStr(query.fieldbyname('largura').AsCurrency-folgaLargura)+' mm.');
                 end;

                 if(tipo='LinhaMedidaPuxador')then
                 begin
                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select largura from tabcomponente_pp where componente='+CodigoMaterial);
                     query.SQL.Add('and pedidoprojeto='+CodigoPedidoProjeto);
                     query.Open;
                     alturalargura:=query.fieldbyname('largura').asstring;

                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select tabcortecomponente_proj.* from tabcortecomponente_proj');
                     query.SQL.Add('join tabcomponente_proj on tabcomponente_proj.codigo=tabcortecomponente_proj.componente_proj');
                     query.SQL.Add('where tabcomponente_proj.componente='+CodigoMaterial);
                     query.SQL.Add('and tabcomponente_proj.projeto='+CodigoProjeto);
                     query.SQL.Add('and tabcortecomponente_proj.puxador=''S'' ');
                     query.Open;

                     PaintBox1.Canvas.Font.Size:=5;
                     GetObject(PaintBox1.Canvas.Font.Handle, SizeOf(TLogFont), @lf);
                     lf.lfHeight := PaintBox1.Canvas.Font.Height;
                     lf.lfEscapement:=0*10;
                     lf.lfQuality := PROOF_QUALITY;
                     PaintBox1.Canvas.Font.Handle := CreateFontIndirect(lf);

                     texto:= StrReplace(query.fieldbyname('formulaposicaocorte').AsString,'LARGURA',alturalargura);

                     PaintBox1.Canvas.TextOut(TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).pt1.x,TDesenho( listaFigurasParaProducao[StrToInt(nome2)]).pt2.y+10,texto+'mm');
                 end;

              end;

              if((TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).pt2.y-TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).pt1.y)>1)then
              begin
                 NomeRestante2:=retornaPalavrasDepoisSimbolo(ListBox2.Items[i],'-');
                 tipo:=retornaPalavrasAntesSimbolo(NomeRestante2,'-');
                 NomeRestante2:=retornaPalavrasDepoisSimbolo(NomeRestante2,'-');
                 NomeRestante2:=retornaPalavrasDepoisSimbolo(NomeRestante2,'-');
                 CodigoMaterial:= retornaPalavrasAntesSimbolo(NomeRestante2,'-');
                 if(tipo='LinhaMedida')then
                 begin
                     FolgaAltura:=0;
                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select cp.folgaaltura from tabcomponente_proj cp where projeto='+CodigoProjeto);
                     query.SQL.Add('and componente='+CodigoMaterial);
                     query.Open;

                     FolgaAltura:=query.fieldbyname('folgaaltura').AsCurrency;


                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select altura from tabcomponente_pp where componente='+CodigoMaterial);
                     query.SQL.Add('and pedidoprojeto='+CodigoPedidoProjeto);
                     query.Open;

                     PaintBox1.Canvas.Font.Size:=5;
                     GetObject(PaintBox1.Canvas.Font.Handle, SizeOf(TLogFont), @lf);
                     lf.lfHeight := PaintBox1.Canvas.Font.Height;
                     lf.lfEscapement:=90*10;
                     lf.lfQuality := PROOF_QUALITY;
                     PaintBox1.Canvas.Font.Handle := CreateFontIndirect(lf);

                     PaintBox1.Canvas.TextOut(TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).pt1.x,TDesenho( listaFigurasParaProducao[StrToInt(nome2)]).pt2.y-50,CurrToStr(query.fieldbyname('altura').AsCurrency-FolgaAltura)+' mm.');
                 end;
                 if(tipo='LinhaMedidaPuxador')then
                 begin
                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select altura from tabcomponente_pp where componente='+CodigoMaterial);
                     query.SQL.Add('and pedidoprojeto='+CodigoPedidoProjeto);
                     query.Open;
                     alturalargura:=query.fieldbyname('altura').asstring;

                     query.Close;
                     query.SQL.Clear;
                     query.SQL.Add('select tabcortecomponente_proj.* from tabcortecomponente_proj');
                     query.SQL.Add('join tabcomponente_proj on tabcomponente_proj.codigo=tabcortecomponente_proj.componente_proj');
                     query.SQL.Add('where tabcomponente_proj.componente='+CodigoMaterial);
                     query.SQL.Add('and tabcomponente_proj.projeto='+CodigoProjeto);
                     query.SQL.Add('and tabcortecomponente_proj.puxador=''S'' ');
                     query.Open;


                     texto:= StrReplace(query.fieldbyname('formulaalturacorte').AsString,'ALTURA',alturalargura);

                     operador:=RetornaOperadorMatematico(query.fieldbyname('formulaalturacorte').AsString);
                     texto2:=retornaPalavrasAntesSimbolo(texto,operador);
                     texto:=retornaPalavrasDepoisSimbolo2(texto,operador);

                     if(operador='/')
                     then Resultado:=StrToCurr(texto2)/StrToCurr(texto);

                     PaintBox1.Canvas.Font.Size:=5;
                     GetObject(PaintBox1.Canvas.Font.Handle, SizeOf(TLogFont), @lf);
                     lf.lfHeight := PaintBox1.Canvas.Font.Height;
                     lf.lfEscapement:=90*10;
                     lf.lfQuality := PROOF_QUALITY;
                     PaintBox1.Canvas.Font.Handle := CreateFontIndirect(lf);
                     

                     PaintBox1.Canvas.TextOut(TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).pt1.x,TDesenho( listaFigurasParaProducao[StrToInt(nome2)]).pt2.y-40,CurrToStr(Resultado)+' mm.');

                 end;
              end;
           end;
        end;
    finally
        FreeAndNil(query);
    end;


   { PaintBoxTela.Font.Color:=clBlack;
    PaintBoxTela.Font.Size:=6;
    for i:=0 to ListaComponentes.Count-1 do
    begin

    end;    }


end;

function TFProducao.RetornaOperadorMatematico(str:string):string;
var
  i,j:Integer;
  aux:string ;
begin
      Result:='';
      aux:='';
      i:=0;
      if(Str='')
      then Exit;
      while (i <= Length (str)) do
      begin
          if(Str[i]='/') or (Str[i]='+') or (Str[i]='-') or (Str[i]='*')
          then aux:=Str[i];

          Inc(i,1);
      end;
      Result:=aux;

end;


procedure TFProducao.STRGLinhaServicosDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
    if((ARow = 1))
    then begin
        if(STRGLinhaServicos.Cells[ACol,0] <> '' ) then
        begin
             if(STRGLinhaServicos.Cells[ACol,2] = 'A' )
             then Il1.Draw(STRGLinhaServicos.Canvas,Rect.Left ,Rect.Top,ACol)
             else
             begin
                  if(STRGLinhaServicos.Cells[ACol,2] = 'P' )
                  then Il1.Draw(STRGLinhaServicos.Canvas,Rect.Left ,Rect.Top,ACol+16)
                  else Il1.Draw(STRGLinhaServicos.Canvas,Rect.Left ,Rect.Top,ACol+8)
             end;
        end
    end;
end;

//Split Quebra a string conforme um delimitador que � passado
procedure Split (const Delimiter: Char; Input: string; const Strings: TStrings);
begin
   Assert(Assigned(Strings));
   Strings.Clear;
   Strings.Delimiter := Delimiter;
   Strings.DelimitedText := Input;
end;

//Procedimento que limpa todas as figuras existentes no paintbox
//Chama o procedimento de retirada de figura do listbox
procedure TFProducao.__LimpaTudoIMGProducao();
var i : integer;
    str : string;
    parte : TStringList;
begin
  try
    // exclui todos os objetos da tela
    for i := contProducao-1 downto 0 do
    begin
      parte := TStringList.Create;
      str := ListBox2.Items[i];

      try
       Split(' ', str, parte);
       TDesenho (listaFigurasParaProducao[strtoint(parte[1])]).Free;
      finally
       parte.Free;
      end;
    end;
    listaFigurasParaProducao.Clear;
    //Limpa o listbox onde mostra cada figura
    for i:= ListBox2.Count - 1 downto 0 do
       ListBox2.Items.Delete(i);
    ListBox2.Clear;
    contProducao := 0;
    PaintBox1.Repaint;
    Refresh;
  finally

  end;
end;


//Carregar os desenhos do projeto
procedure TFProducao.__CarregaDesenhoProjetoProducao(ComReferencias:Boolean;MostraLinhas:Boolean;Projeto:string);
var query: TIBQuery;  //declara��o da query
    enviar : string;
    busc : string;
    str : string;
    retorno : TstringList;
    k : integer;
    Indice:Integer;
begin
  if(Projeto='')
  then Exit;

  __LimpaTudoIMGProducao();  //limpa todos os objetos e toda a listbox
  query := TIBQuery.Create(nil);
  query.Database :=FDataModulo.IBDatabase;
  try
    ListaComponentes.Clear;
    listaFigurasParaProducao.Clear;

    //busca o id da figura pelo nome clicado no StringGrid
    with query do
    begin
       close;
       sql.Clear;
       sql.Add('select id from tabprojetos_id where projeto ='+Projeto);

       open;
       Last;
       if(recordcount=0)then
       begin
          idCarr:=False;
          Exit;
       end;
       busc    := fieldbyname('id').AsString;
       idCarr  := True;  //Indica que uma figura ja foi carregada
       idFig   := strtoint(busc);    //Indica o numero da figura que foi carregada
       close;
    end;

    //Busca todos os objetos da figura
    with query do
    begin
      close;
      sql.Clear;
      sql.Add('select * from tabprojetospontos_id where projeto_id = '#39+busc+#39 );
      //SQL.Add('and tipomaterial<>''TABFERRAGEM''');
     // SQL.Add('and nome<>''LinhaMedida''');
      open;
      First;
      contProducao := 0; //Digo que na tela nao tem nenhum figura
      while not (query.eof) do
      begin
          if(MostraLinhas=True) then
          begin
            op := FieldByName('tipo').AsInteger;  //tipo do objeto
            cor := StringToColor (FieldByName('cor').AsString);
            pInic.X := FieldByName('xsup').AsInteger;  //xsuperior  do objeto
            pInic.Y := FieldByName('ysup').AsInteger;  //ysuperior do objeto

            if(op <> 3)then
            begin
              pFim.X := FieldByName('xinf').AsInteger;   //xinferior do objeto
              pFim.Y := FieldByName('yinf').AsInteger;   //yinferior do objeto
            end
            else
            begin
              retorno := TStringList.Create;
              str := FieldByName('pts').AsString;
              Split(';', str, retorno);
              k := 0;
              while (k < (retorno.count-1)/2) do
              begin
                points[k].X := strtoint(retorno[k*2]);
                points[k].Y := strtoint(retorno[(k*2)+1]);
                k := k+1;
              end;
              indCurr := round((retorno.count-1)/2);
              pux_banc := true;
            end;
            __CriarFiguraIMGProducao(fieldbyname('nome').asstring,fieldbyname('tipomaterial').asstring,fieldbyname('codigomaterial').asstring);
            pux_banc := false;
            cor := $00A2663E;
          end
          else
          begin
              if(FieldByName('tipo').AsInteger<>21)then
              begin
                   op := FieldByName('tipo').AsInteger;  //tipo do objeto
                   cor := StringToColor (FieldByName('cor').AsString);
                   pInic.X := FieldByName('xsup').AsInteger;  //xsuperior  do objeto
                   pInic.Y := FieldByName('ysup').AsInteger;  //ysuperior do objeto

                   if(op <> 3)then
                   begin
                     pFim.X := FieldByName('xinf').AsInteger;   //xinferior do objeto
                     pFim.Y := FieldByName('yinf').AsInteger;   //yinferior do objeto
                   end
                   else
                   begin
                        retorno := TStringList.Create;
                        str := FieldByName('pts').AsString;
                        Split(';', str, retorno);
                        k := 0;
                        while (k < (retorno.count-1)/2) do
                        begin
                            points[k].X := strtoint(retorno[k*2]);
                            points[k].Y := strtoint(retorno[(k*2)+1]);
                            k := k+1;
                        end;
                        indCurr := round((retorno.count-1)/2);
                        pux_banc := true;
                   end;
                   __CriarFiguraIMGProducao(fieldbyname('nome').asstring,fieldbyname('tipomaterial').asstring,fieldbyname('codigomaterial').asstring);
                   pux_banc := false;
                   cor := $00A2663E;
              end;
          end;
          if(ComReferencias=true)then
          begin
                 //Se � um componente preciso apenas passar sua referencia pra listanomesmedidas do obj
                 //preciso inseri-lo na lista de componentes (listacomponentes)
                 if(FieldByName('tipomaterial').AsString='TABCOMPONENTE') then
                 begin
                    ListaComponentes.Add(IntToStr(objDesenha.ContFig)+';'+fieldbyname('nome').asstring);
                    objDesenha.ListaNomesMedidas.Add(FieldByName('referencia').AsString)
                 end;
                 //Se for uma ferragem preciso passar para a lista listanomemedidas do componente a referencia da ferragem
                 //posi��o (x,y) da mesma dentro do componente
                 //Preciso inseri-la na lista de itens (listaitens)
                 if(FieldByName('tipomaterial').AsString='TABFERRAGEM') then
                 begin
                     //Localizo em qual componente essa ferragem foi inserida
                     if(fieldbyname('ligadoaocomponente').asstring<>'')then
                     begin
                         indice:=LocalizaTagCompAoCarregar(fieldbyname('ligadoaocomponente').asstring);
                         //Caso o indice seja -1 ent�o a mesma n�o esta ligada em nenhum componente
                         if(Indice<>-1) then
                         begin
                            //Somente se o valor em referencia, q para ferragens � (text;x;y) estiver com algo
                            if(FieldByName('referencia').AsString<>'') then
                            begin
                              TDesenho(listaFigurasParaProducao[Indice]).ListaNomesMedidas.Add(FieldByName('referencia').AsString+';'+inttostr(objDesenha.ContFig));
                             // ListaItens.Add(IntToStr(objDesenha.ContFig)+';'+IntToStr(Indice));
                            end;
                         end;
                     end;
                 end;
          end;
          query.Next;
      end;
      close; 
    end;
    DesabilitaHabilita(False);
  finally
    FreeAndNil(query);
  end;
end;

function TFProducao.LocalizaTagCompAoCarregar(parametro:string):Integer;
var
  Query:TIBQuery;
  i:Integer;
  Nome,NomeRestante:string;
  Nome2,NomeRestante2:string;
begin
    //caso n�o encontre retorna -1
    Result:=-1;

    query:=TIBQuery.Create(nil);
    Query.Database:=FDataModulo.IBDatabase;

    try
      with Query do
      begin
          Close;
          SQL.Clear;
          SQL.Add('select nome from tabprojetospontos_id where id='+parametro);
          Open;
          //percorro a lista de obj procurando pelo nome do componente
          for i:=0 to ListBox2.Count-1 do
          begin
              //Pego o nome do componente
              NomeRestante:=retornaPalavrasDepoisSimbolo(ListBox2.Items[i],'-');
              nome:=retornaPalavrasAntesSimbolo(NomeRestante,'-');
              //Procuro pelo componente na lista de componentes
              if(Nome=FieldByName('nome').AsString) then
              begin
                   //Pego a tag do obj, por exemplo (obj 1), tag=1
                   NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox2.Items[i],' ');
                   nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');

                   Result:=StrToInt(Nome2);
              end;
          end;
      end;
    finally
      FreeAndNil(Query);
    end;

end;


//Faz a inversao do pontos inicial e final da figura desenhada
procedure TFProducao.__Arruma ();
var
  tmp: Integer;
begin
  //Verifico se o ponto y final � menor que ponto y inicial
  if (PFim.Y < PInic.Y) then
  begin
        tmp := PFim.Y;
        PFim.Y := PInic.Y;
        PInic.Y := tmp;
  end;
  //Verifico se o ponto x final � menor que ponto x inicial
  if (PFim.X < PInic.X) then
  begin
        tmp := PFim.X;
        PFim.X := PInic.X;
        PInic.X := tmp;
  end;
end;

//Define a largura e altura que o poligono deve ter
procedure TFProducao.__DefineTamanhoPoligon();
var
    i: integer;
    maiorx : integer;
    maiory : integer;
begin
    menorx := points[0].x;
    menory := points[0].y;
    maiorx := points[0].x;
    maiory := points[0].y;
    for i := 1 to indCurr-1 do
    begin
      if(points[i].x < menorx)then
          menorx := points[i].x;
      if(points[i].y < menory)then
          menory := points[i].y;
      if(points[i].x > maiorx)then
          maiorx := points[i].x;
      if(points[i].y > maiory)then
          maiory := points[i].y;
    end;
    //a largura do poligono sera definida pela diferen�a entre o maior e o menor ponto x
    largura := maiorx-menorx+6;
    //a altura do poligono sera definida pela diferen�a entre o maior e o menor ponto y
    altura := maiory-menory+6;
end;

//Procedimento que passa os dados de cria��o para unit de cria��o de figuras
procedure TFProducao.__CriarFiguraIMGProducao(NomeFigura:string ; TipoMaterial:string; CodigoMaterial:string);
begin
  if(op <> 3) then
  begin
        __Arruma();
        //To mandando vazio o nome do componente, pq na hora de salvar novamente zua tudo
        objDesenha := TDesenho.Create(PaintBox1,cor, op, listaFigurasParaProducao.Count,pinic,pfim, points, indCurr-1,NomeFigura);

  end
  else
  objDesenha := TDesenho.Create(PaintBox1,cor, op, listaFigurasParaProducao.Count,pinic,pfim, points, indCurr-1,'');

  objDesenha.PassaForm(Self);
  objDesenha.Parent := PanelFundoProducao;

  if (op < 3) then
    objDesenha.SetBounds (pInic.X, pInic.Y, pfim.x-pinic.x, pfim.y-pinic.y)
  else
  if(op = 3) then
  begin
       __DefineTamanhoPoligon();
       if(pux_banc = false)then
       objDesenha.SetBounds (menorX, menorY, largura, altura)
       else   objDesenha.SetBounds (pinic.X, pinic.Y, largura, altura) ;
  end
  else
  begin
      if (op=4) or (op>=22) then
          objDesenha.SetBounds (pInic.X, pInic.Y, pfim.x-pinic.x, pfim.y-pinic.y)
      else
      if(op=21)
      then objDesenha.SetBounds (pInic.X, pInic.Y, pfim.x-pinic.x, pfim.y-pinic.y)
      else
      begin
          begin
             objDesenha.SetBounds (pInic.X, pInic.Y, largura*2, altura*2);
          end;
      end;
  end;

  objDesenha.Enabled:=False;

  listaFigurasParaProducao.Add(objDesenha);
  contProducao := contProducao + 1;
  __AdicionaListaIMGProducao(NomeFigura,TipoMaterial,CodigoMaterial);

end;

procedure TFProducao.__AdicionaListaIMGProducao(NomeFigura:string; TipoMaterial:string ; CodigoMaterial:string);
begin
    if(NomeFigura<>'')
    then ListBox2.Items.Add('Obj '+inttostr(objDesenha.ContFig)+' - '+NomeFigura+' - '+TipoMaterial+' - '+CodigoMaterial)
    else ListBox2.Items.Add('Obj '+inttostr(objDesenha.ContFig)+' -');
end;


procedure TFProducao.STRGLinhaServicosDblClick(Sender: TObject);
begin
   { if(STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]='A')then
    begin
       if(STRGLinhaServicos.col>0) then
       begin
          if(STRGLinhaServicos.Cells[STRGLinhaServicos.col-1,2]<>'A')
          then STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]:='P'
       end
       else STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]:='P'
    end
    else
    begin
      if(STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]='P')
      then  STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]:='C';
    end;
    STRGLinhaServicos.Refresh;   }
end;

procedure TFProducao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    __LimpaTudoIMGProducao;
    __LimpaTudo;
    _ParametroPesquisaCliente:='';
    _ParametroPesquisaPedido:='';
    listaFigurasParaProducao.Free;
    ListaComponentes.Free;
   
end;

procedure TFProducao.btConfiguracoesClick(Sender: TObject);
begin
  PopUpConfiguracoes.Popup(Mouse.CursorPos.X,Mouse.CursorPos.Y);
end;

procedure TFProducao.edtPedidoProjetoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   SQL:string;
begin
     If (key <>vk_f9)
     Then exit;
     SQL:=
     'SELECT distinct(pp.codigo) as pedidoprojeto, P.CODIGO as pedido, proj.descricao,C.NOME, proj.codigo '+
     'FROM TABPEDIDOPROJ_ORDEMPRODUCAO PPO '+
     'JOIN TABPEDIDO_PROJ PP ON PP.CODIGO=PPO.PEDIDOPROJETO '+
     'JOIN TABPEDIDO P ON P.CODIGO=PP.PEDIDO '+
     'JOIN TABCLIENTE C ON C.CODIGO=P.CLIENTE '+
     'JOIN TABPROJETO Proj ON Proj.CODIGO=PP.PROJETO ';

     Try
       Fpesquisalocal:=Tfpesquisa.create(Nil);
       If (FpesquisaLocal.PreparaPesquisa(sql,'',nil)=True)
       Then Begin
           Try
              If (FpesquisaLocal.showmodal=mrok)
              Then Begin

                 {TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CodigoPlanoContas.RETORNACAMPOCODIGO).asstring;
                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                 Then Begin
                    If Self.CodigoPlanoContas.RETORNACAMPONOME<>''
                    Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CodigoPlanoContas.RETORNACAMPONOME).asstring
                    Else LABELNOME.caption:='';
                 End;  }
              End;
           Finally
              FpesquisaLocal.QueryPesq.close;
           End;
       End;

     Finally
       FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFProducao.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   SQL:string;
begin
     PopUpPesquisar.Popup(Mouse.CursorPos.X,Mouse.CursorPos.Y);
     {SQL:=
     'SELECT distinct(pp.codigo) as pedidoprojeto, P.CODIGO as pedido, proj.descricao,C.NOME '+
     'FROM TABPEDIDOPROJ_ORDEMPRODUCAO PPO '+
     'JOIN TABPEDIDO_PROJ PP ON PP.CODIGO=PPO.PEDIDOPROJETO '+
     'JOIN TABPEDIDO P ON P.CODIGO=PP.PEDIDO '+
     'JOIN TABCLIENTE C ON C.CODIGO=P.CLIENTE '+
     'JOIN TABPROJETO Proj ON Proj.CODIGO=PP.PROJETO ';

     Try
       Fpesquisalocal:=Tfpesquisa.create(Nil);
       If (FpesquisaLocal.PreparaPesquisa(sql,'',nil)=True) Then
       Begin
          Try
            If (FpesquisaLocal.showmodal=mrok) Then
            Begin
              if(FpesquisaLocal.QueryPesq.fields[0].asstring<>'') then
              begin
                __LimpaTudo;
                _ParametroPesquisaPedido:='';
                _ParametroPesquisaCliente:='';
                ___MostraServicos(FpesquisaLocal.QueryPesq.fields[0].asstring);
                STRGLinhaServicos.row:=2;
              end;
            End;
          Finally
            FpesquisaLocal.QueryPesq.close;
          End;
       End;
     Finally
       FreeandNil(FPesquisaLocal);
     End;  }
end;

procedure TFProducao.MenuItem1Click(Sender: TObject);
begin
//   btPesquisarClick(Sender);
   _ParametroPesquisaPedido:='';
   _ParametroPesquisaCliente:='';
   lbPedidoProjeto.Caption:='';
   btProximoClick(Sender);
  
end;

procedure TFProducao.__LimpaTudo;
var
  i:Integer;
begin
    for i:=0 to STRGLinhaServicos.ColCount-1 do
    begin
         STRGLinhaServicos.Cells[i,0]:='';
         STRGLinhaServicos.Cells[i,1]:='';
         STRGLinhaServicos.Cells[i,2]:='';
    end;
    lbNomedoservicoprod.Caption:='';
    lbNomeProjeto.Caption:='';
    lbPedidoProjeto.Caption:='';
    lbCodigoPedido.Caption:='';
    lbCliente.Caption:='';
    lbCordovidro.Caption:='';
    imgSucesso.Visible:=False;
    imgFalse.Visible:=False;

end;

procedure TFProducao.btExecutarClick(Sender: TObject);
var
   _qryUpdateLocal_QUERY:TIBQuery;
begin
  try
     _qryUpdateLocal_QUERY:=TIBQuery.Create(nil);
     _qryUpdateLocal_QUERY.Database:=FDataModulo.IBDatabase;
  except
     FDataModulo.IBTransaction.RollbackRetaining;
     Exit;
  end;
  try
    if(STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]='A')then
    begin
       STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]:='P';
       lbNomedoservicoprod.Caption:='EXECUTANDO : '+ get_campoTabela('descricao','referencia','tabservico',STRGLinhaServicos.Cells[STRGLinhaServicos.Col,0]);
       //Gravando no Banco de Dados
       _qryUpdateLocal_QUERY.Close;
       _qryUpdateLocal_QUERY.SQL.Clear;
       _qryUpdateLocal_QUERY.SQL.Text:=
       'update tabpedidoproj_ordemproducao set estadoproducao=''P'' where codigo='+STRGLinhaServicos.Cells[STRGLinhaServicos.col,3];
       try
         _qryUpdateLocal_QUERY.ExecSQL;
       except
         FDataModulo.IBTransaction.RollbackRetaining;
         Exit;
       end;

       MarcaExecucaoServico;
    end
    else
    begin
       if(STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]='P')then
       begin
         STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]:='C';
         lbNomedoservicoprod.Caption:='';
         //Gravando no Banco de Dados
         _qryUpdateLocal_QUERY.Close;
         _qryUpdateLocal_QUERY.SQL.Clear;
         _qryUpdateLocal_QUERY.SQL.Text:=
         'update tabpedidoproj_ordemproducao set estadoproducao=''C'' where codigo='+STRGLinhaServicos.Cells[STRGLinhaServicos.col,3];
         try
           _qryUpdateLocal_QUERY.ExecSQL;
         except
           FDataModulo.IBTransaction.RollbackRetaining;
           Exit;
         end;
         DesmarcaExecucaoServico;

         if(STRGLinhaServicos.Col<STRGLinhaServicos.ColCount-1) then
         begin
            STRGLinhaServicos.Col:=STRGLinhaServicos.col+1;
            STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]:='P';
            lbNomedoservicoprod.Caption:='EXECUTANDO : '+ get_campoTabela('descricao','referencia','tabservico',STRGLinhaServicos.Cells[STRGLinhaServicos.Col,0]);
            //Gravando no Banco de Dados
            _qryUpdateLocal_QUERY.Close;
            _qryUpdateLocal_QUERY.SQL.Clear;
            _qryUpdateLocal_QUERY.SQL.Text:=
            'update tabpedidoproj_ordemproducao set estadoproducao=''P'' where codigo='+STRGLinhaServicos.Cells[STRGLinhaServicos.col,3];
            try
              _qryUpdateLocal_QUERY.ExecSQL;
            except
              FDataModulo.IBTransaction.RollbackRetaining;
              Exit;
            end;
            MarcaExecucaoServico;
         end;

       end;
    end;
    STRGLinhaServicos.Refresh;
    FDataModulo.IBTransaction.CommitRetaining;
  finally
    FreeAndNil(_qryUpdateLocal_QUERY);
    FDataModulo.IBTransaction.RollbackRetaining;
  end;
end;

function TFProducao.__MarcaExecucaoServico_Componente(ReferenciaComponente,ESTADOPRODUCAO:string):Boolean;
var
  i:Integer;
  NomeRestante2,nome2:string;
begin
  Result:=False;
  for i:=0 to ListBox2.Count-1 do
  begin
    NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox2.Items[i],' ');
    nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
    if(TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).ListaNomesMedidas.Count>0) then
    begin
      if (pos(TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).ListaNomesMedidas[0],ReferenciaComponente)>0) then
      begin
        if(ESTADOPRODUCAO='C')then
        begin
          TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Color:=$005CADFE;
          TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).tmpColor:=$005CADFE;
          TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Repaint;
        end
        else
        begin
          if(ESTADOPRODUCAO='A')then
          begin
            TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Color:=clWhite;
            TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).tmpColor:=clWhite;
            TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Repaint;
          end;
        end;
        Result:=True;
      end;
    end;
  end;
end;

procedure TFProducao.MarcaExecucaoServico();
var
  i:Integer;
  NomeRestante2,nome2:string;
begin
  for i:=0 to ListBox2.Count-1 do
  begin
    NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox2.Items[i],' ');
    nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
    if(TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).ListaNomesMedidas.Count>0) then
    begin
      if (pos(TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).ListaNomesMedidas[0]+';',STRGLinhaServicos.Cells[STRGLinhaServicos.Col,4])>0) then
      begin
        TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Color:=$005CADFE;
        TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).tmpColor:=$005CADFE;
        TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Repaint;
      end;
    end;
  end;
end;

procedure TFProducao.DesmarcaExecucaoServico;
var
  i:Integer;
  NomeRestante2,nome2:string;
begin
  for i:=0 to ListBox2.Count-1 do
  begin
    NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox2.Items[i],' ');
    nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
    TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Color:=clWhite;
    TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).tmpColor:=clWhite;
    TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Repaint;
  end;
end;

procedure TFProducao.DesabilitaHabilita(acao:Boolean);
var
  i:Integer;
  NomeRestante2,nome2:string;
begin
  //HABILITA
  if(acao=True) then
  begin
    for i:=0 to ListBox2.Count-1 do
    begin
      NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox2.Items[i],' ');
      nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
      TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Enabled:=true;
      TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Botao := true;
    end;
    //PaintBox1.Enabled:=True;
    ListBox2.Enabled:=True;
  end
  else
  begin  //DESABILITA
    for i:=0 to ListBox2.Count-1 do
    begin
      NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox2.Items[i],' ');
      nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
      TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Enabled:=False;
      TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Botao := False ;
    end;
    //PaintBox1.Enabled:=False;
    ListBox2.Enabled:=False;
  end;
end;

procedure TFProducao.btCancelarClick(Sender: TObject);
var
   _qryUpdateLocal_QUERY:TIBQuery;
begin
  try
     _qryUpdateLocal_QUERY:=TIBQuery.Create(nil);
     _qryUpdateLocal_QUERY.Database:=FDataModulo.IBDatabase;
  except
     Exit;
  end;
  try
    if(STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]='P')then
    begin
       STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]:='A';
       lbNomedoservicoprod.Caption:='';
       //Gravando no Banco de Dados
       _qryUpdateLocal_QUERY.Close;
       _qryUpdateLocal_QUERY.SQL.Clear;
       _qryUpdateLocal_QUERY.SQL.Text:=
       'update tabpedidoproj_ordemproducao set estadoproducao=''A'' where codigo='+STRGLinhaServicos.Cells[STRGLinhaServicos.col,3];
       try
         _qryUpdateLocal_QUERY.ExecSQL;
       except
         FDataModulo.IBTransaction.RollbackRetaining;
         Exit;
       end;


       if(STRGLinhaServicos.Col>0) then
       begin
         STRGLinhaServicos.Col:=STRGLinhaServicos.col-1;
         STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]:='P';
         lbNomedoservicoprod.Caption:='EXECUTANDO : '+ get_campoTabela('descricao','referencia','tabservico',STRGLinhaServicos.Cells[STRGLinhaServicos.Col,0]);
         //Gravando no Banco de Dados
         _qryUpdateLocal_QUERY.Close;
         _qryUpdateLocal_QUERY.SQL.Clear;
         _qryUpdateLocal_QUERY.SQL.Text:=
         'update tabpedidoproj_ordemproducao set estadoproducao=''P'' where codigo='+STRGLinhaServicos.Cells[STRGLinhaServicos.col,3];
         try
           _qryUpdateLocal_QUERY.ExecSQL;
         except
           FDataModulo.IBTransaction.RollbackRetaining;
           Exit;
         end;

         MarcaExecucaoServico;
       end
       else __MarcaAguardandoServico;
    end
    else
    begin
       if(STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]='C')then
       begin
         STRGLinhaServicos.Cells[STRGLinhaServicos.col,2]:='P';
         lbNomedoservicoprod.Caption:='EXECUTANDO : '+ get_campoTabela('descricao','referencia','tabservico',STRGLinhaServicos.Cells[STRGLinhaServicos.Col,0]);
         //Gravando no Banco de Dados
         _qryUpdateLocal_QUERY.Close;
         _qryUpdateLocal_QUERY.SQL.Clear;
         _qryUpdateLocal_QUERY.SQL.Text:=
         'update tabpedidoproj_ordemproducao set estadoproducao=''P'' where codigo='+STRGLinhaServicos.Cells[STRGLinhaServicos.col,3];
         try
           _qryUpdateLocal_QUERY.ExecSQL;
         except
           FDataModulo.IBTransaction.RollbackRetaining;
           Exit;
         end;

         MarcaExecucaoServico;

       end;
    end;

    //DESMARCAR NOS COMPONENTES TAMBEM

    STRGLinhaServicos.Refresh;
    FDataModulo.IBTransaction.CommitRetaining;
  finally
    FreeAndNil(_qryUpdateLocal_QUERY);
  end;
end;

procedure TFProducao.__PosicionaServico;
var
  i:Integer;
begin
  for i:=0 to STRGLinhaServicos.ColCount-1 do
  begin
    if(STRGLinhaServicos.Cells[i,2]='P') then
    begin
     STRGLinhaServicos.Col:=i;
     Exit;
    end;

    if(STRGLinhaServicos.Cells[i,2]='C') then
    begin
     STRGLinhaServicos.Col:=i;
    end;
  end;
end;

procedure TFProducao.__MarcaAguardandoServico;
var
  i:Integer;
  NomeRestante2,nome2:string;
begin
  for i:=0 to ListBox2.Count-1 do
  begin
    NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox2.Items[i],' ');
    nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
    TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Color:=$004A4AFF;
    TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).tmpColor:=$004A4AFF;
    TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Repaint;
  end;
end;

procedure TFProducao.lbNomeProjetoMouseLeave(Sender: TObject);
begin
   lbNomeProjeto.Hint:=lbNomeProjeto.Caption;
end;

procedure TFProducao.panel3MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
   lbNomeProjeto.Hint:=lbNomeProjeto.Caption;
end;

procedure TFProducao.__VerificaClickComponente(X,Y:Integer);
var 
  i:Integer;
  NomeRestante2,nome2:string;
  AlgumAberto:Boolean;
  Sender: TObject;
begin
   Exit;
   //SE PRECISAR REATIVAR � SO TIRAR O EXIT
   AlgumAberto:=False;
   for i:=0 to ListBox2.Count-1 do
   begin
      NomeRestante2:=retornaPalavrasDepoisSimbolo2(ListBox2.Items[i],' ');
      nome2:=retornaPalavrasAntesSimbolo(NomeRestante2,' ');
      if(TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).pt1.X <= x) and
      (TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).pt2.X >= x)   and
      (TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).pt1.Y <= y)   and
      (TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).pt2.Y >= Y)   then
      begin
        if(TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Color=$005CADFE) then
        begin
          TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Color:=clWhite;
          TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).tmpColor:=clWhite;
          TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Repaint;
        end
        else Exit;
        //___MostraServicos_PorComponente(TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).ListaNomesMedidas[0],(retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':')));
      end;

      if(TDesenho(listaFigurasParaProducao[StrToInt(nome2)]).Color=$005CADFE)
      then AlgumAberto:=True;
   end;
   if(AlgumAberto=False)
   then btExecutarClick(sender);

end;
procedure TFProducao.PaintBox1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    __VerificaClickComponente(X,Y);
end;

procedure TFProducao.btProximoClick(Sender: TObject);
var
QryListaPedidoProjeto:TIBQuery;
begin

  try
    QryListaPedidoProjeto:=TIBQuery.Create(nil);
    QryListaPedidoProjeto.Database:=FDataModulo.IBDatabase;
  except
    Exit;
  end;

  try
    QryListaPedidoProjeto.Close;
    QryListaPedidoProjeto.SQL.Clear;
    QryListaPedidoProjeto.SQL.Text:=
    'SELECT distinct( pp.codigo) as pedidoprojeto, P.CODIGO as pedido, proj.descricao,C.NOME '+
    'FROM TABPEDIDOPROJ_ORDEMPRODUCAO PPO '+
    'JOIN TABPEDIDO_PROJ PP ON PP.CODIGO=PPO.PEDIDOPROJETO '+
    'JOIN TABPEDIDO P ON P.CODIGO=PP.PEDIDO '+
    'JOIN TABCLIENTE C ON C.CODIGO=P.CLIENTE '+
    'JOIN TABPROJETO Proj ON Proj.CODIGO=PP.PROJETO ';

    if(retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':')<>'') then
    Begin
      QryListaPedidoProjeto.SQL.Add(' WHERE pp.CODIGO > '+retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':'));
      if(_ParametroPesquisaPedido<>'')
      then QryListaPedidoProjeto.SQL.Add('and p.codigo='+_ParametroPesquisaPedido);
      if(_ParametroPesquisaCliente<>'')
      then QryListaPedidoProjeto.SQL.Add('and c.codigo='+_ParametroPesquisaCliente);
    end
    else
    begin
      if(_ParametroPesquisaPedido<>'')
      then QryListaPedidoProjeto.SQL.Add('WHERE p.codigo='+_ParametroPesquisaPedido)
      else
      begin
        if(_ParametroPesquisaCliente<>'')
        then QryListaPedidoProjeto.SQL.Add('and c.codigo='+_ParametroPesquisaCliente);
      end;
    end;

    QryListaPedidoProjeto.SQL.Add('ORDER BY pp.CODIGO');
    QryListaPedidoProjeto.Open;

    if(QryListaPedidoProjeto.RecordCount=0) then
    begin
       Exit;
    end;
    __LimpaTudo;
    ___MostraServicos(QryListaPedidoProjeto.fields[0].asstring);
    lbCliente.Caption:='Cliente :'+QryListaPedidoProjeto.fields[3].asstring;
    STRGLinhaServicos.row:=2;


  finally
    FreeAndNil(QryListaPedidoProjeto);

  end;

end;

procedure TFProducao.btAnteriorClick(Sender: TObject);
var
QryListaPedidoProjeto:TIBQuery;
begin

  try
    QryListaPedidoProjeto:=TIBQuery.Create(nil);
    QryListaPedidoProjeto.Database:=FDataModulo.IBDatabase;
  except
    Exit;
  end;

  try
    QryListaPedidoProjeto.Close;
    QryListaPedidoProjeto.SQL.Clear;
    QryListaPedidoProjeto.SQL.Text:=
    'SELECT distinct(pp.codigo) as pedidoprojeto, P.CODIGO as pedido, proj.descricao,C.NOME '+
    'FROM TABPEDIDOPROJ_ORDEMPRODUCAO PPO '+
    'JOIN TABPEDIDO_PROJ PP ON PP.CODIGO=PPO.PEDIDOPROJETO '+
    'JOIN TABPEDIDO P ON P.CODIGO=PP.PEDIDO '+
    'JOIN TABCLIENTE C ON C.CODIGO=P.CLIENTE '+
    'JOIN TABPROJETO Proj ON Proj.CODIGO=PP.PROJETO ';

    if(retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':')<>'') then
    Begin
      QryListaPedidoProjeto.SQL.Add(' WHERE pp.CODIGO <'+retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':'));
      if(_ParametroPesquisaPedido<>'')
      then QryListaPedidoProjeto.SQL.Add('and p.codigo='+_ParametroPesquisaPedido);
      if(_ParametroPesquisaCliente<>'')
      then QryListaPedidoProjeto.SQL.Add('and c.codigo='+_ParametroPesquisaCliente);
    end
    else
    begin
      if(_ParametroPesquisaPedido<>'')
      then QryListaPedidoProjeto.SQL.Add('WHERE p.codigo='+_ParametroPesquisaPedido)
      else
      begin
        if(_ParametroPesquisaCliente<>'')
        then QryListaPedidoProjeto.SQL.Add('and c.codigo='+_ParametroPesquisaCliente);
      end;
    end;

    QryListaPedidoProjeto.SQL.Add('order BY pp.codigo desc');
    QryListaPedidoProjeto.Open;

    if(QryListaPedidoProjeto.RecordCount=0) then
    begin
       Exit;
    end;

    __LimpaTudo;
    ___MostraServicos(QryListaPedidoProjeto.fields[0].asstring);
    lbCliente.Caption:='Cliente :'+QryListaPedidoProjeto.fields[3].asstring;
    STRGLinhaServicos.row:=2;
  finally
    FreeAndNil(QryListaPedidoProjeto);
  end;

end;

procedure TFProducao.PesquisarporPedido1Click(Sender: TObject);
begin
    with FfiltroImp do
    begin
       DesativaGrupos;
       Grupo01.Enabled:=True;
       edtgrupo01.Text:='';
       LbGrupo01.Caption:='Pedido';
       edtgrupo01.Color:=$005CADFE;
       edtgrupo01.OnKeyDown:=__KeyDownPedido;

       ShowModal;

       if(FfiltroImp.Tag = 0)
       then Exit;

       _ParametroPesquisaPedido:=edtgrupo01.Text;
       _ParametroPesquisaCliente:='';
       lbPedidoProjeto.Caption:='';
       btProximoClick(Sender);

     
    end;
end;

procedure TFProducao.__KeyDownPedido(Sender: TObject;var Key: Word; Shift: TShiftState);
Var
  SQL:string;
  FpesquisaLocal:Tfpesquisa;
begin
  if(key<>vk_f9)
  then Exit;

  SQL:=
  'SELECT DISTINCT(PEDIDO.CODIGO),PEDIDO.DESCRICAO,OBRA,CLIENTE.NOME,VENDEDOR.NOME, '+
  'PEDIDO.VALORFINAL, PEDIDO.DATA,PEDIDO.CONCLUIDO '+
  'FROM TABPEDIDO PEDIDO '+
  'JOIN TABCLIENTE CLIENTE ON CLIENTE.CODIGO=PEDIDO.CLIENTE '+
  'JOIN TABVENDEDOR VENDEDOR ON VENDEDOR.CODIGO=PEDIDO.VENDEDOR '+
  'JOIN TABPEDIDO_PROJ PP ON PP.PEDIDO=PEDIDO.CODIGO '+
  'JOIN TABPEDIDOPROJ_ORDEMPRODUCAO PPO ON PPO.PEDIDOPROJETO = PP.CODIGO';

  Try
     Fpesquisalocal:=Tfpesquisa.create(Nil);
     If (FpesquisaLocal.PreparaPesquisa(SQL,'',nil)=True)
     Then Begin
        Try
            If (FpesquisaLocal.showmodal=mrok)
            Then Begin
                if(FpesquisaLocal.QueryPesq.fields[0].asstring<>'') then
                begin
                   TEdit(Sender).text:= FpesquisaLocal.QueryPesq.fields[0].asstring;
                end;
            End;
        Finally
            FpesquisaLocal.QueryPesq.close;
        End;
     End;

  Finally
     FreeandNil(FPesquisaLocal);
  End;
end;

procedure TFProducao.PesquisarporCliente1Click(Sender: TObject);
begin
  with FfiltroImp do
  begin
     DesativaGrupos;
     Grupo01.Enabled:=True;
     edtgrupo01.Text:='';
     LbGrupo01.Caption:='Cliente';
     edtgrupo01.Color:=$005CADFE;
     edtgrupo01.OnKeyDown:=__KeyDownCliente;

     ShowModal;

     if(FfiltroImp.Tag = 0)
     then Exit;

     _ParametroPesquisaCliente:=edtgrupo01.Text;
     _ParametroPesquisaPedido:='';
     lbPedidoProjeto.Caption:='';
     btProximoClick(Sender);
    
  end;
end;

procedure TFProducao.__KeyDownCliente(Sender: TObject;var Key: Word; Shift: TShiftState);
Var
  SQL:string;
  FpesquisaLocal:Tfpesquisa;
begin
  if(key<>vk_f9)
  then Exit;

  SQL:=
  'SELECT DISTINCT(CLIENTE.CODIGO),CLIENTE.NOME,CLIENTE.FONE '+
  'FROM TABPEDIDO PEDIDO '+
  'JOIN TABCLIENTE CLIENTE ON CLIENTE.CODIGO=PEDIDO.CLIENTE '+
  'JOIN TABVENDEDOR VENDEDOR ON VENDEDOR.CODIGO=PEDIDO.VENDEDOR '+
  'JOIN TABPEDIDO_PROJ PP ON PP.PEDIDO=PEDIDO.CODIGO '+
  'JOIN TABPEDIDOPROJ_ORDEMPRODUCAO PPO ON PPO.PEDIDOPROJETO = PP.CODIGO';

  Try
     Fpesquisalocal:=Tfpesquisa.create(Nil);
     If (FpesquisaLocal.PreparaPesquisa(SQL,'',nil)=True)
     Then Begin
        Try
            If (FpesquisaLocal.showmodal=mrok)
            Then Begin
                if(FpesquisaLocal.QueryPesq.fields[0].asstring<>'') then
                begin
                   TEdit(Sender).text:= FpesquisaLocal.QueryPesq.fields[0].asstring;
                end;
            End;
        Finally
            FpesquisaLocal.QueryPesq.close;
        End;
     End;

  Finally
     FreeandNil(FPesquisaLocal);
  End;
end;

procedure TFProducao.__KeyDownComponentes(Sender: TObject;var Key: Word; Shift: TShiftState);
Var
  SQL:string;
  FpesquisaLocal:Tfpesquisa;
begin
  if(key<>vk_f9)
  then Exit;

  SQL:=
  'SELECT  DISTINCT(PP.CODIGO),C.descricao,C.referencia '+
  'FROM TABPEDIDO_PROJ PP '+
  'JOIN TABCOMPONENTE_PROJ CP ON CP.PROJETO=PP.PROJETO '+
  'JOIN TABCOMPONENTE C ON C.CODIGO=CP.COMPONENTE '+
  'JOIN TABPEDIDOPROJ_ORDEMPRODUCAO PPO ON PPO.PEDIDOPROJETO = PP.CODIGO '+
  'WHERE PP.CODIGO='+retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':');

  Try
     Fpesquisalocal:=Tfpesquisa.create(Nil);
     If (FpesquisaLocal.PreparaPesquisa(SQL,'',nil)=True)
     Then Begin
        Try
            If (FpesquisaLocal.showmodal=mrok)
            Then Begin
                if(FpesquisaLocal.QueryPesq.fields[0].asstring<>'') then
                begin
                   TEdit(Sender).text:= FpesquisaLocal.QueryPesq.fields[2].asstring;
                end;
            End;
        Finally
            FpesquisaLocal.QueryPesq.close;
        End;
     End;

  Finally
     FreeandNil(FPesquisaLocal);
  End;
end;


procedure TFProducao.lbNomedoservicoprodMouseLeave(Sender: TObject);
begin
   lbNomedoservicoprod.Hint:=lbNomedoservicoprod.Caption;
end;

procedure TFProducao.N1Desfazer1Click(Sender: TObject);
begin
  btCancelarClick(Sender);
end;

procedure TFProducao.__ImprimeCodigosBarras;
var
  pgrupo:string;
  pproduto:TStringList;
  tempstr:string;
  PosicaoFolha,temp:Integer;
  ObjQueryLocal : tibquery;
  ObjQueryLocal2 : tibquery;
  ObjRelPersReportBuilder:TObjRELPERSREPORTBUILDER  ;
  pedidoprojeto:string;
  RefComp:string;
begin
  if(retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':')='')
  then Exit
  else pedidoprojeto:=retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':');

  ObjQueryLocal:= TIBQuery.Create(nil);                    //criando um query pra executar SQL
  ObjQueryLocal.Database:= FDataModulo.IBDatabase;
  ObjQueryLocal2:= TIBQuery.Create(nil);                    //criando um query pra executar SQL
  ObjQueryLocal2.Database:= FDataModulo.IBDatabase;
  Try
    pproduto:=TStringList.create;
  Except
    Messagedlg('Erro na tentativa de Criar a String List "PPRODUTO" ',mterror,[mbok],0);
    exit;
  End;
  Try
    //escolhendo o grupo ou o produto
    limpaedit(Ffiltroimp);
    With FfiltroImp do
    Begin
      DesativaGrupos;
      Grupo01.Enabled:=True;
      LbGrupo01.Caption:='Componente';
      edtgrupo01.Text:='';
      edtgrupo01.Color:=$005CADFE;
      edtgrupo01.OnKeyDown:=__KeyDownComponentes;

      Grupo04.Enabled:=True;
      LbGrupo04.caption:='Posi��o na Folha';
      edtGrupo04.EditMask:='';
      edtGrupo04.text:='1';

      showmodal;

      If tag=0
      Then exit;

      posicaofolha:=1;
      Try
        If(edtgrupo04.text<>'')
        Then Begin
          strtoint(edtgrupo04.text);
          posicaofolha:=strtoint(edtgrupo04.text);
          if (PosicaoFolha>0)
          Then dec(posicaofolha,1)
          Else Begin
            if (posicaofolha<0)
            Then PosicaoFolha:=0;
          End;
        End;

        if(edtgrupo01.Text='')
        then RefComp:='0'
        else RefComp:=edtgrupo01.Text;

      Except

      End;
    End;
    try

      try
        ObjRelPersreportBuilder:=TObjRELPERSREPORTBUILDER.Create;
      except
        MensagemErro('Erro ao tentar criar o Objeto ObjRelPersreportBuilder');
        exit;
      end;

      if (ObjRelPersReportBuilder.LocalizaNOme('CODIGO DE BARRAS') = false)
      then Begin
        MensagemErro('O Relatorio de ReporteBuilder "CODIGO DE BARRAS OS" n�o foi econtrado');
        exit;
      end;

      ObjRelPersReportBuilder.TabelaparaObjeto;
      ObjRelPersReportBuilder.SQLCabecalhoPreenchido:=StrReplace(ObjRelPersReportBuilder.Get_SQLCabecalho,':P1',(inttostr(posicaofolha)+','+pedidoprojeto+','+'0'+','+#39+RefComp+#39));
      ObjRelPersReportBuilder.SQLRepeticaoPreenchido:=StrReplace(ObjRelPersReportBuilder.get_SQLRepeticao,':P1',(inttostr(posicaofolha)+','+pedidoprojeto+','+'0'+','+#39+RefComp+#39));

    
      ObjRelPersReportBuilder.ChamaRelatorio(false,true);
    finally
      ObjRelPersreportBuilder.Free;
    end;

  Finally
    freeandnil(pproduto);
  End;
end;

procedure TFProducao.__ImprimeEtiquetas;
var
  pgrupo:string;
  pproduto:TStringList;
  tempstr:string;
  PosicaoFolha,temp:Integer;
  ObjQueryLocal : tibquery;
  ObjQueryLocal2 : tibquery;
  ObjRelPersReportBuilder:TObjRELPERSREPORTBUILDER  ;
  pedidoprojeto:string;
  RefComp:string;
begin
  if(retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':')='')
  then Exit
  else pedidoprojeto:=retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':');

  ObjQueryLocal:= TIBQuery.Create(nil);                    //criando um query pra executar SQL
  ObjQueryLocal.Database:= FDataModulo.IBDatabase;
  ObjQueryLocal2:= TIBQuery.Create(nil);                    //criando um query pra executar SQL
  ObjQueryLocal2.Database:= FDataModulo.IBDatabase;
  Try
    pproduto:=TStringList.create;
  Except
    Messagedlg('Erro na tentativa de Criar a String List "PPRODUTO" ',mterror,[mbok],0);
    exit;
  End;
  Try
    //escolhendo o grupo ou o produto
    limpaedit(Ffiltroimp);
    With FfiltroImp do
    Begin
      DesativaGrupos;
      Grupo01.Enabled:=True;
      LbGrupo01.Caption:='Componente';
      edtgrupo01.Text:='';
      edtgrupo01.Color:=$005CADFE;
      edtgrupo01.OnKeyDown:=__KeyDownComponentes;

      Grupo04.Enabled:=True;
      LbGrupo04.caption:='Posi��o na Folha';
      edtGrupo04.EditMask:='';
      edtGrupo04.text:='1';

      showmodal;

      If tag=0
      Then exit;

      posicaofolha:=1;
      Try
        If(edtgrupo04.text<>'')
        Then Begin
          strtoint(edtgrupo04.text);
          posicaofolha:=strtoint(edtgrupo04.text);
          if (PosicaoFolha>0)
          Then dec(posicaofolha,1)
          Else Begin
            if (posicaofolha<0)
            Then PosicaoFolha:=0;
          End;
        End;

        if(edtgrupo01.Text='')
        then RefComp:='0'
        else RefComp:=edtgrupo01.Text;

      Except

      End;
    End;
    try

      try
        ObjRelPersreportBuilder:=TObjRELPERSREPORTBUILDER.Create;
      except
        MensagemErro('Erro ao tentar criar o Objeto ObjRelPersreportBuilder');
        exit;
      end;

      if (ObjRelPersReportBuilder.LocalizaNOme('ETIQUETAS PRODUCAO') = false)
      then Begin
        MensagemErro('O Relatorio de ReporteBuilder "CODIGO DE BARRAS OS" n�o foi econtrado');
        exit;
      end;

      ObjRelPersReportBuilder.TabelaparaObjeto;
      ObjRelPersReportBuilder.SQLCabecalhoPreenchido:=StrReplace(ObjRelPersReportBuilder.Get_SQLCabecalho,':P1',(inttostr(posicaofolha)+','+pedidoprojeto+','+'0'+','+#39+RefComp+#39));
      ObjRelPersReportBuilder.SQLRepeticaoPreenchido:=StrReplace(ObjRelPersReportBuilder.get_SQLRepeticao,':P1',(inttostr(posicaofolha)+','+pedidoprojeto+','+'0'+','+#39+RefComp+#39));

      ObjRelPersReportBuilder.ChamaRelatorio(false,true);
    finally
      ObjRelPersreportBuilder.Free;
    end;

  Finally
    freeandnil(pproduto);
  End;
end;

procedure TFProducao.ImprimirCodigodeBarras1Click(Sender: TObject);
begin
    __ImprimeEtiquetas;
end;

procedure TFProducao.__LocalizaProjetosPorPedido(ParametroPedido:string);
begin
   _ParametroPesquisaPedido:=ParametroPedido;
end;

procedure TFProducao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
  if (Key = VK_F1) then
  begin
    try
     formPesquisaMenu:=TfPesquisaMenu.Create(nil);
     formPesquisaMenu.submit_formulario(Fprincipal);
     formPesquisaMenu.ShowModal;
    finally
     FreeAndNil(formPesquisaMenu);
    end;

  end;

  if (Key = VK_F2) then
  begin
    FAjuda.PassaAjuda('ORDEM DE PRODUCAO');
    FAjuda.ShowModal;
  end;

end;

procedure TFProducao.ExcluirPedidoProjeto1Click(Sender: TObject);
var
  QryExcluirProjeto:TIBQuery;
begin
  if (messagedlg('Deseja excluir este pedido projeto da linha de produ��o?',mtconfirmation,[mbyes,mbno],0)=mrno)
  then exit ;

  try
    QryExcluirProjeto:=TIBQuery.Create(nil);
    QryExcluirProjeto.Database:=FDataModulo.IBDatabase;
  except
    Exit;
  end;

  try
    QryExcluirProjeto.Close;
    QryExcluirProjeto.SQL.Clear;
    QryExcluirProjeto.SQL.Text:=
    'DELETE FROM TABCOMPONENTES_ORDEMPRODUCAO '+
    'WHERE PEDIDOPROJETO='+retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':');

    try
      QryExcluirProjeto.ExecSQL;
    except
      FDataModulo.IBTransaction.RollbackRetaining;
      Exit;
    end;

    QryExcluirProjeto.Close;
    QryExcluirProjeto.SQL.Clear;
    QryExcluirProjeto.SQL.Text:=
    'DELETE FROM TABPEDIDOPROJ_ORDEMPRODUCAO '+
    'WHERE PEDIDOPROJETO='+retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':');

    try
      QryExcluirProjeto.ExecSQL;
    except
      FDataModulo.IBTransaction.RollbackRetaining;
      Exit;
    end;

    FDataModulo.IBTransaction.CommitRetaining;
    __LimpaTudo;
    __LimpaTudoIMGProducao;
    ___MostraServicos(retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':'));
  finally
    FreeAndNil(QryExcluirProjeto);
  end;

end;

procedure TFProducao.__LocalizaPecaCODIGOBARRAS;
var
QryListaPedidoProjeto:TIBQuery;
QryAtualizaStatusProducao_QUERY:TIBQuery;
begin
  if(edtCodigoBarras.Text='')
  then Exit;
  
  try
    QryListaPedidoProjeto:=TIBQuery.Create(nil);
    QryListaPedidoProjeto.Database:=FDataModulo.IBDatabase;
    QryAtualizaStatusProducao_QUERY:=TIBQuery.Create(nil);
    QryAtualizaStatusProducao_QUERY.Database:=FDataModulo.IBDatabase;
  except
    Exit;
  end;
  //SELECIONO O COMPONENTE DE ACORDO COM O CODIGO DE BARRAS INFORMADO
  try
    QryListaPedidoProjeto.Close;
    QryListaPedidoProjeto.SQL.Clear;
    QryListaPedidoProjeto.SQL.Text:=
    'SELECT PPOP.pedidoprojeto, P.CODIGO as pedido, proj.descricao,C.NOME,PPOP.servico, TC.refcomponente '+
    ',TC.estadoproducao,TC.codigo '+
    'FROM TABCOMPONENTES_ORDEMPRODUCAO TC '+
    'JOIN TABPEDIDOPROJ_ORDEMPRODUCAO PPOP ON (PPOP.codigo=TC.pp_ordemproducao) '+
    'JOIN TABPEDIDO_PROJ PP ON PP.CODIGO=PPOP.PEDIDOPROJETO '+
    'JOIN TABPEDIDO P ON P.CODIGO=PP.PEDIDO '+
    'JOIN TABCLIENTE C ON C.CODIGO=P.CLIENTE '+
    'JOIN TABPROJETO Proj ON Proj.CODIGO=PP.PROJETO '+
    'WHERE TC.codigobarras='+#39+edtCodigoBarras.Text+#39+' '+
    'AND TC.ESTADOPRODUCAO<>''C'' '+
    'ORDER BY TC.CODIGO';


    QryListaPedidoProjeto.Open;

    if(QryListaPedidoProjeto.RecordCount=0) then
    begin
       __LimpaTudo;
       __LimpaTudoIMGProducao;
       if(Length(edtCodigoBarras.Text)=11) then
       begin
          edtCodigoBarras.Text:='';
          imgFalse.Visible:=True;
          imgSucesso.Visible:=False;
          lbNomedoservicoprod.Caption:='N�o h� mais servi�os nesta pe�a!!!';
       end;
       Exit;
    end;

    //verifica se o servi�o � desta mesa
    if(QryListaPedidoProjeto.fields[4].asstring<>_CODIGOSERVICO) then
    begin
      edtCodigoBarras.Text:='';
      imgFalse.Visible:=True;
      imgSucesso.Visible:=False;
      lbNomedoservicoprod.Caption:='O servi�o de '+ get_campoTabela('descricao','codigo','tabservico',QryListaPedidoProjeto.fields[4].asstring)+' deve ser executado em outro terminal!!!';

      Exit;
    end;
    {if(___LiberaParaAlteracaoStatus(QryListaPedidoProjeto.fields[4].asstring,QryListaPedidoProjeto.fields[0].asstring,QryListaPedidoProjeto.fields[7].asstring,QryListaPedidoProjeto.fields[5].asstring)=False)
    then begin
      edtCodigoBarras.Text:='';
      __LimpaTudo;
      ___MostraServicos(QryListaPedidoProjeto.fields[0].asstring);
      lbCliente.Caption:='Cliente :'+QryListaPedidoProjeto.fields[3].asstring;
      STRGLinhaServicos.row:=2;
      __MarcaExecucaoServico_Componente(QryListaPedidoProjeto.fields[5].asstring,get_campoTabela('estadoproducao','codigo','TABCOMPONENTES_ORDEMPRODUCAO',QryListaPedidoProjeto.fields[7].asstring));

      Exit;
    end;   }
    if(___ExecutarServico_Componente(QryListaPedidoProjeto.fields[7].asstring,QryListaPedidoProjeto.fields[6].asstring,QryListaPedidoProjeto.fields[4].asstring)=false)
    then begin
      __LimpaTudo;
      ___MostraServicos(QryListaPedidoProjeto.fields[0].asstring);
      lbCliente.Caption:='Cliente :'+QryListaPedidoProjeto.fields[3].asstring;
      STRGLinhaServicos.row:=2;
      __MarcaExecucaoServico_Componente(QryListaPedidoProjeto.fields[5].asstring,get_campoTabela('estadoproducao','codigo','TABCOMPONENTES_ORDEMPRODUCAO',QryListaPedidoProjeto.fields[7].asstring));
      edtCodigoBarras.Text:='';
      
      Exit;
    end;
    ___VerificaConclusaoServicos(QryListaPedidoProjeto.fields[4].asstring,QryListaPedidoProjeto.fields[0].asstring);
    __LimpaTudo;
    ___MostraServicos(QryListaPedidoProjeto.fields[0].asstring);
    lbCliente.Caption:='Cliente :'+QryListaPedidoProjeto.fields[3].asstring;
    STRGLinhaServicos.row:=2;
    __MarcaExecucaoServico_Componente(QryListaPedidoProjeto.fields[5].asstring,get_campoTabela('estadoproducao','codigo','TABCOMPONENTES_ORDEMPRODUCAO',QryListaPedidoProjeto.fields[7].asstring));
    edtCodigoBarras.Text:='';
  finally
    FreeAndNil(QryListaPedidoProjeto);
    FreeAndNil(QryAtualizaStatusProducao_QUERY);
  end;
end;

function TFProducao.___ExecutarServico_Componente(codigo:string;Status,servico:string):Boolean;
var
   _qryUpdateLocal_QUERY:TIBQuery;
begin
  Result:=True;
  try
     _qryUpdateLocal_QUERY:=TIBQuery.Create(nil);
     _qryUpdateLocal_QUERY.Database:=FDataModulo.IBDatabase;
  except
     FDataModulo.IBTransaction.RollbackRetaining;
     Exit;
  end;
  try
    //Se o estado da produ��o estiver aberto, preciso alterar para concluido naquela etapa
    if(Status='A') then
    begin
      _qryUpdateLocal_QUERY.Close;
      _qryUpdateLocal_QUERY.SQL.Clear;
      _qryUpdateLocal_QUERY.SQL.Text:=
      'UPDATE TABCOMPONENTES_ORDEMPRODUCAO SET ESTADOPRODUCAO =''C'' '+
      'WHERE CODIGO='+codigo;
      try
        _qryUpdateLocal_QUERY.ExecSQL;
      except
        edtCodigoBarras.Text:='';
        Result:=False;
        Exit;
      end;
    end
    else
    begin
      if(Status='C')then
      begin
        imgFalse.Visible:=True;
        imgSucesso.Visible:=False;
        lbNomedoservicoprod.Caption:='J� foi conclu�do a execu��o de todos os servi�os nesta pe�a!';
        edtCodigoBarras.Text:='';
        Result:=False;
      end;
    end;

    FDataModulo.IBTransaction.CommitRetaining;
  finally
    FreeAndNil(_qryUpdateLocal_QUERY);
    FDataModulo.IBTransaction.RollbackRetaining;
  end;
end;

procedure TFProducao.edtCodigoBarrasChange(Sender: TObject);
begin
  __LocalizaPecaCODIGOBARRAS;
  __Preenchemedidas(retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':'),get_campoTabela('projeto','codigo','tabpedido_proj',retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':')));
  __Preenchemedidas(retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':'),get_campoTabela('projeto','codigo','tabpedido_proj',retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':')));

end;

function TFProducao.___VerificaConclusaoServicos(SERVICO,PEDIDOPROJETO:string):Boolean;
var
  QryPesquisaExecucao_QUERY:TIBQuery;
  QryUPExecucao_QUERY:TIBQuery;
  Concluido,Aberto:Integer;
  //Emproducao:Boolean;
begin
  try
    QryPesquisaExecucao_QUERY:=TIBQuery.Create(nil);
    QryPesquisaExecucao_QUERY.Database:=FDataModulo.IBDatabase;
    QryUPExecucao_QUERY:=TIBQuery.Create(nil);
    QryUPExecucao_QUERY.Database:=FDataModulo.IBDatabase;
  except
    Exit;
  end;
  //Preciso verificar como esta o estado em cada servi�o
  try
    QryPesquisaExecucao_QUERY.Close;
    QryPesquisaExecucao_QUERY.SQL.Clear;
    QryPesquisaExecucao_QUERY.SQL.Text:=
    'SELECT ESTADOPRODUCAO FROM TABCOMPONENTES_ORDEMPRODUCAO '+
    'WHERE PEDIDOPROJETO='+PEDIDOPROJETO+' '+
    'AND SERVICO='+SERVICO;

    QryPesquisaExecucao_QUERY.Open;

    if(QryPesquisaExecucao_QUERY.RecordCount=0)
    then Exit;

    Concluido:=0;
    Aberto:=0;
    while not QryPesquisaExecucao_QUERY.Eof do
    begin
      //Conto quantas pe�as ja foram concluidas
      if(QryPesquisaExecucao_QUERY.Fields[0].AsString='C')
      then Inc(Concluido,1)
      else
      begin
        //conto quantas pe�as est�o abertas
        if(QryPesquisaExecucao_QUERY.Fields[0].AsString='A')
        then Inc(Aberto,1)
      end;
      QryPesquisaExecucao_QUERY.Next;
    end;
    //Se todas est�o concluidas, ent�o preciso concluir o servi�o
    if(Concluido=QryPesquisaExecucao_QUERY.RecordCount) then
    begin
      QryUPExecucao_QUERY.Close;
      QryUPExecucao_QUERY.SQL.Clear;
      QryUPExecucao_QUERY.SQL.Text:=
      'update tabpedidoproj_ordemproducao set estadoproducao=''C'' '+
      'WHERE PEDIDOPROJETO='+PEDIDOPROJETO+' '+
      'AND SERVICO='+SERVICO;
      try
        QryUPExecucao_QUERY.ExecSQL;
      except
        Exit;
      end;
    end
    else
    begin
      //Se todas est�o abertas, mostro os servi�os como aberto
      if(Aberto=QryPesquisaExecucao_QUERY.RecordCount) then
      begin
        QryUPExecucao_QUERY.Close;
        QryUPExecucao_QUERY.SQL.Clear;
        QryUPExecucao_QUERY.SQL.Text:=
        'update tabpedidoproj_ordemproducao set estadoproducao=''A'' '+
        'WHERE PEDIDOPROJETO='+PEDIDOPROJETO+' '+
        'AND SERVICO='+SERVICO;

        try
          QryUPExecucao_QUERY.ExecSQL;
        except
          Exit;
        end;
      end
      else
      begin
        //se estiver com um em produ��o, ent�o o determinado servi�o vai ser marcado como em produ��o
        QryUPExecucao_QUERY.Close;
        QryUPExecucao_QUERY.SQL.Clear;
        QryUPExecucao_QUERY.SQL.Text:=
        'update tabpedidoproj_ordemproducao set estadoproducao=''P'' '+
        'WHERE PEDIDOPROJETO='+PEDIDOPROJETO+' '+
        'AND SERVICO='+SERVICO;

        try
          QryUPExecucao_QUERY.ExecSQL;
        except
          Exit;
        end;
      end;
    end;

    FDataModulo.IBTransaction.CommitRetaining;
  finally
    FreeAndNil(QryPesquisaExecucao_QUERY);
    FreeAndNil(QryUPExecucao_QUERY);
  end;     

end;

function TFProducao.___LiberaParaAlteracaoStatus(SERVICO,PEDIDOPROJETO,CODIGO,REFCOMPONENTE:string):Boolean;
var
  Qry_Pesquisa_QUERY:TIBQuery;
begin
  Result:=True;
  try
    Qry_Pesquisa_QUERY:=TIBQuery.Create(nil);
    Qry_Pesquisa_QUERY.Database:=FDataModulo.IBDatabase;
  except
    Exit;
  end;

  try
    {//VERIFICA SE N�O EXISTE SERVI�O PENDENTE PRA PE�A EM ETAPAS ANTERIORES
    Qry_Pesquisa_QUERY.Close;
    Qry_Pesquisa_QUERY.SQL.Clear;
    Qry_Pesquisa_QUERY.SQL.Text:=
    'SELECT ESTADOPRODUCAO,TABSERVICO.DESCRICAO FROM TABCOMPONENTES_ORDEMPRODUCAO '+
    'JOIN TABSERVICO ON TABSERVICO.CODIGO=TABCOMPONENTES_ORDEMPRODUCAO.SERVICO '+
    'WHERE TABCOMPONENTES_ORDEMPRODUCAO.CODIGO<'+CODIGO+' '+
    'AND TABCOMPONENTES_ORDEMPRODUCAO.SERVICO<>'+SERVICO+' '+
    'AND TABCOMPONENTES_ORDEMPRODUCAO.PEDIDOPROJETO='+PEDIDOPROJETO+' '+
    'ORDER BY TABCOMPONENTES_ORDEMPRODUCAO.CODIGO';

    Qry_Pesquisa_QUERY.Open;

    while not Qry_Pesquisa_QUERY.Eof do
    begin
      if(Qry_Pesquisa_QUERY.Fields[0].AsString<>'C') then
      begin
        MensagemAviso('Existem pe�as aguardando execu��o no Servi�o anterior'+#13+'SERVI�O ANTERIOR :'+Qry_Pesquisa_QUERY.Fields[1].AsString);
        Result:=False;
        Exit;
      end;

      Qry_Pesquisa_QUERY.Next;
    end;    }

    //VERIFICA SE ESSE SERVI�O N�O ESTA CONCLU�DO NESSA MESA

   { Qry_Pesquisa_QUERY.Close;
    Qry_Pesquisa_QUERY.SQL.Clear;
    Qry_Pesquisa_QUERY.SQL.Text:=
    'SELECT ESTADOPRODUCAO,TABSERVICO.DESCRICAO,TABCOMPONENTES_ORDEMPRODUCAO.REFCOMPONENTE,TABCOMPONENTE.descricao '+
    'FROM TABCOMPONENTES_ORDEMPRODUCAO  '+
    'JOIN TABSERVICO ON TABSERVICO.CODIGO=TABCOMPONENTES_ORDEMPRODUCAO.SERVICO '+
    'JOIN TABCOMPONENTE ON TABCOMPONENTE.REFERENCIA=TABCOMPONENTES_ORDEMPRODUCAO.REFCOMPONENTE '+
    'WHERE TABCOMPONENTES_ORDEMPRODUCAO.SERVICO ='+SERVICO+' '+
    'AND TABCOMPONENTES_ORDEMPRODUCAO.PEDIDOPROJETO='+PEDIDOPROJETO+' '+
    'AND TABCOMPONENTES_ORDEMPRODUCAO.estadoproducao=''C'' '+
    'AND TABCOMPONENTES_ORDEMPRODUCAO.REFCOMPONENTE<>'+#39+REFCOMPONENTE+#39+' '+
    //'AND TABSERVICO.CODIGO='+_CODIGOSERVICO+' '+
    'ORDER BY TABCOMPONENTES_ORDEMPRODUCAO.CODIGO';

    Qry_Pesquisa_QUERY.Open;
    if(Qry_Pesquisa_QUERY.RecordCount=0) then
    begin
      edtCodigoBarras.Text:='';
      Exit;
    end;

    MensagemAviso('ESSA PE�A JA SOFREU ESTE SERVI�O / V� para a pr�xima etapa!'+#13+
    'PE�A :'+Qry_Pesquisa_QUERY.Fields[2].AsString+' - '+Qry_Pesquisa_QUERY.Fields[3].AsString+#13+
    'SERVI�O: '+Qry_Pesquisa_QUERY.Fields[1].AsString+#13+
    'PEIDOPROJETO :'+PEDIDOPROJETO);
    Result:=False; }

  finally
    FreeAndNil(Qry_Pesquisa_QUERY);
  end;

end;

procedure TFProducao.ImprimirEtiquetasCodigodeBarras1Click(
  Sender: TObject);
begin
     __ImprimeCodigosBarras;
end;

procedure TFProducao.DesfazerUltimoServionapeaSelecionada1Click(
  Sender: TObject);
begin
//
end;

procedure TFProducao.PesquisarporCodigodeBarras1Click(Sender: TObject);
var
QryListaPedidoProjeto:TIBQuery;
QryAtualizaStatusProducao_QUERY:TIBQuery;
codigobarras:string;
begin
  codigobarras:='';
  InputQuery('Informe o codigo de barras','Codigo de barras',codigobarras);

  if(codigobarras='')
  then Exit;

  try
    QryListaPedidoProjeto:=TIBQuery.Create(nil);
    QryListaPedidoProjeto.Database:=FDataModulo.IBDatabase;
    QryAtualizaStatusProducao_QUERY:=TIBQuery.Create(nil);
    QryAtualizaStatusProducao_QUERY.Database:=FDataModulo.IBDatabase;
  except
    Exit;
  end;
  //SELECIONO O COMPONENTE DE ACORDO COM O CODIGO DE BARRAS INFORMADO
  try
    QryListaPedidoProjeto.Close;
    QryListaPedidoProjeto.SQL.Clear;
    QryListaPedidoProjeto.SQL.Text:=
    'SELECT PPOP.pedidoprojeto, P.CODIGO as pedido, proj.descricao,C.NOME,PPOP.servico, TC.refcomponente '+
    ',TC.estadoproducao,TC.codigo '+
    'FROM TABCOMPONENTES_ORDEMPRODUCAO TC '+
    'JOIN TABPEDIDOPROJ_ORDEMPRODUCAO PPOP ON (PPOP.codigo=TC.pp_ordemproducao) '+
    'JOIN TABPEDIDO_PROJ PP ON PP.CODIGO=PPOP.PEDIDOPROJETO '+
    'JOIN TABPEDIDO P ON P.CODIGO=PP.PEDIDO '+
    'JOIN TABCLIENTE C ON C.CODIGO=P.CLIENTE '+
    'JOIN TABPROJETO Proj ON Proj.CODIGO=PP.PROJETO '+
    'WHERE TC.codigobarras='+#39+codigobarras+#39+' '+
    'ORDER BY TC.CODIGO';

    QryListaPedidoProjeto.Open;

    if(QryListaPedidoProjeto.RecordCount=0) then
    begin
       __LimpaTudo;
       __LimpaTudoIMGProducao;
       if(Length(codigobarras)=11) then
       begin
          codigobarras:='';
          imgFalse.Visible:=True;
          imgSucesso.Visible:=False;
          lbNomedoservicoprod.Caption:='PE�A N�O ENCONTRADA!!!';
       end;
       Exit;
    end;
    __LimpaTudo;
    ___MostraServicos(QryListaPedidoProjeto.fields[0].asstring);
    __MarcaExecucaoServico_Componente(QryListaPedidoProjeto.fields[5].asstring,get_campoTabela('estadoproducao','codigo','TABCOMPONENTES_ORDEMPRODUCAO',QryListaPedidoProjeto.fields[7].asstring));
    lbCliente.Caption:='Cliente :'+QryListaPedidoProjeto.fields[3].asstring;
    STRGLinhaServicos.row:=2;
    codigobarras:='';
  finally
    FreeAndNil(QryListaPedidoProjeto);
    FreeAndNil(QryAtualizaStatusProducao_QUERY);
  end;
end;

procedure TFProducao.edtCodigoBarrasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if(Key=vk_f6) then
    begin
      PesquisarporCodigodeBarras1Click(Sender);
      __Preenchemedidas(retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':'),get_campoTabela('projeto','codigo','tabpedido_proj',retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':')));
      __Preenchemedidas(retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':'),get_campoTabela('projeto','codigo','tabpedido_proj',retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':')));
    end;

    if(Key=vk_f5) then
    begin
      __Preenchemedidas(retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':'),get_campoTabela('projeto','codigo','tabpedido_proj',retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':')));
      __Preenchemedidas(retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':'),get_campoTabela('projeto','codigo','tabpedido_proj',retornaPalavrasDepoisSimbolo2(lbPedidoProjeto.Caption,':')));
    end;
end;

end.
