unit UPERFILADO_NF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjPERFILADO_NF,
  jpeg;

type
  TFPERFILADO_NF = class(TForm)
    Guia: TTabbedNotebook;
    Label1: TLabel;
    Edit1: TEdit;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbNOTAFISCAL: TLabel;
    EdtNOTAFISCAL: TEdit;
    LbNomeNOTAFISCAL: TLabel;
    procedure edtNOTAFISCALKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtNOTAFISCALExit(Sender: TObject);
    LbPERFILADOCOR: TLabel;
    EdtPERFILADOCOR: TEdit;
    LbNomePERFILADOCOR: TLabel;
    procedure edtPERFILADOCORKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtPERFILADOCORExit(Sender: TObject);
    LbQUANTIDADE: TLabel;
    EdtQUANTIDADE: TEdit;
    LbVALOR: TLabel;
    EdtVALOR: TEdit;
    LbVALORFINAL: TLabel;
    EdtVALORFINAL: TEdit;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjPERFILADO_NF:TObjPERFILADO_NF;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPERFILADO_NF: TFPERFILADO_NF;


implementation

uses UessencialGlobal, Upesquisa;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFPERFILADO_NF.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjPERFILADO_NF do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        NOTAFISCAL.Submit_codigo(edtNOTAFISCAL.text);
        PERFILADOCOR.Submit_codigo(edtPERFILADOCOR.text);
        Submit_QUANTIDADE(edtQUANTIDADE.text);
        Submit_VALOR(edtVALOR.text);
        Submit_VALORFINAL(edtVALORFINAL.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFPERFILADO_NF.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjPERFILADO_NF do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNOTAFISCAL.text:=NOTAFISCAL.Get_codigo;
        EdtPERFILADOCOR.text:=PERFILADOCOR.Get_codigo;
        EdtQUANTIDADE.text:=Get_QUANTIDADE;
        EdtVALOR.text:=Get_VALOR;
        EdtVALORFINAL.text:=Get_VALORFINAL;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFPERFILADO_NF.TabelaParaControles: Boolean;
begin
     If (Self.ObjPERFILADO_NF.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFPERFILADO_NF.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjPERFILADO_NF:=TObjPERFILADO_NF.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

procedure TFPERFILADO_NF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjPERFILADO_NF=Nil)
     Then exit;

If (Self.ObjPERFILADO_NF.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjPERFILADO_NF.free;
end;

procedure TFPERFILADO_NF.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFPERFILADO_NF.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjPERFILADO_NF.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjPERFILADO_NF.status:=dsInsert;
     Guia.pageindex:=0;
     EdtPrimeiro.setfocus;

end;


procedure TFPERFILADO_NF.btalterarClick(Sender: TObject);
begin
    If (Self.ObjPERFILADO_NF.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjPERFILADO_NF.Status:=dsEdit;
                guia.pageindex:=0;
                edtPrimeiro.setfocus;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;


end;

procedure TFPERFILADO_NF.btgravarClick(Sender: TObject);
begin

     If Self.ObjPERFILADO_NF.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjPERFILADO_NF.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjPERFILADO_NF.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFPERFILADO_NF.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjPERFILADO_NF.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjPERFILADO_NF.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjPERFILADO_NF.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFPERFILADO_NF.btcancelarClick(Sender: TObject);
begin
     Self.ObjPERFILADO_NF.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFPERFILADO_NF.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFPERFILADO_NF.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjPERFILADO_NF.Get_pesquisa,Self.ObjPERFILADO_NF.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjPERFILADO_NF.status<>dsinactive
                                  then exit;

                                  If (Self.ObjPERFILADO_NF.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjPERFILADO_NF.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFPERFILADO_NF.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFPERFILADO_NF.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
procedure TFPERFILADO_NF.edtNOTAFISCALKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjPERFILADO_NF.edtNOTAFISCALkeydown(sender,key,shift,lbnomeNOTAFISCAL);
end;
 
procedure TFPERFILADO_NF.edtNOTAFISCALExit(Sender: TObject);
begin
    ObjPERFILADO_NF.edtNOTAFISCALExit(sender,lbnomeNOTAFISCAL);
end;
procedure TFPERFILADO_NF.edtPERFILADOCORKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjPERFILADO_NF.edtPERFILADOCORkeydown(sender,key,shift,lbnomePERFILADOCOR);
end;
 
procedure TFPERFILADO_NF.edtPERFILADOCORExit(Sender: TObject);
begin
    ObjPERFILADO_NF.edtPERFILADOCORExit(sender,lbnomePERFILADOCOR);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjPERFILADO_NF.OBJETO.Get_Pesquisa,Self.ObjPERFILADO_NF.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjPERFILADO_NF.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjPERFILADO_NF.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjPERFILADO_NF.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
