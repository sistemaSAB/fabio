unit UAcertoUnidadeEntrada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, StdCtrls, ImgList, Buttons,UUNIDADEMEDIDA,UessencialGlobal,UPERFILADO,IBQuery
  ,UDataModulo,UobjRELACAOMEDIDAS,UAcertaUnidades, pngimage;

type
  TFAcertoUnidadeEntrada = class(TForm)
    panel3: TPanel;
    lbDescricaQuantidadeEntrada: TLabel;
    lbNomeMaterial1: TLabel;
    lbValorEquivalencia: TLabel;
    lbNomematerial2: TLabel;
    lbDescricaoquantidadeConversao: TLabel;
    Img1: TImage;
    lbErroUnidadeMedidaEntrada: TLabel;
    lbErroUnidadeMedidaCadastro: TLabel;
    lbErroEquivalencia: TLabel;
    Img2: TImage;
    ImgUnMedidaEntradaCorrigido: TImage;
    ImgUnMedidaCadastroCorrigido: TImage;
    ImgEquivalenciaCorrigido: TImage;
    ImgErroEquivalencia: TImage;
    ImgErroUnidadeCadastro: TImage;
    ImgErroUnidadeEntrada: TImage;
    shp1: TShape;
    shp2: TShape;
    btProcessar: TSpeedButton;
    btCancelar: TSpeedButton;
    bt1: TSpeedButton;
    panel2: TPanel;
    procedure lbErroUnidadeMedidaEntradaMouseLeave(Sender: TObject);
    procedure lbErroUnidadeMedidaEntradaMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbErroUnidadeMedidaEntradaClick(Sender: TObject);
    procedure lbErroUnidadeMedidaCadastroClick(Sender: TObject);
    procedure lbErroEquivalenciaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btProcessarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure bt1Click(Sender: TObject);
  private
    ObjRelacaoMedidas:TObjRELACAOMEDIDAS;
    UnidadeEntradaGlobal:string;
    MaterialGlobal:string;
    TipoMaterialGlobal:string;
    UnidadeMaterialGlobal:string;
    HouveErro:Boolean;
    QuantidadeEntrada:string;
    Peso:string;
  public
     QuantidadeConvertida:Currency;  
     Procedure PassaObjetos(Material,UnidadeEntrada,UnidadeMaterial,TipoMaterial,Quantidade,Peso:string);
     procedure ValidaDados;

  end;

var
  FAcertoUnidadeEntrada: TFAcertoUnidadeEntrada;

implementation

uses UDIVERSO, UPERSIANA, UVIDRO, UKITBOX, UFERRAGEM;

{$R *.dfm}

procedure TFAcertoUnidadeEntrada.lbErroUnidadeMedidaEntradaMouseLeave(Sender: TObject);
begin
   TEdit(sender).Font.Style:=TEdit(sender).Font.Style-[fsBold,fsUnderline];
end;

procedure TFAcertoUnidadeEntrada.lbErroUnidadeMedidaEntradaMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
   TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFAcertoUnidadeEntrada.lbErroUnidadeMedidaEntradaClick(
  Sender: TObject);
begin
   chamaFormulario(TFUNIDADEMEDIDA,self);
   if(get_campoTabela('codigo','sigla','tabunidademedida',UnidadeEntradaGlobal)<>'') then
   begin
       ImgErroUnidadeEntrada.Visible:=False;
       ImgUnMedidaEntradaCorrigido.Visible:=True;
   end;

end;

procedure TFAcertoUnidadeEntrada.lbErroUnidadeMedidaCadastroClick(
  Sender: TObject);
begin
   if(TipoMaterialGlobal='Diverso')
   then chamaFormulario(TFDIVERSO,self,MaterialGlobal);
   if(TipoMaterialGlobal='Perfilado')
   then chamaFormulario(TFPERFILADO,self,MaterialGlobal);
   if(TipoMaterialGlobal='Persiana')
   then chamaFormulario(TFPERSIANA,self,MaterialGlobal);
   if(TipoMaterialGlobal='Vidro')
   then chamaFormulario(TFVIDRO,self,MaterialGlobal);
   if(TipoMaterialGlobal='KitBox')
   then chamaFormulario(TFKITBOX,self,MaterialGlobal);
   if(TipoMaterialGlobal='Ferragem')
   then chamaFormulario(TFFERRAGEM,self,MaterialGlobal);

   if(get_campoTabela('codigo','sigla','tabunidademedida',UnidadeMaterialGlobal)<>'') then
   begin
      ImgErroUnidadeCadastro.visible:=false;
      ImgUnMedidaCadastroCorrigido.visible:=True;
   end;
end;

procedure TFAcertoUnidadeEntrada.lbErroEquivalenciaClick(Sender: TObject);
begin
   chamaFormulario(TFAcertaUnidades,self);
   ValidaDados;
end;

procedure TFAcertoUnidadeEntrada.FormShow(Sender: TObject);
begin
   //Mostrando icone de erro
   ImgErroUnidadeEntrada.Visible:=False;
   ImgUnMedidaEntradaCorrigido.Visible:=False;
   ImgErroUnidadeCadastro.visible:=False;
   ImgUnMedidaCadastroCorrigido.visible:=False;
   ImgErroEquivalencia.visible:=False;
   ImgEquivalenciaCorrigido.visible:=False;

   //Escondendo labels de erro
   lbErroUnidadeMedidaEntrada.Visible:=false;
   lbErroUnidadeMedidaCadastro.Visible:=False;
   lbErroEquivalencia.Visible:=False;


   //OBJETOS
   ObjRelacaoMedidas:=TObjRELACAOMEDIDAS.Create;

   //Valida os dados antes de efetuar a convers�o
   ValidaDados;

end;

Procedure TFAcertoUnidadeEntrada.PassaObjetos(Material,UnidadeEntrada,UnidadeMaterial,TipoMaterial,Quantidade,Peso:string);
begin
   self.UnidadeEntradaGlobal:=UnidadeEntrada;
   self.MaterialGlobal:=Material;
   Self.UnidadeMaterialGlobal:=Unidadematerial;
   self.TipoMaterialGlobal:=TipoMaterial;
   self.QuantidadeEntrada:=Quantidade;
   Self.Peso:=Peso;
end;

procedure TFAcertoUnidadeEntrada.ValidaDados;
Var
  CodigoUnidadeMedidaEntrada:string;
  CodigoUnidadeMedidaCadastro:string;
  NomeUnidadeMedidaEntrada:string;
  NomeUnidadeMedidaCadastro:string;
begin
   panel2.Visible:=True;
   panel3.Visible:=False;
   HouveErro:=False;

   CodigoUnidadeMedidaEntrada:=get_campoTabela('codigo','sigla','tabunidademedida',UnidadeEntradaGlobal);
   CodigoUnidadeMedidaCadastro:=get_campoTabela('codigo','sigla','tabunidademedida',UnidadeMaterialGlobal);

   NomeUnidadeMedidaEntrada:=get_campoTabela('descricao','sigla','tabunidademedida',UnidadeEntradaGlobal);
   NomeUnidadeMedidaCadastro:=get_campoTabela('descricao','sigla','tabunidademedida',UnidadeMaterialGlobal);

   if(CodigoUnidadeMedidaCadastro='') then
   begin
      ImgErroUnidadeCadastro.visible:=True;
      lbErroUnidadeMedidaCadastro.Visible:=True;
      lbErroUnidadeMedidaEntrada.Caption:='Unidade de Medida do cadastro de '+TipoMaterialGlobal+' n�o est� cadastrada';

      ImgErroEquivalencia.visible:=True;
      lbErroEquivalencia.Visible:=True;
      lbErroEquivalencia.Caption:='Equivalencia entre as medidas n�o foi informado';

      HouveErro:=True;
   end;
   if(CodigoUnidadeMedidaEntrada='') then
   begin
      ImgErroUnidadeEntrada.Visible:=True;
      lbErroUnidadeMedidaEntrada.Visible:=True;
      lbErroUnidadeMedidaEntrada.Caption:='Unidade de Medida informada na entrada n�o est� cadastrada';

      ImgErroEquivalencia.visible:=True;
      lbErroEquivalencia.Visible:=True;
      lbErroEquivalencia.Caption:='Equivalencia entre as medidas n�o foi informado';

      HouveErro:=True;
   end;
   if(UnidadeEntradaGlobal='') then
   begin
      ImgErroUnidadeEntrada.Visible:=True;
      lbErroUnidadeMedidaEntrada.Visible:=True;
      lbErroUnidadeMedidaEntrada.Caption:='Unidade de Medida n�o informada na entrada' ;

      ImgErroEquivalencia.visible:=True;
      lbErroEquivalencia.Visible:=True;
      lbErroEquivalencia.Caption:='Equivalencia entre as medidas n�o foi informado';

      HouveErro:=True;
   end;
   if(UnidadeMaterialGlobal='') then
   begin
      ImgErroUnidadeCadastro.visible:=True;
      lbErroUnidadeMedidaCadastro.Visible:=True;
      lbErroUnidadeMedidaEntrada.Caption:='Unidade de Medida n�o informada no cadastro de '+TipoMaterialGlobal;

      ImgErroEquivalencia.visible:=True;
      lbErroEquivalencia.Visible:=True;
      lbErroEquivalencia.Caption:='Equivalencia entre as medidas n�o foi informado';
      
      HouveErro:=True;
   end;
   
   if(CodigoUnidadeMedidaCadastro<>'') and (CodigoUnidadeMedidaEntrada<>'') then
   begin
      //Convers�o de peso para outros

      if(Self.Peso<>'') then
      begin
        HouveErro:=False;
      end
      else
      begin
        if(ObjRelacaoMedidas.RetornaEquivalencia(CodigoUnidadeMedidaEntrada,CodigoUnidadeMedidaCadastro,TipoMaterialGlobal)=False) then
        begin
           HouveErro:=True;
           ImgErroEquivalencia.visible:=True;
           lbErroEquivalencia.Visible:=True;
           lbErroEquivalencia.Caption:='Equivalencia entre as medidas n�o foi informado';
        end;
        ObjRelacaoMedidas.TabelaparaObjeto;
        if(ObjRelacaoMedidas.Get_VALOR='')then
        begin
           HouveErro:=True;
           ImgErroEquivalencia.visible:=True;
           lbErroEquivalencia.Visible:=True;
           lbErroEquivalencia.Caption:='Valor equivalente entre '+UnidadeEntradaGlobal+' e '+UnidadeMaterialGlobal+' est� vazio';
        end;
      end;

   end;

   if(HouveErro=False) then
   begin
       panel2.Visible:=False;
       panel3.Visible:=True;
       lbDescricaQuantidadeEntrada.Caption:=QuantidadeEntrada+' '+UnidadeEntradaGlobal+' ('+NomeUnidadeMedidaEntrada+') de';
       if(TipoMaterialGlobal='Diverso')
       then lbNomeMaterial1.Caption:=get_campoTabela('descricao','codigo','tabdiverso',MaterialGlobal);
       if(TipoMaterialGlobal='Perfilado')
       then lbNomeMaterial1.Caption:=get_campoTabela('descricao','codigo','tabperfilado',MaterialGlobal);
       if(TipoMaterialGlobal='Persiana')
       then lbNomeMaterial1.Caption:=get_campoTabela('nome','codigo','tabpersiana',MaterialGlobal);
       if(TipoMaterialGlobal='Vidro')
       then lbNomeMaterial1.Caption:=get_campoTabela('descricao','codigo','tabvidro',MaterialGlobal);
       if(TipoMaterialGlobal='KitBox')
       then lbNomeMaterial1.Caption:=get_campoTabela('descricao','codigo','tabkitbox',MaterialGlobal);
       if(TipoMaterialGlobal='Ferragem')
       then lbNomeMaterial1.Caption:=get_campoTabela('descricao','codigo','tabferragem',MaterialGlobal);

       lbNomematerial2.Caption:=lbNomeMaterial1.Caption;
       lbValorEquivalencia.Caption:='Valor de equival�ncia entre '+UnidadeEntradaGlobal+' e '+UnidadeMaterialGlobal+'= '+ObjRelacaoMedidas.Get_VALOR;
       if(Self.Peso='')
       then QuantidadeConvertida:= StrToCurr(QuantidadeEntrada)*StrToCurr(ObjRelacaoMedidas.Get_VALOR)
       else QuantidadeConvertida:= StrToCurr(QuantidadeEntrada)/StrToCurr(Peso);

       lbDescricaoquantidadeConversao.caption:=CurrToStr(QuantidadeConvertida)+' '+UnidadeMaterialGlobal+' ('+NomeUnidadeMedidaCadastro+') de';
   end;

end;

procedure TFAcertoUnidadeEntrada.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    ObjRelacaoMedidas.Free;
end;

procedure TFAcertoUnidadeEntrada.btProcessarClick(Sender: TObject);
begin
      tag:=1;
      self.Close;
end;

procedure TFAcertoUnidadeEntrada.btCancelarClick(Sender: TObject);
begin
      Tag:=0;
      self.Close;
end;

procedure TFAcertoUnidadeEntrada.bt1Click(Sender: TObject);
begin
   ImgErroEquivalencia.visible:=False;
   chamaFormulario(TFAcertaUnidades,self);
   ValidaDados;
end;

end.
