object FmenuNfe: TFmenuNfe
  Left = 847
  Top = 251
  Width = 191
  Height = 215
  BiDiMode = bdLeftToRight
  Caption = 'Menu nota fiscal eletr'#244'nica'
  Color = 8539648
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton2: TSpeedButton
    Left = 0
    Top = 103
    Width = 172
    Height = 25
    Caption = 'Consulta pela chave'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = SpeedButton2Click
  end
  object SpeedButton1: TSpeedButton
    Left = 0
    Top = 78
    Width = 172
    Height = 25
    Caption = 'Consultar servi'#231'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = SpeedButton1Click
  end
  object SpeedButton4: TSpeedButton
    Left = 0
    Top = 128
    Width = 172
    Height = 22
    Caption = 'Cancela pela chave'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = SpeedButton4Click
  end
  object btRecuperaProtocolo: TSpeedButton
    Left = 1
    Top = 150
    Width = 172
    Height = 22
    Caption = 'Recupera protocolo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = btRecuperaProtocoloClick
  end
  object btn2: TBitBtn
    Left = 0
    Top = 28
    Width = 172
    Height = 25
    Caption = 'Ca&ncelar do arquivo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = btn2Click
  end
  object BitBtn2: TBitBtn
    Left = 0
    Top = 53
    Width = 172
    Height = 25
    Caption = 'Im&primir do arquivo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = BitBtn2Click
  end
  object BitBtn3: TBitBtn
    Left = 0
    Top = 2
    Width = 172
    Height = 25
    Caption = 'C&onsultar do arquivo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = BitBtn3Click
  end
  object OpenDialog1: TOpenDialog
    Left = 202
    Top = 101
  end
end
