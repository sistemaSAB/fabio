object FFinalizaComissaoColocador: TFFinalizaComissaoColocador
  Left = 221
  Top = 111
  Width = 1278
  Height = 854
  Caption = 
    'M'#243'dulo de Finaliza'#231#227'o de Pedidos Projetos (Comiss'#227'o de Colocador' +
    ')'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1262
    Height = 113
    Align = alTop
    BevelOuter = bvNone
    Color = 10643006
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 49
      Height = 14
      Caption = 'Colocador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 104
      Top = 16
      Width = 32
      Height = 14
      Caption = 'Pedido'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 198
      Top = 16
      Width = 68
      Height = 14
      Caption = 'Pedido/Projeto'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 294
      Top = 16
      Width = 164
      Height = 14
      Caption = 'Pedido Projeto / Pedido de Compra'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object edtcolocador: TEdit
      Left = 24
      Top = 32
      Width = 73
      Height = 19
      Color = 6073854
      TabOrder = 0
      OnDblClick = edtcolocadorDblClick
      OnKeyDown = edtcolocadorKeyDown
    end
    object edtpedido: TEdit
      Left = 104
      Top = 32
      Width = 89
      Height = 19
      Color = 6073854
      TabOrder = 1
      OnDblClick = edtpedidoDblClick
      OnKeyDown = edtpedidoKeyDown
    end
    object edtpedidoprojeto: TEdit
      Left = 198
      Top = 32
      Width = 91
      Height = 19
      Color = 6073854
      TabOrder = 2
      OnDblClick = edtpedidoprojetoDblClick
      OnKeyDown = edtpedidoprojetoKeyDown
    end
    object btfiltrar: TButton
      Left = 624
      Top = 31
      Width = 75
      Height = 25
      Caption = '&Filtrar'
      TabOrder = 3
      OnClick = btfiltrarClick
    end
    object edtpedidoprojeto_pedidocompra: TEdit
      Left = 294
      Top = 32
      Width = 171
      Height = 19
      Color = 6073854
      TabOrder = 4
      OnDblClick = edtpedidoprojeto_pedidocompraDblClick
      OnKeyDown = edtpedidoprojeto_pedidocompraKeyDown
    end
    object btselecionatodas: TButton
      Left = 8
      Top = 80
      Width = 97
      Height = 25
      Caption = 'Seleciona Todas'
      TabOrder = 5
      OnClick = btselecionatodasClick
    end
    object btprocessar: TButton
      Left = 107
      Top = 80
      Width = 97
      Height = 25
      Caption = '&Processar'
      TabOrder = 6
      OnClick = btprocessarClick
    end
    object RadioFinalizado: TRadioButton
      Left = 480
      Top = 22
      Width = 120
      Height = 17
      Caption = 'Somente &Finalizados'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
    end
    object RadioAberto: TRadioButton
      Left = 480
      Top = 40
      Width = 113
      Height = 17
      Caption = 'Somente em &Aberto'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      TabStop = True
    end
  end
  object StringGrid: TStringGrid
    Left = 0
    Top = 113
    Width = 1262
    Height = 703
    Align = alClient
    Ctl3D = False
    FixedCols = 0
    Options = [goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect]
    ParentCtl3D = False
    TabOrder = 1
    OnDblClick = StringGridDblClick
    OnKeyPress = StringGridKeyPress
    ColWidths = (
      64
      64
      64
      64
      64)
  end
end
