unit UobjVIDROSERVICOOS;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc;

Type
   TObjVIDROSERVICOOS=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_ORDEMDESERVICO(parametro: string);
                Function Get_ORDEMDESERVICO: string;
                Procedure Submit_VIDROOS(parametro: string);
                Function Get_VIDROOS: string;
                Procedure Submit_SERVICOOS(parametro: string);
                Function Get_SERVICOOS: string;
                Procedure Submit_DATAC(parametro: string);
                Function Get_DATAC: string;
                Procedure Submit_DATAM(parametro: string);
                Function Get_DATAM: string;
                Procedure Submit_USERC(parametro: string);
                Function Get_USERC: string;
                Procedure Submit_USERM(parametro: string);
                Function Get_USERM: string;
                Procedure Submit_FUNCIONARIO(parametro: string);
                Function Get_FUNCIONARIO: string;
                procedure ResgataVidroServicos(OrdemServ:string;Where:string);
                function Get_Concluido:string;
                procedure Submit_Concluido(parametro:string);
                procedure Submit_OrdemExecucao(parametro:String);
                Function Get_OrdemExecucao:string;
                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               ORDEMDESERVICO:string;
               VIDROOS:string;
               SERVICOOS:string;
               DATAC:string;
               DATAM:string;
               USERC:string;
               USERM:string;
               FUNCIONARIO:string;
               CONCLUIDO:string;
               ORDEMEXECUCAO:string;
//CODIFICA VARIAVEIS PRIVADAS

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls
//USES IMPLEMENTATION
, UMenuRelatorios;


{ TTabTitulo }


Function  TObjVIDROSERVICOOS.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.ORDEMDESERVICO:=fieldbyname('ORDEMDESERVICO').asstring;
        Self.VIDROOS:=fieldbyname('VIDROOS').asstring;
        Self.SERVICOOS:=fieldbyname('SERVICOOS').asstring;
        Self.FUNCIONARIO:=fieldbyname('FUNCIONARIO').asstring;
        self.CONCLUIDO:=fieldbyname('CONCLUIDO').AsString;
        self.ORDEMEXECUCAO:=fieldbyname('ordemexecucao').AsString;
        result:=True;
     End;
end;


Procedure TObjVIDROSERVICOOS.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('ORDEMDESERVICO').asstring:=Self.ORDEMDESERVICO;
        ParamByName('VIDROOS').asstring:=Self.VIDROOS;
        ParamByName('SERVICOOS').asstring:=Self.SERVICOOS;
        ParamByName('FUNCIONARIO').asstring:=Self.FUNCIONARIO;
        ParamByName('CONCLUIDO').AsString:=self.CONCLUIDO;
        ParamByName('ordemexecucao').AsString:=Self.ORDEMEXECUCAO;
//CODIFICA OBJETOPARATABELA
  End;
End;

//***********************************************************************

function TObjVIDROSERVICOOS.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  {if (Self.VerificaNumericos=False)
  Then Exit;   }

 { if (Self.VerificaData=False)
  Then Exit; }

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjVIDROSERVICOOS.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        ORDEMDESERVICO:='';
        VIDROOS:='';
        SERVICOOS:='';
        DATAC:='';
        DATAM:='';
        USERC:='';
        USERM:='';
        FUNCIONARIO:='';
        CONCLUIDO:='';
        ORDEMEXECUCAO:='';
//CODIFICA ZERARTABELA









     End;
end;

Function TObjVIDROSERVICOOS.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjVIDROSERVICOOS.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjVIDROSERVICOOS.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
     try
        Strtoint(Self.ORDEMDESERVICO);
     Except
           Mensagem:=mensagem+'/ORDEMDESERVICO';
     End;
     try
        Strtoint(Self.VIDROOS);
     Except
           Mensagem:=mensagem+'/VIDROOS';
     End;
     try
        Strtoint(Self.SERVICOOS);
     Except
           Mensagem:=mensagem+'/SERVICOOS';
     End;
     try
        Strtoint(Self.FUNCIONARIO);
     Except
           Mensagem:=mensagem+'/FUNCIONARIO';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjVIDROSERVICOOS.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodatetime(Self.DATAC);
     Except
           Mensagem:=mensagem+'/DATAC';
     End;
     try
        Strtodatetime(Self.DATAM);
     Except
           Mensagem:=mensagem+'/DATAM';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjVIDROSERVICOOS.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjVIDROSERVICOOS.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro VIDROSERVICOOS vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,ORDEMDESERVICO,VIDROOS,SERVICOOS,DATAC,DATAM,USERC');
           SQL.ADD(' ,USERM,FUNCIONARIO,CONCLUIDO,ORDEMEXECUCAO');
           SQL.ADD(' from  TABVIDROSERVICOOS');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjVIDROSERVICOOS.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjVIDROSERVICOOS.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjVIDROSERVICOOS.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        ObjqueryPesquisa :=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDataSource.DataSet:=Self.ObjqueryPesquisa;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABVIDROSERVICOOS(CODIGO,ORDEMDESERVICO');
                InsertSQL.add(' ,VIDROOS,SERVICOOS,DATAC,DATAM,USERC,USERM,FUNCIONARIO');
                InsertSQL.add(' ,CONCLUIDO,ORDEMEXECUCAO)');
                InsertSQL.add('values (:CODIGO,:ORDEMDESERVICO,:VIDROOS,:SERVICOOS');
                InsertSQL.add(' ,:DATAC,:DATAM,:USERC,:USERM,:FUNCIONARIO,:CONCLUIDO,:ORDEMEXECUCAO)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABVIDROSERVICOOS set CODIGO=:CODIGO,ORDEMDESERVICO=:ORDEMDESERVICO');
                ModifySQL.add(',VIDROOS=:VIDROOS,SERVICOOS=:SERVICOOS,DATAC=:DATAC');
                ModifySQL.add(',DATAM=:DATAM,USERC=:USERC,USERM=:USERM,FUNCIONARIO=:FUNCIONARIO,CONCLUIDO=:CONCLUIDO');
                ModifySQL.add(',ORDEMEXECUCAO=:ORDEMEXECUCAO');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABVIDROSERVICOOS where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjVIDROSERVICOOS.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjVIDROSERVICOOS.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabVIDROSERVICOOS');
     Result:=Self.ParametroPesquisa;
end;

function TObjVIDROSERVICOOS.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de VIDROSERVICOOS ';
end;


function TObjVIDROSERVICOOS.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENVIDROSERVICOOS,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENVIDROSERVICOOS,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjVIDROSERVICOOS.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Freeandnil(self.ObjqueryPesquisa);
    ObjDatasource.Free;
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjVIDROSERVICOOS.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjVIDROSERVICOOS.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjVIDROSERVICOOS.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjVIDROSERVICOOS.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjVIDROSERVICOOS.Submit_ORDEMDESERVICO(parametro: string);
begin
        Self.ORDEMDESERVICO:=Parametro;
end;
function TObjVIDROSERVICOOS.Get_ORDEMDESERVICO: string;
begin
        Result:=Self.ORDEMDESERVICO;
end;
procedure TObjVIDROSERVICOOS.Submit_VIDROOS(parametro: string);
begin
        Self.VIDROOS:=Parametro;
end;
function TObjVIDROSERVICOOS.Get_VIDROOS: string;
begin
        Result:=Self.VIDROOS;
end;
procedure TObjVIDROSERVICOOS.Submit_SERVICOOS(parametro: string);
begin
        Self.SERVICOOS:=Parametro;
end;
function TObjVIDROSERVICOOS.Get_SERVICOOS: string;
begin
        Result:=Self.SERVICOOS;
end;
procedure TObjVIDROSERVICOOS.Submit_DATAC(parametro: string);
begin
        Self.DATAC:=Parametro;
end;
function TObjVIDROSERVICOOS.Get_DATAC: string;
begin
        Result:=Self.DATAC;
end;
procedure TObjVIDROSERVICOOS.Submit_DATAM(parametro: string);
begin
        Self.DATAM:=Parametro;
end;
function TObjVIDROSERVICOOS.Get_DATAM: string;
begin
        Result:=Self.DATAM;
end;
procedure TObjVIDROSERVICOOS.Submit_USERC(parametro: string);
begin
        Self.USERC:=Parametro;
end;
function TObjVIDROSERVICOOS.Get_USERC: string;
begin
        Result:=Self.USERC;
end;
procedure TObjVIDROSERVICOOS.Submit_USERM(parametro: string);
begin
        Self.USERM:=Parametro;
end;
function TObjVIDROSERVICOOS.Get_USERM: string;
begin
        Result:=Self.USERM;
end;
procedure TObjVIDROSERVICOOS.Submit_FUNCIONARIO(parametro: string);
begin
        Self.FUNCIONARIO:=Parametro;
end;
function TObjVIDROSERVICOOS.Get_FUNCIONARIO: string;
begin
        Result:=Self.FUNCIONARIO;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjVIDROSERVICOOS.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJVIDROSERVICOOS';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;
      end;

end;

procedure TObjVIDROSERVICOOS.ResgataVidroServicos(OrdemServ:string;Where:string);
begin
      with Self.ObjqueryPesquisa do
      begin
            Close;
            sql.Clear;
            sql.add('select tvs.codigo as codigo,tabvidro.descricao as nomevidro,tvs.concluido, ss.descricao as nomeservico');
            sql.Add('from tabvidroservicoos  tvs');
            sql.Add('join tabmateriaisos vs on vs.codigo=tvs.vidroos');
            sql.Add('join tabservicosos  ss on ss.codigo=tvs.servicoos');
            SQL.Add('join tabvidro on tabvidro.codigo=vs.vidro');
            if(where='')
            then  sql.Add('where ordemdeservico='+OrdemServ)
            else
            begin
                SQL.Add(where);
                sql.Add('and ordemdeservico='+OrdemServ);
            end;
            sql.Add('order by tabvidro.descricao');
            if(OrdemServ='')
            then Exit;
            Open;

      end;
end;

procedure TObjVIDROSERVICOOS.Submit_Concluido(parametro:string);
begin
    Self.CONCLUIDO:=parametro;
end;

function TObjVIDROSERVICOOS.Get_Concluido:string;
begin
    Result:=Self.CONCLUIDO;
end;

procedure TObjVIDROSERVICOOS.Submit_OrdemExecucao(parametro:string);
begin
  ORDEMEXECUCAO:=parametro;
end;

function TObjVIDROSERVICOOS.Get_OrdemExecucao:string;
begin
  Result:=ORDEMEXECUCAO;
end;

end.



