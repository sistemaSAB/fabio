unit UPEDIDOCOMPRA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls,db, UObjpedidoprojetoPEDIDOCOMPRA,
  UessencialGlobal, Tabs, Grids, DBGrids,IBQuery,UFORNECEDOR, ComCtrls,UobjPEDIDOCOMPRAMATERIAISAVULSO,
  ImgList, jpeg, Menus,UItensCarrinho,UVisulizarProjetos,UMostraBarraProgresso;

type
  TFPEDIDOCOMPRA = class(TForm)
    pnl3: TPanel;
    cbbComboConcluido: TComboBox;
    lbLbConcluido: TLabel;
    edtData: TMaskEdit;
    lbLbData: TLabel;
    edtCodigoManual: TEdit;
    edtcodigomanual_2: TEdit;
    lb3: TLabel;
    edtcodigomanual_1: TEdit;
    edtFornecedor: TEdit;
    lbLbFornecedor: TLabel;
    lbNomeFornecedor: TLabel;
    lb4: TLabel;
    mmoObservacao: TMemo;
    pnlbotes: TPanel;
    lbcodigo: TLabel;
    lb5: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    imgrodape: TImage;
    lb14: TLabel;
    btAjuda: TSpeedButton;
    lbConcluido: TLabel;
    il1: TImageList;
    pgc1: TPageControl;
    ts1: TTabSheet;
    pnl2: TPanel;
    lb1: TLabel;
    lb2: TLabel;
    edtpedido: TEdit;
    edtpedidoprojeto: TEdit;
    btGravar1: TBitBtn;
    btcancelar1: TBitBtn;
    btxcluir: TBitBtn;
    DBGrid: TDBGrid;
    ts2: TTabSheet;
    dbgrid2: TDBGrid;
    pnl4: TPanel;
    lb9: TLabel;
    lb10: TLabel;
    lb8: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    lbNomeMaterial: TLabel;
    lb11: TLabel;
    lbcormaterial: TLabel;
    edtAltura: TEdit;
    edtLargura: TEdit;
    edtQuantidade: TEdit;
    btGravarAvulso: TBitBtn;
    btExcluirAvulso: TBitBtn;
    btCancelarAvulso: TBitBtn;
    COMBOTipomaterial: TComboBox;
    edtMaterial: TEdit;
    edtCor: TEdit;
    ts3: TTabSheet;
    panel2: TPanel;
    edtPedidoMateriaisporPedido: TEdit;
    lb12: TLabel;
    STRGItensPedido: TStringGrid;
    grpSIMNAO: TGroupBox;
    rbSIM: TRadioButton;
    rbNAo: TRadioButton;
    panel3: TPanel;
    img1: TImage;
    ilProdutos: TImageList;
    lbQuantidadeCarrinho: TLabel;
    PopUpCarrinho: TPopupMenu;
    N1LimparCarrinho1: TMenuItem;
    N2MostrarItensnoCarrinho1: TMenuItem;
    btExecutar: TSpeedButton;
    btUltimo: TSpeedButton;
    chkMostraSomenteItensFalta: TCheckBox;
    lb13: TLabel;
    ckCusto: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtFornecedorExit(Sender: TObject);
    procedure edtFornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure mmoObservacaoKeyPress(Sender: TObject; var Key: Char);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure edtpedidoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtpedidoprojetoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btBtGravarPPClick(Sender: TObject);
    procedure btexcluirppClick(Sender: TObject);
    procedure edtcodigomanual_1KeyPress(Sender: TObject; var Key: Char);
    procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure lbNomeFornecedorClick(Sender: TObject);
    procedure lbNomeFornecedorMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure lbNomeFornecedorMouseLeave(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
    procedure pgc1Change(Sender: TObject);
    procedure COMBOTipomaterialExit(Sender: TObject);
    procedure edtMaterialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btGravarAvulsoClick(Sender: TObject);
    procedure dbgrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edtLarguraExit(Sender: TObject);
    procedure edtAlturaExit(Sender: TObject);
    procedure btCancelarAvulsoClick(Sender: TObject);
    procedure btAjudaClick(Sender: TObject);
    procedure edtFornecedorDblClick(Sender: TObject);
    procedure edtpedidoDblClick(Sender: TObject);
    procedure edtpedidoprojetoDblClick(Sender: TObject);
    procedure edtMaterialDblClick(Sender: TObject);
    procedure edtCorDblClick(Sender: TObject);
    procedure edtPedidoMateriaisporPedidoExit(Sender: TObject);
    procedure STRGItensPedidoDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure STRGItensPedidoDblClick(Sender: TObject);
    procedure img1Click(Sender: TObject);
    procedure rbSIMClick(Sender: TObject);
    procedure rbNAoClick(Sender: TObject);
    procedure STRGItensPedidoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btUltimoClick(Sender: TObject);
    procedure btExecutarClick(Sender: TObject);
    procedure btExcluirAvulsoClick(Sender: TObject);
    procedure N1LimparCarrinho1Click(Sender: TObject);
    procedure N2MostrarItensnoCarrinho1Click(Sender: TObject);
    procedure edtDataKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lb13Click(Sender: TObject);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    procedure MostraQuantidadeCadastrada;
    procedure __CarregaStringGrid;
    procedure __LimpaStringGrid;
    procedure __Delete(Linha:Integer);
    procedure __GerarDesenhor(CodigoPedidoProjeto,codigopedido:string);

    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPEDIDOCOMPRA: TFPEDIDOCOMPRA;
  ObjpedidoProjetoPedidoCompra:TObjpedidoprojetopedidocompra;
  ObjPedidoCompraMateriaisAvulso:TObjPEDIDOCOMPRAMATERIAISAVULSO;

implementation

uses Upesquisa, UessencialLocal, UDataModulo, UescolheImagemBotao,
  UobjPEDIDOCOMPRA, Uprincipal, Math, UAjuda;

{$R *.dfm}


procedure TFPEDIDOCOMPRA.FormShow(Sender: TObject);
begin
      MostraQuantidadeCadastrada;
      FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
      FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
      //FescolheImagemBotao.PegaFiguraBotaopequeno(BtGravar1,'BOTAOINSERIR.BMP');
      //FescolheImagemBotao.PegaFiguraBotaopequeno(btCancelar1,'BOTAOCANCELARPRODUTO.BMP');
      //FescolheImagemBotao.PegaFiguraBotaopequeno(btxcluir,'BOTAORETIRAR.BMP');
      //FescolheImagemBotao.PegaFiguraBotaopequeno(BtGravaravulso,'BOTAOINSERIR.BMP');
      //FescolheImagemBotao.PegaFiguraBotaopequeno(btCancelaravulso,'BOTAOCANCELARPRODUTO.BMP');
      //FescolheImagemBotao.PegaFiguraBotaopequeno(btexcluiravulso,'BOTAORETIRAR.BMP');

      lbcodigo.caption:='';

      limpaedit(Self);
      Self.limpaLabels;
      desabilita_campos(Self);


      Try
         ObjpedidoProjetoPedidoCompra:=TObjpedidoProjetoPedidoCompra.create;
         ObjPedidoCompraMateriaisAvulso:=TObjPEDIDOCOMPRAMATERIAISAVULSO.Create;
         Self.DBGrid.DataSource:=ObjpedidoProjetoPedidoCompra.ObjDatasource;
         self.dbgrid2.DataSource:=ObjPedidoCompraMateriaisAvulso.ObjDatasource;
      Except
            Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
            Self.close;
      End;


      if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE PEDIDO DE COMPRA')=False)
      Then desab_botoes(Self)
      Else habilita_botoes(Self);

      pgc1.TabIndex:=0;
      ObjpedidoProjetoPedidoCompra.ResgataPedidoProjetos(lbcodigo.Caption);
     // Self.Color:=clBtnFace;
     ckCusto.Checked := True;
     ckCusto.Enabled := True;
end;



procedure TFPEDIDOCOMPRA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjpedidoProjetoPedidoCompra=Nil)
     Then exit;

     If (ObjpedidoProjetoPedidoCompra.pedidocompra.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjpedidoProjetoPedidoCompra.free;
    ObjPedidoCompraMateriaisAvulso.Free;

end;



procedure TFPEDIDOCOMPRA.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     //desab_botoes(Self);

     lbcodigo.Caption:='0';
     //edtcodigo.text:=ObjPEDIDOCOMPRA.Get_novocodigo;
     lbConcluido.Caption:='Aberto';
     limpaedit(pnl2);
     limpaedit(pnl4);
     limpaedit(pnl4);
     edtcodigomanual.enabled:=False;
           

     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

      btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

     ObjpedidoProjetoPedidoCompra.pedidocompra.status:=dsInsert;

     cbbComboConcluido.itemindex:=0;
     cbbComboConcluido.Enabled:=False;
     EdtCodigoManual_1.setfocus;
     ObjpedidoProjetoPedidoCompra.ResgataPedidoProjetos(lbcodigo.Caption);

     ckCusto.Checked := True;

end;

procedure TFPEDIDOCOMPRA.btSalvarClick(Sender: TObject);
begin

     If ObjpedidoProjetoPedidoCompra.pedidocompra.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjpedidoProjetoPedidoCompra.pedidocompra.salvar(true)=False)
     Then exit;

     lbcodigo.Caption:=ObjpedidoProjetoPedidoCompra.pedidocompra.Get_codigo;
     ObjpedidoProjetoPedidoCompra.pedidocompra.localizacodigo(lbcodigo.Caption);
     ObjpedidoProjetoPedidoCompra.pedidocompra.TabelaparaObjeto;
     Self.ObjetoParaControles;

     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;

     habilita_botoes(Self);
     MostraQuantidadeCadastrada;
     desabilita_campos(Self);
     edtpedido.Enabled:=True;
     edtpedidoprojeto.Enabled:=True;
     ckCusto.Enabled := True;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFPEDIDOCOMPRA.btAlterarClick(Sender: TObject);
begin
    If (ObjpedidoProjetoPedidoCompra.pedidocompra.Status=dsinactive) and (lbcodigo.Caption<>'')
    Then
    Begin
              habilita_campos(Self);

              EdtCodigomanual.enabled:=False;
              ObjpedidoProjetoPedidoCompra.pedidocompra.Status:=dsEdit;

              EdtCodigoManual_1.setfocus;
              btSalvar.enabled:=True;
              BtCancelar.enabled:=True;
              btpesquisar.enabled:=True;
              btnovo.Visible :=false;
              btalterar.Visible:=false;
              btpesquisar.Visible:=false;
              btrelatorio.Visible:=false;
              btexcluir.Visible:=false;
              btsair.Visible:=false;
              btopcoes.Visible :=false;
    End;

end;

procedure TFPEDIDOCOMPRA.btCancelarClick(Sender: TObject);
begin
     ObjpedidoProjetoPedidoCompra.pedidocompra.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible:=True;
     ckCusto.Checked := True;
end;

procedure TFPEDIDOCOMPRA.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjpedidoProjetoPedidoCompra.pedidocompra.Get_pesquisa,ObjpedidoProjetoPedidoCompra.pedidocompra.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjpedidoProjetoPedidoCompra.pedidocompra.status<>dsinactive
                                  then exit;

                                  If (ObjpedidoProjetoPedidoCompra.pedidocompra.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjpedidoProjetoPedidoCompra.pedidocompra.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                                       ObjpedidoProjetoPedidoCompra.ResgataPedidoProjetos(lbcodigo.Caption);
                                       habilita_campos(pnl2);
                                       habilita_campos(pnl4);
                                       habilita_campos(panel2);


                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFPEDIDOCOMPRA.btExcluirClick(Sender: TObject);
begin
     If (ObjpedidoProjetoPedidoCompra.pedidocompra.status<>dsinactive) or (lbcodigo.Caption='')
     Then exit;

     If (ObjpedidoProjetoPedidoCompra.pedidocompra.LocalizaCodigo(lbcodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjpedidoProjetoPedidoCompra.pedidocompra.exclui(lbcodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFPEDIDOCOMPRA.btRelatorioClick(Sender: TObject);
begin
    ObjpedidoProjetoPedidoCompra.Imprime(lbcodigo.Caption);
end;

procedure TFPEDIDOCOMPRA.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFPEDIDOCOMPRA.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFPEDIDOCOMPRA.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFPEDIDOCOMPRA.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
    if(key=vk_f1) then
    begin
        Fprincipal.chamaPesquisaMenu;
    end;
    
    If (Key = VK_F2) then
    begin

           FAjuda.PassaAjuda('PEDIDO DE COMPRA');
           FAjuda.ShowModal;
    end;
end;

procedure TFPEDIDOCOMPRA.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFPEDIDOCOMPRA.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjpedidoProjetoPedidoCompra.pedidocompra do
    Begin
          Submit_CODIGO(lbcodigo.Caption);
          Submit_CodigoManual_1(edtCodigoManual_1.text);
          Submit_CodigoManual_2(edtCodigoManual_2.text);
          Submit_Data(edtData.text);
          Fornecedor.Submit_codigo(edtFornecedor.text);
          //Submit_Concluido(Submit_ComboBox(cbbComboConcluido));

          if(lbConcluido.Caption='Conclu�do')
          then Submit_Concluido('S')
          else Submit_Concluido('N');

          Submit_observacao(mmoobservacao.text);

          result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFPEDIDOCOMPRA.LimpaLabels;
begin
//LIMPA LABELS
    Lbnomefornecedor.caption:='';
    lbcodigo.Caption:='';
end;

function TFPEDIDOCOMPRA.ObjetoParaControles: Boolean;
begin
  Try
     With ObjpedidoProjetoPedidoCompra.pedidocompra do
     Begin
        lbcodigo.Caption:=Get_CODIGO;
        EdtCodigoManual.text:=Get_CodigoManual;
        EdtCodigoManual_1.text:=Get_CodigoManual_1;
        EdtCodigoManual_2.text:=Get_CodigoManual_2;
        EdtData.text:=Get_Data;
        EdtFornecedor.text:=Fornecedor.Get_codigo;
        lbNomeFornecedor.Caption:=Fornecedor.Get_Fantasia;
         mmoObservacao.Text:=Get_Observacao;

        if (Get_Concluido='S')
        Then lbConcluido.Caption:='Conclu�do'
        Else  lbConcluido.Caption:='Aberto';

        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFPEDIDOCOMPRA.TabelaParaControles: Boolean;
begin
     If (ObjpedidoProjetoPedidoCompra.pedidocompra.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFPEDIDOCOMPRA.edtFornecedorExit(Sender: TObject);
begin
     ObjpedidoProjetoPedidoCompra.pedidocompra.EdtFornecedorExit(sender,LbNomeFornecedor);
end;

procedure TFPEDIDOCOMPRA.edtFornecedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjpedidoProjetoPedidoCompra.pedidocompra.EdtFornecedorKeyDown(sender,key,shift,LbNomeFornecedor);
end;

procedure TFPEDIDOCOMPRA.mmoObservacaoKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     then mmoobservacao.SetFocus;
end;

procedure TFPEDIDOCOMPRA.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     if (newtab=1)//Pedido
     Then Begin
               if (lbcodigo.Caption='') or (ObjpedidoProjetoPedidoCompra.PedidoCompra.Status=dsinsert)
               Then begin
                        AllowChange:=false;
                        exit;
               End;

               ObjpedidoProjetoPedidoCompra.ResgataPedidoProjetos(lbcodigo.Caption);
               edtpedido.Enabled:=true;
               edtpedidoprojeto.Enabled:=true;

               
     End;


end;


procedure TFPEDIDOCOMPRA.edtpedidoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjpedidoProjetoPedidoCompra.PedidoProjeto.EdtPedidoKeyDown(sender,Key,shift,nil);
end;

procedure TFPEDIDOCOMPRA.edtpedidoprojetoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      ObjpedidoProjetoPedidoCompra.edtpedidoprojetoKeyDown(sender,key,shift,nil,edtpedido.text);
end;

procedure TFPEDIDOCOMPRA.btBtGravarPPClick(Sender: TObject);
var
  qryloc:TIBQuery;
begin
     if(edtpedido.Text='')
     then Exit;

     if(edtpedido.Enabled=False)
     then Exit;

     With ObjpedidoProjetoPedidoCompra do
     begin
          if(edtpedidoprojeto.Text<>'') then
          begin
            ZerarTabela;
            Submit_CODIGO('0');
            PedidoCompra.Submit_CODIGO(lbcodigo.Caption);
            PedidoProjeto.Submit_Codigo(edtpedidoprojeto.text);
            PedidoProjeto.Pedido.Submit_Codigo(edtpedido.Text);
            Status:= dsinsert;

            edtpedido.SetFocus;

            if (salvar(true)=False)
            Then exit;

            __GerarDesenhor(edtpedidoprojeto.text,edtpedido.Text);

            edtpedidoprojeto.text:='';

            ObjpedidoProjetoPedidoCompra.ResgataPedidoProjetos(lbcodigo.Caption);
          end
          else
          begin

            qryloc:=TIBQuery.Create(nil);
            qryloc.Database:=FDataModulo.IBDatabase;

            qryloc.Close;
            qryloc.SQL.Clear;
            qryloc.SQL.Text:=
            'select codigo from tabpedido_proj where pedido='+edtpedido.Text;
            qryloc.Open;
            while not qryloc.Eof do
            begin
              if(get_campoTabela('codigo','pedidoprojeto','tabpedidoprojetopedidocompra',qryloc.Fields[0].AsString)='')then
              begin
                ZerarTabela;
                Submit_CODIGO('0');
                PedidoCompra.Submit_CODIGO(lbcodigo.Caption);
                PedidoProjeto.Submit_Codigo(qryloc.Fields[0].AsString);
                PedidoProjeto.Pedido.Submit_Codigo(edtpedido.Text);
                Status:= dsinsert;


                if (salvar(true)=False)
                Then exit;

                __GerarDesenhor(qryloc.Fields[0].AsString,edtpedido.Text);
              end;
               qryloc.Next;
            end;
            edtpedidoprojeto.text:='';

            ObjpedidoProjetoPedidoCompra.ResgataPedidoProjetos(lbcodigo.Caption);
            FreeAndNil(qryloc);
          end;
     End;
end;

procedure TFPEDIDOCOMPRA.btexcluirppClick(Sender: TObject);
begin
     if (DBGrid.DataSource.DataSet.Active=false)
     then exit;

     if (DBGrid.DataSource.DataSet.recordcount=0)
     then exit;

     if (MensagemPergunta('Certeza que deseja excluir?')=mrno)
     then exit;


     if (ObjpedidoProjetoPedidoCompra.LocalizaCodigo(DBGrid.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     then Begin
               mensagemerro('N�o localizado');
               exit;
     End;

     if (ObjpedidoProjetoPedidoCompra.Exclui(DBGrid.DataSource.DataSet.fieldbyname('codigo').asstring,true)=False)
     then exit;

     ObjpedidoProjetoPedidoCompra.ResgataPedidoProjetos(lbcodigo.Caption);
end;

procedure TFPEDIDOCOMPRA.edtcodigomanual_1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then key:=#0;
end;

procedure TFPEDIDOCOMPRA.MostraQuantidadeCadastrada;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabpedidocompra');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb14.Caption:='Existem '+IntToStr(contador)+' pedidos de compra cadastrados'
       else
       begin
            lb14.Caption:='Existe '+IntToStr(contador)+' pedido de compra cadastrado';
       end;

    finally

    end;


end;

procedure TFPEDIDOCOMPRA.DBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With DBGRID.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(DBGRID.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                DBGRID.DefaultDrawDataCell(Rect,DBGRID.Columns[DataCol].Field, State);
          End;
end;

procedure TFPEDIDOCOMPRA.lbNomeFornecedorClick(Sender: TObject);
var
  Fornecedor:TFFORNECEDOR;
begin
       try
          Fornecedor:=TFFORNECEDOR.Create(nil);
       except

       end;

       try
          if(edtFornecedor.Text='')
          then Exit;

          Fornecedor.Tag:=StrToInt(edtFornecedor.Text);
          Fornecedor.ShowModal;

       finally
           FreeAndNil(Fornecedor);
       end;


end;

procedure TFPEDIDOCOMPRA.lbNomeFornecedorMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFPEDIDOCOMPRA.lbNomeFornecedorMouseLeave(Sender: TObject);
begin
     TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFPEDIDOCOMPRA.btopcoesClick(Sender: TObject);
begin
     ObjpedidoProjetoPedidoCompra.Opcoes(lbcodigo.Caption);
end;

procedure TFPEDIDOCOMPRA.pgc1Change(Sender: TObject);
begin
  if (pgc1.TabIndex=1)then
  begin
    if(lbcodigo.Caption='')
    then Exit;

    habilita_campos(pnl4);

    lb9.Visible:=False;
    lb10.Visible:=False;
    edtAltura.Visible:=False;
    edtLargura.Visible:= False;
    COMBOTipomaterial.Items.Clear;
    COMBOTipomaterial.Items.Add('VIDRO');
    COMBOTipomaterial.Items.Add('FERRAGEM');
    COMBOTipomaterial.Items.Add('PERFILADO');
    COMBOTipomaterial.Items.Add('PERSIANA');
    COMBOTipomaterial.Items.Add('DIVERSO');
    COMBOTipomaterial.Items.Add('KIT BOX'); //removi 18/11 - celio, mesmo exibindo n�o inseria no pedido de compra
    ObjPedidoCompraMateriaisAvulso.CarregaMateriais(lbcodigo.Caption);
  end;
  //Itens por pedido
  if (pgc1.TabIndex=2)then
  begin
    if(lbcodigo.Caption<>'')
    then habilita_campos(panel2)
    else desabilita_campos(panel2);

    rbSIM.Enabled:=True;
    rbNAo.Enabled:=True;

    edtPedidoMateriaisporPedido.Text:='';
    __LimpaStringGrid;
    __CarregaStringGrid;

    FItensCarrinhos.__LimpaStringGrid;
    FItensCarrinhos.__MontaStringGrid;
    lbQuantidadeCarrinho.Caption:='0';
  end;
end;

procedure TFPEDIDOCOMPRA.COMBOTipomaterialExit(Sender: TObject);
begin
      if(COMBOTipomaterial.Text='VIDRO')then
      begin
           lb9.Visible:=True;
           lb10.Visible:=True;
           edtAltura.Visible:=True;
           edtLargura.Visible:= True;
           edtQuantidade.Enabled:=False;

      end
      else
      begin
           lb9.Visible:=false;
           lb10.Visible:=false;
           edtAltura.Visible:=false;
           edtLargura.Visible:= false;
           edtQuantidade.Enabled:=True;

      end;
      edtMaterial.text:='';
      edtCor.Text:='';
      edtAltura.Text:='';
      edtLargura.Text:='';
      edtQuantidade.text:='';
end;

procedure TFPEDIDOCOMPRA.edtMaterialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   sql:string;
begin
     if(COMBOTipomaterial.text='VIDRO')
     then sql:='select * from tabvidro';
     if(COMBOTipomaterial.text='FERRAGEM')
     then sql:='select * from tabferragem';
     if(COMBOTipomaterial.text='KIT BOX')
     then sql:='select * from tabkitbox';
     if(COMBOTipomaterial.text='PERFILADO')
     then sql:='select * from tabperfilado';
     if(COMBOTipomaterial.text='DIVERSO')
     then sql:='select * from tabdiverso';
     if(COMBOTipomaterial.text='PERSIANA')
     then sql:='select * from tabpersiana';

     If (key <>vk_f9)
     Then exit;

     Try
              Fpesquisalocal:=Tfpesquisa.create(Nil);
              If (FpesquisaLocal.PreparaPesquisa(sql,'',nil)=True)
              Then Begin
                        Try
                          If (FpesquisaLocal.showmodal=mrok) Then
                          Begin
                                   edtMaterial.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                   lbNomeMaterial.Caption:=FpesquisaLocal.querypesq.fieldbyname('descricao').AsString;


                          End;
                        Finally
                               FpesquisaLocal.QueryPesq.close;
                        End;
              End;

     Finally
             FreeandNil(FPesquisaLocal);

     End;

end;

procedure TFPEDIDOCOMPRA.edtCorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   sql:string;
begin
     if(edtMaterial.Text='')
     then Exit;

     if(COMBOTipomaterial.text='VIDRO')
     then sql:='select tabcor.descricao,tabvidrocor.* from tabvidrocor join tabcor on tabcor.codigo=tabvidrocor.cor where vidro='+edtMaterial.Text;
     if(COMBOTipomaterial.text='FERRAGEM')
     then sql:='select tabcor.descricao,tabferragemcor.* from tabferragemcor join tabcor on tabcor.codigo=tabferragemcor.cor where ferragem='+edtMaterial.Text;
     if(COMBOTipomaterial.text='KIT BOX')
     then sql:='select tabcor.descricao,tabkitboxcor.* from tabkitboxcor join tabcor on tabcor.codigo=tabkitboxcor.cor where kitbox='+edtMaterial.Text;
     if(COMBOTipomaterial.text='PERFILADO')
     then sql:='select tabcor.descricao,tabperfiladocor.* from tabperfiladocor join tabcor on tabcor.codigo=tabperfiladocor.cor where perfilado='+edtMaterial.Text;
     if(COMBOTipomaterial.text='DIVERSO')
     then sql:='select tabcor.descricao,tabdiversocor.* from tabdiversocor join tabcor on tabcor.codigo=tabdiversocor.cor where diverso='+edtMaterial.Text;
     if(COMBOTipomaterial.text='PERSIANA')
     then sql:='select tabcor.descricao,tabpersianagrupodiametrocor.* from tabpersianagrupodiametrocor join tabcor on tabcor.codigo=tabpersianagrupodiametrocor.cor where persiana='+edtMaterial.Text;

     If (key <>vk_f9)
     Then exit;

     Try
              Fpesquisalocal:=Tfpesquisa.create(Nil);
              If (FpesquisaLocal.PreparaPesquisa(sql,'',nil)=True)
              Then Begin
                        Try
                          If (FpesquisaLocal.showmodal=mrok) Then
                          Begin
                                   edtCor.text:=FpesquisaLocal.QueryPesq.fieldbyname('Codigo').asstring;
                                   lbcorMaterial.Caption:=FpesquisaLocal.querypesq.fieldbyname('descricao').AsString;


                          End;
                        Finally
                               FpesquisaLocal.QueryPesq.close;
                        End;
              End;

     Finally
             FreeandNil(FPesquisaLocal);

     End;

end;


procedure TFPEDIDOCOMPRA.btGravarAvulsoClick(Sender: TObject);
begin
        if(edtCor.Text='')
        then Exit;

        with ObjPedidoCompraMateriaisAvulso do
        begin
               ZerarTabela;
               Status:=dsInsert;
               Submit_CODIGO('0');
               if(COMBOTipomaterial.text='VIDRO')then
               begin
                     VIDROCOR.Submit_Codigo(edtCor.Text);
                     Submit_TipoMaterial('VIDRO');
               end ;
               if(COMBOTipomaterial.text='FERRAGEM')then
               begin
                    FERRAGEMCOR.Submit_Codigo(edtCor.Text);
                    Submit_TipoMaterial('FERRAGEM');
               end;
               if(COMBOTipomaterial.text='KIT BOX')then
               begin
                     KITBOXCOR.Submit_Codigo(edtCor.Text);
                     Submit_TipoMaterial('KIT BOX');
               end;
               if(COMBOTipomaterial.text='PERFILADO')then
               begin
                     PERFILADOCOR.Submit_Codigo(edtCor.Text);
                     Submit_TipoMaterial('PERFILADO');
               end;
               if(COMBOTipomaterial.text='DIVERSO')then
               begin
                     DIVERSOCOR.Submit_Codigo(edtCor.Text);
                     Submit_TipoMaterial('DIVERSO');
               end;
               if(COMBOTipomaterial.text='PERSIANA')then
               begin
                     PERSIANACOR.Submit_Codigo(edtCor.Text);
                     Submit_TipoMaterial('PERSIANA');
               end;
               Submit_QUANTIDADE(edtQuantidade.Text);
               Submit_ALTURA(edtAltura.Text);
               Submit_LARGURA(edtLargura.Text);
               PEDIDOCOMPRA.Submit_CODIGO(lbcodigo.Caption);

               if(Salvar(true)=False) then
               begin
                    MensagemErro('Erro ao tentar cadastrar novo material');
                    Exit;
               end;
               COMBOTipomaterial.text:='';
               edtMaterial.text:='';
               edtCor.Text:='';
               edtAltura.Text:='';
               edtLargura.Text:='';
               edtQuantidade.text:='';
               ObjPedidoCompraMateriaisAvulso.CarregaMateriais(lbcodigo.Caption);

        end;

end;

procedure TFPEDIDOCOMPRA.dbgrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With dbgrid2.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(dbgrid2.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                dbgrid2.DefaultDrawDataCell(Rect,dbgrid2.Columns[DataCol].Field, State);
          End;
end;

procedure TFPEDIDOCOMPRA.edtLarguraExit(Sender: TObject);
var
pquantidade,paltura,plargura,plarguraarredondamento,palturaarredondamento:Currency;
begin
   if (EdtAltura.Text <> '') and (EdtLargura.Text <> '') then
   Begin
        Try
           paltura:=strtocurr(tira_ponto(EdtAltura.Text));
        Except
              paltura:=0;
        End;

        Try
           plargura:=strtocurr(tira_ponto(Edtlargura.Text));
        Except
            plargura:=0;
        End;

        FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
        FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);

        pquantidade:=((plargura+plarguraarredondamento)*(paltura+palturaarredondamento))/1000000;
        EdtQuantidade.Text:=formata_valor(CurrToStr(pquantidade));
   end
   Else EdtQuantidade.Text:='0';

end;

procedure TFPEDIDOCOMPRA.edtAlturaExit(Sender: TObject);
var
pquantidade,paltura,plargura,plarguraarredondamento,palturaarredondamento:Currency;
begin
   if (EdtAltura.Text <> '') and (EdtLargura.Text <> '') then
   Begin
        Try
           paltura:=strtocurr(tira_ponto(EdtAltura.Text));
        Except
              paltura:=0;
        End;

        Try
           plargura:=strtocurr(tira_ponto(Edtlargura.Text));
        Except
            plargura:=0;
        End;

        FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
        FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);

        pquantidade:=((plargura+plarguraarredondamento)*(paltura+palturaarredondamento))/1000000;
        EdtQuantidade.Text:=formata_valor(CurrToStr(pquantidade));
   end
   Else EdtQuantidade.Text:='0';

end;

procedure TFPEDIDOCOMPRA.btCancelarAvulsoClick(Sender: TObject);
begin
     COMBOTipomaterial.text:='';
     edtMaterial.text:='';
     edtCor.Text:='';
     edtAltura.Text:='';
     edtLargura.Text:='';
     edtQuantidade.text:='';
     ObjPedidoCompraMateriaisAvulso.Status:=dsInactive;
end;

procedure TFPEDIDOCOMPRA.btAjudaClick(Sender: TObject);
begin
      FAjuda.PassaAjuda('PEDIDO DE COMPRA');
      FAjuda.ShowModal;
end;

{r4mr}
procedure TFPEDIDOCOMPRA.edtFornecedorDblClick(Sender: TObject);
var
  Key: Word;
  Shift: TShiftState;
begin
    key:= VK_F9;
    edtFornecedorKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFPEDIDOCOMPRA.edtpedidoDblClick(Sender: TObject);
var
  Key: Word;
  Shift: TShiftState;
begin
  key:= VK_F9;
  edtpedidoKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFPEDIDOCOMPRA.edtpedidoprojetoDblClick(Sender: TObject);
var
  Key: Word;
  Shift: TShiftState;
begin
  key:= VK_F9;
  edtpedidoprojetoKeyDown (Sender, Key, Shift);
end;

{r4mr}
procedure TFPEDIDOCOMPRA.edtMaterialDblClick(Sender: TObject);
var
  Key: Word;
  Shift: TShiftState;
begin
  key:= VK_F9;
  edtMaterialKeyDown (Sender, Key, Shift);
end;

{r4mr}
procedure TFPEDIDOCOMPRA.edtCorDblClick(Sender: TObject);
var
  Key: Word;
  Shift: TShiftState;
begin
  key:= VK_F9;
  edtCorKeyDown (Sender, Key, Shift);

end;

procedure TFPEDIDOCOMPRA.edtPedidoMateriaisporPedidoExit(Sender: TObject);
begin
  rbSIM.Checked:=True;
  __CarregaStringGrid;
end;

procedure TFPEDIDOCOMPRA.__CarregaStringGrid;
var
  Qry_BuscaItensPedido_QUERY:TIBQuery;
  Qry_BuscaItensEstoque_QUERY:TIBQuery;
begin
  __LimpaStringGrid;

  STRGItensPedido.ColCount:=8;
  STRGItensPedido.RowCount:=2;
  STRGItensPedido.FixedRows:=1;
  STRGItensPedido.ColWidths[0]:=50;
  STRGItensPedido.ColWidths[1]:=0;
  STRGItensPedido.ColWidths[2]:=100;
  STRGItensPedido.ColWidths[3]:=500;
  STRGItensPedido.ColWidths[4]:=0;
  STRGItensPedido.ColWidths[5]:=120;
  STRGItensPedido.ColWidths[6]:=120;
  STRGItensPedido.ColWidths[7]:=0;

  STRGItensPedido.Cells[0,0]:='';
  STRGItensPedido.Cells[1,0]:='Pedido';
  STRGItensPedido.Cells[2,0]:='TIPO';
  STRGItensPedido.Cells[3,0]:='�TEM';
  STRGItensPedido.Cells[4,0]:='Ped/Projeto';
  STRGItensPedido.Cells[5,0]:='Quantidade Vendida';
  STRGItensPedido.Cells[6,0]:='Quantidade Estoque';
  STRGItensPedido.Cells[7,0]:='ItemCor';

  if(edtPedidoMateriaisporPedido.Text='')
  then Exit;

  try
    Qry_BuscaItensPedido_QUERY:=TIBQuery.Create(nil);
    Qry_BuscaItensPedido_QUERY.Database:=FDataModulo.IBDatabase;
    Qry_BuscaItensEstoque_QUERY:=TIBQuery.Create(nil);
    Qry_BuscaItensEstoque_QUERY.Database:=FDataModulo.IBDatabase;
  except
    Exit;
  end;

  try
    //Ferragens
    Qry_BuscaItensPedido_QUERY.Close;
    Qry_BuscaItensPedido_QUERY.SQL.Clear;
    Qry_BuscaItensPedido_QUERY.SQL.Text:=
    'Select pp.pedido,f.descricao||'' - ''||cor.descricao,'''' as pprojeto,fcor.codigo,sum(fpp.quantidade) '+
    'from tabferragem_pp fpp '+
    'join tabferragemcor fcor on fcor.codigo=fpp.ferragemcor '+
    'join tabferragem f on f.codigo=fcor.ferragem '+
    'join tabcor cor on cor.codigo=fcor.cor '+
    'join tabpedido_proj pp on pp.codigo=fpp.pedidoprojeto '+
    'where pp.pedido='+edtPedidoMateriaisporPedido.Text +' '+
    'and '+
    'NOT EXISTS ( '+
    '  SELECT CODIGO  '+
    '  FROM tabpedidocompramateriaisavulso '+
    '   WHERE tabpedidocompramateriaisavulso.pedido = PP.pedido '+
    '  AND tabpedidocompramateriaisavulso.ferragemcor=FCOR.codigo '+
    ') '+
    'group by 1,2,3,4';

    Qry_BuscaItensPedido_QUERY.Open;

    while not Qry_BuscaItensPedido_QUERY.Eof do
    begin
      Qry_BuscaItensEstoque_QUERY.Close;
      Qry_BuscaItensEstoque_QUERY.SQL.Clear;
      Qry_BuscaItensEstoque_QUERY.SQL.Text:=
      'Select coalesce(sum(quantidade),0) as estoque from tabestoque where ferragemcor='+Qry_BuscaItensPedido_QUERY.Fields[3].AsString;
      Qry_BuscaItensEstoque_QUERY.Open;

      if(FItensCarrinhos.___LocalizaItemCarrinho(Qry_BuscaItensPedido_QUERY.Fields[0].AsString,Qry_BuscaItensPedido_QUERY.Fields[2].AsString,Qry_BuscaItensPedido_QUERY.Fields[3].AsString,'FERRAGEM')=False) then
      begin
        if(rbSIM.Checked=True) then
        begin

          if((Qry_BuscaItensEstoque_QUERY.Fields[0].AsCurrency-Qry_BuscaItensPedido_QUERY.Fields[4].AsCurrency){quantidade}<0) then
          begin
            STRGItensPedido.Cells[1,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[0].AsString; //PEDIDO
            STRGItensPedido.Cells[2,STRGItensPedido.RowCount-1]:='FERRAGEM';
            STRGItensPedido.Cells[3,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[1].AsString;//DESCRICAO ITEM
            STRGItensPedido.Cells[4,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[2].AsString;//PEDIDOPROJETO
            STRGItensPedido.Cells[5,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[4].AsString;//Quantidade Vendida
            STRGItensPedido.Cells[6,STRGItensPedido.RowCount-1]:=Qry_BuscaItensEstoque_QUERY.Fields[0].AsString;//Quantidade Vendida
            STRGItensPedido.Cells[7,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[3].AsString;//ItemCOR

            STRGItensPedido.RowCount:=STRGItensPedido.RowCount+1;
          end;
        end
        else
        begin
          STRGItensPedido.Cells[1,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[0].AsString; //PEDIDO
          STRGItensPedido.Cells[2,STRGItensPedido.RowCount-1]:='FERRAGEM';
          STRGItensPedido.Cells[3,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[1].AsString;//DESCRICAO ITEM
          STRGItensPedido.Cells[4,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[2].AsString;//PEDIDOPROJETO
          STRGItensPedido.Cells[5,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[4].AsString;//Quantidade Vendida
          STRGItensPedido.Cells[6,STRGItensPedido.RowCount-1]:=Qry_BuscaItensEstoque_QUERY.Fields[0].AsString;//Quantidade Vendida
          STRGItensPedido.Cells[7,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[3].AsString;//ItemCOR

          STRGItensPedido.RowCount:=STRGItensPedido.RowCount+1;
        end;
      end;
      Qry_BuscaItensPedido_QUERY.Next;
    end;

    //Vidro
    Qry_BuscaItensPedido_QUERY.Close;
    Qry_BuscaItensPedido_QUERY.SQL.Clear;
    Qry_BuscaItensPedido_QUERY.SQL.Text:=
    'Select pp.pedido,v.descricao||'' - ''||cor.descricao,vcor.codigo,sum(vpp.quantidade) '+
    'from tabvidro_pp vpp '+
    'join tabvidrocor vcor on vcor.codigo=vpp.vidrocor '+
    'join tabvidro v on v.codigo=vcor.vidro '+
    'join tabcor cor on cor.codigo=vcor.cor '+
    'join tabpedido_proj pp on pp.codigo=vpp.pedidoprojeto '+
    'where pp.pedido='+edtPedidoMateriaisporPedido.Text +' '+
    'and '+
    'NOT EXISTS ( '+
    '  SELECT CODIGO  '+
    '  FROM tabpedidocompramateriaisavulso '+
    '   WHERE tabpedidocompramateriaisavulso.pedido = PP.pedido '+
    '  AND tabpedidocompramateriaisavulso.vidrocor=vCOR.codigo '+
    ') '+
    'group by 1,2,3 ';
    Qry_BuscaItensPedido_QUERY.Open;

    while not Qry_BuscaItensPedido_QUERY.Eof do
    begin
      Qry_BuscaItensEstoque_QUERY.Close;
      Qry_BuscaItensEstoque_QUERY.SQL.Clear;
      Qry_BuscaItensEstoque_QUERY.SQL.Text:=
      'Select coalesce(sum(quantidade),0) as estoque from tabestoque where vidrocor='+Qry_BuscaItensPedido_QUERY.Fields[2].AsString;
      Qry_BuscaItensEstoque_QUERY.Open;

      if(FItensCarrinhos.___LocalizaItemCarrinho(Qry_BuscaItensPedido_QUERY.Fields[0].AsString,'',Qry_BuscaItensPedido_QUERY.Fields[2].AsString,'VIDRO')=False) then
      begin
        if(rbSIM.Checked=True) then
        begin

          if((Qry_BuscaItensEstoque_QUERY.Fields[0].AsCurrency-Qry_BuscaItensPedido_QUERY.Fields[3].AsCurrency){quantidade}<0) then
          begin
            STRGItensPedido.Cells[1,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[0].AsString; //PEDIDO
            STRGItensPedido.Cells[2,STRGItensPedido.RowCount-1]:='VIDRO';
            STRGItensPedido.Cells[3,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[1].AsString;//DESCRICAO ITEM
            STRGItensPedido.Cells[4,STRGItensPedido.RowCount-1]:='' ;
            STRGItensPedido.Cells[5,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[3].AsString;//Quantidade Vendida
            STRGItensPedido.Cells[6,STRGItensPedido.RowCount-1]:=Qry_BuscaItensEstoque_QUERY.Fields[0].AsString;//Quantidade Vendida
            STRGItensPedido.Cells[7,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[2].AsString;//ItemCOR

            STRGItensPedido.RowCount:=STRGItensPedido.RowCount+1;
          end;
        end
        else
        begin
          STRGItensPedido.Cells[1,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[0].AsString; //PEDIDO
          STRGItensPedido.Cells[2,STRGItensPedido.RowCount-1]:='VIDRO';
          STRGItensPedido.Cells[3,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[1].AsString;//DESCRICAO ITEM
          STRGItensPedido.Cells[4,STRGItensPedido.RowCount-1]:='';
          STRGItensPedido.Cells[5,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[3].AsString;//Quantidade Vendida
          STRGItensPedido.Cells[6,STRGItensPedido.RowCount-1]:=Qry_BuscaItensEstoque_QUERY.Fields[0].AsString;//Quantidade Vendida
          STRGItensPedido.Cells[7,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[2].AsString;//ItemCOR

          STRGItensPedido.RowCount:=STRGItensPedido.RowCount+1;
        end;
      end;
      Qry_BuscaItensPedido_QUERY.Next;
    end;

    //Perfilado
    Qry_BuscaItensPedido_QUERY.Close;
    Qry_BuscaItensPedido_QUERY.SQL.Clear;
    Qry_BuscaItensPedido_QUERY.SQL.Text:=
    'Select pp.pedido,p.descricao||'' - ''||cor.descricao,pcor.codigo,sum(ppp.quantidade) '+
    'from tabperfilado_pp ppp '+
    'join tabperfiladocor pcor on pcor.codigo=ppp.perfiladocor '+
    'join tabperfilado p on p.codigo=pcor.perfilado '+
    'join tabcor cor on cor.codigo=pcor.cor '+
    'join tabpedido_proj pp on pp.codigo=ppp.pedidoprojeto '+
    'where pp.pedido='+edtPedidoMateriaisporPedido.Text +' '+
    'and '+
    'NOT EXISTS ( '+
    '  SELECT CODIGO  '+
    '  FROM tabpedidocompramateriaisavulso '+
    '   WHERE tabpedidocompramateriaisavulso.pedido = PP.pedido '+
    '  AND tabpedidocompramateriaisavulso.perfiladocor=pCOR.codigo '+
    ') '+
    'group by 1,2,3 ';
    Qry_BuscaItensPedido_QUERY.Open;

    while not Qry_BuscaItensPedido_QUERY.Eof do
    begin
      Qry_BuscaItensEstoque_QUERY.Close;
      Qry_BuscaItensEstoque_QUERY.SQL.Clear;
      Qry_BuscaItensEstoque_QUERY.SQL.Text:=
      'Select coalesce(sum(quantidade),0) as estoque from tabestoque where perfiladocor='+Qry_BuscaItensPedido_QUERY.Fields[2].AsString;
      Qry_BuscaItensEstoque_QUERY.Open;

      if(FItensCarrinhos.___LocalizaItemCarrinho(Qry_BuscaItensPedido_QUERY.Fields[0].AsString,'',Qry_BuscaItensPedido_QUERY.Fields[2].AsString,'PERFILADO')=False) then
      begin
        if(rbSIM.Checked=True) then
        begin

          if((Qry_BuscaItensEstoque_QUERY.Fields[0].AsCurrency-Qry_BuscaItensPedido_QUERY.Fields[3].AsCurrency){quantidade}<0) then
          begin
            STRGItensPedido.Cells[1,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[0].AsString; //PEDIDO
            STRGItensPedido.Cells[2,STRGItensPedido.RowCount-1]:='PERFILADO';
            STRGItensPedido.Cells[3,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[1].AsString;//DESCRICAO ITEM
            STRGItensPedido.Cells[4,STRGItensPedido.RowCount-1]:='' ;
            STRGItensPedido.Cells[5,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[3].AsString;//Quantidade Vendida
            STRGItensPedido.Cells[6,STRGItensPedido.RowCount-1]:=Qry_BuscaItensEstoque_QUERY.Fields[0].AsString;//Quantidade Vendida
            STRGItensPedido.Cells[7,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[2].AsString;//ItemCOR

            STRGItensPedido.RowCount:=STRGItensPedido.RowCount+1;
          end;
        end
        else
        begin
          STRGItensPedido.Cells[1,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[0].AsString; //PEDIDO
          STRGItensPedido.Cells[2,STRGItensPedido.RowCount-1]:='PERFILADO';
          STRGItensPedido.Cells[3,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[1].AsString;//DESCRICAO ITEM
          STRGItensPedido.Cells[4,STRGItensPedido.RowCount-1]:='';
          STRGItensPedido.Cells[5,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[3].AsString;//Quantidade Vendida
          STRGItensPedido.Cells[6,STRGItensPedido.RowCount-1]:=Qry_BuscaItensEstoque_QUERY.Fields[0].AsString;//Quantidade Vendida
          STRGItensPedido.Cells[7,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[2].AsString;//ItemCOR

          STRGItensPedido.RowCount:=STRGItensPedido.RowCount+1;
        end;
      end;
      Qry_BuscaItensPedido_QUERY.Next;
    end;

    //Diverso
    Qry_BuscaItensPedido_QUERY.Close;
    Qry_BuscaItensPedido_QUERY.SQL.Clear;
    Qry_BuscaItensPedido_QUERY.SQL.Text:=
    'Select pp.pedido,p.descricao||'' - ''||cor.descricao,pcor.codigo,sum(ppp.quantidade) '+
    'from tabDIVERSO_pp ppp '+
    'join tabDIVERSOcor pcor on pcor.codigo=ppp.DIVERSOcor '+
    'join tabDIVERSO p on p.codigo=pcor.DIVERSO '+
    'join tabcor cor on cor.codigo=pcor.cor '+
    'join tabpedido_proj pp on pp.codigo=ppp.pedidoprojeto '+
    'where pp.pedido='+edtPedidoMateriaisporPedido.Text +' '+
    'and '+
    'NOT EXISTS ( '+
    '  SELECT CODIGO  '+
    '  FROM tabpedidocompramateriaisavulso '+
    '   WHERE tabpedidocompramateriaisavulso.pedido = PP.pedido '+
    '  AND tabpedidocompramateriaisavulso.DIVERSOcor=pCOR.codigo '+
    ') '+
    'group by 1,2,3 ';
    Qry_BuscaItensPedido_QUERY.Open;

    while not Qry_BuscaItensPedido_QUERY.Eof do
    begin
      Qry_BuscaItensEstoque_QUERY.Close;
      Qry_BuscaItensEstoque_QUERY.SQL.Clear;
      Qry_BuscaItensEstoque_QUERY.SQL.Text:=
      'Select coalesce(sum(quantidade),0) as estoque from tabestoque where DIVERSOcor='+Qry_BuscaItensPedido_QUERY.Fields[2].AsString;
      Qry_BuscaItensEstoque_QUERY.Open;

      if(FItensCarrinhos.___LocalizaItemCarrinho(Qry_BuscaItensPedido_QUERY.Fields[0].AsString,'',Qry_BuscaItensPedido_QUERY.Fields[2].AsString,'DIVERSO')=False) then
      begin
        if(rbSIM.Checked=True) then
        begin

          if((Qry_BuscaItensEstoque_QUERY.Fields[0].AsCurrency-Qry_BuscaItensPedido_QUERY.Fields[3].AsCurrency){quantidade}<0) then
          begin
            STRGItensPedido.Cells[1,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[0].AsString; //PEDIDO
            STRGItensPedido.Cells[2,STRGItensPedido.RowCount-1]:='DIVERSO';
            STRGItensPedido.Cells[3,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[1].AsString;//DESCRICAO ITEM
            STRGItensPedido.Cells[4,STRGItensPedido.RowCount-1]:='' ;
            STRGItensPedido.Cells[5,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[3].AsString;//Quantidade Vendida
            STRGItensPedido.Cells[6,STRGItensPedido.RowCount-1]:=Qry_BuscaItensEstoque_QUERY.Fields[0].AsString;//Quantidade Vendida
            STRGItensPedido.Cells[7,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[2].AsString;//ItemCOR

            STRGItensPedido.RowCount:=STRGItensPedido.RowCount+1;
          end;
        end
        else
        begin
          STRGItensPedido.Cells[1,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[0].AsString; //PEDIDO
          STRGItensPedido.Cells[2,STRGItensPedido.RowCount-1]:='DIVERSO';
          STRGItensPedido.Cells[3,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[1].AsString;//DESCRICAO ITEM
          STRGItensPedido.Cells[4,STRGItensPedido.RowCount-1]:='';
          STRGItensPedido.Cells[5,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[3].AsString;//Quantidade Vendida
          STRGItensPedido.Cells[6,STRGItensPedido.RowCount-1]:=Qry_BuscaItensEstoque_QUERY.Fields[0].AsString;//Quantidade Vendida
          STRGItensPedido.Cells[7,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[2].AsString;//ItemCOR

          STRGItensPedido.RowCount:=STRGItensPedido.RowCount+1;
        end;
      end;
      Qry_BuscaItensPedido_QUERY.Next;
    end;

    //KITBOX
    Qry_BuscaItensPedido_QUERY.Close;
    Qry_BuscaItensPedido_QUERY.SQL.Clear;
    Qry_BuscaItensPedido_QUERY.SQL.Text:=
    'Select pp.pedido,p.descricao||'' - ''||cor.descricao,pcor.codigo,sum(ppp.quantidade) '+
    'from tabKITBOX_pp ppp '+
    'join tabKITBOXcor pcor on pcor.codigo=ppp.KITBOXcor '+
    'join tabKITBOX p on p.codigo=pcor.KITBOX '+
    'join tabcor cor on cor.codigo=pcor.cor '+
    'join tabpedido_proj pp on pp.codigo=ppp.pedidoprojeto '+
    'where pp.pedido='+edtPedidoMateriaisporPedido.Text +' '+
    'and '+
    'NOT EXISTS ( '+
    '  SELECT CODIGO  '+
    '  FROM tabpedidocompramateriaisavulso '+
    '   WHERE tabpedidocompramateriaisavulso.pedido = PP.pedido '+
    '  AND tabpedidocompramateriaisavulso.KITBOXcor=pCOR.codigo '+
    ') '+
    'group by 1,2,3 ';
    Qry_BuscaItensPedido_QUERY.Open;

    while not Qry_BuscaItensPedido_QUERY.Eof do
    begin
      Qry_BuscaItensEstoque_QUERY.Close;
      Qry_BuscaItensEstoque_QUERY.SQL.Clear;
      Qry_BuscaItensEstoque_QUERY.SQL.Text:=
      'Select coalesce(sum(quantidade),0) as estoque from tabestoque where KITBOXcor='+Qry_BuscaItensPedido_QUERY.Fields[2].AsString;
      Qry_BuscaItensEstoque_QUERY.Open;

      if(FItensCarrinhos.___LocalizaItemCarrinho(Qry_BuscaItensPedido_QUERY.Fields[0].AsString,'',Qry_BuscaItensPedido_QUERY.Fields[2].AsString,'KITBOX')=False) then
      begin
        if(rbSIM.Checked=True) then
        begin

          if((Qry_BuscaItensEstoque_QUERY.Fields[0].AsCurrency-Qry_BuscaItensPedido_QUERY.Fields[3].AsCurrency){quantidade}<0) then
          begin
            STRGItensPedido.Cells[1,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[0].AsString; //PEDIDO
            STRGItensPedido.Cells[2,STRGItensPedido.RowCount-1]:='KITBOX';
            STRGItensPedido.Cells[3,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[1].AsString;//DESCRICAO ITEM
            STRGItensPedido.Cells[4,STRGItensPedido.RowCount-1]:='' ;
            STRGItensPedido.Cells[5,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[3].AsString;//Quantidade Vendida
            STRGItensPedido.Cells[6,STRGItensPedido.RowCount-1]:=Qry_BuscaItensEstoque_QUERY.Fields[0].AsString;//Quantidade Vendida
            STRGItensPedido.Cells[7,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[2].AsString;//ItemCOR

            STRGItensPedido.RowCount:=STRGItensPedido.RowCount+1;
          end;
        end
        else
        begin
          STRGItensPedido.Cells[1,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[0].AsString; //PEDIDO
          STRGItensPedido.Cells[2,STRGItensPedido.RowCount-1]:='KITBOX';
          STRGItensPedido.Cells[3,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[1].AsString;//DESCRICAO ITEM
          STRGItensPedido.Cells[4,STRGItensPedido.RowCount-1]:='';
          STRGItensPedido.Cells[5,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[3].AsString;//Quantidade Vendida
          STRGItensPedido.Cells[6,STRGItensPedido.RowCount-1]:=Qry_BuscaItensEstoque_QUERY.Fields[0].AsString;//Quantidade Vendida
          STRGItensPedido.Cells[7,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[2].AsString;//ItemCOR

          STRGItensPedido.RowCount:=STRGItensPedido.RowCount+1;
        end;
      end;
      Qry_BuscaItensPedido_QUERY.Next;
    end;

    //PERSIANA
    Qry_BuscaItensPedido_QUERY.Close;
    Qry_BuscaItensPedido_QUERY.SQL.Clear;
    Qry_BuscaItensPedido_QUERY.SQL.Text:=
    'Select pp.pedido,p.nome||'' - ''||cor.descricao,pcor.codigo,sum(ppp.quantidade) '+
    'from tabPERSIANA_pp ppp '+
    'join tabPERSIANAGRUPODIAMETROcor pcor on pcor.codigo=ppp.PERSIANAGRUPODIAMETROcor '+
    'join tabPERSIANA p on p.codigo=pcor.PERSIANA '+
    'join tabcor cor on cor.codigo=pcor.cor '+
    'join tabpedido_proj pp on pp.codigo=ppp.pedidoprojeto '+
    'where pp.pedido='+edtPedidoMateriaisporPedido.Text +' '+
    'and '+
    'NOT EXISTS ( '+
    '  SELECT CODIGO  '+
    '  FROM tabpedidocompramateriaisavulso '+
    '   WHERE tabpedidocompramateriaisavulso.pedido = PP.pedido '+
    '  AND tabpedidocompramateriaisavulso.PERSIANAcor=pCOR.codigo '+
    ') '+
    'group by 1,2,3 ';
    Qry_BuscaItensPedido_QUERY.Open;

    while not Qry_BuscaItensPedido_QUERY.Eof do
    begin
      Qry_BuscaItensEstoque_QUERY.Close;
      Qry_BuscaItensEstoque_QUERY.SQL.Clear;
      Qry_BuscaItensEstoque_QUERY.SQL.Text:=
      'Select coalesce(sum(quantidade),0) as estoque from tabestoque where PERSIANAgrupodiametrocor='+Qry_BuscaItensPedido_QUERY.Fields[2].AsString;
      Qry_BuscaItensEstoque_QUERY.Open;

      if(FItensCarrinhos.___LocalizaItemCarrinho(Qry_BuscaItensPedido_QUERY.Fields[0].AsString,'',Qry_BuscaItensPedido_QUERY.Fields[2].AsString,'PERSIANA')=False) then
      begin
        if(rbSIM.Checked=True) then
        begin

          if((Qry_BuscaItensEstoque_QUERY.Fields[0].AsCurrency-Qry_BuscaItensPedido_QUERY.Fields[3].AsCurrency){quantidade}<0) then
          begin
            STRGItensPedido.Cells[1,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[0].AsString; //PEDIDO
            STRGItensPedido.Cells[2,STRGItensPedido.RowCount-1]:='PERSIANA';
            STRGItensPedido.Cells[3,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[1].AsString;//DESCRICAO ITEM
            STRGItensPedido.Cells[4,STRGItensPedido.RowCount-1]:='' ;
            STRGItensPedido.Cells[5,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[3].AsString;//Quantidade Vendida
            STRGItensPedido.Cells[6,STRGItensPedido.RowCount-1]:=Qry_BuscaItensEstoque_QUERY.Fields[0].AsString;//Quantidade Vendida
            STRGItensPedido.Cells[7,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[2].AsString;//ItemCOR

            STRGItensPedido.RowCount:=STRGItensPedido.RowCount+1;
          end;
        end
        else
        begin
          STRGItensPedido.Cells[1,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[0].AsString; //PEDIDO
          STRGItensPedido.Cells[2,STRGItensPedido.RowCount-1]:='PERSIANA';
          STRGItensPedido.Cells[3,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[1].AsString;//DESCRICAO ITEM
          STRGItensPedido.Cells[4,STRGItensPedido.RowCount-1]:='';
          STRGItensPedido.Cells[5,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[3].AsString;//Quantidade Vendida
          STRGItensPedido.Cells[6,STRGItensPedido.RowCount-1]:=Qry_BuscaItensEstoque_QUERY.Fields[0].AsString;//Quantidade Vendida
          STRGItensPedido.Cells[7,STRGItensPedido.RowCount-1]:=Qry_BuscaItensPedido_QUERY.Fields[2].AsString;//ItemCOR

          STRGItensPedido.RowCount:=STRGItensPedido.RowCount+1;
        end;
      end;
      Qry_BuscaItensPedido_QUERY.Next;
    end;

    STRGItensPedido.RowCount:=STRGItensPedido.RowCount-1;

  finally
    FreeAndNil(Qry_BuscaItensPedido_QUERY);
    FreeAndNil(Qry_BuscaItensEstoque_QUERY);
  end;

end;

procedure TFPEDIDOCOMPRA.STRGItensPedidoDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
    if((ARow <> 0) and (ACol = 0))
    then begin
        if(STRGItensPedido.Cells[ACol,ARow] = '            X' ) or (STRGItensPedido.Cells[ACol,ARow] = 'X' )then
        begin
              Ilprodutos.Draw(STRGItensPedido.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 6); //check box marcado
        end
        else
        begin
              Ilprodutos.Draw(STRGItensPedido.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 14); //check box sem marcar    5
        end;
    end;
end;

procedure TFPEDIDOCOMPRA.STRGItensPedidoDblClick(Sender: TObject);
begin
    if (TStringGrid(sender).cells[0,TStringgrid(sender).row]='')
    Then TStringGrid(sender).cells[0,TStringgrid(sender).row]:='            X'
    Else TStringGrid(sender).cells[0,TStringgrid(sender).row]:='';
end;

procedure TFPEDIDOCOMPRA.img1Click(Sender: TObject);
begin
   //Mostrar Itens no Carrinho
   FItensCarrinhos.ShowModal;
   __CarregaStringGrid;
   if(FItensCarrinhos.STRGItensPedido.Cells[1,FItensCarrinhos.STRGItensPedido.RowCount-1]<>'')
   then lbQuantidadeCarrinho.Caption:=IntToStr(FItensCarrinhos.STRGItensPedido.RowCount-1)
   else lbQuantidadeCarrinho.Caption:=IntToStr(FItensCarrinhos.STRGItensPedido.RowCount-2)
end;

procedure TFPEDIDOCOMPRA.rbSIMClick(Sender: TObject);
begin
  __CarregaStringGrid;
end;

procedure TFPEDIDOCOMPRA.rbNAoClick(Sender: TObject);
begin
  __CarregaStringGrid;
end;

procedure TFPEDIDOCOMPRA.__LimpaStringGrid;
var
  i:Integer;
begin
  for i:=0 to STRGItensPedido.RowCount do
  begin
    STRGItensPedido.Cells[0,i]:='';
    STRGItensPedido.Cells[1,i]:='';
    STRGItensPedido.Cells[2,i]:='';
    STRGItensPedido.Cells[3,i]:='';
    STRGItensPedido.Cells[4,i]:='';
    STRGItensPedido.Cells[5,i]:='';
    STRGItensPedido.Cells[6,i]:='';
    STRGItensPedido.Cells[7,i]:='';
  end;
end;

procedure TFPEDIDOCOMPRA.STRGItensPedidoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if(Key= 13) then
  begin
    Self.STRGItensPedidoDblClick(Sender);
    STRGItensPedido.SetFocus;
  end;
  if(Key=46)then
  begin
    __Delete(STRGItensPedido.Row);
  end;
end;

procedure TFPEDIDOCOMPRA.btUltimoClick(Sender: TObject);
var
  I,i2:Integer;
begin
  //Carrega no carrinho
  for i:=1 to STRGItensPedido.RowCount-1 do
  begin
    if(STRGItensPedido.Cells[0,i]='            X') then
    begin
      if(FItensCarrinhos.STRGItensPedido.Cells[1,FItensCarrinhos.STRGItensPedido.RowCount-1]<>'')
      then FItensCarrinhos.STRGItensPedido.RowCount:=FItensCarrinhos.STRGItensPedido.RowCount+1;
      if(FItensCarrinhos.___LocalizaItemCarrinho(STRGItensPedido.Cells[1,i],STRGItensPedido.Cells[4,i],STRGItensPedido.Cells[7,i],STRGItensPedido.Cells[2,i])=False) then
      begin
        FItensCarrinhos.STRGItensPedido.Cells[1,FItensCarrinhos.STRGItensPedido.RowCount-1]:=STRGItensPedido.Cells[1,i];
        FItensCarrinhos.STRGItensPedido.Cells[2,FItensCarrinhos.STRGItensPedido.RowCount-1]:=STRGItensPedido.Cells[2,i];
        FItensCarrinhos.STRGItensPedido.Cells[3,FItensCarrinhos.STRGItensPedido.RowCount-1]:=STRGItensPedido.Cells[3,i];
        FItensCarrinhos.STRGItensPedido.Cells[4,FItensCarrinhos.STRGItensPedido.RowCount-1]:=STRGItensPedido.Cells[4,i];
        FItensCarrinhos.STRGItensPedido.Cells[5,FItensCarrinhos.STRGItensPedido.RowCount-1]:=STRGItensPedido.Cells[5,i];
        FItensCarrinhos.STRGItensPedido.Cells[6,FItensCarrinhos.STRGItensPedido.RowCount-1]:=STRGItensPedido.Cells[6,i];
        FItensCarrinhos.STRGItensPedido.Cells[7,FItensCarrinhos.STRGItensPedido.RowCount-1]:=STRGItensPedido.Cells[7,i];
        FItensCarrinhos.STRGItensPedido.RowCount:=FItensCarrinhos.STRGItensPedido.RowCount+1;
      end;
    end;
  end;
  i:=1;
  while i<STRGItensPedido.RowCount do
  begin
    if(STRGItensPedido.Cells[0,i]='            X') then
    begin
      __Delete(i);
      i:=i-1;
    end;
    i:=i+1;
  end;
  if(FItensCarrinhos.STRGItensPedido.Cells[1,FItensCarrinhos.STRGItensPedido.RowCount-1]='')
  then FItensCarrinhos.STRGItensPedido.RowCount:=FItensCarrinhos.STRGItensPedido.RowCount-1;
  lbQuantidadeCarrinho.Caption:=IntToStr(FItensCarrinhos.STRGItensPedido.RowCount-1);
  STRGItensPedido.Row:=1;
end;

procedure TFPEDIDOCOMPRA.__Delete(Linha:Integer);
var
    nlinha: integer;
begin
  if(Linha=0)then
  begin
    exit;
  end;

  STRGItensPedido.Row :=Linha;

  if STRGItensPedido.RowCount = 1 then
  begin
    STRGItensPedido.Cells[0,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[1,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[2,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[3,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[4,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[5,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[6,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[7,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[8,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[9,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[10,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[11,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[12,STRGItensPedido.Row] :='' ;
  end;
  if STRGItensPedido.RowCount = 2 then
  begin
    STRGItensPedido.Cells[0,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[1,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[2,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[3,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[4,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[5,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[6,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[7,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[8,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[9,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[10,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[11,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[12,STRGItensPedido.Row] :='' ;
    //Exit;
  end
  else
  begin
    if STRGItensPedido.RowCount-1 = STRGItensPedido.Row then
    begin
      STRGItensPedido.Cells[0,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[1,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[2,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[3,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[4,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[5,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[6,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[7,STRGItensPedido.Row] := '';
      //STRGItensPedido.Cells[8,STRGItensPedido.Row] := '';
      //STRGItensPedido.Cells[9,STRGItensPedido.Row] := '';
      //STRGItensPedido.Cells[10,STRGItensPedido.Row] := '';
      //STRGItensPedido.Cells[11,STRGItensPedido.Row] := '';
      //STRGItensPedido.Cells[12,STRGItensPedido.Row] :='' ;

    end;

    if (STRGItensPedido.RowCount > 1) and (STRGItensPedido.RowCount <> STRGItensPedido.Row) then
    begin

      {Se o numero de linha for igual ao numero de ao numero da linha ent�o ele simplesmente limpa as c�lulas, ou entao ele
      llimpa as celulas da linhas selecionada e copia o conteudo da proxima, e assim por diante ate chegar na ultima, que ser� excluida.}
      STRGItensPedido.Cells[0,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[1,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[2,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[3,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[4,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[5,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[6,STRGItensPedido.Row] := '';
      STRGItensPedido.Cells[7,STRGItensPedido.Row] := '';
      //STRGItensPedido.Cells[8,STRGItensPedido.Row] := '';
      //STRGItensPedido.Cells[9,STRGItensPedido.Row] := '';
      //STRGItensPedido.Cells[10,STRGItensPedido.Row] := '';
      //STRGItensPedido.Cells[11,STRGItensPedido.Row] := '';
      //STRGItensPedido.Cells[12,STRGItensPedido.Row] :='' ;

      nlinha := STRGItensPedido.Row;

      while nlinha <> STRGItensPedido.RowCount do
      begin
        if STRGItensPedido.Cells[1,(nlinha + 1)] <> '' then
        begin
          STRGItensPedido.Cells[0,STRGItensPedido.Row] := STRGItensPedido.Cells[0,(nlinha +1)];
          STRGItensPedido.Cells[1,STRGItensPedido.Row] := STRGItensPedido.Cells[1,(nlinha +1)];
          STRGItensPedido.Cells[2,STRGItensPedido.Row] := STRGItensPedido.Cells[2,(nlinha +1)];
          STRGItensPedido.Cells[3,STRGItensPedido.Row] := STRGItensPedido.Cells[3,(nlinha +1)];
          STRGItensPedido.Cells[4,STRGItensPedido.Row] := STRGItensPedido.Cells[4,(nlinha +1)];
          STRGItensPedido.Cells[5,STRGItensPedido.Row] := STRGItensPedido.Cells[5,(nlinha +1)];
          STRGItensPedido.Cells[6,STRGItensPedido.Row] := STRGItensPedido.Cells[6,(nlinha +1)];
          STRGItensPedido.Cells[7,STRGItensPedido.Row] := STRGItensPedido.Cells[7,(nlinha +1)];

          //STRGItensPedido.Cells[8,STRGItensPedido.Row] := STRGItensPedido.Cells[8,(nlinha +1)];
          //STRGItensPedido.Cells[9,STRGItensPedido.Row] := STRGItensPedido.Cells[9,(nlinha +1)];
          //STRGItensPedido.Cells[10,STRGItensPedido.Row] := STRGItensPedido.Cells[10,(nlinha +1)];
          //STRGItensPedido.Cells[11,STRGItensPedido.Row] := STRGItensPedido.Cells[11,(nlinha +1)];
          //STRGItensPedido.Cells[12,STRGItensPedido.Row] := STRGItensPedido.Cells[12,(nlinha +1)];
          STRGItensPedido.Row := STRGItensPedido.Row + 1;
          nlinha := STRGItensPedido.Row;

        end
        else
        begin
          STRGItensPedido.RowCount := STRGItensPedido.RowCount - 1;
          STRGItensPedido.Cells[0,nlinha] := '';
          STRGItensPedido.Cells[1,nlinha] := '';
          STRGItensPedido.Cells[2,nlinha] := '';
          STRGItensPedido.Cells[3,nlinha] := '';
          STRGItensPedido.Cells[4,nlinha] := '';
          STRGItensPedido.Cells[5,nlinha] := '';
          STRGItensPedido.Cells[6,nlinha] := '';
          STRGItensPedido.Cells[7,nlinha] := '';
          //STRGItensPedido.Cells[8,nlinha] := '';
          //STRGItensPedido.Cells[9,nlinha] := '';
          //STRGItensPedido.Cells[10,nlinha] := '';
          //STRGItensPedido.Cells[11,nlinha] := '';
          //STRGItensPedido.Cells[12,nlinha] := '';
          nlinha:=STRGItensPedido.RowCount;
        end;

      end;
      
    end;
  end;
end;

procedure TFPEDIDOCOMPRA.btExecutarClick(Sender: TObject);
var
   i:Integer;
begin
  for i:=1 to FItensCarrinhos.STRGItensPedido.RowCount do
  begin
     if(FItensCarrinhos.STRGItensPedido.Cells[1,i] = '') then
     begin
       FItensCarrinhos.__LimpaStringGrid;
       FItensCarrinhos.__MontaStringGrid;
       lbQuantidadeCarrinho.Caption:='0';
       Exit;
     end;
     with ObjPedidoCompraMateriaisAvulso do
     begin
        ZerarTabela;
        Status:=dsInsert;
        Submit_CODIGO('0');
        if(FItensCarrinhos.STRGItensPedido.Cells[2,i]='VIDRO')then
        begin
          VIDROCOR.Submit_Codigo(FItensCarrinhos.STRGItensPedido.Cells[7,i]);
          Submit_TipoMaterial('VIDRO');
        end;
        if(FItensCarrinhos.STRGItensPedido.Cells[2,i]='FERRAGEM')then
        begin
          FERRAGEMCOR.Submit_Codigo(FItensCarrinhos.STRGItensPedido.Cells[7,i]);
          Submit_TipoMaterial('FERRAGEM');
        end;
        if(FItensCarrinhos.STRGItensPedido.Cells[2,i]='KITBOX')then
        begin
          KITBOXCOR.Submit_Codigo(FItensCarrinhos.STRGItensPedido.Cells[7,i]);
          Submit_TipoMaterial('KITBOX');
        end;
        if(FItensCarrinhos.STRGItensPedido.Cells[2,i]='PERFILADO')then
        begin
          PERFILADOCOR.Submit_Codigo(FItensCarrinhos.STRGItensPedido.Cells[7,i]);
          Submit_TipoMaterial('PERFILADO');
        end;
        if(FItensCarrinhos.STRGItensPedido.Cells[2,i]='DIVERSO')then
        begin
          DIVERSOCOR.Submit_Codigo(FItensCarrinhos.STRGItensPedido.Cells[7,i]);
          Submit_TipoMaterial('DIVERSO');
        end;
        if(FItensCarrinhos.STRGItensPedido.Cells[2,i]='PERSIANA')then
        begin
          PERSIANACOR.Submit_Codigo(FItensCarrinhos.STRGItensPedido.Cells[7,i]);
          Submit_TipoMaterial('PERSIANA');
        end;

        Submit_QUANTIDADE(FItensCarrinhos.STRGItensPedido.Cells[5,i]);
        Submit_pedido(FItensCarrinhos.STRGItensPedido.Cells[1,i]);

        //Submit_ALTURA(FItensCarrinhos.STRGItensPedido.Cells[5,i]);
        //Submit_LARGURA(FItensCarrinhos.STRGItensPedido.Cells[5,i]);

        PEDIDOCOMPRA.Submit_CODIGO(lbcodigo.Caption);

        if(Salvar(true)=False) then
        begin
          MensagemErro('Erro ao tentar cadastrar novo material');
          Exit;
        end;
     end;
  end;

end;

procedure TFPEDIDOCOMPRA.btExcluirAvulsoClick(Sender: TObject);
begin
  if (DBGrid2.DataSource.DataSet.Active=false)
  then exit;

  if (DBGrid2.DataSource.DataSet.recordcount=0)
  then exit;

  if (MensagemPergunta('Certeza que deseja excluir?')=mrno)
  then exit;


  if (ObjPedidoCompraMateriaisAvulso.LocalizaCodigo(DBGrid2.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
  then Begin
    mensagemerro('N�o localizado');
    exit;
  End;

  if (ObjPedidoCompraMateriaisAvulso.Exclui(DBGrid2.DataSource.DataSet.fieldbyname('codigo').asstring,true)=False)
  then exit;
  ObjPedidoCompraMateriaisAvulso.CarregaMateriais(lbcodigo.Caption);
end;

procedure TFPEDIDOCOMPRA.N1LimparCarrinho1Click(Sender: TObject);
begin
    FItensCarrinhos.__LimpaStringGrid;
    FItensCarrinhos.__MontaStringGrid;
    lbQuantidadeCarrinho.Caption:='0';
    __CarregaStringGrid;
end;

procedure TFPEDIDOCOMPRA.N2MostrarItensnoCarrinho1Click(Sender: TObject);
begin
   FItensCarrinhos.ShowModal;
   __CarregaStringGrid;
   if(FItensCarrinhos.STRGItensPedido.Cells[1,FItensCarrinhos.STRGItensPedido.RowCount-1]<>'')
   then lbQuantidadeCarrinho.Caption:=IntToStr(FItensCarrinhos.STRGItensPedido.RowCount-1)
   else lbQuantidadeCarrinho.Caption:=IntToStr(FItensCarrinhos.STRGItensPedido.RowCount-2)
end;

procedure TFPEDIDOCOMPRA.__GerarDesenhor(CodigoPedidoProjeto,codigopedido:string);
var
  FVisualizarProjetos:TFVisulizarProjetos;
  codigoprojeto:string;
  sender:TObject;
begin
  FVisualizarProjetos:=TFVisulizarProjetos.Create(nil);

  try
    if(CodigoPedidoProjeto='') or (codigopedido='') then
    begin
      Exit;
    end;
    codigoprojeto:=get_campoTabela('projeto','codigo','tabpedido_proj',CodigoPedidoProjeto);
    FVisualizarProjetos.PassaObjetos(codigoprojeto,codigopedido,CodigoPedidoProjeto);
    FVisualizarProjetos.ShowModal;
    
  finally
    FreeAndNil(FVisualizarProjetos);
  end;
end;

procedure TFPEDIDOCOMPRA.edtDataKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(key=vk_Space)
  then edtData.Text:=DateToStr(Now);
end;

procedure TFPEDIDOCOMPRA.lb13Click(Sender: TObject);
begin
  if(lbcodigo.Caption='')
  then Exit;

  objpedidoProjetoPedidoCompra.GerarPedidoCompra(lbcodigo.Caption, ckCusto.Checked);
end;

end.
