unit UObjCidade;
Interface
Uses Ibcustomdataset,IBStoredProc,Db,UessencialGlobal,Classes,IBQuery,StdCtrls,Forms,ComCtrls,Uobjestado;

Type
   TRegCidade=Record
                       Codigo:Tstringlist;
                       Nome:TstringList;
                 End;

   TObjcidade=class

          Public
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                Constructor Create;
                Destructor  Free;
                Function   Salvar                          :Boolean;
                Procedure  TabelaparaObjeto                        ;
                Function   LocalizaCodigo(Parametro:string) :boolean;
                Function   Exclui(Pcodigo:string)            :Boolean;
                Function   Get_Pesquisa                         :string;
                Function   Get_TituloPesquisa                   :string;
                Function   Get_EstadoPEsquisa                   :string;
                Function   Get_EstadoTituloPesquisa                   :string;
                Function   Pega_Cidades:TRegCidade;
                Procedure  ZerarTabela;
                Procedure  Cancelar;
                Procedure  Commit;

                Function  Get_CODIGO                     :string;
                Function  Get_NOME                       :string;
                Function  Get_ESTADO                     :string;
                function  get_codigoCidade               :string;
                procedure submit_codigoCidade(parametro:string);
                

                Procedure Submit_CODIGO           (parametro:string);
                Procedure Submit_NOME             (parametro:string);
                Procedure Submit_ESTADO           (parametro:string);
                function get_uf                   ():string;

                function importaCidade            (statusBar:TStatusBar):Boolean;
                function tiraApostrofo(str:string):string;



                procedure submit_pCancelaImportacao (parametro:boolean);
                function get_pCancelaImportacao     ():Boolean;

                procedure ResgataCidade(PCombo: TComboBox;pESTADO:string);overload;
                procedure ResgataCidade(PCombo: TComboBox);overload;

         Private
               ObjDataset:Tibdataset;
               objquery:tibquery;




                CODIGO          :String[09];
                NOME            :String[40];
                RegCidade       :TRegcidade;
                pCancelaImportacao :Boolean;
                listaDeCidade       :TStringList;

                {tobjestado--> bug}
                Estado          :TObjEstado;

                uf              :string;
                COdigoCidade    :string;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                function gravaCidade(Pnome,estado,PcodigoCidade:string):Boolean;
                function ExisteCidade (Pnome:string):Boolean;
                procedure zeraCidades ();

   End;


implementation
uses SysUtils,Dialogs,UDatamodulo;


{ TTabTitulo }


Procedure  TObjcidade.TabelaparaObjeto;//ok
begin

     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring;
        Self.nome             :=FieldByName('nome').asstring;
        self.COdigoCidade     :=FieldByName('CODIGOCIDADE').AsString;
        self.uf               :=fieldbyname('estado').AsString;


        If (Self.estado.LocalizaCodigo(FieldByName('estado').asstring)=False)
        Then Messagedlg('Erro na localiza��o do Estado!',mterror,[mbok],0);
     End;

end;


Procedure TObjcidade.ObjetoparaTabela;//ok
begin

  With ObjDataset do
  Begin                                                        
      FieldByName('CODIGO').asstring       :=     Self.CODIGO            ;
      FieldByName('nome').asstring         :=     Self.nome              ;
      FieldByName('estado').asstring       :=     Self.estado.Get_CODIGO ;
      FieldByName('CODIGOCIDADE').AsString :=     Self.COdigoCidade;

  End;

End;

//***********************************************************************

function TObjcidade.Salvar: Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if(Self.Status=dsedit)
  Then Begin
            If Self.LocalizaCodigo(Self.CODIGO)=False
            Then Begin
                      Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                      result:=False;
                      exit;
                 End;
       End;


 if Self.status=dsinsert
 then  Self.ObjDataset.Insert//libera para insercao
 Else  Self.ObjDataset.edit;//se for edicao libera para tal
 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;
 FDataModulo.IBTransaction.CommitRetaining;
 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjcidade.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO           :='';
        Self.nome              :='';
        Self.estado.ZerarTabela;
        self.COdigoCidade :='';
     End;
end;

Function TObjcidade.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';
       If (nome='')
       Then Mensagem:=mensagem+'/Nome';
       If (estado.Get_CODIGO='')
       Then Mensagem:=mensagem+'/Estado';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjcidade.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     Try
        If (Self.estado.LocalizaCodigo(Self.estado.Get_CODIGO)=False)
        Then Mensagem:='/ Estado n�o Encontrado!';

        If (mensagem<>'')
        Then Begin
                  Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
                  result:=False;
                  exit;
             End;
        result:=true;
     Finally

     End;
End;

function TObjcidade.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Real;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:='C�digo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjcidade.VerificaData: Boolean;
var
Datas:Tdatetime;
Mensagem:string;
begin
     mensagem:='';

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m datas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjcidade.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjcidade.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('select codigo,nome,estado,codigocidade from tabcidade where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjcidade.Cancelar;
begin
     FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

function TObjcidade.Exclui(Pcodigo: string): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 FDataModulo.IBTransaction.CommitRetaining;
             End
                 
        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjcidade.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.objquery:=TIBQuery.create(nil);
        Self.objquery.Database:=FDataModulo.IBDatabase;

        self.listaDeCidade:=TStringList.Create;

        Self.pCancelaImportacao := False;


        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.estado:=Tobjestado.create;
        ZerarTabela;

        Self.RegCidade.Codigo:=TStringList.create;
        Self.RegCidade.Codigo.Clear;
        Self.RegCidade.Nome:=TStringList.create;
        Self.RegCidade.Nome.Clear;


        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('select codigo,nome,estado,codigocidade from tabcidade where codigo=-1');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' Insert into tabcidade (codigo,nome,estado,codigocidade) values (:codigo,:nome,:estado,:codigocidade)');

                ModifySQL.clear;
                ModifySQL.add(' Update tabcidade set codigo=:codigo,nome=:nome,estado=:estado,codigocidade=:codigocidade where codigo=:codigo ');


                DeleteSQL.clear;
                DeleteSQL.add('Delete from tabcidade where codigo=:codigo ');

                RefreshSQL.clear;
                RefreshSQL.add('select codigo,nome,estado from tabcidade where codigo=-1');
                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjcidade.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjcidade.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjcidade.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjcidade.Get_Pesquisa: string;
begin
     Result:=' Select * from Tabcidade ';
end;

function TObjcidade.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de cidade ';
end;

function TObjcidade.Get_ESTADO: string;
begin
     Result:=Self.uf;
end;

function TObjcidade.Get_NOME: string;
begin
     Result:=Self.NOME;
end;

procedure TObjcidade.Submit_ESTADO(parametro: string);
begin
     Self.estado.Submit_CODIGO(parametro);
end;

procedure TObjcidade.Submit_NOME(parametro: string);
begin
     Self.nome:=parametro;
end;

function TObjcidade.Pega_Cidades: TRegCidade;
var
Qlocal:Tibquery;
begin

    Try
         Qlocal:=Tibquery.create(nil);
         Qlocal.database:=Fdatamodulo.IbDatabase;

         With QLocal do
         Begin
              close;
              sql.clear;
              sql.add('Select codigo,nome from tabcidade order by codigo');
              open;
              first;
              Self.RegCidade.Codigo.clear;
              Self.RegCidade.Nome.clear;
              While not(eof) do
              Begin
                   Self.RegCidade.codigo.add(Fieldbyname('codigo').asstring);
                   Self.RegCidade.Nome.add(Fieldbyname('nome').asstring);
                   next;
              End;
              result:=Self.RegCidade;
         End;
    Finally
           Freeandnil(Qlocal);
    End;

end;

function TObjcidade.Get_EstadoPEsquisa: string;
begin
     result:=Self.estado.Get_Pesquisa;
end;

function TObjcidade.Get_EstadoTituloPesquisa: string;
begin
     result:=Self.estado.Get_TituloPesquisa;
end;

destructor TObjcidade.Free;
begin
     Freeandnil(Self.ObjDataset);
     FreeAndNil(Self.objquery);
     Self.estado.free;
     Freeandnil(Self.RegCidade.Codigo);
     Freeandnil(Self.RegCidade.Nome);
     FreeAndNil(self.listaDeCidade);
end;

function TObjcidade.get_codigoCidade: string;
begin

  result:=self.COdigoCidade;

end;

procedure TObjcidade.submit_codigoCidade(parametro:string);
begin

  self.COdigoCidade:=parametro;

end;

function TObjcidade.importaCidade(statusBar:TStatusBar): Boolean;
var
  openDialog:TOpenDialog;
  strAux,strCidades:TStringList;
  cont,i,j:integer;

begin


  try

    try

      cont:=0;
      openDialog:=TOpenDialog.Create(nil);
      strCidades:=TStringList.Create;
      strAux    :=TStringList.Create;
      openDialog.Filter:='*.txt';

      if (openDialog.Execute) then
      begin

           strCidades.LoadFromFile(openDialog.FileName);

      end else
      begin

        result:=False;
        Exit;

      end;

      self.zeraCidades;
      
      for i:=0  to strCidades.Count-1  do
      begin

         if (pCancelaImportacao) then
         begin

          pCancelaImportacao := False;
          Result := False;
          statusBar.Panels[0].Text := '0';
          Exit;

         end;

         ExplodeStr(strCidades[i],strAux,';','');

         if not (gravaCidade (Trim (tiraApostrofo(strAux[0])), trim (straux[2]),Trim (RetornaSoNumeros(strAux[1])))) then
           result:=False
         else
         begin

           Application.ProcessMessages;
           statusBar.Panels[0].Text := IntToStr(cont);
           cont:=cont+1;
           Application.ProcessMessages;

         end;


      end;

    except

      result:=False;
      
    end;

    result:=True;

  finally

    FreeAndNil(openDialog);
    FreeAndNil(strCidades);
    FreeAndNil(strAux);

  end;

end;

function TObjcidade.gravaCidade(Pnome,estado,PcodigoCidade:string): Boolean;
begin

  with self.objquery do
  begin

    Close;
    sql.Clear;

    sql.Add('insert into tabcidade(codigo,nome,estado,codigocidade) values(0'+','+#39+Pnome+#39+','+#39+estado+#39+','+#39+PcodigoCidade+#39+')');
    //InputBox('','',sql.Text);

    try

      ExecSQL;
      result:=True;

    except

      MensagemErro('N�o foi possivel importar a cidade: '+Pnome);
      result:=False;
      
    end;

  end;

end;

function TObjcidade.tiraApostrofo(str: string): string;
var
  i:Integer;
begin

     result:='';
     
     for i :=1  to Length(str)  do
     begin

         if (str[i] <> #39) then
          result:=result+str[i]
         else
          result:=result+' ';

     end;


end;

function TObjcidade.get_uf: string;
begin

  result:=self.uf;

end;

function TObjcidade.get_pCancelaImportacao: Boolean;
begin

  result := self.pCancelaImportacao;

end;

procedure TObjcidade.submit_pCancelaImportacao(parametro: boolean);
begin

  self.pCancelaImportacao := parametro;

end;

function TObjcidade.ExisteCidade(Pnome: string): Boolean;
begin



end;

procedure TObjcidade.zeraCidades;
var
  queryLocal:TIBQuery;
begin

  try

     queryLocal := TIBQuery.Create(nil);
     queryLocal.Database := FDataModulo.IBDatabase;

      with (queryLocal) do
      begin

        Close;
        SQL.Clear;
        sql.Add('delete from tabcidade');

        try

          ExecSQL;
          FDataModulo.IBTransaction.CommitRetaining;

        except

          MensagemErro ('N�o foi possivel zerar a tabela de cidades');
          Exit;

        end;


      end;

  finally

      FreeAndNil (queryLocal);

  end;

end;

procedure TObjCIDADE.ResgataCidade(PCombo: TComboBox;pESTADO:string);
begin
     PCombo.Items.Clear;
     With Self.ObjQuery do
     Begin
          Close;
          Sql.Clear;
          Sql.Add('Select Nome from TabCidade WHERE ESTADO='+#39+pestado+#39);
          Open;
          While not (eof) do
          Begin
              PCombo.Items.Add(Fieldbyname('NOme').AsString);
              Next;
          end;
     end;

end;


procedure TObjCIDADE.ResgataCidade(PCombo: TComboBox);
begin
     PCombo.Items.Clear;
     With Self.ObjQuery do
     Begin
          Close;
          Sql.Clear;
          Sql.Add('Select Nome from TabCidade ');
          Open;
          While not (eof) do
          Begin
              PCombo.Items.Add(Fieldbyname('NOme').AsString);
              Next;
          end;
     end;

end;

end.
