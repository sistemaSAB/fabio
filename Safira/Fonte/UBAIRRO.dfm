object FBAIRRO: TFBAIRRO
  Left = 426
  Top = 263
  Width = 874
  Height = 187
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'CADASTRO DE BAIRRO'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbLbNome: TLabel
    Left = 15
    Top = 69
    Width = 33
    Height = 13
    Caption = 'Nome'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object edtNome: TEdit
    Left = 56
    Top = 67
    Width = 545
    Height = 19
    MaxLength = 100
    TabOrder = 0
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 858
    Height = 53
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 1
    DesignSize = (
      858
      53)
    object lbnomeformulario: TLabel
      Left = 540
      Top = 3
      Width = 104
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Cadastro de'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbcodigo: TLabel
      Left = 679
      Top = 9
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb1: TLabel
      Left = 540
      Top = 25
      Width = 60
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Bairros'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object btrelatorio: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btRelatorioClick
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btPesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btExcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btCancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btSalvarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btAlterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btNovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btSairClick
      Spacing = 0
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 104
    Width = 858
    Height = 45
    Align = alBottom
    Color = clMedGray
    TabOrder = 2
    DesignSize = (
      858
      45)
    object imgrodape: TImage
      Left = 1
      Top = 0
      Width = 856
      Height = 44
      Align = alBottom
    end
    object lb14: TLabel
      Left = 524
      Top = 11
      Width = 252
      Height = 20
      Anchors = [akTop]
      Caption = 'Existem X clientes cadastrados'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
end
