object FPRAZOPAGAMENTO: TFPRAZOPAGAMENTO
  Left = -6
  Top = 127
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsNone
  Caption = 'FPRAZOPAGAMENTO'
  ClientHeight = 409
  ClientWidth = 792
  Color = 13421772
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ImageGuiaPrincipal: TImage
    Left = 25
    Top = -1
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaPrincipalClick
  end
  object ImageGuiaExtra: TImage
    Left = 25
    Top = 76
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaExtraClick
  end
  object btFechar: TSpeedButton
    Left = 754
    Top = 6
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btFecharClick
  end
  object btMinimizar: TSpeedButton
    Left = 754
    Top = 32
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btMinimizarClick
  end
  object lbAjuda: TLabel
    Left = 649
    Top = 351
    Width = 50
    Height = 13
    Caption = 'Ajuda...'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btAjuda: TSpeedButton
    Left = 719
    Top = 336
    Width = 28
    Height = 38
    Flat = True
  end
  object PainelExtra: TPanel
    Left = 40
    Top = 27
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 0
    object ImagePainelExtra: TImage
      Left = 0
      Top = 14
      Width = 98
      Height = 352
    end
  end
  object PainelPrincipal: TPanel
    Left = 40
    Top = -13
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 1
    object ImagePainelPrincipal: TImage
      Left = 1
      Top = 14
      Width = 98
      Height = 352
    end
    object btNovo: TSpeedButton
      Left = 15
      Top = 25
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btNovoClick
    end
    object btSalvar: TSpeedButton
      Left = 15
      Top = 66
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSalvarClick
    end
    object btAlterar: TSpeedButton
      Left = 15
      Top = 107
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btAlterarClick
    end
    object btCancelar: TSpeedButton
      Left = 15
      Top = 148
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btCancelarClick
    end
    object btSair: TSpeedButton
      Left = 15
      Top = 311
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSairClick
    end
    object btRelatorio: TSpeedButton
      Left = 15
      Top = 270
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btRelatorioClick
    end
    object btExcluir: TSpeedButton
      Left = 15
      Top = 229
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btExcluirClick
    end
    object btPesquisar: TSpeedButton
      Left = 15
      Top = 188
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      Layout = blGlyphRight
      OnClick = btPesquisarClick
    end
  end
  object Notebook: TNotebook
    Left = 139
    Top = 0
    Width = 612
    Height = 337
    TabOrder = 3
    object TPage
      Left = 0
      Top = 0
      Caption = 'Default'
      object Bevel1: TBevel
        Left = 0
        Top = 0
        Width = 612
        Height = 337
        Align = alClient
        Shape = bsFrame
        Style = bsRaised
      end
            object LbCodigo: TLabel                  
              Left = 3                       
              Top = 0             
              Width = 40                     
              Height = 13                    
              Caption = 'C�digo'             
              Font.Charset = ANSI_CHARSET    
              Font.Color = clBlack           
              Font.Height = -11              
              Font.Name = 'Verdana'          
              Font.Style = []                
              ParentFont = False             
      end                                    
      object EdtCodigo: TEdit
        Left = 48
        Top = 0             
        Width = 72           
        Height = 21          
        MaxLength = 9        
        TabOrder = 0
      end                    
      object LbNome: TLabel                  
              Left = 3                       
              Top = 24             
              Width = 40                     
              Height = 13                    
              Caption = 'Nome'             
              Font.Charset = ANSI_CHARSET    
              Font.Color = clBlack           
              Font.Height = -11              
              Font.Name = 'Verdana'          
              Font.Style = []                
              ParentFont = False             
      end                                    
      object EdtNome: TEdit
        Left = 32
        Top = 24             
        Height = 21          
        Width =400
        MaxLength =50
        TabOrder = 1
      end                    
      object LbParcelas: TLabel                  
              Left = 3                       
              Top = 48             
              Width = 40                     
              Height = 13                    
              Caption = 'Parcelas'             
              Font.Charset = ANSI_CHARSET    
              Font.Color = clBlack           
              Font.Height = -11              
              Font.Name = 'Verdana'          
              Font.Style = []                
              ParentFont = False             
      end                                    
      object EdtParcelas: TEdit
        Left = 64
        Top = 48             
        Width = 72           
        Height = 21          
        MaxLength = 9        
        TabOrder = 2
      end                    
      object LbFormula: TLabel                  
              Left = 3                       
              Top = 72             
              Width = 40                     
              Height = 13                    
              Caption = 'F�rmula'             
              Font.Charset = ANSI_CHARSET    
              Font.Color = clBlack           
              Font.Height = -11              
              Font.Name = 'Verdana'          
              Font.Style = []                
              ParentFont = False             
      end                                    
      object EdtFormula: TEdit
        Left = 56
        Top = 72             
        Height = 21          
        Width =2032
        MaxLength =254
        TabOrder = 3
      end                    
      object LbImprimeDuplicatas: TLabel                  
              Left = 3                       
              Top = 96             
              Width = 40                     
              Height = 13                    
              Caption = 'Imprime Duplicatas'             
              Font.Charset = ANSI_CHARSET    
              Font.Color = clBlack           
              Font.Height = -11              
              Font.Name = 'Verdana'          
              Font.Style = []                
              ParentFont = False             
      end                                    
      object EdtImprimeDuplicatas: TEdit
        Left = 144
        Top = 96             
        Height = 21          
        Width =8
        MaxLength =1
        TabOrder = 4
      end                    
      object LbCarteira: TLabel                  
              Left = 3                       
              Top = 120             
              Width = 40                     
              Height = 13                    
              Caption = 'Carteira'             
              Font.Charset = ANSI_CHARSET    
              Font.Color = clBlack           
              Font.Height = -11              
              Font.Name = 'Verdana'          
              Font.Style = []                
              ParentFont = False             
      end                                    
      object EdtCarteira: TEdit
        Left = 64
        Top = 120             
        Height = 21          
        Width =8
        MaxLength =1
        TabOrder = 5
      end                    
object Label1: TLabel

        Left = 376
        Top = 224
        Width = 32
        Height = 13
        Caption = 'Label1'
      end
      object Edit1: TEdit
        Left = 336
        Top = 240
        Width = 121
        Height = 19
        TabOrder = 0
        Text = 'Edit1'
      end
    end
  end
  object Guia: TTabSet
    Left = 141
    Top = 1
    Width = 607
    Height = 22
    BackgroundColor = 6710886
    DitherBackground = False
    EndMargin = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentBackground = True
    StartMargin = -3
    SelectedColor = 15921906
    Tabs.Strings = (
      'Pri&ncipal')
    TabIndex = 0
    UnselectedColor = 13421772
    OnClick = GuiaClick
  end
end
