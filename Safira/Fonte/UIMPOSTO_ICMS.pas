unit UIMPOSTO_ICMS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjIMPOSTO_ICMS,
  jpeg,IBQuery,UDataModulo;

type
  TFIMPOSTO_ICMS = class(TForm)
    edtFORMULA_VALOR_IMPOSTO_ST_ICMS: TEdit;
    lb1: TLabel;
    edtformula_bc_st_ICMS: TEdit;
    lb2: TLabel;
    edtIVA_ST_ICMS: TEdit;
    lbLbIVA_ST: TLabel;
    edtPAUTA_ST_ICMS: TEdit;
    lbLbPAUTA_ST: TLabel;
    lbLbALIQUOTA_ST: TLabel;
    edtPERC_REDUCAO_BC_ST_ICMS: TEdit;
    lbLbPERC_REDUCAO_BC_ST: TLabel;
    edtALIQUOTA_ST_ICMS: TEdit;
    cbbComboMODALIDADE_ST_ICMS: TComboBox;
    lbLbMODALIDADE_ST: TLabel;
    edtFORMULA_VALOR_IMPOSTO_ICMS: TEdit;
    lb3: TLabel;
    edtformula_BC_ICMS: TEdit;
    lb4: TLabel;
    edtIVA_ICMS: TEdit;
    lbLbIVA: TLabel;
    edtPAUTA_ICMS: TEdit;
    lbLbPAUTA: TLabel;
    edtALIQUOTA_ICMS: TEdit;
    lbLbALIQUOTA: TLabel;
    edtPERC_REDUCAO_BC_ICMS: TEdit;
    lbLbPERC_REDUCAO_BC: TLabel;
    cbbComboMODALIDADE_ICMS: TComboBox;
    lbLbMODALIDADE: TLabel;
    edtSTA_ICMS: TEdit;
    lb5: TLabel;
    edtCFOP_ICMS: TEdit;
    lb6: TLabel;
    lbnomeCFOP_ICMS: TLabel;
    lbNomeSTA_ICMS: TLabel;
    edtSTB_ICMS: TEdit;
    lbNomeSTB_ICMS: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb7: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    imgrodape: TImage;
    lb9: TLabel;
//DECLARA COMPONENTES
    procedure edtSTB_ICMSKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtSTB_ICMSExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtCFOP_ICMSExit(Sender: TObject);
    procedure edtCFOP_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSTA_ICMSExit(Sender: TObject);
    procedure edtSTA_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCFOP_ICMSDblClick(Sender: TObject);
    procedure edtSTA_ICMSDblClick(Sender: TObject);
    procedure edtSTB_ICMSDblClick(Sender: TObject);


  private
         ObjIMPOSTO_ICMS:TObjIMPOSTO_ICMS;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         procedure MostraQuantidadeCadastrada;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FIMPOSTO_ICMS: TFIMPOSTO_ICMS;


implementation

uses UessencialGlobal, Upesquisa, UobjTABELAB_ST, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFIMPOSTO_ICMS.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjIMPOSTO_ICMS do
    Begin
        Submit_CODIGO(lbCodigo.Caption);
        STA.Submit_codigo(edtSTA_ICMS.text);
        STB.Submit_codigo(edtSTB_ICMS.text);
        CFOP.Submit_codigo(edtCFOP_ICMS.text);
        Submit_MODALIDADE(inttostr(cbbComboMODALIDADE_ICMS.itemindex));
        Submit_PERC_REDUCAO_BC(edtPERC_REDUCAO_BC_ICMS.text);
        Submit_ALIQUOTA(edtALIQUOTA_ICMS.text);
        Submit_IVA(edtIVA_ICMS.text);
        Submit_PAUTA(edtPAUTA_ICMS.text);
        Submit_MODALIDADE_ST(inttostr(cbbComboMODALIDADE_ST_ICMS.itemindex));
        Submit_PERC_REDUCAO_BC_ST(edtPERC_REDUCAO_BC_ST_ICMS.text);
        Submit_ALIQUOTA_ST(edtALIQUOTA_ST_ICMS.text);
        Submit_IVA_ST(edtIVA_ST_ICMS.text);
        Submit_PAUTA_ST(edtPAUTA_ST_ICMS.text);
        Submit_FORMULA_BC(edtformula_BC_ICMS.text);
        Submit_FORMULA_BC_ST(edtformula_BC_ST_ICMS.text);


        Submit_formula_valor_imposto(edtformula_valor_imposto_ICMS.text);
        Submit_formula_valor_imposto_ST(edtformula_valor_imposto_ST_ICMS.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFIMPOSTO_ICMS.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjIMPOSTO_ICMS do
     Begin
        lbCodigo.Caption:=Get_CODIGO;
        EdtSTA_ICMS.text:=STA.Get_codigo;
        EdtSTB_ICMS.text:=STB.Get_codigo;
        EdtCFOP_ICMS.text:=CFOP.Get_codigo;
        LbNomeSTA_ICMS.caption:=STA.Get_DESCRICAO;
        LbNomeSTB_ICMS.caption:=STB.Get_DESCRICAO;
        lbnomeCFOP_ICMS.caption:=CFOP.Get_NOME;

        cbbComboMODALIDADE_ICMS.ItemIndex:=-1;

        if (Get_MOdalidade<>'')
        Then cbbComboMODALIDADE_ICMS.ItemIndex:=Strtoint(Get_MODALIDADE);

        cbbComboMODALIDADE_ST_ICMS.ItemIndex:=-1;

        if (Get_MODALIDADE_ST<>'')
        Then cbbComboMODALIDADE_ST_ICMS.ItemIndex:=Strtoint(Get_MODALIDADE_ST);


        EdtPERC_REDUCAO_BC_ICMS.text:=Get_PERC_REDUCAO_BC;
        EdtALIQUOTA_ICMS.text:=Get_ALIQUOTA;
        EdtIVA_ICMS.text:=Get_IVA;
        EdtPAUTA_ICMS.text:=Get_PAUTA;

        EdtPERC_REDUCAO_BC_ST_ICMS.text:=Get_PERC_REDUCAO_BC_ST;
        EdtALIQUOTA_ST_ICMS.text:=Get_ALIQUOTA_ST;
        EdtIVA_ST_ICMS.text:=Get_IVA_ST;
        EdtPAUTA_ST_ICMS.text:=Get_PAUTA_ST;

        edtformula_BC_ICMS.text:=Get_FORMULA_BC;
        edtformula_BC_ST_ICMS.text:=Get_FORMULA_BC_ST;

        edtformula_valor_imposto_ICMS.text:=Get_formula_valor_imposto;
        edtformula_valor_imposto_ST_ICMS.text:=Get_formula_valor_imposto_ST;

//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFIMPOSTO_ICMS.TabelaParaControles: Boolean;
begin
     If (Self.ObjIMPOSTO_ICMS.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFIMPOSTO_ICMS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjIMPOSTO_ICMS=Nil)
     Then exit;

If (Self.ObjIMPOSTO_ICMS.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjIMPOSTO_ICMS.free;
end;

procedure TFIMPOSTO_ICMS.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFIMPOSTO_ICMS.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
    
     lbCodigo.Caption:='0';

      BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjIMPOSTO_ICMS.status:=dsInsert;
     edtcfop_ICMS.setfocus;
     btnovo.Visible:=false;
     btalterar.Visible:=False;
     btexcluir.Visible:=false;
     btrelatorios.Visible:=false;
     btopcoes.Visible:=false;
     btsair.Visible:=False;
     btpesquisar.visible:=False;

end;


procedure TFIMPOSTO_ICMS.btalterarClick(Sender: TObject);
begin
    If (Self.ObjIMPOSTO_ICMS.Status=dsinactive) and (lbCodigo.Caption<>'')
    Then Begin
              MensagemAviso('N�o � permitido a altera��o de Impostos');
                (*habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjIMPOSTO_ICMS.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtCFOP_ICMS.setfocus;
                *)
               btnovo.Visible:=false;
               btalterar.Visible:=False;
               btexcluir.Visible:=false;
               btrelatorios.Visible:=false;
               btopcoes.Visible:=false;
               btsair.Visible:=False;
               btpesquisar.visible:=False;
    End;


end;

procedure TFIMPOSTO_ICMS.btgravarClick(Sender: TObject);
begin

      if(edtCFOP_ICMS.Text='')
      then Exit;

     If Self.ObjIMPOSTO_ICMS.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjIMPOSTO_ICMS.salvar(true)=False)
     Then exit;

     lbCodigo.Caption:=Self.ObjIMPOSTO_ICMS.Get_codigo;
     habilita_botoes(Self);
     desabilita_campos(Self);
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;
     MostraQuantidadeCadastrada;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFIMPOSTO_ICMS.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjIMPOSTO_ICMS.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (Self.ObjIMPOSTO_ICMS.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjIMPOSTO_ICMS.exclui(lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFIMPOSTO_ICMS.btcancelarClick(Sender: TObject);
begin
     Self.ObjIMPOSTO_ICMS.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;
     

end;

procedure TFIMPOSTO_ICMS.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFIMPOSTO_ICMS.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjIMPOSTO_ICMS.Get_pesquisa,Self.ObjIMPOSTO_ICMS.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjIMPOSTO_ICMS.status<>dsinactive
                                  then exit;

                                  If (Self.ObjIMPOSTO_ICMS.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjIMPOSTO_ICMS.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFIMPOSTO_ICMS.LimpaLabels;
begin
//LIMPA LABELS
     LbNomeSTB_ICMS.caption:='';
     lbnomeCFOP_ICMS.caption:='';
     lbCodigo.Caption:='';
end;

procedure TFIMPOSTO_ICMS.FormShow(Sender: TObject);
begin
         limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);


     Try
        Self.ObjIMPOSTO_ICMS:=TObjIMPOSTO_ICMS.create(self);
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(imgrodape,'RODAPE');

     MostraQuantidadeCadastrada;
end;
procedure TFIMPOSTO_ICMS.edtSTB_ICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjIMPOSTO_ICMS.edtSTBkeydown(sender,key,shift,lbnomeSTB_ICMS);
end;
 
procedure TFIMPOSTO_ICMS.edtSTB_ICMSExit(Sender: TObject);
begin
    ObjIMPOSTO_ICMS.edtSTBExit(sender,lbnomeSTB_ICMS);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFIMPOSTO_ICMS.edtCFOP_ICMSExit(Sender: TObject);
begin
  ObjIMPOSTO_ICMS.edtCFOPExit(sender,lbnomeCFOP_ICMS);
end;

procedure TFIMPOSTO_ICMS.edtCFOP_ICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjIMPOSTO_ICMS.edtCFOPkeydown(sender,key,shift,lbnomeCFOP_ICMS);
end;

procedure TFIMPOSTO_ICMS.edtSTA_ICMSExit(Sender: TObject);
begin
    ObjIMPOSTO_ICMS.edtSTAExit(sender,lbnomeSTA_ICMS);
end;

procedure TFIMPOSTO_ICMS.edtSTA_ICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjIMPOSTO_ICMS.edtSTAkeydown(sender,key,shift,lbnomeSTA_ICMS);
end;

procedure TFIMPOSTO_ICMS.MostraQuantidadeCadastrada;
var
    Query:TIBQuery;
    cont:Integer;
begin
    try
      Query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    cont:=0;

    try
      with Query Do
      begin
          Close;
          SQL.Clear;
          sql.Add('select * from tabimposto_icms');
          Open;

          while not Eof do
          begin
              Inc(cont,1);
              Next;
          end;

          if(cont>1) then
                lb9.Caption:='Existem '+IntToStr(cont)+' icms cadastrados'
          else
          begin
                lb9.Caption:='Existe '+IntToStr(cont)+'  icms cadastrado';
          end;

      end;
    finally

    end;



end;

{r4mr}
procedure TFIMPOSTO_ICMS.edtCFOP_ICMSDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtCFOP_ICMSKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFIMPOSTO_ICMS.edtSTA_ICMSDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtSTA_ICMSKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFIMPOSTO_ICMS.edtSTB_ICMSDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtSTB_ICMSKeyDown(Sender, Key, Shift);
end;

end.

{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjIMPOSTO_ICMS.OBJETO.Get_Pesquisa,Self.ObjIMPOSTO_ICMS.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjIMPOSTO_ICMS.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjIMPOSTO_ICMS.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjIMPOSTO_ICMS.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
