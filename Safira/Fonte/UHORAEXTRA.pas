unit UHORAEXTRA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls,db, UObjHORAEXTRA,
  UessencialGlobal,uessenciallocal, Tabs;

type
  TFHORAEXTRA = class(TForm)
    btNovo: TSpeedButton;
    btFechar: TSpeedButton;
    btMinimizar: TSpeedButton;
    btSalvar: TSpeedButton;
    btAlterar: TSpeedButton;
    btCancelar: TSpeedButton;
    btSair: TSpeedButton;
    btRelatorio: TSpeedButton;
    btExcluir: TSpeedButton;
    btPesquisar: TSpeedButton;
    lbAjuda: TLabel;
    btAjuda: TSpeedButton;
    PainelExtra: TPanel;
    ImageGuiaPrincipal: TImage;
    ImageGuiaExtra: TImage;
    PainelPrincipal: TPanel;
    ImagePainelPrincipal: TImage;
    ImagePainelExtra: TImage;
    Guia: TTabSet;
    Notebook: TNotebook;
    Bevel: TBevel;
    LbCODIGO: TLabel;
    LbFUNCIONARIO: TLabel;
    LbNomeFUNCIONARIO: TLabel;
    LbVALOR: TLabel;
    LbDATA: TLabel;
    LbDATASALARIO: TLabel;
    EdtCODIGO: TEdit;
    EdtFUNCIONARIO: TEdit;
    EdtVALOR: TEdit;
    EdtDATA: TMaskEdit;
    EdtDATASALARIO: TMaskEdit;
//DECLARA COMPONENTES

    procedure FormCreate(Sender: TObject);
    Procedure PosicionaPaineis;
    Procedure PegaFiguras;
    Procedure ColocaAtalhoBotoes;
    Procedure PosicionaForm;
    procedure FormShow(Sender: TObject);
    procedure ImageGuiaPrincipalClick(Sender: TObject);
    procedure ImageGuiaExtraClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtFUNCIONARIOExit(Sender: TObject);
    procedure EdtFUNCIONARIOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FHORAEXTRA: TFHORAEXTRA;
  ObjHORAEXTRA:TObjHORAEXTRA;

implementation

uses Upesquisa;

{$R *.dfm}


procedure TFHORAEXTRA.FormCreate(Sender: TObject);
var
  Points: array [0..15] of TPoint;
  Regiao1:HRgn;
begin

Points[0].X :=305;      Points[0].Y := 22;
Points[1].X :=286;      Points[1].Y := 3;
Points[2].X :=41;       Points[2].Y := 3;
Points[3].X :=41;       Points[3].Y := 22;
Points[4].X :=28;       Points[4].Y := 35;
Points[5].X :=28;       Points[5].Y := 164;
Points[6].X :=41;       Points[6].Y := 177;
Points[7].X :=41;       Points[7].Y := 360;
Points[8].X :=62;       Points[8].Y := 381;
Points[9].X :=632;      Points[9].Y := 381;
Points[10].X :=647;     Points[10].Y := 396;
Points[11].X :=764;     Points[11].Y := 396;
Points[12].X :=764;     Points[12].Y := 95;
Points[13].X :=780;     Points[13].Y := 79;
Points[14].X :=780;     Points[14].Y := 22;
Points[15].X :=305;     Points[15].Y := 22;

Regiao1:=CreatePolygonRgn(Points, 16, WINDING);
SetWindowRgn(Self.Handle, regiao1, False);


limpaedit(Self);
Self.limpaLabels;
desabilita_campos(Self);
Guia.TabIndex:=0;

Try
   ObjHORAEXTRA:=TObjHORAEXTRA.create;
Except
      Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
      Self.close;
End;

end;

procedure TFHORAEXTRA.PosicionaPaineis;
begin
  With Self.PainelPrincipal do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelPrincipal do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  With Self.PainelExtra  do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelExtra  do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  Self.PainelPrincipal.Enabled:=true;
  Self.PainelPrincipal.Visible:=true;
  Self.PainelExtra.Enabled:=false;
  Self.PainelExtra.Visible:=false;
end;

procedure TFHORAEXTRA.FormShow(Sender: TObject);
begin
    PegaCorForm(Self);
    Self.PosicionaPaineis;
    Self.PosicionaForm;
    Self.PegaFiguras;
    Self.ColocaAtalhoBotoes;

end;

procedure TFHORAEXTRA.ImageGuiaPrincipalClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=true;
PainelPrincipal.Visible:=true;
PainelExtra.Enabled:=false;
PainelExtra.Visible:=false;
end;

procedure TFHORAEXTRA.ImageGuiaExtraClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=false;
PainelPrincipal.Visible:=false;
PainelExtra.Enabled:=true;
PainelExtra.Visible:=true;

end;

procedure TFHORAEXTRA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjHORAEXTRA=Nil)
     Then exit;

     If (ObjHORAEXTRA.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjHORAEXTRA.free;
    Action := caFree;
    Self := nil;
end;

procedure TFHORAEXTRA.PegaFiguras;
begin
    PegaFigura(ImageGuiaPrincipal,'guia_principal.jpg');
    PegaFigura(ImageGuiaExtra,'guia_extra.jpg');
    PegaFigura(ImagePainelPrincipal,'fundo_menu.jpg');
    PegaFigura(ImagePainelExtra,'fundo_menu.jpg');
    PegaFiguraBotao(btNovo,'botao_novo.bmp');
    PegaFiguraBotao(btSalvar,'botao_salvar.bmp');
    PegaFiguraBotao(btAlterar,'botao_alterar.bmp');
    PegaFiguraBotao(btCancelar,'botao_Cancelar.bmp');
    PegaFiguraBotao(btPesquisar,'botao_Pesquisar.bmp');
    PegaFiguraBotao(btExcluir,'botao_excluir.bmp');
    PegaFiguraBotao(btRelatorio,'botao_relatorios.bmp');
    PegaFiguraBotao(btSair,'botao_sair.bmp');
    PegaFiguraBotao(btFechar,'botao_close.bmp');
    PegaFiguraBotao(btMinimizar,'botao_minimizar.bmp');
    PegaFiguraBotao(btAjuda,'lampada.bmp');

end;

procedure TFHORAEXTRA.ColocaAtalhoBotoes;
begin
   Coloca_Atalho_Botoes(BtNovo, 'N');
   Coloca_Atalho_Botoes(BtSalvar, 'S');
   Coloca_Atalho_Botoes(BtAlterar, 'A');
   Coloca_Atalho_Botoes(BtCancelar, 'C');
   Coloca_Atalho_Botoes(BtExcluir, 'E');
   Coloca_Atalho_Botoes(BtPesquisar, 'P');
   Coloca_Atalho_Botoes(BtRelatorio, 'R');
   Coloca_Atalho_Botoes(BtSair, 'I');
end;

procedure TFHORAEXTRA.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjHORAEXTRA.Get_novocodigo;
     edtcodigo.enabled:=False;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjHORAEXTRA.status:=dsInsert;
     Guia.TabIndex:=0;
     EdtFUNCIONARIO.setfocus;

end;

procedure TFHORAEXTRA.btSalvarClick(Sender: TObject);
begin

     If ObjHORAEXTRA.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjHORAEXTRA.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjHORAEXTRA.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     EdtCODIGO.Text:=ObjHORAEXTRA.get_codigo;
     
     if (ObjHORAEXTRA.LocalizaCodigo(EdtCODIGO.Text)=True)
     Then Begin
               ObjHORAEXTRA.TabelaparaObjeto;
               Self.TabelaParaControles;
     End;
     //Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFHORAEXTRA.btAlterarClick(Sender: TObject);
begin
    If (ObjHORAEXTRA.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjHORAEXTRA.Status:=dsEdit;
                guia.TabIndex:=0;
                EdtFUNCIONARIO.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;

end;

procedure TFHORAEXTRA.btCancelarClick(Sender: TObject);
begin
     ObjHORAEXTRA.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFHORAEXTRA.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjHORAEXTRA.Get_pesquisa,ObjHORAEXTRA.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjHORAEXTRA.status<>dsinactive
                                  then exit;

                                  If (ObjHORAEXTRA.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjHORAEXTRA.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFHORAEXTRA.btExcluirClick(Sender: TObject);
begin
     If (ObjHORAEXTRA.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjHORAEXTRA.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjHORAEXTRA.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFHORAEXTRA.btRelatorioClick(Sender: TObject);
begin
    ObjHORAEXTRA.Imprime(edtcodigo.text);;
end;

procedure TFHORAEXTRA.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFHORAEXTRA.PosicionaForm;
begin
    Self.Caption:='      '+Self.Caption;
    Self.Left:=-18;
    Self.Top:=0;
end;
procedure TFHORAEXTRA.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFHORAEXTRA.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFHORAEXTRA.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
end;

procedure TFHORAEXTRA.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFHORAEXTRA.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjHORAEXTRA do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        FUNCIONARIO.Submit_codigo(edtFUNCIONARIO.text);
        Submit_VALOR(edtVALOR.text);
        Submit_DATA(edtDATA.text);
        Submit_DATASALARIO(edtDATASALARIO.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFHORAEXTRA.LimpaLabels;
begin
//LIMPA LABELS
     LbNomeFUNCIONARIO.caption:='';
end;

function TFHORAEXTRA.ObjetoParaControles: Boolean;
begin
  Try
     With ObjHORAEXTRA do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtFUNCIONARIO.text:=FUNCIONARIO.Get_codigo;
        LbNomeFUNCIONARIO.caption:=FUNCIONARIO.get_nome;
        EdtVALOR.text:=Get_VALOR;
        EdtDATA.text:=Get_DATA;
        EdtDATASALARIO.text:=Get_DATASALARIO;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFHORAEXTRA.TabelaParaControles: Boolean;
begin
     If (ObjHORAEXTRA.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFHORAEXTRA.EdtFUNCIONARIOExit(Sender: TObject);
begin
     ObjHORAEXTRA.EdtFUNCIONARIOExit(sender,LbNomeFUNCIONARIO);
end;

procedure TFHORAEXTRA.EdtFUNCIONARIOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjHORAEXTRA.EdtFUNCIONARIOKeyDown(sender,key,shift,LbNomeFUNCIONARIO);
end;

end.
