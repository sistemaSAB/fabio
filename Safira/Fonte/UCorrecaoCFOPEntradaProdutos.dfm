object FCorrecaoCFOPEntradaProdutos: TFCorrecaoCFOPEntradaProdutos
  Left = 374
  Top = 142
  Width = 1024
  Height = 768
  Caption = 'Corre'#231#227'o de CFOPs entrada de produtos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelTopo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 49
    Align = alTop
    TabOrder = 0
    DesignSize = (
      1008
      49)
    object Entrada: TLabel
      Left = 24
      Top = 17
      Width = 70
      Height = 18
      Caption = 'Entrada'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Courier New'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtEntrada: TEdit
      Left = 104
      Top = 13
      Width = 121
      Height = 26
      Color = clSkyBlue
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Text = 'edtEntrada'
      OnKeyDown = edtEntradaKeyDown
    end
    object btOK: TButton
      Left = 912
      Top = 13
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&OK'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btOKClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 49
    Width = 1008
    Height = 681
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 793
      Top = 298
      Width = 8
      Height = 350
    end
    object PanelProdutos: TPanel
      Left = 0
      Top = 177
      Width = 1008
      Height = 121
      Align = alTop
      TabOrder = 0
      object Label3: TLabel
        Left = 8
        Top = 15
        Width = 80
        Height = 18
        AutoSize = False
        Caption = 'Produto:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbProd_Descricao: TLabel
        Left = 235
        Top = 15
        Width = 766
        Height = 18
        AutoSize = False
        Caption = 'Descri'#231#227'o Produto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = lbProd_CodigoClick
        OnMouseMove = lbProd_CodigoMouseMove
        OnMouseLeave = lbProd_CodigoMouseLeave
      end
      object lbProd_Codigo: TLabel
        Left = 91
        Top = 15
        Width = 142
        Height = 18
        AutoSize = False
        Caption = 'Codigo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = lbProd_CodigoClick
        OnMouseMove = lbProd_CodigoMouseMove
        OnMouseLeave = lbProd_CodigoMouseLeave
      end
      object lbProd_CFOP: TLabel
        Left = 141
        Top = 87
        Width = 52
        Height = 18
        AutoSize = False
        Caption = '5404'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object lbProd_PorcentagemReducao: TLabel
        Left = 835
        Top = 63
        Width = 130
        Height = 18
        Caption = 'Red. Base....'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
      end
      object lbProd_SituacaoEntrada: TLabel
        Left = 502
        Top = 63
        Width = 170
        Height = 18
        Caption = 'Sit. Entrada.....'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
      end
      object lbProd_IPI: TLabel
        Left = 93
        Top = 63
        Width = 150
        Height = 18
        Caption = 'Ipi (%)........'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
      end
      object lbProd_ValorPago: TLabel
        Left = 120
        Top = 39
        Width = 170
        Height = 18
        Caption = 'Valor Pago.......'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
      end
      object lbProd_Quantidade: TLabel
        Left = 433
        Top = 39
        Width = 90
        Height = 18
        Caption = '999999,99'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
      end
      object lbProd_Desconto: TLabel
        Left = 659
        Top = 39
        Width = 120
        Height = 18
        Caption = 'Desconto....'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 8
        Top = 39
        Width = 110
        Height = 18
        Caption = 'Valor Pago:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 8
        Top = 63
        Width = 80
        Height = 18
        Caption = 'IPI (%):'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 8
        Top = 87
        Width = 130
        Height = 18
        Caption = 'CFOP Entrada:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object Label20: TLabel
        Left = 320
        Top = 39
        Width = 110
        Height = 18
        Caption = 'Quantidade:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label22: TLabel
        Left = 560
        Top = 39
        Width = 90
        Height = 18
        Caption = 'Desconto:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label27: TLabel
        Left = 320
        Top = 63
        Width = 170
        Height = 18
        Caption = 'Situa'#231#227'o Entrada:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label28: TLabel
        Left = 720
        Top = 63
        Width = 110
        Height = 18
        Caption = 'Redu'#231#227'o BC:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object strgProdutosEntrada: TStringGrid
      Left = 0
      Top = 298
      Width = 793
      Height = 350
      Align = alLeft
      RowCount = 2
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnDblClick = strgProdutosEntradaDblClick
      OnKeyDown = strgProdutosEntradaKeyDown
    end
    object strgNovoCFOP: TStringGrid
      Left = 801
      Top = 298
      Width = 207
      Height = 350
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object Panel2: TPanel
      Left = 0
      Top = 648
      Width = 1008
      Height = 33
      Align = alBottom
      TabOrder = 3
      object btGravar: TButton
        Left = 912
        Top = 5
        Width = 75
        Height = 25
        Caption = '&Gravar'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = btGravarClick
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 1008
      Height = 177
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 4
      object TabSheet1: TTabSheet
        Caption = 'Dados Entrada'
        object Label4: TLabel
          Left = 26
          Top = 119
          Width = 130
          Height = 18
          AutoSize = False
          Caption = 'Valor do IPI:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label12: TLabel
          Left = 301
          Top = 7
          Width = 130
          Height = 18
          AutoSize = False
          Caption = 'Data Emiss'#227'o:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbEnt_DataEmissao: TLabel
          Left = 437
          Top = 7
          Width = 100
          Height = 18
          AutoSize = False
          Caption = '30/12/2010'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label17: TLabel
          Left = 554
          Top = 7
          Width = 50
          Height = 18
          AutoSize = False
          Caption = 'Data:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbEnt_Data: TLabel
          Left = 610
          Top = 7
          Width = 100
          Height = 18
          AutoSize = False
          Caption = '30/12/2010'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label19: TLabel
          Left = 26
          Top = 35
          Width = 120
          Height = 18
          AutoSize = False
          Caption = 'Nota Fiscal:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbEnt_NF: TLabel
          Left = 154
          Top = 35
          Width = 180
          Height = 18
          AutoSize = False
          Caption = 'Codigo Nota Fiscal'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object lbEnt_BaseCalculo: TLabel
          Left = 217
          Top = 63
          Width = 280
          Height = 18
          AutoSize = False
          Caption = 'Base C'#225'lc. ICMS...........'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object lbEnt_ValorICMS: TLabel
          Left = 621
          Top = 63
          Width = 280
          Height = 18
          AutoSize = False
          Caption = 'Valor ICMS.........'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object lbEnt_BaseCalculoST: TLabel
          Left = 268
          Top = 91
          Width = 280
          Height = 18
          AutoSize = False
          Caption = 'Base Subst.........'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object lbEnt_ValorICMSST: TLabel
          Left = 621
          Top = 91
          Width = 280
          Height = 18
          AutoSize = False
          Caption = 'Valor Icms Subst.'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object lbEnt_ModeloNF: TLabel
          Left = 479
          Top = 35
          Width = 110
          Height = 18
          AutoSize = False
          Caption = 'Modelo NF..'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object lbEnt_ValorIPI: TLabel
          Left = 157
          Top = 119
          Width = 280
          Height = 18
          AutoSize = False
          Caption = 'Valor do IPI................'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label21: TLabel
          Left = 370
          Top = 35
          Width = 100
          Height = 18
          AutoSize = False
          Caption = 'Modelo NF:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label25: TLabel
          Left = 26
          Top = 63
          Width = 180
          Height = 18
          AutoSize = False
          Caption = 'Base C'#225'lculo ICMS:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label26: TLabel
          Left = 26
          Top = 91
          Width = 240
          Height = 18
          AutoSize = False
          Caption = 'Base C'#225'lculo Substituto:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label29: TLabel
          Left = 506
          Top = 63
          Width = 110
          Height = 18
          AutoSize = False
          Caption = 'Valor ICMS:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label13: TLabel
          Left = 506
          Top = 91
          Width = 110
          Height = 18
          AutoSize = False
          Caption = 'Valor ICMS:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 26
          Top = 7
          Width = 80
          Height = 18
          Caption = 'Entrada:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbEnt_Codigo: TLabel
          Left = 106
          Top = 7
          Width = 180
          Height = 18
          AutoSize = False
          Caption = 'Codigo Entrada'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Fornecedor'
        ImageIndex = 1
        object Label2: TLabel
          Left = 24
          Top = 11
          Width = 110
          Height = 18
          AutoSize = False
          Caption = 'Fornecedor:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbCodigoFornecedor: TLabel
          Left = 144
          Top = 11
          Width = 113
          Height = 18
          AutoSize = False
          Caption = 'Codigo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = lbCodigoFornecedorClick
          OnMouseMove = lbCodigoFornecedorMouseMove
          OnMouseLeave = lbCodigoFornecedorMouseLeave
        end
        object Label5: TLabel
          Left = 24
          Top = 67
          Width = 90
          Height = 18
          AutoSize = False
          Caption = 'Endere'#231'o:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbendereco: TLabel
          Left = 128
          Top = 67
          Width = 433
          Height = 18
          AutoSize = False
          Caption = 'Endere'#231'o:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 24
          Top = 95
          Width = 70
          Height = 18
          AutoSize = False
          Caption = 'Cidade:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbRazaoSocial: TLabel
          Left = 258
          Top = 11
          Width = 375
          Height = 18
          AutoSize = False
          Caption = 'Raz'#227'o Social'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = lbCodigoFornecedorClick
          OnMouseMove = lbCodigoFornecedorMouseMove
          OnMouseLeave = lbCodigoFornecedorMouseLeave
        end
        object lbCidade: TLabel
          Left = 99
          Top = 95
          Width = 462
          Height = 18
          AutoSize = False
          Caption = 'Cidade'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label24: TLabel
          Left = 651
          Top = 95
          Width = 39
          Height = 18
          AutoSize = False
          Caption = 'CEP:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label23: TLabel
          Left = 570
          Top = 67
          Width = 70
          Height = 18
          AutoSize = False
          Caption = 'Bairro:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbFantasia: TLabel
          Left = 610
          Top = 11
          Width = 367
          Height = 18
          AutoSize = False
          Caption = 'Fantasia'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = lbCodigoFornecedorClick
          OnMouseMove = lbCodigoFornecedorMouseMove
          OnMouseLeave = lbCodigoFornecedorMouseLeave
        end
        object lbBairro: TLabel
          Left = 650
          Top = 67
          Width = 231
          Height = 18
          AutoSize = False
          Caption = 'Bairro'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 569
          Top = 95
          Width = 33
          Height = 18
          AutoSize = False
          Caption = 'UF:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbUF: TLabel
          Left = 601
          Top = 95
          Width = 33
          Height = 18
          AutoSize = False
          Caption = 'MS'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object lbCEP: TLabel
          Left = 699
          Top = 95
          Width = 111
          Height = 18
          AutoSize = False
          Caption = '79.840.490'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label16: TLabel
          Left = 330
          Top = 39
          Width = 63
          Height = 18
          AutoSize = False
          Caption = 'IE/RG:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label11: TLabel
          Left = 25
          Top = 39
          Width = 96
          Height = 18
          AutoSize = False
          Caption = 'CNPJ/CPF:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbCNPJ: TLabel
          Left = 121
          Top = 39
          Width = 200
          Height = 18
          AutoSize = False
          Caption = '05.537.538/0001-53'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object lbIE: TLabel
          Left = 402
          Top = 39
          Width = 127
          Height = 18
          AutoSize = False
          Caption = '28.331.309-9'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label14: TLabel
          Left = 24
          Top = 123
          Width = 49
          Height = 18
          AutoSize = False
          Caption = 'Fone:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label15: TLabel
          Left = 382
          Top = 123
          Width = 43
          Height = 18
          AutoSize = False
          Caption = 'Fax:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbFone: TLabel
          Left = 80
          Top = 123
          Width = 289
          Height = 18
          AutoSize = False
          Caption = '67-3424-5874'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object lbFax: TLabel
          Left = 432
          Top = 123
          Width = 177
          Height = 18
          AutoSize = False
          Caption = '67-3424-1025'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
      end
    end
  end
end
