unit UGRUPOPERFILADO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjPERFILADO,
  UessencialGlobal, Tabs,UPLanodeContas;

type
  TFGRUPOPERFILADO = class(TForm)
    btPrimeiro: TSpeedButton;
    btAnterior: TSpeedButton;
    btProximo: TSpeedButton;
    btUltimo: TSpeedButton;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigoGrupoPerfilados: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    lbLbNome: TLabel;
    edtNome: TEdit;
    lb2: TLabel;
    edtPlanodeContas: TEdit;
    lbNomePlanoDeContas: TLabel;
    grp1: TGroupBox;
    lbLbPorcentagemFornecido: TLabel;
    lbLbPorcentagemRetirado: TLabel;
    lbLbPorcentagemInstalado: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    lb5: TLabel;
    edtPorcentagemInstalado: TEdit;
    edtPorcentagemFornecido: TEdit;
    edtPorcentagemRetirado: TEdit;
//DECLARA COMPONENTES

    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton1Click(Sender: TObject);
    procedure edtPlanodeContasExit(Sender: TObject);
    procedure edtPlanodeContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btPrimeiroClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btUltimoClick(Sender: TObject);
    procedure edtPlanodeContasDblClick(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
    procedure edtPorcentagemInstaladoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemFornecidoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPorcentagemRetiradoKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtPlanodeContasKeyPress(Sender: TObject; var Key: Char);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FGRUPOPERFILADO: TFGRUPOPERFILADO;
  ObjPERFILADO:TObjPERFILADO;

implementation

uses Upesquisa, UessencialLocal, UMenuRelatorios, UDataModulo,
  UescolheImagemBotao;

{$R *.dfm}




procedure TFGRUPOPERFILADO.FormShow(Sender: TObject);
begin

    limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     ColocaUpperCaseEdit(Self);

     Try
        ObjPERFILADO:=TObjPERFILADO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     habilita_botoes(Self);
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE GRUPO DE PERFILADO')=False)
     Then desab_botoes(Self);
    FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
    FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
    lbCodigoGrupoPerfilados.caption:='';

    if(Tag<>0)
    then begin
        if(ObjPERFILADO.GrupoPerfilado.LocalizaCodigo(IntToStr(tag))=True)
        then begin
              ObjPERFILADO.GrupoPerfilado.TabelaparaObjeto;
              Self.ObjetoParaControles;

        end;
    end;

end;


procedure TFGRUPOPERFILADO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjPERFILADO.GrupoPerfilado=Nil)
     Then exit;

     If (ObjPERFILADO.GrupoPerfilado.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjPERFILADO.free;

end;

procedure TFGRUPOPERFILADO.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     lbCodigoGrupoPerfilados.caption:='0';
     //edtcodigo.text:=ObjPerfilado.GrupoPerfilado.Get_novocodigo;

     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjPerfilado.GrupoPerfilado.status:=dsInsert;
     EdtNome.setfocus;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

end;

procedure TFGRUPOPERFILADO.btSalvarClick(Sender: TObject);
begin

     If ObjPerfilado.GrupoPerfilado.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjPerfilado.GrupoPerfilado.salvar(true)=False)
     Then exit;

     lbCodigoGrupoPerfilados.caption:=ObjPerfilado.GrupoPerfilado.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFGRUPOPERFILADO.btAlterarClick(Sender: TObject);
begin
    If (ObjPerfilado.GrupoPerfilado.Status=dsinactive) and (lbCodigoGrupoPerfilados.caption<>'')
    Then Begin
                habilita_campos(Self);
                ObjPerfilado.GrupoPerfilado.Status:=dsEdit;
                edtNome.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btnovo.Visible :=false;
                 btalterar.Visible:=false;
                 btpesquisar.Visible:=false;
                 btrelatorio.Visible:=false;
                 btexcluir.Visible:=false;
                 btsair.Visible:=false;
                 btopcoes.Visible :=false;
          End;

end;

procedure TFGRUPOPERFILADO.btCancelarClick(Sender: TObject);
begin
    ObjPerfilado.GrupoPerfilado.cancelar;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     lbCodigoGrupoPerfilados.caption:='';
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFGRUPOPERFILADO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjPerfilado.GrupoPerfilado.Get_pesquisa,ObjPerfilado.GrupoPerfilado.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjPerfilado.GrupoPerfilado.status<>dsinactive
                                  then exit;

                                  If (ObjPerfilado.GrupoPerfilado.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjPerfilado.GrupoPerfilado.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFGRUPOPERFILADO.btExcluirClick(Sender: TObject);
begin
     If (ObjPerfilado.GrupoPerfilado.status<>dsinactive) or (lbCodigoGrupoPerfilados.caption='')
     Then exit;

     If (ObjPerfilado.GrupoPerfilado.LocalizaCodigo(lbCodigoGrupoPerfilados.caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjPerfilado.GrupoPerfilado.exclui(lbCodigoGrupoPerfilados.caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFGRUPOPERFILADO.btRelatorioClick(Sender: TObject);
begin
//    ObjPerfilado.GrupoPerfilado.Imprime;
end;

procedure TFGRUPOPERFILADO.btSairClick(Sender: TObject);
begin
    Self.close;
end;


procedure TFGRUPOPERFILADO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFGRUPOPERFILADO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFGRUPOPERFILADO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;


end;

procedure TFGRUPOPERFILADO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFGRUPOPERFILADO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjPerfilado.GrupoPerfilado do
    Begin
        Submit_Codigo(lbCodigoGrupoPerfilados.caption);
        Submit_Nome(edtNome.text);
        Submit_PorcentagemInstalado(EdtPorcentagemInstalado.Text);
        Submit_PorcentagemFornecido(EdtPorcentagemFornecido.Text);
        Submit_PorcentagemRetirado(EdtPorcentagemRetirado.Text);
        PlanoDeContas.Submit_CODIGO(EdtPlanodeContas.Text);

        result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFGRUPOPERFILADO.LimpaLabels;
begin
     lbNomePlanoDeContas.Caption:='';
end;

function TFGRUPOPERFILADO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjPerfilado.GrupoPerfilado do
     Begin
        lbCodigoGrupoPerfilados.caption:=Get_Codigo;
        EdtNome.text:=Get_Nome;
        EdtPorcentagemInstalado.Text:=Get_PorcentagemInstalado;
        EdtPorcentagemFornecido.Text:=Get_PorcentagemFornecido;
        EdtPorcentagemRetirado.Text:=Get_PorcentagemRetirado;
        EdtPlanodeContas.Text:=PlanoDeContas.Get_CODIGO;
        lbNomePlanoDeContas.Caption:='TIPO = '+ObjPerfilado.GrupoPerfilado.PlanoDeContas.Get_Tipo+' | ';
        lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' CLASSIF. = '+ObjPerfilado.GrupoPerfilado.PlanoDeContas.Get_Classificacao+' | ';
        lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' NOME = '+ObjPerfilado.GrupoPerfilado.PlanoDeContas.Get_Nome;
        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFGRUPOPERFILADO.TabelaParaControles: Boolean;
begin
     If (ObjPerfilado.GrupoPerfilado.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;


procedure TFGRUPOPERFILADO.SpeedButton1Click(Sender: TObject);
begin
     With FmenuRelatorios do
     Begin
          //era no titulo antes
          NomeObjeto:='UObjGrupoPerfilado';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Atualiza Margens');
                Items.Add('Atualiza Pre�os')
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0 :  ObjPerfilado.AtualizaMargens(lbCodigoGrupoPerfilados.caption);
                1 :  ObjPERFILADO.AtualizaPrecos(lbCodigoGrupoPerfilados.caption);
          End;
     end;

end;


procedure TFGRUPOPERFILADO.edtPlanodeContasExit(Sender: TObject);
begin
    ObjPerfilado.GrupoPerfilado.EdtGrupoPlanoDeContasExit(Sender, lbNomePlanoDeContas);
    lbNomePlanoDeContas.Caption:='TIPO = '+ObjPerfilado.GrupoPerfilado.PlanoDeContas.Get_Tipo+' | ';
    lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' CLASSIF. = '+ObjPerfilado.GrupoPerfilado.PlanoDeContas.Get_Classificacao+' | ';
    lbNomePlanoDeContas.Caption:=lbNomePlanoDeContas.Caption+' NOME = '+ObjPerfilado.GrupoPerfilado.PlanoDeContas.Get_Nome;

end;

procedure TFGRUPOPERFILADO.edtPlanodeContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    ObjPerfilado.GrupoPerfilado.EdtGrupoPlanoDeContasKeyDown(Sender, Key, Shift, lbNomePlanoDeContas);
end;

procedure TFGRUPOPERFILADO.btPrimeiroClick(Sender: TObject);
begin
    if  (ObjPerfilado.GrupoPerfilado.PrimeiroRegistro = false)then
    exit;

    ObjPerfilado.GrupoPerfilado.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOPERFILADO.btAnteriorClick(Sender: TObject);
begin
    if (lbCodigoGrupoPerfilados.caption='')then
    lbCodigoGrupoPerfilados.caption:='0';

    if  (ObjPerfilado.GrupoPerfilado.RegistoAnterior(StrToInt(lbCodigoGrupoPerfilados.caption)) = false)then
    exit;

    ObjPerfilado.GrupoPerfilado.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOPERFILADO.btProximoClick(Sender: TObject);
begin
    if( lbCodigoGrupoPerfilados.caption='')then
    lbCodigoGrupoPerfilados.caption:='0';

    if  (ObjPerfilado.GrupoPerfilado.ProximoRegisto(StrToInt(lbCodigoGrupoPerfilados.caption)) = false)then
    exit;

    ObjPerfilado.GrupoPerfilado.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOPERFILADO.btUltimoClick(Sender: TObject);
begin
    if  (ObjPerfilado.GrupoPerfilado.UltimoRegistro = false)then
    exit;

    ObjPerfilado.GrupoPerfilado.TabelaparaObjeto;
    Self.ObjetoParaControles;

end;

procedure TFGRUPOPERFILADO.edtPlanodeContasDblClick(Sender: TObject);
var
   FpesquisaLocal:Tfpesquisa;
   FplanoContas:TFplanodeContas;
begin
     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FplanoContas:=TFplanodeContas.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from tabplanodecontas','',FplanoContas)=True)
            Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then
                        Begin
                                 EdtPlanodeContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbNomePlanodeContas.Caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;

                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FplanoContas);

     End;
end;

procedure TFGRUPOPERFILADO.btopcoesClick(Sender: TObject);
begin
  With FmenuRelatorios do
  Begin
    //era no titulo antes
    NomeObjeto:='UOBJGRUPOPERFILADO';

    With RgOpcoes do
    Begin
      items.clear;
      items.add('Atualiza Margens');
      Items.Add('Atualiza Pre�o de Custo');
    End;

    showmodal;

    if (Tag=0)//indica botao cancel ou fechar
    Then exit;

    Case RgOpcoes.ItemIndex of
      0 :  ObjPERFILADO.AtualizaMargens(lbCodigoGrupoPerfilados.Caption);
      1 :  ObjPERFILADO.AtualizaPrecos(lbCodigoGrupoPerfilados.Caption);
    End;
  end;

end;

procedure TFGRUPOPERFILADO.edtPorcentagemInstaladoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFGRUPOPERFILADO.edtPorcentagemFornecidoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFGRUPOPERFILADO.edtPorcentagemRetiradoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

procedure TFGRUPOPERFILADO.edtPlanodeContasKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8,','])
     Then key:=#0;
end;

end.
