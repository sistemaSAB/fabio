unit UBACKUP;

interface

uses
  filectrl,Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjhorariosbackup,
  jpeg, Grids, DBGrids, IBServices, UfrScriptSqlLOTE,zlib, ZipMstr;

type
  TFBACKUP = class(TForm)
    Guia: TTabbedNotebook;
    LBDias_backup: TListBox;
    LB_Horarios: TListBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdtHorario_backup: TMaskEdit;
    BtAdicionarHorario_backup: TBitBtn;
    BtExcluirHorario_backup: TBitBtn;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    DBGrid: TDBGrid;
    Panel2: TPanel;
    Edtsenha: TEdit;
    Lbsenha: TLabel;
    Edtusuario: TEdit;
    Lbusuario: TLabel;
    Edtdestino: TEdit;
    Lbdestino: TLabel;
    LbOrigem: TLabel;
    EdtNome: TEdit;
    LbNome: TLabel;
    EdtCODIGO: TEdit;
    LbCODIGO: TLabel;
    EdtOrigem: TEdit;
    btprocurarorigem: TBitBtn;
    btprocurardestino: TBitBtn;
    OpenDialog: TOpenDialog;
    check_verbose: TCheckBox;
    btexecutarbackup: TBitBtn;
    btrestaurabackup: TBitBtn;
    IBRestoreService1: TIBRestoreService;
    SaveDialog: TSaveDialog;
    DriveComboBoxDescompacta: TDriveComboBox;
    DirectoryListBoxDescompacta: TDirectoryListBox;
    FileListBoxDescompacta: TFileListBox;
    btdescompactatodos: TButton;
    btdescompactaselecionado: TButton;
    Label1: TLabel;
    edtcliente_site: TEdit;
    FrScriptSqlLOTE1: TFrScriptSqlLOTE;
    btselecionaparametro: TButton;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridDblClick(Sender: TObject);
    procedure DBGridKeyPress(Sender: TObject; var Key: Char);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure LBDias_backupClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtAdicionarHorario_backupClick(Sender: TObject);
    procedure BtExcluirHorario_backupClick(Sender: TObject);
    procedure LBDias_backupKeyPress(Sender: TObject; var Key: Char);
    procedure LB_HorariosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btprocurarorigemClick(Sender: TObject);
    procedure btprocurardestinoClick(Sender: TObject);
    procedure btexecutarbackupClick(Sender: TObject);
    procedure btrestaurabackupClick(Sender: TObject);
    procedure btdescompactatodosClick(Sender: TObject);
    procedure btdescompactaselecionadoClick(Sender: TObject);
    procedure btselecionaparametroClick(Sender: TObject);
    procedure FrScriptSqlLOTE1btexecutasqlloteClick(Sender: TObject);

  private
         Objhorariosbackup:TObjhorariosbackup;
         Ptemp:TstringList;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure PesquisaBackups;
         Procedure GravaBackup;
         Function ExtraiBackup:boolean;
    { Private declarations }
  public
    { Public declarations }

        Procedure PassaObjeto(temp:TObjhorariosbackup);


  end;

var
  FBACKUP: TFBACKUP;


implementation

uses UUtils, UmostraBackup, Ulogin, UpedeSenha, UobjBACKUP, UdataMOdulo,
  UobjParametrosAvulso;





{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFBACKUP.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.Objhorariosbackup.backup do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_Nome(edtNome.text);
        Submit_Origem(edtOrigem.text);
        Submit_destino(edtdestino.text);
        Submit_usuario(edtusuario.text);
        Submit_senha(edtsenha.text);
        if (check_verbose.checked=true)
        Then Submit_verbose('S')
        else Submit_verbose('N');

        Submit_cliente_site(edtcliente_site.Text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFBACKUP.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.Objhorariosbackup.backup do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNome.text:=Get_Nome;
        EdtOrigem.text:=Get_Origem;
        Edtdestino.text:=Get_destino;
        Edtusuario.text:=Get_usuario;
        Edtsenha.text:=Get_senha;

        if (Get_verbose='S')
        Then check_verbose.checked:=True
        Else check_verbose.checked:=False;

        edtcliente_site.Text:=Get_cliente_site;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFBACKUP.TabelaParaControles: Boolean;
begin
     If (Self.Objhorariosbackup.backup.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFBACKUP.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Begin
                if not(FrScriptSqlLOTE1.memo_script.Focused)
                Then Perform(Wm_NextDlgCtl,0,0);
      End;
end;



procedure TFBACKUP.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.Objhorariosbackup.backup.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btprocurarorigem.enabled:=True;
     btprocurardestino.enabled:=True;

     Self.Objhorariosbackup.backup.status:=dsInsert;
     Guia.pageindex:=0;
     edtNome.setfocus;

end;


procedure TFBACKUP.btalterarClick(Sender: TObject);
begin
    If (Self.Objhorariosbackup.backup.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.Objhorariosbackup.backup.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btprocurarorigem.enabled:=True;
                btprocurardestino.enabled:=True;
                btselecionaparametro.Enabled:=true;
                edtNome.setfocus;
          End;
end;

procedure TFBACKUP.btgravarClick(Sender: TObject);
begin

     If Self.Objhorariosbackup.backup.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.Objhorariosbackup.backup.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.Objhorariosbackup.backup.Get_codigo;
     habilita_botoes(Self);
     btprocurarorigem.enabled:=False;
     btprocurardestino.enabled:=False;
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Self.PesquisaBackups;


end;

procedure TFBACKUP.btexcluirClick(Sender: TObject);
begin
     If (Self.Objhorariosbackup.backup.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.Objhorariosbackup.backup.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;
     //self.Objhorariosbackup.Backup.Commit;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.Objhorariosbackup.exclui_backup(edtcodigo.text,True)=False)
     Then exit;



     limpaedit(Self);
     btprocurarorigem.enabled:=False;
     btprocurardestino.enabled:=False;
     Self.PesquisaBackups;


end;

procedure TFBACKUP.btcancelarClick(Sender: TObject);
begin
     Self.Objhorariosbackup.backup.cancelar;

     limpaedit(Self);
     desabilita_campos(Self);
     habilita_botoes(Self);

     btprocurarorigem.enabled:=False;
     btprocurardestino.enabled:=False;

end;

procedure TFBACKUP.btsairClick(Sender: TObject);
begin
    Close;
end;



procedure TFBACKUP.PassaObjeto(temp: TObjhorariosbackup);
begin
     Self.Objhorariosbackup:=temp;
     Self.Objhorariosbackup.backup:=temp.backup;
     Self.DBGrid.datasource:=temp.backup.objdatasource;
end;

procedure TFBACKUP.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     desabilita_campos(Self);
     Guia.PageIndex:=0;
     btprocurarorigem.enabled:=false;
     btprocurardestino.enabled:=false;
     btselecionaparametro.Enabled:=False;

     Self.PesquisaBackups;
     Try
        Ptemp:=TstringList.create;
     Except

     End;

     if (Self.DBGrid.DataSource.DataSet.recordcount<=0)
     Then exit;

     Self.DBGrid.SetFocus;
     Self.DBGridDblClick(sender);
end;

procedure TFBACKUP.PesquisaBackups;
begin
     Self.Objhorariosbackup.backup.Pesquisabackups;
end;

procedure TFBACKUP.DBGridDblClick(Sender: TObject);
begin
     if (self.Objhorariosbackup.backup.status<>dsinactive)
     then exit;

     if (Self.DBGrid.DataSource.DataSet.RecordCount=0)
     then exit;

     if (Self.Objhorariosbackup.backup.LocalizaCodigo(Self.DBGrid.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then exit;

     Self.Objhorariosbackup.backup.TabelaparaObjeto;
     //self.Objhorariosbackup.Backup.Commit;
     Self.ObjetoParaControles;
end;

procedure TFBACKUP.DBGridKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     then Begin
               Self.DBGridDblClick(sender);
               Self.DBGrid.SetFocus;
     End;
end;

procedure TFBACKUP.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     if newtab=1
     then Begin
               if (edtcodigo.text='')
               or (self.Objhorariosbackup.backup.Status<>dsinactive)
               Then Begin
                          AllowChange:=False;
                          exit;
               End;
               EdtHorario_backup.Enabled:=true;
               LBDias_backup.Enabled:=true;
               LB_Horarios.Enabled:=true;
               LBDias_backup.ItemIndex:=0;
               LBDias_backupClick(sender);
     End;

     if newtab=2
     then Begin
               if (edtcodigo.text='')
               Then Begin
                          AllowChange:=False;
                          exit;
               End;
               if (Objhorariosbackup.backup.LocalizaCodigo(edtcodigo.text)=False)
               Then Begin
                         mensagemErro('Registro n�o localizado');
                         AllowChange:=False;
                         exit;
               End;
               Objhorariosbackup.backup.TabelaparaObjeto;
               //Objhorariosbackup.Backup.Commit;

               Fpedesenha.showmodal;

               if (Fpedesenha.tag=0)
               Then Begin
                         AllowChange:=False;
                         exit;
               End;
               
               if (Fpedesenha.edtsenha.text<>Objhorariosbackup.backup.Get_senha)
               then Begin
                         MensagemErro('Senha Inv�lida');
                         allowchange:=False;
                         exit;
               End;

               if (Self.FrScriptSqlLOTE1.InicializaFrame(Objhorariosbackup.backup.Get_Origem,Objhorariosbackup.backup.Get_usuario,Objhorariosbackup.backup.Get_senha)=False)
               Then Begin
                         mensagemerro('N�o foi poss�vel conectar na base');
                         AllowChange:=False;
                         exit;
               End;
               
     End;
     if NewTab = 0 then
       Self.PesquisaBackups;
end;

procedure TFBACKUP.LBDias_backupClick(Sender: TObject);
begin
     lb_horarios.items.clear;
     if (Self.Objhorariosbackup.Localizabackup(EdtCODIGO.text)=false)
     Then exit;
     Self.Objhorariosbackup.TabelaparaObjeto;
     //Objhorariosbackup.Commit;

     case LBDias_backup.ItemIndex of
        0: ExplodeStr(Self.Objhorariosbackup.Get_Domingo,ptemp,'&','');
        1: ExplodeStr(Self.Objhorariosbackup.Get_Segunda,ptemp,'&','');
        2: ExplodeStr(Self.Objhorariosbackup.Get_Terca,ptemp,'&','');
        3: ExplodeStr(Self.Objhorariosbackup.Get_Quarta,ptemp,'&','');
        4: ExplodeStr(Self.Objhorariosbackup.Get_Quinta,ptemp,'&','');
        5: ExplodeStr(Self.Objhorariosbackup.Get_Sexta,ptemp,'&','');
        6: ExplodeStr(Self.Objhorariosbackup.Get_Sabado,ptemp,'&','');
     End;

     LB_Horarios.items.text:=ptemp.text;
end;

procedure TFBACKUP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Freeandnil(Ptemp);
end;

procedure TFBACKUP.BtAdicionarHorario_backupClick(Sender: TObject);
begin

    if (trim(comebarra(EdtHorario_backup.text))<>'')
    Then Begin
              Try
                 strtotime(EdtHorario_backup.Text);
              Except
                 Messagedlg('Hor�rio Inv�lido',mterror,[mbok],0);
                 exit;
              End;
              LB_Horarios.Items.add(EdtHorario_backup.text);

              Self.GravaBackup;
    End;

end;

procedure TFBACKUP.BtExcluirHorario_backupClick(Sender: TObject);
begin
     if (LB_Horarios.items.count=0)
     Then exit;

     if (LB_Horarios.itemindex<0)
     Then exit;

     LB_Horarios.DeleteSelected;
     Self.GravaBackup;

end;

procedure TFBACKUP.GravaBackup;
var
temp:string;
cont:integer;
begin
     if (LB_Horarios.Items.Count=0)
     Then Temp:=''
     Else Begin
             Temp:=LB_Horarios.Items[0];
             for cont:=1 to LB_Horarios.Items.Count-1 do
             Begin
                  Temp:=temp+'&'+LB_Horarios.Items[cont];
             End;
     End;

     if (Self.Objhorariosbackup.Localizabackup(EdtCODIGO.text)=true)
     Then Begin
               Self.Objhorariosbackup.TabelaparaObjeto;
               //Objhorariosbackup.Commit;
               Self.Objhorariosbackup.Status:=dsedit;
     End
     Else Begin
               Self.Objhorariosbackup.ZerarTabela;
               Self.Objhorariosbackup.backup.submit_codigo(edtcodigo.text);
               Self.Objhorariosbackup.submit_codigo('0');
               Self.Objhorariosbackup.Status:=dsInsert;
     End;

     case LBDias_backup.ItemIndex of

         0:Self.Objhorariosbackup.Submit_Domingo(temp);
         1:Self.Objhorariosbackup.Submit_segunda(temp);
         2:Self.Objhorariosbackup.Submit_terca(temp);
         3:Self.Objhorariosbackup.Submit_quarta(temp);
         4:Self.Objhorariosbackup.Submit_quinta(temp);
         5:Self.Objhorariosbackup.Submit_sexta(temp);
         6:Self.Objhorariosbackup.Submit_sabado(temp);
     End;

     if (Self.Objhorariosbackup.Salvar(true)=False)
     then Begin
               Messagedlg('N�o foi poss�vel salvar',mterror,[mbok],0);
               exit;
     End;
end;

procedure TFBACKUP.LBDias_backupKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     then begin
              BtAdicionarHorario_backupClick(sender);
              LBDias_backup.setfocus;
     End;
end;

procedure TFBACKUP.LB_HorariosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if key=VK_DELETE
     Then BtExcluirHorario_backupClick(sender);

end;

procedure TFBACKUP.btprocurarorigemClick(Sender: TObject);
begin
     OPenDialog.Filter:='Firebird|*.fdb;*.gdb';
     if OpenDialog.Execute=true
     Then EdtOrigem.text:=OpenDialog.filename;
end;

procedure TFBACKUP.btprocurardestinoClick(Sender: TObject);
var
Dir:String;
begin
     if (Edtdestino.Enabled=False)
     then exit;

     if SelectDirectory('Selecione o Diret�rio de Backup', '', Dir )
     then Edtdestino.Text:=dir;

end;

procedure TFBACKUP.btexecutarbackupClick(Sender: TObject);
begin
     if (Self.Objhorariosbackup.backup.Status<>dsinactive) or (EdtCODIGO.text='')
     Then exit;
     
     Self.Objhorariosbackup.backup.LocalizaCodigo(EdtCODIGO.text);
     Self.Objhorariosbackup.backup.TabelaparaObjeto;
     //self.Objhorariosbackup.Backup.Commit;
     Self.Objhorariosbackup.backup.IniciaBackup;

end;

procedure TFBACKUP.btrestaurabackupClick(Sender: TObject);
var
PathSemArq,pusuario,psenha:string;
begin
     OPenDialog.Filter:='Backup Firebird|*.fbk';

     if (OpenDialog.Execute=False)
     Then exit;

     if (SaveDialog.Execute=False)
     Then exit;

     Flogin.edtusuario.text:='';
     Flogin.edtsenha.text:='';
     Flogin.showmodal;
     
     if (Flogin.tag=0)
     Then exit;

     With IBRestoreService1 do
     Begin
          Active:=false;
          LoginPrompt:=FALSE;
          DatabaseName.clear;
          DatabaseName.add(SaveDialog.FileName);
          Protocol:=Local;
          ServerName:='';
          BackupFile.clear;
          BackupFile.add(OpenDialog.FileName);
          Params.Clear;
          Params.add('USER_NAME='+Flogin.edtusuario.text);
          Params.add('PASSWORD='+Flogin.edtsenha.text);
          Try
                  Verbose:=true;
                  Active:=True;
                  ServiceStart;
                  FmostraBackup.caption:='Restaurando Backup';

                  While not(IBRestoreService1.Eof) do
                  Begin
                       FmostraBackup.MemoBackup.lines.add(IBRestoreService1.GetNextLine);
                       FmostraBackup.show;
                       Application.ProcessMessages;
                  End;

                  Active:=false;
          finally
                 FmostraBackup.close;
          End;
     End;
     

end;


procedure TFBACKUP.btdescompactatodosClick(Sender: TObject);
var
cont:integer;
begin
     for cont:=0 to FileListBoxDescompacta.Items.count-1 do
     Begin
          FileListBoxDescompacta.ItemIndex:=cont;
          FdataMOdulo.ZipMaster.ZipFilename :=FileListBoxDescompacta.FileName;

          Application.ProcessMessages;

          if (extraibackup=false)
          Then Begin
                    mensagemerro('Erro ao tentar extrair o arquivo '+FileListBoxDescompacta.FileName);
                    exit;
          End;
     End;
     Showmessage('Conclu�do');
end;


Function TFBackup.ExtraiBackup:boolean;
begin
     result:=false;
     
     If FdataMOdulo.ZipMaster.Count < 1 Then
     Begin
          Mensagemerro('Nenhum arquivo encontrado');
          Exit;
     End;
     FdataMOdulo.ZipMaster.FSpecArgs.Clear;
     With FdataMOdulo.ZipMaster Do
     Begin
        ExtrBaseDir := ExtractFilePath(extractfilepath(FileListBoxDescompacta.FileName));
        ExtrOptions := ExtrOptions + [ExtrDirNames];
        ExtrOptions := ExtrOptions + [ExtrOverwrite];
        Try
            Extract;
        Except
            on e:exception do
            Begin
                MensagemErro('Erro na extra��o: '+E.message);
                exit;
            End;
        End;
     End;
     result:=true;
end;

procedure TFBACKUP.btdescompactaselecionadoClick(Sender: TObject);
begin

     if (FileListBoxDescompacta.ItemIndex<0)
     Then exit;

     if (FileListBoxDescompacta.Items.count=0)
     Then exit;

     FdataMOdulo.ZipMaster.ZipFilename :=FileListBoxDescompacta.FileName;
     Application.ProcessMessages;

     if (extraibackup=false)
     Then Begin
               mensagemerro('Erro ao tentar extrair o arquivo '+FileListBoxDescompacta.FileName);
               exit;
     End;
     Showmessage('Conclu�do');
end;




procedure TFBACKUP.btselecionaparametroClick(Sender: TObject);
var
PcodigoBaseSite:string;
objparametros2:TObjParametrosAvulso;

begin

      Try
         objparametros2:=TObjParametrosAvulso.Create(EdtOrigem.Text,Edtusuario.Text,Edtsenha.Text);
      Except
            on e:exception do
            begin
                Mensagemerro(e.message);
                exit;
            End;
      End;

      Try
        if (objparametros2.ValidaParametro('CODIGO BASE DO CLIENTE NO SITE')=false)
        Then exit;

        PcodigoBaseSite:=DesincriptaSenha(objparametros2.Get_Valor);
        edtcliente_site.Text:=PcodigoBaseSite;
      Finally
             objparametros2.Free;
      End;
end;

procedure TFBACKUP.FrScriptSqlLOTE1btexecutasqlloteClick(Sender: TObject);
begin
  FrScriptSqlLOTE1.btexecutasqlloteClick(Sender);

end;

end.

