object FSuporteverificacoes: TFSuporteverificacoes
  Left = 192
  Top = 107
  Width = 870
  Height = 500
  Caption = 'Verifica'#231#245'es de Clientes no Site'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGridVerificacoes: TDBGrid
    Left = 0
    Top = 127
    Width = 862
    Height = 346
    Align = alClient
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 862
    Height = 127
    Align = alTop
    TabOrder = 1
    object MemoSQL: TMemo
      Left = 8
      Top = 8
      Width = 561
      Height = 113
      Lines.Strings = (
        'Select exclaim_lara_verificacao.base_cliente,'
        'exclaim_lara_verificacao.datahora,'
        'exclaim_cliente.nome_fantasia,'
        'exclaim_cliente_base.nome,'
        'exclaim_software.nome,'
        'exclaim_lara_verificacao.versaoexe'
        #10
        'from exclaim_lara_verificacao '
        
          'join exclaim_cliente_base on exclaim_lara_verificacao.base_clien' +
          'te=exclaim_cliente_base.id'
        
          'join exclaim_cliente on exclaim_cliente_base.id_cliente=exclaim_' +
          'cliente.id'
        
          'join exclaim_software on exclaim_cliente_base.id_software=exclai' +
          'm_software.id'
        'order by datahora desc')
      TabOrder = 0
    end
    object btexecuta: TButton
      Left = 576
      Top = 96
      Width = 75
      Height = 25
      Caption = '&Executa'
      TabOrder = 1
      OnClick = btexecutaClick
    end
    object btreconecta: TButton
      Left = 664
      Top = 96
      Width = 75
      Height = 25
      Caption = 'Reconecta'
      TabOrder = 2
      OnClick = btreconectaClick
    end
  end
end
