object Fgeral: TFgeral
  Left = 192
  Top = 107
  Width = 870
  Height = 500
  Caption = 'Geral'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 120
    Width = 41
    Height = 13
    Caption = 'Caminho'
  end
  object Arquivos: TFileListBox
    Left = 168
    Top = 16
    Width = 145
    Height = 97
    ItemHeight = 13
    TabOrder = 0
  end
  object Diretorio: TDirectoryListBox
    Left = 16
    Top = 16
    Width = 145
    Height = 97
    FileList = Arquivos
    ItemHeight = 16
    TabOrder = 1
  end
  object edtcaminho: TEdit
    Left = 16
    Top = 136
    Width = 281
    Height = 21
    TabOrder = 2
    Text = 'c:\'
  end
  object BtAcessa: TButton
    Left = 309
    Top = 132
    Width = 75
    Height = 25
    Caption = 'Acessar'
    TabOrder = 3
    OnClick = BtAcessaClick
  end
end
