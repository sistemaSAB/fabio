unit USuporteverificacoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids,uobjconexaomysql, StdCtrls;

type
  TFSuporteverificacoes = class(TForm)
    DBGridVerificacoes: TDBGrid;
    Panel1: TPanel;
    MemoSQL: TMemo;
    btexecuta: TButton;
    btreconecta: TButton;
    procedure btexecutaClick(Sender: TObject);
    procedure btreconectaClick(Sender: TObject);
  private
    { Private declarations }
    ObjetoConexao:TObjConexaoMysql;
  public
    { Public declarations }
     Procedure passaobjeto(Pobjeto:TObjConexaoMysql);
  end;

var
  FSuporteverificacoes: TFSuporteverificacoes;

implementation

uses ZDataset, DB, UUtils, UdataMOdulo;



{$R *.dfm}

{ TFSuporteverificacoes }

procedure TFSuporteverificacoes.passaobjeto(Pobjeto: TObjConexaoMysql);
begin
     Self.ObjetoConexao:=Pobjeto;
     Self.DBGridVerificacoes.DataSource:=Self.ObjetoConexao.ObjDataSourceSqlDireto;
end;

procedure TFSuporteverificacoes.btexecutaClick(Sender: TObject);
begin
     With Self.ObjetoConexao.ObjquerySqlDireto do
     Begin
         close;
         SQL.clear;
         sql.add(MemoSQL.Text);
         Try
            open;
         Except
               on e:exception do
               Begin
                     Mensagemerro(e.message);
               End;
         End;

     End;
end;

procedure TFSuporteverificacoes.btreconectaClick(Sender: TObject);
begin
     try
      Self.ObjetoConexao.ZConnection.Disconnect;
     Except
     End;

     Try
        Self.ObjetoConexao.ZConnection.Connect;
     Except

     End;

end;

end.
