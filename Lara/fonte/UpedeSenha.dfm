object Fpedesenha: TFpedesenha
  Left = 346
  Top = 289
  Width = 286
  Height = 118
  Caption = 'Senha'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 8
    Top = 8
    Width = 36
    Height = 15
    Caption = 'Senha'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edtsenha: TEdit
    Left = 8
    Top = 24
    Width = 265
    Height = 21
    PasswordChar = '#'
    TabOrder = 0
    Text = 'edtsenha'
    OnKeyPress = edtsenhaKeyPress
  end
  object Btok: TButton
    Left = 112
    Top = 56
    Width = 75
    Height = 25
    Caption = '&OK'
    TabOrder = 1
    OnClick = BtokClick
  end
  object btcancelar: TButton
    Left = 192
    Top = 56
    Width = 75
    Height = 25
    Caption = '&CANCELAR'
    TabOrder = 2
    OnClick = btcancelarClick
  end
end
