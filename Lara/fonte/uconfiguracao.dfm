object Fconfiguracao: TFconfiguracao
  Left = 407
  Top = 262
  Width = 475
  Height = 196
  Caption = 'Configura'#231#245'es'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btpararvalidacaologin: TBitBtn
    Left = 8
    Top = 8
    Width = 185
    Height = 25
    Caption = 'Parar Valida'#231#227'o de Login'
    TabOrder = 0
    OnClick = btpararvalidacaologinClick
  end
  object btparartimerremoto: TBitBtn
    Left = 8
    Top = 40
    Width = 185
    Height = 25
    Caption = 'Parar Valida'#231#227'o Remota'
    TabOrder = 1
    OnClick = btparartimerremotoClick
  end
end
