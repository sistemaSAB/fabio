unit uconfiguracao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TFconfiguracao = class(TForm)
    btpararvalidacaologin: TBitBtn;
    btparartimerremoto: TBitBtn;
    procedure btpararvalidacaologinClick(Sender: TObject);
    procedure btparartimerremotoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fconfiguracao: TFconfiguracao;

implementation

uses UdataMOdulo;

{$R *.dfm}

procedure TFconfiguracao.btpararvalidacaologinClick(Sender: TObject);
begin
     if (ParaValidacaoglobal)
     Then Begin
               ParaValidacaoglobal:=False;
               btpararvalidacaologin.caption:='Parar Valida��o de Login';
     End
     Else Begin
               ParaValidacaoglobal:=True;
               btpararvalidacaologin.caption:='Iniciar Valida��o de Login';
     End;
end;

procedure TFconfiguracao.btparartimerremotoClick(Sender: TObject);
begin
     if (PararSiteGlobal)
     Then Begin
               PararSiteGlobal:=False;
               btparartimerremoto.caption:='Parar Valida��o Remota';
     End
     Else Begin
               PararSiteGlobal:=True;
               btparartimerremoto.caption:='Iniciar Valida��o Remota';
     End;
end;

end.
