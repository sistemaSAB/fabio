unit USuporte;

interface

uses
  db,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, DBGrids, SynEdit, SynMemo,ZAbstractRODataset,ZAbstractDataset, ZDataset, ZConnection,uobjconexaomysql;

type
  TFsuporte = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    edtcliente: TEdit;
    Panel2: TPanel;
    memosql: TSynMemo;
    btok: TButton;
    comboacao: TComboBox;
    Label2: TLabel;
    DbGridrequisicao: TDBGrid;
    STRGResultado: TStringGrid;
    btconecta: TButton;
    Button1: TButton;
    TimerRequisicao: TTimer;
    GridCliente: TStringGrid;
    Label3: TLabel;
    DBGrid1: TDBGrid;
    CheckSeleciona: TCheckBox;
    combonomesoftware: TComboBox;
    combosoftware: TComboBox;
    LbBase: TLabel;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btokclienteClick(Sender: TObject);
    procedure edtclienteKeyPress(Sender: TObject; var Key: Char);
    procedure btconectaClick(Sender: TObject);
    procedure btokClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure TimerRequisicaoTimer(Sender: TObject);
    procedure GridClienteKeyPress(Sender: TObject; var Key: Char);
    procedure GridClienteDblClick(Sender: TObject);
    procedure CheckSelecionaClick(Sender: TObject);
    procedure DbGridrequisicaoDblClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
    PbaseEscolhida:String;
    Pcliente:String;
    ObjQueryCliente:TZquery;
    ObjQueryRequisicao:TZquery;
    ObjQueryTemp:TZquery;
    DataSourceCliente:TdataSource;
    DataSourceRequisicao:TdataSource;
    objconexaomysql:tobjConexaoMysql;

  public
    { Public declarations }
  end;

var
  Fsuporte: TFsuporte;

implementation

uses UdataMOdulo, UUtils, USuporteverificacoes, Ugeral, uconfiguracao;

{$R *.dfm}

procedure TFsuporte.FormShow(Sender: TObject);
begin
     Self.PbaseEscolhida:='';
     LbBase.caption:='Nenhuma Base Escolhida';
     

     GridCliente.ColCount:=1;
     GridCliente.RowCount:=1;
     GridCliente.Cols[0].clear;


     Pcliente:='';
     Try
        Self.objconexaomysql:=tobjConexaoMysql.create(pathglobal,DesincriptaSenha('��������'),DesincriptaSenha('��������'));

        Self.objconexaomysql.preenchesoftwares(combosoftware.Items,combonomesoftware.Items);
        combonomesoftware.ItemIndex:=0;


        Self.ObjQueryCliente:=TZquery.create(nil);
        Self.ObjQueryCliente.Connection:=objconexaomysql.ZConnection;

        Self.ObjQueryRequisicao:=TZquery.create(nil);
        Self.ObjQueryRequisicao.Connection:=objconexaomysql.ZConnection;

        Self.ObjQueryTemp:=TZquery.create(nil);
        Self.ObjQueryTemp.Connection:=objconexaomysql.ZConnection;

        self.DataSourceCliente:=TdataSource.create(nil);
        self.DataSourceCliente.DataSet:=Self.ObjQueryCliente;

        self.DataSourceRequisicao:=TdataSource.create(nil);
        self.DataSourceRequisicao.DataSet:=Self.ObjQueryRequisicao;

        //Self.DbgridCliente.DataSource:=Self.DataSourceCliente;
        Self.DbGridrequisicao.DataSource:=Self.DataSourceRequisicao;


        
        objconexaomysql.Conecta;

        if (objconexaomysql.Conectado=False)
        Then exit;

        TimerRequisicao.Enabled:=True;


     Except
           mensagemerro('Erro na tentativa de criar as querys');
           exit;
     End;
     
end;

procedure TFsuporte.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     TimerRequisicao.Enabled:=false;


     Try
        ObjQueryCliente.Connection.Commit;
         freeandnil(Self.ObjQueryCliente);
     Except
     End;

     Try
       ObjQueryRequisicao.Connection.Commit;
        freeandnil(Self.ObjQueryRequisicao);
     Except
     End;

     try
       self.ObjQueryTemp.Connection.Commit;
        freeandnil(self.ObjQueryTemp);
     Except
     End;

     Try
        freeandnil(Self.DataSourceCliente);
     Except
     End;

     Try
        freeandnil(Self.DataSourceRequisicao);
     Except
     End;

     Try
        Self.objconexaomysql.free;
     Except
     End;


end;

procedure TFsuporte.btokclienteClick(Sender: TObject);
begin
{     Pcliente:=ObjQueryCliente.fieldbyname('idbase').asstring;
     objconexaomysql.VerificaRequisicoes_baseatual(Pcliente,self.ObjQueryRequisicao);
 }
end;

procedure TFsuporte.edtclienteKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Begin
                objconexaomysql.PesquisaCliente(edtcliente.Text,self.ObjQueryCliente,GridCliente,combosoftware.items[combonomesoftware.itemindex]);
     End;
end;

procedure TFsuporte.btconectaClick(Sender: TObject);
begin
        objconexaomysql.Conecta;

        if (objconexaomysql.Conectado=False)
        Then exit;
        
end;

procedure TFsuporte.btokClick(Sender: TObject);
Begin
     objconexaomysql.InsereRequisicao(comboacao.ItemIndex+1,memosql.Lines.Text,Self.PbaseEscolhida,self.ObjQueryTemp);
     objconexaomysql.VerificaRequisicoes_baseatual(Self.PbaseEscolhida,self.ObjQueryRequisicao);
     
End;

procedure TFsuporte.Button1Click(Sender: TObject);
var
erro:boolean;
begin
     erro:=False;
     Try
        if (Self.objconexaomysql.ZConnection.Connected=True)
        Then Self.ObjQueryRequisicao.Refresh
        Else Begin
                  Self.objconexaomysql.ZConnection.Reconnect;
                  Self.ObjQueryRequisicao.close;
                  Self.ObjQueryRequisicao.Open;
        End;
     Except
           on e:exception do
           Begin
                Mensagemerro(e.message);
                erro:=true;
           End;
     End;
     if (erro)
     then Begin
               Self.ObjQueryRequisicao.Close;
               Self.ObjQueryRequisicao.Open;
     End;

end;

procedure TFsuporte.TimerRequisicaoTimer(Sender: TObject);
begin

     if (Pcliente<>'')
     then objconexaomysql.VerificaRequisicoes_baseatual(Pcliente,self.ObjQueryRequisicao);
     
end;

procedure TFsuporte.GridClienteKeyPress(Sender: TObject; var Key: Char);
begin
      if (key=#13)
      then Self.GridClienteDblClick(sender);
end;

procedure TFsuporte.GridClienteDblClick(Sender: TObject);
begin
     if (GridCliente.row=0)
     Then exit;


     if (GridCliente.Cells[0,GridCliente.row]='X')
     Then GridCliente.Cells[0,GridCliente.row]:=''
     Else Begin
              GridCLiente.Cols[0].clear;
              GridCliente.Cells[0,GridCliente.row]:='X';
              Self.PbaseEscolhida:=GridCliente.Cells[1,GridCliente.row];
              LbBase.Caption:='Base Escolhida '+Self.pbaseescolhida;
              objconexaomysql.VerificaRequisicoes_baseatual(PbaseEscolhida,self.ObjQueryRequisicao);
     End;



end;

procedure TFsuporte.CheckSelecionaClick(Sender: TObject);
var
temp:string;
cont:integer;
begin
     if CheckSeleciona.Checked
     Then temp:='X'
     Else temp:='';

     for cont:=1 to GridCliente.RowCount-1 do
     Begin
          GridCliente.Cells[0,cont]:=temp;
     End;
end;

procedure TFsuporte.DbGridrequisicaoDblClick(Sender: TObject);
var
   cont:integer;
   temp,StrMsg:String;
   PStrtemp:TStringList;
   sai:boolean;
begin
     Self.STRGResultado.ColCount:=1;
     Self.STRGResultado.Cols[0].clear;
     Self.STRGResultado.RowCount:=1;

     



     StrMsg:=Self.ObjQueryRequisicao.fieldbyname('msg').asstring;

     StrMsg:=StringReplace(StrMsg,'�','',[rfReplaceAll,rfIgnoreCase]);
     StrMsg:=StringReplace(StrMsg,'@@LIN@@',#13+#10,[rfReplaceAll,rfIgnoreCase]);

     sai:=False;
     While (sai=false) do
     Begin
         if (pos('@@COL@@@@COL@@',StrMsg)>0)
         Then Begin
                   //criando um espaco entre colunas que estao vazias
                    StrMsg:=StringReplace(StrMsg,'@@COL@@@@COL@@','@@COL@@ @@COL@@',[rfReplaceAll,rfIgnoreCase]);
         End
         Else sai:=True;
     End;




     //substituindo o identificador de coluna por um identificador unico
     StrMsg:=StringReplace(StrMsg,'@@COL@@','�',[rfReplaceAll,rfIgnoreCase]);

     if (StrMsg='')
     Then exit;

     Try
        pstrtemp:=TStringList.Create;
     Except

     End;

     Try
        //transformando a String em uma String List (1 coluna)
        PStrtemp.Clear;
        PStrtemp.Text:=strmsg;

        //Criando uma unica coluna no grid com todas as linhas
        STRGResultado.ColCount:=1;
        STRGResultado.RowCount:=PStrtemp.Count;
        STRGResultado.Cols[0].Text:=PStrtemp.Text;

        ExplodeStr(PStrtemp[0],PStrtemp,'�','string');
        STRGResultado.ColCount:=PStrtemp.count;
        STRGResultado.Rows[0].text:=pstrtemp.text;

        for cont:=1 to STRGResultado.RowCount-1 do
        Begin
             ExplodeStr(Self.STRGResultado.Cells[0,cont],Pstrtemp,'�','string');
             STRGResultado.Rows[cont].text:=PStrTemp.text;
        End;

        if (STRGResultado.RowCount>1)
        Then STRGResultado.FixedRows:=1;
        
        AjustaLArguraColunaGrid(STRGResultado);

     Finally
            freeandnil(pstrtemp);
     End;

    
end;

procedure TFsuporte.Button2Click(Sender: TObject);
begin
     FSuporteverificacoes.passaobjeto(Self.objconexaomysql);
     FSuporteverificacoes.ShowModal;
end;

procedure TFsuporte.Button3Click(Sender: TObject);
begin
     Fgeral.Showmodal;
end;

procedure TFsuporte.Button4Click(Sender: TObject);
begin
   Fconfiguracao.Showmodal;
end;

end.


