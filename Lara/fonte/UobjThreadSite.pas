unit UobjThreadSite;

interface

uses
  forms,Classes,sysutils,useg,db,IBCustomDataSet, ibscript,ZAbstractRODataset,
  ZAbstractDataset, ZDataset, ZConnection,ibquery,ibdatabase, ZipMstr,IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdFTP;

type
  TobjThreadSite = class(TThread)

  public
        constructor create(caminhobanco,usuario,senha:string;PZConnection: TZConnection;
                        PZquery:TZquery;PZquery2:TZquery;PObjQuerySenha:Tibquery;
                        PIBDatabaseBackup: TIBDatabase;PIBTransactionBackup: TIBTransaction;
                        PIBScript: TIBScript;
                        PIBDataSetScript: TIBDataSet;
                        PSL:TSTringList;PFTP:TIdFtp;PZipMaster: TZipMaster);




        //destructor free;
  private
    { Private declarations }

    ZConnection: TZConnection;
    Zquery:TZquery;
    Zquery2:TZquery;

    SL:TSTringList;
    FTP:TIdFtp;
    ZipMaster: TZipMaster;

    ObjQuerySenha:Tibquery;
    IBDatabaseSenha: TIBDatabase;
    IBTransactionSenha: TIBTransaction;


    IBDatabaseBackup: TIBDatabase;
    IBTransactionBackup: TIBTransaction;

    IBScript: TIBScript;
    IBDataSetScript: TIBDataSet;


    EtapaAtual,mensagemrodape:string;
    QuantidadeBytesTotal:integer;

    //*************************************************************************
    Function  VerificaRequisicoes(var Pmsg:String):boolean;
    //*************************************************************************

    procedure UpdateMensagem;


    function ExecutaSQL(PqueryLocal: Tibquery; Pvalor: String;Pid:String;var msg: string;RetornaMySQL:Boolean;PBaseLara:Boolean): boolean;
    //Function ExecutaSQL_LOCAL(PqueryLocal: Tibquery; Pvalor: String;var msg: string): boolean;
    Function EnviaRelatorio(PlocalArquivo:String;ParquivoRemoto:String;var msg:string):boolean;



    Function Conecta:boolean;
    Function Conectado:boolean;



    Function PegaIp:string;

    Function PesquisaCliente(Pcliente:string;Pquery:TZQuery):boolean;
    Function InsereRequisicao(Pinstrucao:integer;pvalor,pidbase:String;Pquery:Tzquery):Boolean;
    function VerificaRequisicoes_baseatual(pidbase: String;Pquery: Tzquery): Boolean;
    function CompactaArquivo(PnomeArquivo, ArquivoFinal: string;var msg: string): boolean;
    function ExecutaBackup(pid:String;var msg: string): boolean;
    function BaixandoNovaVersao(pid: String; pnomearquivo:string;var msg: string): boolean;
    function BaixandoArquivosLara(pid: String; pnomearquivo:string;var msg: string): boolean;




    function baixaarquivo(Parquivosite,Parquivolocal: string;var msg: string): boolean;
    function DesCompactaArquivo(PnomeArquivo, LocalFinal: string;var msg: string): boolean;

    procedure IdFTPWork(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure IdFTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure UpdateRodape;
    function AcessaDiretorio(Diretorio: String; var msg: string;ListaArquivos:Boolean): boolean;
    function TemAtributo(Attr, Val: Integer): Boolean;



  protected
    procedure Execute; override;


  end;

implementation

uses UdataMOdulo, UprincipalExclaim, UUtils,httpsend, UobjBACKUP, Math,
  Ugeral;



{ Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure ThreadSite.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; }

{ ThreadSite }


procedure TobjThreadSite.UpdateMensagem;
begin
     FprincipalExclaim.LbSite.caption:=Self.EtapaAtual;
end;

procedure TobjThreadSite.UpdateRodape;
begin
  FprincipalExclaim.lbrodape.caption:=Self.MensagemRodape;
end;



procedure TobjThreadSite.Execute;
var
temp:string;
begin
  { Place thread code here }
   Self.EtapaAtual:='Verificando Site '+datetimetostr(now);
   Synchronize(UpdateMensagem);
   Self.VerificaRequisicoes(temp);
end;

function TobjThreadSite.Conecta: boolean;
begin
    Self.EtapaAtual:='Conectando no  Site ';
    Synchronize(UpdateMensagem);

     result:=false;
     Try
        With ZConnection do
        Begin
             HostName:=HostSiteglobal;
             Database:=BdSiteglobal;
             Port:=strtoint(portasiteglobal);
             User:=UsuarioSiteglobal;
             Password:=SenhaSiteglobal;
             AutoCommit:=true;
             LoginPrompt:=False;
             Protocol:=ProtocoloSiteglobal;
             Connect;
             result:=true;
        End;
     Except
           on e:exception do
           Begin
                //MensagemErro('Erro na tentativa de se conectar'+#13+E.message);
                Self.EtapaAtual:='Erro na tentativa de se conectar. '+E.message;
                Synchronize(UpdateMensagem);
                exit;
           End;
     End;
end;

function TobjThreadSite.Conectado: boolean;
begin
     Result:=Self.ZConnection.Connected;
end;

function TobjThreadSite.TemAtributo(Attr, Val: Integer): Boolean;
begin
  Result := Attr and Val = Val;
end;


Function TobjThreadSite.AcessaDiretorio(Diretorio: String; var msg: string;ListaArquivos:Boolean): boolean;
var
  F: TSearchRec;
  Ret: Integer;
  TempNome: string;
begin
     result:=False;

     if (DirectoryExists(Diretorio)=False)
     Then Begin
               msg:='Diret�rio n�o existe';
               exit;
     End;

     msg:='@@COL@@DIRETORIO@@LIN@@';
     //Listando o Raiz que foi enviado
     tempNome:=Diretorio+'@@LIN@@';
     //se ja tiver barra dupla troca por �
     tempNome:=StringReplace(tempnome,'\\','�',[rfReplaceAll,rfIgnoreCase]);
     //troco o � por barra simples
     tempNome:=StringReplace(tempnome,'�','\',[rfReplaceAll,rfIgnoreCase]);
     //troco todas as simples por duplas
     tempNome:=StringReplace(tempnome,'\','\\',[rfReplaceAll,rfIgnoreCase]);
     msg:=msg+tempnome;

     Ret := FindFirst(Diretorio+'\*.*', faAnyFile, F);
     try
       while Ret = 0 do
       begin
           if TemAtributo(F.Attr, faDirectory)
           then Begin
                   if (F.Name <> '.') And (F.Name <> '..')
                   then begin
                         tempNome:=Diretorio+'\'+F.Name+'\@@LIN@@';
                         //se ja tiver barra dupla troca por �
                         tempNome:=StringReplace(tempnome,'\\','�',[rfReplaceAll,rfIgnoreCase]);
                         //troco o � por barra simples
                         tempNome:=StringReplace(tempnome,'�','\',[rfReplaceAll,rfIgnoreCase]);
                         //troco todas as simples por duplas
                         tempNome:=StringReplace(tempnome,'\','\\',[rfReplaceAll,rfIgnoreCase]);
                         msg:=msg+tempnome;
                   end;
           End
           else Begin
                    if (ListaArquivos)
                    then Begin
                            //S� retorno o Nome do Arquivo
                            tempNome:=F.Name+'@@LIN@@';
                            //se ja tiver barra duplca troca por �
                            tempNome:=StringReplace(tempnome,'\\','�',[rfReplaceAll,rfIgnoreCase]);
                            //troco o � por barra simples
                            tempNome:=StringReplace(tempnome,'�','\',[rfReplaceAll,rfIgnoreCase]);
                            //troco todas as simples por duplas
                            tempNome:=StringReplace(tempnome,'\','\\',[rfReplaceAll,rfIgnoreCase]);
                            msg:=msg+tempnome;
                    End;
           End;

           Ret := FindNext(F);
       end;
       result:=true;
     finally
       begin
         FindClose(F);
       end;
     end;
End;


function TobjThreadSite.ExecutaSQL(PqueryLocal:Tibquery;Pvalor:String;Pid:String;var msg:string;RetornaMySQL:Boolean;PBaseLara:Boolean): boolean;
var
temp:string;
cont:integer;
Begin

     SL.CLEAR;


     msg:='';
     result:=False;
     Try
       Self.IBDatabasebackup.close;

       IF (PBaseLara=True)
       Then Begin
                 Self.IBDatabasebackup.Databasename:=PqueryLocal.Database.DatabaseName;
                 Self.IBDatabaseBackup.Params.clear;
                 Self.IbDatabasebackup.Params.Add('User_name='+DesincriptaSenha('��������'));
                 Self.IbDatabasebackup.Params.Add('password='+DesincriptaSenha('��������'));
       End
       Else Begin
                 Self.IBDatabaseBackup.Databasename:=PqueryLocal.fieldbyname('origem').asstring;
                 Self.IBDatabaseBackup.Params.clear;
                 Self.IBDatabaseBackup.Params.Add('User_name='+PqueryLocal.fieldbyname('usuario').asstring);
                 Self.IBDatabaseBackup.Params.Add('password='+Useg.desincriptasenha(PqueryLocal.fieldbyname('senha').asstring));
       End;


       Self.IBDatabaseBackup.Open;
     Except
           on e:exception do
           Begin
                msg:=e.message;
                exit;
           End;
     End;


     Self.ibDATASETScript.Transaction:=Self.IBTransactionBackup;
     Self.ibDATASETScript.database:=Self.IBDatabaseBackup;
// gian   Self.IBScript.Dataset:=Self.IBDataSetScript;

     Self.IBScript.Database:=Self.IBDatabaseBackup;
     Self.IBScript.Transaction:=Self.IBTransactionBackup;

//gian 06/02/2012 pra ver se funciona
     with Self.IBScript do
     begin
       AutoDDL := False;
       Dataset := nil;
       if Transaction.InTransaction then
         Transaction.Rollback;
       Transaction.StartTransaction;
     end;
//gian fim

Try
     //IBScript.Terminator:=';';

     IBScript.Script.Clear;
     IBScript.Script.Add(PVALOR);

     try
        IBScript.ExecuteScript;
        IBScript.Transaction.Commit;

        Self.EtapaAtual:='Gerando Relat�rio do SQL';
        Synchronize(UpdateMensagem);

        for cont:=0 to Self.IBDataSetScript.FieldCount-1 do
        Begin
               msg:=msg+'@@COL@@'+Self.IBDataSetScript.Fields[cont].FieldName;
        End;
        sl.Add(msg);
        msg:='';

        While not(Self.IBDataSetScript.Eof) do
        Begin
             for cont:=0 to Self.IBDataSetScript.FieldCount-1 do
             Begin
                  //tenho que substituir o quebra de linha por | por causa de campos MEMO
                  //o mysql suprime o \ por isso devo enviar \\ no lugar
                  msg:=msg+'@@COL@@'+StringReplace(StringReplace(Self.IBDataSetScript.Fields[cont].AsString,#13+#10,'|',[rfReplaceAll]),'\','\\',[rfReplaceAll]);
             End;

             sl.Add(msg);
             msg:='';

             Self.IBDataSetScript.next;
        End;
        sl.Add('$$$$$FIM$$$$');
        Sl.SaveToFile(RetornaPathSistema+pid+'.txt');


        if (RetornaMySQL=false)
        Then Begin
                  Self.EtapaAtual:='Compactando o arquivo';
                  Synchronize(UpdateMensagem);

                  if (Self.CompactaArquivo(RetornaPathSistema+pid+'.txt',RetornaPathSistema+pid+'.zip',msg) =False)
                  Then exit;

                  Try
                     sleep(1000);
                     if (DeleteFile(RetornaPathSistema+pid+'.txt')=false)
                     then msg:='Erro exclus�o '+RetornaPathSistema+pid+'.txt';
                  Except
                        msg:='Erro exclus�o '+RetornaPathSistema+pid+'.txt';
                  End;

                  result:=EnviaRelatorio(RetornaPathSistema+pid+'.zip',pid+'.zip',msg);

                  Try
                     sleep(1000);
                     if (DeleteFile(RetornaPathSistema+pid+'.zip')=false)
                     then msg:='Erro exclus�o '+RetornaPathSistema+pid+'.zip';
                  Except
                        msg:='Erro exclus�o '+RetornaPathSistema+pid+'.zip';
                  End;


                  (*if (Self.CompactaArquivo('d:\teste.txt','d:\teste.zip',msg) =False)
                    Then exit;
                    result:=EnviaRelatorio('d:\teste.zip','teste.zip',msg);*)
        End
        Else Begin
                  msg:='';
                  msg:=StringReplace(sl.text,#13,'@@LIN@@',[rfReplaceAll]);
                  msg:=StringReplace(msg,#10,'',[rfReplaceAll]);
                  result:=True;
        End;




     Except
           on e:exception do
           begin
             if IBScript.Transaction.InTransaction then
               IBScript.Transaction.Rollback;
                msg:=e.message
           End;
     End;

Finally
       Self.EtapaAtual:=msg;
       Synchronize(UpdateMensagem);

       Self.IBDatabaseBackup.Close;
End;

end;

function TobjThreadSite.BaixandoNovaVersao(pid:String;pnomearquivo:string;var msg:string): boolean;
Begin
     result:=False;


     if (DirectoryExists(RetornaPathSistema+'\novo')=false)
     then Begin
               if (createdir(RetornaPathSistema+'\novo')=false)
               then begin
                         msg:='Erro ao criar diretorio '+RetornaPathSistema+'\novo';
                         exit;
               End;
     End;
     
     //baixando o lara.zip do site
     
     if (Self.baixaarquivo(pnomearquivo,RetornaPathSistema+'novo\lara.zip',msg)=false)
     then exit;



     if (Self.DesCompactaArquivo(RetornaPathSistema+'novo\lara.zip',RetornaPathSistema+'novo\',msg)=false)
     then exit;

     Try
        if (deletefile(RetornaPathSistema+'novo\lara.zip')=false)
        Then msg:='Erro ao tentar excluir '+RetornaPathSistema+'novo\lara.zip'; 

     Except
           msg:='Erro ao tentar excluir '+RetornaPathSistema+'novo\lara.zip';
     End;

     //descompacto ele em uma pasta

     //excluo o anovo.zip

     //No procedimento de verifica instrucao
     
     //Fecho o ProgramaRenomeiaLara.exe

     //Encerro a aplicacao

     //Esse programa ira renomear o Lara.exe para laraantigo.exe
     //e renomeara o laranovo.exe para lara.exe
     //chamara o lara.exe


     
     result:=True;

End;

function TobjThreadSite.baixaarquivo(parquivosite,parquivolocal:string;var msg:string):boolean;
Begin
     result:=False;

     With Self do
     Begin
          Self.EtapaAtual:='Conectando FTP';
          Synchronize(UpdateMensagem);

          if (FTP.Connected)
          then FTP.Disconnect;

          ftp.Host:= HostFtpglobal;
          Ftp.Username:= UsuarioFtpglobal;
          ftp.Password:=SenhaFtpglobal;
          FTP.Passive:=PassiveFtpglobal;

          try
             Ftp.Connect;
          except
              on e:exception do
              Begin
                    msg:='Erro conexao ftp: '+e.Message;
                    exit;
              End;
          end;

          try
             Self.EtapaAtual:='Acessando pastas do FTP';
             Synchronize(UpdateMensagem);

             //ftp.ChangeDir(pastaarquivosite);

             Self.EtapaAtual:='Baixando arquivo do FTP';
             Synchronize(UpdateMensagem);

             FTP.OnWork:=Self.IdFTPWork;
             Self.QuantidadeBytesTotal:=Ftp.Size(parquivosite);
             FTP.Get(parquivosite,parquivolocal,true,false);

             result:=True;

          Except
                on e:exception do
                Begin
                      msg:='Erro ftp: '+e.Message;
                      exit;
                End;
          End;
     End;

end;


function TobjThreadSite.ExecutaBackup(pid:String;var msg:string): boolean;
var
  path,temp:string;
  cont:integer;
  Objbackup:TObjBACKUP;

Begin

     result:=False;

     try
        Objbackup:=TObjBackup.Create(pathglobal);
     Except
           on e:exception do
           Begin
                msg:=e.message;
                exit;
           End;
     End;

     Try

        Try
           If (objbackup.LocalizaCodigo(Self.ObjQuerySenha.fieldbyname('codigo').asstring)=False)
           Then Begin
                     msg:='Backup n�o localizado';
                     exit;
           End;

           objbackup.TabelaparaObjeto;
           Objbackup.Commit;
           objbackup.Submit_verbose('N');

           Self.EtapaAtual:='Iniciando Backup '+'BACKUP'+pid+'.fbk';
           Synchronize(UpdateMensagem);

           if (objbackup.IniciaBackup(RetornaPathSistema+'BACKUP'+pid+'.fbk',msg)=False)
           Then exit;

           Self.EtapaAtual:='Compactando Backup '+'BACKUP'+pid+'.zip';
           Synchronize(UpdateMensagem);

           if (Self.CompactaArquivo(RetornaPathSistema+'BACKUP'+pid+'.fbk',RetornaPathSistema+'BACKUP'+pid+'.zip',msg)=False)
           Then exit;

           Self.EtapaAtual:='Enviando FTP '+'BACKUP'+pid+'.zip';
           Synchronize(UpdateMensagem);

           if (Self.EnviaRelatorio(RetornaPathSistema+'BACKUP'+pid+'.zip','BACKUP'+pid+'.zip',msg)=False)
           then exit;

           result:=True;

           Try
              if (DeleteFile(RetornaPathSistema+'BACKUP'+pid+'.fbk')=false)
              Then msg:='Erro exclus�o '+RetornaPathSistema+'BACKUP'+pid+'.fbk';
           Except
                 on e:exception do
                 Begin
                      msg:=e.message;
                 End;
           End;

        Except
             on e:exception do
             begin
                  msg:=e.message
            End;
        End;

     Finally
            Objbackup.Commit;
            Objbackup.Free;
            Self.EtapaAtual:=msg;
            Synchronize(UpdateMensagem);
     End;

End;

(*function TobjThreadSite.ExecutaSQL_LOCAL(PqueryLocal:Tibquery;Pvalor:String;var msg:string): boolean;
var
temp:string;
cont:integer;
Begin
   //****trocando o dataset e o scprit para base local
   Self.ibDATASETScript.database:=Self.IBDatabaseBackup;
   Self.IBScript.Dataset:=Self.IBDataSetScript;
   Self.IBScript.Database:=Self.IBDatabaseBackup;
   Self.IBScript.Transaction:=Self.IBTransactionBackup;


   msg:='';
   result:=False;
   Try
       Self.IBDatabaseBackup.CLOSE;
       Self.IBDatabasebackup.Databasename:=PqueryLocal.Database.DatabaseName;
       Self.IbDatabasebackup.Params.clear;

       IF (pLOCAL=True)
       Then Begin
                 Self.IBDatabasebackup.Databasename:=PqueryLocal.Database.DatabaseName;
                 Self.IbDatabasebackup.Params.Add('User_name='+DesincriptaSenha('��������'));
                 Self.IbDatabasebackup.Params.Add('password='+DesincriptaSenha('��������'));

       End;
       Self.IbDatabasebackup.Open;
   Except
           on e:exception do
           Begin
                msg:=e.message;
                exit;
           End;
     End;

Try

     msg:='';
     result:=False;

     IBScript.Terminator:=';';
     IBScript.Script.text:=Pvalor;
     
     try
        IBScript.ExecuteScript;

        for cont:=0 to Self.IBDataSetScript.FieldCount-1 do
        Begin
              msg:=msg+'@@COL@@'+Self.IBDataSetScript.Fields[cont].FieldName;
        End;
        msg:=msg+'@@LIN@@';

        While not(Self.IBDataSetScript.Eof) do
        Begin
             for cont:=0 to Self.IBDataSetScript.FieldCount-1 do
             Begin
                  msg:=msg+'@@COL@@'+Self.IBDataSetScript.Fields[cont].AsString;
             End;

             msg:=msg+'@@LIN@@';

             Self.IBDataSetScript.next;
        End;

        result:=True;
     Except
           on e:exception do
           begin
                msg:=e.message
           End;
     End;
 Finally
       Self.IBDatabaseBackup.Close;
End;

end;
    *)

function TobjThreadSite.VerificaRequisicoes(var Pmsg:String): boolean;
var
msg:string;
Deuerro:boolean;
pip,pID:string;
contconecta:integer;
Begin


Try
     Self.objquerysenha.sql.clear;
     Self.objquerysenha.sql.add('Select * from Tabbackup where not (cliente_site is null) order by cliente_site');
     Self.objquerysenha.open;

     While not(Self.objquerysenha.eof) do
     begin

           if (Self.Conecta=False)
           Then exit;
           
           Self.EtapaAtual:='Verificando Base '+Self.ObjQuerySenha.fieldbyname('cliente_site').asstring;
           Synchronize(UpdateMensagem);

           contconecta:=0;
           Repeat
                 Self.Zquery.close;
                 Self.Zquery.SQL.clear;
                 Self.Zquery.sql.add('Select * from exclaim_lara_instrucoes where base='+self.objquerysenha.fieldbyname('cliente_site').asstring);
                 Self.Zquery.sql.add('and (atualizado is null or atualizado<>''S'')');
                 Try
                     Self.Zquery.open;
                     contconecta:=2;
                 Except
                       on e:exception do
                       Begin
                             Self.mensagemrodape:=e.message;
                             Synchronize(UpdateRodape);
                             inc(contconecta,1);
                       End;
                 End;
           Until(contconecta=2);
           
           Pid:=Self.Zquery.fieldbyname('id').asstring;

           while not(Self.Zquery.eof) do
           Begin
                //executando a instrucao

                msg:='';
                deuerro:=False;

                case Self.Zquery.FieldByName('instrucao').asinteger of

                  1:Begin
                          //executa um comando sql e retorna pelo MYSQL
                          Self.EtapaAtual:='Executando comando SQL'+Self.ObjQuerySenha.fieldbyname('cliente_site').asstring;
                          Synchronize(UpdateMensagem);
                          DeuErro:=not(Self.ExecutaSQL(self.objquerysenha,Self.zquery.fieldbyname('valor').asstring,Self.zquery.fieldbyname('id').asstring,msg,true,false));
                  End;
                  2:Begin
                          //executa um comando sql e retorna pelo FTP em ZIP
                          Self.EtapaAtual:='Executando comando SQL'+Self.ObjQuerySenha.fieldbyname('cliente_site').asstring;
                          Synchronize(UpdateMensagem);
                          DeuErro:=not(Self.ExecutaSQL(self.objquerysenha,Self.zquery.fieldbyname('valor').asstring,Self.zquery.fieldbyname('id').asstring,msg,false,false));
                  End;
                  3:Begin
                          //executa um comando sql no senha.fdb (LARA) e retorna pelo MySql
                          Self.EtapaAtual:='Executando comando SQL no LARA';
                          Synchronize(UpdateMensagem);
                          //DeuErro:=not(Self.ExecutaSQL_LOCAL(self.objquerysenha,Self.zquery.fieldbyname('valor').asstring,msg));
                          DeuErro:=not(Self.ExecutaSQL(self.objquerysenha,Self.zquery.fieldbyname('valor').asstring,Self.zquery.fieldbyname('id').asstring,msg,true,true));
                  End;
                  4:Begin
                          //executa um comando sql no senha.fdb(lara) e retorna pelo FTP em ZIP
                          Self.EtapaAtual:='Executando comando SQL'+Self.ObjQuerySenha.fieldbyname('cliente_site').asstring;
                          Synchronize(UpdateMensagem);
                          DeuErro:=not(Self.ExecutaSQL(self.objquerysenha,Self.zquery.fieldbyname('valor').asstring,Self.zquery.fieldbyname('id').asstring,msg,false,True));
                  End;
                  5:Begin
                         //Recarrega Parametros
                         Self.EtapaAtual:='Recarregando Par�metros no LARA';
                         Synchronize(UpdateMensagem);
                         DeuErro:=not(Fdatamodulo.RecarregaParametros(msg));
                  End;
                  6:Begin
                         Self.EtapaAtual:='Gerando Backup';
                         Synchronize(UpdateMensagem);
                         DeuErro:=Not(Self.ExecutaBackup(pid,msg));
                  End;
                  7:Begin
                         Self.EtapaAtual:='Baixando Nova Vers�o Lara';
                         Synchronize(UpdateMensagem);
                         DeuErro:=Not(Self.BaixandoNovaVersao(pid,Self.zquery.fieldbyname('valor').asstring,msg));

                         if (DeuErro=false)
                         Then AtualizaexeGlobal:=True;

                  End;
                  8:Begin
                         //baixar arquivos para o lara
                         Self.EtapaAtual:='Baixando Arquivos para o Lara';
                         Synchronize(UpdateMensagem);
                         DeuErro:=Not(Self.BaixandoArquivosLara(pid,Self.zquery.fieldbyname('valor').asstring,msg));
                  End;
                  9:Begin
                         //Acessa Diretorio e Lista pastas
                         Self.EtapaAtual:='Acessando diret�rio';
                         Synchronize(UpdateMensagem);
                         DeuErro:=Not(Self.AcessaDiretorio(Self.zquery.fieldbyname('valor').asstring,msg,False));
                  End;
                   10:Begin
                         //Acessa Diretorio e Lista pastas
                         Self.EtapaAtual:='Listando arquivos';
                         Synchronize(UpdateMensagem);
                         DeuErro:=Not(Self.AcessaDiretorio(Self.zquery.fieldbyname('valor').asstring,msg,True));
                  End;
                  666:Begin
                           //executar as atualiza��es
                  End;


                  


                  else Begin
                            msg:='Instru��o n�o encontrada';
                            deuerro:=true;
                  End;
                End;


                contconecta:=0;

                Self.EtapaAtual:=msg;
                Synchronize(UpdateMensagem);

                Repeat
                    //marcando que ja foi executada
                  Try
                      Self.Zquery2.close;
                      Self.Zquery2.sql.clear;
                      Self.Zquery2.sql.add('update exclaim_lara_instrucoes set atualizado=''S'',datahora=NOW(),msg='''+msg+''',erro='''+converte_boolean(deuerro)+''' where id='+Pid);
                      Self.Zquery2.execsql;
                      ContConecta:=2;
                  Except
                        Self.Conecta;
                        inc(contconecta,1);
                  End;
                Until(ContConecta=2);
                Self.Zquery.next;

           End;//while not (instrucoes para a base atual)


           //gravando a ultima vez que verificou no site

           contconecta:=0;
           pip:=Self.PegaIp;
           //pip:='';

           Repeat
                   Try

                        Self.Zquery2.close;
                        Self.Zquery2.sql.clear;
                        Self.Zquery2.sql.add('update exclaim_lara_verificacao set datahora=NOW(),ip='+#39+pip+#39+
                                            ' ,versaoexe='+#39+pversaoexeglobal+#39+' ,versaobd='+#39+pversaobdglobal+#39+
                                            ' where base_cliente='+self.objquerysenha.fieldbyname('cliente_site').asstring);
                        Self.Zquery2.execsql;

                        if (Self.Zquery2.RowsAffected=0)//NAO TINHA NENHUM REGISTRO NESSA TABELA PARA ESTA BASE
                        Then Begin
                                 //insert
                                 Self.Zquery2.close;
                                 Self.Zquery2.sql.clear;
                                 Self.Zquery2.sql.add('insert exclaim_lara_verificacao (base_cliente,datahora,ip) values ('+self.objquerysenha.fieldbyname('cliente_site').asstring+',NOW(),'+#39+pip+#39+')');
                                 Self.Zquery2.execsql;
                        End;

                        contconecta:=2;
                        
                   Except
                         Self.conecta;
                         inc(contconecta,1);
                   End;
           Until(ContConecta=2);

           //troca de base para verificar no mysql
           self.objquerysenha.next;
     End;

Finally
       self.Zquery.Connection.Commit;
       Self.ObjQuerySenha.Transaction.Commit;
       self.Zquery2.Connection.Commit;
       Self.Zquery.close;
       Self.Zquery2.close;
       Self.objquerysenha.close;
       Self.ZConnection.Disconnect;
End;



end;



function TobjThreadSite.PegaIp: string;
begin
      sl.text:='';
      Try
         //esse endereco retorna apenas o numero do ip
         //sem tag html nenhuma
         HttpGetText('www.exclaim.com.br/meuip/', sl);
      Except
            //pode dar erro ou timeout retorna vazio tmbm
            sl.text:='';
      End;

      result:=sl.Text;

      
end;

function TobjThreadSite.PesquisaCliente(Pcliente: string;
  Pquery: TZQuery): boolean;
begin
     With Pquery do
     Begin
          close;
          sql.clear;
          sql.add('Select exclaim_cliente_base.id as IdBase,');
          sql.add('exclaim_cliente.nome_fantasia as Fantasia,');
          sql.add('exclaim_cliente.cpf_cnpj as Cnpj,');
          sql.add('exclaim_cliente_base.nome as Base');
          sql.add('from exclaim_cliente');
          sql.add('join exclaim_cliente_base on exclaim_cliente.id=exclaim_cliente_base.id_cliente');
          sql.add('where exclaim_cliente.id<>-100');
          sql.add('and exclaim_cliente.nome_fantasia like ''%'+pcliente+'%''');
          Try
              open;
              if (Pquery.RecordCount = 0) then
                Pquery.Connection.Commit;
          Except
                on e:exception do
                Begin
                     mensagemerro(e.Message);
                End;
          End;
     End;
end;



function TobjThreadSite.InsereRequisicao(Pinstrucao: integer; pvalor,pidbase: String; Pquery:Tzquery): Boolean;
begin
     Result:=false;

     With Pquery do
     begin
          close;
          sql.clear;
          sql.add('insert into exclaim_lara_instrucoes(base,instrucao,valor,atualizado) values(');
          sql.add(pidbase+','+inttostr(pinstrucao)+','+#39+pvalor+#39+',''N'')');

          Try
              ExecSQL;
              Pquery.Connection.Commit;
              result:=True;
          Except
                on e:exception do
                Begin
                     Mensagemerro(e.message);
                End;
          End;
     End;
     
end;

function TobjThreadSite.VerificaRequisicoes_baseatual(pidbase: String; Pquery:Tzquery): Boolean;
begin
     Result:=false;

     With Pquery do
     begin
          close;
          sql.clear;
          sql.add('Select id,atualizado,erro,datahora,msg,instrucao,base,valor from  exclaim_lara_instrucoes where base='+pidbase);
          sql.add('order by atualizado,datahora desc,id desc');
          Try
              open;
              first;
              result:=True;
          Except
                on e:exception do
                Begin
                     Mensagemerro(e.message);
                End;
          End;
     End;
     
end;




function TobjThreadSite.EnviaRelatorio(PlocalArquivo: String;
ParquivoRemoto:String;  var msg: string): boolean;
var
SR: TSearchRec;
  I: integer;
begin
     result:=False;


     With Self do
     Begin
          Self.EtapaAtual:='Conectando FTP';
          Synchronize(UpdateMensagem);

          if (FTP.Connected)
          then FTP.Disconnect;

          ftp.Host:= HostFtpglobal;
          Ftp.Username:= UsuarioFtpglobal;
          ftp.Password:=SenhaFtpglobal;
          FTP.Passive:=PassiveFtpglobal;

          try
             Ftp.Connect;
          except
              on e:exception do
              Begin
                    msg:='Erro conexao ftp: '+e.Message;
                    exit;
              End;
          end;

          try
             Self.EtapaAtual:='Acessando pastas do FTP';
             Synchronize(UpdateMensagem);

             ftp.ChangeDir('web'); // Abre a Pasta
             ftp.ChangeDir('intranet');
             ftp.ChangeDir('lara');
             ftp.ChangeDir('relatorios');

             Self.EtapaAtual:='Enviando arquivo para o FTP';
             Synchronize(UpdateMensagem);

             FTP.OnWork:=Self.IdFTPWork;

             //***PEGANDO O TAMANHO EM BYTES DO ARQUIVO******
             Self.QuantidadeBytesTotal:= 0;
             with TFileStream.Create(PlocalArquivo, fmOpenRead or fmShareExclusive) do
             try
                 Self.QuantidadeBytesTotal:= Size;
             finally
                     Free;
             end;
             //**********************************************
             //enviando para o ftp
             FTP.Put(PlocalArquivo,ParquivoRemoto);

             result:=True;

          Except
                on e:exception do
                Begin
                      msg:='Erro ftp: '+e.Message;
                      exit;
                End;
          End;
     End;

end;




function TObjThreadSite.CompactaArquivo(PnomeArquivo,ArquivoFinal: string;var msg:string): boolean;
begin
    msg:='';
    result:=False;

    Self.ZipMaster.ZipFilename :=arquivofinal;

    With Self.ZipMaster Do
    Begin
        AddOptions := [];// Default is plain ADD.
        FSpecArgs.Clear;
        FSpecArgs.Add(PnomeArquivo); { specify filenames }
        Try
           add;
           result:=True;
        Except
            on e:exception do
            Begin
                Msg:=e.message;
            End;
        End;

    End;
End;


function TObjThreadSite.DesCompactaArquivo(PnomeArquivo,LocalFinal: string;var msg:string): boolean;
begin
    msg:='';
    result:=False;

    Self.ZipMaster.ZipFilename :=PnomeArquivo;

    With Self.ZipMaster Do
    Begin
        FSpecArgs.Clear;
        ExtrBaseDir := LocalFinal;
        ExtrOptions := ExtrOptions + [ExtrOverwrite];

        Try
           Extract;
           result:=True;
        Except
            on e:exception do
            Begin
                Msg:=e.message;
            End;
        End;

    End;
End;

procedure TObjThreadSite.IdFTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode;const AWorkCountMax: Integer);
begin
     //este componente � chamado no inicio dos trabalhos de transfer�ncia FTP 
end;



procedure TObjThreadSite.IdFTPWork(Sender: TObject; AWorkMode: TWorkMode;
const AWorkCount: Integer);
var
Percentual:real;
begin
    //Esse procedimento � chamado durante a transfer�ncia de dados com
    //este componente

    //QuantidadeBytesTotal      100
    //workcount                   x

    Try
        if (Self.QuantidadeBytesTotal>0)
        Then percentual:=INT((AWorkcount*100)/Self.QuantidadeBytesTotal)
        Else percentual:=0;
    Except
          percentual:=0;
    End;


    Self.EtapaAtual:=floattostr(percentual)+'% FTP Bytes transferidos: '+inttostr(AWorkCount)+' de '+inttostr(Self.QuantidadeBytesTotal);
    Synchronize(UpdateMensagem);
end;


constructor TobjThreadSite.create(caminhobanco, usuario, senha: string;PZConnection: TZConnection;
PZquery:TZquery;PZquery2:TZquery;PObjQuerySenha:Tibquery;
PIBDatabaseBackup: TIBDatabase;PIBTransactionBackup: TIBTransaction;
PIBScript: TIBScript;
PIBDataSetScript: TIBDataSet;
PSL:TSTringList;PFTP:TIdFtp;PZipMaster: TZipMaster);
begin
         inherited create(true);


         Self.ZConnection:=PZConnection;
         
         Self.Zquery:=PZquery;
         Self.Zquery.Connection:=Self.ZConnection;

         Self.Zquery2:=PZquery2;
         Self.Zquery2.Connection:=Self.ZConnection;

         Self.IBTransactionBackup:=PIBTransactionBackup;

         Self.IBDatabaseBackup:=PIBDatabaseBackup;
         Self.IBDatabaseBackup.DefaultTransaction:=Self.IBTransactionBackup;

         Self.IBScript:=PIBScript;

         Self.IBDataSetScript:=PIBDataSetScript;

         Self.ObjQuerySenha:=PObjQuerySenha;
         
         Self.SL:=PSL;
         Self.FTP:=PFTP;
         Self.ZipMaster:=PZipMaster;


end;

function TobjThreadSite.BaixandoArquivosLara(pid, pnomearquivo: string;
  var msg: string): boolean;
var
parquivosite,pdiretorio:string;
posicao:integer;
begin
     //Essa funcao baixa arquivos do Site para o Lara
     //por�m preciso indicar qual � o arquivo no site
     //e qual � a pasta local que deve ser adicionada
     //para isso no site dever� ser enviado
     //enderecosite@@pastalara se vir vazio
     //jogo na raiz senao pego o nome e procuro
     //a pasta se nao encontrara crio a pasta senao retorno Errro

     result:=False;
     pdiretorio:='';
     posicao:=Pos('@@',pnomearquivo);
     parquivosite:=pnomearquivo;

     if (posicao>0)
     Then Begin
                pdiretorio:=copy(pnomearquivo,posicao+2,length(pnomearquivo)-(posicao+1));
                parquivosite:=copy(pnomearquivo,1,posicao-1);
     End;

     if (pdiretorio<>'')
     then Begin
               if (DirectoryExists(RetornaPathSistema+'\'+pdiretorio)=false)
               then Begin
                        if (createdir(RetornaPathSistema+'\'+pdiretorio)=false)
                        then begin
                                 msg:='Erro ao criar diretorio '+RetornaPathSistema+'\'+pdiretorio;
                                 exit;
                         End;
               End;
     End;


     if (Self.baixaarquivo(parquivosite,RetornaPathSistema+Pid+'.zip',msg)=false)
     then exit;



     if (pdiretorio<>'')
     Then Begin
              if (Self.DesCompactaArquivo(RetornaPathSistema+pid+'.zip',RetornaPathSistema+pdiretorio+'\',msg)=false)
               then exit;
     End
     Else Begin
               if (Self.DesCompactaArquivo(RetornaPathSistema+pid+'.zip',RetornaPathSistema,msg)=false)
               then exit;
     End;
     

     Try
        if (deletefile(RetornaPathSistema+pid+'.zip')=false)
        Then msg:='Erro ao tentar excluir '+RetornaPathSistema+pid+'.zip'; 

     Except
           msg:='Erro ao tentar excluir '+RetornaPathSistema+pid+'.zip';
     End;

     result:=True;

end;

(*destructor TobjThreadSite.free;
begin
     Try
         freeandnil(Self.ZConnection);
     Except
     End;

     Try
         freeandnil(Self.Zquery);
     Except
     End;

     Try
         freeandnil(Self.Zquery2);
     Except
     End;


     Try
        freeandnil(Self.IBTransactionBackup);
     Except
     End;

     Try
        freeandnil(Self.IBDatabaseBackup);
     Except
     End;


     Try
        freeandnil(Self.ibDATASETScript);
     Except
     End;

     Try
        freeandnil(Self.IBScript);
     Except
     End;


     try
        freeandnil(Self.ObjQuerySenha);
     Except

     End;

     Try
        freeandnil(Self.SL)
     Except

     End;

     Try
        freeandnil(Self.FTP);
     Except

     End;

     Try
        Freeandnil(Self.ZipMaster);
     Except

     End;

end;*)

end.
