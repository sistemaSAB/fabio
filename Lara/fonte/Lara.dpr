program Lara;

uses
  fastmm4,
  Forms,
  UprincipalExclaim in 'UprincipalExclaim.pas' {FprincipalExclaim},
  U_Cipher in '..\..\Compartilhados\MD5\U_Cipher.pas',
  Md5 in '..\..\Compartilhados\MD5\Md5.pas',
  DCPcrypt in '..\..\Compartilhados\MD5\DCPcrypt.pas',
  Base64 in '..\..\Compartilhados\MD5\Base64.pas',
  DCPconst in '..\..\Compartilhados\MD5\DCPconst.pas',
  IDEA in '..\..\Compartilhados\MD5\IDEA.pas',
  Sha1 in '..\..\Compartilhados\MD5\Sha1.pas',
  UmostraMEnsagem in '..\..\Compartilhados\UmostraMEnsagem.pas' {Fmostramensagem},
  Usobre in 'Usobre.pas' {Fsobre},
  UobjDecodificaRequisicao in 'UobjDecodificaRequisicao.pas',
  UobjREQUISICAOSENHA in '..\..\Compartilhados\UobjREQUISICAOSENHA.pas',
  UobjVersaoSistema in '..\..\Compartilhados\UobjVersaoSistema.pas',
  UUtils in '..\..\Compartilhados\UUtils.pas',
  UobjVERSAOEXECUTAVEL_LARA in 'UobjVERSAOEXECUTAVEL_LARA.pas',
  UBACKUP in 'UBACKUP.pas' {FBACKUP},
  UobjBACKUP in 'UobjBACKUP.pas',
  UobjHORARIOSBACKUP in 'UobjHORARIOSBACKUP.pas',
  UmostraBackup in 'UmostraBackup.pas' {FmostraBackup},
  Ulogin in 'Ulogin.pas' {Flogin},
  UpedeSenha in 'UpedeSenha.pas' {Fpedesenha},
  dlgSearchText in 'dlgSearchText.pas' {TextSearchDialog},
  dlgConfirmReplace in 'dlgConfirmReplace.pas' {ConfirmReplaceDialog},
  dlgReplaceText in 'dlgReplaceText.pas' {TextReplaceDialog},
  UfrScriptSqlLOTE in '..\..\Compartilhados\UfrScriptSqlLOTE.pas' {FrScriptSqlLOTE: TFrame},
  UparametrosAvulso in '..\..\Compartilhados\UparametrosAvulso.pas' {FparametrosAvulso},
  UobjParametrosAvulso in '..\..\Compartilhados\UobjParametrosAvulso.pas',
  uobjconexaomysql in 'uobjconexaomysql.pas',
  UdataMOdulo in 'UdataMOdulo.pas' {FdataModulo: TDataModule},
  asn1util in '..\..\Compartilhados\synasnap\asn1util.pas',
  synsock in '..\..\Compartilhados\synasnap\synsock.pas',
  synacode in '..\..\Compartilhados\synasnap\synacode.pas',
  mimeinln in '..\..\Compartilhados\synasnap\mimeinln.pas',
  mimemess in '..\..\Compartilhados\synasnap\mimemess.pas',
  mimepart in '..\..\Compartilhados\synasnap\mimepart.pas',
  nntpsend in '..\..\Compartilhados\synasnap\nntpsend.pas',
  pingsend in '..\..\Compartilhados\synasnap\pingsend.pas',
  pop3send in '..\..\Compartilhados\synasnap\pop3send.pas',
  slogsend in '..\..\Compartilhados\synasnap\slogsend.pas',
  smtpsend in '..\..\Compartilhados\synasnap\smtpsend.pas',
  snmpsend in '..\..\Compartilhados\synasnap\snmpsend.pas',
  sntpsend in '..\..\Compartilhados\synasnap\sntpsend.pas',
  ssl_openssl in '..\..\Compartilhados\synasnap\ssl_openssl.pas',
  ssl_openssl_lib in '..\..\Compartilhados\synasnap\ssl_openssl_lib.pas',
  synachar in '..\..\Compartilhados\synasnap\synachar.pas',
  synafpc in '..\..\Compartilhados\synasnap\synafpc.pas',
  synaicnv in '..\..\Compartilhados\synasnap\synaicnv.pas',
  synamisc in '..\..\Compartilhados\synasnap\synamisc.pas',
  synaser in '..\..\Compartilhados\synasnap\synaser.pas',
  synautil in '..\..\Compartilhados\synasnap\synautil.pas',
  tlntsend in '..\..\Compartilhados\synasnap\tlntsend.pas',
  httpsend in '..\..\Compartilhados\synasnap\httpsend.pas',
  blcksock in '..\..\Compartilhados\synasnap\blcksock.pas',
  ftpsend in '..\..\Compartilhados\synasnap\ftpsend.pas',
  USuporte in 'USuporte.pas' {Fsuporte},
  UobjThreadSite in 'UobjThreadSite.pas',
  uconfiguracao in 'uconfiguracao.pas' {Fconfiguracao},
  USuporteverificacoes in 'USuporteverificacoes.pas' {FSuporteverificacoes},
  Ugeral in 'Ugeral.pas' {Fgeral},
  uobjBackupSite in 'uobjBackupSite.pas';

{$R *.RES}

begin
  //CoInitialize(nil);
  Application.CreateForm(TFprincipalExclaim, FprincipalExclaim);
  Application.CreateForm(TFsobre, Fsobre);
  Application.CreateForm(TFBACKUP, FBACKUP);
  Application.CreateForm(TFlogin, Flogin);
  Application.CreateForm(TFmostraBackup, FmostraBackup);
  Application.CreateForm(TFpedesenha, Fpedesenha);
  Application.CreateForm(TConfirmReplaceDialog, ConfirmReplaceDialog);
  Application.CreateForm(TFparametrosAvulso, FparametrosAvulso);
  Application.CreateForm(TFdataModulo, FdataModulo);
  Application.CreateForm(TFsuporte, Fsuporte);
  Application.CreateForm(TFconfiguracao, Fconfiguracao);
  Application.CreateForm(TFSuporteverificacoes, FSuporteverificacoes);
  Application.CreateForm(TFgeral, Fgeral);
  Application.Run;
end.
