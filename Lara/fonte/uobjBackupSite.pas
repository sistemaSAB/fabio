unit uobjBackupSite;

interface
uses
  Classes,httpsend,synacode,Dialogs;

type
  tobjBackupSite =  class(TComponent)
  private
    FFCodigoBase: string;
    FFErro: Boolean;
    FFHora: string;
    FFUrl: string;
    FFData: string;
    FFVersaoBase: string;
    FFVersaoLara: string;
    FFMSG: string;
    FFTempoEspera: Integer;
    procedure SetFCodigoBase(const Value: string);
    procedure SetFData(const Value: string);
    procedure SetFErro(const Value: Boolean);
    procedure SetFHora(const Value: string);
    procedure SetFMSG(const Value: string);
    procedure SetFUrl(const Value: string);
    procedure SetFVersaoBase(const Value: string);
    procedure SetFVersaoLara(const Value: string);
    procedure SetFTempoEspera(const Value: Integer);

  public
    property FCodigoBase : string read FFCodigoBase write SetFCodigoBase;
    property FVersaoBase : string read FFVersaoBase write SetFVersaoBase;
    property FData : string read FFData write SetFData;
    property FHora : string read FFHora write SetFHora;
    property FVersaoLara : string read FFVersaoLara write SetFVersaoLara;
    property FMSG : string read FFMSG write SetFMSG;
    property FUrl : string read FFUrl write SetFUrl;
    property FErro : Boolean read FFErro write SetFErro;
    property FTempoEspera : Integer read FFTempoEspera write SetFTempoEspera;
    destructor destroy;override;

    procedure SetaBackupSite(pErroBackup:boolean;pTempoEspera:integer);

    function MemoryStreamToString(M: TMemoryStream): AnsiString;
end;

  tthreadBackupSite = class(TThread)
    public
      CodigoBase, VersaoBase, data, Hora, VersaoLara, MSG, URL: String;
      Erro: boolean;
      TempoEspera: Integer;
      //constructor create(pCodigoBase, pVersaoBase, pdata, pHora, pVersaoLara, pMSG, pURL:String; pErro:boolean; pTempoEspera:integer);
    protected
      procedure execute;override;
    private


      objBackupSite : tobjBackupSite;
  end;


implementation

uses SysUtils;

{ tthreadBackupSite }
{
constructor tthreadBackupSite.create(pCodigoBase, pVersaoBase, pdata,
  pHora, pVersaoLara, pMSG, pURL: String; pErro: boolean; pTempoEspera:integer);
begin
  CodigoBase := '';
  VersaoBase := '';
  data := '';
  Hora := '';
  VersaoLara := '';
  MSG := '';
  URL := '';
  Erro := false;
  TempoEspera := 0;
end;
}
procedure tthreadBackupSite.execute;
begin
  inherited;
  objBackupSite := tobjBackupSite.Create(nil);
  try
    objBackupSite.FFCodigoBase := CodigoBase;
    objBackupSite.FFVersaoBase := VersaoBase;
    objBackupSite.FFData := data;
    objBackupSite.FFHora := Hora;
    objBackupSite.FFVersaoLara := VersaoLara;
    objBackupSite.FFMSG := MSG;
    objBackupSite.FFUrl := URL;
    objBackupSite.SetaBackupSite(Erro,TempoEspera);

  finally
    FreeAndNil(objBackupSite);
  end;

end;

{ tobjBackupSite }

destructor tobjBackupSite.destroy;
begin

  inherited;
end;

function tobjBackupSite.MemoryStreamToString(M: TMemoryStream): AnsiString;
begin
  SetString(Result, PAnsiChar(M.Memory), M.Size);
end;

procedure tobjBackupSite.SetaBackupSite(pErroBackup:boolean;pTempoEspera:integer);
var
  Resposta : TMemoryStream;
  Parametros : string;
  cont, tempo : Integer;
begin
  Resposta := TMemoryStream.Create;
  try
    try
      //preciso tratar a msg para remover #13#10, (') aspas
      FMSG := StringReplace(FMSG,#13,' ',[rfReplaceAll]);
      FMSG := StringReplace(FMSG,#10,' ',[rfReplaceAll]);
      FMSG := StringReplace(FMSG,chr(39),' ',[rfReplaceAll]);
                                   
      //EXEMPLO DE URL http://www.exclaim.com.br/backupsite.php?codigobase=:codigobase&versaobase=:versaobase&data=:data&hora=:hora&versaolara=:versaolara&msg=:msg&erro=:erro
      //Trocando os parametros na url
      FUrl := StringReplace(FUrl,':codigobase',FCodigoBase,[rfReplaceAll]);
      FUrl := StringReplace(FUrl,':versaobase',FVersaoBase,[rfReplaceAll]);
      FUrl := StringReplace(FUrl,':data',FData,[rfReplaceAll]);
      FUrl := StringReplace(FUrl,':hora',FHora,[rfReplaceAll]);
      FUrl := StringReplace(FUrl,':versaolara',FVersaoLara,[rfReplaceAll]);
      FUrl := StringReplace(FUrl,':msg',FMSG,[rfReplaceAll]);
      FUrl := StringReplace(FUrl,':erro',BoolToStr(pErroBackup),[rfReplaceAll]);
      //Gambi pra usar no componente
      Parametros := copy(FUrl,Pos('?',FUrl)+1,Length(FUrl));
      FUrl := Copy(FUrl,1,Pos('?',FUrl)-1);

      //A id�ia � postar, caso n�o consiga enviar por falta de conex�o
      //incrementar o tempo, e tentar novamente, at� que se consiga ou realize 10 tentativas
      //se nao conseguir em 10 tentativas
      cont := 1;
      //o tempo de 60000 = 1 minuto, como ser� um la�o com 10 passadas, ent�o
      //ter�amos a espera de at� 55 minutos
      while cont <= 10 do
      begin
        Resposta.Clear;
        //Efetua post no link
        if HttpPostURL(FUrl,Parametros,Resposta) then
          if(Pos('OK',MemoryStreamToString(Resposta)) > 0) then
            Break;
        tempo := pTempoEspera * cont;
        //tempo := 60000 * cont;

        Sleep(tempo);
        Inc(cont);
      end;
      //Resposta.SaveToFile('d:\respostaweb.txt');
      //ShowMessage(MemoryStreamToString(Resposta));
    except

    end;

  finally
    FreeAndNil(Resposta);
  end;
end;

procedure tobjBackupSite.SetFCodigoBase(const Value: string);
begin
  FFCodigoBase := Value;
end;

procedure tobjBackupSite.SetFData(const Value: string);
begin
  FFData := Value;
end;

procedure tobjBackupSite.SetFErro(const Value: Boolean);
begin
  FFErro := Value;
end;

procedure tobjBackupSite.SetFHora(const Value: string);
begin
  FFHora := Value;
end;

procedure tobjBackupSite.SetFMSG(const Value: string);
begin
  FFMSG := Value;
end;

procedure tobjBackupSite.SetFTempoEspera(const Value: Integer);
begin
  FFTempoEspera := Value;
end;

procedure tobjBackupSite.SetFUrl(const Value: string);
begin
  FFUrl := Value;
end;

procedure tobjBackupSite.SetFVersaoBase(const Value: string);
begin
  FFVersaoBase := Value;
end;

procedure tobjBackupSite.SetFVersaoLara(const Value: string);
begin
  FFVersaoLara := Value;
end;

end.




