unit UpedeSenha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFpedesenha = class(TForm)
    edtsenha: TEdit;
    Label2: TLabel;
    Btok: TButton;
    btcancelar: TButton;
    procedure FormShow(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure BtokClick(Sender: TObject);
    procedure edtsenhaKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fpedesenha: TFpedesenha;

implementation

{$R *.dfm}

procedure TFpedesenha.FormShow(Sender: TObject);
begin
     Self.Tag:=0;
     Self.edtsenha.text:='';
     Self.edtsenha.setfocus;

end;

procedure TFpedesenha.btcancelarClick(Sender: TObject);
begin
     Self.Tag:=0;
     Self.Close;
end;

procedure TFpedesenha.BtokClick(Sender: TObject);
begin
     Self.Tag:=1;
     Self.close;
end;

procedure TFpedesenha.edtsenhaKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then btokclick(sender);
end;

end.
