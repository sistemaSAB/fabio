unit UFTP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, FileCtrl, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdFTP, Gauges, ComCtrls, IdCustomTCPServer,
  IdTCPServer, IdCmdTCPServer, IdExplicitTLSClientServerBase, IdFTPServer;

type
  TFFTP = class(TForm)
    btEnviar: TBitBtn;
    btBaixar: TBitBtn;
    GroupBox3: TGroupBox;
    btExcluir: TBitBtn;
    Gauge: TGauge;
    btCriaPasta: TBitBtn;
    FTP: TIdFTPServer;
    GroupBox2: TGroupBox;
    btAnterior: TButton;
    ListBoxRemoto: TListBox;
    GroupBox1: TGroupBox;
    DirectoryListBox: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    FileListBox: TFileListBox;
    MemoStatus: TMemo;
    GroupBox: TGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdtFTP: TEdit;
    Edtlogin: TEdit;
    EdtSenha: TEdit;
    btConectar: TBitBtn;
    procedure btConectarClick(Sender: TObject);
    procedure btEnviarClick(Sender: TObject);
    procedure btBaixarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure FTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure FTPWork(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure ListBoxRemotoDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btAnteriorClick(Sender: TObject);
    procedure btCriaPastaClick(Sender: TObject);
  private
    bytesToTransfer: Longint;
    StrListAnterior:TStringList;
    Procedure AtualizaListBoxRemoto;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFTP: TFFTP;

implementation

{$R *.dfm}

procedure TFFTP.btConectarClick(Sender: TObject); 
begin

      (*if (FTP.Connected)
      then FTP.Disconnect;

      ftp.Host:= edtFTP.Text;
      Ftp.Username:= EdtLogin.Text;
      ftp.Password:= edtSenha.Text;
      MemoStatus.Clear;
      MemoStatus.Lines.Add('Conectando a "'+edtLogin.text+'" aguarde...');

      try
          Ftp.Connect;
          AtualizaListBoxRemoto;
      except
          ShowMessage('Erro ao tentar conectar');
          exit;
      end;

      //MemoStatus.Lines:=Ftp.Greeting.Text;
      MemoStatus.Lines.Add('Conectado...');
      *)
end;

procedure TFFTP.btEnviarClick(Sender: TObject);
begin
     (*
     MemoStatus.Lines.Add('Efetuando Upload...');
     bytesToTransfer:=0;
     try
         // Barra de Progresso
         if (FileListBox.Items.Count = 0)
         then Abort;

         try
             bytesToTransfer := FTP.Size(FileListBox.Items.Strings[FileListBox.ItemIndex]);
             FTP.Put(FileListBox.FileName,FileListBox.Items.Strings[FileListBox.ItemIndex]);
         except on e:exception do
             //showmessage(e.Message);
         end;

         // Fim codigo barra de progresso
         Ftp.Put(FileListBox.Items.Strings[FileListBox.ItemIndex], FileListBox.Items.Strings[FileListBox.ItemIndex], false);
         AtualizaListBoxRemoto;
     except
         ShowMessage('Erro ao tentar enviar o arquivo');
         exit;
     end;
     MemoStatus.Lines.Add('Arquivo Enviado !');
     *)
end;

procedure TFFTP.btBaixarClick(Sender: TObject);
begin
    (*MemoStatus.Lines.Add('Efetuando Download...');
    try
       Ftp.Get(ListBoxRemoto.items.Strings[ListBoxRemoto.ItemIndex], DirectoryListBox.Directory+'/',false);
    except
       ShowMessage('Erro ao tentar baixar o arquivo');
       exit;
    end;
    MemoStatus.Lines.Add('Arquivo Baixado !');
      *)
end;

procedure TFFTP.btExcluirClick(Sender: TObject);
begin
     (*
     MemoStatus.Lines.Add('Deletado Arquivo da �rea Remota...');
     try
         ftp.Delete(ListBoxRemoto.Items.Strings[ListBoxRemoto.ItemIndex]);
         AtualizaListBoxRemoto;
     except
         ShowMessage('Erro ao tentar Excluir o arquivo');
         exit;
     end;
     MemoStatus.Lines.Add('Arquivo Deletado...');
       *)
end;

procedure TFFTP.FTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode; const AWorkCountMax: Integer);
begin
    Gauge.Progress := 0;
    if (AWorkCountMax > 0)
    then Gauge.MaxValue := AWorkCountMax
    else Gauge.MaxValue := bytesToTransfer;
end;

procedure TFFTP.FTPWork(Sender: TObject; AWorkMode: TWorkMode;const AWorkCount: Integer);
begin
     Gauge.Progress := AWorkCount;
end;

procedure TFFTP.ListBoxRemotoDblClick(Sender: TObject);
begin
(*    StrListAnterior.Add(ListBoxRemoto.Items[ListBoxRemoto.ItemIndex]);
    ftp.ChangeDir(ListBoxRemoto.Items[ListBoxRemoto.ItemIndex]);
    AtualizaListBoxRemoto;
  *)
end;

procedure TFFTP.FormShow(Sender: TObject);
begin
     try
         StrListAnterior:=TStringList.Create;
     except
         ShowMessage('Erro ao tentar criar a StringList');
         exit;
     end;
end;

procedure TFFTP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if (StrListAnterior <> nil)
     then FreeAndNil(StrListAnterior);
end;

procedure TFFTP.btAnteriorClick(Sender: TObject);
begin
(*    ftp.ChangeDirUp;
    AtualizaListBoxRemoto;
    *)
end;

procedure TFFTP.btCriaPastaClick(Sender: TObject);
begin
(*      ftp.MakeDir(InputBox('Nova Pasta', 'Informe o nome da Pasta', ''));
      AtualizaListBoxRemoto;
      *)
end;

procedure TFFTP.AtualizaListBoxRemoto;
Var StrListPastas, StrListArquivos:TStringList;
    Cont:Integer;
begin
(*     // Como cada ves que a funcao preenche o listbox ela limpa a anterior
     // entaum vou dividi-la em duas antes de preencher
     // umas com as pastas e outra com os arquivos
try
    try
        StrListPastas:=TStringList.Create;
        StrListArquivos:=TStringList.Create;
    except
        Showmessage('Erro ao tentar criar as StringList');
        exit;
    end;

    StrListPastas.Clear;
    StrListArquivos.Clear;

    ftp.List(StrListPastas,'',false);
    StrListPastas.Sorted:=true;  // Ordem Alfabetica

    ftp.List(StrListArquivos,'*',false);
    StrListArquivos.Sorted:=true;  // Ordem Alfabetica

    ListBoxRemoto.Clear;
    // Primeiro Coloco as pastas
    for Cont:=0 to StrListPastas.Count-1 do
    Begin
         ListBoxRemoto.Items.Add(StrListPastas[Cont]);
    end;

    // Depois os arquivos
    for Cont:=0 to StrListArquivos.Count-1 do
    Begin
         ListBoxRemoto.Items.Add(StrListArquivos[Cont]);
    end;

finally
    FreeAndNil(StrListPastas);
    FreeAndNil(StrListArquivos);

end;
*)
end;

end.

