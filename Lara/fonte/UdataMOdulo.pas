unit UdataMOdulo;

interface

uses
  windows,dialogs,forms,graphics,UObjthreadSite,useg,SysUtils, Classes,uobjdecodificarequisicao,
  uobjversaosistema,uobjversaoexecutavel_lara,
  UObjhorariosBackup,uobjbackup,uobjparametrosavulso, ExtCtrls,
  ZConnection, ZipMstr, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset,
  IBDatabase, IBCustomDataSet, IBQuery, IBScript, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdFTP;

type
  TFdataModulo = class(TDataModule)
    TimerSite: TTimer;
    ZConnection: TZConnection;
    ZQuery: TZQuery;
    ZQuery2: TZQuery;
    IBDatabaseSenha: TIBDatabase;
    IBTransactionSenha: TIBTransaction;
    ObjQuerySenha: TIBQuery;
    FTP: TIdFTP;
    IBScript: TIBScript;
    IBDataSetScript: TIBDataSet;
    ZipMaster: TZipMaster;
    ZipMasterSite: TZipMaster;
    IBDatabaseSite: TIBDatabase;
    IBTransactionSite: TIBTransaction;
    IBDatabaseVencimento: TIBDatabase;
    IBTransactionVencimento: TIBTransaction;
    ibtransactionPesquisa: TIBTransaction;
    ibqueryPesquisa: TIBQuery;
    procedure TimerSiteTimer(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
         SLX:TStringList;
    Procedure MyThreadTerminate(sender:Tobject);






  public
    { Public declarations }
    ObjtreadSite:TObjThreadSite;
    Function CompactaArquivo(PnomeArquivo:String;ArquivoFinal:string;var msg:string):boolean;
    Function RecarregaParametros(var msg:string):boolean;
  end;

var
  FdataModulo: TFdataModulo;
  pararsiteglobal,atualizaexeglobal,ParaValidacaoGlobal,conectadoglobal:boolean;

  ObjDecodificaRequisicao:Tobjdecodificarequisicao;
  ObjVersaoSistema:TObjVersaosistema;
  ObjversaoExecutavel_BD:TObjversaoExecutavel;
  ObjhorariosBackup:TobjhorariosBackup;
  ObjparametroGlobal:TObjParametrosAvulso;

  pathglobal:string;
  HostSiteglobal,BdSiteglobal,PortaSiteglobal,UsuarioSiteglobal,SenhaSiteglobal,ProtocoloSiteglobal:String;
  HostFtpglobal,UsuarioFtpglobal,SenhaFtpglobal:string;
  PassiveFtpglobal:boolean;

  pversaobdglobal,pversaoexeglobal:string;
  CorFormGlobal:integer;

implementation

uses UprincipalExclaim;

{$R *.dfm}




function TFdataModulo.CompactaArquivo(PnomeArquivo,
  ArquivoFinal: string;var msg:string): boolean;
begin
    msg:='';
    result:=False;

    Self.ZipMaster.ZipFilename :=arquivofinal;

    With Self.ZipMaster Do
    Begin
        AddOptions := [];// Default is plain ADD.
        FSpecArgs.Clear;
        FSpecArgs.Add(PnomeArquivo); { specify filenames }
        Try
           add;
           result:=True;
        Except
            on e:exception do
            Begin
                Msg:=e.message;
            End;
        End;

    End;

end;

procedure TFdataModulo.TimerSiteTimer(Sender: TObject);
var
texto:string;
begin
      if (pararsiteglobal)
      then exit;


      Self.TimerSite.Enabled:=False;

      Try

           //-- True - Cria a Thread suspensa
           Self.ObjtreadSite:=TObjThreadSite.Create(pathglobal,DesincriptaSenha('��������'),DesincriptaSenha('��������'),Self.ZConnection,Self.zquery,Self.Zquery2,self.ObjQuerySenha,Self.IBDatabaseSite,Self.IBTransactionSite,Self.IBScript,Self.IBDataSetScript,Self.SLX,Self.FTP,Self.ZipMasterSite);
       Except
           on e:exception do
           Begin
                 FprincipalExclaim.lbmensagemrodape.caption:=e.message;
                 FprincipalExclaim.lbmensagemrodape.font.color:=clred;
                 exit;
           End;
       End;
       
      Self.ObjtreadSite.Priority:=tpIdle;//prioridade
      //-- Atribui um procedimento ao terminar
      //-- a execu��o da thread
      //Self.ObjtreadSite.FreeOnTerminate:=true;
      Self.ObjtreadSite.OnTerminate:= Self.MyThreadTerminate;
      Self.ObjtreadSite.Resume;


end;

procedure TFdataModulo.MyThreadTerminate(sender: Tobject);
begin
     Try
         //Self.ObjtreadSite.free;
         Self.ObjtreadSite:=nil; 
     Except
           messagedlg('thread terminate',mterror,[mbok],0);
     End;
     
     FprincipalExclaim.LbSite.caption:='Terminou: '+datetimetostr(now);

     if (AtualizaExeGlobal=True)
     Then FprincipalExclaim.TimerEncerra.enabled:=True
     Else Self.TimerSite.Enabled:=true;

end;

Function TFDataModulo.RecarregaParametros(var msg:string):boolean;

begin
     result:=False;

     if (conectadoglobal=False)
     Then begin
                msg:='N�o conectado';
                exit;
     End;

     if (objparametroglobal.ValidaParametro('TEMPO DE VERIFICACAO NO SITE')=False)
     Then Self.TimerSite.Interval:=300000;//5 minutos

     Try
           Self.timersite.interval:=StrToInt64(objparametroglobal.Get_Valor);
     Except
           Self.timersite.interval:=300000;//5 minutos
     End;


     if (objparametroglobal.ValidaParametro('ENDERECO SITE')=True)
     Then HostSiteglobal:=objparametroglobal.Get_Valor;

     if (objparametroglobal.ValidaParametro('BD SITE')=true)
     Then BdSiteglobal:=objparametroglobal.Get_Valor;

     if (objparametroglobal.ValidaParametro('PORTA SITE')=true)
     Then PortaSiteglobal:=objparametroglobal.Get_Valor;

     if (objparametroglobal.ValidaParametro('USUARIO SITE')=true)
     Then UsuarioSiteglobal:=useg.DesincriptaSenha(objparametroglobal.Get_Valor);

     if (objparametroglobal.ValidaParametro('SENHA SITE')=true)
     Then SenhaSiteglobal:=useg.DesincriptaSenha(objparametroglobal.Get_Valor);

     if (objparametroglobal.ValidaParametro('PROTOCOLO SITE')=true)
     Then ProtocoloSiteglobal:=objparametroglobal.Get_Valor;

     if (objparametroglobal.ValidaParametro('ENDERECO FTP')=true)
     Then HostFtpglobal:=objparametroglobal.Get_Valor;

     if (objparametroglobal.ValidaParametro('USUARIO FTP')=true)
     Then UsuarioFtpglobal:=objparametroglobal.Get_Valor;

     if (objparametroglobal.ValidaParametro('SENHA FTP')=true)
     Then SenhaFtpglobal:=Useg.DesincriptaSenha(objparametroglobal.Get_Valor);

     if (ObjparametroGlobal.ValidaParametro('PASSIVE FTP')=true)
     Then Begin
               PassiveFtpglobal:=False;

               if (objparametroglobal.Get_Valor='SIM')
               Then PassiveFtpglobal:=true;
     End;

     pararsiteglobal:=True;
     FprincipalExclaim.LbSite.caption:='Desativado';
     
     if (ObjparametroGlobal.ValidaParametro('UTILIZA CONEXAO SITE')=true)
     Then Begin
               if (objparametroglobal.Get_Valor='SIM')
               Then Begin
                        pararsiteglobal:=False;
                        FprincipalExclaim.LbSite.caption:='...';
               end;
     End;

     result:=true;
end;




procedure TFdataModulo.DataModuleCreate(Sender: TObject);
begin
     pararsiteglobal:=False;
     Self.SLX:=TStringList.create;
end;

procedure TFdataModulo.DataModuleDestroy(Sender: TObject);
begin
     freeandnil(Self.SLX);
end;

end.



