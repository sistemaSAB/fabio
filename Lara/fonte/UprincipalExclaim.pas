
unit UprincipalExclaim;

interface

uses
  UOBJPARAMETROSavulso,inifiles,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IBCustomDataSet, IBQuery, IBDatabase, StdCtrls, jpeg,
  ExtCtrls, Menus,UobjrequisicaoSenha, abfComponents,IBServices, Buttons,useg,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, UobjBACKUP,psapi;

type
  TFprincipalExclaim = class(TForm)
    TimerLogin: TTimer;
    Lbmensagemrodape: TLabel;
    TimerRequisicao: TTimer;
    abfTrayIcon1: TabfTrayIcon;
    PopupMenu1: TPopupMenu;
    Sobre1: TMenuItem;
    MdulodeBackup1: TMenuItem;
    Sair1: TMenuItem;
    Lbbackup_1: TLabel;
    Lbbackup_2: TLabel;
    Lbbackup_3: TLabel;
    Lbbackup_4: TLabel;
    Lbbackup_5: TLabel;
    abfOneInstance1: TabfOneInstance;
    Parmetros1: TMenuItem;
    Suporte1: TMenuItem;
    LbSite: TLabel;
    Label1: TLabel;
    lbrodape: TLabel;
    TimerEncerra: TTimer;
    LbErros: TLabel;
    TimerAtualizaVencimento: TTimer;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Image1: TImage;
    PanelExclaim: TPanel;
    Memo1: TMemo;
    Panel1: TPanel;
    btAtualizaVencimento: TButton;
    btParar: TButton;
    edtIntervalo: TEdit;
    Label5: TLabel;
    TimerReduzMem: TTimer;
    procedure TimerLoginTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TimerRequisicaoTimer(Sender: TObject);
    procedure Sobre1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MdulodeBackup1Click(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
    procedure Parmetros1Click(Sender: TObject);
    procedure Suporte1Click(Sender: TObject);
    procedure TimerEncerraTimer(Sender: TObject);
    procedure btAtualizaVencimentoClick(Sender: TObject);
    procedure TimerAtualizaVencimentoTimer(Sender: TObject);
    procedure btPararClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TimerReduzMemTimer(Sender: TObject);
  private
    { Private declarations }
         HoraAnterior:TDateTime;

         Erros:Tstringlist;
         URL:string;

         Procedure ChamaLogin;
         function  PegaPathBanco(nomechave: String): String;
         Function  ValidaVersao:Boolean;
         function  come(texto: string; simbolo: Char): string;
         Procedure ChamaBackup;
         function  CarregaAtualizacao: boolean;
         Procedure EncerraTudo;
         procedure VerificaPausa;

         Function ChecaVencimentoAutomatico:boolean;
         function PegaPathSistema: String;
         procedure AtivaVerificacao;
         procedure DesativaVerificacao;
    procedure TrimAppMemorySize;

  public
    { Public declarations }
  end;

  THDChecaVencimento=class(TThread)
    private
      PMensagemErro : TStringList;
    public
      procedure Execute;override;
  end;

var
  FprincipalExclaim: TFprincipalExclaim;


implementation

uses Usobre, UUtils, UBACKUP, UmostraBackup, UparametrosAvulso, UdataMOdulo,
  UobjDecodificaRequisicao, UobjHORARIOSBACKUP,
  UobjVersaoSistema, UobjVERSAOEXECUTAVEL_LARA, USuporte,UObjthreadsite,
  UpedeSenha, uconfiguracao,DateUtils, httpsend, uobjBackupSite;


{$R *.dfm}

function TFprincipalExclaim.come(texto:string;simbolo:Char):string;
var //funcao que retira as barras de data de um tipo data passando para string
i :integer;
textosai:string;
begin

   textosai:='';
   for i :=1 to length(texto)
   do begin

      if (texto[i] =simbolo)
      then
      else
         textosai:=textosai+texto[i];
   end;
   result:=textosai;
end;




function TFprincipalExclaim.PegaPathBanco(nomechave:String): String;
var
Temp:string;
arquivo_ini:Tinifile;
begin
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        Arquivo_ini:=Tinifile.Create(Temp+'Path.Ini');
     Except
           Messagedlg('Erro na Abertura do Arquivo Path.ini',mterror,[mbok],0);
           Result:='';
           exit;
     End;

     Try
        Try
            Temp:='';
            if (nomechave='')
            Then Temp:=arquivo_ini.ReadString('SISTEMA','PATH','')
            Else Temp:=arquivo_ini.ReadString('SISTEMA',uppercase(nomechave),'')
        Except
              Messagedlg('Erro na Leitura da chave PATH do SISTEMA!',mterror,[mbok],0);
        End;
     finally
         freeandnil(arquivo_ini);
     End;

     Result:=Temp;
end;

procedure TFprincipalExclaim.ChamaLogin;
var
tmpsenha,NomeOld,pathsemarq,temp:string;
pvalidaplugue:boolean;
begin
      //Primeiro procuro o path para se conectar no banco de dados
      pathglobal:='';
      pathglobal:=Self.PegaPathBanco('PATH_SENHA');

      Try
         //Os parametros tem sua pr�pria IBDATABASE
         ObjparametroGlobal:=tobjparametrosavulso.create(pathglobal);

         //verificando no parametro
         //se tem plugue ou n�o
         pvalidaplugue:=True;
         
         //Usei o True de SIlencioso, assim basta apagar este parametro
         //para que a software house n�o saiba que tem como ativar sem pluque
         //o parametro procurado � 'VERIFICACAO SITE PLG'
         //Criptografei para evitar busca de string no EXE
         if (ObjparametroGlobal.ValidaParametro(useg.DesincriptaSenha('�����������୷��ఴ�'),True)=True)
         Then Begin
                   //s� verifica se o parametro foi encontrado, caso ele n�o tenha sido
                   //o pvalidaplugue ficou TRUE


                   //o Parametro tem q estar com o valor Criptografado com NAO para que ele nao use
                   //o plugue, qq coisa diferente disso ele vai verificar o plugue
                   //para evitar a tentativa de fraude
                   if (
                   (uppercase(useg.DesincriptaSenha(ObjparametroGlobal.Get_Valor))='N�O')
                    OR (uppercase(useg.DesincriptaSenha(ObjparametroGlobal.Get_Valor))='NAO')
                     )
                  Then Begin
                            //Antes eu validava pelo <>'SIM'   mas a� se alguem fosse
                            //no valor do parametro e digitasse qq coisa ele nao validaria
                            //o plugue
                            pvalidaplugue:=False;
                  End;
         End;


         if (pvalidaplugue=true)
         then Self.caption:=Self.caption+' - HL';

         //Criando o Objeto de Requisi��o de Login do Software, tmbm tem sua pr�pria IBDATABASE
         ObjDecodificaRequisicao:=Tobjdecodificarequisicao.create(pathglobal,pvalidaplugue);

         if (ObjDecodificaRequisicao.PmensagemExterna<>'')
         Then Begin
                   Self.Lbmensagemrodape.caption:=ObjDecodificaRequisicao.PmensagemExterna;
                   Self.LbErros.caption:=ObjDecodificaRequisicao.PmensagemExterna;
         End;

         //Criando o objeto que cuida dos Backups, tmbm tem sua pr�pria ibdatabase
         ObjhorariosBackup:=TobjhorariosBackup.create(pathglobal);
         Lbmensagemrodape.caption:='Conectado';
         Lbmensagemrodape.font.color:=clblue;
         //zero a resposta de tudo que estiver pendente
         ObjDecodificaRequisicao.ExcluiRequisicoesPendentes;
         conectadoglobal:=true;
      Except
            on e:exception do
            begin
                //MensagemErro('N�o foi poss�vel conectar ao servidor'+#13+E.message);
                Lbmensagemrodape.caption:=E.message;
                Lbmensagemrodape.font.color:=clred;
                TimerLogin.Enabled := true;
                exit;
            End;
      End;


    try
       ObjVersaoSistema :=TObjVersaosistema.create;
    Except
          on e:exception do
          Begin
                lbmensagemrodape.caption:=e.message;
                lbmensagemrodape.font.color:=clred;
                exit;
          End;
    End;

    Try
        //USUARIO PROTECAO
        //SENHA   PROTECAO
        ObjversaoExecutavel_BD:=TObjversaoExecutavel.Create(pathglobal,DesincriptaSenha('��������'),DesincriptaSenha('��������'));
    Except
          on e:exception do
          Begin
                lbmensagemrodape.caption:=e.message;
                lbmensagemrodape.font.color:=clred;
                exit;
          End;
    end;

    Try
          FdataModulo.IBDatabaseSenha.Databasename:=pathglobal;
          FdataModulo.IBDatabaseSenha.Params.clear;
          FdataModulo.IBDatabaseSenha.Params.Add('User_name='+DesincriptaSenha('��������'));
          FdataModulo.IBDatabaseSenha.Params.Add('password='+DesincriptaSenha('��������'));
          FdataModulo.IBDatabaseSenha.Open;
    Except
          on e:exception do
          Begin
                lbmensagemrodape.caption:=e.message;
                lbmensagemrodape.font.color:=clred;

                if (FdataModulo.IBDatabaseSenha.Connected) then
                  FdataModulo.IBDatabaseSenha.Close;

                exit;
          End;
    End;



    Try
       Fdatamodulo.RecarregaParametros(temp);
    Except
          on e:exception do
          Begin
                lbmensagemrodape.caption:=e.message;
                lbmensagemrodape.font.color:=clred;
                exit;
          End;
    End;

    TimerRequisicao.Enabled:=True;
    Self.ChamaBackup;
    Fdatamodulo.TimerSite.Enabled:=True;

    //***************Atualiza��a automatica vencimento**********************//

    //URL := 'http://www.exclaim.com.br/intranet/lista_senha_cliente.php?id_cliente_base=:PCODIGOBASE';
    if(ObjparametroGlobal.ValidaParametro('URL VENCIMENTO'))
    then URL := ObjparametroGlobal.Get_Valor;

    //verificar uma vez a senha sempre ao acessar
    Self.ChecaVencimentoAutomatico;

    //depois ativa o timer para iniciar as verifica��es em tempo determinado por parametro
    Self.AtivaVerificacao;

end;

procedure TFprincipalExclaim.TimerLoginTimer(Sender: TObject);
begin
     TimerLogin.enabled:=False;
     Self.ChamaLogin;

end;

procedure TFprincipalExclaim.FormCreate(Sender: TObject);
begin
     Self.LbErros.caption:='';
     Self.HoraAnterior:=now;
     Self.TimerEncerra.enabled:=False;

     ParaValidacaoglobal:=False;

     Lbmensagemrodape.caption:='';
     Lbmensagemrodape.font.color:=clblack;

     Lbbackup_1.caption:='';
     Lbbackup_2.caption:='';
     Lbbackup_3.caption:='';
     Lbbackup_4.caption:='';
     Lbbackup_5.caption:='';

     TimerLogin.Enabled:=True;
     Conectadoglobal:=False;
     atualizaexeglobal:=False;

end;

procedure TFprincipalExclaim.TimerRequisicaoTimer(Sender: TObject);
var
pmensagem:String;
HoraAtual:TDateTime;
begin
     if (ParaValidacaoGlobal=True)
     Then exit;
     Try

        TimerRequisicao.Enabled:=False;

        HoraAtual:=Now;

        {if (MinutesBetween(HoraAtual,Self.HoraAnterior)>1)//a cada 10 Minutos
        Then Begin
                  Self.HoraAnterior:=horaAtual;
                  ObjDecodificaRequisicao.VerificaPlugue;

        End;}

        //Application.ProcessMessages;
        ObjDecodificaRequisicao.PmensagemExterna:='';
        if (ObjDecodificaRequisicao.VerificaRequisicao=False)
        then LbErros.caption:=LbErros.caption+#13+ObjDecodificaRequisicao.PmensagemExterna;
        
        Lbmensagemrodape.caption:=ObjDecodificaRequisicao.PmensagemExterna;

        if (Self.ValidaVersao=False)
        Then Begin
                  ParaValidacaoGlobal:=True;
                  TimerEncerra.Enabled:=True;

        End;


     Finally
            TimerRequisicao.Enabled:=True;
     End;
end;


procedure TFprincipalExclaim.Sobre1Click(Sender: TObject);
begin
     Fsobre.Showmodal;
end;

function TFprincipalExclaim.ValidaVersao: Boolean;
var
PversaoExe:Integer;
PversaoBD:integer;
PversaoExe_S,PversaoBD_S:String;
begin
     Result:=False;

     try
        PversaoExe:=Strtoint(trim(come(ObjVersaoSistema.VersaodoArquivo,'.')));
        PversaoExe_S:=ObjVersaoSistema.VersaodoArquivo;

        Self.caption :='Lara - Sistema de Controle Anti-Pirataria.  Vers�o: '+PversaoExe_S;

     Except
         Lbmensagemrodape.caption:='Erro na tentativa de Recuperar a vers�o do execut�vel';
         Lbmensagemrodape.font.color:=clred;
         exit;
     End;


     if (ObjversaoExecutavel_BD.LocalizaCodigo('0')=False)
     Then Begin
               Messagedlg('Erro na tentativa de Localizar a vers�o do execut�vel no banco de dados',mterror,[mbok],0);
               exit;
     End;
     ObjversaoExecutavel_BD.TabelaparaObjeto;
     ObjversaoExecutavel_BD.Commit;

     Try
       PversaoBD:=Strtoint(trim(come(ObjversaoExecutavel_BD.Get_VERSAO,'.')));
       PversaoBD_S:=ObjversaoExecutavel_BD.Get_VERSAO;
     Except
          Lbmensagemrodape.caption:='Erro na tentativa de resgatar a vers�o do execut�vel no banco de dados';
          Lbmensagemrodape.font.color:=clred;
          exit;
     end;

     pversaoexeglobal:=PversaoExe_s;
     pversaobdglobal:=PversaoBD_S;


     if (PversaoExe<>PversaoBD)
     Then begin
               Self.lbrodape.caption:='VERS�O BD '+PversaoBD_S+' VERS�O EXE '+PversaoExe_S;

                (*//nao pergunto nada, ja tento atualizar
                if (Self.CarregaAtualizacao=True)
                Then Begin
                          result:=true;
                          Self.close;
                End;


                *)
     end
     Else result:=true;
end;




procedure TFprincipalExclaim.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Self.EncerraTudo;

  

end;

procedure TFprincipalExclaim.MdulodeBackup1Click(Sender: TObject);
begin
     if (conectadoglobal=false)
     then exit;


     Fbackup.PassaObjeto(ObjhorariosBackup);
     ObjhorariosBackup.DestroiBackups;
     Fbackup.showmodal;
     Self.ChamaBackup;
end;

procedure TFprincipalExclaim.Sair1Click(Sender: TObject);
begin
     Self.close;
end;



procedure TFprincipalExclaim.ChamaBackup;
begin
    ObjhorariosBackup.ResgataBackups;

    if (ObjhorariosBackup.ObjBackups[0]<>nil)
    Then Begin
              ObjhorariosBackup.ObjBackups[0].Lbbackup:=Self.Lbbackup_1;
              Self.Lbbackup_1.OnClick:=ObjhorariosBackup.ObjBackups[0].LbbackupClick;
    End;

    if (ObjhorariosBackup.ObjBackups[1]<>nil)
    Then Begin
              ObjhorariosBackup.ObjBackups[1].Lbbackup:=Self.Lbbackup_2;
              Self.Lbbackup_2.OnClick:=ObjhorariosBackup.ObjBackups[1].LbbackupClick;
    End;

    if (ObjhorariosBackup.ObjBackups[2]<>nil)
    Then Begin
              ObjhorariosBackup.ObjBackups[2].Lbbackup:=Self.Lbbackup_3;
              Self.Lbbackup_3.OnClick:=ObjhorariosBackup.ObjBackups[2].LbbackupClick;
    End;

    if (ObjhorariosBackup.ObjBackups[3]<>nil)
    Then BEgin
              ObjhorariosBackup.ObjBackups[3].Lbbackup:=Self.Lbbackup_4;
              Self.Lbbackup_4.OnClick:=ObjhorariosBackup.ObjBackups[3].LbbackupClick;
    End;

    if (ObjhorariosBackup.ObjBackups[4]<>nil)
    Then Begin
              ObjhorariosBackup.ObjBackups[4].Lbbackup:=Self.Lbbackup_5;
              Self.Lbbackup_5.OnClick:=ObjhorariosBackup.ObjBackups[4].LbbackupClick;
    End;
end;

procedure TFprincipalExclaim.Parmetros1Click(Sender: TObject);
begin

     FparametrosAvulso.PassaObjeto(ObjparametroGlobal);
     FparametrosAvulso.showmodal;
end;


procedure TFprincipalExclaim.Suporte1Click(Sender: TObject);
begin

     //senha exclaim147


     Fpedesenha.showmodal;

     if (Fpedesenha.tag=0)
     Then exit;

     if (Fpedesenha.edtsenha.text<>useg.DesincriptaSenha('����������'))
     then Begin
               MensagemErro('Senha Inv�lida');
               exit;
     End;


     Fsuporte.showmodal;
end;

Function  TFprincipalExclaim.CarregaAtualizacao:boolean;
var
Path:String;
Path_pchar:pchar;
Begin
     result:=False;

     path:=ExtractFilePath(Application.ExeName);
     if (path[length(path)]<>'\')
     Then path:=path+'\';

     path:=path+'AtualizaExe.Exe';

     if (FileExists(path)=False)
     Then Begin
               Messagedlg('O arquivo '+Path+' n�o foi encontrado',mterror,[mbok],0);
               exit;
     End;

     Winexec(pchar(path),SW_SHOWNORMAL);
     result:=true;
End;

procedure TFprincipalExclaim.TimerEncerraTimer(Sender: TObject);
begin
     Self.TimerEncerra.Enabled:=False;
     
     if (Self.CarregaAtualizacao)
     then Begin
              Self.lbrodape.caption:='Encerrando o aplicativo para nova vers�o';
              Self.TimerLogin.Enabled:=False;
              Self.TimerRequisicao.Enabled:=False;
              Self.EncerraTudo;//procedimento
              Application.Terminate;
     End;
end;


procedure TFprincipalExclaim.EncerraTudo;
begin
   Try
    Self.Timerlogin.enabled:=False;
   Except
   End;

   Try
    Self.TimerRequisicao.enabled:=False;
   Except
   End;

   try
    Self.TimerEncerra.enabled:=False;
   Except
   End;


   Try
    if (ObjversaoExecutavel_BD<>nil)
    Then ObjversaoExecutavel_BD.free;
   Except
   End;


   Try
    if (ObjDecodificaRequisicao<>nil)
    then ObjDecodificaRequisicao.free;
   Except
   End;

   Try
    if (ObjVersaoSistema<>nil)
    Then freeandnil(ObjVersaoSistema);
   Except
   End;

   Try
    if (ObjhorariosBackup<>nil)
    Then ObjhorariosBackup.free;
   Except
   End;

  try
      FdataModulo.IBDatabaseSenha.Close;
  Except
  End;

   //10/11/2010 celio - foi verificado que esses objetos nao estavam sendo liberados
   Try
    if (ObjparametroGlobal<>nil)
    Then ObjparametroGlobal.free;
   Except
   End;

   Try
    if (ObjVersaoSistema<>nil)
    Then ObjVersaoSistema.free;
   Except
   End;

end;

procedure TFprincipalExclaim.VerificaPausa;
var
  path,Temp : string;
  arquivo_ini:Tinifile;
begin
      path := ExtractFilePath(Application.ExeName);
      If (path[length(path)]<>'\')
      Then path:=path+'\';

      //o lance � o seguinte: se n�o encontrar o arquivo de espera ent�o retorno true
      //pois n�o tem configurado
      
      If(FileExists(path+'ESPERA.INI'))
      then begin
           Try
              Arquivo_ini:=Tinifile.Create(path+'Espera.Ini');
           Except
                 Mensagemerro('Erro na Abertura do Arquivo Espera.ini');
                 exit;
           End;

           Try
              Try
                  Temp:='';
                  Temp:=arquivo_ini.ReadString('ESPERA','TEMPO','');
              Except
                    Mensagemerro('Erro na Leitura da chave TEMPO da ESPERA!');
                    exit;
              End;
           finally
               freeandnil(arquivo_ini);
           End;

           Try
                strtoint(temp);
                Sleep(StrToInt(Temp));
           Except
                Mensagemerro('Tempo para espera inv�lido no arquivo '+path+'Espera.Ini');
                exit;
           End;
      end;

end;

function TFprincipalExclaim.ChecaVencimentoAutomatico: boolean;
VAR
    ThreadVerifica:THDChecaVencimento;
begin
      TimerAtualizaVencimento.Enabled:=false;
      try
            ThreadVerifica := THDChecaVencimento.Create(True);
            ThreadVerifica.FreeOnTerminate:=true;
            ThreadVerifica.Resume;
      except
            on e:exception do
                  Mensagemerro(e.Message);
      end;
      TimerAtualizaVencimento.Enabled:=false;//� necess�rio pois ao final da thread ela ativa o timer
end;

procedure TFprincipalExclaim.btAtualizaVencimentoClick(Sender: TObject);
begin

      if(edtIntervalo.Text<>'')
      then begin
            TimerAtualizaVencimento.Interval := StrToInt(edtintervalo.Text);
            TimerAtualizaVencimento.Enabled := true;
      end
      else self.ChecaVencimentoAutomatico;

end;

procedure TFprincipalExclaim.TimerAtualizaVencimentoTimer(Sender: TObject);
VAR
    ThreadVerifica:THDChecaVencimento;
begin
      //self.ChecaVencimentoAutomatico;

      try
            Erros := TStringList.Create;
            ThreadVerifica := THDChecaVencimento.Create(True);
            ThreadVerifica.FreeOnTerminate:=true;
            ThreadVerifica.Resume;
            FreeAndNil(Erros);
      except
            on e:exception do
                  Mensagemerro(e.Message);
      end;
end;

procedure TFprincipalExclaim.btPararClick(Sender: TObject);
begin
      TimerAtualizaVencimento.Enabled := false;
end;


{ THDChecaVencimento }



procedure THDChecaVencimento.Execute;
var
      QueryVencto,QueryServidor:TIBQuery;
      ListaBases,STRLRetorno:TStringList;
      StringRetorno,URLLocal,TEMP,CNPJ:String;
      Backup:Tobjbackup;
      cont:integer;
      Verifica:boolean;
      DataVencimento,DataAtual:TDate;
begin
      inherited;
      Priority := tpLower;

      FprincipalExclaim.TimerAtualizaVencimento.Enabled := false;
      //Se n�o achou a URL, ent�o nem precisa fazer nada, j� sai logo apos desativar o timer para n�o acessar mais
      if(FprincipalExclaim.URL='')
      then exit;

      try
            QueryVencto := TIBQuery.Create(nil);
            QueryVencto.Database := FdataModulo.IBDatabaseSenha;
      except
            on e:exception do
            begin
                  Mensagemerro(e.Message);
                  exit;
            end;
      end;
      try
            ListaBases := TStringList.Create;
      except
            on e:exception do
            begin
                  FreeAndNil(QueryVencto);
                  Mensagemerro(e.Message);
                  exit;
            end;
      end;
      try
            STRLRetorno := TStringList.Create;
      except
            on e:exception do
            begin
                  FreeAndNil(QueryVencto);
                  FreeAndNil(ListaBases);
                  Mensagemerro(e.Message);
                  exit;
            end;
      end;
      try
            Backup := TObjBACKUP.Create(pathglobal);
      except
            on e:exception do
            begin
                  FreeAndNil(QueryVencto);
                  FreeAndNil(ListaBases);
                  FreeAndNil(STRLRetorno);
                  Mensagemerro(e.Message);
                  exit;
            end;
      end;

      try
            PMensagemErro := TStringList.Create;
      except
            on e:exception do
            begin
                  FreeAndNil(QueryVencto);
                  FreeAndNil(ListaBases);
                  FreeAndNil(STRLRetorno);
                  Backup.Free;
                  Mensagemerro(e.Message);
                  exit;
            end;
      end;


      //limpando e preenchendo variaveis
      try
            QueryServidor := TIBQuery.Create(nil);
            QueryServidor.Database := FdataModulo.IBDatabaseVencimento;

      except
            on e:exception do
            begin
                  FreeAndNil(QueryVencto);
                  FreeAndNil(ListaBases);
                  FreeAndNil(STRLRetorno);
                  Backup.Free;
                  FreeAndNil(PMensagemErro);
                  Mensagemerro(e.Message);
                  exit;
            end;
      end;

      //http://www.exclaim.com.br/intranet/lista_senha_cliente.php?id_cliente_base=:PCODIGOBASE&cnpj=:PCNPJ para clientes do bitis c/ paf
      //era: http://www.exclaim.com.br/intranet/lista_senha_cliente.php?id_cliente_base=:PCODIGOBASE

      try
            try
                  //USER PROTECAO SENHA PROTECAO
                  FdataModulo.IBDatabaseVencimento.Close;
                  FdataModulo.IBDatabaseVencimento.Params.Clear;
                  FdataModulo.IBDatabaseVencimento.Params.Add('User_name='+DesincriptaSenha('��������'));
                  FdataModulo.IBDatabaseVencimento.Params.Add('password='+DesincriptaSenha('��������'));
            except
                  on e:exception do
                  begin
                       Mensagemerro(e.message);
                       exit;
                  end;
            end;

            PMensagemErro.Clear;

            {Listando TODOS os bancos cadastrados, selecionei todos e ent�o depois
            verifico se est� vencido ou n�o.
            }
            with QueryVencto do
            begin

                  close;
                  sql.Clear;
                  sql.add('SELECT CODIGO,ORIGEM,CLIENTE_SITE,DATAVENCIMENTO,VERIFICADOR FROM TABBACKUP ');
                  sql.add('ORDER BY CODIGO');
                  //sql.add('WHERE CLIENTE_SITE IS NOT NULL ORDER BY CODIGO');
                  try
                        open;
                  except
                        on e:exception do
                        begin
                              Mensagemerro(e.Message);
                              exit;
                        end;

                  end;
                  Last;
                  if(Recordcount=0)
                  then begin
                        Mensagemerro('N�o existem bases cadastradas');
                        Exit;
                  end;
                  First;

                  ListaBases.Clear;

                  while not(eof) do
                  begin
                        Verifica := true;
                        {22/11/2010 - Vou verificar o path cadastrado na tabbackup, pois ao conectar, se estiver errado
                         trava tudooooo
                        }
                        if(FileExists(FieldbyName('ORIGEM').AsString))
                        then begin

                              {verificando se a senha est� vencida, tomando como base a datavencimento presente na tabbackup
                              vou me conectar no banco, e atraves do firebird retorno a data do servidor.
                              }
                              try
                                    FdataModulo.IBDatabaseVencimento.Close;
                                    FdataModulo.IBDatabaseVencimento.DatabaseName := FieldbyName('ORIGEM').AsString;
                                    FdataModulo.IBDatabaseVencimento.Open;
                              except
                                    on e:exception do
                                    begin
                                          PMensagemErro.Add(FormatDateTime('YYYY_MM_DD_HH_MM_SS',now)+' '+'Erro ao conectar ao banco: '+FieldbyName('ORIGEM').AsString+' Erro: '+e.Message);
                                          Verifica := false;
                                    end;
                              end;

                              try
                                    QueryServidor.Close;
                                    QueryServidor.SQL.Clear;
                                    QueryServidor.SQL.Add('SELECT CAST('+#39+'NOW'+#39+' AS DATE) AS HOJE FROM RDB$DATABASE');

                                    //InputBox('SQL verifica data servidor','',QueryServidor.SQL.Text);

                                    QueryServidor.Open;
                              except
                                    on e:exception do
                                    begin
                                          PMensagemErro.Add(FormatDateTime('YYYY_MM_DD_HH_MM_SS',now)+' '+'Erro ao resgatar data atual do servidor com o banco: '+FieldbyName('ORIGEM').AsString+' Erro: '+e.Message);
                                          Verifica := false;
                                    end;
                              end;

                              try
                                    if(Verifica)//n�o deu problema nos processos anteriores, agora verifico se t� vencido
                                    then if(QueryVencto.FieldbyName('DATAVENCIMENTO').AsString<>'')
                                          then if(strtodate(QueryServidor.FieldByName('HOJE').asstring) <= IncDay(StrToDate(QueryVencto.FieldbyName('DATAVENCIMENTO').AsString),-3))
                                              then Verifica:=false;  //se nao estiver, n�o precisa conectar no site
                              except
                                    on e:exception do
                                    begin
                                          PMensagemErro.Add(FormatDateTime('YYYY_MM_DD_HH_MM_SS',now)+' '+'Erro: '+e.Message);
                                          Verifica := false;
                                    end;
                              end;

                              CNPJ := '';
                              if(Trim( QueryVencto.FieldbyName('CLIENTE_SITE').AsString ) = '0')then
                              begin
                                try
                                      QueryServidor.Close;
                                      QueryServidor.SQL.Clear;
                                      QueryServidor.SQL.Add('SELECT E.CNPJ FROM TABEMPRESA E WHERE E.CODIGO=1');
                                      QueryServidor.Open;

                                      CNPJ := QueryServidor.FieldByName('CNPJ').AsString;

                                      {BASE PAF  : CNPJ + @NAO/SIM = 22digitos criptografado
                                       Amanda/SAB: CNPJ = 18 digitos c/ maskara ou 14 sem maskara

                                      }

                                      if(Length( CNPJ ) = 22) then //PAF
                                      begin
                                        CNPJ := Copy( CNPJ, 1,18);
                                        CNPJ := useg.DesincriptaSenha(CNPJ);
                                      end;

                                      CNPJ := RetornaSoNUmeros(CNPJ);

                                      if( Trim(CNPJ) = '') then
                                      begin
                                        PMensagemErro.Add('CNPJ NAO ENCONTRADO NO BANCO DE DADOS: ' + QueryVencto.FieldbyName('ORIGEM').AsString);
                                        Verifica := false;
                                      end;

                                      CNPJ := CompletaPalavra_a_Esquerda( CNPJ, 14, '0' );
                                      CNPJ := Copy(CNPJ,1,2)+'.'+Copy(CNPJ,3,3)+'.'+Copy(CNPJ,6,3)+'/'+ Copy(CNPJ,9,4)+'-'+Copy(CNPJ,13,2);
                                except
                                      on e:exception do
                                      begin
                                            PMensagemErro.Add('ERRO AO LOCALIZAR CNPJ DA EMPRESA NO BANCO DE DADOS: ' + QueryVencto.FieldbyName('ORIGEM').AsString);
                                            Verifica := false;
                                      end;
                                end;
                              end;
                              if(Verifica)
                              then begin

                                    //para cada uma base encontrada, altero os parametros da URL
                                    URLLocal := FprincipalExclaim.URL;
                                    URLLocal := StringReplace(URLLocal,':PCODIGOBASE',FieldByName('CLIENTE_SITE').asstring,[rfReplaceAll]);
                                    URLLocal := StringReplace(URLLocal,':PCNPJ',CNPJ,[rfReplaceAll]);

                                    STRLRetorno.Clear;
                                    //Acesso a url e resgato o vencimento
                                    try
                                          if not(HttpGetText(URLLocal,STRLRetorno))
                                          then PMensagemErro.Add(FormatDateTime('YYYY_MM_DD_HH_MM_SS',now)+' '+'N�o foi poss�vel realizar o download do novo vencimento para a base: '+FieldByName('CODIGO').asstring+' ID: '+FieldByName('CLIENTE_SITE').asstring);
                                          //Application.ProcessMessages;
                                    except
                                          on e:Exception do
                                          begin
                                                PMensagemErro.Add(FormatDateTime('YYYY_MM_DD_HH_MM_SS',now)+' '+'N�o foi poss�vel realizar o download do novo vencimento para a base '+FieldByName('CODIGO').asstring+' ID: '+FieldByName('CLIENTE_SITE').asstring+'; '+e.Message);
                                          end;
                                    end;

                                    //InputBox('Retorno HTTP','',STRLRetorno.Text);

                                    if(STRLRetorno.Count=0)
                                    then PMensagemErro.Add(FormatDateTime('YYYY_MM_DD_HH_MM_SS',now)+' '+'N�o foi poss�vel resgatar novo vencimento para a base:'+FieldByName('CODIGO').asstring+' ID: '+FieldByName('CLIENTE_SITE').asstring+'; Sem retorno do site')
                                    else begin
                                          {Se tudo estiver OK, na stringlist teremos a 1� linha com os valores que nos interessa
                                           Data atual Vencimento Verificador
                                           25/11/2010;24/12/2010;KGFL132JLGS
                                          }
                                          StringRetorno := STRLRetorno[0];

                                          if(ExplodeStr(StringRetorno,STRLRetorno,';','String'))
                                          then begin
                                                if(STRLRetorno.Count=3)
                                                then begin
                                                      //Concateno o retorno do site na stringlist neste leiaute: CODIGO;DATA_ATUAL;DATA_VENCIMENTO;VERIFICADOR
                                                      ListaBases.Add(FieldByName('CODIGO').asstring+';'+StringRetorno);
                                                end
                                                else PMensagemErro.Add(FormatDateTime('YYYY_MM_DD_HH_MM_SS',now)+' '+'Retorno para a base: '+FieldByName('CODIGO').asstring+' ID: '+FieldByName('CLIENTE_SITE').asstring+' menor que 3 colunas');
                                          end
                                          else PMensagemErro.Add(FormatDateTime('YYYY_MM_DD_HH_MM_SS',now)+' '+'Erro ao separar vari�veis do retorno para a base: '+FieldByName('CODIGO').asstring+' ID: '+FieldByName('CLIENTE_SITE').asstring+' Retorno: '+StringRetorno);
                                    end;
                              end;
                        end
                        else PMensagemErro.Add(FormatDateTime('YYYY_MM_DD_HH_MM_SS',now)+' '+'Diret�rio/Arquivo inv�lido para a base: '+FieldByName('CODIGO').asstring+' ID: '+FieldByName('CLIENTE_SITE').asstring);
                        Next;
                  end;

                  {Agora tenho uma Stringlist contendo: CODIGO;DATA_ATUAL;DATA_VENCIMENTO;VERIFICADOR
                   Ent�o agora s� preciso salvar no banco de dados os valores
                  }

                  for cont:=0 to ListaBases.Count-1 do
                  begin
                        try
                              STRLRetorno.Clear;
                              ExplodeStr(ListaBases[cont],STRLRetorno,';','string');

                              Backup.ZerarTabela;
                              if(Backup.LocalizaCodigo(STRLRetorno[0]))
                              then begin
                                    Backup.TabelaparaObjeto;
                                    Backup.Commit;
                                    Backup.Status:=dsEdit;
                                    Backup.Submit_DataAtual(STRLRetorno[1]);
                                    Backup.Submit_DataVencimento(STRLRetorno[2]);
                                    Backup.Submit_Verificador(STRLRetorno[3]);
                                    if not(Backup.Salvar(true))
                                    then PMensagemErro.Add(FormatDateTime('YYYY_MM_DD_HH_MM_SS',now)+' '+'Erro ao salvar novo vencimento na base: '+Backup.Get_CODIGO);
                              end;
                        except
                              on e:exception do
                                    PMensagemErro.Add(FormatDateTime('YYYY_MM_DD_HH_MM_SS',now)+' '+'Erro no registro: '+ListaBases[cont]+' Erro: '+e.Message);
                        end;

                  end;

            end;
            FprincipalExclaim.Memo1.Text :=FprincipalExclaim.Memo1.Text + PMensagemErro.Text;
            TEMP :='';
            TEMP:=PMensagemErro.Text;
            PMensagemErro.Clear;
            if(FileExists(FprincipalExclaim.PegaPathSistema+'LOG_LARA_'+FormatDateTime('YYYY_MM_DD',Now)+'.TXT'))
            then PMensagemErro.LoadFromFile(FprincipalExclaim.PegaPathSistema+'LOG_LARA_'+FormatDateTime('YYYY_MM_DD',Now)+'.TXT');
            PMensagemErro.Add(temp);
            PMensagemErro.SaveToFile(FprincipalExclaim.PegaPathSistema+'LOG_LARA_'+FormatDateTime('YYYY_MM_DD',Now)+'.TXT');

            //result := true;
      finally

            FreeAndNil(QueryVencto);
            FreeAndNil(ListaBases);
            FreeAndNil(STRLRetorno);
            FreeAndNil(PMensagemErro);
            Backup.Free;
            FreeAndNil(QueryServidor);
            FprincipalExclaim.TimerAtualizaVencimento.Enabled := true;
            FdataModulo.IBDatabaseVencimento.Close;
      end;

end;

Function TFprincipalExclaim.PegaPathSistema:String;
var
Path:String;
Begin
     path:=ExtractFilePath(Application.ExeName);
     if (path[length(path)]<>'\')
     Then path:=path+'\';
     result:=path;
End;


procedure TFprincipalExclaim.AtivaVerificacao;
var
  tempo:string;
begin

     if(ObjparametroGlobal.ValidaParametro('INTERVALO DE TEMPO VERIFICAR VENCIMENTO EM MILISEGUNDOS'))
     then tempo := ObjparametroGlobal.Get_Valor;
     try
          if(tempo<>'')
          then begin
                StrToInt(tempo);
                TimerAtualizaVencimento.Interval:=strtoint(tempo);
                TimerAtualizaVencimento.Enabled := true;
          end;
     except
          Mensagemerro('Valor inv�lido no par�metro: "INTERVALO DE TEMPO VERIFICAR VENCIMENTO EM MILISEGUNDOS"');
          exit;
     end;

end;

procedure TFprincipalExclaim.DesativaVerificacao;
begin
      TimerAtualizaVencimento.Enabled := false;
end;

procedure TFprincipalExclaim.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      If (ssCtrl in Shift)
      then PanelExclaim.Visible := true
      else PanelExclaim.Visible := false;
end;

procedure TFprincipalExclaim.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      If (key=vk_control)
      then PanelExclaim.Visible := false;
end;

{******************************************************************************}
{http://www.agnaldocarmo.com.br/home/comando-milagroso-para-reducao-de-memoria-delphi/}
{******************************************************************************}
procedure TFprincipalExclaim.TrimAppMemorySize;
var
MainHandle : THandle;
begin
  try
    MainHandle := OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessID) ;
    SetProcessWorkingSetSize(MainHandle, $FFFFFFFF, $FFFFFFFF) ;
    CloseHandle(MainHandle) ;
  except
  end;
  Application.ProcessMessages;
end;


procedure TFprincipalExclaim.TimerReduzMemTimer(Sender: TObject);
begin
  Self.TrimAppMemorySize;
end;

end.
