unit UmostraBackup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TFmostraBackup = class(TForm)
    MemoBackup: TMemo;
    Panel1: TPanel;
    Button1: TButton;
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmostraBackup: TFmostraBackup;

implementation

{$R *.dfm}

procedure TFmostraBackup.FormShow(Sender: TObject);
begin
     Self.tag:=0;
end;

procedure TFmostraBackup.Button1Click(Sender: TObject);
begin
     Self.tag:=1;
     Self.close;
end;

end.
