unit UobjHORARIOSBACKUP;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,IBStoredProc,ibdatabase
,UOBJBACKUP,uutils
;
//USES_INTERFACE



Type
   TObjHORARIOSBACKUP=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Objquery2,Objquery3:tibquery;
                Backup:TOBJBACKUP;
                ObjBackups:array[0..100] of TobjBackup;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create(caminhobanco:string);overload;
                //Constructor Create(Pdatabase:TibDatabase;PTransaction:TibTransaction);overload;

                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:Str09) :boolean;
                Function    LocalizaBackup(Parametro:Str09) :boolean;
                Function    Exclui(Pcodigo:str09;ComCommit:boolean)            :Boolean;
                function    Exclui_backup(Pcodigo: str09; ComCommit: boolean): Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :Str100;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:Str09;
                Function  RetornaCampoCodigo:Str100;
                Function  RetornaCampoNome:Str100;

                Procedure Submit_CODIGO(parametro: STR09);
                Function Get_CODIGO: STR09;
                Procedure Submit_Domingo(parametro: string);
                Function Get_Domingo: string;
                Procedure Submit_segunda(parametro: string);
                Function Get_segunda: string;
                Procedure Submit_terca(parametro: string);
                Function Get_terca: string;
                Procedure Submit_quarta(parametro: string);
                Function Get_quarta: string;
                Procedure Submit_quinta(parametro: string);
                Function Get_quinta: string;
                Procedure Submit_sexta(parametro: string);
                Function Get_sexta: string;
                Procedure Submit_sabado(parametro: string);
                Function Get_sabado: string;
                procedure EdtbackupExit(Sender: TObject;LABELNOME:TLABEL);
                constructor Cria;
                procedure DestroiBackups;
                Procedure ResgataBackups;
                Procedure IniciaBackups;

         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               IBDatabase: TIBDatabase;
               IBTransaction: TIBTransaction;

               Quantidadebackups:Integer;

               CODIGO:STR09;
               Domingo:string;
               segunda:string;
               terca:string;
               quarta:string;
               quinta:string;
               sexta:string;
               sabado:string;
//CODIFICA VARIAVEIS PRIVADAS
               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                procedure ZeraBackups;


   End;


implementation
uses SysUtils,Dialogs,Controls;





Function  TObjHORARIOSBACKUP.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('backup').asstring<>'')
        Then Begin
                 If (Self.backup.LocalizaCodigo(FieldByName('backup').asstring)=False)
                 Then Begin
                          Messagedlg('Backup N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else
                 begin
                  Self.backup.TabelaparaObjeto;

                 end
        End;
        Self.Domingo:=fieldbyname('Domingo').asstring;
        Self.segunda:=fieldbyname('segunda').asstring;
        Self.terca:=fieldbyname('terca').asstring;
        Self.quarta:=fieldbyname('quarta').asstring;
        Self.quinta:=fieldbyname('quinta').asstring;
        Self.sexta:=fieldbyname('sexta').asstring;
        Self.sabado:=fieldbyname('sabado').asstring;
//CODIFICA TABELAPARAOBJETO



        result:=True;
        self.Backup.Commit;
     End;
end;


Procedure TObjHORARIOSBACKUP.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('backup').asstring:=Self.backup.GET_CODIGO;
        ParamByName('Domingo').asstring:=Self.Domingo;
        ParamByName('segunda').asstring:=Self.segunda;
        ParamByName('terca').asstring:=Self.terca;
        ParamByName('quarta').asstring:=Self.quarta;
        ParamByName('quinta').asstring:=Self.quinta;
        ParamByName('sexta').asstring:=Self.sexta;
        ParamByName('sabado').asstring:=Self.sabado;
//CODIFICA OBJETOPARATABELA









  End;
End;

//***********************************************************************

function TObjHORARIOSBACKUP.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True Then
  //Self.IBTransaction.CommitRetaining;
  Self.IBTransaction.Commit;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjHORARIOSBACKUP.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        backup.ZerarTabela;
        Domingo:='';
        segunda:='';
        terca:='';
        quarta:='';
        quinta:='';
        sexta:='';
        sabado:='';
//CODIFICA ZERARTABELA









     End;
end;

Function TObjHORARIOSBACKUP.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjHORARIOSBACKUP.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.backup.LocalizaCodigo(Self.backup.Get_CODIGO)=False) Then
        Mensagem := mensagem+'/ Backup n�o Encontrado!'
      else
        Self.Backup.Commit;



     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjHORARIOSBACKUP.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.backup.Get_Codigo<>'')
        Then Strtoint(Self.backup.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Backup';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjHORARIOSBACKUP.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjHORARIOSBACKUP.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjHORARIOSBACKUP.LocalizaCodigo(parametro: Str09): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro HORARIOSBACKUP vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,backup,Domingo,segunda,terca,quarta,quinta,sexta');
           SQL.ADD(' ,sabado');
           SQL.ADD(' from  TabHORARIOSBackup');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else
           begin
            Result:=False;
            Objquery.Transaction.Commit;
           end
       End;
end;

function TObjHORARIOSBACKUP.LocalizaBackup(parametro: Str09): boolean;//ok
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro HORARIOSBACKUP vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,backup,Domingo,segunda,terca,quarta,quinta,sexta');
           SQL.ADD(' ,sabado');
           SQL.ADD(' from  TabHORARIOSBackup');
           SQL.ADD(' WHERE backup='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else
           begin
            Result:=False;
            Objquery.Transaction.Commit;
           end
       End;
end;

procedure TObjHORARIOSBACKUP.Cancelar;
begin
     Self.status:=dsInactive;
end;

Function    TObjHORARIOSBACKUP.Exclui_backup(Pcodigo:str09;ComCommit:boolean)            :Boolean;
Begin
     result:=False;
     
     With Self.Objquery do
     begin
          close;
          sql.clear;
          sql.add('Delete from Tabhorariosbackup where backup='+Pcodigo);
          Try
              execsql;

              result:=Self.backup.Exclui(pcodigo,true);
          Except
                on e:exception do
                begin
                     Messagedlg(e.message,mterror,[mbok],0);
                     exit;
                End;
          End;
     End;
End;

function TObjHORARIOSBACKUP.Exclui(Pcodigo: str09;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True) Then
                  //Self.IBTransaction.CommitRetaining;
                  Self.IBTransaction.Commit;
        End

        Else result:=false;
     Except
           result:=false;
     End;
end;



(*Constructor TObjHorariosBACKUP.Create(Pdatabase:TibDatabase;PTransaction:TibTransaction);
Begin
     Self.IBDatabase:=Pdatabase;
     Self.IBTransaction:=PTransaction;
     Self.Cria;
End;*)

constructor TObjhorariosBACKUP.create(caminhobanco:string);
begin
        Self.IBTransaction:=TIBTransaction.create(nil);
        Self.IBTransaction.DefaultAction:=TARollback;
        Self.IBTransaction.Params.add('read_committed');
        Self.IBTransaction.Params.add('rec_version');
        Self.IBTransaction.Params.add('nowait');
        Self.IBDatabase:=TIBDatabase.create(nil);
        Self.IBDatabase.SQLDialect:=3;
        Self.IBDatabase.LoginPrompt:=False;
        Self.IBDatabase.DefaultTransaction:=Self.IBTransaction;
        Self.IBDatabase.Databasename:=CaminhoBanco;
        //USUARIO PROTECAO
        //SENHA   PROTECAO
        Self.IbDatabase.Params.clear;
        Self.IbDatabase.Params.Add('User_name='+DesincriptaSenha('��������'));
        Self.IbDatabase.Params.Add('password='+DesincriptaSenha('��������'));
        Self.IbDatabase.Open;
        //************************************************************************
        Self.Cria;
end;



constructor TObjHORARIOSBACKUP.Cria;
begin
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=Self.IbDatabase;

        Self.Objquerypesquisa:=TIBQuery.create(nil);
        Self.Objquerypesquisa.Database:=Self.IbDatabase;

        Self.Objquery2:=tibquery.create(nil);
        Self.Objquery2.Database:=Self.IBDatabase;

        Self.Objquery3:=tibquery.create(nil);
        Self.Objquery3.Database:=Self.IBDatabase;


        
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;

        Self.backup:=TOBJBACKUP.create(self.IBDatabase,Self.IBTransaction);
        Self.ZeraBackups;
        Self.Quantidadebackups:=0;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabHORARIOSBackup(CODIGO,backup,Domingo');
                InsertSQL.add(' ,segunda,terca,quarta,quinta,sexta,sabado)');
                InsertSQL.add('values (:CODIGO,:backup,:Domingo,:segunda,:terca,:quarta');
                InsertSQL.add(' ,:quinta,:sexta,:sabado)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabHORARIOSBackup set CODIGO=:CODIGO,backup=:backup');
                ModifySQL.add(',Domingo=:Domingo,segunda=:segunda,terca=:terca,quarta=:quarta');
                ModifySQL.add(',quinta=:quinta,sexta=:sexta,sabado=:sabado');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabHORARIOSBackup where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjHORARIOSBACKUP.Commit;
begin
     Self.IBTransaction.Commit;
end;

function TObjHORARIOSBACKUP.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabHORARIOSBACKUP');
     Result:=Self.ParametroPesquisa;
end;

function TObjHORARIOSBACKUP.Get_TituloPesquisa: Str100;
begin
     Result:=' Pesquisa de HORARIOSBACKUP ';
end;


function TObjHORARIOSBACKUP.Get_NovoCodigo: Str09;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=Self.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENHORARIOSBACKUP,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENHORARIOSBACKUP,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
        IbQueryGen.Transaction.Commit;
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjHORARIOSBACKUP.Free;
begin
    Self.DestroiBackups;
    Freeandnil(Self.Objquery);
    Freeandnil(Self.objquery2);
    Freeandnil(Self.objquery3);

    Freeandnil(Self.Objquerypesquisa);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.backup.FREE;


    //10/11/2010 Celio - Adicionado pois n�o estavam sendo liberados
    FreeAndNil(IBTransaction);
    FreeAndNil(IBDatabase);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjHORARIOSBACKUP.RetornaCampoCodigo: Str100;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjHORARIOSBACKUP.RetornaCampoNome: Str100;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjHORARIOSBackup.Submit_CODIGO(parametro: STR09);
begin
        Self.CODIGO:=Parametro;
end;
function TObjHORARIOSBackup.Get_CODIGO: STR09;
begin
        Result:=Self.CODIGO;
end;
procedure TObjHORARIOSBackup.Submit_Domingo(parametro: string);
begin
        Self.Domingo:=Parametro;
end;
function TObjHORARIOSBackup.Get_Domingo: string;
begin
        Result:=Self.Domingo;
end;
procedure TObjHORARIOSBackup.Submit_segunda(parametro: string);
begin
        Self.segunda:=Parametro;
end;
function TObjHORARIOSBackup.Get_segunda: string;
begin
        Result:=Self.segunda;
end;
procedure TObjHORARIOSBackup.Submit_terca(parametro: string);
begin
        Self.terca:=Parametro;
end;
function TObjHORARIOSBackup.Get_terca: string;
begin
        Result:=Self.terca;
end;
procedure TObjHORARIOSBackup.Submit_quarta(parametro: string);
begin
        Self.quarta:=Parametro;
end;
function TObjHORARIOSBackup.Get_quarta: string;
begin
        Result:=Self.quarta;
end;
procedure TObjHORARIOSBackup.Submit_quinta(parametro: string);
begin
        Self.quinta:=Parametro;
end;
function TObjHORARIOSBackup.Get_quinta: string;
begin
        Result:=Self.quinta;
end;
procedure TObjHORARIOSBackup.Submit_sexta(parametro: string);
begin
        Self.sexta:=Parametro;
end;
function TObjHORARIOSBackup.Get_sexta: string;
begin
        Result:=Self.sexta;
end;
procedure TObjHORARIOSBackup.Submit_sabado(parametro: string);
begin
        Self.sabado:=Parametro;
end;
function TObjHORARIOSBackup.Get_sabado: string;
begin
        Result:=Self.sabado;
end;
//CODIFICA GETSESUBMITS


procedure TObjHORARIOSBACKUP.EdtbackupExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.backup.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.backup.tabelaparaobjeto;
     self.Backup.Commit;
     LABELNOME.CAPTION:=Self.backup.GET_NOME;
End;

procedure Tobjhorariosbackup.ZeraBackups;
var
cont:integer;
begin
     for cont:=0 to 100 do
     Begin
          Self.ObjBackups[cont]:=nil;
     End;
end;


procedure Tobjhorariosbackup.DestroiBackups;
var
cont:integer;
begin
     for cont:=0 to Self.Quantidadebackups-1 do
     Begin
          Self.ObjBackups[cont].TimerBackup.Enabled:=False;
          Self.ObjBackups[cont].free;
          Self.ObjBackups[cont]:=nil;
     End;
     Self.Quantidadebackups:=0;
end;


procedure TObjHORARIOSBACKUP.ResgataBackups;
var
Pdia:integer;
horarios:string;
begin
     Self.DestroiBackups;
     Self.ZeraBackups;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select codigo from tabbackup order by codigo');
          open;

          try

            While not(eof) do
            Begin
                 pdia:=DayOfWeek(now);

                 Self.ObjqueryPesquisa.close;
                 Self.ObjqueryPesquisa.sql.clear;
                 Self.ObjqueryPesquisa.sql.add('Select * from tabhorariosbackup where backup='+Self.Objquery.fieldbyname('codigo').asstring);
                 Self.ObjqueryPesquisa.open;
                 Self.ObjqueryPesquisa.first;

                 horarios:='';
                 case pdia of
                   1:horarios:=Self.objquerypesquisa.fieldbyname('domingo').asstring;
                   2:horarios:=Self.objquerypesquisa.fieldbyname('segunda').asstring;
                   3:horarios:=Self.objquerypesquisa.fieldbyname('terca').asstring;
                   4:horarios:=Self.objquerypesquisa.fieldbyname('quarta').asstring;
                   5:horarios:=Self.objquerypesquisa.fieldbyname('quinta').asstring;
                   6:horarios:=Self.objquerypesquisa.fieldbyname('sexta').asstring;
                   7:horarios:=Self.objquerypesquisa.fieldbyname('sabado').asstring;
                 End;

                 if (horarios<>'')
                 Then Begin
                           if (Self.Quantidadebackups<=100)
                           Then BEgin
                                   Self.ObjBackups[Self.Quantidadebackups]:=TObjBACKUP.create(Self.IBDatabase,Self.IBTransaction);
                                   Self.ObjBackups[Self.Quantidadebackups].Indice:=inttostr(Self.Quantidadebackups+1);
                                   Self.ObjBackups[Self.Quantidadebackups].HorariosBackup:=horarios;
                                   Self.ObjBackups[Self.Quantidadebackups].LocalizaCodigo(Self.Objquery.fieldbyname('codigo').asstring);
                                   Self.ObjBackups[Self.Quantidadebackups].TabelaparaObjeto;
                                   Self.ObjBackups[Self.Quantidadebackups].Lbbackup:=nil;
                                   Self.Quantidadebackups:=Self.Quantidadebackups+1;
                           End;
                 End;
               
                 next;
            End;
            
          finally
            Objquery.Transaction.Commit;
          end;
          close;
     End;

     Self.IniciaBackups;
end;



procedure TObjHORARIOSBACKUP.IniciaBackups;
var
cont:integer;
begin
     for cont:=0 to Self.Quantidadebackups-1 do
     Begin
          Self.ObjBackups[cont].TimerBackup.Enabled:=true;
     End;

end;


end.



