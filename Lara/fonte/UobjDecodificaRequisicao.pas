unit UobjDecodificaRequisicao;
Interface
Uses ibdatabase,forms,Ibquery,windows,stdctrls,Classes,Db,IBStoredProc,
  UobjREQUISICAOSENHA;

Type
   Tobjdecodificarequisicao=class(TObjREQUISICAOSENHA)

          Public

                ProblemaPlugue,ModoDemo:boolean;
                PmensagemErroPlugue:string;

                Function VerificaRequisicao:boolean;
                Constructor Create(PCaminhobanco:String;PVerificaPlugue:boolean);
                Function VerificaPlugue:Boolean;

                Function GeraResultado:boolean;

                destructor free;

         Private
                DataBaseLocal:tibdatabase;
                Transactionlocal:Tibtransaction;
                ObjqueryLocal:Tibquery;
                
   End;


implementation




uses SysUtils,Dialogs,Controls,Math, U_Cipher, UmostraMEnsagem
//, Proteq //-- celio descomentar para rodar
,UUtils;


constructor Tobjdecodificarequisicao.Create(PCaminhobanco:String;PVerificaPlugue:boolean);
begin
     Self.ProblemaPlugue:=False;
     Self.ModoDemo:=False;
     Self.PmensagemErroPlugue:='';

     inherited create(PCaminhobanco);//chama o create do PAI

     Self.DatabaseLocal:=TIBDatabase.Create(nil);
     Self.Transactionlocal:=TIBTransaction.Create(nil);
     Self.ObjqueryLocal:=Tibquery.create(nil);
     Self.ObjqueryLocal.Database:=Self.DataBaseLocal;
     Self.ObjqueryLocal.Transaction:=Self.TransactionLocal;

     //na hora que entra no programa ele verifica o plugue
     if (PVerificaPlugue)
     Then Self.VerificaPlugue;
end;


destructor Tobjdecodificarequisicao.free;
begin
    inherited free;
    Freeandnil(Self.DatabaseLocal);
    Freeandnil(Self.Transactionlocal);
    Freeandnil(Self.ObjqueryLocal);
    //Freeandnil(Self.QuerySenha); //do pai
end;

function Tobjdecodificarequisicao.GeraResultado:boolean;
begin
     result:=False;

     With Self.QuerySenha do
     Begin
         close;
         sql.clear;
         sql.add('select * from TABBACKUP where');
         sql.add('TABBACKUP.CLIENTE_SITE='+Self.Get_CodigoBaseSite);
         open;

         try

           if (fieldbyname('codigo').asstring='')
           Then exit;

           Self.ObjqueryLocal.Close;
           Self.DatabaseLocal.close;
           Self.DatabaseLocal.SQLDialect:=3;
           Self.DatabaseLocal.DatabaseName:=fieldbyname('origem').asstring;
           Self.DatabaseLocal.Params.Clear;
           //PROTECAO PROTECAO
           Self.DatabaseLocal.Params.Add('user_name='+DesincriptaSenha('��������'));
           Self.DatabaseLocal.Params.Add('password='+DesincriptaSenha('��������'));
           Self.DatabaseLocal.LoginPrompt:=False;

           Self.TransactionLocal.DefaultAction:=TARollback;
           Self.TransactionLocal.Params.clear;
           self.TransactionLocal.Params.Add('read_committed');
           self.TransactionLocal.Params.Add('rec_version');
           self.TransactionLocal.Params.Add('nowait');
           self.TransactionLocal.DefaultDatabase:=Self.DatabaseLocal;

           Self.DatabaseLocal.DefaultTransaction:=Self.TransactionLocal;

           Try
               Self.DatabaseLocal.Open;
               Self.ObjqueryLocal.sql.clear;
               Self.ObjqueryLocal.sql.add('insert into tabrequisicaosenha (codigo,resposta,mensagemresposta) values (:codigo,:resposta,:mensagemresposta)');
               Self.ObjqueryLocal.ParamByName('codigo').AsString:=Self.Get_CODIGO;
               Self.ObjqueryLocal.ParamByName('resposta').AsString:=Self.Get_Resposta;
               Self.ObjqueryLocal.ParamByName('mensagemresposta').AsString:=Self.Get_MensagemResposta;
               Self.ObjqueryLocal.ExecSQL;
               //Self.Transactionlocal.CommitRetaining;
               Self.Transactionlocal.Commit;
           Except
                 on e:exception do
                 Begin
                     Messagedlg(e.message,mterror,[mbok],0);
                     exit;
                 end;
           End;

         finally
           QuerySenha.Transaction.Commit;
         end;
     End;

     result:=True;
end;

function Tobjdecodificarequisicao.VerificaPlugue: Boolean;
begin

     Self.ProblemaPlugue:=True;
     Self.PmensagemErroPlugue:='';
     Self.PmensagemExterna:='';

     //Procura pela palavra EXCLAIM_TECNOLOGIA


     //Showmessage('INTERNAL ERROR PLG 666. ENCERRE O SISTEMA AGORA PARA GARANTIR A INTEGRIDADE DOS DADOS');
     {celio descomentar para rodar
     if(FProteq.validaplugue(Self.pmensagemexterna,DesincriptaSenha('������������������'),Self.mododemo)=False)
     Then Self.PmensagemErroPlugue:='Erro na Valida��o do Plugue. '+Self.PmensagemExterna
     Else Self.ProblemaPlugue:=False;
     }
     Self.PmensagemExterna:=Self.PmensagemErroPlugue;
     result := false
end;

function Tobjdecodificarequisicao.VerificaRequisicao: boolean;
var
prequisicao:string;
presposta:String;
Begin
     result:=True;

     if (Self.LocalizaNovaRequisicao=False)
     Then exit;

     result:=False;

     Self.TabelaparaObjeto;
     self.Commit;


     //tenho uma nova requisicao nao atendida

     //************decodifico a requisicao******************
     if (Self.DeCodificoRequisicao=False)
     Then exit;
     //*****************************************************

     if (Self.ProblemaPlugue)
     Then Self.PmensagemExterna:=Self.PmensagemErroPlugue
     Else result:=true;
     
     //ja passou pelo plugue

     if (result=True)
     then Begin
             //Agora Gero a Contra-Senha e envio para o registro
             Self.Submit_REQUISICAO(DesincriptaSenha(Self.Get_REQUISICAO));

             if (Self.GeraContraSenha=False)
             Then Presposta:='0';
             
             Self.Submit_REQUISICAO(EncriptaSenha(Self.Get_REQUISICAO));
             Self.Submit_MensagemResposta(Uutils.EncriptaSenha(formatdatetime('mmddyyyyhhmm',now)));//apenas para confundir

             if (Self.ModoDemo=true)
             Then Self.Submit_mensagemresposta('����༻��');//MODO DEMO 
     End
     Else Begin
              Self.Submit_resposta('0');
              Self.Submit_MensagemResposta(Self.PmensagemExterna);
     End;

     Self.status:=dsedit;

     (*
     Preciso agora pegar o ID da Base do Site
     Localizar isso na tabela de backup
     Conectar nesta base
     e gerar o resultado na tabela tabrequisicaosenha da base que requisitou

     Isso evitar� o cadastramento de multiplas bases pelo cliente
     Porque a resposta sera dada na base que solicitou

     entao se o cliente copiar a base  ele ira pedir requisicao por exemplo
     pra base de id 30, o lara conectara nesta base e mandara a resposta

     porem o sistema estava com outra base copiada que nao estara registada no
     lara, sendo assim nao conseguira entrar

     Fluxo


     EXE   grava BDLARA


     Lara  lE BDLARA e pega o ID da Base que solicitou

     LARA encontra a base na tabbackup e resgata o local da Base

     Lara se conecta nesta BASE

     Lara devolve resposta no BDLARA e Nesta BASE 
     
     *)

     if (Self.salvar(True)=False)
     then Begin
               Self.PmensagemExterna:='Erro na tentativa de gravar a resposta no registro local';
               exit;
     End;

     if (Self.GeraResultado=False)
     Then Begin
               Self.PmensagemExterna:='Erro Valida��o registro BD cliente';
               exit;
     End;

     if (result=true)
     then Self.PmensagemExterna:='Valida��o Gerada '+Self.get_RESPOSTA;
end;

end.



