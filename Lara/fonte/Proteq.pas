unit Proteq;

interface

uses
  Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls, Menus, Buttons, Mask;

type
  TFproteq = class(TForm)
    Label1: TLabel;
    codigo: TEdit;
    Button1: TButton;
    Edit1: TEdit;
    Button2: TButton;
    Button3: TButton;
    Edit2: TEdit;
    Edit3: TEdit;
    Label2: TLabel;
    Edit4: TEdit;
    Edit5: TEdit;
    Label3: TLabel;
    Edit6: TEdit;
    Label4: TLabel;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Memo1: TMemo;
    Button8: TButton;
    Button9: TButton;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);

  private
    function DesincriptaSenha(original: string): string;
    function TrocaLetra(Original: string): string;
    { Private declarations }
  public
    { Public declarations }
    Function ValidaPlugue(var Pmensagemextra:string;PfraseMemoria:string;var PmodoDemo:boolean):boolean;
  end;

var
  Fproteq: TFproteq;

implementation

function C500(Comando : pchar):Integer stdcall; external 'c50032.dll' name 'C500';

{$R *.DFM}

procedure TFproteq.Button1Click(Sender: TObject);

var

   senha : string;
original : string;
       i : integer;
       b : array[0..255] of char;
       c : pchar;
       s : string;
begin

c := @b;

original := 'Demonstra��o das fun��es de Criptografar e Decriptografar';
Edit4.Text := original;

 senha := chr(3) + codigo.Text;

if Length(senha) = 10 
then begin
        StrPcopy(c, senha);
        i := c500(c);

        if i <> 0
        then begin
                 str(i,s);
                messageDlg ('Erro',mtError, [mbOK], 0) ;
                edit1.Text := 'Erro:'+ s;
        end
        else begin
                edit1.Text := 'c�digo interno:'+ copy(b,2,8);
        end;
end
else begin
          messageDlg('A senha de acesso deve conter 9 bytes',mtError, [mbOK], 0) ;
end;

end;


procedure TFproteq.Button2Click(Sender: TObject);
var
intercambio:string;
       i : integer;
       b : array[1..255] of char;
       c : pchar;

begin
c := @b;
intercambio := chr(5) + codigo.Text;
StrPcopy(c, intercambio);
i := c500(c);
IF i<>0 then messageDlg('Erro',mtError, [mbOK], 0) ;
Application.Terminate;
end;



procedure TFproteq.Button3Click(Sender: TObject);
var
intercambio : string;
    memoria : string;
          i : integer;
     inicio : integer;
        fim : integer;
   endereco : integer;
          b : array[1..255] of char;
          c : pchar;

begin
c := @b;   (* ponteiro para a �rea reservada*)
inicio:= 0;
fim := 7;
endereco := inicio;
memoria := '';
while endereco <= fim do begin
intercambio := chr(1) + 'xx' + chr(endereco);
StrPcopy(c, intercambio);
i := c500(c);
IF i<>0 then messageDlg('Erro Lendo:',mtError, [mbOK], 0) ;

memoria := memoria + copy(b,2,2) ;
edit2.Text := memoria ;

endereco := endereco + 1;
end;
end;


procedure TFproteq.Button4Click(Sender: TObject);
  var
    endereco:integer;
    intercambio : string;
    inicio :integer;
    fim:integer;
    i:integer;
    memoria:string;
       b : array[1..255] of char;
       c : pchar;

 Begin
c := @b;

  memoria := EDIT3.Text;
 IF LENGTH(memoria) = 16 then
   begin
   inicio := 0;
   fim := 7;
   endereco := inicio;

   while endereco <= fim do begin
     intercambio := chr(2) + copy(edit3.text,endereco*2+1,2) + chr(endereco);  (* operacao de gravacao *)
     StrPcopy(c, intercambio);
     i := c500(c);
     IF i<>0 then messageDlg('Erro Gravando',mtError, [mbOK], 0) ;
     memoria := memoria + copy(b,2,2);
     endereco := endereco + 1;
    end ;
 end
  else
  messageDlg('A string deve ser igual a 16 bytes',mtError, [mbOK], 0) ;
  end;


procedure TFproteq.Button5Click(Sender: TObject);

 var
 i:integer;
 tamanho : integer;
 intercambio : string;
 intercambio1 : string;
 original : string;
       b : array[1..255] of char;
       c : pchar;

 Begin
c := @b;

original := Edit4.Text;

Edit5.Text := original;

tamanho := length(original);

IF (tamanho >=11) AND (tamanho<251)
THEN BEGIN
        intercambio := chr(20) + chr(tamanho) + original;
         StrPcopy(c, intercambio);
         i := c500(c);
        IF i<>0
        then messageDlg('Erro:',mtError, [mbOK], 0) ;

        Edit5.Text := copy(b, 3, tamanho);

        intercambio1 := chr(21) + chr(tamanho) + Edit5.Text;
         StrPcopy(c, intercambio1);
         i := c500(c);

        IF i<>0
        then messageDlg('Erro:',mtError, [mbOK], 0) ;

        Edit6.Text := copy(b, 3, tamanho);
end
else messageDlg('A string deve possuir um tamanho entre 12 e 251 bytes',mtError, [mbOK], 0) ;


end;

procedure TFproteq.Button6Click(Sender: TObject);
  var
   senha : string;
       i : integer;
       b : array[0..500] of char;
       c : pchar;

begin
      c := @b;
      senha := chr(11)  ;
      StrPcopy(c, senha);
      i := c500(c);

      if i <> 0
      then begin
                messageDlg ('Erro',mtError, [mbOK], 0) ;
      end
      else begin
                  Memo1.Lines.Text := Copy(b,2,480);
                  Memo1.Lines.savetofile('teste.txt');
      end;

end;

procedure TFproteq.Button7Click(Sender: TObject);
    var
       i : integer;
       b : array[0..500] of char;
       c : pchar;
begin

      c := @b;
      StrPcopy(c, chr(12)+ Memo1.Lines.Text);
   
      i := c500(c);

      if i <> 0 then begin
      messageDlg ('Erro',mtError, [mbOK], 0) ;
      end else begin

      messageDlg ('OK',mtInformation,[mbOK],0);
      end;
      end;

procedure TFproteq.Button8Click(Sender: TObject);
var

   senha : string;
       i : integer;
       b : array[0..255] of char;
       c : pchar;
       s : string;
begin
c := @b;

 senha := chr(25) + 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx';

StrPcopy(c, senha);
i := c500(c);

if i <> 0 then begin
   str(i,s);
   messageDlg ('Erro',mtError, [mbOK], 0) ;
   edit1.Text := 'Erro:'+ s;
end else begin
   edit7.Text := copy(b,2,2)+'/'+copy(b,4,2)+'/'+copy(b,6,2);
   edit8.Text := copy(b,8,2)+'/'+copy(b,10,2)+'/'+copy(b,12,2);
   edit9.Text := copy(b,14,5);

end;
end;

procedure TFproteq.Button9Click(Sender: TObject);
var

   senha : string;
       i : integer;
       b : array[0..255] of char;
       c : pchar;
       s : string;
begin
c := @b;

 senha := chr(26) + 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx';

StrPcopy(c, senha);
i := c500(c);

if i <> 0 then begin
   str(i,s);
   messageDlg ('Erro',mtError, [mbOK], 0) ;
   edit1.Text := 'Erro:'+ s;
end else begin
   edit7.Text := copy(b,2,2)+'/'+copy(b,4,2)+'/'+copy(b,6,2);
   edit8.Text := copy(b,8,2)+'/'+copy(b,10,2)+'/'+copy(b,12,2);
   edit9.Text := copy(b,14,5);

end;
end;


function TFproteq.ValidaPlugue(var Pmensagemextra:string;PfraseMemoria:String;var PmodoDemo:boolean): boolean;
var

   senha : string;
original : string;
       cont,i : integer;
       b : array[0..500] of char;
       c : pchar;
       ptemp,ptempcompleta,ponto,s : string;

begin

b:='';
PmodoDemo:=False;
result:=False;
c := @b;

//senha '11N49ZM06'
senha := chr(3) +Self.DesincriptaSenha('�˲�ɟ���');


if Length(senha) = 10
then begin
       // b:='';
        ponto:='a';
        StrPcopy(c, senha);
Try
        i := c500(c);
        //showmessage(b);

        if i <> 0
        then begin
                str(i,s);
                Pmensagemextra:= 'Erro:'+ s;
        end
        else begin
                 c := @b;
                 ponto:='b';
                 senha := chr(11)  ;
                 ponto:='c';
                 StrPcopy(c, senha);
                 i := c500(c);

                 if i <> 0
                 then begin
                          Pmensagemextra:= 'Erro:'+ inttostr(i);
                          showmessage('erro');
                 end
                 else begin
                           Try
                              ptemp:=Copy(b,2,480);
                              ptempcompleta:=trim(ptemp);
                              ptemp:=trim(ptemp);
                              //showmessage(ptemp);

                              if (length(ptemp)=length(PFRASEMEMORIA))
                              Then Begin
                                        if (ptemp=PFRASEMEMORIA)
                                        Then result:=true
                                        Else Pmensagemextra:= 'ERRO NA MEMORIA';
                              End
                              Else Begin
                                        if (length(ptemp)<length(PFRASEMEMORIA))
                                        Then Pmensagemextra:= 'ERRO NA MEMORIA'
                                        Else Begin
                                                  //o tamanho da mensagem � maior, ent�o posso ter a frase do DEMO
                                                  //showmessage(ptemp);
                                                  ptemp:=copy(ptemp,1,length(Pfrasememoria));

                                                  //showmessage(pfrasememoria);

                                                  if (POS(Pfrasememoria,ptempcompleta)>0)
                                                  Then result:=True;

                                                  if (pos('_DEMO',ptempcompleta)>0)
                                                  Then PmodoDemo:=true;


                                        End;
                              End;
                           Except
                                 on e:exception do
                                 Begin
                                      showmessage(e.message);
                                 End;
                           End;
                 end;
        end;
Except
      on e:exception do
      Begin
            PmensagemExtra:=E.message;
      End;
End;

end;
End;


Function TfProteq.TrocaLetra(Original:string):string;
var
cont:integer;
palavra:string;
Begin
 palavra:='';
 palavra:=original;

 for cont:=1 to length(palavra) do
 Begin
        case palavra[cont] of

        '1': palavra[cont]:='5';
        '5': palavra[cont]:='1';

        '2': palavra[cont]:='3';
        '3': palavra[cont]:='2';

        '4': palavra[cont]:='6';
        '6': palavra[cont]:='4';

        '7': palavra[cont]:='9';
        '9': palavra[cont]:='7';
        'a': palavra[cont]:='Z';
        'Z': palavra[cont]:='a';
        'e': palavra[cont]:='X';
        'X': palavra[cont]:='e';
        'i': palavra[cont]:='R';
        'R': palavra[cont]:='i';
        'o': palavra[cont]:='K';
        'K': palavra[cont]:='o';
        'u': palavra[cont]:='H';
        'H': palavra[cont]:='u';
        End;

 End;
 result:=palavra;

End;


Function TfProteq.DesincriptaSenha(original:string):string;
var
final:string;
cont:integer;
palavra:string;
begin
     final:='';
     palavra:='';
     palavra:=original;
     for cont:=1 to length(palavra) do
     begin
	        final:=final+chr(256-ord(copy(palavra,cont,1)[1]));
     end;

     palavra:='';
     palavra:=trocaletra(final);

     result:=palavra;

end;


end.
