object FBACKUP: TFBACKUP
  Left = 457
  Top = 189
  Width = 1082
  Height = 666
  Caption = 
    'Cadastro de Backups e Execu'#231#227'o de Lotes de Sqls - EXCLAIM TECNOL' +
    'OGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 1066
    Height = 628
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    OnChange = GuiaChange
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1-Principal'
      object Panel1: TPanel
        Left = 0
        Top = 510
        Width = 1058
        Height = 90
        Align = alBottom
        Color = 3355443
        TabOrder = 0
        object Btnovo: TBitBtn
          Left = 3
          Top = 6
          Width = 108
          Height = 38
          Caption = '&Novo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = BtnovoClick
        end
        object btalterar: TBitBtn
          Left = 115
          Top = 6
          Width = 108
          Height = 38
          Caption = '&Alterar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnClick = btalterarClick
        end
        object btgravar: TBitBtn
          Left = 227
          Top = 6
          Width = 108
          Height = 38
          Caption = '&Gravar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnClick = btgravarClick
        end
        object btcancelar: TBitBtn
          Left = 339
          Top = 6
          Width = 108
          Height = 38
          Caption = '&Cancelar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnClick = btcancelarClick
        end
        object btexcluir: TBitBtn
          Left = 3
          Top = 46
          Width = 108
          Height = 38
          Caption = '&Excluir'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          OnClick = btexcluirClick
        end
        object btsair: TBitBtn
          Left = 339
          Top = 46
          Width = 108
          Height = 38
          Caption = '&Sair'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
          OnClick = btsairClick
        end
        object btexecutarbackup: TBitBtn
          Left = 115
          Top = 46
          Width = 108
          Height = 38
          Caption = 'E&xecutar Backup'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
          OnClick = btexecutarbackupClick
        end
        object btrestaurabackup: TBitBtn
          Left = 227
          Top = 46
          Width = 108
          Height = 38
          Caption = 'Resta&urar Backup'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 7
          OnClick = btrestaurabackupClick
        end
      end
      object DBGrid: TDBGrid
        Left = 0
        Top = 316
        Width = 1058
        Height = 194
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Verdana'
        TitleFont.Style = []
        OnDblClick = DBGridDblClick
        OnKeyPress = DBGridKeyPress
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1058
        Height = 316
        Align = alTop
        TabOrder = 2
        object Lbsenha: TLabel
          Left = 13
          Top = 210
          Width = 40
          Height = 13
          Caption = 'Senha'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Lbusuario: TLabel
          Left = 13
          Top = 168
          Width = 50
          Height = 13
          Caption = 'Usu'#225'rio'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Lbdestino: TLabel
          Left = 13
          Top = 127
          Width = 49
          Height = 13
          Caption = 'Destino'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LbOrigem: TLabel
          Left = 13
          Top = 86
          Width = 47
          Height = 13
          Caption = 'Origem'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LbNome: TLabel
          Left = 13
          Top = 45
          Width = 37
          Height = 13
          Caption = 'Nome'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LbCODIGO: TLabel
          Left = 13
          Top = 4
          Width = 44
          Height = 13
          Caption = 'C'#243'digo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 13
          Top = 250
          Width = 191
          Height = 13
          Caption = 'ID da Base no Site (Opcional)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Edtsenha: TEdit
          Left = 13
          Top = 226
          Width = 500
          Height = 19
          MaxLength = 100
          PasswordChar = '#'
          TabOrder = 7
        end
        object Edtusuario: TEdit
          Left = 13
          Top = 184
          Width = 500
          Height = 19
          MaxLength = 100
          TabOrder = 6
        end
        object Edtdestino: TEdit
          Left = 13
          Top = 142
          Width = 484
          Height = 19
          MaxLength = 1000
          TabOrder = 4
        end
        object EdtNome: TEdit
          Left = 13
          Top = 58
          Width = 500
          Height = 19
          MaxLength = 100
          TabOrder = 1
        end
        object EdtCODIGO: TEdit
          Left = 13
          Top = 20
          Width = 100
          Height = 19
          MaxLength = 9
          TabOrder = 0
        end
        object EdtOrigem: TEdit
          Left = 13
          Top = 100
          Width = 484
          Height = 19
          MaxLength = 1000
          TabOrder = 2
        end
        object btprocurarorigem: TBitBtn
          Left = 499
          Top = 97
          Width = 27
          Height = 25
          TabOrder = 3
          OnClick = btprocurarorigemClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33033333333333333F7F3333333333333000333333333333F777333333333333
            000333333333333F777333333333333000333333333333F77733333333333300
            033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
            33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
            3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
            33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
            333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
            333333773FF77333333333370007333333333333777333333333}
          NumGlyphs = 2
        end
        object btprocurardestino: TBitBtn
          Left = 499
          Top = 138
          Width = 27
          Height = 25
          TabOrder = 5
          OnClick = btprocurardestinoClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33033333333333333F7F3333333333333000333333333333F777333333333333
            000333333333333F777333333333333000333333333333F77733333333333300
            033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
            33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
            3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
            33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
            333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
            333333773FF77333333333370007333333333333777333333333}
          NumGlyphs = 2
        end
        object check_verbose: TCheckBox
          Left = 16
          Top = 292
          Width = 97
          Height = 17
          Caption = 'Verbose'
          TabOrder = 9
        end
        object edtcliente_site: TEdit
          Left = 13
          Top = 266
          Width = 116
          Height = 19
          MaxLength = 100
          TabOrder = 8
        end
        object btselecionaparametro: TButton
          Left = 133
          Top = 265
          Width = 41
          Height = 21
          Caption = '?'
          TabOrder = 10
          OnClick = btselecionaparametroClick
        end
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2-Hor'#225'rios'
      object Label8: TLabel
        Left = 190
        Top = 12
        Width = 292
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = '&Hor'#225'rios'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 0
        Top = 0
        Width = 1058
        Height = 13
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Determine os dias e hor'#225'rios a efetuar o Backup'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 0
        Top = 12
        Width = 183
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = '&Dias'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 4
        Top = 163
        Width = 40
        Height = 14
        Caption = '&Hor'#225'rio'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LBDias_backup: TListBox
        Left = 0
        Top = 28
        Width = 190
        Height = 129
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ItemHeight = 16
        Items.Strings = (
          'Domingo'
          'Segunda'
          'Ter'#231'a'
          'Quarta'
          'Quinta'
          'Sexta'
          'S'#225'bado')
        ParentFont = False
        TabOrder = 0
        OnClick = LBDias_backupClick
        OnKeyPress = LBDias_backupKeyPress
      end
      object LB_Horarios: TListBox
        Left = 192
        Top = 28
        Width = 329
        Height = 307
        ItemHeight = 13
        TabOrder = 1
        OnKeyDown = LB_HorariosKeyDown
      end
      object EdtHorario_backup: TMaskEdit
        Left = 4
        Top = 179
        Width = 57
        Height = 22
        EditMask = '99:99;1;_'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 5
        ParentFont = False
        TabOrder = 2
        Text = '  :  '
      end
      object BtAdicionarHorario_backup: TBitBtn
        Left = 67
        Top = 178
        Width = 90
        Height = 25
        Caption = 'Adicionar >>'
        TabOrder = 3
        OnClick = BtAdicionarHorario_backupClick
      end
      object BtExcluirHorario_backup: TBitBtn
        Left = 67
        Top = 202
        Width = 90
        Height = 24
        Caption = '<<Excluir'
        TabOrder = 4
        OnClick = BtExcluirHorario_backupClick
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&3 - Execu'#231#227'o de Sqls'
      inline FrScriptSqlLOTE1: TFrScriptSqlLOTE
        Left = 0
        Top = 0
        Width = 1058
        Height = 600
        Align = alClient
        Constraints.MinHeight = 600
        Constraints.MinWidth = 800
        TabOrder = 0
        inherited PanelBotoes: TPanel
          Width = 1058
          inherited Label1: TLabel
            Width = 30
          end
          inherited Label2: TLabel
            Width = 59
          end
          inherited Panelbotoesbaixo: TPanel
            Width = 1056
            inherited lblocalizar: TLabel
              Width = 71
            end
            inherited lblocalizaresubstituir: TLabel
              Width = 119
            end
          end
          inherited PanelBotosCima: TPanel
            Width = 1056
            inherited btexecutasqllote: TBitBtn
              OnClick = FrScriptSqlLOTE1btexecutasqlloteClick
            end
          end
          inherited PanelBotoesMeio: TPanel
            Width = 1056
            inherited Panel2: TPanel
              Width = 551
              inherited Splitter1: TSplitter
                Left = 391
              end
              inherited Memoerro: TMemo
                Width = 391
              end
              inherited memoanotacoes: TMemo
                Left = 396
              end
            end
          end
        end
        inherited StatusBar1: TStatusBar
          Width = 1058
        end
        inherited Panel3: TPanel
          Width = 1058
          inherited DBGridScript: TDBGrid
            Width = 1058
            TitleFont.Color = clNavy
            TitleFont.Name = 'Verdana'
          end
          inherited Panel4: TPanel
            Width = 1058
            inherited Panel5: TPanel
              Width = 1058
              inherited LbScript: TListBox
                Width = 1056
              end
              inherited memo_script: TSynMemo
                Width = 1056
              end
            end
          end
        end
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&4 - Descompacta'
      object DriveComboBoxDescompacta: TDriveComboBox
        Left = 0
        Top = 7
        Width = 265
        Height = 19
        DirList = DirectoryListBoxDescompacta
        TabOrder = 0
      end
      object DirectoryListBoxDescompacta: TDirectoryListBox
        Left = 0
        Top = 31
        Width = 262
        Height = 139
        FileList = FileListBoxDescompacta
        ItemHeight = 16
        TabOrder = 1
      end
      object FileListBoxDescompacta: TFileListBox
        Left = 264
        Top = 31
        Width = 181
        Height = 138
        ItemHeight = 13
        Mask = '*.zip'
        TabOrder = 2
      end
      object btdescompactatodos: TButton
        Left = 0
        Top = 176
        Width = 121
        Height = 25
        Caption = 'Descompacta Todos'
        TabOrder = 3
        OnClick = btdescompactatodosClick
      end
      object btdescompactaselecionado: TButton
        Left = 128
        Top = 176
        Width = 169
        Height = 25
        Caption = 'Descompacta Selecionado'
        TabOrder = 4
        OnClick = btdescompactaselecionadoClick
      end
    end
  end
  object OpenDialog: TOpenDialog
    Filter = 'Firebird|*.fdb;*.gdb'
    Left = 748
    Top = 64
  end
  object IBRestoreService1: TIBRestoreService
    TraceFlags = []
    PageBuffers = 0
    Left = 711
    Top = 64
  end
  object SaveDialog: TSaveDialog
    Filter = 'Firebird|*.fdb;*.gdb'
    Left = 676
    Top = 64
  end
end
