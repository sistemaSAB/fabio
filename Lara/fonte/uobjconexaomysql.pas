unit uobjconexaomysql;

interface

uses Grids, IdFTP, classes,db,IBCustomDataSet, ibscript,useg,ZAbstractRODataset,ZAbstractDataset, ZDataset, ZConnection,sysutils,ibquery,ibdatabase;

Type
    TObjConexaoMysql=class
    public

        ZConnection: TZConnection;
        ObjDataSourceSqlDireto:TDataSource;
        ObjquerySqlDireto:TZQuery;
        

        constructor create(caminhobanco,usuario,senha:string);
        destructor free;

        Function Conecta:boolean;
        Function Conectado:boolean;

        Function PesquisaCliente(Pcliente:string;Pquery:TZQuery;Psoftware:string):boolean;overload;
        Function PesquisaCliente(Pcliente:string;Pquery:TZQuery;Grid:TstringGrid;Psoftware:string):boolean;overload;

        Function InsereRequisicao(Pinstrucao:integer;pvalor,pidbase:String;Pquery:Tzquery):Boolean;
        function VerificaRequisicoes_baseatual(pidbase: String;Pquery: Tzquery): Boolean;

        Function Preenchesoftwares(Software:Tstrings;NomeSoftware:TStrings):boolean;

    private

           Zquery:TZquery;
           Zquery2:TZquery;
           FTP:TIdFtp;




           IBDatabase: TIBDatabase;
           IBTransaction: TIBTransaction;

           ObjQuery:Tibquery;
           IBScript: TIBScript;
           IBDataSetScript: TIBDataSet;
           ObjdatasourceScript:Tdatasource;

           SL:TSTringList;

           


    end;

implementation

uses UUtils, UdataMOdulo, httpsend;

{ TObjConexaoMysql }

function TObjConexaoMysql.Conecta: boolean;
begin
     result:=false;
     Try
        With ZConnection do
        Begin
             HostName:=HostSiteglobal;
             Database:=BdSiteglobal;
             Port:=strtoint(portasiteglobal);
             User:=UsuarioSiteglobal;
             Password:=SenhaSiteglobal;
             AutoCommit:=true;
             LoginPrompt:=False;
             Protocol:=ProtocoloSiteglobal;
             Connect;
             result:=true;
        End;
     Except
           on e:exception do
           Begin
                MensagemErro('Erro na tentativa de se conectar'+#13+E.message);
                exit;
           End;


     End;
end;

function TObjConexaoMysql.Conectado: boolean;
begin
     Result:=Self.ZConnection.Connected;
end;

constructor TObjConexaoMysql.create(caminhobanco,usuario,senha:string);
begin
     Self.ZConnection:=TZConnection.create(nil);
     Self.Zquery:=TZquery.Create(nil);
     Self.Zquery.Connection:=Self.ZConnection;

     Self.Zquery2:=TZquery.Create(nil);
     Self.Zquery2.Connection:=Self.ZConnection;

     Self.IBTransaction:=TIBTransaction.create(nil);
     Self.IBTransaction.DefaultAction:=TARollback;
     Self.IBTransaction.Params.add('read_committed');
     Self.IBTransaction.Params.add('rec_version');
     Self.IBTransaction.Params.add('nowait');


     Self.IBDatabase:=TIBDatabase.create(nil);
     Self.IBDatabase.SQLDialect:=3;
     Self.IBDatabase.LoginPrompt:=False;
     Self.IBDatabase.DefaultTransaction:=Self.IBTransaction;

     Self.Objquery:=tibquery.create(nil);
     Self.ObjQuery.database:=Self.Ibdatabase;

     Self.IBScript:=TIBScript.create(nil);
     Self.IBDataSetScript:=TIBDataSet.create(nil);



     Self.ObjdatasourceScript:=TDataSource.Create(nil);
     Self.ObjdatasourceScript.DataSet:=Self.IBDataSetScript;


     Self.ObjquerySqlDireto:=TZquery.create(nil);
     Self.ObjquerySqlDireto.Connection:=Self.ZConnection;
     
     Self.ObjDataSourceSqlDireto:=TDataSource.create(nil);
     Self.ObjDataSourceSqlDireto.DataSet:=Self.ObjquerySqlDireto;




       Self.SL:=TSTringList.create;
       Self.FTP:=TIdFTP.Create(nil);
end;



destructor TObjConexaoMysql.free;
begin
     Try
         freeandnil(Self.ZConnection);
     Except
     End;

     Try
         freeandnil(Self.Zquery);
     Except
     End;

     Try
         freeandnil(Self.Zquery2);
     Except
     End;


     Try
        freeandnil(Self.IBTransaction);
     Except
     End;

     Try
        freeandnil(Self.IBDatabase);
     Except
     End;

     Try
        freeandnil(Self.Objquery);
     Except
     End;

     Try
        freeandnil(Self.ibDATASETScript);
     Except
     End;

     Try
        freeandnil(Self.IBScript);
     Except
     End;

     Try
        freeandnil(Self.ObjdatasourceScript);
     Except

     End;

     Try
        freeandnil(Self.SL)
     Except

     End;

     Try
        freeandnil(Self.FTP);
     Except

     End;

     Try
        Freeandnil(Self.ObjDataSourceSqlDireto);
     Except
     End;

     Try
        Freeandnil(Self.ObjquerySqlDireto);
     Except
     End;

end;

function TObjConexaoMysql.PesquisaCliente(Pcliente:string;Pquery:TZQuery;Grid:TstringGrid;Psoftware:string):boolean;
Begin
     result:=False;

     if (Self.PesquisaCliente(Pcliente,Pquery,Psoftware)=False)
     Then exit;

     try

       Pquery.first;

       grid.ColCount:=4;
       grid.rowcount:=1;
       grid.cells[0,0]:='X';
       grid.cells[1,0]:='ID Base';
       grid.cells[2,0]:='Nome Base';
       grid.cells[3,0]:='Fantasia';
       grid.FixedCols:=0;
       grid.FixedRows:=0;

       if (Pquery.recordcount=0)
       Then exit;

       grid.rowcount:=2;


       While not(Pquery.eof) do
       Begin
            grid.cells[0,Grid.RowCount-1]:='';
            grid.cells[1,Grid.RowCount-1]:=Pquery.fieldbyname('idbase').asstring;
            grid.cells[2,Grid.RowCount-1]:=Pquery.fieldbyname('base').asstring;
            grid.cells[3,Grid.RowCount-1]:=Pquery.fieldbyname('fantasia').asstring;
            Grid.RowCount:=Grid.RowCount+1;
            Pquery.next;
       End;
       Grid.RowCount:=Grid.RowCount-1;
       grid.FixedRows:=1;
       grid.Fixedcols:=1;

       AjustaLArguraColunaGrid(grid);

     finally
       Pquery.Connection.Commit;
     end


End;


function TObjConexaoMysql.PesquisaCliente(Pcliente: string;
  Pquery: TZQuery;Psoftware:string): boolean;
var
cont:integer;
begin
     if (Self.ZConnection.Connected=False)
     then Begin
               Self.ZConnection.Reconnect;
     End;


     result:=False;
     With Pquery do
     Begin
          cont:=0;
          Repeat
            close;
            sql.clear;
            sql.add('Select exclaim_cliente_base.id as IdBase,');
            sql.add('exclaim_cliente.nome_fantasia as Fantasia,');
            sql.add('exclaim_cliente.cpf_cnpj as Cnpj,');
            sql.add('exclaim_cliente_base.nome as Base');
            sql.add('from exclaim_cliente');
            sql.add('join exclaim_cliente_base on exclaim_cliente.id=exclaim_cliente_base.id_cliente');
            sql.add('where exclaim_cliente.id<>-100');
            sql.add('and exclaim_cliente.nome_fantasia like ''%'+pcliente+'%''');

            if (Psoftware<>'0')
            Then sql.add('and exclaim_cliente_base.id_software='+psoftware);
            Try
                open;
                cont:=2;
                result:=True;
            Except
                  on e:exception do
                  Begin
                       if (cont=2)
                       then mensagemerro(e.Message);

                       inc(cont,1);
                       Self.Conecta;
                  End;
            End;
        Until(cont=2);
     End;

     if Result = False then
      Pquery.Connection.Commit;
end;



function TObjConexaoMysql.InsereRequisicao(Pinstrucao: integer; pvalor,
  pidbase: String; Pquery:Tzquery): Boolean;
begin
     Result:=false;

     With Pquery do
     begin
          close;
          sql.clear;
          sql.add('insert into exclaim_lara_instrucoes(base,instrucao,valor,atualizado) values(');
          sql.add(pidbase+','+inttostr(pinstrucao)+','+#39+pvalor+#39+',''N'')');

          Try
              ExecSQL;
              result:=True;
          Except
                on e:exception do
                Begin
                     Mensagemerro(e.message);
                End;
          End;
     End;
     
end;

function TObjConexaoMysql.VerificaRequisicoes_baseatual(pidbase: String; Pquery:Tzquery): Boolean;
begin
     Result:=false;

     With Pquery do
     begin
          close;
          sql.clear;
          sql.add('Select id,atualizado,erro,datahora,msg,instrucao,base,valor from  exclaim_lara_instrucoes where base='+pidbase);
          sql.add('order by atualizado,datahora desc,id desc');
          Try
              open;
              first;
              result:=True;
          Except
                on e:exception do
                Begin
                     Mensagemerro(e.message);
                End;
          End;
     End;
     
end;





function TObjConexaoMysql.Preenchesoftwares(Software,
  NomeSoftware: TStrings): boolean;
begin
     result:=False;
     software.clear;
     NomeSoftware.clear;
     
     With Self.Zquery do
     begin
          Self.Conecta;
          
          close;
          sql.clear;
          sql.add('Select id,nome from exclaim_software order by id');
          Try
              open;

              software.add('0');
              nomesoftware.add('TODOS');

              While not(eof) do
              Begin
                   software.add(fieldbyname('id').asstring);
                   nomesoftware.add(fieldbyname('nome').asstring);
                   next;
              End;

              result:=true;
          Except
                on e:exception do
                Begin
                    mensagemerro(e.message);
                    exit;
                End;
          End;



     end;
end;

end.
