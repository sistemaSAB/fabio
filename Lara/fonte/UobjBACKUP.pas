unit UobjBACKUP;
Interface
Uses forms,extctrls,Ibquery,windows,stdctrls,Classes,Db,IBStoredProc,ibdatabase,uutils,ibservices, UmostraBackup;
//USES_INTERFACE


Type
   TObjBACKUP=class

          Public
                TimerBackup: TTimer;
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                IBDatabase: TIBDatabase;
                IBTransaction: TIBTransaction;

                Lbbackup:tlabel;
                HorariosBackup:String;
                Indice:String;

//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create(Pdatabase:TibDatabase;PTransaction:TibTransaction);overload;
                Constructor Create(caminhobanco:string);overload;


                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:Str09) :boolean;
                Function    Exclui(Pcodigo:str09;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :Str100;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:Str09;
                Function  RetornaCampoCodigo:Str100;
                Function  RetornaCampoNome:Str100;

                Procedure Submit_CODIGO(parametro: STR09);
                Function Get_CODIGO: STR09;

                Function Get_verbose:str01;
                Procedure Submit_verbose(parametro:str01);

                Procedure Submit_Nome(parametro: STR100);
                Function Get_Nome: STR100;
                Procedure Submit_Origem(parametro: String);
                Function Get_Origem: String;
                Procedure Submit_destino(parametro: String);
                Function Get_destino: String;
                Procedure Submit_usuario(parametro: STR100);
                Function Get_usuario: STR100;
                Procedure Submit_senha(parametro: STR100);
                Function Get_senha: STR100;
                Function Get_cliente_site:str09;
                Procedure Submit_cliente_site(parametro:str09);
                Function Get_DataAtual: String;
                Procedure Submit_DataAtual(parametro: String);
                Function Get_DataVencimento: String;
                Procedure Submit_DataVencimento(parametro: String);
                Function Get_Verificador: String;
                Procedure Submit_Verificador(parametro: String);

                //CODIFICA DECLARA GETSESUBMITS

                Procedure Pesquisabackups;
                Function IniciaBackup:Boolean;overload;
                Function IniciaBackup(PArquivo:string;var msg:String):boolean;overload;
                procedure LbbackupClick(Sender: TObject);
                procedure EnviaResultadoSite(pCodigoBase, pVersaoBase, pdata, pHora, pVersaoLara, pMSG, pURL:String; pErro:boolean; pTempoEspera:integer);

         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               IBBackupService: TIBBackupService;
               TransacaoDatabaseExterna:Boolean;
               FmostraBackup:TFmostraBackup;
               pmostraverbose:boolean;


               CODIGO:STR09;
               Nome:STR100;
               Origem:String;
               destino:string;
               usuario:STR100;
               senha:STR100;
               verbose:str01;
               cliente_site:str09;
               dataatual:string;
               datavencimento:string;
               verificador:string;

               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
               Procedure Cria;
               procedure TimerBackupTimer(Sender: TObject);
    function get_campoTabela(campoSelect, campoWhere, nomeTabela,
      parametro, alias: string): string;

   End;


implementation
uses SysUtils,Dialogs,Controls, uobjBackupSite, UdataMOdulo;

Function  TObjBACKUP.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.Nome:=fieldbyname('Nome').asstring;
        Self.Origem:=fieldbyname('Origem').asstring;
        Self.destino:=fieldbyname('destino').asstring;
        Self.usuario:=fieldbyname('usuario').asstring;
        Self.verbose:=fieldbyname('verbose').asstring;
        Self.senha:=Uutils.DesincriptaSenha(fieldbyname('senha').asstring);
        Self.cliente_site:=FieldByName('cliente_site').asstring;
        Self.dataatual:=FieldByName('dataatual').asstring;
        Self.datavencimento:=FieldByName('datavencimento').asstring;
        Self.verificador:=FieldByName('verificador').asstring;
//CODIFICA TABELAPARAOBJETO
        result:=True;
     End;
end;


Procedure TObjBACKUP.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Nome').asstring:=Self.Nome;
        ParamByName('Origem').asstring:=Self.Origem;
        ParamByName('destino').asstring:=Self.destino;
        ParamByName('usuario').asstring:=Self.usuario;
        ParamByName('verbose').asstring:=Self.verbose;
        ParamByName('senha').asstring:=uutils.EncriptaSenha(Self.senha);
        ParamByName('cliente_site').asstring:=Self.cliente_site;
        ParamByName('dataatual').asstring:=Self.dataatual;
        ParamByName('datavencimento').asstring:=Self.datavencimento;
        ParamByName('verificador').asstring:=Self.verificador;

//CODIFICA OBJETOPARATABELA
  End;
End;

//***********************************************************************

function TObjBACKUP.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       on e:exception do
       begin
             if (Self.Status=dsInsert)
             Then Messagedlg('Erro na  tentativa de Inserir '+e.Message,mterror,[mbok],0)
             Else Messagedlg('Erro na  tentativa de Editar '+e.Message,mterror,[mbok],0);
             exit;
       end;
 End;

 If ComCommit=True Then
  //Self.IBTransaction.CommitRetaining;
  Self.IBTransaction.Commit;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjBACKUP.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Nome:='';
        Origem:='';
        destino:='';
        usuario:='';
        senha:='';
        verbose:='';
        cliente_site:='';
        dataatual := '';
        datavencimento := '';
        verificador := '';
//CODIFICA ZERARTABELA
     End;
end;

Function TObjBACKUP.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Nome='')
      Then Mensagem:=mensagem+'/Nome';
      If (Origem='')
      Then Mensagem:=mensagem+'/Origem';
      If (destino='')
      Then Mensagem:=mensagem+'/Destino';
      If (usuario='')
      Then Mensagem:=mensagem+'/Usu�rio';

      If (senha='')
      Then Mensagem:=mensagem+'/Senha';

      if (verbose='')
      Then Self.verbose:='N';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjBACKUP.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjBACKUP.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjBACKUP.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA
      if(comebarra(trim(self.dataatual))<>'')
      then begin
            try
                  StrToDate(self.dataatual);
            except
                  Mensagem := Mensagem + 'Data Atual';
            end;
      end;

      if(comebarra(trim(self.datavencimento))<>'')
      then begin
            try
                  StrToDate(self.datavencimento);
            except
                  Mensagem := Mensagem + 'Data Vencimento';
            end;
      end;

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjBACKUP.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        if (self.verbose<>'S') and (self.verbose<>'N')
        then Self.verbose:='N';


        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjBACKUP.LocalizaCodigo(parametro: Str09): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro BACKUP vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Nome,Origem,destino,usuario,senha,verbose,cliente_site');
           SQL.ADD(',dataatual,datavencimento,verificador');
           SQL.ADD(' from  TabBackup');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else
           begin
            Objquery.Transaction.Commit;
            Result:=False;
           end

       End;
end;

procedure TObjBACKUP.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjBACKUP.Exclui(Pcodigo: str09;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True) Then
                  //Self.IBTransaction.CommitRetaining;
                  Self.IBTransaction.Commit;
             End

        Else result:=false;
     Except
           on e:exception do
           begin
                result:=false;
                messagedlg(e.message,mterror,[mbok],0);

           End;
     End;
end;

Constructor TObjBACKUP.Create(Pdatabase:TibDatabase;PTransaction:TibTransaction);
Begin
     Self.IBDatabase:=Pdatabase;
     Self.IBTransaction:=PTransaction;
     Self.TransacaoDatabaseExterna:=true;
     Self.Cria;

End;

constructor TObjBACKUP.create(caminhobanco:string);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin

        Self.IBTransaction:=TIBTransaction.create(nil);
        Self.IBTransaction.DefaultAction:=TARollback;
        Self.IBTransaction.Params.add('read_committed');
        Self.IBTransaction.Params.add('rec_version');
        Self.IBTransaction.Params.add('nowait');
        Self.IBDatabase:=TIBDatabase.create(nil);
        Self.IBDatabase.SQLDialect:=3;
        Self.IBDatabase.LoginPrompt:=False;
        Self.IBDatabase.DefaultTransaction:=Self.IBTransaction;
        Self.IBDatabase.Databasename:=CaminhoBanco;
        //USUARIO PROTECAO
        //SENHA   PROTECAO
        Self.IbDatabase.Params.clear;
        Self.IbDatabase.Params.Add('User_name='+DesincriptaSenha('��������'));
        Self.IbDatabase.Params.Add('password='+DesincriptaSenha('��������'));
        Self.IbDatabase.Open;
        //************************************************************************
        Self.TransacaoDatabaseExterna:=False;
        Self.Cria;
end;

procedure TObjBACKUP.Cria;
begin

        Self.TimerBackup:= TTimer.Create(nil);
        Self.TimerBackup.Enabled:=False;
        Self.TimerBackup.Interval:=10000;
        Self.TimerBackup.OnTimer:=Self.TimerBackupTimer;

        Self.IBBackupService:=TIBBackupService.create(nil);



        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=Self.IbDatabase;

        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=Self.IbDatabase;

        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;


        Self.ObjDatasource:=TDataSource.create(nil);
        Self.ObjDatasource.dataset:=Self.ObjqueryPesquisa;

        Self.FmostraBackup:=TFmostraBackup.create(nil);
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabBackup(CODIGO,Nome,Origem,destino,usuario');
                InsertSQL.add(' ,senha,verbose,cliente_site,dataatual,datavencimento,verificador)');
                InsertSQL.add('values (:CODIGO,:Nome,:Origem,:destino,:usuario,:senha,:verbose,:cliente_site');
                InsertSQL.add(',:dataatual,:datavencimento,:verificador )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabBackup set CODIGO=:CODIGO,Nome=:Nome,Origem=:Origem');
                ModifySQL.add(',destino=:destino,usuario=:usuario,senha=:senha,verbose=:verbose,cliente_site=:cliente_site');
                ModifySQl.Add(',dataatual=:dataatual,datavencimento=:datavencimento,verificador=:verificador');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabBackup where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;


end;



procedure TObjBACKUP.Commit;
begin
     //Self.IBTransaction.CommitRetaining;
    Self.IBTransaction.Commit;
end;

function TObjBACKUP.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabBACKUP');
     Result:=Self.ParametroPesquisa;
end;

function TObjBACKUP.Get_TituloPesquisa: Str100;
begin
     Result:=' Pesquisa de Backup';
end;


function TObjBACKUP.Get_NovoCodigo: Str09;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=Self.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENBACKUP,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENBACKUP,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjBACKUP.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);

    {10/11/2010 - celio - removido. Motivo:repetidos
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    }
    Freeandnil(Self.objdatasource);
    freeandnil(Self.objquerypesquisa);

    if (Self.TransacaoDatabaseExterna=False)
    Then Begin
              freeandnil(Self.ibtransaction);
              freeandnil(Self.IBDatabase);
    End;
    freeandnil(Self.timerbackup);
    Freeandnil(Self.FmostraBackup);

    //10/11/2010 - celio adicionado - faltava desalocar
    Freeandnil(Self.IBBackupService);

    //CODIFICA DESTRUICAO DE OBJETOS
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjBACKUP.RetornaCampoCodigo: Str100;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjBACKUP.RetornaCampoNome: Str100;
begin
      result:='Nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjBackup.Submit_CODIGO(parametro: STR09);
begin
        Self.CODIGO:=Parametro;
end;
function TObjBackup.Get_CODIGO: STR09;
begin
        Result:=Self.CODIGO;
end;
procedure TObjBackup.Submit_Nome(parametro: STR100);
begin
        Self.Nome:=Parametro;
end;
function TObjBackup.Get_Nome: STR100;
begin
        Result:=Self.Nome;
end;
procedure TObjBackup.Submit_Origem(parametro: string);
begin
        Self.Origem:=Parametro;
end;
function TObjBackup.Get_Origem: string;
begin
        Result:=Self.Origem;
end;
procedure TObjBackup.Submit_destino(parametro: string);
begin
        Self.destino:=Parametro;
end;
function TObjBackup.Get_destino: string;
begin
        Result:=Self.destino;
end;
procedure TObjBackup.Submit_usuario(parametro: STR100);
begin
        Self.usuario:=Parametro;
end;
function TObjBackup.Get_usuario: STR100;
begin
        Result:=Self.usuario;
end;
procedure TObjBackup.Submit_senha(parametro: STR100);
begin
        Self.senha:=Parametro;
end;
function TObjBackup.Get_senha: STR100;
begin
        Result:=Self.senha;
end;
//CODIFICA GETSESUBMITS



procedure TObjBACKUP.Pesquisabackups;
begin
     With Self.ObjqueryPesquisa do
     begin
          close;
          sql.clear;
          sql.add('Select codigo,nome,cliente_site,origem,destino from Tabbackup order by codigo');
          open;
     End;
end;

function TObjBACKUP.Get_verbose: str01;
begin
     Result:=Self.verbose;
end;

procedure TObjBACKUP.Submit_verbose(parametro: str01);
begin
     Self.verbose:=Parametro;
end;

procedure TobjBackup.TimerBackupTimer(Sender: TObject);
var
phoraatual:String;
Begin
     phoraatual:=formatdatetime('hh:mm',now);

     if (pos(phoraatual,Self.HorariosBackup)<=0)
     Then exit;

     Try
        Self.TimerBackup.Enabled:=False;
        Self.IniciaBackup;
     Finally
            Self.TimerBackup.Enabled:=true;
     End;
End;

Function TobjBackup.IniciaBackup:boolean;
var
msg:string;
Begin
     result:=Self.IniciaBackup('',msg);
End;

Function TobjBackup.IniciaBackup(PArquivo:string;var msg:String):boolean;
var
phorarioatual:Ttime;
pnomearquivo:string;
erro : Boolean;
UrlSite,IntervaloEspera,VersaoLara : string;
Begin
     msg:='';
     erro := false;
     result:=False;

     UrlSite := '';
     if(ObjparametroGlobal.ValidaParametro('URL RESULTADO BACKUP')) then
       UrlSite := ObjparametroGlobal.Get_Valor;

     IntervaloEspera := '1000';
     if(ObjparametroGlobal.ValidaParametro('INTERVALO DE TEMPO REENVIO RESULTADO BACKUP SITE')) then
       IntervaloEspera := ObjparametroGlobal.Get_Valor;

     VersaoLara := get_campoTabela('versao','codigo','tabversaoexecutavel','0','');

     phorarioatual:=now;
     Pnomearquivo:=Self.get_nome+formatdatetime('yy_mm_dd_hh_mm',phorarioatual);

     Try

        if (Self.Lbbackup<>nil)
        Then Self.Lbbackup.caption:='Executando Backup '+Self.indice+': '+Self.nome+'. Aguarde....';

        Self.FmostraBackup.MemoBackup.lines.clear;
        Self.FmostraBackup.MemoBackup.lines.add('Executando Backup : '+Self.nome);
        Self.FmostraBackup.caption:=Self.Get_Nome;
        Application.ProcessMessages;

        if(Self.Get_verbose='S')
        Then pmostraverbose:=true
        Else pmostraverbose:=False;

        if (PmostraVerbose=true)
        Then Self.FmostraBackup.Show;

        With Self.IBBackupService do
        Begin


             Application.ProcessMessages;
             
             Active:=false;
             LoginPrompt:=FALSE;
   
             DatabaseName:=SeparaPath(Self.Get_Origem);
             protocol:=TCP;
             servername:=SeparaIp(Self.Get_Origem);
             
             if (Servername='')(*se retornar '' � porque � local*)
             Then protocol:=Local;
   
   
             BackupFile.clear;

             if (PArquivo='')
             Then BackupFile.add(Self.Get_destino+PnomeArquivo+'.fbk')
             Else BackupFile.add(Parquivo);

             Params.Clear;
             Params.add('USER_NAME='+Self.Get_usuario);
             Params.add('PASSWORD='+Self.Get_senha);
             Try
   
               Verbose := True;
               Attach;
               Options := [NoGarbageCollection];
               Active:=True;
               ServiceStart;
   
               while not Self.IBBackupService.Eof do
               Begin
                   Self.FmostraBackup.MemoBackup.lines.add(Self.IBBackupService.GetNextLine);

                   if (PmostraVerbose)
                   Then Self.FmostraBackup.show;

                   application.processmessages;
                   if (Self.FmostraBackup.tag=1)
                   Then PmostraVerbose:=false;

                   Try

                      if (PArquivo='')
                      Then Self.FmostraBackup.MemoBackup.lines.SaveToFile(Self.destino+PnomeArquivo+'.log');

                      application.processmessages;
                   Except
                   End;
               End;
   
               Active:=false;
             Except
                   on e:exception do
                   Begin
                        if (parquivo='')
                        Then messagedlg('Erro no backup '+Self.nome+#13+e.message,mterror,[mbok],0);
                        
                        msg:=e.message;
                        erro := True;
                        exit;
                   End;
             End;
        End;

        Try
           if (PArquivo='')
           Then Self.FmostraBackup.MemoBackup.lines.SaveToFile(Self.destino+PnomeArquivo+'.log');
        except
        End;
   
        Self.FmostraBackup.MemoBackup.lines.add('Aguarde....');
        while (formatdatetime('hh:mm',now)=formatdatetime('hh:mm',phorarioatual)) do
        Begin
             (*para evitar que dentro do mesmo minuto o backup inicie mais de uma vez*)
             if (PmostraVerbose=true)
             Then Self.FmostraBackup.Show;

             Application.ProcessMessages;

             if (Self.FmostraBackup.tag=1) 
             Then PmostraVerbose:=false;

        End;

        Result:=true;

     Finally
         Self.FmostraBackup.close;
         EnviaResultadoSite(Get_cliente_site,'',FormatDateTime('YYYY/MM/DD',Now),FormatDateTime('HH:NN:SS',Now),VersaoLara,msg,UrlSite,erro,StrToIntDef(IntervaloEspera,1000));
         if (Self.Lbbackup<>nil)
         Then Self.Lbbackup.caption:='';
     End;

End;

procedure Tobjbackup.LbbackupClick(Sender: TObject);
begin
     Self.pmostraverbose:=True;
     Self.FmostraBackup.tag:=0;
end;


function TObjBACKUP.Get_cliente_site: str09;
begin
     Result:=Self.Cliente_Site;
end;

procedure TObjBACKUP.Submit_cliente_site(parametro: str09);
begin
     Self.Cliente_Site:=Parametro;
end;

function TObjBACKUP.Get_DataAtual: String;
begin
      result := self.dataatual;
end;

function TObjBACKUP.Get_DataVencimento: String;
begin
      result := self.datavencimento;
end;

function TObjBACKUP.Get_Verificador: String;
begin
      result := self.verificador;
end;

procedure TObjBACKUP.Submit_DataAtual(parametro: String);
begin
     self.dataatual := parametro;
end;

procedure TObjBACKUP.Submit_DataVencimento(parametro: String);
begin
     self.datavencimento := parametro;
end;

procedure TObjBACKUP.Submit_Verificador(parametro: String);
begin
     self.verificador := parametro;
end;

procedure TObjBACKUP.EnviaResultadoSite(pCodigoBase, pVersaoBase, pdata, pHora, pVersaoLara, pMSG, pURL:String; pErro:boolean; pTempoEspera:integer);
var
  tBackupSite : tthreadBackupSite;
begin
  tBackupSite := tthreadBackupSite.create(True);
  tBackupSite.FreeOnTerminate := True;
  //carregando parametros
  tBackupSite.CodigoBase := pCodigoBase;
  tBackupSite.VersaoBase := pVersaoBase;
  tBackupSite.data := pdata;
  tBackupSite.Hora := pHora;
  tBackupSite.VersaoLara := pVersaoLara;
  tBackupSite.MSG := pMSG;
  tBackupSite.URL := pURL;
  tBackupSite.Erro := pErro;
  tBackupSite.TempoEspera := pTempoEspera;

  tBackupSite.Resume;
end;

function TObjBACKUP.get_campoTabela (campoSelect, campoWhere, nomeTabela, parametro:string; alias:string):string;
var
  query:TIBQuery;
begin

  result:='';

  if (campoSelect = '') then
    Exit;

  if (parametro = '') then
    Exit;

   try

       query:=TIBQuery.Create (nil);

       try

          with (query) do
          begin

            Database:=FDataModulo.IBDatabaseSenha;

            Close;
            sql.Clear;
            sql.Add ('select '+campoSelect);
            sql.Add ('from '+nomeTabela);
            if(Trim(campoWhere) = '') then
              SQL.Add(parametro)
            else
              if(Copy(parametro,0,1) = #39) then
                sql.Add('where '+campoWhere+'='+parametro)
              else
                sql.Add ('where '+campoWhere+'='+#39+parametro+#39);

            //InputBox('','',sql.Text);
            Open;
            First;

            if (alias <> '') then
              result := fieldbyname (alias).AsString
            else
              result := fieldbyname (campoSelect).AsString


          end;


       finally

          FreeAndNil (query);

       end;

   except
      on e:Exception do
      begin
       raise Exception.Create(e.Message);
       result:='';
      end
   end;


end;
end.



