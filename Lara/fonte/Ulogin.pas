unit Ulogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, Buttons;

type
  TFlogin = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    edtusuario: TMaskEdit;
    edtsenha: TMaskEdit;
    btok: TBitBtn;
    btcancelar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure edtusuarioKeyPress(Sender: TObject; var Key: Char);
    procedure edtsenhaKeyPress(Sender: TObject; var Key: Char);
    procedure btokClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Flogin: TFlogin;

implementation

{$R *.dfm}

procedure TFlogin.FormShow(Sender: TObject);
begin
     Self.tag:=0;
     edtusuario.setfocus;
end;

procedure TFlogin.edtusuarioKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then edtusuario.SetFocus;
end;

procedure TFlogin.edtsenhaKeyPress(Sender: TObject; var Key: Char);
begin
     if (key=#13)
     Then btok.setfocus;
end;

procedure TFlogin.btokClick(Sender: TObject);
begin
     Self.tag:=1;
     Self.close;
end;

procedure TFlogin.btcancelarClick(Sender: TObject);
begin
     Self.tag:=0;
     Self.close;
end;

end.
