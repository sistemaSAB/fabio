object FdataModulo: TFdataModulo
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 843
  Top = 160
  Height = 456
  Width = 791
  object TimerSite: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = TimerSiteTimer
    Left = 9
    Top = 16
  end
  object ZConnection: TZConnection
    Left = 48
    Top = 120
  end
  object ZQuery: TZQuery
    Params = <>
    Left = 120
    Top = 120
  end
  object ZQuery2: TZQuery
    Params = <>
    Left = 216
    Top = 104
  end
  object IBDatabaseSenha: TIBDatabase
    LoginPrompt = False
    DefaultTransaction = IBTransactionSenha
    Left = 48
    Top = 200
  end
  object IBTransactionSenha: TIBTransaction
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 160
    Top = 200
  end
  object ObjQuerySenha: TIBQuery
    Database = IBDatabaseSenha
    Transaction = IBTransactionSenha
    Left = 264
    Top = 200
  end
  object FTP: TIdFTP
    MaxLineAction = maException
    ReadTimeout = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 360
    Top = 248
  end
  object IBScript: TIBScript
    Dataset = IBDataSetScript
    Terminator = ';'
    Left = 320
    Top = 16
  end
  object IBDataSetScript: TIBDataSet
    Left = 264
    Top = 8
  end
  object ZipMasterSite: TZipMaster
    AddOptions = []
    AddStoreSuffixes = [assGIF, assPNG, assZ, assZIP, assZOO, assARC, assLZH, assARJ, assTAZ, assTGZ, assLHA, assRAR, assACE, assCAB, assGZ, assGZIP, assJAR, assEXE, assJPG, assJPEG, ass7Zp, assMP3, assWMV, assWMA, assDVR, assAVI]
    Dll_Load = False
    ExtrOptions = []
    KeepFreeOnAllDisks = 0
    KeepFreeOnDisk1 = 0
    MaxVolumeSize = 0
    PasswordReqCount = 1
    SFXOptions = []
    SFXOverWriteMode = OvrConfirm
    SFXPath = 'DZSFXUS.bin'
    SpanOptions = []
    Trace = False
    Unattended = False
    Verbose = False
    Version = '1.79.08.07'
    VersionInfo = '1.79.08.07'
    Left = 392
    Top = 304
  end
  object ZipMaster: TZipMaster
    AddOptions = []
    AddStoreSuffixes = [assGIF, assPNG, assZ, assZIP, assZOO, assARC, assLZH, assARJ, assTAZ, assTGZ, assLHA, assRAR, assACE, assCAB, assGZ, assGZIP, assJAR, assEXE, assJPG, assJPEG, ass7Zp, assMP3, assWMV, assWMA, assDVR, assAVI]
    Dll_Load = False
    ExtrOptions = []
    KeepFreeOnAllDisks = 0
    KeepFreeOnDisk1 = 0
    MaxVolumeSize = 0
    PasswordReqCount = 1
    SFXOptions = []
    SFXOverWriteMode = OvrConfirm
    SFXPath = 'DZSFXUS.bin'
    SpanOptions = []
    Trace = False
    Unattended = False
    Verbose = False
    Version = '1.79.08.07'
    VersionInfo = '1.79.08.07'
    Left = 68
    Top = 8
  end
  object IBDatabaseSite: TIBDatabase
    LoginPrompt = False
    DefaultTransaction = IBTransactionSite
    Left = 40
    Top = 344
  end
  object IBTransactionSite: TIBTransaction
    DefaultDatabase = IBDatabaseSite
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 168
    Top = 344
  end
  object IBDatabaseVencimento: TIBDatabase
    LoginPrompt = False
    DefaultTransaction = IBTransactionVencimento
    Left = 488
    Top = 176
  end
  object IBTransactionVencimento: TIBTransaction
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 488
    Top = 120
  end
  object ibtransactionPesquisa: TIBTransaction
    DefaultDatabase = IBDatabaseSenha
    Left = 680
    Top = 256
  end
  object ibqueryPesquisa: TIBQuery
    Database = IBDatabaseSenha
    Transaction = ibtransactionPesquisa
    Left = 680
    Top = 312
  end
end
