object Fsuporte: TFsuporte
  Left = 269
  Top = 87
  Width = 1278
  Height = 769
  Caption = 'Suporte Exclaim'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1262
    Height = 177
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 13
      Top = 3
      Width = 32
      Height = 13
      Caption = 'Cliente'
    end
    object Label3: TLabel
      Left = 550
      Top = 33
      Width = 24
      Height = 13
      Caption = 'Erros'
    end
    object edtcliente: TEdit
      Left = 13
      Top = 18
      Width = 260
      Height = 21
      TabOrder = 0
      OnKeyPress = edtclienteKeyPress
    end
    object btconecta: TButton
      Left = 952
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Conecta'
      TabOrder = 1
      OnClick = btconectaClick
    end
    object GridCliente: TStringGrid
      Left = 8
      Top = 55
      Width = 529
      Height = 124
      TabOrder = 2
      OnDblClick = GridClienteDblClick
      OnKeyPress = GridClienteKeyPress
    end
    object DBGrid1: TDBGrid
      Left = 544
      Top = 55
      Width = 481
      Height = 121
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object CheckSeleciona: TCheckBox
      Left = 14
      Top = 38
      Width = 97
      Height = 17
      Caption = 'Seleciona Todos'
      TabOrder = 4
      OnClick = CheckSelecionaClick
    end
    object combonomesoftware: TComboBox
      Left = 280
      Top = 19
      Width = 113
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 5
      Text = 'Seleciona Todos'
      Items.Strings = (
        'Seleciona Todos')
    end
    object combosoftware: TComboBox
      Left = 395
      Top = 19
      Width = 131
      Height = 21
      ItemHeight = 13
      TabOrder = 6
      Text = '0'
      Visible = False
    end
    object Button2: TButton
      Left = 1032
      Top = 144
      Width = 161
      Height = 25
      Caption = 'SQL MYSQL DIRETO'
      TabOrder = 7
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 1032
      Top = 104
      Width = 153
      Height = 25
      Caption = 'Geral'
      TabOrder = 8
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 1048
      Top = 24
      Width = 161
      Height = 41
      Caption = 'Configura'#231#245'es de Valida'#231#245'es'
      TabOrder = 9
      OnClick = Button4Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 177
    Width = 1262
    Height = 185
    Align = alTop
    Caption = 'Panel2'
    TabOrder = 1
    object Label2: TLabel
      Left = 5
      Top = 5
      Width = 25
      Height = 13
      Caption = 'A'#231#227'o'
    end
    object LbBase: TLabel
      Left = 560
      Top = 24
      Width = 36
      Height = 13
      Caption = 'LbBase'
    end
    object memosql: TSynMemo
      Left = 1
      Top = 50
      Width = 1260
      Height = 134
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      TabOrder = 0
      Gutter.Font.Charset = DEFAULT_CHARSET
      Gutter.Font.Color = clWindowText
      Gutter.Font.Height = -11
      Gutter.Font.Name = 'Courier New'
      Gutter.Font.Style = []
      Lines.Strings = (
        'memosql')
    end
    object btok: TButton
      Left = 369
      Top = 17
      Width = 75
      Height = 25
      Caption = '&Executar'
      TabOrder = 1
      OnClick = btokClick
    end
    object comboacao: TComboBox
      Left = 5
      Top = 21
      Width = 353
      Height = 21
      ItemHeight = 13
      TabOrder = 2
      Items.Strings = (
        '01-executa um comando sql e retorna pelo MYSQL'
        '02-executa um comando sql e retorna pelo FTP em ZIP'
        
          '03-executa um comando sql no senha.fdb (LARA) e retorna pelo MyS' +
          'ql'
        
          '04-executa um comando sql no senha.fdb(lara) e retorna pelo FTP ' +
          'em ZIP'
        '05-Recarrega Parametros'
        '06-Executa Backup e envia para o site'
        '07-Baixa nova vers'#227'o do Lara'
        '08-Baixa arquivos e salva em uma pasta do Lara'
        '09-Acessar Diret'#243'rio'
        '10-Lista Arquivos de um Diret'#243'rio')
    end
  end
  object DbGridrequisicao: TDBGrid
    Left = 0
    Top = 362
    Width = 1262
    Height = 123
    Align = alTop
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DbGridrequisicaoDblClick
  end
  object STRGResultado: TStringGrid
    Left = 0
    Top = 485
    Width = 1262
    Height = 246
    Align = alClient
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 3
  end
  object Button1: TButton
    Left = 480
    Top = 192
    Width = 75
    Height = 25
    Caption = 'Refresh'
    TabOrder = 4
    OnClick = Button1Click
  end
  object TimerRequisicao: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = TimerRequisicaoTimer
    Left = 728
    Top = 16
  end
end
