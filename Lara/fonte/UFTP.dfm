object FFTP: TFFTP
  Left = 203
  Top = 111
  Width = 696
  Height = 613
  Caption = 'FTP'
  Color = clInactiveBorder
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btEnviar: TBitBtn
    Left = 324
    Top = 184
    Width = 37
    Height = 25
    Hint = 'Enviar arquivo para a '#225'rea remota'
    Caption = '>>'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnClick = btEnviarClick
  end
  object btBaixar: TBitBtn
    Left = 324
    Top = 224
    Width = 37
    Height = 25
    Hint = 'Receber arquivo para a '#225'rea remota'
    Caption = '<<'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clYellow
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = btBaixarClick
  end
  object GroupBox3: TGroupBox
    Left = 5
    Top = 441
    Width = 676
    Height = 140
    Caption = 'Status'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object Gauge: TGauge
      Left = 7
      Top = 15
      Width = 662
      Height = 25
      Progress = 0
    end
    object MemoStatus: TMemo
      Left = 6
      Top = 42
      Width = 663
      Height = 91
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  object btExcluir: TBitBtn
    Left = 324
    Top = 264
    Width = 37
    Height = 25
    Hint = 'Excluir arquivo para a '#225'rea remota'
    Caption = 'X'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnClick = btExcluirClick
  end
  object btCriaPasta: TBitBtn
    Left = 324
    Top = 303
    Width = 37
    Height = 25
    Hint = 'Cria nova Pasta na '#193'rea Remota'
    Caption = 'N'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    OnClick = btCriaPastaClick
  end
  object GroupBox2: TGroupBox
    Left = 366
    Top = 108
    Width = 315
    Height = 327
    Caption = 'Remoto'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    object btAnterior: TButton
      Left = 8
      Top = 16
      Width = 78
      Height = 15
      Caption = 'Anterior'
      TabOrder = 0
      OnClick = btAnteriorClick
    end
    object ListBoxRemoto: TListBox
      Left = 10
      Top = 32
      Width = 311
      Height = 292
      ItemHeight = 14
      TabOrder = 1
      OnDblClick = ListBoxRemotoDblClick
    end
  end
  object GroupBox1: TGroupBox
    Left = 6
    Top = 108
    Width = 312
    Height = 327
    Caption = 'Local'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    object DirectoryListBox: TDirectoryListBox
      Left = 7
      Top = 39
      Width = 137
      Height = 281
      FileList = FileListBox
      ItemHeight = 16
      TabOrder = 0
    end
    object DriveComboBox1: TDriveComboBox
      Left = 7
      Top = 15
      Width = 137
      Height = 20
      DirList = DirectoryListBox
      TabOrder = 1
    end
    object FileListBox: TFileListBox
      Left = 149
      Top = 16
      Width = 155
      Height = 304
      ItemHeight = 14
      TabOrder = 2
    end
  end
  object GroupBox: TGroupBox
    Left = 6
    Top = 4
    Width = 673
    Height = 97
    Caption = 'Configura'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    object Label2: TLabel
      Left = 10
      Top = 17
      Width = 82
      Height = 14
      Caption = 'FTP Host/IP'
    end
    object Label4: TLabel
      Left = 10
      Top = 41
      Width = 36
      Height = 14
      Caption = 'Login'
    end
    object Label5: TLabel
      Left = 10
      Top = 66
      Width = 41
      Height = 14
      Caption = 'Senha'
    end
    object EdtFTP: TEdit
      Left = 96
      Top = 17
      Width = 450
      Height = 20
      TabOrder = 0
      Text = 'ftp.exclaim.com.br'
    end
    object Edtlogin: TEdit
      Left = 96
      Top = 41
      Width = 450
      Height = 20
      TabOrder = 1
      Text = 'exclaim'
    end
    object EdtSenha: TEdit
      Left = 96
      Top = 66
      Width = 450
      Height = 20
      PasswordChar = '*'
      TabOrder = 2
    end
    object btConectar: TBitBtn
      Left = 565
      Top = 16
      Width = 99
      Height = 69
      Caption = '&Conectar'
      TabOrder = 3
      OnClick = btConectarClick
    end
  end
  object FTP: TIdFTPServer
    Bindings = <>
    CommandHandlers = <>
    DefaultPort = 21
    Greeting.NumericCode = 220
    Greeting.Text.Strings = (
      'Indy FTP Server ready.')
    Greeting.TextCode = '220'
    MaxConnectionReply.NumericCode = 0
    MaxConnectionReply.Text.Strings = (
      'Too many connections. Try again later.')
    ReplyExceptionCode = 0
    ReplyTexts = <>
    ReplyUnknownCommand.NumericCode = 500
    ReplyUnknownCommand.Text.Strings = (
      'Syntax error, command unrecognized.')
    ReplyUnknownCommand.TextCode = '500'
    AnonymousAccounts.Strings = (
      'anonymous'
      'ftp'
      'guest')
    SystemType = 'WIN32'
    Left = 336
    Top = 128
  end
end
