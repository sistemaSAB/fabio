object FHORARIOSBACKUP: TFHORARIOSBACKUP
  Left = 109
  Top = 10
  Width = 544
  Height = 439
  Caption = 'Cadastro HORARIOSBACKUP - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 322
    Width = 536
    Height = 90
    Align = alBottom
    Color = 3355443
    TabOrder = 1
    object Btnovo: TBitBtn
      Left = 3
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Novo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 115
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Alterar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 227
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Gravar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 339
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 3
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Pesquisar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 115
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Relat'#243'rios'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object btexcluir: TBitBtn
      Left = 227
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Excluir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 339
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Sair'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btsairClick
    end
  end
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 536
    Height = 322
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = 'Principal'
            object LbCODIGO: TLabel                  
              Left = 13                       
              Top = 100             
              Width = 40                     
              Height = 13                    
              Caption = 'C�digo'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object EdtCODIGO: TEdit
        Left = 48
        Top = 100             
        Width = 100           
        Height = 21          
        MaxLength = 9        
        TabOrder = 0
      end                    
      object Lbbackup: TLabel                  
              Left = 13                       
              Top = 122             
              Width = 40                     
              Height = 13                    
              Caption = 'Backup'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object Edtbackup: TEdit
        Left = 48
        Top = 122             
        Width = 100           
        Height = 21          
        MaxLength = 9        
        TabOrder = 1
        Color = clSkyBlue
        OnExit = edtbackupExit
        OnKeyDown = edtbackupKeyDown
      end                    
      object LbNomebackup: TLabel                  
              Left = 96
              Top = 122             
              Width = 40                     
              Height = 13                    
              Caption = 'Backup'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object LbDomingo: TLabel                  
              Left = 13                       
              Top = 144             
              Width = 40                     
              Height = 13                    
              Caption = 'Domingo'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object EdtDomingo: TEdit
        Left = 56
        Top = 144             
        Height = 21          
        Width =560
        MaxLength =500
        TabOrder = 2
      end                    
      object Lbsegunda: TLabel                  
              Left = 13                       
              Top = 166             
              Width = 40                     
              Height = 13                    
              Caption = 'Segunda'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object Edtsegunda: TEdit
        Left = 56
        Top = 166             
        Height = 21          
        Width =560
        MaxLength =500
        TabOrder = 3
      end                    
      object Lbterca: TLabel                  
              Left = 13                       
              Top = 188             
              Width = 40                     
              Height = 13                    
              Caption = 'Ter�a'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object Edtterca: TEdit
        Left = 40
        Top = 188             
        Height = 21          
        Width =560
        MaxLength =500
        TabOrder = 4
      end                    
      object Lbquarta: TLabel                  
              Left = 13                       
              Top = 210             
              Width = 40                     
              Height = 13                    
              Caption = 'Quarta'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object Edtquarta: TEdit
        Left = 48
        Top = 210             
        Height = 21          
        Width =560
        MaxLength =500
        TabOrder = 5
      end                    
      object Lbquinta: TLabel                  
              Left = 13                       
              Top = 232             
              Width = 40                     
              Height = 13                    
              Caption = 'Quinta'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object Edtquinta: TEdit
        Left = 48
        Top = 232             
        Height = 21          
        Width =560
        MaxLength =500
        TabOrder = 6
      end                    
      object Lbsexta: TLabel                  
              Left = 13                       
              Top = 254             
              Width = 40                     
              Height = 13                    
              Caption = 'Sexta'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object Edtsexta: TEdit
        Left = 40
        Top = 254             
        Height = 21          
        Width =560
        MaxLength =500
        TabOrder = 7
      end                    
      object Lbsabado: TLabel                  
              Left = 13                       
              Top = 276             
              Width = 40                     
              Height = 13                    
              Caption = 'S�bado'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object Edtsabado: TEdit
        Left = 48
        Top = 276             
        Height = 21          
        Width =560
        MaxLength =500
        TabOrder = 8
      end                    
object Label1: TLabel

        Left = 264
        Top = 0
        Width = 37
        Height = 13
        Caption = 'Label1'
      end
      object Edit1: TEdit
        Left = 64
        Top = 64
        Width = 121
        Height = 19
        TabOrder = 0
        Text = 'Edit1'
      end
    end
  end
end
